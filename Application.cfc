component {
	this.name = "SireMobileApplication";

	this.sessionStorage  = "sessionstorage";
	this.sessionCluster = true;
	this.mappings["/cfpayment"] = "/public/sire/models/cfc/cfpayment-master/";

	function onRequestStart(){
		include "/public/sire/configs/paths.cfm";

		if (_SiteMaintenance eq 1) {
			if (structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/coming-soon.cfm')) {
				location("/coming-soon", false);
			}
		} else if(_SiteMaintenance eq 2) {
			if (structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/scheduled-maintenance.cfm')) {
				location("/scheduled-maintenance", false);
			}
		}
	}
}
