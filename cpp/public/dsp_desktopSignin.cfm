﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=640px; initial-scale=.5; maximum-scale=.5;" />

		<title>Customer Preference Portal</title>
        <!--- Google Analytics Code--->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-35693415-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
        
        </script>
    	<cfoutput>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery-1.7.1.min.js"></script>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.alerts.js"></script>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.jgrowl.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jgrowl-theme.js"></script>
	        
			 <style>		
	            <!--- @import url('#rootUrl#/#publicPath#/css/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#publicPath#/css/cpp.css');
	            @import url('#rootUrl#/#publicPath#/css/jquery.alerts.css');
	            @import url('#rootUrl#/#publicPath#/css/jquery.jgrowl.css'); --->
	            @import url('#rootUrl#/#publicPath#/css/jquery.alerts.css');
	            @import url('#rootUrl#/#publicPath#/css/jquery.jgrowl.css');
	            @import url('#rootUrl#/#publicPath#/css/stylelogin.css');
		    </style>	
		</cfoutput>
    </head>	
	<body>	
	<cfoutput>
        <script src="https://apis.google.com/js/client.js?onload=GoogleLogin"></script>
		<script src="https://connect.facebook.net/en_US/all.js"></script>
     </cfoutput> 
    
	<cftry>
	
		<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
			SELECT 
				c.userId_int,
				c.CPP_UUID_vch,
				c.CPP_Template_vch,
				c.MultiplePreference_ti, 
				c.SetCustomValue_ti,
				c.IdentifyGroupId_int,
				c.UpdatePreference_ti, 
				c.VoiceMethod_ti, 
				c.SMSMethod_ti,
				c.EmailMethod_ti, 
				c.IncludeLanguage_ti, 
				c.IncludeIdentity_ti,
				c.StepSetup_vch,
				c.Active_int,
				c.Type_ti,
				c.CPP_Template_vch,
				c.IFrameActive_int,
				c.template_int,
				c.sizeType_int,
				c.width_int,
				c.height_int,
				c.customhtml_txt,
				c.displayType_ti,
				t.templateName_vch,
				t.fontFamily_vch,
				t.fontSize_int,
				t.fontSizeUnit_vch,
				t.fontWeight_vch,
				t.fontStyle_vch,
				t.fontVariant_vch,
				t.backgroundColor_vch,
				t.backgroundImage_vch,
				t.imageRepeat_vch,
				t.borderColor_vch,
				t.borderWidth_int,
				t.borderRadius_int,
				t.fontColor_vch,
				t.customCss_txt
			FROM 
				simplelists.customerpreferenceportal c
			LEFT Join
				simplelists.cpp_template t
			ON
				c.template_int = t.templateId_int
			WHERE
				(
					c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
				OR
					Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
				)
			AND
				c.Active_int > 0
			
		</cfquery>	
		<cfif GetCPPSETUPQUERY.Active_int NEQ 1 OR GetCPPSETUPQUERY.IFrameActive_int NEQ 1>
			This Customer Preference Portal is offline!
			<cfexit>
		</cfif>
		<cfset submitClass = LCase(GetCPPSETUPQUERY.templateName_vch)>
		<cfif submitClass EQ 'plan' OR submitClass EQ ''>
			<cfset submitClass = 'blue'>
		</cfif>
		
		<cfset width = 0>
		<cfset height = 0>
		<cfset containerClass = 'loginform2'>
		<cfset signnewId = 'signnew'>
		<cfset socialbuttonId = 'sign-soc'>
		<cfset singorId = 'singor'>
		<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ 0>
			<cfset socialbuttonId = 'sign-soc1'>
			<cfset singorId = 'singor1'>
			<cfset signnewId = 'signnew1'>
		<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ 1>
			<cfset containerClass = 'loginform'>
		<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ 2>
			<cfset containerClass = 'loginform1'>
		<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ 3>
			<cfset width = '<cfoutput>#GetCPPSETUPQUERY.width_int#</cfoutput>'>
			<cfset height = '<cfoutput>#GetCPPSETUPQUERY.height_int#</cfoutput>'>
			<style>
				<cfoutput>
					.#containerClass#{
						width:#width#px;
						height:#height#px;
					}
				</cfoutput>
			</style>
		</cfif>
		<style>
			<cfoutput>
				.login-or{
					background-color: #GetCPPSETUPQUERY.BACKGROUNDCOLOR_VCH#;
				}
				.portal-add-txt, .inner-txt-hd, .inner-txt-subhd, .portal-add-txt1, .portal-add-txt2{
					<cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
						font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
					</cfif>
					font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
					color: #GetCPPSETUPQUERY.FONTCOLOR_VCH#;
					font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
					font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
					font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				}
				
				<!---##singhd {
				    float: left;
				    <cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
						font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
					</cfif>
					font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
					color: #GetCPPSETUPQUERY.FONTCOLOR_VCH#;
					font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
					font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
				    font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				    text-align: center;
				    width: 100%;
				}--->
				##signnew a {
				  
				    float: left;
				  	<cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
						font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
					</cfif>
					font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
					color: #GetCPPSETUPQUERY.FONTCOLOR_VCH#;
					font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
					font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
					font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				    height: 50px;
				    margin-top: 22px;
				    text-align: left;
				    text-decoration: none;
				    width: 100%;
				}
				
				<!---
				.#containerClass#{
					border: #GetCPPSETUPQUERY.BORDERWIDTH_INT#px solid #GetCPPSETUPQUERY.BORDERCOLOR_VCH#;
					border-radius: #GetCPPSETUPQUERY.BORDERRADIUS_INT#px;
					background-color: #GetCPPSETUPQUERY.BACKGROUNDCOLOR_VCH#;
					
					background-image: url('#rootUrl#/#sessionPath#/act_previewImage.cfm?serverfileName=#GetCPPSETUPQUERY.BACKGROUNDIMAGE_VCH#&userId=#GetCPPSETUPQUERY.userId_int# ');
					background-repeat : <cfif LCase(GetCPPSETUPQUERY.imageRepeat_vch) EQ 'yes' >no-repeat<cfelseif LCase(GetCPPSETUPQUERY.imageRepeat_vch) EQ 'no'>no-repeat</cfif>
				}
				--->
				
				###socialbuttonId#, ###singorId#, ul li{
					font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
					font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
					color: #GetCPPSETUPQUERY.FONTCOLOR_VCH#;
					font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
					font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
					font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				}
				.cppButton
				{
					background: -moz-linear-gradient(center top , ##2484C6, ##2484C6) repeat scroll 0 0 rgba(0, 0, 0, 0);
				    border: 1px solid ##0076A3;
				    color: ##FFF;
				    float: right;
				    padding: 5px;
					border-radius: 1px 1px 1px 1px;
				    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
				    cursor: pointer;
				    display: inline-block;
				    <cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
						font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
					</cfif>
					font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
					font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
					font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
					font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
				    margin: 0 2px;
				    outline: medium none;
				    text-align: center;
				    text-decoration: none;
				    text-transform: uppercase;
				    vertical-align: baseline;
				}
				.cppButton:hover {
					background: ##007ead;
					background: -webkit-gradient(linear, left top, left bottom, from(##0095cc), to(##00678e));
					background: -moz-linear-gradient(top,  ##0095cc,  ##00678e);
					
				}
				.cppButton:active {
					color: ##fff;
					background: -webkit-gradient(linear, left top, left bottom, from(##0078a5), to(##00adee));
					background: -moz-linear-gradient(top,  ##0078a5,  ##00adee);
					
				}
			</cfoutput>
		</style>
		
		<cfset templateId = trim(GetCPPSETUPQUERY.template_int)>
		
		<cfset leftStrLength = FINdNoCase( '{%portal%}', GetCPPSETUPQUERY.customhtml_txt)>
		<cfif leftStrLength GT 0>
			<cfset portalLeft = left(GetCPPSETUPQUERY.customhtml_txt, leftStrLength)>
			<cfset portalRight = replaceNoCase(GetCPPSETUPQUERY.customhtml_txt, portalLeft , '') >
			<cfset portalRight = replaceNoCase(portalRight, '{%portal%}' , '') >
		</cfif>
		
		<cfcatch TYPE="any">
			<cfdump var="#cfcatch#">
			<cfoutput>#cfcatch.MESSAGE#</cfoutput>
			<cfexit>
	    </cfcatch>
		        
	</cftry> 
	<section class="<cfoutput>#containerClass#</cfoutput> cf">
		<div class="loginPanel">
		<cfif GetCPPSETUPQUERY.displayType_ti EQ 1 and trim(GetCPPSETUPQUERY.customHtml_txt) NEQ '<p>You may include the portal any where on the page by typing {%portal%}. You may include it only once.</p>'>
			<cfoutput>#portalLeft#</cfoutput>
			<div style="clear:both"></div>
		</cfif>
		<div id="singhd">Login</div>
	 	<div class="LoginSocial">
	 		<div class="title">Social Logins</div>
			<div class="icon"<!---id="<cfoutput>#socialbuttonId#</cfoutput>"--->>
				<div id="btnFacebook" class="social-icon"><a href="#" id="btnFacebook"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/facebook.png" /></a></div>
	            <div id="btnTwitter" class="social-icon"><a href="#"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/twitter.png" /></a></div>
	            <div id="btnGoogle" class="social-icon"><a href="#"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/google.png" /></a></div>
			</div>
	 	</div>
		<div class="LoginAccount">
			<form name="login" action="index_submit" method="get" accept-charset="utf-8">
		 		<div class="title">Please Enter Your Login Credentials:</div>
				<div class="formLogin">
				<div class="login_box">
					<div class="login_left_box">
						Email
					</div>
					<div class="login_right_box">
						<input type="email" id="inpSigninUserID" class="login_input_box" name="inpSigninUserID" placeholder="yourname@email.com" required onKeyPress="return submitenter(this,event)">
					</div>
				</div>		
				<div class="login_box mtop10">
					<div class="login_left_box">
						Password
					</div>
					<div class="login_right_box">
						<input type="password" placeholder="*********" class="login_input_box" required id="inpSigninPassword" name="inpSigninPassword" onKeyPress="return submitenter(this,event)"></li>
					</div>
				</div>		
				<div class="action">
					<div class="forgotPass">
						<a href="#" id="CPPRequestPassword">Forgot Password? Or<BR />Request a First Time Password</a>
					</div>
					<div class="submitbtn">
						<a href="#" class="cppButton <cfoutput>#submitClass#</cfoutput>" id="CPPSignIn">Login</a>
					</div>
				</div>
			</form>		
			<span class="login-or">or</span>
	 	</div>
		<!---
        <div id="<cfoutput>#socialbuttonId#</cfoutput>">
               <div id="btnFacebook" class="social-icon"><a href="#" id="btnFacebook"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/facebook.png" /></a></div>
               <div id="btnTwitter" class="social-icon"><a href="#"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/twitter.png" /></a></div>
               <div id="btnGoogle" class="social-icon"><a href="#"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/newStyle/images/google.png" /></a></div> 
        </div>
  
	    <div id="<cfoutput>#singorId#</cfoutput>">or</div>
			<form name="login" action="index_submit" method="get" accept-charset="utf-8">
				<input type="hidden" id="inpCPPUUID" name="inpCPPUUID" value="#inpCPPUUID#" />
                <input id="inpSigninMethod" type="hidden" name="method" value="Signin" />
				<ul>
					<li>
						<label for="inpSigninUserID" class="inner-txt-hd">Email</label>
						<input type="email" id="inpSigninUserID" name="inpSigninUserID" placeholder="yourname@email.com" required onKeyPress="return submitenter(this,event)">
					</li>
					<li>
						<label for="inpSigninPassword" class="inner-txt-hd">Password</label>
						<input type="password" placeholder="password" required id="inpSigninPassword" name="inpSigninPassword" onKeyPress="return submitenter(this,event)"></li>
					</ul>
				<div style=" float:right; margin-top:10px; margin-right:17px;">
					<a href="#" class="cppButton <cfoutput>#submitClass#</cfoutput>" id="CPPSignIn">Login</a>
				</div>
				
			</form>
		<div id="<cfoutput>#signnewId#</cfoutput>"><a href="#" id="CPPRequestPassword" class="inner-txt-hd">New User? Lost Password? Enter your email and request a password here.</a></div>
		--->
		<cfif GetCPPSETUPQUERY.displayType_ti EQ 1 and trim(GetCPPSETUPQUERY.customHtml_txt) NEQ '<p>You may include the portal any where on the page by typing {%portal%}. You may include it only once.</p>'>
			<div style="clear:both"></div>
			<cfoutput>#portalRight#</cfoutput>
		</cfif>
		<!---</div>--->
	</section>
	<!--- Details - if going to include custom JS - write as CFM so can document better during development - when stable make compacted version as pure .js--->
    <script type="text/javascript">
		var Facebook_URL = '<cfoutput>#APPLICATION.Facebook_URL#</cfoutput>';
		var Facebook_URL_PARAM = '<cfoutput>#APPLICATION.Facebook_URL_PARAM#</cfoutput>';		
		var Facebook_AppID = '<cfoutput>#APPLICATION.Facebook_AppID#</cfoutput>'; 
		var inpCPPUUID = "<cfoutput>#inpCPPUUID#</cfoutput>";
		$(function() {		
		


			$("#CPPSignIn").click(function(){								
				CPPSignIn();				
			});	
			
			$("#CPPRequestPassword").click(function(){								
				CPPRequestNewPassword();				
			});	
			
			$("#btnFacebook").click(function() {
				// console.log('yo fb');
				//FB.login(getToken());
				
				$.jGrowl("Watch for a possible popup to log in to Facebook, if you are not already logged in.", { life:2000, position:"center", header:' Message' });
				
				getToken();				
			});
			
			
			$("#btnTwitter").click(function() {
				$.jGrowl("Watch for a possible popup to log in to Twitter, if you are not already logged in.", { life:2000, position:"center", header:' Message' });
				
				TwitterLogin();
				
			});
			$("#btnGoogle").click(function () {
				
				$.jGrowl("Watch for a possible popup to log in to Google, if you are not already logged in.", { life:2000, position:"center", header:' Message' });
						
				GoogleLogin();
			});
			
			$("input[name='inpResetPasswordEveryTime']").change(function(){
											
				if ($("input[name='inpResetPasswordEveryTime']:checked").val() == '1')
				{
					$.jGrowl("Password will be changed on each successful login.", { life:2000, position:"center", header:' Message' });
				}
				else
				{
					$.jGrowl("Password is whatever was last sent to you. You may also request a new one at anytime.", { life:2000, position:"center", header:' Message' });
				}
			});
		
			
			<!--- Allow current end user to sign out--->
			$("#CPPPortalSignOut").click(function(){
				
				var ParamStr = "";
	
				<cfoutput>							
					ParamStr += "?inpCPPUUID=-1";
					window.location = "#rootUrl#/#publicPath#/index.cfm" + ParamStr;
				</cfoutput>	
				
			});
			
			
			<!--- Allow current end user to sign out--->
			$("#WhatIsMFA").click(function(){
					
				jGrowlTheme('monocenterauto', 'MFA is...', '<BR/>You don&quot;t need an account.<BR/>You don&quot;t need to sign up.<BR/>Your email address is your primary identification.<BR/><BR/> This site uses Multi-Factor Authentication.<BR/>You just need to request/retrieve a password from your email address you provide to log in. <BR/>', 'images/MenuItemSX.png', 10000);
							
			});
			
			
			<!--- Log in from alternative source drop down --->
			
            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


           
			bb_init_facebook();
			
			
		});
		
		function TwitterLogin(){
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=GetTwitterAuthenticate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:{
					inpCPPUUID : inpCPPUUID
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(d.DATA.RXRESULTCODE[0] == 1)
							{							
								//window.location = d.DATA.REDIRECT_URL[0];
								TWITTERLOGIN._Oauth(d.DATA.REDIRECT_URL[0]);
							} else{
								jAlert(d.DATA.MESSAGE[0] ,"Error." );
							}
						}
						else
						{<!--- No result returned --->
						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}										
				} 		
					
			});
			
			return false;
		}
		
		function CPPSignIn()
		{				
			$("#loadingDlgCPPPublicProcessing").show();		
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/user.cfc?method=Signin&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { 
						inpSigninUserID: $("#inpSigninUserID").val(), 
						inpSigninPassword: $("#inpSigninPassword").val(), 
						inpCPPUUID : '<cfoutput>#GetCPPSETUPQUERY.Cpp_UUID_vch#</cfoutput>', 
						resetPasswordEveryTime : $("input[name='inpResetPasswordEveryTime']:checked").val() , 
						inpVanity: '<cfoutput>#inpCPPUUID#</cfoutput>'
					},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#publicPath#/index.cfm" + ParamStr;
								</cfoutput>										
							}
							else
							if(CurrRXResultCode == 1)
							{					
								<cfoutput>							
									window.location = "#rootUrl#/home/#TRIM(inpCPPUUID)#";
								</cfoutput>	
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });																
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;
			
		}
		
		function CPPRequestNewPassword()
		{
			$("#loadingDlgCPPPublicProcessing").show();		
					
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/user.cfc?method=RequestNewPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { INPCONTACTSTRING: $("#inpSigninUserID").val(), INPCONTACTTYPEID: 2, inpCPPUUID : <cfoutput>'#inpCPPUUID#'</cfoutput>  },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#publicPath#/index.cfm" + ParamStr;
								</cfoutput>										
							}	
							else							
							if(CurrRXResultCode == 1)
							{					
								<!--- refresh List --->
								
								<!---var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/SignIn.cfm" + ParamStr;
								</cfoutput>	--->
								
								<!---$.jGrowl('A new password has been sent to the email address at ' + $("#inpSigninUserID").val() + ' \nThe message is from support@contactpreferenceportal.com. If you dont see it shortly, please check your junk folder(s).' , { life:10000, position:"center", header: ' Message'});--->
								
								jGrowlTheme('monocenter', 'Message', 'A new password has been emailed to:  ' + $("#inpSigninUserID").val(), '', 10000);
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });									
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;			
			
		}
		
	function authenticatePass(uid,access_token){
		$.ajax({
					type : "GET",
					async : false,
					url : "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/user.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					data : {
						inpUserID : '', 
						inpPassword : '', 
						inpAt : access_token,
						inpUUID : '<cfoutput>#inpCPPUUID#</cfoutput>',
						<!---appSecret : Facebook_Appsecret,--->
						appId : Facebook_AppID
					},
					dataType : "json",
					success : function (d) {
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{			
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									window.location.href="<cfoutput>#rootUrl#</cfoutput>/home/<cfoutput>#TRIM(inpCPPUUID)#</cfoutput>";
								}
								else
								{
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
									if(typeof(d.DATA.REASONMSG[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined")
									{											
										jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] + d.DATA.MESSAGE[0] + d.DATA.ERRMESSAGE[0]);										
									}		
									else
									{
										jAlertOK("Error while trying to login.\n" + "Invalid response from server.");
									}
								}
							}
							else
							{<!--- Invalid structure returned --->	
								jAlertOK("Error while trying to login.\n" + "Invalid response from server." );
							}
						}
						else
						{<!--- No result returned --->
							jAlertOK("Error while trying to login.\n" + "No response form remote server.");
						}
							
						}
				});
	}
	
	<!--- get token facebook ---->
	function getToken()
	 {
		 //alert('here')

		 FB.getLoginStatus(function(r)
		 {//alert(response.status)

		 	response = r;
			if (response.status === 'connected') 
			{
				// the user is logged in and connected to your
				// app, and response.authResponse supplies
				// the user's ID, a valid access token, a signed
				// request, and the time the access token 
				// and signed request each expire

				var uid = response.authResponse.userID;
				access_token = response.authResponse.accessToken;
				
				authenticatePass(uid,access_token);
				
				
		  	} 
			else 
			{
				FB.login(function(response){
					if (response.status === 'connected') 
					{
						var uid = response.authResponse.userID;
						access_token = response.authResponse.accessToken;
						authenticatePass(uid,access_token);
				  	} 
				},{scope:'read_stream,publish_stream,email'});				
		  	}
		 	
		}, true);
				
	 }
	 
	 
	<!--- Call back for login google---->
	function googleLoginCallBack(result){
		if(result != null){

			var pData = result;
			access_token = result.access_token;


			$.ajax({
					type : "GET",
					async : false,
					url : "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/user.cfc?method=validateContactGoogle&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					data : {
						accessToken : access_token,
						accessSecret : '',
						userIdGoogle : pData.id,
						FirstName : pData.given_name,
						LastName : pData.family_name,
						email : pData.given_name,
						inpUUID : '<cfoutput>#inpCPPUUID#</cfoutput>'
					},
					dataType : "json",
					success : function (d) {
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];
								if(CurrRXResultCode > 0)
								{
									window.location.href="<cfoutput>#rootUrl#</cfoutput>/home/<cfoutput>#TRIM(inpCPPUUID)#</cfoutput>";
								}
								else
								{
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->
									if(typeof(d.DATA.REASONMSG[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined")
									{
										jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] + d.DATA.MESSAGE[0] + d.DATA.ERRMESSAGE[0]);
									}
									else
									{
										jAlertOK("Error while trying to login.\n" + "Invalid response from server.");
									}
								}
							}
							else
							{<!--- Invalid structure returned --->
								jAlertOK("Error while trying to login.\n" + "Invalid response from server." );
							}
						}
						else
						{<!--- No result returned --->
							jAlertOK("Error while trying to login.\n" + "No response form remote server.");
						}
					}
				});
		}else{
		 	var config = {
	            'client_id': '<cfoutput>#APPLICATION.Google_ClientId#</cfoutput>',
	            'scope': 'https://www.googleapis.com/auth/userinfo.profile',
				'immediate': false
	        };
	        gapi.auth.authorize(config, googleLoginCallBack);
		}
	}
	
    /*var access_token = 0;
    function GoogleLogin() {
        var additionalParams = {
         'callback': googleLoginCallBack
       };
       // Attach a click listener to a button to trigger the flow.
        gapi.auth.signIn(additionalParams);
    }*/


    var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
    var SCOPE       =   'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
    var CLIENTID    =   '<cfoutput>#APPLICATION.Google_ClientId#</cfoutput>';
    var REDIRECT    =   '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/oauth2callback.cfm';
    var LOGOUT      =   'http://accounts.google.com/Logout';
    var TYPE        =   'token';
    var _url        =   OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
    var acToken;
    var tokenType;
    var expiresIn;
    var user;
    var loggedIn    =   false;

	function GoogleLogin(){
        var win =   window.open(_url, "windowname1", 'width=800, height=600');
        var pollTimer   =   window.setInterval(function() {
            try {
                if (win.document.URL.indexOf(REDIRECT) != -1) {
                    window.clearInterval(pollTimer);
                    var url =   win.document.URL;
                    acToken =   gup(url, 'access_token');
                    tokenType = gup(url, 'token_type');
                    expiresIn = gup(url, 'expires_in');
                    win.close();

                    validateToken(acToken);
                }
            } catch(e) {
            }
        }, 500);
    }

    function validateToken(token) {
        $.ajax({
            url: VALIDURL + token,
            data: null,
            success: function(responseText){
                getUserInfo();
                loggedIn = true;
            },
            dataType: "jsonp"
        });
    }

    function getUserInfo() {
        $.ajax({
            url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + acToken,
            data: null,
            success: function(resp) {
                user    =   resp;
                googleLoginCallBack(user);

            },
            dataType: "jsonp"
        });
    }

    //credits: http://www.netlobo.com/url_query_string_javascript.html
    function gup(url, name) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\#&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        if( results == null )
            return "";
        else
            return results[1];
    }





	function bb_init_facebook() {
		
		var curLoc = window.location;

		FB.init({ 
			appId: Facebook_AppID,
			cookie: true,
			status: true,
			xfbml: true,
			channelUrl: '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/channel.html'
	
		});
	}
	
		
	<!--- Auto click Login button when press Enter key --->
	function submitenter(myfield,e) {
	  	var keycode;
	 	if (window.event) keycode = window.event.keyCode;
	  	else if (e) keycode = e.which;
	  	else return true;
	
		if (keycode == 13)
     	{
        	CPPSignIn();
        	return false;
        }
		else
			return true;
	}

	
    var TWITTERLOGIN = {

        _Oauth:function(url){
            var w = 700;
            var h = 500;
            var popupName = popupName ? popupName : '';
            var left = (window.screen.width / 2) - (w / 2),
            top = (window.screen.height / 2) - (h / 2);
            var myWindow = window.open(url, popupName, 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            return false;
        }
    }
		
    </script>
	</body>
</html>