﻿<cfparam name="code" default=""/>
<cfparam name="inpCPPUUID" default=""/>
<cfparam name="access_token" default=""/>

<cfif code neq "">
	<cfset getAccessTokenReturnUrl = URLEncodedFormat(rootUrl & "/" & publicPath & "/facebook_authen_callback.cfm?inpCPPUUID=" & inpCPPUUID)>
	<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#APPLICATION.Facebook_AppID#&client_secret=#APPLICATION.Facebook_AppSecret#&redirect_uri=#getAccessTokenReturnUrl#&code=#code#" 
	        result="response"/>
	<cfloop list="#response.filecontent#" index="i" delimiters="&">
		<cfif listfirst(i, "=") eq "access_token">
			<cfset access_token = listlast(i, "=")>
		</cfif>
	</cfloop>
</cfif>
<cfif access_token neq "">
	<cfoutput>
		<style>
			<!-- -@import url('#rootUrl#/#publicPath#/css/jquery-ui-1.8.7.custom.css');
			@import url('#rootUrl#/#publicPath#/css/cpp.css');
			@import url('#rootUrl#/#publicPath#/css/jquery.alerts.css');
			@import url('#rootUrl#/#publicPath#/css/jquery.jgrowl.css');
			- -->@import url('#rootUrl#/#publicPath#/css/jquery.alerts.css');
			@import url('#rootUrl#/#publicPath#/css/jquery.jgrowl.css');
			@import url('#rootUrl#/#publicPath#/css/styleloginmobile.css');
		</style>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery-1.7.1.min.js">

		</script>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.alerts.js">

		</script>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js">

		</script>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.jgrowl.js">

		</script>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js">

		</script>
		<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jgrowl-theme.js">

		</script>
		<script type="text/javascript">
			var Facebook_URL = '<cfoutput>#APPLICATION.Facebook_URL#</cfoutput>';
						var Facebook_URL_PARAM = '<cfoutput>#APPLICATION.Facebook_URL_PARAM#</cfoutput>';		
						var Facebook_AppID = '<cfoutput>#APPLICATION.Facebook_AppID#</cfoutput>'; 
						
						$(function() {
							var inpCPPUUID = "<cfoutput>#inpCPPUUID#</cfoutput>";
							$.jGrowl("Watch for a possible popup to log in to Facebook, if you are not already logged in.", { life:5000, position:"center", header:' Message' });
							$.ajax({
								type : "GET",
								async : false,
								url : "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/user.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
								data : {
									inpUserID : '', 
									inpPassword : '', 
									inpAt : '<cfoutput>#access_token#</cfoutput>',
									inpUUID : '<cfoutput>#inpCPPUUID#</cfoutput>',
									<!---appSecret : Facebook_Appsecret,--->
									appId : Facebook_AppID
								},
								dataType : "json",
								success : function (d) {
									<!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
									console.log(d);
									if (d.ROWCOUNT > 0) 
									{			
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{							
											CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
											if(CurrRXResultCode > 0)
											{
												window.location.href="<cfoutput>#rootUrl#</cfoutput>/home/<cfoutput>#TRIM(inpCPPUUID)#</cfoutput>";
											}
											else
											{
												<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
												if(typeof(d.DATA.REASONMSG[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined")
												{											
													jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] + d.DATA.MESSAGE[0] + d.DATA.ERRMESSAGE[0]);										
												}		
												else
												{
													jAlertOK("Error while trying to login.\n" + "Invalid response from server.");
												}
												window.location.href="<cfoutput>#rootUrl#/#inpCPPUUID#</cfoutput>";
											}
										}
										else
										{<!--- Invalid structure returned --->	
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." );
											window.location.href="<cfoutput>#rootUrl#/#inpCPPUUID#</cfoutput>";
										}
									}
									else
									{<!--- No result returned --->
										jAlertOK("Error while trying to login.\n" + "No response form remote server.");
										window.location.href="<cfoutput>#rootUrl#/#inpCPPUUID#</cfoutput>";
									}
								}
							});
						});
		</script>
	</cfoutput>
<cfelse>
	<cflocation url="#rootUrl#/#inpCPPUUID#">
</cfif>