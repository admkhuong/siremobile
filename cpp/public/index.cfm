<!---<cfinclude template="paths.cfm" />--->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Customer Preference Portal</title>
        <!--- Google Analytics Code--->
		<script type="text/javascript">
			
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-35693415-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
        
        </script>
    	<cfoutput>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery-1.7.1.min.js"></script>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.alerts.js"></script>
			<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.jgrowl.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jquery.form.js"></script>
	        <script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery/jgrowl-theme.js"></script>
	        
			 <style>		
	            @import url('#rootUrl#/#publicPath#/css/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#publicPath#/css/cpp.css');
	            @import url('#rootUrl#/#publicPath#/css/jquery.alerts.css');
	            @import url('#rootUrl#/#publicPath#/css/jquery.jgrowl.css');
		    </style>	
		</cfoutput>
    </head>	
	<body>
	
<cfset message = ''>
 <script type="text/javascript">
				
		$(function() {		
				
			$("#CPPPortalSignIn").click(function(){				
								
				CPPPortalSignIn();
				
			});	
						
		});
		
		<!--- Details - user input will teed trim before part of URL param--->
		function trim(s)
		{
			return rtrim(ltrim(s));
		}
		
		function ltrim(s)
		{
			var l=0;
			while(l < s.length && s[l] == ' ')
			{	l++; }
			return s.substring(l, s.length);
		}
		
		function rtrim(s)
		{
			var r=s.length -1;
			while(r > 0 && s[r] == ' ')
			{	r-=1;	}
			return s.substring(0, r+1);
		}


		
		function CPPPortalSignIn()
		{									
			$("#loadingDlgCPPPublicProcessing").show();		
					
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=EnterCpp&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  {inpCPPUUID : $("#inpCPPUUID").val() },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
					
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#publicPath#/index.cfm" + ParamStr;
								</cfoutput>										
							}
							else							
							if(CurrRXResultCode == 1)
							{					
								<!--- refresh List --->
								
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=" + trim($("##inpCPPUUID").val());
									window.location = "#rootUrl#/#publicPath#/SignIn.cfm" + ParamStr;
								</cfoutput>	
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });								
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;
			
		}
		
    </script>  
    

<div id="container" class="container">
	<div id="content" class="clf">
		<div id="divEnterCPP" class="rxform2 ui-corner-all">
			<fieldset>
					<legend>Enter your CPP!&nbsp;&nbsp;</legend>
					<p class="red"><cfoutput>#message#</cfoutput></p>
					<p>
						<label for="enterCpp_uuid">Your CPP UUID</label>
                              <!--- Detail - auto populate if provided--->
						<cfif inpCPPUUID NEQ "0">
							<input type="text" id="inpCPPUUID" name="inpCPPUUID" value="<cfoutput>#inpCPPUUID#</cfoutput>" />
                              <cfelse>
                              	<input type="text" id="inpCPPUUID" name="inpCPPUUID" value="" />
                              </cfif>
					</p>
				</fieldset>
				<button id="CPPPortalSignIn">Enter your CPP</button>
                      
				<span id="enterCppLoading" style="display:none;">
					<img alt="Loading" class="loadingDlgRegisterNewAccount" src="#rootUrl#/#PublicPath#/images/loading-small.gif" width="20" height="20" />
				</span>					
		</div>
	</div>
</div>	

</body>
</html>
