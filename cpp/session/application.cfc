<cfcomponent output="false" hint="Handle the application.">

	<!--- Set TimeOutSeconds for 3600 - 60 minutes for one hour timeout by default - change this if you change the application timeout --->
	<!--- Set up the application. --->
	<cfset THIS.Name = "Cpp" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
	<cfset THIS.SessionManagement = true />
	<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 30, 0 ) />
	<cfset THIS.SetClientCookies = true />
	<cfset THIS.clientmanagement = false />
	<cfset THIS.setdomaincookies = false />
	<cfset THIS.setdomaincookies = false />
	<cfset THIS.loginstorage="session" />
	<!--- Define the page request properties. --->
	<cfsetting requesttimeout="120" showdebugoutput="false" enablecfoutputonly="false" />

	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Fires at first part of page processing.">
		<!--- Define arguments. --->
		<cfargument name="TargetPage" TYPE="string" required="true" />
		
		<!--- Bug in CF8 and later - if using the onRequest method in the application.cfc then cfc requests all return empty --->
		<cfscript>          
			if (right(arguments.targetPage, 4) is ".cfc" ) {
			  structDelete(this, "onRequest");
			  structDelete(variables, "onRequest");
			 }
		</cfscript> 
		<!--- Return out. --->


		<!--- Disable while troubleshooting SSL issues--->
        <!--- May be problem when request for cfc comes from non http that redirects https--->
		<!---<cfset this.enforceSSLRequirement() />--->
        
           
       <!--- <cfset Session.CPPUUID = "">--->
        <cfinclude template="../public/paths.cfm" />
		<cfinclude template="../public/socialapp.cfm" />

		<cfreturn true />
	</cffunction>


	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			TYPE="string"
			required="true"
			 />
		<cfset DEBUG = "yes" />
		<cfinclude template="../public/paths.cfm" />
		<cfinclude template="../public/SocialApp.cfm" />
		<!--- Set for one hour timeout by default - change this if you change the application timeout --->
		<cfset TimeOutSeconds = 3600 />
		<cfparam name="PageTitle" default="">
		<cfparam name="inpCPPUUID" default="0" />
        <cfparam name="inpCPPAID" default="0" >
        	
        	<!---I comment this out because it kill my authenticate case logic--->
        	<!---<cfset Session.CPPACOUNTID = inpCPPAID />--->
        
			<!--- Reset enitre session on request--->
	        <cfif inpCPPUUID EQ -1>
	            <cflock scope="session" timeout="10" type="exclusive">
	             	<cfscript>
	            		StructClear(Session);					
	            	</cfscript>
	                
	                <cfset inpCPPUUID = "0">
	            </cflock>
			</cfif>
		    <!--- check including step authentication --->
			<cfquery name="CheckIncludingAuthenticateStep" dataSource="#Session.DBSourceEBM#">
				SELECT
					IncludeIdentity_ti
				FROM
					simplelists.customerpreferenceportal
				WHERE
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
				OR
					Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
			</cfquery>
			
			<cfif CheckIncludingAuthenticateStep.IncludeIdentity_ti NEQ 1>
				<cfset Session.CONSUMEREMAIL = 0 />
				<cfset Session.resetPasswordEveryTime = 0 />
	            <cfset Session.CPPPWD = 0 />
				<cfset Session.cppuuid = inpCPPUUID />
				<cfset Session.CPPACOUNTID = inpCPPAID />
				<!---<cfset Session.Include = CheckIncludingAuthenticateStep.IncludeIdentity_ti />--->
			<cfelse>
				<cfif StructkeyExists(Session, "CONSUMEREMAIL") AND Session.CONSUMEREMAIL EQ 0>
					<cfset structClear(Session)>
				</cfif>
			</cfif>    
			
	        <!--- Details - Skip CPP signin if CPP already provided in URL --->
			<cfif isdefined("Session.CONSUMEREMAIL") and not find("index.cfm", ARGUMENTS.TargetPage) and not find("signin.cfm", ARGUMENTS.TargetPage) and not find("twitter_authorize.cfm", ARGUMENTS.TargetPage) AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "M_" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "dsp_">
		      	<cfif inpCPPUUID NEQ Session.cppuuid>
					<cfset structClear(Session)>
					<cfif structKeyExists(url,'preview') AND url.preview EQ 1>
						<cflocation url="#rootUrl#/#publicPath#/signIn.cfm?inpCppUUID=#url.inpCppUUID#&preview=1" addtoken="false">
					<cfelse>
						<cflocation url="#rootUrl#/#url.inpCppUUID#" addtoken="false">
					</cfif>
				</cfif>
				
				<!--- Start regulaer HTML here --->
                <cfscript>
                    // redirect if mobile browser detected
                    if(reFindNoCase("android|avantgo|blackberry|blazer|iphone|ipad|ipod|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) > 0){
                         location("#rootUrl#/mobile/home/#url.inpCppUUID#",false);
                    }
                </cfscript>
				
		       <cfinclude template="display/default/dsp_header.cfm" />
				<!--- Wrap the body --->
				<cfinclude template="display/default/dsp_body.cfm" />
				<!--- Include the requested page. --->
				<cfinclude template="#ARGUMENTS.TargetPage#" />
				<!--- Do footer stuff here --->
				<cfinclude template="display/default/dsp_footer.cfm" />
			<cfelseif isdefined("Session.CONSUMEREMAIL") and not find("index.cfm", ARGUMENTS.TargetPage) and not find("signin.cfm", ARGUMENTS.TargetPage) and not find("twitter_authorize.cfm", ARGUMENTS.TargetPage) AND find("m_", ARGUMENTS.TargetPage) >
				<cfinclude template="display/mobile/dsp_header.cfm" />
				<!--- Wrap the body --->
				<cfinclude template="display/mobile/dsp_body.cfm" />
				<!--- Include the requested page. --->
				<cfinclude template="#ARGUMENTS.TargetPage#" />
				<!--- Do footer stuff here --->
				<cfinclude template="display/mobile/dsp_footer.cfm" />
		    <cfelseif find("twitter_authorize.cfm", ARGUMENTS.TargetPage) OR find("dsp_", ARGUMENTS.TargetPage)>
		    	<cfinclude template="#ARGUMENTS.TargetPage#" />
		    <cfelse>
				<cfif structKeyExists(url,'preview') AND url.preview EQ 1>
					<cflocation url="#rootUrl#/#publicPath#/signIn.cfm?inpCppUUID=#url.inpCppUUID#&preview=1" addtoken="false">
				<cfelse>
					<cflocation url="#rootUrl#/#url.inpCppUUID#" addtoken="false">
				</cfif>
			</cfif>
        
		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction	name="OnSessionEnd"	access="public"	returntype="void" output="false" hint="Fires when the session is terminated.">
		<!--- Define arguments. --->
		<cfargument
			name="SessionScope"
			type="struct"
			required="true"
			 />
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			 />
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>

	<cffunction	name="enforceSSLRequirement" access="public" returntype="void" output="false" hint="I check to see if the current request aligns properly with the current SSL connection.">
 		
   
 
		<!--- Definet the local scope. --->
		<cfset var local = {} />
 
		<!---
			Get the current directory in case we need to use
			this for SSL logic.
		--->
		<cfset local.directory = listLast(getDirectoryFromPath( cgi.script_name ), "\/"	) />
 
		<!---
			Get the current file in case we need to use this
			for SSL logic.
		--->
		<cfset local.template = listLast( cgi.script_name,"\/"	) />
 
		<!---
			Check to see if the current request is currently
			using an SSL connection.
		--->
		<cfset local.usingSSL = (cgi.https eq "on") />
 
		<!---
			By default, we are going to assume that the current
			request does NOT require an SSL connection (as this
			is usually in the minority of cases).
		--->
		<cfset local.requiresSSL = false />
 
		<!---
			Now, let's figure out if the current request requires
			an SSL connection. To do this, we can use any kind of
			url, form, directory, of file-based logic.
		--->
		<cfif 	(	
					FINDNOCASE("contactpreferenceportal.com", CGI.SERVER_NAME) AND
					(
				
						(UCASE(local.directory) eq "CPP") ||
						(UCASE(local.directory) eq "CFC") ||
						(UCASE(local.directory) eq "CONTACT") ||
						(UCASE(local.directory) eq "cfc") ||
						(
							structKeyExists( url, "action" ) &&
							(url.action eq "top-secret")
						)
					)
				)>
 
			<!---
				This request does require an SSL. Set the flag
				for use in further logic.
			--->
			<cfset local.requiresSSL = true />
 
		</cfif>
 
 
		<!---
			At this point, we know if we are currently using
			SSL or not and we know if the current request
			requires SSL. Let's use this information to see if
			we need to perform any redirect.
		--->
		<cfif (local.usingSSL eq local.requiresSSL)>
 
			<!---
				The current page is synched with the SSL
				requirements of the request (either is requires
				and IS using an SSL connection or it does not
				require and is NOT using an SSL connection).
				Just return out of the function - no further
				action is required.
			--->
			<cfreturn />
 
		<cfelseif local.requiresSSL>
 
			<!---
				At this point, we know that we need to do some
				sort of redirect, and because the requrest
				requires an SSL connection, we know we need to
				redirect to an HTTPS connection. Let's store the
				protocol for use in the following CFLocation.
			--->
			<cfset local.protocol = "https://" />
 
		<cfelse>
 
			<!---
				At this point, we know that we need to do some
				sort of redirect, and because the request does
				NOT requiere an SSL connection, we know we need
				to redirect to an HTTP connection. Let's store the
				protocol for use in the following CFLocation.
			--->
			<cfset local.protocol = "http://" />
 
		</cfif>
 
		<!---
			If we've made it this far, then we are redirecting
			the user to a different page based on the chosen
			protocol. Build the target URL.
		--->
        
       <!--- local.url = #local.url# <BR />
        <cfflush>--->
        
		<cfset local.url = (
			local.protocol &
			cgi.server_name &
			cgi.script_name &
			"?" &
			cgi.query_string
			) />
 
		<!--- Redirect the use to the target connection. --->
		<cflocation
			url="#local.url#"
			addtoken="false"
			/>
 
		<!--- Return out (NOTE: we will never make it here). --->
		<cfreturn />
	</cffunction>
 




</cfcomponent>
