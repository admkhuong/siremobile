<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0" />
	<cfparam name="Session.DBSourceEBM" default="Bishop" />

    
    <!--- ************************************************************************************************************************* --->
    <!--- Get CPP group(s) contac t data --->
    <!--- This method requires a valid CPPUUID and CPPUserID and Session.CONSUMEREMAIL in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID - need to redirtect to login screen on session expired errors --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPContactData" access="remote" output="false" hint="Get CPP contact data for currently logged in user">
       	<cfargument name="inpCPPUUID" required="no" default="0">
        <cfargument name="INPGROUPID" required="no" default="-1">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                            
                            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CONTACTTYPEID, CONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "CONTACTTYPEID", "-1") /> 
            <cfset QuerySetCell(dataout, "CONTACTSTRING", "NA") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No Contacts Found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	
                <!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
                <cfif inpCPPUUID NEQ "0">    
					<!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                      
                    <!--- Auto Repair Expired Session --->
                    <cfinvoke 
                     component="cpp"
                     method="EnterCpp"
                     returnvariable="RetValCPPData">                         
                        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                    </cfinvoke>  
                    
                    <!--- Check for success --->
                    <cfif RetValCPPData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-7">
                    </cfif>
               				
                </cfif>    
                                        
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0 AND TRIM(Session.CONSUMEREMAIL) NEQ "">
                           
                    <!--- Get group counts --->
                    <cfquery name="GetContacts" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            ContactTypeId_int,
                            ContactString_vch                            
                        FROM
                           simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                        AND
                        	CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#">   
                                                
						<cfif INPGROUPID NEQ "" AND INPGROUPID NEQ "0" >
                            AND 
                                ( GroupList_vch LIKE '%,#INPGROUPID#,%' OR GroupList_vch LIKE '#INPGROUPID#,%' )                        
                        </cfif>  
            
            
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, CONTACTTYPEID, CONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>  
                    
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "CONTACTTYPEID", "0") /> 
		            <cfset QuerySetCell(dataout, "CONTACTSTRING", "NA") />  
                          
                    <cfloop query="GetContacts">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "CONTACTTYPEID", "#GetContacts.ContactTypeId_int#") /> 
                        <cfset QuerySetCell(dataout, "CONTACTSTRING", "#GetContacts.ContactString_vch#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-6">
                                    
                </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, CONTACTTYPEID, CONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "CONTACTTYPEID", "-1") /> 
                <cfset QuerySetCell(dataout, "CONTACTSTRING", "NA") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Remove Contacts from Group --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RemoveGroupContacts" access="remote" output="false" hint="Delete contact strings from group">
        <cfargument name="INPGROUPID" required="yes" default="-1">
        <cfargument name="INPCONTACTSTRING" required="yes" default="-1">
        <cfargument name="INPCONTACTTYPEID" required="no" default="-1">
        <cfargument name="inpCPPUUID" required="no" default="0">
        
        
		<cfset var dataout = '0' />    
        
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") /> 
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>                               
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0 AND TRIM(Session.CONSUMEREMAIL) NEQ "">
                
					<!--- Cleanup SQL injection --->
                    
                  
                 	                               
                    <cfif !isnumeric(INPGROUPID) OR !isnumeric(INPGROUPID) >
                        <cfthrow MESSAGE="Invalid Group Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                    
                    <cfif INPGROUPID EQ "0"  >
                        <cfthrow MESSAGE="All is not an remove group option. Try delete number instead." TYPE="Any" detail="" errorcode="-2">
                    </cfif>    
                                				                            												
					<!--- Update list if in middle - don't want 12, and 2, to be treated as the same--->             
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="RES1">
                        UPDATE
                            simplelists.rxmultilist                                
                        SET 
                            GroupList_vch = REPLACE( GroupList_vch , ',#INPGROUPID#,', ','),
                            LastUpdated_dt = NOW()                                                    
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                            AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">  
                            AND GroupList_vch LIKE '%,#INPGROUPID#,%' 
                            <cfif INPCONTACTTYPEID GT 0>
                            	AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                            </cfif>
                    </cfquery>                      
                    
                    <!--- Update list if at start - don't want 12, and 2, to be treated as the same--->               
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#"  result="RES2">
                       UPDATE
                            simplelists.rxmultilist                                
                       SET 
                            GroupList_vch =
                            CASE WHEN LENGTH(GroupList_vch) > #LEN(INPGROUPID) + 1# THEN RIGHT(GroupList_vch, LENGTH(GroupList_vch)-#LEN(INPGROUPID) + 1#)
                            ELSE GroupList_vch = ''
                            END ,
                            LastUpdated_dt = NOW() 
                       WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                            AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">                              
                            AND LEFT(GroupList_vch, #LEN(INPGROUPID) + 1#)  = '#INPGROUPID#,' 
                            <cfif INPCONTACTTYPEID GT 0>
                            	AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                            </cfif>
                    </cfquery> 							
					                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#RES1.RecordCount# #RES2.RecordCount#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />         
                        
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") />  
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTSTRING, INPGROUPID, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
				<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "(#INPCONTACTSTRING#)") />  
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
  
    <!--- ************************************************************************************************************************* --->
    <!--- Add Contacts to Group --->
    <!--- NOTE: There are several unique to CPP things going on here so be careful with changes --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddContactToGroup" access="remote" output="false" hint="Add contact string to multi-list">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = Duplicate
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
         
                                   
       	<cfoutput>
         	                       
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                <cfif IsDefined("Session.CPPUSERID")>
				<cfelse>
					<cfset Session.CPPUSERID = 0>
				</cfif>                              
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0 AND TRIM(Session.CONSUMEREMAIL) NEQ "">
            
            		<!--- Source is the consumer email reference by default - this is used in insert if needed below to uniquly distinguish this entry --->
            		<cfset INP_SOURCEKEY = "CPP UUID = " & TRIM(Session.CPPUUID)>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <cfif INPGROUPID EQ "" OR INPGROUPID LT 1 >
                          <cfthrow MESSAGE="No group specified." TYPE="Any" detail="" errorcode="-3">                      
                    </cfif>
                        
                        
                    <!--- Clean up phone string --->        
                                     
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
		            </cfif>
                                        
                    <!--- Validate phone number --->
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>				
						<!--- Check lenght --->
                    	<cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits!<br/>Need exactly 10 digits to make a call, not start with '0'." TYPE="Any" detail="" errorcode="-6"></cfif>                 
                    	
						<!--- Check expression --->
						<cfinvoke 
							component="validation"
							method="VldPhone10str"
							returnvariable="safeePhoneNumber"><cfinvokeargument name="Input" value="#INPCONTACTSTRING#"/></cfinvoke>
						<cfif safeePhoneNumber NEQ true>
							<cfthrow MESSAGE="Phone number is not valid!<br/>Need exactly 10 digits to make a call, not start with '0'." TYPE="Any" extendedinfo="" errorcode="-6" />
						</cfif>
					</cfif>
					
					<!--- Validate email address --->
					<cfif INPCONTACTTYPEID EQ 2>
						<!--- If not input or input space --->
						<cfif TRIM(INPCONTACTSTRING) EQ "">
							<cfthrow MESSAGE="Please enter email address." TYPE="Any" extendedinfo="" errorcode="-7" />
						</cfif>
						
						<!--- If input invalid email address --->
						<cfinvoke 
							component="validation"
							method="VldEmailAddress"
							returnvariable="safeeMail"><cfinvokeargument name="Input" value="#INPCONTACTSTRING#"/></cfinvoke>
						<cfif safeeMail NEQ true>
							<cfthrow MESSAGE="eMail address is not valid!<br/>Exp: From@somewehere.domain" TYPE="Any" extendedinfo="" errorcode="-7" />
						</cfif>
					</cfif>
					
                    <cfif INPCONTACTTYPEID EQ 4>
					
						<cfif inpAt neq 0>
						<cfhttp url="https://graph.facebook.com/me/?access_token=#inpAt#" result="myDetails" />
						<cfset pData = DeserializeJSON(myDetails.filecontent) />
						</cfif>
					<cfelseif INPCONTACTTYPEID eq 5>
					</cfif>
                    <!--- Check if on list yet--->
                    <cfquery name="CheckIfOnList" datasource="#Session.DBSourceEBM#">
                        SELECT                        	
                            COUNT(*) AS TOTALCOUNT                                                     
                        FROM
                            simplelists.rxmultilist
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                        AND 
                        	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">         
						<cfif INPCONTACTTYPEID GT 0>
                            AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                        </cfif>                                                                         
                        AND
                        	(
                            	CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#">   
                            	OR
                                CPPID_vch = ""
                                OR
                                CPPID_vch IS NULL
                            )                          
                    </cfquery> 		
                                        
                    <!--- If on list already for this EBM User account then just add to group - if not already in group --->            
                    <cfif CheckIfOnList.TOTALCOUNT GT 0>     
                                                    
                      	<!--- This adds CPP reference to existing contact string that does not already have reference --->
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="RES1">
                            UPDATE
                                simplelists.rxmultilist                                
                            SET 
                                CPPID_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#">,
                                CPPPWD_vch =  AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CPPPWD)#">, '#APPLICATION.EncryptionKey_UserDB#')                                                    
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                               AND
                               (
                                    CPPID_vch = ""
                                    OR
                                    CPPID_vch IS NULL
                                )       
                                <cfif INPCONTACTTYPEID GT 0>
                            		AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                            	</cfif>
                        </cfquery> 		  
                      
                        <!--- Only add to group if not already in group --->
                        <!--- Only link to group if linked to CPP ID first --->
                        <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="RES2">
                            UPDATE
                                simplelists.rxmultilist                                
                            SET 
                                GroupList_vch = CONCAT( GroupList_vch , '#INPGROUPID#,'),
                                LastUpdated_dt = NOW()
                            WHERE
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                AND CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#"> 
                                <cfif INPCONTACTTYPEID GT 0>
    	                        	AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
	                            </cfif>
                                AND GroupList_vch NOT LIKE '%,#INPGROUPID#,%' 
                                AND GroupList_vch NOT LIKE '#INPGROUPID#,%' 
                    	</cfquery> 		  
                                       
						<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Updated - Already on List - #RES1.RecordCount# #RES2.RecordCount#") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                    <cfelse>
						<!--- If not on list already for this EBM User account then just add to List AND Group - if not already in list ---> 
                        <!--- Adding here will allow duplicates by Source ID where Source ID by default is the TRIM(Session.CONSUMEREMAIL)--->
                        
                                    
                                            
                        <!--- Get time zone info ---> 
                        <cfset CurrTZ = 0>
                        
                        <!--- Get Locality info --->  
                        <cfset CurrLOC = "">
                        <cfset CurrCity = "">
                        <cfset CurrCellFlag = "0">
                                                       
                        <cfif INPGROUPID NEQ "">
                            <cfset CurrGroupList = "0,#INPGROUPID#,">                        
                        <cfelse>
                            <cfset CurrGroupList = "0,">
                        </cfif>    
                                                     
                        <!--- Set default to -1 --->         
                        <cfset NextContactListId = -1>         
                                 
                                        
                        <!--- Add record --->
                        <cftry>
                                                                                                                        
                            <cfquery name="AddToPhoneList" datasource="#Session.DBSourceEBM#">
                                INSERT INTO simplelists.rxmultilist
                                    (
                                      	CPPPWD_vch,
                                        CPPID_vch, 
                                        UserId_int, 
                                        Created_dt, 
                                        LASTUPDATED_DT, 
                                        LastAccess_dt, 
                                        ContactTypeId_int, 
                                        ContactString_vch, 
                                        SourceKey_vch, 
                                        TimeZone_int, 
                                        CellFlag_int, 
                                        UserSpecifiedData_vch, 
                                        GroupList_vch, 
                                        OptInFlag_int )
                                VALUES 
                                    (
                                    	AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CPPPWD)#">, '#APPLICATION.EncryptionKey_UserDB#'), 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">, 
                                        NOW(), 
                                        NOW(), 
                                        NOW(), 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INP_SOURCEKEY#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">, 
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrGroupList#">, 1)                                        
                            </cfquery>       
                            
                          
    <!---
    
    Time Zone · The number of hours past Greenwich Mean Time. The time zones are:
    
       Hours Time Zone
               27 4 Atlantic
               28 5 Eastern
               29 6 Central
               30 7 Mountain
               31 8 Pacific
               32 9 Alaska
               33 10 Hawaii-Aleutian
               34 11 Samoa
               37 14 Guam
             
    
    --->
                            <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                                
                                <!--- Get time zones, Localities, Cellular data --->
                                <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                    UPDATE MelissaData.FONE AS fx JOIN
                                     MelissaData.CNTY AS cx ON
                                     (fx.FIPS = cx.FIPS) INNER JOIN
                                     simplelists.rxmultilist AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                    SET 
                                      ded.TimeZone_int = (CASE cx.T_Z
                                      WHEN 14 THEN 37 
                                      WHEN 10 THEN 33 
                                      WHEN 9 THEN 32 
                                      WHEN 8 THEN 31 
                                      WHEN 7 THEN 30 
                                      WHEN 6 THEN 29 
                                      WHEN 5 THEN 28 
                                      WHEN 4 THEN 27  
                                     END),
                                     ded.LocationKey2_vch = 
                                     (CASE 
                                      WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                      ELSE '' 
                                      END),
                                      ded.LocationKey1_vch = 
                                      (CASE 
                                      WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                      ELSE '' 
                                      END),
                                      ded.CellFlag_int =  
                                      (CASE 
                                      WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                      ELSE 0 
                                      END)                                                  
                                     WHERE
                                        cx.T_Z IS NOT NULL   
                                     AND
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">                         
                                     AND    
                                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                     AND 
                                        CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(Session.CONSUMEREMAIL)#"> 
                                        
                                        <!--- Limit to US or Canada?--->
                                        <!---  AND fx.CTRY IN ('U', 'u')  --->
                                </cfquery>
                                                                    
                            </cfif>                        
                            
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />   
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Inserted - not already on list") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                                                   
                        <cfcatch TYPE="any">
                            <!--- Squash possible multiple adds at same time --->	
                            
                            <!--- Does it already exist?--->
                            <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                                
                                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                                <cfset QueryAddRow(dataout) />
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                                <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
                                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                
                            <cfelse>
                            
                                <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                                <cfset QueryAddRow(dataout) />
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                                <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
                                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                            
                            </cfif>
                            
                        </cfcatch>       
                        
                        </cftry>
                    
                    </cfif> <!--- CheckIfOnList - not found --->     
                           					     
                <cfelse>
                
                    <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>  
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
	            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>            

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
          
    
</cfcomponent>
  