<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<!--- ************************************************************************************************************************* --->
	<!--- Enter CPP --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="EnterCpp" access="remote" output="true" hint="Enter a CPP">
		<cfargument name="inpCPPUUID" required="yes" default="0" />
		<cfscript>
			dataout = queryNew("SUCCESS, RXRESULTCODE, TYPE, MESSAGE");
			queryAddRow(dataout);
			querySetCell(dataout, "SUCCESS", false);
			querySetCell(dataout, "RXRESULTCODE", -1);
			querySetCell(dataout, "MESSAGE", "");
			
			inpCPPUUID = trim(inpCPPUUID);
			
			if (!len(inpCPPUUID)) {
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "RXRESULTCODE", -1);
				querySetCell(dataout, "MESSAGE", "Data is invalid!");
				return dataout;
			}
		</cfscript> 
		<cftry>
			<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT
					UserId_int,
					GroupId_int,
					CPP_Template_vch,
                    CPPStyleTemplate_vch
				FROM
					simplelists.customerpreferenceportal
				WHERE
					(
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">
					OR
						Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">
					)
				AND
					Active_int = 1
			</cfquery>
			<cfif getUser.UserId_int GT 0>
				<cfset Session.CPPUUID = TRIM(inpCPPUUID) />
				<cfset Session.CPPUSERID = getUser.UserId_int />
				<cfset Session.CPPTemplate = trim(getUser.CPP_Template_vch) />
                <cfset Session.CPPStyleTemplate_vch = trim(getUser.CPPStyleTemplate_vch) />
                <cfset QuerySetCell(dataout, "SUCCESS", true) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfelse>
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", -1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />

			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- Coding Standards?--->	
	<cffunction name="GetGroupList" access="public" output="true" returntype="query" hint="Get group list of CPP">
		<cfquery name="getGroupIds" datasource="#Session.DBSourceEBM#">
			SELECT GroupList_vch FROM simplelists.customerpreferenceportal WHERE CPP_UUID_vch = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUUID#">
		</cfquery>
		<cfset groupList = getGroupIds.GroupList_vch />
		<cfif Right(groupList, 1) eq ",">
			<cfset groupList = Left(groupList, len(groupList) - 1) />
		</cfif>
		<cfquery name="getGroups" datasource="#Session.DBSourceEBM#">
			SELECT 
            	GroupId_bi id, 
                GroupName_vch name 
            FROM 
            	simplelists.grouplist WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
			<cfif len(groupList)>
				AND GroupId_bi IN (#groupList#)
			</cfif>
			ORDER BY GroupName_vch
		</cfquery>
		
        <cfreturn getGroups />
        
	</cffunction>
     
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get CPP group data --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPGroupData" access="remote" output="false" hint="Get CPP group data">
       	<cfargument name="inpCPPUUID" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                            
                            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	
                <!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
                <cfif inpCPPUUID NEQ "0">    
					<!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                      
                    <!--- Auto Repair Expired Session --->
                    <cfinvoke 
                     component="cpp"
                     method="EnterCpp"
                     returnvariable="RetValCPPData">                         
                        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                    </cfinvoke>  
                    
                    <!--- Check for success --->
                    <cfif RetValCPPData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-7">
                    </cfif>
                </cfif>    
                                        
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0>
                                                            
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
	                <cfset LocalGroupIdList = Session.CPPGroupList />
					<cfif Right(LocalGroupIdList, 1) eq ",">
                        <cfset LocalGroupIdList = Left(LocalGroupIdList, len(LocalGroupIdList) - 1) />
                    </cfif>
        
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                
                        SELECT 
                            GroupId_bi id, 
                            GroupName_vch name 
                        FROM 
                            simplelists.grouplist WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
                        <cfif len(LocalGroupIdList)>
                            AND GroupId_bi IN (#LocalGroupIdList#)
                        </cfif>
                        
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "GROUPID", "0") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "Please select an area of interest") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                          
                    <cfloop query="GetGroups">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.id#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.name#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
                                    
                </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- Get detail data of CPP --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPDetailData" access="remote" output="true" hint="Get detail data of CPP">
		<cfargument name="inpCPPUUID" required="yes" default="" />
		<cfargument name="GroupId" required="yes" default="" />
				
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "No user message found.") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			
		<cftry>
			<cfif Session.CPPUSERID GT 0>
				<cfquery name="GetCPPMessage" datasource="#Session.DBSourceEBM#">
					SELECT 
						Desc_vch, 
						PrimaryLink_vch, 
						CPPMessage_vch
					FROM 
						simplelists.customerpreferenceportal 
					WHERE 
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
						AND
							( GroupList_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE=",%#TRIM(GroupId)#%,"> 
							OR GroupList_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#TRIM(GroupId)#%,">)
						AND
						Active_int = 1
				</cfquery>
				<cfif GetCPPMessage.RecordCount GT 0>
					<cfset dataout =  QueryNew("RXRESULTCODE, DESC, PRIMARYLINK, CPPMESSAGE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "DESC", "#GetCPPMessage.Desc_vch#") /> 
		            <cfset QuerySetCell(dataout, "PRIMARYLINK", "#GetCPPMessage.PrimaryLink_vch#") /> 
		            <cfset QuerySetCell(dataout, "CPPMESSAGE", "#GetCPPMessage.CPPMessage_vch#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		            <cfset QuerySetCell(dataout, "TYPE", -1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />               
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				
			<cfelse>
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
            </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.ErrorCode EQ "">
					<cfset cfcatch.ErrorCode = -1>
                </cfif>   

				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
	            <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.ErrorCode#) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
		
	</cffunction>
    
	<cffunction name="GetContactPreference" access="remote" output="true" hint="Get Preference By CPPUUID">
		<cfargument name="inpCPPUUID" required="yes" >
		
		<cfset var dataout = '0' />  
		
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfset CPPUUID = trim(inpCPPUUID)> 
			<cfset AccountID = Session.CPPACOUNTID >
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT 
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					OR
						c.Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					AND
						c.Active_int = 1
				</cfquery>
				
				<cfif GetCPPSETUPQUERY.RecordCount GT 0>
					
					<cfquery name="GetPreferenceCppAuth" datasource="#Session.DBSourceEBM#">
						SELECT 
							preferenceId_int,
							CPP_UUID_vch,
							Value_vch,
							Desc_vch,
							Order_int,
							GroupId_int
						FROM
							simplelists.cpp_preference 
                        where 
                        	CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
					</cfquery>
					<cfset phoneObject = ArrayNew(1)>							
					<cfset emailObject = ArrayNew(1)>
					<cfset smsObject = ArrayNew(1)>
					<cfset a=0>
					<cfset LanguagePreference = "">
					<!--- Must be in group link too ... --->					
                    <cfif GetCPPSETUPQUERY.IncludeIdentity_ti EQ 1 OR Session.CPPACOUNTID NEQ "0">
                        <cfquery name="GetContactFromCppAuth" datasource="#Session.DBSourceEBM#">
                            SELECT DISTINCT
                                cs.ContactAddressId_bi,
                                cs.ContactId_bi,
                                cs.ContactString_vch,
                                cs.ContactType_int,
                                cs.OptIn_int,
                                cl.LanguagePreference_vch
                            FROM 
                                simplelists.contactstring as cs
                            INNER JOIN 
                                simplelists.contactlist as cl
                            ON 
                                cs.ContactId_bi = cl.ContactId_bi
                            INNER JOIN
                            	simplelists.groupcontactlist as gcl   
                            ON 
                                cs.ContactAddressId_bi = gcl.ContactAddressId_bi AND gcl.GroupId_bi IN (SELECT GroupId_int FROM simplelists.cpp_preference WHERE CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">)
                            WHERE 
                                cl.Userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
                            AND 
                                cl.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">						
                            AND
                                cl.CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
                            AND
                                cs.ContactString_vch <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">	
                            ORDER BY 
                                cs.Created_dt DESC 	
                        </cfquery>                        
						<cfif GetContactFromCppAuth.RecordCount GT 0>
							<!---<cfset arrPreference = ListToArray(GetContactFromCppAuth.CustomField1_vch)>--->
							
							<cfloop query="GetContactFromCppAuth">
								<!---<cfset data.LANGUGE = GetContactFromCppAuth.CustomField2_int >--->
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 1>								
									<cfset ArrayAppend(phoneObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 2>
									<cfset ArrayAppend(emailObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
								
								<cfif GetContactFromCppAuth.ContactType_int EQ 3>
									<cfset ArrayAppend(smsObject, GetContactFromCppAuth.ContactString_vch)>
								</cfif>
							</cfloop>
							<cfif GetContactFromCppAuth.RecordCount GT 0>
								<cfloop query="GetContactFromCppAuth">
									<cfset LanguagePreference = GetContactFromCppAuth.LanguagePreference_vch>
									<cfbreak>
								</cfloop>	
							</cfif>
						</cfif>					
					</cfif>
                    					
					<!--- get preference data --->
					<cfset arrPreference = ArrayNew(1)>
					
					<cfset data = StructNew()>
					<cfset data.LANGUGE = "">				
					<cfset data.LanguagePreference_vch = LanguagePreference>
					<cfif GetPreferenceCppAuth.RecordCount GT 0>
						<cfloop query="GetPreferenceCppAuth">
							<cfset ArrayAppend(arrPreference, GetPreferenceCppAuth.preferenceId_int)>
						</cfloop>
					</cfif>
					
					<cfset data.PHONE = phoneObject>
					<cfset data.EMAIL = emailObject>
					<cfset data.SMS = smsObject>
					<cfset data.PREFERENCE = arrPreference>
					<cfset data.userId_int = GetCPPSETUPQUERY.userId_int>
					<cfset data.CPP_UUID_vch = GetCPPSETUPQUERY.CPP_UUID_vch>
					<cfset data.AccountID = AccountID>
					<cfset dataout =  QueryNew("RXRESULTCODE, DATA, MESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "DATA", data) />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "AccountID=#AccountID# inpCPPUUID=#inpCPPUUID# ") />  
				
				</cfif>
				
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="saveCppPortal" access="remote" output="true" hint="Get Preference By CPPUUID">
		<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="Preview" default="0">
		<cfargument name="inpStep" default="1">
		<!---<cfargument name="emailAddress" default="">--->
		<cfargument name="PREFERENCECHECK" default="">
		<!---<cfargument name="customFieldId" default="">--->
		<cfargument name="voiceCheck" default="0">
		<cfargument name="VoiceNumberObject" default="">
		<!---<cfargument name="voiceText1" default="">
		<cfargument name="voiceText2" default="">
		<cfargument name="voiceText3" default="">
        <cfargument name="voiceText4" default="">--->
		<cfargument name="smsCheck" default="0">
		<cfargument name="SMSNumberObject" default="">
		<!---<cfargument name="smsText1" default="">
		<cfargument name="smsText2" default="">
		<cfargument name="smsText3" default="">--->
		<cfargument name="emailCheck" default="0">
		<cfargument name="EmailObject" default="">		
		<!---<cfargument name="emailText" default="">--->
		<cfargument name="language" default="1"> 
		<cfargument name="accepConditionForm" default="0">
		<cfargument name="CDFData" default="">
        <cfset datdaout = {}/>
		<cfset CPPUUID = trim(inpCPPUUID)>
		<cfif VoiceNumberObject NEQ "">
			<cfset VoiceNumberObject = deserializeJSON(VoiceNumberObject)>
		</cfif> 
		<cfif SMSNumberObject NEQ "">
			<cfset SMSNumberObject = deserializeJSON(SMSNumberObject)>
		</cfif>
		<cfif EmailObject NEQ "">
			<cfset EmailObject = deserializeJSON(EmailObject)>
		</cfif>
		<cfif PREFERENCECHECK NEQ "">
			<cfset PREFERENCECHECK = deserializeJSON(PREFERENCECHECK)>
		</cfif>
		<cfset DebugStr = "" >
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout = {}>
            <cfset dataout.TYPE = "">
			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = "">                
            <cfset dataout.ERRMESSAGE = ""> 
			<cfset dataout.ROWCOUNT = 0>
			<cfset dataout.DebugStr = DebugStr>
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						(
								c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
							OR
								Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
							)
						AND
							c.Active_int > 0
				</cfquery>
				
				<!--- Set cpp uuid  --->
				<cfset CPPUUID = GetCPPSETUPQUERY.CPP_UUID_vch>
				
				<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
					<cfif listfind(GetCPPSETUPQUERY.StepSetup_vch,4) GT 0>
						<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,4))>
					</cfif>
				</cfif>
				
				<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 1>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							NOT IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				<cfelse>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				</cfif>
				
				<cfset var AccountID ='0'>
				
				<!---Able to remove all contacts--->
				<!---<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 2)>	
					<cfif GetPreferenceQuery.COUNT EQ 0 OR Arraylen(PREFERENCECHECK) EQ 0>
						<cfset dataout.MESSAGE = "Please check at least one Contact Preference! "> 
						<cfset dataout.RXRESULTCODE = -1>
						<cfset dataout.ROWCOUNT = 1>
						<cfreturn dataout />
					</cfif>
				</cfif>--->
				
				<cfset AccountID = Session.CPPACOUNTID>
				
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 3)>
					
					
					<cfif VOICECHECK EQ 0  AND EMAILCHECK EQ 0 AND SMSCHECK EQ 0 >
						<cfset dataout.MESSAGE = "Please check at least one Contact Method!"> 
						<cfset dataout.RXRESULTCODE = -1>
						<cfset dataout.ROWCOUNT = 1>
						<cfreturn dataout />
					</cfif>
					
					<!---validation per contact--->						
					<cfif voiceCheck EQ 1 >
						<cfif ArrayLen(VoiceNumberObject) GT 0>
							<cfset VIndex = 0>												
							<cfloop array="#VoiceNumberObject#" index="VoiceNumber">																								
								<cfif len(VoiceNumber) GT 0	>
									<cfif NOT isvalid('telephone', VoiceNumber)>
                                        <cfset dataout.MESSAGE = "Voice Number is not a valid telephone number!"> 
                                        <cfset dataout.RXRESULTCODE = -1>
                                        <cfset dataout.ROWCOUNT = 1>
                                        <cfset dataout.ERRORINDEX = VIndex>
                                        <cfset dataout.ERRORTYPE = "VOICE">
                                        <cfreturn dataout />
                                    </cfif>
                                </cfif>
								<cfset VIndex++>
							</cfloop>
						<!---<cfelse>
						 Allow blank for opt out	
							<cfset dataout.MESSAGE = "Voice Number is not a valid telephone number!"> 
							<cfset dataout.RXRESULTCODE = -1>	
							<cfset dataout.ROWCOUNT = 1>
							<cfset dataout.ERRORINDEX = 0>
							<cfset dataout.ERRORTYPE = "VOICE">
							<cfreturn dataout />--->
						</cfif>
					</cfif>
					
					<cfif smsCheck EQ 1 >
						<cfif ArrayLen(SMSNumberObject) GT 0>	
							<cfset SIndex = 0>											
							<cfloop array="#SMSNumberObject#" index="SMSNumber">
                            	<cfif len(SMSNumber) GT 0 >
									<cfif NOT isvalid('telephone', SMSNumber)>
                                        <cfset dataout.MESSAGE = "SMS Number is not a valid telephone number!"> 
                                        <cfset dataout.RXRESULTCODE = -1>
                                        <cfset dataout.ROWCOUNT = 1>
                                        <cfset dataout.ERRORINDEX = SIndex>
                                        <cfset dataout.ERRORTYPE = "SMS">
                                        <cfreturn dataout />
                                    </cfif>
                                </cfif>
								<cfset SIndex++>
							</cfloop>
						<!---<cfelse>
							 Allow blank for opt out
							<cfset dataout.MESSAGE = "SMS Number is not a valid telephone number!"> 
							<cfset dataout.RXRESULTCODE = -1>
							<cfset dataout.ROWCOUNT = 1>
							<cfset dataout.ERRORINDEX = 0>
							<cfset dataout.ERRORTYPE = "SMS">
							<cfreturn dataout />	--->
						</cfif>
					</cfif>
					
					<cfif emailCheck EQ 1 >
						<cfif ArrayLen(EmailObject) GT 0>	
							<cfset EIndex = 0>												
							<cfloop array="#EmailObject#" index="Email">								
								<cfif len(Email) GT 0 >
									<cfif NOT isvalid('email', Email)>
                                        <cfset dataout.MESSAGE = "Email Address invalid!"> 
                                        <cfset dataout.RXRESULTCODE = -1>
                                        <cfset dataout.ROWCOUNT = 1>
                                        <cfset dataout.ERRORINDEX = EIndex>
                                        <cfset dataout.ERRORTYPE = "EMAIL">
                                        <cfreturn dataout />
                                    </cfif>
                                </cfif>                                
								<cfset EIndex++>
							</cfloop>
						<!--- <cfelse>
							 Allow blank for opt out
							<cfset dataout.MESSAGE = "Email Address invalid!"> 
							<cfset dataout.RXRESULTCODE = -1>
							<cfset dataout.ROWCOUNT = 1>
							<cfset dataout.ERRORINDEX = 0>
							<cfset dataout.ERRORTYPE = "EMAIL">
							<cfreturn dataout />--->	
						</cfif>
					</cfif>
					
				</cfif>
				<cfset arrayStep = ListtoArray(GetCPPSETUPQUERY.STEPSETUP_VCH)>				
				<cfset lastStep = arrayStep[Arraylen(arrayStep)]>				
				<cfif accepConditionForm EQ 1 AND(GetCPPSETUPQUERY.Type_ti EQ 1 OR (GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ lastStep)) >	
					<cfif dataout.RXRESULTCODE GT 0>
							<!--- This is wrong - only delete contactstring not from group - not just entire contact --->
							
                            <!--- Add a group subtract to remove? --->
                            
							<!---delete ContactString existing with CPP_UUID_vch and Userid_int param--->
							<cfquery name="GetContactString" datasource="#Session.DBSourceEBM#">
								SELECT DISTINCT
	                                cs.ContactAddressId_bi,
	                                cs.ContactId_bi,
	                                cs.ContactString_vch,
	                                cs.ContactType_int,
	                                cs.OptIn_int,
	                                gcl.GroupId_bi
	                            FROM 
	                                simplelists.contactstring as cs
	                            INNER JOIN 
	                                simplelists.contactlist as cl
	                            ON 
	                                cs.ContactId_bi = cl.ContactId_bi
	                            INNER JOIN
	                            	simplelists.groupcontactlist as gcl   
	                            ON 
	                                cs.ContactAddressId_bi = gcl.ContactAddressId_bi 
	                            	AND 
	                            	gcl.GroupId_bi IN 
	                            	(
	                            		SELECT 
	                            			GroupId_int 
	                            		FROM 
	                            			simplelists.cpp_preference 
	                        			WHERE 
	                        				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
									)
	                            WHERE 
	                                cl.Userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
	                            AND 
	                                cl.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">						
	                            AND
	                                cl.CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
	                            AND
	                                cs.ContactString_vch <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">	
	                            ORDER BY 
	                                cs.Created_dt DESC 	
							</cfquery>
							
							
							<cfif GetContactString.RecordCount GT 0>
								<cfloop query="GetContactString">
									<cfquery name="DeleteGroupContactList" datasource="#Session.DBSourceEBM#">
									DELETE
									FROM
									    simplelists.groupcontactlist
									WHERE
										ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetContactString.ContactAddressId_bi#">
									AND
										GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetContactString.GroupId_bi#">
									</cfquery>	
								</cfloop>
							</cfif>
							
							<cfset groupContactList = []>
							<cfset preferenceArr = []>
							<cfif ArrayLen(PREFERENCECHECK) GT 0>
								<cfloop array="#PREFERENCECHECK#" index="index">
									<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
										SELECT 
											preferenceId_int,
											CPP_UUID_vch,
											Value_vch,
											Desc_vch,
											Order_int,
											GroupId_int
										FROM
											simplelists.cpp_preference 
										WHERE
											preferenceId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#index#">
										ORDER BY 
											Order_int DESC
									</cfquery>
									<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 0>
										<cfset arrayAppend(preferenceArr,SelectPreference.desc_vch )>
									<cfelse>
										<cfset arrayAppend(preferenceArr,SelectPreference.value_vch )>
									</cfif>
									<cfif NOT arrayfindNoCase(groupContactList,SelectPreference.groupId_int )>
										<cfset arrayAppend(groupContactList , SelectPreference.groupId_int )>
									</cfif>
								</cfloop>
							</cfif>
                            <!--- Update contact string if it exists--->
                            
                            <!--- JLP to do --->
                            <!---
							
							Still need to store CPP ID in contact data
							Still need a place to store language preference
							Still need a place to store list of preferences they checked - this should be a DB lookup?
							
							CustomField1_vch,
							CustomField2_int,
							cpp_uuid_vch												
							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
							CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
											
													
							--->
                            
                            
                            		
							<cfif VOICECHECK EQ 1 >
								<cfset DebugStr =DebugStr &  "Start voice = #VOICECHECK#" >								
                                <cfif ArrayLen(VoiceNumberObject) GT 0>																		
									<cfloop array="#VoiceNumberObject#" index="VoiceNumber">
		                                <cfset INPCONTACTSTRING = VoiceNumber>                     
		                                <!--- Allow voice numbers to include extension by default will connect call - wait 2 seconds then dial extension digits--->
		                                <!---<cfif TRIM(VOICETEXT4) NEQ "" AND ISNUMERIC(TRIM(VOICETEXT4))>
		                                	<cfset INPCONTACTSTRING = INPCONTACTSTRING & "X" & TRIM(VOICETEXT4) & "P2">
		                                </cfif> --->
		                                                                
		                                <!--- Add to each group in list --->
	                                	<cfloop array="#groupContactList#" index="GroupIDToAdd">
	                                
		                                	<!--- Validate GroupID--->
		                                	<cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>
		                                		<cfset DebugStr =DebugStr &  "GroupIDToAdd = #GroupIDToAdd#" >		
		                                        <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
		                                            <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
		                                            <cfinvokeargument name="INPCONTACTTYPEID" value="1">
		                                            <cfinvokeargument name="INPUSERID" value="#GetCPPSETUPQUERY.userId_int#">
		                                            <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
		                                            <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
		                                            <cfinvokeargument name="INP_SOURCEKEY" value="">	
													<cfinvokeargument name="INP_CPP_UUID" value="#CPPUUID#">
                                                    <cfinvokeargument name="INPCDFDATA" value="#CDFData#">
													<cfif Session.CPPACOUNTID NEQ 0>
														<cfinvokeargument name="INP_CPPID" value="#Session.CPPACOUNTID#">
													</cfif>
													<cfinvokeargument name="LANGUAGEPREFERENCE" value="#language#">													
		                                        </cfinvoke>
													
		                                        <!--- #AddContactResult.RXRESULTCODE# #AddContactResult.MESSAGE#--->
		                                        <!---<cfset DebugStr = DebugStr & "#AddContactResult.RXRESULTCODE# #AddContactResult.MESSAGE#">--->		                                        
		                                    </cfif>
										</cfloop>
	                              </cfloop>
								</cfif>  
							</cfif>
                                                        
							<cfif SMSCHECK EQ 1 >
                            	<cfset DebugStr =DebugStr &  "Start sms = #SMSCHECK#" >
	                       	 	<cfif ArrayLen(SMSNumberObject) GT 0>												
									<cfloop array="#SmsNumberObject#" index="SmsNumber">
	    	                            <cfset INPCONTACTSTRING = SmsNumber>    
		                                <!--- Add to each group in list --->
		                                <cfloop array="#groupContactList#" index="GroupIDToAdd">
		                                
		                                	<!--- Validate GroupID--->
		                                	<cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>
		                                
		                                        <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
		                                            <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
		                                            <cfinvokeargument name="INPCONTACTTYPEID" value="3">
		                                            <cfinvokeargument name="INPUSERID" value="#GetCPPSETUPQUERY.userId_int#">
		                                            <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
		                                            <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
		                                            <cfinvokeargument name="INP_SOURCEKEY" value="">
													<cfinvokeargument name="INP_CPP_UUID" value="#CPPUUID#">
                                                    <cfinvokeargument name="INPCDFDATA" value="#CDFData#">
													<cfif Session.CPPACOUNTID NEQ 0>
														<cfinvokeargument name="INP_CPPID" value="#Session.CPPACOUNTID#">
													</cfif>
													<cfinvokeargument name="LANGUAGEPREFERENCE" value="#language#">
		                                        </cfinvoke>
		                                        
		                                    </cfif>    
										
										</cfloop>
                                 	</cfloop>
								</cfif>  
							</cfif>          
							<cfif EMAILCHECK EQ 1 >
								<cfset DebugStr = DebugStr & "Start email = #EMAILCHECK#" >
                            	<cfif ArrayLen(EmailObject) GT 0>												
									<cfloop array="#EmailObject#" index="EmailStr">
	    	                            <cfset INPCONTACTSTRING = EmailStr>
		                                <!--- Scrub out any invalid characters - allow extensions SMS Style --->
		                                <!---<cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^##]", "", "ALL")>--->		                           		     

		                                <!--- Add to each group in list --->
		                                <cfloop array="#groupContactList#" index="GroupIDToAdd">
		                                	<!--- Validate GroupID--->
		                                	<cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>		                                			                                		
		                                        <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
		                                            <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
		                                            <cfinvokeargument name="INPCONTACTTYPEID" value="2">
		                                            <cfinvokeargument name="INPUSERID" value="#GetCPPSETUPQUERY.userId_int#">
		                                            <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
		                                            <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
		                                            <cfinvokeargument name="INP_SOURCEKEY" value="">
													<cfinvokeargument name="INP_CPP_UUID" value="#CPPUUID#">
                                                    <cfinvokeargument name="INPCDFDATA" value="#CDFData#">
													<cfif Session.CPPACOUNTID NEQ 0>
														<cfinvokeargument name="INP_CPPID" value="#Session.CPPACOUNTID#">
													</cfif>
													<cfinvokeargument name="LANGUAGEPREFERENCE" value="#language#">
		                                        </cfinvoke>
		                                    </cfif>
										</cfloop>
                                	</cfloop>
								</cfif> 
							</cfif>
						
						<cfset dataout.MESSAGE = "Create Contact Success! #DebugStr#"> 
						<cfset dataout.ERRMESSAGE = "#DebugStr#">  
						<cfset dataout.ROWCOUNT = 1>
                        
					</cfif>
					
	            </cfif>
               	<!---<cfset dataout.DebugStr = DebugStr>--->    
            <cfcatch TYPE="any">
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1>  
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">
                <cfset dataout.TYPE = "#cfcatch.TYPE#">  
                <cfset dataout.ROWCOUNT = 0>  
            </cfcatch>
            
            </cftry>     
			
		</cfoutput>
		   
		
        <cfreturn dataout />
    </cffunction>
	
	
	<cffunction name="GetTwitterAuthenticate" access="remote" output="true" hint="Get twitter authenticate">
		<cfargument name="inpCPPUUID" required="yes" >
	  	<cfset var dataout = '0' />

		<cftry>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

         	<cfscript>
			    application.objMonkehTweet = createObject('component',
			        '#cfcmonkehTweet#')
			        .init(
			            consumerKey         =   '#APPLICATION.Twitter_Consumerkey#' ,
			            consumerSecret      =   '#APPLICATION.Twitter_Consumersecret#' ,
			            oauthToken          =   '#APPLICATION.Twitter_AccessToken#' ,
			            oauthTokenSecret    =   '#APPLICATION.Twitter_Consumersecret#' ,
			            parseResults        =   true
			        );

			    authStruct = application.objMonkehTweet;

				authStruct = application.objMonkehTweet.getAuthorisation(callbackURL='#rootUrl#/session/twitter_authorize.cfm?inpCPPUUID=#inpCPPUUID#');

				if (authStruct.success){
					//	Here, the returned information is being set into the session scope.
					//	You could also store these into a DB (if running an application for multiple users)
					session.oAuthToken		= authStruct.token;
					session.oAuthTokenSecret	= authStruct.token_secret;
					session.CPPUUID		= #inpCPPUUID#;
					session.CPPACOUNTID = 0;
				}
		   </cfscript>
		   <cfif StructKeyExists(authStruct,"authURL")>
			   <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, REDIRECT_URL")>
	           <cfset QueryAddRow(dataout) />
	           <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	           <cfset QuerySetCell(dataout, "REDIRECT_URL", "#authStruct.authURL#") />
	           <cfset QuerySetCell(dataout, "TYPE", "") />
			   <cfset QuerySetCell(dataout, "MESSAGE", "") />
	           <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		   <cfelse>
			   <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
	           <cfset QueryAddRow(dataout) />
	           <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	           <cfset QuerySetCell(dataout, "TYPE", "") />
			   <cfset QuerySetCell(dataout, "MESSAGE", "Cannot Get Twitter Authentication.") />
	           <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		   </cfif>

        <cfcatch TYPE="any">
             <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>

            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />

        </cfcatch>
		</cftry>

		<cfreturn dataout />	
	  
                                             
	</cffunction>

    
    <cffunction name="AddContactStringToList" access="public" output="true" hint="Add phone number to list if not already on it - Assumes no existing unique top level contact already linked - this is stand alone - Only can be called by another method inside this local CFC">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPUSERID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        <cfargument name="INP_CPPID" required="no" default="">
        <cfargument name="INP_CPP_UUID" required="no" default="">
        <cfargument name="INP_SOCIALTOKEN" required="no" default="">
        <cfargument name="INPFIRSTNAME" required="no" default="">
        <cfargument name="INPLASTNAME" required="no" default="">
		<cfargument name="LANGUAGEPREFERENCE" required="no" default="">
        <cfargument name="INPCDFDATA" required="no" default="">

        <cfif INPCDFDATA NEQ "">
            <cfset INPCDFDATA = deserializeJSON(INPCDFDATA)>
        </cfif>
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = Duplicate
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate user is specified - private methods onlylay - handle gracefully if not --->
            	<cfif INPUSERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <!--- Clean up phone string --->        
                                     
                     <!--- Phone number validation--->                 
                    <cfif INPCONTACTTYPEID EQ 1>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
		          	</cfif>
                    
                    <!--- email validation --->
                    <cfif INPCONTACTTYPEID EQ 2>  
						<!--- Add email validation --->                        
		          	</cfif>
                    
                    <!--- SMS validation--->
                    <cfif INPCONTACTTYPEID EQ 3>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d]", "", "ALL")>
		          	</cfif>
                                        
                    <!--- Get time zone info ---> 
                    <cfset CurrTZ = 0>
                    
                    <!--- Get Locality info --->  
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">
                	                             
				  	<!--- Set default to -1 --->         
				   	<cfset NextContactListId = -1>         
				  	
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>				
                    	<cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>                 
                    </cfif>
                                    
					<!--- Add record --->
                    <cftry>
                    
                    
						<!--- Create generic contact first--->
                        <!--- Add Address to contact --->                    
                         <cfif (INPGROUPID EQ "") >
                            <cfthrow MESSAGE="Valid Group ID for this user account required! Please specify a group ID." TYPE="Any" detail="" errorcode="-6">
                         </cfif>
                    
                    
                    	<!--- Only add to group if greater than 0 --->
                       	<cfif TRIM(INP_CPP_UUID) NEQ "" AND INPGROUPID GT 0>  
							<!--- Verify user is group id owner--->                                   
                            <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                                SELECT
                                    COUNT(GroupName_vch) AS TOTALCOUNT 
                                FROM
                                    simplelists.grouplist
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">  
                                    AND
                                    GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                            </cfquery>  
                            
                            <cfif VerifyUnique.TOTALCOUNT EQ 0>
                                <cfthrow MESSAGE="Group ID does not exists for this user account! Try a different group." TYPE="Any" detail="" errorcode="-6">
                            </cfif> 
                    
                    	</cfif>
                        
                    	<cfset NextContactAddressId = "">
                        <cfset NextContactId = "">
                    
                    	<!--- Check if contact string exists in main list --->
                     	<cfquery name="CheckForContactString" datasource="#Session.DBSourceEBM#">
                         	SELECT 
                                   simplelists.contactlist.contactid_bi,
                                   simplelists.contactstring.contactaddressid_bi
                            FROM 
                               simplelists.contactstring
                               INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                            WHERE
                               simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                            AND
                               simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                            AND
                               simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                        	
							<cfif TRIM(INP_CPPID) NEQ "" AND TRIM(INP_CPP_UUID) NEQ "" >
                                AND 
                                    simplelists.contactlist.cppid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPPID)#"> 
                                AND 
                                    simplelists.contactlist.cpp_uuid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPP_UUID)#">                                 
                            </cfif>
                            
                        </cfquery> 
                        
                        
                        <cfif CheckForContactString.RecordCount GT 0> 
                        
							<cfset NextContactId = "#CheckForContactString.ContactId_bi#">
                            <cfset NextContactAddressId = "#CheckForContactString.ContactAddressId_bi#">
                            <cfquery name="DeleteContactVariable" datasource="#Session.DBSourceEBM#">
                                       DELETE FROM simplelists.contactvariable
                                       WHERE
                                        ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">
                                       AND
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                            </cfquery>
                            <cfloop collection="#INPCDFDATA#" item="item">
                                <cfset Fields = StructFind(INPCDFDATA,item)/>
                                <cfloop collection="#Fields#" item="itemField">
                                    <cfset value = StructFind(Fields,itemField)/>
                                    <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                           INSERT INTO simplelists.contactvariable
                                                (
                                                    UserId_int,
                                                    ContactId_bi,
                                                    VariableName_vch,
                                                    VariableValue_vch,
                                                    Created_dt,
                                                    CdfId_int
                                                 )
                                           VALUES
                                                (
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itemField#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item#">
                                                )
                                        </cfquery>
                                </cfloop>
                            </cfloop>

                        <cfelse> <!--- ContactList for this user does not exist yet --->
                                
						  	<!--- By default just add new unique contact during this method - add method later on to search for existing contact--->                        
                         	<cfquery name="AddToContactList" datasource="#Session.DBSourceEBM#" result="AddContactListResult">
                                    INSERT INTO simplelists.contactlist
                                        (
                                            UserId_int, 
                                            Created_dt, 
                                            LASTUPDATED_DT, 
                                            CPPID_vch, 
                                            CPP_UUID_vch, 
                                            socialToken_vch, 
                                            FirstName_vch, 
                                            LastName_vch,
                                            LanguagePreference_vch 
                                        )
                                    VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                            NOW(), 
                                            NOW(), 
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPPID)#">, 
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPP_UUID)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_SOCIALTOKEN)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPFIRSTNAME)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPLASTNAME)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LANGUAGEPREFERENCE)#"> 
                                         )                                        
                            </cfquery>   
                              
                            <cfset NextContactId = "#AddContactListResult.GENERATEDKEY#">  
                                
                                <cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#" result="AddContactResult">
                                   INSERT INTO simplelists.contactstring
                                        (
                                            ContactId_bi,
                                            Created_dt,
                                            LASTUPDATED_DT,
                                            ContactType_int,
                                            ContactString_vch,
                                            TimeZone_int,
                                            CellFlag_int,
                                            UserSpecifiedData_vch,
                                            OptIn_int
                                         )
                                   VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                            NOW(),
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">,
                                            1
                                        )
                                </cfquery>

                                <cfloop collection="#INPCDFDATA#" item="item">
                                    <cfset Fields = StructFind(INPCDFDATA,item)/>
                                    <cfloop collection="#Fields#" item="itemField">
                                        <cfset value = StructFind(Fields,itemField)/>
                                        <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                           INSERT INTO simplelists.contactvariable
                                                (
                                                    UserId_int,
                                                    ContactId_bi,
                                                    VariableName_vch,
                                                    VariableValue_vch,
                                                    Created_dt,
                                                    CdfId_int
                                                 )
                                           VALUES
                                                (
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itemField#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item#">
                                                )
                                        </cfquery>
                                    </cfloop>
                                </cfloop>

                              <cfset NextContactAddressId = #AddContactResult.GENERATEDKEY#>
                         
                        	</cfif>
                                 
                       <!--- Only add to group if greater than 0 --->
                       <cfif TRIM(INP_CPP_UUID) NEQ "" AND INPGROUPID GT 0>   
                         
                           <cfquery name="CheckForGroupLink" datasource="#Session.DBSourceEBM#">
                               SELECT 
                                   ContactAddressId_bi
                               FROM
                                   simplelists.groupcontactlist
                               WHERE
                                   ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
                               AND
                                   GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                           </cfquery> 
                                                 
                            <cfif CheckForGroupLink.RecordCount EQ 0> 
                           
                             <cfquery name="AddToGroup" datasource="#Session.DBSourceEBM#" result="AddToGroupResult">
                                   INSERT INTO 
                                       simplelists.groupcontactlist
                                       (ContactAddressId_bi, GroupId_bi)
                                   VALUES
                                       (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">)                                        
                               </cfquery>    
                           </cfif>
                        
                      </cfif>
                                                            
                       
<!---

Time Zone · The number of hours past Greenwich Mean Time. The time zones are:

   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
					<cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                            
							<!--- Get time zones, Localities, Cellular data --->
                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                UPDATE MelissaData.FONE AS fx JOIN
                                 MelissaData.CNTY AS cx ON
                                 (fx.FIPS = cx.FIPS) INNER JOIN
                                 simplelists.contactstring AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                SET 
                                  ded.TimeZone_int = (CASE cx.T_Z
                                  WHEN 14 THEN 37 
                                  WHEN 10 THEN 33 
                                  WHEN 9 THEN 32 
                                  WHEN 8 THEN 31 
                                  WHEN 7 THEN 30 
                                  WHEN 6 THEN 29 
                                  WHEN 5 THEN 28 
                                  WHEN 4 THEN 27  
                                 END),
                                 ded.State_vch = 
                                 (CASE 
                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                  ELSE '' 
                                  END),
                                  ded.City_vch = 
                                  (CASE 
                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                  ELSE '' 
                                  END),
                                  ded.CellFlag_int =  
                                  (CASE 
                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                  ELSE 0 
                                  END)                                                  
                                 WHERE
                                    cx.T_Z IS NOT NULL   
                                 AND
                                    ded.ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">                         
                                 AND    
	                                ded.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                    
                                    <!--- Limit to US or Canada?--->
                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
                            </cfquery>                                                 
                                        	                    
                        </cfif>                        
                        
						<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                                               
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->	
                        
                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                        	
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
                        <cfelse>
                        
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                        
                        </cfif>
                        
                    </cfcatch>       
                    
                    </cftry>
					     
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            		<cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Invalid UserId sepecified!") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
	            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	
	 <cffunction name="GetCPPSETUPQUERY" access="remote" output="false" hint="Get Cpp setup">
		<cfargument name="inpCPPUUID" required="yes" >
        <cfset dataout = {} />        
        <cftry>
			<cfquery name="getCppSetupQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					c.userId_int,
					c.CPP_UUID_vch,
					c.CPP_Template_vch,
					c.MultiplePreference_ti, 
					c.SetCustomValue_ti,
					c.IdentifyGroupId_int,
					c.UpdatePreference_ti, 
					c.VoiceMethod_ti, 
					c.SMSMethod_ti,
					c.EmailMethod_ti, 
					c.IncludeLanguage_ti, 
					c.IncludeIdentity_ti,
					c.StepSetup_vch,
					c.Active_int,
					c.Type_ti,
					c.CPP_Template_vch,
					c.IFrameActive_int,
					c.template_int,
					c.sizeType_int,
					c.width_int,
					c.height_int,
					c.customhtml_txt,
					c.displayType_ti,
					t.templateName_vch,
					t.fontFamily_vch,
					t.fontSize_int,
					t.fontSizeUnit_vch,
					t.fontWeight_vch,
					t.fontStyle_vch,
					t.fontVariant_vch,
					t.backgroundColor_vch,
					t.backgroundImage_vch,
					t.imageRepeat_vch,
					t.borderColor_vch,
					t.borderWidth_int,
					t.borderRadius_int,
					t.fontColor_vch,
					t.customCss_txt
				FROM 
					simplelists.customerpreferenceportal c
				LEFT Join
					simplelists.cpp_template t
				ON
					c.template_int = t.templateId_int
				WHERE
					(
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
					OR
						Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
					)
				AND
					c.Active_int > 0
			</cfquery>
            <cfquery name="GetLanguage" datasource="#Session.DBSourceEBM#">
				SELECT 
					LanguagePreference_vch  as LANGUAGE
				FROM 
					simplelists.cpp_languagepreference
				WHERE 
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#getCppSetupQuery.CPP_UUID_vch#">
			</cfquery>			
			<cfset dataout = {} />
			
			<cfset dataout.LANGDATA = Arraynew(1) />
			<cfif GetLanguage.RecordCount GT 0>
				<cfloop query="GetLanguage">
					<cfset ArrayAppend(dataout.LANGDATA, GetLanguage.LANGUAGE)> 
				</cfloop>
			</cfif>   
			    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.userId_int = getCppSetupQuery.userId_int/>
			<cfset dataout.CPP_UUID_vch = getCppSetupQuery.CPP_UUID_vch />
			<cfset dataout.CPP_Template_vch = getCppSetupQuery.CPP_Template_vch />
			<cfset dataout.MultiplePreference_ti = getCppSetupQuery.MultiplePreference_ti />
			<cfset dataout.SetCustomValue_ti = getCppSetupQuery.SetCustomValue_ti />
			<cfset dataout.IdentifyGroupId_int = getCppSetupQuery.IdentifyGroupId_int />		
			<cfset dataout.UpdatePreference_ti = getCppSetupQuery.UpdatePreference_ti />		
			<cfset dataout.VoiceMethod_ti = getCppSetupQuery.VoiceMethod_ti /> 
			<cfset dataout.SMSMethod_ti = getCppSetupQuery.SMSMethod_ti /> 
			<cfset dataout.EmailMethod_ti = getCppSetupQuery.EmailMethod_ti />
			<cfset dataout.IncludeLanguage_ti = getCppSetupQuery.IncludeLanguage_ti /> 
			<cfset dataout.IncludeIdentity_ti = getCppSetupQuery.IncludeIdentity_ti /> 
			<cfset dataout.StepSetup_vch = getCppSetupQuery.StepSetup_vch />
			<cfset dataout.Active_int = getCppSetupQuery.Active_int />
			<cfset dataout.Type_ti = getCppSetupQuery.Type_ti />
			<cfset dataout.CPP_Template_vch = getCppSetupQuery.CPP_Template_vch />
			<cfset dataout.IFrameActive_int = getCppSetupQuery.IFrameActive_int />
			<cfset dataout.template_int = getCppSetupQuery.template_int />
			<cfset dataout.sizeType_int = getCppSetupQuery.sizeType_int />
			<cfset dataout.width_int = getCppSetupQuery.width_int />
			<cfset dataout.height_int = getCppSetupQuery.height_int />
			<cfset dataout.customhtml_txt = getCppSetupQuery.customhtml_txt />
			<cfset dataout.displayType_ti = getCppSetupQuery.displayType_ti />
			<cfset dataout.templateName_vch = getCppSetupQuery.templateName_vch />
			<cfset dataout.fontFamily_vch = getCppSetupQuery.fontFamily_vch />
			<cfset dataout.fontSize_int = getCppSetupQuery.fontSize_int />
			<cfset dataout.fontSizeUnit_vch = getCppSetupQuery.fontSizeUnit_vch />
			<cfset dataout.fontWeight_vch = getCppSetupQuery.fontWeight_vch />
			<cfset dataout.fontStyle_vch = getCppSetupQuery.fontStyle_vch />
			<cfset dataout.fontVariant_vch = getCppSetupQuery.fontVariant_vch />
			<cfset dataout.backgroundColor_vch = getCppSetupQuery.backgroundColor_vch />
			<cfset dataout.backgroundImage_vch = getCppSetupQuery.backgroundImage_vch />
			<cfset dataout.imageRepeat_vch = getCppSetupQuery.imageRepeat_vch />
			<cfset dataout.borderColor_vch = getCppSetupQuery.borderColor_vch />
			<cfset dataout.borderWidth_int = getCppSetupQuery.borderWidth_int />
			<cfset dataout.borderRadius_int = getCppSetupQuery.borderRadius_int />
			<cfset dataout.fontColor_vch = getCppSetupQuery.fontColor_vch />
			<cfset dataout.customCss_txt = getCppSetupQuery.customCss_txt />			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	
	<cffunction name="GetPreferenceQuery" access="remote" output="false" hint="Get Preference">
		<cfargument name="CPP_UUID" required="yes" type="string" >
		<cfargument name="SetCustomValue" required="yes" type="numeric" >
        <cfset dataout = {} />        
        <cftry>
        
        	<!--- Decide which ones are already checked based on account id if specfied --->
            <cfquery name="getPreferenceQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					preferenceId_int,
					desc_vch,
                    IF(GroupId_int in 
                    (
                        SELECT DISTINCT 
                            GroupId_bi
                        FROM
                            simplelists.groupcontactlist
                        WHERE
                            GroupContactId_bi IN
                            (
                                SELECT * FROM
                                (	
                                    SELECT 
                                        GroupContactId_bi
                                    FROM
                                        simplelists.groupcontactlist 
                                    WHERE
                                        ContactAddressId_bi IN
                                        (
                                         SELECT * FROM 
                                            (
                                                SELECT 
                                                    cs.ContactAddressId_bi
                                                FROM 
                                                    simplelists.contactstring as cs
                                                INNER JOIN 
                                                    simplelists.contactlist as cl
                                                ON 
                                                    cs.ContactId_bi = cl.ContactId_bi
                                                WHERE                                               
                                                    cl.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID#">     
                                                    AND
                                                    cl.CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPACOUNTID#">
                                                                                                
                                            ) AS temp
                                      )
                                ) AS temp1	  
                            ) 
                    ), 'Yes', 'No') AS OnCheckList
				FROM 
					simplelists.cpp_preference
				WHERE
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID#">
				AND 
					<cfif SetCustomValue EQ 1>
						NOT IsNull(Value_vch)
					<cfelse>
						IsNull(Value_vch)
					</cfif>
				ORDER BY 
					Order_int 
			</cfquery>
                
			<cfset dataout = {} />    
			<cfset dataout.DATA = ArrayNew(1) />    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfloop query="getPreferenceQuery">
				<cfset item = {} />
				<cfset item.preferenceId_int = getPreferenceQuery.preferenceId_int/>
				<cfset item.desc_vch = getPreferenceQuery.desc_vch />	
                <cfset item.OnCheckList = getPreferenceQuery.OnCheckList />	
				<cfset ArrayAppend(dataout.DATA, item) />
			</cfloop>
			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetIdentifyGroupQuery" access="remote" output="false" hint="Get Identify Group">
		<cfargument name="CPP_UUID" required="yes" type="string" >
		<cfargument name="IdentifyGroupId" required="yes" type="numeric" >
        <cfset dataout = {} />        
        <cftry>
			
			<cfquery name="getIdentifyGroupQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					groupName_vch,
					hyphen_ti
				FROM 
					simplelists.cpp_identifygroup
				WHERE
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPP_UUID#">
				AND 
					IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IdentifyGroupId#">
			</cfquery>
			    
			<cfset dataout = {} />    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.groupName_vch = getIdentifyGroupQuery.groupName_vch/>
			<cfset dataout.hyphen_ti = getIdentifyGroupQuery.hyphen_ti />
			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetIdentifyQuery" access="remote" output="false" hint="Get Identify">
		<cfargument name="IdentifyGroupId" required="yes" type="numeric" >
        <cfset dataout = {} />        
        <cftry>
			
			<cfquery name="getIdentifyQuery" datasource="#Session.DBSourceEBM#">
				SELECT 
					size_int,
					identifyId_int
				FROM 
					simplelists.cpp_identify
				WHERE
					IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#IdentifyGroupId#">
			</cfquery>
			    
			<cfset dataout = {} />    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.size_int = getIdentifyQuery.size_int/>
			<cfset dataout.identifyId_int = getIdentifyQuery.identifyId_int />
			
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetCustomHtml" access="remote" output="false">
		<cfparam name="customHtmlId" default="-1">
		<cfset dataout = {}>
		<cfset dataout.HTML = "">
		<cfif customHtmlId EQ -1>
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Not valid custom html Id." />
			<cfset dataout.ERRMESSAGE = "Not valid custom html Id." />
			<cfreturn dataout>
		</cfif>
		<cftry>
        	<cfquery name="getCustomHtml" datasource="#Session.DBSourceEBM#">
				SELECT 
					customHtmlId_bi,
					customHtml_txt as html,
					CPP_UUID_vch 
				FROM 
					simplelists.cpp_customhtml
				WHERE
					customHtmlId_bi = <CFQUERYPARAM CFSQLTYPE="cf_sql_bigint" VALUE="#customHtmlId#">
			</cfquery>
			
			<cfloop query="getCustomHtml">
				<cfset dataout.HTML = (getCustomHtml.html)>
			</cfloop>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout.HTML = "">
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="GetStepInfo" access="remote" output="false">
		<cfparam name="inpCPPUUID" default="">
		<cfset dataout = {}>
		<cfset dataout.Data = Arraynew(1)>
		<cfif inpCPPUUID EQ "">
		    <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Not exist CPP." />
			<cfset dataout.ERRMESSAGE = "Not exist CPP." />
			<cfreturn dataout>
		</cfif>
		<cftry>
        	<cfquery name="getStepInfo" datasource="#Session.DBSourceEBM#">
				SELECT 
					sd.CPP_UUID_vch,
					sd.title_vch Title,
					sd.description_vch Description,
					sd.step_type_ti StepId 
				FROM 
					simplelists.cpp_stepdata AS sd
				JOIN
					simplelists.customerpreferenceportal AS cpp
				ON
					sd.CPP_UUID_vch = cpp.CPP_UUID_vch
				WHERE
				(
					sd.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#TRIM(inpCPPUUID)#">
					OR
					cpp.Vanity_vch = <CFQUERYPARAM CFSQLTYPE="cf_sql_varchar" VALUE="#TRIM(inpCPPUUID)#">
				)
			</cfquery>
			<cfif getStepInfo.RecordCount GT 0>
				<cfloop query="getStepInfo">
					<cfset StepInfo = {}>
					<cfset StepInfo.CPP_UUID_vch = getStepInfo.CPP_UUID_vch>
					<cfset StepInfo.Title = getStepInfo.Title>
					<cfset StepInfo.Description = getStepInfo.Description>
					<cfset StepInfo.StepId = getStepInfo.StepId>
					<cfset Arrayappend(dataout.Data,StepInfo)>
				</cfloop>
			</cfif>			
			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />			
        </cfcatch>
        </cftry>
		<cfreturn dataout>
	</cffunction>

    <cffunction name="GetCdfCppByID" output="true" access="remote">
        <cfargument name="inpCPPUUID" required="yes" default="" >
        <cfset dataout = {}>
        <cftry>
            <cfquery name="GetFieldsDataList" datasource="#Session.DBSourceEBM#">
                SELECT cd.title, cd.description, cdf.CdfName_vch, cdf.CdfId_int, cd.cppCdfHtmlId_bi, cdf.type_field
                FROM simplelists.cpp_cdf_html as cd
                left join simplelists.cpp_cdf_groups as cdg on cd.cppCdfHtmlId_bi = cdg.cppCdfHtmlId_bi
                right join simplelists.customdefinedfields as cdf on cdf.CdfId_int = cdg.CdfId_int
                WHERE
                    cd.CPP_UUID_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpCPPUUID#">
                Order by cd.cppCdfHtmlId_bi DESC
            </cfquery>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset arrFiled = ArrayNew(1)>
            <cfset strCDFID = '' />
            <cfloop query="GetFieldsDataList">
                <cfset FIELD = StructNew()>
                <cfset FIELD.title = GetFieldsDataList.title />
                <cfset FIELD.description = GetFieldsDataList.description />
                <cfset FIELD.NAME = GetFieldsDataList.CdfName_vch/>
                <cfset FIELD.FieldTPYE = GetFieldsDataList.type_field/>
                <cfset FIELD.IDSTEP = "step6." & GetFieldsDataList.cppCdfHtmlId_bi/>
                <cfset FIELD.ID = GetFieldsDataList.CdfId_int/>
                <cfset ArrayAppend(arrFiled, FIELD)>
                <cfif GetFieldsDataList.type_field EQ 1>
                    <cfset strCDFID = ListAppend(strCDFID, GetFieldsDataList.CdfId_int)>
                </cfif>
            </cfloop>
            <cfset dataout.field = arrFiled />
            <cfif strCDFID NEQ ''>
                <cfquery name="GetDefaultValues" datasource="#Session.DBSourceEBM#">
                    SELECT id, CdfId_int, default_value
                    FROM simplelists.default_value_custom_defined_fields
                    WHERE CdfId_int in (#strCDFID#);
                </cfquery>
            </cfif>
            <cfset arrDefaultFiled = ArrayNew(1)>
            <cfloop query="GetDefaultValues">
                <cfset DEFAULTVALUES = StructNew()>
                <cfset DEFAULTVALUES.VALUE = GetDefaultValues.default_value/>
                <cfset DEFAULTVALUES.ID = GetDefaultValues.id/>
                <cfset DEFAULTVALUES.CDFID = GetDefaultValues.CdfId_int/>
                <cfset ArrayAppend(arrDefaultFiled, DEFAULTVALUES)>
            </cfloop>
            <cfset dataout.Defaultfields = arrDefaultFiled />
            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>

    <cffunction name="GetCdfVariable" output="true" access="remote">
        <cfargument name="inpCPPUUID" required="yes" default="" >
        <cfset dataout = {}>
        <cftry>
            <cfquery name="GetVariableData" datasource="#Session.DBSourceEBM#">
                SELECT cv.CdfId_int, cv.VariableValue_vch, cdf.type_field
                FROM simplelists.contactvariable cv
                INNER JOIN simplelists.contactlist cl
                ON cv.ContactId_bi = cl.ContactId_bi
                INNER JOIN simplelists.cpp_cdf_groups cg
                ON cg.CdfId_int = cv.CdfId_int
                INNER JOIN simplelists.customdefinedfields cdf
                ON cdf.CdfId_int = cv.CdfId_int
                WHERE
                 cl.CPP_UUID_vch= <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpCPPUUID#">
                AND
                 cl.CPPID_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#Session.CPPACOUNTID#">
                Group By cg.CdfId_int
            </cfquery>

            <cfif GetVariableData.RecordCount GT 0>
                <cfset arrFiled = ArrayNew(1)>
                <cfloop query="GetVariableData">
                    <cfset DATA = StructNew()>
                    <cfset DATA.CDFID = GetVariableData.CdfId_int />
                    <cfset DATA.VALUE = GetVariableData.VariableValue_vch/>
                    <cfset DATA.FieldType = GetVariableData.type_field />
                    <cfset ArrayAppend(arrFiled, DATA)>
                </cfloop>
                <cfset dataout.DATA = arrFiled />
            <cfelse>
                <cfset dataout.DATA = [] />
            </cfif>
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataOut>
    </cffunction>
</cfcomponent>
