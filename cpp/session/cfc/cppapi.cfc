<cfcomponent>
	
    
    
    
    
    <cffunction name="turnIPAddressIntoBigInteger" access="public" returntype="numeric" hint="Converts IP Addresses to big integers">   
    <cfargument name="ipAddress" required="yes" type="string" hint="The IP Address">
   
		<cfset var IPAddressPart1 = "255">
        <cfset var IPAddressPart2 = "255">
        <cfset var IPAddressPart3 = "255">
        <cfset var IPAddressPart4 = "255">
        <cfset var cIpAddress = "">
            
   		<cftry>
			<cfset cIpAddress = TRIM(arguments.ipAddress)>
                   
            <!---  Treat each IP as a list delimited by periods. Get all 4 parts --->
            <cfset IPAddressPart1 = listGetAt(cIpAddress,1,".")>
            <cfset IPAddressPart2 = listGetAt(cIpAddress,2,".")>
            <cfset IPAddressPart3 = listGetAt(cIpAddress,3,".")>
            <cfset IPAddressPart4 = listGetAt(cIpAddress,4,".")>
       <cfcatch type="any">
       
			<cfset IPAddressPart1 = "255">
            <cfset IPAddressPart2 = "255">
            <cfset IPAddressPart3 = "255">
            <cfset IPAddressPart4 = "255">
        
       </cfcatch>
       
       </cftry>
            
           
        <!--- Return the big integer version --->
        <cfreturn (IPAddressPart1 * 256 * 256 * 256) + (IPAddressPart2 * 256 * 256) + (IPAddressPart3 * 256) + IPAddressPart4>
           
	</cffunction>

    
    
    
    
    <cffunction name="isIPInThisRange" access="public" returntype="boolean" hint="Determines if the IP is in the range">   
    <cfargument name="ipAddress"    required="yes" type="string" hint="The IP Address to test">
    <cfargument name="rangeStartIP" required="yes" type="string" hint="The IP Address that starts the range">
    <cfargument name="rangeEndIP"   required="yes" type="string" hint="The IP Address that ends the range"> 
   
		<cfset var isInTheRange = false> 
       
        <!--- Convert the input IPs --->
        <cfset var convertedIpAddress = turnIPAddressIntoBigInteger(arguments.ipAddress)>
        <cfset var convertedRangeStartIP = turnIPAddressIntoBigInteger(arguments.rangeStartIP)>
        <cfset var convertedRangeEndIP = turnIPAddressIntoBigInteger(arguments.rangeEndIP)>
       
        <!--- If the IP big integer is in between the range big integers, then yes, this IP is in the range --->   
        <cfif convertedIpAddress GTE convertedRangeStartIP AND convertedIpAddress LTE convertedRangeEndIP>
            <cfset isInTheRange = true>
        </cfif>
       
        <cfreturn isInTheRange>
               
    </cffunction>

   
    
    <cffunction name="getIPRangeQuery" access="public" returntype="query" hint="Converts IP ranges into a query of big integer representations">   
   
		<!--- Create the query to hold the IP ranges --->
        <cfset var IPRangesQuery = queryNew("startIp,endIp","BigInt,BigInt")>
       
        <!--- All of the IP Ranges in ColdfusionCountry --->
        <cfset var ipRangeList = "62.245.2.224 62.245.2.255
                                  80.94.96.0 80.94.111.255
                                  193.201.151.64 193.201.151.127
                                  194.9.12.0 194.9.13.255
                                  217.204.159.108 217.204.159.111">
       
        <cfset var thisRowIPRange = "">
        <cfset var thisRowStartIp = "">
        <cfset var thisRowEndIp = "">
        <cfset var bigIntStartIp = "">
        <cfset var bigIntEndIp = "">
       
        <!---  Treat the list as delimited by new lines --->
        <cfloop list="#ipRangeList#" index="thisRowIPRange" delimiters="#chr(10)##chr(13)#">
              
            <!--- Treat each row as a list delimited by a space and get the first and second items --->
            <cfset thisRowStartIp = TRIM(listGetAT(TRIM(thisRowIPRange),1," "))>
            <cfset thisRowEndIp = TRIM(listGetAT(TRIM(thisRowIPRange),2," "))>
              
            <!--- Get the big int version of the Range IPs --->
            <cfset bigIntStartIp = turnIPAddressIntoBigInteger(thisRowStartIp)>
            <cfset bigIntEndIp = turnIPAddressIntoBigInteger(thisRowEndIp)>
              
            <!--- Add a new row to the query --->
            <cfset queryAddRow(IPRangesQuery)>
           
            <!--- Save the bigints into the query --->
            <cfset querySetCell(IPRangesQuery,"startIp",bigIntStartIp)>
            <cfset querySetCell(IPRangesQuery,"endIp",bigIntEndIp)>
              
        </cfloop>
                   
        <cfreturn IPRangesQuery>
               
    </cffunction>

    
    
    
    <cffunction name="isIPInThisRange2" access="public" returntype="boolean" hint="Determines if the IP is in the ColdfusionCountry IP ranges">   
    <cfargument name="ipAddress"    required="yes" type="string" hint="The IP Address to test">
   
    <cfset var isInTheRange = false> 
		<cfset var isThisIPInTheRangeQry = "">
       
        <!--- Convert the input IPs --->
        <cfset var convertedIpAddress = turnIPAddressIntoBigInteger(arguments.ipAddress)>
        <!--- Get the converted range query --->
        <cfset var coldfusionCountryRangesQry = getIPRangeQuery()>  
       
        <!--- Check if this IP range falls in between any of the converted IP ranges --->
        <cfquery name="isThisIPInTheRangeQry" dbtype="query">
            SELECT COUNT(*)
            FROM coldfusionCountryRangesQry
            WHERE <cfqueryparam value="#convertedIpAddress#" cfsqltype="CF_SQL_BIGINT"> BETWEEN startIp AND endIp
        </cfquery>
       
        <!--- If the count is greater than 0, then yes, this IP does fall in the range. --->
        <cfif isThisIPInTheRangeQry.recordCount gt 0>
            <cfset isInTheRange = true>
        </cfif>
       
        <cfreturn isInTheRange>
               
    </cffunction>




	<!--- Insert contact to list  - Ignore passwords this version DO NOT UPDATE? --->
    
    <!--- Delete contact from list --->    
    
    <!--- Get list of all groups available ot this CPP UUID --->
    
    <!--- Get list of contacts for a particular group--->
    
    <!---
	
		test strings
		http://dev.telespeech.com/devjlp/EBM_DEV/public/CPP/cfc/cppapi.cfc?inpCPPUUID=6493698516&inpGroupID=12&inpCPPUSERID=jpijeff@hotmail.com&method=GetCPPContactData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true		
		http://dev.telespeech.com/devjlp/EBM_DEV/public/CPP/cfc/cppapi.cfc?inpCPPUUID=6493698516&inpGroupID=12&inpCPPUSERID=jpeterson@messagebroadcast.com&method=GetCPPContactData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true		
		
		
	
	--->        
    
    <cffunction name="GetCPPContactData" access="remote" output="false" hint="Get CPP contact data for currently logged in user.">
       	<cfargument name="inpCPPUUID" required="yes">
        <cfargument name="inpCPPUSERID" required="yes" hint="Unique idendifier for the current user - can be email address or account number">
        <cfargument name="inpServicePassword" required="no" default="" hint="Your CPP system administrator sets this for you - can optionally also require IP ranges be pre-specified">
        <cfargument name="inpGroupID" required="yes">
                        
        <!--- Only accept request through HTTPS - require SSL on CPP to use these calls --->
                	
		<cfset var dataout = '0' />    
        <cfset DebugStr = "">                                       
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Invalid CPP
		-4 = Invalid IP
		-5 = Invalid Password
		    
	     --->          
                           
       	<cfoutput>
                            
           	<cftry> 
            
            	<!--- Check if there is an existing authenticated session for this current request --->
                
				<cfif Session.CPPUUID NEQ TRIM(inpCPPUUID)>
                   
					<!--- Validate request --->
                    <cfquery name="GetCPPData" datasource="#Session.DBSourceEBM#">
                        SELECT  
                            UserId_int,
                            grouplist_vch,
                            AES_DECRYPT( SystemPassword_vch, '#APPLICATION.EncryptionKey_UserDB#') AS SystemPassword_vch,                     
                            IPFilter_vch
                        FROM 
                            simplelists.customerpreferenceportal 
                        WHERE 
                            CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
                            AND 
                            Active_int = 1
                    </cfquery>
                       
                       
                    <cfif GetCPPData.RecordCount EQ 0>   
                        <cfthrow MESSAGE="CPP UUID is not active or does not exist." TYPE="Any" detail="" errorcode="-3">
                    </cfif>
                                     
                    
                    <!--- Validate IP address --->
                    
                    <!--- Validate Password ---> 
                    <cfif GetCPPData.SystemPassword_vch NEQ "">  
                    
                    	<cfif GetCPPData.SystemPassword_vch NEQ TRIM(inpServicePassword)> 
	                        <cfthrow MESSAGE="CPP password is invalid." TYPE="Any" detail="" errorcode="-1">
                        </cfif>
                    
                    </cfif>
                    
                    
                    <!--- Only accept request through HTTPS - require SSL on CPP to use these calls handle through application at the domain level--->
                    
                    <cfset Session.CPPUSERID = GetCPPData.UserId_int>
                    <cfset Session.CPPUUID = TRIM(inpCPPUUID)>
                
				<cfelse>
                
                                
                
                </cfif>                
                    
                
				
				<!--- This is not part of permanent session and after security checks are complete this will need to be upadted to latest request --->
				<cfset Session.CONSUMEREMAIL = inpCPPUSERID> 
                                
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0 AND TRIM(Session.CONSUMEREMAIL) NEQ "">
                     
                    <cfinvoke 
                     component="contact"
                     method="GetCPPContactData"
                     returnvariable="RetValCPPData">                         
                        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                        <cfinvokeargument name="inpGroupID" value="#inpGroupID#"/>
                    </cfinvoke>  
                                                            
                    <cfset dataout = RetValCPPData>
                                                                                            
                <cfelse>
                
	                <cfthrow MESSAGE="Session can not be established! general Error." TYPE="Any" detail="" errorcode="-1">
                                    
                </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, CONTACTTYPEID, CONTACTSTRING, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "CONTACTTYPEID", "-1") /> 
                <cfset QuerySetCell(dataout, "CONTACTSTRING", "NA") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>
    
     	<cfreturn dataout />
     
     </cffunction>
     
    
    
    
    
</cfcomponent>