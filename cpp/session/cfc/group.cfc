<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0" />
	<cfparam name="Session.DBSourceEBM" default="Bishop" />

	<!--- ************************************************************************************************************************* --->
	<!--- Get group data --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="GetGroupData" access="remote" output="false" hint="Get contact data">
		<cfargument name="page" type="numeric" required="no" default="1" />
		<cfargument name="rows" type="numeric" required="no" default="10" />
		<cfargument name="sidx" required="no" default="name" />
		<cfargument name="sord" required="no" default="ASC" />
		<cfargument name="_search" type="boolean" required="no" default=false />
		<cfargument name="searchField" required="no" default="" />
		<cfargument name="searchString" required="no" default="" />
		<cfargument name="searchOper" required="no" default="" />
		<cfscript>
			var dataout = structNew();
			dataout.page = page;
			dataout.rows = arrayNew(1);
			switch (sidx) {
				case "name": 
				{
					sidx = "Desc_vch";
					break;
				}
			}
		</cfscript> 
		<cfquery name="getGroups" dataSource="#Session.DBSourceEBM#">
			SELECT GroupId_int id, Desc_vch name FROM simplelists.simplephonelistgroups WHERE UserId_int = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.CPPUSERID#">
			<cfif #_search#>
				<cfswitch expression="#searchField#">
					<cfcase value="name">
						<cfswitch expression="#searchOper#">
							<cfcase value="eq">
								AND Desc_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
							</cfcase>
							<cfcase value="ne">
								AND Desc_vch != <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#">
							</cfcase>
							<cfcase value="bw">
								AND Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#%">
							</cfcase>
							<cfcase value="bn">
								AND Desc_vch NOT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#searchString#%">
							</cfcase>
							<cfcase value="ew">
								AND Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#">
							</cfcase>
							<cfcase value="en">
								AND Desc_vch NOT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#">
							</cfcase>
							<cfcase value="cn">
								AND Desc_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#%">
							</cfcase>
							<cfcase value="nc">
								AND Desc_vch NOT LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#searchString#%">
							</cfcase>
						</cfswitch>
					</cfcase>
				</cfswitch>
			</cfif>
			ORDER BY #sidx# #sord# 
		</cfquery>
		<cfset dataout.total = ceiling(getGroups.RecordCount/rows) />
		<cfset dataout.records = getGroups.RecordCount />
		<cfset start = rows * (page - 1) + 1 />
		<cfset end = (start-1) + rows />
		<cfloop query="getGroups" startrow="#start#" endrow="#end#">
			<cfif find("," & getGroups.id & ",", Session.CPPgrouplist)>
				<cfset inCPP = 1 />
			<cfelse>
				<cfset inCPP = 0 />
			</cfif>
			<cfset group = [getGroups.id, getGroups.name, inCPP] />
			<cfset arrayAppend(dataout.rows, group) />
		</cfloop>
		<cfreturn dataout />
	</cffunction>


	<cffunction name="jqGridAction" access="remote" output="true" hint="Actions for jqGrid">
		<cfargument name="oper" required="no" default="add" />
		<cfargument name="id" required="no" default="_empty" />
		<cfargument name="name" required="no" default="" />
		<cfargument name="possible" required="no" default="0" />
		<cfscript>
			dataout = structNew();
			dataout.success = true;
			dataout.message = "";
		</cfscript> 
		<cftry>
			<cfswitch expression="#oper#">
				<cfcase value="add">
					<!--- Get next Lib ID for current user --->
					<cfquery name="GetNextGroupId" datasource="#Session.DBSourceEBM#">
						SELECT CASE WHEN MAX(GroupId_int) IS NULL THEN 5 ELSE MAX(GroupId_int) + 1 END AS NextGroupId FROM simplelists.simplephonelistgroups WHERE UserId_int = #Session.CPPUSERID# 
					</cfquery>
					<cfset NextGroupId = #GetNextGroupId.NextGroupId# />
					<!---Pre-reserve the first five group ids for DNC, and other defaults--->
					<cfif NextGroupId LT 5>
						<cfset NextGroupId = 5 />
					</cfif>
					<!--- Add record --->
					<cfquery name="InsertGroup" datasource="#Session.DBSourceEBM#">
						INSERT INTO simplelists.simplephonelistgroups (GroupId_int, UserId_int, Desc_vch) VALUES ( 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextGroupId#">
						, #Session.CPPUSERID#, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#name#">
						) 
					</cfquery>
					<!--- Set possible --->
					<cfif possible>
						<cfset Session.CPPgrouplist &= NextGroupId & "," />
						<cfset nucpp = true />
					</cfif>
				</cfcase>
				<cfcase value="edit">
					<!--- Update record --->
					<cfquery name="UpdateGroup" datasource="#Session.DBSourceEBM#">
						UPDATE simplelists.simplephonelistgroups SET Desc_vch = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#name#">
						WHERE UserId_int = #Session.CPPUSERID# AND GroupId_int = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#id#">
					</cfquery>
					<!--- Set possible --->
					<cfif possible>
						<cfif not find("," & id & ",", Session.CPPgrouplist)>
							<cfset Session.CPPgrouplist &= id & "," />
							<cfset nucpp = true />
						</cfif>
					<cfelse>
						<cfif find("," & id & ",", Session.CPPgrouplist)>
							<cfset Session.CPPgrouplist = Replace(Session.CPPgrouplist, id & ",", "") />
							<cfset nucpp = true />
						</cfif>
					</cfif>
				</cfcase>
				<cfcase value="del">
					<!--- Delete record --->
					<cfquery name="DeleteGroup" datasource="#Session.DBSourceEBM#">
						DELETE FROM simplelists.simplephonelistgroups WHERE UserId_int = #Session.CPPUSERID# AND GroupId_int = 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#id#">
					</cfquery>
					<cfif find("," & id & ",", Session.CPPgrouplist)>
						<cfset Session.CPPgrouplist = Replace(Session.CPPgrouplist, id & ",", "") />
						<cfset nucpp = true />
					</cfif>
				</cfcase>
				<cfdefaultcase></cfdefaultcase>
			</cfswitch>
			<cfif isDefined("nucpp") and nucpp>
				<cfquery name="updateCPP" datasource="#Session.DBSourceEBM#">
					UPDATE simplelists.customerpreferenceportal SET grouplist_vch = 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPgrouplist#">
					WHERE CPP_UUID_vch = 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUUID#">
				</cfquery>
				<cfset dataout.gl = Session.CPPgrouplist />
			</cfif>
			<cfcatch type="any">
				<cfset dataout.success = false />
				<cfset dataout.message = cfcatch.MESSAGE />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


</cfcomponent>
