<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<!--- ************************************************************************************************************************* --->
	<!--- Enter CPP --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="EnterCpp" access="remote" output="true" hint="Enter a CPP">
		<cfargument name="inpCPPUUID" required="yes" default="" />
		<cfscript>
			dataout = queryNew("SUCCESS, RXRESULTCODE, TYPE, MESSAGE");
			queryAddRow(dataout);
			querySetCell(dataout, "SUCCESS", false);
			querySetCell(dataout, "RXRESULTCODE", -1);
			querySetCell(dataout, "MESSAGE", "");
			
			inpCPPUUID = trim(inpCPPUUID);
			
			if (!len(inpCPPUUID)) {
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "RXRESULTCODE", -1);
				querySetCell(dataout, "MESSAGE", "Data is invalid!");
				return dataout;
			}
		</cfscript> 
		<cftry>
			<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, 
					GroupId_int, 
					CPP_Template_vch,
                    CPPStyleTemplate_vch 
				FROM 
					simplelists.customerpreferenceportal 
				WHERE 
					(
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
					OR
						Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
					)
				AND 
					Active_int = 1
			</cfquery>
			<cfif getUser.UserId_int GT 0>
				<cfset Session.CPPUUID = TRIM(inpCPPUUID) />
				<cfset Session.CPPUSERID = getUser.UserId_int />
				<cfset Session.CPPTemplate = trim(getUser.CPP_Template_vch) />
                <cfset Session.CPPStyleTemplate_vch = trim(getUser.CPPStyleTemplate_vch) />
                <cfset QuerySetCell(dataout, "SUCCESS", true) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfelse>
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", -1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />
				
			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- Coding Standards?--->	
	<cffunction name="Getgrouplist" access="public" output="true" returntype="query" hint="Get group list of CPP">
		<cfquery name="getGroupIds" datasource="#Session.DBSourceEBM#">
			SELECT grouplist_vch FROM simplelists.customerpreferenceportal WHERE CPP_UUID_vch = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUUID#">
		</cfquery>
		<cfset groupList = getGroupIds.grouplist_vch />
		<cfif Right(groupList, 1) eq ",">
			<cfset groupList = Left(groupList, len(groupList) - 1) />
		</cfif>
		<cfquery name="getGroups" datasource="#Session.DBSourceEBM#">
			SELECT GroupId_int id, Desc_vch name FROM simplelists.simplephonelistgroups WHERE UserId_int = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
			<cfif len(groupList)>
				AND GroupId_int IN (#groupList#)
			</cfif>
			ORDER BY Desc_vch
		</cfquery>
		<cfreturn getGroups />
	</cffunction>
     
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get CPP group data --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPGroupData" access="remote" output="false" hint="Get CPP group data">
       	<cfargument name="inpCPPUUID" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                            
                            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	
                <!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
                <cfif inpCPPUUID NEQ "0">    
					<!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                      
                    <!--- Auto Repair Expired Session --->
                    <cfinvoke 
                     component="cpp"
                     method="EnterCpp"
                     returnvariable="RetValCPPData">                         
                        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                    </cfinvoke>  
                    
                    <!--- Check for success --->
                    <cfif RetValCPPData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-7">
                    </cfif>
                </cfif>    
                                        
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0>
                                                            
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
	                <cfset LocalGroupIdList = Session.CPPgrouplist />
					<cfif Right(LocalGroupIdList, 1) eq ",">
                        <cfset LocalGroupIdList = Left(LocalGroupIdList, len(LocalGroupIdList) - 1) />
                    </cfif>
        
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            GROUPID_INT,
                            DESC_VCH
                        FROM
                           simplelists.simplephonelistgroups
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                        <cfif LEN(LocalGroupIdList)>
                            AND GROUPID_INT IN (#LocalGroupIdList#)
                        </cfif>
            
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "GROUPID", "0") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "Please select an area of interest") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                          
                    <cfloop query="GetGroups">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.GROUPID_INT#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
                                    
                </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- Get detail data of CPP --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPDetailData" access="remote" output="true" hint="Get detail data of CPP">
		<cfargument name="inpCPPUUID" required="yes" default="" />
		<cfargument name="GroupId" required="yes" default="" />
				
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "No user message found.") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			
		<cftry>
			<cfif Session.CPPUSERID GT 0>
				<cfquery name="GetCPPMessage" datasource="#Session.DBSourceEBM#">
					SELECT 
						Desc_vch, 
						PrimaryLink_vch, 
						CPPMessage_vch
					FROM 
						simplelists.customerpreferenceportal 
					WHERE 
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
						AND
							( grouplist_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE=",%#TRIM(GroupId)#%,"> 
							OR grouplist_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#TRIM(GroupId)#%,">)
						AND
						Active_int = 1
				</cfquery>
				<cfif GetCPPMessage.RecordCount GT 0>
					<cfset dataout =  QueryNew("RXRESULTCODE, DESC, PRIMARYLINK, CPPMESSAGE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "DESC", "#GetCPPMessage.Desc_vch#") /> 
		            <cfset QuerySetCell(dataout, "PRIMARYLINK", "#GetCPPMessage.PrimaryLink_vch#") /> 
		            <cfset QuerySetCell(dataout, "CPPMESSAGE", "#GetCPPMessage.CPPMessage_vch#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		            <cfset QuerySetCell(dataout, "TYPE", -1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />               
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				
			<cfelse>
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
            </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.ErrorCode EQ "">
					<cfset cfcatch.ErrorCode = -1>
                </cfif>   

				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
	            <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.ErrorCode#) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
		
	</cffunction>
    
	<cffunction name="GetContactPreference" access="remote" output="false" hint="Get Preference By CPPUUID">
		<cfargument name="inpCPPUUID" required="yes" >
		
		<cfset var dataout = '0' />  
		
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
			
			<cfset CPPUUID = trim(inpCPPUUID)> 
			<cfset AccountID = Session.CPPACOUNTID >
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT 
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					OR
						c.Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					AND
						c.Active_int = 1
				</cfquery>
				
				<cfif GetCPPSETUPQUERY.RecordCount GT 0>
					<cfquery name="GetContactFromCppAuth" datasource="#Session.DBSourceEBM#">
						SELECT 
							UserId_int,
					 		ContactTypeId_int,
					 		TimeZone_int,
					 		CPPID_vch,
					 		ContactString_vch,
					 		grouplist_vch,
					 		CustomField1_vch,
					 		CustomField2_int,
					 		Created_dt,
					 		cpp_uuid_vch
					 	FROM
					 		simplelists.rxmultilist
						WHERE 
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
						AND 
							CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
						AND
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">	
					</cfquery>
					
					<!--- get preference data --->
					<cfset arrPreference = ArrayNew(1)>
					
					<cfset data = StructNew()>
					<cfset data.LANGUGE = "">
					
					<cfset phoneObject = StructNew()>
					<cfset phoneObject.voiceText1 = "" >
					<cfset phoneObject.voiceText2 = "" >
					<cfset phoneObject.voiceText3 = "">
							
					<cfset email = "">
					<cfset smsObject = StructNew()>
					<cfset smsObject.smsText1 = "" >
					<cfset smsObject.smsText2 = "" >
					<cfset smsObject.smsText3 = "" >
					
					<cfif GetContactFromCppAuth.CPPID_vch NEQ 0>
						<cfset arrPreference = ListToArray(GetContactFromCppAuth.CustomField1_vch)>
						
						<cfloop query="GetContactFromCppAuth">
							<cfset data.LANGUGE = GetContactFromCppAuth.CustomField2_int >
							
							<cfif GetContactFromCppAuth.ContactTypeId_int EQ 1>
								<cfset phoneObject = StructNew()>
								<cfset phoneObject.voiceText1 = Mid(GetContactFromCppAuth.ContactString_vch, 1, 3) >
								<cfset phoneObject.voiceText2 = Mid(GetContactFromCppAuth.ContactString_vch, 4, 4) >
								<cfset phoneObject.voiceText3 = Mid(GetContactFromCppAuth.ContactString_vch, 8, 3)>
							</cfif>
							
							<cfif GetContactFromCppAuth.ContactTypeId_int EQ 2>
								<cfset email = GetContactFromCppAuth.ContactString_vch>
							</cfif>
							
							<cfif GetContactFromCppAuth.ContactTypeId_int EQ 3>
								<cfset smsObject = StructNew()>
								<cfset smsObject.smsText1 = Mid(GetContactFromCppAuth.ContactString_vch, 1, 3) >
								<cfset smsObject.smsText2 = Mid(GetContactFromCppAuth.ContactString_vch, 4, 4) >
								<cfset smsObject.smsText3 = Mid(GetContactFromCppAuth.ContactString_vch, 8, 3)>
							</cfif>
						</cfloop>
					</cfif>
					
					<cfset data.PHONE = phoneObject>
					<cfset data.EMAIL = email>
					<cfset data.SMS = smsObject>
					<cfset data.PREFERENCE = arrPreference>
					
					<cfset dataout =  QueryNew("RXRESULTCODE, DATA")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "DATA", data) />  
				
				</cfif>
				
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="saveCppPortal" access="remote" output="true" hint="Get Preference By CPPUUID">
		<cfargument name="inpCPPUUID" required="yes" >
		<cfargument name="Preview" default="0" >
		<cfargument name="inpStep" default="1" >
		<cfargument name="emailAddress" default="">
		<cfargument name="PREFERENCECHECK" default="">
		<cfargument name="customFieldId" default="">
		<cfargument name="voiceCheck" default="0">
		<cfargument name="voiceText1" default="">
		<cfargument name="voiceText2" default="">
		<cfargument name="voiceText3" default="">
		<cfargument name="smsCheck" default="0">
		<cfargument name="smsText1" default="">
		<cfargument name="smsText2" default="">
		<cfargument name="smsText3" default="">
		<cfargument name="emailCheck" default="0">
		<cfargument name="emailText" default="">
		<cfargument name="language" default="1"> 
		<cfargument name="accepConditionForm" default="0">
		<cfset var dataout = '0' />  
		<cfset CPPUUID = trim(inpCPPUUID)>

		<cfset voiceText1 = trim(voiceText1)>
		<cfset voiceText2 = trim(voiceText2)>
		<cfset voiceText3 = trim(voiceText3)>
		<cfset smsText1 = trim(smsText1)>
		<cfset smsText2 = trim(smsText2)>
		<cfset smsText3 = trim(smsText3)>
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT 
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
					OR
						c.Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
				</cfquery>
				
				<!--- Set cpp uuid  --->
				<cfset CPPUUID = GetCPPSETUPQUERY.CPP_UUID_vch>
				
				<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
					<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,4))>
				</cfif>
				
				<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 1>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							NOT IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				<cfelse>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				</cfif>
				
				<cfset var AccountID ='0'>
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 2)>	
					<cfif GetPreferenceQuery.COUNT EQ 0 OR  PREFERENCECHECK EQ ''>
						<cfset QuerySetCell(dataout, "MESSAGE","Please check at least one Contact Preference! ") /> 
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					</cfif>
				</cfif>
				
				<cfset AccountID = Session.CPPACOUNTID>
				
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 3)>	
				
					<cfif VOICECHECK EQ 0  AND EMAILCHECK EQ 0 AND SMSCHECK EQ 0 >
						<cfset QuerySetCell(dataout, "MESSAGE","Please check at least one Contact Method!") /> 
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					</cfif>
					
					<cfif VOICECHECK EQ 1 >
						<cfif VOICETEXT1 EQ '' OR VOICETEXT2 EQ '' OR VOICETEXT3 EQ '' 
									OR NOT isvalid('telephone',VOICETEXT1& VOICETEXT2 & VOICETEXT3)>
							<cfset QuerySetCell(dataout, "MESSAGE","Voice Number is not a telephone!") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
					
					<cfif SMSCHECK EQ 1 >
						<cfif SMSTEXT1 EQ '' OR SMSTEXT2 EQ '' OR SMSTEXT3 EQ '' OR NOT isvalid('telephone', SMSTEXT1 & SMSTEXT2 & SMSTEXT3)>
							<cfset QuerySetCell(dataout, "MESSAGE","SMS Number is not a telephone!") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
					
					<cfif EMAILCHECK EQ 1 >
						<cfif NOT isvalid('email',emailText)>	
							<cfset QuerySetCell(dataout, "MESSAGE","Email Address invalid!") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
				</cfif>
				
				<cfif accepConditionForm EQ 1 AND(GetCPPSETUPQUERY.Type_ti EQ 1 OR (GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep GTE listlen(GetCPPSETUPQUERY.STEPSETUP_VCH) - 1 )) >
					
					<cfif dataout.RXRESULTCODE GT 0>
						<!--- <cfif Preview EQ 0> --->
							<cfif AccountID NEQ 0>
								<!--- Clear previous data --->
								<cfquery name="ClearPreviousData" datasource="#Session.DBSourceEBM#">
									DELETE FROM
								 		simplelists.rxmultilist
								 	WHERE
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									AND 
										CPP_UUID_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
								</cfquery>
						    </cfif>
							
							<cfset groupContactList = [0]>
							<cfset preferenceArr = []>
							
							<cfif PREFERENCECHECK NEQ ''>
								<cfloop list="#PREFERENCECHECK#" index="index">
									<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
										SELECT 
											preferenceId_int,
											CPP_UUID_vch,
											Value_vch,
											Desc_vch,
											Order_int,
											GroupId_int
										FROM
											simplelists.cpp_preference 
										WHERE
											preferenceId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#index#">
										ORDER BY 
											Order_int DESC
									</cfquery>
									<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 0>
										<cfset arrayAppend(preferenceArr,SelectPreference.desc_vch )>
									<cfelse>
										<cfset arrayAppend(preferenceArr,SelectPreference.value_vch )>
									</cfif>
									<cfif NOT arrayfindNoCase(groupContactList,SelectPreference.groupId_int )>
										<cfset  arrayAppend(groupContactList , SelectPreference.groupId_int )>
									</cfif>
								</cfloop>
							</cfif>
							
							<cfif VOICECHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkVoiceExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
								 	WHERE
										ContactTypeId_int = 1
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									AND 
										ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">
								</cfquery>
								<cfif checkVoiceExist.RecordCount  GT 0 >
									<cfloop array="#groupContactList#" index="iCheckVoice">
										<cfif listfind(checkVoiceExist.grouplist_vch, iCheckVoice) EQ 0>
											<cfset checkVoiceExist.grouplist_vch &= iCheckVoice & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkVoiceExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 1
										AND 
											UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
										AND 
											CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											1,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
									<cfset SaveTimezone(
											inpContactString = "#VOICETEXT1& VOICETEXT2 & VOICETEXT3#",
											inpUserid = GetCPPSETUPQUERY.userId_int,
											inpContactType = 1,
											inpCppId = AccountID	
										)>
								</cfif>
							</cfif>
							<cfif SMSCHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkSMSExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
									WHERE 
										ContactTypeId_int = 3
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									AND 
										ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">
								</cfquery>

								<cfif checkSMSExist.RecordCount GT 0 >
									<cfloop array="#groupContactList#" index="iCheckSMS">
										<cfif listfind(checkSMSExist.grouplist_vch, iCheckSMS) EQ 0>
											<cfset checkSMSExist.grouplist_vch &= iCheckSMS & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkSMSExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 3
										AND 
											UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
										AND 
											CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											3,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
									<cfset SaveTimezone(
										inpContactString = "#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#",
										inpUserid = GetCPPSETUPQUERY.userId_int,
										inpContactType = 3,
										inpCppId = AccountID	
											)>
								</cfif>
							</cfif>
							<cfif EMAILCHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkEmailExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
								 	WHERE
										ContactTypeId_int = 2
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									AND 
										ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">
								</cfquery>
								<cfif checkEmailExist.RecordCount GT 0 >
									<cfloop array="#groupContactList#" index="iCheckEmail">
										<cfif listfind(checkEmailExist.grouplist_vch, iCheckEmail) EQ 0>
											<cfset checkEmailExist.grouplist_vch &= iCheckEmail & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET 
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkEmailExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 2
										AND 
											UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
										AND 
											CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											2,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
								</cfif>
							</cfif>
						<!--- </cfif> --->
						<cfset QuerySetCell(dataout, "MESSAGE","Create Contact Success!") />
					</cfif>
					
	            </cfif>
                           
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="SaveTimezone" access="remote" output="false" hint="Check save timezone">
		<cfargument name="inpContactString" required="yes" >
		<cfargument name="inpUserid" required="yes" >
		<cfargument name="inpContactType" required="yes" >
		<cfargument name="inpCppId" required="yes" >
		
		<cfif (inpContactType EQ 1 OR inpContactType EQ 3) >
	         <!--- Get time zones, Localities, Cellular data --->
	         <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
	             UPDATE MelissaData.FONE AS fx JOIN
	              MelissaData.CNTY AS cx ON
	              (fx.FIPS = cx.FIPS) INNER JOIN
	              simplelists.rxmultilist AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
	             SET 
	               ded.TimeZone_int = (CASE cx.T_Z
	               WHEN 14 THEN 37 
	               WHEN 10 THEN 33 
	               WHEN 9 THEN 32 
	               WHEN 8 THEN 31 
	               WHEN 7 THEN 30 
	               WHEN 6 THEN 29 
	               WHEN 5 THEN 28 
	               WHEN 4 THEN 27  
	              END),
	              ded.LocationKey2_vch = 
	              (CASE 
	               WHEN fx.STATE IS NOT NULL THEN fx.STATE
	               ELSE '' 
	               END),
	               ded.LocationKey1_vch = 
	               (CASE 
	               WHEN fx.CITY IS NOT NULL THEN fx.CITY
	               ELSE '' 
	               END),
	               ded.CellFlag_int =  
	               (CASE 
	               WHEN fx.Cell IS NOT NULL THEN fx.Cell
	               ELSE 0 
	               END)                                                  
	              WHERE
	                 cx.T_Z IS NOT NULL   
	              AND
	                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">                         
	              AND    
	                 ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString), 255)#">
	              AND 
	                 CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCppId)#"> 
	         </cfquery>
                                             
     	</cfif> 
	</cffunction>
	
	<cffunction name="GetTwitterAuthenticate" access="remote" output="false" hint="Get twitter authenticate">
		<cfargument name="inpCPPUUID" required="yes" >
	  	<cfset var dataout = '0' />
            
		<cftry>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
         	<cfscript>
			    application.objMonkehTweet = createObject('component',  
			        '#cfcmonkehTweet#')  
			        .init(  
			            consumerKey         =   '#APPLICATION.Twitter_Consumerkey#',  
			            consumerSecret      =   '#APPLICATION.Twitter_Consumersecret#',  
			            oauthToken          =   '#APPLICATION.Twitter_AccessToken#',  
			            oauthTokenSecret    =   '#APPLICATION.Twitter_Consumersecret#',  
			            parseResults        =   true  
			        ); 
			        
			    authStruct = application.objMonkehTweet;

				authStruct = application.objMonkehTweet.getAuthentication(callbackURL='#rootUrl#/#publicPath#/twitter_authorize.cfm?inpCPPUUID=#inpCPPUUID#');

				if (authStruct.success){
					//	Here, the returned information is being set into the session scope.
					//	You could also store these into a DB (if running an application for multiple users)
					session.oAuthToken		= authStruct.token;
					session.oAuthTokenSecret	= authStruct.token_secret;
					session.CPPUUID		= #inpCPPUUID#;
				}
		   </cfscript>
		   <cfif StructKeyExists(authStruct,"authURL")>
			   <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, REDIRECT_URL")>  
	           <cfset QueryAddRow(dataout) />
	           <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	           <cfset QuerySetCell(dataout, "REDIRECT_URL", "#authStruct.authURL#") />
	           <cfset QuerySetCell(dataout, "TYPE", "") />
			   <cfset QuerySetCell(dataout, "MESSAGE", "") />                
	           <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
		   <cfelse>
			   <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
	           <cfset QueryAddRow(dataout) />
	           <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	           <cfset QuerySetCell(dataout, "TYPE", "") />
			   <cfset QuerySetCell(dataout, "MESSAGE", "Can not get twitter authentication.") />                
	           <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
		   </cfif>
            
        <cfcatch TYPE="any">
             <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>       
            
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
        </cfcatch>
		</cftry>
        
		<cfreturn dataout />	
	  
                                             
	</cffunction>
	
</cfcomponent>
