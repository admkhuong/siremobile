<cfcomponent output="true">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../public/paths.cfm" >
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0" />
	<cfparam name="Session.DBSourceEBM" default="Bishop" />

	<cffunction name="generatePassword" access="public" returntype="string" output="false" hint="generate a new password">
		<!--- Set up available lower case values. --->
		<cfset strLowerCaseAlpha = "abcdefghijklmnopqrstuvwxyz" />
		<!---
			Set up available upper case values. In this instance, we
			want the upper case to correspond to the lower case, so
			we are leveraging that character set.
			--->
		<cfset strUpperCaseAlpha = UCase( strLowerCaseAlpha ) />
		<!--- Set up available numbers. --->
		<cfset strNumbers = "0123456789" />
		<!--- Set up additional valid password chars. --->
		<cfset strOtherChars = "~!@##$%^&*" />
		<!---
			When selecting random value, we want to be able to easily
			choose from the entire set. To this effect, we are going
			to concatenate all the previous valid character sets.
			--->
		<cfset strAllValidChars = (
			strLowerCaseAlpha &
			strUpperCaseAlpha &
			strNumbers &
			strOtherChars
			) />
		<!---
			Create an array to contain the password ( think of a
			string as an array of character).
			--->
		<cfset arrPassword = ArrayNew(1) />
		<!---
			When creating a password, there are certain rules that we
			need to follow (as deemed by the business logic). That is,
			the password must:
			- must be exactly 8 characters in length
			- must have at least 1 number
			- must have at least 1 uppercase letter
			- must have at least 1 lower case letter
			--->
		<!--- Select the random number from our number set. --->
		<cfset arrPassword[1] = Mid(strNumbers, RandRange(1, Len(strNumbers)), 1) />
		<!--- Select the random letter from our lower case set. --->
		<cfset arrPassword[ 2 ] = Mid(strLowerCaseAlpha, RandRange(1, Len(strLowerCaseAlpha)), 1) />
		<!--- Select the random letter from our upper case set. --->
		<cfset arrPassword[ 3 ] = Mid(strUpperCaseAlpha, RandRange(1, Len(strUpperCaseAlpha)), 1) />
		<!---
			ASSERT: At this time, we have satisfied the character
			requirements of the password, but NOT the length
			requirement. In order to do that, we must add more
			random characters to make up a proper length.
			--->
		<!--- Create rest of the password. --->
		<cfloop index="intChar" from="#(ArrayLen(arrPassword) + 1)#" to="8" step="1">
			<!---
				Pick random value. For this character, we can choose
				from the entire set of valid characters.
				--->
			<cfset arrPassword[intChar] = Mid(strAllValidChars, RandRange( 1, Len(strAllValidChars)), 1) />
		</cfloop>
		<!---
			Now, we have an array that has the proper number of
			characters and fits the business rules. But, we don't
			always want the first three characters to be of the
			same order (by type). Therefore, let's use the Java
			Collections utility class to shuffle this array into
			a "random" order.
			If you are not comfortable using the Java class, you
			can create your own shuffle algorithm.
			--->
		<cfset CreateObject("java", "java.util.Collections").Shuffle(arrPassword) />
		<!---
			We now have a randomly shuffled array. Now, we just need
			to join all the characters into a single string. We can
			do this by converting the array to a list and then just
			providing no delimiters (empty string delimiter).
			--->
		<cfset strPassword = ArrayToList(arrPassword, "") />
		<cfreturn strPassword />
	</cffunction>


	<cffunction name="RequestNewPassword" access="remote" output="true" hint="Consumer request new password">
        <cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="no" default="2">
        <cfargument name="inpCPPUUID" required="no" default="0">
           
        
        <cfset var dataout = '0' />
        
		<cfscript>
			dataout = queryNew("RXRESULTCODE, SUCCESS, MESSAGE");
			queryAddRow(dataout);
			QuerySetCell(dataout, "RXRESULTCODE", -1);
			querySetCell(dataout, "SUCCESS", false);
			querySetCell(dataout, "MESSAGE", "");
			
			INPCONTACTSTRING = trim(INPCONTACTSTRING);
			
			if (!len(INPCONTACTSTRING)) {
				QuerySetCell(dataout, "RXRESULTCODE", -1);
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "MESSAGE", "eMail address is not valid!");
				return dataout;
			}
		</cfscript> 
		
        <!--- Source is the consumer email reference by default - this is used in insert if needed below to uniquly distinguish this entry --->
        <cfset INP_SOURCEKEY = TRIM(INPCONTACTSTRING)>
        
        <cftry>
            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
         	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpCPPUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! Valid CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
            
            
			<!--- Validate email address --->
			<cfinvoke 
				component="validation"
				method="VldEmailAddress"
				returnvariable="safeeMail"><cfinvokeargument name="Input" value="#TRIM(INPCONTACTSTRING)#"/></cfinvoke>
			<cfif safeeMail NEQ true OR TRIM(INPCONTACTSTRING) EQ "">
				<cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
            
            <!--- Validate session still in play - handle gracefully if not --->
            <cfif Session.CPPUSERID GT 0>
            
             	<!--- Check if on list yet--->
                <!--- Contact is unique for each CPP UUID, CPP cuontact Identifier--->
                <cfquery name="CheckIfOnList" datasource="#Session.DBSourceEBM#">
                    SELECT                        	
                        COUNT(*) AS TOTALCOUNT                                                     
                    FROM
                        simplelists.contactlist
                    WHERE                
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                    AND 
                        CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#"> 
                    AND 
                        CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">                                   
                </cfquery> 		
                                    
                <!--- If on list already for this EBM User account then just add to group - if not already in group --->            
                <cfif CheckIfOnList.TOTALCOUNT EQ 0>     
              
              		<!--- Specify CPP info as well as group 0 so it wont be added to any group yet --->
              		<cfinvoke component="cpp" method="AddContactStringToList" returnVariable="AddContactResult">
                        <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
                        <cfinvokeargument name="INPCONTACTTYPEID" value="#INPCONTACTTYPEID#">
                        <cfinvokeargument name="INPUSERID" value="#Session.CPPUSERID#">
                        <cfinvokeargument name="INPGROUPID" value="0">
                        <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                        <cfinvokeargument name="INP_SOURCEKEY" value="">
                        <cfinvokeargument name="INP_CPPID" value="#TRIM(INPCONTACTSTRING)#">
                        <cfinvokeargument name="INP_CPP_UUID" value="#TRIM(inpCPPUUID)#">                        
                        <cfinvokeargument name="INP_SOCIALTOKEN" value="">
                    </cfinvoke>
                   
                              
                </cfif> <!--- CheckIfOnList - not found --->                    
                        
              <cfelse>
                
                    <cfthrow MESSAGE="Session Expired! No Valid CPP UUID Found." TYPE="Any" detail="" errorcode="-2">
                            
              </cfif>     
                        
			<!--- Set new password --->
			<cfset newPassword = generatePassword() />
			<cfquery name="updatePassword" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simplelists.contactlist 
				SET 
					CPPPWD_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#newPassword#">, '#APPLICATION.EncryptionKey_UserDB#') 				
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                AND 
                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">  
                AND 
                    CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">     
			</cfquery>
            
            <!--- Detail - email address can't be from MessageBroadcast - EBM User will want it to come from their own email address--->
			<!--- Send email --->
			
            
            <cfmail server="smtp.gmail.com" username="support@siremobile.com" password="SF927$tyir3" port="465" useSSL="true" to="#TRIM(INPCONTACTSTRING)#" from="support@siremobile.com" subject="Email new password for the Contact Preference Portal">
				Your new Multi-Factor Authentication password to signin is: #newPassword# 
			</cfmail>
    
      <!---      
            <cfmail to="#TRIM(INPCONTACTSTRING)#" from="info@messagebroadcast.com" subject="Email new password for MessageBroadcast Account" type="html">
				Your new password to signin is: #newPassword#
			</cfmail>
            
            <cfmail server="smtp.gmail.com" username="messagebroadcastsystem@gmail.com" password="mbs123456" port="465" useSSL="true" to="#TRIM(INPCONTACTSTRING)#" from="info@messagebroadcast.com" subject="Email new password for MessageBroadcast Account">
				Your new password to signin is: #newPassword# 
			</cfmail>
	 --->
            
            
            
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>  
            
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
        </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- Detail - Use like instead of = for email CPPID Match (Case Sensitivity) --->
    <!--- Research - Is it faster to do UCASE compare or LIKE statement? - --->
	<cffunction name="Signin" access="remote" output="true" hint="Consumer signin by email">
		<cfargument name="inpSigninUserID" required="yes" default="" />
		<cfargument name="inpSigninPassword" required="yes" default="" />
		<cfargument name="resetPasswordEveryTime" required="no" default="0" />
        <cfargument name="inpCPPUUID" required="no" default="0">
        <cfargument name="inpVanity" required="no" default="0">
                
        <cfset var dataout = '0' />
            
		<cftry>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
         	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpCPPUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
            
            <!--- Data validation -Start --->
			<cfif LEN(TRIM(inpSigninUserID)) GT 0>
				<!--- Validate email address --->
				<cfinvoke 
					component="validation"
					method="VldEmailAddress"
					returnvariable="safeeMail"><cfinvokeargument name="Input" value="#inpSigninUserID#"/></cfinvoke>
				<cfif safeeMail NEQ true>
					<cfthrow MESSAGE="eMail address is not valid!<br/>Exp: From@somewehere.domain" TYPE="Any" extendedinfo="" errorcode="-3" />
				</cfif>
				
			</cfif>
			
			<cfif LEN(TRIM(inpSigninUserID)) EQ 0 XOR LEN(TRIM(inpSigninPassword)) EQ 0> 
				<!--- if the email field is empty, but not password, insert "Please enter your email address" above the email form field. --->
	            <cfif LEN(TRIM(inpSigninUserID)) EQ 0 >     
		            <cfthrow MESSAGE="Please enter your email address." TYPE="Any" detail="" errorcode="-3">            
	            <cfelseif LEN(TRIM(inpSigninPassword)) EQ 0 > 
	             	<!--- If the password field is empty, but not the email address, and the email address has requested a password, insert "Please enter your password or request a new one below." above the password field --->
					<!--- <cfif getSigninUserID.RecordCount GT 0> --->
		            	<cfthrow MESSAGE="Please enter your password or request a new one below." TYPE="Any" detail="" errorcode="-3">
	            	<!--- If the password field is empty, and the email address is new, insert "Please request your password below." above the password form field. --->
	            	<!--- <cfelse>
	            		<cfthrow MESSAGE="Please request your password below." TYPE="Any" detail="" errorcode="-3">
					</cfif> --->
				</cfif>
				
			<!--- if the fields are both empty, insert "Please enter your email address and password" above the email form field. --->
			<cfelseif LEN(TRIM(inpSigninUserID)) EQ 0 AND LEN(TRIM(inpSigninPassword)) EQ 0>
				<cfthrow MESSAGE="Please enter your email address and password." TYPE="Any" detail="" errorcode="-4">
				
			<!--- If the password and email are not empty, and the email address exists, but the password is incorrect, insert "Invalid password. Request a new one below." above the password  form field. --->
				
			<cfelseif LEN(TRIM(inpSigninUserID)) GT 0 AND LEN(TRIM(inpSigninPassword)) GT 0>
				<!--- Look for SigninUserPW in List--->
				<cfquery name="getCPPPWD" datasource="#Session.DBSourceEBM#">
					SELECT 
						UserId_int
                    FROM
                        simplelists.contactlist
                    WHERE                
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                    AND 
                        CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninUserID)#"> 
                    AND 
                        (
                        	CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">
							OR
							CPP_UUID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpVanity)#">
                        )    
                    AND
						CPPPWD_vch = AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninPassword)#">, '#APPLICATION.EncryptionKey_UserDB#') 
				</cfquery>
				<!--- If the password and email are not empty, and the email address exists, but the password is incorrect, insert "Invalid password. Request a new one below." above the password  form field. --->
				<cfif getCPPPWD.RecordCount EQ 0>
					<cfthrow MESSAGE="Invalid password. Request a new one below." TYPE="Any" detail="" errorcode="-4">
				<!--- If the password and email are not empty, and the email address does not exist, insert "Invalid password. Please request a password below." above the password form field. --->
				<!--- <cfelseif getSigninUserID.RecordCount EQ 0>
					<cfthrow MESSAGE="Invalid password. Please request a password below." TYPE="Any" detail="" errorcode="-4"> --->
				</cfif>
			</cfif>
			
			
            <!--- Data validation -End --->
			
            <!--- Look for User in List--->
			<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT 
                    UserId_int,
                    ContactId_bi
                FROM
                    simplelists.contactlist
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                AND 
                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninUserID)#"> 
                AND 
                     (
                       	CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#">
						OR
						CPP_UUID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpVanity)#">
                    )      
                AND
                    CPPPWD_vch = AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninPassword)#">, '#APPLICATION.EncryptionKey_UserDB#') 
			</cfquery>
			<cfif getUser.RecordCount GT 0>
				<!--- Signin success --->
				<!--- Set session variables --->
				<!--- <cfset Session.CONSUMERUUID = getUser.UniqueCustomer_UUID_vch /> --->
				<cfset Session.CONSUMEREMAIL = TRIM(inpSigninUserID) />
				<cfset Session.resetPasswordEveryTime = resetPasswordEveryTime />
                <cfset Session.CPPPWD = inpSigninPassword />
                
				<!--- Update last access --->
				<cfquery name="updateLastAccess" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simplelists.contactlist
					SET 
						LastUpdated_dt = NOW() 
                    
                    <!--- Optional reset password on log in - require MFA--->
                    <cfif  resetPasswordEveryTime GT 0>
                    	, CPPPWD_vch = ""
                    </cfif>   
                        
					WHERE 
						ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.ContactId_bi#">					
				</cfquery>
                              
				<cfset Session.CPPACOUNTID = inpSigninUserID>	
				<cfset Session.cppuuid = inpVanity>	
				
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "OK") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
                <cfset QuerySetCell(dataout, "TYPE", "") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Invalid email or password") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			</cfif>
            
            
        <cfcatch TYPE="any">
             <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>       
            
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
        </cfcatch>
		</cftry>
        
		<cfreturn dataout />	
    </cffunction>


	<cffunction name="Signout" access="remote" output="true" hint="Consumer signout">
		<cfif (isDefined("Session.resetPasswordEveryTime") and Session.resetPasswordEveryTime)>
			<cfquery name="updatePassword" datasource="#Session.DBSourceEBM#">
				UPDATE simplelists.rxmultilist SET CPPPWD_vch = null WHERE UserId_int = 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
				AND CPPID_vch LIKE 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CONSUMEREMAIL#">
			</cfquery>
		</cfif>
		<cfset StructClear(Session) />
		<cfreturn />
	</cffunction>

	<!--- ************************************************************************************************************************* --->
	<!--- Add a new user account --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="RegisterNewAccount" access="remote" output="true" hint="Start a new user account">
		<cfargument name="email" required="yes" default="" />
		<cfargument name="password" required="yes" default="" />
        <cfargument name="inpUUID" required="no" default="Session.cppuuid" />
        
		<cfscript>
			dataout = queryNew("SUCCESS, MESSAGE");
			queryAddRow(dataout);
			querySetCell(dataout, "SUCCESS", true);
			querySetCell(dataout, "MESSAGE", "");
			
			email = trim(email);
			password = trim(password);
			
			if (!email.len() || !password.len()) {
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "MESSAGE", "Datas is invalid!");
				return dataout;
			}
		</cfscript> 
        
		<cftry>
			<!--- Validate email address --->
			<cfinvoke 
				component="validation"
				method="VldEmailAddress"
				returnvariable="safeeMail"><cfinvokeargument name="Input" value="#TRIM(email)#"/>
            </cfinvoke>
			
			<cfif safeeMail NEQ true OR email EQ "">
				<cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
                        
			<!--- Verify does not already exist--->
			<!--- Get next Lib ID for current user --->
			<cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TOTALCOUNT 				                  
                FROM
                    simplelists.contactlist
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                AND 
                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(email)#"> 
                AND 
                    CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpUUID)#">                               
			</cfquery>
                        
			<cfif VerifyUnique.TOTALCOUNT GT 0>
				<cfthrow MESSAGE="Email already already in use! Try a different email." TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
                        
			<!--- Validate proper password--->
			<cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safePass"><cfinvokeargument name="Input" value="#password#"/></cfinvoke>
			
			<cfif safePass NEQ true OR password EQ "">
				<cfthrow MESSAGE="Invalid password - try another" TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
							
			<!--- Specify CPP info as well as group 0 so it wont be added to any group yet --->
            <cfinvoke component="cpp" method="AddContactStringToList" returnVariable="AddContactResult">
                <cfinvokeargument name="INPCONTACTSTRING" value="#TRIM(email)#">
                <cfinvokeargument name="INPCONTACTTYPEID" value="2">
                <cfinvokeargument name="INPUSERID" value="#Session.CPPUSERID#">
                <cfinvokeargument name="INPGROUPID" value="0">
                <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                <cfinvokeargument name="INP_SOURCEKEY" value="">
                <cfinvokeargument name="INP_CPPID" value="#TRIM(email)#">
                <cfinvokeargument name="INP_CPP_UUID" value="#TRIM(inpCPPUUID)#">                        
                <cfinvokeargument name="INP_SOCIALTOKEN" value="">
            </cfinvoke>
                  
        <cfcatch TYPE="any">
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />				
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
        
		</cftry>
		
        
        <cfreturn dataout />
	</cffunction>

    
	<cffunction name="validateUsers" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's name and password match.">
		<cfargument name="inpUserID" TYPE="string" />
		<cfargument name="inpPassword" TYPE="string" />
		<cfargument name="inpRememberMe" TYPE="string" default="0" required="no" />
		<cfargument name="inpAt" TYPE="string" required="no" default="0" />
		<cfargument name="appId" TYPE="string" required="no" default="0" />
		<cfargument name="inpUUID" TYPE="string" required="no" default="0" />
		<cfargument name="appSecret" TYPE="string" required="no" default="0" />
		<cfargument name="facebook" TYPE="numeric" required="no" default="0" />
		<cfset var dataout = '0' />
		<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, USERNAME, EMAILADDRESS, MESSAGE, ERRMESSAGE") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "REASONMSG", "") />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = CF Failure
			-3 = 
			--->
		<!--- Ensure that attempts to authenticate start with new credentials. --->
		<cflogout/>
		<cftry>
        
        	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! Valid CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
                        
           	<cfset appSecret = "#APPLICATION.Facebook_Appsecret#">
			<!--- Check for too many tries this session and lockout for 5 min --->
			<cfset inpUserID = TRIM(inpUserID) />
			<cfset inpPassword = TRIM(inpPassword) />
			<!--- Server Level Input Protection      component="PremierIVR.model.Validation.validation" --->
			<cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safeName"><cfinvokeargument name="Input" value="#inpUserID#"/>
            </cfinvoke>
			
            <cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safePass"><cfinvokeargument name="Input" value="#inpPassword#"/>
            </cfinvoke>
			
			<cfif safeName eq true and safePass eq true>
				<!--- No reference to current session variables - exisits in its on session/application scope --->
				<cfif inpAt neq 0>

					<cfhttp url="https://graph.facebook.com/me/?access_token=#inpAt#" result="myDetails" />
					<cfset pData = DeserializeJSON(myDetails.filecontent) />
					<!--- if login by 'connect with facebook button' --->

					<cfif inpUserID eq ''>
						<CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
							SELECT 
                            	ContactId_bi 
                            FROM
                                simplelists.contactlist
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                            AND 
                                CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(pData.id)#"> 
                            AND 
                                CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpUUID)#">     
						</CFQUERY>
					
						<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#appId#&client_secret=#appSecret#&grant_type=fb_exchange_token&fb_exchange_token=#inpAt#" result="token_url" />
						<cfset accessContent = Right(token_url.filecontent,len(token_url.filecontent)-13) />
						<cfif find("&",accessContent)>
							<cfset accessContent = Left(accessContent,find("&",accessContent)-1)>
						</cfif>
                        
                        <!---- Store contact string to session----->
						<cfset Session.CPPACOUNTID = pData.id>
						
                        <!--- if facebook user id is already on contact list for the current CPP --->
						<cfif getUser.RecordCount GT 0>
							<CFQUERY name="UpdateContact" datasource="#Session.DBSourceEBM#">
                                UPDATE 
                                    simplelists.contactlist
                                SET 
 		                            socialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#accessContent#">,
                                    LastUpdated_dt = NOW()      
                                WHERE 
                                    ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.ContactId_bi#">
							</CFQUERY>
						<cfelse>
                                                
                        	<!--- Specify CPP info as well as group 0 so it wont be added to any group yet --->
                            <cfinvoke component="cpp" method="AddContactStringToList" returnVariable="AddContactResult">
                                <cfinvokeargument name="INPCONTACTSTRING" value="#pData.id#">
                                <cfinvokeargument name="INPCONTACTTYPEID" value="4">
                                <cfinvokeargument name="INPUSERID" value="#Session.CPPUSERID#">
                                <cfinvokeargument name="INPGROUPID" value="0">
                                <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                                <cfinvokeargument name="INP_SOURCEKEY" value="">
                                <cfinvokeargument name="INP_CPPID" value="#TRIM(pData.id)#">
                                <cfinvokeargument name="INP_CPP_UUID" value="#TRIM(inpUUID)#">                        
                                <cfinvokeargument name="INP_SOCIALTOKEN" value="#accessContent#">
                                <cfinvokeargument name="INPFIRSTNAME" value="#pData.first_name#">
                                <cfinvokeargument name="INPLASTNAME" value="#pData.last_name#">
                            </cfinvoke>
							
						</cfif>
						
                        <!--- Set CPP Session variables --->
						<cfset Session.cppuuid = #inpUUID#>
                        <cfset Session.UserName = #pData.name#>
						<cfset Session.CONSUMEREMAIL = TRIM(pData.email) />
						<cfset Session.resetPasswordEveryTime = 0 />
		                <cfset Session.CPPPWD = '' />
                        
						<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG,FBUSERID, USERNAME, EMAILADDRESS, TYPE, MESSAGE, ERRMESSAGE") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "REASONMSG", "Login Success! ") />
						<cfset QuerySetCell(dataout, "FBUSERID", #pData.id#) />
						<cfset QuerySetCell(dataout, "USERNAME", #pData.name#) />
						<cfset QuerySetCell(dataout, "EMAILADDRESS", #pData.email#) />
					</cfif>				
				</cfif>
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, USERNAME, EMAILADDRESS, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
				<cfset QuerySetCell(dataout, "REASONMSG", "Failed Server Level Validation") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, USERNAME, EMAILADDRESS, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
				<cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="validateContentTwitters" access="remote" output="true" hint="add contact into rxmultilist when signin by account twitter">
		<cfargument name="accessToken" TYPE="string" required="no" default="0" />
		<cfargument name="accessSecret" TYPE="string" required="no" default="0" />
		<cfargument name="userIdTwitter" TYPE="string" required="no" default="0" />
		<cfargument name="UserName" TYPE="string" required="no" default="0" />
		<cfargument name="email" TYPE="string" required="no" default="0" />
		<cfargument name="inpUUID" TYPE="string" required="no" default="0" />
		<cfset access = accessToken & '---' & accessSecret>
		<cfset dataout = ''>
		<cftry>

        	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpUUID NEQ "0">
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP --->

                <!--- Auto Repair Expired Session --->
                <!---<cfinvoke
                 component="cpp"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">
                    <cfinvokeargument name="inpCPPUUID" value="#inpUUID#"/>
                </cfinvoke>

                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! Valid CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>--->
            </cfif>

			<CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT
                    ContactId_bi
                FROM
                    simplelists.contactlist
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                AND
                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(userIdTwitter)#">
                AND
                    CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpUUID)#">
			</CFQUERY>

			<!---- Store contact string to session----->

			<cfset Session.CPPACOUNTID = userIdTwitter>


            <!--- if twitter user id is already on contact list for the current CPP --->
			<cfif getUser.RecordCount GT 0>
				<CFQUERY name="UpdateContact" datasource="#Session.DBSourceEBM#">
                	UPDATE
                        simplelists.contactlist
                    SET
                        socialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#access#">,
                        LastUpdated_dt = NOW()
                    WHERE
                        ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.ContactId_bi#">
				</CFQUERY>
			<cfelse>

            	<!--- Specify CPP info as well as group 0 so it wont be added to any group yet --->
                <cfinvoke component="cfc.cpp" method="AddContactStringToList" returnVariable="AddContactResult">
                    <cfinvokeargument name="INPCONTACTSTRING" value="#TRIM(userIdTwitter)#">
                    <cfinvokeargument name="INPCONTACTTYPEID" value="5">
                    <cfinvokeargument name="INPUSERID" value="#Session.CPPUSERID#">
                    <cfinvokeargument name="INPGROUPID" value="0">
                    <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                    <cfinvokeargument name="INP_SOURCEKEY" value="">
                    <cfinvokeargument name="INP_CPPID" value="#TRIM(userIdTwitter)#">
                    <cfinvokeargument name="INP_CPP_UUID" value="#TRIM(inpUUID)#">
                    <cfinvokeargument name="INP_SOCIALTOKEN" value="#access#">
                    <cfinvokeargument name="INPFIRSTNAME" value="#UserName#">
                    <cfinvokeargument name="INPLASTNAME" value="#UserName#">
                </cfinvoke>

			</cfif>

			<!--- Set CPP Session variables --->
            <cfset Session.CPPPWD = 'TWITTER#generatePassword()#' />
            <cfset Session.CPPACOUNTID = '#userIdTwitter#' />

            <cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />

            <cfset QuerySetCell(dataout, "TYPE", 1) />
            <cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

        <cfcatch TYPE="any">
            <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
            <cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
		</cftry>
		<cfreturn dataout/>
	</cffunction>


<!--- How do we get the correct CPPUUID here???????--->
	<cffunction name="validateContactGoogle" access="remote" output="true" hint="add contact into rxmultilist when signin by account google">
		<cfargument name="accessToken" TYPE="string" required="no" default="0" />
		<cfargument name="accessSecret" TYPE="string" required="no" default="0" />
		<cfargument name="userIdGoogle" TYPE="string" required="no" default="0" />
		<cfargument name="email" TYPE="string" required="no" default="0" />
		<cfargument name="FirstName" TYPE="string" required="no" default="0" />
		<cfargument name="LastName" TYPE="string" required="no" default="0" />
        <cfargument name="inpUUID" TYPE="string" required="no" default="0" />

		<cfset dataout = ''>
		<cftry>
        
        	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! Valid CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
            
            <cfset Session.UserName = '#FirstName# #LastName#'>
			<cfset Session.CONSUMEREMAIL = TRIM(email) />
			<cfset Session.resetPasswordEveryTime = 0 />
            <cfset Session.CPPPWD = '' />
			
            <CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">            
            	SELECT 
                    ContactId_bi 
                FROM
                    simplelists.contactlist
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                AND 
                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(userIdGoogle)#"> 
                AND 
                    CPP_UUID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpUUID)#">                      				
			</CFQUERY>
            
			<!---- Store contact string to session----->
			<cfset Session.CPPACOUNTID = userIdGoogle>
			
            <!--- if google user id is already on contact list for the current CPP --->
			<cfif getUser.RecordCount GT 0>
				<CFQUERY name="UpdateContact" datasource="#Session.DBSourceEBM#">                
                	UPDATE 
                        simplelists.contactlist
                    SET 
                        socialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#accessToken#">,
                        LastUpdated_dt = NOW()      
                    WHERE 
                        ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.ContactId_bi#">                     	
				</CFQUERY>
			<cfelse>
            
            <!--- Specify CPP info as well as group 0 so it wont be added to any group yet --->
                <cfinvoke component="cpp" method="AddContactStringToList" returnVariable="AddContactResult">
                    <cfinvokeargument name="INPCONTACTSTRING" value="#TRIM(userIdGoogle)#">
                    <cfinvokeargument name="INPCONTACTTYPEID" value="6">
                    <cfinvokeargument name="INPUSERID" value="#Session.CPPUSERID#">
                    <cfinvokeargument name="INPGROUPID" value="0">
                    <cfinvokeargument name="INPUSERSPECIFIEDDATA" value="">
                    <cfinvokeargument name="INP_SOURCEKEY" value="">
                    <cfinvokeargument name="INP_CPPID" value="#TRIM(userIdGoogle)#">
                    <cfinvokeargument name="INP_CPP_UUID" value="#TRIM(inpUUID)#">                        
                    <cfinvokeargument name="INP_SOCIALTOKEN" value="#accessToken#">
                    <cfinvokeargument name="INPFIRSTNAME" value="#FirstName#">
                    <cfinvokeargument name="INPLASTNAME" value="#LastName#">
                </cfinvoke>   
				
			</cfif>
			
            <!--- Set CPP Session variables --->	
			<cfset Session.cppuuid = #inpUUID#>
            <cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />

            <cfset QuerySetCell(dataout, "TYPE", 1) />
            <cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

        <cfcatch TYPE="any">
            <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, MESSAGE, ERRMESSAGE") />
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
            <cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
        </cfcatch>
		</cftry>
		<cfreturn dataout/>
	</cffunction>
	
</cfcomponent>
