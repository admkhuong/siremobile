<cfparam name="inpCPPUUID" default="" >
<cfparam name="Preview" default="0" >
<cfparam name="STEP" default="1" >
<cfset inpCPPUUID = trim(inpCPPUUID)>
<cfset portalLeft = ''>
<cfset portalRight = ''>
<cftry>
	
	<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
		SELECT 
			c.CPP_UUID_vch,
			c.CPP_Template_vch,
			c.MultiplePreference_ti, 
			c.SetCustomValue_ti,
			c.IdentifyGroupId_int,
			c.UpdatePreference_ti, 
			c.VoiceMethod_ti, 
			c.SMSMethod_ti,
			c.EmailMethod_ti, 
			c.IncludeLanguage_ti, 
			c.IncludeIdentity_ti,
			c.StepSetup_vch,
			c.Active_int,
			c.Type_ti,
			c.CPP_Template_vch,
			c.IFrameActive_int,
			c.template_int,
			t.customhtml
		FROM 
			simplelists.customerpreferenceportal c
		LEFT Join
			simplelists.cpptemplate t
		ON
			c.template_int = t.tempalteId_int
		WHERE
			c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
		OR
			Vanity_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCPPUUID#">
		
	</cfquery>	
	
	<cfif GetCPPSETUPQUERY.Active_int NEQ 1 OR GetCPPSETUPQUERY.IFrameActive_int NEQ 1>
		This Customer Preference Portal is offline!
		<cfexit>
	</cfif>
	
	<cfset templateId = trim(GetCPPSETUPQUERY.template_int)>
	<cfinclude template="templateStyle.cfm">
	
	<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 1>
		<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				preferenceId_int,
				desc_vch
			FROM 
				simplelists.cpp_preference
			WHERE
				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
			AND 
				NOT IsNull(Value_vch)
			ORDER BY 
				Order_int 
		</cfquery>
	<cfelse>
		<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				preferenceId_int,
				desc_vch
			FROM 
				simplelists.cpp_preference
			WHERE
				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
			AND 
				IsNull(Value_vch)
			ORDER BY 
				Order_int 
		</cfquery>
	</cfif>
	
	<cfif GetCPPSETUPQUERY.IdentifyGroupId_int GT 0>
		<cfquery name="GetIdentifyGroupQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				groupName_vch,
				hyphen_ti
			FROM 
				simplelists.cpp_identifygroup
			WHERE
				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetCPPSETUPQUERY.CPP_UUID_vch#">
			AND 
				IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfquery>
		<cfquery name="GetIdentifyQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				size_int,
				identifyId_int
			FROM 
				simplelists.cpp_identify
			WHERE
				IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfquery>
	</cfif>
	
	<cfset leftStrLength = FINdNoCase( '{%portal%}', GetCPPSETUPQUERY.customhtml)>
	<cfif leftStrLength GT 0>
		<cfset portalLeft = left(GetCPPSETUPQUERY.customhtml, leftStrLength - 1)>
		<cfset portalRight = replaceNoCase(GetCPPSETUPQUERY.customhtml, portalLeft , '') >
		<cfset portalRight = replaceNoCase(portalRight, '{%portal%}' , '') >
	</cfif>
	
	<cfcatch TYPE="any">
		<cfoutput>#cfcatch.MESSAGE#</cfoutput>
		<cfexit>
    </cfcatch>
	        
</cftry> 

<cfoutput>#portalLeft#</cfoutput>

<cfinvoke 
	component="#cfcDotPath#.cpp" 
	method="GetContactPreference" 
	returnvariable="retGetContactPreferenceValue">
	<cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
</cfinvoke>

<cfset data = retGetContactPreferenceValue.DATA>
<cfset i =1>
<cfform method="post" name="cppLaunchForm" id="cppLaunchForm">
	<cfinput type="hidden" name="inpStep" id="inpStep" value= "">
	<cfinput type="hidden" name="preview" id="preview" value= "0">
	<cfinput type="hidden" name="includeLanguage" id="includeLanguage" value= "#GetCPPSETUPQUERY.INCLUDELANGUAGE_TI#">
	<cfinput type="hidden" name="accepConditionForm" id="accepConditionForm">
	
	<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
		<cfif listfind(GetCPPSETUPQUERY.StepSetup_vch,4) GT 0>
			<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,4))>
		</cfif>
	</cfif>
	
	<img src ="<cfoutput>#rootUrl#/#publicPath#/images/logo-ebm.png</cfoutput>">
	
	<div id="containerLaunch">
		<cfif Session.CONSUMEREMAIL NEQ 0>
  			<a href="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/act_logout.cfm?inpCPPUUID=<cfoutput>#url.inpCPPUUID#</cfoutput>" >Log out</a>
	  	</cfif>
		<cfloop list="#GetCPPSETUPQUERY.StepSetup_vch#" index="index">
			<cfinput name="stepVal#index#" type="hidden" id="stepVal#i#" value ="#index#" >
			<div class = "cppPortalRow" id="stepContent<cfoutput>#i#</cfoutput>" <cfif GetCPPSETUPQUERY.Type_ti EQ 2 AND i NEQ STEP> style="display:none" </cfif>>
				<cfif index eq 2>
					<h2 class="cppStepTitle preTitle" >Step <cfoutput>#i#</cfoutput>:<span> (Contact Preference)</span></h2>
					<div class="cppPortalDesc preDesc" >
						Please select which services you wish to stay informed about by checking the box next to the selected service.
					</div>
					<div class="cppPortalContent">
						<cfloop query="GetPreferenceQuery">
							<cfset checked = false>
							<!--- check value selected --->
							<cfloop array="#data.PREFERENCE#" index="preIndex">
								<cfif GetPreferenceQuery.DESC_VCH EQ preIndex>
									<cfset checked = true>
								</cfif>
							</cfloop>
							
							<div class="cppPortalContentRow preContent" >
								<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
									<cfinput type = "checkbox" name="preferenceCheck" value="#GetPreferenceQuery.preferenceId_int#" checked="#checked#" class="removeCheck">
								<cfelse>
									<cfinput type = "radio" name="preferenceCheck" value="#GetPreferenceQuery.preferenceId_int#" checked="#checked#" class="removeCheck">
								</cfif>
								<cfoutput>#GetPreferenceQuery.Desc_vch#</cfoutput>
							</div>
						</cfloop>
					</div>
					<cfset i++>
				</cfif>
				<!--- <cfif index eq 2 >
					<h2 class="cppStepTitle" id="accTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Account ID)</span></h2>
					<div class="cppPortalDesc" id="accDesc">
						Please enter the Billing phone number associated with your AT&T account.
					</div>
					<div class="cppPortalContent">
						<div class="cppPortalContentRow">
							<cfif GetCPPSETUPQUERY.IDENTIFYGROUPID_INT EQ -2>
								<label id="accLabel" >Billing Number*</label> <br>
								<cfinput type="text" name="billing1" class="removeText" size="4">
								- <cfinput type="text" name="billing2" class="removeText" size="4">
								- <cfinput type="text" name="billing3" class="removeText" size="4">
							<cfelseif GetCPPSETUPQUERY.IDENTIFYGROUPID_INT EQ -1>
								<label id="accLabel">Email Address*</label><br>
								<cfinput type="text" name="emailAddress" class="removeText emailText">
							<cfelse>
								<label id="accLabel"><cfoutput>#GetIdentifyGroupQuery.groupName_vch#</cfoutput>*</label><br>
								<cfset iIdentify = 0>
								<cfloop query="GetIdentifyQuery">
									<cfif iIdentify GT 0 AND GetIdentifyGroupQuery.hyphen_ti EQ 1>
										 - 
									</cfif>
									<cfinput type="text" size="15" maxlength="#GetIdentifyQuery.size_int#" name="customFieldValue_#GetIdentifyQuery.identifyId_int#" class="removeText">
									<cfinput type="hidden" name="customFieldId" value="#GetIdentifyQuery.identifyId_int#">
									<cfset iIdentify ++>
								</cfloop>
							</cfif>
							
						</div>
					</div>
					<cfset i++>
				</cfif> --->
				
				<!-----Identify does not make sense in cpp authentication ----->
				<cfif index eq 3>
					<h2 class="cppStepTitle contactTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Contact Method)</span></h2>
					<div class="cppPortalDesc contactDesc">
						<p>Please check the contact method you prefer to be contacted by for the above services.</p>
					</div>
					<div class="cppPortalContent">
						<cfif GetCPPSETUPQUERY.VOICEMETHOD_TI EQ 1>
							<cfset phone = data.PHONE>
							<cfset voiceText1 = phone.voiceText1>
							<cfset voiceText2 = phone.voiceText2>
							<cfset voiceText3 = phone.voiceText3>
							<cfset voiceCheck = "">
							<cfif voiceText1 NEQ "">
								<cfset voiceCheck = 'checked="true"'>
							</cfif>
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="voiceCheck" value="1" class="removeCheck" <cfoutput>#voiceCheck#</cfoutput> >
								<label class="label VoiceNumberText contactLabel" >Voice Number:</label>
								<input type="text" name="voiceText1" class="removeText" size="4" value="<cfoutput>#voiceText1#</cfoutput>">
								- <input type="text" name="voiceText2" class="removeText" size="4" value="<cfoutput>#voiceText2#</cfoutput>">
								- <input type="text" name="voiceText3" class="removeText" size="4" value="<cfoutput>#voiceText3#</cfoutput>" >
							</div>
						</cfif>
						<cfif GetCPPSETUPQUERY.SMSMETHOD_TI EQ 1>
							<cfset sms = data.SMS>
							<cfset smsText1 = sms.smsText1>
							<cfset smsText2 = sms.smsText2>
							<cfset smsText3 = sms.smsText3>
							<cfset smsCheck = "">
							<cfif smsText1 NEQ "">
								<cfset smsCheck = 'checked="true"'>
							</cfif>
							
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="smsCheck" value="1" class="removeCheck" <cfoutput>#smsCheck#</cfoutput>>
								<label class="label SMSNumberText contactLabel" >SMS Number:</label>
								<input type="text" name="smsText1" class="removeText" size="4" value="<cfoutput>#smsText1#</cfoutput>">
								- <input type="text" name="smsText2" name="smsText2" class="removeText" size="4" value="<cfoutput>#smsText2#</cfoutput>">
								- <input type="text" name="smsText3" class="removeText" size="4" value="<cfoutput>#smsText3#</cfoutput>" >
							</div>
						</cfif>
						<cfif GetCPPSETUPQUERY.EMAILMETHOD_TI EQ 1>
							<cfset email = data.EMAIL>
							<cfset emailCheck = "">
							<cfif email NEQ "">
								<cfset emailCheck = 'checked="true"'>
							</cfif>
							
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="emailCheck" value="1" class="removeCheck"  <cfoutput>#emailCheck#</cfoutput> >
								<label class="contactLabel" >Email Address:</label>
								<input type="text" name="emailText" class="removeText emailText" value="<cfoutput>#email#</cfoutput>">
							</div>
						</cfif>
					</div>
					<cfset i++>
				</cfif>
				
				<cfif index eq 4 AND GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 1 >
					<h2 class="cppStepTitle langTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Desired Language)</span></h2>
					<div class="cppPortalDesc langDesc" >
						Please select the desired communication language you want your service announcements to be delivered.
					</div>
					<div class="cppPortalContent">
						<div class="cppPortalContentRow">
							<label class="langLabel" >Select language:</label><br/>
							<select name="language" class="langContent">
								<option value="1">English</option>
							</select>
							
						</div>
					</div>
					<cfset i++>
				</cfif>
			</div>
			
		</cfloop>
		<cfinput type="hidden" name="totalStep" id="totalStep" value="#listlen(GetCPPSETUPQUERY.StepSetup_vch)#">
		<cfinput type="hidden" name="inpCPPUUID" value="#inpCPPUUID#">
		<div class = "cppPortalSubmit">
			<input type = "button" value="Back" id ="cppBack" onclick="return false;" style="display:none" >
			<cfif GetCPPSETUPQUERY.Type_ti EQ 1  >
				<input type = "submit" value="Submit" id ="cppSubmit" onclick="return false;" >
			<cfelse>
				<input type = "submit" value="Next" id ="cppNext" onclick="nextStep(); return false;" >
			</cfif>
		</div>
	</div>
	
</cfform>
<cfoutput>
	<style>
		@import url('#rootUrl#/#publicPath#/css/default.css');
	</style>
</cfoutput>
<cfinclude template="launchJs.cfm">

<cfoutput>#portalRight#</cfoutput>