<cfinclude template="../public/constants.cfm" >
<cfparam name="inpCPPUUID" default="" >
<cfparam name="inpCPPAID" default="" >
<cfparam name="Preview" default="0" >
<cfparam name="STEP" default="1" >

<cfset inpCPPUUID = trim(inpCPPUUID)>
<cfset portalLeft = ''>
<cfset portalRight = ''>
<cfset maxPhone = 0>
<cfset maxSMS = 0>
<cfset maxEmail = 0>
<cftry>
	<!---get step info--->
	<cfinvoke 
		component="#cfcDotPath#.cpp" 
		method="GetStepInfo" 
		returnvariable="retGetStepInfo">
		<cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
	</cfinvoke>
	<cfset StepInfoDATA = retGetStepInfo.DATA>
	<cfset TitleStep2 = "">
	<cfset DescriptionStep2 = "">
	<cfset TitleStep3 = "">
	<cfset DescriptionStep3 = "">
	<cfset TitleStep4 = "">
	<cfset DescriptionStep4 = "">
	
	<cfif Arraylen(StepInfoDATA) GT 0>
		<cfloop array="#StepInfoDATA#" index="StepData">		
			<cfif StepData.StepId EQ 2>
				<cfif StepData.Title EQ "">
					<cfset TitleStep2 = TITLE2>
				<cfelse>
					<cfset TitleStep2 = StepData.Title>	
				</cfif>
				<cfif StepData.Description EQ "">				
					<cfset DescriptionStep2 = DESCIRPTION2>
				<cfelse>
					<cfset DescriptionStep2 = StepData.Description>	
				</cfif>
			</cfif>
			<cfif StepData.StepId EQ 3>
				<cfif StepData.Title EQ "">
					<cfset TitleStep3 = TITLE3>
				<cfelse>
					<cfset TitleStep3 = StepData.Title>	
				</cfif>
				<cfif StepData.Description EQ "">				
					<cfset DescriptionStep3 = DESCIRPTION3>
				<cfelse>
					<cfset DescriptionStep3 = StepData.Description>	
				</cfif>
			</cfif>
			<cfif StepData.StepId EQ 4>
				<cfif StepData.Title EQ "">
					<cfset TitleStep4 = TITLE4>
				<cfelse>
					<cfset TitleStep4 = StepData.Title>	
				</cfif>
				<cfif StepData.Description EQ "">				
					<cfset DescriptionStep4 = DESCIRPTION4>
				<cfelse>
					<cfset DescriptionStep4 = StepData.Description>	
				</cfif>
			</cfif>
		</cfloop>
	<cfelse>
		<cfset TitleStep2 = TITLE2>
		<cfset DescriptionStep2 = DESCIRPTION2>
		<cfset TitleStep3 = TITLE3>
		<cfset DescriptionStep3 = DESCIRPTION3>
		<cfset TitleStep4 = TITLE4>
		<cfset DescriptionStep4 = DESCIRPTION4>
	</cfif>	
	<!---get CPP setup--->
	<cfinvoke 
		component="#cfcDotPath#.cpp" 
		method="GetCPPSETUPQUERY" 
		returnvariable="GetCPPSETUPQUERY">
		<cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
	</cfinvoke>
	
	<cfif GetCPPSETUPQUERY.Active_int NEQ 1 OR GetCPPSETUPQUERY.IFrameActive_int NEQ 1>
		This Customer Preference Portal is offline!
		<cfexit>
	</cfif>
	<cfset CppSetupLanguage = GetCPPSETUPQUERY.LANGDATA>
	<!---start set width and height for CPP following CPP setup--->
	<cfset width = 0>
	<cfset height = 0>
	<cfset emailWidth = '250'>
	<cfset mleft = '15'>
	<cfset mright = '15'>
	<cfset submitButtonContainer = 'submitButtonContainer'>
	<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
		<cfset width = '360px'>
		<cfset height = 'auto'>
		<cfset emailWidth = '185'>
		<cfset mleft = '15'>
		<cfset submitButtonContainer = ''>
	<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_MEDIUM>
		<cfset width = '800px'>
		<cfset height = 'auto'>
	<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_LARGE>
		<cfset width = '800px'>
		<cfset height = 'auto'>
	<cfelseif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_CUSTOM>
		<cfset width = '#GetCPPSETUPQUERY.width_int#'>
		<cfset height = '#GetCPPSETUPQUERY.height_int#'>
	</cfif>
	

	<!---end set width and height for CPP following CPP setup--->
		
	<!---start built style for CPP following CPP setup--->
	<cfoutput>
		<style>
		
			##main{
				font-size:#GetCPPSETUPQUERY.FONTSIZE_INT##GetCPPSETUPQUERY.fontSizeUnit_vch#;	
				<cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
					font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
				</cfif>
			}
			
			.portal-add-txt, .inner-txt-hd, .inner-txt-subhd, .portal-add-txt1, .portal-add-txt2{
				<cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
					font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
				</cfif>
				font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
				font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				color: #GetCPPSETUPQUERY.FONTCOLOR_VCH#;
				font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
				font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
			}
			
			.cppButton
			{
				background: ##2484C6;
				background: -moz-linear-gradient(center top , ##2484C6, ##2484C6) repeat scroll 0 0 rgba(0, 0, 0, 0);
				background: -webkit-gradient(linear, left top, left bottom, from(##2484C6), to(##2484C6));
				border: 1px solid ##0076A3;
			    color: ##FFF;
			    float: right;
			    padding: 5px;
				border-radius: 1px 1px 1px 1px;
			    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
			    cursor: pointer;
			    display: inline-block;
			    <cfif GetCPPSETUPQUERY.FONTFAMILY_VCH NEQ "None">
					font-family: #GetCPPSETUPQUERY.FONTFAMILY_VCH#;
				</cfif>
				font-style: #GetCPPSETUPQUERY.FONTSTYLE_VCH#;
				font-weight: #GetCPPSETUPQUERY.FONTWEIGHT_VCH#;
				font-variant: #GetCPPSETUPQUERY.FONTVARIANT_VCH#;
				font-size: #GetCPPSETUPQUERY.FONTSIZE_INT##GetCPPSETUPQUERY.fontSizeUnit_vch#;
			    margin: 0 2px;
			    outline: medium none;
			    text-align: center;
			    text-decoration: none;
			    text-transform: uppercase;
			    vertical-align: baseline;
				margin-bottom:20px;
				border-radius:5px;
			}
			.cppButton:hover {
				background: ##007ead;
				background: -webkit-gradient(linear, left top, left bottom, from(##0095cc), to(##00678e));
				background: -moz-linear-gradient(top,  ##0095cc,  ##00678e);
				
			}
			.cppButton:active {
				color: ##fff;
				background: -webkit-gradient(linear, left top, left bottom, from(##0078a5), to(##00adee));
				background: -moz-linear-gradient(top,  ##0078a5,  ##00adee);
				
			}
			.portaladd-input-box{
				width: #emailWidth#px
			}
			
			
			##innerCPP{
				<!---<cfif width LT 450>
					width: 430px;				
				<cfelse>
					width: #width-50#px
				</cfif>--->
				width:100%;
				min-height:#GetCPPSETUPQUERY.HEIGHT_INT#px;
			}
			
			##formcontent{
				<!---<cfif width LT 450>
					width: 450px;				
				<cfelse>
					width:#width-50#px;			
				</cfif>
				<cfif height LT 400>
					height:400px;
				<cfelse>
					height:#height-35#px;
				</cfif>
				--->
				<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
					width:343px;
				<cfelse>
				<!---	margin-right:57px;	--->
				</cfif>
				<!---margin-top:20px;--->
				height:#height#px;
			}
			
			.formContainer {
				padding-left:#mleft#px;
				padding-right:#mright#px;
				overflow-y:auto;
				margin-left:auto;
				margin-right:auto;
				<!---position:relative;--->
				border: #GetCPPSETUPQUERY.BORDERWIDTH_INT#px solid #GetCPPSETUPQUERY.BORDERCOLOR_VCH#;
				border-radius: #GetCPPSETUPQUERY.BORDERRADIUS_INT#px;
				background-color: #GetCPPSETUPQUERY.BACKGROUNDCOLOR_VCH#;
				font-size: #GetCPPSETUPQUERY.FONTSIZE_INT#px;
				<cfif GetCPPSETUPQUERY.BACKGROUNDIMAGE_VCH NEQ "">
					background-image: url('#rootUrl#/#sessionPath#/act_previewImage.cfm?serverfileName=#GetCPPSETUPQUERY.BACKGROUNDIMAGE_VCH#&userId=#GetCPPSETUPQUERY.userId_int# ');
					background-repeat : <cfif LCase(GetCPPSETUPQUERY.imageRepeat_vch) EQ 'yes' >repeat<cfelseif LCase(GetCPPSETUPQUERY.imageRepeat_vch) EQ 'no'>no-repeat</cfif>
				</cfif>
			}
		.portal-add-inp {
		    float: left;
		}
		.addContact{
			background: url("../public/images/add.gif") no-repeat;
		    float: left;
		    margin-bottom: 2px;
			
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				margin-left: 5px;
			<cfelse>
				margin-left: 12px;
				margin-top: 8px;
			</cfif>
		    
		    width: 16px;
			height:18px;
			cursor:pointer;
		}
		.delContact{
			background: url("../public/images/del.gif") no-repeat;
		    float: left;
		    margin-bottom: 2px;
			margin-top: 8px;
		    margin-left: 12px;
		    width: 16px;
			height:18px;
			cursor:pointer;
		}
		.delHide {
			display:none;
		}
		.delShow{
			display:block;
		}
		##loading
		{
		    position: fixed;
		    top: 0;
		    left: 0;
		    width: 100%;
		    height: 100%;
		    opacity: 0.7;
		    background-color: ##000;
		    z-index: 1000;
			display:none;		
		}
	    ##loading div
	    {
	        position: absolute;
	        top: 50%;
	        left: 50%;
	        width: 5em;
	        height: 2em;
	        margin: -1em 0 0 -2.5em;
	        color: ##fff;
	        font-weight: bold;
	    }
		.custom-checkbox label, .custom-radio label {
	    	font-weight: normal !important;
			text-transform: none !important;
			font-family: Calibri,Regular,sans-serif;
			font-size:1em !important;
			padding: 2px 10px 0px 35px !important;
		}
		
		.box label{
	    	font-weight: normal !important;
			text-transform: none !important;
			font-family: Verdana,Regular,sans-serif;
			font-size:1em !important;
			padding: 0px 0px 0 25px !important;
			line-height:21px;
		}
		
		.box{
			float:left;
			border: 1px solid ##CCCCCC;
			border-radius:5px;
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				width:280px;
			<cfelse>
				width:341px;
			</cfif>
			height:auto;	
			min-height:29px;
			background-color: ##fff;
		}
		
		
		.CDF_BOX{
			float:left;
			border: none;
			width:100%;			
			height:auto;	
			min-height:29px;			
		}
		
		.CDF_DESC{
			float:none;
			text-align:left;
			line-height: auto;
			margin-left: 6px;
			width:100%;
			border-radius:5px 0 0 5px;
			height:auto;
			min-height:29px;
			padding-bottom: .7em;
		}
		
		.CDF_SELECT{
			float:none;
			width:100%;
			min-width:300px;
			height:auto;
			min-height:29px;
			
		}
		
		.left_box{
			float:left;
			text-align:left;
			line-height: auto;
			margin-left: 6px;
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				width:274px;
			<cfelse>
				width:130px;
			</cfif>
			border-radius:5px 0 0 5px;
			height:auto;
			min-height:29px;
		}
		.right_box{
			float:left;
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				width:280px;
			<cfelse>
				width:204px;
				border-left: 1px solid ##CCCCCC;
			</cfif>
			height:auto;
			min-height:29px;
			
		}
		.input_box{
			float:left;
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				width:268px;
				border-top: 1px solid ##CCCCCC;
				border-radius:0 0 5px 5px;
			<cfelse>
				width:192px;
				border-radius:5px;
			</cfif>
			height:30px;
			padding-left:12px;	
			font-size:1em;	
		}
		.input_box_last{
			float:left;
			<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
				width:268px;
				border-radius:0 0 5px 5px;
			<cfelse>
				width:192px;
				border-radius:0 0 5px 0;
			</cfif>
			height:27px;
			border-top: 1px solid ##CCCCCC;
			padding-left:12px;
		}
		.mtop30{
			margin-top:30px;
		}
		.mtop11{
			margin-top:11px;
		}
		.stepCPP{
		    <!---float: left;--->
		    font-family: Calibri,Regular,sans-serif;
		    text-align: left;
			font-size:1.3em;
			text-transform:none;
			float:left;
			width: 100%;
		}	
		<!---.step{
			color:##006699;
			font-weight:bold;
		}--->
		<!---.stepDes{
			color:##666666;
		}--->
		.stepNote{
			color:#GetCPPSETUPQUERY.fontColor_vch#;
			
			<cfif GetCPPSETUPQUERY.fontFamily_vch NEQ "None">
				font-family: #GetCPPSETUPQUERY.fontFamily_vch#;
			</cfif>
			
			font-size:#GetCPPSETUPQUERY.FONTSIZE_INT##GetCPPSETUPQUERY.fontSizeUnit_vch#;
			text-align: left;
			float:left;
			margin-top:13px;
		}
		.checkevent{
			color: ##006699;
			text-decoration:none;		
			font-size: .8em;	
		}
		.checkevent:hover{
			color: ##2496D6;
		}
		##CppContact{
			font-family: Calibri,Regular,sans-serif;
			font-size:1.2em;
			text-align: left;
			float:left;
			margin-top:26px;
		}
		.mleft10{
			margin-left:10px;
		}
		.mleft15{
			margin-left:15px;
		}
		.mtop38{
			margin-top:38px;
		}
		.mtop43{
			margin-top:43px;
		}
		.mtop47{
			margin-top:47px;
		}
		.mtop15{
			margin-top:15px;
		}
		.mtop10{
			margin-top:10px;
		}
		.mtop20{
			margin-top:20px;
		}
		.ui-selectmenu{
			border: 1px solid ##CCC;
		}
		
		
		.custom-checkbox{
			padding-top: 7px;
			min-height: 25px;
		}
		.domain{
			font-size: 1em;
	    	padding-top: 19px;
			color: ##666666;
		}
		##dialog_thankyou{
			width:400px;
			height:160px;
			 box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
		}
		##dialog_thankyou .title{
			color:##006699;
			font-weight:bold;
			font-size:2em;
			font-family:Calibri;
			margin-top:30px;
			text-align:center;
		}
		##dialog_thankyou .desc{
			color:##666666;
			font-weight:normal;
			font-size:1.2em;
			font-family:Calibri;
			margin-top:33px;
			text-align:center;
		}
		##dialog_thankyou .thankyou_btn{
			margin-top:23px;
			height:32px;
		} 
		.closeThank{
			float:right;
			margin-right:8px;
			background: ##46a6dd;
		    border: 1px solid ##0076A3;
		    border-radius: 1px;
		    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
		    color: ##FFFFFF;
		    cursor: pointer;
		    display: inline-block;
		    float: right;
		    font-size: 1em;
		    font-style: normal;
		    font-variant: normal;
		    font-weight: normal;
		    outline: medium none;
		    padding:5px 11px;
		    text-align: center;
		    text-decoration: none;
		    text-transform: none;
		    vertical-align: baseline;
			border-radius:5px;
			font-family:Verdana;
		}
		.closeThank:hover{
			background: ##0076A3;
		}
		.logoutbtn{
			float:right;
			margin-right: 12px;
		    border: 1px solid ##ccc;
		    border-radius: 1px;
		    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
		    color: ##666;
		    cursor: pointer;
		    display: inline-block;
		    float: right;
		    font-size: 1em;
		    font-style: normal;
		    font-variant: normal;
		    font-weight: normal;
		    outline: medium none;
		    padding:5px 11px;
		    text-align: center;
		    text-decoration: none;
		    text-transform: none;
		    vertical-align: baseline;
			border-radius:5px;
			font-family:Verdana;
		}
		.logoutbtn:hover{
			background: ##ccc;
			color: ##fff;
		}
		.scrollable{
			float: left;
    		width: 250px;
			height:29px;
		}
		.submitButtonContainer, .smalltheme {		    
			float:right;
			margin-top:15px; 
			margin-bottom:15px;
		}
		.step ol{
			margin-left:40px;
		}	
		.stepNote ol{
			margin-left:40px;
		}
		.field-value .ui-state-default{
            background: none;
        }
        .field-value .ui-selectmenu{
            /*border: none;*/
        }
        .field-value .ui-selectmenu span{
            color: ##666;
            display: block;
            float: left;
            font-family: "Verdana",geneva,sans-serif;
            font-size: 1em;
            font-weight: normal;
            margin-bottom: 0;
            padding-left: 12px;
        }
        .field-value .ui-selectmenu-icon{
            right: -8px;
        }
        <cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
            .ui-selectmenu{
                width: 100% !important;
            }
        </cfif>
        .cppPortalRow .custom-checkbox ol,
        .cppPortalRow .custom-checkbox ul{
            padding-left: 15px;
        }
    <!---generate style from ebm site --->
	<!---	#GetCPPSETUPQUERY.CUSTOMCSS_TXT#--->
	</style>
	</cfoutput>
	<!---end built style for CPP following CPP setup--->
	<cfset submitClass = LCase(GetCPPSETUPQUERY.templateName_vch)>
	<cfif submitClass EQ 'plan' OR submitClass EQ ''>
		<cfset submitClass = 'blue'>
	</cfif>
	
	<cfset templateId = trim(GetCPPSETUPQUERY.template_int)>
	
	<!---Get Preference--->
	<cfinvoke 
		component="#cfcDotPath#.cpp" 
		method="GetPreferenceQuery" 
		returnvariable="GetPreferenceQuery">
		<cfinvokeargument name="CPP_UUID" value="#GetCPPSETUPQUERY.CPP_UUID_vch#">
		<cfinvokeargument name="SetCustomValue" value="1">
	</cfinvoke>
    
 <!---   <cfdump var="#GetPreferenceQuery#">--->
    
	<cfif GetCPPSETUPQUERY.IdentifyGroupId_int GT 0>
		<cfinvoke 
			component="#cfcDotPath#.cpp" 
			method="GetIdentifyGroupQuery" 
			returnvariable="GetIdentifyGroupQuery">
			<cfinvokeargument name="CPP_UUID" value="#GetCPPSETUPQUERY.CPP_UUID_vch#">
			<cfinvokeargument name="SetCustomValue" value="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfinvoke>	
		
		<cfinvoke 
			component="#cfcDotPath#.cpp" 
			method="GetIdentifyQuery" 
			returnvariable="GetIdentifyQuery">
			<cfinvokeargument name="SetCustomValue" value="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfinvoke>
	</cfif>
	
	<cfset leftStrLength = FINdNoCase( '{%portal%}', GetCPPSETUPQUERY.customhtml_txt)>
	
	<cfif leftStrLength GT 0>
		<cfset portalLeft = left(GetCPPSETUPQUERY.customhtml_txt, leftStrLength)>
		<cfset portalRight = replaceNoCase(GetCPPSETUPQUERY.customhtml_txt, portalLeft , '') >
		<cfset portalRight = replaceNoCase(portalRight, '{%portal%}' , '', 'all') >
	</cfif>
	
	<cfcatch TYPE="any">
		<cfdump var="#cfcatch#">
		<cfoutput>#cfcatch.MESSAGE#</cfoutput>
		<cfexit>
    </cfcatch>
	        
</cftry>
<cfinvoke 
	component="#cfcDotPath#.cpp" 
	method="GetContactPreference" 
	returnvariable="retGetContactPreferenceValue">
	<cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
</cfinvoke>
<!---<cfdump var="#retGetContactPreferenceValue#" />--->
<cfinvoke
        component="#cfcDotPath#.cpp"
        method="GetCdfVariable"
        returnvariable="reGetCdfVariable">
        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
</cfinvoke>

<cfset data = retGetContactPreferenceValue.DATA>
<cfset dataCdfVariable = reGetCdfVariable.DATA>
<!---<cfdump var="#Session.CPPACOUNTID#"/>--->
<cfif GetCPPSETUPQUERY.VOICEMETHOD_TI EQ 1>
	<cfif ArrayLen(data.PHONE) GT 0>
		<cfset maxPhone = ArrayLen(data.PHONE)>
	<cfelse>
		<cfset maxPhone = 1>
	</cfif>
</cfif>
<cfif GetCPPSETUPQUERY.SMSMETHOD_TI EQ 1>	
	<cfif ArrayLen(data.SMS) GT 0>
		<cfset maxSMS = ArrayLen(data.SMS)>
	<cfelse>
		<cfset maxSMS = 1>
	</cfif>
</cfif>	
<cfif GetCPPSETUPQUERY.EMAILMETHOD_TI EQ 1>
	<cfif ArrayLen(data.EMAIL) GT 0>
		<cfset maxEmail = ArrayLen(data.EMAIL)>
	<cfelse>
		<cfset maxEmail = 1>
	</cfif>	
</cfif>


<!--- Calaulate --->



<cfset i =1>
<cfset indexWithOutHtml = 1><!---this var is used to define stepcontent id which is not custom html --->
<div id="main" name="main" align="">
	<div class="formContainer previewStyle" style="
		<cfif width LT 450 OR height LT 375>
			overflow-y: auto;
			overflow-x: hidden;
		</cfif>
		width: <cfoutput>#width#</cfoutput>; height: <cfoutput>#GetCPPSETUPQUERY.HEIGHT_INT + 100#px</cfoutput>; 
		">
		<div id="formcontent" align="">
			<cfif GetCPPSETUPQUERY.displayType_ti EQ 1 and LEN(trim(GetCPPSETUPQUERY.customHtml_txt)) GT 0 AND trim(GetCPPSETUPQUERY.customHtml_txt) NEQ '<p>You may include the portal any where on the page by typing {%portal%}. You may include it only once.</p>'>
				<cfoutput>#portalLeft#</cfoutput>
				<div style="clear:both"></div>
			</cfif>
			<!---<div id="innerCPP" style="
			<cfif width GT 450 OR height GT 400>
				overflow: auto;
				height: <cfoutput>#height - 48#px</cfoutput>;
			</cfif>
			">--->

			<div id="innerCPP" >
				<form method="post" name="cppLaunchForm" id="cppLaunchForm">
					<input type="hidden" name="inpStep" id="inpStep" value= "">
					<input type="hidden" name="preview" id="preview" value= "0">
					<input type="hidden" name="includeLanguage" id="includeLanguage" value= "<cfoutput>#GetCPPSETUPQUERY.INCLUDELANGUAGE_TI#</cfoutput>">
					<input type="hidden" name="accepConditionForm" id="accepConditionForm">
					
					<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
						<cfif listfind(GetCPPSETUPQUERY.StepSetup_vch,5) GT 0>
							<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,5))>
						</cfif>
					</cfif>
					
					<!--- <img src ="<cfoutput>#rootUrl#/#publicPath#/images/logo-ebm.png</cfoutput>"> --->
					
					<cfif Session.CONSUMEREMAIL NEQ 0>
			  			<a class="logoutbtn" href="<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/act_logout.cfm?inpCPPUUID=<cfoutput>#url.inpCPPUUID#</cfoutput>" >Logout</a>
				  	</cfif>
				  	<div style="clear:both;"></div>
					<cfif listfind(GetCPPSETUPQUERY.StepSetup_vch,1) GT 0>
						<cfset newSetup = ListDeleteAt(GetCPPSETUPQUERY.StepSetup_vch,listfind(GetCPPSETUPQUERY.StepSetup_vch,1))>
					<cfelse>
						<cfset newSetup = GetCPPSETUPQUERY.StepSetup_vch>
					</cfif>
					
					<cfloop list="#newSetup#" index="index">									
						<input name="<cfoutput>stepVal#index#</cfoutput>" type="hidden" id="<cfoutput>stepVal#i#</cfoutput>" value ="<cfoutput>#index#</cfoutput>" >
						<div class = "cppPortalRow" id="stepContent<cfoutput>#i#</cfoutput>" <cfif GetCPPSETUPQUERY.Type_ti EQ 2 AND i NEQ STEP> style="display:none;" </cfif>>
							<cfif index eq 2>
								<div style="clear:both;"></div>
								<div class="stepCPP mtop10"><span class="step" ><!---Step <cfoutput>#indexWithOutHtml#</cfoutput>: ---><cfoutput>#HTMLStringFormat(TitleStep2)#</cfoutput></span></div>
								<div style="clear:both;"></div>
								<div class="stepNote">
									<cfoutput>#HTMLStringFormat(DescriptionStep2)#</cfoutput>
								</div>
                                <div style="clear:both;"></div>
								<div id="CppContact">
									<div class="mleft15">
										<cfset jj = 0>										
										<cfloop array="#GetPreferenceQuery.DATA#" index="PreferenceItem">
											<cfset checked = false>
											<cfset jj++>
											<!--- check value selected --->
												
											<cfloop array="#data.PREFERENCE#" index="preIndex">
												<cfif #PreferenceItem.PREFERENCEID_INT# EQ preIndex>
													<cfset checked = "checked">
												</cfif>
											</cfloop>
                                            
                                            
                                            <cfif Session.CPPACOUNTID NEQ "0" AND ArrayLen(GetPreferenceQuery.DATA) GT 1>
                                            	<cfif PreferenceItem.ONCHECKLIST EQ "Yes"> 
    	                                            <cfset checked = "checked">
                                            	<cfelse>
	                                                <cfset checked = "unchecked">
                                            	</cfif>
                                            </cfif>
											
											<div>
												<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
													<input type="checkbox" id="preference<cfoutput>#jj#</cfoutput>" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>" <cfoutput>#checked#</cfoutput> class="removeCheck">
												<cfelse>
													<input type="radio" id="preference<cfoutput>#jj#</cfoutput>" name="preferenceCheck" value="<cfoutput>#PreferenceItem.preferenceId_int#</cfoutput>" <cfoutput>#checked#</cfoutput> class="removeCheck">
												</cfif>
												<label for="preference<cfoutput>#jj#</cfoutput>"><cfoutput>#PreferenceItem.Desc_vch#</cfoutput></label>
												<!--- IE bug bix - remove this <br/>--->
											</div>
											
										</cfloop>
										<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
											<cfif #Arraylen(GetPreferenceQuery.DATA)# GT 0>
												<div>
													<a class="checkevent" href="javascript:CheckEvent()" id="CheckEvent">Uncheck all</a>
												</div>
											</cfif>
										</cfif>
									</div>
									
								</div>
								<cfset indexWithOutHtml++>
							</cfif>
							<div style="clear:both;"></div>
							<!-----Identify does not make sense in cpp authentication ----->
							<cfif index eq 3>
								<div style="clear:both;"></div>
								<div class="stepCPP mtop15"><span class="step"><!---Step <cfoutput>#indexWithOutHtml#</cfoutput>: ---><cfoutput>#HTMLStringFormat(TitleStep3)#</cfoutput></span></div>
								<div style="clear:both;"></div>
								<div class="stepNote">
									<cfoutput>#HTMLStringFormat(DescriptionStep3)#</cfoutput>
								</div>
								<div style ="clear:both"></div>
								<cfif GetCPPSETUPQUERY.VOICEMETHOD_TI EQ 1>
									<cfset phoneArray = data.PHONE>									
									<cfset voiceCheck = "">
									<cfif ArrayLen(phoneArray) GT 0>
										<cfset voiceCheck = 'checked="true"'>
									</cfif>
									<div class="inputcontent mtop20">
										<div class="box">
											<div class="left_box">
												<input type = "checkbox" id="voiceCheck" name="voiceCheck" value="1" class="removeCheck" <cfoutput>#voiceCheck#</cfoutput> >
												<label for="voiceCheck">Voice Number</label>
											</div>
											<div class="right_box" id="inpVoiceNumber">
												<cfset voicenumberlist = "">
												
												<cfset del_phone_class = "delHide">
												<cfif ArrayLen(phoneArray) GT 1>
													<cfset del_phone_class = "delShow">
												</cfif>	
                                                
                                                <!--- Always show - new ui for opt out --->
                                                <cfset del_phone_class = "delShow">
                                                
												<cfif ArrayLen(phoneArray) GT 0>
													<cfset j = 1>
													<cfset inputClass="input_box">
													<cfloop array="#phoneArray#" index="phone">
														<cfif j GT 1>
															<cfset inputClass="input_box_last">
														</cfif>													
														<div id="VoiceNumnerPanel<cfoutput>#j#</cfoutput>">
															<input type="text" name="voiceText_<cfoutput>#j#</cfoutput>" id="voiceText_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#phone#</cfoutput>" >
														</div>
														<cfset voicenumberlist = voicenumberlist & j &",">
														<cfset j++>
													</cfloop>
												<cfelse>
													<div id="VoiceNumnerPanel1">														
														<input type="text" name="voiceText_1" id="voiceText_1" class="input_box maskPhone" size="4" value="" >
													</div>
													<cfset voicenumberlist = "1,">
												</cfif>
											</div>
										</div>
										<div id="actionVoice" style="width:28px; float:left;<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL AND ArrayLen(phoneArray) GT 0> margin-top:29px;</cfif>">
											<cfif ArrayLen(phoneArray) GT 0>
												<cfset j = 1>
												<cfloop array="#phoneArray#" index="phone">														
													<input type="button" name="DelVoiceNumber_<cfoutput>#j#</cfoutput>" id="DelVoiceNumber_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_phone_class#</cfoutput>" onclick="fnDelVoiceNumber('VoiceNumnerPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
													<cfset j++>
												</cfloop>
											<cfelse>
												<input type="button" name="DelVoiceNumber_1" id="DelVoiceNumber_1" class="delContact delHide" onclick="fnDelVoiceNumber('VoiceNumnerPanel1','1,');">	
											</cfif>
										</div>
										<div style="float:left;">
											<cfif ArrayLen(phoneArray) GT 0>
												<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
													<input style="margin-top: <cfoutput>#((ArrayLen(phoneArray)-1)* 28 + 37)#px</cfoutput>" type="button" onclick="fnAddVoiceNumber()" id="AddVoiceNumber" name="AddVoiceNumber" class="addContact">
												<cfelse>
													<input style="margin-top: <cfoutput>#((ArrayLen(phoneArray)-1)* 28 + 8)#px</cfoutput>" type="button" onclick="fnAddVoiceNumber()" id="AddVoiceNumber" name="AddVoiceNumber" class="addContact">
												</cfif>
											<cfelse>
												<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
													<input type="button" style="margin-top: 37px" onclick="fnAddVoiceNumber()" id="AddVoiceNumber" name="AddVoiceNumber" class="addContact">
												<cfelse>
													<input type="button" onclick="fnAddVoiceNumber()" id="AddVoiceNumber" name="AddVoiceNumber" class="addContact">
												</cfif>
											</cfif>	
										</div>	
										<input type="hidden" id="VoiceNumberList" name="VoiceNumberList" value="<cfoutput>#voicenumberlist#</cfoutput>" data-init="<cfoutput>#voicenumberlist#</cfoutput>">
									</div>
									<div style = "clear:both"></div>
								</cfif>
								<cfif GetCPPSETUPQUERY.SMSMETHOD_TI EQ 1>
									<cfset smsArray = data.SMS>
									<cfset smsCheck = "">
									<cfif ArrayLen(smsArray) GT 0>
										<cfset smsCheck = 'checked="true"'>
									</cfif>
									<div class="inputcontent mtop10">
										<div class="box">
											<div class="left_box">
												<input type = "checkbox" id="smsCheck" name="smsCheck" value="1" class="removeCheck" <cfoutput>#smsCheck#</cfoutput> >
												<label for="smsCheck">SMS Number</label>
											</div>
											<div class="right_box" id="inpSMSNumber">
												<cfset smsnumberlist = "">
												<cfset del_sms_class = "delHide">
												<cfif ArrayLen(smsArray) GT 1>
													<cfset del_sms_class = "delShow">
												</cfif>	
                                                
                                                <!--- Always show - new ui for opt out --->
                                                <cfset del_sms_class = "delShow">
                                                
												<cfset inputClass = "input_box">
												<cfif ArrayLen(smsArray) GT 0>
													<cfset j = 1>													
													<cfloop array="#smsArray#" index="sms">
														<cfif j GT 1>
															<cfset inputClass = "input_box_last">
														</cfif>													
														<div id="SMSNumnerPanel<cfoutput>#j#</cfoutput>">
															<input type="text" name="smsText_<cfoutput>#j#</cfoutput>" id="smsText_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#sms#</cfoutput>" >
														</div>
														<cfset smsnumberlist = smsnumberlist & j &",">
														<cfset j++>
													</cfloop>
												<cfelse>
													<div id="SMSNumnerPanel1">														
														<input type="text" name="smsText_1" id="smsText_1" class="input_box input_mask mask_phone" size="4" value="" >
													</div>
													<cfset smsnumberlist = "1,">
												</cfif>
											</div>
										</div>
										<div id="actionSms" style="width:28px; float:left;<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL AND ArrayLen(smsArray) GT 0> margin-top:29px;	</cfif>	">
											<cfif ArrayLen(smsArray) GT 0>
												<cfset j = 1>
												<cfloop array="#smsArray#" index="sms">														
													<input type="button" name="DelSMSNumber_<cfoutput>#j#</cfoutput>" id="DelSMSNumber_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_sms_class#</cfoutput>" onclick="fnDelSMSNumber('SMSNumnerPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
													<cfset j++>
												</cfloop>
											<cfelse>
												<input type="button" name="DelSMSNumber_1" id="DelSMSNumber_1" class="delContact delHide" onclick="fnDelSMSNumber('SMSNumnerPanel1','1,');">	
											</cfif>
										</div>
										<div style="float:left;">
										<cfif ArrayLen(smsArray) GT 0>
											<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
												<input type="button" style="margin-top: <cfoutput>#((ArrayLen(smsArray)-1)* 28 + 37)#px</cfoutput>" onclick="fnAddSMSNumber()" id="AddSMSNumber" name="AddSMSNumber" class="addContact">
											<cfelse>
												<input type="button" style="margin-top: <cfoutput>#((ArrayLen(smsArray)-1)* 28 + 8)#px</cfoutput>" onclick="fnAddSMSNumber()" id="AddSMSNumber" name="AddSMSNumber" class="addContact">
											</cfif>
										<cfelse>
											<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
												<input type="button" style="margin-top: 37px;" onclick="fnAddSMSNumber()" id="AddSMSNumber" name="AddSMSNumber" class="addContact">
											<cfelse>
												<input type="button" onclick="fnAddSMSNumber()" id="AddSMSNumber" name="AddSMSNumber" class="addContact">
											</cfif>
										</cfif>
										</div>	
										<input type="hidden" id="SMSNumberList" name="SMSNumberList" value="<cfoutput>#smsnumberlist#</cfoutput>" data-init="<cfoutput>#smsnumberlist#</cfoutput>">
									</div>
									<div style = "clear:both"></div>
								</cfif>
								<cfif GetCPPSETUPQUERY.EMAILMETHOD_TI EQ 1>
									<cfset emailArray = data.EMAIL>
									<cfset emailCheck = "">
									<cfif ArrayLen(emailArray) GT 0>
										<cfset emailCheck = 'checked="true"'>
									</cfif>
									
									<div class="inputcontent mtop10">
										<div class="box">
											<div class="left_box">
												<input type = "checkbox" id="emailCheck" name="emailCheck" value="1" class="removeCheck" <cfoutput>#emailCheck#</cfoutput> >
												<label for="emailCheck">Email Address</label>
											</div>
											<div class="right_box" id="inpEmail">
											<cfset emailnumberlist = "">
											<cfset del_email_class = "delHide">
												<cfif ArrayLen(emailArray) GT 1>
													<cfset del_email_class = "delShow">
												</cfif>	
                                                
                                                <!--- Always show - new ui for opt out --->
                                                <cfset del_email_class = "delShow">                                                
                                                
												<cfset inputClass = "input_box">
												<cfif ArrayLen(emailArray) GT 0>
													<cfset j = 1>													
													<cfloop array="#emailArray#" index="email">
														<cfif j GT 1>
															<cfset inputClass = "input_box_last">
														</cfif>													
														<div id="EmailPanel<cfoutput>#j#</cfoutput>">
															<input type="text" name="email_<cfoutput>#j#</cfoutput>" id="email_<cfoutput>#j#</cfoutput>" class="<cfoutput>#inputClass#</cfoutput>" size="4" value="<cfoutput>#email#</cfoutput>" >
														</div>
														<cfset emailnumberlist = emailnumberlist & j &",">
														<cfset j++>
													</cfloop>
												<cfelse>
													<div id="EmailPanel1">														
														<input type="text" name="email_1" id="email_1" class="input_box" size="4" value="" >
													</div>
													<cfset emailnumberlist = "1,">
												</cfif>
											</div>
										</div>
										<div id="actionEmail" style="width:28px; float:left;<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL AND ArrayLen(emailArray) GT 0>margin-top:29px;</cfif>">
											<cfif ArrayLen(emailArray) GT 0>
												<cfset j = 1>
												<cfloop array="#emailArray#" index="email">														
													<input type="button" name="DelEmail_<cfoutput>#j#</cfoutput>" id="DelEmail_<cfoutput>#j#</cfoutput>" class="delContact <cfoutput>#del_email_class#</cfoutput>" onclick="fnDelEmail('EmailPanel<cfoutput>#j#</cfoutput>','<cfoutput>#j#</cfoutput>,');">												
													<cfset j++>
												</cfloop>
											<cfelse>
												<input type="button" name="DelEmail_1" id="DelEmail_1" class="delContact delHide" onclick="fnDelEmail('EmailPanel1','1,');">	
											</cfif>
										</div>
										<div style="float:left;">
											<cfif ArrayLen(emailArray) GT 0>
												<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
													<input type="button" style="margin-top: <cfoutput>#((ArrayLen(emailArray)-1)* 28 + 37)#px</cfoutput>" onclick="fnAddEmail()" id="AddEmail" name="AddEmail" class="addContact">
												<cfelse>
													<input type="button" style="margin-top: <cfoutput>#((ArrayLen(emailArray)-1)* 28 + 8)#px</cfoutput>" onclick="fnAddEmail()" id="AddEmail" name="AddEmail" class="addContact">
												</cfif>
											<cfelse>
												<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
													<input type="button" style="margin-top: 37px;" onclick="fnAddEmail()" id="AddEmail" name="AddEmail" class="addContact">
												<cfelse>
													<input type="button" onclick="fnAddEmail()" id="AddEmail" name="AddEmail" class="addContact">
												</cfif>
													
											</cfif>	
										</div>	
										<input type="hidden" id="EmailList" name="EmailList" value="<cfoutput>#emailnumberlist#</cfoutput>" data-init="<cfoutput>#emailnumberlist#</cfoutput>">
									</div>
								</cfif>
								<cfset indexWithOutHtml++>
								<div class="clear-both" style=""></div>
							</cfif>
							
							<cfif index eq 4 AND GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 1 >
								<div style="clear:both;"></div>
								<div class="stepCPP mtop15"><span class="step"><!---Step <cfoutput>#indexWithOutHtml#</cfoutput>: ---><cfoutput>#HTMLStringFormat(TitleStep4)#</cfoutput></span></div>
								<div style="clear:both;"></div>
								<div class="stepNote">
									<cfoutput>#HTMLStringFormat(DescriptionStep4)#</cfoutput>
								</div>
								<div class="domain"> 
									<div class="scrollable">
										<select name="language" id="language" class="select1" style="border:1px solid red;">
											<cfif Arraylen(CppSetupLanguage) GT 0>																					
												<cfloop array="#CppSetupLanguage#" index="language">
													<cfif data.LanguagePreference_vch NEQ "">
														<cfif language EQ data.LanguagePreference_vch>
															<option value="<cfoutput>#language#</cfoutput>" selected="selected"><cfoutput>#language#</cfoutput></option>
														<cfelse>
															<option value="<cfoutput>#language#</cfoutput>"><cfoutput>#language#</cfoutput></option>	
														</cfif>
													<cfelse>
														<option value="<cfoutput>#language#</cfoutput>"><cfoutput>#language#</cfoutput></option>	
													</cfif>	
												</cfloop>
											</cfif>	
										</select>
									</div>
								</div>
								<cfset indexWithOutHtml++>
							</cfif>
							
                            <!--- Custom HTML Section(s)--->
							<cfif Find("5.",index) GT 0>
								<cfset tmpArray = arrayNew(1)/>
								<cfset tmpArray = ListToArray(index,".")>
								<cfinvoke 
									component="#cfcDotPath#.cpp" 
									method="GetCustomHtml" 
									returnvariable="GetCustomHtml">
									<cfinvokeargument name="customHtmlId" value="#tmpArray[2]#">
								</cfinvoke>
								<cfif #GetCustomHtml.RXRESULTCODE# EQ 1>
									<!---<div class="stepCPP mtop38"><span class="step">Step <cfoutput>#i#</cfoutput>:</span><span class="stepDes"></span></div>--->
									<div style="clear:both;" class="mtop20"></div>
									<cfoutput>#replaceList(GetCustomHtml.HTML, "&lt;,&gt;,&amp;,&quot;,&##39;", '<,>,&,",''')#</cfoutput>									
								</cfif>
							</cfif>
                            
                            <!--- CDF Section(s) --->
                            <cfif Find("6.",index) GT 0>
                                <cfinvoke method="GetCdfCppByID" component="#cfcDotPath#.cpp" returnvariable="getCdfHtml">
                                        <cfinvokeargument name="inpCPPUUID" value="#url.inpCPPUUID#">
                                </cfinvoke>
                                <cfset nameStep = "step" & index>
                                <cfif IsDefined("getCdfHtml.FIELD") AND ArrayLen(getCdfHtml.FIELD)>
                                    <cfset desc = ''/>
                                    <cfloop array="#getCdfHtml.FIELD#" index="fieldItem">
                                        <cfif nameStep EQ fieldItem.IDSTEP>
                                            <cfset desc = HTMLStringFormat(fieldItem.DESCRIPTION) />
                                            <cfbreak/>
                                        </cfif>
                                    </cfloop>
                                    <cfloop array="#getCdfHtml.FIELD#" index="fieldItem">
                                        <div style="clear:both"></div>
                                        <cfif nameStep EQ fieldItem.IDSTEP>
                                            <div class="inputcontent mtop10">
                                                <div class="CDF_BOX">
                                                    <div class="CDF_DESC">
                                                        <div class="custom-checkbox">
                                                            <span class="title-field" rel="<cfoutput>#fieldItem.ID#</cfoutput>"><cfoutput>#desc#</cfoutput></span>
                                                        </div>
                                                    </div>
                                                    <div class="CDF_SELECT field-<cfoutput>#fieldItem.ID#</cfoutput>">
                                                        <div class="field-value">
                                                            <cfif arraylen(dataCdfVariable) GT 0>
                                                                <cfset valueCDF = "">
                                                                <cfloop array="#dataCdfVariable#" index="itemVariable">
                                                                    <cfif itemVariable.CDFID EQ fieldItem.ID >
                                                                        <cfset valueCDF = itemVariable.VALUE>
                                                                        <cfbreak>
                                                                    </cfif>
                                                                </cfloop>
                                                                <cfif fieldItem.FIELDTPYE EQ 1>
                                                                    <select class="select1 select-default-value cdf-data" name="<cfoutput>#fieldItem.NAME#</cfoutput>" rel="<cfoutput>#fieldItem.ID#</cfoutput>">
                                                                        <cfloop array="#getCdfHtml.DEFAULTFIELDS#" index="value">
                                                                            <cfif fieldItem.ID EQ value.CDFID>
                                                                                <cfif value.ID EQ valueCDF>
                                                                                    <option value="<cfoutput>#value.ID#</cfoutput>" selected="true"><cfoutput>#value.VALUE#</cfoutput></option>
                                                                                <cfelse>
                                                                                    <option value="<cfoutput>#value.ID#</cfoutput>"><cfoutput>#value.VALUE#</cfoutput></option>
                                                                                </cfif>

                                                                            </cfif>
                                                                        </cfloop>
                                                                    </select>
                                                                <cfelse>
                                                                    <input  class="input_box cdf-data" rel="<cfoutput>#fieldItem.ID#</cfoutput>" name="<cfoutput>#fieldItem.NAME#</cfoutput>" style="text" value="<cfoutput>#valueCDF#</cfoutput>">
                                                                </cfif>
                                                            <cfelse>
                                                                <cfif fieldItem.FIELDTPYE EQ 1>
                                                                        <select class="select1 select-default-value cdf-data" name="<cfoutput>#fieldItem.NAME#</cfoutput>" rel="<cfoutput>#fieldItem.ID#</cfoutput>">
                                                                        <cfloop array="#getCdfHtml.DEFAULTFIELDS#" index="value">
                                                                            <cfif fieldItem.ID EQ value.CDFID>
                                                                                <option value="<cfoutput>#value.ID#</cfoutput>"><cfoutput>#value.VALUE#</cfoutput></option>
                                                                            </cfif>
                                                                        </cfloop>
                                                                        </select>
                                                                <cfelse>
                                                                    <input  class="input_box cdf-data" rel="<cfoutput>#fieldItem.ID#</cfoutput>" name="<cfoutput>#fieldItem.NAME#</cfoutput>" style="text">
                                                                </cfif>
                                                            </cfif>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </cfif>
                                    </cfloop>
                                </cfif>
                            </cfif>
						</div>
						<cfset i++>
                        <div class="clear-both"></div>
					</cfloop>
					<input type="hidden" name="totalStep" id="totalStep" value="<cfoutput>#listlen(newSetup)#</cfoutput>">
					<input type="hidden" id="inpCPPUUID" name="inpCPPUUID" value="<cfoutput>#inpCPPUUID#</cfoutput>">
					<div style="clear:both"></div>
				</form>
                
                <div class="mtop15">
                
                    <a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary dropdown-toggle btnapply" id ="cppBack" onclick="return false;" style="display:none">Back </a>
                    <cfif GetCPPSETUPQUERY.Type_ti EQ 1>
                        <a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary btnapply" id ="cppSubmit" onclick="return false;">Submit </a>
                        <!---<a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary btnapply" id ="cppOptOut" onclick="return false;">Opt Out </a>--->
                    <cfelse>
                        <a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary dropdown-toggle btnapply" id ="cppNext" onclick="nextStep(); return false;">Next </a>
                    </cfif>
                
                </div>
                
			</div>
			<!---<div class="<cfoutput>#submitButtonContainer#</cfoutput> smalltheme">
				<a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary dropdown-toggle btnapply" id ="cppBack" onclick="return false;" style="display:none">Back </a>
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1>
					<a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary btnapply" id ="cppSubmit" onclick="return false;">Submit </a>
                    <!---<a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary btnapply" id ="cppOptOut" onclick="return false;">Opt Out </a>--->
				<cfelse>
					<a href="#" class="<cfoutput>#submitClass#</cfoutput> btn btn-primary dropdown-toggle btnapply" id ="cppNext" onclick="nextStep(); return false;">Next </a>
				</cfif>
			</div>--->
			<cfif GetCPPSETUPQUERY.displayType_ti EQ 1 and trim(GetCPPSETUPQUERY.customHtml_txt) NEQ '<p>You may include the portal any where on the page by typing {%portal%}. You may include it only once.</p>'>
				<div style="clear:both"></div>
				<cfoutput>#portalRight#</cfoutput>
			</cfif>
		</div>
		<div style="clear:both"></div>
	</div>
</div>

<cfoutput>
	<div id="loading">
       <div><img src="../public/images/saving.gif"></div>
   </div>
	<style>
		@import url('#rootUrl#/#publicPath#/css/default.css');
	</style>
</cfoutput>
<cfinclude template="launchJs.cfm">


<cffunction name="HTMLStringFormat" access="public" output="No" >
	<cfargument name="string" type="string" required="Yes" >
	<cfset special = "&ndash;,&mdash;,&iexcl;,&iquest;,&quot;,&ldquo;,&rdquo;,&lsquo;,&rsquo;,&laquo;,&raquo;,&nbsp;,&amp;,&cent;,&copy;,&divide;,&gt;,&lt;,&micro;,&middot;,&para;,&plusmn;,&euro;,&pound;,&reg;,&sect;,&trade;,&yen;,&aacute;,&Aacute;,&agrave;,&Agrave;,&acirc;,&Acirc;,&aring;,&Aring;,&atilde;,&Atilde;,&auml;,&Auml;,&aelig;,&AElig;,&ccedil;,&Ccedil;,&eacute;,&Eacute;,&egrave;,&Egrave;,&ecirc;,&Ecirc;,&euml;,&Euml;,&iacute;,&Iacute;,&igrave;,&Igrave;,&icirc;,&Icirc;,&iuml;,&Iuml;,&ntilde;,&Ntilde;,&oacute;,&Oacute;,&ograve;,&Ograve;,&ocirc;,&Ocirc;,&oslash;,&Oslash;,&otilde;,&Otilde;,&ouml;,&Ouml;,&szlig;,&uacute;,&Uacute;,&ugrave;,&Ugrave;,&ucirc;,&Ucirc;,&uuml;,&Uuml;,&yuml;">
	<cfset normal = "#chr(8211)#,#chr(8212)#,#chr(161)#,#chr(191)#,#chr(34)#,#chr(8220)#,#chr(8221)#,#chr(39)#,#chr(39)#,#chr(171)#,#chr(187)#,#chr(32)#,#chr(38)#,#chr(162)#,#chr(169)#,#chr(247)#,#chr(62)#,#chr(60)#,#chr(181)#,#chr(183)#,#chr(182)#,#chr(177)#,#chr(8364)#,#chr(163)#,#chr(174)#,#chr(167)#,#chr(8482)#,#chr(165)#,#chr(225)#,#chr(193)#,#chr(224)#,#chr(192)#,#chr(226)#,#chr(194)#,#chr(229)#,#chr(197)#,#chr(227)#,#chr(195)#,#chr(228)#,#chr(196)#,#chr(230)#,#chr(198)#,#chr(231)#,#chr(199)#,#chr(233)#,#chr(201)#,#chr(232)#,#chr(200)#,#chr(234)#,#chr(202)#,#chr(235)#,#chr(203)#,#chr(237)#,#chr(205)#,#chr(236)#,#chr(204)#,#chr(238)#,#chr(206)#,#chr(239)#,#chr(207)#,#chr(241)#,#chr(209)#,#chr(243)#,#chr(211)#,#chr(242)#,#chr(210)#,#chr(244)#,#chr(212)#,#chr(248)#,#chr(216)#,#chr(245)#,#chr(213)#,#chr(246)#,#chr(214)#,#chr(223)#,#chr(250)#,#chr(218)#,#chr(249)#,#chr(217)#,#chr(251)#,#chr(219)#,#chr(252)#,#chr(220)#,#chr(255)#">
	<cfset formated = ReplaceList(arguments.string, special, normal)>
	<cfreturn formated>
</cffunction>