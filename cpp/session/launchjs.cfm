<div id="overlay" class="web_dialog_overlay"></div>
<!--- Invite User Popup --->
<div id="dialog_termCondition" class="web_dialog" style="display: none;">
	<form action="" method="POST">
		<div class="dialog_contentddd" style="padding:10px; margin-bottom:10px;">
			<input type="checkbox" id="accepCondition" class="removeCheck" name="accepCondition" value = "1" style="display:inline;">
			<label for="accepCondition"><cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput></label>			
			<cfif trim(GetCPPSETUPQUERY.CPP_Template_vch) EQ ''>
				<cfset cppTemplateIsEmpty = true>
			<cfelse>
				<cfset cppTemplateIsEmpty = false>
			</cfif>
		</div>
		<div style="clear:both"></div>
       	<div class="dialog_content" style="margin:20px; float:none;">
			<button  
				type="button" 
				class="blue btn btn-primary btnapply"
				onClick="savePortal(); return false;"	
			>SUBMIT</button>
			<button 
				type="button" 
				class="blue btn btn-primary btnapply"
				onClick="CloseDialog('termCondition');"	
			>CANCEL</button>
		</div>
	</form>
</div>
<cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_SMALL>
	<cfset mtop = 37>
	<cfset small = true>
<cfelse>
	<cfset small = false>
	<cfset mtop = 8>
</cfif>
<div id="dialog_thankyou" class="web_dialog" style="display: none;">
	<div class="title">Thank You!</div>
	<div class="desc">Thank you for submitting your contact preferences.</div>
	<div style="clear:both"></div>
		<div class="thankyou_btn">
			<a href="<cfoutput>#rootUrl#</cfoutput>/session/home<cfoutput>?inpCPPUUID=#inpCPPUUID#</cfoutput><cfif inpCPPAID NEQ "0">&inpCPPAID=<cfoutput>#inpCPPAID#</cfoutput></cfif>" class="closeThank">Finish</a>
		</div>
</div>
<!--- Need to allow HTML single quotes and such kill old method--->
<!---var CPP_Template_vch = '<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>';--->
<script id="CPP_Template_HTML" type="text/x-jquery-tmpl">
	<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>
</script>

<script type="text/javascript">
    <cfif GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_LARGE OR GetCPPSETUPQUERY.SIZETYPE_INT EQ SIZETYPE_MEDIUM>
    $("#cppLaunchForm span.title-field").each(function(){
        var iHeight = $(this).height();
        if(iHeight > 29){
            var id = $(this).attr('rel');
            $('.field-'+id).height(iHeight+3);
            $('.field-'+id).find('.field-value').css('line-height',iHeight+'px');
        }
    });
    </cfif>
	var mtop = <cfoutput>#mtop#</cfoutput>
	var small = <cfoutput>#small#</cfoutput>
	//custom input for checkbox type
	$('input').customInput();
    $('.select-default-value').css("display","none");
    $(function(){
        $('.select-default-value').selectmenu({
            style:'popup',
            width: 500,
            format: SelectOptionFormatting
        });
    });
	$(function(){
		$('select#language').selectmenu({
			style:'popup',
			width: 176,
			format: addressFormatting
		});
	});
	
	<!--- Bad code - someone copied from tutorial - deos more than it needs and messes up other things - see SelectOptionFormatting instead --->
	//a custom format option callback
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	<!--- http://jsfiddle.net/fnagel/GXtpC/ --->
	var SelectOptionFormatting = function(text, opt){
		var newText = text;
		<!--- Rplace anything () is capture group where $! is where it will go and [\s\S]* is everthing --->
		newText = newText.replace(/([\s\S]*)/, '<span class="ui-selectmenu-item-content">$1</span>');
		
		return newText;
	}
	
	<!---//init type mask phone US--->
	var oStringMask = new Mask("(###) ###-####");
	
	$("#VoiceNumberList").val($("#VoiceNumberList").attr("data-init"));
	$("#SMSNumberList").val($("#SMSNumberList").attr("data-init"));
	$("#EmailList").val($("#EmailList").attr("data-init"));
	
	var CPP_Template_vch = $("#CPP_Template_HTML").tmpl();
	
	var cppTemplateIsEmpty = <cfoutput>#cppTemplateIsEmpty#</cfoutput>;
	
	if($('#formcontent').width() <= 310){
		$('#formcontent').css('overflow-x','auto');
	}
	
	$('#cppSubmit').click(function(){
		if(cppTemplateIsEmpty){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{
			ShowDialog('termCondition');
			$('#accepConditionForm').val(0);
		}
		
	});
	var step = 1;
	
	$('#cppBack').click(function(){
		if(step >= $('#totalStep').val()){
			$('#cppNext').html('Next');
		}
		$('#stepContent'+step).hide();
		step -=1;
		$('#stepContent'+step).show();
		$('#cppNext').show();
		if(step == 1){
			$('#cppBack').hide();
		}
	});
	
	function nextStep(){
		$('#inpStep').val($('#stepVal'+step).val()); 
		
		var voiceCheck = $("input[name='voiceCheck']:checked" ).length;
		var smsCheck = $("input[name='smsCheck']:checked" ).length;
		var emailCheck = $("input[name='emailCheck']:checked" ).length;
		var VoiceNumberObject = voiceCheck > 0 ? GetVoiceNumberObject() : [];
		var SMSNumberObject = smsCheck > 0 ? GetSMSNumberObject(): [];
		var EmailObject = emailCheck > 0 ? GetEmailObject(): [];
		var inpCPPUUID = $("#inpCPPUUID").val();
		var Preview = $("#Preview").val();
		var inpStep = $("#inpStep").val();		
		var PREFERENCECHECK = GetPreference();				
		var language = $("#language").val();
		var accepConditionForm = $("#accepConditionForm").val();
		var dataObj = {
			VoiceNumberObject: JSON.stringify(VoiceNumberObject),
			SMSNumberObject: JSON.stringify(SMSNumberObject),
			EmailObject: JSON.stringify(EmailObject),
			inpCPPUUID: inpCPPUUID,
			Preview: Preview,
			inpStep: inpStep,
			PREFERENCECHECK: JSON.stringify(PREFERENCECHECK),
			voiceCheck: voiceCheck,
			smsCheck: smsCheck,
			emailCheck: emailCheck,
			language: language,
			accepConditionForm: accepConditionForm
		};
		$.ajax({
        	type: "POST",
           	url:"<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           	dataType: "json",           	
			data: dataObj,
           	success: function(d){
				//remove css error
				$("#inpVoiceNumber input").each(function() {
			       	$(this).css("border","");
			    });
				$("#inpSMSNumber input").each(function() {
			       	$(this).css("border","");
			    });
				$("#inpEmail input").each(function() {
			       	$(this).css("border","");
			    });
				//Check if variable is part of JSON result string								
				if(typeof(d.RXRESULTCODE) != "undefined")
				{
					CurrRXResultCode = d.RXRESULTCODE;
					
					if(CurrRXResultCode > 0)
					{
           				$('#cppBack').show();
           				if(step == $('#totalStep').val() ){
           					if(cppTemplateIsEmpty){
								$('#accepConditionForm').val(1);								
           						savePortalAjax();
           					}else{
           						
           						ShowDialog('termCondition');
           						$('#accepConditionForm').val(0);
           					}
           				}else{
           					$('#stepContent'+(step)).hide();
							
           					step +=1;
           					if(step == $('#totalStep').val()){
								$('#cppNext').html('Submit');
							}
           					$('#stepContent'+step).show();
           				}
           			}else{
						jAlert(d.MESSAGE, "Failure!", function(error){
           					if(error){
           						if(d.ERRORINDEX != "undefined" && d.ERRORTYPE != "undefined")
								{
									if(d.ERRORTYPE == "VOICE")
									{
										var voiceNumber = $("#VoiceNumberList").val();
										var ListVoiceNumber = voiceNumber.split(",");
										var indexError = ListVoiceNumber[d.ERRORINDEX]; 
										$("#voiceText1_"+indexError).css("border","1px solid red");
										$("#voiceText2_"+indexError).css("border","1px solid red");
										$("#voiceText3_"+indexError).css("border","1px solid red");
										$("#voiceText1_"+indexError).focus();
									}
									else if(d.ERRORTYPE == "SMS")
									{
										var smsNumber = $("#SMSNumberList").val();
										var ListSMSNumber = smsNumber.split(",");
										var indexError = ListSMSNumber[d.ERRORINDEX]; 
										$("#smsText1_"+indexError).css("border","1px solid red");
										$("#smsText2_"+indexError).css("border","1px solid red");
										$("#smsText3_"+indexError).css("border","1px solid red");
										$("#smsText1_"+indexError).focus();
									} 
									else if(d.ERRORTYPE == "EMAIL")
									{
										var emails = $("#EmailList").val();
										var ListEmail = emails.split(",");
										var indexError = ListEmail[d.ERRORINDEX]; 
										$("#email_"+indexError).css("border","1px solid red");
										$("#email_"+indexError).focus();
									}
								}
           					}
						});		
					}
           		}
         	}
		});
		
	}
	
	function savePortal(){
		if($('#accepCondition').is(':checked')){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{
			jAlert('Please check the term conditon checkbox!', "Checking term condition !");	
			$('#accepConditionForm').val(0);
		}
	}
	function GetPreference(){
		 var PreferenceArray = [];
	     $("input[name='preferenceCheck']:checked").each(function() {
	       PreferenceArray.push($(this).val());
	     });
		 return PreferenceArray;
	}
	function GetCDFData(){
        var CDFData = {};
        $("#cppLaunchForm .cdf-data").each(function(){
            var group = {};
            var value = $(this).val();
            var name = $(this).attr('name');
            var id = $(this).attr('rel');
            if(typeof value !="undefined" && value !=""){
               group[name] = value;
               CDFData[id] = group;

            }
        });
        return CDFData;
    }
	function savePortalAjax(){
		var voiceCheck = $("input[name='voiceCheck']:checked" ).length;
		var smsCheck = $("input[name='smsCheck']:checked" ).length;
		var emailCheck = $("input[name='emailCheck']:checked" ).length;
		var VoiceNumberObject = voiceCheck > 0 ? GetVoiceNumberObject() : [];
		var SMSNumberObject = smsCheck > 0 ? GetSMSNumberObject(): [];
		var EmailObject = emailCheck > 0 ? GetEmailObject(): [];
		var inpCPPUUID = $("#inpCPPUUID").val();
		var Preview = $("#Preview").val();
		var inpStep = $("#inpStep").val();		
		var PREFERENCECHECK = GetPreference();				
		var language = $("#language").val();
		var accepConditionForm = $("#accepConditionForm").val();
		var dataObj = {
			VoiceNumberObject: JSON.stringify(VoiceNumberObject),
			SMSNumberObject: JSON.stringify(SMSNumberObject),
			EmailObject: JSON.stringify(EmailObject),
			inpCPPUUID: inpCPPUUID,
			Preview: Preview,
			inpStep: inpStep,
			PREFERENCECHECK: JSON.stringify(PREFERENCECHECK),
			voiceCheck: voiceCheck,
			smsCheck: smsCheck,
			emailCheck: emailCheck,
			language: language,
			accepConditionForm: accepConditionForm,
            CDFData: JSON.stringify(GetCDFData())
		};		
		
		$("#loading").show();
		$.ajax({
           type: "POST",
           url:"<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: dataObj,		   
           success: function(d){	
	           	if (d.ROWCOUNT > 0) 
				{
					$("#inpVoiceNumber input").each(function() {
				       	$(this).css("border","");
				    });
					$("#inpSMSNumber input").each(function() {
				       	$(this).css("border","");
				    });
					$("#inpEmail input").each(function() {
				       	$(this).css("border","");
				    });
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;
						if(CurrRXResultCode > 0)
						{
	           				CloseDialog('termCondition');
	           				$('.removeCheck').removeAttr('checked');
	          				$('.removeText').val('');
	          				$('#accepConditionForm').val(0);
							$("#loading").hide();	          				
       						//location.href ="/thankyou/<cfoutput>#inpCPPUUID#</cfoutput>";
							ShowDialog('thankyou');	           					
	           			}
						else{
	           				CloseDialog('termCondition');							
							$("#loading").hide();
							jAlert(d.MESSAGE, "Failure!", function(error){
	           					if(error){
	           						if(d.ERRORINDEX != "undefined" && d.ERRORTYPE != "undefined")
									{
										if(d.ERRORTYPE == "VOICE")
										{
											var voiceNumber = $("#VoiceNumberList").val();
											var ListVoiceNumber = voiceNumber.split(",");
											var indexError = ListVoiceNumber[d.ERRORINDEX]; 
											$("#voiceText_"+indexError).css("border","1px solid red");
											$("#voiceText_"+indexError).focus();
										}
										else if(d.ERRORTYPE == "SMS")
										{
											var smsNumber = $("#SMSNumberList").val();
											var ListSMSNumber = smsNumber.split(",");
											var indexError = ListSMSNumber[d.ERRORINDEX]; 
											$("#smsText_"+indexError).css("border","1px solid red");
											$("#smsText_"+indexError).focus();
										} 
										else if(d.ERRORTYPE == "EMAIL")
										{
											var emails = $("#EmailList").val();
											var ListEmail = emails.split(",");
											var indexError = ListEmail[d.ERRORINDEX]; 
											$("#email_"+indexError).css("border","1px solid red");
											$("#email_"+indexError).focus();
										}
									}
	           					}
							});		
						}
	           		}
	           	}
				else{
					$("#loading").hide();
					ShowDialog('thankyou');	  
				}
           		$('#accepCondition').removeAttr('checked');
           }
  		});
	}
	
	function ShowDialog(dialog_id){
		$('#accepCondition').customInput();
		$("#overlay").show();
	    $("#dialog_" + dialog_id).fadeIn(300);
	    $("#dialog_" + dialog_id).css('left', ($(window).width() - $("#dialog_" + dialog_id).width())/ 2 +  'px');
	    $("#dialog_" + dialog_id).css('top', ($(window).height() - $("#dialog_" + dialog_id).height())/ 2 +  'px');
	    $(".groupRequire").hide();
	}
	
	function CloseDialog(dialog_id){
		//clear text box data
		$("#inpGroupContactName").val("");
		$("#overlay").hide();
	    $("#dialog_" + dialog_id).fadeOut(300);
	    $(".groupRequire").hide();
	    $('#accepCondition').removeAttr('checked');
	}
	
	//start add or del Voice number contact
	var VoiceNumberIndex = <cfoutput>#maxPhone#</cfoutput>;
	
	//attach mask phone US for new voice contact
	if (VoiceNumberIndex > 0) {
		for (var i = 0; i < VoiceNumberIndex; i++) {
			var j = (i + 1).toString();
			oStringMask.attach(document.getElementById("voiceText_" + j));
			$("#voiceText_" + j).blur();
		}
	}
	else{
		VoiceNumberIndex = 1;
	}
	var newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
	
	function fnAddVoiceNumber() {
		if(small){
			$("#actionVoice").css("margin-top", "29px");	
		}
		var voiceNumber = $("#VoiceNumberList").val();
		VoiceNumberIndex++;
		var row = "<div id='VoiceNumnerPanel"+VoiceNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='voiceText_"+VoiceNumberIndex.toString()+"' id='voiceText_"+VoiceNumberIndex.toString()+"' class=\"input_box_last\" size=\"4\" value=\"\">"+
		"</div>";
		var action = "<input type=\"button\" name='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' id='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' class=\"delContact\" onclick=\"fnDelVoiceNumber('VoiceNumnerPanel"+VoiceNumberIndex.toString()+"','"+VoiceNumberIndex.toString()+",')\">";
		$("#inpVoiceNumber").append(row);
		$("#actionVoice").append(action);
		$("#VoiceNumberList").val(voiceNumber + VoiceNumberIndex.toString() + ",");
		$("#innerCPP").animate({ scrollTop: $('#inpVoiceNumber')[0].scrollHeight + 100}, 1000);
		if(voiceNumber.length > 0)
		{
			var voice = voiceNumber.split(",");
			if(voice.length >= 2){
				$("#DelVoiceNumber_"+voice[0]).removeClass("delHide").addClass("delShow");
			}
			$("#AddVoiceNumber").css("margin-top", (voice.length - 1) * 28 + parseInt(mtop));
		}
		newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
		oStringMask.attach(document.getElementById("voiceText_"+VoiceNumberIndex));
		return false;
	}; 
	
	function fnDelVoiceNumber(delPanel, index)
	{
		
		var voiceNumber = $("#VoiceNumberList").val();
		$("#"+delPanel).remove();
		$("#DelVoiceNumber_"+index).remove();
		$("#VoiceNumberList").val(voiceNumber.replace(index,""));
		
		var newVoiceNumber = $("#VoiceNumberList").val();
		if(newVoiceNumber.length > 0)
		{
			var voice = newVoiceNumber.split(",");
			var newVoiceNumberBeforeDelArray = newVoiceNumberBeforeDel.split(",");
			var firstIndex = newVoiceNumberBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#voiceText_"+voice[0]).removeClass("input_box_last").addClass("input_box");	
			}
						
			<!--- Allow delete to stay - user uses this to opt out --->
			if(voice.length == 2){
				<!---$("#DelVoiceNumber_"+voice[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionVoice").css("margin-top", "29px");
				}
			}
					
			
			$("#AddVoiceNumber").css("margin-top", (voice.length - 2) * 28 + parseInt(mtop));
		}
		newVoiceNumberBeforeDel = $("#VoiceNumberList").val();
		
		
		
	}
	
	function GetVoiceNumberObject()
	{
		var voiceNumber = $("#VoiceNumberList").val();
		var ListVoiceNumber = voiceNumber.split(",");
		var VoiceNumberArray = [];
		for (var i=0; i < ListVoiceNumber.length; i++)
		{
			var index = ListVoiceNumber[i];
			if(index!="" && parseInt(index) > 0)
			{
				var voiceContact = $("#voiceText_"+index).val().trim().replace(/\D/g, '');
				VoiceNumberArray.push(voiceContact);
			} 
		}
		return  VoiceNumberArray;
	}
	
	//end add or del Voice number contact
	
	
	//start add or del SMS number contact
	var  SMSNumberIndex = <cfoutput>#maxSms#</cfoutput>;
	//attach mask phone US for new sms contact
	if (SMSNumberIndex > 0) {
		for (var i = 0; i < SMSNumberIndex; i++) {
			var j = (i + 1).toString();
			oStringMask.attach(document.getElementById("smsText_" + j));
			$("#smsText_" + j).blur();
		}
	}
	else{
		SMSNumberIndex = 1;
	}
	var newSMSNumberBeforeDel = $("#SMSNumberList").val();
	function fnAddSMSNumber() {
		SMSNumberIndex++;
		if (small) {
			$("#actionSms").css("margin-top", "29px");
		}
		var row = "<div id='SMSNumnerPanel"+SMSNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='smsText_"+SMSNumberIndex.toString()+"' id='smsText_"+SMSNumberIndex.toString()+"' class=\"input_box_last\" size=\"4\" value=\"\">"+
		"</div>";
		var action = "<input type=\"button\" name='DelSMSNumber_"+SMSNumberIndex.toString()+"' id='DelSMSNumber_"+SMSNumberIndex.toString()+"' class=\"delContact\" onclick=\"fnDelSMSNumber('SMSNumnerPanel"+SMSNumberIndex.toString()+"','"+SMSNumberIndex.toString()+",')\">";
		$("#inpSMSNumber").append(row);
		$("#actionSms").append(action);
		var SMSNumber = $("#SMSNumberList").val();
		$("#SMSNumberList").val(SMSNumber + SMSNumberIndex.toString() + ",");
		if(typeof $('#inpVoiceNumber')[0] !=="undefined")
		{
			$("#innerCPP").animate({ scrollTop: $('#inpVoiceNumber')[0].scrollHeight + $('#inpSMSNumber')[0].scrollHeight}, 1000);	
		}
		else{
			$("#innerCPP").animate({ scrollTop: $('#inpSMSNumber')[0].scrollHeight}, 1000);
		}
		
		if(SMSNumber.length > 0)
		{
			var sms = SMSNumber.split(",");
			if(sms.length >= 2){
				$("#DelSMSNumber_"+sms[0]).removeClass("delHide").addClass("delShow");
			}
			$("#AddSMSNumber").css("margin-top", (sms.length - 1) * 28 + parseInt(mtop));
		}
		newSMSNumberBeforeDel = $("#SMSNumberList").val();
		oStringMask.attach(document.getElementById("smsText_"+SMSNumberIndex));
		return false;
	}; 
	
	function fnDelSMSNumber(delPanel, index)
	{
		$("#"+delPanel).remove();
		var SMSNumber = $("#SMSNumberList").val();
		$("#DelSMSNumber_"+index).remove();
		$("#SMSNumberList").val(SMSNumber.replace(index,""));
		
		var newSMSNumber = $("#SMSNumberList").val();
		if(newSMSNumber.length > 0)
		{
			var sms = newSMSNumber.split(",");
			var newSMSNumberBeforeDelArray = newSMSNumberBeforeDel.split(",");
			var firstIndex = newSMSNumberBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#smsText_"+sms[0]).removeClass("input_box_last").addClass("input_box");	
			}
			if(sms.length == 2){
				
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelSMSNumber_"+sms[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionSms").css("margin-top", "29px");
				}
			}
			$("#AddSMSNumber").css("margin-top", (sms.length - 2) * 28 + parseInt(mtop));
		}
		newSMSNumberBeforeDel = $("#SMSNumberList").val();
	}
	
	function GetSMSNumberObject()
	{
		var smsNumber = $("#SMSNumberList").val();
		var ListSMSNumber = smsNumber.split(",");
		var SMSNumberArray = [];
		for (var i=0; i < ListSMSNumber.length; i++)
		{
			var index = ListSMSNumber[i];
			if(index!="" && parseInt(index) > 0)
			{
				var smsContact = $("#smsText_"+index).val().trim().replace(/\D/g, '');
				SMSNumberArray.push(smsContact);
			}
		}
		return SMSNumberArray;
	}
	
	//end add or del SMS number contact
	
	
	//start add or del Email contact
	var  EmailIndex = <cfoutput>#maxEmail#</cfoutput>;
	var newEmailBeforeDel = $("#EmailList").val();
	function fnAddEmail() {		
		EmailIndex++;
		if (small) {
			$("#actionEmail").css("margin-top", "29px");
		}
		var row = "<div id='EmailPanel"+EmailIndex.toString()+"'>"+
			"<input type=\"text\" name='email_"+EmailIndex.toString()+"' id='email_"+EmailIndex.toString()+"' class=\"input_box_last\" value=\"\">"+
		"</div>";		
		var action = "<input type=\"button\" name='DelEmail_"+EmailIndex.toString()+"' id='DelEmail_"+EmailIndex.toString()+"' class=\"delContact\" onclick=\"fnDelEmail('EmailPanel"+EmailIndex.toString()+"','"+EmailIndex.toString()+",')\">";
		$("#inpEmail").append(row);		
		$("#actionEmail").append(action);
		var Emailnumber = $("#EmailList").val();
		$("#EmailList").val(Emailnumber + EmailIndex.toString() + ",");
		$("#innerCPP").animate({ scrollTop: $('#innerCPP')[0].scrollHeight}, 1000);
		
		if(Emailnumber.length > 0)
		{
			var email = Emailnumber.split(",");
			if(email.length >= 2){
				$("#DelEmail_"+email[0]).removeClass("delHide").addClass("delShow");
			}
			$("#AddEmail").css("margin-top", (email.length - 1) * 28 + parseInt(mtop));
		}
		newEmailBeforeDel = $("#EmailList").val();
		return false;
	}; 
	
	function fnDelEmail(delPanel, index)
	{
		$("#"+delPanel).remove();
		$("#EmailList").val($("#EmailList").val().replace(index,""));
		$("#DelEmail_"+index).remove();
		var newEmailNumber = $("#EmailList").val();
		if(newEmailNumber.length > 0)
		{
			var email = newEmailNumber.split(",");
			var newEmailBeforeDelArray = newEmailBeforeDel.split(",");
			var firstIndex = newEmailBeforeDelArray[0];
			if(parseInt(index) == parseInt(firstIndex)){
				$("#email_"+email[0]).removeClass("input_box_last").addClass("input_box");	
			}
			if(email.length == 2){
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelEmail_"+email[0]).removeClass("delShow").addClass("delHide");--->
				if (small) {
					$("#actionEmail").css("margin-top", "29px");
				}
			}
			$("#AddEmail").css("margin-top", (email.length - 2) * 28 + parseInt(mtop));
		}
		newEmailBeforeDel = $("#EmailList").val();
	}
	
	function GetEmailObject()
	{
		var Email = $("#EmailList").val();
		var ListEmail = Email.split(",");
		var ListEmailArray = [];
		for (var i=0; i < ListEmail.length; i++)
		{
			var index = ListEmail[i];
			if(index!="" && parseInt(index) > 0)
			{
				var email = $("#email_"+index).val().trim();
				ListEmailArray.push(email);
			} 
		}
		return  ListEmailArray;
	}
	//end add or del Email contact
	
	function CheckEvent(){
		var object = $("input[name='preferenceCheck']:checked");
		if(object.length > 0){
			$("input[name='preferenceCheck']").each(function(){
				$(this).removeAttr("checked");
				$('label[for='+$(this).attr('id')+']').removeClass('checked');				
			});
			$("#CheckEvent").html("Check all");	
		}
		else{
			$("input[name='preferenceCheck']").each(function(){
				$(this).attr("checked", "checked");
				$('label[for='+$(this).attr('id')+']').addClass('checked');				
			});
			$("#CheckEvent").html("Uncheck all");	
		}		
	}
</script>