<div id="overlay" class="web_dialog_overlay"></div>
<!--- Invite User Popup --->
<cfif trim(GetCPPSETUPQUERY.CPP_Template_vch) EQ ''>
	<cfset cppTemplateIsEmpty = true>
<cfelse>
	<cfset cppTemplateIsEmpty = false>
</cfif>
<div id="dialog_termCondition" class="web_dialog" style="display: none;">
	<form action="" method="POST">
		<div class="dialog_content">
			<input type="checkbox" id="accepCondition" class="removeCheck" name="accepCondition" value = "1" >
			<span id="textCondition" >
				<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>
			</span>
		</div>
	</form>
</div>

<!--- Need to allow HTML single quotes and such kill old method --->
<!---var CPP_Template_vch = '<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>';--->
<cfoutput>
	<script id="CPP_Template_HTML" type="text/x-jquery-tmpl">
        #GetCPPSETUPQUERY.CPP_Template_vch#
    </script>
</cfoutput>

<script type="text/javascript">	
	$('input').customInput();
	
	$(function(){
		$('select#language').selectmenu({
			style:'popup',
			width: 176,
			format: addressFormatting
		});
	});
	//a custom format option callback
	var addressFormatting = function(text, opt){
		var newText = text;
		//array of find replaces
		var findreps = [
			{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
			{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
			{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
			{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
		];

		for(var i in findreps){
			newText = newText.replace(findreps[i].find, findreps[i].rep);
		}
		return newText;
	}
	var CPP_Template_vch = $("#CPP_Template_HTML").tmpl();	
	var cppTemplateIsEmpty = <cfoutput>#cppTemplateIsEmpty#</cfoutput>;
	
	<!--- need to format for possible double quotes --->
	var textCondition = "<cfoutput>#Replace(GetCPPSETUPQUERY.CPP_Template_vch, '"', '\"', "ALL")#</cfoutput>";
		
	var step = 1;
	
	function launchBack(){
		if(step >= $('#totalStep').val()){
			$('#cppNext').val('Next');
		}
		$('#stepContent'+step).hide();
		step -=1;
		$('#stepContent'+step).show();
		$('#cppNext').show();
		if(step == 1){
			$('#cppBackContainer').hide();
		}
	}
		
	function launchSubmit(){
		if(cppTemplateIsEmpty){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{			
			//ShowDialog('termCondition');
			$('<div>').simpledialog2({
				mode: 'blank',
				headerText: 'Term condition',
			    headerClose: false,
				width: 300,
			    blankContent : 
			      "<form><label><input data-theme='d' name=\"checkboxCondition\" type=\"checkbox\" onclick=\"CheckCondition();\">"+textCondition+"</label></form>"+
				  "<a rel='' data-role='button' data-theme='d' data-mini='true' href='#' onclick='savePortal();'>Submit</a>"+								      
			      "<a rel='close' data-role='button' data-theme='d' data-mini='true' href='#'>Close</a>"
			});
			$('#accepConditionForm').val(0);
		}
	}
	
	function nextStep(){
		$.mobile.loading( 'show', {
			text: 'Loading',
			textVisible: true,
			theme: 'd',
			html: ""
		});
		$('#inpStep').val($('#stepVal'+step).val());
		var voiceCheck = $("input[name='voiceCheck']:checked" ).length;
		var smsCheck = $("input[name='smsCheck']:checked" ).length;
		var emailCheck = $("input[name='emailCheck']:checked" ).length;
		var VoiceNumberObject = voiceCheck > 0 ? GetVoiceNumberObject() : [];
		var SMSNumberObject = smsCheck > 0 ? GetSMSNumberObject(): [];
		var EmailObject = emailCheck > 0 ? GetEmailObject(): [];
		var inpCPPUUID = $("#inpCPPUUID").val();
		var Preview = $("#Preview").val();
		var inpStep = $("#inpStep").val();		
		var PREFERENCECHECK = GetPreference();				
		var language = $("#language").val();
		var accepConditionForm = $("#accepConditionForm").val();
		var dataObj = {
			VoiceNumberObject: JSON.stringify(VoiceNumberObject),
			SMSNumberObject: JSON.stringify(SMSNumberObject),
			EmailObject: JSON.stringify(EmailObject),
			inpCPPUUID: inpCPPUUID,
			Preview: Preview,
			inpStep: inpStep,
			PREFERENCECHECK: JSON.stringify(PREFERENCECHECK),
			voiceCheck: voiceCheck,
			smsCheck: smsCheck,
			emailCheck: emailCheck,
			language: language,
			accepConditionForm: accepConditionForm
		};
		$.ajax({
        	type: "POST",
           	url:"<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           	dataType: "json", 
			data: dataObj,
           	success: function(d){
				$.mobile.loading( 'hide');
				
				//remove border erorr
				$("#inpVoiceNumber input").each(function() {
			       	$(this).parent().css("border","");
			    });
				$("#inpSMSNumber input").each(function() {
			       	$(this).parent().css("border","");
			    });
				$("#inpEmail input").each(function() {
			       	$(this).parent().css("border","");
			    });
				
				//Check if variable is part of JSON result string								
				if(typeof(d.RXRESULTCODE) != "undefined")
				{	
					CurrRXResultCode = d.RXRESULTCODE;	
					
					if(CurrRXResultCode > 0)
					{
           				$('#cppBackContainer').show();
           				if(step == $('#totalStep').val() ){
           					if(cppTemplateIsEmpty){
								$('#accepConditionForm').val(1);
           						savePortalAjax();
           					}else{
								$('<div>').simpledialog2({
									mode: 'blank',
									headerText: 'Term condition',
								    headerClose: false,
									width: 300,
								    blankContent : 
								      "<form><label><input data-theme='d' name=\"checkboxCondition\" type=\"checkbox\" onclick=\"CheckCondition();\">"+textCondition+"</label></form>"+
									  "<a rel='' data-role='button' data-theme='d' data-mini='true' href='#' onclick='savePortal();'>Submit</a>"+								      
								      "<a rel='close' data-role='button' data-theme='d' data-mini='true' href='#'>Close</a>"
								});
           						$('#accepConditionForm').val(0);
           					}
           				}else{
           					$('#stepContent'+(step)).hide();
           					step +=1;
           					if(step == $('#totalStep').val()){
								$('#cppNext').val('Submit');
							}
           					$('#stepContent'+step).show();
           				}
           				
           			}else{		
						$('<div>').simpledialog2({
						    mode: 'button',
						    headerText: 'Failure!',
						    headerClose: false,
						    buttonPrompt: d.MESSAGE,
						    buttons : {
						      'OK': {
						        click: function () { 
						          if(d.ERRORINDEX != "undefined" && d.ERRORTYPE != "undefined")
									{
										if(d.ERRORTYPE == "VOICE")
										{
											var voiceNumber = $("#VoiceNumberList").val();
											var ListVoiceNumber = voiceNumber.split(",");
											var indexError = ListVoiceNumber[d.ERRORINDEX]; 
											$("#voiceText1_"+indexError).parent().css("border","1px solid red");
											$("#voiceText2_"+indexError).parent().css("border","1px solid red");
											$("#voiceText3_"+indexError).parent().css("border","1px solid red");
											$("#voiceText1_"+indexError).focus();
										}
										else if(d.ERRORTYPE == "SMS")
										{
											var smsNumber = $("#SMSNumberList").val();
											var ListSMSNumber = smsNumber.split(",");
											var indexError = ListSMSNumber[d.ERRORINDEX]; 
											$("#smsText1_"+indexError).parent().css("border","1px solid red");
											$("#smsText2_"+indexError).parent().css("border","1px solid red");
											$("#smsText3_"+indexError).parent().css("border","1px solid red");
											$("#smsText1_"+indexError).focus();
										} 
										else if(d.ERRORTYPE == "EMAIL")
										{
											var emails = $("#EmailList").val();
											var ListEmail = emails.split(",");
											var indexError = ListEmail[d.ERRORINDEX]; 
											$("#email_"+indexError).parent().css("border","1px solid red");
											$("#email_"+indexError).focus();
										}
									}
						        },
						        icon: "alert",
						        theme: "d"
						      }
						  }
					  	})				
						return false;
					}
           		}
           	}
		});
		
	}
	
	function CheckCondition(){		
		if($("input[name='checkboxCondition']:checked" ).length > 0){
			$('#accepCondition').attr('checked', 'checked');			
		}
		else{
			$('#accepCondition').removeAttr('checked');
		}
		console.log($('#accepCondition').is(':checked'));
	}
	
	function savePortal(){
		if($('#accepCondition').is(':checked')){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{
			alert('Please check the term conditon checkbox!');	
			$('#accepConditionForm').val(0);			
		}		
	}
	
	function savePortalAjax(){
		var voiceCheck = $("input[name='voiceCheck']:checked" ).length;
		var smsCheck = $("input[name='smsCheck']:checked" ).length;
		var emailCheck = $("input[name='emailCheck']:checked" ).length;
		var VoiceNumberObject = voiceCheck > 0 ? GetVoiceNumberObject() : [];
		var SMSNumberObject = smsCheck > 0 ? GetSMSNumberObject(): [];
		var EmailObject = emailCheck > 0 ? GetEmailObject(): [];
		var inpCPPUUID = $("#inpCPPUUID").val();
		var Preview = $("#Preview").val();
		var inpStep = $("#inpStep").val();		
		var PREFERENCECHECK = GetPreference();				
		var language = $("#language").val();
		var accepConditionForm = $("#accepConditionForm").val();
		var dataObj = {
			VoiceNumberObject: JSON.stringify(VoiceNumberObject),
			SMSNumberObject: JSON.stringify(SMSNumberObject),
			EmailObject: JSON.stringify(EmailObject),
			inpCPPUUID: inpCPPUUID,
			Preview: Preview,
			inpStep: inpStep,
			PREFERENCECHECK: JSON.stringify(PREFERENCECHECK),
			voiceCheck: voiceCheck,
			smsCheck: smsCheck,
			emailCheck: emailCheck,
			language: language,
			accepConditionForm: accepConditionForm
		};
		$.mobile.loading( 'show', {
			text: 'Saving',
			textVisible: true,
			theme: 'd',
			html: ""
		});
		$.ajax({
           type: "POST",
           url:"<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/cpp.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: dataObj,		   
           success: function(d){
	           	if (d.ROWCOUNT > 0) 
				{
					//remove border erorr
					$("#inpVoiceNumber input").each(function() {
				       	$(this).parent().css("border","");
				    });
					$("#inpSMSNumber input").each(function() {
				       	$(this).parent().css("border","");
				    });
					$("#inpSMSNumber input").each(function() {
				       	$(this).parent().css("border","");
				    });
					//Check if variable is part of JSON result string 								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;
						if(CurrRXResultCode > 0)
						{
	           				CloseDialog('termCondition');
	           				$('.removeCheck').removeAttr('checked');
	          				$('.removeText').val('');
	          				$('#accepConditionForm').val(0);
							$.mobile.loading( 'hide');         				
       						location.href ="/mobile/thankyou/<cfoutput>#inpCPPUUID#</cfoutput><cfif inpCPPAID NEQ "0">?inpCPPAID=<cfoutput>#inpCPPAID#</cfoutput></cfif>";	           					
	           			}
						else{
	           				CloseDialog('termCondition');							
							$.mobile.loading( 'hide');
							$('<div>').simpledialog2({
						    mode: 'button',
						    headerText: 'Failure!',
						    headerClose: false,
						    buttonPrompt: d.MESSAGE,
						    buttons : {
						      'OK': {
						        click: function () { 
						          if(d.ERRORINDEX != "undefined" && d.ERRORTYPE != "undefined")
									{
										if(d.ERRORTYPE == "VOICE")
										{
											var voiceNumber = $("#VoiceNumberList").val();
											var ListVoiceNumber = voiceNumber.split(",");
											var indexError = ListVoiceNumber[d.ERRORINDEX]; 
											$("#voiceText1_"+indexError).parent().css("border","1px solid red");
											$("#voiceText2_"+indexError).parent().css("border","1px solid red");
											$("#voiceText3_"+indexError).parent().css("border","1px solid red");
											$("#voiceText1_"+indexError).focus();
										}
										else if(d.ERRORTYPE == "SMS")
										{
											var smsNumber = $("#SMSNumberList").val();
											var ListSMSNumber = smsNumber.split(",");
											var indexError = ListSMSNumber[d.ERRORINDEX]; 
											$("#smsText1_"+indexError).parent().css("border","1px solid red");
											$("#smsText2_"+indexError).parent().css("border","1px solid red");
											$("#smsText3_"+indexError).parent().css("border","1px solid red");
											$("#smsText1_"+indexError).focus();
										} 
										else if(d.ERRORTYPE == "EMAIL")
										{
											var emails = $("#EmailList").val();
											var ListEmail = emails.split(",");
											var indexError = ListEmail[d.ERRORINDEX]; 
											$("#email_"+indexError).parent().css("border","1px solid red");
											$("#email_"+indexError).focus();
										}
									}
						        },
						        icon: "alert",
						        theme: "d"
					    	  }
						 	 }
				  			})		
						}
	           		}
	           	}
           		$('#accepCondition').removeAttr('checked');
           }
  		});
		return false;
	}
	
	function ShowDialog(dialog_id){
		$("#overlay").show();
	    $("#dialog_" + dialog_id).fadeIn(300);
	    $(".groupRequire").hide();
	}
	
	function CloseDialog(dialog_id){
		//clear text box data
		$("#inpGroupContactName").val("");
		$("#overlay").hide();
	    $("#dialog_" + dialog_id).fadeOut(300);
	    $(".groupRequire").hide();
	    $('#accepCondition').removeAttr('checked');
	}
	
	function GetPreference(){
		 var PreferenceArray = [];
	     $("input[name='preferenceCheck']:checked").each(function() {
	       PreferenceArray.push($(this).val());
	     });
		 return PreferenceArray;
	}
	
	//start add or del Voice number contact
	var VoiceNumberIndex = <cfoutput>#maxPhone#</cfoutput>;
	function fnAddVoiceNumber() {
		var voiceNumber = $("#VoiceNumberList").val();
		VoiceNumberIndex++;
		var row = "<div id='VoiceNumnerPanel"+VoiceNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='voiceText1_"+VoiceNumberIndex.toString()+"' id='voiceText1_"+VoiceNumberIndex.toString()+"' class=\"portaladd-input-box1\" size=\"4\" value=\"\">"+
			"<input type=\"text\" name='voiceText2_"+VoiceNumberIndex.toString()+"' id='voiceText2_"+VoiceNumberIndex.toString()+"' class=\"portaladd-input-box1\" size=\"4\" value=\"\">"+
			"<input type=\"text\" name='voiceText3_"+VoiceNumberIndex.toString()+"' id='voiceText3_"+VoiceNumberIndex.toString()+"' class=\"portaladd-input-box2\" size=\"4\" value=\"\">"+		
			"<a style=\"float:left;\" name='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' id='DelVoiceNumber_"+VoiceNumberIndex.toString()+"' href=\"javascript:fnDelVoiceNumber('VoiceNumnerPanel"+VoiceNumberIndex.toString()+"','"+VoiceNumberIndex.toString()+",');\" data-role=\"button\" data-icon=\"delete\" data-iconpos=\"notext\">Delete Voice number</a>"+
		"</div><div style=\"clear:both;\"></div>";
		
		$("#inpVoiceNumber").append(row);	
		
		$("#voiceText1_" + VoiceNumberIndex.toString()).textinput();
		$("#voiceText2_" + VoiceNumberIndex.toString()).textinput();
		$("#voiceText3_" + VoiceNumberIndex.toString()).textinput();
		
		$("#DelVoiceNumber_" + VoiceNumberIndex.toString()).buttonMarkup({ iconpos: "notext"});
			
		$("#VoiceNumberList").val(voiceNumber + VoiceNumberIndex.toString() + ",");
		$("#inner-txt-box").animate({ scrollTop: $('#inpVoiceNumber')[0].scrollHeight + 100}, 1000);
		if(voiceNumber.length > 0)
		{
			var voice = voiceNumber.split(",");
			if(voice.length >= 2){
				$("#DelVoiceNumber_"+voice[0]).removeClass("delHide").addClass("delShow");
			}
		}		
	}; 
	
	function fnDelVoiceNumber(delPanel, index)
	{
		var voiceNumber = $("#VoiceNumberList").val();
		$("#"+delPanel).remove();
		$("#VoiceNumberList").val(voiceNumber.replace(index,""));
		
		var newVoiceNumber = $("#VoiceNumberList").val();
		if(newVoiceNumber.length > 0)
		{
			var voice = newVoiceNumber.split(",");
			
			
			<!--- Allow delete to stay - user uses this to opt out --->
			<!---if(voice.length == 2){
				$("#DelVoiceNumber_"+voice[0]).removeClass("delShow").addClass("delHide");
			}--->
		}
	}
	
	function GetVoiceNumberObject()
	{
		var voiceNumber = $("#VoiceNumberList").val();
		var ListVoiceNumber = voiceNumber.split(",");
		var VoiceNumberArray = [];
		for (var i=0; i < ListVoiceNumber.length; i++)
		{
			var index = ListVoiceNumber[i];
			if(index!="" && parseInt(index) > 0)
			{
				var voiceText1 = $("#voiceText1_"+index).val().trim();
				var voiceText2 = $("#voiceText2_"+index).val().trim();
				var voiceText3 = $("#voiceText3_"+index).val().trim();
				var voiceContact = voiceText1 + voiceText2 + voiceText3;
				VoiceNumberArray.push(voiceContact);
			} 
		}
		return  VoiceNumberArray;
	}
	
	//end add or del Voice number contact
	
	
	//start add or del SMS number contact
	var  SMSNumberIndex = <cfoutput>#maxSms#</cfoutput>;
	function fnAddSMSNumber() {
		SMSNumberIndex++;
		var row = "<div id='SMSNumnerPanel"+SMSNumberIndex.toString()+"'>"+
			"<input type=\"text\" name='smsText1_"+SMSNumberIndex.toString()+"' id='smsText1_"+SMSNumberIndex.toString()+"' class=\"portaladd-input-box1\" size=\"4\" value=\"\">"+
			"<input type=\"text\" name='smsText2_"+SMSNumberIndex.toString()+"' id='smsText2_"+SMSNumberIndex.toString()+"' class=\"portaladd-input-box1\" size=\"4\" value=\"\">"+
			"<input type=\"text\" name='smsText3_"+SMSNumberIndex.toString()+"' id='smsText3_"+SMSNumberIndex.toString()+"' class=\"portaladd-input-box2\" size=\"4\" value=\"\">"+
			"<a style=\"float:left;\" name='DelSMSNumber_"+SMSNumberIndex.toString()+"' id='DelSMSNumber_"+SMSNumberIndex.toString()+"' href=\"javascript:fnDelSMSNumber('SMSNumnerPanel"+SMSNumberIndex.toString()+"','"+SMSNumberIndex.toString()+",');\" data-role=\"button\" data-icon=\"delete\" data-iconpos=\"notext\">Delete SMS number</a>"+
		"</div><div style=\"clear:both;\"></div>";
		
		$("#inpSMSNumber").append(row);
		var SMSNumber = $("#SMSNumberList").val();
		$("#SMSNumberList").val(SMSNumber + SMSNumberIndex.toString() + ",");
		$("#inner-txt-box").animate({ scrollTop: $('#inpVoiceNumber')[0].scrollHeight + $('#inpSMSNumber')[0].scrollHeight}, 1000);
		if(SMSNumber.length > 0)
		{
			var sms = SMSNumber.split(",");
			if(sms.length >= 2){
				$("#DelSMSNumber_"+sms[0]).removeClass("delHide").addClass("delShow");
			}
		}
		$("#smsText1_" + SMSNumberIndex.toString()).textinput();
		$("#smsText2_" + SMSNumberIndex.toString()).textinput();
		$("#smsText3_" + SMSNumberIndex.toString()).textinput();
		$("#DelSMSNumber_" + SMSNumberIndex.toString()).buttonMarkup({ iconpos: "notext"});
	}; 
	
	function fnDelSMSNumber(delPanel, index)
	{
		$("#"+delPanel).remove();
		var SMSNumber = $("#SMSNumberList").val();
		$("#SMSNumberList").val(SMSNumber.replace(index,""));
		
		var newSMSNumber = $("#SMSNumberList").val();
		if(newSMSNumber.length > 0)
		{
			var sms = newSMSNumber.split(",");
			if(sms.length == 2){
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelSMSNumber_"+sms[0]).removeClass("delShow").addClass("delHide");--->
			}
		}
	}
	
	function GetSMSNumberObject()
	{
		var smsNumber = $("#SMSNumberList").val();
		var ListSMSNumber = smsNumber.split(",");
		var SMSNumberArray = [];
		for (var i=0; i < ListSMSNumber.length; i++)
		{
			var index = ListSMSNumber[i];
			if(index!="" && parseInt(index) > 0)
			{
				var smsText1 = $("#smsText1_"+index).val().trim();
				var smsText2 = $("#smsText2_"+index).val().trim();
				var smsText3 = $("#smsText3_"+index).val().trim();
				var smsContact = smsText1 + smsText2 + smsText3;
				SMSNumberArray.push(smsContact);
			} 
		}
		return SMSNumberArray;
	}
	
	//end add or del SMS number contact
	
	
	//start add or del Email contact
	var  EmailIndex = <cfoutput>#maxEmail#</cfoutput>;
	function fnAddEmail() {		
		EmailIndex++;
		var row = "<div id='EmailPanel"+EmailIndex.toString()+"'>"+
			"<input type=\"text\" name='email_"+EmailIndex.toString()+"' id='email_"+EmailIndex.toString()+"' class=\"portaladd-input-box\" value=\"\">"+
			"<a style=\"float:left;\" name='DelEmail_"+EmailIndex.toString()+"' id='DelEmail_"+EmailIndex.toString()+"' href=\"javascript:fnDelEmail('EmailPanel"+EmailIndex.toString()+"','"+EmailIndex.toString()+",');\" data-role=\"button\" data-icon=\"delete\" data-iconpos=\"notext\">Delete Email contact</a>"+
			"</div><div style=\"clear:both;\"></div>";
		$("#inpEmail").append(row);		
		var Emailnumber = $("#EmailList").val();
		$("#EmailList").val(Emailnumber + EmailIndex.toString() + ",");
		$("#inner-txt-box").animate({ scrollTop: $('#inner-txt-box')[0].scrollHeight}, 1000);
		
		if(Emailnumber.length > 0)
		{
			var email = Emailnumber.split(",");
			if(email.length >= 2){
				$("#DelEmail_"+email[0]).removeClass("delHide").addClass("delShow");
			}
		}
		$("#email_" + EmailIndex.toString()).textinput();
		$("#DelEmail_" + EmailIndex.toString()).buttonMarkup({ iconpos: "notext"});
	}; 
	
	function fnDelEmail(delPanel, index)
	{
		$("#"+delPanel).remove();
		$("#EmailList").val($("#EmailList").val().replace(index,""));
		
		var newEmailNumber = $("#EmailList").val();
		if(newEmailNumber.length > 0)
		{
			var email = newEmailNumber.split(",");			
			if(email.length == 2){
				<!--- Allow delete to stay - user uses this to opt out --->
				<!---$("#DelEmail_"+email[0]).removeClass("delShow").addClass("delHide");--->
			}
		}
	}
	
	function GetEmailObject()
	{
		var Email = $("#EmailList").val();
		var ListEmail = Email.split(",");
		var ListEmailArray = [];
		for (var i=0; i < ListEmail.length; i++)
		{
			var index = ListEmail[i];
			if(index!="" && parseInt(index) > 0)
			{
				var email = $("#email_"+index).val().trim();
				ListEmailArray.push(email);
			} 
		}
		return  ListEmailArray;
	}
	//end add or del Email contact
</script>

