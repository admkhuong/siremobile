<cfif templateId EQ ''>
	<cfset templateId = 0>
</cfif>

<cfquery name="GetCppTemp" datasource="#Session.DBSourceEBM#">
	SELECT 
		tempalteId_int,
		title_vch,
		description_vch,
		XML_txt,
		image_vch
	FROM 
		simplelists.cpptemplate 
	WHERE
		tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TemplateId#">
</cfquery> 

<cfif GetCppTemp.tempalteId_int GT 0>
	<cfset tempStrXML = xmlParse(GetCppTemp.XML_TXT)>
	<cfset tempStr = tempStrXML.CppTemplate>
<cfelse>
	<cfset tempStr = {
		preference = {
			title = {font ='Arial, Helvetica, sans-serif', size = '25px', color = '##000000'},	
			description = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'},
			content = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'}
		},
		contact = {
			title = {font ='Arial, Helvetica, sans-serif', size = '25px', color = '##000000'},	
			description = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'},
			label = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'}
		},
		language = {
			title = {font ='Arial, Helvetica, sans-serif', size = '25px', color = '##000000'},	
			description = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'},
			label = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'},
			content = {font ='Arial, Helvetica, sans-serif', size = '16px', color = '##000000'}
		}
	}>
</cfif>

<style>
	<cfoutput>
		<cfif templateId  EQ 0>
			##preTitle{
				font-size: #tempStr.preference.title.size#;
				color: #tempStr.preference.title.color#;
				font-family: #tempStr.preference.title.font#;
			}
			##preDesc{
				font-size: #tempStr.preference.description.size#;
				color: #tempStr.preference.description.color#;
				font-family: #tempStr.preference.description.font#;
			}
			##preContent{
				font-size: #tempStr.preference.description.size#;
				color: #tempStr.preference.description.color#;
				font-family: #tempStr.preference.description.font#;
			}
			
			##contactTitle{
				font-size: #tempStr.contact.title.size#;
				color: #tempStr.contact.title.color#;
				font-family: #tempStr.contact.title.font#;
			}
			##contactDesc{
				font-size: #tempStr.contact.description.size#;
				color: #tempStr.contact.description.color#;
				font-family: #tempStr.contact.description.font#;
			}
			##contactLabel{
				font-size: #tempStr.contact.label.size#;
				color: #tempStr.contact.label.color#;
				font-family: #tempStr.contact.label.font#;
			}
			
			##langTitle{
				font-size: #tempStr.language.title.size#;
				color: #tempStr.language.title.color#;
				font-family: #tempStr.language.title.font#;
			}
			##langDesc{
				font-size: #tempStr.language.description.size#;
				color: #tempStr.language.description.color#;
				font-family: #tempStr.language.description.font#;
			}
			##langLabel{
				font-size: #tempStr.language.label.size#;
				color: #tempStr.language.label.color#;
				font-family: #tempStr.language.label.font#;
			}
			##langContent{
				font-size: #tempStr.language.content.size#;
				color: #tempStr.language.content.color#;
				font-family: #tempStr.language.content.font#;
			}
		<cfelse>
			##preTitle{
				font-size: #tempStr.preference.title.size.XmlText#;
				color: #tempStr.preference.title.color.XmlText#;
				font-family: #tempStr.preference.title.font.XmlText#;
			}
			##preDesc{
				font-size: #tempStr.preference.description.size.XmlText#;
				color: #tempStr.preference.description.color.XmlText#;
				font-family: #tempStr.preference.description.font.XmlText#;
			}
			##preContent{
				font-size: #tempStr.preference.description.size.XmlText#;
				color: #tempStr.preference.description.color.XmlText#;
				font-family: #tempStr.preference.description.font.XmlText#;
			}
			
			##contactTitle{
				font-size: #tempStr.contact.title.size.XmlText#;
				color: #tempStr.contact.title.color.XmlText#;
				font-family: #tempStr.contact.title.font.XmlText#;
			}
			##contactDesc{
				font-size: #tempStr.contact.description.size.XmlText#;
				color: #tempStr.contact.description.color.XmlText#;
				font-family: #tempStr.contact.description.font.XmlText#;
			}
			##contactLabel{
				font-size: #tempStr.contact.label.size.XmlText#;
				color: #tempStr.contact.label.color.XmlText#;
				font-family: #tempStr.contact.label.font.XmlText#;
			}
			
			##langTitle{
				font-size: #tempStr.language.title.size.XmlText#;
				color: #tempStr.language.title.color.XmlText#;
				font-family: #tempStr.language.title.font.XmlText#;
			}
			##langDesc{
				font-size: #tempStr.language.description.size.XmlText#;
				color: #tempStr.language.description.color.XmlText#;
				font-family: #tempStr.language.description.font.XmlText#;
			}
			##langLabel{
				font-size: #tempStr.language.label.size.XmlText#;
				color: #tempStr.language.label.color.XmlText#;
				font-family: #tempStr.language.label.font.XmlText#;
			}
			##langContent{
				font-size: #tempStr.language.content.size.XmlText#;
				color: #tempStr.language.content.color.XmlText#;
				font-family: #tempStr.language.content.font.XmlText#;
			}
		</cfif>
		
	</cfoutput>
</style>