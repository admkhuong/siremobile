<cfcomponent>
	<cffunction name="ConvertMBLoxTime" output="true" access="public" >
		<cfargument name="inpTime" TYPE="string" required="yes" default=""/>
		
        <cfset var year	= '' />
		<cfset var month	= '' />
        <cfset var day	= '' />
        <cfset var hour	= '' />
        <cfset var minute	= '' />

		<!---convert mblox time format YYYYMMDDHHmm to timestamp---> 
		<cfset year = mid(inpTime,1,4)>
		<cfset month = mid(inpTime,5,2)>
		<cfset day = mid(inpTime,7,2)>
		<cfset hour = mid(inpTime,9,2)>
		<cfset minute = mid(inpTime,11,2)>
		<cfreturn '#year#-#month#-#day# #hour#:#minute#'>
	</cffunction>
</cfcomponent>