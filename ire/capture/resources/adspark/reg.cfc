<cfcomponent extends="taffy.core.resource" taffy:uri="/adspark/regsarisari" hint="Add a new registration record for Adspark Sari Sari campaign">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Event Program"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpRegKeySS" required="yes" hint="BRAND_<SSSO Name>_<Store Name>_<Store Location>_<Retailer>_<Retailer Location>">
        		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var GetExistingCode = '' />
		<cfset var InsertToRegSariSari = '' />		
		<cfset var InsertToErrorLog = '' />
		<cfset var DebugAPI = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
		<cfset var insertResult = '' />
		
		<cfset dataOut.Brand_vch = '' />
		<cfset dataOut.SSSOName_vch = '' />
		<cfset dataOut.SSSOStoreName_vch = '' />
		<cfset dataOut.StoreLocation_vch = '' />
		<cfset dataOut.Retailer_vch = '' />
		<cfset dataOut.RetailerLocation_vch = '' />
		<cfset dataOut.RawData_vch = '' />
		<cfset dataOut.Notes_vch = '' />		
		<cfset dataOut.ErrMessage = '' />
		<cfset dataOut.PKId = '0' />
		<cfset dataOut.KeyCode = '0' />
	  
		<cftry>
        
        	<!--- 
	        	SSSO will send SMS to 2338 (with the following SYNTAX keywords)
				BRAND_<SSSO Name>_<Store Name>_<Store Location>_<Retailer>_<Retailer Location>
				SSSO received auto system generated SMS REPLY
				Thank you for registration, you're now part of TEAM. Your store unique identifier is K80806.
			--->	
        
			<cfset dataOut.RawData_vch = inpRegKeySS />
        
			<!--- Validate list length is as expected --->
			<cfif ListLen(inpRegKeySS, '_', true) EQ 6 >
				
				<cfset dataOut.Brand_vch = ListGetAt(inpRegKeySS, 1, '_', true) />
				<cfset dataOut.SSSOName_vch = ListGetAt(inpRegKeySS, 2, '_', true) />
				<cfset dataOut.SSSOStoreName_vch = ListGetAt(inpRegKeySS, 3, '_', true) />
				<cfset dataOut.StoreLocation_vch = ListGetAt(inpRegKeySS, 4, '_', true) />
				<cfset dataOut.Retailer_vch = ListGetAt(inpRegKeySS, 5, '_', true) />
				<cfset dataOut.RetailerLocation_vch = ListGetAt(inpRegKeySS, 6, '_', true) />				
				<cfset dataOut.Notes_vch = 'API Inserted' />
				<cfset dataOut.ErrMessage = '' />
				
			<cfelse>
			
				<cfset dataOut.Brand_vch = '' />
				<cfset dataOut.SSSOName_vch = '' />
				<cfset dataOut.SSSOStoreName_vch = '' />
				<cfset dataOut.StoreLocation_vch = '' />
				<cfset dataOut.Retailer_vch = '' />
				<cfset dataOut.RetailerLocation_vch = '' />
				<cfset dataOut.Notes_vch = '' />
		
				<cfset dataOut.ErrMessage = 'Not in proper format. (#ListLen(inpRegKeySS, '_', true)#)' />
			
			</cfif>	
			
			<!--- Look for existing?  Batch ContactString Brand for now --->
			<!--- Check for existing key code --->                   
            <cfquery name="GetExistingCode" datasource="#DBSourceEBM#">                                                                                                   
                 SELECT                        	
                    PKId_int					
                 FROM
                    simplexprogramdata.adspark_reg_sari_sari
                 WHERE
                 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
                 AND
                 	ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                 AND
                 	Brand_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.Brand_vch), 255)#">	
            </cfquery>
            
            
            <!--- Only if record exists AND Brand is found --->
            <cfif GetExistingCode.RecordCount GT 0 AND LEN(LEFT(TRIM(dataOut.Brand_vch), 255)) GT 0>
	            
	            <!--- Get existing key  --->
	            <cfif GetExistingCode.PKId_int GT 0>
		        	<cfset dataout.PKId = GetExistingCode.PKId_int />
		        	<cfset dataout.KeyCode = 'K808#GetExistingCode.PKId_int#' />
		        	<cfset dataOut.ErrMessage = 'Existing Key Found.' />
		        <cfelse>
		        	<cfset dataout.PKId = 0>	
		        	<cfset dataout.KeyCode = '0' />
		        	<cfset dataOut.ErrMessage = 'Duplicate Key Error.' />
		        </cfif>
		        
	        <cfelse>    
           
	            <!--- Add a status update for today --->                          
	            <cfquery name="InsertToRegSariSari" datasource="#DBSourceEBM#" result="insertResult" >              
	              
	              INSERT INTO 
	              	simplexprogramdata.adspark_reg_sari_sari 
	                (	
	                	PKId_int,
						BatchId_bi,
						ContactString_vch,
						Brand_vch,
						SSSOName_vch,
						SSSOStoreName_vch,
						StoreLocation_vch,
						Retailer_vch,
						RetailerLocation_vch,
						RawData_vch,
						Notes_vch,
						Created_dt,
						Updated_dt
	                )
	                VALUES
	                (
	                	NULL,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
	                	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.Brand_vch), 255)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.SSSOName_vch), 255)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.SSSOStoreName_vch), 255)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.StoreLocation_vch), 255)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.Retailer_vch), 255)#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(dataOut.RetailerLocation_vch), 255)#">,
					    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpRegKeySS)#">,
					    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataOut.Notes_vch)#">,
	                    NOW(),
	                    NOW()                     
	                 )             
	            </cfquery>           
	            
	            
	            <!--- Get inserted key  --->
	            <cfif insertResult.GENERATED_KEY GT 0>
		        	<cfset dataout.PKId = insertResult.GENERATED_KEY />
		        	<cfset dataout.KeyCode = 'K808#insertResult.GENERATED_KEY#' />
		        <cfelse>
		        	<cfset dataout.PKId = 0>	
		        	<cfset dataout.KeyCode = '0' />
		        </cfif>
	        
            </cfif>
            		   	                                   
        <cfcatch>        	
           
            <cfset dataOut = {}>
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
                              
                <cftry>        
                
					<!---                 	<cfset xmlDataString = URLDecode(XMLDATA)> --->
					<cfset ENA_Message = " inpContactString=(#inpContactString#) inpBatchId=(#inpBatchId#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
   
</cfcomponent>