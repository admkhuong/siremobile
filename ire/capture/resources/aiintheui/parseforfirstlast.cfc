<cfcomponent extends="taffy.core.resource" taffy:uri="/parse/fullname" hint="Parse full name for first and last name">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Validate email is not already in use">
 		<cfargument name="inpFullName" type="string" required="false" default="" hint="Full name to parse"/>
 		<cfargument name="DebugAPI" required="no" default="0">
 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset dataOut.FIRST = 'NEW' />
        <cfset dataOut.MIDDLE = '' />
        <cfset dataOut.LAST = 'USER' />
        <cfset dataOut.SALUTATION = '' />
        <cfset dataOut.SUFFIX = '' />
        <cfset dataOut.FULL =  TRIM(arguments.inpFullName) />
                
 		<cfset var DebugStr = "" />
                        
        <!--- Set the default Session DB Source --->
        <cfset Session.DBSourceEBM = "Bishop"/>
        <cfset Session.COMPANYUSERID = "Session.UserId"/>
        
        <!--- Based (roughly) on https://github.com/chovy/humanparser/blob/master/index.js added my own touches, may not be perfect but close enough for now --->
                			                   
 		<cftry>
 		
 			<cfset arguments.inpFullName = TRIM(arguments.inpFullName) />
 		 	
 		 	
 		 	<cfif ListLen(arguments.inpFullName, " ", false) GT 0 >	
 			
		 		<cfset var salutations = "'mr', 'master', 'mister', 'mrs', 'miss', 'ms', 'dr', 'prof', 'rev', 'fr', 'judge', 'honorable', 'hon', 'tuan', 'sr', 'srta', 'br', 'pr', 'mx', 'sra'" />
				<cfset var suffixes = "'i', 'ii', 'iii', 'iv', 'v', 'senior', 'junior', 'jr', 'sr', 'phd', 'apr', 'rph', 'pe', 'md', 'ma', 'dmd', 'cme'" />
				<cfset var compound = "'vere', 'von', 'van', 'de', 'del', 'della', 'der', 'di', 'da', 'pietro', 'vanden', 'du', 'st.', 'st', 'la', 'lo', 'ter', 'bin', 'ibn', 'te', 'ten', 'op', 'ben'" />
	
				<!--- Look for suffix and then remove it from the list -- auto scrub for period before trying to match to suffix list--->
				<cfif ListLen(arguments.inpFullName, " ", false) GT 1 AND ListContainsNoCase(suffixes, Replace( ListLast(arguments.inpFullName, " ", false), ".", "", "all" ), ",") GT 0 >
		
					<cfset dataOut.SUFFIX = ListLast(arguments.inpFullName, " ", false)  />
						
					<cfset arguments.inpFullName = listDeleteAt(arguments.inpFullName,  ListLen(arguments.inpFullName, " ", false), " ", false) />
					
				</cfif>
							
				<!--- Look for salutations and then remove it from the list -- auto scrub for period before trying to match to salutations list--->
				<cfif ListLen(arguments.inpFullName, " ", false) GT 1 AND ListContainsNoCase(salutations, Replace( ListGetAt(arguments.inpFullName, 1, " ", false), ".", "", "all" ), ",") GT 0 >
		
					<cfset dataOut.SALUTATION = ListGetAt(arguments.inpFullName, 1, " ", false)  />
						
					<cfset arguments.inpFullName = listDeleteAt(arguments.inpFullName, 1, " ", false) />
					
				</cfif>								
	
				<!--- Only one name provided --->
	 			<cfif ListLen(arguments.inpFullName, " ", false) EQ 1 >
		 			
		 			<cfset dataOut.FIRST = ListGetAt(arguments.inpFullName, 1, " ", false) />
		 			 			
	 			<!--- simple case only two values --->
	 			<cfelseif ListLen(arguments.inpFullName, " ", false) EQ 2 >
			 		
			 		<!--- Look for comma - assume last, first if comma found --->
			 		<cfif Find(",", arguments.inpFullName) GT 0> 
			 			<cfset dataOut.LAST = Replace(ListGetAt(arguments.inpFullName, 1, " ", false), ",", "", "all") />
				 		<cfset dataOut.FIRST = ListLast(arguments.inpFullName, " ", false) />
			 		<cfelse>		 		
				 		<cfset dataOut.FIRST = ListGetAt(arguments.inpFullName, 1, " ", false) />
				 		<cfset dataOut.LAST = ListLast(arguments.inpFullName, " ", false) />
			 		</cfif>
			 		
			 	<cfelseif ListLen(arguments.inpFullName, " ", false) EQ 3>
			 					 					 		
			 		<cfif Find(",", arguments.inpFullName) GT 0>				 		
				 		
				 		<cfset dataOut.LAST = ListGetAt(arguments.inpFullName, 1, " ", false) />
			 		
				 		<!--- Look for compound middle names --->
				 		<cfif ListContainsNoCase(compound, Replace( ListGetAt(arguments.inpFullName, 2, " ", false), ".", "", "all" ), ",") GT 0 OR ListContainsNoCase(compound, Replace( ListGetAt(arguments.inpFullName, 1, " ", false), ".", "", "all" ), ",") GT 0>
				 			
				 			<cfset dataOut.LAST = ListGetAt(arguments.inpFullName, 1, " ", false) & " " & ListGetAt(arguments.inpFullName, 2, " ", false)  />		
				 			<cfset dataOut.FIRST = ListLast(arguments.inpFullName, " ", false) />
				 			
				 		<cfelse>
				 			<cfset dataOut.MIDDLE = ListGetAt(arguments.inpFullName, 2, " ", false) />
				 			<cfset dataOut.FIRST = ListLast(arguments.inpFullName, " ", false) />		 			
				 		</cfif>
				 						 		
				 		<cfset dataOut.LAST = Replace(dataOut.LAST, ",", "", "all") />
				 		
				 	<cfelse>
				 	
				 		<cfset dataOut.FIRST = ListGetAt(arguments.inpFullName, 1, " ", false) />
			 		
				 		<!--- Look for compound middle names --->
				 		<cfif ListContainsNoCase(compound, Replace( ListGetAt(arguments.inpFullName, 2, " ", false), ".", "", "all" ), ",") GT 0>
				 			
				 			<cfset dataOut.LAST = ListGetAt(arguments.inpFullName, 2, " ", false) & " " & ListLast(arguments.inpFullName, " ", false) />		
				 			
				 		<cfelse>
				 			<cfset dataOut.MIDDLE = ListGetAt(arguments.inpFullName, 2, " ", false) />
				 			<cfset dataOut.LAST = ListLast(arguments.inpFullName, " ", false) />		 			
				 		</cfif>
				 		
			 		</cfif>	 
			 		
			 	
			 	<cfelseif ListLen(arguments.inpFullName, " ", false) GT 3>
			 	
				 	<cfset dataOut.FIRST = ListGetAt(arguments.inpFullName, 1, " ", false) />
			 				 		
			 		<cfset arguments.inpFullName = listDeleteAt(arguments.inpFullName, 1, " ", false) />
			 		
			 		<!--- Last name is just everything else --->
			 		<cfset dataOut.LAST = arguments.inpFullName />
			 			 			 			 		
		 		</cfif>		 
		 		
		 		<!--- Format - uppercase first letter no matter wat --->
		 		<cfset dataOut.LAST = ReReplace(LCASE(dataOut.LAST),"\b(\w)","\u\1","ALL") />
		 		<cfset dataOut.FIRST = ReReplace(LCASE(dataOut.FIRST),"\b(\w)","\u\1","ALL") />
		 		<cfset dataOut.MIDDLE = ReReplace(LCASE(dataOut.MIDDLE),"\b(\w)","\u\1","ALL") />
		 		<cfset dataOut.SALUTATION = ReReplace(dataOut.SALUTATION,"\b(\w)","\u\1","ALL") />
		 		<cfset dataOut.SUFFIX = ReReplace(dataOut.SUFFIX,"\b(\w)","\u\1","ALL") />
		 		
		 		<cfset dataout.RXRESULTCODE = 1 />
		 			
 			<cfelse>
 			
	 			<cfset dataout.RXRESULTCODE = -2 />
	 			<cfset dataout.MESSAGE = "Nothing to parse!" />
	 			 			
 			</cfif>
 		 	    
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />

    </cffunction>
   
</cfcomponent>