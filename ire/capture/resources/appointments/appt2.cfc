<cfcomponent extends="taffy.core.resource" taffy:uri="/appointments/confirm" hint="Confirm Appointment">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
    <cfinclude template="../../../../session/cfc/appointment/inc_appointment.cfm">
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://ebmii.messagebroadcast.com/ire/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="XMLDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
 
    <cffunction name="post" access="public" output="false" hint="Get response from customer for appointment">
		<cfargument name="inpBatchIdAppt" TYPE="string" required="yes"/>
        <cfargument name="inpContactStringAppt" required="yes" hint="Contact string">
        <cfargument name="inpContactTypeIdAppt" TYPE="string" required="yes" default="0" hint="Contact Type - 0=Unknown, 1=Voice, 2=eMail, 3=SMS "/>
        <cfargument name="inpStart" required="yes" type="string" hint="Appointment Date and Time">
        <cfargument name="inpConfirm" TYPE="string" required="yes" default="0" hint="0=No, 1=Yes, 2=Maybe, 3=Need Reschedule, 4=Running Late"/>
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so includes have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>

		<cftry>
           			  
            <!--- For debugging only - comment out in production --->  
          	<!---
				<cfmail to="jpeterson@messagebroadcast.com;" from="it@messagebroadcast.com" subject="AT&T MO Received" type="html">
					<cfdump var="#XMLDATA#">
					<cfdump var="#ATTData#">               
				</cfmail> 
			--->
           	<!--- <cfreturn representationOf(dataout).withStatus(200) />--->


 			<cfinvoke                    
                 method="EditConfirmation"
                 returnvariable="RetValEditConfirmation">                         
                    <cfinvokeargument name="inpBatchIdAppt" value="#inpBatchIdAppt#"/>
                    <cfinvokeargument name="inpContactStringAppt" value="#inpContactStringAppt#"/>
                    <cfinvokeargument name="inpContactTypeIdAppt" value="#inpContactTypeIdAppt#"/>
                    <cfinvokeargument name="inpStart" value="#inpStart#"/>
                    <cfinvokeargument name="inpConfirm" value="#inpConfirm#"/>                    
            </cfinvoke>  
            

			<cfif RetValEditConfirmation.RXRESULTCODE EQ 1>
            	
				<cfset dataOut.RXRESULTCODE = 1 />
				<cfset dataOut.TYPE = "" />
                <cfset dataOut.MESSAGE = "" />
            
            <cfelse>    
            
	            <cfset dataOut.RXRESULTCODE = 1 />
				<cfset dataOut.TYPE = "#RetValEditConfirmation.TYPE#" />
                <cfset dataOut.MESSAGE = "#RetValEditConfirmation.MESSAGE#" />
                
            </cfif>
                                    
        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
                        
                <cftry>        
                
                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "inpConfirm = (#inpConfirm#)  inpContactString=(#inpContactStringAppt#) inpBatchId=(#inpBatchIdAppt#) inpContactTypeIdAppt=(#inpContactTypeIdAppt#) inpQAMode=(#inpQAMode#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "#inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>