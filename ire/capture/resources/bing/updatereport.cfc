<cfcomponent extends="taffy.core.resource" taffy:uri="/bing/updatereport" hint="update bing report">
	<cfinclude template="../../paths.cfm" >
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->

	<cffunction name="post" access="public" output="false" hint="Post Protocol">
		<cfargument name="bdata" type="string" required="false" hint="Bing report data"/>

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var bingReport_arr = [] />
		<cfset var UpdateBingReport = '' />
		<cfset var itm = '' />
		<cfset var i = 0 />
		<cfset var reportCount = 0 />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var InsertToErrorLog = '' />

		<cftry>

			<cfif isJSON(arguments.bdata)>
				<cfset bingReport_arr = deserializeJSON(arguments.bdata) />
				<cfif isArray(bingReport_arr) AND arrayLen(bingReport_arr) GT 0>
					<cfset reportCount = arrayLen(bingReport_arr) />
	                <cfquery name="UpdateBingReport" datasource="#DBSourceEBM#">
	                    REPLACE INTO 
	                    	simplelists.bingreport
	                    	(
								GregorianDate,
								AccountId,
								AccountName,
								CampaignId,
								CampaignName,
								Clicks,
								Impressions,
								Ctr,
								AverageCpc,
								Spend
	                    	)
                    	VALUES 
                    		<cfloop array="#bingReport_arr#" item="itm">
                    			<cfset itm.Ctr = REReplace(itm.Ctr, '[^\d\.]', '', 'all') />
                    			<cfif trim(itm.Ctr) EQ ''>
                    				<cfset itm.Ctr = 0 />
                    			</cfif>
                    		(
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#itm.GregorianDate#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itm.AccountId#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itm.AccountName#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itm.CampaignId#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itm.CampaignName#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itm.Clicks#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itm.Impressions#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#itm.Ctr#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#itm.AverageCpc#">, 
                    			<CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#itm.Spend#">
                    		)<cfif ++i LT reportCount>,</cfif>
                    		</cfloop>
                    </cfquery>
                    <cfset dataout.RXRESULTCODE = 1 />
				</cfif>
			</cfif>


			<cfcatch>
				<!--- For debbugging only! --->
				<!--- <cfset dataout = SerializeJSON(cfcatch) /> --->
				<cftry>

					<cfset ENA_Message = "">
					<cfset SubjectLine = "Update Bing Report">
					<cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
					<cfset ErrorNumber="1111">

					<cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
						INSERT INTO simplequeue.errorlogs
						(
							ErrorNumber_int,
							Created_dt,
							Subject_vch,
							Message_vch,
							TroubleShootingTips_vch,
							CatchType_vch,
							CatchMessage_vch,
							CatchDetail_vch,
							Host_vch,
							Referer_vch,
							UserAgent_vch,
							Path_vch,
							QueryString_vch
							)
						VALUES
						(
							#ErrorNumber#,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
							)
					</cfquery>

					<cfcatch type="any">
					</cfcatch>

				</cftry>

				<cfset dataout = SerializeJSON(cfcatch)>

				<cfreturn representationOf(dataout).withStatus(200) />
			</cfcatch>

		</cftry>

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
</cfcomponent>