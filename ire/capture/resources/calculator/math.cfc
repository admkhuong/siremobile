<cfcomponent extends="taffy.core.resource" taffy:uri="/calculator/basic" hint="Evaluates the mathmatical expression passed in + * / MOD %">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Evaluate equation - Returns the result">
		<cfargument name="inpEquation" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Event Program"/>
               		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
                
        <!--- Formatted Date Time --->
        <cfset dataOut = "ERROR" />
                      
      
		<cftry>
           
        
        	 <cfset dataOut = Evaluate(inpEquation) />
           
            		   	                                   
        <cfcatch>        	
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
    
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = {} />  
        
        <cfset dataout.MESSAGE = "You should use the verb POST JSON or XML to this API location. Currently you only using GET!" />
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>