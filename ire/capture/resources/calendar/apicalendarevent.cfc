<cfcomponent extends="taffy.core.resource" taffy:uri="/calendar/eventapi" hint="api sync event on the calender">
	<cfinclude template="../../paths.cfm" >
    <cfinclude template="/public/sire/configs/paths.cfm">    
    <cfinclude template="../../../../session/cfc/csc/constants.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">

	<cffunction name="post" access="public" output="false" hint="api sync an event on the calender">
        <cfargument name="inpUserId" type="string" required="true" hint="The UserID of the Calendar system"/>
        <cfargument name="inptype" type="string" required="true" hint="add: create new events, update: update events, delete: delete events, getall: get all transaction, gettrans: get transaction by Id"/>
 		<cfargument name="inpEventId" type="string" hint="The Id of the event (required with type: gettrans or delete, update)"/>
        <cfargument name="inpEventName" type="string" hint="The Name of the event"/>
        <cfargument name="inpPhonenumber" type="string" hint="The Phone number of the event (required with type: add)"/>
        <cfargument name="inpEventStartDate" type="string" hint="The Start date of the event format: yyyy-mm-dd HH:mm:ss (required with type: add)"/>
        <cfargument name="inpEventEndDate" type="string" hint="The End date of the event format: yyyy-mm-dd HH:mm:ss (required with type: add)"/>
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset var data = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var GetUserConfigInfor = '' />
        <cfset var AddEventQuery = ''/>
        <cfset var insertResult = ''/>

        <cfset var getEventInfor = ''/>
        <cfset var UpdateEventtoDeleteQuery = ''/>
        <cfset var UpdateOutboundQueue = ''/>
        <cfset var CloseSessionReminder = ''/>

        <cfset var MatchedStartDate = ''/> 
        <cfset var MatchedEndDate = ''/>                  
		<cfset var DebugStr = "1" />
        <cfset var valid = 1/>
        
        <!--- Set the default DB Source --->
        <cfset var DBSourceEBM = "Bishop"/>
        
        <cfset ENA_Message = "Error: " >
        <cfset SubjectLine = "API Calendar Subject Line">
        <cfset TroubleShootingTips="API Calendar Trouble">
        <cfset ErrorNumber="9999">
        <cfset AlertType="1">

 		<cftry>
           
        	<!---
					ConfirmationFlag States
					0 - No reminder sent
					1 - Reminder inQueue
					2 - Reminder Accepted
					3 - Reminder Declined
					4 - Reminder Change Request
					5 - Reminder Error - Bad SMS, eMail or other error
					6 - Dont send Reminder	
                    7 - Reminder Sent		          	
			--->
            <!--- Send contact info to User when change status --->
            <cfif inptype EQ "">
                <cfset ENA_Message = "inptype is required.">
            <cfelseif inptype EQ "add"> <!--- create new event --->
                <!--- validate Date --->
                <cfset regex = '[0-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]'>
                <cfset MatchedStartDate = REMatchNoCase(regex, inpEventStartDate)>
                <cfif arrayLen(MatchedStartDate) AND isDate(inpEventStartDate) AND MatchedStartDate[1] EQ inpEventStartDate>
                <cfelse>
                    <cfset ENA_Message = ENA_Message & ", Event start date is invalid date"/>
                    <cfset valid = 0>
                </cfif>
                <cfset MatchedEndDate = REMatchNoCase(regex, inpEventEndDate)>
                <cfif arrayLen(MatchedEndDate) AND isDate(inpEventEndDate) AND MatchedEndDate[1] EQ inpEventEndDate>
                <cfelse>
                    <cfset ENA_Message = ENA_Message & ", Event end date is invalid date"/>
                    <cfset valid = 0>
                </cfif>

                <!--- Check phone number US --->
                <cfset inpPhonenumber = REReplaceNoCase(inpPhonenumber, "[^\d^\*^P^X^##]", "", "ALL")>
                <cfif NOT isvalid('telephone', inpPhonenumber)>
                    <cfset ENA_Message = ENA_Message & ', SMS Number is not a valid telephone number !'> 
                    <cfset valid = 0>
                </cfif>

                <cfquery name="GetUserConfigInfor" datasource="#DBSourceEBM#">
                    SELECT 
                        `configuration`.`PKId_bi`,
                        `configuration`.`CalendarId_int`,
                        `configuration`.`UserId_int`,
                        `configuration`.`PhoneNumberFormat_int`,
                        `configuration`.`Confirmation_BatchId_bi`,
                        `configuration`.`ScrollTime_vch`,
                        `configuration`.`AutoSendReminder_int`,
                        `configuration`.`ReminderIntervalOne_vch`,
                        `configuration`.`DefaultEventTitle_vch`,
                        `configuration`.`ReminderIntervalResend_vch`,
                        `configuration`.`FilterConfirmationFlag_vch`,
                        `configuration`.`NotificationMethod_int`,
                        `configuration`.`NotificationItem_vch`,
                        `configuration`.`Chat_BatchId_bi`
                    FROM 
                        `calendar`.`configuration`
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserId#">                      
                </cfquery>
                <cfif GetUserConfigInfor.RecordCount LT 1>
                    <cfset ENA_Message = ENA_Message & "UserId is invalid."/>
                    <cfset valid = 0>
                </cfif>
                <cfif valid EQ 1>
                    <cfif DateCompare(inpEventEndDate, inpEventStartDate) GT 0>
                        <cfquery name="AddEventQuery" datasource="#DBSourceEBM#" result="insertResult">
                            INSERT INTO 
                                calendar.events
                                (
                                    PKId_bi, 
                                    UserId_int, 
                                    CalendarId_int, 
                                    Title_vch,
                                    AllDayFlag_int,
                                    Start_dt,
                                    End_dt,
                                    URL_vch,
                                    ClassName_vch,
                                    EditableFlag_int,
                                    StartEditableFlag_int,
                                    DurationEditableFlag_int,
                                    ResourceEditableFlag_int,
                                    Rendering_vch,
                                    OverlapFlag_int,
                                    Constraint_vch,
                                    Color_vch,
                                    BackgroundColor_vch,
                                    BorderColor_vch,
                                    TextColor_vch,
                                    eMailAddress_vch,
                                    SMSNumber_vch,
                                    Created_dt, 
                                    Updated_dt, 
                                    Notes_vch,
                                    ConfirmationFlag_int,
                                    CalendarGoogleId_vch,
                                    ConfirmationOneDTSId_int,
                                    Active_int
                                )
                            VALUES
                                (
                                    NULL,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserConfigInfor.PKId_bi#">, <!---inpCalendarId--->
                                    <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpEventName#">, <!---inpTitle --->
                                    0, <!---inpAllDayFlag --->
                                    <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#inpEventStartDate#">, <!---inpStart --->
                                    <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#inpEventEndDate#">, <!---inpEnd --->
                                    NULL, <!---inpURL --->
                                    NULL, <!---inpClassName --->
                                    1, <!---inpEditableFlag --->
                                    1, <!---inpStartEditableFlag --->
                                    1, <!---inpDurationEditableFlag --->
                                    1, <!---inpResourceEditableFlag --->
                                    NULL, <!---inpRendering --->
                                    1, <!---inpOverlapFlag --->
                                    NULL, <!---inpConstraint --->
                                    NULL, <!---inpColor --->
                                    NULL, <!---inpBackgroundColor --->
                                    NULL, <!---inpBorderColor --->
                                    NULL, <!---inpTextColor --->
                                    NULL, <!---inpeMail --->
                                    <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpPhonenumber#">, <!---inpSMSNumber --->
                                    NOW(),
                                    NOW(),
                                    NULL, <!---inpNotes --->
                                    6, <!---inpConfirmationFlag --->
                                    '', <!---GoogleCalendar Id --->
                                    0, <!---inpConfirmationOneDTSId --->
                                    1
                                )                             
                        </cfquery>
                        <cfif insertResult.GENERATED_KEY GT 0>
                            <cfset ENA_Message = "SUSSCESS">
                        </cfif>
                    <cfelse>
                        <cfset ENA_Message = ENA_Message & ", event end date must greater than event start date.">
                    </cfif>
                </cfif>
            <cfelseif inptype EQ "update"><!--- edit event --->
                <cfif inpEventId EQ "">
                    <cfset ENA_Message = "inpEventId is required.">
                <cfelse>
                    <cfquery name="getEventInfor" datasource="#DBSourceEBM#">
                        SELECT 
                            `events`.`PKId_bi`,
                            `events`.`UserId_int`,
                            `events`.`CalendarId_int`,
                            `events`.`Title_vch`,
                            `events`.`AllDayFlag_int`,
                            `events`.`Start_dt`,
                            `events`.`End_dt`,
                            `events`.`URL_vch`,
                            `events`.`ClassName_vch`,
                            `events`.`EditableFlag_int`,
                            `events`.`StartEditableFlag_int`,
                            `events`.`DurationEditableFlag_int`,
                            `events`.`ResourceEditableFlag_int`,
                            `events`.`Rendering_vch`,
                            `events`.`OverlapFlag_int`,
                            `events`.`Active_int`,
                            `events`.`Created_dt`,
                            `events`.`Updated_dt`,
                            `events`.`Constraint_vch`,
                            `events`.`Color_vch`,
                            `events`.`BackgroundColor_vch`,
                            `events`.`BorderColor_vch`,
                            `events`.`TextColor_vch`,
                            `events`.`eMailAddress_vch`,
                            `events`.`SMSNumber_vch`,
                            `events`.`Notes_vch`,
                            `events`.`ConfirmationFlag_int`,
                            `events`.`Confirmation_dt`,
                            `events`.`ConfirmationSessionId_bi`,
                            `events`.`ConfirmationOneDTSId_int`,
                            `events`.`ChatSessionId_bi`,
                            `events`.`CalendarGoogleId_vch`,
                            `events`.`ResendDTSId_int`,
                            `events`.`ChatNotes_vch`
                        FROM `calendar`.`events`
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND 
                            PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEventId#">
                        AND 
                            Active_int = 1
                        AND 
                            ConfirmationOneDTSId_int = 0
                    </cfquery>
                    <cfif getEventInfor.RecordCount LT 1>
                        <cfset ENA_Message = "Event can not update."/>
                    <cfelse>
                        <!--- validate Date --->
                        <cfset regex = '[0-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]'>
                        <cfif inpEventStartDate NEQ "">
                            <cfset MatchedStartDate = REMatchNoCase(regex, inpEventStartDate)>
                            <cfif arrayLen(MatchedStartDate) AND isDate(inpEventStartDate) AND MatchedStartDate[1] EQ inpEventStartDate>
                            <cfelse>
                                <cfset ENA_Message = ENA_Message & ", Event start date is invalid date"/>
                                <cfset valid = 0>
                            </cfif>
                        </cfif>

                        <cfif inpEventEndDate NEQ "">
                            <cfset MatchedEndDate = REMatchNoCase(regex, inpEventEndDate)>
                            <cfif arrayLen(MatchedEndDate) AND isDate(inpEventEndDate) AND MatchedEndDate[1] EQ inpEventEndDate>
                            <cfelse>
                                <cfset ENA_Message = ENA_Message & ", Event end date is invalid date"/>
                                <cfset valid = 0>
                            </cfif>
                        </cfif>

                        <cfif inpPhonenumber NEQ "">
                            <!--- Check phone number US --->
                            <cfset inpPhonenumber = REReplaceNoCase(inpPhonenumber, "[^\d^\*^P^X^##]", "", "ALL")>
                            <cfif NOT isvalid('telephone', inpPhonenumber)>
                                <cfset ENA_Message = ENA_Message & ', SMS Number is not a valid telephone number !'> 
                                <cfset valid = 0>
                            </cfif>
                        </cfif>

                        <cfif valid = 1>
                            <cfquery name="UpdateEventtoDeleteQuery" datasource="#DBSourceEBM#">
                                UPDATE
                                    calendar.events
                                SET
                                    Active_int = 1,
                                    <cfif inpEventName NEQ "">Title_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpEventName#">,</cfif>
                                    <cfif inpPhonenumber NEQ "">SMSNumber_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpPhonenumber#">,</cfif>
                                    <cfif inpEventStartDate NEQ "">Start_dt = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#inpEventStartDate#">,</cfif>
                                    <cfif inpEventEndDate NEQ "">End_dt = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#inpEventEndDate#">,</cfif>
                                    Updated_dt = NOW()
                                WHERE
                                    PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEventId#">
                                AND
                                    UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                     
                            </cfquery>      
                            <cfset ENA_Message = "SUSSCESS">
                        </cfif>
                    </cfif>
                </cfif>
            <cfelseif inptype EQ "delete"><!--- delete event --->
                <cfif inpEventId EQ "">
                    <cfset ENA_Message = "inpEventId is required.">
                <cfelse>
                    <cfquery name="getEventInfor" datasource="#DBSourceEBM#">
                        SELECT 
                            `events`.`PKId_bi`,
                            `events`.`UserId_int`,
                            `events`.`CalendarId_int`,
                            `events`.`Title_vch`,
                            `events`.`AllDayFlag_int`,
                            `events`.`Start_dt`,
                            `events`.`End_dt`,
                            `events`.`URL_vch`,
                            `events`.`ClassName_vch`,
                            `events`.`EditableFlag_int`,
                            `events`.`StartEditableFlag_int`,
                            `events`.`DurationEditableFlag_int`,
                            `events`.`ResourceEditableFlag_int`,
                            `events`.`Rendering_vch`,
                            `events`.`OverlapFlag_int`,
                            `events`.`Active_int`,
                            `events`.`Created_dt`,
                            `events`.`Updated_dt`,
                            `events`.`Constraint_vch`,
                            `events`.`Color_vch`,
                            `events`.`BackgroundColor_vch`,
                            `events`.`BorderColor_vch`,
                            `events`.`TextColor_vch`,
                            `events`.`eMailAddress_vch`,
                            `events`.`SMSNumber_vch`,
                            `events`.`Notes_vch`,
                            `events`.`ConfirmationFlag_int`,
                            `events`.`Confirmation_dt`,
                            `events`.`ConfirmationSessionId_bi`,
                            `events`.`ConfirmationOneDTSId_int`,
                            `events`.`ChatSessionId_bi`,
                            `events`.`CalendarGoogleId_vch`,
                            `events`.`ResendDTSId_int`,
                            `events`.`ChatNotes_vch`
                        FROM `calendar`.`events`
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        AND 
                            PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpEventId#">
                        AND 
                            Active_int = 1
                        AND 
                            ConfirmationOneDTSId_int = 0
                    </cfquery>
                    <cfif getEventInfor.RecordCount LT 1>
                        <cfset ENA_Message = "Event can not delete."/>
                    <cfelse>
                        <cfquery name="UpdateEventtoDeleteQuery" datasource="#DBSourceEBM#">
                            UPDATE
                                calendar.events
                            SET
                                Active_int = 0 
                            WHERE
                                PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEventId#">
                            AND
                                UserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                     
                        </cfquery>      
                        <!---
                            <cfif getEventInfor.ConfirmationOneDTSId_int GT 0 >
                                <!--- Remove any queue reminders --->
                                <cfquery name="UpdateOutboundQueue" datasource="#Session.DBSourceEBM#" >
                                    UPDATE
                                        simplequeue.contactqueue
                                    SET
                                        DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSOUT_RECORD_REMOVED#"/> ,
                                        Scheduled_dt = Scheduled_dt + INTERVAL 1000 YEAR <!--- Set a scheduled date wayyyyyy in the future - this will allow new events to be schedule in this slot. Use 1000 years by convention, so you can still see when an original was scheduled for debugging--->	                                    
                                    WHERE
                                        DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getEventInfor.ConfirmationOneDTSId_int#"/>
                                    AND	
                                        DTSStatusType_ti IN (#SMSOUT_PAUSED#, #SMSOUT_QUEUED#)
                                </cfquery>  
                                <!--- Close session any queue reminders --->
                                <cfquery name="CloseSessionReminder" datasource="#Session.DBSourceEBM#" >
                                    UPDATE
                                        simplequeue.sessionire
                                    SET
                                        SessionState_int = 4
                                    WHERE
                                        DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getEventInfor.ConfirmationOneDTSId_int#"/>
                                </cfquery> 
                            </cfif>
                        --->
                        <cfset ENA_Message = "SUSSCESS">
                    </cfif>
                </cfif>
            <cfelseif inptype EQ "getall"> <!--- get all event by userid --->
            <cfelseif inptype EQ "gettrans"><!--- get event by id --->
            </cfif>    

            <cfset dataout.MESSAGE = ENA_Message /> 
            <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch type="any">
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
    
                <cfset dataout = StructNew() />            
                <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
            
                <cfif DebugAPI GT 0>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
                <cfelse>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
                </cfif>
     
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.ERRMESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
            </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>