<cfcomponent extends="taffy.core.resource" taffy:uri="/calendar/eventconfirmation" hint="Confirm an event on the calender">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="get" access="public" output="false" hint="Confirm an event on the calender">
 		<cfargument name="inpEventId" type="string" required="true" hint="The structure of the event"/>
 		<cfargument name="inpUserId" type="string" required="true" hint="The structure of the event"/>
        <cfargument name="inpConfirmationFlag" type="string" required="true" hint="0 - No reminder sent, 1 - Reminder Sent, 2 - Reminder Accepted, 3 - Reminder Declined, 4 - Reminder Change Request, 5 - Reminder Error - Bad SMS, eMail or other error, 6 - Dont send Reminder"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpSessionId" required="yes" hint="The confirmation message's Session Id">
        <cfargument name="DebugAPI" required="no" default="0">
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var UpdateEventQuery = '' />
                           
		<cfset var DebugStr = "" />
        
        <!--- Set the default DB Source --->
        <cfset var DBSourceEBM = "Bishop"/>
                        
 		<cftry>
 		
 			<cfif !IsNumeric(inpEventId)>
            	<cfthrow message="Invalid Event Specified"/>
            </cfif>
        
        	<!---
					ConfirmationFlag States
					
					0 - No reminder sent
					1 - Reminder Sent
					2 - Reminder Accepted
					3 - Reminder Declined
					4 - Reminder Change Request
					5 - Reminder Error - Bad SMS, eMail or other error
					6 - Dont send Reminder
								          	
			--->
			
        	<cfif !IsNumeric(inpConfirmationFlag) OR inpConfirmationFlag LT 0 OR inpConfirmationFlag GT 6>
            	<cfthrow message="Invalid Confirmation Value Specified"/>
            </cfif>    
             		 	    
			<!--- always get latest entry for operator Id regardless of whose list it is on --->
            <cfquery name="UpdateEventQuery" datasource="#DBSourceEBM#">
               	UPDATE
               		calendar.events
			   	SET
			   		ConfirmationFlag_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfirmationFlag#">,
			   		Confirmation_dt = NOW(),
			   		ConfirmationSessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpSessionId#">,
			   		Updated_dt = NOW()
                WHERE
	            	PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpEventId#">
	           	AND
	           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                     
            </cfquery>       
			
            <cfset dataout.RXRESULTCODE = 1 />
        	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>