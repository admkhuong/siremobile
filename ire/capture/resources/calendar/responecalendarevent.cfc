<cfcomponent extends="taffy.core.resource" taffy:uri="/calendar/eventrespone" hint="Respone event on the calender">
	<cfinclude template="../../paths.cfm" >
    <cfinclude template="/public/sire/configs/paths.cfm">    
    <cfinclude template="../../../../session/cfc/csc/constants.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">

	<cffunction name="post" access="public" output="false" hint="respone an event on the calender">
 		<cfargument name="inpContactString" type="string" required="true" hint="The structure of the event"/>
 		<cfargument name="inpBatchId" type="string" required="true" hint="The structure of the event"/>
        <cfargument name="inpShortCode" type="string" required="true" hint=""/>
        <cfargument name="inpstatus" required="no" default="0">
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset var data = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var UpdateEventQuery = '' />
        <cfset var GetSessionIdEventRespone = ''/>
        <cfset var GetQueueIdEventRespone = ''/>
        <cfset var GetContentEmail = ''/>
        <cfset var GetEventInfor = ''/>
        <cfset var GetListNotificationMethod = ""/>
        <cfset var RetVarSendSingleMT = ""/>
        <cfset var getUserShortCode = ""/>
                           
		<cfset var DebugStr = "1" />
        <cfset var inpConfirmationFlag = 0/>
        <cfset var inpConfirmationFlag_name = ""/>
        <cfset var inpcalendaruseremail = ""/>
        <cfset var listemail = ""/>
        <cfset var listphone = ""/>
        <cfset var phone = ""/>
        <cfset var rxGetQueueIdEventRespone = ''/>
        
        <!--- Set the default DB Source --->
        <cfset var DBSourceEBM = "Bishop"/>
        
        <cfset ENA_Message = #inpContactString# & " - " & #inpBatchId# >
        <cfset SubjectLine = "API Calendar Subject Line - Status: " & #inpstatus#>
        <cfset TroubleShootingTips="API Calendar Trouble">
        <cfset ErrorNumber="9999">
        <cfset AlertType="1">

 		<cftry>
            <cfif inpstatus EQ 0>
                <cfset  inpConfirmationFlag = 4/>
                <cfset inpConfirmationFlag_name = "Reminder Change Request"/>
            <cfelseif inpstatus EQ 1>
                <cfset inpConfirmationFlag = 2/>
                <cfset inpConfirmationFlag_name = "Reminder Accepted"/>
            <cfelseif inpstatus EQ 2>
                <cfset inpConfirmationFlag = 3/>
                <cfset inpConfirmationFlag_name = "Reminder Declined"/>
            <cfelseif inpstatus EQ 3>
                <cfset inpConfirmationFlag = 7/>
                <cfset inpConfirmationFlag_name = "Reminder Sent"/>
            </cfif>
 		
 			<cfif !IsNumeric(inpBatchId)>
            	<cfthrow message="Invalid Event Specified"/>
            </cfif>
        	<!---
					ConfirmationFlag States
					0 - No reminder sent
					1 - Reminder inQueue
					2 - Reminder Accepted
					3 - Reminder Declined
					4 - Reminder Change Request
					5 - Reminder Error - Bad SMS, eMail or other error
					6 - Dont send Reminder	
                    7 - Reminder Sent		          	
			--->	 	    
			<!--- always get latest entry for operator Id regardless of whose list it is on --->
             <cfquery name="GetQueueIdEventRespone" datasource="#DBSourceEBM#" result="rxGetQueueIdEventRespone">
               	Select
                    DTSId_int,
                    DTSStatusType_ti
                FROM
               		simplequeue.contactqueue
                WHERE
	            	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#inpBatchId#">
	           	AND
	           		ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">   
                AND  
                    DTSStatusType_ti = 5  
                Order by 
                    Queue_dt DESC   
                Limit 1                               
            </cfquery>

            <cfset ENA_Message = ENA_Message & serializeJSON(GetQueueIdEventRespone)/>

            <cfif GetQueueIdEventRespone.RecordCount GT 0 AND inpstatus NEQ 0>
                <cfquery name="GetSessionIdEventRespone" datasource="#DBSourceEBM#">
                    Select
                        SessionId_bi, UserId_int
                    FROM
                        simplequeue.sessionire
                    WHERE
                        DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetQueueIdEventRespone.DTSId_int#">                            
                </cfquery>

                <cfset ENA_Message = ENA_Message & serializeJSON(GetSessionIdEventRespone)/>

                <cfquery name="UpdateEventQuery" datasource="#DBSourceEBM#">
                   	UPDATE
                   		calendar.events
    			   	SET
    			   		ConfirmationFlag_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfirmationFlag#">,
    			   		Confirmation_dt = NOW(),
    			   		ConfirmationSessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetSessionIdEventRespone.SessionId_bi#">,
    			   		Updated_dt = NOW()
                    WHERE
    	            	ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetQueueIdEventRespone.DTSId_int#">      
                    AND    
                        ConfirmationFlag_int in (1,7)                   
                </cfquery>  

                <cfset ENA_Message = ENA_Message & serializeJSON(UpdateEventQuery)/>

                <!--- Send contact info to User when change status --->
                <cfquery name="GetContentEmail" datasource="#DBSourceEBM#">
                   	SELECT
                        UserId_int,
                   		CONCAT(FirstName_vch ,' ', LastName_vch) as USERNAME,
                        EmailAddress_vch,
                        MFAContactString_vch
    			   	FROM 
                        simpleobjects.useraccount
                    WHERE
    	            	UserId_int in (SELECT UserId_int FROM calendar.events WHERE ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetQueueIdEventRespone.DTSId_int#">)                        
                </cfquery> 

                <cfquery name="GetEventInfor" datasource="#DBSourceEBM#">
                   	SELECT
                        Title_vch,
                   		SMSNumber_vch,
                        Start_dt,
                        End_dt
                    FROM
                        calendar.events
    			   	WHERE
    	            	ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetQueueIdEventRespone.DTSId_int#">
                </cfquery> 
                <!--- Get config notification --->
                <cfquery name="GetListNotificationMethod" datasource="#DBSourceEBM#">
                   	SELECT
                        NotificationMethod_int,
                        NotificationItem_vch
    			   	FROM 
                        calendar.configuration
                    WHERE
    	            	UserId_int in (SELECT UserId_int FROM calendar.events WHERE ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetQueueIdEventRespone.DTSId_int#">)                        
                </cfquery>
                
                <cfif GetListNotificationMethod.RecordCount GT 0>
                    <cfif GetListNotificationMethod.NotificationMethod_int EQ 0>  <!--- Email notification --->
                        <cfset listemail = GetListNotificationMethod.NotificationItem_vch>
                        <!---
                        <cfif listemail EQ "">
                            <cfset listemail = GetContentEmail.EmailAddress_vch>
                        </cfif>
                        --->
                    <cfelse>                                                      <!--- SMS notification --->
                        <cfset listphone = GetListNotificationMethod.NotificationItem_vch>
                        <!---
                        <cfif listphone EQ "">
                            <cfset listphone = REReplaceNoCase(GetContentEmail.MFAContactString_vch, "[^\d^\*^P^X^##]", "", "ALL")>
                        </cfif>
                        --->
                    </cfif>
                </cfif>

                <cfset USERNAME = #GetContentEmail.USERNAME#/>
                <cfset EVENTNAME = #GetEventInfor.Title_vch#/>
                <cfset EVENTSTART = LSDateFormat(#GetEventInfor.Start_dt#, 'mm/dd/yyyy') & ' ' & LSTimeFormat(#GetEventInfor.Start_dt#, 'hh:mm tt')/>
                <cfset EVENTEND = LSDateFormat(#GetEventInfor.End_dt#, 'mm/dd/yyyy') & ' ' & LSTimeFormat(#GetEventInfor.End_dt#, 'hh:mm tt')/>
                <cfset CRRSTATUSNAME = #inpConfirmationFlag_name#/>
                <cfset PHONENUMBER = #GetEventInfor.SMSNumber_vch#/>
                        
                <!---  <cfif GetContentEmail.RecordCount GT 0>  --->
                <cfif GetListNotificationMethod.NotificationMethod_int EQ 0>
                    <cfif listemail NEQ "">
                
                        <cfset listemail = replace(listemail,"#chr(44)#",";","ALL")>
                        <cfmail to="#listemail#" subject="Notify Calendar Event Change Status" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>
                                <cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">
                                <html>
                                    <head>
                                        <meta charset="utf-8">
                                        <title>Notify Calendar Event Change Status</title>
                                    </head>
                                    <!---
                                            ConfirmationFlag States
                                            
                                            0 - No reminder sent
                                            1 - Reminder inQueue
                                            2 - Reminder Accepted
                                            3 - Reminder Declined
                                            4 - Reminder Change Request
                                            5 - Reminder Error - Bad SMS, eMail or other error
                                            6 - Dont send Reminder	
                                            7 - Reminder Sent		          	
                                    --->	
                                    <body style="font-family: Arial; font-size: 12px;">
                                        <table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="50%">
                                                    <a href="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/logo.jpg" alt=""></a>
                                                </td>
                                                <td style="text-align: right; color: ##5c5c5c; font-size: 11px;">
                                                    <!--- <p>Trouble seeing this email? <a href="<cfoutput>#content.ROOTURL#</cfoutput>/public/sire/pages/contact-us">Click here</a></p>--->
                                                    <p>Add <a href="mailto:support@siremobile.com">support@siremobile.com</a> to your contacts</p>
                                                </td>
                                            </tr>
                                            <tbody style="margin: 0; padding: 0;">
                                                <tr>
                                                    <td colspan="2" style="margin: 0; padding: 0;">
                                                        <div style="text-align: center;">
                                                            <img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="padding: 0 30px;">
                                                        <p style="font-size: 16px;">Hi <cfoutput>#USERNAME#</cfoutput>!</p>

                                                        <p style="font-size: 16px;">The event name :<b><cfoutput>#EVENTNAME#</cfoutput>.</b></p>
                                                        <p style="font-size: 16px;">Just changed status to: <b><cfoutput>#CRRSTATUSNAME#</cfoutput>.</b></p>
                                                        <p style="font-size: 16px;">Event start: <cfoutput>#EVENTSTART#</cfoutput>.</p>
                                                        <p style="font-size: 16px;">Event end: <cfoutput>#EVENTEND#</cfoutput>.</p>
                                                        <p style="font-size: 16px;">Phone number: <cfoutput>#PHONENUMBER#</cfoutput>.</p>
                                                        <p style="font-size: 16px;">
                                                            Thanks,<br>
                                                            SIRE Calendar
                                                        </p>
                                                    </td>
                                                </tr>	
                                            </tbody>
                                            <tfoot>
                                                <tr style="text-align: center" bgcolor="##274e60">
                                                    <td style="font-family: Arial; font-size: 12px; padding-left: 15px; color: white" colspan="2">
                                                    <p>For support requests, please call or email us at:<br>
                                                    (888) 747-4411 | <a href="mailto:support@siremobile.com" style="text-decoration: none; color: white">support@siremobile.com</a> | Text "<a style="color: white; text-decoration: none;"><strong>support</strong></a>" to <a style="color: white; text-decoration: none;"><strong>39492</strong></a></p>
                                                    </td>
                                                </tr>

                                                <tr style="text-align: center" bgcolor="##274e60">
                                                    <td width="100%" style="padding-bottom: 15px" colspan="2">
                                                        <a style="padding-right: 7px" href="https://www.facebook.com/siremobile/"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
                                                        <a style="padding-right: 7px" href="https://twitter.com/sire_mobile"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
                                                        <a style="padding-right: 7px" href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
                                                        <a style="padding-right: 7px" href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
                                                        <a href="https://plus.google.com/116619152084831668774"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </body>

                                </html>
                            </cfoutput>
                        </cfmail>
                    </cfif>
                <cfelse>
                    <cfif listphone NEQ "">

                        <cfloop list="#listphone#" index="phone" delimiters=",">
                            <!--- sent sms --->
                            <cfset phone = ReReplaceNoCase(phone,"[^0-9,]","","ALL")>
                            <cfset Session.UserId = GetSessionIdEventRespone.UserId_int/>
                            <cfset Session.DBSourceEBM = DBSourceEBM/>


                            <cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
                                <cfinvokeargument name="inpContactString" value="#phone#"/>
                                <cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
                                <cfinvokeargument name="inpTextToSend" value="Hi, Event name: #EVENTNAME# just change status to #CRRSTATUSNAME# by phone number #PHONENUMBER#. Thanks!."/>
                            </cfinvoke>

                            <cfset ENA_Message = ENA_Message &" phone:"& phone & " mt result:"& serializeJSON(RetVarSendSingleMT) />

                        </cfloop>
                    </cfif>
                </cfif>

                <cfset dataout.MESSAGE = ENA_Message /> 
                <cfset dataout.RXRESULTCODE = 1 />
            </cfif>
            <cfcatch type="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                
                <cfset dataout = StructNew() />            
                
                <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
            
                <cfif DebugAPI GT 0>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
                <cfelse>
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
                </cfif>
                <!---
                <cfset ENA_Message = "">
                <cfset SubjectLine = "API Calendar Subject Line">
                <cfset TroubleShootingTips="API Calendar Trouble">
                <cfset ErrorNumber="9999">
                <cfset AlertType="1">
                --->       
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.ERRMESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
            </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>