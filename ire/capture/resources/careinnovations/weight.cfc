<cfcomponent extends="taffy.core.resource" taffy:uri="/careinnovations/weight/{inpWeight}/{inpContactString}/" hint="Weight Protocol">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://ebmii.messagebroadcast.com/ire/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="XMLDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
        
	<cffunction name="get" access="public" output="false" hint="Weight Protocol">
		<cfargument name="inpWeight" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so includes have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>

		<cftry>
           
			<cfif inpWeight GT 190>
            	<!--- Weight High --->
            	<cfset dataOut = 1 />
            
			<cfelseif inpWeight LT 180>
            	<!--- Weight Low --->
            	<cfset dataOut = 2 />
            
            <cfelse>
            	<!--- Weight Normal --->
            	<cfset dataOut = 3 />
            
            </cfif>
                                    
        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
                        
                <cftry>        
                
                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "inpConfirm = (#inpConfirm#)  inpContactString=(#inpContactStringAppt#) inpBatchId=(#inpBatchIdAppt#) inpContactTypeIdAppt=(#inpContactTypeIdAppt#) inpQAMode=(#inpQAMode#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>