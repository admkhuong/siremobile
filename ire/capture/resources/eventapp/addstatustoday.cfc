<cfcomponent extends="taffy.core.resource" taffy:uri="/eventapp/addstatustoday" hint="Add a Status Record for this Event - Today only version">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Event Program"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpStatus" required="yes" hint="1=IN, 2=OUT, 3=MAYBE">
        		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
                
        <!--- Formatted Date Time --->
        <cfset dataOut = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
                      
      
		<cftry>
           
        
        	<!--- Replace any existing status for today with current status (use update with insert? ) --->	
        
        	<!---
				This method is best since it checks PRIMARY keys, UNIQUE indexes and auto increment columns. So INSERT INTO table (a,b,c) VALUES (NULL, "A", 19) will still cause an update event. 
				INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE    name="A", age=19
			--->
           
            <!--- Add a status update for today --->                          
            <cfquery name="InsertToEventStatus" datasource="#DBSourceEBM#" >              
              
              INSERT INTO 
              	simplequeue.eventapp 
                (	
                	PKId_bi, 
                    BatchId_bi, 
                    Status_int, 
                    Event_date, 
                    Created_dt,
                    ContactString_vch
                )
                VALUES
                (
                	NULL,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                    NOW(),
                 	NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                     
                 ) ON DUPLICATE KEY UPDATE 
                 
                 	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                    Event_date = NOW(),                    
                    Created_dt = NOW(),
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">                    
                               
            </cfquery>
            		   	                                   
        <cfcatch>        	
           
            <cfset dataOut = {}>
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
                              
                <cftry>        
                
                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "inpConfirm = (#inpConfirm#)  inpContactString=(#inpContactStringAppt#) inpBatchId=(#inpBatchIdAppt#) inpContactTypeIdAppt=(#inpContactTypeIdAppt#) inpQAMode=(#inpQAMode#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>