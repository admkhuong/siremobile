<cfcomponent extends="taffy.core.resource" taffy:uri="/eventapp/getstatuscountstoday" hint="Get counts of Status Records for this Event - Today only version">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="GET" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Event Program"/>
             		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var GetCountEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
        <cfset var StatusIn = "0"/> 
        <cfset var StatusMaybe = "0"/> 
        <cfset var StatusOut = "0"/> 
                
        <!--- Formatted Date Time --->
        <cfset dataOut = "0 out of 0" />                      
      
		<cftry>
                   
        	<!--- Replace any existing status for today with current status (use update with insert? ) --->	
        
        	<!---
				This method is best since it checks PRIMARY keys, UNIQUE indexes and auto increment columns. So INSERT INTO table (a,b,c) VALUES (NULL, "A", 19) will still cause an update event. 
				INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE    name="A", age=19
			--->
           
            <!--- Add a status update for today --->                          
            <cfquery name="GetCountEventStatus" datasource="#DBSourceEBM#" >   
                SELECT 
                	COUNT(Status_int) As TOTALCOUNT,
               		Status_int              	
                FROM
                	simplequeue.eventapp
                WHERE
                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">
                AND
                	Event_date = '#LSDateFormat(NOW(), 'yyyy-mm-dd')#'
                GROUP BY
                	Status_int            
            </cfquery>
            
            <cfloop query="GetCountEventStatus">
            
            	<cfif GetCountEventStatus.Status_int EQ 1>                
               		<cfset StatusIn = GetCountEventStatus.TOTALCOUNT/> 
                </cfif>
                
                <cfif GetCountEventStatus.Status_int EQ 2>                
               		<cfset StatusOut = GetCountEventStatus.TOTALCOUNT/> 
                </cfif>
                
                <cfif GetCountEventStatus.Status_int EQ 3>                
               		<cfset StatusMaybe = GetCountEventStatus.TOTALCOUNT/> 
                </cfif>               
            
            </cfloop>
            
            
             <cfset dataOut = "(#StatusIn#) IN today APINewLine" /> 
             <cfset dataOut = dataOut & "(#StatusMaybe#) MAYBE today APINewLine" /> 
             <cfset dataOut = dataOut & "(#StatusOut#) OUT today APINewLine" />      
             
             
            		   	                                   
        <cfcatch>        	
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
                              
                <cftry>        
                
                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "inpConfirm = (#inpConfirm#)  inpContactString=(#inpContactStringAppt#) inpBatchId=(#inpBatchIdAppt#) inpContactTypeIdAppt=(#inpContactTypeIdAppt#) inpQAMode=(#inpQAMode#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>