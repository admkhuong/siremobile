<cfcomponent extends="taffy.core.resource" taffy:uri="/finance/profile" hint="Calculate 1 of 5 possible Investor Types">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Evaluate equation - Returns the result">
		<cfargument name="inpTimeHorizon" TYPE="string" required="yes" hint="Required Time Horizon Score"/>
        <cfargument name="inpRiskTolerance" TYPE="string" required="yes" hint="Required Risk Tolerance Score"/>
               		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
               
      
        <cfset dataOut = "0" />
        
        <!--- https://www.schwab.com/public/file/P-778947/InvestorProfileQuestionnaire.pdf --->              
      
		<cftry>
           
           
           	<cfif inpTimeHorizon GTE 0 AND inpTimeHorizon LTE 4>
                      
            	<cfif inpRiskTolerance GTE 0 AND inpRiskTolerance LTE 18>
                	
                	<cfset dataOut = 1 />
                
                <cfelseif inpRiskTolerance GTE 19 AND inpRiskTolerance LTE 31>
                
                	<cfset dataOut = 2 />
                
                <cfelseif inpRiskTolerance GTE 31>
                
	                <cfset dataOut = 3 />
    
                </cfif>      
                       
            <cfelseif inpTimeHorizon EQ 5 OR inpTimeHorizon EQ 6>
                
                <cfif inpRiskTolerance GTE 0 AND inpRiskTolerance LTE 15>
                	
                	<cfset dataOut = 1 />
                
                <cfelseif inpRiskTolerance GTE 16 AND inpRiskTolerance LTE 24>
                
                	<cfset dataOut = 2 />
                    
                <cfelseif inpRiskTolerance GTE 25 AND inpRiskTolerance LTE 35>
                
                	<cfset dataOut = 3 />    
                
                <cfelseif inpRiskTolerance GTE 36>
                
	                <cfset dataOut = 4 />
    
                </cfif>   
            
            <cfelseif inpTimeHorizon GTE 7 AND inpTimeHorizon LTE 9>
            
            	<cfif inpRiskTolerance GTE 0 AND inpRiskTolerance LTE 12>
                	
                	<cfset dataOut = 1 />
                
                <cfelseif inpRiskTolerance GTE 13 AND inpRiskTolerance LTE 20>
                
                	<cfset dataOut = 2 />
                    
                <cfelseif inpRiskTolerance GTE 21 AND inpRiskTolerance LTE 28>
                
                	<cfset dataOut = 3 />    
                
                <cfelseif inpRiskTolerance GTE 29 AND inpRiskTolerance LTE 37>
                
                	<cfset dataOut = 4 />    
                    
                <cfelseif inpRiskTolerance GTE 38>
                
	                <cfset dataOut = 5 />
    
                </cfif>   
                
            
            <cfelseif inpTimeHorizon GTE 10 AND inpTimeHorizon LTE 12>
             
             	<cfif inpRiskTolerance GTE 0 AND inpRiskTolerance LTE 11>
                	
                	<cfset dataOut = 1 />
                
                <cfelseif inpRiskTolerance GTE 12 AND inpRiskTolerance LTE 18>
                
                	<cfset dataOut = 2 />
                    
                <cfelseif inpRiskTolerance GTE 19 AND inpRiskTolerance LTE 26>
                
                	<cfset dataOut = 3 />    
                
                <cfelseif inpRiskTolerance GTE 27 AND inpRiskTolerance LTE 34>
                
                	<cfset dataOut = 4 />    
                    
                <cfelseif inpRiskTolerance GTE 35>
                
	                <cfset dataOut = 5 />
    
                </cfif>   
             
            <cfelseif inpTimeHorizon GTE 14>
            
            	<cfif inpRiskTolerance GTE 0 AND inpRiskTolerance LTE 10>
                	
                	<cfset dataOut = 1 />
                
                <cfelseif inpRiskTolerance GTE 11 AND inpRiskTolerance LTE 17>
                
                	<cfset dataOut = 2 />
                    
                <cfelseif inpRiskTolerance GTE 18 AND inpRiskTolerance LTE 24>
                
                	<cfset dataOut = 3 />    
                
                <cfelseif inpRiskTolerance GTE 25 AND inpRiskTolerance LTE 31>
                
                	<cfset dataOut = 4 />    
                    
                <cfelseif inpRiskTolerance GTE 32>
                
	                <cfset dataOut = 5 />
    
                </cfif>  
            
           	</cfif>      
            		   	                                   
        <cfcatch>        	
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
    
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = {} />  
        
        <cfset dataout.MESSAGE = "You should use the verb POST JSON or FORM variables to this API location. Currently you only using GET!" />
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>