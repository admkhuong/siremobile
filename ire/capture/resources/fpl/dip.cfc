<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/dip" hint="FPL dipping service">


	<!--- MB Account info for voxtelesys --->
    <cfset variables.ServiceLogin = "90020">
    <cfset variables.ServicePassword = "81E4EAE6-48B7-4267-B787-C04E8BF2C63B">

    <!---API urls at voxtelesys--->
    <cfset variables.URL1 = "https://ops-srvphp01.voxtelesys.net/numberLookup.php">
    <cfset variables.URL2 = "https://ops-srvphp02.voxtelesys.net/numberLookup.php">

    <!---local error var--->
    <cfset variables.error = "">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->

	<cffunction name="get" access="public" output="false" hint="" returnformat="JSON">
	    <cfargument name="phoneNumber" type="string" required="true">

<!--- 		<cftry>

			<cfhttp url="http://dev10.telespeech.com/atif/fplStaging/cfcs/fpldip.cfc?method=IsCell&PHONENUMBER=#arguments.phoneNumber#" method="get" result="httpResp" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="text/json" />
			</cfhttp>

            <cfcatch type="any">

            </cfcatch>

		</cftry> --->


        <cfset var sResult = structNew()>

        <cfset var sReturn = structNew()>
        <cfset structInsert(sReturn,'error','')>
        <cfset structInsert(sReturn,'origPhoneNumber',arguments.phoneNumber)>
        <cfset structInsert(sReturn,'isCell','')>

        <cfset variables.error = "">

        <cfset sResult = deSerializeJSON(getPhoneInfo(arguments.phoneNumber))>

        <cfif variables.Error EQ "" AND structKeyExists(sResult,'numberType')>

        	<cfswitch expression="#UCase(sResult.numberType)#">
	            <cfcase value="CELLULAR">
                	<cfset sReturn.isCell = True>
                </cfcase>
	            <cfcase value="LANDLINE">
                	<cfset sReturn.isCell = False>
                </cfcase>
                <cfdefaultcase>

                </cfdefaultcase>
            </cfswitch>

		<cfelse>
        	<cfset sReturn.error = variables.Error>
        </cfif>

		<cfset OdIP = CreateObject("component","fplopt")>
		<cfset OdIP.logServiceCall(serviceType="DIP", serviceData = serializeJSON(sReturn))>

		<cfreturn representationOf(sReturn).withStatus(200) />

	</cffunction>


	 <!--- get information about cell phone --->
    <cffunction name="getPhoneInfo" access="private" output="false" returnformat="json">
	    <cfargument name="phoneNumber" type="string" required="true">

        <!---initialize the return struct--->
        <cfset var sReturn = structNew()>
        <cfset var strURL = "">

        <cfset structInsert(sReturn,'error','')>
        <cfset structInsert(sReturn,'origPhoneNumber',arguments.phoneNumber)>
        <cfset structInsert(sReturn,'numberType','UNKNOWN')>

        <cfset variables.Error = "">

       <cftry>

       		<!---alternate b/w server 1 & 2--->
       		<cfif second(now()) mod 2 EQ 1>
            	<cfset strURL = variables.URL1>
            <cfelse>
            	<cfset strURL = variables.URL2>
            </cfif>

        	<!---construct soap request--->
            <cfsavecontent variable="soapBody">
            <cfoutput>
            <?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:urn="urn:vtsPhoneNumberLookup">
                <soapenv:Header/>
                    <soapenv:Body>
                        <urn:numberLookup soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                            <phoneNumber xsi:type="xsd:string">#arguments.phoneNumber#</phoneNumber>
                            <userName xsi:type="xsd:string">#variables.ServiceLogin#</userName>
                            <password xsi:type="xsd:string">#variables.ServicePassword#</password>
                        </urn:numberLookup>
                    </soapenv:Body>
                </soapenv:Envelope>
            </cfoutput>
            </cfsavecontent>

			<!---make https call to voxtelesys--->
			<cfhttp url="#strURL#" method="post" result="httpResponse">
				<cfhttpparam type="header"	name="Content-Type" value="text/xml" />
             	<cfhttpparam type="header"	name="SOAPAction" value="urn:vtsPhoneNumberLookup##phoneNumber" />
             	<cfhttpparam type="xml" value="#trim(soapBody)#"/>
            </cfhttp>

			<cfif find( "200", httpResponse.statusCode )>
                <cfset soapResponse = xmlParse( httpResponse.fileContent ) />
                <cfset responseNodes = xmlSearch(soapResponse,"//*[ local-name() = 'numberType' ]") />
                <cfif arrayLen(responseNodes) GT 0>
                	<cfset sReturn.numberType = responseNodes[1].XmlText>
                </cfif>
			<cfelse>
            	<cfset variables.Error = 'Error occured while dipping: ' & httpResponse.statusCode>
            </cfif>

            <cfcatch type="any">
            	<cfset variables.Error = 'Error occured while dipping: ' & cfcatch.Message>
            </cfcatch>
		</cftry>

		<cfset sReturn.error = variables.Error>

        <cfreturn serializeJSON(sReturn)>
    </cffunction>


	<cffunction name="getOld" access="public" output="false" hint="" returnformat="JSON">
	    <cfargument name="phoneNumber" type="string" required="true">

		<cftry>

			<cfhttp url="http://dev10.telespeech.com/atif/fplStaging/cfcs/fpldip.cfc?method=IsCell&PHONENUMBER=#arguments.phoneNumber#" method="get" result="httpResp" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="text/json" />
			</cfhttp>

            <cfcatch type="any">

            </cfcatch>

		</cftry>

		<cfcontent type="application/json" reset="yes">

		<cfreturn representationOf(DESerializeJSON(httpResp.filecontent.toString())).withStatus(200) />

	</cffunction>

</cfcomponent>


<!---
Working Dippping service
<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/dip" hint="FPL dipping service">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->

	<cffunction name="get" access="public" output="false" hint="" returnformat="JSON">
	    <cfargument name="phoneNumber" type="string" required="true">

		<cftry>

			<cfhttp url="http://dev10.telespeech.com/atif/fplStaging/cfcs/fpldip.cfc?method=IsCell&PHONENUMBER=#arguments.phoneNumber#" method="get" result="httpResp" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="text/json" />
			</cfhttp>

            <cfcatch type="any">

            </cfcatch>

		</cftry>


		<cfreturn representationOf(DESerializeJSON(httpResp.filecontent.toString())).withStatus(200) />

	</cffunction>

</cfcomponent> --->