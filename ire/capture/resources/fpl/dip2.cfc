<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/dip2" hint="FPL dipping service">


	<!--- MB Account info for voxtelesys --->
    <cfset variables.ServiceLogin = "90020">
    <cfset variables.ServicePassword = "81E4EAE6-48B7-4267-B787-C04E8BF2C63B">

    <!---API urls at voxtelesys--->
    <cfset variables.URL1 = "https://ops-srvphp01.voxtelesys.net/numberLookup.php">
    <cfset variables.URL2 = "https://ops-srvphp02.voxtelesys.net/numberLookup.php">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->

	<!--- stopgap measure --->
	<cffunction name="get" access="public" output="false" hint="" returnformat="JSON">

	    <cfset phoneNumber = "9492786295">

        <!---initialize the return struct--->
        <cfset var sReturn = structNew()>
        <cfset var strURL = "">

        <cfset structInsert(sReturn,'error','')>
        <cfset structInsert(sReturn,'origPhoneNumber',phoneNumber)>
        <cfset structInsert(sReturn,'numberType','UNKNOWN')>

        <cfset variables.Error = "">

       <cftry>

       		<!---alternate b/w server 1 & 2--->
       		<cfif second(now()) mod 2 EQ 1>
            	<cfset strURL = variables.URL1>
            <cfelse>
            	<cfset strURL = variables.URL2>
            </cfif>

        	<!---construct soap request--->
            <cfsavecontent variable="soapBody">
            <cfoutput>
            <?xml version="1.0" encoding="UTF-8"?>
                <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:urn="urn:vtsPhoneNumberLookup">
                <soapenv:Header/>
                    <soapenv:Body>
                        <urn:numberLookup soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                            <phoneNumber xsi:type="xsd:string">#phoneNumber#</phoneNumber>
                            <userName xsi:type="xsd:string">#variables.ServiceLogin#</userName>
                            <password xsi:type="xsd:string">#variables.ServicePassword#</password>
                        </urn:numberLookup>
                    </soapenv:Body>
                </soapenv:Envelope>
            </cfoutput>
            </cfsavecontent>

			<!---make https call to voxtelesys--->
			<cfhttp url="#strURL#" method="post" result="httpResponse">
				<cfhttpparam type="header"	name="Content-Type" value="text/xml" />
             	<cfhttpparam type="header"	name="SOAPAction" value="urn:vtsPhoneNumberLookup##phoneNumber" />
             	<cfhttpparam type="xml" value="#trim(soapBody)#"/>
            </cfhttp>

			<cfif find( "200", httpResponse.statusCode )>
                <cfset soapResponse = xmlParse( httpResponse.fileContent ) />
                <cfset responseNodes = xmlSearch(soapResponse,"//*[ local-name() = 'numberType' ]") />
                <cfif arrayLen(responseNodes) GT 0>
                	<cfset sReturn.numberType = responseNodes[1].XmlText>
                </cfif>
			<cfelse>
            	<cfset variables.Error = 'Error occured while dipping: ' & httpResponse.statusCode>
            </cfif>

            <cfcatch type="any">
            	<cfset variables.Error = 'Error occured while dipping: ' & cfcatch.Message>
            </cfcatch>
		</cftry>

		<cfset sReturn.error = variables.Error>

		<cfset aa.hr = httpResponse>
		<cfset aa.url =  strURL>
		<cfreturn representationOf(aa).withStatus(200) />

	</cffunction>

</cfcomponent>