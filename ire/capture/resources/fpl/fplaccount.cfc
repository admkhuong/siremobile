<!---
  --- fplaccount
  --- ----------
  ---
  --- author: ahasan
  --- date:   4/9/15
  --->
<cfcomponent accessors="true" output="true" persistent="true" extends="fpl">

	<cffunction name="getAccount" returntype="query" access="public">
		<cfargument name="ContactString" required="false" default="">
		<cfargument name="AccountId" required="false" default="">

		<cfquery name="qAccount" datasource="#getEBMDsn()#">
			SELECT	*
			FROM	simplexprogramdata.portal_link_c10_p1_accounts
			WHERE	1=1
					<cfif arguments.ContactString NEQ "">
						AND phoneNo_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ContactString#">
					</cfif>
					<cfif arguments.AccountId NEQ "">
						AND AccountId_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.AccountId#">
					</cfif>
		</cfquery>

		<cfreturn qAccount>
	</cffunction>

	<cffunction name="mergeAccount" access="public">
		<cfargument name="ContactString" required="true" default="">
		<cfargument name="AccountId" required="true" default="">

		<cfquery name="qAccount" datasource="#getEBMDsn()#">
			INSERT INTO simplexprogramdata.portal_link_c10_p1_accounts
			(
				AccountId_vch,
				PhoneNo_vch,
				Collection_int,
				<cfif isdefined("arguments.SendToNCO") AND arguments.SendToNCO>UpdateTimeStampToNCO_dt,</cfif>
				<cfif isdefined("arguments.SendToFPL") AND arguments.SendToFPL>UpdateTimeStampToFPL_dt,</cfif>
				<cfif isdefined("arguments.SendToNCOStatus")>SendToNCOStatus_vch,</cfif>
				<cfif isdefined("arguments.SendToFPLStatus")>SendToFPLStatus_vch,</cfif>
				SendToNCO_int,
				SendToFPL_int
			)
			VALUES
			(
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.AccountId#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ContactString#">,
				<cfif isdefined("arguments.Collection")><cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Collection#"><cfelse>0</cfif>,
				<cfif isdefined("arguments.SendToNCO") AND arguments.SendToNCO>now(),</cfif>
				<cfif isdefined("arguments.SendToFPL") AND arguments.SendToFPL>now(),</cfif>
				<cfif isdefined("arguments.SendToNCOStatus")><cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.SendToNCOStatus#">,</cfif>
				<cfif isdefined("arguments.SendToFPLStatus")><cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.SendToFPLStatus#">,</cfif>
				<cfif isdefined("arguments.SendToNCO")><cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.SendToNCO#"><cfelse>0</cfif>,
				<cfif isdefined("arguments.SendToFPL")><cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.SendToFPL#"><cfelse>0</cfif>
			)
			ON DUPLICATE KEY UPDATE
				<cfif isdefined("arguments.Collection")>Collection_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.Collection#">,</cfif>
				<cfif isdefined("arguments.SendToFPL")>SendToFPL_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.SendToFPL#">,</cfif>
				<cfif isdefined("arguments.SendToNCO")>SendToNCO_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.SendToNCO#">,</cfif>
				<cfif isdefined("arguments.SendToNCOStatus")>SendToNCOStatus_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.SendToNCOStatus#">,</cfif>
				<cfif isdefined("arguments.SendToFPLStatus")>SendToFPLStatus_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.SendToFPLStatus#">,</cfif>
				<cfif isdefined("arguments.SendToNCO") AND arguments.SendToNCO>UpdateTimeStampToNCO_dt = now(),</cfif>
				<cfif isdefined("arguments.SendToFPL") AND arguments.SendToFPL>UpdateTimeStampToFPL_dt = now(),</cfif>
				PhoneNo_vch = PhoneNo_vch;
		</cfquery>

	</cffunction>

	<!--- update misc fields to send it to NCO --->
	<cffunction name="sendToNCOByContactString" access="public">
		<cfargument name="ContactString" required="true" default="">
		<cfargument name="Collection" required="false" default="">
		<cfargument name="OptTimeStamp" required="false" default="">
		<cfargument name="SendToNCOStatus" required="false" default="">

		<cfquery name="qAccount" datasource="#getEBMDsn()#">
			UPDATE 	simplexprogramdata.portal_link_c10_p1_accounts
			SET 	SendToNCO_int = 1,
					<cfif arguments.Collection NEQ "">
						Collection_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.collection#">,
					</cfif>
					<cfif arguments.OptTimeStamp NEQ "">
						OptTimeStampToNCO_dt = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.OptTimeStamp#">,
					</cfif>
					<cfif arguments.SendToNCOStatus NEQ "">
						SendToNCOStatus_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.SendToNCOStatus#">,
					</cfif>
					UpdateTimeStampToNCO_dt = now()
			WHERE	PhoneNo_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.ContactString#">
		</cfquery>

	</cffunction>

</cfcomponent>