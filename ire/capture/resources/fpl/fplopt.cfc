<!---
  --- fplopt
  --- --------
  ---
  --- author: ahasan
  --- date:   4/7/15
  --- component that deals with opt-in opt-out and opt-down requests from FPL. Main method is "Opt" that is avaialbe for remote access. Rest of the methods are private
  --->
<cfcomponent  accessors="yes" extends="fpl" persistent="yes">

    <cfproperty name="BatchId" type="numeric" default="1374"><!---  batch id for opt-in campaign --->

	<!--- only public/remote function called to do optin, optout and optdowns --->
	<cffunction name="opt" access="remote" returnformat="JSON" Output="true">
		<cfargument name="inpJSON" required="true">

		<!---
		expected request data in JSON format
		{
		  "notification": {
		    "token": "a7f1fbb0-24d2-11df-8a39-0800200c9a99",
		    "uniqueId": "ACT001",
		    "language": "EN",
		    "device": {
		      "sms": "9492786295"
		    },
		    "application": "FPLOPT",
		    "action": "IN, OUT, UC, OSC",
		    "message": {
		      "text": "TEXT MESSAGE",
		      "additionalInfo": {
		        "info": [
		          "EXTRA INFO1",
		          "EXTRA INFO2"
		        ]
		      }
		    },
		    "preferences": {
		      "collection": "1"
		    }
		  }
		}
		--->

		<cfset var oFPLAccount = CreateObject('component','fplAccount')>

		<cfset jReturn = "">

		<!---initialize vars--->
		<cfset FPLData = structnew()>
		<cfset FPLData.uniqueId = "">
		<cfset FPLData.language = "EN">
		<cfset FPLData.phoneNumber = "">
		<cfset FPLData.application = "">
		<cfset FPLData.smsnumber = "">
		<cfset FPLData.type = "">
		<cfset FPLData.text = "">
		<cfset FPLData.info1 = "">
		<cfset FPLData.info2 = "">
		<cfset FPLData.info3 = "">
		<cfset FPLData.info4 = "">
		<cfset FPLData.info5 = "">
		<cfset FPLData.info6 = "">
		<cfset FPLData.info7 = "">
		<cfset FPLData.info8 = "">
		<cfset FPLData.info9 = "">
		<cfset FPLData.info10 = "">
		<cfset FPLData.action = "IN"> <!--- default action is OPT-IN--->
		<cfset FPLData.collection = 0>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="OPT", serviceData = inpJSON)>

		<cftry>

		    <!---get values from request JSON--->
			<cfscript>

				myJSONdoc = DeserializeJSON(inpJSON);
				if(isdefined('myJSONdoc.notification.token'))
				{
					FPLData.token = myJSONdoc.notification.token;
				}

				if(isdefined('myJSONdoc.notification.uniqueId'))
				{
					FPLData.uniqueId = myJSONdoc.notification.uniqueId;
				}

				if(isdefined('myJSONdoc.notification.device.phoneNumber'))
				{
					FPLData.phoneNumber = myJSONdoc.notification.device.phoneNumber;
				}

				if(isdefined('myJSONdoc.notification.device.sms'))
				{
					FPLData.smsnumber = myJSONdoc.notification.device.sms;
				}

				if(isdefined('myJSONdoc.notification.device.email'))
				{
					FPLData.email = myJSONdoc.notification.device.email;
				}

				if(isdefined('myJSONdoc.notification.application'))
				{
					FPLData.application = myJSONdoc.notification.application;
				}

				if(isdefined('myJSONdoc.notification.message.type'))
				{
					FPLData.type = myJSONdoc.notification.message.type;
				}

				if(isdefined('myJSONdoc.notification.message.text'))
				{
					FPLData.text1 = myJSONdoc.notification.message.text;
				}

				if(isdefined('myJSONdoc.notification.message.additionalInfo.info'))
				{
					for(i=1;i LTE arrayLen(myJSONdoc.notification.message.additionalInfo.info);i++)
					{
						"FPLData.info#i#" = myJSONdoc.notification.message.additionalInfo.info[i] ;
					}
				}

				if(isdefined('myJSONdoc.notification.action'))
				{
					FPLData.action = ucase(myJSONdoc.notification.action);
				}

				if(isdefined('myJSONdoc.notification.preferences.collection'))
				{
					FPLData.collection = myJSONdoc.notification.preferences.collection;
				}

			</cfscript>

			<cfif FPLData.application contains "FPLOPT">

				<!---decide about the action to be taken i.e. opt-in or out-out. If none of the 2, raise error--->
		    	<cfswitch expression="#FPLData.action#">
		        	<cfcase value="IN">
						<!--- send MTO opt-in request --->
				        <cfset jReturn = DeSerializeJson(optIn(FPLData))>

						<!--- Add or update record in account table --->
				        <cfset oFPLAccount.mergeAccount(	AccountId = FPLData.uniqueId,
							        								ContactString = FPLData.smsnumber,
							        								Collection = FPLData.collection,
							        								SendToNCOStatus = FPLData.action,
							        								SendToNCO = 0,
							        								SendToFPL = 0 )>
					</cfcase>
		            <cfcase value="OUT">
						<!--- Add opt out record in opt table and kill any messages in queue --->
			            <cfset jReturn = DeSerializeJson(optOut(FPLData))>

						<!--- Add or update record in account table to send opt-out to NCO --->
				        <cfset oFPLAccount.mergeAccount(	AccountId = FPLData.uniqueId,
							        								ContactString = FPLData.smsnumber,
							        								Collection = 0,
							        								SendToNCOStatus = FPLData.action,
							        								SendToNCO = 1,
							        								SendToFPL = 0 )>

		            </cfcase>
		            <cfcase value="UC">
			            <!---
			            un changed opt status. service is called coz user has either checked or
			            un checked the collection flag so send the update to NCO
						 --->

						<cfset jReturn = {ack={inserted="1",read="1",errcode="0"}}>

						<!--- Add or update record in account table to send opt-out to NCO --->
				        <cfset oFPLAccount.mergeAccount(	AccountId = FPLData.uniqueId,
							        								ContactString = FPLData.smsnumber,
							        								Collection = FPLData.collection,
							        								SendToNCOStatus = FPLData.action,
							        								SendToNCO = 1,
							        								SendToFPL = 0 )>

		            </cfcase>
		            <cfcase value="OSC">
			            <!---
			            customer was signed up with Old Short Code and has now changed the collection flag
						status. dont send myfpl opt-in/out just send flag to NCO
						 --->

						<cfset jReturn = {ack={inserted="1",read="1",errcode="0"}}>

						<!--- What should be done about this record? should we sent it to NCO right away?
						thhis should not turn on the flag to NCO as authentication might be in pending
						but this collection flag is coming for just old Short code (OSC)
						Add or update record in account table to send opt-out to NCO --->
				        <cfset oFPLAccount.mergeAccount(	AccountId = FPLData.uniqueId,
							        								ContactString = FPLData.smsnumber,
							        								Collection = FPLData.collection,
							        								SendToNCOStatus = FPLData.action,
							        								SendToNCO = 1,
							        								SendToFPL = 0 )>
		            </cfcase>

		            <cfdefaultcase>
			            <cfset jReturn = {ack={inserted="0",read="0",errcode="3"}}>
		            </cfdefaultcase>
		        </cfswitch>

				<!--- log activity --->
				<!--- <cfset logFPLMsg("fplopt.cfc - #GetFunctionCalledName()# - FPLData.action=#FPLData.action# - FPLData.smsnumber=#FPLData.smsnumber# - FPLData.uniqueId=#FPLData.uniqueId# - FPLData.collection=#FPLData.collection#")> --->


		    <cfelse>
	            <cfset jReturn = {ack={inserted="0",read="0",errcode="3"}}>
		    </cfif>

			<cfcatch type="any">

		    	<cfset jReturn = {ack={inserted="0",read="0",errcode="5"}}>

		    	<cfsavecontent variable="datainfo">
			    	<cfdump var="#FPLData#">
				</cfsavecontent>

	            <cfset SubjectLine = "FPL ERROR - OPT - Error in Opt-in service on server #CGI.SERVER_NAME#">
	            <cfset ENA_Message = "Error while making opt call #datainfo#">
       			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

			</cfcatch>


		</cftry>


		<cfreturn SerializeJson(jReturn)>

	</cffunction>

	<!--- function to opt-in a number into FPL --->
	<cffunction name="OptIn" access="public" output="true" returnformat="JSON">
		<cfargument name="FPLData" required="true" type="struct">

		<cfset jReturn = "">

		<cftry>

			<cfif arguments.FPLData.smsnumber GT 0 AND (NOT getEnforceWhiteList() OR  listFindNoCase(getSMSWhiteList(),arguments.FPLData.smsnumber))>

			    <!---authenticate EBM API--->
				<cfset local.sign = generateSignature("POST") />
		        <cfset datetime = local.sign.DATE>
		        <cfset signature = local.sign.SIGNATURE>
		        <cfset authorization = getAccessKey() & ":" & signature>

		        <cfhttp url="#getEBMApiUrlBase()#/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >

		           <!--- Required components --->

		           <!--- By default EBM API will return json or XML --->
		           <cfhttpparam name="Accept" type="header" value="application/json" />
		           <cfhttpparam type="header" name="datetime" value="#datetime#" />
		           <cfhttpparam type="header" name="authorization" value="#authorization#" />

		           <!--- Batch Id controls which pre-defined campaign to run --->
		           <cfhttpparam type="formfield" name="inpBatchId" value="#getBatchId()#" />

		           <!--- Contact string--->
		           <cfhttpparam type="formfield" name="inpContactString" value="#arguments.FPLData.smsnumber#" />

		           <!--- 1=voice 2=email 3=SMS--->
		           <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />

		           <!--- Optional Components --->

		           <!--- Custom data element for PDC Batch Id 1135 --->
		           <cfhttpparam type="formfield" name="inpCustomSMS" value="#arguments.FPLData.text1#" />

					<!---because this is a Opt-In campaign message so skip the DNC check--->
		           <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />

					<!--- <cfhttpparam type="formfield" name="inpUserDefinedKey" value="#arguments.FPLData.uniqueId#-#arguments.FPLData.smsnumber#" /> --->


		        </cfhttp>

				<cfset jReturn = {ack={inserted="1",read="1",errcode="0"}}>

			<cfelse>

				<cfset jReturn = {ack={inserted="0",read="1",errcode="2"}}>

			</cfif>

			<cfcatch type="any">

				<cfset jReturn = {ack={inserted="0",read="0",errcode="5"}}>

	            <cfset SubjectLine = "FPL ERROR - OPT - Error in Opt-in service on server #CGI.SERVER_NAME#">
	            <cfset ENA_Message = "Error while trying to send opt in request">
       			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

			</cfcatch>

		</cftry>

		<cfreturn SerializeJson(jReturn)>

	</cffunction>

	<!--- function to opt-out a number from myFPL --->
	<cffunction name="OptOut" access="public" output="true"  returnformat="JSON">
		<cfargument name="FPLData" required="true" type="struct">

		<cfset jReturn = "">

		<cftry>

			<cfif arguments.FPLData.smsnumber GT 0>

			    <!---authenticate EBM API--->
				<cfset local.sign = generateSignature("POST") />
		        <cfset datetime = local.sign.DATE>
		        <cfset signature = local.sign.SIGNATURE>
		        <cfset authorization = getAccessKey() & ":" & signature>

		    	<!---call opt out service--->
		        <cfhttp url="#getEBMApiUrlBase()#/ebm/opt/out" method="POST" result="returnStruct" >

		           <!--- By default EBM API will return json or XML --->
		           <cfhttpparam name="Accept" type="header" value="application/json" />
		           <cfhttpparam type="header" name="datetime" value="#datetime#" />
		           <cfhttpparam type="header" name="authorization" value="#authorization#" />

					<!---Required fields--->
		           <cfhttpparam type="formfield" name="inpContactString" value="#arguments.FPLData.smsnumber#" />
		           <cfhttpparam type="formfield" name="inpShortCode" value="#getmyFPLShortCode()#" />
		           <cfhttpparam type="formfield" name="inpOptOutSource" value="FPL" />

		        </cfhttp>

		        <cfset sResult = DeSerializeJSON(returnStruct.FileContent)>

		        <!---If there was no error, send back no error else send error code--->
		        <cfif sResult.RXRESULTCODE GT 0>
					<cfset jReturn = {ack={inserted="0",read="0",errcode="0"}}>
		        <cfelse>
					<cfset jReturn = {ack={inserted="0",read="0",errcode="5"}}>
		        </cfif>

			<cfelse>

				<cfset jReturn = {ack={inserted="0",read="1",errcode="2"}}>

			</cfif>

			<cfcatch type="any">

				<cfset jReturn = {ack={inserted="0",read="0",errcode="5"}}>

	            <cfset SubjectLine = "FPL UNICA - Error in Opt-out service on server #CGI.SERVER_NAME#">
	            <cfset ENA_Message = "Error while trying to send opt out request">
       			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

			</cfcatch>

		</cftry>

		<cfreturn SerializeJson(jReturn)>

	</cffunction>

	<!---
	this is scheduled to run after specific interval and checks for new MTO opt-in and opt-outs
	and makrs necessary update in accounts table (send to NCO, FPL etc) and
	call service hosted at FPL
	 --->
	<cffunction name="NewOptCheck" access="remote" output="true" returnformat="JSON">
		<cfargument name="fromTimeStamp" default="#getVar('OPT_CHECK_TIMESTAMP')#">

		<cfset var oFPLAccount = CreateObject('component','fplAccount')>
		<cfset sReturn = getReturnStruct()>

		<cftry>

			<!--- get all opt-ins  and opt-outs since last timestamp --->
			<cfquery name="qOptCheck" datasource="#getEBMDsn()#">
				select O.OptId_int, O.ContactSTring_vch, O.OptOut_dt, O.OptIn_dt, O.OptOutSource_vch, O.OptInSource_vch, O.BatchId_bi
				from (
						SELECT 	Max(OptId_int) as MaxId, ContactSTring_vch
							FROM 	simplelists.optinout
							where 	ShortCode_vch = '#getmyFPLShortCode()#'
									AND (
											OptOut_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now()
											OR
											OptIn_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now()
										)
							Group by ContactSTring_vch
				) as x inner join simplelists.optinout as o on o.ContactSTring_vch = x.ContactSTring_vch and o.OptId_int = x.MaxId
				where OptOutSource_vch is null or OptOutSource_vch !=  'FPL';  <!---  Shilpi/rikin asked not to post the results back when opt out is requested from FPL --->
			</cfquery>

			<cfloop query="qOptCheck">

				<!--- see if that was confirmed opt in or opt out --->
				<cfif isdate(qOptCheck.OptOut_dt)>
					<cfset opt = 0>

					<!--- update account table to send the record to NCO --->
			        <cfset oFPLAccount.sendToNCOByContactString(ContactString = qOptCheck.CONTACTSTRING_VCH, Collection = 0, OptTimeStamp = qOptCheck.OptOut_dt, SendToNCOStatus='OUT')>

					<!--- call service at FPL to send real-time opt-in/optout --->
					<cfset temp = postToFPL(contactString=qOptCheck.CONTACTSTRING_VCH,optIn=false)>
				<cfelse>
					<cfset opt = 1>

					<!--- update account table to send the record to NCO --->
			        <cfset oFPLAccount.sendToNCOByContactString(ContactString = qOptCheck.CONTACTSTRING_VCH, OptTimeStamp = qOptCheck.OptIn_dt, SendToNCOStatus='IN')>

					<!--- call service at FPL to send real-time opt-in/optout --->
					<cfset temp = postToFPL(contactString=qOptCheck.CONTACTSTRING_VCH,optIn=true,optDate=qOptCheck.OptIn_dt)>
				</cfif>



			</cfloop>

			<!--- update last check timestamp in var table so next time we check new optins from here on--->
			<cfset setVar(VarName='OPT_CHECK_TIMESTAMP',VarValue="#dateformat(now(),'yyyy-mm-dd')# #timeformat(now(),'HH:mm:ss')#")>

			<cfset sReturn.RecCount = qOptCheck.Recordcount>

			<!--- log activity --->
			<!--- <cfset logFPLMsg("fplopt.cfc - #GetFunctionCalledName()# - #qOptCheck.Recordcount# Records")> --->

			<cfcatch>

				<cfset SubjectLine = "FPL ERROR - OPT CHECK - Error while checking new opts on server #CGI.SERVER_NAME#">
				<cfset ENA_Message = "Error while checking for new opt-ins and opt-outs at timestamp #arguments.fromTimeStamp#">
				<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

				<!---set error in struct--->
				<cfset sReturn.ErrMsg = cfcatch.Message>
				<cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="NEW_OPT_CHK", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>

	</cffunction>


	<!--- method that checks if users have declined as NO,N to opt-in message and send it to FPL --->
	<!--- <cffunction name="NewDeclineCheck"  access="remote" output="true" returnformat="JSON">
		<cfargument name="fromTimeStamp" default="#getVar('OPT_CHECK_TIMESTAMP')#">

		<cfset sReturn = getReturnStruct()>

		<cftry>

			<!--- get all opt-ins  and opt-outs since last timestamp --->
			<cfquery name="qOptCheck" datasource="#getEBMDsn()#">
				select O.OptId_int, O.ContactSTring_vch, O.OptOut_dt, O.OptIn_dt, O.OptOutSource_vch, O.OptInSource_vch, O.BatchId_bi
				from (
						SELECT 	Max(OptId_int) as MaxId, ContactSTring_vch
							FROM 	simplelists.optinout
							where 	ShortCode_vch = '#getmyFPLShortCode()#'
									AND (
											OptOut_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now()
											OR
											OptIn_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now()
										)
							Group by ContactSTring_vch
				) as x inner join simplelists.optinout as o on o.ContactSTring_vch = x.ContactSTring_vch and o.OptId_int = x.MaxId;
			</cfquery>

			<cfloop query="qOptCheck">

				<!--- see if that was confirmed opt in or opt out --->
				<cfif isdate(qOptCheck.OptOut_dt)>
					<cfset opt = 0>

					<!--- update account table to send the record to NCO --->
			        <cfset oFPLAccount.sendToNCOByContactString(ContactString = qOptCheck.CONTACTSTRING_VCH, Collection = 0, OptTimeStamp = qOptCheck.OptOut_dt, SendToNCOStatus='OUT')>

					<!--- call service at FPL to send real-time opt-in/optout --->
					<cfset temp = postToFPL(contactString=qOptCheck.CONTACTSTRING_VCH,optIn=false)>
				<cfelse>
					<cfset opt = 1>

					<!--- update account table to send the record to NCO --->
			        <cfset oFPLAccount.sendToNCOByContactString(ContactString = qOptCheck.CONTACTSTRING_VCH, OptTimeStamp = qOptCheck.OptIn_dt, SendToNCOStatus='IN')>

					<!--- call service at FPL to send real-time opt-in/optout --->
					<cfset temp = postToFPL(contactString=qOptCheck.CONTACTSTRING_VCH,optIn=true,optDate=qOptCheck.OptIn_dt)>
				</cfif>



			</cfloop>

			<!--- update last check timestamp in var table so next time we check new optins from here on--->
			<cfset setVar(VarName='OPT_CHECK_TIMESTAMP',VarValue="#dateformat(now(),'yyyy-mm-dd')# #timeformat(now(),'HH:mm:ss')#")>

			<cfset sReturn.RecCount = qOptCheck.Recordcount>

			<!--- log activity --->
			<!--- <cfset logFPLMsg("fplopt.cfc - #GetFunctionCalledName()# - #qOptCheck.Recordcount# Records")> --->

			<cfcatch>

				<cfset SubjectLine = "FPL ERROR - OPT CHECK - Error while checking new opts on server #CGI.SERVER_NAME#">
				<cfset ENA_Message = "Error while checking for new opt-ins and opt-outs at timestamp #arguments.fromTimeStamp#">
				<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

				<!---set error in struct--->
				<cfset sReturn.ErrMsg = cfcatch.Message>
				<cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="NEW_OPT_CHK", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>
	</cffunction> --->


	<!---
	method to call service at FPL to post opt-ins and outs from
	handsets and/or NCO
	 --->
	<cffunction name="postToFPL" access="public" output="true">
		<cfargument name="contactString" required="true" type="string">
		<cfargument name="optIn" required="true" type="boolean">
		<cfargument name="optDate" required="false"  type="string" default="#now()#">
		<cfargument name="accountNo" required="false" type="string" default="">


		<cfset var thisAccountNo = "">
		<cfset var oFPLAccount = CreateObject('component','fplAccount')>

		<!--- get account number of this phone --->
		<cfif arguments.accountNo EQ "">
			<cfset qAccount = oFPLAccount.getAccount(ContactString=arguments.contactString)>
			<cfif qAccount.recordcount GT 0>
				<!--- get first account number as per discussion with Shilpi and other Service dev team --->
				<cfset thisAccountNo = qAccount.AccountId_vch>
			<cfelse>
				<!--- use fake account no --->
				<cfset thisAccountNo = "0123456789">
			</cfif>
		<cfelse>
			<cfset thisAccountNo = arguments.accountNO>
		</cfif>

		<!--- make sure that account number is of length even if its a fake account		 --->
		<cfif len(thisAccountNO) LT 10>
			<cfset thisAccountNO = repeatstring("0",10-len(thisAccountNO)) & thisAccountNO>
		</cfif>


		<cfset FPLURL = "#getFPLServiceURL()#/#thisAccountNo#/phoneOptIn/#left(arguments.contactString,3)#-#mid(arguments.contactString,4,3)#-#right(arguments.contactString,4)#">

		<!--- build JSON to send. Date/time is not required for opt out --->
		<cfif arguments.optIn EQ 1>
			<!--- 01/01/2015 10:00:00AM --->
			<cfset arguments.optDate = PSTtoEST(arguments.optDate)>
			<cfset stJSON = '{"channel":"messagebroadcast", "termsAndConditionsAcceptDate":"' & dateformat(arguments.optDate,'mm/dd/yyyy') & " " & timeformat(arguments.optDate,'hh:mm:sstt') & '"}'>
			<cfset requestMethod = "POST">
		<cfelse>
			<cfset stJSON = '{"channel":"messagebroadcast"}'>
			<cfset requestMethod = "DELETE">
		</cfif>

		<cfhttp url="#FPLURL#" method="#requestMethod#" result="httpResult" timeout="20">

			<cfhttpparam type="header" name="Accept" value="application/json">
			<cfhttpparam type="header" name="Content-Type" value="application/json">
			<!---
				<cfhttpparam type="header" name="Username" value="messagebroadcast">
				<cfhttpparam type="header" name="Password" value="passw0rd">
			--->
			<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

			<cfif not arguments.optIn>
				<cfhttpparam type="header" name="channel" value="messagebroadcast">
			</cfif>

			<cfhttpparam type="body" value="#stJSON#">

		</cfhttp>

		<!--- create string to log --->
		<cfset thisResult = "PostToFPL;#FPLURL#;#httpResult.StatusCode#;#arguments.contactString#;#arguments.optIn#;#thisAccountNo#;">

		<cfif isdefined("httpResult.FileContent")>
			<cfset thisResult = thisResult & httpResult.FileContent.toString()>
		</cfif>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="FPL_API_CALL", serviceData = thisResult)>

		<!--- TODO Remove this after QA --->
					<cfif isProd()>
						<cfhttp url="https://webqa.fpl.com/api/partners/preferences/#thisAccountNo#/phoneOptIn/#left(arguments.contactString,3)#-#mid(arguments.contactString,4,3)#-#right(arguments.contactString,4)#" method="#requestMethod#" result="httpResult" timeout="20">

							<cfhttpparam type="header" name="Accept" value="application/json">
							<cfhttpparam type="header" name="Content-Type" value="application/json">
							<!---
								<cfhttpparam type="header" name="Username" value="messagebroadcast">
								<cfhttpparam type="header" name="Password" value="passw0rd">
							--->
							<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

							<cfif not arguments.optIn>
								<cfhttpparam type="header" name="channel" value="messagebroadcast">
							</cfif>

							<cfhttpparam type="body" value="#stJSON#">

						</cfhttp>

						<!--- create string to log --->
						<cfset thisResult = "PostToFPL;webqa.fpl.com;#httpResult.StatusCode#;#arguments.contactString#;#arguments.optIn#;#thisAccountNo#;">

						<cfif isdefined("httpResult.FileContent")>
							<cfset thisResult = thisResult & httpResult.FileContent.toString()>
						</cfif>

						<!---Lot it into table--->
						<cfset logServiceCall(serviceType="FPL_API_CALL", serviceData = thisResult)>


					</cfif>

					<cfhttp url="https://webtest.fpl.com/api/partners/preferences/#thisAccountNo#/phoneOptIn/#left(arguments.contactString,3)#-#mid(arguments.contactString,4,3)#-#right(arguments.contactString,4)#" method="#requestMethod#" result="httpResult" timeout="20">

						<cfhttpparam type="header" name="Accept" value="application/json">
						<cfhttpparam type="header" name="Content-Type" value="application/json">
						<!---
							<cfhttpparam type="header" name="Username" value="messagebroadcast">
							<cfhttpparam type="header" name="Password" value="passw0rd">
						--->
						<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

						<cfif not arguments.optIn>
							<cfhttpparam type="header" name="channel" value="messagebroadcast">
						</cfif>

						<cfhttpparam type="body" value="#stJSON#">

					</cfhttp>

					<!--- create string to log --->
					<cfset thisResult = "PostToFPL;webtest.fpl.com;#httpResult.StatusCode#;#arguments.contactString#;#arguments.optIn#;#thisAccountNo#;">

					<cfif isdefined("httpResult.FileContent")>
						<cfset thisResult = thisResult & httpResult.FileContent.toString()>
					</cfif>

					<!---Lot it into table--->
					<cfset logServiceCall(serviceType="FPL_API_CALL", serviceData = thisResult)>
		<!--- TODO Remove this after QA --->

		<cfreturn httpResult.StatusCode>

	</cffunction>

	<!--- just add opt-out entry in database. No MTO initiations/conf etc --->
	<cffunction name="addOptOutEntry" access="remote" output="true">
		<cfargument name="inpContactString" required="true" type="string">
		<cfargument name="inpShortCode" required="false" type="string" default="#getmyFPLShortCode()#">
		<cfargument name="inpOptOutSource" required="false" type="string" default="FPL">
		<cfargument name="inpOptOutDate" required="false" type="string" default="">
		<cfargument name="inpUserId" required="false" type="string" default="#getFPLEBMUserId()#">
		<cfargument name="inpBatchId" required="false" type="string" default="#getBatchId()#">

		 <cfset sReturn = getReturnStruct()>

		<cftry>

			<cfif arguments.inpContactString GT 0>

			    <!---authenticate EBM API--->
				<cfset local.sign = generateSignature("POST") />
		        <cfset datetime = local.sign.DATE>
		        <cfset signature = local.sign.SIGNATURE>
		        <cfset authorization = getAccessKey() & ":" & signature>

		    	<!---call opt out service--->
		        <cfhttp url="#getEBMApiUrlBase()#/ebm/opt/out" method="POST" result="returnStruct" >

		           <!--- By default EBM API will return json or XML --->
		           <cfhttpparam name="Accept" type="header" value="application/json" />
		           <cfhttpparam type="header" name="datetime" value="#datetime#" />
		           <cfhttpparam type="header" name="authorization" value="#authorization#" />

					<!---Required fields--->
		           	<cfhttpparam type="formfield" name="inpContactString" value="#arguments.inpContactString#" />
		           	<cfhttpparam type="formfield" name="inpShortCode" value="#arguments.inpShortCode#" />
		           	<cfhttpparam type="formfield" name="inpOptOutSource" value="#arguments.inpOptOutSource#" />
					<cfhttpparam type="formfield" name="inpUserId" value="#arguments.inpUserId#" />
					<cfhttpparam type="formfield" name="inpBatchId" value="#arguments.inpBatchId#" />


		           <cfif isdate(arguments.inpOptOutDate)>
			           <cfhttpparam type="formfield" name="inpOptOutDate" value="#arguments.inpOptOutDate#" />
		           </cfif>

		        </cfhttp>

		        <cfset sResult = DeSerializeJSON(returnStruct.FileContent)>

		        <!---If there was no error, send back no error else send error code--->
		        <cfif sResult.RXRESULTCODE GT 0>

		        <cfelse>
	                <!---set error in struct--->
	                <cfset sReturn.ErrMsg = sResult.RXRESULTCODE>
	                <cfset sReturn.ErrCode = -1>
		        </cfif>

			<cfelse>
                <!---set error in struct--->
                <cfset sReturn.ErrMsg = "Invalid Contact String">
                <cfset sReturn.ErrCode = -1>
			</cfif>

			<cfcatch type="any">

	            <cfset SubjectLine = "FPL UNICA - Error in addOptOutEntry service on server #CGI.SERVER_NAME#">
	            <cfset ENA_Message = "Error while trying to addOptOutEntry request">
       			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                <!---set error in struct--->
                <cfset sReturn.ErrMsg = cfcatch.Message>
                <cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<cfreturn SerializeJson(sReturn)>


	</cffunction>


	<!--- just add opt-in entry in database. No MTO initiations/conf etc --->
	<cffunction name="addOptInEntry" access="remote" output="true">
		<cfargument name="inpContactString" required="true" type="string">
		<cfargument name="inpShortCode" required="false" type="string" default="#getmyFPLShortCode()#">
		<cfargument name="inpOptInSource" required="false" type="string" default="FPL">
		<cfargument name="inpOptInDate" required="false" type="string" default="">
		<cfargument name="inpUserId" required="false" type="string" default="#getFPLEBMUserId()#">
		<cfargument name="inpBatchId" required="false" type="string" default="#getBatchId()#">

		 <cfset sReturn = getReturnStruct()>

		<cftry>

			<cfif arguments.inpContactString GT 0>

			    <!---authenticate EBM API--->
				<cfset local.sign = generateSignature("POST") />
		        <cfset datetime = local.sign.DATE>
		        <cfset signature = local.sign.SIGNATURE>
		        <cfset authorization = getAccessKey() & ":" & signature>

		    	<!---call opt out service--->
		        <cfhttp url="#getEBMApiUrlBase()#/ebm/opt/in" method="POST" result="returnStruct" >

		           <!--- By default EBM API will return json or XML --->
		           <cfhttpparam name="Accept" type="header" value="application/json" />
		           <cfhttpparam type="header" name="datetime" value="#datetime#" />
		           <cfhttpparam type="header" name="authorization" value="#authorization#" />

					<!---Required fields--->
		           	<cfhttpparam type="formfield" name="inpContactString" value="#arguments.inpContactString#" />
		           	<cfhttpparam type="formfield" name="inpShortCode" value="#arguments.inpShortCode#" />
		           	<cfhttpparam type="formfield" name="inpOptInSource" value="#arguments.inpOptInSource#" />
					<cfhttpparam type="formfield" name="inpUserId" value="#arguments.inpUserId#" />
					<cfhttpparam type="formfield" name="inpBatchId" value="#arguments.inpBatchId#" />


		           <cfif isdate(arguments.inpOptInDate)>
			           <cfhttpparam type="formfield" name="inpOptInDate" value="#arguments.inpOptInDate#" />
		           </cfif>

		        </cfhttp>

		        <cfset sResult = DeSerializeJSON(returnStruct.FileContent)>

		        <!---If there was no error, send back no error else send error code--->
		        <cfif sResult.RXRESULTCODE GT 0>

		        <cfelse>
	                <!---set error in struct--->
	                <cfset sReturn.ErrMsg = sResult.RXRESULTCODE>
	                <cfset sReturn.ErrCode = -1>
		        </cfif>

			<cfelse>

                <!---set error in struct--->
                <cfset sReturn.ErrMsg = "Invalid Contact String">
                <cfset sReturn.ErrCode = -1>

			</cfif>

			<cfcatch type="any">

	            <cfset SubjectLine = "FPL UNICA - Error in addOptInEntry service on server #CGI.SERVER_NAME#">
	            <cfset ENA_Message = "Error while trying to addOptInEntry request">
       			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                <!---set error in struct--->
                <cfset sReturn.ErrMsg = cfcatch.Message>
                <cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<cfreturn SerializeJson(sReturn)>


	</cffunction>

</cfcomponent>

