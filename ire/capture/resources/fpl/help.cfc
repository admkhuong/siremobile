<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/help" hint="API to get proper phone number related to area code">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->


	<cffunction name="post" access="public" output="false">

		<cfset var thisAreaCode = "">

<!---
Data coming in this post as JSON
{"phoneNumber":"{%SRCONTACTSTRING%}","helpmsg": "FPL Alert: Go to FPL.com/TEXTHELP to review FAQ's or call DYNAMIC_PHONE_NUMBER_HERE for customer service. Alert frequency varies. Msg & data rates may apply. STOP to cancel"}
 --->


		<!---get request data --->
		<cfif isBinary(GetHTTPRequestData().content)>
			<cfset inData = ToString(GetHTTPRequestData().content)>
		<cfelse>
			<cfset inData = GetHTTPRequestData().content>
		</cfif>

		<cfset sData = DeserializeJSON(inData)>

		<cfset thisAreaCode = left(sData.phonenumber,3)>

		<!--- -1 area code is default help number --->
		<cfquery name="qHelpNo" datasource="Bishop">
			SELECT *
			FROM simplexprogramdata.portal_link_c10_p1_help_numbers
			WHERE AreaCode_vch in ('-1','#thisAreaCode#')
			ORDER BY PKID_bi desc
		</cfquery>


		<cfset var dataOut = replaceNoCase(sData.helpmsg,'DYNAMIC_PHONE_NUMBER_HERE',qHelpNo.Residential_vch)>

		<cfreturn representationOf(dataOut).withStatus(200) />

	</cffunction>

</cfcomponent>