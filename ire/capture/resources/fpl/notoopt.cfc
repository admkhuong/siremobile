<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/notoopt" hint="No aswer after JOIN  question">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->


	<cffunction name="get" access="public" output="false">

		<cfset var dataOut = "">

		<cfif not isdefined("url.phoneNumber") OR trim(url.phonenumber) EQ "">
			<cfset url.phoneNumber = "9999999999">
		</cfif>

		<cfthread action="run" name="FPL#CreateUUID()#" URLphoneNumber="#url.phoneNumber#">

			<cfset var thisAccountNo = "">
			<cfset var thisPhoneNumber = REReplacenocase(attributes.URLphoneNumber,"[^0-9]","","all")>
			<cfset var oFPLAccount = CreateObject('component','fplAccount')>

			<!--- get account number of this phone --->
			<cfset qAccount = oFPLAccount.getAccount(ContactString=thisPhoneNumber)>
			<cfif qAccount.recordcount GT 0>
				<!--- get first account number as per discussion with Shilpi and other Service dev team --->
				<cfset thisAccountNo = qAccount.AccountId_vch>
			<cfelse>
				<!--- use fake account no --->
				<cfset thisAccountNo = "0123456789">
			</cfif>

			<!--- make sure that account number is of length even if its a fake account		 --->
			<cfif len(thisAccountNO) LT 10>
				<cfset thisAccountNO = repeatstring("0",10-len(thisAccountNO)) & thisAccountNO>
			</cfif>

			<cfset FPLURL = "#oFPLAccount.getFPLServiceURL()#/#thisAccountNo#/phoneOptIn/#left(thisPhoneNumber,3)#-#mid(thisPhoneNumber,4,3)#-#right(thisPhoneNumber,4)#">

			<!--- build JSON to send. this will always be opt-out --->
			<cfset stJSON = '{"channel":"messagebroadcast"}'>
			<cfset requestMethod = "DELETE">

			<cfhttp url="#FPLURL#" method="#requestMethod#" result="httpResult" timeout="20">

				<cfhttpparam type="header" name="Accept" value="application/json">
				<cfhttpparam type="header" name="Content-Type" value="application/json">
				<!---
					<cfhttpparam type="header" name="Username" value="messagebroadcast">
					<cfhttpparam type="header" name="Password" value="passw0rd">
				--->
				<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

				<cfhttpparam type="header" name="channel" value="messagebroadcast">

				<cfhttpparam type="body" value="#stJSON#">

			</cfhttp>

			<!--- create string to log --->
			<cfset thisResult = "PostToFPL;#httpResult.StatusCode#;#thisPhoneNumber#;no_to_join;#thisAccountNo#;">
			<cfif isdefined("httpResult.FileContent")>
				<cfset thisResult = thisResult & httpResult.FileContent.toString()>
			</cfif>

			<!--- TODO Remove this when going live --->
			<cfif oFPLAccount.isProd()>
				<cfhttp url="https://webqa.fpl.com/api/partners/preferences/#thisAccountNo#/phoneOptIn/#left(thisPhoneNumber,3)#-#mid(thisPhoneNumber,4,3)#-#right(thisPhoneNumber,4)#" method="#requestMethod#" result="httpResult" timeout="20">

					<cfhttpparam type="header" name="Accept" value="application/json">
					<cfhttpparam type="header" name="Content-Type" value="application/json">
					<!---
						<cfhttpparam type="header" name="Username" value="messagebroadcast">
						<cfhttpparam type="header" name="Password" value="passw0rd">
					--->
					<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

					<cfhttpparam type="header" name="channel" value="messagebroadcast">

					<cfhttpparam type="body" value="#stJSON#">

				</cfhttp>
			</cfif>

			<cfhttp url="https://webtest.fpl.com/api/partners/preferences/#thisAccountNo#/phoneOptIn/#left(thisPhoneNumber,3)#-#mid(thisPhoneNumber,4,3)#-#right(thisPhoneNumber,4)#" method="#requestMethod#" result="httpResult" timeout="20">

				<cfhttpparam type="header" name="Accept" value="application/json">
				<cfhttpparam type="header" name="Content-Type" value="application/json">
				<!---
					<cfhttpparam type="header" name="Username" value="messagebroadcast">
					<cfhttpparam type="header" name="Password" value="passw0rd">
				--->
				<cfhttpparam type="header" name="Authorization" value="Basic bWVzc2FnZWJyb2FkY2FzdDpwYXNzdzByZA==">

				<cfhttpparam type="header" name="channel" value="messagebroadcast">

				<cfhttpparam type="body" value="#stJSON#">

			</cfhttp>
			<!--- TODO Remove this when going live --->

			<!---Lot it into table--->
			<cfset oFPLAccount.logServiceCall(serviceType="FPL_API_CALL_NO_TO_OPT", serviceData = thisResult)>

		</cfthread>

		<cfreturn representationOf(dataOut).withStatus(200) />

	</cffunction>

</cfcomponent>