<cfcomponent extends="taffy.core.resource" taffy:uri="/fpl/opt" hint="FPL Opt-In and Opt-Out">

	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->

	<!--- <cffunction name="postOLD" access="public" output="false" hint="" returnformat="JSON">

 		<cfcontent type="application/json" reset="yes">

		<!---get request data --->
		<cfset inJSON = GetHTTPRequestData().content>


		<cfhttp url="http://dev10.telespeech.com/atif/fplstaging/cfcs/fplopt.cfc?method=opt" method="post" result="httpResp" timeout="60">
		    <cfhttpparam type="header" name="Content-Type" value="text/json" />
		    <cfhttpparam type="body" value="#inJSON#">
		</cfhttp>

		<cfreturn representationOf(DESerializeJSON(httpResp.filecontent.toString())).withStatus(200) />

	</cffunction> --->

	<cffunction name="post" access="public" output="false" hint="" returnformat="JSON">


		<!---get request data --->
		<cfif isBinary(GetHTTPRequestData().content)>
			<cfset inJSON = ToString(GetHTTPRequestData().content)>
		<cfelse>
			<cfset inJSON = GetHTTPRequestData().content>
		</cfif>

		<cfset oOpt = CreateObject("component","fplopt")>
		<cfset oResult = oOpt.Opt(inJSON)>

		<cfreturn representationOf(DeSerializeJSON(oResult)).withStatus(200) />

	</cffunction>

</cfcomponent>