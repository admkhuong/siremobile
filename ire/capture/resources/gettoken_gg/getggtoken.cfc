<cfcomponent extends="taffy.core.resource" taffy:uri="/google/gettoken" hint="Capture gg token">

    <cfinclude template="../../paths.cfm" >

	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		
        <cfset var dataOut = {}>		

        <!---  --->
        <cfset dataOut.RXRESULTCODE = "-1" />

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>


     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		<cfparam name="token" default="123">  
        <cfset var dataout = {} />  
        <cfset dataout.TOKEN=token>
        <cfset dataout.TIME=now()>
        
        <cfset var GetAvailableToken=''>
        <cfset var InsertGGToken=''>
		<cfset var DBSourceEBM = "Bishop"/> 
        <cfquery name="GetAvailableToken" datasource="#DBSourceEBM#">
            SELECT
                Value_txt
            FROM
                 simpleobjects.sire_setting
            WHERE
                VariableName_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="googletoken">
        </cfquery>
        <cfif GetAvailableToken.recordcount GT 0>
            <cfquery name="InsertGGToken" datasource="#DBSourceEBM#">
                UPDATE simpleobjects.sire_setting
                SET 
                    Value_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#token#">
                WHERE
                    VariableName_txt=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="googletoken">                
            </cfquery>
        <cfelse>
            <cfquery name="InsertGGToken" datasource="#DBSourceEBM#">
                INSERT INTO simpleobjects.sire_setting
                (
                Value_txt,
                VariableName_txt
                )
                VALUES
                (
                    
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#token#">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="googletoken">
                
                )
            </cfquery>
        </cfif>
        <!---
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                        
        --->        
		<cfreturn representationOf(dataout).withStatus(200) />
        
	</cffunction>
</cfcomponent>