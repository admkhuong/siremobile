<cfcomponent extends="taffy.core.resource" taffy:uri="/integrate/signup" hint="Sign up a sire user to a free account">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Sign up a sire user to a free account"> 		 		
 		<cfargument name="inpFirst" type="string" required="false" default="New User" hint="The full user name - expected format FIRST LAST"/>
        <cfargument name="inpLast" type="string" required="false" default="New User" hint="The full user name - expected format FIRST LAST"/>       
        <cfargument name="inpPassword" required="false" default="" type="string">
        <cfargument name="inpSMSNumber" required="true" default="" type="string">
        <cfargument name="inpEmail" type="string" required="true" default="" hint="eMail address to validate"/>        
        <cfargument name="DebugAPI" required="no" default="0">
		
		<cfargument name="inpIntegrateCompanyToken" required="yes" default="">
		<cfargument name="inpIntegrateCompanyName" required="yes" default="">		
		<cfargument name="inpIntegrateHigherUserID" required="yes" default="0">
		<cfargument name="inpIntegrateUserID" required="yes" default="">
		<cfargument name="inpIntegrateUserLevel" required="yes" default="1">				
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var RetVarRegisterNewAccountNew = 'true' />
                     
		<cfset var DebugStr = "" />		
       
        <!--- Info: The Sesioon.UserId is set based on the authticated user by the custom API authentication method --->
        
        <!--- Set the default Session DB Source --->
        <cfset DBSourceEBM = "Bishop"/>
        <cfset inpAccountType="Free"/>
		<cfif arguments.inpIntegrateUserLevel EQ "">
			<cfset arguments.inpIntegrateUserLevel="1"/>
		</cfif>
                        
 		<cftry>
		 	<cfset Session.DBSourceEBM = "Bishop"/>
			<cfif arguments.inpPassword EQ "">
				<cfset arguments.inpPassword="SireIntegrate@PW2017">
			</cfif>		 				
		 				

			<cfif arguments.inpIntegrateUserID EQ 0 OR trim(arguments.inpIntegrateUserID) EQ "">
				<cfthrow type="Any" message="inpIntegrateUserID is required" detail="inpIntegrateUserID is required">			
			</cfif>
	 		
	 		<!--- <cfset Session.COMPANYUSERID = "Session.UserId"/> --->
			 <cfif arguments.inpIntegrateUserLevel GT 1 AND (arguments.inpIntegrateHigherUserID EQ 0 OR trim(arguments.inpIntegrateHigherUserID) EQ "")>
					<cfthrow type="Any" message="For User Level #arguments.inpIntegrateUserLevel#, inpIntegrateHigherUserID is required!" detail="For User Level #arguments.inpIntegrateUserLevel#, inpIntegrateHigherUserID is required!">				
			 </cfif>
			<cfset HttpRequest=getHTTPRequestData()>
			<cfset HostRequest= HttpRequest.headers.host>
			<cfset AllowCallFrom="">
			<cfquery name="GetIntegrateCompanyID" datasource="#Session.DBSourceEBM#">
				SELECT 	ID_int,
						AllowCallFrom_Vch
				FROM
					simpleobjects.integrate_company
				WHERE
					Token_Integrate_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpIntegrateCompanyToken#">
			</cfquery>  	
			<cfif GetIntegrateCompanyID.RECORDCOUNT EQ 0>
				<cfthrow type="Any" message="Invalid Integrate Token" detail="Invalid Integrate Token">
			<cfelse>
				<cfset IntegrateCompanyID=GetIntegrateCompanyID.ID_int>
				<cfset AllowCallFrom=GetIntegrateCompanyID.AllowCallFrom_Vch>
			</cfif>	 
			<cfif application.Env AND Len(AllowCallFrom) GT 0>
				<cfif lcase(cgi.REMOTE_ADDR) NEQ lcase(Trim(AllowCallFrom))>
					<cfthrow type="Any" message="Invalid Request, We not support call from #cgi.REMOTE_ADDR#" detail="Invalid Request, We not support call from #cgi.REMOTE_ADDR#">			
				</cfif>
			</cfif>

			<!--- valid higher user level--->
			<cfset SireHigherUserID=""/>
			<cfif arguments.inpIntegrateHigherUserID GT 0>
				<cfquery name="GetHigherLevelUserInfo" datasource="#Session.DBSourceEBM#">
					SELECT UserId_int
					FROM
						simpleobjects.useraccount
					WHERE
						IntegrateUserID_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateHigherUserID#">
					AND
						UserLevel_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserLevel -1 #">
					AND
						IntegrateCompanyID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#IntegrateCompanyID#">
				</cfquery>
				<cfif GetHigherLevelUserInfo.RECORDCOUNT EQ 0>
					<cfthrow type="Any" message="Have no inpIntegrateHigherUserID (#arguments.inpIntegrateHigherUserID#) at level #arguments.inpIntegrateUserLevel-1#" detail="Invalid Higher Level User ID">
				<cfelse>
					<cfset SireHigherUserID=GetHigherLevelUserInfo.UserId_int/>
				</cfif>
			</cfif>

			<cfset inpPlanId=1> 
			<cfif arguments.inpIntegrateUserLevel EQ 3 OR arguments.inpIntegrateUserLevel EQ 2>
				<cfquery name="GetIntegratePlanID" datasource="#Session.DBSourceEBM#">
					SELECT PlanId_int
					FROM
						simpleobjects.integrate_company
					WHERE
						ID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#IntegrateCompanyID#">
					LIMIT 1
				</cfquery> 
				<cfif GetIntegratePlanID.PlanId_int EQ 0 OR GetIntegratePlanID.PlanId_int EQ "">
					<cfthrow type="Any" message="Have no plan for integrate" detail="Have no plan for integrate">
				<cfelse>
					<cfset inpPlanId=GetIntegratePlanID.PlanId_int>
				</cfif>	 
			</cfif>

			<cfquery name="CheckExistUser" datasource="#Session.DBSourceEBM#">
				SELECT UserId_int
				FROM
					simpleobjects.useraccount
				WHERE
					IntegrateUserID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserID#">
				AND
					IntegrateCompanyID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#IntegrateCompanyID#">
			</cfquery>
			<cfif CheckExistUser.RECORDCOUNT GT 0>
				<cfthrow type="Any" message="This user id (#arguments.inpIntegrateUserID#) have already created before that" detail="This user id have already created before that">			
			</cfif>
			<cfset inpUnlimitedCredit=0>
			<cfinvoke method="RegisterNewAccountNew" component="public.sire.models.cfc.userstools" returnvariable="RetVarRegisterNewAccountNew">
				<cfinvokeargument name="INPMAINEMAIL" value="#inpEmail#">
				<cfinvokeargument name="INPFNAME" value="#inpFirst#">
				<cfinvokeargument name="INPLNAME" value="#inpLast#">
				<cfinvokeargument name="INPNAPASSWORD" value="#inpPassword#">
				<cfinvokeargument name="INPACCOUNTTYPE" value="#inpAccountType#">
				<cfinvokeargument name="INSMSNUMBER" value="#inpSMSNumber#">
				<cfinvokeargument name="INPTIMEZONE" value="31">
				<cfinvokeargument name="inpPlanId" value="#inpPlanId#">			

				<cfinvokeargument name="inpAllowDuplicateMail" value="1">
				<cfinvokeargument name="inpAllowDuplicatePhone" value="1"> 									
								
				<cfinvokeargument name="inpUnlimitedCredit" value="#inpUnlimitedCredit#">
				<cfinvokeargument name="IntegrateCompanyID" value="#IntegrateCompanyID#">
				<cfinvokeargument name="inpUserType" value="2">
				<cfinvokeargument name="MFAEnable" value="0">				
				<cfinvokeargument name="inpIntegrateHigherUserID" value="#inpIntegrateHigherUserID#">
				<cfinvokeargument name="inpIntegrateUserID" value="#inpIntegrateUserID#">
				<cfinvokeargument name="inpIntegrateUserLevel" value="#inpIntegrateUserLevel#">								
				<cfinvokeargument name="INPCOMPANYNAME" value="#inpIntegrateCompanyName#">
				<cfinvokeargument name="INPORGNAME" value="#inpIntegrateCompanyName#">		
				<cfinvokeargument name="inpHigherUserID" value="#SireHigherUserID#">		

						
				
			</cfinvoke> 			
			
            <cfset dataout = RetVarRegisterNewAccountNew />
			<cfif RetVarRegisterNewAccountNew.USERID GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />        
			</cfif>
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>