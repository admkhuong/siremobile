<cfcomponent extends="taffy.core.resource" taffy:uri="/integrate/updateuserlevel" hint="Up date user level">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Update user level">
 		 		 
        <cfargument name="DebugAPI" required="no" default="0">		
		<cfargument name="inpIntegrateCompanyToken" required="yes" default="">		
		<cfargument name="inpIntegrateUserID" required="yes" default="">
		<cfargument name="inpIntegrateHigherUserID" required="yes" default="0">		
		<cfargument name="inpIntegrateUserLevel" required="yes" default="1">
		
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />        
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />		
        
                     
		<cfset var DebugStr = "" />
				               
        <cfset DBSourceEBM = "Bishop"/>
        
                        
 		<cftry>
 		
	 		<cfset Session.DBSourceEBM = "Bishop"/>
	 		<!--- <cfset Session.COMPANYUSERID = "Session.UserId"/> --->	
			 <cfif trim(arguments.inpIntegrateUserLevel) EQ "" OR arguments.inpIntegrateUserLevel EQ 0>
				<cfthrow type="Any" message="inpIntegrateUserLevel is required!" detail="inpIntegrateUserLevel is required!">
			 </cfif>
			 <cfif trim(arguments.inpIntegrateHigherUserID) EQ "">
				<cfthrow type="Any" message="inpIntegrateHigherUserID is required!" detail="inpIntegrateHigherUserID is required!">
			 </cfif>
			<cfif arguments.inpIntegrateUserLevel EQ 1>
				<cfset arguments.inpIntegrateHigherUserID="">
			</cfif>
			<cfset HttpRequest=getHTTPRequestData()>
			<cfset HostRequest= HttpRequest.headers.host>
			<cfset AllowCallFrom="">
			<cfquery name="GetIntegrateCompanyID" datasource="#Session.DBSourceEBM#">
				SELECT 	ID_int,
						AllowCallFrom_Vch
				FROM
					simpleobjects.integrate_company
				WHERE
					Token_Integrate_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpIntegrateCompanyToken#">
			</cfquery>  	
			<cfif GetIntegrateCompanyID.RECORDCOUNT EQ 0>
				<cfthrow type="Any" message="Invalid Integrate Token" detail="Invalid Integrate Token">
			<cfelse>
				<cfset IntegrateCompanyID=GetIntegrateCompanyID.ID_int>
				<cfset AllowCallFrom=GetIntegrateCompanyID.AllowCallFrom_Vch>
			</cfif>	 
			<cfif application.Env AND Len(AllowCallFrom) GT 0>
				<cfif lcase(cgi.REMOTE_ADDR) NEQ lcase(Trim(AllowCallFrom))>
					<cfthrow type="Any" message="Invalid Request, We not support call from #cgi.REMOTE_ADDR#" detail="Invalid Request, We not support call from #cgi.REMOTE_ADDR#">			
				</cfif>
			</cfif>

			<cfset UserID="">
			<cfquery name="GetUserID" datasource="#Session.DBSourceEBM#">
				SELECT UserId_int
				FROM
					simpleobjects.useraccount
				WHERE
					IntegrateUserID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserID#">	
				AND
					IntegrateCompanyID_int in (
						SELECT 	ID_int
						FROM	simpleobjects.integrate_company
						WHERE	Token_Integrate_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpIntegrateCompanyToken#">
						AND		Status_ti=1
					)
			</cfquery>
			<cfif GetUserID.RECORDCOUNT EQ 0>
				<cfthrow type="Any" message="Invalid Integrate Token or User ID" detail="Invalid Integrate Token or User ID">
			<cfelse>
				<cfset UserID=GetUserID.UserId_int>
			</cfif>	
			<!--- check if have subacc then not allow update level--->
			<cfquery name="GetAllSubAcc" datasource="#Session.DBSourceEBM#">
				SELECT count(UserId_int) as TotalSubAcc
				FROM
					simpleobjects.useraccount
				WHERE
					IntegrateHigherUserID= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserID#">
				AND
					IntegrateCompanyID_int=  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#IntegrateCompanyID#">
			</cfquery>
			<cfif GetAllSubAcc.TotalSubAcc GT 0>
				<cfthrow type="Any" message="We not allow update level for user exists subaccount" detail="We not allow update level for user exists subaccount">			
			</cfif>	
			<!--- Get New HigherUserInfo--->
			<cfset NewSireHighUserID=""/>
			<cfquery name="GetNewHigherInfo" datasource="#Session.DBSourceEBM#">
				SELECT 	UserId_int
				FROM	simpleobjects.useraccount
				WHERE	IntegrateUserID_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateHigherUserID#">
				AND		IntegrateCompanyID_int=  <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#IntegrateCompanyID#">	
				AND		UserLevel_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserLevel -1#">	
			</cfquery>
			<cfif arguments.inpIntegrateUserLevel GT 1>
				<cfif GetNewHigherInfo.RECORDCOUNT EQ 0>
					<cfthrow type="Any" message="Have no inpIntegrateHigherUserID (#arguments.inpIntegrateHigherUserID#) at level #arguments.inpIntegrateUserLevel-1#" detail="Have no inpIntegrateHigherUserID (#arguments.inpIntegrateHigherUserID#) at level #arguments.inpIntegrateUserLevel-1#">			
				<cfelse>
					<cfset NewSireHighUserID=GetNewHigherInfo.UserId_int/>
				</cfif>
			</cfif>
			<cfquery name="UpdateUserLevel" datasource="#Session.DBSourceEBM#">
				UPDATE 	simpleobjects.useraccount
				SET 	
					UserLevel_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserLevel#">,
					IntegrateHigherUserID=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateHigherUserID#">,
					HigherUserID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#NewSireHighUserID#">
				WHERE
					UserId_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#UserID#">	
			</cfquery>  	
			<cfset dataout.MESSAGE = "Update Successfully" />             
			<cfset dataout.RXRESULTCODE = 1 />			
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>