<cfcomponent extends="taffy.core.resource" taffy:uri="/integrate/usersetthreshold" hint="Up date user ">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Update user">
 		 		 
        <cfargument name="DebugAPI" required="no" default="0">		
		<cfargument name="inpIntegrateCompanyToken" required="yes" default="">		
		<cfargument name="inpIntegrateUserID" required="yes" default="">
		<cfargument name="inpThreshold" required="yes" default="0">				
		
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />        
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />		
        
                     
		<cfset var DebugStr = "" />
				               
        <cfset DBSourceEBM = "Bishop"/>
        
                        
 		<cftry>
 		
	 		<cfset Session.DBSourceEBM = "Bishop"/>	 		
			<cfif trim(arguments.inpThreshold) EQ "">
				<cfset arguments.inpThreshold=0>
			</cfif>

			<cfset HttpRequest=getHTTPRequestData()>
			<cfset HostRequest= HttpRequest.headers.host>
			<cfset AllowCallFrom="">
			<cfquery name="GetIntegrateCompanyID" datasource="#Session.DBSourceEBM#">
				SELECT 	ID_int,
						AllowCallFrom_Vch
				FROM
					simpleobjects.integrate_company
				WHERE
					Token_Integrate_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpIntegrateCompanyToken#">
			</cfquery>  	
			<cfif GetIntegrateCompanyID.RECORDCOUNT EQ 0>
				<cfthrow type="Any" message="Invalid Integrate Token" detail="Invalid Integrate Token">
			<cfelse>
				<cfset IntegrateCompanyID=GetIntegrateCompanyID.ID_int>
				<cfset AllowCallFrom=GetIntegrateCompanyID.AllowCallFrom_Vch>
			</cfif>	 
			<cfif application.Env AND Len(AllowCallFrom) GT 0>
				<cfif lcase(cgi.REMOTE_ADDR) NEQ lcase(Trim(AllowCallFrom))>
					<cfthrow type="Any" message="Invalid Request, We not support call from #cgi.REMOTE_ADDR#" detail="Invalid Request, We not support call from #cgi.REMOTE_ADDR#">			
				</cfif>
			</cfif>
			
			<cfset UserID="">
			<cfquery name="GetUserID" datasource="#Session.DBSourceEBM#">
				SELECT UserId_int
				FROM
					simpleobjects.useraccount
				WHERE
					IntegrateUserID_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpIntegrateUserID#">	
				AND
					IntegrateCompanyID_int in (
						SELECT 	ID_int
						FROM	simpleobjects.integrate_company
						WHERE	Token_Integrate_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpIntegrateCompanyToken#">
						AND		Status_ti=1
					)
			</cfquery>
			<cfif GetUserID.RECORDCOUNT EQ 0>
				<cfthrow type="Any" message="Invalid Integrate Token or User ID" detail="Invalid Integrate Token or User ID">
			<cfelse>
				<cfset UserID=GetUserID.UserId_int>
			</cfif>	

			<cfquery name="UpdateUser" datasource="#Session.DBSourceEBM#">
				UPDATE 	simpleobjects.useraccount
				SET 	
					ThresholdAlerts_int=<CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.inpThreshold#">					
				WHERE
					UserId_int= <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#UserID#">	
			</cfquery>  	
			<cfset dataout.MESSAGE = "Update Successfully" />             
			<cfset dataout.RXRESULTCODE = 1 />			
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>