<cfcomponent extends="taffy.core.resource" taffy:uri="/interest/mortgage" hint="Evaluates the estimated payment for given amounts ">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Evaluate for payment - Returns the monthly payment amount">
		<cfargument name="inpAmount" TYPE="string" required="yes" hint=""/>
		<cfargument name="inpMonths" TYPE="string" required="yes" hint=""/>
		<cfargument name="inpRate" TYPE="string" required="yes" hint=""/>
		<cfargument name="inpDownPaymentAmount" TYPE="string" required="no" default="0" hint=""/>
		<cfargument name="inpDownPayementPercentage" TYPE="string" required="no" default="0" hint=""/>
               		
		<cfset var dataOut = {}>
		<cfset var DBSourceEBM = "Bishop"/> 
		<cfset var monthly_interest = 0 />
	   	<cfset var monthly_payment = 0 />
                
        <!--- Formatted Date Time --->
        <cfset dataOut = "ERROR" />
      
		<cftry>


			<!--- NLP - cleanup inputs --->
			<cfset arguments.inpAmount = REReplace(trim(arguments.inpAmount),"[\$,]","","ALL") />
			<cfset arguments.inpRate = TRIM(Replace(arguments.inpRate,"%","","ALL")) />

			<!--- Accept days , months , years - scrub for number and then use it based on months --->

			<!--- M = P [ i(1 + i)n ] / [ (1 + i)n - 1] --->
           	
		   	<cfset monthly_interest = inpAmount / ( 12 * 100 ) />
		   	<cfset monthly_payment = inpAmount * (monthly_interest / (1 - ((1 + monthly_interest)^(-240))))> 
		   
<!---

			<cfset downpayment = Form.property_price * ( Form.down_payment / 100 ) />
		   	<cfset financed = Form.property_price - downpayment />
		   	
		   	Var years = 0; 
		    Var interestRate = 0; 
		    Var totalInterest = 0; 
		    principal = trim(principal); 
		    principal = REReplace(principal,"[\$,]","","ALL"); 
		    annualPercent = Replace(annualPercent,"%","","ALL"); 
		    interestRate = annualPercent / 100; 
		    years = months / 12; 
		    totalInterest = principal*(((1+ interestRate)^years)-1); 
		    Return DollarFormat(totalInterest); 
		    
		    <cfset dataOut = Evaluate(inpEquation) />
--->
        
        	 <cfset dataOut = monthly_payment />
            		   	                                   
        <cfcatch>        	
           
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type />
            <cfset dataout.MESSAGE = "#cfcatch.Message# #CGI.SERVER_NAME# #DBSourceEBM#" />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
    
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = {} />  
        
        <cfset dataout.MESSAGE = "You should use the verb POST JSON or XML to this API location. Currently you only using GET!" />
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>