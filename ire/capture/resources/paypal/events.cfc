<cfcomponent extends="taffy.core.resource" taffy:uri="/paypal/events" hint="">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="">
		
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />        
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />		
        
                     
		<cfset var DebugStr = "" />
				               
        <cfset DBSourceEBM = "Bishop"/>
        
                        
 		<cftry>
 		
	 		<cfset Session.DBSourceEBM = "Bishop"/>	 		
			<cfset HttpRequest=getHTTPRequestData().content>
			            
             
			<cfquery name="UpdateUser" datasource="#Session.DBSourceEBM#">
				INSERT INTO simplebilling.hunglogstest
                (
                    FileContent
                )
				VALUES
                (                    
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#HttpRequest#"/>
                ) 						
				
			</cfquery>  
			<cfset dataout.MESSAGE = "Successfully" />             
			<cfset dataout.RXRESULTCODE = 1 />			
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>        
		<cfreturn representationOf(dataout).withStatus(200) /> 


    </cffunction>
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />        
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />		
        
                     
		<cfset var DebugStr = "" />
				               
        <cfset DBSourceEBM = "Bishop"/>
        
                        
 		<cftry>
 		
	 		<cfset Session.DBSourceEBM = "Bishop"/>	 		
			<cfset HttpRequest=getHTTPRequestData().content>
			            
             
			<cfquery name="UpdateUser" datasource="#Session.DBSourceEBM#">
				INSERT INTO simplebilling.hunglogstest
                (
                    FileContent
                )
				VALUES
                (                    
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#HttpRequest#"/>
                ) 						
				
			</cfquery>  
			<cfset dataout.MESSAGE = "Successfully" />             
			<cfset dataout.RXRESULTCODE = 1 />			
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>        
		<cfreturn representationOf(dataout).withStatus(200) /> 


    </cffunction>
    
   
</cfcomponent>