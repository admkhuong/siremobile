<cfcomponent extends="taffy.core.resource" taffy:uri="/samples/logapiresponse" hint="Log Response for Testing">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://ebmii.messagebroadcast.com/ire/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="XMLDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
        
	<cffunction name="get" access="public" output="false" hint="Date Time Protocol">
		<cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
        <cfargument name="inpUserId" required="no" default="0"/>
		
		<cfset var dataOut = {}>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
        <cfset var InsertToResponseLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so includes have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>

		<cftry>
           
		   	<!--- Formatted Date Time --->
           	<cfset dataOut = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
         
            <cfquery name="InsertToResponseLog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexprogramdata.sampleapilog
                (
                    PKId_int,
                    UserId_int,
                    Created_dt,
                    CGI_vch
                )
                VALUES
                (
                    NULL,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#" null="#not(len(inpUserId))#">,            
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SerializeJSON(CGI))#">             
                )
            </cfquery>
                                    
        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
                        
                <cftry>        
                
                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "inpConfirm = (#inpConfirm#)  inpContactString=(#inpContactStringAppt#) inpBatchId=(#inpBatchIdAppt#) inpContactTypeIdAppt=(#inpContactTypeIdAppt#) inpQAMode=(#inpQAMode#)">
                    <cfset SubjectLine = "SimpleX Capture API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>