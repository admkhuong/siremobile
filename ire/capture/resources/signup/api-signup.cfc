<cfcomponent extends="taffy.core.resource" taffy:uri="/sire/signup" hint="Sign up a sire user to a free account">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Sign up a sire user to a free account">
 		
 		<cfargument name="inpAccountType" type="string" required="false" default="Free" hint="The type of account being registered"/>
 		<cfargument name="inpFirst" type="string" required="false" default="New User" hint="The full user name - expected format FIRST LAST"/>
        <cfargument name="inpLast" type="string" required="false" default="New User" hint="The full user name - expected format FIRST LAST"/>       
        <cfargument name="inpPassword" required="true" default="" type="string">
        <cfargument name="inpSMSNumber" required="true" default="" type="string">
        <cfargument name="inpEmail" type="string" required="true" default="" hint="eMail address to validate"/>        
        <cfargument name="DebugAPI" required="no" default="0">
 		 		 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset var RetVarRegisterNewAccountNew = 'true' />
                     
		<cfset var DebugStr = "" />
		
		<cfset var FirstName = 'New' />
		<cfset var LastName= 'User' />
       
        <!--- Info: The Sesioon.UserId is set based on the authticated user by the custom API authentication method --->
        
        <!--- Set the default Session DB Source --->
        <cfset DBSourceEBM = "Bishop"/>
        
                        
 		<cftry>
 		
	 		<cfset Session.DBSourceEBM = "Bishop"/>
	 		<cfset Session.COMPANYUSERID = "Session.UserId"/>
 		
 		<!--- 
	 		
	 		Notes
	 			Free plan only - we DO NOT want credit card over SMS
	 			
	 			
	 			We need
	 			First Name
	 			Last Name
	 			email Address
	 			password
	 			
	 			
	 			SMS is part of sign up
	 			
	 		
	 		
	 			No need to confirm - user can go back in change to get password
	 			
	 			
	 			
	 		Tasks
	 		
	 			New Reserved keyword to halt current flow - exit
	 			New reserved keyword to reverse to last question - back
	 			Start over option?
	 				 		
	 		
	 			Link in final message?   https://dev.siremobile.com/thankyou?pt=1
	 			Link to login with username filled out
	 		
	 		
	 			Lower the reuqirments for password
	 		
	 		
	 			Special bonus for signing up via SMS? Extra credits? Coupon code?
	 			
	 			
	 			Flow needs to validate password.....
	 			Flow needs to validate email address .....
	 				 		
	 				 		
	 			Can I use regexp in answers to validate format instead of API call? - Yesssss!
	 			
	 			
	 	
{
   "inpFirst": "Lee",
   "inpLast": "Peterson",
   "inpSMSNumber": "9494000553",
   "inpAccountType": "free",
   "inpPassword": "12345678a",
   "inpEmail": "Lee+4@siremobile.com",
   "DebugAPI": ""
}
			 		
	 				 		
	 	--->
 		
 		
 		<!--- 
	 		
	 		<cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
        <cfargument name="INSMSNUMBER" required="yes" default="" type="string">
        <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
        <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
        <cfargument name="INPFNAME" required="yes" default="" type="string">
        <cfargument name="INPLNAME" required="yes" default="" type="string">
        
					 		<cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
				        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
				        <cfargument name="INSMSNUMBER" required="yes" default="" type="string">
				        <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
				        <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
				        <cfargument name="INPFNAME" required="yes" default="" type="string">
				        <cfargument name="INPLNAME" required="yes" default="" type="string">
        <cfargument name="INPORGNAME" required="no" default="" type="string">
        <cfargument name="INPCBPID" required="no" default="1" type="string">
        <cfargument name="INPSHAREDRESOURCES" required="no" default="0" type="string">
        <cfargument name="INPCOMPANYID" required="no" default="0" type="string">
        <cfargument name="inpReferralCode" required="no" default="" type="string">
        <cfargument name="inpReferralType" required="no" default="" type="string">
        <cfargument name="inpPromotionCode" required="no" default="" type="string">
        <cfargument name="inpPlanId" required="yes" default="1">
        <cfargument name="inpIsYearly" required="no" default="0">

        <cfargument name="payment_method" required="no" default="">
        <cfargument name="line1" required="no" default="">
        <cfargument name="city" required="no" default="">
        <cfargument name="country" required="no" default="">
        <cfargument name="phone" required="no" default="">
        <cfargument name="state" required="no" default="">
        <cfargument name="zip" required="no" default="">
        <cfargument name="email" required="no" default="">
        <cfargument name="cvv" required="no" default="">
        <cfargument name="expirationDate" required="no" default="">
        <cfargument name="firstName" required="no" default="">
        <cfargument name="lastName" required="no" default="">
        <cfargument name="number" required="no" default="">
        <cfargument name="primaryPaymentMethodId" required="no" default="1">


	 		type: "POST",
					url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccountNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  {
						INPMAINEMAIL: userEmail,
						INPTIMEZONE: 31,
						INPACCOUNTTYPE: accountType,
						INPNAPASSWORD: pass,
						INPFNAME: fname,
						INPLNAME: lname,
						INSMSNUMBER: sms_number,
						inpReferralCode: rf,
						inpReferralType: rftype,
						inpPromotionCode: pcode,
						payment_method: 0,
						number: cardNumber,
						cvv: securityCode,
						expirationDate: expireDate,
						firstName: cardHolderFirstName,
						lastName: cardHolderLastName,
						line1: address,
						city: city,
						state: state,
						zip: zipcode,
						country: 'US',
						primaryPaymentMethodId: 1,
						email: userEmail,
						inpPlanId: planid,
						inpIsYearly: isYearlyPlan	 		


<cfinvoke method="VldEmailAddress" component="public.sire.models.cfc.userstools" returnvariable="RetVarRegisterNewAccountNew">
	<cfinvokeargument name="Input" value="#inpEmail#">
</cfinvoke>

	 		 --->
	 	 			 	 		 		 		
	 		 
		 	<!---Call centralized registration logic --->




			<cfinvoke method="RegisterNewAccountNew" component="public.sire.models.cfc.userstools" returnvariable="RetVarRegisterNewAccountNew">
				<cfinvokeargument name="INPMAINEMAIL" value="#inpEmail#">
				<cfinvokeargument name="INPFNAME" value="#inpFirst#">
				<cfinvokeargument name="INPLNAME" value="#inpLast#">
				<cfinvokeargument name="INPNAPASSWORD" value="#inpPassword#">
				<cfinvokeargument name="INPACCOUNTTYPE" value="#inpAccountType#">
				<cfinvokeargument name="INSMSNUMBER" value="#inpSMSNumber#">
				<cfinvokeargument name="INPTIMEZONE" value="31">
				<cfinvokeargument name="inpPlanId" value="1">
			</cfinvoke> 

		
            <cfset dataout = RetVarRegisterNewAccountNew />
                    	
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>
    
   
</cfcomponent>