<cfinclude template="../../paths.cfm" >

<cfinclude template="../../../../session/cfc/csc/constants.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
<cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">

<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'api.siremobile.com') GT 0 OR LCase(CGI.SERVER_NAME) EQ 'dev.siremobile.com'>
  	<cfset SupportEMailFromNoReply = "noreply@esiremobile.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />
    <cfset application.Env = true />
	<cfset EnvPrefix = 'PRO_'/>
<cfelse>
	<cfset SupportEMailFromNoReply = "noreply@siremobile.com" />
	<cfset SupportEMailFrom = "sirevn1@gmail.com" />
	<cfset SupportEMailServer = "smtp.gmail.com" />
	<cfset SupportEMailUserName = "sirevn1@gmail.com" />
	<cfset SupportEMailPassword = "set@2016" />
	<cfset SupportEMailPort = "465" />
	<cfset SupportEMailUseSSL = "true" />
	<cfset application.Env = false />
	<cfset EnvPrefix = 'DEV_'/>
</cfif>
<cfset ServerAPIKeyShortUrl = "AIzaSyB0c-sx7hvlu8j2UdScdK2vVIj4qQD8-Yg" />
<cfset DBSourceEBM = "Bishop"/>

<cffunction name="PushWSNew" output="false" access="private" hint="Push to web socket">
	<cfargument name="inpTextToSend" required="true" default="" />
	<cfargument name="inpContactString" required="true" default=""/>
	<cfargument name="inpType" required="true" default="1"/>
	<cfargument name="inpSessionId" required="true" default="0"/>
	<cfargument name="inpUserId" required="false" default="#session.UserId#"/>

	<cfset var dataout = {} />
	<cfset var reg = '' />
	<cfset var channels = []/>

	<cfset dataout.RXRESULTCODE = -1/>
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout.TYPE = '' />
	<cfset var EnvPrefix = '' />

	<!--- Environment prefix, to separete DEV and PRO env --->
	<cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 OR LCase(CGI.SERVER_NAME) EQ 'dev.siremobile.com' OR LCase(CGI.SERVER_NAME) EQ 'smsmo.siremobile.com' OR LCase(CGI.SERVER_NAME) EQ 'api.siremobile.com'>
		<cfset EnvPrefix = 'PRO_'/>
	<cfelse>
		<cfset EnvPrefix = 'DEV_'/>
	</cfif>

	<cftry>
		<!--- Session channel, this channel was listening on response page --->
		<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_SESSION_" & "#arguments.inpSessionId#"), 6))/>

		<!--- User channel, this channel is listening on other pages, to make sure user see the notification --->
		<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_" & "#arguments.inpUserId#"), 6))/>

		<!--- Push to web socket server --->
		<cfhttp url="https://ws.siremobile.com:8443/send" result="reg" method="post">
		    <cfhttpparam name="msg" type="formfield" value="#arguments.inpTextToSend#"/>
		    <cfhttpparam name="time" type="formfield" value="#timeFormat(now(), "HH:mm tt")# #dateFormat(now(), "yyyy/mm/dd")#"/>
		    <cfhttpparam name="phone" type="formfield" value="#arguments.inpContactString#"/>
		    <cfhttpparam name="channel" type="formfield" value="#serializeJSON(channels)#"/>
		    <cfhttpparam name="type" type="formfield" value="#arguments.inpType#"/>
		    <cfhttpparam name="userid" type="formfield" value="#arguments.inpUserId#"/>
		    <cfhttpparam name="sessionid" type="formfield" value="#arguments.inpSessionId#"/>
		    <cfhttpparam name="host" type="formfield" value="#CGI.SERVER_NAME#"/>
		</cfhttp>
		<cfif reg.status_code EQ "200">
			<cfset dataout.RXRESULTCODE = 1/>
		<cfelse>
			<cfset dataout.ERRMESSAGE = reg.errordetail/>
		</cfif>
		<cfcatch type="any">
			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
			<cfset dataout.TYPE = cfcatch.TYPE/>
		</cfcatch>
	</cftry>

	<cfreturn dataout/>
</cffunction>

<cffunction name="TerminateOtherChatSessions" access="private" hint="Terminate other chat session still active with contact string">
	<cfargument name="inpContactString" required="true" type="string"/>
	<cfargument name="inpSessionId" required="true" type="string"/>
	<cfargument name="inpShortCode" required="true" type="string"/>

	<cfset var dataout = {}/>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout.TYPE = '' />

	<cfset var TerminateSessions = '' />
	<cfset var GetOldSession = '' />
	<cfset var RetVarLog = '' />

	<cftry>
		<cfquery datasource="#DBSourceEBM#" name="GetOldSession">
			SELECT
				ss.SessionId_bi
			FROM
				simplequeue.sessionire ss
			INNER JOIN
				simpleobjects.batch b
			ON
				ss.BatchId_bi = b.BatchId_bi
			WHERE
				ss.SessionState_int = 1
			AND
				ss.SessionId_bi < <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpSessionId#"/>
			AND
				ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
			AND
				b.EMS_Flag_int = 20
			AND
				ss.CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
		</cfquery>

		<cfif GetOldSession.RECORDCOUNT GT 0>
			<cfloop query="GetOldSession">
				<cfquery datasource="#DBSourceEBM#" result="TerminateSessions">
					UPDATE
						simplequeue.sessionire
					SET
						SessionState_int = 4
					WHERE
						SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetOldSession.SessionId_bi#"/>
				</cfquery>
				<cfif TerminateSessions.RECORDCOUNT GT 0>
					<cfinvoke method="LogTerminateSession" returnvariable="RetVarLog">
						<cfinvokeargument name="inpNewSessionId" value="#arguments.inpSessionId#"/>
						<cfinvokeargument name="inpOldSessionId" value="#GetOldSession.SessionId_bi#"/>
						<cfinvokeargument name="inpContactString" value="#arguments.inpContactString#"/>
						<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
					</cfinvoke>

					<cfif RetVarLog.RXRESULTCODE NEQ 1>
						<cfthrow type="Log" message="Log terminated session errors" detail="#RetVarLog.MESSAGE# #RetVarLog.ERRMESSAGE#"/>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfset dataout.RXRESULTCODE = 1/>

		<cfcatch type="any">
			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
			<cfset dataout.TYPE = cfcatch.TYPE/>
		</cfcatch>
	</cftry>

	<cfreturn dataout/>
</cffunction>

<cffunction name="LogTerminateSession" access="private" output="false" hint="Log terminated chat session">
	<cfargument name="inpNewSessionId" required="true" hint="New chat session that make other to be terminated"/>
	<cfargument name="inpOldSessionId" required="true" hint="Old session that is to be terminated"/>
	<cfargument name="inpContactString" required="true" hint="Contact string that chatting"/>
	<cfargument name="inpShortCode" required="true" hint="Customer shortcode"/>

	<cfset var dataout = {}/>
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout.TYPE = '' />

	<cfset var LogResult = '' />

	<cftry>
		<cfquery datasource="#DBSourceEBM#" result="LogResult">
			INSERT INTO simplequeue.sire_terminate_sessionire_logs
			(
				NewSessionId_bi,
				TerminatedSessionId_bi,
				ContactString_vch,
				CSC_vch
			)
			VALUES
			(
				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpNewSessionId#"/>,
				<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpOldSessionId#"/>,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
			)
		</cfquery>

		<cfif LogResult.RECORDCOUNT EQ 1>
			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGE = "Log success"/>
		</cfif>
		<cfcatch type="any">
			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
			<cfset dataout.TYPE = cfcatch.TYPE/>
		</cfcatch>
	</cftry>

	<cfreturn dataout/>
</cffunction>

<cffunction name="FormatContactString" access="private" returnformat="plain" returntype="string" output="false" hint="Return format for contact number">
	<cfargument name="inpContactString" required="true" type="string" hint="Contact String"/>
	<cfset arguments.inpContactString = ReReplaceNoCase(arguments.inpContactString,"[^0-9,]","","ALL")>
	<cfset var phone = '' />
	<cfset var lead = '' />
	<cfif Len(arguments.inpContactString) NEQ 10 AND Len(arguments.inpContactString) NEQ 11>
		<cfreturn arguments.inpContactString/>
	<cfelseif Len(arguments.inpContactString) EQ 11>
		<cfset phone = Right(arguments.inpContactString, 10)/>
		<cfset lead = Left(arguments.inpContactString, 1)/>
	<cfelse>
		<cfset phone = arguments.inpContactString/>
	</cfif>
	<cfset var areaCode = Left(phone, 3)>
	<cfset var firstThree = Mid(phone, 4,3)>
	<cfset var lastFour = Right(phone, 4)>
	<cfset var formatedNumber = "#lead#(#areaCode#) #firstThree#-#lastFour#"/>

	<cfreturn formatedNumber>
</cffunction>

<cffunction name="GetSimpleUserInfo" access="private" hint="Get user username or company name" output="false">
	<cfargument name="inpUserId" required="true"/>

	<cfset var dataout = {}/>
	<cfset var getUser = '' />
	<cfset dataout.USERNAME = '' />
	<cfset dataout.COMPANYNAME = ''/>
	<cfset dataout.RXRESULTCODE = '' />

	<cftry>
		<cfquery datasource="#DBSourceEBM#" name="getUser">
			SELECT
				so.OrganizationName_vch,
				uc.FirstName_vch,
				uc.LastName_vch
			FROM
				simpleobjects.useraccount uc
			JOIN
				simpleobjects.Organization so
			ON
				uc.UserId_int = so.UserId_int
			WHERE
				uc.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#"/>
		</cfquery>

		<cfif getUser.RECORDCOUNT GT 0>
			<cfset dataout.COMPANYNAME = getUser.OrganizationName_vch/>
			<cfset dataout.USERNAME = "#getUser.FirstName_vch# #getUser.LastName_vch#"/>
			<cfset dataout.RXRESULTCODE = 1/>
		</cfif>

		<cfcatch type="any">
			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
			<cfset dataout.TYPE = cfcatch.TYPE/>
		</cfcatch>
	</cftry>

	<cfreturn dataout/>
</cffunction>

<cffunction name="GenerateLinkCode" access="public" output="false" hint="Generate a alphanumberic key - non-case sensitive">   
	<cfargument name="inpLength" type="Numeric" required="true" default="6">   

	<cfset var i = '' />

	<!--- create a list of all allowable characters for our short URL link - not case sensitive --->   
	<cfset var chars="a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,-">   

	<!--- our radix is the total number of possible characters --->   
	<cfset var radix=listlen(local.chars)>

	<cfset var randnum = 1>  

	<!---   Then, after setting our return variable to an empty string, we're going to add random characters from that list in a loop: --->
	<cfset var dataout=StructNew()>

	<!--- initialise our return variable --->   
	<cfset dataout.SHORTURLKEY="">
	<cfset var i = '' /> 

<!--- loop from 1 until the number of characters our URL should be,   
	adding a random character from our master list on each iteration  --->   
	<cfloop from="1" to="#arguments.inpLength#" index="i">   

		<!--- generate a random number in the range of 1 to the total number of possible characters we have defined --->   
		<cfset randnum = RandRange(1, radix)>   

		<!--- add the character from a random position in our list to our short link --->   
		<cfset dataout.SHORTURLKEY=dataout.SHORTURLKEY & listGetAt(chars, randnum)>   

	</cfloop>

	<!--- Finally, return the newly generated short link to the calling code: --->	
	<!--- return the generated random short link --->   
	<cfreturn dataout>   

</cffunction>

<cffunction name="AddShortURL" access="public" output="false" hint="Generate a short URL - if one for the currently logged in user account">
	<cfargument name="inpDesc" TYPE="string" required="no" default="" hint="Freindly name for organizing list of shortened URLs"/>
	<cfargument name="inpTargetURL" TYPE="string" required="yes" hint="Where is this short URL going to re-driect to?"/>
	<cfargument name="inpLength" type="Numeric" required="no" default="6" hint="The length of the short URL key - 6 by default" />
	<cfargument name="inpUniqueTarget" type="Numeric" required="no" default="0" hint="Allow duplicate targets on request. inpUniqueTarget=1 to create duplicate shortened URL for target. inpUniqueTarget=0 will just return existing short key if found otherwise will create a new key" /> 
	<cfargument name="inpRedirectCode" TYPE="Numeric" required="no" default="301" hint="Allow user to specify alternate types of Re-Directs - Default is HTTP status 301 (permanent redirect), 302, or 307 (temporary redirect)."/>
	<cfargument name="inpType" TYPE="numeric" required="no" default="1" hint="Short url type, 1 - user create, 2 - collect feedback, 3 - sms chat"/>
	<cfargument name="inpUserId" type="numeric" required="no" hint="User id"/>

	<cfset var dataout = {} />    
	<cfset var AddShortURLToDB = '' />
	<cfset var RetVarGenerateLinkCode = '' />
	<cfset var currentIndex = '' />
	<cfset var CheckForCollisions = '' />
	<cfset var GetUniqueTargetId = '' />
	<cfset var UniqueTargetId = '0' />
	<cfset var allowedAddUrl = false />
	<cfset var CheckDuplicatedShortKey = ''/>
	<cfset var RetValInsertShortUrl = '' />

	<cfset var ShortURLSite = "http://mlp-x.com" />
	<cfset var insertUserLog = '' />
	<cfset var type = '' />

	<cftry>      

		<!--- Set default return values --->
		<cfset dataout.RXRESULTCODE = "-1" />
		<cfset dataout.SHORTURLKEY = ""/> 
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.SHORTURL = '' />

		<!--- Verify this target Id is unique --->
		<cfquery name="CheckForCollisions" datasource="#DBSourceEBM#">
			SELECT
				ShortURL_vch
			FROM
				simplelists.url_shortner
			WHERE
				TargetURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTargetURL#"/>
			AND
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>
			AND
				Active_int = 1	
			ORDER BY
				UniqueTargetId_int DESC	
		</cfquery>

		<cfif inpUniqueTarget EQ 0 AND CheckForCollisions.RECORDCOUNT GT 0> <!--- Use existing key --->
			<cfset dataout.RXRESULTCODE = "2" />            
			<cfset dataout.SHORTURLKEY = "#CheckForCollisions.ShortURL_vch#"/>
			<cfset dataout.MESSAGE = "Target URL already exists!" />
			<cfset dataout.ERRMESSAGE = "Target URL key already exists!" />
			<cfset dataout.SHORTURL = "#ShortURLSite#/#CheckForCollisions.ShortURL_vch#"/>
			<cfreturn dataout/>
		<cfelse> <!--- Create a new key --->
			<cfloop index = "currentIndex" from = "1" to = "100" step = "1">
				<!--- Generate a randon key --->
				<cfinvoke method="GenerateLinkCode" returnvariable="RetVarGenerateLinkCode">
					<cfinvokeargument name="inpLength" value="#inpLength#"/>
				</cfinvoke>   	

				<!--- Lookup in DB for Collisions - key must be unique - dont rely on error handling do an actual lookup --->
				<!--- always get latest entry for operator Id regardless of whose list it is on --->
				<cfquery name="CheckForCollisions" datasource="#DBSourceEBM#">
					SELECT 
						COUNT(*) AS TOTALCOUNT
					FROM
						simplelists.url_shortner
					WHERE
						ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGenerateLinkCode.SHORTURLKEY#"/> 
					AND
						Active_int = 1
				</cfquery>

				<cfif CheckForCollisions.TOTALCOUNT EQ 0>
					<!--- Exit loop if no collision found--->
					<cfbreak />
				</cfif>
			</cfloop>

			<!--- Insert entry into DB --->
			<cfquery name="AddShortURLToDB" datasource="#DBSourceEBM#" result="RetValInsertShortUrl">
				INSERT INTO 
				simplelists.url_shortner
				(
					UserId_int, 
					UniqueTargetId_int,
					RedirectCode_int,
					Created_dt, 
					LastClicked_dt,
					Desc_vch,
					ShortURL_vch,
					TargetURL_vch,
					DynamicData_vch,
					ClickCount_int,
					Type_ti
					)
				VALUES
				(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UniqueTargetId#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRedirectCode#"/>,
					NOW(), 
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpDesc,255)#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RetVarGenerateLinkCode.SHORTURLKEY#"/>,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(inpTargetURL,2048)#"/>,
					"",
					0,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpType#"/>
				)
			</cfquery>
		</cfif>    

		<cfset dataout.RXRESULTCODE = "1" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.SHORTURLKEY = RetVarGenerateLinkCode.SHORTURLKEY/>
		<cfset dataout.SHORTURL = "#ShortURLSite#/#RetVarGenerateLinkCode.SHORTURLKEY#"/>

		<cfif arguments.inpType EQ 2>
			<cfset type = "Collect Feedback"/>
		<cfelseif arguments.inpType EQ 3>
			<cfset type = "SMS Chat"/>
		</cfif>

		<cfquery name="insertUserLog" datasource="#DBSourceEBM#">
			INSERT INTO
				simpleobjects.userlogs
			(
				UserId_int,
				ModuleName_vch,
				Operator_vch,
				Timestamp_dt
			)
			VALUES
			(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#" />,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="ShortURLId = #RetValInsertShortUrl.GENERATED_KEY#" />, 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="ShortURL created. Type: #type#" />,
				NOW()
			)
		</cfquery>

		<cfcatch TYPE="any">
			<cfset dataout.RXRESULTCODE = "-2" />
			<cfset dataout.SHORTURLKEY = ""/> 
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>

	</cftry>

	<cfreturn dataout />
</cffunction>