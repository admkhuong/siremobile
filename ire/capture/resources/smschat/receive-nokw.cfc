<cfcomponent extends="taffy.core.resource" taffy:uri="/smschat/receivenokw" hint="Capture user message to smschat">
	
	<cfinclude template="inc_functions.cfm"/>

	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string"/>
        <cfargument name="inpMessageText" required="yes" hint="Customer Question or Issue"/>
        <cfargument name="inpShortCode" required="yes" hint="Short code"/>
        
		<cfset var dataOut = {}>
		<cfset var DBSourceEBM = "Bishop"/>
		<cfset var GetSession = '' />
		<cfset var pushWS = '' />
		<cfset var UpdateUnread = '' />
		<cfset var getUser = '' />
		<cfset var RetVarSendSingleMT = '' />
		<cfset var errors = []/>
		<cfset var errorMessage = '' />
		<cfset var longUrl = '' />
		<cfset var phone = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var rsSUrl = '' />

        <!---  --->
        <cfset dataOut.RXRESULTCODE = "-1" />
        <cfset dataOut.MESSAGE = "receivenokw" />

		<cftry>
			<!--- Get current chat session from batch id and contact string --->
			<cfquery name="GetSession" datasource="#DBSourceEBM#">
				SELECT
					ss.SessionId_bi,
					ss.UserId_int
				FROM
					simplequeue.sessionire ss
				INNER JOIN
					simpleobjects.batch b
				ON
					ss.BatchId_bi = b.BatchId_bi
				WHERE
					ss.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					ss.SessionState_int = 1
				AND
					(
						b.EMS_Flag_int = 20
					OR
						b.TemplateId_int = 9
					)
				AND
					ss.CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
				ORDER BY
					ss.SessionId_bi DESC
				LIMIT
					1
			</cfquery>

			<cfif GetSession.RECORDCOUNT GT 0>
				<!--- Push to web socket --->
				<cfset pushWS = PushWSNew(arguments.inpMessageText, arguments.inpContactString, 2, GetSession.SessionId_bi, GetSession.UserId_int)/>
				<cfif pushWS.RXRESULTCODE NEQ 1>
					<cfset errorMessage = errorMessage & "Push web socket errors. "/>
					<cfset arrayAppend(errors, "#pushWS.MESSAGE# #pushWS.ERRMESSAGE#")/>
				</cfif>
			</cfif>

			<cfset dataout.RXRESULTCODE = "1"/>

			<cfif errorMessage NEQ ''>
				<cfset var ENA_Message = "">
                <cfset var SubjectLine = "SimpleX SMSChat/receive API Notification">
                <cfset var TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset var ErrorNumber="1112">
                <cfset var AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM("Functions")#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(errorMessage)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(serializeJSON(errors))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
			</cfif>
        <cfcatch>
            <!--- For debbugging only! --->
			<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->
            <cftry>
    
				<cfset ENA_Message = "">
                <cfset SubjectLine = "SimpleX SMSChat/receive API Notification">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="1112">
                <cfset AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">


            </cfcatch>

            </cftry>

            <cfset dataout = SerializeJSON(cfcatch)>

            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>

		</cftry>

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>


     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
</cfcomponent>