<cfcomponent extends="taffy.core.resource" taffy:uri="/smschat/start" hint="Capture user message to smschat">

	<cfinclude template="inc_functions.cfm"/>

	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string"/>
        <cfargument name="inpKeyword" required="yes" hint="Customer Question or Issue"/>
        <cfargument name="inpShortCode" required="yes" hint="Customer shortcode"/>
        <cfargument name="inpSendEmailStart" required="no" default="false"/>
        <cfargument name="inpEmailList" required="no" default=""/>
        <cfargument name="inpSendSMSStart" required="no" default="false"/>
        <cfargument name="inpSMSList" required="no" default=""/>

		<cfset var dataOut = {}>
		<cfset var DBSourceEBM = "Bishop"/>
		<cfset var GetSession = '' />
		<cfset var pushWS = '' />
		<cfset var UpdateUnread = '' />
		<cfset var getUser = '' />
		<cfset var phone = '' />
		<cfset var longUrl = '' />
		<cfset var rsSUrl = '' />
		<cfset var errors = []/>
		<cfset var errorMessage = '' />
		<cfset var RetVarSendSingleMT = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var TerminateSessions = '' />

        <!---  --->
        <cfset dataOut.RXRESULTCODE = "-1" />

		<cftry>
			<!--- Get current chat session from batch id and contact string --->
			<cfquery name="GetSession" datasource="#DBSourceEBM#">
				SELECT
					ss.SessionId_bi,
					ss.UserId_int
				FROM
					simplequeue.sessionire ss
				INNER JOIN
					simpleobjects.batch b
				ON
					ss.BatchId_bi = b.BatchId_bi
				WHERE
					ss.BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					ss.ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					ss.SessionState_int = 1
				AND
					b.EMS_Flag_int = 20
				AND
					ss.CSC_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
				ORDER BY
					ss.SessionId_bi DESC
				LIMIT
					1
			</cfquery>

			<cfif GetSession.RECORDCOUNT GT 0>
				<!--- Update unread message --->
				<cfquery datasource="#DBSourceEBM#" result="UpdateUnread">
					INSERT INTO 
						`simplequeue`.`smschat_sessionire_messageunread` (`SessionId_bi`, `MessageUnread_int`) 
					VALUES 
					(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetSession.SessionId_bi#">, 
						1
					)
					ON DUPLICATE KEY UPDATE
						`MessageUnread_int` = `MessageUnread_int` + 1;
				</cfquery>

				<cfinvoke method="TerminateOtherChatSessions" returnvariable="TerminateSessions">
					<cfinvokeargument name="inpSessionId" value="#GetSession.SessionId_bi#"/>
					<cfinvokeargument name="inpContactString" value="#arguments.inpContactString#"/>
					<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
				</cfinvoke>

				<cfif TerminateSessions.RXRESULTCODE NEQ 1>
					<cfset errorMessage = errorMessage & "Terminate old session errors. "/>
					<cfset arrayAppend(errorMessage, "#TerminateSessions.MESSAGE# #TerminateSessions.ERRMESSAGE#")/>
				</cfif>

				<cfset longUrl = "https://#(findNoCase("api.siremobile.com", CGI.SERVER_NAME) ? "www.siremobile.com" : CGI.SERVER_NAME)#/session/sire/pages/sms-response?ssid=#GetSession.SessionId_bi#" />

				<cfif len(arguments.inpContactString) GTE 14>
					<cfset longUrl &= "&showPreview=1"/>
				</cfif>

				<cfif arguments.inpSendEmailStart>
					<cfinvoke method="GetSimpleUserInfo" returnvariable="getUser">
						<cfinvokeargument name="inpUserId" value="#GetSession.UserId_int#"/>
					</cfinvoke>

					<cfif getUser.RXRESULTCODE NEQ 1>
						<cfset errorMessage = errorMessage & "Get user info errors. "/>
						<cfset arrayAppend(errorMessage, "#getUser.MESSAGE# #getUser.ERRMESSAGE#")/>
					</cfif>

			        <!--- Send email to support list of emails - optional - if blank will not send anything --->
					<cfif LEN(TRIM(arguments.inpEmailList)) GT 0>
						<cfmail to="#arguments.inpEmailList#" subject="SIRE SMS Chat Conversation Started" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
							<cfoutput>
					            <html>
									<head>
										<meta charset="utf-8">
										<title>SMS Chat Conversation Started</title>
									</head>
								
									<body style="font-family: Arial; font-size: 12px;">
					
										<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
											<cfinclude template="mail_header.cfm">
										
										  	<tbody style="margin: 0; padding: 0;">
										  		<tr>
										  			<td colspan="2" style="margin: 0; padding: 0;">
										  				
										  				<div style="text-align: center;">
										  					<img src="http://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
										  				</div>
										  			</td>
										  		</tr>
										  		<tr>
										  			<td colspan="2" style="padding: 0 30px;">

														<p style="font-size: 16px;color: ##5c5c5c;">#(getUser.COMPANYNAME NEQ "" ? getUser.COMPANYNAME : (getUser.USERNAME NEQ "" ? getUser.USERNAME : "Customer"))#,</p>

														<br>

														<p style="font-size: 18px;color: ##74c37f; text-align:center; font-weight:bold">You have a new Chat conversation</p>
														<hr style="border: 0;border-bottom: 1px solid ##74c37f;">

														<table style="width:100%">
															<tr>
																<td style="width:10%"></td>
																<td style="width:30%">			
																	<p style="font-size: 16px;color: ##5c5c5c;">Contact From: </p>					
																</td>
																<td style="width:50%">
																	<p style="font-size: 16px;color: ##5c5c5c;">#FormatContactString(arguments.inpContactString)#</p>																	
																</td>
																<td style="width:10%"></td>
															</tr>
															
															<tr>
																<td style="width:10%"></td>
																<td style="width:30%">														
																	<p style="font-size: 16px;color: ##5c5c5c;">Keyword:</p>														
																</td>
																<td style="width:50%">
																	<p style="font-size: 16px;color: ##5c5c5c;">#arguments.inpKeyword#</p>				
																</td>
																<td style="width:10%"></td>
															</tr>
															
															<tr>
																<td style="width:10%"></td>
																<td style="width:30%">														
																	<p style="font-size: 16px;color: ##5c5c5c;">Go to conversation:</p>
																</td>
																<td style="width:50%">														
																	<p style="font-size: 16px;color: ##5c5c5c;"><a href="#longUrl#">Go</a></p>					
																</td>
																<td style="width:10%"></td>
															</tr>
															
															
														</table>
																									
														<p style="font-size: 16px;color: ##5c5c5c;">From the SIRE Assistance Team</p>
										  			</td>
										  		</tr>	
										  	</tbody>
																		
											<cfinclude template="mail_footer.cfm">
										</table>
									</body>
								</html>
							</cfoutput>
						</cfmail>
					</cfif>
				</cfif>

				<cfif arguments.inpSendSMSStart>
					<!--- Send one single MT --->
					<cfif len(trim(arguments.inpSMSList)) GT 0>
						<cftry>
			                <cfinvoke method="AddShortURL" returnvariable="rsSUrl">
			                    <cfinvokeargument name="inpTargetURL" value="#longUrl#" />
			                    <cfinvokeargument name="inpDesc" value="SMS chat short url" />
			                    <cfinvokeargument name="inpUserId" value="#GetSession.UserId_int#"/>
			                    <cfinvokeargument name="inpType" value="3"/>
			                </cfinvoke>

			                <cfif rsSUrl.RXRESULTCODE LT 1>
			                	<cfthrow type="Function" message="Generate short url failed" detail="#rsSUrl.MESSAGE# #rsSUrl.ERRMESSAGE#"/>
			                </cfif>

			                <cfset session.UserId = GetSession.UserId_int/>
			                <cfset Session.DBSourceEBM = DBSourceEBM/>

							<cfloop array="#listToArray(arguments.inpSMSList)#" index="phone">
								<cfset phone = ReReplaceNoCase(phone,"[^0-9,]","","ALL")>
								<cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
									<cfinvokeargument name="inpContactString" value="#phone#"/>
									<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
									<cfinvokeargument name="inpTextToSend" value="You have a new Chat conversation. Please go to this link to view your: #rsSUrl.SHORTURL#"/>
								</cfinvoke>
							</cfloop>

							<cfcatch type="any">
								<cfset errorMessage = errorMessage & "Send SMS notification failed. "/>
								<cfset arrayAppend(errors, "#cfcatch.MESSAGE# #cfcatch.DETAIL#")/>
							</cfcatch>
						</cftry>
					</cfif>
				</cfif>

				<!--- Push to web socket --->
				<cfset pushWS = PushWSNew(arguments.inpKeyword, arguments.inpContactString, 2, GetSession.SessionId_bi, GetSession.UserId_int)/>

				<cfif pushWS.RXRESULTCODE NEQ 1>
					<cfset errorMessage = errorMessage & "Push web socket errors. "/>
					<cfset arrayAppend(errors, "#pushWS.MESSAGE# #pushWS.ERRMESSAGE#")/>
				</cfif>
			</cfif>

			<cfset dataout.RXRESULTCODE = "1"/>

			<cfif errorMessage NEQ ''>
				<cfset var ENA_Message = "">
                <cfset var SubjectLine = "SimpleX SMSChat/start API Notification">
                <cfset var TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset var ErrorNumber="1111">
                <cfset var AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM("Functions")#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(errorMessage)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(serializeJSON(errors))#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
			</cfif>

        <cfcatch>
            <!--- For debbugging only! --->
			<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->
            <cftry>
    
				<cfset ENA_Message = "">
                <cfset SubjectLine = "SimpleX SMSChat/start API Notification">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="1111">
                <cfset AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>

            <cfcatch type="any">


            </cfcatch>

            </cftry>

            <cfset dataout = SerializeJSON(cfcatch)>

            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>

		</cftry>

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>


     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
</cfcomponent>