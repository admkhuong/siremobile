<cfcomponent extends="taffy.core.resource" taffy:uri="/support/tellmgr" hint="Capture user input, send email to team">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpListOfEmails" required="no" default="" hint="Specify a comma seperated list of emails - someone@somewhere.com - optional - if blank will not send anything">
        <cfargument name="inpMessageText" required="no" hint="Customer Question or Issue">
        <cfargument name="inpCompanyName" required="no" default="Company Manager" hint="Company Name">
        		
		<cfset var dataOut = ''>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var insertResult = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
		
		<cfset var SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
	    <cfset var SupportEMailFrom = "support@siremobile.com" />
	    <cfset var SupportEMailServer = "smtp.gmail.com" />
	    <cfset var SupportEMailUserName = "support@siremobile.com" />
	    <cfset var SupportEMailPassword = "SF927$tyir3" />
	    <cfset var SupportEMailPort = "465" />
	    <cfset var SupportEMailUseSSL = "true" />
                    
        <!---  --->
        <cfset dataOut = "-1" />                      
      
		<cftry>
           
            <!--- Add a status update for today --->                          
            <cfquery name="InsertToEventStatus" datasource="#DBSourceEBM#" result="insertResult">              
              
              INSERT INTO 
              	simplequeue.supportapp 
                (	                	 
                    BatchId_bi, 
                    UserId_int,
                    Status_int, 
                    Event_date, 
                    Created_dt,
                    LastUpdated_dt,
                    ContactString_vch,
                    CustomerMessage_vch
                )
                VALUES
                (                	
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    0,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    NOW(),
                 	NOW(),
                 	NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpMessageText#">                     
                 )             
            </cfquery>
            
            <!--- Send back new trouble ticket number - if this number is 0 then branch to no number available logic --->
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout = 0>	
	        </cfif>
	        
	        
	        <!--- Send email to support list of emails - optional - if blank will not send anything --->
			<cfif LEN(TRIM(inpListOfEmails)) GT 0>
				<cfmail to="#inpListOfEmails#" subject="SIRE Tell the Manger" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		           <html>
						<head>
							<meta charset="utf-8">
							<title>Tell the Manager Ticket Submitted</title>
						</head>
					
						<body style="font-family: Arial; font-size: 12px;">
		
							<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
								<thead>
								    <tr>
								        <td width="50%">
											<a href="http://siremobile.com"><img src="http://siremobile.com/public/sire/images/emailtemplate/logo.jpg" alt=""></a>
										</td>								     	
								    </tr>
								</thead>
							
							  	<tbody style="margin: 0; padding: 0;">
							  		<tr>
							  			<td colspan="2" style="margin: 0; padding: 0;">
							  				
							  				<div style="text-align: center;">
							  					<img src="http://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
							  				</div>
							  			</td>
							  		</tr>
							  		<tr>
							  			<td colspan="2" style="padding: 0 30px;">
					
											<p style="font-size: 16px;color: ##5c5c5c;">#inpCompanyName#,</p>
											
											<br>
					
											<p style="font-size: 18px;color: ##74c37f; text-align:center; font-weight:bold">Support Information</p>
											<hr style="border: 0;border-bottom: 1px solid ##74c37f;">
					
											<table style="width:100%">
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">
														<p style="font-size: 16px;color: ##5c5c5c;">Batch Id:</p> 														
													</td>
													<td style="width:50%">
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpBatchId#</cfoutput></p>																	
													</td>
													<td style="width:10%"></td>
												</tr>
												
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Contact From: </p>														
													</td>
													<td style="width:50%">														
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpContactString#</cfoutput></p>																	
													</td>
													<td style="width:10%"></td>
												</tr>
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Message From Customer:</p>														
													</td>
													<td style="width:50%">
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpMessageText#</cfoutput></p>				
													</td>
													<td style="width:10%"></td>
												</tr>
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Trouble Ticket Number:</p>
													</td>
													<td style="width:50%">														
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#dataout#</cfoutput></p>					
													</td>
													<td style="width:10%"></td>
												</tr>
												
												
											</table>
																						
											<p style="font-size: 16px;color: ##5c5c5c;">From the SIRE Assistance Team</p>
							  			</td>
							  		</tr>	
							  	</tbody>
															
								<tfoot>
								    <tr style="background-color:##f2f2f2;">
								    	<td width="60%" style="color: ##5c5c5c; font-size: 11px;">									    		
											<p>https://siremobile.com </p>
								    	</td>
								    	
								    	<td style="text-align: right;  margin: 0; padding: 0;">
								    	
								    	</td>
								    </tr>
								</tfoot>
		
							</table>
		
						</body>
					
					</html>		
				</cfmail>
			</cfif>	
            		   	                                   
        <cfcatch>        	
                            
                        <!--- For debbugging only! --->    
						<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->             
                <cftry>        
        
					<cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX Support/Sire API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            
            <cfset dataout = SerializeJSON(cfcatch)>
            
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
   
</cfcomponent>