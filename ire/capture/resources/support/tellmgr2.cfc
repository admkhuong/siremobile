<cfcomponent extends="taffy.core.resource" taffy:uri="/support/tellmgr2" hint="Capture user input, send email to team">
	<cfinclude template="../../paths.cfm" >

	<cfinclude template="../smschat/inc_functions.cfm"/>
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string"/>
        <cfargument name="inpListOfEmails" required="no" default="" hint="Specify a comma seperated list of emails - someone@somewhere.com - optional - if blank will not send anything"/>
        <cfargument name="inpListOfPhones" required="no" default="" hint="Specify a comma seperated list of phone number - 4045051234 - optional - if blank will not send anything"/>
        <cfargument name="inpMessageText" required="no" default="" hint="Customer Question or Issue"/>
        <cfargument name="inpShortCode" required="no" default="" hint="User short code"/>

		<cfset var dataOut = ''>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var insertResult = '' />
		<cfset var GetUserOfBatch = '' />
		<cfset var DBSourceEBM = "Bishop"/>

		<cfset var getUser = '' />
		<cfset var longUrl = '' />
		<cfset var phone = '' />
		<cfset var rsSUrl = '' />
		<cfset var RetVarSendSingleMT = '' />
		<cfset var CheckAbuseAPI = '' />
		<cfset var KeywordOfCampaign=''>
		<cfset var SessionOfCampaign=''>

        <!---  --->
        <cfset dataOut = "-1" />

		<cftry>
			<!--- Check for abuse --->
			<cfquery name="CheckAbuseAPI" datasource="#DBSourceEBM#">
				SELECT
					SessionId_bi
				FROM
					simplequeue.sessionire
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					ContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					SessionState_int = 1
				ORDER BY SessionId_bi DESC
				LIMIT 1
			</cfquery>
			<cfif CheckAbuseAPI.RECORDCOUNT LT 1>
				<cfthrow message="Abuse API - No session ire found!" detail="#SerializeJSON(arguments)#"/>
			<cfelse>
				<cfset var SessionOfCampaign=CheckAbuseAPI.SessionId_bi>				
			</cfif>

			<cfquery name="GetUserOfBatch" datasource="#DBSourceEBM#">
				SELECT
					UserId_int
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
			</cfquery>

			<cfif GetUserOfBatch.RECORDCOUNT LT 0>
				<cfthrow message="User not found!"/>
			</cfif>

			<cfinvoke method="GetSimpleUserInfo" returnvariable="getUser">
				<cfinvokeargument name="inpUserId" value="#GetUserOfBatch.UserId_int#"/>
			</cfinvoke>

			<cfif getUser.RXRESULTCODE LT 0>
				<cfthrow message="User info not found!" detail="#getUser.MESSAGE# #getUser.ERRMESSAGE#"/>
			</cfif>

            <!--- SIRE User trouble ticket, disable for now --->
            <!--- <cfquery name="InsertToEventStatus" datasource="#DBSourceEBM#" result="insertResult">

              INSERT INTO 
              	simplequeue.sire_user_supportapp
                (
                    BatchId_bi, 
                    UserId_int,
                    Status_int, 
                    Event_date, 
                    Created_dt,
                    LastUpdated_dt,
                    ContactString_vch,
                    CustomerMessage_vch
                )
                VALUES
                (                	
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserOfBatch.UserId_int#"/>,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    NOW(),
                 	NOW(),
                 	NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpMessageText#">
                 )
            </cfquery>
            
            <!--- Send back new trouble ticket number - if this number is 0 then branch to no number available logic --->
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout = 0>	
	        </cfif> --->
			<cfquery name="GetKeyWordOfBatch" datasource="#DBSourceEBM#">
				SELECT Keyword_vch FROM  sms.keyword where BatchId_bi=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>				
			</cfquery>
	        <cfset KeywordOfCampaign=GetKeyWordOfBatch.Keyword_vch>
	        <cfset longUrl = "https://#(findNoCase("api.siremobile.com", CGI.SERVER_NAME) ? "www.siremobile.com" : CGI.SERVER_NAME)#/session/sire/pages/sms-response?keyword=#KeywordOfCampaign#" />
	        <cfif SessionOfCampaign neq "">
				<cfset longUrl =longUrl & "&ssid=#SessionOfCampaign#">
			</cfif>				
			
	        <!--- Send email to support list of emails - optional - if blank will not send anything --->
			<cfif LEN(TRIM(inpListOfEmails)) GT 0>
				<cfmail to="#inpListOfEmails#" subject="SIRE Chat with Customers" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		           <html>
						<head>
							<meta charset="utf-8">
							<title>Chat with Customers Ticket Submitted</title>
						</head>
					
						<body style="font-family: Arial; font-size: 12px;">
		
							<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
								<cfinclude template="../smschat/mail_header.cfm"/>
							
							  	<tbody style="margin: 0; padding: 0;">
							  		<tr>
							  			<td colspan="2" style="margin: 0; padding: 0;">
							  				
							  				<div style="text-align: center;">
							  					<img src="http://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
							  				</div>
							  			</td>
							  		</tr>
							  		<tr>
							  			<td colspan="2" style="padding: 0 30px;">
					
											<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#(getUser.COMPANYNAME NEQ "" ? getUser.COMPANYNAME : (getUser.USERNAME NEQ "" ? getUser.USERNAME : "Dear Customer"))#</cfoutput>,</p>
											
											<br>
					
											<p style="font-size: 18px;color: ##74c37f; text-align:center; font-weight:bold">Chat with Customers Ticket Submitted</p>
											<hr style="border: 0;border-bottom: 1px solid ##74c37f;">
					
											<table style="width:100%">
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">
														<p style="font-size: 16px;color: ##5c5c5c;">Campaign Id:</p> 														
													</td>
													<td style="width:50%">
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpBatchId#</cfoutput></p>																	
													</td>
													<td style="width:10%"></td>
												</tr>
												
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Contact From: </p>														
													</td>
													<td style="width:50%">														
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#FormatContactString(inpContactString)#</cfoutput></p>																	
													</td>
													<td style="width:10%"></td>
												</tr>
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Message From Customer:</p>														
													</td>
													<td style="width:50%">
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpMessageText#</cfoutput></p>				
													</td>
													<td style="width:10%"></td>
												</tr>
												
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">														
														<p style="font-size: 16px;color: ##5c5c5c;">Feedback Manager:</p>
													</td>
													<td style="width:50%">														
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput><a href="#longUrl#">Go</a></cfoutput></p>					
													</td>
													<td style="width:10%"></td>
												</tr>
												
												
											</table>
																						
											<p style="font-size: 16px;color: ##5c5c5c;">From the SIRE Assistance Team</p>
							  			</td>
							  		</tr>	
							  	</tbody>
															
								<cfinclude template="../smschat/mail_footer.cfm"/>
		
							</table>
		
						</body>
					
					</html>		
				</cfmail>
			</cfif>

			<!--- Send SMS notification --->
			<cfif len(trim(arguments.inpListOfPhones)) GT 0>
				<!--- Get short url for saving SMS length --->
                <cfinvoke method="AddShortURL" returnvariable="rsSUrl">
                    <cfinvokeargument name="inpTargetURL" value="#longUrl#" />
                    <cfinvokeargument name="inpDesc" value="Collect feedback short url" />
                    <cfinvokeargument name="inpUserId" value="#GetUserOfBatch.UserId_int#"/>
                    <cfinvokeargument name="inpType" value="2"/>
                </cfinvoke>
                <cfif rsSUrl.RXRESULTCODE LT 1>
                	<cfthrow type="Function" message="Generate short url failed" detail="#rsSUrl.MESSAGE# #rsSUrl.ERRMESSAGE#"/>
                </cfif>
                <cfset session.UserId = GetUserOfBatch.UserId_int/>
                <cfset Session.DBSourceEBM = DBSourceEBM/>
				<cfloop array="#listToArray(arguments.inpListOfPhones)#" index="phone">
					<cfset phone = ReReplaceNoCase(phone,"[^0-9,]","","ALL")>
					<cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
						<cfinvokeargument name="inpContactString" value="#phone#"/>
						<cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
						<cfinvokeargument name="inpTextToSend" value="Chat with Customers Ticket Submitted. Please go to this link to go to your ticket manager page: #rsSUrl.SHORTURL#"/>
					</cfinvoke>

					<cfif RetVarSendSingleMT.RXRESULTCODE NEQ 1>
						<cfthrow type="Function" message="Send SMS notification failed" detail="#RetVarSendSingleMT.MESSAGE# #RetVarSendSingleMT.ERRMESSAGE#"/>
					</cfif>
				</cfloop>
			</cfif>

			<cfset dataout = 1/>
        <cfcatch>        	

                        <!--- For debbugging only! --->    
						<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->             
                <cftry>        
        
					<cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX Collect Feedback/Sire API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="111012"/>
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            
            <cfset dataout = -1>
            
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
    
     <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
   
</cfcomponent>