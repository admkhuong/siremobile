<cfcomponent extends="taffy.core.resource" taffy:uri="/troubleticket/responsetroubleticket" hint="Save response trouble ticket">
    <cfinclude template="../../paths.cfm" >

    <cfinclude template="../../../../session/cfc/csc/constants.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">

	<cffunction name="post" access="public" output="false" hint="Save response trouble ticket">
 		<cfargument name="inpBatchId" type="string" required="true" hint="The trouble ticket batch Id"/>
 		<cfargument name="inpContactString" type="string" required="true" hint="The customer's phone number"/>
        <cfargument name="inpMessageText" type="string" required="true" hint="The response from customer "/>
        <cfargument name="inpShortCode" type="string" required="true" hint="short code"/>
 		 		 				 		
 		<cfset var dataOut = {}/>
        <cfset var data = {}/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var GetUserInfo = {}/>
        <cfset var RetvarSaveTroubleTicket ={}/>
        <cfset var GetUserConfigInfor ={}/>
        <cfset var ticketObject ={}/>
        <cfset var phone = ""/>
        <cfset var mailTo = ""/>
        <cfset var textToSend = ""/>
        <cfset var formatphonenumber = ''/>

        <cfset var DBSourceREAD = "bishop_read"/>
        <cfset var DBSourceEBM = "bishop"/>
        <cfset var dataMail ={}/>
        
        <cfset ENA_Message = #arguments.inpContactString# & " - " & #arguments.inpBatchId#/>
        <cfset subjectLine = "API Response Trouble Ticket Subject Line - Status: "/>
        <cfset troubleShootingTips="API Trouble Ticket"/>
        <cfset ErrorNumber="9999"/>

        <cftry>
 			<cfif !IsNumeric(inpBatchId)>
            	<cfthrow message="Invalid Batch Specified"/>
            </cfif>
            <!--- desc API log --->
            <cfset ENA_Message = ENA_Message & "#arguments.inpMessageText#"/>

            <!--- Get user infor config by Batchid --->
            <cfquery name="GetUserConfigInfor" datasource="#DBSourceREAD#">
				SELECT 
					UserId_int,
                    OverDueTime_des
				FROM 
					troubletickets.configuration
				WHERE 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#arguments.inpBatchId#">
			</cfquery>
            <cfif GetUserConfigInfor.RecordCount GT 0>
                <!--- Save reponse and create trouble ticket --->
                <!---inital object of trouble ticket --->
                <cfset ticketObject = {
                    inpTicketNo: '',
                    inpTicketName: "Ticket #arguments.inpContactString#",
                    inpTicketContactString: "#arguments.inpContactString#",
                    inpTicketStatusId: 1,
                    inpTicketTypeId: '',                            
                    inpTicketPriorityId: 1,
                    inpTicketETA: #GetUserConfigInfor.OverDueTime_des#,
                    inpDescription: "#arguments.inpMessageText#"
                }/> 
                <cfinvoke method="SaveTroubleTicket" component="session.sire.models.cfc.troubleTicket.trouble-ticket" returnvariable="RetvarSaveTroubleTicket">
                    <cfinvokeargument name="inpTroubleTicketObject" value="#SerializeJSON(ticketObject)#"> 
                    <cfinvokeargument name="inpUserId" value="#GetUserConfigInfor.UserId_int#">        
                </cfinvoke>

                <!--- Get info notify --->
                <cfquery name="GetUserInfo" datasource="#DBSourceREAD#">
                    SELECT
                        UserId_int,
                        CONCAT(FirstName_vch ,' ', LastName_vch) as USERNAME,
                        EmailAddress_vch,
                        MFAContactString_vch
                    FROM 
                        simpleobjects.useraccount
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetUserConfigInfor.UserId_int#">                         
                </cfquery>

                <!--- Notify to User ---> 
                <cfif GetUserInfo.RecordCount GT 0>
                    <!--- format phone number (XXX) XXX-XXX --->
                    <cfif len(arguments.inpContactString) EQ 10>
                        <!--- This only works with 10-digit US/Canada phone numbers --->
                        <cfset formatphonenumber =  "(#left(arguments.inpContactString,3)#) #mid(arguments.inpContactString,4,3)#-#right(arguments.inpContactString,4)#" />
                    <cfelse>
                        <cfset formatphonenumber = arguments.inpContactString/>
                    </cfif>

                    <cfif RetvarSaveTroubleTicket.RXRESULTCODE EQ 1>
                        <!--- Create trouble ticket successfully --->
                        <cfset dataout.MESSAGE = "Your ticket id is ##" & RetvarSaveTroubleTicket.TROUBLETICKETID/>
                        <cfset dataMail.TROUBLETICKETID=RetvarSaveTroubleTicket.TROUBLETICKETID/>
                        <cfset textToSend = "A new trouble ticket (Id: #RetvarSaveTroubleTicket.TROUBLETICKETID#) just have created, Customer's phone number is #formatphonenumber#. Thanks!."/>
                    <cfelse>
                        <!--- Create trouble ticket fail --->
                        <cfset dataout.MESSAGE = ""/>
                        <cfset dataMail.TROUBLETICKETID=0/>
                        <cfset textToSend = "A new trouble ticket just have created fail, Customer's phone number is #formatphonenumber#. Please check email for detail."/>
                    </cfif>
                    <!--- Notify email --->
                    <cfif GetUserInfo.EmailAddress_vch NEQ "">
                        <cftry>
                            <cfset mailTo=GetUserInfo.EmailAddress_vch/>

                            <cfset dataMail.USERNAME=GetUserInfo.USERNAME/>
                            <cfset dataMail.MESSAGETEXT=arguments.inpMessageText/>
                            <cfset dataMail.CONTACTSTRING=formatphonenumber/>
                        
                           <!---  <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                <cfinvokeargument name="to" value="#mailTo#">
                                <cfinvokeargument name="type" value="html">
                                <cfinvokeargument name="subject" value="Notify Trouble Ticket Created">
                                <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_trouble_ticket_created.cfm">
                                <cfinvokeargument name="data" value="#serializeJSON(dataMail)#">
                            </cfinvoke> --->
                            <cfset arguments.data = serializeJSON(dataMail)/>
                            <cfmail to="#mailTo#" subject="Notify Trouble Ticket Created" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#"  port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                    <cfinclude template="/public/sire/views/emailtemplates/mail_template_notify_trouble_ticket_created.cfm" >
                            </cfmail>
                        <cfcatch>
                        </cfcatch>
                        </cftry>
                    </cfif>
                    <!--- Notify SMS --->
                    <cfif GetUserInfo.MFAContactString_vch NEQ "">
                        <cftry>
                            <cfset session.UserId = GetUserInfo.UserId_int/>
                            <cfset phone = ReReplaceNoCase(GetUserInfo.MFAContactString_vch,"[^0-9,]","","ALL")/>
                            <cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
                                <cfinvokeargument name="inpContactString" value="#phone#"/>
                                <cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
                                <cfinvokeargument name="inpTextToSend" value="#textToSend#"/>
                                <cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/> <!--- option to send to phone number has opt-out --->
                            </cfinvoke>
                        <cfcatch>
                        </cfcatch>
                        </cftry>
                    </cfif>
                </cfif>
                
                <!--- End notify --->
                <cfset dataout.RXRESULTCODE = 1 />
            </cfif>

            
            <cfcatch type="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1/>
                </cfif>
                <cfset dataout = StructNew() />            
                
                <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
                      
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(subjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(troubleShootingTips)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(dataout.ERRMESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
            </cfcatch>
        </cftry>
       
		<cfreturn representationOf(dataout.MESSAGE).withStatus(200) />

    </cffunction>
    
</cfcomponent>