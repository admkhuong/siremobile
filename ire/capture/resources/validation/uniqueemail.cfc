<cfcomponent extends="taffy.core.resource" taffy:uri="/validation/uniqueemail" hint="Validate email is not already in use">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Validate email is not already in use">
 		<cfargument name="inpEmail" type="string" required="false" default="" hint="eMail address to validate"/>
 		<cfargument name="inpCBPId" required="no" default="1" type="string">
        <cfargument name="DebugAPI" required="no" default="0">
 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var VerifyUnique = '' />
 		<cfset var DebugStr = "" />
                        
        <!--- Set the default Session DB Source --->
        <cfset Session.DBSourceEBM = "Bishop"/>
        <cfset Session.COMPANYUSERID = "Session.UserId"/>
        			                   
 		<cftry>
 			 	      	        	
        	<!--- Validate Email Unique --->               
            <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(EmailAddress_vch) AS TOTALCOUNT
                FROM
                    simpleobjects.useraccount
                WHERE                
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(arguments.inpEmail)#">
                AND
                    CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCBPId#"> 
                AND
                    Active_int > 0       
            </cfquery>  
            
            <cfif VerifyUnique.TOTALCOUNT GT 0>
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
            <cfelse>
                <cfset dataout.RXRESULTCODE = 1 />     
            </cfif>          
            
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />

    </cffunction>
   
</cfcomponent>