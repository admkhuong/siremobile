<cfcomponent extends="taffy.core.resource" taffy:uri="/validation/email" hint="Validate email is in proper format">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Validate email is in proper format">
 		<cfargument name="inpEmail" type="string" required="false" default="" hint="eMail address to validate"/>
 		<cfargument name="inpRegExpEStr" TYPE="string" required="false" default="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$" hint="optional regular expression string to validate against">
        <cfargument name="DebugAPI" required="no" default="0">
 		 		 				 		
 		<cfset var dataOut = false>
        <cfset var DebugStr = "" />
			                   
 		<cftry>
 		
	 		<cfif len(TRIM(inpEmail)) eq 0>
	        	<cfset dataout = false />
	        </cfif>
	
	        <cfif REFind(inpRegExpEStr,TRIM(inpEmail)) eq 1>
	        	<cfset dataout = true />
	        <cfelse>
	        	<cfset dataout = false />
	        </cfif>
        	
<!---
        	<cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "1" />
            <cfset dataout.TYPE = "test" />
            <cfset dataout.ERRMESSAGE = "test" />
--->
            
            
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />

    </cffunction>
   
</cfcomponent>