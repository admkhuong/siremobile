<cfcomponent extends="taffy.core.resource" taffy:uri="/validation/password" hint="Validate password is 8 character, has at least 1 alpha and 1 number, and no 'Special' characters and only contains Alphabet, numbers and _ *%$@!?+-">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="post" access="public" output="false" hint="Validate email is in proper format">
 		<cfargument name="inpPassword" type="string" required="false" default="" hint="eMail address to validate"/>
 		<cfargument name="inpRegExpStr" TYPE="string" required="false" default="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" hint="optional regular expression string to validate against">
        <cfargument name="DebugAPI" required="no" default="0">
 		 		 				 		
 		<cfset var dataOut = false>
        <cfset var DebugStr = "" />
			                   
 		<cftry>
 			
	        <cfif REFind(inpRegExpStr,TRIM(inpPassword)) eq 1>
	        	<cfset dataout = true />
	        <cfelse>
	        	<cfset dataout = false />
	        </cfif>
        	
<!---
        	<cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "1" />
            <cfset dataout.TYPE = "test" />
            <cfset dataout.ERRMESSAGE = "test" />
--->
            
            
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>
			
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />

    </cffunction>
   
</cfcomponent>