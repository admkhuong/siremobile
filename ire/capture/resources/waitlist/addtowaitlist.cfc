<cfcomponent extends="taffy.core.resource" taffy:uri="/waitlist/addtowaitlist" hint="Add customer to waitlist">
	<cfinclude template="../../paths.cfm" >
	<!--- <cfinclude template="../../../../public/sire/configs/paths.cfm"/> --->
	
	<cffunction name="POST" access="public" output="false" hint="Add customer to waitlist">
 		<cfargument name="inpContactString" required="true" hint="phone number">
		<cfargument name="inpBatchId" required="true" hint="batch id">

		<cfset var dataout = {} />
		<cfset var insertCustomerToWaitlist = '' />
		<cfset var selectWaitlist = '' />
		<cfset var selectWaitlistId = '' />
		<cfset var getLastOrder = ''/>
		<cfset var lastOrder = 1/>
		<cfset var createWaitlist = ''/>
		<cfset var createWaitlistResult = ''/>
		<cfset var token = ''/>
		<cfset var rootUrl =''/>
		<cfset var listId = 0/>
        <cfset var i = '' />
		<cfset var rsInsertLog = "">
		<cfset var apiUrl = "https://www.googleapis.com/urlshortener/v1/url?key="&#ServerAPIKeyShortUrl# />
		<cfset var httpResp = {} />
        <cfset var dataResp = {} />
        <cfset var dataPost = "" />
		<cfset var inpLongUrl = "" />


		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.ORDER = 0>
		<cfset dataout.WAITLISTNAME = ''>
		<cfset dataout.LINK = ''>

	<cftry>
			<!--- CHECK IF WAILIST IS EXITS OR ACTIVE ---> 
			<cfquery name="selectWaitlistId" datasource="#DBSourceEBM#">
				SELECT
					w.ListId_bi,w.UserId_int, w.ListName_vch
				FROM
					simpleobjects.waitlist w
				WHERE
					w.CaptureBatchId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpBatchId#"/>
				AND
					w.Active_ti = 1
				LIMIT 
					1
			</cfquery>

			<cfif selectWaitlistId.Recordcount EQ 0>
				<cfset dataout.RXRESULTCODE = 2>
				<cfset dataout.MESSAGE = 'This waitlist does not exits or inactived'>
				<cfreturn representationOf(dataout).withStatus(200) />
			<cfelse>
				<cfset listId = selectWaitlistId.ListId_bi/>
			</cfif>

			<!--- CHECK IF PREVIEW --->
			<cfif FindNoCase('900000001', arguments.inpContactString) GT 0>
				<cfset dataout.ORDER = 1>
				<cfset dataout.WAITLISTNAME = '#selectWaitlistId.ListName_vch#'>
				<cfset dataout.LINK = "https://siremobile.com">
				<cfset dataout.RXRESULTCODE = 1 />
				<cfreturn representationOf(dataout).withStatus(200) />
			</cfif>

			<cfquery name="selectWaitlist" datasource="#DBSourceEBM#">
				SELECT
					wa.WaitListId_bi
				FROM
					simpleobjects.waitlistapp wa
				WHERE
					wa.CustomerContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpContactString#"/>
				AND
					wa.ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#listId#"/>	
				AND
					wa.Active_ti = 1
				AND
					wa.Status_int = 1
				LIMIT 
					1
			</cfquery>

			<cfif selectWaitlist.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.WAITLISTNAME = '#selectWaitlistId.ListName_vch#'>
				<cfset dataout.MESSAGE = 'This phone number already in waitlist.'>
			<cfelse>
				<!--- GET MAX ORDER IN WAILIST APP --->
				<cfquery name="getLastOrder" datasource="#DBSourceEBM#">
					SELECT
						Order_bi
					FROM
						simpleobjects.waitlistapp
					WHERE
						ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#listId#"/>
					AND
						Active_ti = 1
					AND
						Status_int = 1
					ORDER BY
						Order_bi
					DESC
					LIMIT 1
				</cfquery>

				<cfif getLastOrder.Recordcount GT 0>
					<cfset lastOrder = getLastOrder.Order_bi+1>
				</cfif>

				<!--- GEN TOKEN --->
		        <!--- Create string --->
		        <cfloop index="i" from="1" to="9">
		            <!--- Random character in range A-Z --->
		            <cfset token=token&Chr(RandRange(97, 122))>
		        </cfloop>
		        <!--- Encrtypt --->
		        <cfset token = toBase64(token&'_'&NOW())>

				<!--- INSERT NEW WAITLIST APP --->
				<cfquery name="createWaitlist" datasource="#DBSourceEBM#" result="createWaitlistResult">
					INSERT INTO
						simpleobjects.waitlistapp 
						(
							Order_bi,
							ServiceId_int,
							Status_int,
							CustomerName_vch,
							CustomerEmail_vch,
							CustomerContactString_vch,
							WaitListSize_int,
							WaitTime_int,
							Note_vch,
							Created_dt,
							UserId_int,
							Token_vch,
							ListId_bi
						)
					VALUES
						(
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#lastOrder#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
							NOW(),
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#selectWaitlistId.UserId_int#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#token#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#listId#"/>
						)
				</cfquery>

				<cfif createWaitlistResult.Recordcount GT 0>
					<!--- Log state --->
					<cfquery result="rsInsertLog" datasource="#DBSourceEBM#">
						INSERT INTO 
							simpleobjects.waitlistapplogs
							(
								WaitlistID_bi, 
								Status_int, 
								CreatedAt_dt
							)
						VALUES
							(
								<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#createWaitlistResult.GENERATED_KEY#">,
								<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="1">,
								NOW()
							)
					</cfquery>

					<cfif FINDNOCASE(CGI.SERVER_NAME,'sire.lc') GT 0>
						<cfset rootUrl = 'sire.lc'>
					<cfelseif FINDNOCASE(CGI.SERVER_NAME,'apiqa.siremobile.com') GT 0>
						<cfset rootUrl = 'awsqa.siremobile.com'>
					<cfelseif FINDNOCASE(CGI.SERVER_NAME,'api.siremobile.com') GT 0>
						<cfset rootUrl = 'siremobile.com'>	
					</cfif>

					<!--- Get short URL --->
					<cfset inpLongUrl = "https://#rootUrl#/waitlist-customer-view?token=#token#" />

					<cfset dataPost = "{longUrl: '"&#inpLongUrl#&"'}" />

					<cfhttp url="#apiUrl#" charset="utf-8" method="POST" result="httpResp" timeout="120">
		                <cfhttpparam type="header" name="Content-Type" value="application/json" />
		                <cfhttpparam type="body" value="#dataPost#">
		            </cfhttp>

            		<cfset dataResp = DeserializeJSON(httpResp.filecontent) />

					<cfset dataout.ORDER = lastOrder>
					<cfset dataout.WAITLISTNAME = '#selectWaitlistId.ListName_vch#'>
					<cfset dataout.LINK = dataResp.id>
					<cfset dataout.RXRESULTCODE = 1 />

				</cfif>

			</cfif>	
			
		<cfcatch type="any">
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
				<cfset dataout.RXRESULTCODE = -1 />
			</cfcatch>
		</cftry>  

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>


    <cffunction name="GET" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = {} />  
        
        <cfset dataout.MESSAGE = "You should use the verb POST JSON or FORM variables to this API location. Currently you only using GET!" />
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>

</cfcomponent>