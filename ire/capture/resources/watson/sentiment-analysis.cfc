<cfcomponent extends="taffy.core.resource" taffy:uri="/watson/sentiment-analysis" hint="Capture user input, return their sentiment">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword and which User to bill extra credits for this feature"/>
        <cfargument name="inpContactString" required="no" default="" hint="Contact string">
        <cfargument name="inpMessageText" required="no" default="" hint="Customer Input or Issue">
        		
		<cfset var dataOut = ''>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var insertResult = '' />
		<cfset var DBSourceEBM = "Bishop"/> 		
		
		<cfset var WatsonAPIKey = "20088fb0eb5b3ee6ecff1bc867b53c014ccfb940" />
				                    
        <!--- Formatted Date Time --->
        <cfset dataOut = StructNew() /> 
        <cfset dataOut.status = "ERROR" />  
        <cfset dataOut.language = "English" /> 
        <cfset dataOut.docSentiment = StructNew() />
        <cfset dataOut.mixed = 0 />  
        <cfset dataOut.score = "0.85" />  
        <cfset dataOut.type = "unknown" />  
                      
         
         <!--- 
	         RESPONSE
Name	Description
status	OK or ERROR
Indicates whether the request was successful
url	HTTP URL that was passed in the input
language	Detected language of the source text (text with fewer than 15 characters is assumed to be English)
docSentiment	Object containing document-level sentiment information
mixed	1 indicates that the sentiment is both positive and negative
score	Sentiment strength (0.0 == neutral)
type	Sentiment polarity: positive, negative, or neutral

  
	          --->                 
      
		<cftry>
           
           
           
           <!--- Charge user account credits for access --->
           
           
        
        	<!--- Replace any existing status for today with current status (use update with insert? ) --->	
        
        	<!---
	        	
	        	
	        	{inpBatchId:"{%SRBATCHID%}",inpContactString:"{%SRCONTACTSTRING%}",inpListOfEmails:"lee@siremobile.com",inpMessageText:"{%INPPA=2%}"}

{
	"inpBatchId": "{%SRBATCHID%}",
	"inpContactString": "{%SRCONTACTSTRING%}",
	"inpListOfEmails": "lee@siremobile.com",
	"inpMessageText": "{%INPPA=2%}"
}

				{ "status": "OK", "usage": "By accessing AlchemyAPI or using information generated by AlchemyAPI, you are agreeing to be bound by the AlchemyAPI Terms of Use: http://www.alchemyapi.com/company/terms.html", "totalTransactions": "1", "language": "english", "docSentiment": { "score": "-0.794958", "type": "negative" } }

  
			--->
           
           
			<cfhttp url="https://access.alchemyapi.com/calls/text/TextGetTextSentiment" method="get" result="returnStruct" port="443"  >
			    
				<cfhttpparam type="URL" name="apikey" value="#WatsonAPIKey#">	
				<cfhttpparam type="URL" name="text" value="#inpMessageText#">	
				<cfhttpparam type="URL" name="outputMode" value="json">	
			     
			</cfhttp>
			
			
<!--- 			<cfset dataOut.json = "#returnStruct.Filecontent#" /> --->   
			
			<cfif IsJSON(returnStruct.Filecontent)>
	                    	
            	<cfset PostResultJSON = deSerializeJSON(returnStruct.Filecontent) />
            		                        	                        	
            	<cfif StructKeyExists(PostResultJSON, "status")>
                	<cfset dataOut.status = "#PostResultJSON.status#" />  
                </cfif>	

				<cfif StructKeyExists(PostResultJSON, "language")>
                	<cfset dataOut.language = "#PostResultJSON.language#" />  
                </cfif>	
                               
                <cfif StructKeyExists(PostResultJSON, "docSentiment")>
                	                	
                	<cfif StructKeyExists(PostResultJSON.docSentiment, "mixed")>
					  	<cfset dataOut.mixed = "#PostResultJSON.docSentiment.mixed#" />  
                	</cfif>	                  
                  
					<cfif StructKeyExists(PostResultJSON.docSentiment, "score")>
					  	<cfset dataOut.score = "#PostResultJSON.docSentiment.score#" /> 
                	</cfif>	
                	
                	<cfif StructKeyExists(PostResultJSON.docSentiment, "type")>
					  	<cfset dataOut.type = "#PostResultJSON.docSentiment.type#" /> 
                	</cfif>	
                	
                </cfif>	
            
            <cfelse>
            	
            
        	</cfif>	
						    		
            		   	                                   
        <cfcatch>        	
                            
                        <!--- For debbugging only! --->    
						<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->             
                <cftry>        
                
                	<cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX Watson sentiment-analysis API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>