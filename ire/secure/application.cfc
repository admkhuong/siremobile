<cfcomponent extends="taffy.core.api">
    
    <!--- Set up the application. --->
    
    <!--- Naming application as a hash of path helps keep it unique--->
    <cfset THIS.Name = hash(getCurrentTemplatePath()) />
    <cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
    <cfset THIS.SessionManagement = true />
    <cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 5, 0 ) />
    <cfset THIS.SetClientCookies = false />
    <cfset THIS.clientmanagement = false />
    <cfset THIS.setdomaincookies = false />
    <cfset THIS.setdomaincookies = false />
    
    <!--- Make an application level reference to the RAM memory virtual file system so we can reference it in cfincludes --->
	<cfset THIS.mappings[ "/ram" ] = "ram://" />	

	<cfinclude template="paths.cfm" >
	
	<!---
		variables.framework = {};
		variables.framework.debugKey = "debug";
		variables.framework.reloadKey = "reload";
		variables.framework.reloadPassword = "true";
		variables.framework.representationClass = "com.ebm.core.genericRepresentation";
		variables.framework.returnExceptionsAsJson = true;
		
		
		variables.framework = {};
		variables.framework.representationClass = "taffy.core.EBMRepresentation";
	--->
    
    <!---session.skey=headers["authorization"];--->
            
	<cfscript>
	
		// Need this as of taffy 3.1 and later
		this.mappings["/resources"] = listDeleteAt(cgi.script_name, listLen(cgi.script_name, "/"), "/") & "/resources";


		variables.framework = {};
		// Name of the url parameter that enables CF Debug Output.
		variables.framework.debugKey = "debug";
		// This lets us define multiple return formats 
		variables.framework.serializer = "#websericeCommonDotPath#.multiformatserializerclass";
		variables.framework.deserializer = "#websericeCommonDotPath#.multiformatdeserializerclass";
		
		// Turns off error logging 
		variables.framework.exceptionLogAdapter = "taffy.bonus.LogToDevNull";
		
		// Requires this key to reload config		
		variables.framework.reloadPassword = "secret";
			
		

		// this function is called after the request has been parsed and all request details are known
		function onTaffyRequest(verb, cfc, requestArguments, mimeExt, headers){
			
			
			APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L";
     		APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString";
     	
			// Moved to global inside multiFormatRepresentationClass.cfc
			// application.JsonUtil = createObject("component", "#websericeCommonDotPath#.JSONUtil.JSONUtil");
			// application.AnythingToXML = createObject("component", "#websericeCommonDotPath#.AnythingToXML.AnythingToXML");
			
			
			createObject("component", "#websericeCommonDotPath#.logging").LogRequest(headers, verb, requestArguments, cfc);
									
			//require some api key
			authorizeObj = createObject("component", "#websericeCommonDotPath#.Authorization").checkAuthorization(headers, verb, requestArguments);
			if(authorizeObj["SUCCESS"] == false){
				return newRepresentation().noData().withStatus(authorizeObj["STATUS_CODE"], authorizeObj["MESSAGE"]);
			}

			session.userid= authorizeObj['USER_ID'];

			return true;
		}

		// called when taffy is initializing or when a reload is requested
		function configureTaffy(){
			// Usage of this function is entirely optional. You may omit it if you want to use the default representation class.
			// Change this to a custom class to change the default for the entire API instead of overriding for every individual response.
			// super.setDefaultRepresentationClass("#websericeCommonDotPath#.multiFormatRepresentationClass");
						
        	// super.registerMimeType("json", "application/json");
			// super.registerMimeType("xml", "application/xml");
			
        	// super.setDefaultMime("json");     
		
			//setDefaultRepresentationClass("com.ebm.bonus.AnythingToXmlRepresentation");
			
		}
	</cfscript>
</cfcomponent>
