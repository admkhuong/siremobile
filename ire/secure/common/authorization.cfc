<cfcomponent output="false" name="Authorization" hint="check authorization">
	<cfinclude template="../paths.cfm" >
	
	<cffunction name="init" access="public" output="false" returntype="Object" hint="constructor">
        <cfreturn this />
    </cffunction>
	
	<cffunction name="checkAuthorization" access="public" output="false">
		<cfargument name="requestHeader" type="struct" required="true" default="" />
		<cfargument name="verb" type="string" required="true" default="GET" />
        <cfargument name="requestArguments" type="struct" required="true" default="" />
		
        
        
        <cfset var retVal	= '' />
		<cfset var requestDate	= '' />
        <cfset var authorization	= '' />
        <cfset var authorArray	= '' />
        <cfset var requestAccessKey	= '' />
        <cfset var requestSignature	= '' />
        <cfset var stringToSign	= '' />
        <cfset var signature	= '' />
        <cfset var GetSecretKey	= '' />

		<cftry>
			<!--- init return value----->
			<cfset retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "Authorization failure.">
			<cfset retVal.STATUS_CODE = 401>
			
			<cfif NOT structKeyExists(requestHeader, "datetime")>
				
                <!---<cfthrow errorcode="401" type="any" message="Authorization failure. Datetime header is required.  {#DBSourceEBM#}">	--->
                <cfset requestHeader.datetime = "#LSDateFormat(now(), 'yyyy-mm-dd')##LSTimeFormat(now(), 'HH:mm:ss')#" />                
                
			</cfif>
            
			<cfif NOT structKeyExists(requestHeader, "authorization")>
            
            	<cfif NOT structKeyExists(requestArguments, "authorization")> 
				
					<!--- Check for alternate request headers --->
                    <cfif NOT structKeyExists(requestHeader, "inpusername") OR NOT structKeyExists(requestHeader, "inppassword") >
                    
                        <!--- Check for alternate request parameters  --->
                        <cfif NOT structKeyExists(requestArguments, "inpusername") OR NOT structKeyExists(requestArguments, "inppassword") >
                        
                           <cfthrow errorcode="401" type="any" message="Authorization failure. Authorization header is required. {#DBSourceEBM#}">	
                                
                        <cfelse>                		
                            
                             <cfset authorization = "#requestArguments.inpusername#:#requestArguments.inppassword#" /> 
                        
                        </cfif>
                        
                    <cfelse>
                        
                        <cfset authorization = "#requestArguments.inpusername#:#requestArguments.inppassword#" />  
                    
                    </cfif>
               
               <cfelse>
               
               		<cfset authorization = requestArguments["authorization"]>
               
               </cfif>                         
                
            <cfelse>
                
				<cfset authorization = requestHeader["authorization"]>
                
			</cfif>
                        
			<cfset requestDate = requestHeader["datetime"]>		
			
			<!--- Support for basic authentication if basic is found then assume remainder is base64 encoded --->
			<cfif FindNoCase("basic", authorization) GT 0>
				
				<cfset authorization = ReplaceNoCase(authorization, "basic ", "", "all")>
				
				<!--- Base64 decode --->
				<cfset authorization = ToString( ToBinary( authorization ) ) />
				
			</cfif>
			
			<cfset authorArray = authorization.split(":")>
			<cfif arrayLen(authorArray) NEQ 2>
				<cfthrow errorcode="401" type="any" message="Authorization failure. Signature and access key headers are required. {#DBSourceEBM#} {#authorization#}">
			</cfif>

			<cfset requestAccessKey = authorArray[1]>
			<cfset requestSignature = authorArray[2]>
			
			<!--- get scret key from database --->
			<cfquery name="GetSecretKey" datasource="#DBSourceEBM#">
				SELECT 
					SECRETKEY_VCH,
					USERID_INT,
                    SECRETNAME_VCH
				FROM 
					simpleobjects.securitycredentials
				WHERE 
					ACCESSKEY_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#requestAccessKey#">
				AND
					ACTIVE_TI = 1
			</cfquery>
			
			<cfif GetSecretKey.RecordCount EQ 0>
				<cfthrow errorcode="401" type="any" message="Authorization failure. Key combo does not exist or is inactive. {#DBSourceEBM#}">
			<cfelse>
            	
                <!--- Optionally validate agains Secret name - easier but less secure  --->
                <!--- WARNING _ NO : allowed in name --->
               
				<cfif UCASE(TRIM(GetSecretKey.SECRETNAME_VCH)) EQ UCASE(TRIM(requestSignature)) OR UCASE(TRIM(GetSecretKey.SECRETKEY_VCH)) EQ UCASE(TRIM(requestSignature)) >
                
                    <cfset retVal = StructNew()>
                    <cfset retVal.SUCCESS = true>
                    <cfset retVal.MESSAGE = "Authorization successfully.">
                    <cfset retVal.USER_ID = "#GetSecretKey.USERID_INT#">
                    <cfset retVal.STATUS_CODE = 200>
                    
                    <cfreturn retVal />
                
                </cfif>
                
            
				<!--- Create signature --->
				<cfset stringToSign = "#arguments.verb#\n\n\n#requestDate#\n\n\n">
				<cfset signature = createSignature(stringToSign, GetSecretKey.SECRETKEY_VCH)>
				<cfif signature NEQ requestSignature>
                	<!--- gs=#signature# rs=#requestSignature# pk=#requestAccessKey# sk=#GetSecretKey.SECRETKEY_VCH# --->
					<cfthrow errorcode="401" type="any" message="Authorization failure. Secret key does not match! verb=#arguments.verb# ts=#requestDate# ">
				<cfelse>
					<cfset retVal = StructNew()>
					<cfset retVal.SUCCESS = true>
					<cfset retVal.MESSAGE = "Authorization successfully.">
					<cfset retVal.USER_ID = "#GetSecretKey.USERID_INT#">
					<cfset retVal.STATUS_CODE = 200>
				</cfif>
			</cfif>
		<cfcatch>
			<cfset retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "#cfcatch.MESSAGE# #cfcatch.Detail#  {#DBSourceEBM#} {#CGI.SERVER_NAME#}">
			<cfset retVal.STATUS_CODE = 401>
		</cfcatch>	
		</cftry>
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="checkSoapAuthorization" access="public" output="false">
		<cfargument name="operation" required="true" type="string" default="" hint="Operation" />
		<cfargument name="accessKeyId" required="true" type="string" default="" hint="access key" />
		<cfargument name="timestamp" required="true" type="string" default="" hint="date time" />
		<cfargument name="signature" required="true" type="string" default="" hint="date time" />
		
        <cfset var stringToSign	= '' />
		<cfset var dbSignature	= '' />
		<cfset var GetSecretKey	= '' />

		<cftry>
			<!--- init return value----->
			<cfset var retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "Authorization fail.">
			
			<cfif accessKeyId EQ "">
				<cfthrow errorcode="401" type="any" message="Authorization failure. Authorization is required.  {#DBSourceEBM#}">	
			</cfif>
			<cfif timestamp EQ "">
				<cfthrow errorcode="401" type="any" message="Authorization failure. Timestamp is required.  {#DBSourceEBM#}">	
			</cfif>
			<cfif signature EQ "">
				<cfthrow errorcode="401" type="any" message="Authorization failure. Signature is required.  {#DBSourceEBM#}">
			</cfif>

			<!--- get scret key from database --->
			<cfquery name="GetSecretKey" datasource="#DBSourceEBM#">
				SELECT 
					SECRETKEY_VCH,
					USERID_INT,
                    SECRETNAME_VCH
				FROM 
					simpleobjects.securitycredentials
				WHERE 
					ACCESSKEY_VCH = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.accessKeyId#">
				AND
					ACTIVE_TI = 1
			</cfquery>
			
			<cfif GetSecretKey.RecordCount EQ 0>
				<cfthrow errorcode="401" type="any" message="Authorization failure. Seccret key does not exist or is inactived  {#DBSourceEBM#}">
			<cfelse>
            
             	<!--- Optionally validate agains Secret name - easier but less secure  --->
                <!--- WARNING _ NO : allowed in name --->
                <cfif LEN(GetSecretKey.SECRETNAME_VCH) GT 0> 
					<cfif UCASE(TRIM(GetSecretKey.SECRETNAME_VCH)) EQ UCASE(TRIM(requestSignature))>
                    
						<cfset retVal = StructNew()>
                        <cfset retVal.SUCCESS = true>
                        <cfset retVal.MESSAGE = "Authorization successfully.">
                        <cfset retVal.USER_ID = "#GetSecretKey.USERID_INT#">
                        <cfset retVal.STATUS_CODE = 200>
                        
                        <cfreturn retVal />
                    
                    </cfif>
                </cfif>   
                 
				<!--- Create signature --->
				<cfset stringToSign = "#arguments.operation#\n\n\n#arguments.timestamp#\n\n\n">
				<cfset dbSignature = createSignature(stringToSign, GetSecretKey.SECRETKEY_VCH)>
				<cfif signature NEQ dbSignature>
					<cfthrow errorcode="401" type="any" message="Authorization failure. Secret key does not match.  {#DBSourceEBM#}">
				<cfelse>
					<cfset retVal = StructNew()>
					<cfset retVal.SUCCESS = true>
					<cfset retVal.MESSAGE = "Authorization successfully.">
					<cfset retVal.USER_ID = "#GetSecretKey.USERID_INT#">
				</cfif>
			</cfif>
		<cfcatch>
			<cfset retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "#cfcatch.MESSAGE#  {#DBSourceEBM#} {#CGI.SERVER_NAME#}">
		</cfcatch>	
		</cftry>
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	   <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="secretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(secretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
</cfcomponent>