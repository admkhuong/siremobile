<cfcomponent output="false">
	<cfinclude template="../paths.cfm" >
	<cfinclude template="constants.cfm" >
	
	<cffunction name="checkLimitAccess" access="public" output="false">
		<cfargument name="inpUserId" type="string" required="true" default="" />
		
        <cfset var retVal	= '' />
		<cfset var accessLimit	= '' />
        <cfset var accessPeriod	= '' />
        <cfset var usage	= '' />
        <cfset var GetLimitAccessConfig	= '' />

		<cftry>
			<!--- init return value----->
			<cfset retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "You have been getting limit access.">
			<cfset retVal.STATUS_CODE = 401>
			
			<!----Getting limit access configuration----->
			<cfquery name="GetLimitAccessConfig" datasource="#DBSourceEBM#">
				SELECT
					UserId_int, 
					Type_ti, 
					Updated_dt, 
					ApiLimit_int, 
					time_ti
				FROM
					simpleobjects.apiaccessthrottling
				WHERE 
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
			</cfquery>
			
			<!--- unlimit access if super admin does not set ApiThrottling --->
			<cfif GetLimitAccessConfig.RecordCount GT 0>
				<cfset accessLimit = GetLimitAccessConfig.ApiLimit_int>
				<cfset accessPeriod = 60> <!--- 60 seconds --->
				
				<cfswitch expression="#GetLimitAccessConfig.time_ti#">
					<cfcase value="0">
						<cfset accessPeriod = limit_by_minute>
					</cfcase>
					<cfcase value="1">
						<cfset accessPeriod = limit_by_hour>
					</cfcase>
					<cfcase value="2">
						<cfset accessPeriod = limit_by_day>
					</cfcase>
					<cfcase value="3">
						<cfset accessPeriod = limit_by_week>
					</cfcase>
					<cfcase value="4">
						<cfset accessPeriod = limit_by_month>
					</cfcase>
				</cfswitch>
				<cfset usage = getAccessRate(inpUserId, accessLimit,accessPeriod)>
				<cfif usage LT accessLimit>
					<cfset logAccess(inpUserId)>
					<cfset retVal.SUCCESS = true>
					<cfset retVal.MESSAGE = "Access ok.">
					<cfset retVal.STATUS_CODE = 200>
				<cfelse>
					<cfset retVal.SUCCESS = false>
					<cfset retVal.MESSAGE = "You have been getting limit access.">
					<cfset retVal.STATUS_CODE = 401>
				</cfif>
			<cfelse>
				<cfset retVal.SUCCESS = true>
				<cfset retVal.MESSAGE = "Access ok.">
				<cfset retVal.STATUS_CODE = 200>
			</cfif>			
		<cfcatch>
			<cfset retVal = StructNew()>
			<cfset retVal.SUCCESS = false>
			<cfset retVal.MESSAGE = "#cfcatch.MESSAGE# #cfcatch.Detail#">
			<cfset retVal.STATUS_CODE = 401>
		</cfcatch>	
		</cftry>
		<cfreturn retVal />
	</cffunction>
	
	<cffunction name="getAccessRate" access="private" output="false">
		<cfargument name="inpUserId" required="true" />
		<cfargument name="inpAccessLimit" required="true" />
		<cfargument name="inpAccessPeriod" required="true" />
		
		<cfset var local = structNew() />
        <cfset var GetAccessTime	= '' />
        
		<!--- now get matches for the current api key --->
		<cfquery name="GetAccessTime" datasource="#DBSourceEBM#">
			SELECT 
				COUNT(*) AS TOTALCOUNT
			FROM 
				simplexresults.apiaccesslog
			WHERE 
				userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#" />
			AND 
				Created_dt > DATE_ADD(now(), INTERVAL -#inpAccessPeriod# SECOND)
		</cfquery>
		
		<!---  *** Nthis needs to be moved to CRON job if access log is getting long, do some cleanup --->
		<cfif GetAccessTime.TOTALCOUNT GTE inpAccessLimit>
			<cfset pruneAccessLog(inpUserId, inpAccessPeriod) />
		</cfif>
		
		<cfreturn GetAccessTime.TOTALCOUNT />
	</cffunction>

	<cffunction name="logAccess" access="private" output="true">
		<cfargument name="inpUserId" required="true" type="string" />
		
        <cfset var logAccessTime	= '' />
        
		<cfquery name="logAccessTime" datasource="#DBSourceEBM#" >
			INSERT INTO
				simplexresults.apiaccesslog
                (
					UserId_int,
					Created_dt
				)
			VALUES(
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
				NOW()
			)
		</cfquery>
	</cffunction>

	<cffunction name="pruneAccessLog" access="private" output="false">
		<cfargument name="inpUserId" required="true" type="string" />
		<cfargument name="inpAccessPeriod" required="true" />
		
        <cfset var pruneAccessLog	= '' />
        
		<cfquery name="pruneAccessLog" datasource="#DBSourceEBM#">
			DELETE FROM 
				simplexresults.apiaccesslog
			WHERE 
				Created_dt <  DATE_ADD(now(), INTERVAL -#inpAccessPeriod# SECOND)
			AND
				UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#" />
		</cfquery>
	</cffunction>
	
</cfcomponent>