<cfscript>
	limit_by_minute = 60;  // seconds
	limit_by_hour = 60*60; // seconds
	limit_by_day = 60*60*24; // seconds
	limit_by_week = 60*60*24 * 7; // seconds
	limit_by_month = 60*60*24*30; // seconds
	
	SCHEDULETYPE_INT_NOW = 0;
	START_DT_NOW = "";
	STOP_DT_NOW = "";
	STARTHOUR_TI_NOW = 0;
	STARTMINUTE_TI_NOW = 0;
	ENDHOUR_TI_NOW = 0;
	ENDMINUTE_TI_NOW = 0;
	ENABLEDBLACKOUT_BT_NOW = true;
	BLACKOUTSTARTHOUR_TI_NOW = 0;
	BLACKOUTSTARTMINUTE_TI_NOW = 0;
	BLACKOUTENDHOUR_TI_NOW = 0;
	BLACKOUTENDMINUTE_TI_NOW = 0;
	DAYID_INT_NOW = 0;
	TIMEZONE_NOW = "";
	
	
	SCHEDULETYPE_INT_ENABLE = 2;
	START_DT_ENABLE = #DateFormat(Now(),'yyyy/mm/dd')#;
	STOP_DT_ENABLE = #DateFormat(Now()+30,'yyyy/mm/dd')#;
	STARTHOUR_TI_ENABLE = 9;
	STARTMINUTE_TI_ENABLE = 0;
	ENDHOUR_TI_ENABLE = 19;
	ENDMINUTE_TI_ENABLE = 0;
	ENABLEDBLACKOUT_BT_ENABLE = false;
	BLACKOUTSTARTHOUR_TI_ENABLE = 0;
	BLACKOUTSTARTMINUTE_TI_ENABLE = 0;
	BLACKOUTENDHOUR_TI_ENABLE = 0;
	BLACKOUTENDMINUTE_TI_ENABLE = 0;
	DAYID_INT_ENABLE = 0;
	TIMEZONE_ENABLE = "";
	
	HTTPSTATUSCODES = {
		OK = 200,
		Created = 201,
		Bad_Request = 400,
		Unauthorized = 401,
		Forbidden = 403,
		Not_Found = 404,
		Method_Not_Allowed = 405,
		Conflict = 409,
		Internal_Server_Error = 500
	};
    CONTACTTYPE ={
        VOICE = 1,
        EMAIL = 2,
        SMS = 3
    };
    
    TITLE = "Section Title";
    DESCRIPTION = "Section Description";
</cfscript>

<cfset sessionDotPath = "session.cfc" >
