<cfcomponent output="false" name="Authorization" hint="check authorization">
	<cfinclude template="../paths.cfm" >
	
	<cffunction name="init" access="public" output="false" returntype="Object" hint="constructor">
        <cfreturn this />
    </cffunction>
	
	<cffunction name="LogRequest" access="public" output="false">
		<cfargument name="requestHeader" type="struct" required="true" default="" />
		<cfargument name="verb" type="string" required="true" default="GET" />
        <cfargument name="requestArguments" type="struct" required="true" default="" />
        <cfargument name="cfc" type="string" required="true" default="" />
		       
        <cfset var InsertToAPILog	= '' />
        
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
                                                  
            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1113,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,  
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SimpleX API Notification - Entry Point">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(requestHeader)# #verb# #serializeJSON(requestArguments)# #cfc#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(GetHttpRequestData())#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_REFERE),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                )
            </cfquery>
        
        <cfcatch type="any">
        
        
        </cfcatch>
            
        </cftry>            
        
        
	</cffunction>
	
</cfcomponent>