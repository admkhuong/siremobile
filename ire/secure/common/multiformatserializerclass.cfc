<!---<cfcomponent extends="taffy.core.baseRepresentation">
	<!---<cfinclude template="../../../paths.cfm" >--->
	<cfset variables.jsonUtil = createObject("component", "ire.sms.common.JSONUtil.JSONUtil") />
	<cfset variables.AnythingToXML = createObject("component", "ire.sms.common.AnythingToXML.AnythingToXML") />

	<cffunction name="getAsJSON" taffy:mime="application/json" taffy:default="true" output="false">
		<cfreturn variables.jsonUtil.serializeToJSON(variables.data) />
	</cffunction>

	<cffunction name="getAsXML" taffy:mime="application/xml" output="false">
		<cfreturn variables.AnythingToXML.ToXML(variables.data) />
	</cffunction>

</cfcomponent>
--->


<cfcomponent extends="taffy.core.baseSerializer" output="false">

	<cfset variables.jsonUtil = createObject("component", "ire.sms.common.JSONUtil.JSONUtil") />
	<cfset variables.AnythingToXML = createObject("component", "ire.sms.common.AnythingToXML.AnythingToXML") />
    
    <cffunction
        name="getAsJson"
        output="false"
        taffy:mime="application/json;text/json"
        taffy:default="true"
        hint="serializes data as JSON">
            <cfreturn serializeJSON(variables.data) />
    </cffunction>
    
    <cffunction name="getAsXML" taffy:mime="application/xml;text/xml" output="false">
		<cfreturn variables.AnythingToXML.ToXML(variables.data) />
	</cffunction>

</cfcomponent>


