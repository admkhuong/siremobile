<cfcomponent>
	<cffunction name="VldInput" access="public" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$@!?+-]*[##]{0,1}[A-Za-z0-9._ *%$@!?+-]*$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>
        
        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>
    
    <cffunction name="VldEmailAddress" access="public" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>

        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>
	
	<cffunction name="VldEmailAddressList" access="public" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="Array" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$">
       
        <cfset var i	= '' />
       
		<cfloop from="1" to="#ArrayLen(Input)#" index="i">
			<cfif len(Input[i]) eq 0>
				<cfreturn false>
			</cfif>

			<cfif REFind(REStr,Input[i]) neq 1>
				<cfreturn false>
			</cfif>
		</cfloop>
		<cfreturn true>        
	</cffunction>
	    
    <cffunction name="VldPhone10str" access="public" returntype="boolean" hint="Validates a 10 character string phone number against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[1-9][0-9]{2}[1-9][0-9]{6}$">
        
        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>
        
        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>

	<cffunction name="VldCompanyName" access="public" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
	<cfargument name="Input" TYPE="string" required="yes">
       <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$@!?+-]*[##]{0,1}[A-Za-z0-9._ *%$@!?+-]*$">
       
       <cfif len(Input) eq 0>
       	<cfreturn true>
       </cfif>
       
       <cfif REFind(REStr,Input) eq 1>
       	<cfreturn true>
       <cfelse>
       	<cfreturn false>
       </cfif>
       
	</cffunction>

</cfcomponent>