<cfcomponent extends="taffy.core.resource" taffy_uri="/getHistoryFromDateTime" output="false">
    <cfinclude template="../../paths.cfm" >
    <!---
    <cfinclude template="../../../../session/sire/models/cfc/subscribers.cfc">
    --->
                
    <!--- ************************************************************************************************************************* --->
    <!---GET HISTORY OF UUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="POST" access="public" output="false" hint="Get history log for a contactstring">
        <cfabort>
        <cfargument name="inpFromDateTime" required="yes" default="" hint="The start datetime for get data">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - device token">
        <cfargument name="DebugAPI" required="no" default="0">
      
        <cfset var DBSourceEBM = '' />
        <cfset var ESIID = '' />
        <cfset var InsertToAPILog = '' />
        <!---<cfargument name="inpOverRideInterval" required="no" default="0" hint="Force override of the next interval">
        <cfargument name="inpTimeOutNextQID" required="no" default="" hint="Advance - specify next QID to go to on forced interval">--->                       
                             
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        <cfset var DebugStr = ''/>
        <cfset var dataout = StructNew()/>
        

        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.RESPONSETIME = ""/>
        <cfset APIResponse.RESPONSE = ""/>
        <cfset APIResponse.TOTALRECORDS = 0/>
                
        <cfif DebugAPI GT 0 >
            <cfset DBSourceEBM = "BishopDev" /> 
            <cfset Session.DBSourceEBM = "Bishop" />  
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
        </cfif>            
                                
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
        <cfoutput>
        
            <!--- Start timing test 
            <cfset tickEnd = GetTickCount() />
            --->
            <cftry>
              
            <!--- Start timing test --->
            <!---
            <cfset tickBegin = GetTickCount()>             
            --->
            
            <cfset var historyCount = "">
            <cfset var cppDisplay = 0>
            <cfset var historyList = "">

            <cfset var dataList = []>
            <cfquery name="historyCount" datasource="#Session.DBSourceEBM#">
                SELECT COUNT(IREResultsId_bi) AS TOTALCOUNT
                FROM simplexresults.ireresults
                WHERE ( 
                UserId_int > 0
                    OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
                )
                AND IREType_int IN (1,2) 
                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">
                AND Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFromDateTime#">
            </cfquery>
            
            <cfset dataout["iTotalRecords"] = (historyCount.RecordCount GT 1 ? historyCount.RecordCount : historyCount.TOTALCOUNT)>
            <!--- <cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]> --->
            <cfset var offset = GetTimeZoneInfo().utcHourOffset>
            <cfif dataout["iTotalRecords"] GT 0>
                <cfquery name="historyList" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        Created_dt, IREType_int, MasterRXCallDetailId_bi, Response_vch, IREResultsId_bi AS ID
                    FROM simplexresults.ireresults
                    WHERE 
                        (UserId_int > 0 OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop') )
                    AND IREType_int IN (1,2) 
                    AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">
                    AND Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFromDateTime#">
                    ORDER BY IREResultsId_bi DESC
                </cfquery>
               
                <cfloop query="historyList">
                    <cfset var dataItem =structNew()>
                    <cfset dataItem.CONTENT = Response_vch>
                    <cfset dataItem.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItem.TYPE =  IREType_int>
                    <cfset dataItem.RESULTID =  ID>

                    <cfset arrayappend(dataList,dataItem)>
                </cfloop>
            
            </cfif>

            <cfset APIResponse.RXRESULTCODE = 1/>
            <cfset APIResponse.RESPONSE = dataList/>
            <cfset APIResponse.TOTALRECORDS = dataout['iTotalRecords']/>
            <cfset APIResponse.RESPONSETIME = LSDateFormat(now(), 'yyyy-mm-dd')&' '&LSTimeFormat(now(),'HH:mm:ss')&' -0'&offset&'00'>            
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                <cfset APIResponse.RXRESULTCODE = -1/>                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
            
            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1116,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="API Result-pdc-getHistoryFromDateTime">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-getHistoryFromDateTime Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(APIResponse)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
        
        <cfcatch type="any">

        </cfcatch>
            
        </cftry>            

        <cfreturn representationOf(APIResponse).withStatus(200) />       
        
    </cffunction>
    
    
    <cffunction name="GET" access="public" output="false" hint="Get history log for a contactstring">           
        <cfabort>
        <cfargument name="inpFromDateTime" required="yes" default="" hint="The start datetime for get data">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - device token">
        <cfargument name="DebugAPI" required="no" default="0">
      
        <cfset var DBSourceEBM = '' />
        <cfset var ESIID = '' />
        <cfset var InsertToAPILog = '' />
        <!---<cfargument name="inpOverRideInterval" required="no" default="0" hint="Force override of the next interval">
        <cfargument name="inpTimeOutNextQID" required="no" default="" hint="Advance - specify next QID to go to on forced interval">--->                       
                             
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        <cfset var DebugStr = ''/>
        <cfset var dataout = StructNew()/>
        

        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.RESPONSETIME = ""/>
        <cfset APIResponse.RESPONSE = ""/>
        <cfset APIResponse.TOTALRECORDS = 0/>
                
        <cfif DebugAPI GT 0 >
            <cfset DBSourceEBM = "BishopDev" /> 
            <cfset Session.DBSourceEBM = "Bishop" />  
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
        </cfif>            
                                
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
        <cfoutput>
        
            <!--- Start timing test 
            <cfset tickEnd = GetTickCount() />
            --->
            <cftry>
              
            <!--- Start timing test --->
            <!---
            <cfset tickBegin = GetTickCount()>             
            --->
            
            <cfset var historyCount = "">
            <cfset var cppDisplay = 0>
            <cfset var historyList = "">

            <cfset var dataList = []>
            <cfquery name="historyCount" datasource="#Session.DBSourceEBM#">
                SELECT COUNT(IREResultsId_bi) AS TOTALCOUNT
                FROM simplexresults.ireresults
                WHERE ( 
                UserId_int > 0
                    OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop')
                )
                AND IREType_int IN (1,2) 
                AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">
                AND Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFromDateTime#">
            </cfquery>
            
            <cfset dataout["iTotalRecords"] = (historyCount.RecordCount GT 1 ? historyCount.RecordCount : historyCount.TOTALCOUNT)>
            <!--- <cfset dataout["iTotalDisplayRecords"] = dataout["iTotalRecords"]> --->
            <cfset var offset = GetTimeZoneInfo().utcHourOffset>
            
            <cfif dataout["iTotalRecords"] GT 0>
                <cfquery name="historyList" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        Created_dt, IREType_int, MasterRXCallDetailId_bi, Response_vch, IREResultsId_bi AS ID
                    FROM simplexresults.ireresults
                    WHERE 
                        (UserId_int > 0 OR (UserId_int IS NULL AND LOWER(Response_vch) = 'stop') )
                    AND IREType_int IN (1,2) 
                    AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpContactString#">
                    AND Created_dt >= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpFromDateTime#">
                    ORDER BY IREResultsId_bi DESC
                </cfquery>
               
                <cfloop query="historyList">
                    <cfset var dataItem =structNew()>
                    <cfset dataItem.CONTENT = Response_vch>
                    <cfset dataItem.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItem.TYPE =  IREType_int>
                    <cfset dataItem.RESULTID =  ID>

                    <cfset arrayappend(dataList,dataItem)>
                </cfloop>
            
            </cfif>

            <cfset APIResponse.RXRESULTCODE = 1/>
            <cfset APIResponse.RESPONSE = dataList/>
            <cfset APIResponse.TOTALRECORDS = dataout['iTotalRecords']/>
            <cfset APIResponse.RESPONSETIME = LSDateFormat(now(), 'yyyy-mm-dd')&' '&LSTimeFormat(now(),'HH:mm:ss')&' -0'&offset&'00'>           
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                <cfset APIResponse.RXRESULTCODE = -1/>                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
            
            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1116,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="API Result-pdc-getHistoryFromDateTime">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-getHistoryFromDateTime Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(APIResponse)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
        
        <cfcatch type="any">

        </cfcatch>
            
        </cftry>            

        <cfreturn representationOf(APIResponse).withStatus(200) />      
        
    </cffunction>
      
</cfcomponent>