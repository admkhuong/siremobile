<cfcomponent extends="taffy.core.resource" taffy_uri="/sendQuestion" output="false">
	<cfinclude template="../../paths.cfm" >
	
	<!--- Overrides remote component request so method can be called locally - helps to eliminates cross session varibale scope issues --->
	<cfset DeliveryServiceComponentPath = "">
    <cfset DeliveryServiceMCIDDMsComponentPath="">
    
	<cfinclude template="../../../../session/cfc/csc/constants.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
   	
	<!--- Common distirbution methods for web services --->    
    <cfinclude template="../../../../session/cfc/includes/inc_billing.cfm">
    <cfinclude template="../../../../session/cfc/includes/inc_emailsettings.cfm">
    
    <!--- Real time methods--->
    <cfinclude template="../../../../session/cfc/includes/inc_realtime.cfm">
    <!---
    <cfinclude template="../../../../public/sire/configs/paths.cfm">
    --->

    <cfset _APNS_SANDBOX = 0/>

    <cfif _APNS_SANDBOX EQ 1>
        <cfset _CertPath = ExpandPath('/') & '/apns/' & 'SIRE_Development_Push_Certificates.p12'>
        <cfset _CertPass = 'sire@2016'>
            <cfelse>
        <cfset _CertPath = ExpandPath('/') & '/apns/' & 'SIRE_Production_Push_Certificate.p12'>
        <cfset _CertPass = 'Sire@2016'>
    </cfif>

    
    	        
    <!--- ************************************************************************************************************************* --->
    <!--- Add a single record to the real time system --->
    <!--- ************************************************************************************************************************* --->       
    <cffunction name="POST" access="public" output="false" hint="Send a message to system">
        <cfabort>
        <cfargument name="inpKeyword" required="yes" default="0" hint="Campaign keyword">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - deviceToken">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="0">
        <cfargument name="inpShortCode" required="no" default="QA2Nowhere" hint="The Shorcode or Application code to run this keyword against.">
        <cfargument name="inpContactTypeId" required="no" default="4" hint="1=Phone number, 2=eMail, 3=SMS, 4=deviceToken">
        <cfargument name="inpNewLine" required="no" default="<BR>" hint="Depending on target environment - specify newline charcter(s) - default is <BR>">
        <cfargument name="inpFormDataJSON" required="no" default="">
        <cfargument name="inpDeviceUUID" required="no" default="">
        <cfargument name="DebugAPI" required="no" default="0">
      
        <cfset var DBSourceEBM = '' />
        <cfset var ESIID = '' />
        <cfset var InsertToAPILog = '' />
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        <cfset var dataItemAPNS = structNew()>
        <cfset var dataItemResponse = structNew()>
        <cfset var DebugStr = ''/>
        <cfset var payload =''/>
        <cfset var pushResult =''/>

        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.RESPONSETIME = ""/>
        <cfset APIResponse.RESPONSE = []/>
        <cfset APIResponse.TOTALRECORDS = 0/>
                
        
        <!--- Then include main processing --->                       
        <!---<cfinclude template="inc_addtoproc.cfm">  --->    
        
        <cfset DBSourceEBM = "Bishop"/> 
        <cfset Session.DBSourceEBM = "Bishop"/> 
                                
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                                     
        <cfoutput>
        
            <!--- Start timing test --->
            <cfset tickEnd = GetTickCount() />

            <cftry>


                <cfinvoke method="GetSMSResponse" returnvariable="RetValGetResponse">                         
                    <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                    <cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
                    <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                    <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                    <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>    
                    <cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
                    <cfinvokeargument name="inpFormDataJSON" value="#inpFormDataJSON#"/>
                    <cfinvokeargument name="inpQATool" value="1"/>
                </cfinvoke>      

                <cfset var Created_dt = now()>
                
                <!--- CREATE APNS SERVICE --->
                <cfset var fileCert = _CertPath>
                <cfset var fileCertPass = _CertPass>

                <cfif _APNS_SANDBOX EQ 1>
                    <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withSandboxDestination().build()>    
                <cfelse>
                    <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withProductionDestination().build()>
                </cfif>

                <!--- <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().badge(threadUnreadCount[1].UNREAD_COUNT).alertBody(message).build()> --->
                <!--- CHECK IF MESSAGE > 160 THEN CUT STRING --->
                
                <cfset var responseTime = now()>
                <cfset var offset = GetTimeZoneInfo().utcHourOffset>

                <cfset APIResponse.RESPONSETIME = DateFormat(responseTime, 'yyyy-mm-dd')&' '&LSTimeFormat(responseTime, 'HH:mm:ss')&' -0'&offset&'00'>

                <cfif RetValGetResponse.RXRESULTCODE GT 0>
                    <cfif RetValGetResponse.RESPONSELENGTH GT 160>
                        <cfset var sendMessage = LEFT(RetValGetResponse.RESPONSE,50)> 
                    <cfelse>       
                        <cfset var sendMessage = RetValGetResponse.RESPONSE>
                    </cfif>

                    <cfset dataItemResponse.CONTENT = inpKeyword>
                    <cfset dataItemResponse.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItemResponse.TYPE =  2>
                    <cfset dataItemResponse.RESULTID =  RetValGetResponse.LASTSURVEYRESULTID>
                    <cfset arrayappend(APIResponse.RESPONSE,dataItemResponse)>

                    <cfset APIResponse.ERRMESSAGE = RetValGetResponse.ERRMESSAGE/>
                    <cfset APIResponse.RXRESULTCODE = RetValGetResponse.RXRESULTCODE/>
                    <cfset APIResponse.MESSAGE = RetValGetResponse.MESSAGE/>

                    <!---
                    <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().alertBody(SerializeJSON(dataItemAPNS)).build()>
                    --->
                    <cfset dataItemAPNS.CONTENT = sendMessage>
                    <cfset dataItemAPNS.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItemAPNS.TYPE =  1>
                    <cfset dataItemAPNS.RESULTID =  RetValGetResponse.LASTSURVEYRESULTID>

                   

                    <cfif RetValGetResponse.RESPONSELENGTH GT 6>
                        <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().customField("Info", SerializeJSON(dataItemAPNS)).badge(1).alertBody(sendMessage).build()>
                        <cfset pushResult = APNSService.push(inpContactString, payload)>
                    </cfif>
                 <cfelse>
                    <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                </cfif>    
              
            <!--- Start timing test --->
            <!---
            <cfset tickBegin = GetTickCount()>             
            --->
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
                                                  
            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1115,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="API Result-pdc-SendQuestion">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-SendQuestion Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(APIResponse)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
        
        <cfcatch type="any">
        
        </cfcatch>
            
        </cftry>            

        <cfreturn representationOf(APIResponse).withStatus(200) />    
      
    </cffunction>
    
    
    <cffunction name="GET" access="public" output="false" hint="Returns REST Verb Warning">         
        <cfabort>
        <cfargument name="inpKeyword" required="yes" default="0" hint="Campaign keyword">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - deviceToken">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="0">
        <cfargument name="inpShortCode" required="no" default="QA2Nowhere" hint="The Shorcode or Application code to run this keyword against.">
        <cfargument name="inpContactTypeId" required="no" default="4" hint="1=Phone number, 2=eMail, 3=SMS, 4=deviceToken">
        <cfargument name="inpNewLine" required="no" default="<BR>" hint="Depending on target environment - specify newline charcter(s) - default is <BR>">
        <cfargument name="inpFormDataJSON" required="no" default="">
        <cfargument name="inpDeviceUUID" required="no" default="">
        <cfargument name="DebugAPI" required="no" default="0">
      
        <cfset var DBSourceEBM = '' />
        <cfset var ESIID = '' />
        <cfset var InsertToAPILog = '' />
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        <cfset var dataItemAPNS = structNew()>
        <cfset var dataItemResponse = structNew()>
        <cfset var DebugStr = ''/>
        <cfset var payload =''/>
        <cfset var pushResult =''/>

        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.RESPONSETIME = ""/>
        <cfset APIResponse.RESPONSE = []/>
        <cfset APIResponse.TOTALRECORDS = 0/>
                
        
        <!--- Then include main processing --->                       
        <!---<cfinclude template="inc_addtoproc.cfm">  --->    
        
        <cfset DBSourceEBM = "Bishop"/> 
        <cfset Session.DBSourceEBM = "Bishop"/> 
                                
         <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
    
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
         --->
                                     
        <cfoutput>
        
            <!--- Start timing test --->
            <cfset tickEnd = GetTickCount() />

            <cftry>

                <cfinvoke method="GetSMSResponse" returnvariable="RetValGetResponse">                         
                    <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                    <cfinvokeargument name="inpShortCode" value="#arguments.inpShortCode#"/>
                    <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                    <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                    <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>    
                    <cfinvokeargument name="inpContactTypeId" value="#inpContactTypeId#"/>
                    <cfinvokeargument name="inpFormDataJSON" value="#inpFormDataJSON#"/>
                    <cfinvokeargument name="inpQATool" value="1"/>
                </cfinvoke>      

                <cfset var Created_dt = now()>
                
                <!--- CREATE APNS SERVICE --->
                <cfset var fileCert = _CertPath>
                <cfset var fileCertPass = _CertPass>

               <cfif _APNS_SANDBOX EQ 1>
                    <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withSandboxDestination().build()>    
                <cfelse>
                    <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withProductionDestination().build()>
                </cfif>

                <!--- <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().badge(threadUnreadCount[1].UNREAD_COUNT).alertBody(message).build()> --->
                <!--- CHECK IF MESSAGE > 160 THEN CUT STRING --->
                
                <cfset var responseTime = now()>
                <cfset var offset = GetTimeZoneInfo().utcHourOffset>

                <cfset APIResponse.RESPONSETIME = DateFormat(responseTime, 'yyyy-mm-dd')&' '&LSTimeFormat(responseTime, 'HH:mm:ss')&' -0'&offset&'00'>

                <cfif RetValGetResponse.RXRESULTCODE GT 0>
                    <cfif RetValGetResponse.RESPONSELENGTH GT 160>
                        <cfset var sendMessage = LEFT(RetValGetResponse.RESPONSE,50)> 
                    <cfelse>       
                        <cfset var sendMessage = RetValGetResponse.RESPONSE>
                    </cfif>

                    <cfset dataItemResponse.CONTENT = inpKeyword>
                    <cfset dataItemResponse.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItemResponse.TYPE =  2>
                    <cfset dataItemResponse.RESULTID =  RetValGetResponse.LASTSURVEYRESULTID>
                    <cfset arrayappend(APIResponse.RESPONSE,dataItemResponse)>

                    <cfset APIResponse.ERRMESSAGE = RetValGetResponse.ERRMESSAGE/>
                    <cfset APIResponse.RXRESULTCODE = RetValGetResponse.RXRESULTCODE/>
                    <cfset APIResponse.MESSAGE = RetValGetResponse.MESSAGE/>

                    <!---
                    <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().alertBody(SerializeJSON(dataItemAPNS)).build()>
                    --->
                    <cfset dataItemAPNS.CONTENT = sendMessage>
                    <cfset dataItemAPNS.CREATED =  DateFormat(Created_dt, 'yyyy-mm-dd')&' '&LSTimeFormat(Created_dt, 'HH:mm:ss')&' -0'&offset&'00'>
                    <cfset dataItemAPNS.TYPE =  1>
                    <cfset dataItemAPNS.RESULTID =  RetValGetResponse.LASTSURVEYRESULTID>

                   

                    <cfif RetValGetResponse.RESPONSELENGTH GT 6>
                        <cfset payload = createObject("java", "com.notnoop.apns.APNS").newPayload().customField("Info", SerializeJSON(dataItemAPNS)).badge(1).alertBody(sendMessage).build()>
                        <cfset pushResult = APNSService.push(inpContactString, payload)>
                    </cfif>
                 <cfelse>
                    <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                </cfif>    
              
            <!--- Start timing test --->
            <!---
            <cfset tickBegin = GetTickCount()>             
            --->
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

        </cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
                                                  
            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1115,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="API Result-pdc-SendQuestion">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-SendQuestion Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(APIResponse)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
        
        <cfcatch type="any">
        
        </cfcatch>
            
        </cftry>            

        <cfreturn representationOf(APIResponse).withStatus(200) />          
    </cffunction>
      
</cfcomponent>