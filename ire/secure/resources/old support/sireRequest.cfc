<cfcomponent extends="taffy.core.resource" taffy:uri="/support/sireRequest" hint="Capture user input, send email to team, give user a trouble ticket id.">
	<cfinclude template="../../paths.cfm" >
    	             
	<cffunction name="POST" access="public" output="false" hint="Add a Status Record for this Event">
		<cfargument name="inpBatchId" TYPE="string" required="yes" hint="Required Batch Id to uniquly idenntify each Support Program-Keyword"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpListOfEmails" required="yes" hint="someone@somewhere.com">
        <cfargument name="inpMessageText" required="yes" hint="Customer Question or Issue">
        <cfargument name="inpUserId" required="yes" hint="User ID">
        		
		<cfset var dataOut = ''>
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
        <cfset var InsertToEventStatus = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var insertResult = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
		
		<cfset var SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
	    <cfset var SupportEMailFrom = "support@siremobile.com" />
	    <cfset var SupportEMailServer = "smtp.gmail.com" />
	    <cfset var SupportEMailUserName = "support@siremobile.com" />
	    <cfset var SupportEMailPassword = "SF927$tyir3" />
	    <cfset var SupportEMailPort = "465" />
	    <cfset var SupportEMailUseSSL = "true" />
                    
        <!--- Formatted Date Time --->
        <cfset dataOut = "" />                      
      
		<cftry>
           
        
        	<!--- Replace any existing status for today with current status (use update with insert? ) --->	
        
        	<!---
	        	
	        	
	        	{inpBatchId:"{%SRBATCHID%}",inpContactString:"{%SRCONTACTSTRING%}",inpListOfEmails:"lee@siremobile.com",inpMessageText:"{%INPPA=2%}"}

{
	"inpBatchId": "{%SRBATCHID%}",
	"inpContactString": "{%SRCONTACTSTRING%}",
	"inpListOfEmails": "lee@siremobile.com",
	"inpMessageText": "{%INPPA=2%}"
}

				
				simplequeue.`supportapp` (
  `PKId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `BatchId_bi` bigint(20) NOT NULL,
  `Status_int` int(11) NOT NULL,
  `Event_date` date NOT NULL,
  `Created_dt` datetime NOT NULL,
  `ContactString_vch` varchar(255) DEFAULT NULL,
  `CustomerMessage_vch` text,
  
			--->
           
            <!--- Add a status update for today --->                          
            <cfquery name="InsertToEventStatus" datasource="#DBSourceEBM#" result="insertResult">              
              
              INSERT INTO 
              	simplequeue.supportapp 
                (	                	 
                    BatchId_bi, 
                    Status_int, 
                    Event_date, 
                    Created_dt,
                    LastUpdated_dt,
                    ContactString_vch,
                    CustomerMessage_vch,
                    UserId_int
                )
                VALUES
                (                	
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#INPBATCHID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                    NOW(),
                 	NOW(),
                 	NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_CLOB" VALUE="#inpMessageText#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                     
                 )             
            </cfquery>
            
            <!--- Send back new trouble ticket number - if this number is 0 then branch to no number available logic --->
            <cfif insertResult.GENERATED_KEY GT 0>
	        	<cfset dataout = insertResult.GENERATED_KEY />
	        <cfelse>
	        	<cfset dataout = 0>	
	        </cfif>
	        
	        
	        <!--- Send email to support list of emails --->
<!---
	        
	        <cfmail to="#inpListOfEmails#" subject="SIRE Support Request" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		        <cfoutput>		          
		            <!--- Inline styles only! - Don't use style tags - gmail wont honor them --->                       
		            <body style='background-image: linear-gradient(to bottom, ##FFAE00, ##0085c8);	background-repeat:no-repeat; font-family: "Helvetica";'>
		        
		                <div id="divConfirm" align="left" style="padding:25px; width:600px;">
		                    <div ><h4>SIRE Information Request</h4></div>
		                   
		                    <div class="message-block  m_top_10" style='font-family: "helvetica";	font-size: 12px;margin-left: 21px; margin-top: 10px;'>
		                        <div class="left-input" style="border-radius: 4px 0 0 4px;	border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666;	float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px;	background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
		                            <span class="em-lbl" style="width: auto;">Batch Id</span>
		                        </div>
		                        <div class="right-input" style="width: 226px; display:inline; height: 28px;">		        		
		                            <input type="text" readonly value="#inpBatchId#" style="width: 210px; background-color: ##FBFBFB; color:##000000;  font-size: 12px;  height: 28px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">
		                        </div>				
		                    </div>
		                    <div class="clear"></div>
		                    <div class="message-block  m_top_10" style='font-family: "helvetica";	font-size: 12px;margin-left: 21px; margin-top: 10px;'>
		                        <div class="left-input" style="border-radius: 4px 0 0 4px;	border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666;	float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px;	background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
		                            <span class="em-lbl" style="width: auto;">Contact Information</span>
		                        </div>
		                        <div class="right-input" style="width: 226px; display:inline; height: 28px;">	        		
		                            <input type="text" readonly value="#inpContactString#" style="width: 210px; background-color: ##FBFBFB;color: ##000000;  font-size: 12px;  height: 28px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">
		                        </div>				
		                    </div>
		                    <div class="clear"></div>
		                   		                    
		                    <div class="message-block  m_top_10" style='font-family: "helvetica";	font-size: 12px;margin-left: 21px; margin-top: 10px;'>
		                        <div class="left-input" style="border-radius: 4px 0 0 4px;	border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666;	float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px;	background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
		                            <span class="em-lbl" style="width: auto;">Message From Device</span>
		                        </div>
		                        <div class="right-input" style=" width: 226px; display:inline; height: 28px;">	        		
		                            <textarea style="width: 210px; background-color: ##FBFBFB;color: ##000000;  font-size: 12px;  height: 90px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">#inpMessageText#</textarea>                     </div>				
		                    </div>
		                    
		                    
		                    <div class="clear"></div>       
		
		                    <div style="height:200px;"></div>
		    
		                    <div class="clear"></div> 
		                        
		            </body>
		        
		        </cfoutput>
		    </cfmail>
--->
    
       <cfmail to="#inputListOfMails#" subject="SIRE Support Request" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		           <html>
						<head>
							<meta charset="utf-8">
							<title>Support Request</title>
						</head>
					
						<body style="font-family: Arial; font-size: 12px;">

							<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
								<thead>
											    <tr>
											        <td width="50%">
														<a href="http://siremobile.com"><img src="http://siremobile.com/public/sire/images/emailtemplate/logo.jpg" alt=""></a>
													</td>
											     	<td style="text-align: right; color: ##5c5c5c; font-size: 11px;">
														<!--- <p>Trouble seeing this email? <a href="<cfoutput></cfoutput>/public/sire/pages/contact-us">Click here</a></p>--->
														<p>Add <a href="mailto:support@siremobile.com">support@siremobile.com</a> to your contacts</p>
													</td>
											    </tr>
								</thead>
							
							  	<tbody style="margin: 0; padding: 0;">
							  		<tr>
							  			<td colspan="2" style="margin: 0; padding: 0;">
							  				
							  				<div style="text-align: center;">
							  					<img src="http://siremobile.com/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
							  				</div>
							  			</td>
							  		</tr>
							  		<tr>
							  			<td colspan="2" style="padding: 0 30px;">
					
											<p style="font-size: 16px;color: ##5c5c5c;">Support,</p>
											
											<br>
					
											<p style="font-size: 18px;color: ##74c37f; text-align:center; font-weight:bold">Support Information</p>
											<hr style="border: 0;border-bottom: 1px solid ##74c37f;">
					
											<table style="width:100%">
												<tr>
													<td style="width:10%"></td>
													<td style="width:30%">
														<p style="font-size: 16px;color: ##5c5c5c;">Batch Id:</p> 
														<p style="font-size: 16px;color: ##5c5c5c;">Contact From: </p>
														<p style="font-size: 16px;color: ##5c5c5c;">Message From Customer:</p>
														<p style="font-size: 16px;color: ##5c5c5c;">Trouble Ticket Number:</p>
													</td>
													<td style="width:50%">
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpBatchId#</cfoutput></p>
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpContactString#</cfoutput></p>
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#inpMessageText#</cfoutput></p>
														<p style="font-size: 16px;color: ##5c5c5c;"><cfoutput>#dataout#</cfoutput></p>					
													</td>
													<td style="width:10%"></td>
												</tr>
												
											</table>
																						
											<p style="font-size: 16px;color: ##5c5c5c;">Thanks,<br>
											SIRE Assistance Team
											</p>
							  			</td>
							  		</tr>	
							  	</tbody>
															
								<tfoot>
									    <tr style="background-color:##f2f2f2;">
									    	<td width="60%" style="color: ##5c5c5c; font-size: 11px;">
									    		<p>For support requests, please call or email us at: </p>
												<p>https://siremobile.com &nbsp|&nbsp888.747.4411&nbsp|&nbspsupport@siremobile.com</p>
									    	</td>
									    	
									    	<td style="text-align: right;  margin: 0; padding: 0;">
									    	
									    	</td>
									    </tr>
								</tfoot>

							</table>

						</body>
					
					</html>		
		    </cfmail>

    
						    		
            		   	                                   
        <cfcatch>        	
                            
                        <!--- For debbugging only! --->    
						<!---  <cfset dataout = SerializeJSON(cfcatch) />	 --->             
                <cftry>        
        
					<cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX Support/Sire API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
   
</cfcomponent>