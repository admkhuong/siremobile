<cfcomponent extends="taffy.core.resource" taffy_uri="/pdc/GetNextResponse" output="false">
	<cfinclude template="../../paths.cfm" >
	
	<!--- Overrides remote component request so method can be called locally - helps to eliminates cross session varibale scope issues --->
	<cfset DeliveryServiceComponentPath = "">
    <cfset DeliveryServiceMCIDDMsComponentPath="">
    
	<cfinclude template="../../../../session/cfc/csc/constants.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
   	
	<!--- Common distirbution methods for web services --->    
    <cfinclude template="../../../../session/cfc/includes/inc_billing.cfm">
    <cfinclude template="../../../../session/cfc/includes/inc_emailsettings.cfm">
    
    <!--- Real time methods--->
    <cfinclude template="../../../../session/cfc/includes/inc_realtime.cfm">
    
    	        
   	<!--- ************************************************************************************************************************* --->
    <!--- Add a single record to the real time system --->
    <!--- ************************************************************************************************************************* --->       
 
 	<cffunction name="POST" access="public" output="false" hint="Add a single request to the real time system">
 		<cfargument name="inpKeyword" required="yes" default="0" hint="The pre-defined campaign keyword you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpShortCode" required="yes" default="" hint="The Shorcode or Application code to run this keyword against.">
        <cfargument name="inpContactTypeId" required="no" default="2" hint="1=Phone number, 2=SMS, 3=eMail">
        <cfargument name="inpNewLine" required="no" default="<BR>" hint="Depending on target environment - specify newline charcter(s) - default is <BR>">
        <cfargument name="inpFormDataJSON" required="no" default="">
        <cfargument name="DebugAPI" required="no" default="0">
      
      	<cfset var DBSourceEBM = '' />
		<cfset var ESIID = '' />
		<cfset var InsertToAPILog = '' />

        <!---<cfargument name="inpOverRideInterval" required="no" default="0" hint="Force override of the next interval">
        <cfargument name="inpTimeOutNextQID" required="no" default="" hint="Advance - specify next QID to go to on forced interval">--->                       
        
        <!---
		
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpFormDataJSON : $('#inpJSON').val(),
					inpQATool : 1					
				},	
		
		
		--->                     
                             
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        
        
        <cfset APIResponse.INTERVALSCHEDULED = "" />
		<cfset APIResponse.LASTCP = 0/>
        <cfset APIResponse.BATCHID = 0/>
        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RESPONSELENGTH = 0/>
        <cfset APIResponse.RESPONSETYPE = "NA"/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.LASTSURVEYRESULTID = ""/>
        <cfset APIResponse.INTERVALEXPIREDNEXTQID = 0/>
        <cfset APIResponse.RESPONSE = ""/>
        <cfset APIResponse.LASTRQ = 0/>
                
        
        <!--- Then include main processing --->                       
        <!---<cfinclude template="inc_addtoproc.cfm">  --->    
        
                          	               	
        <cfif DebugAPI GT 0 >
               
            <cfset DBSourceEBM = "BishopDev" /> 
            <cfset Session.DBSourceEBM = "BishopDev" />  
            <cfset ESIID = "-15" />   
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
            <cfset ESIID = "-14"/>
           
        </cfif>            
                                
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                     
       	<cfoutput>
        
	        <!--- Start timing test --->
            <cfset tickEnd = GetTickCount() />
                	            
       
            <cftry>
            
            
           			<cfinvoke    
						 method="GetSMSResponse"
                         returnvariable="RetValGetResponse">                         
                            <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>    
                            <!---<cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>  ---> 
                            <!---<cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>--->
                            <!---<cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>--->
                            <!---<cfinvokeargument name="inpFormData" value="#inpFormDataJSON#"/>--->
                            <cfinvokeargument name="inpFormDataJSON" value="#inpFormDataJSON#"/>
                            <cfinvokeargument name="inpQATool" value="1"/>
                    </cfinvoke>      
                    
                    
                <cfif IsDate(RetValGetResponse.IntervalScheduled)>
					<cfset RetValGetResponse.IntervalScheduled = "#LSDateFormat(RetValGetResponse.IntervalScheduled, 'yyyy-mm-dd')# #LSTimeFormat(RetValGetResponse.IntervalScheduled, 'HH:mm:ss')#" />
                <cfelse>
                    <cfset RetValGetResponse.IntervalScheduled = "#RetValGetResponse.IntervalScheduled#"/>
                </cfif>
                
                
                <cfset APIResponse.INTERVALSCHEDULED = RetValGetResponse.IntervalScheduled />
                <cfset APIResponse.LASTCP = RetValGetResponse.LASTCP/>
                <cfset APIResponse.BATCHID = RetValGetResponse.BATCHID/>
                <cfset APIResponse.ERRMESSAGE = RetValGetResponse.ERRMESSAGE/>
                <cfset APIResponse.RESPONSELENGTH = RetValGetResponse.RESPONSELENGTH/>
                <cfset APIResponse.RESPONSETYPE = RetValGetResponse.RESPONSETYPE/>
                <cfset APIResponse.RXRESULTCODE = RetValGetResponse.RXRESULTCODE/>
                <cfset APIResponse.MESSAGE = RetValGetResponse.MESSAGE/>
                <cfset APIResponse.LASTSURVEYRESULTID = RetValGetResponse.LASTSURVEYRESULTID/>
                <cfset APIResponse.INTERVALEXPIREDNEXTQID = RetValGetResponse.INTERVALEXPIREDNEXTQID/>
                <cfset APIResponse.RESPONSE = RetValGetResponse.RESPONSE/>
                <cfset APIResponse.LASTRQ = RetValGetResponse.LASTRQ/>
                                    
              
			<!--- Start timing test --->
            <cfset tickBegin = GetTickCount()>		       
               
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                				
                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
                                   
                                                  
           <!--- <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,  
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SimpleX API Result - pdc-AddToRealTime">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-AddToRealTime Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>--->
        
        <cfcatch type="any">
        
        
        </cfcatch>
            
        </cftry>            

		<cfreturn representationOf(APIResponse).withStatus(200) />
                    
      
    </cffunction>
    
    
    <cffunction name="GET" access="public" output="false" hint="Returns REST Verb Warning">			
		<cfargument name="inpKeyword" required="yes" default="0" hint="The pre-defined campaign keyword you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpScrubContactString" required="no" hint="Scrub Contact string for numberic only" default="1">
        <cfargument name="inpShortCode" required="yes" default="" hint="The Shorcode or Application code to run this keyword against.">
        <cfargument name="inpContactTypeId" required="no" default="2" hint="1=Phone number, 2=SMS, 3=eMail">
        <cfargument name="inpNewLine" required="no" default="<BR>" hint="Depending on target environment - specify newline charcter(s) - default is <BR>">
        <cfargument name="inpFormDataJSON" required="no" default="">
        <cfargument name="DebugAPI" required="no" default="0">
      
      
      	<cfset var DBSourceEBM = '' />
		<cfset var ESIID = '' />
        <cfset var InsertToAPILog = '' />
        <!---<cfargument name="inpOverRideInterval" required="no" default="0" hint="Force override of the next interval">
        <cfargument name="inpTimeOutNextQID" required="no" default="" hint="Advance - specify next QID to go to on forced interval">--->                       
        
        <!---
		
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpFormDataJSON : $('#inpJSON').val(),
					inpQATool : 1					
				},	
		
		
		--->                     
                             
                                
        <cfset var RetValGetResponse = StructNew() />
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var APIResponse = StructNew() />
        
        
        <cfset APIResponse.INTERVALSCHEDULED = "" />
		<cfset APIResponse.LASTCP = 0/>
        <cfset APIResponse.BATCHID = 0/>
        <cfset APIResponse.ERRMESSAGE = ""/>
        <cfset APIResponse.RESPONSELENGTH = 0/>
        <cfset APIResponse.RESPONSETYPE = "NA"/>
        <cfset APIResponse.RXRESULTCODE = -1/>
        <cfset APIResponse.MESSAGE = ""/>
        <cfset APIResponse.LASTSURVEYRESULTID = ""/>
        <cfset APIResponse.INTERVALEXPIREDNEXTQID = 0/>
        <cfset APIResponse.RESPONSE = ""/>
        <cfset APIResponse.LASTRQ = 0/>
                
        
        <!--- Then include main processing --->                       
        <!---<cfinclude template="inc_addtoproc.cfm">  --->    
        
                          	               	
        <cfif DebugAPI GT 0 >
               
            <cfset DBSourceEBM = "BishopDev" /> 
            <cfset Session.DBSourceEBM = "BishopDev" />  
            <cfset ESIID = "-15" />   
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
            <cfset ESIID = "-14"/>
           
        </cfif>            
                                
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                     
       	<cfoutput>
        
	        <!--- Start timing test --->
            <cfset tickEnd = GetTickCount() />
                	            
       
            <cftry>
            
            
           			<cfinvoke    
						 method="GetSMSResponse"
                         returnvariable="RetValGetResponse">                         
                            <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                            <cfinvokeargument name="inpScrubContactString" value="#inpScrubContactString#"/>
                            <cfinvokeargument name="inpNewLine" value="#inpNewLine#"/>    
                            <!---<cfinvokeargument name="inpOverRideInterval" value="#inpOverRideInterval#"/>  ---> 
                            <!---<cfinvokeargument name="inpTimeOutNextQID" value="#inpTimeOutNextQID#"/>--->
                            <!---<cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>--->
                            <!---<cfinvokeargument name="inpFormData" value="#inpFormDataJSON#"/>--->
                            <cfinvokeargument name="inpFormDataJSON" value="#inpFormDataJSON#"/>
                            <cfinvokeargument name="inpQATool" value="1"/>
                    </cfinvoke>      
                    
                    
                <cfif IsDate(RetValGetResponse.IntervalScheduled)>
					<cfset RetValGetResponse.IntervalScheduled = "#LSDateFormat(RetValGetResponse.IntervalScheduled, 'yyyy-mm-dd')# #LSTimeFormat(RetValGetResponse.IntervalScheduled, 'HH:mm:ss')#" />
                <cfelse>
                    <cfset RetValGetResponse.IntervalScheduled = "#RetValGetResponse.IntervalScheduled#"/>
                </cfif>
                
                
                <cfset APIResponse.INTERVALSCHEDULED = RetValGetResponse.IntervalScheduled />
                <cfset APIResponse.LASTCP = RetValGetResponse.LASTCP/>
                <cfset APIResponse.BATCHID = RetValGetResponse.BATCHID/>
                <cfset APIResponse.ERRMESSAGE = RetValGetResponse.ERRMESSAGE/>
                <cfset APIResponse.RESPONSELENGTH = RetValGetResponse.RESPONSELENGTH/>
                <cfset APIResponse.RESPONSETYPE = RetValGetResponse.RESPONSETYPE/>
                <cfset APIResponse.RXRESULTCODE = RetValGetResponse.RXRESULTCODE/>
                <cfset APIResponse.MESSAGE = RetValGetResponse.MESSAGE/>
                <cfset APIResponse.LASTSURVEYRESULTID = RetValGetResponse.LASTSURVEYRESULTID/>
                <cfset APIResponse.INTERVALEXPIREDNEXTQID = RetValGetResponse.INTERVALEXPIREDNEXTQID/>
                <cfset APIResponse.RESPONSE = RetValGetResponse.RESPONSE/>
                <cfset APIResponse.LASTRQ = RetValGetResponse.LASTRQ/>
                                    
              
			<!--- Start timing test --->
            <cfset tickBegin = GetTickCount()>		       
               
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>
                				
                                
                <cfset APIResponse.ERRMESSAGE = SerializeJSON(cfcatch)/>
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>
                                   
        <!--- Log all API call outputs for debugging purposes --->
        <cftry>  
                                   
                                                  
           <!--- <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,                   
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,  
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SimpleX API Result - pdc-AddToRealTime">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-AddToRealTime Debugging Info">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,     
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">, 
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>--->
        
        <cfcatch type="any">
        
        
        </cfcatch>
            
        </cftry>            

		<cfreturn representationOf(APIResponse).withStatus(200) />		
		
		<!---<cfargument name="inpKeyword" required="no" default="0" hint="The pre-defined campaign keyword you created under your account.">
        <cfargument name="inpContactString" required="no" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpShortCode" required="no" default="" hint="The Shorcode or Application code to run this keyword against.">
        <cfargument name="inpContactTypeId" required="no" default="2" hint="1=Phone number, 2=SMS, 3=eMail">
        <cfargument name="inpNewLine" required="no" default="<BR>" hint="Depending on target environment - specify newline charcter(s) - default is <BR>">
        <cfargument name="inpFormDataJSON" required="no" default="">
        <cfargument name="DebugAPI" required="no" default="0">
        
		<cfset var retval = StructNew()>
		  
       	<cfoutput>
                       	       
            <cftry>
                           
				<cfset retval = StructNew()>
                <cfset retval.RXRESULTCODE = 1>
                <cfset retval.MESSAGE = "'Doh">
                <cfset retval.DESC = "You need to use the POST verb to access this functionality.">
                
                <cfreturn representationOf(retval).withStatus(200) />
            
            <cfcatch>
				<cfset retval = StructNew()>
				<cfset retval.RXRESULTCODE = -1>
                <cfset retval.MESSAGE = "'Doh">
                <cfset retval.DESC = "You need to use the POST verb to access this functionality.">
                <cfreturn representationOf(retval).withStatus(401, '#cfcatch.Message#') />
            </cfcatch>                
            </cftry>     

		</cfoutput>
        --->
    </cffunction>
      
</cfcomponent>