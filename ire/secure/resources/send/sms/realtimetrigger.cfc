<cfcomponent extends="taffy.core.resource" taffy_uri="/triggerSMS" output="false">

	<cfinclude template="../../../paths.cfm" >

	<!--- Overrides remote component request so method can be called locally - helps to eliminates cross session varibale scope issues --->
	<cfset DeliveryServiceComponentPath = "">
    <cfset DeliveryServiceMCIDDMsComponentPath="">

	<cfinclude template="../../../../../session/cfc/csc/constants.cfm">
	<cfinclude template="../../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../../session/cfc/csc/inc-billing-sire.cfm">
        

    <!--- Used in load queue includes --->
    <cfinclude template="../../../../../session/cfc/inc_MCIDDMs.cfm">

	<!--- Common distirbution methods for web services --->
    <cfinclude template="../../../../../session/cfc/includes/inc_emailsettings.cfm">

   	<!--- ************************************************************************************************************************* --->
    <!--- Add a single record to the real time system --->
    <!--- ************************************************************************************************************************* --->

 	<cffunction name="post" access="public" output="false" hint="Add a single request to the real time system">
 		<cfargument name="inpBatchId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpContactTypeId" required="no" default="2" hint="1=Phone number,  2=eMail, 3=SMS">
        <cfargument name="inpInternational" required="no" default="0" hint=" 1 will flag this number as international">
        <cfargument name="inpValidateScheduleActive" required="no" default="0" hint="1 will halt message if time is currently outside of the pre-defined campaigns's scheduled range">
        <cfargument name="inpTimeZone" required="no" default="">
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">
        <cfargument name="inpBlockDuplicates" required="no" default="0">
        <cfargument name="inpValidateCPPActive" required="no" default="" hint="If provided - will validate request is valid against the CPP Id">
        <cfargument name="inpSkipNumberValidations" required="no" default="0">
        <cfargument name="inpSkipLocalUserDNCCheck" required="no" default="0">
        <cfargument name="inpDistributionProcessId" required="no" default="0">
        <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts. 0 – no delivery receipt requested, 1 - return delivery receipt on final state (i.e. delivered, expired, or rejected), 2 - only return delivery receipt when final state is failed (expired or rejected)"/>
        <cfargument name="inpXMLControlString" required="no" default="" hint="Allow custom XMLControlString to be passed in - still must be valid user/batch but will override the default XMLControlString in the Batch">
        <cfargument name="DebugAPI" required="no" default="0">

        <!--- Change flags for special processing --->
        <cfset var inpPostToQueueForWebServiceDeviceFulfillment = 0 />

        <cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />

        <!--- SOAP, REST, FORM, or URL --->
        <cfset var dataout = '0' />
        <cfset var ServiceRequestdataout = '0' />
        <cfset var INPSCRIPTID = "-1" />
        <cfset var inpLibId = "-1" />
        <cfset var inpEleId = "-1" />
        <cfset var inpLimitDistribution = 0 />
        <cfset var ABTestingBatches = "" />
		<cfset var BlockGroupMembersAlreadyInQueue = 1 />
		<cfset var QueuedScheduledDate = "NOW()" />
		<cfset var DebugStr = "Start" />
		<cfset var NEXTBATCHID = -1 />
        <cfset var CurrUserId = -1 />
        <cfset var CurrTZ = "31" />
        <cfset var inpLibId = "0" />
        <cfset var inpEleId = "0" />
        <cfset var inpScriptId = "0" />
        <cfset var INPGROUPID = "0" />
        <cfset var CURRTZVOICE = "-1" />
        <cfset var BLOCKEDBYDNC = 0 />
        <cfset var SMSINIT = 0 />
        <cfset var SMSINITMESSAGE = "" />
        <cfset var COUNTQUEUEDUPVOICE = 0 />
        <cfset var COUNTQUEUEDUPEMAIL = 0 />
        <cfset var COUNTQUEUEDUPSMS = 0 />
        <cfset var LASTQUEUEDUPID = 0 />
		<cfset var EstimatedCost = 0.00 />
		<cfset var SMSONLYXMLCONTROLSTRING_VCH = "" />
        <cfset var DBSourceEBM = "BishopDev"/>
		<cfset var ESIID = "-15"/>
		<cfset var VariableNamesArray = ArrayNew(1) />
		<cfset var LocalGMTRelative = -8 />
		<cfset var AfterHoursBlocked = 0 />
		<cfset var CurrGMTRelative = -8 />
		<cfset var LocalRelative = 0 />
		<cfset var CurrTime = NOW() />
		<cfset var ServiceInputFlag = 1 />
		<cfset var ENA_Message = "pdc-AddToQueue Debugging Info" />
		<cfset var SubjectLine = "SimpleX SMS API Result - pdc-AddToQueue" />
		<cfset var TroubleShootingTips="See CatchMessage_vch in this log for details" />
		<cfset var ErrorNumber="1114" />
		<cfset var AlertType="1" />
        <cfset var InsertToAPILog = '' />
		<cfset arguments.inpContactString = TRIM(arguments.inpContactString) />

		<cfset var whichField = 0 />
		<cfset var GetTZInfo = 0 />
		<cfset var GetRawDataTZs = 0 />
		<cfset var GetRawDataCount = 0 />
		<cfset var InsertToErrorLog = 0 />

        <cfset var temp = '' />
        <cfset var RowCountVar = 0>
        <cfset var VOICEONLYXMLCONTROLSTRING_VCH = "">
        <cfset var GetUserDNCFroServiceRequest = ''/>
        <cfset var UserLocalDNCBlocked = 0 />

        <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
        <cfset var InvalidMCIDXML	= '' />
		<cfset var DDMBuffA	= '' />
        <cfset var DDMBuffB	= '' />
        <cfset var DDMBuffC	= '' />
        <cfset var DDMPos1	= '' />
        <cfset var DDMPos2	= '' />
        <cfset var DDMReultsArray	= '' />
        <cfset var RawDataFromDB	= '' />
        <cfset var myxmldocResultDoc	= '' />
        <cfset var DebugStr	= '' />
        <cfset var selectedElements	= '' />
        <cfset var CURRVAL	= '' />
        <cfset var DDMSWITCHReultsArray	= '' />
        <cfset var CurrSwitchValue	= '' />
        <cfset var CurrSwitchQIDValue	= '' />
        <cfset var CurrSwitchBSValue	= '' />
        <cfset var CaseMatchFound	= '' />
        <cfset var CurrCaseReplaceString	= '' />
        <cfset var XMLFDDoc	= '' />
        <cfset var selectedElementsII	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var XMLDefaultDoc	= '' />
        <cfset var DDMCONVReultsArray	= '' />
        <cfset var CurrXMLConversionType	= '' />
        <cfset var AccountnumberTwoAtATimeBuffer	= '' />
        <cfset var CurrAccountNum	= '' />
        <cfset var TwoDigitLibrary	= '' />
        <cfset var SingleDigitLibrary	= '' />
        <cfset var PausesLibrary	= '' />
        <cfset var PausesStyle	= '' />
        <cfset var ISDollars	= '' />
        <cfset var ISDecimalPlace	= '' />
        <cfset var DecimalLibrary	= '' />
        <cfset var MoneyLibrary	= '' />
        <cfset var HundredsLibrary	= '' />
        <cfset var CurrDynamicAmount	= '' />
        <cfset var IncludeDayOfWeek	= '' />
        <cfset var IncludeTimeOfWeek	= '' />
        <cfset var DayofWeekElement	= '' />
        <cfset var MonthElement	= '' />
        <cfset var DayofMonthElement	= '' />
        <cfset var YearElement	= '' />
        <cfset var DSTimeElement	= '' />
        <cfset var TwoDigitElement	= '' />
        <cfset var CurrMessageXMLTransactionDate	= '' />
        <cfset var CurrTransactionDate	= '' />
        <cfset var CurrXMLConvDesc	= '' />
        <cfset var CurrTransactionVar	= '' />
        <cfset var CurrXMLConversionMessage	= '' />
        <cfset var CurrDSHour	= '' />
        <cfset var CurrDSMinute	= '' />
        <cfset var CurrCentAmountBuff	= '' />
        <cfset var CurrMessageXMLDecimal	= '' />
        <cfset var CurrDynamicAmountBuff	= '' />
        <cfset var BuffStr	= '' />
        <cfset var IsDecimal	= '' />
        <cfset var CurrDDMVar	= '' />
        <cfset var CurrDDMSWITCHVar	= '' />
        <cfset var CurrFDXML	= '' />
        <cfset var CurrWildcardXML	= '' />
        <cfset var CurrDefaultXML	= '' />
        <cfset var CurrDDMCONVVar	= '' />
        <cfset var AccountnumberIndex	= '' />
        <cfset var ixi	= '' />
        <cfset var iixi	= '' />
        <cfset var GetCustomFields = '' />
        <cfset var GetShortCodeData = '' />
        <cfset var GetRawData = '' />
        <cfset var inpCarrier = ""/>
		<cfset var inpShortCode = ""/>
        <cfset var inpKeyword = ""/>
        <cfset var inpTransactionId = ""/>
        <cfset var inpServiceId = ""/>
        <cfset var inpXMLDATA = ""/>
        <cfset var inpOverRideInterval = "0"/>
        <cfset var inpTimeOutNextQID = "0"/>
        <cfset var inpQAToolRequest = "0"/>
        <cfset var PassOnFormData = ''>
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var RetValGetXML = "">
        <cfset var NEWUUID = "" />
        <cfset var GetTimeZoneInfoData = "">
        <cfset var RetValSchedule = "" />

        <cfset var RetValGetCurrentUserEmailSubaccountInfo = '' />

        <cfset var eMailSubUserAccountName = '' />
		<cfset var eMailSubUserAccountPassword = '' />
        <cfset var eMailSubUserAccountRemoteAddress = '' />
        <cfset var eMailSubUserAccountRemotePort = '' />
        <cfset var eMailSubUserAccountTLSFlag = '' />
        <cfset var eMailSubUserAccountSSLFlag = '' />

        <cfset var inpeMailHTMLTransformed = '' />
        <cfset var PreviewFileName = '' />
        <cfset var ScriptProcessedOutput = ''/ >
        <cfset var inpIRETypeAdd = IREMESSAGETYPE_API_TRIGGERED /> 
        <cfset var CurrNPA = '' />
		<cfset var CurrNXX = '' />


        <!--- Then include main processing --->
        <!---<cfinclude template="inc_addtoproc.cfm">  --->


        <cfif DebugAPI GT 0 >

<!---             <cfset DBSourceEBM = "BishopDev" />
            <cfset Session.DBSourceEBM = "BishopDev" />
            <cfset ESIID = "-15" />    --->
            <cfset DBSourceEBM = "Bishop"/>
            <cfset Session.DBSourceEBM = "Bishop"/>
            <cfset ESIID = "-14"/>

        <cfelse>

            <cfset DBSourceEBM = "Bishop"/>
            <cfset Session.DBSourceEBM = "Bishop"/>
            <cfset ESIID = "-14"/>

        </cfif>

         <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	     --->

       	<cfoutput>
	       	
	       	

	        <!--- Start timing test --->
            <cfset tickBegin = GetTickCount()>

			<cfif !isnumeric(inpDistributionProcessIdLocal)>
            	<cfset inpDistributionProcessIdLocal = 0 />
        	</cfif>

        	<!--- Even though no group is specified the default group of 0 is used.--->
        	<cfif inpBlockDuplicates GT 0>
	        	<cfset BlockGroupMembersAlreadyInQueue = 1>
    		<cfelse>
            	<cfset BlockGroupMembersAlreadyInQueue = 0>
            </cfif>

            <cfif TRIM(inpScheduleOffsetSeconds) NEQ 0 AND ISNUMERIC(TRIM(inpScheduleOffsetSeconds))>
            	<cfset QueuedScheduledDate = "DATE_ADD(NOW(), INTERVAL #inpScheduleOffsetSeconds# SECOND)">
            </cfif>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout = StructNew() />   
			<cfset dataout.STATUSCODE = "-1" />
            <cfset dataout.inpContactString = "#inpContactString#" />
            <cfset dataout.inpBatchId = "#inpBatchId#" />
            <cfset dataout.inpTimeZone = "#inpTimeZone#" />
            <cfset dataout.CURRTZ = "#inpTimeZone#" />
            <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
            <cfset dataout.REQUESTUUID = "#NEWUUID#" />
            <cfset dataout.SMSINIT = "0" />
            <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
            <cfset dataout.SMSDEST = "#inpShortCode#" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.MESSAGE = "" />		
            
            <cftry>

              	<cfif inpContactString EQ "">
                	<cfthrow MESSAGE="Invalid inpContactString Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

				<!--- Cleanup SQL injection --->

                <cfif !isnumeric(inpBatchId) OR inpBatchId EQ 0 OR inpBatchId EQ "">
                    <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-6">
                </cfif>

                <cfset NEXTBATCHID = inpBatchId>

                <cfset CurrUserId = Session.UserId>

                <cfif CurrUserId EQ "" OR CurrUserId LT 1>
                	<cfthrow MESSAGE="Invalid Authentication Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

                    <!--- Other security checks --->

					<cfinvoke method="GetXMLControlString" returnvariable="RetValGetXML">
	                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
	                    <cfinvokeargument name="REQSESSION" value="1">
	                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">
	                </cfinvoke>

                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <cfif RetValGetXML.REALTIMEFLAG LT 1>
                        <cfthrow MESSAGE="Error - This Batch is not enabled for real time processing." TYPE="Any" detail="Contact your system administrator for help. #Session.DBSourceEBM# #INPBATCHID#" errorcode="-15">
                    </cfif>

                    <!--- Verify all numbers are actual numbers --->
                    <cfif !isnumeric(inpBatchId)>
                        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>

                	<!--- Allow users to create their own control strings --->
                	<cfif LEN(arguments.inpXMLControlString) GT 0>
                        <cfset RetValGetXML.XMLCONTROLSTRING = arguments.inpXMLControlString />
                    </cfif>

                    <cfset NEXTBATCHID = "#inpBatchId#">

                   <!--- Auto create schedule with defaults or specified values--->
                   <!--- Validate Schedule is elligible --->

					<cfset CurrTZ = "31"> <!--- Default to PST--->

                    <cfif inpContactString NEQ "" >

                        <cfif inpSkipNumberValidations EQ 0 >

                            <!---Find and replace all non numerics except P X * #--->
                            <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

                            <!--- Clean up where start character is a 1 --->
                            <cfif LEFT(inpContactString, 1) EQ "1">
                                <cfset arguments.inpContactString = RemoveChars(inpContactString, 1, 1)>
                            </cfif>

                        </cfif>

                        <!--- Is International number--->
                        <cfif inpInternational GT 0>
                            <!--- Validate allowed to / Security--->

                            <!--- Validate 011 / Country code --->
                            <cfif LEFT(inpContactString, 3) NEQ "011" >
                                <cfthrow MESSAGE="International number must specify 011 and country code " TYPE="Any" detail="" errorcode="-4">
                            </cfif>

                            <cfset CurrTZ = "0" >

                        <cfelse>
                            <!--- Validate proper 10 digit North American phone number--->

                            <cfif LEN(LEFT(inpContactString, 10)) LT 10 OR !ISNUMERIC(LEFT(inpContactString, 10))>
                                <cfthrow MESSAGE="Not a valid 10 digit North American phone number" TYPE="Any" detail="" errorcode="-3">
                            </cfif>

                            <!--- Check for supplied Time Zone --->
                            <cfswitch expression="#UCASE(inpTimeZone)#">

                                <cfcase value="UNK"><cfset CurrTZ = "0"></cfcase>
                                <cfcase value="GUAM"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "38"> <cfelse> <cfset CurrTZ = "37"> </cfif></cfcase>
                                <cfcase value="SAMOA"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "35"> <cfelse> <cfset CurrTZ = "34"> </cfif></cfcase>
                                <cfcase value="HAWAII"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                <cfcase value="HAST"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                <cfcase value="ALASKA"><cfset CurrTZ = "32"></cfcase>
                                <cfcase value="PACIFIC"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="MOUNTAIN"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="CENTRAL"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="EASTERN"><cfset CurrTZ = "28"></cfcase>
                                <cfcase value="PDT"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="PST"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="MDT"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="MST"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="CDT"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="CST"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="EDT"><cfset CurrTZ = "28"></cfcase>
                                <cfcase value="EST"><cfset CurrTZ = "28"></cfcase>

                                <cfdefaultcase>
                                    <!--- DO lookup--->
                                    <cfset CurrTZ = "-1">
                                    
                                    <!--- Adjust for numers that start with 1 --->
						            <cfif LEFT(inpContactString, 1) EQ 1>
						                <cfset CurrNPA = MID(arguments.inpContactString, 2, 3)>
						                <cfset CurrNXX = MID(arguments.inpContactString, 5, 3)>
						                
						                <!--- Get rid of 1 when looking up in DB --->
						                <cfset arguments.inpContactString = RIGHT(inpContactString, LEN(arguments.inpContactString)-1)>
						            <cfelse>
						                <cfset CurrNPA = LEFT(arguments.inpContactString, 3) >
						                <cfset CurrNXX = MID(arguments.inpContactString, 4, 3)>
						            </cfif>
		            
                                    <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                        SELECT
                                            CASE cx.T_Z
                                                  WHEN 14 THEN 37
                                                  WHEN 11 THEN 34
                                                  WHEN 10 THEN 33
                                                  WHEN 9 THEN 32
                                                  WHEN 8 THEN 31
                                                  WHEN 7 THEN 30
                                                  WHEN 6 THEN 29
                                                  WHEN 5 THEN 28
                                                  WHEN 4 THEN 27
                                                 END AS TimeZone,
                                            CASE
                                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                                  ELSE 0
                                                  END  AS CellFlag
                                        FROM
                                            MelissaData.FONE AS fx JOIN
                                            MelissaData.CNTY AS cx ON
                                            (fx.FIPS = cx.FIPS)
                                        WHERE
                                        	fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                                        AND
                                        	fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                                     </cfquery>

                                     <cfif GetTZInfo.RecordCount GT 0>
                                        <cfset CurrTZ = "#GetTZInfo.TimeZone#">
                                     <cfelse>
                                        <cfset CurrTZ = "-1">
                                     </cfif>

                                </cfdefaultcase>

                            </cfswitch>

                            <cfif CurrTZ EQ "-1" >
                                <!---<cfthrow MESSAGE="Could not find time zone and no valid default value provided. Possible inpTimeZone values are UNK,GUAM,SAMOA,HAWAII,ALASKA,PACIFIC,PDT,PST,MOUNTAIN,MDT,MST,CENTRAL,CDT,CST,EASTERN,EDT,EST" TYPE="Any" detail="" errorcode="-5">--->
                                <!--- For real time triggered SMS TimeZone is not required --->
                                <cfset CurrTZ = "31">
                            </cfif>

                        </cfif>

                    </cfif>
                    
					<!---	<cfset DebugStr = DebugStr & " URLData=#SerializeJSON(URL)#"> --->
					<cfset StructAppend(FORM, URL, true) />
					<cfset StructAppend(FORM, arguments, true) />

					<cfif inpContactString NEQ "">

                        <cfset ServiceRequestdataout =  QueryNew("	UserId_int,
                                                                    ContactTypeId_int,
                                                                    ContactString_vch,
                                                                    TimeZone_int,
																	OperatorId_int                                                                   
                                                                     ", "VarChar,VarChar,VarChar,VarChar,VarChar")>

                        <cfset QueryAddRow(ServiceRequestdataout) />


						<!--- Add form data  --->
                        <cfloop collection="#FORM#" item="whichField">

                           <!--- Only add fields not already on list of standard fields --->
                           <cfif NOT structKeyExists(ServiceRequestdataout, '#whichField#')  >

                                <!---<cfoutput>#whichField# = #FORM[whichField]#</cfoutput><br>--->

                                <cfset VariableNamesArray = ArrayNew(1)>
                                <cfset ArraySet(VariableNamesArray, 1, 1, "#FORM[whichField]#")>
                                <cfset QueryAddColumn(ServiceRequestdataout, whichField, "VarChar", VariableNamesArray)>

                            </cfif>

                        </cfloop>

                        <cfset QuerySetCell(ServiceRequestdataout, "UserId_int", "#Session.UserId#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "ContactString_vch", "#inpContactString#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "ContactTypeId_int", "#inpContactTypeId#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "TimeZone_int", "#CurrTZ#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "OperatorId_int", "0") />
                     
                    </cfif>

               		<!--- Validate Schedule Active --->
               		<cfif inpValidateScheduleActive GT 0>

						<!--- Verify all numbers are actual numbers --->
                        <cfquery name="GetRawDataTZs" dbTYPE="query">
                            SELECT
                                DISTINCT TimeZone_int
                            FROM
                                ServiceRequestdataout
                        </cfquery>

                        <!---component="schedule"--->
                        <cfinvoke
                             method="GetModernSchedule"
                             returnvariable="RetValSchedule">
                                <cfinvokeargument name="inpBatchId" value="#NEXTBATCHID#"/>
                        </cfinvoke>

<!---
						<cfif RetValSchedule.RXRESULTCODE NEQ "1"> <!---// allows 24 hr operation--->
                        	<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Schedule Not Set For This Day" errorcode="-10">
                        </cfif>
--->
                                
                        <!--- set these in session--->
                        <cfset LocalGMTRelative = -8>
                        <cfset AfterHoursBlocked = 0>

                        <cfset CurrGMTRelative = -8>
                        <cfset LocalRelative = CurrGMTRelative - LocalGMTRelative>

                        <cfset CurrGMTRelative = 0>

                        <cfloop query="GetRawDataTZs">

							 <cfswitch expression="#GetRawDataTZs.TimeZone_int#" >
                             	<cfcase value="0"><cfset CurrGMTRelative = 0></cfcase>
                                <cfcase value="1"><cfset CurrGMTRelative = 0></cfcase>
                                <cfcase value="2"><cfset CurrGMTRelative = 1></cfcase>
                                <cfcase value="3"><cfset CurrGMTRelative = 2></cfcase>
                                <cfcase value="4"><cfset CurrGMTRelative = 3></cfcase>
                                <cfcase value="5"><cfset CurrGMTRelative = 3></cfcase>
                                <cfcase value="6"><cfset CurrGMTRelative = 4></cfcase>
                                <cfcase value="7"><cfset CurrGMTRelative = 4></cfcase>
                                <cfcase value="8"><cfset CurrGMTRelative = 5></cfcase>
                                <cfcase value="9"><cfset CurrGMTRelative = 5></cfcase>
                                <cfcase value="10"><cfset CurrGMTRelative = 6></cfcase>
                                <cfcase value="11"><cfset CurrGMTRelative = 6></cfcase>
                                <cfcase value="12"><cfset CurrGMTRelative = 7></cfcase>
                                <cfcase value="13"><cfset CurrGMTRelative = 8></cfcase>
                                <cfcase value="14"><cfset CurrGMTRelative = 9></cfcase>
                                <cfcase value="15"><cfset CurrGMTRelative = 9></cfcase>
                                <cfcase value="16"><cfset CurrGMTRelative = 10></cfcase>
                                <cfcase value="17"><cfset CurrGMTRelative = 10></cfcase>
                                <cfcase value="18"><cfset CurrGMTRelative = 11></cfcase>
                                <cfcase value="19"><cfset CurrGMTRelative = 11></cfcase>
                                <cfcase value="20"><cfset CurrGMTRelative = 12></cfcase>
                                <cfcase value="21"><cfset CurrGMTRelative = 13></cfcase>
                                <cfcase value="22"><cfset CurrGMTRelative = 14></cfcase>
                                <cfcase value="23"><cfset CurrGMTRelative = -1></cfcase>
                                <cfcase value="24"><cfset CurrGMTRelative = -2></cfcase>
                                <cfcase value="25"><cfset CurrGMTRelative = -3></cfcase>
                                <cfcase value="26"><cfset CurrGMTRelative = -3></cfcase>
                                <cfcase value="27"><cfset CurrGMTRelative = -4></cfcase>
                                <cfcase value="28"><cfset CurrGMTRelative = -5></cfcase>
                                <cfcase value="29"><cfset CurrGMTRelative = -6></cfcase>
                                <cfcase value="30"><cfset CurrGMTRelative = -7></cfcase>
                                <cfcase value="31"><cfset CurrGMTRelative = -8></cfcase>
                                <cfcase value="32"><cfset CurrGMTRelative = -9></cfcase>
                                <cfcase value="33"><cfset CurrGMTRelative = -10></cfcase>
                                <cfcase value="34"><cfset CurrGMTRelative = -11></cfcase>
                                <cfcase value="35"><cfset CurrGMTRelative = -12></cfcase>
                                <cfcase value="36"><cfset CurrGMTRelative = -13></cfcase>
                                <cfcase value="37"><cfset CurrGMTRelative = -14></cfcase>

                             </cfswitch>

							<cfset LocalRelative = CurrGMTRelative - LocalGMTRelative>
 
                            <cfset CurrTime = NOW()>

							<!---// do not go past midnight or start over--->
                            <cfif HOUR(CurrTime) + LocalRelative  LT 25><!---// Not past midnight--->

								<!---// Validate Start hour--->
                                <cfif RetValSchedule.STARTHOUR_TI GT 0> <!---// allows 24 hr operation--->
                                	<cfif HOUR(CurrTime) + LocalRelative LT RetValSchedule.STARTHOUR_TI>
    									<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<!---// allows blackout hours--->
								<cfif RetValSchedule.BLACKOUTSTARTHOUR_TI GT 0 AND RetValSchedule.BLACKOUTENDHOUR_TI GT 0 AND RetValSchedule.BLACKOUTSTARTHOUR_TI LT RetValSchedule.RetValSchedule.BLACKOUTENDHOUR_TI>
									<cfif  (HOUR(CurrTime) + LocalRelative) GTE RetValSchedule.BLACKOUTSTARTHOUR_TI AND (HOUR(CurrTime) + LocalRelative) LT RetValSchedule.BLACKOUTENDHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Blackout Period Set #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.BLACKOUTENDHOUR_TI#" errorcode="-10">
	                                </cfif>
                                </cfif>

    							<cfif RetValSchedule.ENDHOUR_TI LT 24> <!---// allows 24 hr operation--->
									<!---// Validate End Hour--->

										<cfif HOUR(CurrTime) + LocalRelative EQ RetValSchedule.ENDHOUR_TI AND ( RetValSchedule.ENDMINUTE_TI EQ 0 OR RetValSchedule.ENDMINUTE_TI LT 0 OR RetValSchedule.ENDMINUTE_TI GT 59 )>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>

										<cfif HOUR(CurrTime) + LocalRelative GT RetValSchedule.ENDHOUR_TI>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>
								</cfif>


								<!---// Check start minutes--->
								<cfif RetValSchedule.STARTMINUTE_TI GT 0 AND RetValSchedule.STARTMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative) EQ RetValSchedule.STARTHOUR_TI ) AND ( MINUTE(CurrTime) LT RetValSchedule.STARTMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<!---// Check end minutes--->
								<cfif RetValSchedule.ENDMINUTE_TI GT 0 AND RetValSchedule.ENDMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative) EQ RetValSchedule.ENDHOUR_TI ) AND ( MINUTE(CurrTime) GTE RetValSchedule.ENDMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<cfif AfterHoursBlocked GT 0>
								<!---// Extra block to prevent after hours dialing--->

									<cfif HOUR(CurrTime) + LocalRelative LT 8>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early After Hours Blocked #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>

									<cfif HOUR(CurrTime) + LocalRelative GTE 21>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late After Hours Blocked #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>

								<!---// Extra block to prevent after hours dialing--->
                                </cfif>

							<!---// Not past midnight--->
							<cfelse>
							<!---// Past midnight--->
								<!---// Validate Start hour--->
								<cfif RetValSchedule.STARTHOUR_TI GT 0> <!---// allows 24 hr operation--->
									<cfif HOUR(CurrTime) + LocalRelative  - 24 LT RetValSchedule.STARTHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<cfif RetValSchedule.ENDHOUR_TI LT 24><!--- // allows 24 hr operation--->
									<!---// Validate End Hour--->

										<cfif HOUR(CurrTime) + LocalRelative - 24 EQ RetValSchedule.ENDHOUR_TI AND ( RetValSchedule.ENDMINUTE_TI EQ 0 OR RetValSchedule.ENDMINUTE_TI LT 0 OR RetValSchedule.ENDMINUTE_TI GT 59 )>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>

										<cfif HOUR(CurrTime) + LocalRelative - 24 GT RetValSchedule.ENDHOUR_TI>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>
								</cfif>

									<!---// Check start minutes--->
								<cfif RetValSchedule.STARTMINUTE_TI GT 0 AND RetValSchedule.STARTMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative - 24) EQ RetValSchedule.STARTHOUR_TI ) AND ( MINUTE(CurrTime) LT RetValSchedule.STARTMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<!---// Check end minutes--->
								<cfif RetValSchedule.ENDMINUTE_TI GT 0 AND RetValSchedule.ENDMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative - 24) EQ RetValSchedule.ENDHOUR_TI ) AND ( MINUTE(CurrTime) GTE RetValSchedule.ENDMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<!---// allows blackout hours--->
								<cfif RetValSchedule.BLACKOUTSTARTHOUR_TI GT 0 AND RetValSchedule.BLACKOUTENDHOUR_TI GT 0 AND RetValSchedule.BLACKOUTSTARTHOUR_TI LT RetValSchedule.BLACKOUTENDHOUR_TI>
									<cfif  (HOUR(CurrTime) + LocalRelative - 24) GTE RetValSchedule.BLACKOUTSTARTHOUR_TI AND (HOUR(CurrTime) + LocalRelative - 24) LT RetValSchedule.BLACKOUTENDHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Blackout Period Set #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.BLACKOUTENDHOUR_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<cfif AfterHoursBlocked GT 0>
								<!---// Extra block top prevent after hours dialing--->

									<cfif HOUR(CurrTime) + LocalRelative - 24 LT 8>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early After Hours Blocked #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
									</cfif>

									<cfif HOUR(CurrTime) + LocalRelative - 24 GTE 21>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late After Hours Blocked #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
									</cfif>

								</cfif><!---// Extra block top prevent after hours dialing--->


                            </cfif><!---// Past midnight--->

                        </cfloop>

					</cfif>


              		<cfset DebugStr = DebugStr & " GetRawDataCount">


   					<!--- Verify all numbers are actual numbers --->
		            <cfquery name="GetRawDataCount" dbTYPE="query">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            ServiceRequestdataout
                    </cfquery>

                    <cfif GetRawDataCount.TOTALCOUNT EQ "">

         	           <cfthrow MESSAGE="Distribution Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) No data elligable to send - check your contact strings." errorcode="-5">

                    </cfif>

                   
                   <!--- Old billing logic - now handled in SMS directly
                   
                    <cfset var TotalUnitCost = GetRawDataCount.TOTALCOUNT/>

                    <!--- Voice Credit surcharge --->
                    <cfif inpContactTypeId EQ 1>
                    	<cfset TotalUnitCost = TotalUnitCost * 3 />
                    </cfif>

                    <!--- SMS Credit surcharge --->
                    <cfif inpContactTypeId EQ 3>
                    	<cfset TotalUnitCost = TotalUnitCost * 2 />
                    </cfif>

                    <!--- Make sure it is ok with billing - only charge when dial complete --->
                    <!---component="billing"--->
                    <cfinvoke
                     method="ValidateBilling"
                     returnvariable="local.RetValBillingData">
                        <cfinvokeargument name="inpCount" value="#TotalUnitCost#"/>
                     	<cfinvokeargument name="inpUserId" value="#CurrUserId#"/>
                        <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                    </cfinvoke>

                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) #RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <!--- This is inserted into Queue  --->
                    <cfset EstimatedCost = RetValBillingData.ESTIMATEDCOSTPERUNIT>
                   
                   --->
                   
                    
                    
                    <cfset EstimatedCost = 0>


                    <!--- Changes which data source to process for each channel--->
                    <cfset ServiceInputFlag = 1>



                    <!--- Load current user's email sub account information if any --->
                    <cfif ListContains(inpContactTypeId, 2)>

                        <cfinvoke
                        	method="GetCurrentUserEmailSubaccountInfo"
                         	returnvariable="RetValGetCurrentUserEmailSubaccountInfo">
                            <cfinvokeargument name="inpUserId" value="#CurrUserId#"/>
                        </cfinvoke>

                        <cfset eMailSubUserAccountName = "#RetValGetCurrentUserEmailSubaccountInfo.USERNAME#" />
                        <cfset eMailSubUserAccountPassword = "#RetValGetCurrentUserEmailSubaccountInfo.PASSWORD#" />
                        <cfset eMailSubUserAccountRemoteAddress = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEADDRESS#" />
                        <cfset eMailSubUserAccountRemotePort = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEPORT#" />
                        <cfset eMailSubUserAccountTLSFlag = "#RetValGetCurrentUserEmailSubaccountInfo.TLSFLAG#" />
                        <cfset eMailSubUserAccountSSLFlag = "#RetValGetCurrentUserEmailSubaccountInfo.SSLFLAG#" />

					</cfif>


                    <!--- Insert Voice, eMAil, SMS into dial queue--->



                    <!---           <cfthrow MESSAGE="Debug Error" TYPE="Any" detail="Made It Here" errorcode="-5">         --->

                    <cfset DebugStr = DebugStr & " Load Start">

					<!--- SMS only so far--->	
					<cfset inpContactTypeId = 3 />
                    <cfinclude template="../../../../../session/cfc/Includes/LoadQueueSMS_Dynamic.cfm">
                   
                    <cfset DebugStr = DebugStr & " Load Finished">

					<cfset dataout = StructNew() />   
					<cfset dataout.STATUSCODE = "1" />
                    <cfset dataout.inpContactString = "#inpContactString#" />
                    <cfset dataout.inpBatchId = "#inpBatchId#" />
                    <cfset dataout.inpTimeZone = "#inpTimeZone#" />
                    <cfset dataout.CURRTZ = "#CURRTZ#" />
                    <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
                    <cfset dataout.REQUESTUUID = "#NEWUUID#" />
                    <cfset dataout.SMSINIT = "#SMSINIT#" />
                    <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
                    <cfset dataout.SMSDEST = "#inpShortCode#" />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.ERRMESSAGE = "" />
                  
                    <cfif DebugAPI GT 0>
                        <cfset dataout.MESSAGE = "#SMSINITMESSAGE# DebugAPI=#DebugAPI# #DebugStr# local.RetValProcessNextResponseSMS=#SerializeJSON(local.RetValProcessNextResponseSMS)#" />					
                    <cfelse>
                        <cfset dataout.MESSAGE = "#SMSINITMESSAGE#" />             
                    </cfif>
                

            <cfcatch TYPE="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

				<!---<cfset dataout =  QueryNew("STATUSCODE, inpContactString, inpBatchId, inpTimeZone, CURRTZ, DISTRIBUTIONID, REQUESTUUID, BLOCKEDBYDNC, SMSINIT, SMSDEST, TYPE, MESSAGE, ERRMESSAGE")>--->
                <cfset dataout = StructNew() />            
                
                <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
                <cfset dataout.inpContactString = "#inpContactString#" />
                <cfset dataout.inpBatchId = "#inpBatchId#" />
                <cfset dataout.inpTimeZone = "#inpTimeZone#" />
                <cfset dataout.CURRTZ = "#CURRTZ#" />
                <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
                <cfset dataout.REQUESTUUID = "#NEWUUID#" />
                <cfset dataout.SMSINIT = "0" />
                <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
                <cfset dataout.SMSDEST = "#inpShortCode#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
              
                <cfif DebugAPI GT 0>
	                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />					
                <cfelse>
	                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
                </cfif>

            </cfcatch>

            </cftry>

		</cfoutput>

        <!--- Log all API call outputs for debugging purposes --->
        <cftry>

            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SimpleX API Result - pdc-AddToRealTime">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-AddToRealTime Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>

        <cfcatch type="any">


        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>

    <cffunction name="GET" access="public" output="false" hint="Testing - Returns DESC=Hello! and Message=OK">		
        <cfargument name="inpBatchId" required="yes" default="0" hint="The pre-defined campaign Id you created under your account.">
        <cfargument name="inpContactString" required="yes" default="" hint="The contact string - Phone number ,eMail, or SMS number">
        <cfargument name="inpContactTypeId" required="no" default="2" hint="1=Phone number,  2=eMail, 3=SMS">
        <cfargument name="inpInternational" required="no" default="0" hint=" 1 will flag this number as international">
        <cfargument name="inpValidateScheduleActive" required="no" default="0" hint="1 will halt message if time is currently outside of the pre-defined campaigns's scheduled range">
        <cfargument name="inpTimeZone" required="no" default="">
        <cfargument name="inpMMLS" required="no" default="1">
        <cfargument name="inpScheduleOffsetSeconds" required="no" default="0">
        <cfargument name="inpBlockDuplicates" required="no" default="0">
        <cfargument name="inpValidateCPPActive" required="no" default="" hint="If provided - will validate request is valid against the CPP Id">
        <cfargument name="inpSkipNumberValidations" required="no" default="0">
        <cfargument name="inpSkipLocalUserDNCCheck" required="no" default="0">
        <cfargument name="inpDistributionProcessId" required="no" default="0">
        <cfargument name="inpRegisteredDeliverySMS" required="no" default="0" hint="Allow each request to require SMS delivery receipts. 0 – no delivery receipt requested, 1 - return delivery receipt on final state (i.e. delivered, expired, or rejected), 2 - only return delivery receipt when final state is failed (expired or rejected)"/>
        <cfargument name="inpXMLControlString" required="no" default="" hint="Allow custom XMLControlString to be passed in - still must be valid user/batch but will override the default XMLControlString in the Batch">
        <cfargument name="DebugAPI" required="no" default="0">

        <!--- Change flags for special processing --->
        <cfset var inpPostToQueueForWebServiceDeviceFulfillment = 0 />

        <cfset var inpDistributionProcessIdLocal = arguments.inpDistributionProcessId />

        <!--- SOAP, REST, FORM, or URL --->
        <cfset var dataout = '0' />
        <cfset var ServiceRequestdataout = '0' />
        <cfset var INPSCRIPTID = "-1" />
        <cfset var inpLibId = "-1" />
        <cfset var inpEleId = "-1" />
        <cfset var inpLimitDistribution = 0 />
        <cfset var ABTestingBatches = "" />
		<cfset var BlockGroupMembersAlreadyInQueue = 1 />
		<cfset var QueuedScheduledDate = "NOW()" />
		<cfset var DebugStr = "Start" />
		<cfset var NEXTBATCHID = -1 />
        <cfset var CurrUserId = -1 />
        <cfset var CurrTZ = "31" />
        <cfset var inpLibId = "0" />
        <cfset var inpEleId = "0" />
        <cfset var inpScriptId = "0" />
        <cfset var INPGROUPID = "0" />
        <cfset var CURRTZVOICE = "-1" />
        <cfset var BLOCKEDBYDNC = 0 />
        <cfset var SMSINIT = 0 />
        <cfset var SMSINITMESSAGE = "" />
        <cfset var COUNTQUEUEDUPVOICE = 0 />
        <cfset var COUNTQUEUEDUPEMAIL = 0 />
        <cfset var COUNTQUEUEDUPSMS = 0 />
        <cfset var LASTQUEUEDUPID = 0 />
		<cfset var EstimatedCost = 0.00 />
		<cfset var SMSONLYXMLCONTROLSTRING_VCH = "" />
        <cfset var DBSourceEBM = "BishopDev"/>
		<cfset var ESIID = "-15"/>
		<cfset var VariableNamesArray = ArrayNew(1) />
		<cfset var LocalGMTRelative = -8 />
		<cfset var AfterHoursBlocked = 0 />
		<cfset var CurrGMTRelative = -8 />
		<cfset var LocalRelative = 0 />
		<cfset var CurrTime = NOW() />
		<cfset var ServiceInputFlag = 1 />
		<cfset var ENA_Message = "pdc-AddToQueue Debugging Info" />
		<cfset var SubjectLine = "SimpleX SMS API Result - pdc-AddToQueue" />
		<cfset var TroubleShootingTips="See CatchMessage_vch in this log for details" />
		<cfset var ErrorNumber="1114" />
		<cfset var AlertType="1" />
        <cfset var InsertToAPILog = '' />
		<cfset arguments.inpContactString = TRIM(arguments.inpContactString) />

		<cfset var whichField = 0 />
		<cfset var GetTZInfo = 0 />
		<cfset var GetRawDataTZs = 0 />
		<cfset var GetRawDataCount = 0 />
		<cfset var InsertToErrorLog = 0 />

        <cfset var temp = '' />
        <cfset var RowCountVar = 0>
        <cfset var VOICEONLYXMLCONTROLSTRING_VCH = "">
        <cfset var GetUserDNCFroServiceRequest = ''/>
        <cfset var UserLocalDNCBlocked = 0 />

        <!--- Dynamic data processing variables for local scope in Voice and email stuff --->
        <cfset var InvalidMCIDXML	= '' />
		<cfset var DDMBuffA	= '' />
        <cfset var DDMBuffB	= '' />
        <cfset var DDMBuffC	= '' />
        <cfset var DDMPos1	= '' />
        <cfset var DDMPos2	= '' />
        <cfset var DDMReultsArray	= '' />
        <cfset var RawDataFromDB	= '' />
        <cfset var myxmldocResultDoc	= '' />
        <cfset var DebugStr	= '' />
        <cfset var selectedElements	= '' />
        <cfset var CURRVAL	= '' />
        <cfset var DDMSWITCHReultsArray	= '' />
        <cfset var CurrSwitchValue	= '' />
        <cfset var CurrSwitchQIDValue	= '' />
        <cfset var CurrSwitchBSValue	= '' />
        <cfset var CaseMatchFound	= '' />
        <cfset var CurrCaseReplaceString	= '' />
        <cfset var XMLFDDoc	= '' />
        <cfset var selectedElementsII	= '' />
        <cfset var OutToDBXMLBuff	= '' />
        <cfset var XMLDefaultDoc	= '' />
        <cfset var DDMCONVReultsArray	= '' />
        <cfset var CurrXMLConversionType	= '' />
        <cfset var AccountnumberTwoAtATimeBuffer	= '' />
        <cfset var CurrAccountNum	= '' />
        <cfset var TwoDigitLibrary	= '' />
        <cfset var SingleDigitLibrary	= '' />
        <cfset var PausesLibrary	= '' />
        <cfset var PausesStyle	= '' />
        <cfset var ISDollars	= '' />
        <cfset var ISDecimalPlace	= '' />
        <cfset var DecimalLibrary	= '' />
        <cfset var MoneyLibrary	= '' />
        <cfset var HundredsLibrary	= '' />
        <cfset var CurrDynamicAmount	= '' />
        <cfset var IncludeDayOfWeek	= '' />
        <cfset var IncludeTimeOfWeek	= '' />
        <cfset var DayofWeekElement	= '' />
        <cfset var MonthElement	= '' />
        <cfset var DayofMonthElement	= '' />
        <cfset var YearElement	= '' />
        <cfset var DSTimeElement	= '' />
        <cfset var TwoDigitElement	= '' />
        <cfset var CurrMessageXMLTransactionDate	= '' />
        <cfset var CurrTransactionDate	= '' />
        <cfset var CurrXMLConvDesc	= '' />
        <cfset var CurrTransactionVar	= '' />
        <cfset var CurrXMLConversionMessage	= '' />
        <cfset var CurrDSHour	= '' />
        <cfset var CurrDSMinute	= '' />
        <cfset var CurrCentAmountBuff	= '' />
        <cfset var CurrMessageXMLDecimal	= '' />
        <cfset var CurrDynamicAmountBuff	= '' />
        <cfset var BuffStr	= '' />
        <cfset var IsDecimal	= '' />
        <cfset var CurrDDMVar	= '' />
        <cfset var CurrDDMSWITCHVar	= '' />
        <cfset var CurrFDXML	= '' />
        <cfset var CurrWildcardXML	= '' />
        <cfset var CurrDefaultXML	= '' />
        <cfset var CurrDDMCONVVar	= '' />
        <cfset var AccountnumberIndex	= '' />
        <cfset var ixi	= '' />
        <cfset var iixi	= '' />
        <cfset var GetCustomFields = '' />
        <cfset var GetShortCodeData = '' />
        <cfset var GetRawData = '' />
        <cfset var inpCarrier = ""/>
		<cfset var inpShortCode = ""/>
        <cfset var inpKeyword = ""/>
        <cfset var inpTransactionId = ""/>
        <cfset var inpServiceId = ""/>
        <cfset var inpXMLDATA = ""/>
        <cfset var inpOverRideInterval = "0"/>
        <cfset var inpTimeOutNextQID = "0"/>
        <cfset var inpQAToolRequest = "0"/>
        <cfset var PassOnFormData = ''>
        <cfset var tickBegin = 0 />
        <cfset var tickEnd = 0 />
        <cfset var RetValGetXML = "">
        <cfset var NEWUUID = "" />
        <cfset var GetTimeZoneInfoData = "">
        <cfset var RetValSchedule = "" />

        <cfset var RetValGetCurrentUserEmailSubaccountInfo = '' />

        <cfset var eMailSubUserAccountName = '' />
		<cfset var eMailSubUserAccountPassword = '' />
        <cfset var eMailSubUserAccountRemoteAddress = '' />
        <cfset var eMailSubUserAccountRemotePort = '' />
        <cfset var eMailSubUserAccountTLSFlag = '' />
        <cfset var eMailSubUserAccountSSLFlag = '' />

        <cfset var inpeMailHTMLTransformed = '' />
        <cfset var PreviewFileName = '' />
        <cfset var ScriptProcessedOutput = ''/ >
        <cfset var inpIRETypeAdd = IREMESSAGETYPE_API_TRIGGERED /> 
		<cfset var CurrNPA = '' />
		<cfset var CurrNXX = '' />

        <!--- Then include main processing --->
        <!---<cfinclude template="inc_addtoproc.cfm">  --->


        <cfif DebugAPI GT 0 >

<!---             <cfset DBSourceEBM = "BishopDev" />
            <cfset Session.DBSourceEBM = "BishopDev" />
            <cfset ESIID = "-15" />    --->
            <cfset DBSourceEBM = "Bishop"/>
            <cfset Session.DBSourceEBM = "Bishop"/>
            <cfset ESIID = "-14"/>

        <cfelse>

            <cfset DBSourceEBM = "Bishop"/>
            <cfset Session.DBSourceEBM = "Bishop"/>
            <cfset ESIID = "-14"/>

        </cfif>

         <!---
        Positive is success
        1 = OK
		2 =
		3 =
		4 =

        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 =

	     --->

       	<cfoutput>

	        <!--- Start timing test --->
            <cfset tickBegin = GetTickCount()>

        	<!--- Even though no group is specified the default group of 0 is used.--->
        	<cfif inpBlockDuplicates GT 0>
	        	<cfset BlockGroupMembersAlreadyInQueue = 1>
    		<cfelse>
            	<cfset BlockGroupMembersAlreadyInQueue = 0>
            </cfif>

            <cfif TRIM(inpScheduleOffsetSeconds) NEQ 0 AND ISNUMERIC(TRIM(inpScheduleOffsetSeconds))>
            	<cfset QueuedScheduledDate = "DATE_ADD(NOW(), INTERVAL #inpScheduleOffsetSeconds# SECOND)">
            </cfif>

        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout = StructNew() />   
			<cfset dataout.STATUSCODE = "-1" />
            <cfset dataout.inpContactString = "#inpContactString#" />
            <cfset dataout.inpBatchId = "#inpBatchId#" />
            <cfset dataout.inpTimeZone = "#inpTimeZone#" />
            <cfset dataout.CURRTZ = "#inpTimeZone#" />
            <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
            <cfset dataout.REQUESTUUID = "#NEWUUID#" />
            <cfset dataout.SMSINIT = "0" />
            <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
            <cfset dataout.SMSDEST = "#inpShortCode#" />
            <cfset dataout.TYPE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.MESSAGE = "" />		
            
            <cftry>

              	<cfif inpContactString EQ "">
                	<cfthrow MESSAGE="Invalid inpContactString Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

				<!--- Cleanup SQL injection --->

                <cfif !isnumeric(inpBatchId) OR inpBatchId EQ 0 OR inpBatchId EQ "">
                    <cfthrow MESSAGE="Invalid Batch Id Specified" TYPE="Any" detail="" errorcode="-6">
                </cfif>

                <cfset NEXTBATCHID = inpBatchId>

                <cfset CurrUserId = Session.UserId>

                <cfif CurrUserId EQ "" OR CurrUserId LT 1>
                	<cfthrow MESSAGE="Invalid Authentication Specified" TYPE="Any" detail="" errorcode="-3">
                </cfif>

                    <!--- Other security checks --->

					<cfinvoke method="GetXMLControlString" returnvariable="RetValGetXML">
	                    <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
	                    <cfinvokeargument name="REQSESSION" value="1">
	                    <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#">
	                </cfinvoke>

                    <cfif RetValGetXML.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Error getting XML control String." TYPE="Any" detail="#RetValGetXML.MESSAGE# - #RetValGetXML.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <cfif RetValGetXML.REALTIMEFLAG LT 1>
                        <cfthrow MESSAGE="Error - This Batch is not enabled for real time processing." TYPE="Any" detail="Contact your system administrator for help. #Session.DBSourceEBM# #INPBATCHID#" errorcode="-15">
                    </cfif>

                    <!--- Verify all numbers are actual numbers --->
                    <cfif !isnumeric(inpBatchId)>
                        <cfthrow MESSAGE="Invalid Batch ID Specified" TYPE="Any" detail="" errorcode="-5">
                    </cfif>

                	<!--- Allow users to create their own control strings --->
                	<cfif LEN(arguments.inpXMLControlString) GT 0>
                        <cfset RetValGetXML.XMLCONTROLSTRING = arguments.inpXMLControlString />
                    </cfif>

                    <cfset NEXTBATCHID = "#inpBatchId#">

                   <!--- Auto create schedule with defaults or specified values--->
                   <!--- Validate Schedule is elligible --->

					<cfset CurrTZ = "31"> <!--- Default to PST--->

                    <cfif inpContactString NEQ "" >

                        <cfif inpSkipNumberValidations EQ 0 >

                            <!---Find and replace all non numerics except P X * #--->
                            <cfset arguments.inpContactString = REReplaceNoCase(inpContactString, "[^\d^\*^P^X^##]", "", "ALL")>

                            <!--- Clean up where start character is a 1 --->
                            <cfif LEFT(inpContactString, 1) EQ "1">
                                <cfset arguments.inpContactString = RemoveChars(inpContactString, 1, 1)>
                            </cfif>

                        </cfif>

                        <!--- Is International number--->
                        <cfif inpInternational GT 0>
                            <!--- Validate allowed to / Security--->

                            <!--- Validate 011 / Country code --->
                            <cfif LEFT(inpContactString, 3) NEQ "011" >
                                <cfthrow MESSAGE="International number must specify 011 and country code " TYPE="Any" detail="" errorcode="-4">
                            </cfif>

                            <cfset CurrTZ = "0" >

                        <cfelse>
                            <!--- Validate proper 10 digit North American phone number--->

                            <cfif LEN(LEFT(inpContactString, 10)) LT 10 OR !ISNUMERIC(LEFT(inpContactString, 10))>
                                <cfthrow MESSAGE="Not a valid 10 digit North American phone number" TYPE="Any" detail="" errorcode="-3">
                            </cfif>

                            <!--- Check for supplied Time Zone --->
                            <cfswitch expression="#UCASE(inpTimeZone)#">

                                <cfcase value="UNK"><cfset CurrTZ = "0"></cfcase>
                                <cfcase value="GUAM"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "38"> <cfelse> <cfset CurrTZ = "37"> </cfif></cfcase>
                                <cfcase value="SAMOA"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "35"> <cfelse> <cfset CurrTZ = "34"> </cfif></cfcase>
                                <cfcase value="HAWAII"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                <cfcase value="HAST"> <cfset GetTimeZoneInfoData = GetTimeZoneInfo() /> <cfif GetTimeZoneInfoData.isDSTOn>  <cfset CurrTZ = "34"> <cfelse> <cfset CurrTZ = "33"> </cfif></cfcase>
                                <cfcase value="ALASKA"><cfset CurrTZ = "32"></cfcase>
                                <cfcase value="PACIFIC"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="MOUNTAIN"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="CENTRAL"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="EASTERN"><cfset CurrTZ = "28"></cfcase>
                                <cfcase value="PDT"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="PST"><cfset CurrTZ = "31"></cfcase>
                                <cfcase value="MDT"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="MST"><cfset CurrTZ = "30"></cfcase>
                                <cfcase value="CDT"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="CST"><cfset CurrTZ = "29"></cfcase>
                                <cfcase value="EDT"><cfset CurrTZ = "28"></cfcase>
                                <cfcase value="EST"><cfset CurrTZ = "28"></cfcase>

                                <cfdefaultcase>
                                    <!--- DO lookup--->
                                    <cfset CurrTZ = "-1">
                                    
                                    <!--- Adjust for numers that start with 1 --->
						            <cfif LEFT(inpContactString, 1) EQ 1>
						                <cfset CurrNPA = MID(arguments.inpContactString, 2, 3)>
						                <cfset CurrNXX = MID(arguments.inpContactString, 5, 3)>
						                
						                <!--- Get rid of 1 when looking up in DB --->
						                <cfset arguments.inpContactString = RIGHT(inpContactString, LEN(arguments.inpContactString)-1)>
						            <cfelse>
						                <cfset CurrNPA = LEFT(arguments.inpContactString, 3) >
						                <cfset CurrNXX = MID(arguments.inpContactString, 4, 3)>
						            </cfif>

                                    <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                        SELECT
                                            CASE cx.T_Z
                                                  WHEN 14 THEN 37
                                                  WHEN 11 THEN 34
                                                  WHEN 10 THEN 33
                                                  WHEN 9 THEN 32
                                                  WHEN 8 THEN 31
                                                  WHEN 7 THEN 30
                                                  WHEN 6 THEN 29
                                                  WHEN 5 THEN 28
                                                  WHEN 4 THEN 27
                                                 END AS TimeZone,
                                            CASE
                                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                                  ELSE 0
                                                  END  AS CellFlag
                                        FROM
                                            MelissaData.FONE AS fx JOIN
                                            MelissaData.CNTY AS cx ON
                                            (fx.FIPS = cx.FIPS)
                                         WHERE
                                            fx.NPA = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNPA#">
                                        AND
                                        	fx.NXX = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrNXX#">
                                     </cfquery>

                                     <cfif GetTZInfo.RecordCount GT 0>
                                        <cfset CurrTZ = "#GetTZInfo.TimeZone#">
                                     <cfelse>
                                        <cfset CurrTZ = "-1">
                                     </cfif>

                                </cfdefaultcase>

                            </cfswitch>

                            <cfif CurrTZ EQ "-1" >
                                <!---<cfthrow MESSAGE="Could not find time zone and no valid default value provided. Possible inpTimeZone values are UNK,GUAM,SAMOA,HAWAII,ALASKA,PACIFIC,PDT,PST,MOUNTAIN,MDT,MST,CENTRAL,CDT,CST,EASTERN,EDT,EST" TYPE="Any" detail="" errorcode="-5">--->
                                <!--- For real time triggered SMS TimeZone is not required --->
                                <cfset CurrTZ = "31">
                            </cfif>

                        </cfif>

                    </cfif>
                       

					<!---	<cfset DebugStr = DebugStr & " URLData=#SerializeJSON(URL)#"> --->
					<cfset StructAppend(FORM, URL, true) />
					                       

					<cfif inpContactString NEQ "">

                        <cfset ServiceRequestdataout =  QueryNew("	UserId_int,
                                                                    ContactTypeId_int,
                                                                    ContactString_vch,
                                                                    TimeZone_int,
																	OperatorId_int                                                                   
                                                                     ", "VarChar,VarChar,VarChar,VarChar,VarChar")>

                        <cfset QueryAddRow(ServiceRequestdataout) />


                        <cfloop collection="#FORM#" item="whichField">

                           <!--- Only add fields not already on list of standard fields --->
                           <cfif NOT structKeyExists(ServiceRequestdataout, '#whichField#')  >

                                <!---<cfoutput>#whichField# = #FORM[whichField]#</cfoutput><br>--->

                                <cfset VariableNamesArray = ArrayNew(1)>
                                <cfset ArraySet(VariableNamesArray, 1, 1, "#FORM[whichField]#")>
                                <cfset QueryAddColumn(ServiceRequestdataout, whichField, "VarChar", VariableNamesArray)>

                            </cfif>

                        </cfloop>

                        <cfset QuerySetCell(ServiceRequestdataout, "UserId_int", "#Session.UserId#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "ContactString_vch", "#inpContactString#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "ContactTypeId_int", "#inpContactTypeId#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "TimeZone_int", "#CurrTZ#") />
                        <cfset QuerySetCell(ServiceRequestdataout, "OperatorId_int", "0") />
                     
                    </cfif>


               		<!--- Validate Schedule Active --->
               		<cfif inpValidateScheduleActive GT 0>

						<!--- Verify all numbers are actual numbers --->
                        <cfquery name="GetRawDataTZs" dbTYPE="query">
                            SELECT
                                DISTINCT TimeZone_int
                            FROM
                                ServiceRequestdataout
                        </cfquery>

                        <!---component="schedule"--->
                        <cfinvoke
                             method="GetModernSchedule"
                             returnvariable="RetValSchedule">
                                <cfinvokeargument name="inpBatchId" value="#NEXTBATCHID#"/>
                        </cfinvoke>

<!---
						<cfif RetValSchedule.RXRESULTCODE NEQ "1"> <!---// allows 24 hr operation--->
                        	<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Schedule Not Set For This Day" errorcode="-10">
                        </cfif>
--->
                        
                        <!--- set these in session--->
                        <cfset LocalGMTRelative = -8>
                        <cfset AfterHoursBlocked = 0>

                        <cfset CurrGMTRelative = -8>
                        <cfset LocalRelative = CurrGMTRelative - LocalGMTRelative>

                        <cfset CurrGMTRelative = 0>

                        <cfloop query="GetRawDataTZs">

							 <cfswitch expression="#GetRawDataTZs.TimeZone_int#" >
                             	<cfcase value="0"><cfset CurrGMTRelative = 0></cfcase>
                                <cfcase value="1"><cfset CurrGMTRelative = 0></cfcase>
                                <cfcase value="2"><cfset CurrGMTRelative = 1></cfcase>
                                <cfcase value="3"><cfset CurrGMTRelative = 2></cfcase>
                                <cfcase value="4"><cfset CurrGMTRelative = 3></cfcase>
                                <cfcase value="5"><cfset CurrGMTRelative = 3></cfcase>
                                <cfcase value="6"><cfset CurrGMTRelative = 4></cfcase>
                                <cfcase value="7"><cfset CurrGMTRelative = 4></cfcase>
                                <cfcase value="8"><cfset CurrGMTRelative = 5></cfcase>
                                <cfcase value="9"><cfset CurrGMTRelative = 5></cfcase>
                                <cfcase value="10"><cfset CurrGMTRelative = 6></cfcase>
                                <cfcase value="11"><cfset CurrGMTRelative = 6></cfcase>
                                <cfcase value="12"><cfset CurrGMTRelative = 7></cfcase>
                                <cfcase value="13"><cfset CurrGMTRelative = 8></cfcase>
                                <cfcase value="14"><cfset CurrGMTRelative = 9></cfcase>
                                <cfcase value="15"><cfset CurrGMTRelative = 9></cfcase>
                                <cfcase value="16"><cfset CurrGMTRelative = 10></cfcase>
                                <cfcase value="17"><cfset CurrGMTRelative = 10></cfcase>
                                <cfcase value="18"><cfset CurrGMTRelative = 11></cfcase>
                                <cfcase value="19"><cfset CurrGMTRelative = 11></cfcase>
                                <cfcase value="20"><cfset CurrGMTRelative = 12></cfcase>
                                <cfcase value="21"><cfset CurrGMTRelative = 13></cfcase>
                                <cfcase value="22"><cfset CurrGMTRelative = 14></cfcase>
                                <cfcase value="23"><cfset CurrGMTRelative = -1></cfcase>
                                <cfcase value="24"><cfset CurrGMTRelative = -2></cfcase>
                                <cfcase value="25"><cfset CurrGMTRelative = -3></cfcase>
                                <cfcase value="26"><cfset CurrGMTRelative = -3></cfcase>
                                <cfcase value="27"><cfset CurrGMTRelative = -4></cfcase>
                                <cfcase value="28"><cfset CurrGMTRelative = -5></cfcase>
                                <cfcase value="29"><cfset CurrGMTRelative = -6></cfcase>
                                <cfcase value="30"><cfset CurrGMTRelative = -7></cfcase>
                                <cfcase value="31"><cfset CurrGMTRelative = -8></cfcase>
                                <cfcase value="32"><cfset CurrGMTRelative = -9></cfcase>
                                <cfcase value="33"><cfset CurrGMTRelative = -10></cfcase>
                                <cfcase value="34"><cfset CurrGMTRelative = -11></cfcase>
                                <cfcase value="35"><cfset CurrGMTRelative = -12></cfcase>
                                <cfcase value="36"><cfset CurrGMTRelative = -13></cfcase>
                                <cfcase value="37"><cfset CurrGMTRelative = -14></cfcase>

                            </cfswitch>


							<cfset LocalRelative = CurrGMTRelative - LocalGMTRelative>

                            <cfset CurrTime = NOW()>

							<!---// do not go past midnight or start over--->
                            <cfif HOUR(CurrTime) + LocalRelative  LT 25><!---// Not past midnight--->

								<!---// Validate Start hour--->
                                <cfif RetValSchedule.STARTHOUR_TI GT 0> <!---// allows 24 hr operation--->
                                	<cfif HOUR(CurrTime) + LocalRelative LT RetValSchedule.STARTHOUR_TI>
    									<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<!---// allows blackout hours--->
								<cfif RetValSchedule.BLACKOUTSTARTHOUR_TI GT 0 AND RetValSchedule.BLACKOUTENDHOUR_TI GT 0 AND RetValSchedule.BLACKOUTSTARTHOUR_TI LT RetValSchedule.RetValSchedule.BLACKOUTENDHOUR_TI>
									<cfif  (HOUR(CurrTime) + LocalRelative) GTE RetValSchedule.BLACKOUTSTARTHOUR_TI AND (HOUR(CurrTime) + LocalRelative) LT RetValSchedule.BLACKOUTENDHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Blackout Period Set #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.BLACKOUTENDHOUR_TI#" errorcode="-10">
	                                </cfif>
                                </cfif>

    							<cfif RetValSchedule.ENDHOUR_TI LT 24> <!---// allows 24 hr operation--->
									<!---// Validate End Hour--->

										<cfif HOUR(CurrTime) + LocalRelative EQ RetValSchedule.ENDHOUR_TI AND ( RetValSchedule.ENDMINUTE_TI EQ 0 OR RetValSchedule.ENDMINUTE_TI LT 0 OR RetValSchedule.ENDMINUTE_TI GT 59 )>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>

										<cfif HOUR(CurrTime) + LocalRelative GT RetValSchedule.ENDHOUR_TI>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>
								</cfif>


								<!---// Check start minutes--->
								<cfif RetValSchedule.STARTMINUTE_TI GT 0 AND RetValSchedule.STARTMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative) EQ RetValSchedule.STARTHOUR_TI ) AND ( MINUTE(CurrTime) LT RetValSchedule.STARTMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<!---// Check end minutes--->
								<cfif RetValSchedule.ENDMINUTE_TI GT 0 AND RetValSchedule.ENDMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative) EQ RetValSchedule.ENDHOUR_TI ) AND ( MINUTE(CurrTime) GTE RetValSchedule.ENDMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>


								<cfif AfterHoursBlocked GT 0>
								<!---// Extra block to prevent after hours dialing--->

									<cfif HOUR(CurrTime) + LocalRelative LT 8>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early After Hours Blocked #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>

									<cfif HOUR(CurrTime) + LocalRelative GTE 21>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late After Hours Blocked #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>

								<!---// Extra block to prevent after hours dialing--->
                                </cfif>

							<!---// Not past midnight--->
							<cfelse>
							<!---// Past midnight--->
								<!---// Validate Start hour--->
								<cfif RetValSchedule.STARTHOUR_TI GT 0> <!---// allows 24 hr operation--->
									<cfif HOUR(CurrTime) + LocalRelative  - 24 LT RetValSchedule.STARTHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<cfif RetValSchedule.ENDHOUR_TI LT 24><!--- // allows 24 hr operation--->
									<!---// Validate End Hour--->

										<cfif HOUR(CurrTime) + LocalRelative - 24 EQ RetValSchedule.ENDHOUR_TI AND ( RetValSchedule.ENDMINUTE_TI EQ 0 OR RetValSchedule.ENDMINUTE_TI LT 0 OR RetValSchedule.ENDMINUTE_TI GT 59 )>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>

										<cfif HOUR(CurrTime) + LocalRelative - 24 GT RetValSchedule.ENDHOUR_TI>
											<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                        </cfif>
								</cfif>

									<!---// Check start minutes--->
								<cfif RetValSchedule.STARTMINUTE_TI GT 0 AND RetValSchedule.STARTMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative - 24) EQ RetValSchedule.STARTHOUR_TI ) AND ( MINUTE(CurrTime) LT RetValSchedule.STARTMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<!---// Check end minutes--->
								<cfif RetValSchedule.ENDMINUTE_TI GT 0 AND RetValSchedule.ENDMINUTE_TI LT 60>
									<cfif  ( (HOUR(CurrTime) + LocalRelative - 24) EQ RetValSchedule.ENDHOUR_TI ) AND ( MINUTE(CurrTime) GTE RetValSchedule.ENDMINUTE_TI ) >
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<!---// allows blackout hours--->
								<cfif RetValSchedule.BLACKOUTSTARTHOUR_TI GT 0 AND RetValSchedule.BLACKOUTENDHOUR_TI GT 0 AND RetValSchedule.BLACKOUTSTARTHOUR_TI LT RetValSchedule.BLACKOUTENDHOUR_TI>
									<cfif  (HOUR(CurrTime) + LocalRelative - 24) GTE RetValSchedule.BLACKOUTSTARTHOUR_TI AND (HOUR(CurrTime) + LocalRelative - 24) LT RetValSchedule.BLACKOUTENDHOUR_TI>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Blackout Period Set #RetValSchedule.BLACKOUTSTARTHOUR_TI# #RetValSchedule.BLACKOUTENDHOUR_TI#" errorcode="-10">
                                    </cfif>
                                </cfif>

								<cfif AfterHoursBlocked GT 0>
								<!---// Extra block top prevent after hours dialing--->

									<cfif HOUR(CurrTime) + LocalRelative - 24 LT 8>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Early After Hours Blocked #RetValSchedule.STARTHOUR_TI# #RetValSchedule.STARTMINUTE_TI#" errorcode="-10">
									</cfif>

									<cfif HOUR(CurrTime) + LocalRelative - 24 GTE 21>
										<cfthrow MESSAGE="Schedule Violation" TYPE="Any" detail="Too Late After Hours Blocked #RetValSchedule.ENDHOUR_TI# #RetValSchedule.ENDMINUTE_TI#" errorcode="-10">
									</cfif>

								</cfif><!---// Extra block top prevent after hours dialing--->


                            </cfif><!---// Past midnight--->

                        </cfloop>

					</cfif>


              		<cfset DebugStr = DebugStr & " GetRawDataCount">


   					<!--- Verify all numbers are actual numbers --->
		            <cfquery name="GetRawDataCount" dbTYPE="query">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            ServiceRequestdataout
                    </cfquery>

                    <cfif GetRawDataCount.TOTALCOUNT EQ "">

         	           <cfthrow MESSAGE="Distribution Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) No data elligable to send - check your contact strings." errorcode="-5">

                    </cfif>

                   
                   <!--- Old billing logic - now handled in SMS directly
                   
                    <cfset var TotalUnitCost = GetRawDataCount.TOTALCOUNT/>

                    <!--- Voice Credit surcharge --->
                    <cfif inpContactTypeId EQ 1>
                    	<cfset TotalUnitCost = TotalUnitCost * 3 />
                    </cfif>

                    <!--- SMS Credit surcharge --->
                    <cfif inpContactTypeId EQ 3>
                    	<cfset TotalUnitCost = TotalUnitCost * 2 />
                    </cfif>

                    <!--- Make sure it is ok with billing - only charge when dial complete --->
                    <!---component="billing"--->
                    <cfinvoke
                     method="ValidateBilling"
                     returnvariable="local.RetValBillingData">
                        <cfinvokeargument name="inpCount" value="#TotalUnitCost#"/>
                     	<cfinvokeargument name="inpUserId" value="#CurrUserId#"/>
                        <cfinvokeargument name="inpMMLS" value="#inpMMLS#"/>
                    </cfinvoke>

                    <cfif RetValBillingData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) #RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">
                    </cfif>

                    <!--- This is inserted into Queue  --->
                    <cfset EstimatedCost = RetValBillingData.ESTIMATEDCOSTPERUNIT>
                   
                   --->
                   
                    
                    
                    <cfset EstimatedCost = 0>


                    <!--- Changes which data source to process for each channel--->
                    <cfset ServiceInputFlag = 1>



                    <!--- Load current user's email sub account information if any --->
                    <cfif ListContains(inpContactTypeId, 2)>

                        <cfinvoke
                        	method="GetCurrentUserEmailSubaccountInfo"
                         	returnvariable="RetValGetCurrentUserEmailSubaccountInfo">
                            <cfinvokeargument name="inpUserId" value="#CurrUserId#"/>
                        </cfinvoke>

                        <cfset eMailSubUserAccountName = "#RetValGetCurrentUserEmailSubaccountInfo.USERNAME#" />
                        <cfset eMailSubUserAccountPassword = "#RetValGetCurrentUserEmailSubaccountInfo.PASSWORD#" />
                        <cfset eMailSubUserAccountRemoteAddress = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEADDRESS#" />
                        <cfset eMailSubUserAccountRemotePort = "#RetValGetCurrentUserEmailSubaccountInfo.REMOTEPORT#" />
                        <cfset eMailSubUserAccountTLSFlag = "#RetValGetCurrentUserEmailSubaccountInfo.TLSFLAG#" />
                        <cfset eMailSubUserAccountSSLFlag = "#RetValGetCurrentUserEmailSubaccountInfo.SSLFLAG#" />

					</cfif>


                    <!--- Insert Voice, eMAil, SMS into dial queue--->



                    <!---           <cfthrow MESSAGE="Debug Error" TYPE="Any" detail="Made It Here" errorcode="-5">         --->

                    <cfset DebugStr = DebugStr & " Load Start ">

					<!--- SMS only so far--->	
					<cfset inpContactTypeId = 3 />
                    <cfinclude template="../../../../../session/cfc/Includes/LoadQueueSMS_Dynamic.cfm">

                   
                    <cfset DebugStr = DebugStr & " Load Finished">

					<cfset dataout = StructNew() />   
					<cfset dataout.STATUSCODE = "1" />
                    <cfset dataout.inpContactString = "#inpContactString#" />
                    <cfset dataout.inpBatchId = "#inpBatchId#" />
                    <cfset dataout.inpTimeZone = "#inpTimeZone#" />
                    <cfset dataout.CURRTZ = "#CURRTZ#" />
                    <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
                    <cfset dataout.REQUESTUUID = "#NEWUUID#" />
                    <cfset dataout.SMSINIT = "#SMSINIT#" />
                    <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
                    <cfset dataout.SMSDEST = "#inpShortCode#" />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.ERRMESSAGE = "" />
                  
                    <cfif DebugAPI GT 0>
                        <cfset dataout.MESSAGE = "#SMSINITMESSAGE# DebugAPI=#DebugAPI# #DebugStr#" />					
                    <cfelse>
                        <cfset dataout.MESSAGE = "#SMSINITMESSAGE#" />             
                    </cfif>
                

            <cfcatch TYPE="any">

                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

				<!---<cfset dataout =  QueryNew("STATUSCODE, inpContactString, inpBatchId, inpTimeZone, CURRTZ, DISTRIBUTIONID, REQUESTUUID, BLOCKEDBYDNC, SMSINIT, SMSDEST, TYPE, MESSAGE, ERRMESSAGE")>--->
                <cfset dataout = StructNew() />            
                
                <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
                <cfset dataout.inpContactString = "#inpContactString#" />
                <cfset dataout.inpBatchId = "#inpBatchId#" />
                <cfset dataout.inpTimeZone = "#inpTimeZone#" />
                <cfset dataout.CURRTZ = "#CURRTZ#" />
                <cfset dataout.DISTRIBUTIONID = "#LASTQUEUEDUPID#" />
                <cfset dataout.REQUESTUUID = "#NEWUUID#" />
                <cfset dataout.SMSINIT = "0" />
                <cfset dataout.BLOCKEDBYDNC = "#UserLocalDNCBlocked#" />
                <cfset dataout.SMSDEST = "#inpShortCode#" />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
              
                <cfif DebugAPI GT 0>
	                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />					
                <cfelse>
	                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
                </cfif>

            </cfcatch>

            </cftry>

		</cfoutput>

        <!--- Log all API call outputs for debugging purposes --->
        <cftry>

            <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SimpleX API Result - pdc-AddToRealTime">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="pdc-AddToRealTime Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>

        <cfcatch type="any">


        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


       <!--- <cfset var retval = StructNew()>

       	<cfoutput>

            <cftry>

				<cfset retval = StructNew()>
                <cfset retval.STATUSCODE = 1>
                <cfset retval.MESSAGE = "OK">
                <cfset retval.DESC = "Hello">

                <cfreturn representationOf(retval).withStatus(200) />

            <cfcatch>
				<cfset retval = StructNew()>
				<cfset retval.STATUSCODE = -1>
                <cfset retval.MESSAGE = "NOT OK">
                <cfset retval.DESC = "'Doh">
                <cfreturn representationOf(retval).withStatus(401, '#cfcatch.Message#') />
            </cfcatch>
            </cftry>

		</cfoutput>--->

    </cffunction>
    
    
    <cffunction name="GetModernSchedule" output="false" hint="Get Modern Schedule" access="public">
		<cfargument name="inpBatchId" TYPE="string" default="0" required="yes"/>
        
		<cfset var schedules = ArrayNew(1) />
		<cfset var i = 0/>
		<cfset var schedule = StructNew() />
		<cfset var DYNAMICSCHEDULEDAYOFWEEK_TI = 0 />
		<cfset var START_DT = now() />
		<cfset var GetSchedule = '' />
		
		
		<cfset var DataOut = StructNew()/>
		
		<cfset DataOut.RXRESULTCODE = -1>
		<cfset DataOut.INPBATCHID = "#INPBATCHID#">
		<cfset DataOut.TYPE = "">
		<cfset DataOut.MESSAGE = "">
		<cfset DataOut.DYNAMICSCHEDULEDAYOFWEEK_TI = "0">
		<cfset DataOut.STARTHOUR_TI = "0">
		<cfset DataOut.ENDHOUR_TI = "0">
		<cfset DataOut.SUNDAY_TI = "0">
		<cfset DataOut.MONDAY_TI = "0">
		<cfset DataOut.TUESDAY_TI = "0">
		<cfset DataOut.WEDNESDAY_TI = "0">
		<cfset DataOut.THURSDAY_TI = "0">
		<cfset DataOut.FRIDAY_TI = "0">
		<cfset DataOut.SATURDAY_TI = "0">
		<cfset DataOut.LOOPLIMIT_INT = "100">
		<cfset DataOut.STOP_DT = "2001-01-01">
		<cfset DataOut.START_DT = "2001-01-01">
		<cfset DataOut.STARTMINUTE_TI = "0">
		<cfset DataOut.ENDMINUTE_TI = "0">
		<cfset DataOut.BLACKOUTSTARTHOUR_TI = "0">
		<cfset DataOut.BLACKOUTENDHOUR_TI = "0">
		<cfset DataOut.LASTUPDATED_DT = "2001-01-01">
		             
       	<cftry>
                  
				<cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">                        
					SELECT 
						DYNAMICSCHEDULEDAYOFWEEK_TI,
		                STARTHOUR_TI,
		                ENDHOUR_TI, 
		                SUNDAY_TI,
		                MONDAY_TI,
		                TUESDAY_TI,
		                WEDNESDAY_TI,
		                THURSDAY_TI,
		                FRIDAY_TI,
		                SATURDAY_TI,
		                LOOPLIMIT_INT,
		                STOP_DT,
		                START_DT,
		                STARTMINUTE_TI,
		                ENDMINUTE_TI,
		                BLACKOUTSTARTHOUR_TI,
		                BLACKOUTENDHOUR_TI,		                
		                LASTUPDATED_DT
                	FROM
                		simpleobjects.scheduleoptions
                	WHERE 
	                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
	                AND
	                	DYNAMICSCHEDULEDAYOFWEEK_TI = 0	                	    
				</cfquery>     
								
				<cfif GetSchedule.RecordCount GT 0 >
				
					<cfset DataOut.DYNAMICSCHEDULEDAYOFWEEK_TI = "#GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI#" />
					<cfset DataOut.STARTHOUR_TI = "#GetSchedule.STARTHOUR_TI#" />
					<cfset DataOut.ENDHOUR_TI = "#GetSchedule.ENDHOUR_TI#" />
					<cfset DataOut.SUNDAY_TI = "#GetSchedule.SUNDAY_TI#" />
					<cfset DataOut.MONDAY_TI = "#GetSchedule.MONDAY_TI#" />
					<cfset DataOut.TUESDAY_TI = "#GetSchedule.TUESDAY_TI#" />
					<cfset DataOut.WEDNESDAY_TI = "#GetSchedule.WEDNESDAY_TI#" />
					<cfset DataOut.THURSDAY_TI = "#GetSchedule.THURSDAY_TI#" />
					<cfset DataOut.FRIDAY_TI = "#GetSchedule.FRIDAY_TI#" />
					<cfset DataOut.SATURDAY_TI = "#GetSchedule.SATURDAY_TI#" />
					<cfset DataOut.LOOPLIMIT_INT = "#GetSchedule.LOOPLIMIT_INT#" />
					<cfset DataOut.STOP_DT =  "#LSDateFormat(GetSchedule.STOP_DT,  'm-d-yyyy')#" />
					<cfset DataOut.START_DT = "#LSDateFormat(GetSchedule.START_DT, 'm-d-yyyy')#" />	
					<cfset DataOut.STARTMINUTE_TI = "#GetSchedule.STARTMINUTE_TI#" />
					<cfset DataOut.ENDMINUTE_TI = "#GetSchedule.ENDMINUTE_TI#" />
					<cfset DataOut.BLACKOUTSTARTHOUR_TI = "#GetSchedule.BLACKOUTSTARTHOUR_TI#" />
					<cfset DataOut.BLACKOUTENDHOUR_TI = "#GetSchedule.BLACKOUTENDHOUR_TI#" />
					<cfset DataOut.LASTUPDATED_DT = "#LSDateFormat(GetSchedule.LASTUPDATED_DT, 'm-d-yyyy')#" />		
					
					<!--- Schedule found OK --->	     	
					<cfset DataOut.RXRESULTCODE = 1>
				
				<cfelse>			
					
			
					<!--- Look for dynamic schedule for today's day 1-7 Sun-Sat --->
					<cfquery name="GetSchedule" datasource="#Session.DBSourceEBM#">                        
						SELECT 
							DYNAMICSCHEDULEDAYOFWEEK_TI,
			                STARTHOUR_TI,
			                ENDHOUR_TI, 
			                SUNDAY_TI,
			                MONDAY_TI,
			                TUESDAY_TI,
			                WEDNESDAY_TI,
			                THURSDAY_TI,
			                FRIDAY_TI,
			                SATURDAY_TI,
			                LOOPLIMIT_INT,
			                STOP_DT,
			                START_DT,
			                STARTMINUTE_TI,
			                ENDMINUTE_TI,
			                BLACKOUTSTARTHOUR_TI,
			                BLACKOUTENDHOUR_TI,		                
			                LASTUPDATED_DT
	                	FROM
	                		simpleobjects.scheduleoptions
	                	WHERE 
		                    BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpBatchId#">
		                AND
		                	DYNAMICSCHEDULEDAYOFWEEK_TI = #DayOfWeek(NOW())#	                	    
					</cfquery>  
					
					<cfif GetSchedule.RecordCount GT 0 >
					
						<cfset DataOut.DYNAMICSCHEDULEDAYOFWEEK_TI = "#GetSchedule.DYNAMICSCHEDULEDAYOFWEEK_TI#" />
						<cfset DataOut.STARTHOUR_TI = "#GetSchedule.STARTHOUR_TI#" />
						<cfset DataOut.ENDHOUR_TI = "#GetSchedule.ENDHOUR_TI#" />
						<cfset DataOut.SUNDAY_TI = "#GetSchedule.SUNDAY_TI#" />
						<cfset DataOut.MONDAY_TI = "#GetSchedule.MONDAY_TI#" />
						<cfset DataOut.TUESDAY_TI = "#GetSchedule.TUESDAY_TI#" />
						<cfset DataOut.WEDNESDAY_TI = "#GetSchedule.WEDNESDAY_TI#" />
						<cfset DataOut.THURSDAY_TI = "#GetSchedule.THURSDAY_TI#" />
						<cfset DataOut.FRIDAY_TI = "#GetSchedule.FRIDAY_TI#" />
						<cfset DataOut.SATURDAY_TI = "#GetSchedule.SATURDAY_TI#" />
						<cfset DataOut.LOOPLIMIT_INT = "#GetSchedule.LOOPLIMIT_INT#" />
						<cfset DataOut.STOP_DT =  "#LSDateFormat(GetSchedule.STOP_DT,  'm-d-yyyy')#" />
						<cfset DataOut.START_DT = "#LSDateFormat(GetSchedule.START_DT, 'm-d-yyyy')#" />	
						<cfset DataOut.STARTMINUTE_TI = "#GetSchedule.STARTMINUTE_TI#" />
						<cfset DataOut.ENDMINUTE_TI = "#GetSchedule.ENDMINUTE_TI#" />
						<cfset DataOut.BLACKOUTSTARTHOUR_TI = "#GetSchedule.BLACKOUTSTARTHOUR_TI#" />
						<cfset DataOut.BLACKOUTENDHOUR_TI = "#GetSchedule.BLACKOUTENDHOUR_TI#" />
						<cfset DataOut.LASTUPDATED_DT = "#LSDateFormat(GetSchedule.LASTUPDATED_DT, 'm-d-yyyy')#" />
						
						<!--- Schedule found OK --->	     	
						<cfset DataOut.RXRESULTCODE = 1>
					
					</cfif>					
					
				</cfif>	
			
			                           
		<cfcatch TYPE="any">
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -1 />
			</cfif>
			
			<cfset DataOut.RXRESULTCODE = cfcatch.errorcode>
			<cfset DataOut.TYPE = "#cfcatch.TYPE#">
			<cfset DataOut.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset DataOut.ERRMESSAGE = "#cfcatch.detail# DebugStr=#DebugStr#"> 
		</cfcatch>
		</cftry>    
		 
        <cfreturn DataOut />
	</cffunction>
	

</cfcomponent>