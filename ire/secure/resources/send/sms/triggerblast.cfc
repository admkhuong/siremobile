<cfcomponent extends="taffy.core.resource" taffy_uri="/triggerBlast" output="false">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	<cfinclude template="../../../paths.cfm" >

<!--- 	<cfinclude template="../../../../../session/cfc/csc/constants.cfm"> --->

	<!--- Overrides remote component request so method can be called locally - helps to eliminates cross session varibale scope issues --->
	<cfset DeliveryServiceComponentPath = "">
    <cfset DeliveryServiceMCIDDMsComponentPath="">


   	<!--- ************************************************************************************************************************* --->
    <!--- Blast a list or lists to a BatchId - auto queued and processed asyncronously --->
    <!--- ************************************************************************************************************************* --->

 	<cffunction name="post" access="public" output="false" hint="Send a list (or comma separated lists) of subscribers a message campaign.">
 		<cfargument name="inpBatchId" required="true" default="0" hint="The pre-defined campaign Id you created under your account.">
 		<cfargument name="inpGroupId" required="true" default="0" hint="The pre-defined subscriber list Id from your account OR Optionally a CSV list of List Ids">
 		<cfargument name="inpClone" required="false" default="1" hint="Default will create a new Batch (Clone) for the blast - Clone based on on passed in inpBatchId. This allows seperate tracking and multiple reuse of given campaigns. Subject to inpMinBR limitations">
 		<cfargument name="inpMinBR" required="false" default="60" hint="Business rule - block new campaign blasts if duplicate in last (inpMinBR) minutes. Default is 60 minutes. Shit happens. When you automate systems, shit happens automatically.">
 		<cfargument name="inpSchedule" TYPE="string" required="false" default="" hint="A JSON string of schedule options you wish to override. Each variable name must match expected list of possibilities. See docs for further info." />
 		<cfargument name="inpShortCode" TYPE="string" required="false" hint="Shortcode to send this from. User must have access to specified short code or default will be used intsead." default="" /> 
 		<cfargument name="inpDesc" required="false" default="" hint="Name of the blast - cloned campaing option only - Limit 255 character max">
 		<cfargument name="DebugAPI" required="no" default="0">
 		 		
 		<!---  Any extra fields will need to be on URL or FORM scope - these will be auto appended to the inpAPIRequestJSON inside the method="BlastToList" --->
 				 		
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var RetVarNewBlastToList = '' />
                     
		<cfset var DebugStr = "" />
        <!--- Info: The Sesioon.UserId is set based on the authticated user by the custom API authentication method --->
        
        <!--- Set the default Session DB Source --->
        <cfset Session.DBSourceEBM = "Bishop"/>
        <cfset Session.COMPANYUSERID = "Session.UserId"/>
        
        
        
        
                
 		<cftry>
 		 				 		
 			    
			<!---Call centralized blast logic --->
			<cfinvoke method="BlastToList" component="session.sire.models.cfc.control-point" returnvariable="RetVarNewBlastToList">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpClone" value="#inpClone#">
				<cfinvokeargument name="inpSkipDNCCheck" value="0">
				<cfinvokeargument name="inpSchedule" value="#inpSchedule#">
				<cfinvokeargument name="inpCDFFilter" value=""/>
				<cfinvokeargument name="inpMinBR" value="#inpMinBR#"/>
				<cfinvokeargument name="inpContactFilter" value=""/>
				<cfinvokeargument name="inpDesc" value="#inpDesc#">				
			</cfinvoke> 
						
 			 			
 			<!--- Check response for error and stats - return results to API user --->
 						 			
 			
 			<!--- Log API request --->
 			<cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="Sire API Result - Blast Trigger">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="Blast Trigger Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
            
            
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.DETAIL = RetVarNewBlastToList />
             
			
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>

			<!---<cfset dataout =  QueryNew("STATUSCODE, inpContactString, inpBatchId, inpTimeZone, CURRTZ, DISTRIBUTIONID, REQUESTUUID, BLOCKEDBYDNC, SMSINIT, SMSDEST, TYPE, MESSAGE, ERRMESSAGE")>--->
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.BATCHID = "#inpBatchId#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>

    <cffunction name="GET" access="public" output="false" hint="Testing - Returns DESC=Hello! and Message=OK">		
       	<cfargument name="inpBatchId" required="true" default="0" hint="The pre-defined campaign Id you created under your account.">
 		<cfargument name="inpGroupId" required="true" default="0" hint="The pre-defined subscriber list Id from your account OR Optionally a CSV list of List Ids">
 		<cfargument name="inpClone" required="false" default="1" hint="Default will create a new Batch (Clone) for the blast - Clone based on on passed in inpBatchId. This allows seperate tracking and multiple reuse of given campaigns. Subject to inpMinBR limitations">
 		<cfargument name="inpMinBR" required="false" default="60" hint="Business rule - block new campaign blasts if duplicate in last (inpMinBR) minutes. Default is 60 minutes. Shit happens. When you automate systems, shit happens automatically.">
 		<cfargument name="inpSchedule" TYPE="string" required="false" default="" hint="A JSON string of schedule options you wish to override. Each variable name must match expected list of possibilities. See docs for further info." />
 		<cfargument name="inpShortCode" TYPE="string" required="false" hint="Shortcode to send this from. User must have access to specified short code or default will be used intsead." default="" /> 
 		<cfargument name="inpDesc" required="false" default="" hint="Name of the blast - cloned campaing option only - Limit 255 character max">
 		<cfargument name="DebugAPI" required="no" default="0">
 		 		
 		<!---  Any extra fields will need to be on URL or FORM scope - these will be auto appended to the inpAPIRequestJSON inside the method="BlastToList" --->
 		<cfset var dataOut = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.DETAIL = "" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        
        <cfset var RetVarNewBlastToList = '' />
                     
		<cfset var DebugStr = "" />
        <!--- Info: The Sesioon.UserId is set based on the authticated user by the custom API authentication method --->
        
        <!--- Set the default Session DB Source --->
        <cfset Session.DBSourceEBM = "Bishop"/>
        <cfset Session.COMPANYUSERID = "Session.UserId"/>
              
                
 		<cftry>
 		 				 		
 			    
			<!---Call centralized blast logic --->
			<cfinvoke method="BlastToList" component="session.sire.models.cfc.control-point" returnvariable="RetVarNewBlastToList">
				<cfinvokeargument name="inpBatchId" value="#inpBatchId#">
				<cfinvokeargument name="inpGroupId" value="#inpGroupId#">
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#">
				<cfinvokeargument name="inpClone" value="#inpClone#">
				<cfinvokeargument name="inpSkipDNCCheck" value="0">
				<cfinvokeargument name="inpSchedule" value="#inpSchedule#">
				<cfinvokeargument name="inpCDFFilter" value=""/>
				<cfinvokeargument name="inpMinBR" value="#inpMinBR#"/>
				<cfinvokeargument name="inpContactFilter" value=""/>
				<cfinvokeargument name="inpDesc" value="#inpDesc#">				
			</cfinvoke> 
						
 			 			
 			<!--- Check response for error and stats - return results to API user --->
 						 			
 			
 			<!--- Log API request --->
 			<cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    1114,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="Sire API Result - Blast Trigger">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="Blast Trigger Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#serializeJSON(dataout)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#DebugStr#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)# #SerializeJSON(FORM)#">
                )
            </cfquery>
            
            
            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.DETAIL = RetVarNewBlastToList />
             
			
        <cfcatch type="any">

			<cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>

			<!---<cfset dataout =  QueryNew("STATUSCODE, inpContactString, inpBatchId, inpTimeZone, CURRTZ, DISTRIBUTIONID, REQUESTUUID, BLOCKEDBYDNC, SMSINIT, SMSDEST, TYPE, MESSAGE, ERRMESSAGE")>--->
            <cfset dataout = StructNew() />            
            
            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.BATCHID = "#inpBatchId#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />
          
            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />					
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />             
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />

    </cffunction>
 
    
 
</cfcomponent>