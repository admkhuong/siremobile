<cfcomponent extends="taffy.core.resource" taffy:uri="/twitter/follow" hint="Follow Input User for Input ShortCode - Authenticated user must have provisioned rights to the twitter short code specified.">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
    <cfset CommonDotPath = "ebmresponse.sms.common"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://ebmii.messagebroadcast.com/ebmresponse/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="JSONDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
    
    
    <!--- 
		*** JLP - Dont kill the DB looking this up but....
		Option to ignore MO if same as last MO AND no new MT sent since duplicate MO 
		
	--->
    
    
	<!---Twitter DM only support POST method, with only JSONDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Follow User">
		<cfargument name="inpTwitterScreen_Name" TYPE="string" required="yes" hint="Screen name of the user to follow" />
        <cfargument name="inpTwitterUserID" TYPE="string" required="yes" hint="User Id of the user to follow" />
        <cfargument name="inpShortCode" TYPE="string" required="yes" hint="Short code with provisioning info to allow post to twitter" />
        <cfargument name="inpTransactionId" TYPE="string" required="no" default=""/>
        <cfargument name="JSONDATA" TYPE="string" required="no" default=""/>
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataout = StructNew() />  
		<cfset var TwitterDMData = '' />
		<cfset var contactString = '' />
		<cfset var shortCode = '' />
		<cfset var JSONDATAString = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetValProcessNextResponseSMS = '' />
        <cfset var RetValCheckForDuplicateMO = '' />
        <cfset var RetValInsertMessagePart = ''>
        <cfset var DBSourceEBM = "Bishop"/> 
        <cfset var GetShortCodeInternal = "" />
        <cfset var TwitterParamsStruct = "" />
        <cfset var oauthSigMethodSHA = "" />
        <cfset var oauthRequestCFC = "" />
        <cfset var oauthConsumerCFC = "" />
        <cfset var oauthTokenCFC = "" />
        <cfset var oTwitterConsumer = "" />
        <cfset var oTwitterAccessToken = "" />
        <cfset var oTwitterReqest = "" />
        <cfset var smsreturn =  StructNew() />
       
        
        <cfset var DebugStr = "Go Debug" />
        
        
        <cfset dataout.RXRESULTCODE = 0/>
        <cfset dataout.POSTRESULT = "NA" />
          
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>


		<cftry>
           
			            
			<!--- For debugging only - comment out in production --->  
            <!---
                <cfmail to="jpeterson@messagebroadcast.com;" from="it@messagebroadcast.com" subject="AT&T MO Received" type="html">
                    <cfdump var="#JSONDATA#">
                    <cfdump var="#TwitterDMData#">               
                </cfmail> 
            --->
            <!--- <cfreturn representationOf(dataout).withStatus(200) />--->
                        
                        
           	<!--- Verify user has the right to access short code - throw error if not --->             
                    
            
            <!--- Lookup Short Code --->
            <!--- Validate this is not a duplicate ... --->
            <cfinvoke                    
                 method="GetShortCodeInternal"
                 returnvariable="RetValGetShortCodeInternal"> 
                    <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
            </cfinvoke>  
            
            
            <!---
            
            https://dev.twitter.com/rest/reference/post/friendships/create
            
            https://api.twitter.com/1.1/friendships/create.json?user_id=1401881&follow=true
            
             --->
                
             <!--- Validate has rights and exists--->   
             <cfif RetValGetShortCodeInternal.RXRESULTCODE EQ 1>               
                         
                <!--- Validate keys are set --->
                <cfif RetValGetShortCodeInternal.CUSTOMSERVICEID1 NEQ "" AND RetValGetShortCodeInternal.CUSTOMSERVICEID2 NEQ "" AND RetValGetShortCodeInternal.CUSTOMSERVICEID3 NEQ "" AND RetValGetShortCodeInternal.CUSTOMSERVICEID4 NEQ "">
                
                    <cfscript>
						
						oauthSigMethodSHA = CreateObject("component", "#CommonDotPath#.OAuth.oauthsignaturemethod_hmac_sha1");
						oauthRequestCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthrequest");
						oauthConsumerCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthconsumer");
						oauthTokenCFC = CreateObject("component", "#CommonDotPath#.OAuth.oauthtoken");
                    
                        oTwitterConsumer = oauthConsumerCFC.init(sKey = RetValGetShortCodeInternal.CUSTOMSERVICEID1, sSecret = RetValGetShortCodeInternal.CUSTOMSERVICEID2);
                    
                        TwitterParamsStruct = StructNew();
                       
                        TwitterParamsStruct['screen_name'] = inpTwitterScreen_Name;
                        TwitterParamsStruct['user_id'] = inpTwitterUserID; 
						TwitterParamsStruct['follow'] = "true"; 
                    
                        oTwitterAccessToken = oauthTokenCFC.init(sKey = RetValGetShortCodeInternal.CUSTOMSERVICEID3, sSecret = RetValGetShortCodeInternal.CUSTOMSERVICEID4);
                    
                        //create request
                        oTwitterReqest = oauthRequestCFC.fromConsumerAndToken(
                            oConsumer   : oTwitterConsumer,
                            oToken      : oTwitterAccessToken,
                            sHttpMethod : "POST",
                            sHttpURL    : 'https://api.twitter.com/1.1/friendships/create.json',
                            stParameters: TwitterParamsStruct
                        );
                    
                        //sign request
                        oTwitterReqest.signRequest(
                            oSignatureMethod    : oauthSigMethodSHA,
                            oConsumer           : oTwitterConsumer,
                            oToken              : oTwitterAccessToken
                        );
                    </cfscript>
                    
                    
                    <cfset dataout.oTwitterReqest = oTwitterReqest.getString()/>  
                    
                    <cfhttp method="POST" url="#oTwitterReqest.getString()#" result="smsreturn" throwonerror="yes" charset="utf-8" redirect="no">
                        <cfhttpparam type="header" name="screen_name" value="#inpTwitterScreen_Name#"> <!--- Only here because cfhttp POST REQUIRES at least one cfhttpparam - the actual data is on the URL of the REST request --->
                    </cfhttp>
        
        
			        <cfset dataout.RXRESULTCODE = 1/>
                    <cfset dataout.POSTRESULT = smsreturn.FileContent />
                    <!---<cfset dataout.USERID = Session.UserId/>--->
        
                <cfelse><!--- Validate keys are set --->
                
	                <cfset dataout.RXRESULTCODE = -2/>               
                	<cfset dataout.TYPE = "ERROR" />
                	<cfset dataout.MESSAGE = "Keys not defined" />
                	<cfset dataout.ERRMESSAGE = "No keys defined for this Short Code" />                 
                
                </cfif><!--- Validate keys are set --->
            
        	
            <cfelse><!--- Validate has rights and exists--->  
            
	            <cfset dataout.RXRESULTCODE = -3/>
                <cfset dataout.TYPE = "ERROR" />
                <cfset dataout.MESSAGE = "Unable to access ShortCode Data" />
                <cfset dataout.ERRMESSAGE = "#RetValGetShortCodeInternal.RXRESULTCODE# #RetValGetShortCodeInternal.MESSAGE# #RetValGetShortCodeInternal.ERRMESSAGE#" />            
            
            </cfif><!--- Validate has rights and exists--->   
				                    
        <cfcatch>
        
		        <cfset dataout.RXRESULTCODE = -1/>
                <cfset dataout.TYPE = cfcatch.Type />
                <cfset dataout.MESSAGE =  "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM# inpTwitterScreen_Name=#inpTwitterScreen_Name# inpTwitterUserID=#inpTwitterUserID#" />
                <cfset dataout.ERRMESSAGE = cfcatch.Detail />   
                        
                <cftry>        
                
                	<cfset JSONDATAString = URLDecode(JSONDATA)>
					<cfset ENA_Message = "JSONDATA = (#JSONDATA#) JSONDATAString=(#JSONDATAString#)">
                    <cfset SubjectLine = "SimpleX API Notification - /responsetwitterdm/follow">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
        
                            
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>


