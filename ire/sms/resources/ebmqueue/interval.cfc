<cfcomponent extends="taffy.core.resource" taffy:uri="/ebmqueue/it" hint="Scheduled Event in Queue Fulfillment - IT=Interval Trigger">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
    <cfset CommonDotPath = "ire.sms.common"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://mo.internal.xxx.com/ire/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="JSONDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
    
           
    
	<!---Twitter DM only support POST method, with only JSONDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Scheduled Event in Queue Fulfillment - Make rocket go now!">
		<cfargument name="inpCarrierId" TYPE="string" required="yes" />
        <cfargument name="inpmoInboundQueueId" TYPE="string" required="yes" />
        <cfargument name="inpContactString" TYPE="string" required="yes" />
        <cfargument name="inpShortCode" TYPE="string" required="yes" />
        <cfargument name="inpKeyword" TYPE="string" required="yes" />
        <cfargument name="inpTransactionId" TYPE="string" required="yes" />
        <cfargument name="inpTime" TYPE="string" required="yes" />
        <cfargument name="inpCustomServiceId1" TYPE="string" required="yes" />
        <cfargument name="inpRawData" TYPE="string" required="yes" />
        <cfargument name="inpQuestionTimeOutQID" TYPE="string" required="yes" />
        <cfargument name="inpPreferredAggregator" TYPE="string" required="yes" />
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
        <cfargument name="VerboseDebug" TYPE="string" required="no" default="0"/>
        
		<cfset var dataout = '0' />  
        <cfset var RetValUpdateQueueNextResponseSMSStatus = ''/>
        <cfset var RetValProcessNextResponseSMS = '' />
        <cfset var RetValUpdateQueueNextResponseSMSStatus = '' />       
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var DBSourceEBM = "Bishop"/> 
        <cfset var DebugStr = "Go Debug" />  
        <cfset var tickBegin = ""/>    
        <cfset var tickEnd = ""/>
        <cfset var InitLockQueueIdTime = ""/>    
        <cfset var NextResponseTime = ""/>    
        <cfset var FinishQueueIdTime = ""/>   
        
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so includes have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>

		<cftry>
                       
            <!--- For debugging only - comment out in production --->  
          	<!---
				<cfmail to="jpeterson@messagebroadcast.com;" from="it@messagebroadcast.com" subject="AT&T MO Received" type="html">
					<cfdump var="#JSONDATA#">
					<cfdump var="#TwitterDMData#">               
				</cfmail> 
			--->
           	<!--- <cfreturn representationOf(dataout).withStatus(200) />--->
            
            <cfset tickBegin = GetTickCount() />
            			
		    <!--- Update to processing --->        
            <cfinvoke                    
                 method="UpdateQueueNextResponseSMSStatus"
                 returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                    <cfinvokeargument name="inpQueueId" value="#inpmoInboundQueueId#"/>
                    <cfinvokeargument name="inpQueueState" value="#SMSQCODE_INPROCESS#"/>                                            
            </cfinvoke>     
            
            <cfset tickEnd = GetTickCount()>
            <cfset InitLockQueueIdTime = tickEnd - tickBegin>
            
            <cfset DebugStr = DebugStr & " InitLockQueueIdTime = #InitLockQueueIdTime#" />     
                                            
            <!--- <cfset DebugStr = DebugStr & " #SerializeJSON(RetValUpdateQueueNextResponseSMSStatus)#" /> --->
            
            <!--- All is well - then send --->
            <cfif RetValUpdateQueueNextResponseSMSStatus.RXRESULTCODE GT 0>    
                
                <cfset tickBegin = GetTickCount() />
                                        
                <!--- Process next response --->
                <cfinvoke                    
                     method="ProcessNextResponseSMS"
                     returnvariable="RetValProcessNextResponseSMS">                         
                        <cfinvokeargument name="inpContactString" value="#inpContactString#"/>
                        <cfinvokeargument name="inpCarrier" value="#inpCarrierId#"/>
                        <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                        <cfinvokeargument name="inpKeyword" value="#inpKeyword#"/>
                        <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                        <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                        <cfinvokeargument name="inpTime" value="#inpTime#"/>
                        <cfinvokeargument name="inpServiceId" value="#inpCustomServiceId1#"/>
                        <cfinvokeargument name="inpXMLDATA" value="#inpRawData#"/>
                        <cfinvokeargument name="inpOverRideInterval" value="1"/>  
                        <cfinvokeargument name="inpTimeOutNextQID" value="#inpQuestionTimeOutQID#"/>                          
                </cfinvoke>  
                
                <cfset tickEnd = GetTickCount()>
            	<cfset NextResponseTime = tickEnd - tickBegin>            
              
              	<cfset DebugStr = DebugStr & " NextResponseTime = #NextResponseTime#" /> 
              
                <cfif RetValProcessNextResponseSMS.RXRESULTCODE GT 0> 
                            
		            <cfset tickBegin = GetTickCount() />
        
                    <!--- Update to processed --->        
                    <cfinvoke                                           
                         method="UpdateQueueNextResponseSMSStatus"
                         returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                            <cfinvokeargument name="inpQueueId" value="#inpmoInboundQueueId#"/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED#"/>   
                    </cfinvoke>  
                    
                    <cfset tickEnd = GetTickCount()>
            		<cfset FinishQueueIdTime = tickEnd - tickBegin>      
                
                    <cfset DebugStr = DebugStr & " RetValProcessNextResponseSMS OK FinishQueueIdTime = #FinishQueueIdTime#" />     
                    
                <cfelse>
            
                    <!--- Update to Error- This could be partially processed by this point so do not re-try --->        
                    <cfinvoke                                             
                         method="UpdateQueueNextResponseSMSStatus"
                         returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                            <cfinvokeargument name="inpQueueId" value="#inpmoInboundQueueId#"/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR_POSSIBLE_PROCESSED#"/> 
                    </cfinvoke>   
                    
                    <!--- Log errors --->
                    <cftry>        
            
                        <cfset ENA_Message ="SimpleX Error Notice. SMS Queue  Process next response Error - ProcessNextResponseSMS - #RetValProcessNextResponseSMS.MESSAGE# #RetValProcessNextResponseSMS.ERRMESSAGE#">
                        <cfset ErrorNumber = "10004">
                        <cfset SubjectLine = "SimpleX SMS API Notification">
                        <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                                                                                                  
                        <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                            INSERT INTO simplequeue.errorlogs
                            (
                                ErrorNumber_int,
                                Created_dt,
                                Subject_vch,
                                Message_vch,
                                TroubleShootingTips_vch,
                                CatchType_vch,
                                CatchMessage_vch,
                                CatchDetail_vch,
                                Host_vch,
                                Referer_vch,
                                UserAgent_vch,
                                Path_vch,
                                QueryString_vch
                            )
                            VALUES
                            (
                                #ErrorNumber#,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                            )
                        </cfquery>
                    
                    <cfcatch type="any">
                    
                    
                    </cfcatch>
                        
                    </cftry>    
            
                </cfif> <!--- Log errors --->            
            
            <cfelse> <!--- All is well - then send --->
            
                <!--- Update to Error --->        
                <cfinvoke                    
                     method="UpdateQueueNextResponseSMSStatus"
                     returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                        <cfinvokeargument name="inpQueueId" value="#inpmoInboundQueueId#"/>
                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR#"/> 
                </cfinvoke>    
                
            </cfif> <!--- All is well - then send --->      
                                    
        <cfcatch>
                       
                <cfset dataout =  StructNew() />  
				<cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = cfcatch.Type />
                <cfset dataout.MESSAGE = "inpmoInboundQueueId=#inpmoInboundQueueId# #cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#" />
                <cfset dataout.ERRMESSAGE = cfcatch.Detail />
         
                <cftry>        
                                	
					<cfset ENA_Message = "">
                    <cfset SubjectLine = "SimpleX SMS API Notification - /ebmqueue/it">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="10003">
                                                      
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
        
        <cfset dataout =  StructNew() />  
		
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "inpmoInboundQueueId=#inpmoInboundQueueId# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM# #inpkeyword# #inpShortCode# #DebugStr#" />
        <cfset dataout.ERRMESSAGE = "" />
                           
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>


