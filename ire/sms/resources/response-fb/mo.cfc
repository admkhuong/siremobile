<cfcomponent extends="taffy.core.resource" taffy:uri="/fb/mo" hint="MO response inbound for fb SMS Enabled Number Webhook">
	<cfinclude template="../../paths.cfm" >
	
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">
    
    
	<!--- only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get Request from fb">
		<cfargument name="XMLDATA" TYPE="string" required="no" default="" hint="" />
		<cfargument name="JSONDATA" TYPE="string" required="no" default="" hint="This will be raw JSON text from fb" />
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>				
        <cfargument name="From" TYPE="string" required="no" default="0"/>				
        <cfargument name="To" TYPE="string" required="no" default="1849455028701601"/>
        <cfargument name="Body" TYPE="string" required="no" default="0" hint="keywords or text"/>				
        <cfargument name="MessageSid" TYPE="string" required="no" default="0"/>
        
        <cfif arguments.To is "">
		    <cfset arguments.To="1849455028701601">
        </cfif>
		<cfset var dataout = {} /> 		
		<cfset var contactString = '' />
		<cfset var transactionId = '' />
		<cfset var carrier = '' />
		<cfset var shortCode = '' />
		<cfset var keyword = '' />
		<cfset var time = '' />
		<cfset var ServiceId = '' />
		<cfset var xmlDataString = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetValProcessNextResponseSMS = '' />
        <cfset var DBSourceEBM = "Bishop"/>         
        <cfset var RetValCheckForDuplicateMO = "" />
        <cfset var RetValInsertMessagePart = ''>
        <cfset var inpContactStringBuff = "" />
        <cfset var RetValLogDuplicateResponseSMS = '' />
		<cfset var RequestHeaders = {} />		
		<cfset var GetShortCodeData = "" />		
											

				
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>
        
		<cftry>
		
					
			
			<cfquery name="GetShortCodeData" datasource="#DBSourceEBM#" >
                SELECT                    
                    CustomServiceId3_vch                                        
                FROM		       		
                    SMS.ShortCode 		       
                WHERE		       	
                    ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.To#">		                 	   
            </cfquery> 

			<!--- IF there is no short code set up for the passed in channel then there will be no data. --->
            <cfif GetShortCodeData.RecordCount EQ 0>
	            <!--- This is not a valid request - ignore it and return with error--->
	            
	            <cfset dataout.RXRESULTCODE = 1 />
		        <cfset dataout.TYPE = ""/>
		        <cfset dataout.MESSAGE = "#arguments.To# not found in System #CGI.SERVER_NAME# #Session.DBSourceEBM#" /> 
				<cfset dataout.ERRMESSAGE = "" />

				<cfreturn representationOf(dataout).withStatus(401) />
            </cfif>  
					
			<cfset contactString = "#arguments.From#">
			<cfset transactionId = "#arguments.MessageSid#">
			<cfset carrier = "Facebook">
			<cfset shortCode = "#arguments.To#">
			<cfset keyword = "#arguments.Body#">
			<cfset time = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">            
            <cfset ServiceId = "16">	                       
                          
            <cfif TRIM(XMLDATA) EQ "">
	            <cfset XMLDATA = #SerializeJSON(FORM)# />
            </cfif>
            		
            <cfset inpContactStringBuff = contactString />			
                       
            <!--- Don't fill DB with empty data --->
            <cfif LEN(inpContactStringBuff) GT 0 AND LEN(shortCode) GT 0 AND LEN(keyword) GT 0 and 1 eq 2>
             
				<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->
            
				<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->            
            
                <!--- Log the fact that we processed to the DB --->    
                <cfinvoke                    
                        method="QueueNextResponseSMS"
                        returnvariable="RetValQueueNextResponseSMS">                         
                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                        <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                        <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                        <cfinvokeargument name="inpPreferredAggregator" value="16"/>
                        <cfinvokeargument name="inpTime" value="#time#"/>
                        <cfinvokeargument name="inpScheduled" value=""/>
                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                        <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                        <cfinvokeargument name="inpXMLDATA" value="#JSONDATA# #XMLDATA#"/>
                        <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                </cfinvoke>                      
                                            
                <!--- Validate this is not a duplicate ... --->
                <cfinvoke                    
                        method="CheckForDuplicateMO"
                        returnvariable="RetValCheckForDuplicateMO">                         
                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                            
                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                        <cfinvokeargument name="inpKeyword" value="#keyword#"/> 
                        <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/> 
                        <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>                         
                </cfinvoke>  
                
                <!--- Check for duplicate --->
                <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2 OR RetValCheckForDuplicateMO.RXRESULTCODE EQ 3 >
                    
                    <!--- Flag as duplicate --->
                
                    <!--- Log Duplicates ignored--->
                    <cfinvoke                    
                            method="LogDuplicateResponseSMS"
                            returnvariable="RetValLogDuplicateResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="16"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>  
                        
                    <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->
                    
                <cfelse> <!--- Not a duplicate --->
                
                                            
                    <!--- Process next response --->
                    <cfinvoke                    
                            method="ProcessNextResponseSMS"
                            returnvariable="RetValProcessNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="16"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpOverrideAggregator" value="1"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>                                    
                    </cfinvoke>
                                    
                </cfif> <!--- Check for duplicate --->    
            
           	</cfif>

            <!--- simple example first--->
            
            <cfset token=GetShortCodeData.CustomServiceId3_vch>

            <cfset timenow=dateTimeFormat(Now(), "mm-dd-yyyy HH:mm:ss")>
            <cfset returnlog="">
            <cfset HttpRequest=getHTTPRequestData()>
            <cfset Data=DeserializeJSON(HttpRequest.content)>
            
            <cfif ISDEFINED("Data.object")>
                <cfif Data.object =="page">
                    <cfset Entry=Data.entry>
                    <cfset EntryId=Entry[1].id>
                    <cfset EntryTime=Entry[1].time>

                    <cfset EntryMessaging=Entry[1].messaging>
                    
                    <cfset SenderId=EntryMessaging[1].sender.id>
                    <cfset RecipientId=EntryMessaging[1].recipient.id>
                    <cfif ISDEFINED("EntryMessaging[1].message")>
                    <cfset Message=EntryMessaging[1].message>
                    <cfif ISDEFINED("Message.text")>
                        <cfset MessageText=Message.text>        
                        <!---send back --->            
                        <cfset JSONDATA='{"recipient": { "id": "#SenderId#"},"message": {"text": "This is auto mesage from sire system!"}}'>            
                        <cfhttp url="https://graph.facebook.com/v2.10/me/messages?access_token=#token#" method="POST" resolveurl="no" throwonerror="yes" result="smsreturn" timeout="25">					          
                        <cfhttpparam type="header" name="Content-Type" value="application/json" />
                        <cfhttpparam type="body" value="#JSONDATA#">   
                        </cfhttp>       
                        <!---end back --->
                        <cfset returnlog = MessageText & " - From :" & SenderId & " - To :" & RecipientId> 
                    <cfelseif ISDEFINED("Message.sticker_id")>       
                        <cfset returnlog = "User Sent Sticker" & " - From :" & SenderId & " - To :" & RecipientId> 
                    </cfif>          
                    <cfelseif ISDEFINED("EntryMessaging[1].delivery")>
                    <cfset returnlog = "Send Back Successfully to user: " & SenderId> 
                    </cfif>                 
                    
                </cfif>
            </cfif>            

            <cfif returnlog neq "">
            <cffile  
                action = "append" 
                file = "#ExpandPath('/')#session/sire/logs/test-fb-wk.txt" 
                output = "#timenow#----#returnlog#" 
                addNewLine = "yes" 
                attributes = "normal"
                charset = "utf-8"  
                fixnewline = "yes" 
                mode = "777">
            </cfif>
            <!--- end simple example--->
                                    
        <cfcatch>
                       
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type/>
            <cfset dataout.MESSAGE = cfcatch.Message />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />  
                        
            <cftry>        
            
            	<cfset xmlDataString = URLDecode(XMLDATA)>
				<cfset ENA_Message = "XMLDATA = (#XMLDATA#) xmlDataString=(#xmlDataString#)">
                <cfset SubjectLine = "SimpleX SMS API Notification">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="100">
                <cfset AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
            
            <cfcatch type="any">
            
            
            </cfcatch>
                
            </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
               
		<cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = ""/>
        <cfset dataout.MESSAGE = "#DebugAPI# #CGI.SERVER_NAME# #Session.DBSourceEBM#" /> 
		<cfset dataout.ERRMESSAGE = "" />
                             
		<!---	<cfreturn representationOf(dataout).withStatus(200) /> --->
		<cfreturn representationOf("").withStatus(200) />

	</cffunction>
	
		
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		     
        <cfset var dataout = {} />  
        
		<cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = ""/>
        <cfset dataout.MESSAGE = "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!" />         
		<cfset dataout.ERRMESSAGE = "" />
        <cfif ISDEFINED("hub.challenge")>                  
            <cfreturn representationOf(hub.challenge).withStatus(200) />
        </cfif>
		            
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
</cfcomponent>