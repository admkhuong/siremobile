<cfcomponent extends="taffy.core.resource" taffy:uri="/line/mo/{LineChannelId}" hint="MO response inbound for Line App Webhook">
	<cfinclude template="../../paths.cfm" >

	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">


    <!---
	https://devdocs.line.me/en/#getting-started

	Request headers

	Request header	Description
	X-Line-Signature	Used for signature validation

	Signature validation

	The signature in the X-Line-Signature request header must be verified to confirm that the request was sent from the LINE Platform.

	Authentication is performed as follows.

	1. With the Channel secret as the secret key, your application retrieves the digest value in the request body created using the HMAC-SHA256 algorithm.
	2. The server confirms that the signature in the request header matches the digest value which is Base64 encoded.





	Request body

	The request body contains a JSON object with an array of webhook event objects

	Field	Type	Description
	events	Array of webhook event objects	Information about the event


		{"events":[{"type":"message","replyToken":"9126204681e0465dbe7ba07d2fe2bdbf","source":{"userId":"U115f7114aaa8dc4a88a335f9b3252b7f","type":"user"},"timestamp":1488338442843,"message":{"type":"text","id":"5715936951861","text":"Hello 2"}}]}







For events that you can respond to, a replyToken is issued for replying to messages.
Because the replyToken becomes invalid after a certain period of time, responses should be sent as soon as a message is received. Reply tokens can only be used once.

HTTP request
POST https://api.line.me/v2/bot/message/reply



Need to pass - ReplyToken
Channel Authorization key (Related to the passed in LineChannelId) - RTOXiM6dGlKmhKvIAiUBjFcXW3zHYosECY280G0tqv701UyybHZPkgGvHQKkx5XaZzPStuOybRDYC/CojL5WgASNpb8YBYCiBHOGLqsyyPf/I87Nni8BtzshN5OR4HZ2cLcP1f+ZCRux0wRmd5f2DwdB04t89/1O/w1cDnyilFU=


Passin as combo to service Id?






		Questions:

			Can/Does Line send more than one event per post?
			Do I need to loop over events and process them individually? Loop over events[x] array?

	--->


	<!--- only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get Request from Line">
		<cfargument name="XMLDATA" TYPE="string" required="no" default="" hint="" />
		<cfargument name="LineChannelId" TYPE="string" required="no" default="" hint="This is the line target channel Id for the account that messages are being sent to" />
		<cfargument name="JSONDATA" TYPE="string" required="no" default="" hint="This will be raw JSON text from Line" />
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>

		<cfset var LineData = '0' />
		<cfset var dataout = '0' />
		<cfset var mbloxData = '' />
		<cfset var contactString = '' />
		<cfset var transactionId = '' />
		<cfset var carrier = '' />
		<cfset var shortCode = '' />
		<cfset var keyword = '' />
		<cfset var time = '' />
		<cfset var ServiceId = '' />
		<cfset var xmlDataString = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetValProcessNextResponseSMS = '' />
        <cfset var DBSourceEBM = "Bishop"/>
        <cfset var UDHString = "">
        <cfset var RetValCheckForDuplicateMO = "" />
        <cfset var RetValInsertMessagePart = ''>
        <cfset var inpContactStringBuff = "" />
        <cfset var RetValLogDuplicateResponseSMS = '' />
		<cfset var RequestHeaders = {} />
		<cfset var XLineSignature = "" />
		<cfset var GetShortCodeData = "" />
		<cfset var secret = '' />
		<cfset var mac = '' />
		<cfset var digest = '' />
		<cfset var signature = '' />
		<cfset var RequestIsSecure = false />
		<cfset var channelSecret = "" />
		<cfset var httpRequestBody = "" />


  		<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        <!--- Assume no session --->
        <cfif inpQAMode GT 0 >

            <cfset DBSourceEBM = "BishopDev"/>
            <cfset Session.DBSourceEBM = "BishopDev"/>

        <cfelse>

            <cfset DBSourceEBM = "Bishop"/>
            <cfset Session.DBSourceEBM = "Bishop"/>

        </cfif>

<!---
	   <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
		  INSERT INTO simplequeue.errorlogs
		  (
			 ErrorNumber_int,
			 Created_dt,
			 Subject_vch,
			 Message_vch,
			 TroubleShootingTips_vch,
			 CatchType_vch,
			 CatchMessage_vch,
			 CatchDetail_vch,
			 Host_vch,
			 Referer_vch,
			 UserAgent_vch,
			 Path_vch,
			 QueryString_vch
		  )
		  VALUES
		  (
			 7,
			 NOW(),
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Line Log">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SerializeJSON(GetHttpRequestData().Headers)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#JSONDATA#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SerializeJSON(FORM)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LineChannelId#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
			 <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
		  )
	   </cfquery> --->

<!--- <cfreturn representationOf(dataout).withStatus(200) /> --->


		<cftry>

			<cfset RequestHeaders = GetHttpRequestData().Headers />

			<cfif structKeyExists(RequestHeaders, "X-Line-Signature")>

				<cfset XLineSignature = RequestHeaders["X-Line-Signature"] />

			<cfelse>


			</cfif>

			<!--- Validate Request is from Line --->
			<!---  From Line API Java sample code

			String channelSecret = ...; // Channel secret string
			String httpRequestBody = ...; // Request body string
			SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(key);
			byte[] source = httpRequestBody.getBytes("UTF-8");
			String signature = Base64.encodeBase64String(mac.doFinal(source));
			// Compare X-Line-Signature request header string and the signature

			--->


			<!--- Lookup the Channel Secret Key for this request using the passed in LineChannelId - Channel Secret from Channel Id 1503546117 --->

			<cfquery name="GetShortCodeData" datasource="#DBSourceEBM#" >
                SELECT
                    CustomServiceId1_vch
                FROM
                    SMS.ShortCode
                WHERE
                    ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.LineChannelId#">
            </cfquery>

            <!--- IF there is no short code set up for the passed in channel then there will be no data. --->
            <cfif GetShortCodeData.RecordCount EQ 0>
	            <!--- This is not a valid request - ignore it and return with error--->
				<cfreturn representationOf(dataout).withStatus(401) />
            </cfif>

			<cfset channelSecret = GetShortCodeData.CustomServiceId1_vch />
			<cfset httpRequestBody = getHTTPRequestData().content />

			<cfset secret = createObject('java', 'javax.crypto.spec.SecretKeySpec' ).Init(JavaCast("string",channelSecret).getBytes("iso-8859-1"), 'HmacSHA256')/>
			<cfset mac = createObject('java', "javax.crypto.Mac") />
			<cfset mac = mac.getInstance("HmacSHA256") />
			<cfset mac.init(secret) />

			<!--- Encrypt the body sent using the secret key that matches this account --->
			<cfset digest = mac.doFinal(JavaCast("string",httpRequestBody).getBytes("UTF-8")) />

			<!--- String readable version of binary data (Bytes) --->
			<cfset signature = ToBase64("#digest#")>

			<!--- Compare the header passed with a locally encrypted using secret key version of the sent body --->
			<cfif Compare(signature, XLineSignature) NEQ 0 >

				<!--- This is not a valid request - ignore it and return with error--->
				<cfreturn representationOf(dataout).withStatus(401) />

			</cfif>


			<cfset contactString = "">
			<cfset transactionId = "">
			<cfset carrier = "Line">
			<cfset shortCode = arguments.LineChannelId>
			<cfset keyword = "">
			<cfset time = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">
            <cfset ServiceId = "0">
            <cfset UDHString = "">

            <cfif TRIM(XMLDATA) EQ "">
	            <cfset XMLDATA = #SerializeJSON(FORM)# />
            </cfif>

			<cfif not isJson(JSONDATA)>
				<cfreturn representationOf(dataout).withStatus(400) />
			</cfif>


            <cfset LineData = deserializeJSON(JSONDATA) />
			<cfif isStruct(LineData)>


				<cfset time = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">

	            <!--- Pass the ReplyToken as the Service Id --->
				<cfif structKeyExists(LineData.events[1], "replyToken")>
					<cfset ServiceId = LineData.events[1].replyToken>
				</cfif>

				<cfif LineData.events[1].type EQ "message" >

					<cfset keyword = LineData.events[1].message.text>
					<cfset transactionId = LineData.events[1].message.id>
					<cfset contactString = LineData.events[1].source.userId>

				</cfif>

			</cfif>

            <cfset inpContactStringBuff = contactString />
			<!--- Carriers uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->
            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
            </cfloop>


            <!--- Don't fill DB with empty data --->
            <cfif LEN(inpContactStringBuff) GT 0 AND LEN(shortCode) GT 0 AND LEN(keyword) GT 0>

				<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->

				<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->
                <cfif 1 EQ 1>

                	<!--- Log the fact that we processed to the DB --->
                    <cfinvoke
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="15"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>

                	<!--- Validate this is not a duplicate ... --->
                    <cfinvoke
                         method="CheckForDuplicateMO"
                         returnvariable="RetValCheckForDuplicateMO">
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                    </cfinvoke>

                    <!--- Check for duplicate --->
                    <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2 OR RetValCheckForDuplicateMO.RXRESULTCODE EQ 3 >

                        <!--- Flag as duplicate --->

                    	<!--- Log Duplicates ignored--->
                        <cfinvoke
                             method="LogDuplicateResponseSMS"
                             returnvariable="RetValLogDuplicateResponseSMS">
                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="15"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpScheduled" value=""/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>

                        <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->

                    <cfelse> <!--- Not a duplicate --->


                        <!--- Process next response --->

                        <cfinvoke
                             method="ProcessNextResponseSMS"
                             returnvariable="RetValProcessNextResponseSMS">
                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="15"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpOverrideAggregator" value="1"/>
                                <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>

                        <cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire">
                            <cfinvokeargument name="inpUserId" value="#RetValProcessNextResponseSMS.GETRESPONSE.USERID#"/>
                            <cfinvokeargument name="inpCredits" value="1"/>
                        </cfinvoke>

                    </cfif> <!--- Check for duplicate --->

                <cfelse>

					<!--- Queue up future response tracking in DB--->
                    <cfinvoke
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="15"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_SECONDARYQUEUE#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>

            	</cfif>

           	</cfif>

        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", cfcatch.Message) />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />

                <cftry>

                	<cfset xmlDataString = URLDecode(XMLDATA)>
					<cfset ENA_Message = "XMLDATA = (#XMLDATA#) xmlDataString=(#xmlDataString#)">
                    <cfset SubjectLine = "SimpleX SMS API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">

                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>

                <cfcatch type="any">


                </cfcatch>

                </cftry>

            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>

		</cftry>

        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <!--- contactString=#contactString# retv=#SerializeJSON(RetValCheckForDuplicateMO)# shortCode=#shortCode#  keyword=#keyword#  transactionId=#transactionId# --->
        <cfset QuerySetCell(dataout, "MESSAGE", "#DebugAPI# #CGI.SERVER_NAME# #Session.DBSourceEBM#") />
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />

		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>


    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">

        <cfset var dataout = '0' />

        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!") />
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />


		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>

</cfcomponent>
