<cfcomponent extends="taffy.core.resource" taffy:uri="/mosaic/dr" hint="Update status of MT Mosaic">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../paths.cfm" >
    
    <!--- 
	    
	    
	    {"sourceNational":"9494000553","connectionGUID":"858E176E-E03D-4BEC-832F-C4505C69A915","fieldnames":"sourceNational,connectionGUID,destination,source,transactionGUID,sourceCountryCode,sourceCountryAbbreviation,sourceCountry,concatenatedMessage,messageText","destination":"18887474411","source":"19494000553","transactionGUID":"2b9fdfdc-b45f-45f1-83e3-0299164168d5","sourceCountryCode":"1","sourceCountryAbbreviation":"US","sourceCountry":"","concatenatedMessage":"false","messageText":"id:b52f5fe7-98b3-4b92-803a-8dcc980d835f sub:000 dlvrd:000 
	    
	    submit date:1606301656 done date:1606301656 stat:DELIVRD err:3ec text: "}
	    
	    
	    
	     --->
    
	<!---Mosaic only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get message status updates from Mosaic">
		<cfargument name="XMLDATA" TYPE="string" required="no" default=""/>
		<cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		<cfargument name="transactionGUID" TYPE="string" required="no" default="" hint="[string(36)] The globally unique transaction ID for the message." />
		<cfargument name="connectionGUID" TYPE="string" required="no" default="" hint="[string(36)] The globally unique identifier for the Aerialink network connection in your account which processed the transaction."/>
		<cfargument name="source" TYPE="string" required="no" default="" hint="[string(15)]	The source address for the message. This is the end user mobile phone which sent you the message."/>
		<cfargument name="sourceCountry" TYPE="string" required="no" default="" hint="[string(255)]	The registered country name for the number."/>
		<cfargument name="sourceCountryCode" TYPE="string" required="no" default="" hint="[integer] The associated country code for the number."/>
		<cfargument name="sourceCountryAbbreviation" TYPE="string" required="no" default="" hint="[string(2)] The two-letter country abbreviation (ISO 3166-1 alpha-2)"/>
		<cfargument name="destination" TYPE="string" required="no" default="" hint="[string(15)] The destination number that the end user sent the message to. This will match with a Short/Long Code configured on your account. "/>
		<cfargument name="messageText" TYPE="string" default="" required="no" hint="[string(160)] The SMS message text that the end user sent to your Short/Long Code. This is limited to the general standard of 160 characters unless concatenation is enabled on your account. In some cases your character limit may be less depending on your specific use case and configuration or a carrier limitation."/>
		<cfargument name="concatenatedMessage" TYPE="string" default="" required="no" hint="[string(5)]	true/false "/>
		<cfargument name="sourceNational" TYPE="string" default="" required="no" hint="[string(15] 	The source address for the message in national format, excluding the country code. This is the end user mobile phone which sent you the message."/>
		<cfargument name="stat" TYPE="string" required="no" default="UNKNOWN"/>
		<cfargument name="err" TYPE="string" required="no" default=""/>
		
		<cfset var dataout = '0' /> 
		<cfset var nSMSGlobalMessageID = '' />
		<cfset var InsertMTToContactResults = '' />
		<cfset var inpContactStringBuff = "" />
		<cfset var DBSourceEBM = "Bishop"/> 
 
		<cftry>
		
		
			<cfif TRIM(XMLDATA) EQ "">
	             <cfset XMLDATA = #SerializeJSON(FORM)# />
             </cfif>
             
			<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
	        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
	        <cfif inpQAMode GT 0 >
	               
	            <cfset DBSourceEBM = "BishopDev"/> 
	          
	        <cfelse>
	        
	            <cfset DBSourceEBM = "Bishop"/> 
	           
	        </cfif>
                
        	<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
            <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        	<cfset Session.DBSourceEBM = DBSourceEBM />
           
            <!--- Mosaci and Carriers use ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->  
			<cfset inpContactStringBuff = arguments.source />
			          
            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
            </cfloop>
            
        						
			<!---Insert MT to simplexresults.contactresults--->
			<cfquery name="InsertMTToContactResults" datasource="#DBSourceEBM#">
				INSERT INTO simplexresults.smsdisposition
				(
	                SubscriberNumber_vch,					
                    Time_vch,
                    Status_vch,
                    Status_ti,
                    MsgReference_vch,
                    Reason_vch,
                    ShortCode_vch,
                    nSMSGlobalMessageID_vch,
                    SequenceNumber_vch,
                    SecondarySequenceNumber_vch,
                    RawData_vch,
                    AggregatorId_int,
                    Created_dt
				)
				VALUES
				(
	                <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#inpContactStringBuff#">,								
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">, <!---Time_dt--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.stat#">,<!---Status_ti--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.transactionGUID#">,<!---MsgReference_vch--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.err#">,<!---Reason_vch--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.destination#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#arguments.messageText#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="">,		
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="">,          
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#XMLDATA#">, 
                    667242,                   
					NOW()
				)
			</cfquery>
            
			<cfcatch>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
				<cfset QueryAddRow(dataout) />
		        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		        <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
		        <cfset QuerySetCell(dataout, "MESSAGE", cfcatch.Message) />                
		        <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
		        
				<cfquery name="InsertMTToContactResults" datasource="#DBSourceEBM#">
					INSERT INTO simplexresults.smsdisposition
					(
		                
	                    Status_ti,
	                    ShortCode_vch,
	                    Reason_vch,
					    RawData_vch,
	                    AggregatorId_int,
	                    Created_dt
					)
					VALUES
					(	               
	                    0,
	                    '',        
	                    '#LEFT(SerializeJSON(cfcatch),512)#',                   
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#XMLDATA#">,
	                    667242,                   
						NOW()
					)
										
				</cfquery>

		                    
				<cfreturn representationOf(dataout).withStatus(200) />
			</cfcatch>
		</cftry>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	<cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST FORM data to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>

</cfcomponent>