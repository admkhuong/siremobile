<cfcomponent extends="taffy.core.resource" taffy:uri="/twilio/mo" hint="MO response inbound for Twilio SMS Enabled Number Webhook">
	<cfinclude template="../../paths.cfm" >
	
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">
    
    
    <!--- 
	
			/{TwiliNumberId}
			
			
			Twilio will post form data
			
			
			ToCountry	US
			ToState	CA
			SmsMessageSid	SMbca7e97ef414918133abfe9afddd4b09
			NumMedia	0
			ToCity	
			FromZip	95054
			SmsSid	SMbca7e97ef414918133abfe9afddd4b09
			FromState	CA
			SmsStatus	received
			FromCity	IRVINE
			Body	Hello
			FromCountry	US
			To	+19494080493
			ToZip	
			NumSegments	1
			MessageSid	SMbca7e97ef414918133abfe9afddd4b09
			AccountSid	AC321118d53df23eacf8d5957b0d3b87d3
			From	+19494000553
			ApiVersion	2010-04-01

	--->
    
    
	<!--- only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get Request from Twilio">
		<cfargument name="XMLDATA" TYPE="string" required="no" default="" hint="" />
		<cfargument name="JSONDATA" TYPE="string" required="no" default="" hint="This will be raw JSON text from Line" />
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		<cfargument name="ToCountry" TYPE="any" required="no" default="" hint="" />
		<cfargument name="ToState" TYPE="any" required="no" default="" hint="" />
		<cfargument name="SmsMessageSid" TYPE="any" required="no" default="" hint="" />
		<cfargument name="NumMedia" TYPE="any" required="no" default="" hint="" />
		<cfargument name="ToCity" TYPE="any" required="no" default="" hint="" />
		<cfargument name="FromZip" TYPE="any" required="no" default="" hint="" />
		<cfargument name="SmsSid" TYPE="any" required="no" default="" hint="" />
		<cfargument name="FromState" TYPE="any" required="no" default="" hint="" />
		<cfargument name="SmsStatus" TYPE="any" required="no" default="" hint="" />
		<cfargument name="FromCity" TYPE="any" required="no" default="" hint="" />
		<cfargument name="Body" TYPE="any" required="no" default="" hint="" />
		<cfargument name="FromCountry" TYPE="any" required="no" default="" hint="" />
		<cfargument name="To" TYPE="any" required="no" default="" hint="" />
		<cfargument name="ToZip" TYPE="any" required="no" default="" hint="" />
		<cfargument name="NumSegments" TYPE="any" required="no" default="" hint="" />
		<cfargument name="MessageSid" TYPE="any" required="no" default="" hint="" />
		<cfargument name="AccountSid" TYPE="any" required="no" default="" hint="" />
		<cfargument name="From" TYPE="any" required="no" default="" hint="" />
		<cfargument name="ApiVersion" TYPE="any" required="no" default="" hint="" />
				
		<cfset var dataout = {} /> 
		<cfset var mbloxData = '' />
		<cfset var contactString = '' />
		<cfset var transactionId = '' />
		<cfset var carrier = '' />
		<cfset var shortCode = '' />
		<cfset var keyword = '' />
		<cfset var time = '' />
		<cfset var ServiceId = '' />
		<cfset var xmlDataString = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetValProcessNextResponseSMS = '' />
        <cfset var DBSourceEBM = "Bishop"/> 
        <cfset var UDHString = "">	
        <cfset var RetValCheckForDuplicateMO = "" />
        <cfset var RetValInsertMessagePart = ''>
        <cfset var inpContactStringBuff = "" />
        <cfset var RetValLogDuplicateResponseSMS = '' />
		<cfset var RequestHeaders = {} />
		<cfset var XLineSignature = "" />
		<cfset var GetShortCodeData = "" />
		<cfset var secret = '' />
		<cfset var mac = '' />
		<cfset var digest = '' />
		<cfset var signature = '' />
		<cfset var RequestIsSecure = false />
		<cfset var channelSecret = "" />
		<cfset var httpRequestBody = "" />

				
  		<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so includes have a Session.DBSourceEBM--->
        <!--- Assume no session to be safe--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>
        
		<cftry>
		
					
						
			<!--- 
				
				 {"ToCity":"","FromCountry":"US","ToZip":"","ToCountry":"US","MessageSid":"SMbca7e97ef414918133abfe9afddd4b09","NumSegments":"1","SmsMessageSid":"SMbca7e97ef414918133abfe9afddd4b09","AccountSid":"AC321118d53df23eacf8d5957b0d3b87d3","From":" 19494000553","fieldnames":"ToCity,FromCountry,ToZip,ToCountry,MessageSid,NumSegments,SmsMessageSid,AccountSid,From,SmsStatus,SmsSid,To,ToState,FromZip,NumMedia,Body,FromCity,FromState,ApiVersion","SmsStatus":"received","SmsSid":"SMbca7e97ef414918133abfe9afddd4b09","To":" 19494080493","ToState":"CA","FromZip":"95054","NumMedia":"0","Body":"Hello","FromCity":"IRVINE","FromState":"CA","ApiVersion":"2010-04-01"}
				
			 -<Response>
				    <Message>Goodbye!</Message>
				</Response>

			--->
			
			
			<cfquery name="GetShortCodeData" datasource="#DBSourceEBM#" >
                SELECT                    
                    CustomServiceId3_vch                                        
                FROM		       		
                    SMS.ShortCode 		       
                WHERE		       	
                    ShortCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.To#">		                 	   
            </cfquery> 

			<!--- IF there is no short code set up for the passed in channel then there will be no data. --->
            <cfif GetShortCodeData.RecordCount EQ 0>
	            <!--- This is not a valid request - ignore it and return with error--->
	            
	            <cfset dataout.RXRESULTCODE = 1 />
		        <cfset dataout.TYPE = ""/>
		        <cfset dataout.MESSAGE = "#arguments.To# not found in System #CGI.SERVER_NAME# #Session.DBSourceEBM#" /> 
				<cfset dataout.ERRMESSAGE = "" />

				<cfreturn representationOf(dataout).withStatus(401) />
            </cfif>  
					
			<cfset contactString = "#arguments.From#">
			<cfset transactionId = "#arguments.MessageSid#">
			<cfset carrier = "Twilio">
			<cfset shortCode = "#arguments.To#">
			<cfset keyword = "#arguments.Body#">
			<cfset time = "#DateFormat(Now(),'yyyy-mm-dd')# #LSTimeFormat(Now(),'HH:mm')#">            
            <cfset ServiceId = "0">	           
            <cfset UDHString = "">	
                          
            <cfif TRIM(XMLDATA) EQ "">
	            <cfset XMLDATA = #SerializeJSON(FORM)# />
            </cfif>
            		
            <cfset inpContactStringBuff = contactString />
			
			<!--- Dont do this for international numbers! For Twilio US numbers are CustomServiceId3_vch = "1" --->
			<cfif GetShortCodeData.CustomServiceId3_vch EQ "1">
				<!--- Carriers uses +'s but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
	            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '+' AND LEN(inpContactStringBuff) GT 1">
	                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
	            </cfloop>
				
				<!--- Carriers uses 1's but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
	            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
	                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
	            </cfloop>
            
			</cfif>
                       
            <!--- Don't fill DB with empty data --->
            <cfif LEN(inpContactStringBuff) GT 0 AND LEN(shortCode) GT 0 AND LEN(keyword) GT 0>
             
				<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->
            
				<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->
                <cfif 1 EQ 1>
                
                	<!--- Log the fact that we processed to the DB --->    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="13"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA# #XMLDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>                      
                                	            
                	<!--- Validate this is not a duplicate ... --->
                    <cfinvoke                    
                         method="CheckForDuplicateMO"
                         returnvariable="RetValCheckForDuplicateMO">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                            
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/> 
                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/> 
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>                         
                    </cfinvoke>  
                    
                    <!--- Check for duplicate --->
                    <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2 OR RetValCheckForDuplicateMO.RXRESULTCODE EQ 3 >
                    	
                        <!--- Flag as duplicate --->
                    
                    	<!--- Log Duplicates ignored--->
                        <cfinvoke                    
                             method="LogDuplicateResponseSMS"
                             returnvariable="RetValLogDuplicateResponseSMS">                         
                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="13"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpScheduled" value=""/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>  
                            
                        <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->
                        
                    <cfelse> <!--- Not a duplicate --->
                    
						                       
                        <!--- Process next response --->
                        <cfinvoke                    
                             method="ProcessNextResponseSMS"
                             returnvariable="RetValProcessNextResponseSMS">                         
                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="13"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpOverrideAggregator" value="1"/>
                                <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>                                    
                        </cfinvoke>
                                                    
                        <cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire">
                            <cfinvokeargument name="inpUserId" value="#RetValProcessNextResponseSMS.GETRESPONSE.USERID#"/>
                            <cfinvokeargument name="inpCredits" value="1"/>
                        </cfinvoke>

                	
                    </cfif> <!--- Check for duplicate --->
                                                
                <cfelse>

					<!--- Queue up future response tracking in DB--->                    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="13"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_SECONDARYQUEUE#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>                                
                
            	</cfif>            
            
           	</cfif>
                                    
        <cfcatch>
                       
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = cfcatch.Type/>
            <cfset dataout.MESSAGE = cfcatch.Message />
            <cfset dataout.ERRMESSAGE = cfcatch.Detail />  
                        
            <cftry>        
            
            	<cfset xmlDataString = URLDecode(XMLDATA)>
				<cfset ENA_Message = "XMLDATA = (#XMLDATA#) xmlDataString=(#xmlDataString#)">
                <cfset SubjectLine = "SimpleX SMS API Notification">
                <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                <cfset ErrorNumber="1110">
                <cfset AlertType="1">
                               
                <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                    INSERT INTO simplequeue.errorlogs
                    (
                        ErrorNumber_int,
                        Created_dt,
                        Subject_vch,
                        Message_vch,
                        TroubleShootingTips_vch,
                        CatchType_vch,
                        CatchMessage_vch,
                        CatchDetail_vch,
                        Host_vch,
                        Referer_vch,
                        UserAgent_vch,
                        Path_vch,
                        QueryString_vch
                    )
                    VALUES
                    (
                        #ErrorNumber#,
                        NOW(),
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                    )
                </cfquery>
            
            <cfcatch type="any">
            
            
            </cfcatch>
                
            </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
               
		<cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = ""/>
        <cfset dataout.MESSAGE = "#DebugAPI# #CGI.SERVER_NAME# #Session.DBSourceEBM#" /> 
		<cfset dataout.ERRMESSAGE = "" />
                             
		<!---	<cfreturn representationOf(dataout).withStatus(200) /> --->
		<cfreturn representationOf("").withStatus(200) />

	</cffunction>
	
		
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = {} />  
        
		<cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = ""/>
        <cfset dataout.MESSAGE = "You should POST FORM Data, JSON or XML to this API location. Currently you only sent GET!" /> 
		<cfset dataout.ERRMESSAGE = "" />
		            
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
</cfcomponent>