<cfcomponent extends="taffy.core.resource" taffy:uri="/clx_long/mo" hint="MO response inbound for MBlox - CLX Long Codes">

	<cfinclude template="../../paths.cfm" >
	
	<cfset DeliveryServiceComponentPath = "">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    <cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">
    
	<!---MBLox only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get response from MBLox">
		<cfargument name="XMLDATA" TYPE="string" required="no" default=""/>
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataout = {} />
		
		<cfset var NewThreadName = "MOReceiptThread_#CreateUUID()#" />
		
		<cfset dataout.RXRESULTCODE = "0" />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />   
		<cfset dataout.MESSAGE = "NewThreadName=#NewThreadName#" />
		

		<!--- Run processing in thread so as not to backup delivery of messages --->
		<cfthread name="#NewThreadName#" action="run" XMLDATA="#XMLDATA#" inpQAMode="#inpQAMode#">
		
	
			<cfset var mbloxData = '' />
			<cfset var contactString = '' />
			<cfset var transactionId = '' />
			<cfset var carrier = '' />
			<cfset var shortCode = '' />
			<cfset var keyword = '' />
			<cfset var time = '' />
			<cfset var ServiceId = '' />
			<cfset var xmlDataString = '' />
			<cfset var ENA_Message = '' />
			<cfset var SubjectLine = '' />
			<cfset var TroubleShootingTips = '' />
			<cfset var ErrorNumber = '' />
			<cfset var AlertType = '' />
			<cfset var InsertToErrorLog = '' />
			<cfset var RetValQueueNextResponseSMS = '' />
			<cfset var RetValProcessNextResponseSMS = '' />
	        <cfset var DBSourceEBM = "Bishop"/> 
	        <cfset var UDHString = "">	
	        <cfset var RetValCheckForDuplicateMO = "" />
	        <cfset var RetValInsertMessagePart = ''>
	        <cfset var inpContactStringBuff = "" />
	        <cfset var RetValLogDuplicateResponseSMS = '' />
	        <cfset var DebugStr = '' />
	        <cfset var RetValAppendMessagePart = '' />
	 
	  		<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
	        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
	        <cfif inpQAMode GT 0 >
	               
	            <cfset DBSourceEBM = "BishopDev"/> 
	            <cfset Session.DBSourceEBM = "BishopDev"/>  
	                       
	        <cfelse>
	        
	            <cfset DBSourceEBM = "Bishop"/> 
	            <cfset Session.DBSourceEBM = "Bishop"/> 
	                       
	        </cfif>
	        
			<cftry>			
			
				<cfset DebugStr = 'Start MO Processing' />
	                
				<cfset mbloxData = ParseMBloxData(XMLDATA)>
				<cfset contactString = mbloxData.SmsNumber>
				<cfset transactionId = mbloxData.TransactionID>
				<cfset carrier = mbloxData.Operator>
				<cfset shortCode = mbloxData.ShortCode>
				
				<!--- MBlox comes in as  encoding="ISO-8859-1" which still wont store in DB unicode characters - convert data to UTF-8 and later routines will base64 encode it for DB storage --->
				<cfset keyword = CharsetDecode(mbloxData.Data, "UTF-8")>
				<cfset time = mbloxData.Time>            
	            <cfset ServiceId = mbloxData.ServiceId>	
	            <!--- Need to get UDH string from future MBlox --->
	            <cfset UDHString = mbloxData.UDHString>	
	            
	            <cfset inpContactStringBuff = contactString />
				<!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->            
	            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
	                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
	            </cfloop>
	                    
	            
	            <!--- Don't fill DB with empty data --->
	            <cfif TRIM(XMLDATA) NEQ "">
	             
					<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
	                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->
	            
					<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->
	                <cfif 1 EQ 1>
	                
	                	<!--- Log the fact that we processed to the DB --->    
	                    <cfinvoke                    
	                         method="QueueNextResponseSMS"
	                         returnvariable="RetValQueueNextResponseSMS">                         
	                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
	                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
	                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
	                            <cfinvokeargument name="inpPreferredAggregator" value="11"/>
	                            <cfinvokeargument name="inpTime" value="#time#"/>
	                            <cfinvokeargument name="inpScheduled" value=""/>
	                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
	                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
	                            <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
	                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
	                    </cfinvoke>                      
	                                	            
	                	<!--- Validate this is not a duplicate ... --->
	                    <cfinvoke                    
	                         method="CheckForDuplicateMO"
	                         returnvariable="RetValCheckForDuplicateMO">                         
	                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                            
	                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                            <cfinvokeargument name="inpKeyword" value="#keyword#"/> 
	                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/> 
	                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>                         
	                    </cfinvoke>  
	                    
	                    <!--- Check for duplicate --->
	                    <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2 OR RetValCheckForDuplicateMO.RXRESULTCODE EQ 3 >
	                    	
	                        <!--- Flag as duplicate --->
	                    
	                    	<!--- Log Duplicates ignored--->
	                        <cfinvoke                    
	                             method="LogDuplicateResponseSMS"
	                             returnvariable="RetValLogDuplicateResponseSMS">                         
	                                <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
	                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
	                                <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                                <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                                <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
	                                <cfinvokeargument name="inpPreferredAggregator" value="11"/>
	                                <cfinvokeargument name="inpTime" value="#time#"/>
	                                <cfinvokeargument name="inpScheduled" value=""/>
	                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
	                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
	                                <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
	                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
	                        </cfinvoke>  
	                            
	                        <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->
	                        
	                    <cfelse> <!--- Not a duplicate --->
	                    
							<!--- Validate not a concatenated message --->
	                        <cfif LEN(UDHString) EQ 12>
	                         	                                                    
	                            <!--- Only process for next response on first part of message --->
	                            <cfif RIGHT(UDHString, 2 ) EQ "01" >
	                            
	                                <!--- Process next response --->
	                                <!--- component="session.cfc.csc.csc"  ---> 
	                                <cfinvoke                  
	                                     method="ProcessNextResponseSMS"
	                                     returnvariable="RetValProcessNextResponseSMS">                         
	                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
	                                        <cfinvokeargument name="inpCarrier" value="#carrier#"/>
	                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                                        <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
	                                        <cfinvokeargument name="inpPreferredAggregator" value="11"/>
	                                        <cfinvokeargument name="inpTime" value="#time#"/>
	                                        <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
	                                        <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
	                                        <cfinvokeargument name="inpOverrideAggregator" value="1"/>
	                                        <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
	                                        <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
	                                </cfinvoke>
	                                                        
	                                <cfinvoke                    
	                                     method="InsertMessagePart"
	                                     returnvariable="RetValInsertMessagePart">                         
	                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                                        
	                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                                        <cfinvokeargument name="inpSurveyResultsId" value="#RetValProcessNextResponseSMS.LASTSURVEYRESULTID#"/>  
	                                        <cfinvokeargument name="inpReferenceNumber" value="#MID(UDHString, 7, 2 )#"/>
	                                        <cfinvokeargument name="inpTotalParts" value="#MID(UDHString, 9, 2 )#"/>
	                                        <cfinvokeargument name="inpPartNumber" value="#RIGHT(UDHString, 2 )#"/>                                         
	                                </cfinvoke>
	                                
	                                <cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire">
	                                    <cfinvokeargument name="inpUserId" value="#RetValProcessNextResponseSMS.GETRESPONSE.USERID#"/>
	                                    <cfinvokeargument name="inpCredits" value="#MID(UDHString, 9, 2 )#"/>
	                                </cfinvoke>
	                                                                                        
	                                <!--- Check if all parts of the message have arrived yet inside of InsertMessagePart function --->
	                                
	                            <cfelse> <!--- Alternate message part - not part 1 --->
	                            
	                                <!--- Log message MO just dont respond to it --->
	                                
	                                <cfinvoke                    
	                                     method="InsertMessagePart"
	                                     returnvariable="RetValInsertMessagePart">                         
	                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                                        
	                                        <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                                        <cfinvokeargument name="inpReferenceNumber" value="#MID(UDHString, 7, 2 )#"/>
	                                        <cfinvokeargument name="inpTotalParts" value="#MID(UDHString, 9, 2 )#"/>
	                                        <cfinvokeargument name="inpPartNumber" value="#RIGHT(UDHString, 2 )#"/>  
	                                </cfinvoke>                              
	                                
	                                <!--- Check if all parts of the message have arrived yet inside of InsertMessagePart function --->
	                            
	                            </cfif> <!--- Only process for next response on first part of message --->
	                        
	                        <cfelse> <!--- not a concatenated message --->
	                        
	                        	<!--- Look at deduplication result for message within 4 seconds - this may be another message part but not listed as concatenated --->
								 <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 1 AND RetValCheckForDuplicateMO.RECENT GT 0 >
									 
									<!--- Not a true "duplicate" but close enough to treat as an unconcatenated message part --->
									<cfinvoke                    
	                                     method="AppendMessagePart"
	                                     returnvariable="RetValAppendMessagePart">                         
	                                        <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>                                        
	                                        <cfinvokeargument name="inpKeyword" value="#keyword#"/>
									</cfinvoke>   									 
									 
								 <cfelse>
								
									 <!--- Process next response --->
									 <!--- component="session.cfc.csc.csc"  ---> 
		                            <cfinvoke                   
		                                 method="ProcessNextResponseSMS"
		                                 returnvariable="RetValProcessNextResponseSMS">                         
		                                    <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
		                                    <cfinvokeargument name="inpCarrier" value="#carrier#"/>
		                                    <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
		                                    <cfinvokeargument name="inpKeyword" value="#keyword#"/>
		                                    <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
		                                    <cfinvokeargument name="inpPreferredAggregator" value="11"/>
		                                    <cfinvokeargument name="inpTime" value="#time#"/>
		                                    <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
		                                    <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
		                                    <cfinvokeargument name="inpOverrideAggregator" value="1"/>
		                                    <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
		                                    <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>                                    
		                            </cfinvoke>         	 
									 
								 </cfif>	 
	                                                
	<!---                       <cfset DebugStr = DebugStr & ' RetValProcessNextResponseSMS = #SerializeJSON(RetValProcessNextResponseSMS)#' /> --->
	                                                        
	                            <cfinvoke method="DeductCreditsSire" returnvariable="RetDeductCreditsSire">
	                                <cfinvokeargument name="inpUserId" value="#RetValProcessNextResponseSMS.GETRESPONSE.USERID#"/>
	                                <cfinvokeargument name="inpCredits" value="1"/>
	                            </cfinvoke>
	                                        
	                        </cfif> <!--- Validate not a concatenated message --->
	                	
	                    </cfif> <!--- Check for duplicate --->
	                                                
	                <cfelse>
	
						<!--- Queue up future response tracking in DB--->                    
	                    <cfinvoke                    
	                         method="QueueNextResponseSMS"
	                         returnvariable="RetValQueueNextResponseSMS">                         
	                            <cfinvokeargument name="inpContactString" value="#inpContactStringBuff#"/>
	                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
	                            <cfinvokeargument name="inpShortCode" value="#shortCode#"/>
	                            <cfinvokeargument name="inpKeyword" value="#keyword#"/>
	                            <cfinvokeargument name="inpTransactionId" value="#transactionId#"/>
	                            <cfinvokeargument name="inpPreferredAggregator" value="11"/>
	                            <cfinvokeargument name="inpTime" value="#time#"/>
	                            <cfinvokeargument name="inpScheduled" value=""/>
	                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_SECONDARYQUEUE#"/>
	                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
	                            <cfinvokeargument name="inpXMLDATA" value="#XMLDATA#"/>
	                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
	                    </cfinvoke>                                
	                
	            	</cfif>            
	            
	           	</cfif>
	                                    
	        <cfcatch>
	          
	          		<!--- No return for Threaded Model --->   
<!---
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
					<cfset QueryAddRow(dataout) />
			        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
			        <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
			        <cfset QuerySetCell(dataout, "MESSAGE", cfcatch.Message) />                
			        <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
--->
	                        
	                <cftry>        
	                
	                	<cfset xmlDataString = URLDecode(XMLDATA)>
						<cfset ENA_Message = "XMLDATA = (#XMLDATA#) xmlDataString=(#xmlDataString#)">
	                    <cfset SubjectLine = "SimpleX SMS API Notification">
	                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
	                    <cfset ErrorNumber="1110">
	                    <cfset AlertType="1">
	                                   
	                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
	                        INSERT INTO simplequeue.errorlogs
	                        (
	                            ErrorNumber_int,
	                            Created_dt,
	                            Subject_vch,
	                            Message_vch,
	                            TroubleShootingTips_vch,
	                            CatchType_vch,
	                            CatchMessage_vch,
	                            CatchDetail_vch,
	                            Host_vch,
	                            Referer_vch,
	                            UserAgent_vch,
	                            Path_vch,
	                            QueryString_vch
	                        )
	                        VALUES
	                        (
	                            #ErrorNumber#,
	                            NOW(),
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
	                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
	                        )
	                    </cfquery>
	                
	                <cfcatch type="any">
	                
	                
	                </cfcatch>
	                    
	                </cftry>    
	            
                <!--- No return for Threaded Model --->            
				<!--- <cfreturn representationOf(dataout).withStatus(200) /> --->
	        </cfcatch>         
	                
			</cftry>
        
 		</cfthread>        
<!---         <cfset dataout.MESSAGE = "NewThreadName=#NewThreadName# #DebugAPI# #CGI.SERVER_NAME# #Session.DBSourceEBM#" /> --->
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	<cffunction name="ParseMBloxData" output="true" access="private" >
		<cfargument name="XMLDATA" TYPE="string" required="no" default=""/>
		
        
        <!--- Declare return object as local variable - use var in declaration --->
        <cfset var mbloxData = StructNew() />
        <cfset var inpXML = '' />
		<cfset var commonObject = '' />
		<cfset var xmlDataString = '' />
		<cfset var SmsNumber = '' />
		<cfset var TransactionID = '' />
		<cfset var Time = '' />
		<cfset var Data = '' />
		<cfset var Operator = '' />
		<cfset var ShortCode = '' />
		<cfset var MOServiceId = '' />
		<cfset var xmlDoc = '' />
		<cfset var selectedElements = '' />
        <cfset var UDHString = '' />
		<!---
		
		Data coming in has extra info attached ?!?! need to remove.
		<cfset inpXML = replace(#inpXML2#,"XMLDATA=","")>
		
		
		
		Use Taffy POST to test
		Request - Sent by MBLOX to Message Broadcast
		 
		<?xml version="1.0" encoding="ISO??8859??1" standalone="yes"?>
		<ResponseService Version="2.3">
         <Header>
			<Partner>UserName</Partner>
			<Password>Password</Password>
			<ServiceID>1</ServiceID>
	     </Header>
		 <ResponseList>
           	<Response SequenceNumber="1" Type="SMS" Format="Text">
               <TransactionID>215980</TransactionID>
               <OriginatingNumber>9494000553</OriginatingNumber>
               <Time>200412181427</Time>
               <Data>siremobile</Data>
               <Deliverer>1</Deliverer>
               <Destination>28444</Destination>
               <Operator></Operator>
               <Tariff>150</Tariff>
               <SessionId>SessionID</SessionId>
               <Tags>
                   <Tag Name="Number">12</Tag>
                   <Tag Name="City">Rome</Tag>
               </Tags>
		    </Response>
		 </ResponseList>
		</ResponseService>
		--->
		<!---parse xml data from mblox platform--->
		
        
        <!--- xmlDataString = "<?xml version='1.0' encoding='ISO??8859??1' standalone='yes'?><NODATA/>" --->
        
		<cfset commonObject = createObject("component", "#websericeCommonDotPath#.common") />
		
        
        <!--- Contact String coming back with a 1 in it --->
              
              
              <!--- MBlox adds : - remove them --->
              <!--- Add correction logic to get it down to expected 12 characters - case for muli-byte udh id just use last byte only --->
					
              
              
		<cfscript>
			xmlDataString = URLDecode(XMLDATA);		
			xmlDataString = ReplaceNoCase(xmlDataString,"XMLDATA=","");
			
			SmsNumber = "";
			TransactionID = "";
			Time = "";
			Data = "";
			Operator = "";
			ShortCode = "";
			MOServiceId = "";
			UDHString = "";
							
			if(TRIM(xmlDataString) NEQ "")
			{
				xmlDoc = XmlParse(xmlDataString);
												
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/Header/ServiceID");
				if(ArrayLen(selectedElements) GT 0)
				{
					MOServiceId = selectedElements[1].XmlText;
				}
				
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/OriginatingNumber");
				if(ArrayLen(selectedElements) GT 0)
				{
					smsNumber = selectedElements[1].XmlText;
				}
			   
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/TransactionID");
				if(ArrayLen(selectedElements) GT 0)
				{
					TransactionID = selectedElements[1].XmlText;
				}
			   
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/Time");
				if(ArrayLen(selectedElements) GT 0)
				{
					Time = selectedElements[1].XmlText;
				}
								   
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/Data");
				if(ArrayLen(selectedElements) GT 0)
				{
					Data = selectedElements[1].XmlText;
				}
			   
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/Operator");
				if(ArrayLen(selectedElements) GT 0)
				{
					Operator = selectedElements[1].XmlText;
				}
			   
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/Destination");
				if(ArrayLen(selectedElements) GT 0)
				{
					ShortCode = selectedElements[1].XmlText;
				} 	
				
				selectedElements = XmlSearch(xmlDoc, "/ResponseService/ResponseList/Response/Udh");
				if(ArrayLen(selectedElements) GT 0)
				{
					UDHString = selectedElements[1].XmlText;
					
					UDHString = replace(UDHString, ':', '', 'ALL');		
					
					UDHString = RIGHT(UDHString, 12);								
				}
			}			
			
		    mbloxData.SmsNumber = SmsNumber;
        	mbloxData.TransactionID = TransactionID;
			
			if(TRIM(Time) NEQ "")
        		Time = commonObject.ConvertMBLoxTime(Time);
						
			mbloxData.Time= Time;
        	
			mbloxData.Data = Data;
        	mbloxData.Operator = Operator;
        	mbloxData.ShortCode = ShortCode;
        	mbloxData.ServiceId = MOServiceId;
			mbloxData.UDHString = UDHString;
			
        	return mbloxData;
        </cfscript>
	</cffunction>
	
    <cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST JSON or XML to this API location. Currently you only sent GET! ID-003") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
    
</cfcomponent>