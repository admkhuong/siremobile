<cfcomponent extends="taffy.core.resource" taffy:uri="/syniverse/dr" hint="Update status of MT Syniverse">
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../paths.cfm" >
    
    
    
	<!---MBLox only support POST method, with only XMLDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get message status updates from MBLox">
		<cfargument name="XMLDATA" TYPE="string" required="no" default=""/>
		<cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataout = '0' /> 
		<cfset var mbloxData = StructNew() />
		<cfset var nSMSGlobalMessageID = '' />
		<cfset var InsertMTToContactResults = '' />
		<cfset var inpContactStringBuff = "" />
		<cfset var DBSourceEBM = "Bishop"/> 
 
		<cftry>
		
		<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
          
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
           
        </cfif>
        
        
        	<!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
            <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        	<cfset Session.DBSourceEBM = DBSourceEBM />
            
            <cfscript>
	            mbloxData.SubscriberNumber = '';
				mbloxData.SequenceNumber = 0;
	        	mbloxData.Status = 0;
	        	mbloxData.TimeStamp = '2016-01-01 00:00:00';
	        	mbloxData.MsgReference = '';
	        	mbloxData.Reason = '';
	        	mbloxData.BatchId = '';
            </cfscript>
                                  
			<cfset mbloxData = ParseMBloxData(TRIM(XMLDATA))>            
           
            <!--- MBLOX uses ones but EBM does not - messes with time zone and api calls - scrub leading ones here --->  
			<cfset inpContactStringBuff = mbloxData.SubscriberNumber />
			          
            <cfloop condition="LEFT(inpContactStringBuff,1) EQ '1' AND LEN(inpContactStringBuff) GT 1">
                <cfset inpContactStringBuff = RIGHT(inpContactStringBuff, LEN(inpContactStringBuff) - 1) />
            </cfloop>
            
            <cfset mbloxData.SubscriberNumber = inpContactStringBuff />
            
            <!---          
		    mbloxData.SubscriberNumber = SubscriberNumber;
			mbloxData.SequenceNumber = SequenceNumber;
        	mbloxData.Status = Status;
        	mbloxData.TimeStamp = commonObject.ConvertMBLoxTime(TimeStamp);
        	mbloxData.MsgReference = MsgReference;
        	mbloxData.Reason = Reason;
        	mbloxData.BatchId = BatchId;
            --->
                        
            <!---
				???1??? = buffered phone 
				???2??? = buffered smsc 
				???3??? = acked
				???4??? = Delivered
				???5??? = Non Delivered 
				???6??? = Lost Notification
			--->

			<cfset nSMSGlobalMessageID = mbloxData.BatchID & numberFormat(mbloxData.SequenceNumber,'00000')>
          						
			<!---Insert MT to simplexresults.contactresults--->
			<cfquery name="InsertMTToContactResults" datasource="#DBSourceEBM#">
				INSERT INTO simplexresults.smsdisposition
				(
	                SubscriberNumber_vch,					
                    Time_vch,
                    Status_vch,
                    Status_ti,
                    MsgReference_vch,
                    Reason_vch,
                    ShortCode_vch,
                    nSMSGlobalMessageID_vch,
                    SequenceNumber_vch,
                    SecondarySequenceNumber_vch,
                    RawData_vch,
                    AggregatorId_int,
                    Created_dt
				)
				VALUES
				(
	                <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.SubscriberNumber#">,								
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.TimeStamp#">, <!---Time_dt--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.Status#">,<!---Status_ti--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="0">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.MsgReference#">,<!---MsgReference_vch--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.Reason#">,<!---Reason_vch--->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#nSMSGlobalMessageID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.SequenceNumber#">,		
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#mbloxData.BatchId#">,          
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#URLDecode(XMLDATA)#">, 
                    62569,                   
					NOW()
				)
			</cfquery>
            
			<cfcatch>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
				<cfset QueryAddRow(dataout) />
		        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		        <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
		        <cfset QuerySetCell(dataout, "MESSAGE", cfcatch.Message) />                
		        <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
		        
				<cfquery name="InsertMTToContactResults" datasource="#DBSourceEBM#">
					INSERT INTO simplexresults.smsdisposition
					(
		                
	                    Status_ti,
	                    ShortCode_vch,
	                    Reason_vch,
					    RawData_vch,
	                    AggregatorId_int,
	                    Created_dt
					)
					VALUES
					(	               
	                    0,
	                    '',        
	                    '#LEFT(SerializeJSON(cfcatch),512)#',                   
	                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#URLDecode(XMLDATA)#">,
	                    62569,                   
						NOW()
					)
										
				</cfquery>

		                    
				<cfreturn representationOf(dataout).withStatus(200) />
			</cfcatch>
		</cftry>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	<cffunction name="ParseMBloxData" output="true" access="private" >
		<cfargument name="XMLDATA" TYPE="string" required="no" default=""/>	
        
        <!--- Declare return object as local variable - use var in declaration --->
        <cfset var mbloxData = StructNew() />
		<cfset var commonObject = '' />
		<cfset var xmlDataString = '' />
		<cfset var xmlDoc = '' />
		<cfset var SequenceNumber = '' />
		<cfset var BatchID = '' />
		<cfset var SubscriberNumber = '' />
		<cfset var Status = '' />
		<cfset var TimeStamp = '' />
		<cfset var Reason = '' />
		<cfset var MsgReference = '' />
		<cfset var selectedElements = '' />
        	
		<!---
		NotificationService - Sent by MBLOX to Message Broadcast
		
		<NotificationService Version="2.3">
		<Header>
		    <Partner>AccountName</Partner>
		    <Password>Password</Password>
		    <ServiceID>1</ServiceID>
		</Header>
		<NotificationList>
		    <Notification BatchID="1046" SequenceNumber="1">
		        <Subscriber>
		            <SubscriberNumber>8443565656</SubscriberNumber>
		            <Status>1</Status>
		            <TimeStamp>201304061416</TimeStamp>
		            <MsgReference>message 1</MsgReference>
		            <Reason>3</Reason>
		        </Subscriber>
		    </Notification>
		</NotificationList>
		</NotificationService>
		--->
		<cfset commonObject = createObject("component", "#websericeCommonDotPath#.common") />
		
		<!---parse xml data from mblox platform--->
		<cfscript>
			xmlDataString = URLDecode(XMLDATA);
			xmlDataString = ReplaceNoCase(xmlDataString,"XMLDATA=","");
        	xmlDoc = XmlParse(xmlDataString);
        	
        	SequenceNumber = "";
        	BatchID = "";
        	SubscriberNumber = "";
        	Status = 0;
        	TimeStamp = "2016-01-01 00:00:00";
        	Reason = "";
			MsgReference = "";			
						       	
        	/*get SubscriberNumber*/
        	selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/Subscriber/SubscriberNumber");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        SubscriberNumber = selectedElements[1].XmlText;
		    }
		   	/*get SequenceNumber*/
       		selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        SequenceNumber = selectedElements[1].XmlAttributes.SequenceNumber;
		    }
		   	/*get TimeStamp*/
		    selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/Subscriber/TimeStamp");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        TimeStamp = selectedElements[1].XmlText;
		    }
		    /*get Status*/                   
		    selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/Subscriber/Status");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        Status = selectedElements[1].XmlText;
		    }
		   	/*get Reason*/
		    selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/Subscriber/Reason");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        Reason = selectedElements[1].XmlText;
		    }
			/*get MsgReference*/
		    selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/Subscriber/MsgReference");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        MsgReference = selectedElements[1].XmlText;
		    }
		   
		   	/*get batchid*/
		    selectedElements = XmlSearch(xmlDoc, "/NotificationService/NotificationList/Notification/");
		    if(ArrayLen(selectedElements) GT 0)
		    {
		        BatchId = selectedElements[1].XmlAttributes.BatchID;
		    }
		  		  
		    mbloxData.SubscriberNumber = SubscriberNumber;
			mbloxData.SequenceNumber = SequenceNumber;
        	mbloxData.Status = Status;
        	mbloxData.TimeStamp = commonObject.ConvertMBLoxTime(TimeStamp);
        	mbloxData.MsgReference = MsgReference;
        	mbloxData.Reason = Reason;
        	mbloxData.BatchId = BatchId;
        	
        	return mbloxData;
        </cfscript>
	</cffunction>
	
	<cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST JSON or XML to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>

</cfcomponent>