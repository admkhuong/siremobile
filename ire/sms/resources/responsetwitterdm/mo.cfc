<cfcomponent extends="taffy.core.resource" taffy:uri="/responsetwitterdm/mo" hint="MO Indbound for Twitter Direct Messages">
	<cfinclude template="../../paths.cfm" >
    
	<cfset DeliveryServiceComponentPath = "">
    <cfset CommonDotPath = "ire.sms.common"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->
	<cfinclude template="../../../../session/cfc/csc/constants.cfm" > <!--- Use one SMS constants in EBM so as not to confuse or miss any --->
	<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
	<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
    
    <!--- Put the below code in the MO capture service
	
			<!--- pass undefined keywords on to EBM via GetHTTPRequestData().content--->
            <cftry>
                 
                <!--- Development is in EBMII and Production is EBM only --->                
                <cfhttp url="http://ebmii.messagebroadcast.com/ire/sms/Response288/mo" method="POST" result="returnStruct" >
                    <cfhttpparam type="formfield" name="JSONDATA" value="#GetHTTPRequestData().content#">	
                </cfhttp>
                
            <cfcatch type="any">
                
            </cfcatch>
            
            </cftry> 
	
	--->
    
    
    <!--- 
		*** JLP - Dont kill the DB looking this up but....
		Option to ignore MO if same as last MO AND no new MT sent since duplicate MO 
		
	--->
    
    
	<!---Twitter DM only support POST method, with only JSONDATA parameter--->
	<cffunction name="post" access="public" output="false" hint="Get response from Twitter DM">
		<cfargument name="screen_name" TYPE="string" required="yes" />
        <cfargument name="inpShortCode" TYPE="string" required="yes" />
        <cfargument name="inpkeyword" TYPE="string" required="yes" />
        <cfargument name="inpTransactionId" TYPE="string" required="no" default=""/>
        <cfargument name="JSONDATA" TYPE="string" required="no" default=""/>
        <cfargument name="inpQAMode" TYPE="string" required="no" default="0"/>
		
		<cfset var dataout = '0' />  
		<cfset var TwitterDMData = '' />
		<cfset var contactString = '' />
		<cfset var transactionId = '' />
		<cfset var carrier = '' />
		<cfset var shortCode = '' />
		<cfset var keyword = '' />
		<cfset var time = '' />
		<cfset var ServiceId = '' />
        <cfset var UDHString = '' />
		<cfset var JSONDATAString = '' />
		<cfset var ENA_Message = '' />
		<cfset var SubjectLine = '' />
		<cfset var TroubleShootingTips = '' />
		<cfset var ErrorNumber = '' />
		<cfset var AlertType = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var RetValQueueNextResponseSMS = '' />
		<cfset var RetValProcessNextResponseSMS = '' />
        <cfset var RetValCheckForDuplicateMO = '' />
        <cfset var RetValInsertMessagePart = ''>
        <cfset var DBSourceEBM = "Bishop"/> 
        
        <cfset var DebugStr = "Go Debug" />
        
        
        
        <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
        <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        <cfif inpQAMode GT 0 >
               
            <cfset DBSourceEBM = "BishopDev"/> 
            <cfset Session.DBSourceEBM = "BishopDev"/>     
        
        <cfelse>
        
            <cfset DBSourceEBM = "Bishop"/> 
            <cfset Session.DBSourceEBM = "Bishop"/> 
           
        </cfif>


		<cftry>
           
			            
            <!--- For debugging only - comment out in production --->  
          	<!---
				<cfmail to="jpeterson@messagebroadcast.com;" from="it@messagebroadcast.com" subject="AT&T MO Received" type="html">
					<cfdump var="#JSONDATA#">
					<cfdump var="#TwitterDMData#">               
				</cfmail> 
			--->
           	<!--- <cfreturn representationOf(dataout).withStatus(200) />--->
            			
			<cfset carrier = "TWITTERDM">			
			
            
             
				<!--- Check for instant process for now - still need processer externally SMSQCODE_READYTOPROCESS OR SMSQCODE_PROCESSED --->
                <!--- Check if TYPE is INTERVAL or API or .... and force into queue --->
            
				<!--- Process here if allowed by BR - this will slow down rate of messages accepted but can be offset with load balancers and DB mirrors --->
                <cfif 1 EQ 1>
                
                	<!--- Log the fact that we processed to the DB --->    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#screen_name#"/>
                            <cfinvokeargument name="inpScrubContactString" value="0"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#inpkeyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="6"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                            <cfinvokeargument name="inpServiceId" value=""/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>  
                    
                    <cfset DebugStr = DebugStr & " #SerializeJSON(RetValQueueNextResponseSMS)#" />
                                        
                    <!--- Validate this is not a duplicate ... --->
                    <cfinvoke                    
                         method="CheckForDuplicateMO"
                         returnvariable="RetValCheckForDuplicateMO">                         
                            <cfinvokeargument name="inpContactString" value="#screen_name#"/>                            
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#inpkeyword#"/> 
                            <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>                          
                    </cfinvoke>  
                    
                    <!--- Check for duplicate --->
                    <cfif RetValCheckForDuplicateMO.RXRESULTCODE EQ 2>
                    	
                        <!--- Flag as duplicate --->
                    
                    	<!--- Log Duplicates ignored--->
                        <cfinvoke                    
                             method="LogDuplicateResponseSMS"
                             returnvariable="RetValQueueNextResponseSMS">                         
                                <cfinvokeargument name="inpContactString" value="#screen_name#"/>
                                <cfinvokeargument name="inpScrubContactString" value="0"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#inpkeyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="6"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpScheduled" value=""/>
                                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED_ON_MO#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>  
                            
                        <!--- Store result ???  BATCHID ??? LOOKUP LAST BATCH???--->
                        
                    <cfelse> <!--- Not a duplicate --->
                    					
						<!--- Process next response --->
                        <cfinvoke                    
                             method="ProcessNextResponseSMS"
                             returnvariable="RetValProcessNextResponseSMS">                         
                                <cfinvokeargument name="inpContactString" value="#screen_name#"/>
                                <cfinvokeargument name="inpScrubContactString" value="0"/>
                                <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                                <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                                <cfinvokeargument name="inpKeyword" value="#inpkeyword#"/>
                                <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                                <cfinvokeargument name="inpPreferredAggregator" value="6"/>
                                <cfinvokeargument name="inpTime" value="#time#"/>
                                <cfinvokeargument name="inpServiceId" value="#ServiceId#"/> <!--- MO SERVICEID NOT SAME AS MT ONE USED TO SEND ??? --->
                                <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                                <cfinvokeargument name="inpOverrideAggregator" value="6"/>
                                <cfinvokeargument name="inpLastQueuedId" value="#RetValQueueNextResponseSMS.LASTQUEUEDUPID#"/>
                                <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                        </cfinvoke>
                        
                         <cfset DebugStr = DebugStr & " #SerializeJSON(RetValProcessNextResponseSMS)#" />
                	
                    </cfif> <!--- Check for duplicate --->
                                
                <cfelse>

					<!--- Queue up future response tracking in DB--->                    
                    <cfinvoke                    
                         method="QueueNextResponseSMS"
                         returnvariable="RetValQueueNextResponseSMS">                         
                            <cfinvokeargument name="inpContactString" value="#screen_name#"/>
                            <cfinvokeargument name="inpScrubContactString" value="0"/>
                            <cfinvokeargument name="inpCarrier" value="#carrier#"/>
                            <cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
                            <cfinvokeargument name="inpKeyword" value="#inpkeyword#"/>
                            <cfinvokeargument name="inpTransactionId" value="#inpTransactionId#"/>
                            <cfinvokeargument name="inpPreferredAggregator" value="6"/>
                            <cfinvokeargument name="inpTime" value="#time#"/>
                            <cfinvokeargument name="inpScheduled" value=""/>
                            <cfinvokeargument name="inpQueueState" value="#SMSQCODE_SECONDARYQUEUE#"/>
                            <cfinvokeargument name="inpServiceId" value="#ServiceId#"/>
                            <cfinvokeargument name="inpXMLDATA" value="#JSONDATA#"/>
                            <cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>
                    </cfinvoke>                                
                
            	</cfif>            
            
           
                                    
        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.Message# #inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail) />  
                        
                <cftry>        
                
                	<cfset JSONDATAString = URLDecode(JSONDATA)>
					<cfset ENA_Message = "JSONDATA = (#JSONDATA#) JSONDATAString=(#JSONDATAString#)">
                    <cfset SubjectLine = "SimpleX SMS API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="1110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(200) />
        </cfcatch>         
                
		</cftry>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "#inpQAMode# #DebugAPI# #CGI.SERVER_NAME# #DBSourceEBM# #screen_name# #inpkeyword# #inpShortCode# #DebugStr#") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	
	
	
</cfcomponent>


