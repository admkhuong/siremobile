<cfcomponent extends="taffy.core.resource" taffy:uri="/sgresults/delivery" hint="Send Grid Delivery Results">
	<cfinclude template="../../paths.cfm" >
			
	<!--- Data is posted as JSON string in the body from SendGrid--->
	<cffunction name="post" access="public" output="false" hint="Get response from Send Grid">
				
        <cfset var DebugStr = "" />     
        <cfset var dataout = '1' />
        <cfset var incomingData = "" /> 
        <cfset var djs = "" /> 
        <cfset var ENA_Message = "" /> 
        <cfset var SubjectLine = "" /> 
        <cfset var TroubleShootingTips = "" /> 
        <cfset var ErrorNumber = "" /> 
        <cfset var AlertType = "" /> 
        <cfset var InsertToErrorLog = "" /> 
                
		<cftry>
			             
                         
            <!--- Todo: How in the heck is taffy killing session between paths.cfm and here ?!?--->
            <!--- Bandaid fix so incluses have a Session.DBSourceEBM--->
        	<cfset Session.DBSourceEBM = DBSourceEBM />
                              
            <!--- Data is posted as JSON string in the body from SendGrid--->
            <cfset incomingData = toString(getHttpRequestData().content) />

			<cfset djs = arrayNew(1) />
        
        	<!--- Make sure string is not blank before trying to de-serialize JSON --->
            <cfif TRIM(incomingData) NEQ "">	
        
                <cfset djs = DeserializeJSON(incomingData)/>
                
            </cfif>
                
            <cfset var DTSIDVal = "0"/>
            <cfset var UUIDVal = ""/>
            <cfset var attemptVal = "0"/>
            <cfset var categoryVal = "0"/>
            <cfset var emailVal = ""/>
            <cfset var eventVal = ""/>
            <cfset var responseVal = ""/>
            <cfset var smtpIDVal = ""/>
            <cfset var TGtimestampVal = ""/>
            <cfset var sgTimeStampVal = "#NOW()#"/>
            <cfset var typeVal = ""/>
            <cfset var reasonVal = ""/>
            <cfset var statusVal = ""/>
            <cfset var i = ''/>
                        
            <cfif IsArray(djs)>            
                
                <cfloop from="1" to="#arraylen(djs)#" index="i">
                    
                    <cfset DTSIDVal = "0">
                    <cfset UUIDVal = "">
                    <cfset attemptVal = "0">
                    <cfset categoryVal = "0">
                    <cfset emailVal = "">
                    <cfset eventVal = "">
                    <cfset responseVal = "">
                    <cfset smtpIDVal = "">
                    <cfset TGtimestampVal = "">
                    <cfset sgTimeStampVal = "#NOW()#">
                    <cfset typeVal = "">
                    <cfset reasonVal = "">
                    <cfset statusVal = "">                    
                    
                    <cfset DebugStr = DebugStr & "Loop Inside "  />
                    
                    <cfif StructKeyExists(djs[i],'DTSId')>
                        <cfset DTSIDVal  	   = djs[i].DTSId />
                    </cfif>
                
                    <cfif StructKeyExists(djs[i],'UUID')>
                        <cfset UUIDVal	       = djs[i].UUID />
                    </cfif>
                    <cfif StructKeyExists(djs[i],'attempt')>
                        <cfset attemptVal 	   = djs[i].attempt />
                    </cfif>
                    <cfif StructKeyExists(djs[i],'category')>
                        <cfset categoryVal	   = djs[i].category />
                    </cfif>     
                    <cfif StructKeyExists(djs[i],'email')>
                        <cfset emailVal 	   = djs[i].email />
                    </cfif>
                   <cfif StructKeyExists(djs[i],'event')>
                        <cfset eventVal	       = djs[i].event />
                    </cfif>
                    <cfif StructKeyExists(djs[i],'response')>
                        <cfset responseVal	   = djs[i].response />
                    </cfif>
                    
                    <cfif StructKeyExists(djs[i],'timestamp')>
                        {                                                                         
                        <cfset TGtimestampVal  = djs[i].timestamp />
                        <cfset sgTimeStampVal  = dateAdd("s", TGtimestampVal, createDateTime(1970, 1, 1, 0, 0, 0))/>
                        }
                    </cfif>
                    <cfif StructKeyExists(djs[i],'type')>
                        <cfset typeVal         = djs[i].type />
                    </cfif>
                    <cfif StructKeyExists(djs[i],'reason')>
                        <cfset reasonVal       = djs[i].reason />
                    </cfif>
                    <cfif StructKeyExists(djs[i],'status')>
                        <cfset statusVal       = djs[i].status /> 
                    </cfif>
                    
                    <cfset DebugStr = DebugStr & "SP Write Start "  />
                    
                    <cfstoredproc procedure="simplexresults.usp_sg_ins_emailv3" datasource="#DBSourceEBM#">
                        <cfprocparam cfsqltype="cf_sql_bigint" value="#DTSIDVal#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(UUIDVal,199)#">
                        <cfprocparam cfsqltype="cf_sql_integer" value="#attemptVal#">
                        <cfprocparam cfsqltype="cf_sql_integer" value="#categoryVal#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(emailVal,199)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(eventVal,199)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(responseVal,499)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(smtpIDVal,199)#">
                        <cfprocparam cfsqltype="cf_sql_timestamp" value="#sgTimeStampVal#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(typeVal,199)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#LEFT(reasonVal,499)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="#left(statusVal,99)#">
                        <cfprocparam cfsqltype="cf_sql_varchar" value="10.26.0.19">
                    </cfstoredproc>
                                
                    <cfset DebugStr = DebugStr & "SP Write End "  />
                                
                </cfloop>
			
            <cfelse>
            
            	<cfset DTSIDVal = "0">
				<cfset UUIDVal = "">
                <cfset attemptVal = "0">
                <cfset categoryVal = "0">
                <cfset emailVal = "">
                <cfset eventVal = "">
                <cfset responseVal = "">
                <cfset smtpIDVal = "">
                <cfset TGtimestampVal = "">
                <cfset sgTimeStampVal = "#NOW()#">
                <cfset typeVal = "">
                <cfset reasonVal = "">
                <cfset statusVal = "">                    
                
                <cfset DebugStr = DebugStr & "Loop Inside "  />
                
                <cfif StructKeyExists(djs,'DTSId')>
                    <cfset DTSIDVal  	   = djs.DTSId />
                </cfif>
            
                <cfif StructKeyExists(djs,'UUID')>
                    <cfset UUIDVal	       = djs.UUID />
                </cfif>
                <cfif StructKeyExists(djs,'attempt')>
                    <cfset attemptVal 	   = djs.attempt />
                </cfif>
                <cfif StructKeyExists(djs,'category')>
                    <cfset categoryVal	   = djs.category />
                </cfif>     
                <cfif StructKeyExists(djs,'email')>
                    <cfset emailVal 	   = djs.email />
                </cfif>
               <cfif StructKeyExists(djs,'event')>
                    <cfset eventVal	       = djs.event />
                </cfif>
                <cfif StructKeyExists(djs,'response')>
                    <cfset responseVal	   = djs.response />
                </cfif>
                
                <cfif StructKeyExists(djs,'timestamp')>
                    {                                                                         
                    <cfset TGtimestampVal  = djs.timestamp />
                    <cfset sgTimeStampVal  = dateAdd("s", TGtimestampVal, createDateTime(1970, 1, 1, 0, 0, 0))/>
                    }
                </cfif>
                <cfif StructKeyExists(djs,'type')>
                    <cfset typeVal         = djs.type />
                </cfif>
                <cfif StructKeyExists(djs,'reason')>
                    <cfset reasonVal       = djs.reason />
                </cfif>
                <cfif StructKeyExists(djs,'status')>
                    <cfset statusVal       = djs.status /> 
                </cfif>
                
                <cfset DebugStr = DebugStr & "SP Write Start "  />
                
                <cfstoredproc procedure="simplexresults.usp_sg_ins_emailv3" datasource="#DBSourceEBM#">
                    <cfprocparam cfsqltype="cf_sql_bigint" value="#DTSIDVal#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(UUIDVal,199)#">
                    <cfprocparam cfsqltype="cf_sql_integer" value="#attemptVal#">
                    <cfprocparam cfsqltype="cf_sql_integer" value="#categoryVal#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(emailVal,199)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(eventVal,199)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(responseVal,499)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(smtpIDVal,199)#">
                    <cfprocparam cfsqltype="cf_sql_timestamp" value="#sgTimeStampVal#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(typeVal,199)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#LEFT(reasonVal,499)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#left(statusVal,99)#">
                    <cfprocparam cfsqltype="cf_sql_varchar" value="#DBSourceEBM#">
                </cfstoredproc>
                            
                <cfset DebugStr = DebugStr & "SP Write End "  />
            
            </cfif>
            
            	                                    
        <cfcatch>
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", cfcatch.Type) />
            <cfset QuerySetCell(dataout, "MESSAGE", cfcatch.Message) />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", cfcatch.Detail & "#DebugStr# #DBSourceEBM#  #toString(getHttpRequestData().content)#" ) />  
                        
                <cftry>        
                
					<cfset ENA_Message = "JSON = #toString(getHttpRequestData().content)# ">
                    <cfset SubjectLine = "SimpleX eMail Send Grid Results API Notification">
                    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
                    <cfset ErrorNumber="2110">
                    <cfset AlertType="1">
                                   
                    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                        )
                    </cfquery>
                
                <cfcatch type="any">
                
                
                </cfcatch>
                    
                </cftry>    
            
            <cfreturn representationOf(dataout).withStatus(201) />
        </cfcatch>         
                
		</cftry>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "#DebugAPI# #CGI.SERVER_NAME#") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>
	    
   
	<cffunction name="get" access="public" output="false" hint="Delivery GET Warning">
		        
        <cfset var dataout = '0' />  
        
        <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
		<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
        <cfset QuerySetCell(dataout, "MESSAGE", "You should POST JSON to this API location. Currently you only sent GET!") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                    
                    
		<cfreturn representationOf(dataout).withStatus(200) />
	</cffunction>

</cfcomponent>