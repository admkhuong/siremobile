<cfcomponent extends="taffy.core.api">

    <!--- Set up the application. --->
    <cfset THIS.Name = "SireAPIToolsApplication" />
    <cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
    <cfset THIS.SessionManagement = true />
    <cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 5, 0 ) />
    <cfset THIS.SetClientCookies = false />
    <cfset THIS.clientmanagement = false />
    <cfset THIS.setdomaincookies = false />
    <cfset THIS.setdomaincookies = false />

    <cfinclude template="paths.cfm" >

	<!---
		variables.framework = {};
		variables.framework.debugKey = "debug";
		variables.framework.reloadKey = "reload";
		variables.framework.reloadPassword = "true";
		variables.framework.representationClass = "com.ebm.core.genericRepresentation";
		variables.framework.returnExceptionsAsJson = true;

	--->

    <!---session.skey=headers["authorization"];--->

	<cfscript>

		this.name = hash(getCurrentTemplatePath());

		// Need this as of taffy 3.1 and later
		this.mappings["/resources"] = listDeleteAt(cgi.script_name, listLen(cgi.script_name, "/"), "/") & "/resources";


		////variables.framework = {};
		////variables.framework.debugKey = "debug";
		// variables.framework.reloadKey = "reload";
		// variables.framework.reloadPassword = "true";
		// variables.framework.representationClass = "taffy.core.genericRepresentation";
		// variables.framework.returnExceptionsAsJson = true;

		////variables.framework.exceptionLogAdapter = "taffy.bonus.LogToDevNull";

		////variables.framework.reloadPassword = "secret";

		//variables.framework = {};
		//variables.framework.representationClass = "resources.CustomRepresentationClass";


		variables.framework = {};
		// Name of the url parameter that enables CF Debug Output.
		variables.framework.debugKey = "debug";

		<!---// This lets us define multiple send and return formats --->
		variables.framework.serializer = "#websericeCommonDotPath#.multiformatserializerclass";
		variables.framework.deserializer = "#websericeCommonDotPath#.multiformatdeserializerclass";

		// Turns off error logging
		variables.framework.exceptionLogAdapter = "taffy.bonus.LogToDevNull";


          // variables.framework.mimeExtensions = {json = "application/json"};

		// Requires this key to reload config
		// variables.framework.reloadPassword = "secret";


		// do your onApplicationStart stuff here
		function onApplicationStart()
		{
			//application.JsonUtil = createObject("component", "resources.JSONUtil.JSONUtil");
			//application.AnythingToXML = createObject("component", "resources.AnythingToXML.AnythingToXML");
			super.onApplicationStart();

		}

		// do your onRequestStart stuff here
		function onRequestStart(targetPath)
		{
			super.onRequestStart(arguments.targetPath);
	    }


		// this function is called after the request has been parsed and all request details are known
		function onTaffyRequest(verb, cfc, requestArguments, mimeExt){
			// this would be a good place for you to check API key validity and other non-resource-specific validation
			return true;
		}

	</cfscript>
</cfcomponent>
