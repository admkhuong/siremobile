<cfcomponent extends="taffy.core.baseDeserializer">

    <cffunction name="getFromJson"
    output="false"
    taffy:mime="application/json,text/json"
    hint="get data from json">
        <cfargument name="body" hint="the textual request body" />
        <cfset var data = 0 />
        <cfset var response = {} />

        <cfif not isJson(arguments.body)>
            <cfset throwError(msg="Input JSON is not well formed", statusCode="400") />
        </cfif>
        <cfset data = deserializeJSON(arguments.body) />
        <cfif not isStruct(data)>
            <cfset response['_body'] = data />
        <cfelse>
            <cfset response = data />
            <cfset response.JSONDATA = arguments.body />
        </cfif>

        <cfreturn response />
    </cffunction>


    <cffunction name="getFromXML"
    output="false"
    taffy:mime="application/xml,text/xml"
    hint="get xml text as a variable anmed XMLData from xml">
        <cfargument name="body" hint="the textual request body" />
        <cfset var data = 0 />
        <cfset var response = {} />

		<!---
            All data that is posted to this page will now
            be part of the HTTP Request data structure. Get
            a reference to this structure.
        --->
        <cfset var objRequest = GetHttpRequestData() />

        <!---
            Our XML data will be the content of the request.
            Let's grab it out of the request structure. When
            we do this, trim the value to help create a
            valid XML string.
        --->
        <cfset var strXML = objRequest.Content.Trim() />

		<cfset response.XMLDATA = strXML />

        <cfreturn response />
    </cffunction>

</cfcomponent>
