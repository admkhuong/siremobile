<cfcomponent extends="taffy.core.baseSerializer" output="false">

	<cfset variables.jsonUtil = createObject("component", "ire.sms.common.JSONUtil.JSONUtil") />
	<cfset variables.AnythingToXML = createObject("component", "ire.sms.common.AnythingToXML.AnythingToXML") />

    <cffunction
        name="getAsJson"
        output="false"
        taffy:mime="application/json;text/json"
        taffy:default="true"
        hint="serializes data as JSON">
            <cfreturn variables.jsonUtil.serializeToJSON(variables.data) />
    </cffunction>

    <cffunction name="getAsXML" taffy:mime="application/xml;text/xml" output="false">
		<cfreturn variables.AnythingToXML.ToXML(variables.data) />
	</cffunction>

     <cffunction
        name="getAsHTML"
        output="false"
        taffy:mime="text/html;application/html;"
        taffy:default="true"
        hint="serializes data as JSON">
            <cfreturn variables.jsonUtil.serializeToJSON(variables.data) />
    </cffunction>

</cfcomponent>
