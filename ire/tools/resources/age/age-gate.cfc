<cfcomponent extends="taffy.core.resource" taffy:uri="/age-gate" output="false">

	<cfset application.Env = true /> <!--- Access to session/sire resources is looking for this variable to be set - True is production --->
	<cfinclude template="/ire/tools/paths.cfm" >


	<!--- 	<cfinclude template="../../../../../session/cfc/csc/constants.cfm"> --->

	<!--- Overrides remote component request so method can be called locally - helps to eliminates cross session varibale scope issues --->
	<cfset DeliveryServiceComponentPath = "">
	<cfset DeliveryServiceMCIDDMsComponentPath="">


	<!--- ************************************************************************************************************************* --->
    	<!--- Age Gate - Verify User meets minimumn age requirements --->
	<!--- CF converts inp string to a date - accept a variety of possible formats --->
     <!--- ************************************************************************************************************************* --->

 	<cffunction name="post" access="public" output="false" hint="Verify input Birthday and return age.">
		<cfargument name="inpBDay" required="true" type="string" default="0" hint="The Birthday to check - CF converts inp string to a date - accept a variety of possible formats (MM/DD/YYYY) (08/15/1993) (08-15-1993) (08.15.1993) (1993/08) (1993-08) (1993.08)(Aug 15, 1993) Aug 1993">

		<cfargument name="DebugAPI" required="no" default="0">

		<!---  Any extra fields will need to be on URL or FORM scope --->

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.INPBDAY = "#inpBDay#" />
		<cfset dataout.AGE = "0" />
		<cfset dataout.SOURCE = "#CGI.REMOTE_ADDR#" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var DebugStr = "" />

		<!--- Set the default Session DB Source --->
		<cfset Session.DBSourceEBM = "Bishop"/>
		<cfset Session.COMPANYUSERID = "Session.UserId"/>

 		<cftry>

			<cfif IsDate(inpBDay) >

				<cfset dataout.AGE = DateDiff('yyyy', inpBDay , now()) />

				<cfset dataout.INPBDAY = "#inpBDay#" />

				<cfset dataout.RXRESULTCODE = 1 />
			     <cfset dataout.MESSAGE = "" />

			<cfelse>


				<cfset dataout.RXRESULTCODE = 0 />
			     <cfset dataout.MESSAGE = "Not a recognizable date" />

			</cfif>

        <cfcatch type="any">

		  <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>

            <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />

            <cfif DebugAPI GT 0>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />
            <cfelse>
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            </cfif>

        </cfcatch>

        </cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>

	<cffunction name="get" access="public" output="false" hint="Verify input Birthday and return age.">
		<cfargument name="inpBDay" required="true" type="string" default="0" hint="The Birthday to check - CF converts inp string to a date - accept a variety of possible formats (MM/DD/YYYY) (08/15/1993) (08-15-1993) (08.15.1993) (1993/08) (1993-08) (1993.08)(Aug 15, 1993) Aug 1993">

		<cfargument name="DebugAPI" required="no" default="0">

		<!---  Any extra fields will need to be on URL or FORM scope --->

		<cfset var dataOut = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.INPBDAY = "#inpBDay#" />
		<cfset dataout.AGE = "0" />
		<cfset dataout.SOURCE = "#CGI.REMOTE_ADDR#" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cfset var DebugStr = "" />

		<!--- Set the default Session DB Source --->
		<cfset Session.DBSourceEBM = "Bishop"/>
		<cfset Session.COMPANYUSERID = "Session.UserId"/>

		<cftry>

			<cfif IsDate(inpBDay) >

				<cfset dataout.AGE = DateDiff('yyyy', inpBDay , now()) />

				<cfset dataout.INPBDAY = "#inpBDay#" />

				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.MESSAGE = "" />

			<cfelse>


				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = "Not a recognizable date" />

			</cfif>

		<cfcatch type="any">

		  <cfif cfcatch.errorcode EQ "">
			 <cfset cfcatch.errorcode = -1>
		  </cfif>

		  <cfset dataout.STATUSCODE = "#cfcatch.errorcode#" />
		  <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		  <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#" />

		  <cfif DebugAPI GT 0>
			 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE# #DebugStr# #Session.DBSourceEBM#" />
		  <cfelse>
			 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		  </cfif>

		</cfcatch>

		</cftry>

		<cfreturn representationOf(dataout).withStatus(200) />


    </cffunction>




</cfcomponent>
