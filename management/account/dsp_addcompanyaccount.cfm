<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
<cfset companyList="companyList">
<script TYPE="text/javascript">
	function AddNewCompanyAccount()
	{			
					
		$("#loadingDlgAddNewCompanyAccount").show();		
	
		if($("#inpPhone").val() != $("#inpVerifiedPhone").val())
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Company phone don't match - please retype.\n", "Failure!", function(result) { $("#loadingDlgAddNewCompanyAccount").hide(); return false; } );							
			return false;	
		}
		
		if($("#inpCompanyName").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a company name.\n", "Failure!", function(result) { $("#loadingDlgAddNewCompanyAccount").hide(); return false; } );							
						
			return false;	
		}
		

		
		if($("#inpPhone").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgAddNewCompanyAccount").hide(); return false; } );							
						
			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/company.cfc?method=AddNewCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpCompanyName : $("#AddNewCompanyAccountDiv #inpCompanyName").val(), inpPhone : $("#AddNewCompanyAccountDiv #inpPhone").val() }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Company Account has been added.", "Success", function(result) 
																			{ 
																				$("#loadingDlgAddNewCompanyAccount").hide();
																				AddNewCompanyAccountDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;

	}	
	
	
	$(function()
	{	
	
		$("#RegisterNewAccountDiv #RegisterNewAccountButton").click( function() { RegisterNewAccount(); return false;  });
		$("#AddNewCompanyAccountDiv #AddNewCompanyAccountButton").click( function() { AddNewCompanyAccount(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#AddNewCompanyAccountDiv #Cancel").click( function() 
			{
					$("#loadingDlgAddNewCompanyAccount").hide();					
					AddNewCompanyAccountDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgAddNewCompanyAccount").hide();	
		  
		  $("#inpNAPassword").keyup( function(event){  
//		         cur_val = $(this).val(); // grab what's in the field       							    
				 PasswordStrength($(this).val());
//				 $(this).val(cur_val);    
		  } );
	} );



function PasswordStrength(password)  
{  
	var desc = new Array();  
	desc[0] = "No Blanks";  
	desc[1] = "Weak";  
	desc[2] = "OK";  
	desc[3] = "Good";  
	desc[4] = "Tough";  
	desc[5] = "Strongest"; 

	var strength   = 0;
	
	if (password.length > 6) strength++;

	if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) strength++;
	
	if (password.match(/\d+/)) strength++;
	
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) strength++;
	
	if (password.length > 9) strength++;
	
	$("#pwdesc").html(desc[strength]);
	
	$("#pwstrength").removeClass();
	$("#pwstrength").addClass("strength" + strength);
	
	switch(strength)
	{
		case 1:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 2:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 3:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 4:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 5:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
	
		default:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;	
			
	}
	
	
//	$("pwstrength").html(desc[strength]);
	
//	document.getElementById("pwdesc").innerHTML = desc[strength];
//	document.getElementById("pwstrength").className = "strength" + strength; 

}

		
</script>


<style>

#AddNewCompanyAccountDiv
{
	margin:0 0;
	width: 700px;
	padding:0px;
	border: none;
	min-height: 530px;
	height: auto;
	font-size:12px;
}


#AddNewCompanyAccountDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#AddNewCompanyAccountDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
	width: 700px;
}


#AddNewCompanyAccountDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 
   <!---
<cfoutput>


<div id='AddNewCompanyAccountDiv' class="RXForm">

<form id="AddNewCompanyAccountForm" name="AddNewCompanyAccountForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>Company Name
        </label>
        <input TYPE="text" name="inpCompanyName" id="inpCompanyName" size="255" class="ui-corner-all" /> 
        
        
        <label>Primary Phone
        </label>
        <input TYPE="text" name="inpPhone" id="inpPhone" size="255" class="ui-corner-all password" title="Company Phone"  /> 
        
        <br /> 
       
        <label>Verified Primary Phone
        </label>
        <input TYPE="text" name="inpVerifiedPhone" id="inpVerifiedPhone" size="255" class="ui-corner-all password"  /> 
                
        <BR>
		<cfif StructKeyExists(SESSION.accessRights,companyList)>
        <button id="AddNewCompanyAccountButton" TYPE="button" class="ui-corner-all">Add</button>
			<cfelse>
			<label>You don't have right to modify company list</label>
		</cfif>		

        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
        <div id="loadingDlgAddNewCompanyAccount" style="display:inline;">
            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</div>
</cfoutput>
--->
<cfoutput>
        
<div id='AddNewCompanyAccountDiv' class="RXForm">

<form id="AddNewCompanyAccountForm" name="AddNewCompanyAccountForm" action="" method="POST">
        <div id="LeftMenu">     

            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Update Company Account</h1></div>

            <div width="100%" align="center" style="margin-bottom:20px;"><img src="../../public/images/Activity-Monitor-icon_web.png" /></div>
                    <cfif not StructKeyExists(SESSION.accessRights,companyList)>
                <BR />
                <i>DEFINITION:</i> You don't have permission to modify company list.
                <BR />
					</cfif>
                                            
        </div>
        
        
		
		<div id="RightStage">
               
            <div style="width 350px; min-width: 350px; margin:0px 0 0px 0;">
            	
 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>Company Name
        </label>
        <input TYPE="text" name="inpCompanyName" id="inpCompanyName" size="255" class="ui-corner-all" /> 
        
        
        <label>Primary Phone
        </label>
        <input TYPE="text" name="inpPhone" id="inpPhone" size="255" class="ui-corner-all password" title="Company Phone"  /> 
        
        <br /> 
       
        <label>Verified Primary Phone
        </label>
        <input TYPE="text" name="inpVerifiedPhone" id="inpVerifiedPhone" size="255" class="ui-corner-all password"  /> 
                
        <BR>
		
        <label>Email Address
        </label>
        <input TYPE="text" name="inpEmailAddress" id="inpEmailAddress" size="255" class="ui-corner-all password"  /> 
                
        <BR>		
		<cfif StructKeyExists(SESSION.accessRights,companyList)>
        <button id="AddNewCompanyAccountButton" TYPE="button" class="ui-corner-all">Add</button>
			<cfelse>
			<label>You don't have right to modify company list</label>
		</cfif>		

        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
        <div id="loadingDlgAddNewCompanyAccount" style="display:inline;">
            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
                     	
            </div>
		</div>          
</form>

</div>


</cfoutput>
