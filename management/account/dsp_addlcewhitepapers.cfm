<cfparam name="LCEWhitePapersID" default="-1">

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
<cfset companyList="companyList">
<script TYPE="text/javascript">
	function SaveLCEWhitePapers()
	{			
		$("#loadingDlgEditLCEWhitePapers").show();		
		
		if($("#txtContent").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a Content.\n", "Failure!", function(result) { $("#loadingDlgEditLCEWhitePapers").hide(); return false; } );							
						
			return false;	
		}
		
		var methodName = isEditLCEWhitePapers ? 'UpdateLCEWhitePapers' : 'AddNewLCEWhitePapers'
		var params = isEditLCEWhitePapers ? { lCEWhitePapersID_int: <cfoutput>#LCEWhitePapersID#</cfoutput>, content : $("#txtContent").val(), desc_vch : $("#txtDescription").val() } : { content : $("#txtContent").val(), desc_vch : $("#txtDescription").val() };
		$.getJSON('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=' + methodName + '&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  params, 
			function(d) 
			{																						;
				<!--- Check if variable is part of JSON result string --->								
				if(d.DATA.RESULT == 'true')
				{							
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					var message = isEditLCEWhitePapers ? 'LCE WhitePapers updated.' : 'LCE WhitePapers added.';
					jAlert(message, "Success", function(result) 
																{ 
																	$("#loadingDlgEditLCEWhitePapers").hide();
																	editLCEWhitePapersDialog.remove(); 
																} );								
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					var errorMessage = isEditLCEWhitePapers ? 'LCE WhitePapers has NOT been updated.' : 'LCE WhitePapers has NOT been added.';
					jAlert(errorMessage  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
				}
				
				$("#loadingDlgEditLCEWhitePapers").hide();
			});		
	
		return false;

	}	
	
	function BindEditLCEWhitePapersData() {
		$.getJSON('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetLCEWhitePapersByID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { lCEWhitePapersID_int : <cfoutput>#LCEWhitePapersID#</cfoutput> }, 
			function(d) {
				<!--- Check if variable is part of JSON result string --->								
				if(d.DATA.RESULT == 'true')
				{
					$("#txtContent").val(d.DATA.LCEWHITEPAPERSDATA[0][1]);
					$("#txtDescription").val(d.DATA.LCEWHITEPAPERSDATA[0][2]);
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Cannot get LCE WhitePapers data.\n"  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
				}
				
				$("#loadingDlgEditLCEWhitePapers").hide();
			});	
	}
	
	$(function()
	{	
		isEditLCEWhitePapers = '<cfoutput>#LCEWhitePapersID#</cfoutput>' != '-1';
		if (isEditLCEWhitePapers) {
			isEditLCEWhitePapers = true;
			BindEditLCEWhitePapersData();
			$('#btnEditLCEWhitePapers').html('Update');
		}
		$("#btnEditLCEWhitePapers").click( function() { SaveLCEWhitePapers(); return false;  }); 	
		
		<!--- close dialog --->
		$("#btnCancel").click(function() {
			$("#loadingDlgEditLCEWhitePapers").hide();					
			editLCEWhitePapersDialog.remove(); 
			return false;
	  	}); 	
		  
		$("#loadingDlgEditLCEWhitePapers").hide();	
	} );
		
</script>


<style>

#lCEWhitePapersDiv
{
	margin:0 0;
	width: 700px;
	padding:0px;
	border: none;
	min-height: 530px;
	height: auto;
	font-size:12px;
}


#lCEWhitePapersDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#lCEWhitePapersDiv #RightStage
{
	position:absolute;
	top:0;
	padding:15px;
	margin:0px;	
	border: 0;
	width: 700px;
}


#lCEWhitePapersDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 
 
<cfoutput>
	<div id='lCEWhitePapersDiv' class="RXForm">
		<form id="lCEWhitePapersForm" name="AlCEWhitePapersForm" action="" method="POST">
				<div id="RightStage">
			        <div style="width 350px; min-width: 350px; margin:0px 0 0px 0;">
						<label>Content</label>
				        <input TYPE="text" name="txtContent" id="txtContent" size="255" class="ui-corner-all" /> 
				        <label>Desctiption</label>
				        <input TYPE="text" name="txtDescription" id="txtDescription" size="255" class="ui-corner-all" /> 
						<button id="btnEditLCEWhitePapers" TYPE="button" class="ui-corner-all">Add</button>
				        <button id="btnCancel" TYPE="button" class="ui-corner-all">Cancel</button>
			                
				        <div id="loadingDlgEditLCEWhitePapers" style="display:inline;">
				            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
				        </div>             	
			        </div>
				</div>          
		</form>
	</div>
</cfoutput>

























