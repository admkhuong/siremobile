
<cfparam name="SESSION.CompanyUserId" default="#SESSION.USERID#">

<script>


$(function() {
<!---LoadSummaryInfo();--->
	jQuery("#TransactionList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#</cfoutput>/Session/cfc/billing.cfc?method=getTransactionHistory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 200,
		width:800,
		postData:{inpUserId:userIdForBilling},
		colNames:['ID','Event','User Id','Amount','Event Data','Created Date'],
		colModel:[
			{name:'TransactionId_int',index:'TransactionId_int', width:40, editable:true,align:'left'},
			{name:'Event_vch',index:'Event_vch', width:150, editable:true,align:'left'},
			{name:'UserId_int',index:'UserId_int', width:50, editable:false},
			{name:'AmountTenthPenny_int',index:'AmountTenthPenny_int', width:100, editable:true},
			{name:'EventData_vch',index:'EventData_vch', width:100, editable:true,align:'left'},
			{name:'Created_dt',index:'Created_dt', width:100, editable:true,align:'left'},
		],
		rowNum:20,
	   	rowList:[5, 10, 20, 30],
		mtype: "POST",
		pager: jQuery('#pagerBillingTransLog'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'TransactionId_int',
		viewrecords: true,
		sortorder: "asc",
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		 loadComplete: function(data){
				//reloadUserTaskList();
				return false;	 
   			},		
			onSelectRow: function(TransactionId_int,status){
				if(TransactionId_int && TransactionId_int!==lastsel){
					jQuery('#TransactionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=TransactionId_int;
				}
			},
			
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_TransactionList").height(55);
	$("#loading").hide();
		
});
	//remote existing element in array1 from array2]
	

	
</script>

<div style="padding-left:40px"><table id="TransactionList" align="center"></table></div>
<div id="pagerBillingTransLog"></div>