
<cfparam name="SESSION.CompanyUserId" default="#SESSION.USERID#">
<style>
	#FieldList ui-state-highlight {background: yellow !important;}
	#FieldList ui-state-hover {background: yellow !important;}
</style>
<script>

var permissionIds=0;
var selectedLevelId=0;
var localSelectedValues=new Array();
var localDefaultList=new Array();
$(function() {
<!---LoadSummaryInfo();--->
	var grid = $("#FieldList");
	
	jQuery("#FieldList").jqGrid({
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getFieldList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		postData:{inpTableName:selectedTableName},
		height: 200,
		colNames:['<font size="2"><b>Field Name</b></font>','<font size="2"><b>Description</b></font>','<font size="2"><b>Required</b></font>'],
		colModel:[
			{name:'column_name',index:'column_name', width:150, editable:false},
			{name:'column_comment',index:'column_comment', width:400, editable:true},
			{name:'required',index:'required', sortable:false,editrules:{required:false}, editoptions:{value:"1:0"},
				  formatter: checkboxFormatter, formatoptions: {disabled : true}, editable: true, edittype:"checkbox",width:50,align : 'center'}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		pager: jQuery('#pagerEditColl'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'column_name',
		viewrecords: true,
		sortorder: "asc",
		//hoverrows:true,
		
		multiselect: true,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		beforeRequest: function (){reloadDefaultColumnList(); return false;},
   //			gridComplete: function(){return false;},
		loadComplete: function(data){
			 	reloadUserTaskList();	
   		},

		beforeSelectRow: function(rowid, e) {
			jQuery("#FieldList").setSelection(rowid,true);
			if($("#jqg_FieldList_"+rowid).attr("disabled")){
				jQuery("#jqg_FieldList_"+rowid).attr("checked", true);
			}
		},
		onSelectAll: function(aRowids,status) {
			for(i=0;i<=aRowids.length;i++){
				if($("#jqg_FieldList_"+aRowids[i]).attr("disabled")){
					jQuery("#jqg_FieldList_"+aRowids[i]).attr("checked", true);
				}
			}
		},
		onSelectRow: function(rowId,status){
<!--- 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				} --->
			if(jQuery("#jqg_FieldList_"+rowId).attr("disabled")){
				if(!status){
					jQuery("#FieldList").setSelection(rowId,false);
				}
			}
		},
		onClose: function(){
			alert('aaaa');
		},
			
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

	  jsonReader: {
		  root: "ROWS", //our data
		  page: "PAGE", //current page
		  total: "TOTAL", //total pages
		  records:"RECORDS", //total records
		  cell: "", //not used
		  id: "0" //will default first column as ID
	  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_FieldList").height(55);
	$("#t_FieldList").html(<cfinclude template="dsp_EditLevelToolbar.cfm">);
	

	// Dock initialize
	$('#dockMCCompanyEditColloboration').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_BabbleMCEditColloboration',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
	
	$("#loading").hide();
		
});
	//remote existing element in array1 from array2]
	
	function getNeedInsertList(originalList,selectedList){
		var result=new Array();
		if(originalList.length == 0 && localDefaultList.length > 0){
			for(i =0;i<localDefaultList.length;i++){
				result.push(localDefaultList[i]+'');
			}
		}		
		if(originalList.length > 0 && selectedList.length > 0){
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]+'',originalList) < 0){
					result.push(selectedList[i]+'');
				}
			}
		}
		else if(originalList.length == 0 && selectedList.length > 0)
		{
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]+'',result) < 0){
					result.push(selectedList[i]+'');
				}
			}			
		}
		return result;
	}
	function getRequiredValueList(list){
		var result=new Array();
		if(list.length>0){
			for(i=0;i<list.length;i++){
				//var rowData = jQuery('#TaskList').getRowData(list[i]);
				//var temp= rowData['required'];
				var temp=getCellValue(jQuery('#FieldList').getCell(list[i],'required'));
				result.push(temp);
				//result.push($('#' + list[i] + '_required').val());
			}
		}
		//alert(result);
		return result;
	}
	
	
	//get the not existing element from array 1 from array2
	function getNeedRemoveList(selectedList,originalList){
		var res=new Array();
		if(selectedList.length>0 && originalList.length>0){
			for(i=0;i<originalList.length;i++){
				if($.inArray(originalList[i]+'',selectedList) < 0){
					res.push(originalList[i]+'');
				}
			}
		}else{
			return originalList;
		}
		return res;
	}

	function UpdateUserTaskList(){
		var needInsertList=new Array();
		needInsertList=getNeedInsertList(taskItemIds,jQuery("#FieldList").getGridParam('selarrrow'));
		needRemoveList=getNeedRemoveList(jQuery("#FieldList").getGridParam('selarrrow'),taskItemIds);
		
		var requiredValueList=new Array();
		requiredValueList=getRequiredValueList(needInsertList);
		
		var needUpdateList = new Array();
		if(needInsertList.length > 0){
			needUpdateList=diffArrays(jQuery("#FieldList").getGridParam('selarrrow'),needInsertList);
		}else{
			needUpdateList=jQuery("#FieldList").getGridParam('selarrrow');
		}
		
		
		var requireValueListOfUpdateList = new Array();
		requireValueListOfUpdateList=getRequiredValueList(needUpdateList);
			//alert(lastsel);
		if(needInsertList.length==0 && needRemoveList.length == 0 && needUpdateList.length==0){
			jAlert( "No item was selected!","Failed");
			return;
		}
		
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=AssignCollaborationCheckListToUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
			{ inpUserId : editedCompanyAccountId,inpTaskList : needInsertList.join('~'),inpToRemoveItems : needRemoveList.join('~'),inpRequiredValues : requiredValueList.join('~')
				,inpNeedUpdateItems:needUpdateList.join('~'),inpRequiredValuesUpdateItems:requireValueListOfUpdateList.join('~'),inpTableName : selectedTableName}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								//jAlert("Update Task sucessfully", "Success!", function(result) {EditCollaborationDialog.remove();});
								$("#UpdateCollaborationButton").attr("disabled",true);
								$.jGrowl("Update Task sucessfully", { life:2000, position:"center", header:' Message',open: function(e,m,o){EditCollaborationDialog.remove();} });
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Update failed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					$("#loadingDlgLinkUserToCompany").hide();
			} );
		return false;
	}
	function reloadUserTaskList(){
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getTaskListOfUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			 { inpUserId : editedCompanyAccountId,inpTableName : selectedTableName }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							taskItemIds=d.DATA.TASKITEMS[0];
							requireds=d.DATA.REQUIRED[0];
							//set local selected values to array of original task list
							localSelectedValues=taskItemIds;
							//jQuery("#TaskList").resetSelection();
							if(taskItemIds.length > 0)
							{
								for(var i=0;i<taskItemIds.length;i++){
									if(!jQuery("#jqg_FieldList_"+taskItemIds[i]).attr("disabled")){
										jQuery("#FieldList").setSelection(taskItemIds[i],false);
									}
										//$(#ur_checkbox_id).attr('disabled', true);
										if(requireds[i] ==1){
											jQuery("input[name=chkRequired]").filter("[rel='"+ $.trim(taskItemIds[i]) +"']").attr("checked", true);
											jQuery("input[name=chkRequired]").filter("[rel='"+ $.trim(taskItemIds[i]) +"']").attr("value", 1);
										}										
								}
							}
	<!--- 									else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										} --->
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} );
	}
	
	function reloadDefaultColumnList(){
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getDefaultColumnList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			 { inpTableName : selectedTableName }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							localDefaultList=d.DATA.DEFAULTLIST[0];
							if(localDefaultList.length>0){
								for(i=0;i<=localDefaultList.length;i++){
																		jQuery("#FieldList").setSelection(localDefaultList[i],false);
									//jQuery("#jqg_FieldList_"+localDefaultList[i]).attr("checked", true);
									jQuery("#jqg_FieldList_"+localDefaultList[i]).attr("disabled", true);
									jQuery("input[name=chkRequired]").filter("[rel='"+ $.trim(localDefaultList[i]) +"']").attr("checked", true);
									jQuery("input[name=chkRequired]").filter("[rel='"+ $.trim(localDefaultList[i]) +"']").attr("disabled", true);
									jQuery("input[name=chkRequired]").filter("[rel='"+ $.trim(localDefaultList[i]) +"']").attr("value", 1);
								}
							}								
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} );	
	}

		function diffArrays (A, B) {
		
		  var strA = ":" + A.join("::") + ":";
		  var strB = ":" +  B.join(":|:") + ":";
		
		  var reg = new RegExp("(" + strB + ")","gi");
		
		  var strDiff = strA.replace(reg,"").replace(/^:/,"").replace(/:$/,"");
		
		  var arrDiff = strDiff.split("::");
		
		  return arrDiff;
		}
		function checkboxFormatter(cellvalue, options, rowObject) {
		    cellvalue = cellvalue + "";
		    cellvalue = cellvalue.toLowerCase();
<!---		    if(jQuery.inArray(rowObject[0], localDefaultList)>-1)
		    {
		    	var bchk = " checked='checked'";
		    	return '<input disabled="true" type="checkbox" onclick="clientSave(' + '\'' + options.rowId + '\'' +','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';
		    }else{
		    	var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked='checked'" : "";
		    	return '<input type="checkbox" onclick="clientSave(' + '\'' + options.rowId + '\'' +','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';
		    }
		    --->
		    	var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked='checked'" : "";
		    	return '<input type="checkbox" rel="'+ options.rowId +'" name="chkRequired" onclick="clientSave(' + '\'' + options.rowId + '\'' +','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';		    
		}
		function clientSave(rowId,value){
			if(value == 0 || value == 'undefined' || value === undefined){
				jQuery("#FieldList").setCell(rowId,'required',1);
			}else{
				jQuery("#FieldList").setCell(rowId,'required',0);
			}
		}
		function getCellValue(content) {
		  var k1, val = 0;
		  if(content==false) return 0;
		  k1 = content.indexOf(' value=') + 7;
		  val = content.substr(k1,3);
		  val = val.replace(/[^0-1]/g, '');
		  if(val=='undefined' || val=='') return 0;
		  return val;
		}
		function wait(msecs)
		{
			var start = new Date().getTime();
			var cur = start
			while(cur - start < msecs)
			{
				cur = new Date().getTime();
			}
		}
		
		
		<!---  --->
</script>
<BR />
<div style="padding-left:40px"><table id="FieldList" align="center"></table></div>
<div id="pagerEditColl"></div>

<!--- <div id="dockMCCompanyEditColloboration" style="text-align:left;">
     <div class="dock-container_BabbleMCEditColloboration">
         <a class="dock-item BBMainMenu" href="##" onclick="UpdateUserTaskList(); return false;"><span>Update Colloboration</span><img src="../../public/images/dock/SystemGears_Web_64x64II.png" alt="Update Colloboration" /></a> 
     </div>		
 </div> --->