<cfparam name="LCEROIID" default="-1">

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
<cfset companyList="companyList">
<script TYPE="text/javascript">
	function SaveLCEROI()
	{					
		var companyId = $.trim($('#txtCompanyId').val());
		var numberOfCustomers = $.trim($('#txtNumberOfCustomers').val());
		var percent = $.trim($('#txtPercent').val());
		var averageProfit = $.trim($('#txtAverageProfit').val());
		var totalProfit = $.trim($('#txtTotalProfit').val());
		
		if (!ValidateNumber("Company Id", companyId)) {
			return;
		}
		
		if (!ValidateNumber("Number of Customers", numberOfCustomers)) {
			return;
		}
		
		if (!ValidateNumber("Percent", percent)) {
			return;
		}
		
		if (!ValidateNumber("Average Profit", averageProfit)) {
			return;
		}
		
		if (!ValidateNumber("Total Profit", totalProfit)) {
			return;
		}
		$("#loadingDlgEditLCEROI").show();		
		var methodName = isEditLCEROI ? 'UpdateLCEROI' : 'AddNewLCEROI'
		var params = isEditLCEROI ? { lCEROIID_int: <cfoutput>#LCEROIID#</cfoutput>, companyId : companyId, desc_vch : $("#txtDescription").val(), numberOfCustomers: numberOfCustomers, percent: percent, averageProfit: averageProfit, totalProfit: totalProfit } : { companyId : companyId, desc_vch : $("#txtDescription").val(), numberOfCustomers: numberOfCustomers, percent: percent, averageProfit: averageProfit, totalProfit: totalProfit };
		$.getJSON('<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEROI.cfc?method=' + methodName + '&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  params, 
			function(d) 
			{																
				<!--- Check if variable is part of JSON result string --->	
				if(d.DATA.RESULT == 'true')
				{							
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					var message = isEditLCEROI ? 'LCE ROI updated.' : 'LCE ROI added.';
					jAlert(message, "Success", function(result) 
																{ 
																	$("#loadingDlgEditLCEROI").hide();
																	editLCEROIDialog.remove(); 
																} );								
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					var errorMessage = isEditLCEROI ? 'LCE ROI has NOT been updated.' : 'LCE ROI has NOT been added.';
					jAlert(errorMessage  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
				}
				
				$("#loadingDlgEditLCEROI").hide();
			});		
	
		return false;

	}	
	
	function ValidateNumber(fieldName, value) {
		if (value == '') {
			jAlert(fieldName + " is required field", "Failure!", function(result) { } );	
			return false;
		}
		if (!IsNumber(value)) {
			jAlert(fieldName + " must be a number", "Failure!", function(result) { } );	
			return false;
		}
		return true;
	}
	function BindEditLCEROIData() {
		$.getJSON('<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEROI.cfc?method=GetLCEROIByID&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { lCEROIID_int : <cfoutput>#LCEROIID#</cfoutput> }, 
			function(d) {
				<!--- Check if variable is part of JSON result string --->								
				if(d.DATA.RESULT == 'true')
				{
					$("#txtCompanyId").val(d.DATA.LCEROIDATA[0][1]);
					$("#txtDescription").val(d.DATA.LCEROIDATA[0][2]);
					$("#txtNumberOfCustomers").val(d.DATA.LCEROIDATA[0][3]);
					$("#txtPercent").val(d.DATA.LCEROIDATA[0][4]);
					$("#txtAverageProfit").val(d.DATA.LCEROIDATA[0][5]);
					$("#txtTotalProfit").val(d.DATA.LCEROIDATA[0][6]);
				}
				else
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Cannot get LCE ROI data.\n"  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
				}
				
				$("#loadingDlgEditLCEROI").hide();
			});	
	}
	
	$(function()
	{	
		isEditLCEROI = '<cfoutput>#LCEROIID#</cfoutput>' != '-1';
		if (isEditLCEROI) {
			isEditLCEROI = true;
			BindEditLCEROIData();
			$('#btnEditLCEROI').html('Update');
		}
		$("#btnEditLCEROI").click( function() { SaveLCEROI(); return false;  }); 	
		
		<!--- close dialog --->
		$("#btnCancel").click(function() {
			$("#loadingDlgEditLCEROI").hide();					
			editLCEROIDialog.remove(); 
			return false;
	  	}); 	
		
		$("#loadingDlgEditLCEROI").hide();	
	} );
		
</script>


<style>

#lCEROIDiv
{
	margin:0 0;
	width: 700px;
	padding:0px;
	border: none;
	min-height: 530px;
	height: auto;
	font-size:12px;
}


#lCEROIDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#lCEROIDiv #RightStage
{
	position:absolute;
	top:0;
	padding:15px;
	margin:0px;	
	border: 0;
	width: 700px;
}


#lCEROIDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 
 
<cfoutput>
	<div id='lCEROIDiv' class="RXForm">
		<form id="lCEROIForm" name="AlCEROIForm" action="" method="POST">
				<div id="RightStage">
			        <div style="width 350px; min-width: 350px; margin:0px 0 0px 0;">
						<label>Company Id</label>
				        <input TYPE="text" id="txtCompanyId" size="255" class="ui-corner-all" /> 
				        
				        <label>Desctiption</label>
				        <input TYPE="text" id="txtDescription" size="255" class="ui-corner-all" /> 
				        
				        <label>Number of Customers</label>
				        <input TYPE="text" id="txtNumberOfCustomers" size="255" class="ui-corner-all" /> 
				        
				        <label>Percent</label>
				        <input TYPE="text" id="txtPercent" size="255" class="ui-corner-all" /> 

				        <label>Average Profit</label>
				        <input TYPE="text" id="txtAverageProfit" size="255" class="ui-corner-all" /> 

				        <label>Total Profit</label>
				        <input TYPE="text" id="txtTotalProfit" size="255" class="ui-corner-all" /> 
				        				        				        
						<button id="btnEditLCEROI" TYPE="button" class="ui-corner-all">Add</button>
				        <button id="btnCancel" TYPE="button" class="ui-corner-all">Cancel</button>
			                
				        <div id="loadingDlgEditLCEROI" style="display:inline;">
				            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
				        </div>             	
			        </div>
				</div>          
		</form>
	</div>
</cfoutput>

























