<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>


<script TYPE="text/javascript">

	function _LinkUserToCompany()
	{			
		if ($("#LinkUserToCompanyDiv #isAdminManager:checked").val() == 'undefined') {
			var isAdminManager = 0;
		} else {
			var isAdminManager = 1;
		}

		$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=LinkUserToCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
		{ 
			inpUserId : $("#LinkUserToCompanyDiv #inpUserId").val(), 
			inpCompanyId : $("#LinkUserToCompanyDiv #inpCompanyId").val(),
			isAdminManager:  isAdminManager
		}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Adding user to company account sucessfully", "Success!", function(result) 
																			{ 
																				$("#loadingDlgLinkUserToCompany").hide();
																				LinkUserToCompanyDialog.remove();
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgLinkUserToCompany").hide();
			} );		

		return false;

	}
	
	
	$(function()
	{
		$("#LinkUserToCompanyDiv #LinkUserToCompanyButton").click( function() { _LinkUserToCompany(); return false;  }); 	
		<!--- Kill the new dialog --->
		$("#LinkUserToCompanyDiv #Cancel").click( function() 
			{
					$("#loadingDlgLinkUserToCompany").hide();					
					LinkUserToCompanyDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgAddNewCompanyAccount").hide();	
		  
		  $("#inpNAPassword").keyup( function(event){  
//		         cur_val = $(this).val(); // grab what's in the field       							    
				 PasswordStrength($(this).val());
//				 $(this).val(cur_val);    
		  } );
	} );


	function getUserList()
	{
		var UserData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=RetrieveUserIdList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPACTIVE : 1,inpUserRole: '<cfoutput>#Session.UserRole#</cfoutput>'  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.USERACCOUNTDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpUserId" id="inpUserId" size="1" class="ui-corner-all" onChange="getUserInfo();"> ';
							template += '<option value="-1">Select an user</option>';
							for(i=0; i<d.DATA.USERACCOUNTDATA[0].length ; i++){
								var value=d.DATA.USERACCOUNTDATA[0][i][0];
								var text=d.DATA.USERACCOUNTDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#LinkUserToCompanyDiv #UserListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}	

	function getCompanyList()
	{			
		var CompanyData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/company.cfc?method=RetrieveCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPACTIVE : 1  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.COMPANYDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all"  > ';
							template += '<option value="-1">-----Select a Company-----</option>';
							for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
								var value=d.DATA.COMPANYDATA[0][i][0];
								var text=d.DATA.COMPANYDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
							};	
							template += '</select>';	
							$("#LinkUserToCompanyDiv #CompanyListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}	
		
		function getUserInfo(){
			$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/management.cfc?method=getUserCompanyInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#LinkUserToCompanyDiv #inpUserId").val() }, 
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{
<!--- 							if(typeof(d.DATA.ISCOMPANYADMIN) != "undefined" && d.DATA.ISCOMPANYADMIN == 1)
							{
								$("#isAdminDiv #isAdminManager").attr('checked', true);
							}
							else
							{
								$("#isAdminDiv #isAdminManager").attr('checked', false);
							} --->
							if(typeof(d.DATA.COMPANYID) != "undefined" && d.DATA.COMPANYID > 0){
								$("#inpCompanyId").val(d.DATA.COMPANYID);
							}else{
								$("#inpCompanyId").val(0);
							}
						}
						else
						{<!--- No result returned --->							
							jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
						$("#loadingDlgRegisterNewAccount").hide();
				} );		
		
			return false;			
		}
		
</script>


<style>

#RegisterNewAccountDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#RegisterNewAccountDiv div{
display:inline;
border:none;
}

</style> 
    
	

<cfoutput>

<div id='LinkUserToCompanyDiv' class="RXForm">

<form id="LinkUserToCompanyForm" name="LinkUserToCompanyForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>User Name
        </label>
		<div id="UserListDiv"/>
        
		<br/>

		<label>Company Name</label>
		<div id="CompanyListDiv"/>
        <br /> 
       
			
<!--- 			<div id="isAdminDiv">
				<label>Is Admin ?</label>
		<input type="checkbox" name="isAdminManager"  id="isAdminManager" value="1" style="vertical-align:middle;float:right;width:0px;margin-right:312px;"> 
		</div> --->
		<br/>
        <button id="LinkUserToCompanyButton" TYPE="button" class="ui-corner-all">Add</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>

</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		getCompanyList();
		getUserList();
	});	
</script>
</cfoutput>

























