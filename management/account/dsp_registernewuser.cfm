<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>

<script TYPE="text/javascript">


	function RegisterNewAccount()
	{			
					
		$("#loadingDlgRegisterNewAccount").show();		
	
		if($("#inpNAPassword").val() != $("#inpNAPassword2").val())
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Passwords dont match - please retype.\n", "Failure!", function(result) { $("#loadingDlgRegisterNewAccount").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpNAUserName").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a login name.\n", "Failure!", function(result) { $("#loadingDlgRegisterNewAccount").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpNAPassword").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid password.\n", "Failure!", function(result) { $("#loadingDlgRegisterNewAccount").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpMainPhone").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgRegisterNewAccount").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpMainEmail").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid email address.\n", "Failure!", function(result) { $("#loadingDlgRegisterNewAccount").hide(); return false; } );							
						
			return false;	
		}
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { 
				inpNAUserName : $("#RegisterNewAccountDiv #inpNAUserName").val(), 
				inpNAPassword : $("#RegisterNewAccountDiv #inpNAPassword").val(), 
				inpMainPhone : $("#RegisterNewAccountDiv #inpMainPhone").val(), 
				inpNAUserName : $("#RegisterNewAccountDiv #inpNAUserName").val(), 
				inpMainEmail : $("#RegisterNewAccountDiv #inpMainEmail").val(),
				inpCompanyId : $("#RegisterNewAccountDiv #inpCompanyId").val(),
			}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								//jAlert("User has beed added.", "Success!", function(result) { $("#loadingDlgRegisterNewAccount").hide();RegisterNewAccountDialog.remove(); } );
								$.jGrowl("User has beed added", { life:2000, position:"center", header:' Success' });							
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{	
	
		$("#RegisterNewAccountDiv #RegisterNewAccountButton").click( function() { RegisterNewAccount(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#RegisterNewAccountDiv #Cancel").click( function() 
			{
					$("#loadingDlgRegisterNewAccount").hide();					
					RegisterNewAccountDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgRegisterNewAccount").hide();	
		  
		  $("#inpNAPassword").keyup( function(event){  
//		         cur_val = $(this).val(); // grab what's in the field       							    
				 PasswordStrength($(this).val());
//				 $(this).val(cur_val);    
		  } );
	} );
	
	$(function()
	{
		var CompanyData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/company.cfc?method=RetrieveCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPACTIVE : 1, inpUserId : '<cfoutput>#Session.USERID#</cfoutput>'  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.COMPANYDATA[0]) != "undefined")
						{
							var isAdmin = <cfoutput>"#SESSION.UserRole#"</cfoutput>;
							if(isAdmin == "SuperUser"){
								template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all"  > ';
								template += '<option value="">--- Select a company ---</option>';
								for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
									var value=d.DATA.COMPANYDATA[0][i][0];
									var text=d.DATA.COMPANYDATA[0][i][1];
									template += '<option value="'+ value +'">'+ text + '</option>';
								};	
								template += '</select>';
							}else{
								template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all"  disabled="true"> ';
								for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
									var value=d.DATA.COMPANYDATA[0][i][0];
									var text=d.DATA.COMPANYDATA[0][i][1];
									template += '<option value="'+ value +'">'+ text + '</option>';
								};	
								template += '</select>';
							}

							$("#RegisterNewAccountDiv #CompanyAccountDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} );		
	
		return false;		
	});



function PasswordStrength(password)  
{  
	var desc = new Array();  
	desc[0] = "No Blanks";  
	desc[1] = "Weak";  
	desc[2] = "OK";  
	desc[3] = "Good";  
	desc[4] = "Tough";  
	desc[5] = "Strongest"; 

	var strength   = 0;
	
	if (password.length > 6) strength++;

	if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) strength++;
	
	if (password.match(/\d+/)) strength++;
	
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) strength++;
	
	if (password.length > 9) strength++;
	
	$("#pwdesc").html(desc[strength]);
	
	$("#pwstrength").removeClass();
	$("#pwstrength").addClass("strength" + strength);
	
	switch(strength)
	{
		case 1:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 2:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 3:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 4:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
		
		case 5:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;
	
		default:
			$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			break;	
			
	}
	
	
//	$("pwstrength").html(desc[strength]);
	
//	document.getElementById("pwdesc").innerHTML = desc[strength];
//	document.getElementById("pwstrength").className = "strength" + strength; 

}


		
		
</script>


<style>

#RegisterNewAccountDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#RegisterNewAccountDiv div{
display:inline;
border:none;
}

</style> 
    
<cfoutput>
<div id='RegisterNewAccountDiv' class="RXForm">

<form id="RegisterNewAccountForm" name="RegisterNewAccountForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>Preferred Login
        <span class="small">Must be unique.</span>
        </label>
        <span class="required">*required</span>
        <input TYPE="text" name="inpNAUserName" id="inpNAUserName" size="255" class="ui-corner-all" /> 

        <label>Password
        <span class="small">Good idea to make it strong.</span>        
        </label>
        <span class="required">*required</span>
        <input TYPE="password" name="inpNAPassword" id="inpNAPassword" size="255" class="ui-corner-all password" title="A strong password contains both upper and lower case letters, AND numbers, AND special characters, AND be at least 10 characters long."  /> 
        
        <span class="small" style="padding-top:10px; padding-bottom:10px;">Password Strength: 
        <div id="pwdesc">No Blanks</div>  
		<div id="pwstrength" class="strength0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </span>
        
        <br /> 
       
        <label>Confirm Password 
        <span class="small">Just to be sure you typed it correctly.</span>
        </label>
        <span class="required">*required</span>
        <input TYPE="password" name="inpNAPassword2" id="inpNAPassword2" size="255" class="ui-corner-all password"  /> 
		
        <label>Company Account
        <span class="small">Select Company Account</span>
        </label>
        <div id="CompanyAccountDiv"/>
        
        <label>Contact Phone Number
        <span class="small">Preferred phone number for account verfication.</span>
        </label>
        <span class="required">*required</span>
        <input TYPE="text" name="inpMainPhone" id="inpMainPhone" size="255" class="ui-corner-all" /> 
        
        <label>Contact Email Address
        <span class="small">Email recovery and account verification.</span>
        </label>
        <span class="required">*required</span>
        <input TYPE="text" name="inpMainEmail" id="inpMainEmail" size="255" class="ui-corner-all" /> 
                
        <BR>
                
<!---         <button id="RegisterNewAccountButton" TYPE="button" class="ui-corner-all">Add</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button> --->
                
        <div id="loadingDlgRegisterNewAccount" style="display:inline;">
            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</div>
</cfoutput>

























