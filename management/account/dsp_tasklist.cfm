
<cfparam name="SESSION.CompanyUserId" default="#SESSION.USERID#">

<script>

var taskItemIds=0;
var selectedUserId=0;
var localSelectedValues=new Array();
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#TaskList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=getTaskListItems&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 200,
		width:700,
		colNames:['Task Id','Item Name','Description','Required'],
		colModel:[
			{name:'id',index:'id', width:70, editable:false},
			{name:'ItemName_vch',index:'ItemName_vch', width:150, editable:true},
			{name:'description',index:'description', width:200, editable:true},
			{name:'required',index:'required', sortable:false,editrules:{required:false}, editoptions:{value:"Yes:No"},
				  formatter: checkboxFormatter, formatoptions: {disabled : false}, editable: true, edittype:"checkbox",width:50}
		],
		rowNum:500,
	   	//rowList:[20,4,8,10,30],
		mtype: "POST",
		pager: jQuery('#pagerTaskList'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'id',
		toolbar:[true,"both"],
		viewrecords: true,
		sortorder: "asc",
		formatter: checkboxFormatter,
		multiselect: true,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		 loadComplete: function(data){
				reloadUserTaskList();
				return false;	 
   			},		
			onSelectRow: function(ItemId_int,status){
				//alert(ItemId_int + '-' + status);
<!--- 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				} --->
			},
			
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_TaskList").height(55);
	$("#loading").hide();
	// Dock initialize
	$('#dockMCCotactList').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_BabbleMCContacts',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);	
		
});
	//remote existing element in array1 from array2]
	
	function getNeedInsertList(originalList,selectedList){
		var result=new Array();
		if(originalList.length > 0 && selectedList.length > 0){
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]*1,originalList) < 0){
					result.push(selectedList[i]*1);
				}
			}
		}else
		{
			return selectedList;
		}
		return result;
	}
	function getRequiredValueList(list){
		var result=new Array();
		if(list.length>0){
			for(i=0;i<list.length;i++){
				//var rowData = jQuery('#TaskList').getRowData(list[i]);
				//var temp= rowData['required'];
				var temp=getCellValue(jQuery('#TaskList').getCell(list[i],'required'));
				result.push(temp);
				//result.push($('#' + list[i] + '_required').val());
			}
		}
		//alert(result);
		return result;
	}
	
	
	//get the not existing element from array 1 from array2
	function getNeedRemoveList(selectedList,originalList){
		var res=new Array();
		if(selectedList.length>0 && originalList.length>0){
			for(i=0;i<originalList.length;i++){
				if($.inArray(originalList[i]+"",selectedList) < 0){
					res.push(originalList[i]*1);
				}
			}
		}else{
			return originalList;
		}
		return res;
	}
	
	function UpdateUserTaskList(){
		var needInsertList=new Array();
		needInsertList=getNeedInsertList(taskItemIds,jQuery("#TaskList").getGridParam('selarrrow'));
		
		var needRemoveList=new Array();
		needRemoveList=getNeedRemoveList(jQuery("#TaskList").getGridParam('selarrrow'),taskItemIds);
		
		var requiredValueList=new Array();
		requiredValueList=getRequiredValueList(needInsertList);
		
		var needUpdateList = new Array();
		if(needInsertList.length > 0){
			needUpdateList=diffArrays(jQuery("#TaskList").getGridParam('selarrrow'),needInsertList);
		}else{
			needUpdateList=jQuery("#TaskList").getGridParam('selarrrow');
		}
		
		
		var requireValueListOfUpdateList = new Array();
		requireValueListOfUpdateList=getRequiredValueList(needUpdateList);
			//alert(lastsel);
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=AssignTaskListToUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
			{ inpUserId : selectedUserId,inpTaskList : needInsertList.join('~'),inpToRemoveItems : needRemoveList.join('~'),inpRequiredValues : requiredValueList.join('~')
				,inpNeedUpdateItems:needUpdateList.join('~'),inpRequiredValuesUpdateItems:requireValueListOfUpdateList.join('~')}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Update Task sucessfully", "Success!", function(result) 
																			{ 
																				//$("#loadingDlgLinkUserToCompany").hide();
																				UpdateUserTaskListDialog.remove();
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Update failed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					$("#loadingDlgLinkUserToCompany").hide();
			} );
		return false;
	}
	function reloadUserTaskList(){
	 	selectedUserId=lastsel-0;
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getTaskListOfUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : lastsel }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							taskItemIds=d.DATA.TASKITEMS[0];
							requireds=d.DATA.REQUIRED[0];
							//set local selected values to array of original task list
							localSelectedValues=taskItemIds;
							//jQuery("#TaskList").resetSelection();
							if(taskItemIds.length > 0)
							{
								for(var i=0;i<=taskItemIds.length;i++){
									jQuery("#TaskList").setSelection(taskItemIds[i],false);
									//$(#ur_checkbox_id).attr('disabled', true);
									jQuery("#TaskList").setCell(taskItemIds[i],'required',requireds[i]);
								}
							}
	<!--- 									else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										} --->
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgLinkUserToCompany").hide();
			} );
	}
	
	

	
		function diffArrays (A, B) {
		
		  var strA = ":" + A.join("::") + ":";
		  var strB = ":" +  B.join(":|:") + ":";
		
		  var reg = new RegExp("(" + strB + ")","gi");
		
		  var strDiff = strA.replace(reg,"").replace(/^:/,"").replace(/:$/,"");
		
		  var arrDiff = strDiff.split("::");
		
		  return arrDiff;
		}
		function checkboxFormatter(cellvalue, options, rowObject) {
		    cellvalue = cellvalue + "";
		    cellvalue = cellvalue.toLowerCase();
		    var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked='checked'" : "";
		    return '<input type="checkbox" onclick="clientSave(' + options.rowId + ','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';
		}
		function clientSave(rowId,value){

			if(value == 0 || value == 'undefined' || value === undefined){
				jQuery("#TaskList").setCell(rowId,'required',1);
			}else{
				jQuery("#TaskList").setCell(rowId,'required',0);
			}
		}
		function getCellValue(content) {
		  var k1, val = 0;
		  if(content==false) return 0;
		  k1 = content.indexOf(' value=') + 7;
		  val = content.substr(k1,3);
		  val = val.replace(/[^0-1]/g, '');
		  if(val=='undefined' || val=='') return 0;
		  return val;
		}
	
</script>



<BR />

<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->

<BR />

<div style="padding-left:40px"><table id="TaskList" align="center"></table></div>
<div id="pagerTaskList"></div>
   <div style="position:relative; bottom:-80px;">                           

 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockMCCotactList" style="text-align:left;">
            <div class="dock-container_BabbleMCContacts">
                <a class="dock-item BBMainMenu" href="##" onclick="UpdateUserTaskList(); return false;"><span>Update User's Task List</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Update User's Task List" /></a> 
           </div>
           
        </div>
        
    </div>