<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
<!--- Define name for access permission --->
<cfset companyList="companyList">


<script TYPE="text/javascript">

	function UpdateCompanyAccount()
	{			
		
		if($("#inpCompanyName").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a company name.\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpPhone").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							
						
			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/company.cfc?method=UpdateCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpCompanyAccountId : $("#UpdateCompanyAccountDiv #inpCompanyAccountId").val(),inpCompanyName : $("#UpdateCompanyAccountDiv #inpCompanyName").val(), inpPrimaryPhone : $("#UpdateCompanyAccountDiv #inpPrimaryPhone").val(), inpAddress1 : $("#UpdateCompanyAccountDiv #inpAddress1").val(),inpAddress2 : $("#UpdateCompanyAccountDiv #inpAddress2").val()}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Company Account has been updated.", "Success!", function(result) 
																			{ 
																				$("#loadingDlgUpdateCompanyAccount").hide();
																				UpdateCompanyAccountDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Company has NOT beed updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;

	}
	
	function getSelectedCompanyAccountData()
	{			
		
		var CompanyData=0;
		var template="";
		var selectedId=lastsel;
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/company.cfc?method=RetrieveACompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPCOMPANYACCOUNTID :  lastsel  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.COMPANYDATA[0]) != "undefined")
						{
							<!---
							template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all" > ';
							for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
								var value=d.DATA.COMPANYDATA[0][i][0];
								var text=d.DATA.COMPANYDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#BSContentHome #CompanyList").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	--->
							$("#UpdateCompanyAccountDiv #inpCompanyAccountId").val(d.DATA.COMPANYDATA[0][0]);
							$("#UpdateCompanyAccountDiv #inpCompanyName").val(d.DATA.COMPANYDATA[0][1]);
							$("#UpdateCompanyAccountDiv #inpPrimaryPhone").val(d.DATA.COMPANYDATA[0][2]);
							$("#UpdateCompanyAccountDiv #inpAddress1").val(d.DATA.COMPANYDATA[0][3]);
							$("#UpdateCompanyAccountDiv #inpAddress2").val(d.DATA.COMPANYDATA[0][4]);
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdateCompanyAccount").hide();
			} );		
	
		return false;

	}		
	
	
	$(function()
	{	
	
		if($("#UpdateCompanyAccountDiv #UpdateCompanyAccountButton").val() != 'undefined'){
			$("#UpdateCompanyAccountDiv #UpdateCompanyAccountButton").click( function() { UpdateCompanyAccount(); return false;  });
		}
		
		<!--- Kill the new dialog --->
		$("#UpdateCompanyAccountDiv #Cancel").click( function() 
			{
					$("#loadingDlgUpdateCompanyAccount").hide();					
					UpdateCompanyAccountDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgUpdateCompanyAccount").hide();	
	} );






		function enableButton(){
			//$("#UpdateCompanyAccountDiv #UpdateCompanyAccountButton").set=false;
			document.UpdateCompanyAccountForm.UpdateCompanyAccountButton.disabled=false;
			
		}

		
</script>
<script type="text/javascript">
	getSelectedCompanyAccountData();
	//alert(lastsel);
			$('#UpdateCompanyAccountForm :input').keyup(function(){
				document.UpdateCompanyAccountForm.UpdateCompanyAccountButton.disabled=false;
				
			});
</script>


<style>


#UpdateCompanyAccountDiv
{
	margin:0 0;
	width: 700px;
	padding:0px;
	border: none;
	min-height: 530px;
	height: 500px;
	font-size:12px;
}


#UpdateCompanyAccountDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#UpdateCompanyAccountDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
	width: 700px;
}


#UpdateCompanyAccountDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 
    <!---
<cfoutput>

<div id='UpdateCompanyAccountDiv' class="RXForm">

<form id="UpdateCompanyAccountForm" name="UpdateCompanyAccountForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />
 	<input TYPE="hidden" name="inpCompanyAccountId" id="inpCompanyAccountId"/>
		<label>Company Name
        </label>
        <input TYPE="text" name="inpCompanyName" id="inpCompanyName" size="255" class="ui-corner-all" disabled="true"/> 
        
        
        <label>Primary Phone
        </label>
        <input TYPE="text" name="inpPrimaryPhone" id="inpPrimaryPhone" size="255" class="ui-corner-all password" title="Company Phone"  onChange="enableButton();"/> 
        
        <br /> 
       
        <label>Address 1
        </label>
        <input TYPE="text" name="inpAddress1" id="inpAddress1" size="255" class="ui-corner-all password"  onChange="enableButton();"/> 
                
        <BR>
		
        <label>Address 2
        </label>
        <input TYPE="text" name="inpAddress2" id="inpAddress2" size="255" class="ui-corner-all password"  onChange="enableButton();"/> 
                
        <BR>		
		<cfif StructKeyExists(SESSION.accessRights,companyList)>
        	<button id="UpdateCompanyAccountButton" TYPE="button" class="ui-corner-all" disabled="true">Update</button>
			<cfelse>
			<label>You don't have right to modify company list</label>
		</cfif>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
       <!--- <div id="loadingDlgUpdateCompanyAccount" style="display:inline;">
            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>--->
</form>
</div>
</cfoutput>--->
<cfoutput>
        
<div id='UpdateCompanyAccountDiv' class="RXForm">

<form id="UpdateCompanyAccountForm" name="UpdateCompanyAccountForm" action="" method="POST">

        <div id="LeftMenu">

            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Update Company Account</h1></div>

            <div width="100%" align="center" style="margin-bottom:20px;"><img src="../../public/images/Activity-Monitor-icon_web.png" /></div>
                    <cfif not StructKeyExists(SESSION.accessRights,companyList)>
                <BR />
                <i>WARNING:</i> You don't have permission to modify company list.
                <BR />
					</cfif>
                                            
        </div>
        
		
		<div id="RightStage">
               
            <div style="width 350px; min-width: 350px; margin:0px 0 0px 0;">
            	
			                 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />
			 	<input TYPE="hidden" name="inpCompanyAccountId" id="inpCompanyAccountId"/>
					<label>Company Name
			        </label>
			        <input TYPE="text" name="inpCompanyName" id="inpCompanyName" size="255" class="ui-corner-all" disabled="true"/> 
			        
			        
			        <label>Primary Phone
			        </label>
			        <input TYPE="text" name="inpPrimaryPhone" id="inpPrimaryPhone" size="255" class="ui-corner-all password" title="Company Phone"  onChange="enableButton();"/> 
			        
			        <br /> 
			       
			        <label>Address 1
			        </label>
			        <input TYPE="text" name="inpAddress1" id="inpAddress1" size="255" class="ui-corner-all password"  /> 
			                
			        <BR>
					
			        <label>Address 2
			        </label>
			        <input TYPE="text" name="inpAddress2" id="inpAddress2" size="255" class="ui-corner-all password"  /> 
			                
			        <BR>		
					<cfif StructKeyExists(SESSION.accessRights,companyList)>
			        	<button id="UpdateCompanyAccountButton" TYPE="button" class="ui-corner-all" disabled="true">Update</button>
					</cfif>
			        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
			                
			       <!--- <div id="loadingDlgUpdateCompanyAccount" style="display:inline;">
			            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
			        </div>--->
                     	
            </div>
		</div>          
</form>

</div>


</cfoutput>

























