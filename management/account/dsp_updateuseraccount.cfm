<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>




<script TYPE="text/javascript">
	function UpdateUserAccount()
	{
		
		if($("#inpPhone").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							

			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=UpdateUserAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#UpdateUserAccountDiv #inpUserId").val(),inpUserLevelId : $("#UpdateUserAccountDiv #inpUserLevelId").val(), inpPrimaryPhone : $("#UpdateUserAccountDiv #inpPrimaryPhone").val(), inpAddress1 : $("#UpdateUserAccountDiv #inpAddress1").val(),inpAddress2 : $("#UpdateUserAccountDiv #inpAddress2").val(),inpIsActive: $("#UpdateUserAccountDiv #isActiveDiv #isActive").val(),inpCompanyAccountId : $("#UpdateUserAccountDiv #inpCompanyId").val(),inpEmail : $("#UpdateUserAccountDiv #inpEmail").val()}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								//jAlert("User Account has been updated.", "Success!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide();UpdateUserAccountDialog.remove(); } );
								$.jGrowl("Update User sucessfully", { life:2000, position:"center", header:' Message' });
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT beed updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;

	}
	
	function getSelectedUserAccountData()
	{			
		var UserAccountData=0;
		var template="";
		var selectedId=selectedUserId;
		alert
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=getUserAccountData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPUSERID :  selectedUserId  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.USERDATA[0]) != "undefined")
						{
							<!---
							template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all" > ';
							for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
								var value=d.DATA.COMPANYDATA[0][i][0];
								var text=d.DATA.COMPANYDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#BSContentHome #CompanyList").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	--->
							
							$("#UpdateUserAccountDiv #inpUserId").val(d.DATA.USERDATA[0][0]);
							$("#UpdateUserAccountDiv #inpUserName").val(d.DATA.USERDATA[0][1]);
							$("#UpdateUserAccountDiv #inpPrimaryPhone").val(d.DATA.USERDATA[0][2]);
							$("#UpdateUserAccountDiv #inpAddress1").val(d.DATA.USERDATA[0][3]);
							$("#UpdateUserAccountDiv #inpAddress2").val(d.DATA.USERDATA[0][4]);
							$("#UpdateUserAccountDiv #UserLevelList #inpUserLevelId").val(d.DATA.USERDATA[0][5]);
							if(d.DATA.USERDATA[0][6]==1){
								$("#UpdateUserAccountDiv #isActiveDiv #isActive").attr('checked',true);
							}else{
								$("#UpdateUserAccountDiv #isActiveDiv# #isActive").attr('checked',false);
							}
							if(typeof(d.DATA.USERDATA[0][7]) != "undefined" && d.DATA.USERDATA[0][7] > 0){
								//alert(d.DATA.USERDATA[0][7]);
								while(typeof($("#UpdateUserAccountDiv #inpCompanyId")) == "undefined"){
									
								}
									$("#UpdateUserAccountDiv #inpCompanyId").val(d.DATA.USERDATA[0][7]);
							}
							$("#UpdateUserAccountDiv #inpEmail").val(d.DATA.USERDATA[0][8]);
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdateCompanyAccount").hide();
			} );		
	
		return false;

	}	
	
	function getUserLevelList()
	{			
		var CompanyData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=RetrieveUserLevelList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.USERLEVELDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpUserLevelId" id="inpUserLevelId" size="1" class="ui-corner-all" onChange="enableButton();"> ';
							for(i=0; i<d.DATA.USERLEVELDATA[0].length ; i++){
								var value=d.DATA.USERLEVELDATA[0][i][0];
								var text=d.DATA.USERLEVELDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#UpdateUserAccountDiv #UserLevelList").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}
	
	function getCompanyList()
	{
		var CompanyData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/company.cfc?method=RetrieveCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPACTIVE : 1  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.COMPANYDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all"  > ';
							template += '<option value="0">--- Select a company ---</option>';
							for(i=0; i<d.DATA.COMPANYDATA[0].length ; i++){
								var value=d.DATA.COMPANYDATA[0][i][0];
								var text=d.DATA.COMPANYDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
							};	
							template += '</select>';	
							$("#UpdateUserAccountDiv #CompanyListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}		
	
	
	$(function()
	{	
	
		//$("#UpdateUserAccountDiv #UpdateUserAccountButton").click( function() { UpdateUserAccount(); return false;  }); 			
		<!--- Kill the new dialog --->
		$("#UpdateUserAccountDiv #Cancel").click( function() 
			{
					$("#loadingDlgUpdateCompanyAccount").hide();					
					UpdateUserAccountDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgUpdateCompanyAccount").hide();	
	} );

		function enableButton(){
			//$("#UpdateCompanyAccountDiv #UpdateCompanyAccountButton").set=false;
			$("#UpdateUserAccountButton").removeAttr('disabled').removeClass( 'ui-state-disabled' );

			//document.UpdateUserAccountForm.UpdateUserAccountButton.disabled=false;
		}
		
</script>
<script type="text/javascript">


	$(document).ready(function() {
		getCompanyList();
		getUserLevelList();
		
		while(true){
			if($("#inpCompanyId").val() != "undefined"){
				getSelectedUserAccountData();
				break;
			}
		}
		
	});	
	//alert(lastsel);
			$('#UpdateUserAccountForm :input').keyup(function(){
				$("#UpdateUserAccountButton").removeAttr('disabled').removeClass( 'ui-state-disabled' );

				//document.UpdateUserAccountForm.UpdateUserAccountButton.disabled=false;
				
			});
</script>


<style>

#RegisterNewAccountDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#RegisterNewAccountDiv div{
display:inline;
border:none;
}

</style> 
    
<cfoutput>

<div id='UpdateUserAccountDiv' class="RXForm">

<form id="UpdateUserAccountForm" name="UpdateUserAccountForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />
 	<input TYPE="hidden" name="inpUserId" id="inpUserId"/>
		<label>User Name
        </label>
        <input TYPE="text" name="inpUserName" id="inpUserName" size="255" class="ui-corner-all" disabled="true"/> 
		
        <label>User Level
        </label>
        <div id="UserLevelList"/>
        
        <br />
<cfif #Session.UserRole# EQ "SuperUser">
		<label>Company Name</label>
		<div id="CompanyListDiv"/>
        <br />
</cfif>
        
        <label>Primary Phone
        </label>
        <input TYPE="text" name="inpPrimaryPhone" id="inpPrimaryPhone" size="255" class="ui-corner-all password" title="Company Phone"  onChange="enableButton();"/> 
        <br /> 
		
        <label>Email
        </label>
        <input TYPE="text" name="inpEmail" id="inpEmail" size="255" class="ui-corner-all password" title="Email Address"  onChange="enableButton();"/> 
        <br />
       
        <label>Address 1
        </label>
        <input TYPE="text" name="inpAddress1" id="inpAddress1" size="255" class="ui-corner-all password"  onChange="enableButton();"/> 
                
        <BR>
		
        <label>Address 2
        </label>
        <input TYPE="text" name="inpAddress2" id="inpAddress2" size="255" class="ui-corner-all password"  onChange="enableButton();"/> 
		
		<BR>
		
		<div id="isActiveDiv">
				<label>Is Active ?</label>
	<cfif find('Firefox',cgi.HTTP_USER_AGENT)>
		<input type="checkbox" name="isActive"  id="isActive" value="1" style="vertical-align:middle;float:right;width:0px;margin-right:312px;"> 
	<cfelseif find('MSIE 9.0',cgi.HTTP_USER_AGENT)>
		<input type="checkbox" name="isActive"  id="isActive" value="1" style="vertical-align:middle;float:right;margin-right:1000px;"> 
	<cfelseif find('MSIE',cgi.HTTP_USER_AGENT)>
		<input type="checkbox" name="isActive"  id="isActive" value="1" style="vertical-align:middle;float:right;margin-right:1000px;"> 
	<cfelseif find('Chrome',cgi.HTTP_USER_AGENT)>
		<input type="checkbox" name="isActive"  id="isActive" value="1" style="vertical-align:middle;float:right;"> 
	<cfelseif find('Safari',cgi.HTTP_USER_AGENT)>
		<input type="checkbox" name="isActive"  id="isActive" value="1" style="vertical-align:middle;float:right;"> 
	</cfif>				
		
		</div>				
                
        <BR>		
		

<!--- 		<button id="UpdateUserAccountButton" TYPE="button" class="ui-corner-all" disabled="true">Update</button>			
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button> --->
		

                
       <!--- <div id="loadingDlgUpdateCompanyAccount" style="display:inline;">
            <img class="loadingDlgRegisterNewAccount" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>--->
</form>
</div>
</cfoutput>

























