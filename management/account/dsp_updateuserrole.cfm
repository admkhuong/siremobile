<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
</cfoutput>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>


<script TYPE="text/javascript">

	function _UpdateUserRole()
	{
		if($("#UpdateUserRoleDiv #inpUserId").val() == 0){
			jAlert('Please choose an user first','Failed');
			return;
		}
		if($("#UpdateUserRoleDiv #inpRoleId").val() == 0){
			jAlert('Please choose a role to assign','Failed');
			return;
		}

		$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=assignRoleToUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
		{ 
			inpUserId : $("#UpdateUserRoleDiv #inpUserId").val(), 
			inpRoleId : $("#UpdateUserRoleDiv #inpRoleId").val(),
		}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								//jAlert("Assign role to user sucessfully", "Success!", function(result) { UpdateUserRoleDialog.remove();} );
								$.jGrowl("Assign role to user sucessfully", { life:2000, position:"center", header:' Message' });
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT beed updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} );		

		return false;

	}	
	
	
	$(function()
	{	
		$("#UpdateUserRoleDiv #UpdateUserRoleButton").click( function() { _UpdateUserRole(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#UpdateUserRoleDiv #Cancel").click( function() 
			{
					UpdateUserRoleDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#loadingDlgAddNewCompanyAccount").hide();
		  
		  $("#inpNAPassword").keyup( function(event){  
//		         cur_val = $(this).val(); // grab what's in the field       							    
				 PasswordStrength($(this).val());
//				 $(this).val(cur_val);    
		  } );
	} );


	function getUserList()
	{
		var UserData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/management.cfc?method=RetrieveUserIdList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPACTIVE : 1  }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.USERACCOUNTDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpUserId" id="inpUserId" size="1" class="ui-corner-all" onChange="getUserRoleInfo();"> ';
							template += '<option value="0">Select an user</option>';
							
							for(i=0; i<d.DATA.USERACCOUNTDATA[0].length ; i++){
								var value=d.DATA.USERACCOUNTDATA[0][i][0];
								var text=d.DATA.USERACCOUNTDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#UpdateUserRoleDiv #UserListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}	

	function getRoleList()
	{
		var CompanyData=0;
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/management.cfc?method=getRoleList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  {   }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.ROLEDATA[0]) != "undefined")
						{
							template += '<select TYPE="select" name="inpRoleID" id="inpRoleId" size="1" class="ui-corner-all"  > ';
							template += '<option value="0">-----Select a Role-----</option>';
							for(i=0; i<d.DATA.ROLEDATA[0].length ; i++){
								var value=d.DATA.ROLEDATA[0][i][0];
								var text=d.DATA.ROLEDATA[0][i][1];
								template += '<option value="'+ value +'">'+ text + '</option>';
								
							};	
							template += '</select>';	
							$("#UpdateUserRoleDiv #RoleListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}	
		
		function getUserRoleInfo(){
		if($("#inpUserId").val()==0){
			$("#inpRoleId").val(0);
			return;
		}
			$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/management.cfc?method=getUserRoleInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#inpUserId").val() }, 
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.USERROLE) != "undefined" && d.DATA.USERROLE >0)
							{
								$("#inpRoleId").val(d.DATA.USERROLE);
							}else{
								$("#inpRoleId").val(0);
							}
						}
						else
						{<!--- No result returned --->							
							jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
						$("#loadingDlgRegisterNewAccount").hide();
				} );		
		
			return false;			
		}
		
</script>


<style>

#RegisterNewAccountDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#RegisterNewAccountDiv div{
display:inline;
border:none;
}

</style> 
    
	

<cfoutput>

<div id='UpdateUserRoleDiv' class="RXForm">

<form id="UpdateUserRoleForm" name="UpdateUserRoleForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>User Name
        </label>
		<div id="UserListDiv"/>
        
		<br/>

		<label>Role Name</label>
		<div id="RoleListDiv"/>
        <br /> 
<!---         <button id="UpdateUserRoleButton" TYPE="button" class="ui-corner-all">Assign</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button> --->

</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		getRoleList();
		getUserList();
	});	
</script>
</cfoutput>

