
<!--- 
Login page to check if user is logged in.

Include in application.cfc to ensure all pages are in session (user is logged in)

jQuery to call AJAX authentication - lighter and more control than coldfusion AJAX proxy and cfwindow vewrsion

Countdown timer triggered when login complete

Warning when about to expire.

 --->


<!--- Include countdown javascript --->
<cfoutput>
<cfinclude template="../public/js/SessionCountDown.cfm" /> 
</cfoutput>

<!--- If this is a logout request - go ahead and delete current login session --->
<cfif structKeyExists(URL,"logout") and URL.logout>
	<cflogout />
</cfif>

<!--- If session is still good default jQuery dialog to initally be hidden --->
<cfset autoOpen = "false">
    
<!--- Code between cflogin only executes if the cfloginuser is undefined --->  
<!--- cfloginuser is defined in the authentication.cfc component--->  
<cflogin>    
	<cfif NOT IsDefined("cflogin")>
        <cfset autoOpen = "true">
    <cfelse>
        <cfset autoOpen = "false">
    </cfif>
</cflogin>                



<!--- jQuery objects and authentication AJAX call(s) --->                
<script TYPE="text/javascript">

	
	$(function()
	{
		<!--- var UserId = <cfoutput>#Session.USERID#</cfoutput>;
		if (<cfoutput>#Session.USERID#</cfoutput>) { --->
		if(<cfoutput>#autoOpen#</cfoutput>){
			$('#container').show();
			$('#BSHeadLogin').hide();
			$('#BSContentWide').hide();
		}
		else{
			$('#container').hide();
			$('#BSHeadLogin').show();
			$('#BSContentWide').show();
			$
		}
 	
	});
	
	
	

	
	

	<!--- If just a reguilar page start auto count down --->
	<cfif autoOpen EQ "false">CountDown();</cfif>
	

</script>
