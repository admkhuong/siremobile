
<cfcomponent output="false" hint="Handle the application.">
	<cfparam name="Session.accessRights" default="#StructNew()#"/> 


<!--- Set TimeOutSeconds for 3600 - 60 minutes for one hour timeout by default - change this if you change the application timeout --->

<!--- Set up the application. --->
<cfset THIS.Name = "RXSS" />
<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
<cfset THIS.SessionManagement = true />
<cfset THIS.SetClientCookies = true />
<cfset THIS.clientmanagement = false />
<cfset THIS.setdomaincookies = false />
<cfset THIS.setdomaincookies = false />
<cfset THIS.loginstorage="session" />


<!--- Define the page request properties. --->
<cfsetting
    requesttimeout="3600"
    showdebugoutput="false"
    enablecfoutputonly="false"
/>
 

<cffunction name="OnApplicationStart"
access="public"
returntype="boolean"
output="false"
hint="Fires when the application is first CREATED.">
     
    
    <!--- Return out. --->
    <cfreturn true />
</cffunction>
 

<cffunction name="OnSessionStart"
access="public"
returntype="void"
output="true"
hint="Fires when the session is first CREATED.">
 <cfinclude template="paths.cfm" >
<!--- Return out. --->
<cfreturn />
</cffunction>
 

 
    
<cffunction name="OnRequestStart"
        access="public"
        returntype="boolean"
        output="true"
        hint="Fires at first part of page processing.">
    
    <!--- Define arguments. --->
    <cfargument name="TargetPage" TYPE="string" required="true"/>

		<!--- Bug in CF8 and later - if using the onRequest method in the application.cfc then cfc requests all return empty --->
		<cfscript>
          if ( right(arguments.targetPage,4) is ".cfc" ) {
            structDelete(this,"onRequest");
            structDelete(variables,"onRequest");
            }
		</cfscript>
	<cfif isDefined("Session.USERID") >
    
    	<cfinclude template="paths.cfm" >
    
    	<!---<cfset Session.USERID = "">--->
    
		<cfif Len(#Session.USERID#) GT 0>
			<cfset Session.accessRights=structNew()>
			<cfset args=StructNew()>
			<cfset args.inpUserId=#Session.USERID#>
			<cfinvoke argumentcollection="#args#" method="getRights" component="#Session.SessionCFCPath#.permission" returnvariable="Session.accessRights"/>		
		</cfif>

	</cfif>
        
    
        <!--- Return out. --->
        <cfreturn true />
    </cffunction>
     

 

<cffunction name="OnRequest"
access="public"
returntype="void"
output="true"
hint="Fires after pre page processing is complete.">
 

<!--- Define arguments. --->
<cfargument
name="TargetPage"
TYPE="string"
required="true"
/>
 
 	<cfinclude template="paths.cfm" >
 
    
 	<cfset MainCFCPath="#LocalServerDirPath#session/cfc">
    <cfset PublicPath="#LocalServerDirPath#public">
    <cfset ManagementPath="#LocalServerDirPath#management">
	<cfset SessionPath="#LocalServerDirPath#session">
    <cfset SessionDisplayPath="../Session/display/MB"> <!--- <cfset SessionDisplayPath="display/MB"> <cfset SessionDisplayPath="display/BabbleSphere">--->
    <cfset ManagementDisplayPath="../management/display/default">   
    <!--- Require HTTPS --->
    <cfset LocalProtocol="http"> <!--- replace with https later --->
    
    <cfparam name="Session.CUSERID" default="">
    <cfparam name="Session.USERID" default="">
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.UserLevel" default="10">
    <cfparam name="Session.AfterHours" default="0">
    <cfparam name="Session.AdvancedScheduleOptions" default="0">
    <cfparam name="Session.AdditionalDNC" default="">
    <cfparam name="Session.AdministratorAccessLevel" default="10">
    <cfparam name="Session.DefaultRate1" default="0.050">
    <cfparam name="Session.DefaultRate2" default="0.050">
    <cfparam name="Session.DefaultRate3" default="0.050">
    <cfparam name="Session.DefaultIncrement1" default="0.050">
    <cfparam name="Session.DefaultIncrement2" default="0.050">
    <cfparam name="Session.DefaultIncrement3" default="0.050">
    <cfparam name="Session.DefaultBalance" default="100">
    <cfparam name="Session.DefaultRateType" default="2">
    <cfparam name="Session.PrimaryPhone" default="0000000000">
    <cfparam name="Session.EmailAddress" default="unknown@unknown.com">
    <cfparam name="Session.generalAlertsDBSource" default="GlobalAlertsDB"/>
   <!--- <cfset Session.DBSourceEBM = "Bishop">       
    <!--- generalAlertsDBSource--->
    <cfset Session.generalAlertsDBSource = "GlobalAlertsDB">          
    <cfset Session.IsIntegratedSite = "1">        
    <cfset Session.AdministratorAccessLevel = 20>
    <cfset Session.DefaultRateType = "2">
    <cfset Session.DefaultRate1 = "0.050">
    <cfset Session.DefaultRate2 = "0.050">
    <cfset Session.DefaultRate3 = "0.050">
    <cfset Session.DefaultIncrement1 = "1">
    <cfset Session.DefaultIncrement2 = "0">
    <cfset Session.DefaultIncrement3 = "0">
   
    <cfset Session.DBSourceEBM = "Bishop"/> 
	--->
    
    <cfparam name="Session.MFAISON" default="0">
    <cfparam name="Session.MFALENGTH" default="0">
     
    <!--- Set for one hour timeout by default - change this if you change the application timeout --->
	<cfset TimeOutSeconds = 3600>
     
    <cfparam name="PageTitle" default="">
    
    
    <cfset ROOTPATH = "../">
    <cfset CBCompanyName = "ReactionX SS">
    <cfset DEBUG = "no">
        
        <!--- AND RIGHT(cgi.script_name, 3) NEQ "FileTreeConnector.cfm" AND ListLast(GetTemplatePath(), "\") DOES NOT CONTAIN "FileTreeConnector.cfm" AND ListLast(GetTemplatePath(), "/") DOES NOT CONTAIN ".swf" --->  
        <!--- AND RIGHT(cgi.script_name, 3) NEQ "swf" AND ListLast(GetTemplatePath(), "\") DOES NOT CONTAIN ".swf" AND ListLast(GetTemplatePath(), "/") DOES NOT CONTAIN ".swf" --->         
        <!--- Dont want all the display crap coming back for just CFC requests --->         
   
   
   <!---
        <cfif UCASE(RIGHT(cgi.script_name, 3)) NEQ "CFC" AND UCASE(ListLast(GetTemplatePath(), "/")) DOES NOT CONTAIN ".CFC" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "FILETREECONNECTOR.CFM" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "ACT_" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "DSP_" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "MHOME.CFM">
                    
          <!--- 
		    <cfoutput>
                RIGHT(cgi.script_name) = #RIGHT(cgi.script_name, 3)# <BR>
         		GetTemplatePath() = #GetTemplatePath()# <BR>
            	TargetPage = #TargetPage# <BR>    
            </cfoutput> 
			 --->
                                    
   			<!--- Start regulaer HTML here --->
            <cfinclude template="#SessionDisplayPath#/dsp_header.cfm" />
                             
            <!--- Secure the site in the header each display page--->
            <cfinclude template="act_login.cfm"> 	
					 			
            
			<!--- Wrap the body --->
		  	<cfinclude template="#ManagementDisplayPath#/dsp_body_management.cfm" /> 
                      
            <!--- Include the requested page. --->
            <cfinclude template="#ARGUMENTS.TargetPage#" />
		            
			<!--- Do footer stuff here --->
			<cfinclude template="#ManagementDisplayPath#/dsp_footer.cfm" />
		     
        <cfelseif UCASE(ListLast(GetTemplatePath(), "\")) CONTAINS "MHOME.CFM">
        	<!--- Stand alone page for log in - no session checks --->
        	<!--- Include the requested page. --->
            <cfinclude template="#ARGUMENTS.TargetPage#" />
        
        <cfelse>
                         
			  <!--- Just Include the requested page. --->
              <cfinclude template="#ARGUMENTS.TargetPage#" />
              
              
                
                
        </cfif>
         --->


		<!--- Just Include the requested page. --->
        <cfinclude template="#ARGUMENTS.TargetPage#" />
              
 

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnRequestEnd"
access="public"
returntype="void"
output="true"
hint="Fires after the page processing is complete.">
 

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnSessionEnd"
access="public"
returntype="void"
output="false"
hint="Fires when the session is terminated.">
 

<!--- Define arguments. --->
<cfargument
name="SessionScope"
TYPE="struct"
required="true"
/>
 

<cfargument
name="ApplicationScope"
TYPE="struct"
required="false"
default="#StructNew()#"
/>
 

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnApplicationEnd"
access="public"
returntype="void"
output="false"
hint="Fires when the application is terminated.">
 

<!--- Define arguments. --->
<cfargument name="ApplicationScope"
TYPE="struct"
required="false"
default="#StructNew()#"
/>
 

<!--- Return out. --->
<cfreturn />
</cffunction>
					 <!--- 
                    
                    
                        <!--- Application wide exception handler --->   
                        <cffunction
                            name="OnError"
                            access="public"
                            returntype="void"
                            output="true"
                            hint="Fires when an exception occures that is not caught by a try/catch.">
                         
                        
                            <!--- Define arguments. --->
                            <cfargument
                            name="Exception"
                            TYPE="any"
                            required="true"
                            />
                             
                            
                            <cfargument
                            name="EventName"
                            TYPE="string"
                            required="false"
                            default=""
                            />
                             
                             
                             To get more info - comment out OnError event handler in application.cfc <BR>
                             Exception = #Exception# <BR>
                             EventName = #EventName# <BR>
                             
                             
                            
                            <!--- Return out. --->
                            <cfreturn />
                        </cffunction>
                      --->

<!--- 

<cfapplication 
name="RXSS" 
loginstorage="session"  
clientmanagement="no" 
sessionmanagement="yes" 
setclientcookies="yes" 
setdomaincookies="no" 
sessiontimeout="#CreateTimeSpan(0, 0, 0, 30)#"
/>



<cfscript>
application.name = "RXSS";
application.sessionTimeout = CreateTimeSpan(0, 0, 0, 30); // session timeout in seconds (we set it to 10 secs for this test)
application.sessionTimeoutAlert = 5; //how many seconds before the timeout to send the alert
application.sessionTimeoutUseAjax=true; //use Ajax or reload the page
application.loginstorage="session";
application.sessionmanagement="yes"; 
application.clientmanagement="no";
application.setclientcookies="yes"; 
application.setdomaincookies="no"; 
</cfscript> 
 --->

</cfcomponent> 






<!--- <cfcomponent output="false">


request.sessionTimeoutURL="index.cfm?fuseaction=home.logout"; //URL to jump when client denies refreshing
request.scriptDir = "scripts/"; //folder for the javascripts
request.componentDir = "components/"; //folder for the components
request.sessionRefreshFunction = "sessionRefresh"; //name of the function to call


	<cfset this.mappings["/SitePath"]="c:\DEV3.TELESPEECH\devjlp\EBM_Dev">
	
    <cffunction name="onSessionStart">
    <cfscript>
        Session.started = now();
        Session.shoppingCart = StructNew();
        Session.shoppingCart.items =0;
    </cfscript>
        <cflock scope="Application" timeout="5" TYPE="Exclusive">
            <cfset Application.sessions = Application.sessions + 1>
    </cflock>


	</cffunction>

	
</cfcomponent> --->