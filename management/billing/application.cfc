
<cfcomponent output="true" hint="Handle the application.">
<cfparam name="Session.accessRights" default="#StructNew()#"/> 
		

 
<!--- Set TimeOutSeconds for 3600 - 60 minutes for one hour timeout by default - change this if you change the application timeout --->

<!--- Set up the application. --->
<cfset THIS.Name = "RXMBEBMDemo" />
<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
<cfset THIS.SessionManagement = true />
<!---<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 60, 0 ) />--->
<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 20, 10 ) />
<cfset THIS.SetClientCookies = false />
<cfset THIS.clientmanagement = false />
<cfset THIS.setdomaincookies = false />
<cfset THIS.setdomaincookies = false />
<cfset THIS.loginstorage="session" />


<!--- Define the page request properties. --->
<cfsetting
    requesttimeout="20"
    showdebugoutput="false"
    enablecfoutputonly="false"
/> 

<cffunction name="OnApplicationStart"
access="public"
returntype="boolean"
output="false"
hint="Fires when the application is first CREATED.">
     
     <!--- Facebook Application id --->
     <cfset APPLICATION.appId = 196029783766423 />
     <!--- Application wide key used to encrypt sensitive information in the client cookies --->
     <cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
     <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
     <cfset APPLICATION.sessions = 0>
     <cfset APPLICATION.SiteAdmin = 'jeff@messagebroadcast.com;jpeterson@messagebroadcast.com;tbayramkul@messagebroadcast.com'>
	 <cfset APPLICATION.regularsite = "dev.telespeech.com/devjlp/EBM_DEV">
     <cfset APPLICATION.enemv = "9+XTr15IklW/mfg2kU0NnQ==">
    <!--- Return out. --->
    <cfreturn true />
</cffunction>
 

 

<cffunction name="OnSessionStart"
access="public"
returntype="void"
output="false"
hint="Fires when the session is first CREATED.">

 	
     <!--- Define the LOCALOUTPUT scope. --->
    <cfset var LOCALOUTPUT = {} />
   
    <!--- solves the problem of persisting session variables in IE --->
	<cfcookie name="CFID" value="#Session.CFID#" domain="#CGI.SERVER_NAME#">
	<cfcookie name="CFTOKEN" value="#Session.CFTOKEN#" domain="#CGI.SERVER_NAME#">
 	<!--- END:: solves the problem of persisting session variables in IE --->
 
 	<cfinclude template="../public/paths.cfm" >
 
	
    <cfset APPLICATION.sessions = APPLICATION.sessions + 1>   
    <!---
    Store the CF id and token. We are about to clear the session scope for intialization and want to make sure
    we don't lose our auto-generated tokens.
    --->
    <cfset LOCALOUTPUT.CFID = Session.CFID />
    <cfset LOCALOUTPUT.CFTOKEN = Session.CFTOKEN />
    
    <!--- Clear the Session. --->
    <cfset StructClear( Session ) />
    
    <!---
    Replace the id and token so that the ColdFusion
    application knows who we are.
    --->
    <cfset Session.CFID = LOCALOUTPUT.CFID />
    <cfset Session.CFTOKEN = LOCALOUTPUT.CFTOKEN />
            
    <!---
    Now that we are starting a new session, let's check to see if this user want to be automatically logged in using their cookies.
         
    Since we don't know if the user has this "remember me" cookie in place, I would normally say let's param it
    and then use it. However, since this process involves decryption which might throw an error, I say, let's
    just wrap the whole thing in a TRY / CATCH and that way we don't have to worry about the multiple checks.
    --->
    <cftry>
			

    
		<!--- Decrypt out remember me cookie. --->
        <cfset LOCALOUTPUT.RememberMe = Decrypt(
        COOKIE.RememberMe,
        APPLICATION.EncryptionKey_Cookies,
        "cfmx_compat",
        "hex"
        ) />
        
        <!---
        For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
        of list. Extract it from the list.
        --->
        <cfset CurrRememberMe = ListGetAt(
        LOCALOUTPUT.RememberMe,
        3,
        ":"
        ) />
        
        
        <!---
        Check to make sure this value is numeric, otherwise, it was not a valid value.
        --->
        <cfif IsNumeric( CurrRememberMe )>
            
            <!---
            We have successfully retreived the "remember me" ID from the user's cookie. Now, store
            that ID into the session as that is how we are tracking the logged-in status.
            --->
            <cfset Session.USERID = CurrRememberMe />
                           
        </cfif>
    	
    	<cfset Session.PrimaryPhone = ListGetAt(LOCALOUTPUT.RememberMe, 4, ":") />
        <cfset Session.at = ListGetAt(LOCALOUTPUT.RememberMe, 5, ":") />
        <cfset Session.facebook = ListGetAt(LOCALOUTPUT.RememberMe, 6, ":") />
        <cfset Session.fbuserid = ListGetAt(LOCALOUTPUT.RememberMe, 7, ":") />
        <cfset Session.EmailAddress = ListGetAt(LOCALOUTPUT.RememberMe, 8, ":") /> 
        <cfset Session.UserNAme = ListGetAt(LOCALOUTPUT.RememberMe, 10, ":") />
        <cfset SESSION.CompanyUserId = ListGetAt(LOCALOUTPUT.RememberMe, 11, ":") />  
        <cfset Session.permissionManaged = ListGetAt(LOCALOUTPUT.RememberMe, 12, ":") />  
		
        <cfparam name="Session.DBSourceEBM" default="BISHOP"/> 
        
        <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
           	UPDATE
            	simpleobjects.useraccount
            SET
            	LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'  
            WHERE                
           		UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">  
            AND (CBPID_int = 1 OR CBPID_int IS NULL)                          
        </cfquery>  
    
    <!--- Catch any errors. --->
    <cfcatch TYPE="any">
	    <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
        <!--- redirect to login--->
        <!---<cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home" addtoken="false">--->
        
    </cfcatch>
    </cftry>
 
 	<cfinclude template="../public/paths.cfm" >
   
	<cfset PublicPath="#LocalServerDirPath#public">
    <cfset ManagementPath="#LocalServerDirPath#management">
	<cfset SessionPath="#LocalServerDirPath#session">
    <cfset SessionDisplayPath="display/MB"> <!--- <cfset SessionDisplayPath="display/MB"> <cfset SessionDisplayPath="display/BabbleSphere">--->
    
    <cfparam name="Session.USERID" default="-1">
    <cfparam name="SESSION.CompanyUserId" default="#Session.USERID#">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/> 
    <cfparam name="Session.UserLevel" default="10">
    <cfparam name="Session.UserName" default="NA">
    <cfparam name="Session.rxdsShowAllUsers" default="0">
    <cfparam name="Session.AfterHours" default="0">
    <cfparam name="Session.AdvancedScheduleOptions" default="0">
    <cfparam name="Session.AdditionalDNC" default="">
    <cfparam name="Session.AdministratorAccessLevel" default="10">
    <cfparam name="Session.DefaultRate1" default="0.050">
    <cfparam name="Session.DefaultRate2" default="0.050">
    <cfparam name="Session.DefaultRate3" default="0.050">
    <cfparam name="Session.DefaultIncrement1" default="0.050">
    <cfparam name="Session.DefaultIncrement2" default="0.050">
    <cfparam name="Session.DefaultIncrement3" default="0.050">
    <cfparam name="Session.DefaultBalance" default="100">
    <cfparam name="Session.DefaultRateType" default="2">
    <cfparam name="Session.PrimaryPhone" default="0000000000">
    <cfparam name="Session.PrimaryContact" default="0000000000">
    <cfparam name="Session.EmailAddress" default="unknown@unknown.com">
    <cfparam name="Session.QARXDialer" default="10.11.100.104">    
    
    <cfset session.abcd = 12>
    
    <cfset Session.IsIntegratedSite = "1">   
    <cfset Session.AdministratorAccessLevel = 20>
    <cfset Session.DefaultRateType = "2">
    <cfset Session.DefaultRate1 = "0.050">
    <cfset Session.DefaultRate2 = "0.050">
    <cfset Session.DefaultRate3 = "0.050">
    <cfset Session.DefaultIncrement1 = "1">
    <cfset Session.DefaultIncrement2 = "0">
    <cfset Session.DefaultIncrement3 = "0">
    <cfset Session.DBSourceEBM = "Bishop"/> 
     
    <!--- Set for one hour timeout by default - change this if you change the application timeout --->
	<cfset TimeOutSeconds = 3600>
             
    <cfparam name="PageTitle" default="">

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 
    
<cffunction name="OnRequestStart"
    access="public"
    returntype="boolean"
    output="true"
    hint="Fires at first part of page processing.">

<!--- Define arguments. --->
<cfargument name="TargetPage" TYPE="string" required="true"/>


    <!--- Bug in CF8 and later - if using the onRequest method in the application.cfc then cfc requests all return empty --->
    <cfscript>          
      if ( right(arguments.targetPage,4) is ".cfc" ) {
        structDelete(this,"onRequest");
        structDelete(variables,"onRequest");
        }
    </cfscript>
	
	<cfif Session.USERID EQ "" OR Session.USERID LT 1>
    
    	<cfinclude template="../public/paths.cfm" >
    	<cfif IsDefined("SESSION.CompanyUserId")>
		<cfif Len(#SESSION.CompanyUserId#) GT 0>
			<cfset Session.accessRights=structNew()>
			<cfset args=StructNew()>
			<cfset args.inpUserId=#SESSION.CompanyUserId#>
			<cfinvoke argumentcollection="#args#" method="getRights" component="#Session.SessionCFCPath#.permission" returnvariable="Session.accessRights"/>		
		</cfif>
		</cfif>
    <cfelse>    
   <!---  	<cfif (Session.USERID EQ "" OR Session.USERID LT 1) AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "DSP_COMMENTS" AND FINDNOCASE("#PublicPath#/home", GetTemplatePath())>
             <cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home" addtoken="false">
		</cfif>		    --->            
	</cfif>

    <!--- Return out. --->
    <cfreturn true />
</cffunction>
     

 

<cffunction name="OnRequest"
access="public"
returntype="void"
output="true"
hint="Fires after pre page processing is complete.">
 

<!--- Define arguments. --->
<cfargument
name="TargetPage"
TYPE="string"
required="true"
/>
	<cfinclude template="../public/paths.cfm" >

   
    
	<cfset PublicPath="#LocalServerDirPath#public">
    <cfset ManagementPath="#LocalServerDirPath#management">
	<cfset SessionPath="#LocalServerDirPath#session">
    <cfset SessionDisplayPath="display/MB"> <!--- <cfset SessionDisplayPath="display/MB"> <cfset SessionDisplayPath="display/BabbleSphere">--->
    
    <cfparam name="Session.USERID" default="">
    <cfparam name="SESSION.CompanyUserId" default="#Session.USERID#">
    <cfparam name="Session.DBSourceEBM" default="BISHOP"/> 
    <cfparam name="Session.UserLevel" default="10">
    <cfparam name="Session.UserName" default="NA">
    <cfparam name="Session.rxdsShowAllUsers" default="0">
    <cfparam name="Session.AfterHours" default="0">
    <cfparam name="Session.AdvancedScheduleOptions" default="0">
    <cfparam name="Session.AdditionalDNC" default="">
    <cfparam name="Session.AdministratorAccessLevel" default="10">
    <cfparam name="Session.DefaultRate1" default="0.050">
    <cfparam name="Session.DefaultRate2" default="0.050">
    <cfparam name="Session.DefaultRate3" default="0.050">
    <cfparam name="Session.DefaultIncrement1" default="0.050">
    <cfparam name="Session.DefaultIncrement2" default="0.050">
    <cfparam name="Session.DefaultIncrement3" default="0.050">
    <cfparam name="Session.DefaultBalance" default="100">
    <cfparam name="Session.DefaultRateType" default="2">
    <cfparam name="Session.PrimaryPhone" default="0000000000">
    <cfparam name="Session.PrimaryContact" default="0000000000">
    <cfparam name="Session.EmailAddress" default="unknown@unknown.com">
    <cfparam name="Session.QARXDialer" default="10.11.100.104">    
    
    <cfparam name="Session.permissionManaged" default=true>
    
    <cfparam name="Session.accessRights" default="#StructNew()#"/> 
    
        
    <cfset session.abcd = 12>
    
    <cfset Session.IsIntegratedSite = "1">   
    <cfset Session.AdministratorAccessLevel = 20>
    <cfset Session.DefaultRateType = "2">
    <cfset Session.DefaultRate1 = "0.050">
    <cfset Session.DefaultRate2 = "0.050">
    <cfset Session.DefaultRate3 = "0.050">
    <cfset Session.DefaultIncrement1 = "1">
    <cfset Session.DefaultIncrement2 = "0">
    <cfset Session.DefaultIncrement3 = "0">
   
    <cfset Session.DBSourceEBM = "Bishop"/> 
     
    <!--- Set for one hour timeout by default - change this if you change the application timeout --->
	<cfset TimeOutSeconds = 3600>
     
    <cfparam name="PageTitle" default="">
    
    <!---- we need to read cookies on every FIRST request when user registered, as user might logout and try to create a new account --->
    <cfif isdefined("session") and Session.USERID LT 1>
    	<cftry>
            <!--- Decrypt out remember me cookie. --->
            <cfset LOCALOUTPUT.RememberMe = Decrypt(
            COOKIE.RememberMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
            ) />
            
            <!---
            For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
            of list. Extract it from the list.
            --->
            <cfset CurrRememberMe = ListGetAt(
            LOCALOUTPUT.RememberMe,
            3,
            ":"
            ) />
            
            
            <!---
            Check to make sure this value is numeric, otherwise, it was not a valid value.
            --->
            <cfif IsNumeric( CurrRememberMe )>
                
                <!---
                We have successfully retreived the "remember me" ID from the user's cookie. Now, store
                that ID into the session as that is how we are tracking the logged-in status.
                --->
                <cfset Session.USERID = CurrRememberMe />                
                <cfset SESSION.isAdmin = true>
                <cfset SESSION.loggedIn = "1" />
                
                     
            </cfif>
            
            <cfset Session.PrimaryPhone = ListGetAt(LOCALOUTPUT.RememberMe, 4, ":") />
            <cfset Session.at = ListGetAt(LOCALOUTPUT.RememberMe, 5, ":") />
            <cfset Session.facebook = ListGetAt(LOCALOUTPUT.RememberMe, 6, ":") />
            <cfset Session.fbuserid = ListGetAt(LOCALOUTPUT.RememberMe, 7, ":") />
            <cfset Session.EmailAddress = ListGetAt(LOCALOUTPUT.RememberMe, 8, ":") /> 
            <cfset Session.UserNAme = ListGetAt(LOCALOUTPUT.RememberMe, 10, ":") /> 
            <cfset SESSION.CompanyUserId = ListGetAt(LOCALOUTPUT.RememberMe, 11, ":") />  
            <cfset Session.permissionManaged = ListGetAt(LOCALOUTPUT.RememberMe, 12, ":") /> 
            
            <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
                UPDATE
                    simpleobjects.useraccount
                SET
                    LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'  
                WHERE                
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">       
                AND (CBPID_int = 1 OR CBPID_int IS NULL)                 
            </cfquery>  
        
        <!--- Catch any errors. --->
        <cfcatch TYPE="any">
            <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
        </cfcatch>
        </cftry>
        <!---- END :: we need to read cookies on every request, as user might logout and try to create a new account --->
    </cfif>
    
    <cfset ROOTPATH = "../">
    <cfset CBCompanyName = "MessageBroadcast">
    <cfset DEBUG = "no">

        <cfset userObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.usersTool")>
		<cfset checkLoginUser = userObject.GetUserRoleName(Session.USERID)>
		<cfset Session.userRole = checkLoginUser.USERROLE>
        <!--- AND RIGHT(cgi.script_name, 3) NEQ "FileTreeConnector.cfm" AND ListLast(GetTemplatePath(), "\") DOES NOT CONTAIN "FileTreeConnector.cfm" AND ListLast(GetTemplatePath(), "/") DOES NOT CONTAIN ".swf" --->  
        <!--- AND RIGHT(cgi.script_name, 3) NEQ "swf" AND ListLast(GetTemplatePath(), "\") DOES NOT CONTAIN ".swf" AND ListLast(GetTemplatePath(), "/") DOES NOT CONTAIN ".swf" --->         
        <!--- Dont want all the display crap coming back for just CFC requests --->         
        <cfif UCASE(ListLast(GetTemplatePath(), "/")) DOES NOT CONTAIN "CFDIV" AND UCASE(RIGHT(cgi.script_name, 3)) NEQ "CFC" AND UCASE(ListLast(GetTemplatePath(), "/")) DOES NOT CONTAIN ".CFC" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "FILETREECONNECTOR.CFM" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "ACT_" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "DSP_" AND  UCASE(GetTemplatePath()) DOES NOT CONTAIN "rxds">
                     
          <!--- 
		    <cfoutput>
                RIGHT(cgi.script_name) = #RIGHT(cgi.script_name, 3)# <BR>
         		GetTemplatePath() = #GetTemplatePath()# <BR>
            	TargetPage = #TargetPage# <BR>    
            </cfoutput> 
			 --->
            
            <!--- Start regulaer HTML here --->
            <cfinclude template="#SessionDisplayPath#/dsp_header.cfm" />
                             
            <!--- Secure the site in the header each display page--->
            <cfinclude template="act_login.cfm"> 			 			
            
			<!--- Wrap the body --->
		  	<cfinclude template="#SessionDisplayPath#/dsp_body.cfm" /> 
                      
            <!--- Include the requested page. --->
            <cfinclude template="#ARGUMENTS.TargetPage#" />
		            
			<!--- Do footer stuff here --->
			<cfinclude template="#SessionDisplayPath#/dsp_footer.cfm" />
		
	        <script TYPE="text/javascript">			
				function SessionExpired()
				{		
					window.location.href= "#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home?inpMSG=#URLEncodedFormat('Your Account has been logged out due to inactivity.')#"; 
				}
			</script> 
                
       
       <cfelseif  UCASE(ListLast(GetTemplatePath(), "\")) CONTAINS "DSP_"> 
	                   
             <cfif (Session.USERID EQ "" OR Session.USERID LT 1) AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "DSP_COMMENTS" AND FINDNOCASE("#PublicPath#/home", GetTemplatePath())>
                
                <cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home" addtoken="false">
				
				<script TYPE="text/javascript">			
					 SessionExpired();					
				</script> 
                
              <cfelse>
              
              	<!--- Just Include the requested page. --->
              	<cfinclude template="#ARGUMENTS.TargetPage#" />
                             
              </cfif>
              
        <cfelse>

     <!---         
	 		 <cfif UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "_NORESET" >
                <script TYPE="text/javascript">					
					
				</script>
              
              </cfif>
      --->
                         
			  <!--- Just Include the requested page. --->
              <cfinclude template="#ARGUMENTS.TargetPage#" />
              
              
                
                
        </cfif>
         


 

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnRequestEnd"
access="public"
returntype="void"
output="true"
hint="Fires after the page processing is complete.">
 

<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnSessionEnd"
access="public"
returntype="void"
output="false"
hint="Fires when the session is terminated.">
 

<!--- Define arguments. --->
<cfargument
name="SessionScope"
TYPE="struct"
required="true"
/>
 

<cfargument
name="ApplicationScope"
TYPE="struct"
required="false"
default="#StructNew()#"
/>
<cfset THIS.SessionManagement = true />
<!---<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 60, 0 ) />--->
<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 0, 0 ) />
<cfset structClear(arguments.sessionScope)>


<!--- Return out. --->
<cfreturn />
</cffunction>
 

 

<cffunction name="OnApplicationEnd"
access="public"
returntype="void"
output="false"
hint="Fires when the application is terminated.">
 

<!--- Define arguments. --->
<cfargument
name="ApplicationScope"
TYPE="struct"
required="false"
default="#StructNew()#"
/>
 

<!--- Return out. --->
<cfreturn />
</cffunction>
					 <!--- 
                    
                    
                        <!--- Application wide exception handler --->   
                        <cffunction
                            name="OnError"
                            access="public"
                            returntype="void"
                            output="true"
                            hint="Fires when an exception occures that is not caught by a try/catch.">
                         
                        
                            <!--- Define arguments. --->
                            <cfargument
                            name="Exception"
                            TYPE="any"
                            required="true"
                            />
                             
                            
                            <cfargument
                            name="EventName"
                            TYPE="string"
                            required="false"
                            default=""
                            />
                             
                             
                             To get more info - comment out OnError event handler in application.cfc <BR>
                             Exception = #Exception# <BR>
                             EventName = #EventName# <BR>
                             
                             
                            
                            <!--- Return out. --->
                            <cfreturn />
                        </cffunction>
                      --->

<!--- 

<cfapplication 
name="RXSS" 
loginstorage="session"  
clientmanagement="no" 
sessionmanagement="yes" 
setclientcookies="yes" 
setdomaincookies="no" 
sessiontimeout="#CreateTimeSpan(0, 0, 0, 30)#"
/>



<cfscript>
application.name = "RXSS";
application.sessionTimeout = CreateTimeSpan(0, 0, 0, 30); // session timeout in seconds (we set it to 10 secs for this test)
application.sessionTimeoutAlert = 5; //how many seconds before the timeout to send the alert
application.sessionTimeoutUseAjax=true; //use Ajax or reload the page
application.loginstorage="session";
application.sessionmanagement="yes"; 
application.clientmanagement="no";
application.setclientcookies="yes"; 
application.setdomaincookies="no"; 
</cfscript> 
 --->

</cfcomponent> 






<!--- <cfcomponent output="false">


request.sessionTimeoutURL="index.cfm?fuseaction=home.logout"; //URL to jump when client denies refreshing
request.scriptDir = "scripts/"; //folder for the javascripts
request.componentDir = "components/"; //folder for the components
request.sessionRefreshFunction = "sessionRefresh"; //name of the function to call


	<cfset this.mappings["/SitePath"]="c:\DEV3.TELESPEECH\devjlp\SimpleFramedX">
	
    <cffunction name="onSessionStart">
    <cfscript>
        Session.started = now();
        Session.shoppingCart = StructNew();
        Session.shoppingCart.items =0;
    </cfscript>
        <cflock scope="Application" timeout="5" TYPE="Exclusive">
            <cfset Application.sessions = Application.sessions + 1>
    </cflock>


	</cffunction>

	
</cfcomponent> --->