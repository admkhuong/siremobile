<!--- Most java and css goes before here --->
</head>



<!--- Start the main body --->
<body >
<div id="fb-root"></div>
<div id="BSHeader">

	<div id="BSHeadCenter">
    	<div id="BSHeadLogo">
    		<a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home" style="text-decoration:none;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/MBIcon1Web.png"></a>
            <BR>
            <h4>Management Console</h4>
        </div>
        
        <div id="BSHeadLogin">
        	<div style="display:block; text-align:right;">
				<cfset currentURL="#CGI.SCRIPT_NAME#">
				<cfif currentURL CONTAINS "management/account/home">
					<a href='<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/account/home'>My Account</a> | <a href='<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/act_logout.cfm'>Logout</a>
					<cfelse>
					<a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home'>My Account</a> | <a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout.cfm'>Logout</a>
				</cfif>
       	</div>

            <div id="sessionAccountInfoContainer" style="text-align:right;" title="<cfoutput>#Session.UserId#</cfoutput>">You are currently logged in as <cfoutput>#Session.EmailAddress#</cfoutput></div>  
        
        </div>
    </div>

</div>

<div id="BSContentWide">

      
    <div id="BSCLeft" align="left">
