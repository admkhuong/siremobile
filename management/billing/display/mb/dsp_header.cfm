<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!---<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">--->
<!---<meta http-equiv="Content-Type" content="application/json; charset=utf-8"/>--->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!---<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script src="json.org/json2.js" type="text/javascript"></script>
--->

<title></title>

<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link type="text/css" href="#rootUrl#/#SessionPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <!---<link type="text/css" href="#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link type="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/RXForm.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/rxform2.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/MB.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/vtabs.css" />
	<link type="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
	<link rel="stylesheet" type="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/BB.ui.jqgrid.css" />
	--->
	
	<style>
		/*@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');*/
		@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');
		@import url('#rootUrl#/#PublicPath#/css/RXForm.css');
		@import url('#rootUrl#/#PublicPath#/css/rxform2.css');
		@import url('#rootUrl#/#PublicPath#/css/style_ebm.css');
		@import url('#rootUrl#/#PublicPath#/css/vtabs.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
		@import url('#rootUrl#/#PublicPath#/css/BB.ui.jqgrid.css');
		@import url('#rootUrl#/#SessionPath#/MCID/css/override.css');
		@import url('#rootUrl#/#PublicPath#/js/validate/validationEngine.jquery.css');
		@import url('#rootUrl#/#PublicPath#/js/validate/template.css');	
		@import url('#rootUrl#/#PublicPath#/js/help/tipsy.css');
		@import url('#rootUrl#/#SessionPath#/MCID/css/pagination.css');
		@import url('#rootUrl#/#PublicPath#/css/tabicons.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.ui.timepicker.css');
		@import url('#rootUrl#/#SessionPath#/MCID/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/mediaelement/mediaelementplayer.min.css');
		@import url('#rootUrl#/#PublicPath#/css/socialmedia.css');
	</style>
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#SessionPath#/MCID/css/poolballs/IVR.css" />	
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#PublicPath#/css/MB.css" />	
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#SessionPath#/MCID/css/rxt/edit_rxt.css" />	
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#SessionPath#/MCID/css/scriptbuilder2.css" />	
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#SessionPath#/MCID/css/campaignicons.css" />	
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#PublicPath#/css/VoiceToolIcons.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#SessionPath#/MCID/css/ruler.css" />
	
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
  	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.mousewheel.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <script src="http://cdn.jquerytools.org/1.2.6/form/jquery.tools.min.js"></script>
 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
    <script src="#rootUrl#/#PublicPath#/js/grid.locale-en.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.jqGrid.min.Light.RXMod.js" type="text/javascript"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.print.js" type="text/javascript"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/fisheye-iutil.min.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jeditable.js"></script>
    <script src="#rootUrl#/#PublicPath#/js/pluginDetect.js" type="text/javascript"></script>
<!---   	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/cloud-carousel.1.0.5.js"></script>--->
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/cloud-carousel.1.0.5.RXMod.js"></script>
    <!---<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/tiny_mce/tiny_mce.js"></script>--->
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-swapsies.js"></script>
 	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.tablednd.0.6.min.js"></script>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/validate/jquery.validate.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#SessionPath#/MCID/js/jquery.jgrowl.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/mediaelement/mediaelement-and-player.min.js"></script> <!--- media player for recorded responses --->

 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
 	<!--- Javascript encoding for XML - make sure your queries protect against SQL injection attacks --->
  	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/RXXML.js"></script>
    
    <!--- Removed for better one - JLP--->
    <!---<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/timepicker.js"></script>--->
    
    <!---http://fgelinas.com/code/timepicker/ --->
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.ui.timepicker.js"></script>
	<!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-timepicker-addon.js"></script> --->
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#PublicPath#/css/colorpicker.css" />
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/colorpicker.js"></script> 
	<link rel="stylesheet" href="#rootUrl#/#SessionPath#/MCID/css/print-preview.css" type="text/css" media="screen">
	
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.ui-jalert.js"></script>
<!--- 	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-themeroller.js"></script>
	<link rel="stylesheet" media="screen" type="text/css" href="#rootUrl#/#PublicPath#/css/jquery-ui-themeroller.css" /> --->	
<style>

<!--- Allows easier access to jquery dialogs with no title bar just add dialogClass: 'noTitleStuff' to dialog init --->
.noTitleStuff .ui-dialog-titlebar {display:none} 

</style>


<script type="text/javascript">
	var CurrSitePath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
	
	var Tipsy
</script>

<!--- Enable hover functionality ---> 
<script type="text/javascript">
$(function(){

});
</script>


</cfoutput>
  


<cfif (Session.USERID EQ "" OR Session.USERID LT 1) AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "DSP_COMMENTS" AND FINDNOCASE("#PublicPath#/home", GetTemplatePath())>
	 <cflocation url="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home" addtoken="false">
</cfif>		        
        
        
<cfset Session.IsIntegratedSite = 1>