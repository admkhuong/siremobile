<cfsetting showdebugoutput="no">
<cfsetting showdebugoutput="no">
<cfsetting requesttimeout="3600" />


<cfset AppDBSwitchMainDB = "Bishop">
<!--- <cfset AppDBSwitchMainDB = "ChaseBUDB"> --->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Chase Alerts Billing</title>

<link rel="stylesheet" type="text/css" href="css/reports.css" />

<cfhtmlhead text='<script language="JavaScript" src="javascript/CalendarDatePickerPopup.js"></script>'>  

<script language="javascript">

	function LocalForm()
	{				
		document.MainForm.action ="dsp_CallDetailsCSV.cfm";	
		document.MainForm.target ="_blank"		
	}

</script>


<style>

input#AtoInv_dt 
{
 font-size: 12px; 
 font-weight: bold; 
 color:#BBBDC0; 
 width: 90px; 
 margin-right: 6px;
 background-color:#FFFFFF;   
}

</style>


</head>

<body class="FontTiny">


<cfparam name="AtoInv" default="">
<cfparam name="AtoInv_dt" default="">


<cfparam name="Start_dt" default="">
<cfparam name="Stop_dt" default="">


<cfparam name="Start2_dt" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="TransID" default="">

<cfparam name="BatchIDA" default="356">
<!---<cfparam name="BatchIDA" default="48842">---> <!--- Alerts --->
<cfparam name="BatchIDB" default="48841"> <!--- Security --->
<cfparam name="BatchIDC" default="48840"> <!--- MFA --->


<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>

<!--- 
<cfif NOT IsDate(Start_dt)>
	<cfset Start_dt = dateformat(NOW(), "yyyy-mm-dd")>
</cfif>
 --->


<cfif NOT IsDate(Start_time)>
	<cfset Start_time = "00:00:00">
</cfif>


<!--- Make it easier for web-page user:  Enter Begin Date Only... means make End Date equal to Midnight on the same date (as Start Date) --->
<cfif IsDate(Start_dt) AND NOT IsDate(Stop_dt)>
	<cfset Stop_dt = dateformat(dateadd("d", 30, Start_dt), "yyyy-mm-dd")>
</cfif>

<cfif IsDate(Start_dt) AND NOT IsDate(Start2_dt)>
	<cfset Buff2_dt = dateformat(dateadd("m", -1, Start_dt), "yyyy-mm-dd")>
	<cfset Start2_dt = dateformat(  "#Year(Buff2_dt)#-#Month(Buff2_dt)#-01", "yyyy-mm-dd")>		
</cfif>

<cfif NOT IsDate(Stop_time)>
	<cfset Stop_time = "23:59:59">
</cfif>

<cfoutput>



<div style="position:absolute; top:5px; left:0px">
	<a href="ReportMenu.cfm" title="Click here for reports main menu"><img src="images/MBLogo.gif" border="0"></img></a>
</div>

<div align="left" style="position:absolute; top:5px; left:150px">

<form action="" method="post" name="MainForm">

<table border="0" cellspacing="0" cellpadding="3" width="600px">
	<tr>
	
		<td class="FontTitle" align="left">
			MessageBroadcast
		</td>
        
        <td align="right" class="FontBold">
	  		<cfif IsDate(AtoInv_dt)>
                Invoice Number: #AtoInv#<BR>
                Invoice Date: #dateformat(AtoInv_dt, "mm-dd-yyyy")#             		
            </cfif>            
            
            
        </td>
	</tr>
	<tr>
		<td class="FontSmall" align="left" colspan="2">
			4685 MacArthur Ct, Suite 250<BR>
			Newport Beach, CA 92660<BR>
			Tel:(949) 428-3111    Fax:(949)219-1948<BR>
			Federal Tax ID## 77-0480271
		</td>
               
	</tr>
	

	<tr>
		<td class="FontTitle">
			
            <cfif NOT IsDate(AtoInv_dt)>
            	<a href="##" onClick="MainForm.submit()" class="textlink"> Chase Alerts Billing Detail Report </a>
             </cfif>
            
		</td>
	</tr>
</table>
<HR width="700px">
<table>
	<tr>
		<td valign="top">
			<table border="0" cellspacing="0" cellpadding="3">
			<tr class="FontSmall">
			
				<td align="center" valign="too" nowrap>
					<!--- YYYY-MM-DD --->
				</td>
			
				               
                <cfif NOT IsDate(Start_dt)>
                
                    <td align="center" valign="too" nowrap>
                        <a href="javascript:show_calendar('MainForm.Start_dt');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date</a>
                        <br>
                        <input type="text" id="Start_dt" name="Start_dt" value="#Start_dt#" size="15" style="text-align: center;">
					</td>               
                	
               	<cfelse>
                
	                <td align="center" valign="too" nowrap class="FontBold">                         
                       From #dateformat(Start_dt, "mm-dd-yyyy")#                            
					</td>      
                    
	                <input type="hidden" id="Start_dt" name="Start_dt" value="#Start_dt#" size="15" style="text-align: center;">
                    
                </cfif> 	
                
                <cfif NOT IsDate(Stop_dt)>
                
                    <td align="center" valign="too" nowrap>
                        <a href="javascript:show_calendar('MainForm.Stop_dt');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">End Date</a>
                        <br>
                        <input type="text" id="Stop_dt" name="Stop_dt" value="#Stop_dt#" size="15" style="text-align: center;">
                    </td>	               
                	
               	<cfelse>
                
                    <td align="center" valign="too" nowrap class="FontBold">
                        To #dateformat(Stop_dt, "mm-dd-yyyy")#                          
					</td> 
                    
	                <input type="hidden" id="Stop_dt" name="Stop_dt" value="#Stop_dt#" size="15" style="text-align: center;">
                    
                </cfif> 	
                
                <cfif AtoInv EQ "">
                
                    <td align="center" valign="too" nowrap class="textlink">
                        Invoice Number
                        <br>
                        <input type="text" id="AtoInv" name="AtoInv" value="#AtoInv#" size="15" style="text-align: center;">
                    </td>	               
                	
               	<cfelse>
                
	                <input type="hidden" id="AtoInv" name="AtoInv" value="#AtoInv#" size="15" style="text-align: center;">
                    
                </cfif> 	
                
                
                 <cfif AtoInv_dt EQ "">
                
                    <td align="center" valign="too" nowrap>
                        <a href="javascript:show_calendar('MainForm.AtoInv_dt');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Invoice Date</a>
                        <br>
                        <input type="text" id="AtoInv_dt" name="AtoInv_dt" value="#AtoInv_dt#" size="15" style="text-align: center;">
                    </td>	               
                	
               	<cfelse>
                                                    
	                <input type="hidden" id="AtoInv_dt" name="AtoInv_dt" value="#AtoInv_dt#" size="15" style="text-align: center;">
                    
                </cfif> 	
                
                
                
				
			<!--- 	
				<td align="center" valign="bottom" nowrap>
					<a href="javascript:show_calendar('MainForm.Start2_dt');" class="textlink" onClick="sceenLocationY = event.screenY + this.offsetHeight / 2 + 1; sceenLocationX = event.screenX;">Begin Date<BR>2 Mo Rolling Avg</a>
					<br>
					<input type="text" id="Start2_dt" name="Start2_dt" value="#Start2_dt#" size="15" style="text-align: center;">
				</td>
			 --->
															
			<!--- 	<td align="center" valign="bottom" nowrap>&nbsp;<input type="submit" value="Refresh" class="ButtonStyle">&nbsp;&nbsp; --->
					
                    
                    <td width="25px">&nbsp;
	                    
                    </td>
                    							
				<td align="center" valign="top" nowrap rowspan="5">					
					<table border="0" cellspacing="0" cellpadding="0">						
						<tr>
						
							<th align="left" class="FontBold" nowrap width="80px" valign="top">
								BILL TO:
							</th>
							
							<td align="left" valign="top">                              
                                JPMorgan Chase Bank, National Association<BR>
                                McCoy Center, OH1-1280<BR>
                                1111 Polaris Parkway, Floor 4N<BR>
                                Columbus, OH  43240<BR><BR>
   								Attn: Lisa Cecil - Contract ID No. CW167907							
							</td>
						<tr>						
					</table>				
				</td>
			</tr>
		
		</table>	
		</td>
	</tr>
	
</table>
</form>
<!--- End little form thing --->

	
</div>

<BR>
<cfflush>

<div style="position:absolute; top:230px; left:25px;">
	
<!--- Over 5,000,000 --->
<!--- 
<cfset SixSecRate = .0048>
<cfset FirstThirtyRate = .024>
 --->

<!--- Under 5,000,000 --->
<cfset SixSecRate = .0052>
<cfset FirstThirtyRate = .026> 

<cfset RunningTotalA = 0>
<cfset RunningTotalB = 0>
<cfset RunningTotalC = 0>

	
<cfif IsDate(Start_dt) AND IsDate(Stop_dt)>			

<!--- Build Stage Tables --->


<cfset StageDateString = "#LSDateFormat(Now(), 'yyyymmdd')#_#LSTimeFormat(Now(), 'HHmmss')#">
<cfset StageDateString = "MANUAL">

<!--- Warn if stage tables still exist ??? --->

	<cfset SourceTableName = "#BatchIDA#">
    <cfset StageTableName = "#SourceTableName#_STAGE_#StageDateString#">
 <!---    <cfinclude template="StageChaseAlertResults.cfm"> --->
		
	<cfquery name="BatchInfoA" datasource="#AppDBSwitchMainDB#">
		<!---SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDA#--->
        SELECT 
            Desc_vch
        FROM 
            `simpleobjects`.`batch`
        WHERE 
            BatchId_bi=#BatchIDA#
	</cfquery>
	
	<cfquery name="MessageTime1st30SecInc" datasource="#AppDBSwitchMainDB#">
		<!---SELECT 
		  COUNT(*) AS MessageTime1st30SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 			
		WHERE			
			rxcd1.RXCDLStartTime_dt = 
			(
				SELECT 
					MAX(rxcd2.RXCDLStartTime_dt) 
				FROM 
					RXBatchResults.dbo.RXCallDetails_#StageTableName# rxcd2 (NOLOCK)
				WHERE 
					rxcd2.DialString_vch = rxcd1.DialString_vch
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
			)
		AND
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)--->
        SELECT 
		  	COUNT(*) AS MessageTime1st30SecInc
		FROM 
        	`simplexresults`.`callresults` rxcd1
		WHERE			
        	BatchId_bi=#BatchIDA#
          	AND RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>

<!--- 	
	<cfquery name="MessageTime6SecOver30Inc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int - 30 +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecOver30Inc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND rxcd1.CallResult_int IN (3,4,5)
			AND TotalConnectTime_int > 30
	</cfquery>
 --->

	<cfquery name="AVGConnectTimeA" datasource="#AppDBSwitchMainDB#">	
		<!---SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0	--->	
        SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			`simplexresults`.`callresults` rxcd1
		WHERE 
        	BatchId_bi=#BatchIDA#
            AND RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0		
	</cfquery>
	
	<cfquery name="AVGConnectTime" datasource="#AppDBSwitchMainDB#">	
		<!---SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start2_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0--->		
        SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			`simplexresults`.`callresults` rxcd1
		WHERE 
        	BatchId_bi=#BatchIDA#
            AND	RXCDLStartTime_dt BETWEEN '#Start2_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0     	
	</cfquery>
			

<BR>
<BR>
<BR>

<table border="1" cellpadding="5" cellspacing="0"  width="850px">

	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoA.Desc_vch#</td>
	</tr>	
	
	<tr>
		<td colspan="2" align="center" nowrap="nowrap">		
			<!--- Non-Security Alerts --->
			<cfset inpBatchId = StageTableName>
			<cfinclude template="dsp_FinalCallResultsB.cfm">		
		</td>	
	<tr>
		
	<tr>
	
		<td align="center" width="50%">
		
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
				
				<tr>
					<th colspan="3" align="center" class="TDSubHeaderStyle" nowrap>From #Start_dt# To #Stop_dt#<BR>Current Avg Message Length <i>#AVGConnectTimeA.AVGConnectTime#</i></th> 				 						
				</tr>
								
				<tr>
					<th align="right" class="FontBold" nowrap>Per Message (Up to 30 Secs) Count</th> 
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
				<cfset LocalTotal = 0>
						
				<tr>
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(FirstThirtyRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalB = RunningTotalB + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				<cfset LocalTotal = LocalTotal + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				
			</table>
		
		</td>
				
		<td align="center" width="50%">
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	
				<tr>
					<th colspan="4" align="center" class="TDSubHeaderStyle" nowrap>From #Start2_dt# To #Stop_dt#<BR>Rolling Avg Message Length <i>#AVGConnectTime.AVGConnectTime#</i></th> 						
				</tr>
				
				<tr>
					<th align="right" class="FontBold" nowrap>Avg Overage Inc</th> 
					<th align="left" class="FontBold" nowrap>Messages</th>	
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
						
				<tr>
					
					<cfif AVGConnectTime.AVGConnectTime GT 30>					
						<cfset CurrOverageInc = Numberformat( INT( (#AVGConnectTime.AVGConnectTime# - 30) / 6 ) + 1, "0")>																	
					<cfelse>					
						<cfset CurrOverageInc = 0>
					</cfif>
										
					<td align="right" class="FontSmall" nowrap>#CurrOverageInc#</td>					
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalC = RunningTotalC + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				<cfset LocalTotal = LocalTotal +Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				
			</table>	
		</td>
	
	</tr>

	
		
</table>	

<BR />
<cfflush>

<!---
	<cfset SourceTableName = "#BatchIDB#">
    <cfset StageTableName = "#SourceTableName#_STAGE_#StageDateString#">
    <!--- <cfinclude template="StageChaseAlertResults.cfm"> --->


	<cfquery name="BatchInfoB" datasource="#AppDBSwitchMainDB#">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDB#
	</cfquery>
	
<cfquery name="MessageTime1st30SecInc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  COUNT(*) AS MessageTime1st30SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 			
		WHERE			
			rxcd1.RXCDLStartTime_dt = 
			(
				SELECT 
					MAX(rxcd2.RXCDLStartTime_dt) 
				FROM 
					RXBatchResults.dbo.RXCallDetails_#StageTableName# rxcd2 (NOLOCK)
				WHERE 
					rxcd2.DialString_vch = rxcd1.DialString_vch
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
			)
		AND
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>

	<!--- 
	<cfquery name="MessageTime6SecOver30Inc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int - 30 +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecOver30Inc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND rxcd1.CallResult_int IN (3,4,5)
			AND TotalConnectTime_int > 30
	</cfquery> 
	--->

	<cfquery name="AVGConnectTimeA" datasource="#AppDBSwitchMainDB#">	
		SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0			
	</cfquery>
	

	<cfquery name="AVGConnectTime" datasource="#AppDBSwitchMainDB#">	
		SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start2_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0			
	</cfquery>

<BR />

<table border="1" cellpadding="5" cellspacing="0"  width="850px">

	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoB.Desc_vch#</td>
	</tr>	
	
	<tr>
		<td colspan="2" align="center" nowrap="nowrap">		
			<!--- Non-Security Alerts --->
			<cfset inpBatchId = StageTableName>
			<cfinclude template="dsp_FinalCallResultsB.cfm">		
		</td>	
	<tr>
				
	<tr>
	
		<td align="center" width="50%">
		
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
				
				<tr>
					<th colspan="3" align="center" class="TDSubHeaderStyle" nowrap>From #Start_dt# To #Stop_dt#<BR>Current Avg Message Length <i>#AVGConnectTimeA.AVGConnectTime#</i></th> 				 						
				</tr>
								
				<tr>
					<th align="right" class="FontBold" nowrap>Per Message (Up to 30 Secs) Count</th> 
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
				<cfset LocalTotal = 0>
						
				<tr>
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(FirstThirtyRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalB = RunningTotalB + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				<cfset LocalTotal = LocalTotal + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				
			</table>
		
		</td>
				
		<td align="center" width="50%">
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	
				<tr>
					<th colspan="4" align="center" class="TDSubHeaderStyle" nowrap>From #Start2_dt# To #Stop_dt#<BR>Rolling Avg Message Length <i>#AVGConnectTime.AVGConnectTime#</i></th> 						
				</tr>
				
				<tr>
					<th align="right" class="FontBold" nowrap>Avg Overage Inc</th> 
					<th align="left" class="FontBold" nowrap>Messages</th>	
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
						
				<tr>
					
					<cfif AVGConnectTime.AVGConnectTime GT 30>					
						<cfset CurrOverageInc = Numberformat( INT( (#AVGConnectTime.AVGConnectTime# - 30) / 6 ) + 1, "0")>																	
					<cfelse>					
						<cfset CurrOverageInc = 0>
					</cfif>
										
					<td align="right" class="FontSmall" nowrap>#CurrOverageInc#</td>					
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalC = RunningTotalC + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				<cfset LocalTotal = LocalTotal +Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				
			</table>	
		</td>
	
	</tr>

	
		
</table>	

<BR />
<cfflush>

	<cfset SourceTableName = "#BatchIDC#">
    <cfset StageTableName = "#SourceTableName#_STAGE_#StageDateString#">
   <!---  <cfinclude template="StageChaseAlertResults.cfm"> --->

	<cfquery name="BatchInfoC" datasource="#AppDBSwitchMainDB#">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDC#
	</cfquery>
	
	
	<cfquery name="MessageTime1st30SecInc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  COUNT(*) AS MessageTime1st30SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 			
		WHERE			
			rxcd1.RXCDLStartTime_dt = 
			(
				SELECT 
					MAX(rxcd2.RXCDLStartTime_dt) 
				FROM 
					RXBatchResults.dbo.RXCallDetails_#StageTableName# rxcd2 (NOLOCK)
				WHERE 
					rxcd2.DialString_vch = rxcd1.DialString_vch
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.FileSeqNumber_int = rxcd1.FileSeqNumber_int
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
					AND rxcd2.UserSpecifiedData_vch = rxcd1.UserSpecifiedData_vch
			)
		AND
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>
	
	<!--- 
	<cfquery name="MessageTime6SecOver30Inc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int - 30 +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecOver30Inc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND rxcd1.CallResult_int IN (3,4,5)
			AND TotalConnectTime_int > 30
	</cfquery>
	 --->

	<cfquery name="AVGConnectTimeA" datasource="#AppDBSwitchMainDB#">	
		SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0			
	</cfquery>
		
	<cfquery name="AVGConnectTime" datasource="#AppDBSwitchMainDB#">	
		SELECT 
			AVG(TotalConnectTime_int) AS AVGConnectTime
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK)
		WHERE 
			RXCDLStartTime_dt BETWEEN '#Start2_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
			AND	CallResult_int IN (3,4,5)
			AND	TotalConnectTime_int > 0			
	</cfquery>

	<cfquery name="SpanishOptionCount" datasource="#AppDBSwitchMainDB#">
        SELECT 
            COUNT(*) AS TotalCount				
        FROM 
            RXBatchResults..RXCallDetails_#StageTableName# (NOLOCK)
        WHERE
            RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
            AND XMLResultStr_vch LIKE '%<Q ID=''1''>2</Q>%'		
	</cfquery>	
    
    <cfquery name="RepeatOptionCount" datasource="#AppDBSwitchMainDB#">
        SELECT 
            COUNT(*) AS TotalCount				
        FROM 
            RXBatchResults..RXCallDetails_#StageTableName# (NOLOCK)
        WHERE
            RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'
            AND ReplayTotalCallTime_int > 0		
	</cfquery>	
    
    
<BR />

<table border="1" cellpadding="5" cellspacing="0"  width="850px" style="page-break-before:always;">

	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoC.Desc_vch#</td>
	</tr>	
	
	<tr>
		<td colspan="2" align="center" nowrap="nowrap">		
			<!--- Non-Security Alerts --->
			<cfset inpBatchId = StageTableName>
			<cfinclude template="dsp_FinalCallResultsB.cfm">		
		</td>	
	<tr>
        		
	<tr>
	
		<td align="center" width="50%">
		
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
					
				<tr>
					<th colspan="3" align="center" class="TDSubHeaderStyle" nowrap>From #Start_dt# To #Stop_dt#<BR>Current Avg Message Length <i>#AVGConnectTimeA.AVGConnectTime#</i></th> 				 						
				</tr>
								
				<tr>
					<th align="right" class="FontBold" nowrap>Per Message (Up to 30 Secs) Count</th> 
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
				<cfset LocalTotal = 0>
						
				<tr>
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(FirstThirtyRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalB = RunningTotalB + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				<cfset LocalTotal = LocalTotal + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * FirstThirtyRate, "0.00")>
				
			</table>
          		
		</td>
				
		<td align="center" width="50%">
			<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	
				<tr>
					<th colspan="4" align="center" class="TDSubHeaderStyle" nowrap>From #Start2_dt# To #Stop_dt#<BR>Rolling Avg Message Length <i>#AVGConnectTime.AVGConnectTime#</i></th> 						
				</tr>
				
				<tr>
					<th align="right" class="FontBold" nowrap>Avg Overage Inc</th> 
					<th align="left" class="FontBold" nowrap>Messages</th>	
					<th align="left" class="FontBold" nowrap>Rate</th>	
					<th align="left" class="FontBold" nowrap>Total</th>		
				</tr>
				
						
				<tr>
					
					<cfif AVGConnectTime.AVGConnectTime GT 30>					
						<cfset CurrOverageInc = Numberformat( INT( (#AVGConnectTime.AVGConnectTime# - 30) / 6 ) + 1, "0")>																	
					<cfelse>					
						<cfset CurrOverageInc = 0>
					</cfif>
										
					<td align="right" class="FontSmall" nowrap>#CurrOverageInc#</td>					
					<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc, ",")#</td>
					<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
					<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, ",0.00")#</td>				
				</tr>
				
				<cfset RunningTotalC = RunningTotalC + Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				<cfset LocalTotal = LocalTotal +Numberformat(MessageTime1st30SecInc.MessageTime1st30SecInc * CurrOverageInc * SixSecRate, "0.00")>
				
			</table>	
		</td>
	</tr>
		
</table>	

<BR />
<BR />
<BR />
<BR />
<BR />
<cfflush>



	<table border="1" cellpadding="5" cellspacing="0"  width="850px">    
    
    	<tr>
			<td colspan="10" align="center" nowrap class="TDHeaderStyle">Additional Detail Information</td>
		</tr>	
    
		<tr>
        
            <td colspan="2" align="center" nowrap="nowrap">
                <table border="1" cellpadding="5" cellspacing="0"  width="400px">
                    <tr>
                        <th colspan="3" align="center" class="TDSubHeaderStyle" nowrap>MFA Option Selection Count</th> 				 						
                    </tr>
                                   
                    <tr> 
                        <td width="50%" align="left" nowrap class="FontBold">Repeat</td><td align="right" width="25%" nowrap class="FontSmall">#Numberformat(RepeatOptionCount.TotalCount, ",")#</td><td align="right" width="25%" nowrap class="FontSmall"><cfif RepeatOptionCount.TotalCount 	GT 0>#Round((RepeatOptionCount.TotalCount  / (SummaryLive+SummaryMachine) ) * 100)#%<cfelse>0&nbsp;</cfif></td>
                    </tr>
                    
                    <tr> 
                        <td width="50%" align="left" nowrap class="FontBold">Spanish</td><td align="right" width="25%" nowrap class="FontSmall">#Numberformat(SpanishOptionCount.TotalCount, ",")#</td><td align="right" width="25%" nowrap class="FontSmall"><cfif SpanishOptionCount.TotalCount 	GT 0>#Round((SpanishOptionCount.TotalCount  / (SummaryLive+SummaryMachine)) * 100)#%<cfelse>0&nbsp;</cfif></td>
                    </tr>
                    
                     <tr> 
                        <td width="50%" align="left" nowrap class="FontBold">English Only</td><td align="right" width="25%" nowrap class="FontSmall">#Numberformat((SummaryLive+SummaryMachine)-SpanishOptionCount.TotalCount, ",")#</td><td align="right" width="25%" nowrap class="FontSmall"><cfif (SummaryLive+SummaryMachine) - SpanishOptionCount.TotalCount 	GT 0>#Round((  ((SummaryLive+SummaryMachine)-SpanishOptionCount.TotalCount)  / (SummaryLive+SummaryMachine)) * 100)#%<cfelse>0&nbsp;</cfif></td>
                    </tr>
                    
                        
                </table>
                
            </td>
        </tr>
	</table>
    
    <BR>
	<BR>
	<BR>
    --->
		<table border="1" cellpadding="5" cellspacing="0"  width="850px">
		
			<tr>
				<td colspan="2" align="center" nowrap class="TDHeaderStyle">Amount(s) Due</td>
			</tr>			
			
			<tr>
				<td align="center" nowrap class="FontBold">Message Delivered Total</td>				
				<td align="left" class="FontBold">$#Numberformat(RunningTotalB, ",0.00")#</td>	
			</tr>		
		
			<tr>
				<td align="center" nowrap class="FontBold">Overage Charges Total</td>
				<td align="left" class="FontBold" width="250px">$#Numberformat(RunningTotalC, ",0.00")#</td>	
			<tr>

			<tr>
				<td align="right" class="FontBold">Total</td>
				<td align="left" class="FontBold" >$#Numberformat(RunningTotalB + RunningTotalC, ",0.00")#</td>	
			<tr>
										
		</table>
	
	
	<BR>
	<BR>
	<BR>
            
		<table border="0" cellpadding="5" cellspacing="0"  width="800px">			
			<tr>
				<td colspan="10" align="center" class="FontBold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>		
		</table>
	
	
</cfif>

<br><br>






</div>



<!--- Drop Stage Tables --->


</cfoutput>





</body>
</html>










<!--- 


	<cfquery name="BatchInfoA" datasource="#AppDBSwitchMainDB#">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDA#
	</cfquery>
	
	<cfquery name="MessageTime6SecInc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>
	

Average Connect Time = #AVGConnectTime.AVGConnectTime#
<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoA.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>6 Sec Inc Count</th> 
		<th align="left" class="FontBold" nowrap>Rate</th>	
		<th align="left" class="FontBold" nowrap>Total</th>		
	</tr>
	
			
	<tr>
		<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime6SecInc.MessageTime6SecInc, ",")#</td>
		<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
		<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>				
	</tr>
	
	<cfset RunningTotalA = RunningTotalA + Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")>
	
	<tr>
		<td  align="right" class="FontBold" colspan="2">Total</td>
		<td  align="left" class="FontBold">$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>	
	<tr>
		
</table>	

<BR />
OR
<BR />
<BR />




<cfquery name="BatchInfoB" datasource="#AppDBSwitchMainDB#">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDB#
	</cfquery>
	
	<cfquery name="MessageTime6SecInc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>


<BR>
Average Connect Time = #AVGConnectTime.AVGConnectTime#
	
<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoB.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>6 Sec Inc Count</th> 
		<th align="left" class="FontBold" nowrap>Rate</th>	
		<th align="left" class="FontBold" nowrap>Total</th>		
	</tr>
	
			
	<tr>
		<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime6SecInc.MessageTime6SecInc, ",")#</td>
		<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
		<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>				
	</tr>
	
	<cfset RunningTotalA = RunningTotalA + Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")>
	
	<tr>
		<td  align="right" class="FontBold" colspan="2">Total</td>
		<td  align="left" class="FontBold">$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>	
	<tr>
		
</table>	

<BR />
OR
<BR />


<cfquery name="BatchInfoC" datasource="#AppDBSwitchMainDB#">
		SELECT 
			Desc_vch
		FROM
			RX..RX_Batch (NOLOCK)
		WHERE 
			BatchId_num = #BatchIDC#
	</cfquery>
	
	<cfquery name="MessageTime6SecInc" datasource="#AppDBSwitchMainDB#">
		SELECT 
		  SUM(TotalConnectTime_int +
		  CASE TotalConnectTime_int%6
			  WHEN 0 THEN 0
			  WHEN 1 THEN 5
			  WHEN 2 THEN 4
			  WHEN 3 THEN 3
			  WHEN 4 THEN 2
			  WHEN 5 THEN 1      
			  ELSE 0
		  END) / 6 AS MessageTime6SecInc
		FROM
			RXBatchResults..RXCallDetails_#StageTableName# rxcd1 (NOLOCK) 
		WHERE
			RXCDLStartTime_dt BETWEEN '#Start_dt# #Start_time#' AND '#Stop_dt# #Stop_time#'	
			AND rxcd1.CallResult_int IN (3,4,5)
	</cfquery>
	
	
	
	
<BR>
Average Connect Time = #AVGConnectTime.AVGConnectTime#
<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">#BatchInfoC.Desc_vch#</td>
	</tr>		
	
	<tr>
		<th align="right" class="FontBold" nowrap>6 Sec Inc Count</th> 
		<th align="left" class="FontBold" nowrap>Rate</th>	
		<th align="left" class="FontBold" nowrap>Total</th>		
	</tr>
	
			
	<tr>
		<td align="right" class="FontSmall" nowrap>#Numberformat(MessageTime6SecInc.MessageTime6SecInc, ",")#</td>
		<td align="left" class="FontSmall" nowrap>$#Numberformat(SixSecRate, "0.0000")#</td>
		<td align="left" class="FontBold" nowrap>$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>				
	</tr>
	
	<cfset RunningTotalA = RunningTotalA + Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")>
	
	<tr>
		<td  align="right" class="FontBold" colspan="2">Total</td>
		<td  align="left" class="FontBold">$#Numberformat(MessageTime6SecInc.MessageTime6SecInc * SixSecRate, "0.00")#</td>	
	<tr>
		
</table>	

<BR />
OR
<BR />




<table border="1" cellpadding="5" cellspacing="0"  width="400px">
	<tr>
		<td colspan="10" align="center" nowrap class="TDHeaderStyle">Program Total - 6 Sec Inc Count</td>
	</tr>		

	<tr>
		<td  align="right" class="FontBold" colspan="2">Total</td>
		<td  align="left" class="FontBold">$#Numberformat(RunningTotalA, "0.00")#</td>	
	<tr>
		
</table>	

<BR />
OR
<BR /> 



--->

<!---<cfset AppDBSwitchMainDB = "MBASPSQL2K">--->
<!--- <cfset AppDBSwitchMainDB = "ChaseBUDB"> --->



