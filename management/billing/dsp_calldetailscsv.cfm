<cfsetting showdebugoutput="no">

<cfparam name="Start_dt" default="">
<cfparam name="Stop_dt" default="">

<cfparam name="Start_time" default="00:00:00">
<cfparam name="Stop_time" default="23:59:59">

<cfparam name="TransID" default="">
<cfparam name="BatchIDA" default="48842"> <!--- Alerts --->
<cfparam name="BatchIDB" default="48841"> <!--- Security --->
<cfparam name="BatchIDC" default="48840"> <!--- MFA --->
<cfparam name="DialString_vch" default="0000000000">
<cfparam name="ShowAllCampaignTypes" default="All"><!--- to filter on/off the Public Web Site campaigns --->
<cfparam name="ThisBatchGroup" default="">
<cfparam name="ThisBatchGroup_ALL" default="">

<cfset currusertotaldials = 0> 
<cfset pagetotaldials = 0>


<cfif IsDate(Start_dt) AND IsDate(Stop_dt)>			
	
	<cfquery name="GetCallList" datasource="#AppDBSwitchMainDB#" blockfactor="1" >
			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDA# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			     RXCDLStartTime_dt > '#Start_dt# #Start_time#'
			  AND RXCDLStartTime_dt < '#Stop_dt# #Stop_time#'			
   			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION

			SELECT 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch						
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDB# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num	
			WHERE
			    RXCDLStartTime_dt > '#Start_dt# #Start_time#'
			  AND RXCDLStartTime_dt < '#Stop_dt# #Stop_time#'			
			  			<!--- ORDER BY
			    RXCallDetailId_bi ASC			  			  			
		 --->
		
		
		UNION
		
		SELECT 			 
				RXCallDetailId_bi,							  
				RXCDLStartTime_dt,
				DialString_vch,
				TotalConnectTime_int,
				ax.BatchId_bi,
				bx.Desc_vch,
				CASE CallResult_int
					WHEN 3 THEN 'LIVE'
					WHEN 5 THEN 'LIVE'
					WHEN 4 THEN 'MACHINE'
					WHEN 7 THEN 'BUSY'
					WHEN 9 THEN 'FAX' 
					WHEN 10 THEN 'NO ANSWER'
					WHEN 48 THEN 'NO ANSWER'
					ELSE 'OTHER'		
				END AS CallResult_int,
				RedialNumber_int,
				TimeZone_ti,
				DialerName_vch,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangupDetectedTS_dt,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
				XMLResultStr_vch							
			FROM 
				RXBatchResults..RXCallDetails_#BatchIDC# ax (NOLOCK)		
				left outer join RX..RX_Batch bx (NOLOCK) on ax.BatchId_bi = bx.BatchId_num		
			WHERE
			    RXCDLStartTime_dt > '#Start_dt# #Start_time#'
			  AND RXCDLStartTime_dt < '#Stop_dt# #Stop_time#'			
			  
		</cfquery>
	
	
		Date, 
		Dial String,
		Connect Time,
		Batch Name ,
		Call Result,
		Dial Attempt,
		File Seq Num,
		User Spec Data,
		Alert ID<BR>

		Do nada!
	<!--- 
	<cfoutput query="GetCallList">
		#GetCallList.RXCDLStartTime_dt#,
		#GetCallList.DialString_vch#,
		#GetCallList.TotalConnectTime_int#,
		#GetCallList.Desc_vch#,
		#GetCallList.CallResult_int#,
		#GetCallList.RedialNumber_int#,
		#GetCallList.FileSeqNumber_int#,
		#GetCallList.UserSpecifiedData_vch#,
		#GetCallList.XMLResultStr_vch#<BR />
		<cfflush interval="1000">
	</cfoutput> --->
	

</cfif>

