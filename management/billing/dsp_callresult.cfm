<cfparam name="BatchId_int" default="0">
<h4>Call result by: <cfoutput>#BatchId_int#</cfoutput></h4>
<div class="" >
<table id="callRresult"></table>
</div>
<script>
	$(function() {
		$("#callRresult").jqGrid({        
			url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=getCallResult&INPBATCHID=<cfoutput>#BatchId_int#</cfoutput>&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
			datatype: "json",
			height: 385,
			colNames:['Id','BatchId_bi','TotalCallTime','MessageDelivered', 'CallStartTime','CallEndTime'],
			colModel:[			
				{name:'MasterRXCallDetailId_int',index:'MasterRXCallDetailId_int', width:30, editable:false, sortable:false, resizable:false,align:'center' },
				{name:'BatchId_bi',index:'BatchId_bi', width:100, editable:false, resizable:false,align:'center'},
				{name:'TotalCallTime_int',index:'TotalCallTime_int', width:120, editable:true, resizable:false},
				{name:'MessageDelivered_si',index:'MessageDelivered_si', width:110, editable:false, resizable:false,align:'center'},
				{name:'CallStartTime',index:'CallStartTime', width:150, editable:false, resizable:false,align:'center'},
				{name:'CallEndTime',index:'CallEndTime', width:150, editable:false, resizable:false,align:'center'}
			],
			rowNum:15,
		   	rowList:[20,4,8,10,20,30],
			mtype: "POST",
			pager: $('#pagerDiv'),
		//	pagerpos: "right",
		//	cellEdit: false,
			toppager: true,
	    	emptyrecords: "No Current Call Result Found.",
	    	pgbuttons: true,
			altRows: true,
			altclass: "ui-priority-altRow",
			pgtext: false,
			pginput:true,
			sortname: 'BatchId_bi',
			toolbar:[false,"top"],
			viewrecords: true,
			sortorder: "DESC",
			caption: "",	
			multiselect: false,

			onSelectRow: function(BatchId_bi){

				},
			 loadComplete: function(data){ 		
		 		<!--- $(".view_ReportBilling").click( function() {
		 			viewcallresult($(this).attr('rel'));
		 		}); --->
				}, 

		
			<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
	
			  jsonReader: {
				  root: "ROWS", //our data
				  page: "PAGE", //current page
				  total: "TOTAL", //total pages
				  records:"RECORDS", //total records
				  cell: "", //not used
				  id: "0" //will default second column as ID
				  }	
		});
	});
</script>