<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<title>Billing</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
			   $('ul.sf-menu').superfish({
				  dropShadows: false, 
				  autoArrows: false
			   });
			});
		</script>
        
	</head>
	<body>
    <div id="centeredmenu">
		<ul class="sf-menu">
        	<li>
				<a href="#">Report</a>
                <ul>
					<li>
						<a href="dsp_BillingSV.cfm?BatchIDA=356">Invoice</a>
					</li>
				</ul>
			</li>	
		</ul>
    </div>
	</body>
</html>