<cfcomponent output="false">
<cfparam name="Session.accessRights" default="#StructNew()#"/> 
<cfparam name="Session.userRole" default="User"/>
<!---

CREATE TABLE `useraccount` (
  `UserId_int` int(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` varchar(255) NOT NULL DEFAULT '',
  `Password_vch` blob NOT NULL,
  `AltUserName_vch` varchar(255) DEFAULT NULL,
  `AltPassword_vch` varchar(255) DEFAULT NULL,
  `UserLevel_int` int(11) NOT NULL DEFAULT '0',
  `DESC_VCH` varchar(255) DEFAULT NULL,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `CompanyName_vch` varchar(255) DEFAULT NULL,
  `HomePhoneId_int` int(11) DEFAULT NULL,
  `WorkPhoneId_int` int(11) DEFAULT NULL,
  `AlternatePhoneId_int` int(11) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Reactivate_int` tinyint(4) DEFAULT '0',
  `Manager_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1$$


--->


	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 

    <cffunction name="validateUsers" access="remote" returnformat="JSON" output="false" hint="Validate a specified users's name and password match.">
        <cfargument name="inpUserID" TYPE="string"/>
        <cfargument name="inpPassword" TYPE="string"/>
        
        <cfset var dataout = '0' /> 
	    <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
    	<cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "REASONMSG", "") />
    
       <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = CF Failure
        -3 = 
    
	     --->
             
        
        <!--- Ensure that attempts to authenticate start with new credentials. --->
        <cflogout/>
            
        <cftry>
        	
            <!--- Check for too many tries this session and lockout for 5 min --->
			<cfset inpUserID =  TRIM(inpUserID)>
            <cfset inpPassword = TRIM(inpPassword)>
			
			<!--- Server Level Input Protection      component="PremierIVR.model.Validation.validation" --->
        	<cfinvoke 
             component="validation"
             method="VldInput"
             returnvariable="safeName">
                <cfinvokeargument name="Input" value="#inpUserID#"/>
            </cfinvoke>
            
            <cfinvoke 
             component="validation"
             method="VldInput"
             returnvariable="safePass">
                <cfinvokeargument name="Input" value="#inpPassword#"/>
            </cfinvoke> 
			
            <cfif safeName eq true and safePass eq true>
				<!--- No reference to current session variables - exisits in its on session/application scope --->
                <CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
                    SELECT 
                        Password_vch,
                        UserId_int,
                        PrimaryPhoneStr_vch,
                        Manager_int,
                        EmailAddressVerified_vch,
                        EmailAddress_vch
                    FROM 
                        simpleobjects.useraccount
                    WHERE 
                        UserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserID#">
                        AND Password_vch = AES_ENCRYPT('#inpPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString')				                   
                </CFQUERY>
                 
                <cflogin>
                    <cfif getUser.recordcount gt 0>
                    	<cfset args=structNew()>
						<cfset isManager=false>
						<cfset args.inpUserId=#getUser.UserId_int#>
						<cfinvoke method="validateUserRole" argumentcollection="#args#" returnvariable="isManager">
                    	<cfif getUser.Manager_int GT 0 or isManager> 
                    
                            <cfloginuser name="#arguments.inpUserID#"
                                         password="#arguments.inpPassword#" 
                                         roles="user"/>
                                         
                            <cfset validated = true/>
                            <cfset Session.USERID = getUser.UserId_int>
                            <cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch>
							<cfset Session.EmailAddress = getUser.EmailAddress_vch>
                            <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "REASONMSG", "Login Success!") />
							<cfif len(#Session.USERID#) GT 0>
										<cfset args=StructNew()>
									<cfset args.inpUserId="#Session.USERID#">
									<cfinvoke argumentcollection="#args#" method="getRights" component="devjlp.EBM_DEV.management.cfc.permission" 
									returnvariable="Session.accessRights">
									</cfinvoke>	
							</cfif>                            
                        <cfelse>
                        
							<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                            <cfset QuerySetCell(dataout, "REASONMSG", "User name not authorized as a manager") />
                        
                        </cfif>


        			<cfelse>
	                    <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                        <cfset QuerySetCell(dataout, "REASONMSG", "User name password combination invalid") />
                    
                    </cfif>
                       
                </cflogin>
                
                <cfelse>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                    <cfset QuerySetCell(dataout, "REASONMSG", "Failed Server Level Validation") />
        
            </cfif>

	 	<cfcatch TYPE="any">
        
	        <cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE,  MESSAGE, ERRMESSAGE")>  
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
            <cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 
        </cfcatch>
        
		</cftry> 
        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="validateUserRole" access="remote" output="false" returntype="boolean" hint="check user role, return false if user have no right to access management page">
		<cfargument name="inpUserId">
		
		<cfset roleName="">
		<cfquery name = "getUserRole" datasource="#Session.DBSourceEBM#"> 
		    SELECT 
		    		r.rolename_vch
		    FROM 
		    		simpleobjects.userrole r
		    JOIN
		    		simpleobjects.userroleuseraccountref ref
		    ON
		    		r.roleId_int=ref.roleid_int
		    WHERE
		    		ref.userAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		<cfif isDefined("getUserRole.rolename_vch") >
			<cfset roleName=getUserRole.rolename_vch>
			<cfif roleName EQ "CompanyAdmin" OR roleName EQ "SuperUser">
				<cfset Session.userRole=roleName>
				<cfreturn true>
			</cfif>
		</cfif>
		
		<cfreturn false>
	</cffunction>
</cfcomponent>
