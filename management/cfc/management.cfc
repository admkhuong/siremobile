<cfcomponent>
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
		
	<cffunction name="myFunction" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
	<cffunction name="RetrieveCompanyAccount" access="remote" output="false" hint="Retrieve all company account data based on Active value">
		<cfargument name="inpActive" TYPE="integer" default="1"/>
		<cfset var companyAccountData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getCompanyAccounts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.companyaccount 
		    WHERE
		    		Active_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpActive#">
		</cfquery>
		
		<cfloop query="getCompanyAccounts">
			<cfset data=ArrayNew(1)>
			<cfset ArrayAppend(data,#getCompanyAccounts.CompanyAccountId_int#)>
			<cfset ArrayAppend(data,#getCompanyAccounts.CompanyName_vch#)>
			<cfset ArrayAppend(companyAccountData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("COMPANYDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "COMPANYDATA", #companyAccountData#) />
		<cfreturn dataout />
	</cffunction>	

	<cffunction name="RetrieveUserIdList" access="remote" output="false" hint="Retrieve all user account data based on Active value">
		<cfargument name="inpActive" TYPE="numeric" default=1/>
		<cfset var userAccountData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getUserAccounts" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.useraccount 
		    WHERE
		    		Active_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpActive#">
		</cfquery>

			<cfif Session.UserRole EQ "CompanyAdmin">
				<cfquery name= "getCompanyId" datasource="#Session.DBSourceEBM#">
					SELECT
							CompanyAccountId_int
					FROM
							simpleobjects.useraccount
					WHERE
							userId_int=	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
				</cfquery>
				<cfset inpCompanyId=getCompanyId.CompanyAccountId_int>
			</cfif>		
		<cfloop query="getUserAccounts">
			<cfif Session.userRole eq "SuperUser" or getUserAccounts.companyAccountId_int eq inpCompanyId>
				<cfset data=ArrayNew(1)>
				<cfset ArrayAppend(data,#getUserAccounts.UserId_int#)>
				<cfset ArrayAppend(data,#getUserAccounts.UserName_vch#)>
				<cfset ArrayAppend(userAccountData, data)>
			</cfif>
		</cfloop>
		<cfset dataout =  QueryNew("USERACCOUNTDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERACCOUNTDATA", #userAccountData#) />
		<cfreturn dataout />
	</cffunction>	

    <cffunction name="LinkUserToCompanyAccount" access="remote" output="false" hint="Link an user to company account">
       	<cfargument name="inpUserId" required="yes" default="">
        <cfargument name="inpCompanyId" required="yes" default="">
		<cfargument name="isAdminManager" required="no" default="0">

       	<cfoutput>
         
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpCompanyId, isAdminManager, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
            <cfset QuerySetCell(dataout, "inpCompanyId", "#inpCompanyId#") />   
			<cfset QuerySetCell(dataout, "isAdminManager", "#isAdminManager#") />    
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
		

            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->		
		        <cfquery name="LinkUserToCompanyQuery" datasource="#Session.DBSourceEBM#">
						UPDATE
								simpleobjects.useraccount
						SET
								companyaccountid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpCompanyId#">
						WHERE
								UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		        </cfquery>
		
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpCompanyId, isAdminManager, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpCompanyId", "#inpCompanyId#") /> 
		            <cfset QuerySetCell(dataout, "isAdminManager", "#isAdminManager#") />  
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />


					<cfelse>
	                    <cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpCompanyId, isAdminManager, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
			            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
			            <cfset QuerySetCell(dataout, "inpCompanyId", "#inpCompanyId#") /> 
			            <cfset QuerySetCell(dataout, "isAdminManager", "#isAdminManager#") />    
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 					
				</cfif>

			
				<cfcatch TYPE="any">
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpCompanyId, isAdminManager, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpCompanyId", "#inpCompanyId#") />  
		            <cfset QuerySetCell(dataout, "isAdminManager", "#isAdminManager#") />   
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 					
				</cfcatch>
			</cftry>
		 
       </cfoutput>	
		<cfreturn dataout />			
		
	</cffunction>	

	<cffunction name="getUserCompanyInfo" access="remote" output="false" hint="Get Info related to company account of specific user">
		<cfargument name="inpUserId">
		<cfquery name="getInfo" dataSource = "#Session.DBSourceEBM#">
			SELECT 
					CompanyAccountId_int, Manager_int 
			FROM 	
					simpleobjects.useraccount 
			WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		<cfset dataout =  QueryNew("COMPANYID,ISCOMPANYADMIN")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "ISCOMPANYADMIN", #getInfo.Manager_int#) />
		<cfset QuerySetCell(dataout, "COMPANYID", #getInfo.CompanyAccountId_int#) />
		<cfreturn dataout/>
	</cffunction>
	<cffunction name="getUserRoleInfo" access="remote" output="false" hint="Get Info related to role of specific user">
		<cfargument name="inpUserId">
		<cfquery name="getInfo" dataSource = "#Session.DBSourceEBM#">
			SELECT 
					roleId_int
			FROM 	
					simpleobjects.userroleuseraccountref 
			WHERE 
					userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>
		<cfset dataout =  QueryNew("USERROLE")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "USERROLE", #getInfo.roleId_int#) />
		<cfreturn dataout/>
	</cffunction>	
	
	<cffunction name="getTaskListItems" access="remote" output="false" hint="Get all task list Item">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="id">
        <cfargument name="sord" required="no" default="ASC">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
        <!--- get Total for pager --->
        <cfquery name="getTaskCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.tasklistitems
            WHERE                
                1=1
        </cfquery>

		<cfif getTaskCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getTaskCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="getTaskList" datasource="#Session.DBSourceEBM#">
			SELECT
				id,
             	itemName_vch,
                description
            FROM
                simpleobjects.tasklistitems
            WHERE                
                1=1
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getTaskCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="getTaskList" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">	
			<cfset taskId="#getTaskList.id#">
            <!---
				<cfset DisplayOptions = DisplayOptions & "<img class='del_Permission ListIconLinks delete_16x16' rel='#id#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">          
			--->
			<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' rel='#id#' name='#id#' value=''> ">          
            
            <cfset LOCALOUTPUT.rows[i] = [#getTaskList.id#, #getTaskList.itemName_vch#,#getTaskList.description#]>
			<cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />						
	</cffunction>
<!---
	<cffunction name="getTaskListOfUser" access="remote" output="false" hint="get all task list items were assigned to an userId/CompanyId">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">
		<cfset var taskListItemIdArray=ArrayNew(1)>
		<cfset var requiredArray=ArrayNew(1)>
		<cfset result=StructNew()>
		<cftry>
		<!------>
		<cfquery name = "getTaskListItemId" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		i.id,l.required_int
		    FROM 
		    		simpleobjects.tasklistitems i
		    JOIN
		    		simpleobjects.tasklist l
		    ON
		    		i.id=l.itemId_int
		    WHERE
		    		l.userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		</cfquery>		
		
		<cfloop query="getTaskListItemId">
			<cfset ArrayAppend(taskListItemIdArray,#getTaskListItemId.id#)>
			<cfset ArrayAppend(requiredArray,#getTaskListItemId.required_int#)>
		</cfloop>
		<!------>
	        <cfset dataout =  QueryNew("RXRESULTCODE, TASKITEMS,REQUIRED, TYPE, MESSAGE, ERRMESSAGE")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "TASKITEMS", "#taskListItemIdArray#") />     
	        <cfset QuerySetCell(dataout, "REQUIRED", "#requiredArray#") />     
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />		
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />           
			</cfcatch>
		</cftry>
		

		<cfreturn dataout />		
	</cffunction>	
	--->	
	<cffunction name="AssignTaskListToUser" access="remote" output="false" hint="Assign/update task list of an userId/companyId">
		<cfargument name="inpTaskList" TYPE="string" default=""/>
		<cfargument name="inpToRemoveItems" TYPE="string" default=""/>
		<cfargument name="inpRequiredValues" TYPE="string" default=""/>
		<cfargument name="inpNeedUpdateItems" TYPE="string" default=""/>
		<cfargument name="inpRequiredValuesUpdateItems" TYPE="string" default=""/>		
		<cfargument name="inpUserId" TYPE="numeric" default=0/>
		<cfset var dataout = '0' /> 
		
				<!--- Prevent user from updating permissions of level if NOT Admin of CompanyAccount --->
<!--- 				<cfset adminInCompany = false/>
				<cfset args=StructNew()/>
				<cfset args.inpUserId="#inpUserId#"/>				
				<cfinvoke method="isAdminWithinCompany" returnvariable="adminInCompany">
				</cfinvoke>
				<cfif not adminInCompany>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpLevelId,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpLevelId", "#inpLevelId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "You don't have right to modify user's permissions") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					<cfreturn dataout >
				</cfif>	 --->	
       	<cfoutput>
           

			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
       
            <cftry>
            	<cfif 1 EQ 1> 
				<cfset tmpTaskList = arrayNew(1)/>
				<cfset tmpTaskList=ListToArray(#inpTaskList#,"~")>				

				<cfset tempRequiredList=arrayNew(1)/>
				<cfset tempRequiredList=ListToArray(#inpRequiredValues#,"~")>				

				<cfset tmpInpNeedUpdateItems = arrayNew(1)/>
				<cfset tmpInpNeedUpdateItems=ListToArray(#inpNeedUpdateItems#,"~")>				

				<cfset tmpInpRequiredValuesUpdateItems = arrayNew(1)/>
				<cfset tmpInpRequiredValuesUpdateItems=ListToArray(#inpRequiredValuesUpdateItems#,"~")>												

  					<cftransaction>
						<!--- Add record --->
						<cfloop list="#inpTaskList#" index="itemId" delimiters ="~">
							<cfset requiredValue=tempRequiredList.elementAt(tmpTaskList.indexOf("#itemId#"))>
		                    <cfquery name="updateUserTaskList" datasource="#Session.DBSourceEBM#">
		                            INSERT INTO
		                             	simpleobjects.tasklist(userid_int,itemid_int,required_int,createddate)
		                            VALUES
		                            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#itemId#">,
		                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requiredValue#">,NOW())
		                    </cfquery>
						</cfloop>
						<cfloop list="#inpToRemoveItems#" index="removedItemId" delimiters ="~">
		                    <cfquery name="deleteUserTaskItem" datasource="#Session.DBSourceEBM#">
		                            DELETE FROM
		                             	simpleobjects.tasklist
		                            WHERE
		                            	userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		                            AND
		                            	itemId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#removedItemId#">                
		                    </cfquery>  						
						</cfloop>
						<cfloop list="#inpNeedUpdateItems#" index="UpdateItemId" delimiters ="~">
							<cfset requiredUpdateValue=tmpInpRequiredValuesUpdateItems.elementAt(tmpInpNeedUpdateItems.indexOf("#UpdateItemId#"))>
		                    <cfquery name="updateRequiredValue" datasource="#Session.DBSourceEBM#">
		                            UPDATE
		                             	simpleobjects.tasklist
		                             SET
		                             	required_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requiredUpdateValue#">
		                            WHERE
		                            	userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		                            AND
		                            	itemId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UpdateItemId#">                
		                    </cfquery>  						
						</cfloop>
					</cftransaction>  
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    <!--- Mail user their welcome aboard email --->			
                <cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                </cfif>          
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
				
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				<cfelse>
					<cfset cfcatch.errorcode = -3>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
       		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />			
	</cffunction>

	<cffunction name="getFieldList" access="remote" output="false" hint="get all columns name of a table">
		<cfargument name="inpTableName" TYPE="string" required="yes">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="column_name">
        <cfargument name="sord" required="no" default="ASC">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
        <!--- get Total for pager --->
        <cfquery name="getFieldCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                information_schema.columns
            WHERE                
                table_name=<CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#inpTableName#">
			AND
				column_name != <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="BATCHID_BI">				
        </cfquery>

		<cfif getFieldCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getFieldCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="getColumnList" datasource="#Session.DBSourceEBM#">
			SELECT
				UCASE(column_name) as column_name,column_comment,is_nullable
            FROM
                information_schema.columns
            WHERE
                table_name=<CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#inpTableName#">
			AND
				column_name != <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="BATCHID_BI">
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getFieldCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="getColumnList" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">	
			<cfset fieldId="#getColumnList.column_name#">
            <!---
				<cfset DisplayOptions = DisplayOptions & "<img class='del_Permission ListIconLinks delete_16x16' rel='#id#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">          
			--->
			<cfset DisplayOptions = DisplayOptions & "<input type='checkbox' rel='#column_name#' name='#column_name#' value=''> ">          
            
            <cfset LOCALOUTPUT.rows[i] = [#getColumnList.column_name#, #getColumnList.column_comment#]>
			<cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />		
	</cffunction>

	<cffunction name="getCollaborationTableList" access="remote" output="false" hint="get all table of collaboration tool (start with pm_)">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="table_name">
        <cfargument name="sord" required="no" default="ASC">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
        <!--- get Total for pager --->
		<cfset prefix='pm_'>
        <cfquery name="getTableCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                information_schema.tables
            WHERE                
                table_name like <CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#prefix#%">
        </cfquery>

		<cfif getTableCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getTableCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="getTableList" datasource="#Session.DBSourceEBM#">
			SELECT
				UCASE(table_name) as table_name,table_comment
            FROM
                information_schema.tables
            WHERE                
                table_name like <CFQUERYPARAM CFSQLTYPE="CF_SQL_CHARACTER" VALUE="#prefix#%">
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getTableCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>
			<cfinclude template="../../public/paths.cfm" >
			<cfscript>
				 //files = '\devjlp\EBM_DEV\management\collaboration.properties';
				 files = '\#ManagementPath#\collaboration.properties';
				 properties = createObject( 'component', '\#SessionPath#\cfc\PropertiesUtil' ).init( files );
				//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
			</cfscript>		
        <cfloop query="getTableList" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">	
			<cfset tableId="#getTableList.table_name#">
            <!---
				<cfset DisplayOptions = DisplayOptions & "<img class='del_Permission ListIconLinks delete_16x16' rel='#id#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">          
			--->
			<cfset DisplayOptions = DisplayOptions & "<img class='edit_CheckList ListIconLinks Magnify-Purple-icon' rel='#tableId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>">
			<cfset tableDesc=properties.getProperty("desc." & "#getTableList.table_name#")>
			
            <!--- <cfset LOCALOUTPUT.rows[i] = [#getTableList.table_name#, #getTableList.table_comment#,#DisplayOptions#]> --->
			<cfset LOCALOUTPUT.rows[i] = [#getTableList.table_name#, #tableDesc#,#DisplayOptions#]>
			<cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />			
	</cffunction>

	<cffunction name="getPrintableTable" access="remote" output="false" hint="get all table of collaboration tool (start with pm_)">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="TableName_vch">
		<cfargument name="inpUserId" required="no" default=0>
        <cfargument name="sord" required="no" default="ASC">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
        <!--- get Total for pager --->
		<cfset prefix='pm_'>

        <cfquery name="getTableCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(distinct tableName_vch) AS TOTALCOUNT
            FROM
                simpleobjects.tasklist
		<cfif inpUserId GT 0>
			WHERE UserId_int= #inpUserId#
		</cfif>				
        </cfquery>

		<cfif getTableCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getTableCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="getTableList" datasource="#Session.DBSourceEBM#">
			SELECT DISTINCT
				tableName_vch
            FROM
                simpleobjects.tasklist
			<cfif inpUserId GT 0>
				WHERE UserId_int= #inpUserId#
			</cfif>					
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
	        
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getTableCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>
			<cfinclude template="../../public/paths.cfm" >
			<cfscript>
				 //files = '\devjlp\EBM_DEV\management\collaboration.properties';
				 files = '\#ManagementPath#\collaboration.properties';
				 properties = createObject( 'component', '\#SessionPath#\cfc\PropertiesUtil' ).init( files );
				//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
			</cfscript>		

        <cfloop query="getTableList" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">	
			<cfset tableId="#getTableList.tableName_vch#">
            <!---
				<cfset DisplayOptions = DisplayOptions & "<img class='del_Permission ListIconLinks delete_16x16' rel='#id#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">          
			--->
			<cfset DisplayOptions = DisplayOptions & "<img class='edit_CheckList ListIconLinks Magnify-Purple-icon' rel='#tableId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>">
			<cfset tableDesc=properties.getProperty("desc." & "#getTableList.tableName_vch#")>
            <!--- <cfset LOCALOUTPUT.rows[i] = [#getTableList.table_name#, #getTableList.table_comment#,#DisplayOptions#]> --->
			<cfset LOCALOUTPUT.rows[i] = [#getTableList.tableName_vch#, #tableDesc#,#DisplayOptions#]>
			<cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />			
	</cffunction>

	<cffunction name="AssignCollaborationCheckListToUser" access="remote" output="false" hint="Assign/update task list of an userId/companyId">
		<cfargument name="inpTaskList" TYPE="string" default=""/>
		<cfargument name="inpToRemoveItems" TYPE="string" default=""/>
		<cfargument name="inpRequiredValues" TYPE="string" default=""/>
		<cfargument name="inpNeedUpdateItems" TYPE="string" default=""/>
		<cfargument name="inpRequiredValuesUpdateItems" TYPE="string" default=""/>		
		<cfargument name="inpUserId" TYPE="numeric" default=0/>
		<cfargument name="inpTableName" TYPE="string" default=""/>
		<cfset var dataout = '0' /> 
       	<cfoutput>
           

			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
       
            <cftry>
            	<cfif 1 EQ 1> 
				<cfset tmpTaskList = arrayNew(1)/>
				<cfset tmpTaskList=ListToArray(#inpTaskList#,"~")>				

				<cfset tempRequiredList=arrayNew(1)/>
				<cfset tempRequiredList=ListToArray(#inpRequiredValues#,"~")>				

				<cfset tmpInpNeedUpdateItems = arrayNew(1)/>
				<cfset tmpInpNeedUpdateItems=ListToArray(#inpNeedUpdateItems#,"~")>				

				<cfset tmpInpRequiredValuesUpdateItems = arrayNew(1)/>
				<cfset tmpInpRequiredValuesUpdateItems=ListToArray(#inpRequiredValuesUpdateItems#,"~")>												

  					<cftransaction>
						<!--- Add record --->
						<cfloop list="#inpTaskList#" index="fieldName" delimiters ="~">
							<cfset requiredValue=tempRequiredList.elementAt(tmpTaskList.indexOf("#fieldName#"))>
		                    <cfquery name="updateUserTaskList" datasource="#Session.DBSourceEBM#">
		                            INSERT INTO
		                             	simpleobjects.tasklist(userid_int,fieldname_vch,tablename_vch,required_int,createddate)
		                            VALUES
		                            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
		                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#fieldName#">,
		                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">,
		                            	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requiredValue#">,
		                            	NOW())
		                    </cfquery>
						</cfloop>
						<cfloop list="#inpToRemoveItems#" index="removedItemId" delimiters ="~">
		                    <cfquery name="deleteUserTaskItem" datasource="#Session.DBSourceEBM#">
		                            DELETE FROM
		                             	simpleobjects.tasklist
		                            WHERE
		                            	userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		                            AND
		                            	fieldName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#removedItemId#">
		                            AND
		                            	tableName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">
		                            
		                    </cfquery>  						
						</cfloop>
						<cfloop list="#inpNeedUpdateItems#" index="UpdateItemId" delimiters ="~">
							<cfset requiredUpdateValue=tmpInpRequiredValuesUpdateItems.elementAt(tmpInpNeedUpdateItems.indexOf("#UpdateItemId#"))>
		                    <cfquery name="updateRequiredValue" datasource="#Session.DBSourceEBM#">
		                            UPDATE
		                             	simpleobjects.tasklist
		                             SET
		                             	required_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requiredUpdateValue#">
		                            WHERE
		                            	userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		                            AND
		                            	fieldName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UpdateItemId#">
		                            AND
		                            	tableName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">
		                    </cfquery>  						
						</cfloop>
					</cftransaction>  
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
                    <!--- Mail user their welcome aboard email --->			
                <cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                </cfif>          
            <cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId,TYPE, MESSAGE, ERRMESSAGE")>  
				
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				<cfelse>
					<cfset cfcatch.errorcode = -3>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
       		    <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>
        <cfreturn dataout />			
	</cffunction>

	<cffunction name="getTaskListOfUser" access="remote" output="false" hint="get all task list items were assigned to an userId/CompanyId">
		<cfargument name="inpUserId" TYPE="numeric" required="yes">
		<cfargument name="inpTableName" TYPE="string" required="yes">
		<cfset var taskListItemIdArray=ArrayNew(1)>
		<cfset var requiredArray=ArrayNew(1)>
		<cfset var dataout = '0' /> 
		<cfset result=StructNew()>
		
		<cftry>
		<!------>
		<cfquery name = "getTaskListItemId" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		l.fieldName_vch,l.required_int
		    FROM 
		    		simpleobjects.tasklist l 
		    LEFT JOIN 
		    		information_schema.columns c
		    ON
		    		l.fieldName_vch=c.column_name
		    WHERE
		    		l.TableName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">
		    AND
		    		c.Table_Name=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpTableName#">
		    AND
		    		l.userId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
		    AND
		    		c.column_name != <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="BATCHID_BI">
		</cfquery>		
		
		<cfloop query="getTaskListItemId">
			<cfset ArrayAppend(taskListItemIdArray,#getTaskListItemId.fieldName_vch#)>
			<cfset ArrayAppend(requiredArray,#getTaskListItemId.required_int#)>
		</cfloop>
		<!------>
	        <cfset dataout =  QueryNew("RXRESULTCODE, TASKITEMS,REQUIRED, TYPE, MESSAGE, ERRMESSAGE")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "TASKITEMS", "#taskListItemIdArray#") />     
	        <cfset QuerySetCell(dataout, "REQUIRED", "#requiredArray#") />     
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />		
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />           
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="getDefaultColumnList" access="remote" output="false" hint="get all columns that always show in collaboration tool">
		<cfargument name="inpTableName" TYPE="string" required="yes">
		<cfset var defaultList=ArrayNew(1)>
		<cfset var dataout = '0' /> 
		<cfset result=StructNew()>
		
		<cftry>
			<cfinclude template="../../public/paths.cfm" >
			<cfscript>
				 files = '\#ManagementPath#\defaultField.ini';
				 //files = '\devjlp\EBM_DEV\management\collaboration.properties';
				 properties = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
				//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
			</cfscript>		
			<cfset columnString=properties.getProperty("colla.default.#inpTableName#")>
			
			<cfset defaultList=ListToArray(#columnString#,',')>
			
	        <cfset dataout =  QueryNew("RXRESULTCODE, DEFAULTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "DEFAULTLIST", "#defaultList#") />     
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 				
			</cfcatch>
		</cftry>
			<cfreturn dataout />
	</cffunction>

	<cffunction name="setDefaultColumnList" access="remote" output="false" hint="set all columns that always show in collaboration tool">
		<cfargument name="inpTableName" TYPE="string" required="yes">
		<cfargument name="inpDefaultColumnList" type="string" required="no">
		
		<cfset dataout = ''/>
		<cftry>
			<cfinclude template="../../public/paths.cfm" >
			<cfscript>
				 files = '\#ManagementPath#\defaultField.ini';
				 //files = '\devjlp\EBM_DEV\management\collaboration.properties';
				 properties = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
				//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
			</cfscript>
			<cfset properties.setProperties("colla.default.#inpTableName#","#inpDefaultColumnList#",files)>
			
	        <cfset dataout =  QueryNew("RXRESULTCODE, DEFAULTLIST, TYPE, MESSAGE, ERRMESSAGE")>  
	        <cfset QueryAddRow(dataout) />
	        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	        <cfset QuerySetCell(dataout, "DEFAULTLIST", "#inpDefaultColumnList#") />     
	        <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />
	        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 				
			</cfcatch>
		</cftry>
			<cfreturn dataout />		
	</cffunction>

	<cffunction name="assignRoleToUser" access="remote" output="false" hint="add a role to an user">
		<cfargument name="inpUserId" type="numeric" required="true">
		<cfargument name="inpRoleId" type="numeric" required="true">
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpRoleId, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
            <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />   
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->	
				<cfset isExisted = false/>
				<cfquery name = "checkExist" datasource="#Session.DBSourceEBM#">
					SELECT
							*
					FROM
							simpleobjects.userroleuseraccountref
					WHERE
							userAccountId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
				</cfquery>
				<cfif checkExist.RecordCount GT 0> 
					<cfquery name="updateRole" datasource="#Session.DBSourceEBM#">
						UPDATE
								simpleobjects.userroleuseraccountref
						SET
								roleId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRoleId#">
								
						WHERE
								userAccountId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
					</cfquery>
				<cfelse>
					<cfquery name="assignRole" datasource="#Session.DBSourceEBM#">
						INSERT INTO
								simpleobjects.userroleuseraccountref(useraccountid_int,roleid_int)
						VALUES
								(#inpUserId#,#inpRoleId#)
					</cfquery>
				</cfif>
		
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpRoleId, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />  
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />


					<cfelse>
	                    <cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpRoleId, TYPE, MESSAGE, ERRMESSAGE")>  
	                    <cfset QueryAddRow(dataout) />
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
			            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
			            <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />  
	                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
	                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
	                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 					
				</cfif>

			
				<cfcatch TYPE="any">
					<cfset dataout =  QueryNew("RXRESULTCODE, inpUserId, inpRoleId, TYPE, MESSAGE, ERRMESSAGE")>  
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />     
		            <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />   
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") /> 					
				</cfcatch>
			</cftry>
		 
       </cfoutput>	
		<cfreturn dataout />			
		
	</cffunction>

	<cffunction name="getRoleList" access="remote" output="false" hint="get All Roles from db">
		<cfset var RoleData = ArrayNew(2)>
		<cfset var data=ArrayNew(1)>
		<cfquery name = "getRoles" dataSource = "#Session.DBSourceEBM#"> 
		    SELECT 
		    		* 
		    FROM 
		    		simpleobjects.userrole 
		    WHERE
		    		1=1
		    <cfif Session.UserRole NEQ "SuperUser">
			    AND RoleName_vch in ('CompanyAdmin','User')
		    </cfif>
		</cfquery>
		
		<cfloop query="getRoles">
			<cfset data=ArrayNew(1)>
			<cfset ArrayAppend(data,#getRoles.RoleId_int#)>
			<cfset ArrayAppend(data,#getRoles.RoleName_vch#)>
			<cfset ArrayAppend(RoleData, data)>
		</cfloop>
		<cfset dataout =  QueryNew("ROLEDATA")> 
        <cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "ROLEDATA", #RoleData#) />
		<cfreturn dataout />
	</cffunction>	

	<cffunction name="getRolesData" access="remote" hint="Get All Roles Data for grid display" output="false">	
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="RoleId_int">
        <cfargument name="sord" required="no" default="ASC">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
           
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
        
        <cfset TotalPages = 0>
              
        <!--- get Total for pager --->
        <cfquery name="getRolesCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.userrole
            WHERE                
                1=1
        </cfquery>

		<cfif getRolesCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(getRolesCount.TOTALCOUNT/rows + 1)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="getRoles" datasource="#Session.DBSourceEBM#">
			SELECT
             	RoleId_int,
                RoleName_vch,
				description_vch
            FROM
                simpleobjects.userrole
            WHERE                
                1=1
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#getRolesCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.rows = arrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="getRoles" startrow="#start#" endrow="#end#">
			<cfset DisplayOptions = "">	
			<cfset roleId="#getRoles.RoleId_int#">

    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
            
			<!--- <cfset DisplayOptions = DisplayOptions & "<img class='edit_Permission ListIconLinks Magnify-Purple-icon' rel='#permissionId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>"> --->
            <cfset DisplayOptions = DisplayOptions & "<img class='del_Role ListIconLinks delete_16x16' rel='#roleId#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>">          
            
            <cfset LOCALOUTPUT.rows[i] = [#getRoles.RoleId_int#, #getRoles.RoleName_vch#,#getRoles.description_vch#,#DisplayOptions#]>
			<cfset i = i + 1> 
        </cfloop>
        
        <cfreturn LOCALOUTPUT />		
	</cffunction> 
	<cffunction name="updateRole" access="remote" output="true">
        <cfargument name="roleId_int" required="no" default=0>
		<cfargument name="RoleName_vch" required="no" default="">
        <cfargument name="description_vch" required="no" default="">
		<cfargument name="inpAction" required="no" default="">
		<cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Already exist
    
	     --->
                           
       	<cfoutput>
           
			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "updateRole" />
			       
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,	INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPROLEID", "#roleId_int#") />  
            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />			
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
       
            <cftry>
				<!--- Verify does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.userrole
                        WHERE
                            RoleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RoleName_vch#">                                               
                    </cfquery>				
 				<cfif inpAction EQ "insert">
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
						<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
			            <cfset QuerySetCell(dataout, "INPROLEID", "#roleId_int#") />  
			            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
			            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />	
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "Role Name already in use") /> 							
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
	                    <cfthrow MESSAGE="Role Name already in use !!!" TYPE="Any" extendedinfo="" errorcode="-6">
	                    <cfreturn dataout/>
                    </cfif> 
				<cfelseif inpAction EQ "" and RoleName_vch NEQ "">
                    <cfif VerifyUnique.TOTALCOUNT GT 1>
						<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
			            <cfset QuerySetCell(dataout, "INPROLEID", "#roleId_int#") />  
			            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
			            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />	
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "Role Name already in use") /> 							
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
	                    <cfthrow MESSAGE="Role Name already in use !!!" TYPE="Any" extendedinfo="" errorcode="-6">
	                    <cfreturn dataout/>
                    </cfif> 					
				</cfif> 
				
				
				<cfif inpAction EQ "">
					<cfif  RoleName_vch NEQ "">
                        <cfquery name="UpdateRoleName" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.userrole
                            SET   
                                RoleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RoleName_vch#">                               
                            WHERE
                                RoleId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#roleId_int#">
                        </cfquery>  
					</cfif>
					<cfif description_vch NEQ "">
                        <cfquery name="UpdateDescription" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simpleobjects.userrole
                            SET   
                                description_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#description_vch#">                               
                            WHERE
                                RoleId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#roleId_int#">
                        </cfquery> 
					</cfif>
						<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			            <cfset QuerySetCell(dataout, "INPROLEID", "#roleId_int#") />  
			            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
			            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />	
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 					
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
						<cfreturn dataout>
				</cfif>
					
					<cfif inpAction EQ "insert" and RoleName_vch NEQ "">
                        <cfquery name="AddNewRole" datasource="#Session.DBSourceEBM#">
                            INSERT INTO
                                simpleobjects.userrole (RoleName_vch,description_vch)
                            VALUES   
                                (<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RoleName_vch#">,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#description_vch#">)
                        </cfquery>
						
                    	<!--- Get next User ID for new user --->               
	                    <cfquery name="GetNextRoleId" datasource="#Session.DBSourceEBM#">
	                        SELECT
	                            RoleId_int
	                        FROM
	                            simpleobjects.userrole
	                        WHERE                
	                            RoleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#RoleName_vch#">                        
	                    </cfquery> 	
	                    
	                    <cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					 	<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
	                    <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			            <cfset QuerySetCell(dataout, "INPROLEID", "#GetNextRoleId.roleId_int#") />  
			            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
			            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />	
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 	                    					 
						<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
						<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
						<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
					</cfif>					

            <cfcatch TYPE="any">
                
				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>					
				<cfset dataout =  QueryNew("RXRESULTCODE, INPROLEID,INPROLENAME,INPDESCRIPTION , TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "INPROLEID", "#roleId_int#") />  
	            <cfset QuerySetCell(dataout, "INPROLENAME", "#RoleName_vch#") />  
	            <cfset QuerySetCell(dataout, "INPDESCRIPTION", "#description_vch#") />	
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />    
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
				<cfreturn dataout />
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

<!--- 		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke> --->
		
        <cfreturn dataout />		
	</cffunction>

	<cffunction name="deleteRole" access="remote" output="true">
		<cfargument name="inpRoleId" TYPE="numeric" default=0/>
		<cfset var dataout = '0' /> 
		
       	<cfoutput>

			<cfset startLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
			<cfset methodName = "deleteRole" />
			
			<cfset dataout =  QueryNew("RXRESULTCODE, inpRoleId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />     
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
			<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
			<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
       
            <cftry>
            	<cfif 1 EQ 1> 
  					<cftransaction>
	                    <cfquery name="deleteRef" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.userroleuseraccountref
	                            WHERE
	                            	roleId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRoleId#">
	                    </cfquery> 
	                    <cfquery name="deleteRole" datasource="#Session.DBSourceEBM#">
	                            DELETE FROM
	                             	simpleobjects.userrole
	                            WHERE
	                            	roleId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpRoleId#">
	                    </cfquery> 	                    
					</cftransaction>  

					
                                                                                    
					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpRoleId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
        		    <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />     
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
					<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
					<cfset dataout =  QueryNew("RXRESULTCODE, inpRoleId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
        		    <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
					<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
					<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
					<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset endLog = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss.lll')#"/>
				<cfset dataout =  QueryNew("RXRESULTCODE, inpRoleId,TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>					
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
       		    <cfset QuerySetCell(dataout, "inpRoleId", "#inpRoleId#") />     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
				<cfset QuerySetCell(dataout, "STARTEVENT", "#startLog#") />
				<cfset QuerySetCell(dataout, "ENDEVENT", "#endLog#") />
				<cfset QuerySetCell(dataout, "METHODNAME", "#methodName#") />
            </cfcatch>
            
            </cftry>     

		</cfoutput>

<!--- 		<cfinvoke method="logChanges">
                <cfinvokeargument name="dataLog" value="#dataout#"/>
        </cfinvoke> --->

        <cfreturn dataout />		
	</cffunction>

	<cffunction name="switchPermissionManaged" access="remote" output="false">
		<cfargument name="inpPermissionManaged" type="string" default="no" required="true">
		<cfset var dataout = '0' />    
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
				
				
				<cfinclude template="../../public/paths.cfm" >
				<cfscript>
					 files = '\#ManagementPath#\defaultField.ini';
					 propUtil = createObject( 'component', '\#SessionPath#\cfc\PropertiesUtil' ).init( files );
					//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
				</cfscript>					
				<cfset propUtil.setProperties("permission.managed","#inpPermissionManaged#",files)>

			 	<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />    
				<cfreturn dataout />
            </cfcatch>
            
            </cftry>     
        <cfreturn dataout />		
	</cffunction>
	<cffunction name="getPermissionManagedStatus" access="remote" output="false">
		<cfargument name="inpPermissionManaged" type="string" default="no" required="true">
		<cfset var dataout = '0' />    
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
				
				
				<cfinclude template="../../public/paths.cfm" >
				<cfscript>
					 files = '\#ManagementPath#\defaultField.ini';
					 propUtil = createObject( 'component', '\#SessionPath#\cfc\PropertiesUtil' ).init( files );
					//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
				</cfscript>					
				<cfset propUtil.setProperties("permission.managed","#inpPermissionManaged#",files)>

			 	<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>
				<cfset dataout =  QueryNew("RXRESULTCODE, PERMISSIONMANAGED, TYPE, MESSAGE, ERRMESSAGE, STARTEVENT, ENDEVENT, METHODNAME")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	            <cfset QuerySetCell(dataout, "PERMISSIONMANAGED", "#inpPermissionManaged#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />    
				<cfreturn dataout />
            </cfcatch>
            
            </cftry>     
        <cfreturn dataout />		
	</cffunction>
</cfcomponent>