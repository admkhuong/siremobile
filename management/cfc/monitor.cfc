<cfcomponent>

	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 

    <!--- Validate paths is on remote dialer --->
    <cffunction name="GetPublishQueueStats" access="remote" output="false" hint="Get list of data points about publish queue process">
    	<cfargument name="inpDateFilter" TYPE="string" required="yes" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), '00:00:00.000')#"/> <!--- Default to midnight current day--->
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK - 
		2 = OK - 
		3 = OK - 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                                     
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") /> 
            <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
            <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
        
            <cftry>
        
            	<cfset inpUserId = 1>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
                    <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                    <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
	                                             
            		<!--- Get last start entry --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(Distribution_dt) AS MaxStartDate
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 10    
                        AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'  
                                                                              
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "LASTSTART", "#LSDateFormat(GetLatestQueueStats.MaxStartDate, 'yyyy-mm-dd')# #LSTimeFormat(GetLatestQueueStats.MaxStartDate, 'HH:mm:ss')#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "LASTSTART", "2000-01-01 00:00:00") />                     
                    </cfif>
                    
                    <!--- Get last end entry --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(Distribution_dt) AS MaxEndDate
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 20           
                       	AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'               
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "LASTEND", "#LSDateFormat(GetLatestQueueStats.MaxEndDate, 'yyyy-mm-dd')# #LSTimeFormat(GetLatestQueueStats.MaxEndDate, 'HH:mm:ss')#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "LASTEND", "2000-01-01 00:00:00") />                     
                    </cfif> 
                    
                    
                    <!--- Get Total runs since midnight --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(Distribution_dt) AS RunCount
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 10           
                       	AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'                  
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "RUNCOUNT", "#GetLatestQueueStats.RunCount#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "RUNCOUNT", "0") />                     
                    </cfif> 
                    
                    
                    <!--- Calculate Expected Runs--->                    
                                                             
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
		            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                    <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
	            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>



    <!--- Validate paths is on remote dialer --->
    <cffunction name="GetEBMStats" access="remote" output="false" hint="Get list of data points about Event Based Messaging (EBM) system">
    	<cfargument name="inpDateFilter" TYPE="string" required="yes" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), '00:00:00.000')#"/> <!--- Default to midnight current day--->
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK - 
		2 = OK - 
		3 = OK - 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, USERCOUNT, USERSLOGINLAST30, USERSREGLAST30, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
            <cfset QuerySetCell(dataout, "USERCOUNT", "0") /> 
            <cfset QuerySetCell(dataout, "USERSLOGINLAST30", "0") />  
            <cfset QuerySetCell(dataout, "USERSREGLAST30", "0") />
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
        
            <cftry>
        
            	<cfset inpUserId = 1>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, USERCOUNT, USERSLOGINLAST30, USERSREGLAST30, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "USERCOUNT", "0") /> 
                    <cfset QuerySetCell(dataout, "USERSLOGINLAST30", "0") />  
                    <cfset QuerySetCell(dataout, "USERSREGLAST30", "0") />
	                                             
            		<!--- Get User Counts--->
                    <cfquery name="GetEBMStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount                                                 
                    </cfquery>  
            
            		<cfif GetEBMStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "USERCOUNT", "#GetEBMStats.UserCount#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "USERCOUNT", "-1") />                     
                    </cfif>
                    
                    <!--- Get New Users Last 30 days --->
                    <cfquery name="GetEBMStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount    
                        WHERE
                        	Created_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'                     
                    </cfquery>  
            
					<cfif GetEBMStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "USERSREGLAST30", "#GetEBMStats.UserCount#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "USERSREGLAST30", "-1") />                     
                    </cfif>
                    
                    <!--- Get User Log In last 30 days--->
                    <cfquery name="GetEBMStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(UserId_int) AS UserCount
                        FROM
                            simpleobjects.useraccount
                        WHERE
                        	LastLogIn_dt > '#LSDateFormat(DATEADD('d', -30, NOW()), 'yyyy-mm-dd')# #LSTimeFormat(DATEADD('d', -30, NOW()), 'HH:mm:ss')#'              
                    </cfquery>  
                               
                    <cfif GetEBMStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "USERSLOGINLAST30", "#GetEBMStats.UserCount#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "USERSLOGINLAST30", "-1") />                     
                    </cfif>
                    
                  
                                                             
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, USERCOUNT, USERSLOGINLAST30, USERSREGLAST30, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "USERCOUNT", "0") /> 
		            <cfset QuerySetCell(dataout, "USERSLOGINLAST30", "0") />  
                    <cfset QuerySetCell(dataout, "USERSREGLAST30", "0") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, USERCOUNT, USERSLOGINLAST30, USERSREGLAST30, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                <cfset QuerySetCell(dataout, "USERCOUNT", "0") /> 
	            <cfset QuerySetCell(dataout, "USERSLOGINLAST30", "0") />  
                <cfset QuerySetCell(dataout, "USERSREGLAST30", "0") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
 
    <!--- Validate paths is on remote dialer --->
    <cffunction name="GetFulfillmentStats" access="remote" output="false" hint="Get list of data points about Fulfillment for the Event Based Messaging (EBM) system">
    	<cfargument name="inpDateFilter" TYPE="string" required="yes" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), '00:00:00.000')#"/> <!--- Default to midnight current day--->
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK - 
		2 = OK - 
		3 = OK - 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
            
			<cfset ProcessedRXDialers = "">
            <cfset UNIQUERXDIALERCOUNT = 0>
            <cfset DTSQUEUEDELLIGIBLECOUNT = 0>
            <cfset DTSQUEUEDCOUNT = 0>
            <cfset DTSPROCCOUNT = 0>
            <cfset CALLRESULTQUEUECOUNT = 0>
            <cfset CALLRESULTCOMPLETECOUNT = 0>
                    	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, UNIQUERXDIALERCOUNT, DTSQUEUEDELLIGIBLECOUNT, DTSQUEUEDCOUNT, DTSPROCCOUNT, CALLRESULTQUEUECOUNT, CALLRESULTCOMPLETECOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
			<cfset QuerySetCell(dataout, "UNIQUERXDIALERCOUNT", "#UNIQUERXDIALERCOUNT#") /> 
            <cfset QuerySetCell(dataout, "DTSQUEUEDELLIGIBLECOUNT", "#DTSQUEUEDELLIGIBLECOUNT#") /> 
            <cfset QuerySetCell(dataout, "DTSQUEUEDCOUNT", "#DTSQUEUEDCOUNT#") /> 
            <cfset QuerySetCell(dataout, "DTSPROCCOUNT", "#DTSPROCCOUNT#") /> 
            <cfset QuerySetCell(dataout, "CALLRESULTQUEUECOUNT", "#CALLRESULTQUEUECOUNT#") /> 
            <cfset QuerySetCell(dataout, "CALLRESULTCOMPLETECOUNT", "#CALLRESULTCOMPLETECOUNT#") /> 
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
        
            <cftry>
        
            	<cfset inpUserId = 1>
               
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
                               
					<cfinclude template="../processing/act_GetDialerInfo.cfm">
                    
					<!--- Loop over each dialer in array --->
					<cfloop index="CurrDialerArrayIndex" from="1" to="#ArrayLen(inpSimpleXDialersArray)#">
		
				        <cfset inpDialerIPAddr = #inpSimpleXDialersArray[CurrDialerArrayIndex][1]#>
						<cfset ServerId_si = #inpSimpleXDialersArray[CurrDialerArrayIndex][2]#>
                                                
        				<!--- Dedupe already processed RXDialer - allows same RXDialer to be specified multiple times in GetDialerInfo but only checks once--->
                        <cfif ListContainsNoCase(ProcessedRXDialers, "#inpDialerIPAddr#" , ',') EQ 0>
                        
                        	<cfset UNIQUERXDIALERCOUNT = UNIQUERXDIALERCOUNT + 1>
                        
                        	<cftry>
                            
                            	 <cfquery name="GetDTSCounts" datasource="#inpDialerIPAddr#">
                                    SELECT
                                        COUNT(*) AS TOTALCOUNT
                                    FROM
                                        DistributedToSend.dts dts JOIN
                                        CallControl.ScheduleOptions so ON (so.BatchId_bi = dts.BatchId_bi)
                                    WHERE
                                        dts.DTSStatusType_ti = 1
                                        AND so.enabled_ti = 1
                                        AND so.STARTHOUR_TI <= (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                                        AND so.ENDHOUR_TI > (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                                        AND #DateFormat(NOW(),"dddd")#_ti = 1 
                                        AND Scheduled_dt < NOW()
                                </cfquery>	  
                                
                                <cfset DTSQUEUEDELLIGIBLECOUNT = DTSQUEUEDELLIGIBLECOUNT + GetDTSCounts.TOTALCOUNT>
                                
                                <cfquery name="GetDTSCounts" datasource="#inpDialerIPAddr#">
                                    SELECT
                                        COUNT(*) AS TOTALCOUNT
                                    FROM
                                        DistributedToSend.dts dts 
                                    WHERE
                                        dts.DTSStatusType_ti = 1                                      
                                </cfquery>	  
                                                                
                                <cfset DTSQUEUEDCOUNT = DTSQUEUEDCOUNT + GetDTSCounts.TOTALCOUNT>
                                
                                <cfquery name="GetDTSCounts" datasource="#inpDialerIPAddr#">
                                    SELECT
                                        COUNT(*) AS TOTALCOUNT
                                    FROM
                                        DistributedToSend.dts dts 
                                    WHERE
                                        dts.DTSStatusType_ti IN (2,3)                                      
                                </cfquery>	  
                                                                
                                <cfset DTSPROCCOUNT = DTSPROCCOUNT + GetDTSCounts.TOTALCOUNT>
                                
                                
                                <cfquery name="ExtractFromDialer" datasource="#inpDialerIPAddr#">
                                    SELECT
                                        COUNT(*) AS TOTALCOUNT
                                    FROM
                                        CallDetail.rxcalldetails
                                    WHERE			
                                        <!--- Active Dialers --->
                                        CallDetailsStatusId_ti = -10
                                </cfquery>
        
                                <cfset CALLRESULTQUEUECOUNT = CALLRESULTQUEUECOUNT + ExtractFromDialer.TOTALCOUNT>
                               
                                <cfquery name="ExtractFromDialer" datasource="#inpDialerIPAddr#">
                                    SELECT
                                        COUNT(*) AS TOTALCOUNT
                                    FROM
                                        CallDetail.rxcalldetails
                                    WHERE			
                                        <!--- Active Dialers --->
                                        CallDetailsStatusId_ti > 2
                                   	AND
			                        	RXCDLStartTime_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'           
                                        
                                </cfquery>
        
                                <cfset CALLRESULTCOMPLETECOUNT = CALLRESULTCOMPLETECOUNT + ExtractFromDialer.TOTALCOUNT>
                                                               
                            <cfcatch TYPE="any">
                                <cfset ENA_Message ="Error - SimpleX NOC - Problem getting RXDialer info from #inpDialerIPAddr#">
                                <cfset ErrorNumber = 200>
                                <cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
                            </cfcatch>
                            </cftry>                                         
                        
                        </cfif>
        
        				<cfset ProcessedRXDialers = ProcessedRXDialers & ",#inpDialerIPAddr#">
        
        			</cfloop>
        
        
					<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, UNIQUERXDIALERCOUNT, DTSQUEUEDELLIGIBLECOUNT, DTSQUEUEDCOUNT, DTSPROCCOUNT, CALLRESULTQUEUECOUNT, CALLRESULTCOMPLETECOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "UNIQUERXDIALERCOUNT", "#UNIQUERXDIALERCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "DTSQUEUEDELLIGIBLECOUNT", "#DTSQUEUEDELLIGIBLECOUNT#") /> 
					<cfset QuerySetCell(dataout, "DTSQUEUEDCOUNT", "#DTSQUEUEDCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "DTSPROCCOUNT", "#DTSPROCCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "CALLRESULTQUEUECOUNT", "#CALLRESULTQUEUECOUNT#") /> 
                    <cfset QuerySetCell(dataout, "CALLRESULTCOMPLETECOUNT", "#CALLRESULTCOMPLETECOUNT#") /> 
	                                                         
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, UNIQUERXDIALERCOUNT, DTSQUEUEDELLIGIBLECOUNT, DTSQUEUEDCOUNT, DTSPROCCOUNT, CALLRESULTQUEUECOUNT, CALLRESULTCOMPLETECOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            		<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "UNIQUERXDIALERCOUNT", "#UNIQUERXDIALERCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "DTSQUEUEDELLIGIBLECOUNT", "#DTSQUEUEDELLIGIBLECOUNT#") /> 
					<cfset QuerySetCell(dataout, "DTSQUEUEDCOUNT", "#DTSQUEUEDCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "DTSPROCCOUNT", "#DTSPROCCOUNT#") /> 
                    <cfset QuerySetCell(dataout, "CALLRESULTQUEUECOUNT", "#CALLRESULTQUEUECOUNT#") /> 
                    <cfset QuerySetCell(dataout, "CALLRESULTCOMPLETECOUNT", "#CALLRESULTCOMPLETECOUNT#") /> 
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, UNIQUERXDIALERCOUNT, DTSQUEUEDELLIGIBLECOUNT, DTSQUEUEDCOUNT, DTSPROCCOUNT, CALLRESULTQUEUECOUNT, CALLRESULTCOMPLETECOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            	<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                <cfset QuerySetCell(dataout, "UNIQUERXDIALERCOUNT", "#UNIQUERXDIALERCOUNT#") /> 
                <cfset QuerySetCell(dataout, "DTSQUEUEDELLIGIBLECOUNT", "#DTSQUEUEDELLIGIBLECOUNT#") /> 
				<cfset QuerySetCell(dataout, "DTSQUEUEDCOUNT", "#DTSQUEUEDCOUNT#") /> 
                <cfset QuerySetCell(dataout, "DTSPROCCOUNT", "#DTSPROCCOUNT#") /> 
                <cfset QuerySetCell(dataout, "CALLRESULTQUEUECOUNT", "#CALLRESULTQUEUECOUNT#") /> 
                <cfset QuerySetCell(dataout, "CALLRESULTCOMPLETECOUNT", "#CALLRESULTCOMPLETECOUNT#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="GetExtractionStats" access="remote" output="false" hint="Get list of data points about Extraction for the Event Based Messaging (EBM) system">
       	<cfargument name="inpDateFilter" TYPE="string" required="yes" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), '00:00:00.000')#"/> <!--- Default to midnight current day--->
        
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK - 
		2 = OK - 
		3 = OK - 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") /> 
            <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
            <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
        
            <cftry>
        
            	<cfset inpUserId = 1>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
					<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
                    <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                    <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
	                                             
            		<!--- Get last start entry --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(Distribution_dt) AS MaxStartDate
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 110    
                        AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'  
                                                                              
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "LASTSTART", "#LSDateFormat(GetLatestQueueStats.MaxStartDate, 'yyyy-mm-dd')# #LSTimeFormat(GetLatestQueueStats.MaxStartDate, 'HH:mm:ss')#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "LASTSTART", "2000-01-01 00:00:00") />                     
                    </cfif>
                    
                    <!--- Get last end entry --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            MAX(Distribution_dt) AS MaxEndDate
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 120           
                       	AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'               
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "LASTEND", "#LSDateFormat(GetLatestQueueStats.MaxEndDate, 'yyyy-mm-dd')# #LSTimeFormat(GetLatestQueueStats.MaxEndDate, 'HH:mm:ss')#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "LASTEND", "2000-01-01 00:00:00") />                     
                    </cfif> 
                    
                    
                    <!--- Get Total runs since midnight --->
                    <cfquery name="GetLatestQueueStats" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(Distribution_dt) AS RunCount
                        FROM
                            simplequeue.simplexprod_log
                        WHERE
                            LogType_int = 110           
                       	AND
                        	Distribution_dt > '#LSDateFormat(inpDateFilter, 'yyyy-mm-dd')# #LSTimeFormat(inpDateFilter, 'HH:mm:ss')#'                  
                    </cfquery>  
            
            		<cfif GetLatestQueueStats.RecordCount GT 0>                    
                    	<cfset QuerySetCell(dataout, "RUNCOUNT", "#GetLatestQueueStats.RunCount#") /> 
                    <cfelse>
          	        	<cfset QuerySetCell(dataout, "RUNCOUNT", "0") />                     
                    </cfif> 
                    
                    
                    <!--- Calculate Expected Runs--->                    
                                                             
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                    <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
		            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                    <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, LASTUPDATESTATS, LASTSTART, LASTEND, RUNCOUNT, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "LASTUPDATESTATS", "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#") />
                <cfset QuerySetCell(dataout, "LASTSTART", "0") /> 
	            <cfset QuerySetCell(dataout, "LASTEND", "0") />  
                <cfset QuerySetCell(dataout, "RUNCOUNT", "0") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
       
</cfcomponent>