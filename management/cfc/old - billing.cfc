<cfcomponent>
	        
            
            
   <!---
   
   
delimiter $$

CREATE TABLE `billing` (
  `UserId_int` int(11) NOT NULL,
  `Balance_int` float(10,3) NOT NULL DEFAULT '0.000',
  `RateType_int` int(4) NOT NULL DEFAULT '1',
  `Rate1_int` float(10,3) NOT NULL DEFAULT '0.050',
  `Rate2_int` float(10,3) NOT NULL DEFAULT '0.050',
  `Rate3_int` float(10,3) NOT NULL DEFAULT '0.050',
  `Increment1_int` int(4) NOT NULL DEFAULT '30',
  `Increment2_int` int(4) NOT NULL DEFAULT '30',
  `Increment3_int` int(4) NOT NULL DEFAULT '30',
  PRIMARY KEY (`UserId_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE  TABLE `simplebilling`.`ratetypes` (
  `RateTypeId_int` INT NOT NULL ,
  `DESC_VCH` VARCHAR(255) NULL ,
  PRIMARY KEY (`RateTypeId_int`) );




CREATE TABLE `balance` (
  `UserId_int` INTEGER(11) NOT NULL,
  `Balance_int` FLOAT(10,3) NOT NULL DEFAULT '0.000',
  `Rate1_int` FLOAT(10,3) NOT NULL DEFAULT '0.050',
  PRIMARY KEY (`UserId_int`)

)ENGINE=InnoDB
CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';
   
   
CREATE TABLE `transactionlog` (
  `TransactionId_int` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `UserId_int` INTEGER(11) NOT NULL,
  `AmountTenthPenny_int` INTEGER(11) NOT NULL,
  `Event_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `EventData_vch` TEXT COLLATE latin1_swedish_ci,
  `Created_dt` DATETIME NOT NULL,
  UNIQUE KEY `TransactionId_int` (`TransactionId_int`)

)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';




   --->         
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBalance" access="remote" output="false" hint="Get simple billing balance to the tenth of a penny">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "Balance", "0") /> 
            <cfset QuerySetCell(dataout, "Rate1", "100") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->                     
                    
					<!--- Get description for each group--->
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            Rate1_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                        <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
                    
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
	                                Rate1_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#0#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">                                
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                Rate1_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                                                                                                            
                        </cfquery>                      
	                   
                        <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "Balance", "0") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "100") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                    
                  </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "Balance", "0") />         
                    <cfset QuerySetCell(dataout, "Rate1", "100") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "Balance", "0") />  
                <cfset QuerySetCell(dataout, "Rate1", "100") />   
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- ADMIN : Get billing balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetBalanceAdmin" access="remote" output="false" hint="Get simple billing balance to the tenth of a penny - Admin version">
        <cfargument name="inpUserId" required="no" default="0">
       
        <cfset var dataout = '0' />           
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "Balance", "0") /> 
            <cfset QuerySetCell(dataout, "Rate1", "100") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                                        
					<!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->  
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>                   
                    
					<!--- Get description for each group--->
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            Rate1_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                        <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
                    
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
	                                Rate1_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#0#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">                                
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">,
                                    'Create Default User Billing Info',
                                    'GetBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                Rate1_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>                      
	                   
                        <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                            <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "Balance", "0") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "100") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                    
                  </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "Balance", "0") />         
                    <cfset QuerySetCell(dataout, "Rate1", "100") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "Balance", "0") />  
                <cfset QuerySetCell(dataout, "Rate1", "100") />   
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update balance count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateBalance" access="remote" output="false" hint="Update account balance by input amount - can be negative - requires higher administrator level access">
		<cfargument name="inpAmountToAdd" required="yes" default="0">
        <cfargument name="inpUserId" required="yes" default="0">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
			<cfset QuerySetCell(dataout, "Balance", "0") /> 
            <cfset QuerySetCell(dataout, "Rate1", "100") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpAmountToAdd) OR !isnumeric(inpAmountToAdd) >
                    	<cfthrow MESSAGE="Invalid Ammount Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
					<!--- Get description for each group--->
                    <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        UPDATE
                        	simplebilling.billing
                         SET                         	  
                            Balance_int = Balance_int + #inpAmountToAdd#                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpAmountToAdd#">,
                                'Update User Billing Info',
                                'UpdateBalance',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  

					<!--- Get current values --->                    
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            Rate1_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
	                    <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                        <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
                    	<!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
	                                Rate1_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpAmountToAdd#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">                                
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.transactionlog
                                (
	                                UserId_int,
    	                            AmountTenthPenny_int,
	                                Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpAmountToAdd#">,
                                    'Create Default User Billing Info',
                                    'UpdateBalance',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                Rate1_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>  
                    
	                    <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
	    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
		  			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "Balance", "0") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "100") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>                              
                    </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
            		<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "Balance", "0") />         
                    <cfset QuerySetCell(dataout, "Rate1", "100") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpAmountToAdd, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpAmountToAdd", "#inpAmountToAdd#") />
	            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                <cfset QuerySetCell(dataout, "Balance", "0") />  
                <cfset QuerySetCell(dataout, "Rate1", "100") />   
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Update rate 1 count(s) --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="UpdateRate1TenthPenny" access="remote" output="false" hint="Update rate 1 to input amount - requires higher administrator level access">
		<cfargument name="inpNewRate" required="yes" default="0">
        <cfargument name="inpUserId" required="yes" default="0">
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
			<cfset QuerySetCell(dataout, "Balance", "0") /> 
            <cfset QuerySetCell(dataout, "Rate1", "100") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "unknown error") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.USERID GT 0>
            
		            <cfif Session.AdministratorAccessLevel LT 10>
                    	<cfthrow MESSAGE="You do not have enough system privilages to access this functionality." TYPE="Any" detail="Contact support for help." errorcode="-2">
                    </cfif>
                    
					<!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->                     
                               
                    <cfif !isnumeric(inpNewRate) OR !isnumeric(inpNewRate) >
                    	<cfthrow MESSAGE="Invalid rate 1 Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
                    <cfif !isnumeric(inpUserId) OR !isnumeric(inpUserId) >
                    	<cfthrow MESSAGE="Invalid User Id Specified" TYPE="Any" detail="" errorcode="-2">
                    </cfif>
                    
					<!--- Get description for each group--->
                    <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        UPDATE
                        	simplebilling.billing
                         SET                         	  
                            Rate1_int = #inpNewRate#                            
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                    </cfquery>  
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpNewRate#">,
                                'Update User Billing Rate 1',
                                'UpdateRate1TenthPenny',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  

					<!--- Get current values --->                    
                    <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT
                            Balance_int,
                            Rate1_int
                        FROM
                           simplebilling.billing
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">                                                                                                                                                            
                    </cfquery>  
            
            		<cfif GetBalance.RecordCount GT 0>            
						<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
	                    <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                        <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />                     
                    <cfelse>
	                    <!--- Self repair --->
                        <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                            	simplebilling.billing
                                (
	                                UserId_int,
    	                            Balance_int,
	                                Rate1_int
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#0#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpNewRate#">                                
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Log all billing transactions--->
                        <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                            INSERT INTO
                                simplebilling.transactionlog
                                (
                                    UserId_int,
                                    AmountTenthPenny_int,
                                    Event_vch,
                                    EventData_vch,
                                    Created_dt
                                )
                                VALUES
                                (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#inpNewRate#">,
                                    'Create Default User Billing Info',
                                    'UpdateRate1TenthPenny',
                                    NOW()                               
                                )                                                                                                                                                  
                        </cfquery>  
                        
                        <!--- Get current values --->                    
                        <cfquery name="GetBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                            SELECT
                                Balance_int,
                                Rate1_int
                            FROM
                               simplebilling.billing
                            WHERE                
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                                                                                                                                            
                        </cfquery>  
                        
                        <cfif GetBalance.RecordCount GT 0>            
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                             <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
	    			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "Balance", "#GetBalance.Balance_int#") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "#GetBalance.Rate1_int#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />   
                        <cfelse>
							<cfset dataout =  QueryNew("RXRESULTCODE, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
                            <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
		  			        <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                            <cfset QuerySetCell(dataout, "Balance", "0") /> 
                            <cfset QuerySetCell(dataout, "Rate1", "100") />  
                            <cfset QuerySetCell(dataout, "TYPE", "") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "Unable to get Billing information") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "User information not found.") />    
                        </cfif>      
                                            
                    </cfif>
                    
                            
                  <cfelse>
				
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate, inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>    
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
            		<cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") /> 
                    <cfset QuerySetCell(dataout, "Balance", "0") />         
                    <cfset QuerySetCell(dataout, "Rate1", "100") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpNewRate,inpUserId, Balance, Rate1, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpNewRate", "#inpNewRate#") />
	            <cfset QuerySetCell(dataout, "inpUserId", "#inpUserId#") />
                <cfset QuerySetCell(dataout, "Balance", "0") />  
                <cfset QuerySetCell(dataout, "Rate1", "100") />   
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    

</cfcomponent>
