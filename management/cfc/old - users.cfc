<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
	<!---
	
	
CREATE TABLE `useraccount` (
  `UserId_int` int(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` varchar(255) NOT NULL DEFAULT '',
  `Password_vch` blob NOT NULL,
  `AltUserName_vch` varchar(255) DEFAULT NULL,
  `AltPassword_vch` varchar(255) DEFAULT NULL,
  `UserLevel_int` int(11) NOT NULL DEFAULT '0',
  `DESC_VCH` varchar(255) DEFAULT NULL,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `CompanyName_vch` varchar(255) DEFAULT NULL,
  `HomePhoneId_int` int(11) DEFAULT NULL,
  `WorkPhoneId_int` int(11) DEFAULT NULL,
  `AlternatePhoneId_int` int(11) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Reactivate_int` tinyint(4) DEFAULT '0',
  `Manager_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1$$




	
CREATE TABLE `useraccount` (
  `UserId_int` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `UserName_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `Password_vch` BLOB NOT NULL,
  `AltUserName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `AltPassword_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `UserLevel_int` INTEGER(11) NOT NULL DEFAULT '0',
  `DESC_VCH` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `ClientServicesReps_vch` VARCHAR(2048) COLLATE latin1_swedish_ci DEFAULT NULL,
  `PrimaryRep_vch` VARCHAR(1024) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CSRNotes_vch` VARCHAR(8000) COLLATE latin1_swedish_ci DEFAULT NULL,
  `FirstName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `LastName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Address1_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Address2_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `City_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `State_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `Country_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `PostalCode_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `EmailAddress_vch` VARCHAR(2000) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CompanyName_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `HomePhoneId_int` INTEGER(11) DEFAULT NULL,
  `WorkPhoneId_int` INTEGER(11) DEFAULT NULL,
  `AlternatePhoneId_int` INTEGER(11) DEFAULT NULL,
  `FaxPhoneId_int` INTEGER(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `HomePhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `WorkPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `AlternatePhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `FaxPhoneStr_vch` VARCHAR(255) COLLATE latin1_swedish_ci DEFAULT NULL,
  `CBPId_int` INTEGER(11) DEFAULT NULL,
  `Created_dt` DATETIME NOT NULL,
  `LastLogIn_dt` DATETIME DEFAULT NULL,
  `Public_int` INTEGER(11) DEFAULT NULL,
  `Active_int` INTEGER(11) DEFAULT NULL,
  PRIMARY KEY (`UserId_int`),
  UNIQUE KEY `UserName_vch` (`UserName_vch`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)

)ENGINE=InnoDB
AUTO_INCREMENT=9 CHARACTER SET 'latin1' COLLATE 'latin1_swedish_ci';
	
	--->

	




	<cffunction name="AddUser" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="RetrieveUserInfo" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="ChangePassword" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
        
    <cffunction name="ResetPassword" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <!--- ************************************************************************************************************************* --->
    <!--- Navigate simple list of phone numbers --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetSimpleUserData" access="remote">
        <cfargument name="q" TYPE="numeric" required="no" default="1">
        <cfargument name="page" TYPE="numeric" required="no" default="1">
        <cfargument name="rows" TYPE="numeric" required="no" default="10">
        <cfargument name="sidx" required="no" default="UserId_int">
        <cfargument name="sord" required="no" default="ASC">
        <cfargument name="inpGroupId" required="no" default="0">
        <cfargument name="UserId_mask" required="no" default="">
        <cfargument name="UserName_mask" required="no" default="">
       
       
		<cfset var dataout = '0' /> 
        <cfset var LOCALOUTPUT = {} />

		<!--- LOCALOUTPUT variables --->
        <cfset var GetSimplePhoneListNumbers="">
           
	   <!--- Cleanup SQL injection --->
       <!--- Replace ' with '' --->
       <!--- Verify all numbers are actual numbers ---> 
       <cfset sidx = REPLACE(sidx, "'", "''", "ALL") /> 
           
   	   <!--- Cleanup SQL injection --->
       <cfif UCASE(sord) NEQ "ASC" AND UCASE(sord) NEQ "DESC">
	       <cfreturn LOCALOUTPUT />
       </cfif> 
        
		<!--- Null results --->
		<cfif #Session.USERID# EQ ""><cfset #Session.USERID# = 0></cfif>
        
        <cfset TotalPages = 0>
              
        <!--- Get data --->
        <cfquery name="GetUsersCount" datasource="#Session.DBSourceEBM#">
			SELECT
             	COUNT(*) AS TOTALCOUNT
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1
                    
            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">              
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">            
            </cfif>              
                
        </cfquery>

		<cfif GetUsersCount.TOTALCOUNT GT 0 AND rows GT 0>
	        <cfset total_pages = ROUND(GetUsersCount.TOTALCOUNT/rows)>
        <cfelse>
        	<cfset total_pages = 0>        
        </cfif>
	
    	<cfif page GT total_pages>
        	<cfset page = total_pages>        
        </cfif>
        
        <cfif rows LT 0>
        	<cfset rows = 0>        
        </cfif>
        
        <cfset start = rows*page - rows>
        
        <!--- Calculate the Start Position for the loop query.
		So, if you are on 1st page and want to display 4 rows per page, for first page you start at: (1-1)*4+1 = 1.
		If you go to page 2, you start at (2-)1*4+1 = 5  --->
		<cfset start = ((arguments.page-1)*arguments.rows)+1>
        
        <cfif start LT 0><cfset start = 1></cfif>
		
		<!--- Calculate the end row for the query. So on the first page you go from row 1 to row 4. --->
		<cfset end = (start-1) + arguments.rows>
        
        
        <!--- Get data --->
        <cfquery name="GetUsers" datasource="#Session.DBSourceEBM#">
			SELECT
             	UserId_int,
                UserName_vch
            FROM
                simpleobjects.useraccount
            WHERE                
                1=1

            <cfif UserId_mask NEQ "" AND UserId_mask NEQ "undefined">
            	AND UserId_int LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserId_mask#%">           
            </cfif>
            
            <cfif UserName_mask NEQ "" AND UserName_mask NEQ "undefined">
            	AND UserName_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#UserName_mask#%">        
            </cfif>   
                    
		    <cfif sidx NEQ "" AND sord NEQ "">
    	        ORDER BY #sidx# #sord#
	        </cfif>
                        
        </cfquery>

            <cfset LOCALOUTPUT.page = "#page#" />
            <cfset LOCALOUTPUT.total = "#total_pages#" />
            <cfset LOCALOUTPUT.records = "#GetUsersCount.TOTALCOUNT#" />
            <cfset LOCALOUTPUT.GROUPID = "#inpGroupId#" />
            <cfset LOCALOUTPUT.rows = ArrayNew(1) />
            
            <cfset i = 1>                
                                
        <cfloop query="GetUsers" startrow="#start#" endrow="#end#">
        			            	
            <!---
			
			 LIMIT #rows#
            
            <cfif start GT 0>
            	OFFSET #start#
			</cfif>            
			
            $responce->rows[$i]['id']=$row[item_id];
    		$responce->rows[$i]['cell']=array($row[item_id],$row[item],$row[item_cd]);
    --->
    		<!---<cfset LOCALOUTPUT.rows[i].id = i />--->
    		<!---<cfset LOCALOUTPUT.rows[i].cell[0] = "#GetUsers.PhoneListId_int#" />
			<cfset LOCALOUTPUT.rows[i].cell[1] = "#GetUsers.UserId_int#" />--->
            
          
            
            <cfset LOCALOUTPUT.rows[i] = [#GetUsers.UserId_int#, #GetUsers.UserName_vch#]>
			<cfset i = i + 1> 
        </cfloop>
		
        <cfset LOCALOUTPUT.rows[i] = [91, "JLP Testing"]>
        
        <cfreturn LOCALOUTPUT />
        

    </cffunction>
      
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add a new user account --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="RegisterNewAccount" access="remote" output="false" hint="Start a new user account">
       	<cfargument name="inpNAUserName" required="yes" default="">
        <cfargument name="inpNAPassword" required="yes" default="">
        <cfargument name="inpMainPhone" required="yes" default="">
        <cfargument name="inpMainEmail" required="yes" default="">
  
        <cfset var dataout = '0' />    
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NextUserId", "0") />     
            <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
            <cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
            <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset inpNAUserName = TRIM(inpNAUserName)>
                    <cfset inpNAPassword = TRIM(inpNAPassword)>
                    <cfset inpMainPhone = TRIM(inpMainPhone)>
                    <cfset inpMainEmail = TRIM(inpMainEmail)>
            
					<!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers ---> 
                    <cfset inpNAUserName = REPLACE(inpNAUserName, "'", "''", "ALL") /> 
                                 	                	                                        
				                        
                    <!--- Validate User Name --->                   
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safeName">
                        <cfinvokeargument name="Input" value="#inpNAUserName#"/>
                    </cfinvoke>
                    
                    <cfif safeName NEQ true OR inpNAUserName EQ "">
	                    <cfthrow MESSAGE="Invalid login name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>  
                    
                    <!--- Validate proper password---> 
                    <cfinvoke 
                     component="validation"
                     method="VldInput"
                     returnvariable="safePass">
                        <cfinvokeargument name="Input" value="#inpNAPassword#"/>
                    </cfinvoke>
                   
                    <cfif safePass NEQ true OR inpNAPassword EQ "">
	                    <cfthrow MESSAGE="Invalid password - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>  
                                                             
                    <!--- Validate primary phone ---> 
                    
					<!---Find and replace all non numerics except P X * #--->
                    <cfset inpMainPhone = REReplaceNoCase(inpMainPhone, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#inpMainPhone#"/>
                    </cfinvoke>
            
            		<cfif safePhone NEQ true OR inpMainPhone EQ "">
	                    <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
            
            		<!--- Validate email address --->
                    <cfinvoke 
                     component="validation"
                     method="VldEmailAddress"
                     returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#inpMainEmail#"/>
                    </cfinvoke>
                    
                    <cfif safeeMail NEQ true OR inpMainEmail EQ "">
	                    <cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>                   
                    
                    
                    <!--- Verify does not already exist--->
                    <!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            UserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
	                    <cfthrow MESSAGE="User name already in use! Try a different user name." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
                                        
                  
					<!--- Add record --->	                                                                                        
                    <cfquery name="AddUser" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simpleobjects.useraccount
                            (UserName_vch, Password_vch, PrimaryPhoneStr_vch, EmailAddress_vch, Created_dt )
                        VALUES
                            (<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">, AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'), <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMainPhone#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMainEmail#">, NOW())                                        
                    </cfquery>     
                    
                    
                    <!--- Get next User ID for new user --->               
                    <cfquery name="GetNextUserId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            UserId_int
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            UserName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpNAUserName#">                        
                    </cfquery>  
                    
                    
                    <!--- Start New Billing --->
                    <cfquery name="StartNewBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.billing
                            (
                                UserId_int,
                                Balance_int,
                                Rate1_int
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultBalance#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultRate1#">                                
                            )                                                                                                                                                  
                    </cfquery>  
                    
                    <!--- Log all billing transactions--->
                    <cfquery name="TransLog" datasource="#Session.DBSourceEBM#">                                                                                                   
                        INSERT INTO
                            simplebilling.transactionlog
                            (
                                UserId_int,
                                AmountTenthPenny_int,
                                Event_vch,
                                EventData_vch,
                                Created_dt
                            )
                            VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNextUserId.UserId_int#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_FLOAT" VALUE="#Session.DefaultBalance#">,
                                'Create Default User Billing Info - Registration Page',
                                'UpdateBalance',
                                NOW()                               
                            )                                                                                                                                                  
                    </cfquery>  
  
                                                                                                         
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NextUserId", "#GetNextUserId.UserId_int#") />   
                    <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
                    <cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                    <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
            
                    <!--- Mail user their welcome aboard email --->			
            
					       
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>     
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NextUserId", "0") />   
                    <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
					<cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                    <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NextUserId, inpNAUserName, inpMainPhone, inpMainEmail, TYPE, MESSAGE, ERRMESSAGE")>    
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NextUserId", "0") />   
                <cfset QuerySetCell(dataout, "inpNAUserName", "#inpNAUserName#") />     
				<cfset QuerySetCell(dataout, "inpMainPhone", "#inpMainPhone#") /> 
                <cfset QuerySetCell(dataout, "inpMainEmail", "#inpMainEmail#") />    
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>  
      
    
    
</cfcomponent>