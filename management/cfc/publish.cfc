<cfcomponent>

	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfinclude template="../../public/paths.cfm" >

<!---
delimiter $$

CREATE TABLE `callresults` (
  `MasterRXCallDetailId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RXCallDetailId_int` int(11) unsigned NOT NULL,
  `CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',
  `DTSID_int` int(10) unsigned DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `PhoneId_int` int(11) DEFAULT NULL,
  `TotalObjectTime_int` int(11) DEFAULT NULL,
  `TotalCallTimeLiveTransfer_int` int(11) DEFAULT NULL,
  `TotalCallTime_int` int(11) DEFAULT NULL,
  `TotalConnectTime_int` int(11) DEFAULT '0',
  `ReplayTotalCallTime_int` int(11) DEFAULT NULL,
  `SixSecondBilling_int` int(11) DEFAULT NULL,
  `SystemBilling_int` int(11) DEFAULT NULL,
  `TotalAnswerTime_int` int(11) DEFAULT NULL,
  `CallResult_int` int(11) DEFAULT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `MessageDelivered_si` smallint(6) DEFAULT NULL,
  `SingleResponseSurvey_si` smallint(6) DEFAULT NULL,
  `UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,
  `NumberOfHoursToRescheduleRedial_si` smallint(6) DEFAULT NULL,
  `TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,
  `TransferStatusId_ti` tinyint(4) DEFAULT NULL,
  `IsHangUpDetected_ti` tinyint(4) DEFAULT NULL,
  `IsOptOut_ti` tinyint(4) DEFAULT NULL,
  `IsOptIn_ti` tinyint(4) DEFAULT '0',
  `IsMaxRedialsReached_ti` tinyint(4) DEFAULT NULL,
  `IsRescheduled_ti` tinyint(4) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `CallStartTime_dt` datetime DEFAULT NULL,
  `CallEndTime_dt` datetime DEFAULT NULL,
  `CallStartTimeLiveTransfer_dt` datetime DEFAULT NULL,
  `CallEndTimeLiveTransfer_dt` datetime DEFAULT NULL,
  `CallResultTS_dt` datetime DEFAULT NULL,
  `PlayFileStartTime_dt` datetime DEFAULT NULL,
  `PlayFileEndTime_dt` datetime DEFAULT NULL,
  `HangUpDetectedTS_dt` datetime DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `DialerName_vch` varchar(255) DEFAULT NULL,
  `CurrTS_vch` varchar(255) DEFAULT NULL,
  `CurrVoice_vch` varchar(255) DEFAULT NULL,
  `CurrTSLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `CurrVoiceLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `CurrCDP_vch` varchar(255) DEFAULT NULL,
  `CurrCDPLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `DialString_vch` varchar(255) DEFAULT NULL,
  `XMLResultStr_vch` text,
  `XMLControlString_vch` varchar(8000) DEFAULT NULL,
  `FileSeqNumber_int` int(11) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  KEY `XPKRXCallDetails` (`MasterRXCallDetailId_int`),
  KEY `IDX_DialString` (`DialString_vch`),
  KEY `IDX_DTSID_int` (`DTSID_int`),
  KEY `IDX_CallResult_int` (`CallResult_int`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1$$

delimiter $$

CREATE TABLE `contactqueue` (
  `DTSId_int` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BatchId_bi` bigint(20) unsigned DEFAULT NULL,
  `DTSStatusType_ti` tinyint(4) DEFAULT '0',
  `TimeZone_ti` tinyint(4) DEFAULT '0',
  `CurrentRedialCount_ti` tinyint(3) unsigned DEFAULT '0',
  `UserId_int` int(10) unsigned DEFAULT NULL,
  `PushLibrary_int` int(11) NOT NULL DEFAULT '0',
  `PushElement_int` int(11) NOT NULL DEFAULT '0',
  `PushScript_int` int(11) NOT NULL DEFAULT '0',
  `EstimatedCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  `CampaignTypeId_int` int(11) DEFAULT NULL,
  `GroupId_int` int(11) NOT NULL DEFAULT '0',
  `Scheduled_dt` datetime DEFAULT NULL,
  `Queue_dt` datetime DEFAULT NULL,
  `Queued_DialerIP_vch` varchar(255) DEFAULT NULL,
  `DialString_vch` varchar(255) NOT NULL,
  `Sender_vch` varchar(255) DEFAULT NULL,
  `XMLControlString_vch` text,
  PRIMARY KEY (`DTSId_int`),
  UNIQUE KEY `UC_DTSId_int` (`DTSId_int`),
  KEY `IDX_Batch_Status_TimeZone_Combo` (`BatchId_bi`,`DTSStatusType_ti`,`TimeZone_ti`),
  KEY `IDX_Scheduled` (`Scheduled_dt`),
  KEY `IDX_UserId_int` (`UserId_int`),
  KEY `IDX_Queued_DialerIP` (`Queued_DialerIP_vch`),
  KEY `IDX_DialString` (`DialString_vch`),
  KEY `IDX_BatchId` (`BatchId_bi`),
  KEY `IDX_DtsStatusType` (`DTSStatusType_ti`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1$$

SELECT * FROM `simplequeue`.`contactqueue`;


--->

	        
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptLibraryDataAsync" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPNEXTELEID" TYPE="string" default="-1"/>
        <cfargument name="INPNEXTDATAID" TYPE="string" default="-1"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" required="yes" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        <cfargument name="inpUserId" TYPE="string" required="yes" default="0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if INPNEXTELEID GT 0 - keep going
	
	INPNEXTDATAID can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry> 
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<!---<cfinclude template="../Distribution/data_ScriptPaths.cfm">--->--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTELEID)>
                    	<cfthrow message="Invalid Element Id Specified" type="any" detail="(#INPNEXTELEID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTDATAID)>
                    	<cfthrow message="Invalid Script Id Specified" type="any" detail="(#INPNEXTDATAID#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DESC_VCH
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                                        
                    <cfif INPNEXTELEID EQ 0><!--- What data to look for--->
                    	<!--- Get first element --->
                        <cfquery name="GetFirstElement" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DSEId_int,
                                DESC_VCH                    
                            FROM
                                rxds.dselement
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                            ORDER BY
                                DSEId_int ASC
                            LIMIT 1            
                        </cfquery>
                        
                        <cfif GetFirstElement.RecordCount EQ 0><!--- Check Element Records --->
							
                            <!--- All still good? Warning Only? TTS Only? --->
							<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "-1") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "-1") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - No Elements Found in Library ID (#INPLIBID#) Library Name(#ValidateLibId.DESC_VCH#") />
                            
                            
                        <cfelse><!--- Check Element Records --->
                        	
							<!--- Get first script --->
                            <cfquery name="GetFirstScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DATAID_INT,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    DESC_VCH
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                                    AND DSEId_int = #GetFirstElement.DSEId_int#
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                ORDER BY
                                    DATAID_INT ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
							<cfif GetFirstScript.RecordCount EQ 0><!--- Check Script Records --->
                                                            
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        DESC_VCH                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                        AND DSEId_int > #GetFirstElement.DSEId_int#
                                    ORDER BY
                                        DSEId_int ASC
                                    LIMIT 1              
                                </cfquery>
                                                                
                                <cfif GetNextElement.RecordCount LT 1>
                                	<cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = -1>
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Finished - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.DESC_VCH#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.DESC_VCH#) - No more Elements found.")  />
                                
                                <cfelse>                                
	                                
									<!--- <cfthrow message="Made it here 1" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
									<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = -1>
                                    
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 7) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.DESC_VCH#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.DESC_VCH#")  />
                                
                                </cfif>
                                
                            <cfelse><!--- Check Script Records --->
                            
                   
                           
                            
                            	<!--- _PhoneRes = Phone Resolustion --->	
            					<cfset MainConversionfile = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\PhoneRes\rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav">     
                                
                                <cfset MainConversionfilePath = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\PhoneRes">           
                            
                            	<!--- Get current side last modified date --->                                
                                <cfset MainfilePath = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.mp3">
							<!--- 	<cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                                <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
 --->
  
								<!--- Verify Directory Exists --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                            
                                    <!--- Need to have WebAdmin as service login and WebAdmin as a LOCALOUTPUT user account on the RXDialer --->
                                    <cfdirectory action="create" directory="#MainConversionfilePath#">
            
                                    <cfset CreationCount = CreationCount + 1> 
                                    
                                    <!--- Still doesn't exist - check your access settings --->
                                    <cfif !DirectoryExists("#MainConversionfilePath#")>
                                        <cfthrow message="Unable to create remote phone resolution script library directory " type="any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                    </cfif>
                                    
                                <cfelse>
                                
                                    <cfset ExistsCount = ExistsCount + 1> 
                                
                                </cfif>
                            
								<cfset MainPathFileExists = FileExists(MainfilePath)>
                                <cfif MainPathFileExists> 
                                    <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                                <cfelse>
                                     <cfset MainfileDate = '1901-01-01 00:00:00'>
                                </cfif>
                            
 
                                <!--- Get remote last modified date --->
                              	<cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#inpUserId#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav">
								<!--- <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                                <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())> --->
								<cfset RemotePathFileExists = FileExists(RemotefilePath)>
                                <cfif RemotePathFileExists> 
									<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                                <cfelse>
                                     <cfset RemotefileDate = '1900-01-01 00:00:00'>
                                </cfif>
                                
                                <!--- Start Result set Here --->
                                <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>        
                                <cfset QueryAddRow(dataout) />                     	
                                                        
                                <!--- if same dont update unless forced too --->
								<cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                               
                                    <cfif MainPathFileExists>
                                        
										<cfif FileExists(MainConversionfile)> 
                                            <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                        <cfelse>
                                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                        </cfif>
                                
                                         <!--- Compare phoneres with main before generating --->
                                        <!--- Only reconvert if file is out of date --->
                                        <cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                        
                                            <!--- Convert File --->
                                            <!--- /fmt WAV = .wav NOTE: This IS case sensitive--->
                                            <!--- /wavm 0 = uncompressed --->
                                            <!--- /wavf 7= 11025 Hz --->
                                            <!--- /wavc 1 = mono --->
                                            <!--- wavbd 0 = 16 Bit --->
                                            <!--- /owe = overwirte existing files --->
                                            <cftry>
                                              <!---  <cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                                </cfexecute>--->
                                                
                                                <cfexecute 	name="#SOXAudiopath#" 
                                                        arguments=' #MainfilePath# -b 16 -r 11025 -c 1 #MainConversionfile#' 
                                                        timeout="120"> 
                                                </cfexecute>
                    
                                            
                                            <cfcatch type="any">
                                            
                                            
                                            </cfcatch>
                                            
                                            </cftry>
                                            
                                        </cfif>
                                        
                                        <!--- Validate new audio CREATED OK--->
                                        <cfif FileExists(MainConversionfile)> 
                                        
                                            <!--- Copy file --->
                                            <cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                        
                                        <cfelse>
                                        
                                            <cfthrow message="Unable to create LOCALOUTPUT phone resolution script library file" type="any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                        
                                        </cfif>
                                                                                
                                        
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                        
                                        <cfif RemotePathFileExists>                                        
                                        	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav #GetFirstScript.DESC_VCH# (#MainfileDate#) (#RemotefileDate#)")  />                             
                             			<cfelse>                                         
										 	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav #GetFirstScript.DESC_VCH# (#MainfileDate#) (N/A)")  />                             			
                                        </cfif>
                             
                             
                                       
                                     <cfelse>
                                        
                                          <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                          <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - File Does not Exists - rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav #GetFirstScript.DESC_VCH#")  />
                                        
                                     </cfif>
                                     
                                     
                                <cfelse><!--- Date Compare --->
                                
                                    <!--- Skip file ---> 
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                    
                                    <cfif RemotePathFileExists> 
	                                    <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav #GetFirstScript.DESC_VCH# (#MainfileDate#) (#RemotefileDate#)")  />
                                    <cfelse>                                    
                                     	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#inpUserId#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DATAID_INT#.wav #GetFirstScript.DESC_VCH# (#MainfileDate#) (N/A)")  />                                     
                                    </cfif>
                                 
                                </cfif><!--- Date Compare --->
                            
                                
                                
                                <!--- Check for next script --->
                                <!--- Get Next script --->
                                <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DATAID_INT                                        
                                    FROM
                                        rxds.scriptdata
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                                        AND DSEId_int = #GetFirstElement.DSEId_int#
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                        AND DATAID_INT > #GetFirstScript.DATAID_INT#
                                    ORDER BY
                                        DATAID_INT ASC
                                    LIMIT 1              
                                </cfquery>                               		
                                                            
                                <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records II--->
                                	                                
                                    <!--- Move on - Get next element --->                              
                                    <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                        SELECT 
                                            DSEId_int,
                                            DESC_VCH                    
                                        FROM
                                            rxds.dselement
                                        WHERE
                                            Active_int = 1
                                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                
                                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                            AND DSEId_int > #GetFirstElement.DSEId_int#
                                        ORDER BY
                                            DSEId_int ASC            
                                        LIMIT 1  
                                    </cfquery>
                                    
                                    <cfif GetNextElement.RecordCount LT 1>
                                        <cfset INPNEXTELEID = -1>
                                        <cfset INPNEXTDATAID = -1>
                                    <cfelse>                                
                                        
										<!--- <cfthrow message="Made it here 2" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->                                        
										<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                        
                                        <!--- Start at first script ID --->
                                        <cfset INPNEXTDATAID = -1>
                                    </cfif>
                                                                    
                                <cfelse><!--- Check Script Records II --->
                                
	                                
    	                            <!--- <cfset INPNEXTELEID = GetNextElement.DSEId_int> --->
                                    <cfset INPNEXTELEID = GetFirstElement.DSEId_int>
                                    <!--- <cfthrow message="Made it here 3" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                    
	   								<cfset INPNEXTDATAID = GetNextScript.DATAID_INT>
                                </cfif><!--- Check Script Records II --->       
                                
                                <!--- All still good --->
                           		<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                                        
							</cfif><!--- Check Script Records --->

                        </cfif><!--- Check Element Records --->
                        
                    
                    <cfelseif INPNEXTELEID GT 0><!--- What data to look for--->
                    	<!--- Get as specified --->
                        <!--- Get script ---> <!--- Always start where you specify and work your way up --->
                        <cfquery name="GetScript" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DATAID_INT,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                DESC_VCH
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                                AND DSEId_int = #INPNEXTELEID#
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                AND DATAID_INT > #INPNEXTDATAID# - 1
                            ORDER BY
                                DATAID_INT ASC
                            LIMIT 1              
                        </cfquery>      
                        
                       <cfif GetScript.RecordCount EQ 0><!--- Check Script Records III --->
                       
                       		<!--- Move on - Get next element --->                              
                            <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DSEId_int,
                                    DESC_VCH                    
                                FROM
                                    rxds.dselement
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                    AND DSEId_int > #INPNEXTELEID#
                                ORDER BY
                                    DSEId_int ASC            
                                LIMIT 1  
                            </cfquery>
                            
                            <cfset LastINPNEXTELEID = INPNEXTELEID>
                            
                            <cfif GetNextElement.RecordCount LT 1>
                                <cfset INPNEXTELEID = -1>
                                <cfset INPNEXTDATAID = -1>
                            <cfelse>                                
                                
								<!--- <cfthrow message="Made it here 4" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
								<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                                                
                                
                                <!--- Start at first script ID --->
                                <cfset INPNEXTDATAID = -1>
                            </cfif>
                                                  
                            
                            <!--- All still good? Warning Only? TTS Only? --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.DESC_VCH#) Element Id(#LastINPNEXTELEID#)")  />
                                
                       
                       <cfelse><!--- Check Script Records III --->
                       <!--- Get current side last modified date --->                                
		
        					<!--- _PhoneRes = Phone Resolustion --->	
            				<cfset MainConversionfile = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#INPNEXTELEID#\PhoneRes\rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav">       
                            <cfset MainConversionfilePath = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#INPNEXTELEID#\PhoneRes">            
            
            				<cfset MainfilePath = "#rxdsLocalWritePath#\U#inpUserId#\L#INPLIBID#\E#INPNEXTELEID#\rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.mp3">
                           <!---  <cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                            <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
                            --->     
                            
                            <!--- Verify Directory Exists --->
							<cfif !DirectoryExists("#MainConversionfilePath#")>
                        
                                <!--- Need to have WebAdmin as service login and WebAdmin as a LOCALOUTPUT user account on the RXDialer --->
                                <cfdirectory action="create" directory="#MainConversionfilePath#">
        
                                <cfset CreationCount = CreationCount + 1> 
                                
                                <!--- Still doesn't exist - check your access settings --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                                    <cfthrow message="Unable to create remote phone resolution script library directory " type="any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                </cfif>
                                
                            <cfelse>
                            
                                <cfset ExistsCount = ExistsCount + 1> 
                            
                            </cfif>
                                
                                
                            <cfset MainPathFileExists = FileExists(MainfilePath)>
                            <cfif MainPathFileExists> 
                            	<cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                            <cfelse>
	                             <cfset MainfileDate = '1901-01-01 00:00:00'>
                            </cfif>
                                                        
                            <!--- Get remote last modified date --->
                            <cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#inpUserId#\L#INPLIBID#\E#INPNEXTELEID#\rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav">
                          <!---   <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                            <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())>
                             --->
                                                        
                            <cfif FileExists(MainConversionfile)> 
                            	<cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                            <cfelse>
	                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                            </cfif>
                            
                            <cfset RemotePathFileExists = FileExists(RemotefilePath)>
                            <cfif RemotePathFileExists> 
                                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                            <cfelse>
                                 <cfset RemotefileDate = '1900-01-01 00:00:00'>
                            </cfif>
                             
                            <!--- Start Result set Here --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>                          
                            <cfset QueryAddRow(dataout) />   	
                            
                            <!--- if same dont update unless forced too --->
                            <cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                           
                           		<cfif MainPathFileExists>
                                   
                                 	<cfif FileExists(MainConversionfile)> 
                                        <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                    <cfelse>
                                         <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                    </cfif>
                            
		                             <!--- Compare phoneres with main before generating --->
                            		<!--- Only reconvert if file is out of date --->
									<cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                    
										<!--- Convert File --->
                                        <!--- /fmt WAV = .wav NOTE: This IS case sensitive --->
                                        <!--- /wavm 0 = uncompressed --->
                                        <!--- /wavf 7= 11025 Hz --->
                                        <!--- /wavc 1 = mono --->
                                        <!--- wavbd 0 = 16 Bit --->
                                        <!--- /owe = overwirte existing files --->
                                        <cftry>
                                           <!--- <cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                            </cfexecute>--->
                                            
                                            <cfexecute name="#SOXAudiopath#" 
                                                    arguments=' #MainfilePath# -b 16 -r 11025 -c 1 #MainConversionfile#' 
                                                    timeout="120"> 
                                            </cfexecute>
                                        
                                        <cfcatch type="any">
                                        
                                        
                                        </cfcatch>
                                        
                                        </cftry>
                                        
                                    </cfif>
                                    
                                    <!--- Validate new audio CREATED OK--->
                                    <cfif FileExists(MainConversionfile)> 
                                    
                                    	<!--- Copy file --->
                                    	<cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                    
                                    <cfelse>
                                    
                                    	<cfthrow message="Unable to create LOCALOUTPUT phone resolution script library file" type="any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                    
                                    </cfif>
                                    
                                    
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                    
                                    <cfif RemotePathFileExists> 
                                    	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav #GetScript.DESC_VCH# (#MainfileDate#) (#RemotefileDate#)")  />			<cfelse>
                                       	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav #GetScript.DESC_VCH# (#MainfileDate#) (N/A)")  />		                                     </cfif>
                                    
                                 <cfelse>
                                    
                                      <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                      <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Main File Does not Exists - rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav #GetScript.DESC_VCH#")  />
                                    
                                 </cfif>
                                 
                                 
                            <cfelse><!--- Date Compare --->
                            
                                <!--- Skip file ---> 
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                
								<cfif RemotePathFileExists>
	                                <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav #GetScript.DESC_VCH# (#MainfileDate#) (#RemotefileDate#)")  />
                                <cfelse>
									<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#inpUserId#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DATAID_INT#.wav #GetScript.DESC_VCH# (#MainfileDate#) (N/A)")  />
                                </cfif>
                             
                            </cfif><!--- Date Compare --->
                           
                            
                            <!--- Check for next script --->
                            <!--- Get Next script --->
                            <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DATAID_INT                                        
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                                    AND DSEId_int = #INPNEXTELEID#
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                    AND DATAID_INT > #GetScript.DATAID_INT#
                                ORDER BY
                                    DATAID_INT ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
                            <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records IV--->
                                                                
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        DESC_VCH                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                                        AND DSEId_int > #INPNEXTELEID#
                                    ORDER BY
                                        DSEId_int ASC            
                                    LIMIT 1  
                                </cfquery>
                                
                                <cfif GetNextElement.RecordCount LT 1>
	                                <!--- If no more data and no more elements then just exit --->
                                    <cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = 0>
                                <cfelse>                                
                                
                                	<!--- <cfthrow message="Made it here 5" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                
                                    <cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = 0>
                                </cfif>
                                                                
                            <cfelse><!--- Check Script Records IV --->
                                <cfset INPNEXTELEID = INPNEXTELEID>
                                <cfset INPNEXTDATAID = GetNextScript.DATAID_INT>
                            </cfif><!--- Check Script Records IV --->       
                            
                            <!--- All still good --->
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                       
                       </cfif><!--- Check Script Records III --->
                    
                    <cfelse><!--- What data to look for--->
                    	<!--- No valid data found --->
                         	<cfthrow message="Invalid Data Specified" type="any" detail="Data specified in the request is invalid." errorcode="-4">  
                    
                    </cfif><!--- What data to look for--->
                        
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
		            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
	            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <!--- Validate script Lib paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptLibrary" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele - Script">
        <cfargument name="inpUserId" TYPE="string" required="yes" default="0"/>
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" required="yes" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        
        <cfset var dataout = '0' />    
                                      
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if INPELEID GT 0 - keep going
	
	INPNEXTDATAID can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
       	<cfoutput>
         
         	
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, CURRELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry>             
	             
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Get first script --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT                           
                            DSEId_int                           
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        ORDER BY
                            DSEId_int ASC
                    </cfquery>  
                    
                    <cfif GetElements.RecordCount GT 0>
 	                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, CURRELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfelse>         
                    	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, CURRELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                        <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                        <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                        <cfset QuerySetCell(dataout, "MESSAGE", "No Elements found for specified Library") />    
                    </cfif>
                    
                    <cfloop query="GetElements">
                                            
                    	   <cfinvoke 
                             component="#Session.ManagementCFCPath#.publish"
                             method="ValidateRemoteScriptLibraryDataAsync"
                             returnvariable="RetValAudio">
                                <cfinvokeargument name="inpUserId" value="#inpUserId#"/>
                                <cfinvokeargument name="INPLIBID" value="#INPLIBID#"/>
                                <cfinvokeargument name="INPELEID" value="#GetElements.DSEId_int#"/>
                                <cfinvokeargument name="INPDIALERIPADDR" value="#INPDIALERIPADDR#"/>
                                <cfinvokeargument name="inpForceUpdate" value="0"/>            
                            </cfinvoke>
                                        
                        <cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                        <cfset QuerySetCell(dataout, "CURRELEID", "#GetElements.DSEId_int#") /> 
                        <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                        <cfset QuerySetCell(dataout, "MESSAGE", "#RetValAudio.MESSAGE#") />     
                        
                    </cfloop>
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, CURRELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Id is invalid") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, CURRELEID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- Validate script element paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptElement" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPELEID" TYPE="string" default="-1"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" required="yes" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        <cfargument name="inpUserId" TYPE="string" required="yes" default="0"/>
        <cfset var dataout = '0' />    
                                      
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if INPELEID GT 0 - keep going
	
	INPNEXTDATAID can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry>             
	        
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPELEID)>
                    	<cfthrow message="Invalid Element Id Specified" type="any" detail="(#INPELEID#) is invalid." errorcode="-3">
                    </cfif>
                                                                                       
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            DESC_VCH
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Get first script --->
                    <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            DATAID_INT,
                            DSEId_int,
                            DSId_int,
                            UserId_int,
                            StatusId_int,
                            AltId_vch,
                            Length_int,
                            Format_int,
                            DESC_VCH
                        FROM
                            rxds.scriptdata
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">
                            AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPELEID#">
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">
                        ORDER BY
                            DATAID_INT ASC
                    </cfquery>  
                    
                    <cfloop query="GetScripts">
                    
                    	   <cfinvoke 
                             component="#Session.ManagementCFCPath#.publish"
                             method="ValidateRemoteScriptLibraryDataAsync"
                             returnvariable="RetValAudio">
                                <cfinvokeargument name="inpUserId" value="#inpUserId#"/>
                                <cfinvokeargument name="INPLIBID" value="#INPLIBID#"/>
                                <cfinvokeargument name="INPELEID" value="#INPELEID#"/>
                                <cfinvokeargument name="INPSCRIPTID" value="#GetScripts.DATAID_INT#"/>
                                <cfinvokeargument name="INPDIALERIPADDR" value="#INPDIALERIPADDR#"/>
                                <cfinvokeargument name="inpForceUpdate" value="0"/>            
                            </cfinvoke>
                    
                    </cfloop>
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Id is invalid") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPELEID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPELEID", "#INPELEID#") /> 
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
  
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptData" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="INPUSERID" TYPE="string" required="yes" default="0"/>
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPNEXTELEID" TYPE="string" default="-1"/>
        <cfargument name="INPNEXTDATAID" TYPE="string" default="-1"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" TYPE="string" default="0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if INPNEXTELEID GT 0 - keep going
	
	INPNEXTDATAID can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
                           
       	<cfoutput>
        
            <cfset PSADebug = "START" />
             
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry> 
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../Distribution/data_ScriptPaths.cfm">--->
            
             	<cfset PSADebug = PSADebug & " inpUserId=#inpUserId#" />
             
             	<cfset PSADebug = PSADebug & " Session.DBSourceEBM=#Session.DBSourceEBM#" />
             
             
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif INPUSERID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTELEID)>
                    	<cfthrow message="Invalid Element Id Specified" type="any" detail="(#INPNEXTELEID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPNEXTDATAID)>
                    	<cfthrow message="Invalid Script Id Specified" type="any" detail="(#INPNEXTDATAID#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            Desc_vch
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <cfset PSADebug = PSADebug & " INPNEXTELEID=#INPNEXTELEID#" />
                    
                    
                                        
                    <cfif INPNEXTELEID EQ 0><!--- What data to look for--->
                    	<!--- Get first element --->
                        <cfquery name="GetFirstElement" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DSEId_int,
                                Desc_vch                    
                            FROM
                                rxds.dselement
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                            ORDER BY
                                DSEId_int ASC
                            LIMIT 1            
                        </cfquery>
                        
                        
                        <cfset PSADebug = PSADebug & " GetFirstElement.RecordCount=#GetFirstElement.RecordCount#" />
                        
                        <cfif GetFirstElement.RecordCount EQ 0><!--- Check Element Records --->
							
                            <!--- All still good? Warning Only? TTS Only? --->
							<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "-1") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "-1") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - No Elements Found in Library ID (#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#") />
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "WARNING - No Elements Found in Library ID (#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#") />
                            
                            
                        <cfelse><!--- Check Element Records --->
                        	
							<!--- Get first script --->
                            <cfquery name="GetFirstScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    Desc_vch
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                    AND DSEId_int = #GetFirstElement.DSEId_int#
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
							<cfif GetFirstScript.RecordCount EQ 0><!--- Check Script Records --->
                                                            
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                        AND DSEId_int > #GetFirstElement.DSEId_int#
                                    ORDER BY
                                        DSEId_int ASC
                                    LIMIT 1              
                                </cfquery>
                                                                
                                <cfif GetNextElement.RecordCount LT 1>
                                	<cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = -1>
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Finished - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#) - No more Elements found.")  />
                                
                                <cfelse>                                
	                                
									<!--- <cfthrow message="Made it here 1" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
									<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = -1>
                                    
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 7) />
                                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                    <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                    <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#")  />
                                
                                </cfif>
                                
                            <cfelse><!--- Check Script Records --->
                            
                   
                           		<cfset PSADebug = PSADebug & " Script Records Exist" />
                           
                            
                            	<!--- _PhoneRes = Phone Resolustion --->	
            					<cfset MainConversionfile = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\PhoneRes\rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">     
                                
                                <cfset MainConversionfilePath = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\PhoneRes">           
                                                       
                            	<!--- Get current side last modified date --->                                
                                <cfset MainfilePath = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.mp3">
							<!--- 	<cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                                <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
 --->
  
  								<cfset PSADebug = PSADebug & " Checking Local Paths Now" />
  
  
								<!--- Verify Directory Exists --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                            
                                    <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                    <cfdirectory action="create" directory="#MainConversionfilePath#">
            
                                    <cfset CreationCount = CreationCount + 1> 
                                    
                                    <!--- Still doesn't exist - check your access settings --->
                                    <cfif !DirectoryExists("#MainConversionfilePath#")>
                                        <cfthrow message="Unable to create remote phone resolution script library directory " type="any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                    </cfif>
                                    
                                <cfelse>
                                
                                    <cfset ExistsCount = ExistsCount + 1> 
                                
                                </cfif>
                            
								<cfset MainPathFileExists = FileExists(MainfilePath)>
                                <cfif MainPathFileExists> 
                                    <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                                <cfelse>
                                     <cfset MainfileDate = '1901-01-01 00:00:00'>
                                </cfif>
                            
                            	<cfset PSADebug = PSADebug & " Checking Remote Paths Next" />
                            
 
                                <!--- Get remote last modified date --->
                              	<cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#INPUSERID#\L#INPLIBID#\E#GetFirstElement.DSEId_int#\rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
								<!--- <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                                <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())> --->
								<cfset RemotePathFileExists = FileExists(RemotefilePath)>
                                <cfif RemotePathFileExists> 
									<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                                <cfelse>
                                     <cfset RemotefileDate = '1900-01-01 00:00:00'>
                                </cfif>
                               
                               <cfset PSADebug = PSADebug & " Start Result set Here" />
                                
                                <!--- Start Result set Here --->
                                <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>        
                                <cfset QueryAddRow(dataout) />                     	
                                                        
                                <!--- if same dont update unless forced too --->
								<cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                               
                                    <cfif MainPathFileExists>
                                        
										<cfif FileExists(MainConversionfile)> 
                                            <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                        <cfelse>
                                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                        </cfif>
                                
                                         <!--- Compare phoneres with main before generating --->
                                        <!--- Only reconvert if file is out of date --->
                                        <cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                        
                                            <!--- Convert File --->
                                            <!--- /fmt WAV = .wav NOTE: This IS case sensitive--->
                                            <!--- /wavm 0 = uncompressed --->
                                            <!--- /wavf 7= 11025 Hz --->
                                            <!--- /wavc 1 = mono --->
                                            <!--- wavbd 0 = 16 Bit --->
                                            <!--- /owe = overwirte existing files --->
                                            <cftry>
                                      			<!---          
									  			<cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                                </cfexecute>
												--->
                                            
                                                <cfexecute 	name="#SOXAudiopath#" 
                                                        arguments='#MainfilePath# -r 11025 -c 1 -b 16 #MainConversionfile#' 
                                                        timeout="60"> 
                                                </cfexecute>
            
                                            <cfcatch type="any">
                                            
                                            
                                            </cfcatch>
                                            
                                            </cftry>
                                            
                                        </cfif>
                                        
                                        <!--- Validate new audio created OK--->
                                        <cfif FileExists(MainConversionfile)> 
                                        
                                            <!--- Copy file --->
                                            <cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                        
                                        <cfelse>
                                        
                                            <cfthrow message="Unable to create local phone resolution script library file" type="any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                        
                                        </cfif>
                                                                                
                                        
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                        
                                        <cfif RemotePathFileExists>                                        
                                        	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />                             
                             			<cfelse>                                         
										 	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                             			
                                        </cfif>
                             
                             
                                       
                                     <cfelse>
                                        
                                          <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                          <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - File Does not Exists - rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch#")  />
                                        
                                     </cfif>
                                     
                                     
                                <cfelse><!--- Date Compare --->
                                
                                    <!--- Skip file ---> 
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                    
                                    <cfif RemotePathFileExists> 
	                                    <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                    <cfelse>                                    
                                     	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#INPUSERID#_#INPLIBID#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                                     
                                    </cfif>
                                 
                                </cfif><!--- Date Compare --->
                            
                                
                                
                                <!--- Check for next script --->
                                <!--- Get Next script --->
                                <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DataId_int                                        
                                    FROM
                                        rxds.scriptdata
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                        AND DSEId_int = #GetFirstElement.DSEId_int#
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                        AND DataId_int > #GetFirstScript.DataId_int#
                                    ORDER BY
                                        DataId_int ASC
                                    LIMIT 1              
                                </cfquery>                               		
                                                            
                                <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records II--->
                                	                                
                                    <!--- Move on - Get next element --->                              
                                    <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                        SELECT 
                                            DSEId_int,
                                            Desc_vch                    
                                        FROM
                                            rxds.dselement
                                        WHERE
                                            Active_int = 1
                                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                            AND DSEId_int > #GetFirstElement.DSEId_int#
                                        ORDER BY
                                            DSEId_int ASC            
                                        LIMIT 1  
                                    </cfquery>
                                    
                                    <cfif GetNextElement.RecordCount LT 1>
                                        <cfset INPNEXTELEID = -1>
                                        <cfset INPNEXTDATAID = -1>
                                    <cfelse>                                
                                        
										<!--- <cfthrow message="Made it here 2" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->                                        
										<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                        
                                        <!--- Start at first script ID --->
                                        <cfset INPNEXTDATAID = -1>
                                    </cfif>
                                                                    
                                <cfelse><!--- Check Script Records II --->
                                
	                                
    	                            <!--- <cfset INPNEXTELEID = GetNextElement.DSEId_int> --->
                                    <cfset INPNEXTELEID = GetFirstElement.DSEId_int>
                                    <!--- <cfthrow message="Made it here 3" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                    
	   								<cfset INPNEXTDATAID = GetNextScript.DataId_int>
                                </cfif><!--- Check Script Records II --->       
                                
                                <!--- All still good --->
                           		<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                                <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                                                        
							</cfif><!--- Check Script Records --->

                        </cfif><!--- Check Element Records --->
                        
                    
                    <cfelseif INPNEXTELEID GT 0><!--- What data to look for--->
                    	<!--- Get as specified --->
                        <!--- Get script ---> <!--- Always start where you specify and work your way up --->
                        <cfquery name="GetScript" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DataId_int,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                Desc_vch
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPNEXTELEID#">  
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                AND DataId_int > #INPNEXTDATAID# - 1
                            ORDER BY
                                DataId_int ASC
                            LIMIT 1              
                        </cfquery>      
                        
                        
                       <cfset PSADebug = PSADebug & " GetScript.RecordCount=#GetScript.RecordCount#" />
                      
                        
                       <cfif GetScript.RecordCount EQ 0><!--- Check Script Records III --->
                       
                       		<!--- Move on - Get next element --->                              
                            <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DSEId_int,
                                    Desc_vch                    
                                FROM
                                    rxds.dselement
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                    AND DSEId_int > #INPNEXTELEID#
                                ORDER BY
                                    DSEId_int ASC            
                                LIMIT 1  
                            </cfquery>
                            
                            <cfset LastINPNEXTELEID = INPNEXTELEID>
                            
                            <cfif GetNextElement.RecordCount LT 1>
                                <cfset INPNEXTELEID = -1>
                                <cfset INPNEXTDATAID = -1>
                            <cfelse>                                
                                
								<!--- <cfthrow message="Made it here 4" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
								<cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                                                
                                
                                <!--- Start at first script ID --->
                                <cfset INPNEXTDATAID = -1>
                            </cfif>
                                                  
                            
                            <!--- All still good? Warning Only? TTS Only? --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "0") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                            <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Moving on - No Script Data Found in Library Id(#INPLIBID#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#LastINPNEXTELEID#)")  />
                                
                       
                       <cfelse><!--- Check Script Records III --->
                       <!--- Get current side last modified date --->                                
		
        					<!--- _PhoneRes = Phone Resolustion --->	
            				<cfset MainConversionfile = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#INPNEXTELEID#\PhoneRes\rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav">       
                            <cfset MainConversionfilePath = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#INPNEXTELEID#\PhoneRes">            
            
            				<cfset MainfilePath = "#rxdsLocalWritePath#\U#INPUSERID#\L#INPLIBID#\E#INPNEXTELEID#\rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.mp3">
                           <!---  <cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                            <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
                            --->     
                            
                            
                            <cfset PSADebug = PSADebug & " Go DirectoryExists(#MainConversionfilePath#)" />
                      
                      
                            <!--- Verify Directory Exists --->
							<cfif !DirectoryExists("#MainConversionfilePath#")>
                        
                                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                <cfdirectory action="create" directory="#MainConversionfilePath#">
        
                                <cfset CreationCount = CreationCount + 1> 
                                
                                <!--- Still doesn't exist - check your access settings --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                                    <cfthrow message="Unable to create remote phone resolution script library directory " type="any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                </cfif>
                                
                            <cfelse>
                            
                                <cfset ExistsCount = ExistsCount + 1> 
                            
                            </cfif>
                                
                                
                            <cfset MainPathFileExists = FileExists(MainfilePath)>
                            <cfif MainPathFileExists> 
                            	<cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                            <cfelse>
	                             <cfset MainfileDate = '1901-01-01 00:00:00'>
                            </cfif>
                                                        
                            <!--- Get remote last modified date --->
                            <cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#INPUSERID#\L#INPLIBID#\E#INPNEXTELEID#\rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav">
                          <!---   <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                            <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())>
                             --->
                                                        
                            <cfif FileExists(MainConversionfile)> 
                            	<cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                            <cfelse>
	                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                            </cfif>
                            
                            
                            <cfset PSADebug = PSADebug & " Go FileExists(#RemotefilePath#)" />
                      
                            <cfset RemotePathFileExists = FileExists(RemotefilePath)>
                            <cfif RemotePathFileExists> 
                                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                            <cfelse>
                                 <cfset RemotefileDate = '1900-01-01 00:00:00'>
                            </cfif>
                             
                            <!--- Start Result set Here --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>                          
                            <cfset QueryAddRow(dataout) />   	
                            
                            <cfset PSADebug = PSADebug & " Go DateCompare(MainfileDate,RemotefileDate)=#DateCompare(MainfileDate,RemotefileDate)#" />
                                            
                            <!--- if same dont update unless forced too --->
                            <cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                           
                           		<cfif MainPathFileExists>
                                   
                                 	<cfif FileExists(MainConversionfile)> 
                                        <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                    <cfelse>
                                         <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                    </cfif>
                            
		                             <!--- Compare phoneres with main before generating --->
                            		<!--- Only reconvert if file is out of date --->
									<cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                    
                                    
                                        <cfset PSADebug = PSADebug & " Go SOXAudiopath=#SOXAudiopath#" />                        
                        
										<!--- Convert File --->
                                        <!--- /fmt WAV = .wav NOTE: This IS case sensitive --->
                                        <!--- /wavm 0 = uncompressed --->
                                        <!--- /wavf 7= 11025 Hz --->
                                        <!--- /wavc 1 = mono --->
                                        <!--- wavbd 0 = 16 Bit --->
                                        <!--- /owe = overwirte existing files --->
                                        <cftry>
                                          	<!---  
										  	<cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                            </cfexecute>
											--->
                                            
                                            <cfexecute 	name="#SOXAudiopath#" 
                                                        arguments='#MainfilePath# -r 11025 -c 1 -b 16 #MainConversionfile#' 
                                                        timeout="60"> 
                                            </cfexecute>                                                
                                        
                                        <cfcatch type="any">
                                        
                                        
                                        </cfcatch>
                                        
                                        </cftry>
                                        
                                    </cfif>
                                    
                                    <!--- Validate new audio created OK--->
                                    <cfif FileExists(MainConversionfile)> 
                                    
                                     	<cfset PSADebug = PSADebug & " Go copy MainConversionfile=#MainConversionfile# RemotefilePath=#RemotefilePath#" />
                                     
                                    	<!--- Copy file --->
                                    	<cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                    
                                    <cfelse>
                                    
                                    	<cfthrow message="Unable to create local phone resolution script library file" type="any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                    
                                    </cfif>
                                    
                                    
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                    
                                    <cfif RemotePathFileExists> 
                                    	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />			<cfelse>
                                       	<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Copied - rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />		                                     </cfif>
                                    
                                 <cfelse>
                                    
                                      <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                      <cfset QuerySetCell(dataout, "MESSAGE", "WARNING - Main File Does not Exists - rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch#")  />
                                    
                                 </cfif>
                                 
                                 
                            <cfelse><!--- Date Compare --->
                            
                                <!--- Skip file ---> 
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                
								<cfif RemotePathFileExists>
	                                <cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                <cfelse>
									<cfset QuerySetCell(dataout, "MESSAGE", "OK - File Already Up To Date - rxds_#INPUSERID#_#INPLIBID#_#INPNEXTELEID#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />
                                </cfif>
                             
                            </cfif><!--- Date Compare --->
                            
                            
                            <!--- Check for next script --->
                            <!--- Get Next script --->
                            <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int                                        
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                                    AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPNEXTELEID#">  
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                    AND DataId_int > #GetScript.DataId_int#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
                            <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records IV--->
                                                                
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#">                                 
                                        AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                                        AND DSEId_int > #INPNEXTELEID#
                                    ORDER BY
                                        DSEId_int ASC            
                                    LIMIT 1  

                                </cfquery>
                                
                                <cfif GetNextElement.RecordCount LT 1>
	                                <!--- If no more data and no more elements then just exit --->
                                    <cfset INPNEXTELEID = -1>
                                    <cfset INPNEXTDATAID = 0>
                                <cfelse>                                
                                
                                	<!--- <cfthrow message="Made it here 5" type="any" detail="(#INPLIBID#) is invalid." errorcode="-15"> --->
                                
                                    <cfset INPNEXTELEID = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset INPNEXTDATAID = 0>
                                </cfif>
                                                                
                            <cfelse><!--- Check Script Records IV --->
                                <cfset INPNEXTELEID = INPNEXTELEID>
                                <cfset INPNEXTDATAID = GetNextScript.DataId_int>
                            </cfif><!--- Check Script Records IV --->       
                            
                            <!--- All still good --->
                            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />       
                            <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 	
                            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") />  
                            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                       
                       </cfif><!--- Check Script Records III --->
                    
                    <cfelse><!--- What data to look for--->
                    	<!--- No valid data found --->
                        
                            <cfset PSADebug = PSADebug & " Invalid Data Specified generic" />                        
                        
                         	<cfthrow message="Invalid Data Specified" type="any" detail="Data specified in the request is invalid." errorcode="-4">  
                    
                    </cfif><!--- What data to look for--->
                        
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
		            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />  
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "User Session is expired") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPNEXTELEID, INPNEXTDATAID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPNEXTELEID", "#INPNEXTELEID#") /> 
	            <cfset QuerySetCell(dataout, "INPNEXTDATAID", "#INPNEXTDATAID#") /> 
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE# #PSADebug#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
      
    
    
	<cffunction name="UpdateScheduleOptionsRemoteRXDialer" output="false" hint="Update Remote RXDialer Schedule Object - No session validation version" access="remote">                              
        <cfargument name="INPBATCHID" default="0">
        <cfargument name="INPDIALERIPADDR" default="0.0.0.0">
                        
        <cfset var dataout = '0' />             
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -5 = Bad batch id specified - no SQL injection attacks
    
	     --->          
                           
       	<cfoutput>
                       
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />   
            <cfset QuerySetCell(dataout, "MESSAGE", "") />   
            
        	<cftry>
                                      
                <!--- Loop over 0-7--->
                <cfloop from="0" to="7" step="1" index="CurrDSDOW">
                                
					<!--- Get Master Batch Options --->
                    <cfquery name="GetMasterScheduleOptions" datasource="#Session.DBSourceEBM#">
                        SELECT  
                            <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->							  
                            ENABLED_TI,
                            STARTHOUR_TI,
                            ENDHOUR_TI,
                            SUNDAY_TI,
                            MONDAY_TI,
                            TUESDAY_TI,
                            WEDNESDAY_TI,
                            THURSDAY_TI,
                            FRIDAY_TI,
                            SATURDAY_TI,
                            LOOPLIMIT_INT,
                            STOP_DT,
                            START_DT,
                            STARTMINUTE_TI,
                            ENDMINUTE_TI,
                            BLACKOUTSTARTHOUR_TI,
                            BLACKOUTENDHOUR_TI,
                            LASTUPDATED_DT,
                            DynamicScheduleDayOfWeek_ti                            
                        FROM 
                            simpleobjects.scheduleoptions 	
                        WHERE 
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                        AND
                          DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                    </cfquery>
    
    
                    <cfif GetMasterScheduleOptions.RecordCount GT 0>
                    
                        <cfquery name="GetRemoteScheduleOptions" datasource="#INPDIALERIPADDR#">
                            SELECT  
                                <!--- CONVERT(varchar(20), LASTUPDATED_DT, 120) as LASTUPDATED_DT --->
                                LASTUPDATED_DT
                            FROM 
                                CallControl.ScheduleOptions 	
                            WHERE 
                              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">	
                            AND
                              DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">				  
                        </cfquery>
                        
                        
                        <cfif GetRemoteScheduleOptions.RecordCount GT 0>
                            
                            <cfif DateCompare(GetMasterScheduleOptions.LASTUPDATED_DT, GetRemoteScheduleOptions.LASTUPDATED_DT) EQ 1 >
                                                           
                                <!--- Do Update on Remote Dialer --->	
                                <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                    UPDATE
                                        CallControl.ScheduleOptions
                                    SET                                                                              
                                        ENABLED_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,
                                        STARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                        ENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">, 
                                        SUNDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                        MONDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                        TUESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                        WEDNESDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                        THURSDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                        FRIDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                        SATURDAY_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                        LOOPLIMIT_INT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                        STOP_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                        START_DT = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                        STARTMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                        ENDMINUTE_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                        BLACKOUTSTARTHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                        BLACKOUTENDHOUR_TI = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,                            
                                        LASTUPDATED_DT = NOW()        
                                    WHERE 
                                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">
                                    AND
                                        DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                </cfquery>
                            
                            </cfif>
                        
                        <cfelse>
                            
                            <!--- Do Update on Remote Dialer --->	
                            <cfquery name="UpdateRemoteDialerData" datasource="#INPDIALERIPADDR#">
                                 INSERT INTO CallControl.ScheduleOptions 
                                    (
                                    BatchId_bi,
                                    ENABLED_TI,
                                    STARTHOUR_TI,
                                    ENDHOUR_TI,
                                    SUNDAY_TI,
                                    MONDAY_TI,
                                    TUESDAY_TI,
                                    WEDNESDAY_TI,
                                    THURSDAY_TI,
                                    FRIDAY_TI,
                                    SATURDAY_TI,
                                    LOOPLIMIT_INT,
                                    STOP_DT,
                                    START_DT,
                                    STARTMINUTE_TI,
                                    ENDMINUTE_TI,
                                    BLACKOUTSTARTHOUR_TI,
                                    BLACKOUTENDHOUR_TI,
                                    LASTUPDATED_DT,
                                    DynamicScheduleDayOfWeek_ti
                                    )
                                VALUES
                                    (
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TRIM(INPBATCHID)#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENABLED_TI#">,<!--- ENABLED_TI = YES for web site users --->
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SUNDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.MONDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.TUESDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.WEDNESDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.THURSDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.FRIDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.SATURDAY_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetMasterScheduleOptions.LOOPLIMIT_INT#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.STOP_DT,'YYYY-MM-DD'))#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#Trim(DateFormat(GetMasterScheduleOptions.START_DT,'YYYY-MM-DD'))#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.STARTMINUTE_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.ENDMINUTE_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTSTARTHOUR_TI#">,
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#GetMasterScheduleOptions.BLACKOUTENDHOUR_TI#">,
                                    NOW(),
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">
                                    )								         
                            </cfquery> 	
                        
                        </cfif>
    
                    
                    <cfelse>
                    
                    	<!--- If no master scheule for this day then remove from remote--->
                        <cfquery name="DeleteSchedule" datasource="#INPDIALERIPADDR#">                        
                            DELETE FROM
                                CallControl.ScheduleOptions 
                            WHERE 
                                BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">
                            AND
                                DynamicScheduleDayOfWeek_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TINYINT" VALUE="#CurrDSDOW#">	
                        </cfquery>                                                     
                    
                    </cfif>

                </cfloop> 


       
                <!--- All good --->
                <cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />
                <cfset QuerySetCell(dataout, "MESSAGE", "Remote RXDialer Schedule Updated OK") />    
                
                                          
                                                     
            <cfcatch type="any">
            
	            <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>                
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />     
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
	<!--- ************************************************************************************************************************* --->
    <!--- input Batch ID Then Output a List of Script Libray Ids --->
    <!--- ************************************************************************************************************************* --->
       
  	<cffunction name="GetSCRIPTLIBs" access="remote" output="false" hint="Replace Existing DM, RXSS ELE, OR CCD in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="inpStartLibId" TYPE="string" default="0"/>
                             
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = No Libraries Found
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
            
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataoutBuff = QueryNew("RXRESULTCODE, INPBATCHID, SCRIPTLIB, DEBUG, MESSAGE, ERRMESSAGE")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            
            <cftry>                	
      
      			<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch type="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           
                           
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">

                
	                <cfset CURRLib = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch type="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@LIB")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURRLib = "-1">                        
                    </cfif>
                    
                              
          			<cfset QueryAddRow(dataoutBuff) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "SCRIPTLIB", "#CURRLib#") /> 
               	
               		<cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />     
              
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRLib = "-1">
                                       	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch type="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DS") />
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                                             
                        
                    <cfelse>
                        <cfset CURRLib = "-1" />                        
                    </cfif>
          
          			<cfset QueryAddRow(dataoutBuff) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
           			<cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />
           
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "SCRIPTLIB", "#CURRLib#") /> 
                                                
                </cfloop>
                                          
            
            	<cfset dataout = QueryNew("RXRESULTCODE, SCRIPTLIB, INPBATCHID, MESSAGE, ERRMESSAGE")>   
            
            	<!--- Write out local Query ---> 
                
                <!--- DMs First --->
                <cfquery dbTYPE="query" name="GetLibs">
                    SELECT 
                    	DISTINCT SCRIPTLIB    
                    FROM 
                        dataoutBuff
                    WHERE 
                        SCRIPTLIB > 0
                    
                    <cfif inpStartLibId GT 0>
                       	AND SCRIPTLIB > #inpStartLibId#
                    </cfif>
                    ORDER BY
                    	SCRIPTLIB
                </cfquery>
                 
                 
                <cfif GetLibs.RecordCount GT 0>
               
                    <cfloop query="GetLibs">
                        <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "SCRIPTLIB", "#GetLibs.SCRIPTLIB#") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "Library Found Lib ID=(#GetLibs.SCRIPTLIB#)") /> 
                        
                    </cfloop>
                 
                <cfelse>
                 
                <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "3") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "SCRIPTLIB", "0") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "No More Libraries Found") /> 
               
                </cfif>
                 
                                                            
		  		                             
            <cfcatch type="any">
                
                <cfif cfcatch.errorcode EQ "">
                	<cfset ErrorCodeBuff = "-10">
                <cfelse>
                	<cfset ErrorCodeBuff = "#cfcatch.errorcode#">
                </cfif>
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#ErrorCodeBuff#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>      
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
        
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemotePaths" access="remote" output="false" hint="Verify all Script Library paths are valid in remote location User - Lib - Ele">
        <cfargument name="INPLIBID" TYPE="string" default="0"/>
        <cfargument name="INPDIALERIPADDR" TYPE="string" default="0.0.0.0"/>
        <cfargument name="inpUserId" TYPE="string" required="yes" default="0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - All Paths already exist
		2 = OK - Partial Paths Existed - New Paths Added
		3 = OK - OK - No Paths were there but now all paths have been created
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = No paths to create
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
            <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
            <cfset QuerySetCell(dataout, "MESSAGE", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry>
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../Distribution/data_ScriptPaths.cfm">--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserId GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(INPLIBID)>
                    	<cfthrow MESSAGE="Invalid Library Id TYPE Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#"> 
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow MESSAGE="Invalid Library Id Specified" TYPE="Any" detail="(#INPLIBID#) is invalid." errorcode="-3">
                    </cfif>
                    
                   
                   	<!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#inpUserId#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#inpUserId#\L#INPLIBID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Ele Directory(s) Exist --->
                    <!--- Get list of all elements --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT                            
                            DSEId_int                                                
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPLIBID#"> 
                            AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserId#">                                    
                    </cfquery>       
                    
                    <cfloop query="GetElements">
                    
                     
                    	<cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#inpUserId#\L#INPLIBID#\E#GetElements.DSEId_int#">
                    
						<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            
                            <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                            <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                            <cfset CreationCount = CreationCount + 1> 
                            
                            <!--- Still doesn't exist - check your access settings --->
                            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                            </cfif>
                            
                        <cfelse>
                        
                            <cfset ExistsCount = ExistsCount + 1> 
                        
                        </cfif>
                    
                    
                    </cfloop>
                    
                                 
                    
                    <!--- Four possibilities --->
                   	<cfif CreationCount EQ 0 AND ExistsCount GT 0>
                   
					  	<!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - All Paths already exist") />    
                    
                   	<cfelseif CreationCount GT 0 AND ExistsCount GT 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - Partial Paths Existed - New Paths Added") />   
                    
                    <cfelseif CreationCount GT 0 AND ExistsCount EQ 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                       	<cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                       	<cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                   		<cfset QuerySetCell(dataout, "MESSAGE", "OK - No Paths were there but now all paths have been created") />                                    
                   	<cfelse>
						<cfthrow MESSAGE="Unable to create any user script library directory paths." TYPE="Any" detail="Failed to create any paths - Systme Error" errorcode="-4"> 
                   	</cfif>
                 
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") /> 
                    <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") />  
                    <cfset QuerySetCell(dataout, "MESSAGE", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch TYPE="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, INPLIBID, INPDIALERIPADDR, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPLIBID", "#INPLIBID#") />  
                <cfset QuerySetCell(dataout, "INPDIALERIPADDR", "#INPDIALERIPADDR#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
</cfcomponent>