<cfcomponent>
	
	<cfset ProdDB = "Bishop" />    
    
    <!--- Get user status by contact strind as id --->
	<cffunction name="GetCurrentUserStatus" access="remote" output="false" hint="Get user limits.">
		<cfargument name="inpContactString" TYPE="string" required="yes" default="" hint="The user contact string of the account we want the status information from." />
		<cfset var LOCALOUTPUT = {}>
		<cfset var getContactStatus = '' />
		        
        <cfset LOCALOUTPUT.RXRESULTCODE = '-1'>        
        <cfset LOCALOUTPUT.FCCCSKINID = "">
        <cfset LOCALOUTPUT.PROCESSEDCOUNTAM = "">
        <cfset LOCALOUTPUT.PROCESSEDCOUNTPM = "">
        <cfset LOCALOUTPUT.TIMEZONE = "">
        <cfset LOCALOUTPUT.CREATED = "">
        <cfset LOCALOUTPUT.UPDATED = "">
        <cfset LOCALOUTPUT.INVITE = "">
        <cfset LOCALOUTPUT.COMPLETEDAM = "">
        <cfset LOCALOUTPUT.COMPLETEDPM = "">
        <cfset LOCALOUTPUT.DOUBLEOPTINPROCESSED = "">
        <cfset LOCALOUTPUT.OPTOUT = "">
        <cfset LOCALOUTPUT.PROGRAM = "">
        <cfset LOCALOUTPUT.SEVENDAY = "">
        <cfset LOCALOUTPUT.FOURTEENDAY = "">
        <cfset LOCALOUTPUT.CONTACTSTRING = "">
        <cfset LOCALOUTPUT.AMSEQUENCESTRING = "">
        <cfset LOCALOUTPUT.SFTPFILESOURCE = "">
        <cfset LOCALOUTPUT.MESSAGE = "Not Found"/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/>  
                    
		<cfif TRIM(inpContactString) eq "">
			<cfset LOCALOUTPUT.RXRESULTCODE = '-2'>
			<cfreturn LOCALOUTPUT>
		</cfif>
        
		<cftry>
                	            
 			<cfquery name="getContactStatus" datasource="#ProdDB#">
				SELECT 
                	FCCCSkinId_int,
                    ProcessedCountAM_int,
                    ProcessedCountPM_int,
                    TimeZone_int,
                    Created_dt,
                    Updated_dt,
                    Invite_dt,
                    CompletedAM_dt,
                    CompletedPM_dt,
                    DoubleOptInProcessed_dt,
                    Program_vch,
                    ContactString_vch,
                    AMSequenceString_vch,
                    SFTPFileSource_vch,
                    7DaySummarySent_int,
                    14DaySummarySent_int,
                    OptOut_dt
                FROM 
                	simplexprogramdata.fcccskin
				WHERE
					ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
			</cfquery>			
            
			<cfif getContactStatus.RecordCount GT 0>
				<cfset LOCALOUTPUT.RXRESULTCODE = '1'>
                <cfset LOCALOUTPUT.FCCCSKINID = "#getContactStatus.FCCCSkinId_int#">
				<cfset LOCALOUTPUT.PROCESSEDCOUNTAM = "#getContactStatus.ProcessedCountAM_int#">
                <cfset LOCALOUTPUT.PROCESSEDCOUNTPM = "#getContactStatus.ProcessedCountPM_int#">
                <cfset LOCALOUTPUT.TIMEZONE = "#getContactStatus.TimeZone_int#">
                
				<cfif getContactStatus.Created_dt EQ "">
               		<cfset LOCALOUTPUT.CREATED = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.CREATED = "#LSDateFormat(getContactStatus.Created_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.Created_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfif getContactStatus.Updated_dt EQ "">
               		<cfset LOCALOUTPUT.UPDATED = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.UPDATED = "#LSDateFormat(getContactStatus.Updated_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.Updated_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfif getContactStatus.Invite_dt EQ "">
               		<cfset LOCALOUTPUT.INVITE = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.INVITE = "#LSDateFormat(getContactStatus.Invite_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.Invite_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfif getContactStatus.CompletedAM_dt EQ "">
               		<cfset LOCALOUTPUT.COMPLETEDAM = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.COMPLETEDAM = "#LSDateFormat(getContactStatus.CompletedAM_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.CompletedAM_dt, 'HH:mm:ss')#">                
                </cfif>
                
                 <cfif getContactStatus.CompletedPM_dt EQ "">
               		<cfset LOCALOUTPUT.COMPLETEDPM = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.COMPLETEDPM = "#LSDateFormat(getContactStatus.CompletedPM_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.CompletedPM_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfif getContactStatus.DoubleOptInProcessed_dt EQ "">
               		<cfset LOCALOUTPUT.DOUBLEOPTINPROCESSED = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.DOUBLEOPTINPROCESSED = "#LSDateFormat(getContactStatus.DoubleOptInProcessed_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.DoubleOptInProcessed_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfif getContactStatus.OptOut_dt EQ "">
               		<cfset LOCALOUTPUT.OPTOUT = "NULL">                
                <cfelse>
                	<cfset LOCALOUTPUT.OPTOUT = "#LSDateFormat(getContactStatus.OptOut_dt, 'yyyy-mm-dd')# #LSTimeFormat(getContactStatus.OptOut_dt, 'HH:mm:ss')#">                
                </cfif>
                
                <cfset LOCALOUTPUT.SEVENDAY = "#getContactStatus.7DaySummarySent_int#">
                <cfset LOCALOUTPUT.FOURTEENDAY = "#getContactStatus.14DaySummarySent_int#">
                
				<cfset LOCALOUTPUT.PROGRAM = "#getContactStatus.Program_vch#">
                <cfset LOCALOUTPUT.CONTACTSTRING = "#getContactStatus.ContactString_vch#">
                <cfset LOCALOUTPUT.AMSEQUENCESTRING = "#getContactStatus.AMSequenceString_vch#">
                <cfset LOCALOUTPUT.SFTPFILESOURCE = "#getContactStatus.SFTPFileSource_vch#">
            </cfif>
			          	
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RXRESULTCODE = '-3'>
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
    
    
    <cffunction name="UpdateCurrentUserStatus" access="remote" output="false" hint="Get user limits.">
		<cfargument name="FCCCSkinId_int" TYPE="string" required="yes" default="" hint="The user contact string of the account we want the status information from." />
		<cfargument name="ProcessedCountAM_int" TYPE="string" required="no" default="" hint="" />
        <cfargument name="ProcessedCountPM_int" TYPE="string" required="no" default="" hint="" />
        <cfargument name="TimeZone_int" TYPE="string" required="no" default="" hint="" />
        <cfargument name="Created_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="Updated_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="Invite_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="CompletedAM_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="CompletedPM_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="DoubleOptInProcessed_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="Program_vch" TYPE="string" required="no" default="" hint="" />
        <cfargument name="ContactString_vch" TYPE="string" required="no" default="" hint="" />
        <cfargument name="AMSequenceString_vch" TYPE="string" required="no" default="" hint="" />
        <cfargument name="SFTPFileSource_vch" TYPE="string" required="no" default="" hint="" />
        <cfargument name="OptOut_dt" TYPE="string" required="no" default="" hint="" />
        <cfargument name="SevenDaySummarySent_int" TYPE="string" required="no" default="" hint="" />
        <cfargument name="FourteenDaySummarySent_int" TYPE="string" required="no" default="" hint="" />
        
        
        
		<cfset var LOCALOUTPUT = {}>
		<cfset var SetContactStatus = '' />
		        
        <cfset LOCALOUTPUT.RXRESULTCODE = '-1'>        
        <cfset LOCALOUTPUT.MESSAGE = "All Good"/>
		<cfset LOCALOUTPUT.ERRMESSAGE = ""/>  
                    
		<cfif TRIM(ContactString_vch) eq "">
			<cfset LOCALOUTPUT.RXRESULTCODE = '-2'>
			<cfreturn LOCALOUTPUT>
		</cfif>
        
		<cftry>
                	            
            
            <cfif FCCCSkinId_int EQ 0 or FCCCSkinId_int EQ "">      
            
            	<!--- Insert into DB --->
                <cfquery name="InsertContactStatus" datasource="#ProdDB#">
                    INSERT INTO 
                        simplexprogramdata.fcccskin
                        (
                            ProcessedCountAM_int,
                            ProcessedCountPM_int,
                            TimeZone_int,
                            Created_dt,
                            Updated_dt,
                            Invite_dt,
                            CompletedAM_dt,
                            CompletedPM_dt,
                            DoubleOptInProcessed_dt,
                            Program_vch,
                            ContactString_vch,
                            AMSequenceString_vch,
                            SFTPFileSource_vch,
                            OptOut_dt
                        
                        )
                        VALUES
                        (
                            0,
                            0,
                            31,
                            NOW(),
                            NOW(),
                            NOW(),
                            NULL,
                            NULL,
                            NULL,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Program_vch#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString_vch#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AMSequenceString_vch#">,
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="QATesting">,
                            NULL                                
                        )
                </cfquery>	    
            
            
            <cfelse>          
                                
                <cfquery name="SetContactStatus" datasource="#ProdDB#">
                    UPDATE 
                        simplexprogramdata.fcccskin
                    SET
                        ProcessedCountAM_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ProcessedCountAM_int#">,
                        ProcessedCountPM_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ProcessedCountPM_int#">,
                        TimeZone_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TimeZone_int#">,
                        
                        <cfif Created_dt NEQ "NULL">
                            Created_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Created_dt#">,
                        <cfelse>
                        	Created_dt = NULL,                            
                        </cfif>
                        
                        <cfif Updated_dt NEQ "NULL">
                            Updated_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Updated_dt#">,
                        <cfelse>
                        	Updated_dt = NULL,                            
                        </cfif>
                        
                        <cfif Invite_dt NEQ "NULL">
                            Invite_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Invite_dt#">,
                        <cfelse>
                        	Invite_dt = NULL,                            
                        </cfif>
                        
                        <cfif CompletedAM_dt NEQ "NULL">
                            CompletedAM_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CompletedAM_dt#">,
                        <cfelse>
                        	CompletedAM_dt = NULL,                            
                        </cfif>
                        
                        <cfif CompletedPM_dt NEQ "NULL">
                            CompletedPM_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CompletedPM_dt#">,
                        <cfelse>
                        	CompletedPM_dt = NULL,                            
                        </cfif>
                        
                        <cfif DoubleOptInProcessed_dt NEQ "NULL">
                            DoubleOptInProcessed_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#DoubleOptInProcessed_dt#">,
                        <cfelse>
                        	DoubleOptInProcessed_dt = NULL,                            
                        </cfif>
                        
                        <cfif OptOut_dt NEQ "NULL">
                            OptOut_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OptOut_dt#">,
                        <cfelse>
                        	OptOut_dt = NULL,                            
                        </cfif>
                        
                        Program_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Program_vch#">,
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ContactString_vch#">,
                        AMSequenceString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AMSequenceString_vch#">,
                        SFTPFileSource_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SFTPFileSource_vch#">,
                        7DaySummarySent_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SevenDaySummarySent_int#">,
                        14DaySummarySent_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#FourteenDaySummarySent_int#">
                    WHERE
                        FCCCSkinId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#FCCCSkinId_int#">
                </cfquery>		
            
            </cfif>
            	
            
			<cfset LOCALOUTPUT.RXRESULTCODE = '1'>
   		          	
			<cfreturn LOCALOUTPUT>
			<cfcatch type="any">
				<cfset LOCALOUTPUT.RXRESULTCODE = '-3'>
                <cfset LOCALOUTPUT.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset LOCALOUTPUT.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn LOCALOUTPUT>
			</cfcatch>
		</cftry>
	</cffunction>
    
    
</cfcomponent>