<!--- Check everyone on FCCC Skip Invite List for Double Opt In--->


<cfset ProdDB = "Bishop" />
<cfset DefaultTimeZone = 31 />

          
<!--- Get elligable Contacts from DB --->
<cfquery name="getContacts" datasource="#ProdDB#">
    SELECT 
        FCCCSkinId_int,
        ContactString_vch,
        ProcessedCountAM_int,
        ProcessedCountPM_int,
        TimeZone_int,
        Created_dt,
        Updated_dt,
        Invite_dt,
        CompletedAM_dt,
        CompletedPM_dt,
        DoubleOptInProcessed_dt,
        Program_vch,
        ContactString_vch,
        AMSequenceString_vch,
        SFTPFileSource_vch
    FROM 
        simplexprogramdata.fcccskin
    WHERE		
    	DoubleOptInProcessed_dt IS NULL
    AND
    	OptOut_dt IS NULL         
</cfquery>


<cfset OptInAddedCount = 0/>


<cfloop query="getContacts">
	
        
    <cfquery name="getOptIns" datasource="#ProdDB#">
        SELECT 
            COUNT(*) AS TOTALCOUNT 
        FROM 
            simplelists.optinout
        WHERE
            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getContacts.ContactString_vch#"> 
        AND
            OptIn_dt IS NOT NULL
        AND
            OptOut_dt IS NULL
        AND
            ShortCode_vch = '244687'
    </cfquery>	
    
    <cfif getOptIns.TOTALCOUNT GT 0>
    
    
	    <cfquery name="SetOptIns" datasource="#ProdDB#">
            UPDATE
                simplexprogramdata.fcccskin
            SET
                DoubleOptInProcessed_dt = NOW(),
                Updated_dt = NOW()                                               
            WHERE
                FCCCSkinId_int = #getContacts.FCCCSkinId_int#  
        </cfquery>                        
    
    
    	<cfset OptInAddedCount = OptInAddedCount + 1 />
    
    </cfif>        


</cfloop>

<cfsavecontent variable="DoubleOptInDetailDump">
	<cfoutput>
    	Total count double opted in today is (#OptInAddedCount#)    
    </cfoutput>

</cfsavecontent>

<cfoutput>
  
	#DoubleOptInDetailDump#

</cfoutput>
    

<cfmail to="jpeterson@messagebroadcast.com;pshah@messagebroadcast.com;swazzan@messagebroadcast.com" from="mbreports@messagebroadcast.com" subject="Fox Chase Cancer Center Skin Double Opt In Processing" type="html">
   	<cfoutput>
    
  		#DoubleOptInDetailDump#
        
    </cfoutput>
</cfmail>
            
