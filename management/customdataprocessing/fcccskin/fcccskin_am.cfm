<cfparam name="inpContactString" default="" />
<cfparam name="inpBlockSend" default="0" />

<cfset ProdDB = "Bishop" />

<cfinclude template="fcccskin_2optin.cfm" />

<!--- Change this to production Batch Id - Make sure it is elligable for real time --->
<cfset AMBatchId = "1173">


<!--- Queries for meta-data --->

<!--- Read Message from DB --->	
<cfquery name="getAMQuestions" datasource="#ProdDB#">
    SELECT 
        TipId_int,
        Desc_vch,
        Class_vch
    FROM 
        simplexprogramdata.fcccskin_am_tips	
    ORDER BY
        TipId_int ASC			
</cfquery>	



<!--- Get counts from DB --->
<cfquery name="getCountInDB" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBAM" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPM" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'    
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTH" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'    
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBAMComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
    AND
 		CompletedAM_dt IS NOT NULL  
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPMComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'  
    AND
 		CompletedPM_dt IS NOT NULL   
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTHComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'
    AND
 		CompletedAM_dt IS NOT NULL 
    AND
        CompletedPM_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBAMOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
    AND
 		DoubleOptInProcessed_dt IS NOT NULL  
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPMOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'  
    AND
 		DoubleOptInProcessed_dt IS NOT NULL   
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTHOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'
    AND
 		DoubleOptInProcessed_dt IS NOT NULL     
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBAMOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'AM'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBPMOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'PM'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBBothOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'BOTH'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	



<!--- API Public/Private Keys--->


<!--- Get elligable Contacts from DB --->
<cfquery name="getContacts" datasource="#ProdDB#">
    SELECT 
        FCCCSkinId_int,
        ContactString_vch,
        ProcessedCountAM_int,
        ProcessedCountPM_int,
        TimeZone_int,
        Created_dt,
        Updated_dt,
        Invite_dt,
        CompletedAM_dt,
        CompletedPM_dt,
        DoubleOptInProcessed_dt,
        Program_vch,
        ContactString_vch,
        AMSequenceString_vch,
        SFTPFileSource_vch
    FROM 
        simplexprogramdata.fcccskin
    WHERE
		<cfif TRIM(inpContactString) NEQ "">
                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
            AND
        </cfif>
    	Program_vch IN ('AM','BOTH')    
    AND 
    	CompletedAM_dt IS NULL  
    AND 
    	DoubleOptInProcessed_dt IS NOT NULL
    AND
    	OptOut_dt IS NULL         
</cfquery>	
            
<!--- Run AM Process --->
<cfsavecontent variable="DetailDump">
	<cfoutput>          
    	
        <div style="padding:20px;">
        
            <h1>Fox Chase Cancer Center Skin Campaign Summary Statistics</h1>
            <b>#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#</b>
            <BR />
            <BR />
            Total Loaded in FCCC Skin Campaign (<b>#getCountInDB.TotalCount#</b>)
            <BR />
            <BR />
            Total Count Elligable to Send Am Message Now (<b>#getContacts.RecordCount#</b>)
            <BR />
            <BR />
            Total Count for AM (<b>#getCountInDBAM.TotalCount#</b>)	
            <BR />
            Total Count for AM Opt In (<b>#getCountInDBAMOptIn.TotalCount#</b>)	
            <BR />
            Total Count for AM Opt Out (<b>#getCountInDBAMOptOut.TotalCount#</b>)	
            <BR />
            Total Count for AM Completed (<b>#getCountInDBAMComplete.TotalCount#</b>)	
            <BR />
            <BR />
            Total Count for PM (<b>#getCountInDBPM.TotalCount#</b>)	
            <BR />
            Total Count for PM Opt In (<b>#getCountInDBPMOptIn.TotalCount#</b>)	
            <BR />
            Total Count for PM Opt Out (<b>#getCountInDBPMOptOut.TotalCount#</b>)	
            <BR />
            Total Count for PM Completed (<b>#getCountInDBPMComplete.TotalCount#</b>)	
            <BR />
            <BR />
            Total Count for BOTH (<b>#getCountInDBBOTH.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Opt In (<b>#getCountInDBBOTHOptIn.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Opt Out (<b>#getCountInDBBOTHOptOut.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Completed (<b>#getCountInDBBOTHComplete.TotalCount#</b>)	
            <BR />
            <BR />
           
            <HR />
            
            
            <h1>AM Load Results</h1>
            
            <BR/>
                        
            <cfif getContacts.RecordCount EQ 0>
                No Data Elligable for AM Messages
                <BR/>
            </cfif>  
        
            <cfloop query="getContacts">
            
                Contact String = #getContacts.ContactString_vch#
                <BR/>            
                Sequence String = #getContacts.AMSequenceString_vch#
                <BR/>
                
                <cfset NextProcessCountAM = getContacts.ProcessedCountAM_int + 1 />
                <cfset DailyMessageAM = "" />
                
                NextProcessCountAM = #NextProcessCountAM#
                <BR/>
                
                <!--- Squash errors and move on - Log an warn through email --->
                <cftry>
                
                
                    <!--- What day are we on --->
                    <cfif NextProcessCountAM GT 0 AND NextProcessCountAM LT 15 AND ListLen(getContacts.AMSequenceString_vch) GTE NextProcessCountAM>
                    
                        <cfif IsNumeric(ListGetAt(getContacts.AMSequenceString_vch, NextProcessCountAM)) AND ListGetAt(getContacts.AMSequenceString_vch, NextProcessCountAM) GT 0 >
                            
                            <cfset DailyMessageAM = getAMQuestions.Desc_vch[ListGetAt(getContacts.AMSequenceString_vch, NextProcessCountAM)] />
                    
                            AM Message Id = #ListGetAt(getContacts.AMSequenceString_vch, NextProcessCountAM)#
                            <BR/>
                
                        </cfif>
                        
                    </cfif>
            
                <cfcatch type="any" >
                	LOAD NEXT MESSAGE ERROR
                    <BR/>
                    <cfdump var="#cfcatch#" />            
                </cfcatch>
                
                </cftry>
                
                <!--- Update to Contact --->
                <cfquery name="UpdateContacts" datasource="#ProdDB#">
                    UPDATE
                        simplexprogramdata.fcccskin
                    SET
                        ProcessedCountAM_int = ProcessedCountAM_int + 1 ,
                        Updated_dt = NOW()
                        <cfif NextProcessCountAM GT 13>
                            , CompletedAM_dt = NOW()
                        </cfif>                
                    WHERE
                        FCCCSkinId_int = #getContacts.FCCCSkinId_int#        
                </cfquery>	
                
                Data Updated OK	
                <BR/>
            
            
                <cfif NextProcessCountAM GT 13>
                    AM Campaign Completed
                    <BR/>
                </cfif>
                
                <!--- Send Daily AM Message if any --->
                <cfif TRIM(DailyMessageAM) NEQ "">
                                        
                    AM Message = #DailyMessageAM#
                    <BR/>	
                    
                    
                    <!--- This is better/faster in separate process but this is low volume so do it here to be safe.--->
                    <!--- Check for Opt Out --->
                       
                       
					<!--- Get counts from DB --->
                    <cfquery name="CheckForRecentOptOut" datasource="#ProdDB#">
                        SELECT 
                        	COUNT(oi.ContactString_vch) AS TotalCount,
                            oi.OptOut_dt 
                        FROM 
                            simplelists.optinout AS oi 
                        WHERE 
                        	OptOut_dt IS NOT NULL 
                        AND
                        	OptIn_dt IS NULL 
                        AND 
                        	ShortCode_vch = '244687'
                        AND 
                        	oi.ContactString_vch = #getContacts.ContactString_vch#
                    </cfquery>	
                    
                    <cfif CheckForRecentOptOut.TotalCount EQ "0">
                    
						<cfif inpBlockSend EQ 0>
                        
                            <cftry>
                            
                                <cfset VerboseDebug = "0" />
                                <cfset inpBatchId = "#AMBatchId#" />
                                <cfset inpContactString= "#getContacts.ContactString_vch#" />
                                <cfset inpMessage= "#DailyMessageAM#" />
                
                                <!--- Do this once - separated for quicker looping where needed --->
                                <cfinclude template="authsetup.cfm" />
                                
                                <!--- Post to API --->
                                <cfinclude template="sendsinglesms.cfm" />
    
                            <cfcatch type="any" >
                                SEND MESSAGE ERROR
                                <BR/>
                                <cfdump var="#cfcatch#" />            
                            </cfcatch>
                            
                            </cftry>
                    
                        </cfif>
                    
                    
                    <cfelse>
                    
                    	<!--- Update to Contact - Opt Out --->
                        <cfquery name="UpdateContactsOptOut" datasource="#ProdDB#">
                            UPDATE
                                simplexprogramdata.fcccskin
                            SET
                                OptOut_dt = '#CheckForRecentOptOut.OptOut_dt#' ,
                                Updated_dt = NOW()                                               
                            WHERE
                                FCCCSkinId_int = #getContacts.FCCCSkinId_int#        
                        </cfquery>	
                        
                        CONTACT HAS OPTED OUT
                        <BR/>
                    
                    </cfif>
               
                </cfif>
        
                <hr/>	
                
            </cfloop>
	
    	</div>
        
	</cfoutput>        
    
</cfsavecontent>
    



<!--- Mail Report Daily --->

<cfoutput>
<!---
<div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">

<img src="https://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;"/>--->
    
<BR/>
#DetailDump#
<!---
</div>---> 

</cfoutput>


<cfmail to="jpeterson@messagebroadcast.com;pshah@messagebroadcast.com;swazzan@messagebroadcast.com" from="mbreports@messagebroadcast.com" subject="Fox Chase Cancer Center Skin AM Processing" type="html">
   	<cfoutput>
    
  <!---  <div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">
    
    	<img src="http://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;" />
        <BR/>
 --->  		#DetailDump#
        
    <!---</div>--->    
    </cfoutput>
</cfmail>
            
            




