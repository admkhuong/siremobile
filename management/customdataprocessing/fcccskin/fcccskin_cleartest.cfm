<!--- Remove all test data for given contact string --->

<cfparam name="inpContactString" default="" />
<cfparam name="inpFileName" default="" />
<cfparam name="inpShortCode" default="244687" />
<cfparam name="inpBatchId" default="1174" />

<cfset ProdDB = "Bishop" />

<cfsavecontent variable="DetailDump">
	<cfoutput>          
    
        
        <!--- Remove ContactFrom FCCC Skin DB--->
        <cfquery name="RemoveFromFCCCDB" datasource="#ProdDB#">
            DELETE
            FROM
                simplexprogramdata.fcccskin
            WHERE
                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">	
        </cfquery>	
        
        #inpContactString# Removed from FCCC Skin DB 
        <BR/>
        
        <!--- Remove ContactFrom FCCC Skin Survey Results --->
        <cfquery name="RemoveFromFCCCDBResults" datasource="#ProdDB#">
            DELETE                
            FROM 
                simplexresults.surveyresults
            WHERE
                 batchid_bi = 1174
            AND
                 ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
        </cfquery>	
                 
        #inpContactString# Removed from FCCC Skin PM Survey Results  
        <BR/>
                       
        <!--- Remove any opt outs on the 244687 short code --->   
        
        <cfquery name="OptInSMS_UpdateOldOptOuts" datasource="#ProdDB#" >                            
            UPDATE
                simplelists.optinout
            SET
                OptIn_dt = NOW()                                 
            WHERE
                ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
            AND 
                OptIn_dt IS NULL
            AND
                (
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">
                OR
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1#TRIM(inpContactString)#">
                )                                                         	            	    	
        </cfquery> 
        
        #inpContactString# Old Opt Outs (if any) updated to opt in 
        <BR/>                              
                                
        <!--- Update contact as opt out on this short code for any future SMS MT Campaigns - include check for this in any non-MO triggered MT by putting check in session/cfc/includes/loadqueuesms_dynamic.cfm --->
        <cfquery name="InsertSMSStopRequest_optinout" datasource="#ProdDB#">
            INSERT INTO 
                simplelists.optinout
                (
                    UserId_int,
                    ContactString_vch,
                    OptOut_dt,
                    OptIn_dt,
                    OptOutSource_vch,
                    OptInSource_vch,
                    ShortCode_vch,
                    FromAddress_vch,
                    CallerId_vch,
                    BatchId_bi
                )
            VALUES
                (                        
                    0, <!--- SMS Opt in is tied to short code - not user id - so shared short codes are S.O.L. --->
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpContactString)#">,
                    NULL,
                    NOW(),
                    NULL,
                    'MO Confirm Opt In',
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">,
                    NULL,
                    NULL,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#inpBatchId#">
                )
        </cfquery>                     
                                
                                
	</cfoutput>        
    
</cfsavecontent>
    



<!--- Mail Report Daily --->

<cfoutput>
<!---
<div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">

<img src="https://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;"/>--->
    
<BR/>
#DetailDump#
<!---
</div>---> 

</cfoutput>


<cfmail to="jpeterson@messagebroadcast.com" from="mbalerts@messagebroadcast.com" subject="Fox Chase Cancer Center Skin Clear Contact String Processing" type="html">
   	<cfoutput>
    
  <!---  <div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">
    
    	<img src="http://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;" />
        <BR/>
 --->  		#DetailDump#
        
    <!---</div>--->    
    </cfoutput>
</cfmail>
            
            
            