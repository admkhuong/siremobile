

<cfset StartPageTime = NOW()>
<cfoutput>

<cfset LocalProcessingDir = "C:\temp2">
<cfset FileNameNotPath = "" />

<!--- Directory is local to current webserver file path --->
<!--- BEWARE of local security read/write rights - will cause GET Failure --->
<cfset UploadDestinationPathWebServer = "c:/Websites/ebmmanagement/management/customdataprocessing/fcccskin/sftparchive">

<cfset SFTPHostName = "Sftp01.messagebroadcast.com">
<cfset SFTPUserName = "foxch">
<cfset SFTPPassword = "aB467@g1"> 
<cfset SFTPInDir = "/">
<cfset SFTPOutDir = "/output">  

<cfparam name="ProcessedRecordCount" default="0">
<cfparam name="SuppressedCount" default="0">
<cfparam name="DuplicateRecordCount" default="0">
<cfparam name="DistirbutionCount" default="0">
<cfparam name="DistirbutionCountRegular" default="0">
<cfparam name="DistirbutionCountSecurity" default="0">
<cfparam name="InvalidRecordCount" default="0">
<cfparam name="MaxInvalidRecords" default="1">
	   
<cfset FileNameSearchValue = "FCCCSKIN_" />	
	
<cftry>
	 	
     Start...
     <BR/>
     
     <!--- You need to connect with some other tool at least once to get the remote server's finger print and put it here in call to open--->
     <!--- from a mc/linux box you just go to command terminal and type: ssh SFTPHostName --->
     <cfftp action = "open"
      username = "#SFTPUserName#"
      connection = "CurrSFTPConn"
      password = "#SFTPPassword#"
      fingerprint = "0f:6b:f2:23:56:34:39:07:63:a4:82:fb:09:a2:ae:90"
      server = "#SFTPHostName#"
      secure = "yes"
      stoponerror="Yes">
      
      <cfdump var="#CurrSFTPConn#">
      
    <cfif cfftp.succeeded>
    
    	<cftry>
    
    
            <cfftp action="listDir"
                name="RemoteDirectoryContents"
                directory="#SFTPInDir#"
                timeout="300"
                connection = "CurrSFTPConn"
                stoponerror="Yes">
    
            <cfdump var="#RemoteDirectoryContents#">
            
            <cfloop query="RemoteDirectoryContents" >
            
            	<cfif RemoteDirectoryContents.ISDIRECTORY EQ FALSE AND FIND("#SFTPInDir##FileNameSearchValue#", RemoteDirectoryContents.PATH) GT 0> 
            		
                    <!--- File Found --->
                    File Pattern (#FileNameSearchValue#) Match Found  <BR/>
                    #RemoteDirectoryContents.PATH#  <BR/>
                    
                    <cfset FileNameNotPath = Replace(RemoteDirectoryContents.PATH, "#SFTPInDir#", "", "one") />
                    
                    FileNameNotPath = #FileNameNotPath# <BR />
                    
                    <!--- failIfExists="no" - Something stupid going on but will fail and lock file when transfering this way unless overwrite is allowed - even if file not there to begin with. --->
                    <cfftp action = "getFile"
                    connection = "CurrSFTPConn" 
                    transferMode = "ASCII" 
                    localFile = "#UploadDestinationPathWebServer#/#FileNameNotPath#.InProcess" 
                    remoteFile = "#RemoteDirectoryContents.PATH#"
                    failIfExists="no"
                    >

					File Downloaded OK  "#UploadDestinationPathWebServer##RemoteDirectoryContents.PATH#.InProcess" <BR />                    
                    
                    
                    
                    <cfftp action = "remove"
                    connection = "CurrSFTPConn"                    
                    item = "#RemoteDirectoryContents.PATH#" 
                    failIfExists="no"                  
                    >
                    
                    SFTP File Remove From Remote - Processed File Removed #RemoteDirectoryContents.PATH# <BR/>
                    
                    
                    <cfinclude template="fcccskin_processinvite.cfm" />
                    
            	<cfelse>
                	
                    <!--- File Found --->
                    File Does not Match Pattern #FileNameSearchValue#  <BR/>
                    #RemoteDirectoryContents.PATH#  <BR/>
                	
            	</cfif>
                
            </cfloop>
            
            <cfftp action="close" connection="CurrSFTPConn"/>


		<cfcatch type="any">
	        <cfdump var="#cfcatch#">
        	<cfftp action="close" connection="CurrSFTPConn"/>
        </cfcatch>
        
        </cftry>

	</cfif>

  
      
      <!---
		
	<!--- Gets a query object of the files --->
	<cfset dirarray = mysftp.dir()>
	<!--- <cfdump var="#dirarray#"> --->
	
	<cfloop query="dirarray" >
		
		<!--- Ignore directory and return file names - only files with the text VoiceAlert_ in them --->
		<cfif dirarray.filetype EQ "file" AND FIND("#FileNameSearchValue#", dirarray.filename) GT 0> 
			dirarray.filename = #dirarray.filename#<BR>
			
			
		<!---	<cfset CurrSFTPFileName = dirarray.filename>
			<cfset CurrSFTPFileNameProcessing = dirarray.filename & ".Processing">
				
			<!--- Copy file to working directory --->	
			<cfset mysftp.get(CurrSFTPFileName, CurrSFTPFileNameProcessing)> 
						
			<!--- now delete the remote file --->
			<cfset mysftp.del(CurrSFTPFileName)> 
			
			<!--- Process File --->
			
			<!--- Get file Sequence Number --->
			<cfset FileSeqBuff = Replace(CurrSFTPFileName, "#FileNameSearchValue#", "")>		
			<cfset FileSeqBuff = Replace(FileSeqBuff, ".txt", "")>			
			<cfset FileSeqNumber_int = FileSeqBuff>
			
            
            <!---  Log if any - validate not already processed --->
			<!---
			<cfquery name="LogFile" datasource="MBASPSQL2K">
				INSERT INTO
				  ClientProductionData..ChaseAlertFiles2007QA_log
				  (	
				  	LogType_int,							
					FileSeqNum_int,
					START_DT					
				  )
				VALUES
				  (
				  	1,
					#FileSeqNumber_int#,
					GETDATE()		  
				  )
			</cfquery>  
			--->
			
			<cfset FileToProcess = UploadDestinationPathWebServer & dirarray.filename & ".Processing">
			
			<!--- Process file --->
			<!---<cfinclude template="fcccskin_processinvite.cfm">--->
			
			
			<!--- Rename file as complete --->
			<cfset SourceFile = UploadDestinationPathWebServer & dirarray.filename & ".Processing">
			<cfset DestinationFile = UploadDestinationPathWebServer & dirarray.filename & ".Complete">
						
			<cffile action="rename" source="#SourceFile#" destination="#DestinationFile#">--->
									
			<cfset EndPageTime = NOW()>
			<cfset PageTime = datediff("s", StartPageTime,EndPageTime) >
	
    		<!--- Log processing results --->
			<!---
			<cfquery name="UpdateFileData" datasource="MBASPSQL2K">
				UPDATE
				  ClientProductionData..ChaseAlertFiles2007QA_log 
				SET
				 	CountDistributed_int = #DistirbutionCount#,
					CountDistributedRegular_int = #DistirbutionCountRegular#,
					CountDistributedSecurity_int = #DistirbutionCountSecurity#,
				    End_dt = GETDATE(),
				    CountProcessed_int = #ProcessedRecordCount#,
				    CountSuppressed_int = #SuppressedCount#,
				    CountDuplicate_int = #DuplicateRecordCount#,
					RunTimeSeconds_int = #PageTime#
				WHERE
				   FileSeqNum_int = #FileSeqNumber_int#									   
			</cfquery> 
			--->
			
            
            <!---
			<!--- Post result file --->
			<cfset CurrResponseFileName = "AlertsProcessed_" & FileSeqNumber_int & ".txt">

			<!--- Write file to temp location --->
			<cfset FileWrite_OutputBuffer = FileSeqNumber_int & ","  & "#LSDateFormat(Now(), 'yyyy-mm-dd')# " & "#LSTimeFormat(Now(), 'hh:mm:ss')#" & "," & ProcessedRecordCount & chr(13) & chr(10)>
			<cffile action="APPEND" file="#UploadDestinationPathWebServer##CurrResponseFileName#" output="#FileWrite_OutputBuffer#" attributes="Normal" addnewline="No">
			<cfset FileWrite_OutputBuffer = "">

			
			<!---<cfset mysftp.cd("#SFTPOutDir#")>
			
			<cfset mysftp.put("#CurrResponseFileName#","#CurrResponseFileName#")>--->
			--->		
			<!--- Only process one file - will start again in 5 min --->
			<cfbreak>
		</cfif>
		
		
	</cfloop> --->
		

	<cfset EndPageTime = NOW()>
	<cfset PageTime = datediff("s", StartPageTime, EndPageTime) >

	<!--- Log The Check for File DateTime --->
	<!---<cfquery name="LogFile" datasource="MBASPSQL2K">
		INSERT INTO
		  ClientProductionData..ChaseAlertFiles2007QA_log
		  (	
			LogType_int,							
			Check_dt,
			RunTimeSeconds_int					
		  )
		VALUES
		  (
			2,
			GETDATE(),
			#PageTime#		  
		  )
	</cfquery> 
	---> 
	
<cfcatch>

	<!--- Log and notify of SFTP error --->
	<cfset ENA_Message = "FCCC Skin SFTP File Processing Error">	
	<cfinclude template="../../noc/act_EscalationNotificationActionshighpriority.cfm">
    
    <cfdump var="#cfcatch#">

</cfcatch>

</cftry>				




</cfoutput>