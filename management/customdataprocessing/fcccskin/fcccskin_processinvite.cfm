<cfparam name="FileNameNotPath" default="">
<cfparam name="UploadDestinationPathWebServer" default="">
<cfparam name="WebLocalPathForFileRead" default="sftparchive">

<cfset ProdDB = "Bishop" />
<cfset DefaultTimeZone = 31 />


	<cfsavecontent variable="DetailDumpProcessInvite">
    
    
        <cfoutput>
    
			<cfset StartPageTimeProcessInvite = NOW()>
            
            <!--- Verify File has not already been processed --->
            <cfquery name="getFileStatus" datasource="#ProdDB#">
                SELECT 
                    COUNT(FCCCSkinId_int) AS TotalCount                	
                FROM 
                    simplexprogramdata.fcccskin
                WHERE
                    SFTPFileSource_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#FileNameNotPath#">
            </cfquery>		
            
            
            
            <!--- Process File --->
            <cfif getFileStatus.TotalCount EQ 0>
    
        
                File Name = #FileNameNotPath# <BR/>
                
                <HR/>
                       
                <!--- Read each line --->
                <cfloop index="line" file="#UploadDestinationPathWebServer#/#FileNameNotPath#.InProcess"> 
                
                    Line Read: #line#<br />
                    
                    <!--- Check if valid record --->
                    <cfif ListLen(line,"|",true) GT 1 >
                    
                        <cfset CurrProgram  = TRIM(ListGetAt(line,1, "|", true)) />
                        <cfset CurrContact  = TRIM(ListGetAt(line,2, "|", true)) />
                        
                        <cfif ListLen(line,"|",true) EQ 3 >
                            <cfset CurrSequence = TRIM(ListGetAt(line,3, "|", true)) />
                        <cfelse>
                          	 <cfset CurrSequence = "" />  
                        </cfif>
                        
                        <!--- Check if already in DB --->
                        
                        <!--- Verify File has not already been processed --->
                        <cfquery name="getContactStatus" datasource="#ProdDB#">
                            SELECT 
                                COUNT(FCCCSkinId_int) AS TotalCount                	
                            FROM 
                                simplexprogramdata.fcccskin
                            WHERE
                                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrContact#">
                        </cfquery>	
        
                        <cfif getContactStatus.TotalCount EQ 0 AND ListContainsNoCase("AM,PM,BOTH,CONTROL",CurrProgram) GT 0 >
                                        
                            <!--- Insert into DB --->
                            <cfquery name="InsertContactStatus" datasource="#ProdDB#">
                                INSERT INTO 
                                    simplexprogramdata.fcccskin
                                    (
                                        ProcessedCountAM_int,
                                        ProcessedCountPM_int,
                                        TimeZone_int,
                                        Created_dt,
                                        Updated_dt,
                                        Invite_dt,
                                        CompletedAM_dt,
                                        CompletedPM_dt,
                                        DoubleOptInProcessed_dt,
                                        Program_vch,
                                        ContactString_vch,
                                        AMSequenceString_vch,
                                        SFTPFileSource_vch,
                                        OptOut_dt
                                    
                                    )
                                    VALUES
                                    (
                                        0,
                                        0,
                                        #DefaultTimeZone#,
                                        NOW(),
                                        NOW(),
                                        NOW(),
                                        NULL,
                                        NULL,
                                        NULL,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrProgram#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrContact#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CurrSequence#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#FileNameNotPath#">,
                                        NULL                                
                                    )
                            </cfquery>	
                            
                        
                            <!--- Send invite --->
                            <cfset inpContactString = CurrContact />
                            <cfset VerboseDebug = "0" />
                            <cfinclude template="fcccskin_sendinvite.cfm"/>
                        
                        <cfelse>
                            
                            #CurrContact# is already in program - ignored. <BR/>
                        
                        </cfif>
                        
                    <cfelse>
                    		
                        Invalid Record Found! <BR/>    
                        
                    </cfif>
                    
                    
                    
                    <HR/>
             
                </cfloop>
                
               

			
            <cfelse>
            	
                <h2>WARNING:</h2> #FileNameNotPath# has already been processed!	
		
    		</cfif>
            
               
			<cfset EndPageTimeProcessInvite = NOW()>
            <cfset PageTimeProcessInvite = datediff("s", StartPageTimeProcessInvite, EndPageTimeProcessInvite) >
    
            Process Run Time in Seconds: #PageTimeProcessInvite# <BR/>
                         
        </cfoutput>
    
    </cfsavecontent>
    
    
    
<!--- Mail Report Daily --->

<cfoutput>
<!---
<div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">

<img src="https://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;"/>--->
    
<BR/>
#DetailDumpProcessInvite#
<!---
</div>---> 

</cfoutput>


<cfmail to="jpeterson@messagebroadcast.com;pshah@messagebroadcast.com;swazzan@messagebroadcast.com" from="mbreports@messagebroadcast.com" subject="Fox Chase Cancer Center Skin Invite Processing" type="html">
   	<cfoutput>
    
  <!---  <div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">
    
    	<img src="http://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;" />
        <BR/>
 --->  		#DetailDumpProcessInvite#
        
    <!---</div>--->    
    </cfoutput>
</cfmail>
            




