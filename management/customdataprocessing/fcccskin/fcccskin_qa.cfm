<cfinclude template="../../../session/display/default/dsp_header.cfm">


<cfset ProdDB = "Bishop" />

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Fox Chase Cancer Center - Skin QA</title>


<cfparam name="inpContactString" default="" />
<cfparam name="inpAMQID" default="1" />
 

<style>
	body
	{
		font:Verdana, Geneva, sans-serif;	
		margin:50px;		
	}

	.SummarryData
	{
		font-weight:700;
		font-size:12px;
		color:#666;	
	}
	
	.web_dialog2 {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 2px solid #CECECE;
    border-radius: 5px;
    display: none;
    font-family: "Verdana",geneva,sans-serif;
    font-size: 13px;
    height: auto;
    left: 719.5px;
    padding-bottom: 25px;
    position: fixed;
    top: 170px;
    width: auto;
    z-index: 102;
}

	
	.table {
		display:table;
	}
	.header {
		display:table-header-group;
		font-weight:bold;
	}
	.rowGroup {
		display:table-row-group;
	}
	.row {
		display:table-row;
	}
	.cell {
		display:table-cell;		
	}


	fieldset { border:3px solid #999999 }

	legend 
	{
	  padding: 0.2em 0.5em;
	  border:none;
	  color:#333333;
	  font-size:14px;
	  text-align:right;
	}

	.label-a
	{
		font-weight:bold;
		margin-top: 10px;
		display:block;
	}
	
	.StatusInfo
	{
		border:#999 1px solid; 
		padding: 3px 10px 3px 10px;
		min-height:19px;
		min-width: 200px;
	}


</style>


<!--- Queries for meta-data --->
<cfquery name="getAMQuestions" datasource="#ProdDB#">
    SELECT 
        TipId_int,
        Desc_vch,
        Class_vch
    FROM 
        simplexprogramdata.fcccskin_am_tips	
    ORDER BY
        TipId_int ASC			
</cfquery>	
        

<script type="text/javascript">

	<!--- Init function --->
	$(function() 
	{	
	
	
	
	});
	
	
	function GetContactStringsStatus()
	{		
		<!--- Read data in from current user --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: 'fcccskin.cfc?method=GetCurrentUserStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { inpContactString : $('#inpContactString').val()},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					try
					{
						if(d.RXRESULTCODE != "1")
						{
							jAlert('Cannot find user information.', 'Warning');
							
							$('#ContactSummaryData #CONTACTSTRING').val('');
							$('#ContactSummaryData #FCCCSKINID').val('');
							$('#ContactSummaryData #PROCESSEDCOUNTAM').val('');
							$('#ContactSummaryData #PROCESSEDCOUNTPM').val('');
							$('#ContactSummaryData #TIMEZONE').val('');
							$('#ContactSummaryData #CREATED').val('');
							$('#ContactSummaryData #INVITE').val('');
							$('#ContactSummaryData #COMPLETEDAM').val('');
							$('#ContactSummaryData #COMPLETEDPM').val('');
							$('#ContactSummaryData #DOUBLEOPTINPROCESSED').val('');
							$('#ContactSummaryData #OPTOUT').val('');
							$('#ContactSummaryData #PROGRAM').val('');
							$('#ContactSummaryData #AMSEQUENCESTRING').val('');
							$('#ContactSummaryData #SFTPFILESOURCE').val('');		
							$('#ContactSummaryData #UPDATED').val('');	
							$('#ContactSummaryData #SEVENDAY').val('');
							$('#ContactSummaryData #FOURTEENDAY').val('');	
							
							return false;
						}
						else
						{														
							$('#ContactSummaryData #CONTACTSTRING').val(d.CONTACTSTRING);
							$('#ContactSummaryData #FCCCSKINID').val(d.FCCCSKINID);
							$('#ContactSummaryData #PROCESSEDCOUNTAM').val(d.PROCESSEDCOUNTAM);
							$('#ContactSummaryData #PROCESSEDCOUNTPM').val(d.PROCESSEDCOUNTPM);
							$('#ContactSummaryData #TIMEZONE').val(d.TIMEZONE);
							$('#ContactSummaryData #CREATED').val(d.CREATED);
							$('#ContactSummaryData #INVITE').val(d.INVITE);
							$('#ContactSummaryData #COMPLETEDAM').val(d.COMPLETEDAM);
							$('#ContactSummaryData #COMPLETEDPM').val(d.COMPLETEDPM);
							$('#ContactSummaryData #DOUBLEOPTINPROCESSED').val(d.DOUBLEOPTINPROCESSED);
							$('#ContactSummaryData #OPTOUT').val(d.OPTOUT);
							$('#ContactSummaryData #PROGRAM').val(d.PROGRAM);
							$('#ContactSummaryData #AMSEQUENCESTRING').val(d.AMSEQUENCESTRING);
							$('#ContactSummaryData #SFTPFILESOURCE').val(d.SFTPFILESOURCE);		
							$('#ContactSummaryData #UPDATED').val(d.UPDATED);
							$('#ContactSummaryData #SEVENDAY').val(d.SEVENDAY);
							$('#ContactSummaryData #FOURTEENDAY').val(d.FOURTEENDAY);													
						}						
					}
					catch(exception)
					{
						return false;
					}
				} 		
				
			});
	}	
	
	function UpdateContactStringsStatus()
	{		
		<!--- Read data in from current user --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: 'fcccskin.cfc?method=UpdateCurrentUserStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
					FCCCSkinId_int : $('#ContactSummaryData #FCCCSKINID').val(),
                    ProcessedCountAM_int : $('#ContactSummaryData #PROCESSEDCOUNTAM').val(),
                    ProcessedCountPM_int : $('#ContactSummaryData #PROCESSEDCOUNTPM').val(),
                    TimeZone_int : $('#ContactSummaryData #TIMEZONE').val(),
                    Created_dt : $('#ContactSummaryData #CREATED').val(),
                    Updated_dt : $('#ContactSummaryData #UPDATED').val(),
                    Invite_dt : $('#ContactSummaryData #INVITE').val(),
                    CompletedAM_dt : $('#ContactSummaryData #COMPLETEDAM').val(),
                    CompletedPM_dt : $('#ContactSummaryData #COMPLETEDPM').val(),
                    DoubleOptInProcessed_dt : $('#ContactSummaryData #DOUBLEOPTINPROCESSED').val(),
                    Program_vch : $('#ContactSummaryData #PROGRAM').val(),
                    ContactString_vch : $('#ContactSummaryData #CONTACTSTRING').val(),
                    AMSequenceString_vch : $('#ContactSummaryData #AMSEQUENCESTRING').val(),
                    SFTPFileSource_vch : $('#ContactSummaryData #SFTPFILESOURCE').val()	,
                    SevenDaySummarySent_int : $('#ContactSummaryData #SEVENDAY').val(),	
                    FourteenDaySummarySent_int : $('#ContactSummaryData #FOURTEENDAY').val(),
                    OptOut_dt : $('#ContactSummaryData #OPTOUT').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					try
					{
						GetContactStringsStatus();
							
					}
					catch(exception)
					{
						return false;
					}
				} 		
				
			});
	}	
	
	
	var $PushAMQuestionDialog = null;
	
	<!--- Resuse add box ???? --->
	function PushAMQuestion(inpParent)
	{		
		<!--- Erase any existing dialog data --->
		if($PushAMQuestionDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$PushAMQuestionDialog.remove();
			$PushAMQuestionDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpContactString=' + $("#inpContactString").val() + '&inpMessage=' + encodeURIComponent($("#inpAMQIDText").val()) + '&VerboseDebug=1' ;

		$PushAMQuestionDialog = $('<div></div>').append($loading.clone());
			
		$PushAMQuestionDialog
			.load('fcccsendam.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $PushAMQuestionDialog.remove(); $PushAMQuestionDialog = null; GetContactStringsStatus();}, 
			title: 'Push AM Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$PushAMQuestionDialog.dialog('option', 'position', [x,y]);				
		$PushAMQuestionDialog.dialog('open');
	}	
	
	var $PushPMQuestionDialog = null;
	
	<!--- Resuse add box ???? --->
	function PushPMQuestion(inpParent)
	{		
		<!--- Erase any existing dialog data --->
		if($PushPMQuestionDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$PushPMQuestionDialog.remove();
			$PushPMQuestionDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpContactString=' + $("#inpContactString").val() + '&inpMessage=' + encodeURIComponent($("#inpAMQIDText").val()) + '&VerboseDebug=1' ;

		$PushPMQuestionDialog = $('<div></div>').append($loading.clone());
			
		$PushPMQuestionDialog
			.load('fcccsendpm.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $PushPMQuestionDialog.remove(); $PushPMQuestionDialog = null; GetContactStringsStatus();}, 
			title: 'Push PM Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$PushPMQuestionDialog.dialog('option', 'position', [x,y]);				
		$PushPMQuestionDialog.dialog('open');
	}	
	
	
	var $PushAMProcessingDialog = null;
	
	<!--- Resuse add box ???? --->
	function PushAMProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($PushAMProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$PushAMProcessingDialog.remove();
			$PushAMProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$PushAMProcessingDialog = $('<div></div>').append($loading.clone());
			
		$PushAMProcessingDialog
			.load('fcccskin_am.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $PushAMProcessingDialog.remove(); $PushAMProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Push AM Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$PushAMProcessingDialog.dialog('option', 'position', [x,y]);				
		$PushAMProcessingDialog.dialog('open');
	}		
	
	var $PushPMProcessingDialog = null;
	
	<!--- Resuse add box ???? --->
	function PushPMProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($PushPMProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open sPMe dialog --->
			$PushPMProcessingDialog.remove();
			$PushPMProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$PushPMProcessingDialog = $('<div></div>').append($loading.clone());
			
		$PushPMProcessingDialog
			.load('fcccskin_PM.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $PushPMProcessingDialog.remove(); $PushPMProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Push PM Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$PushPMProcessingDialog.dialog('option', 'position', [x,y]);				
		$PushPMProcessingDialog.dialog('open');
	}		
	
	
	var $PushAMOptInDialog = null;
	
	<!--- Resuse add box ???? --->
	function PushOptInProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($PushAMOptInDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$PushAMOptInDialog.remove();
			$PushAMOptInDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpSkipLocalUserDNCCheck=1&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$PushAMOptInDialog = $('<div></div>').append($loading.clone());
			
		$PushAMOptInDialog
			.load('fcccskin_sendinvite.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $PushAMOptInDialog.remove(); $PushAMOptInDialog = null; GetContactStringsStatus();}, 
			title: 'Push AM Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$PushAMOptInDialog.dialog('option', 'position', [x,y]);				
		$PushAMOptInDialog.dialog('open');
	}		
		
	
	var $Summary7ProcessingDialog = null;
		
	function Summary7Push(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($Summary7ProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$Summary7ProcessingDialog.remove();
			$Summary7ProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$Summary7ProcessingDialog = $('<div></div>').append($loading.clone());
			
		$Summary7ProcessingDialog
			.load('fcccskin_summary7.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $Summary7ProcessingDialog.remove(); $Summary7ProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Push Summary 7 Day Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$Summary7ProcessingDialog.dialog('option', 'position', [x,y]);				
		$Summary7ProcessingDialog.dialog('open');
	}		
	
	
	var $Summary14ProcessingDialog = null;
		
	function Summary14Push(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($Summary14ProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$Summary14ProcessingDialog.remove();
			$Summary14ProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$Summary14ProcessingDialog = $('<div></div>').append($loading.clone());
			
		$Summary14ProcessingDialog
			.load('fcccskin_summary14.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $Summary14ProcessingDialog.remove(); $Summary14ProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Push Summary 14 Day Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$Summary14ProcessingDialog.dialog('option', 'position', [x,y]);				
		$Summary14ProcessingDialog.dialog('open');
	}		
	
	var $DoubleOptInProcessingDialog = null;
		
	function DoubleOptInProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($DoubleOptInProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$DoubleOptInProcessingDialog.remove();
			$DoubleOptInProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$DoubleOptInProcessingDialog = $('<div></div>').append($loading.clone());
			
		$DoubleOptInProcessingDialog
			.load('fcccskin_2optin.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $DoubleOptInProcessingDialog.remove(); $DoubleOptInProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Push Summary 14 Day Message',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$DoubleOptInProcessingDialog.dialog('option', 'position', [x,y]);				
		$DoubleOptInProcessingDialog.dialog('open');
	}			
	
	var $ClearProcessingDialog = null;
		
	function ClearProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($ClearProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$ClearProcessingDialog.remove();
			$ClearProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$ClearProcessingDialog = $('<div></div>').append($loading.clone());
			
		$ClearProcessingDialog
			.load('fcccskin_cleartest.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $ClearProcessingDialog.remove(); $ClearProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'Clear Testing Data',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$ClearProcessingDialog.dialog('option', 'position', [x,y]);				
		$ClearProcessingDialog.dialog('open');
	}		
	
	var $SFTPProcessingDialog = null;
		
	function SFTPProcess(inpParent)
	{		
	
		if($("#inpContactString").val() == "")
		{
			jAlert('Contact String can not be blank for one off testing.', 'Warning');
			return false;
		}
	
		<!--- Erase any existing dialog data --->
		if($SFTPProcessingDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$SFTPProcessingDialog.remove();
			$SFTPProcessingDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&inpContactString=' + $("#inpContactString").val() + '&VerboseDebug=1' ;

		$SFTPProcessingDialog = $('<div></div>').append($loading.clone());
			
		$SFTPProcessingDialog
			.load('fcccskin_invite_sftp_check.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $SFTPProcessingDialog.remove(); $SFTPProcessingDialog = null; GetContactStringsStatus();}, 
			title: 'SFTP Testing Data',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$SFTPProcessingDialog.dialog('option', 'position', [x,y]);				
		$SFTPProcessingDialog.dialog('open');
	}			
	
	
	var $ReportOptInsDialog = null;
		
	function ReportOptIns(inpParent)
	{		
		<!--- Erase any existing dialog data --->
		if($ReportOptInsDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$ReportOptInsDialog.remove();
			$ReportOptInsDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&VerboseDebug=1' ;

		$ReportOptInsDialog = $('<div></div>').append($loading.clone());
			
		$ReportOptInsDialog
			.load('fcccskin_report_optins.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $ReportOptInsDialog.remove(); $ReportOptInsDialog = null; GetContactStringsStatus();}, 
			title: 'Opt In Report Data',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$ReportOptInsDialog.dialog('option', 'position', [x,y]);				
		$ReportOptInsDialog.dialog('open');
	}		
	
	
	var $ReportOptOutDialog = null;
		
	function ReportOptOuts(inpParent)
	{		
		<!--- Erase any existing dialog data --->
		if($ReportOptOutDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$ReportOptOutDialog.remove();
			$ReportOptOutDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&VerboseDebug=1' ;

		$ReportOptOutDialog = $('<div></div>').append($loading.clone());
			
		$ReportOptOutDialog
			.load('fcccskin_report_optouts.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $ReportOptOutDialog.remove(); $ReportOptOutDialog = null; GetContactStringsStatus();}, 
			title: 'Opt Out Report Data',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$ReportOptOutDialog.dialog('option', 'position', [x,y]);				
		$ReportOptOutDialog.dialog('open');
	}	
	
	
	var $ReportDetailsDialog = null;
		
	function ReportDetails(inpParent)
	{		
		<!--- Erase any existing dialog data --->
		if($ReportDetailsDialog != null)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			$ReportDetailsDialog.remove();
			$ReportDetailsDialog = null;
		}			
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		var CurrCPGID = 0;
			
		var ParamStr = '';
	
	    <!--- encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>)  --->
		ParamStr = '?inpBlockSend=' + $("#inpBlockSend").is(":checked") + '&VerboseDebug=1' ;

		$ReportDetailsDialog = $('<div></div>').append($loading.clone());
			
		$ReportDetailsDialog
			.load('fcccskin_report_results.cfm' + ParamStr)
			.dialog({
			modal : true,
			close: function() { $ReportDetailsDialog.remove(); $ReportDetailsDialog = null; GetContactStringsStatus();}, 
			title: 'Opt Out Report Data',
			width: 900,
			resizable: false,
			height: 'auto',
			position: 'top',
			draggable: false,
			autoOpen: false,				
			beforeClose: function(event, ui) { 	}
		}).parent().draggable();
		
		<!--- Tie this dialog to the bottom of the object that opend it.--->
		var x = $(inpParent).position().left; 
		var y = $(inpParent).position().top + 30 - $(document).scrollTop();
		$ReportDetailsDialog.dialog('option', 'position', [x,y]);				
		$ReportDetailsDialog.dialog('open');
	}	
	
		
	
	
	
	
</script>
       

</head>

<cfoutput>
    <body>
    
        <h1>Fox Chase Cancer Center - Skin QA Tools</h1>
        
        <HR />
        
        <div style="margin:10px;">
        
            <label class="label-a">Contact String</label>
            <input name="inpContactString" id="inpContactString" value="#inpContactString#">
        </div>
            
        <div id="ProgramSummaryData">
        
        </div>
        
        <fieldset style="width:500px; padding:10px;">
        	
            <legend>ContactString Status Information</legend>      
            
            <div id="ContactSummaryData">
                
                <div style="float:left;">
                    <label class="label-a">Contact String</label>
                    <input type="text" id="CONTACTSTRING" class="StatusInfo">
                    
                    <label class="label-a">FCCC Skin DB Id</label>
                    <input type="text" id="FCCCSKINID" class="StatusInfo">
                    
                    <label class="label-a">Processed Count AM</label>
                    <input type="text" id="PROCESSEDCOUNTAM" class="StatusInfo">
                    
                    <label class="label-a">Processed Count PM</label>
                    <input type="text" id="PROCESSEDCOUNTPM" class="StatusInfo">
                    
                    <label class="label-a">Time Zone</label>
                    <input type="text" id="TIMEZONE" class="StatusInfo">
                    
                    <label class="label-a">Created Date</label>
                    <input type="text" id="CREATED" class="StatusInfo">
                    
                    <label class="label-a">Invited</label>
                    <input type="text" id="INVITE" class="StatusInfo">
                    
                    <label class="label-a">SFTP File Sourc</label>
                    <input type="text" id="SFTPFILESOURCE" class="StatusInfo">
                </div>
                <div style="float:right" >
                    <label class="label-a">Completed AM Date</label>
                    <input type="text" id="COMPLETEDAM" class="StatusInfo">
                    
                    <label class="label-a">Completed PM Date</label>
                    <input type="text" id="COMPLETEDPM" class="StatusInfo">
                    
                    <label class="label-a">Program Assigned</label>
                    <input type="text" id="PROGRAM" class="StatusInfo">
                    
                    <label class="label-a">Double Opted In Date</label>
                    <input type="text" id="DOUBLEOPTINPROCESSED" class="StatusInfo">
                    
                    <label class="label-a">Opt Out Date</label>
                    <input type="text" id="OPTOUT" class="StatusInfo">
                    
                    <label class="label-a">AM Sequence String</label>
                    <input type="text" id="AMSEQUENCESTRING" class="StatusInfo">
                    
                    <label class="label-a">Last Updated</label>
                    <input type="text" id="UPDATED" class="StatusInfo">
                    
                    <label class="label-a">7 Day Summary Sent</label>
                    <input type="text" id="SEVENDAY" class="StatusInfo">
                    
                    <label class="label-a">14 Day Summary Sent</label>
                    <input type="text" id="FOURTEENDAY" class="StatusInfo">
                
                </div>
                
                <div style="clear:both"></div>
                
                <BR />
                <hr />
                <BR />
                
                <button onClick="GetContactStringsStatus();">Get Contact String's Data</button>
                <button onClick="UpdateContactStringsStatus();">Update Contact String's Data</button>
                
            </div>
         </fieldset>
        
        	
                    
        <BR />
        
        <!--- One off Testing Section --->
        <fieldset style="width:900px; padding:10px;">
        	
            <legend>One off Testing Section</legend>              
            <div>
                <select id="inpAMQIDText" name="inpAMQIDText">
                    <cfloop query="getAMQuestions">
                                   
                        <cfif inpAMQID EQ getAMQuestions.TipId_int>
                            <option selected="selected" value="#getAMQuestions.Desc_vch#">(#getAMQuestions.TipId_int#) #getAMQuestions.Desc_vch#</option>
                        <cfelse>
                            <option value="#getAMQuestions.Desc_vch#">(#getAMQuestions.TipId_int#) #getAMQuestions.Desc_vch#</option>	
                        </cfif>
                                   
                    </cfloop>
                </select>
            </div>
            
            <br/>
            This will send the AM Alert Selected above to the Contact String at top of the page
            <BR />
            <button onClick="PushAMQuestion(this);">Push AM Alert by Id</button>
         
         	<BR />
            <BR />
           
			 This will just send the PM question to the Contact String at top of the page
            <BR /> 
            <button onClick="PushPMQuestion(this);">Push PM Questions</button>
                     
        </fieldset>
       
       
       <BR />
       
        <!--- One off Testing Section --->
        <fieldset style="width:900px; padding:10px;">
        	
            <legend>One off Testing Opt In Section</legend>              
            <BR />
                     
          <button onClick="PushOptInProcess(this);">Invite Opt In For Contact String - will send message to device always</button>
                        
            <BR />
            
        </fieldset>
       
       
       <BR />
       
       
       <!--- One off Testing Section --->
       <fieldset style="width:900px; padding:10px;">
        	
            <legend>Production Testing Section </legend>              
            
            <div>
            	<label class="label-a" style="display:inline">Check this box to block physical send:</label>
                <input id="inpBlockSend" name="inpBlockSend" type="checkbox" value="1" checked="checked" style="display:inline" />
          
         		<BR/>
                
                <button onClick="SFTPProcess(this);">Load File from SFTP</button>
                          
                <BR/>
                
                <button onClick="ClearProcess(this);">Clear FCCC Data for Contact String</button>
             
                <BR/>
                
                <button onClick="PushAMProcess(this);">Process AM Message For Contact String</button>
             
                <BR/>
                
                <button onClick="PushPMProcess(this);">Process PM Question For Contact String</button>
                
                <BR/>
                
                <button onClick="DoubleOptInProcess(this);">Process Double Opt Ins</button>
                
                <BR/>
                
                <button onClick="Summary7Push(this);">Process 7 Day Summary For Contact String</button>
                
                <BR/>
                
                <button onClick="Summary14Push(this);">Process 14 Day Summary For Contact String</button>
          
            </div>
      
        </fieldset>
        
        
        <!--- One off Testing Section --->
       <fieldset style="width:900px; padding:10px;">
        	
            <legend>Meta Data for Program </legend>              
            
            <div>
            	<button onClick="ShowDialog('AMQs');">List AM Questions</button>
            </div>
      
        </fieldset>
       
       
          <!--- One off Testing Section --->
       <fieldset style="width:900px; padding:10px;">
        	
            <legend>Reporting Buttons </legend>              
            
            <div>
            	<button onClick="ReportOptIns(this)">List Opt Ins</button>
            </div>
            
            <div>
            	<button onClick="ReportOptOuts(this)">List Opt Outs</button>
            </div>
            
            <div>
            	<button onClick="ReportDetails(this);">Final Results</button>
            </div>
      
        </fieldset>
       
       
        
        
         
        
        <!---
        
            Get User Stats
                
            Force Next Day
        
		
			<!--- Get counts per day Opt In - Send for Each Batch --->
			
			<!--- Export raw data option --->
        
        --->
    
    
<!--- Change email subaccount user information --->
<div id="dialog_userlimits" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Edit eMail Target Info</span></strong></label><span id="closeDialog" onClick="CloseDialog('emailsu');">Close</span></div>
	<form action="" method="POST" id="form_userlimits">
	
    	<input type="hidden" id="inpUserId" name="inpUserId" >
		<div class="dialog_content">
			<label><strong>Max amount allowed to send per EMS</strong></label>
			<input type="text" name="inpMaxEMSPerSend" id="inpMaxEMSPerSend" />
		</div>
        <div class="dialog_content">
			<label><strong>Max Length of audio recording for EMS</strong></label>
			<input type="text" name="inpMaxEMSAudioFileLength" id="inpMaxEMSAudioFileLength" />
		</div>
                      
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateUserLimits(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('userlimits');"	
			>Close</button>
		</div>
	</form>
</div>

    
    
<!--- Show List of AM Questions  --->
<div id="dialog_AMQs" class="web_dialog2" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Am Questions</span></strong></label><span id="closeDialog" onClick="CloseDialog('AMQs');">Close</span></div>
	    	        
        <div style="max-height:400px; overflow:scroll;">
            <div class="table" style="margin:10px; border-collapse: collapse; border-spacing:5px;">
                <cfloop query="getAMQuestions">
                    <div class="row" style="border:##999 3px solid;">
                        <span class="cell" style="border:##999 1px solid; width:30px; margin:5px; font-weight:700; text-align:center;">#getAMQuestions.TipId_int#</span>
                        <span class="cell" style="border:##999 1px solid; width:600px; padding:5px;overflow:hidden;">#getAMQuestions.Desc_vch#</span>
                        <span class="cell" style="border:##999 1px solid; width:300px; padding:5px;overflow:hidden;">#getAMQuestions.Class_vch#</span>            
                    </div>
                </cfloop>
            </div>
        </div>    
    
    
</div>




    
    </body>
</cfoutput>

</html>