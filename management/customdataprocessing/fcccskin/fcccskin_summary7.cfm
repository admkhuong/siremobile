<cfparam name="inpContactString" default="" />
<cfparam name="inpBlockSend" default="0" />

<cfset ProdDB = "Bishop" />


<!--- Change this to production Batch Id - Make sure it is elligable for real time --->
<cfset SummaryBatchId = "1175">


<!--- Queries for meta-data --->

<!--- Read Message from DB --->	
<cfquery name="getAMQuestions" datasource="#ProdDB#">
    SELECT 
        TipId_int,
        Desc_vch,
        Class_vch
    FROM 
        simplexprogramdata.fcccskin_am_tips	
    ORDER BY
        TipId_int ASC			
</cfquery>	



<!--- Get counts from DB --->
<cfquery name="getCountInDB" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBAM" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPM" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'    
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTH" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'    
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBAMComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
    AND
 		CompletedAM_dt IS NOT NULL  
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPMComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'  
    AND
 		CompletedPM_dt IS NOT NULL   
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTHComplete" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'
    AND
 		CompletedAM_dt IS NOT NULL 
    AND
        CompletedPM_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBAMOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'AM'    
    AND
 		DoubleOptInProcessed_dt IS NOT NULL  
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBPMOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'PM'  
    AND
 		DoubleOptInProcessed_dt IS NOT NULL   
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBBOTHOptIn" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
    	Program_vch = 'BOTH'
    AND
 		DoubleOptInProcessed_dt IS NOT NULL     
</cfquery>	

<!--- Get counts from DB --->
<cfquery name="getCountInDBAMOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'AM'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBPMOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'PM'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	


<!--- Get counts from DB --->
<cfquery name="getCountInDBBothOptOut" datasource="#ProdDB#">
    SELECT 
       Count(FCCCSkinId_int) AS TotalCount
    FROM 
        simplexprogramdata.fcccskin        
    WHERE
	    Program_vch = 'BOTH'
    AND
    	OptOut_dt IS NOT NULL     
</cfquery>	



<!--- API Public/Private Keys--->


<!--- Get elligable Contacts from DB --->
<cfquery name="getContacts" datasource="#ProdDB#">
    SELECT 
        FCCCSkinId_int,
        ContactString_vch,
        ProcessedCountAM_int,
        ProcessedCountPM_int,
        TimeZone_int,
        Created_dt,
        Updated_dt,
        Invite_dt,
        CompletedAM_dt,
        CompletedPM_dt,
        DoubleOptInProcessed_dt,
        Program_vch,
        ContactString_vch,
        AMSequenceString_vch,
        SFTPFileSource_vch
    FROM 
        simplexprogramdata.fcccskin
    WHERE
		<cfif TRIM(inpContactString) NEQ "">
                ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpContactString#">
            AND
        </cfif>
    	Program_vch IN ('PM','BOTH')    
    AND 
    	DoubleOptInProcessed_dt IS NOT NULL
    AND
    	OptOut_dt IS NULL 
    AND
    	7DaySummarySent_int = 0
    AND
    	ProcessedCountPM_int > 6                
</cfquery>	
            
<!--- Run AM Process --->
<cfsavecontent variable="DetailDump">
	<cfoutput>          
    	
        <div style="padding:20px;">
        
            <h1>Fox Chase Cancer Center Skin Campaign Summary Statistics</h1>
            <b>#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#</b>
            <BR />
            <BR />
            Total Loaded in FCCC Skin Campaign (<b>#getCountInDB.TotalCount#</b>)
            <BR />
            <BR />
            Total Count Elligable to Send 7 Day Summary Message Now (<b>#getContacts.RecordCount#</b>)
            <BR />
            <BR />
            Total Count for AM (<b>#getCountInDBAM.TotalCount#</b>)	
            <BR />
            Total Count for AM Opt In (<b>#getCountInDBAMOptIn.TotalCount#</b>)	
            <BR />
            Total Count for AM Opt Out (<b>#getCountInDBAMOptOut.TotalCount#</b>)	
            <BR />
            Total Count for AM Completed (<b>#getCountInDBAMComplete.TotalCount#</b>)	
            <BR />
            <BR />
            Total Count for PM (<b>#getCountInDBPM.TotalCount#</b>)	
            <BR />
            Total Count for PM Opt In (<b>#getCountInDBPMOptIn.TotalCount#</b>)	
            <BR />
            Total Count for PM Opt Out (<b>#getCountInDBPMOptOut.TotalCount#</b>)	
            <BR />
            Total Count for PM Completed (<b>#getCountInDBPMComplete.TotalCount#</b>)	
            <BR />
            <BR />
            Total Count for BOTH (<b>#getCountInDBBOTH.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Opt In (<b>#getCountInDBBOTHOptIn.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Opt Out (<b>#getCountInDBBOTHOptOut.TotalCount#</b>)	
            <BR />
            Total Count for BOTH Completed (<b>#getCountInDBBOTHComplete.TotalCount#</b>)	
            <BR />
            <BR />
           
            <HR />
            
            
            <h1>7 Day Summary Load Results</h1>
            
            <BR/>
                        
            <cfif getContacts.RecordCount EQ 0>
                No Data Elligable for 7 Day Summary Messages
                <BR/>
            </cfif>  
        
            <cfloop query="getContacts">
            
                Contact String = #getContacts.ContactString_vch#
                <BR/>            
                Sequence String = N/A
                <BR/>
                
                <cfset Summary7DayMessage = "" />
                
               <BR/>
                
                <!--- Squash errors and move on - Log an warn through email --->
                <cftry>
                
                
                    <!--- What day are we on --->                            
					<cfset Summary7DayMessage = "1175" />
            
                    7 Day Summary Message Id = 1175
                    <BR/>
                
                
                <!---				
					
					Day 7: If # days of sun protection = 7
					Day 14: If # days of sun protection > 10
					133 characters
						FCCC Alert: Out of X days, you said you tanned on X days and used sun protection X days. Great job on protecting your skin everyday! 
								
					
					Day 7: If # days of sun protection is 4-6 
					Day 14: If # days of sun protection is 5-10
					138 characters
						FCCC Alert: Out of X days, you said you tanned on X days and used sun protection X days. Good job, but try to protect your skin everyday! 
									
					Day 7: If # days of sun protection is < 4 
					Day 14: If # days of sun protection is < 5
					138 characters
						FCCC Alert: Out of X days, you said you tanned on X days and used sun protection X days. Try to protect your skin from the sun more often!
				
				--->
                
                <!--- Count Tanning days only get to CP3 IF yes to CP 1 --->
                <cfquery name="GetCountTanning" datasource="#ProdDB#">
                    SELECT 
                        COUNT(SurveyResultsId_bi) AS TotalCount
                    FROM 
                        simplexresults.surveyresults
                    WHERE
                    
                         batchid_bi = 1174
                    AND
                         ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getContacts.ContactString_vch#">
                    AND
                        CPId_int = 3
                </cfquery>
                    
                <!--- Count Tanning days only get to CP7 IF yes to CP 5 - Protection? --->
                <cfquery name="GetCountProtection" datasource="#ProdDB#">
                    SELECT 
                        COUNT(SurveyResultsId_bi) AS TotalCount
                    FROM 
                        simplexresults.surveyresults
                    WHERE
                    
                         batchid_bi = 1174
                    AND
                         ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getContacts.ContactString_vch#">
                    AND
                        CPId_int = 7
                </cfquery>    
                
                <cfset Summary7DayMessage = '' />
                
                <cfif GetCountProtection.TotalCount GTE 7>
                	
                	<cfset Summary7DayMessage = 'FCCC Alert: Out of #getContacts.ProcessedCountPM_int# days, you said you tanned on #GetCountTanning.TotalCount# days and used sun protection #GetCountProtection.TotalCount# days. Great job on protecting your skin everyday!' />
                 
                <cfelseif GetCountProtection.TotalCount GT 3 AND GetCountProtection.TotalCount LT 7>
                
                   <cfset Summary7DayMessage = 'FCCC Alert: Out of #getContacts.ProcessedCountPM_int# days, you said you tanned on #GetCountTanning.TotalCount# days and used sun protection #GetCountProtection.TotalCount# days. Good job, but try to protect your skin everyday!' />
                
                <cfelseif GetCountProtection.TotalCount LT 4 >
                
                	<cfset Summary7DayMessage = 'FCCC Alert: Out of #getContacts.ProcessedCountPM_int# days, you said you tanned on #GetCountTanning.TotalCount# days and used sun protection #GetCountProtection.TotalCount# days. Try to protect your skin from the sun more often!' />
				
                </cfif>
            
                <cfcatch type="any" >
                	LOAD NEXT MESSAGE ERROR
                    <BR/>
                    <cfdump var="#cfcatch#" />            
                </cfcatch>
                
                </cftry>
                
                <!--- Update to Contact --->
                <cfquery name="UpdateContacts" datasource="#ProdDB#">
                    UPDATE
                        simplexprogramdata.fcccskin
                    SET
                        7DaySummarySent_int = 1 ,
                        Updated_dt = NOW()                                      
                    WHERE
                        FCCCSkinId_int = #getContacts.FCCCSkinId_int#        
                </cfquery>	
                
                Data Updated OK	
                <BR/>
                        
                               
                <!--- Send Summary Message if any --->
                <cfif TRIM(Summary7DayMessage) NEQ "">
                                        
                    7 Day Summary Message = #Summary7DayMessage#
                    <BR/>	
                    
                    
                    <!--- This is better/faster in separate process but this is low volume so do it here to be safe.--->
                    <!--- Check for Opt Out --->
                       
                       
					<!--- Get counts from DB --->
                    <cfquery name="CheckForRecentOptOut" datasource="#ProdDB#">
                        SELECT 
                        	COUNT(oi.ContactString_vch) AS TotalCount,
                            oi.OptOut_dt 
                        FROM 
                            simplelists.optinout AS oi 
                        WHERE 
                        	OptOut_dt IS NOT NULL 
                        AND
                        	OptIn_dt IS NULL 
                        AND 
                        	ShortCode_vch = '244687'
                        AND 
                        	oi.ContactString_vch = #getContacts.ContactString_vch#
                    </cfquery>	
                    
                    <cfif CheckForRecentOptOut.TotalCount EQ "0">
                    
						<cfif inpBlockSend EQ 0>
                        
                            <cftry>
                            
                                <cfset VerboseDebug = "0" />
                                <cfset inpBatchId = "#SummaryBatchId#" />
                                <cfset inpContactString= "#getContacts.ContactString_vch#" />
                                <cfset inpMessage= "#Summary7DayMessage#" />
                
                                <!--- Do this once - separated for quicker looping where needed --->
                                <cfinclude template="authsetup.cfm" />
                                
                                <!--- Post to API --->
                                <cfinclude template="sendsinglesms.cfm" />
    
                            <cfcatch type="any" >
                                SEND MESSAGE ERROR
                                <BR/>
                                <cfdump var="#cfcatch#" />            
                            </cfcatch>
                            
                            </cftry>
                    
                        </cfif>
                    
                    
                    <cfelse>
                    
                    	<!--- Update to Contact - Opt Out --->
                        <cfquery name="UpdateContactsOptOut" datasource="#ProdDB#">
                            UPDATE
                                simplexprogramdata.fcccskin
                            SET
                                OptOut_dt = '#CheckForRecentOptOut.OptOut_dt#',
                                Updated_dt = NOW()                                               
                            WHERE
                                FCCCSkinId_int = #getContacts.FCCCSkinId_int#        
                        </cfquery>	
                        
                        CONTACT HAS OPTED OUT
                        <BR/>
                    
                    </cfif>
               
                </cfif>
        
                <hr/>	
                
            </cfloop>
	
    	</div>
        
	</cfoutput>        
    
</cfsavecontent>
    



<!--- Mail Report Daily --->

<cfoutput>
<!---
<div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">

<img src="https://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;"/>--->
    
<BR/>
#DetailDump#
<!---
</div>---> 

</cfoutput>


<cfmail to="jpeterson@messagebroadcast.com;pshah@messagebroadcast.com;swazzan@messagebroadcast.com" from="mbreports@messagebroadcast.com" subject="Fox Chase Cancer Center Skin Summary 7 Day Processing" type="html">
   	<cfoutput>
    
  <!---  <div style="background-image: url(http://www.fccc.edu/images/splashNew2/hmpgBK-newGradient-right.jpg); background-repeat: repeat-x; position:fixed;">
    
    	<img src="http://www.fccc.edu/images/topNav/fccc-tuhs-shaded.jpg" style="left:0; top:0; position:absolute;" />
        <BR/>
 --->  		#DetailDump#
        
    <!---</div>--->    
    </cfoutput>
</cfmail>
            
            




