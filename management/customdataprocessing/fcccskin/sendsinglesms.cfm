<cfparam name="VerboseDebug" default="0" />
<cfparam name="inpBatchId" default="0" />
<cfparam name="inpContactString" default="" />
<cfparam name="inpMessage" default="" />
<cfparam name="inpSkipLocalUserDNCCheck" default="0" />

<cfparam name="datetime" default="" />
<cfparam name="authorization" default="" />

<!--- For low volume ok to use RealTime API - careful if this scales up!--->    
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<cfhttp url="http://ebm.messagebroadcast.com/webservice/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >
	
	    <!--- By default EBM API will return json or XML --->
	    <cfhttpparam name="Accept" type="header" value="application/json" />    	
	    <cfhttpparam type="header" name="datetime" value="#datetime#" />
        <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	   
          
       
       <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
       <!--- If in debug mode make sure you are using the correct credentials--->
       <cfhttpparam type="formfield" name="DebugAPI" value="0" />
       
       <cfhttpparam type="formfield" name="inpBatchId" value="#inpBatchId#" />
       <cfhttpparam type="formfield" name="inpContactString" value="#inpContactString#" />
       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />
       <cfhttpparam type="formfield" name="inpMessage" value="#inpMessage#" />
       <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="#inpSkipLocalUserDNCCheck#" />
          
    </cfhttp>
        
    <cfif VerboseDebug GT 0>
    
		<style>
            .wordwrap { 
           white-space: pre-wrap;      /* CSS3 */   
           white-space: -moz-pre-wrap; /* Firefox */    
           white-space: -pre-wrap;     /* Opera <7 */   
           white-space: -o-pre-wrap;   /* Opera 7 */    
           word-wrap: break-word;      /* IE */
        }
        
        </style>
    
    	<div style="max-width:800px; margin:20px;" class="wordwrap">
    
            returnStruct<BR />                        
            <cfdump var="#returnStruct#">  
            
            <BR />
            
            <cfoutput>
            
                #returnStruct.Filecontent#                      
            
            </cfoutput>
 
 		</div>
 
	</cfif>