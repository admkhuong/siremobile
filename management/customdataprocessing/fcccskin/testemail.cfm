

<cfset Session.UserId = "">
<cfset Session.UserName = "Joe Blow">
<cfset Session.EmailAddress = "joeblow@aol3.com">
<cfset Session.CompanyUserId = "0">
<cfset Session.companyId = "0">

<cfsavecontent variable="outContent">

	<cfoutput>
  
   <style type="text/css">
        
										body 
										{ 
											background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
											background-repeat:no-repeat; 
										}
										.message-block {
											font-family: "Verdana";
											font-size: 12px;
											margin-left: 21px;
										}
										
										.m_top_10 {
											margin-top: 10px;
										}
									
										##divConfirm{
											padding:25px;
											width:600px;
																					
										}
									
										##divConfirm .left-input{
											border-radius: 4px 0 0 4px;
											border-style: solid none solid solid;
											border-width: 1px 0 1px 1px;
											color: ##666666;
											float: left;
											height: 28px;
											line-height: 25px;
											padding-left: 10px;
											width: 170px;
											background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
										}
										##divConfirm .right-input {
										   width: 226px;									   
										   display:inline;
										   height: 28px;
										}
										##divConfirm input {
											width: 210px;
											background-color: ##FBFBFB;
											color: ##000000 !important;
											font-family: "Verdana" !important;
											font-size: 12px !important;
											height: 30px;
											line-height: 30px;
											
											border: 1px solid rgba(0, 0, 0, 0.3);
											border-radius: 0 3px 3px 0;
											box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
											margin-bottom: 8px;
											padding: 0 8px;
											
									
										}
										##divConfirm .left-input > span.em-lbl {
										   width: auto;
										}
									
									</style>
                                
                                	
		                            <div id="divConfirm" align="left">
                                        <div ><h4>EMS Launch Alert!</h4></div>
                                       
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">User Name</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.UserName#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.EmailAddress#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.CompanyUserId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>                                      
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="#Session.companyId#">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
									   	<!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Eligible contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.ELIGIBLECOUNT#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---do not contact --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Do not contact</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.DNCCOUNT#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---duplicate --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Duplicate</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.DUPLICATECOUNT#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---invalid timezone --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Invalid Timezone</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.INVALIDTIMEZONECOUNT#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---cost per unit--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Cost per unit</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.ESTIMATEDCOSTPERUNIT#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current in queue cost--->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current in queue cost</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.COSTCURRENTQUEUE#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        <!---current balance --->
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Current balance</span>
                                            </div>
                                            <div class="right-input">		        		
                                                <input type="text" readonly value="0<!---#retValGETEMSConfirmationData.CURRENTBALANCE#--->">
                                            </div>				
                                        </div>
                                        <div class="clear"></div>
                                        
                                    </div>
                                	
                                    
  		<!---<style type="text/css">
        
			body 
			{ 
				background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
				background-repeat:no-repeat; 
			}
            .message-block {
                font-family: "Verdana";
                font-size: 12px;
                margin-left: 21px;
            }
            
            .m_top_10 {
                margin-top: 10px;
            }
        
            ##divConfirm{
                padding:25px;
                width:600px;
                										
            }
        
            ##divConfirm .left-input{
                border-radius: 4px 0 0 4px;
                border-style: solid none solid solid;
                border-width: 1px 0 1px 1px;
                color: ##666666;
                float: left;
                height: 28px;
                line-height: 25px;
                padding-left: 10px;
                width: 170px;
                background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
            }
            ##divConfirm .right-input {
               width: 226px;									   
			   display:inline;
			   height: 28px;
            }
            ##divConfirm input {
                width: 210px;
                background-color: ##FBFBFB;
                color: ##000000 !important;
                font-family: "Verdana" !important;
                font-size: 12px !important;
                height: 30px;
                line-height: 30px;
                
                border: 1px solid rgba(0, 0, 0, 0.3);
                border-radius: 0 3px 3px 0;
                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
                margin-bottom: 8px;
                padding: 0 8px;
                
        
            }
            ##divConfirm .left-input > span.em-lbl {
               width: auto;
            }
        
        </style>
        
        
                <div id="divConfirm" align="Left" class="NoticeContainer">
                    <div ><h4>EMS Launch Alert!</h4></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Eligible contact</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Do not contact</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Duplicate</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Invalid Timezone</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Cost per unit</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Current in queue cost</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    
                    <div class="message-block  m_top_10">
                        <div class="left-input">
                            <span class="em-lbl">Current balance</span>
                        </div>
                        <div class="right-input">		        		
                            <input type="text" readonly value="0">
                        </div>				
                    </div>
                    <div class="clear"></div>
                
            </div>--->
        
                            
                              
    </cfoutput>


</cfsavecontent>

 	<cfoutput>
  	  #outContent#
    </cfoutput>

<cfmail to="jpeterson@messagebroadcast.com" subject="Test Email Processing" type="html" spoolenable="no" from="support@eventbasedmessaging.com" username="support@eventbasedmessaging.com" password="dEF!1048" server="smtp.gmail.com" port="465" useSSL="true"  >
   	<cfoutput>
  	  #outContent#
    </cfoutput>
</cfmail>
            
            
            
            
            