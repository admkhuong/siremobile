<!---
  --- fpl
  --- --------
  ---
  --- author: ahasan
  --- date:   4/2/15
  --- Base component for all other FPL components. This one contains generic/reusable functions and constant values
  --->

<cfcomponent  accessors="yes"  persistent="yes">

	<!--- whitelist settings --->
	<!--- whitelist settings --->
	<cfproperty name="myFPLShortCode" type="string" default="69375">
	<cfproperty name="SMSWhiteList"  type="string" default="7862017508,3055988342,6125328085,3055522939,9543212166,3055524805,3053898100,3052056446,7866310378,7865032063,9544154476,3052058047,5613088171,5615730913,5619043463,9044516484,5619043253,5613088171,9542326477,5616767030,5613891386,4102712376,5614012305,2019934123,5613088171,3055863626,5613891386,5616767030,3053389892,7862466393,9545990530,5616944987,9543477682,9542883107,3055522091,3055523870,3055522749,3055523786,3054950434,3055522341,3055523608,3055522981,3055524481,9543825110,9543825133,3055523608,3058015434,9495790525,7148789710,5514823846,5615730913,8477219652,9044516484,5613246237,5617586653,5613194800,5616402276,5735297059,5615730913,6306744283,9492786295,5613246237,9044516484,5619098634,5613102551,5615431106,7143563304,6302420839,5617589591,3059702534">
	<cfproperty name="VoiceWhiteList"  type="string" default="7862017508,3055988342,6125328085,3055522939,9543212166,3055524805,3053898100,3052056446,7866310378,7865032063,9544154476,3052058047,5613088171,5615730913,5619043463,9044516484,5619043253,5613088171,9542326477,5616767030,5613891386,4102712376,5614012305,2019934123,5613088171,3055863626,5613891386,5616767030,3053389892,7862466393,9545990530,5616944987,9543477682,9542883107,3055522091,3055523870,3055522749,3055523786,3054950434,3055522341,3055523608,3055522981,3055524481,9543825110,9543825133,3055523608,3058015434,9543825110,3055522341,9543477682,3054950434,3055522749,3055523870,3055522091,9542883107,9495790525,7148789710,5514823846,5615730913,8477219652,9044516484,5613246237,5617586653,5613194800,5616402276,5615730913,9494284893,5613102551,8034931310,5617589591,5616943859,5616944987,6306744283,9492786295,5613246237,9044516484,5619098634,5613102551,5615431106,7143563304,6302420839,5617589591,3059702534">

	<!--- EBM Settings --->
	<!--- EBM Settings --->
    <cfproperty name="EBMDsn" type="string" default="Bishop">
	<cfproperty name="accessKey"  type="string" default="66D8F53DE9B97F11E689">
	<cfproperty name="secretKey"  type="string"  default="57166/51960116+DFCc5B444F518C571c9/C1cdD">
	<cfproperty name="FPLEBMUserId" type="string" default="377">

	<!--- SMS & Voice Settings --->
	<!--- SMS & Voice Settings --->
	<cfproperty name="SMSFilePattern" type="string" default="*_sms_*.csv">
	<cfproperty name="VoiceFIlePattern" type="string" default="*_voice_*.csv">

	<!--- NCO Settings --->
	<!--- NCO Settings --->
	<cfproperty name="NCOFilePattern" type="string" default="NCO_*.csv">

	<!--- Environment based variables --->
	<!--- Environment based variables --->
	<!--- <cfproperty name="FPLServiceURL" type="string" default="https://betatest2.fpl.com/api/partners/preferences"> --->
	<!--- <cfproperty name="EBMApiUrlBase" type="string" default="http://ebmapi.messagebroadcast.com/webservice"> --->
	<!--- <cfproperty name="EnforceWhiteList" type="boolean" default="true"> --->
    <!--- <cfproperty name="SMSPerThread" type="numeric" default="10"> --->
    <!--- <cfproperty name="VoicePerThread" type="numeric" default="10"> --->
	<!--- <cfproperty name="errorEmail" type="string" default="ahasan@messagebroadcast.com">	 --->
	<!--- <cfproperty name="remoteInFolder" type="string" default="\\10.11.1.21\FPL1\campaign\data\in">
    <cfproperty name="RemoteOutFolder" type="string" default="\\10.11.1.21\FPL1\campaign\data\out"> --->
	<!--- <cfproperty name="remoteInFolderNCO" type="string" default="\\10.11.1.21\NCO\data\in"> --->
    <!--- <cfproperty name="RemoteOutFolderNCO" type="string" default="\\10.11.1.21\NCO\data\out">	 --->
	<cfif isProd()>
		<cfset variables.FPLServiceURL = "https://www.fpl.com/api/partners/preferences">
		<cfset variables.EBMApiUrlBase = "http://ebmapi.messagebroadcast.com/webservice">
		<cfset variables.EnforceWhiteList = true>
		<cfset variables.SMSPerThread = 50>
		<cfset variables.VoicePerThread = 50>
		<cfset variables.errorEmail = "IT@messagebroadcast.com">
		<cfset variables.RemoteInFolder = "\\10.11.1.21\FPL1\campaign\data\in">
		<cfset variables.RemoteOutFolder = "\\10.11.1.21\FPL1\campaign\data\out">
		<cfset variables.remoteInFolderNCO  = "\\10.11.1.21\NCO\data\in">
    	<cfset variables.RemoteOutFolderNCO = "\\10.11.1.21\NCO\data\out">
	<cfelse>
		<!--- <cfset variables.FPLServiceURL = "https://betatest2.fpl.com/api/partners/preferences"> --->
		<!--- <cfset variables.FPLServiceURL = "https://webtest.fpl.com/api/partners/preferences"> --->
		<cfset variables.FPLServiceURL = "https://webqa.fpl.com/api/partners/preferences">
		<cfset variables.EBMApiUrlBase = "http://ebmapi.messagebroadcast.com/webservice">
		<cfset variables.EnforceWhiteList = true>
		<cfset variables.SMSPerThread = 10>
		<cfset variables.VoicePerThread = 10>
		<cfset variables.errorEmail = "ahasan@messagebroadcast.com">
		<cfset variables.RemoteInFolder = '\\10.11.1.21\FPL1\campaign\test\in'>
		<cfset variables.RemoteOutFolder = '\\10.11.1.21\FPL1\campaign\test\out'>
		<cfset variables.remoteInFolderNCO  = "\\10.11.1.21\NCO\data\in">
    	<cfset variables.RemoteOutFolderNCO = "\\10.11.1.21\NCO\data\out">
	</cfif>

	<!-------------------------------------------------------- functions ------------------------------------------------------------>
	<!-------------------------------------------------------- functions ------------------------------------------------------------>
	<!-------------------------------------------------------- functions ------------------------------------------------------------>
	<!-------------------------------------------------------- functions ------------------------------------------------------------>
	<!-------------------------------------------------------- functions ------------------------------------------------------------>

	<cffunction name="Init" access="public" returntype="any">

		<!---initialize error email     --->
    	<cfif isdefined("arguments.ErrorEmail")>
	    	<cfset setErrorEmail(arguments.ErrorEmail)>
		</cfif>

		<!--- SMS/Voice in folder  --->
    	<cfif isdefined("arguments.RemoteInFolder")>
	    	<cfset setRemoteInFolder(arguments.RemoteInFolder)>
		</cfif>

		<!--- SMS/VOICE out folder --->
    	<cfif isdefined("arguments.RemoteOutFolder")>
	    	<cfset setRemoteOutFolder(arguments.RemoteOutFolder)>
		</cfif>

		<!--- DSN --->
    	<cfif isdefined("arguments.EBMDsn")>
	    	<cfset setEBMDsn(arguments.EBMDsn)>
		</cfif>

		<!--- accessKey --->
    	<cfif isdefined("arguments.accessKey")>
	    	<cfset setaccessKey(arguments.accessKey)>
		</cfif>

		<cfreturn this>
	</cffunction>

	<cffunction name="getFPLServiceURL" output="false" access="remote">
		<cfreturn variables.FPLServiceURL>
	</cffunction>

	<cffunction name="getEBMApiUrlBase" output="false" access="remote">
		<cfreturn variables.EBMApiUrlBase>
	</cffunction>

	<cffunction name="getEnforceWhiteList" output="false" access="remote">
		<cfreturn variables.EnforceWhiteList>
	</cffunction>

	<cffunction name="getSMSPerThread" output="false" access="remote">
		<cfreturn variables.SMSPerThread>
	</cffunction>

	<cffunction name="getVoicePerThread" output="false" access="remote">
		<cfreturn variables.VoicePerThread>
	</cffunction>

	<cffunction name="getErrorEmail" output="false" access="remote">
		<cfreturn variables.errorEmail>
	</cffunction>

	<cffunction name="getRemoteInFolder" output="false" access="remote">
		<cfreturn variables.RemoteInFolder>
	</cffunction>

	<cffunction name="getRemoteOutFolder" output="false" access="remote">
		<cfreturn variables.RemoteOutFolder>
	</cffunction>

	<cffunction name="getremoteInFolderNCO" output="false" access="remote">
		<cfreturn variables.remoteInFolderNCO>
	</cffunction>

	<cffunction name="getRemoteOutFolderNCO" output="false" access="remote">
		<cfreturn variables.RemoteOutFolderNCO>
	</cffunction>

    <!---method to send error email--->
	<cffunction name="sendErrorNotification" access="public">
		<cfargument name="ErrSubject" default="FPL Error on #CGI.SERVER_NAME#" required="no">
		<cfargument name="ErrMsg" default="No Message Specified" required="no">

		<cfset var thisDetail = "">
		<cfset var thisMessage = "">
		<cfset var thisType = "">

	    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
	    <cfset SupportEMailFrom = "support@eventbasedmessaging.com" />
	    <cfset SupportEMailServer = "smtp.gmail.com" />
	    <cfset SupportEMailUserName = "support@eventbasedmessaging.com" />
	    <cfset SupportEMailPassword = "dEF!1048" />
	    <cfset SupportEMailPort = "465" />
	    <cfset SupportEMailUseSSL = "true" />

	    <cfif isdefined("cfcatch")>
			<cfset thisType = TRIM(cfcatch.TYPE)>
			<cfset thisMessage = TRIM(cfcatch.MESSAGE)>

			<cfsavecontent variable="thisDetail">
				<cfdump var="#cfcatch#" label="CFCATCH">
			</cfsavecontent>
	    </cfif>


        <cftry>

			<cfmail to="#getErrorEmail()#" subject="#arguments.ErrSubject#" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
			<!--- <cfmail to="#getErrorEmail()#" from="mbreports@messagebroadcast.com" subject="#arguments.ErrSubject#" type="html" > --->

                <cfoutput>#arguments.ErrMsg#</cfoutput>

                <cfif isdefined("cfcatch")>
                    <cfdump var="#cfcatch#" label="CFCATCH">
                </cfif>

                <cfif isdefined("CGI")>
                    <cfdump var="#CGI#" label="CGI">
                </cfif>

            </cfmail>

            <cfquery name="InsertToErrorLog" datasource="#getEBMDsn()#">
                INSERT INTO simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    -9999999,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(arguments.ErrSubject)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(arguments.ErrMsg)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See CatchMessage_vch in this log for details">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#thisType#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#thisMessage#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#thisDetail#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                )
            </cfquery>




            <cfcatch type="any">
        		<cfdump var="#cfcatch#">
            </cfcatch>

        </cftry>

	</cffunction>

	<!---return universal return struct--->
    <cffunction name="getReturnStruct" access="private" returntype="struct">
		<cfset s = structNew()>
        <cfset s.ErrMsg = "">
        <cfset s.ErrCode = 0>

        <cfreturn s>
    </cffunction>

	<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="public" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />

		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>

		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>

		<cfreturn retval>
	</cffunction>

	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />

	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />

	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />

	   <cfreturn mac.doFinal() />
	</cffunction>

	<cffunction name="createSignature" returntype="string" access="private" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>

		<cfreturn signature>
	</cffunction>

	<cffunction name="getVar" access="public">
		<cfargument name="VarName" required="true" type="string">

		<cfset returnValue = "">

		<cfquery name="qVar" datasource="#getEBMDsn()#">
				SELECT	*
				FROM	simplexprogramdata.portal_link_c10_p1_vars
				WHERE	VarName_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.VarName#">
			</cfquery>

			<cfif qVar.recordCount>
				<cfset returnValue = trim(qVar.VarValue_vch)>
			</cfif>

			<cfreturn returnValue>

		</cffunction>

	<cffunction name="setVar" access="public">
			<cfargument name="VarName" required="true" type="string">
			<cfargument name="VarValue" required="true" type="string">

			<cfquery name="qVar" datasource="#getEBMDsn()#">
				INSERT INTO simplexprogramdata.portal_link_c10_p1_vars
				(
					VarName_vch,
					VarValue_vch
				)
				VALUES
				(
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.VarName#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.VarValue#">
				)
				ON DUPLICATE KEY UPDATE
					VarValue_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.VarValue#">;
			</cfquery>

	</cffunction>

	<!--- write in ColdFusion error log file --->
	<cffunction name="logFPLMsg" access="public">
		<cfargument name="msg" required="true" type="string">
		<cfargument name="error" required="false" type="string" default="0">

		<cfif arguments.error>
			<cflog file="FPL_ERROR"
					application="yes"
					text="#arguments.msg#">
		<cfelse>
			<cflog file="FPL_ACTIVITY"
					application="yes"
					text="#arguments.msg#">
		</cfif>

	</cffunction>

	<!--- get var values from struct return from EBM pdc APIs --->
	<cffunction name="extractAPIResultEle" access="public" output="false">
		<cfargument name="resultStruct" type="any">
		<cfargument name="ele" type="string">

		<cfset resultEelments = resultStruct.Columns>
		<cfset resultData = resultStruct.Data[1]>

		<cfset pos = ArrayFind(resultEelments,arguments.ele)>

		<cfif pos EQ 0>
			<cfset sReturn = "">
		<cfelse>
			<cfset sReturn = trim(resultData[pos])>
		</cfif>

		<cfreturn sReturn>
	</cffunction>

	<!--- replace high ASCII characters with their substitutes that MBlox support --->
	<cffunction name="cleanHighASCiiChar" access="public" output="false">
		<cfargument name="inpStr">

		<cfset var localStr = "">

		<!--- replace upper case spanish accent characters --->
		<cfset localStr = replace(arguments.inpStr,chr(192),'A','all')>
		<cfset localStr = replace(localStr,chr(193),'A','all')>
		<cfset localStr = replace(localStr,chr(201),'E','all')>
		<cfset localStr = replace(localStr,chr(205),'I','all')>
		<cfset localStr = replace(localStr,chr(211),'O','all')>
		<cfset localStr = replace(localStr,chr(218),'U','all')>
		<cfset localStr = replace(localStr,chr(209),'N','all')>
		<cfset localStr = replace(localStr,chr(220),'U','all')>

		<!--- replace lower case spanish accent characters --->
		<cfset localStr = replace(localStr,chr(225),'a','all')>
		<cfset localStr = replace(localStr,chr(233),'e','all')>
		<cfset localStr = replace(localStr,chr(237),'i','all')>
		<cfset localStr = replace(localStr,chr(243),'o','all')>
		<cfset localStr = replace(localStr,chr(250),'u','all')>
		<cfset localStr = replace(localStr,chr(241),'n','all')>
		<cfset localStr = replace(localStr,chr(252),'u','all')>

		<!--- � to ?--->
		<cfset localStr = replace(localStr,chr(191),'?','all')>

		<!--- � to ! --->
		<cfset localStr = replace(localStr,chr(161),'!','all')>

		<cfreturn localStr>

	</cffunction>

	<!--- convert from PST date/time to EST datetime --->
	<cffunction name="PSTtoEST" access="public" output="false">
		<cfargument name="inpDate" type="string" required="true">

		<cfif isdate(arguments.inpDate)>
			<cfset arguments.inpDate = dateAdd("h",3,arguments.inpDate)>
		</cfif>

		<cfreturn arguments.inpDate>

	</cffunction>

	<!--- convert from EST date/time to PST datetime --->
	<cffunction name="ESTtoPST" access="public" output="false">
		<cfargument name="inpDate" type="string" required="true">

		<cfif isdate(arguments.inpDate)>
			<cfset arguments.inpDate = dateAdd("h",-3,arguments.inpDate)>
		</cfif>

		<cfreturn arguments.inpDate>

	</cffunction>

	<!--- Log service requests such as dipping request, opt request etc --->
	<cffunction name="logServiceCall" access="public">
		<cfargument name="serviceType" required="true" type="string">
		<cfargument name="serviceData" required="true" type="string">

		<cfquery name="qAccount" datasource="#getEBMDsn()#">
			INSERT INTO simplexprogramdata.portal_link_c10_p1_service_log
			(ServiceType_vch,
			ServiceData_vch,
			CallDateTime_dt)
			VALUES
			(
				<cfqueryparam cfsqltype="" value="#arguments.serviceType#">,
				<cfqueryparam cfsqltype="" value="#CGI.SERVER_NAME#;#arguments.serviceData#">,
				now()
			);
		</cfquery>

	</cffunction>

	<!--- check if environment is production --->
	<cffunction name="IsProd" access="remote" output="true">

		<cfif listFindNoCase('ebmmanagement.mb,ebmapi.messagebroadcast.com',CGI.SERVER_NAME)>
			<cfset bFlag = true>
		<cfelse>
			<cfset bFlag = false>
		</cfif>

		<cfreturn bFlag>
	</cffunction>

		<cfscript>
		/**
		 * CSVFormat accepts the name of an existing query and converts it to csv format.
		 * Updated version of UDF orig. written by Simon Horwith
		 *
		 * @param query 	 The query to format. (Required)
		 * @param qualifer 	 A string to qualify the data with. (Optional)
		 * @param columns 	 The columns ot use. Defaults to all columns. (Optional)
		 * @return A CSV formatted string.
		 * @author Jeff Howden (cflib@jeffhowden.com)
		 * @version 2, August 26, 2008
		 */
		function CSVFormat(query) {
		  var returnValue = ArrayNew(1);
		  var rowValue = '';
		  var columns = query.columnlist;
		  var qualifier = '';
		  var i = 1;
		  var bNCO = false;
		  var addcolumnheader = true;
		  if(ArrayLen(Arguments) GTE 2) qualifier = Arguments[2];
	      if(ArrayLen(Arguments) GTE 3 AND Len(Arguments[3])) columns = Arguments[3];
	      if(ArrayLen(Arguments) GTE 4) addcolumnheader = Arguments[4];
	      if(ArrayLen(Arguments) GTE 5) bNCO = Arguments[5];
	      if(addcolumnheader)
	      {
			  returnValue[1] = ListQualify(columns, qualifier,"|");
			  arroffset = 0;
	      }
	      else
	      {
	      	  arroffset = -1;
	      }
		  ArrayResize(returnValue, query.recordcount + 1 + arroffset);

		  columns = ListToArray(columns,"|");
		  for(i = 1; i LTE query.recordcount; i = i + 1)
		  {
		    rowValue = ArrayNew(1);
		    ArrayResize(rowValue, ArrayLen(columns));
		    for(j = 1; j LTE ArrayLen(columns); j = j + 1)
		      rowValue[j] = qualifier & query[columns[j]][i] & qualifier;
		    returnValue[i + 1 + arroffset] = ArrayToList(rowValue,"|");
		  }
		  returnValue = ArrayToList(returnValue, Chr(10));

		  //NCO want additional line feed char at the end of last record
		  if(bNCO)
		  	returnValue = returnValue & chr(10);

		  return returnValue;
		}
		</cfscript>

	</cfcomponent>