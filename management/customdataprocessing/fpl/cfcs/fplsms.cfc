<!---
  --- fplsms
  --- --------
  ---
  --- author: ahasan
  --- date:   4/2/15
  --- SMS server component
  --->
<cfcomponent  accessors="yes" extends="fpl" persistent="yes">


    <cfproperty name="BatchId" type="numeric" default="1404">

	<cffunction name="init" access="public" returntype="any">

    	<cfset super.init(argumentCollection=arguments)>

		<!--- SMS per thread --->
    	<cfif isdefined("arguments.SMSPerThread")>
	    	<cfset setSMSPerThread(arguments.SMSPerThread)>
		</cfif>

		<cfreturn this>
	</cffunction>

    <!---function to import SMS data files from unica to EBM table simplexprogramdata.portal_link_c10_p1_unicafiles --->
	<cffunction name="importSMSFiles" access="remote" output="false" returnformat="JSON" >

        <cfset sReturn = getReturnStruct()>

    	<!---get list of files from the server--->
	    <cfdirectory action="list" directory="#getRemoteInFolder()#" name="qFiles" filter="#getSMSFilePattern()#" sort="DATELASTMODIFIED ASC">

		<!---if files found, move to local folder for processing and rename original files--->
        <cfset aFiles = arraynew(2)>

        <cfloop query="qFiles">

            <cftry>

                <cfquery name="qData" datasource="#getEBMDsn()#" result="queryResults">
                    LOAD DATA LOCAL INFILE  '#replacenocase(getRemoteInFolder(),"\","\\","all")#\\#qFiles.Name#'
                    INTO TABLE simplexprogramdata.portal_link_c10_p1_unicafiles
					CHARACTER SET utf8
                    FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES
                    (TRANSID_vch,Channel_vch,PFPID_bi,AccountNo_vch,Premise_vch,TextMessage_vch,VoiceScriptIdentifier_vch,PrimaryPhoneNumber_vch,CellPhoneFlag1_ch,SecondaryPhoneNumber_vch,CellPhoneFlag2_ch,
                    CampaignCode_vch,CampaignName_vch, @ts1 ,WholeName_vch,PremAddrComplete_vch,PremCity_vch,PremState_vch,PremZip_vch,EmailAddress_vch,
                    Custom1_vch,Custom2_vch,Custom3_vch,Custom4_vch,Custom5_vch)
                      SET InFileName_vch = '#qFiles.Name#',
                        timestamp1_dt = DATE_ADD(STR_TO_DATE(@ts1,'%m/%d/%Y %T'),INTERVAL -3 HOUR),  <!--- substract 3 hours to translate time to PST from EST --->
                        status_int = 0,
                        statusTimeStamp_dt = now(),
                        importTimeStamp_dt = now(),
						isProd_ti = <cfif isProd()>1<cfelse>0</cfif>;
                </cfquery>

                <!---add file name etc to array that will be returned --->
                <cfset aFiles[qFiles.currentRow][1] = qFiles.Name>
                <cfset aFiles[qFiles.currentRow][2] = queryResults.recordcount>

                <!---move file to local processed folder--->
                <cffile action="move" source="#getRemoteInFolder()#\#qFiles.name#" destination="#getRemoteInFolder()#\processed\#qFiles.name#">

				<!--- log activity --->
				<!--- <cfset logFPLMsg("fplsms.cfc - #GetFunctionCalledName()# - File=#qFiles.name# - No Of Records=#queryResults.recordcount#")> --->

                <cfcatch type="any">

                    <!---move file to local error folder--->
                    <cffile action="move" source="#getRemoteInFolder()#\#qFiles.name#" destination="#getRemoteInFolder()#\error\#qFiles.name#">

                    <cfset SubjectLine = "FPL ERROR - UNICA - Error processing file #qFiles.name#">
                    <cfset ENA_Message = "Error in processing file #qFiles.name#. File moved to error folder (#getRemoteInFolder()#\error\)">
                    <cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                    <!---set error in struct--->
                    <cfset sReturn.ErrMsg = cfcatch.Message>
                    <cfset sReturn.ErrCode = -1>

                </cfcatch>

            </cftry>

        </cfloop>

        <cfset sReturn.Files = aFiles>

		<cfset logServiceCall(serviceType="SMS_IMP", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>

    </cffunction>

    <!---function to import SMS data files from unica to EBM table simplexprogramdata.portal_link_c10_p1_unicafiles --->
	<cffunction name="sendSMS" access="remote" output="true" returnformat="JSON" >

        <cfset sReturn = getReturnStruct()>

	    <!---Verb must match that of request type--->
		<cfset local.sign = generateSignature("POST") />
        <cfset datetime = local.sign.DATE>
        <cfset signature = local.sign.SIGNATURE>
        <cfset authorization = getAccessKey() & ":" & signature>


		<!--- Get threads that aren't running. --->
        <cfquery name="qAvailableThreads" datasource="#getEBMDsn()#">
	        SELECT	ThreadId_int
	        FROM	simplexprogramdata.portal_link_c10_p1_threads
	        WHERE	IsRunning_int = 0
					AND
					ThreadType_ch = 'S'
	        ORDER BY ThreadId_int
        </cfquery>

       	<cfloop query="qAvailableThreads">

            <!---get top SMSPerThread records to send--->
            <cfquery name="qRecordsToSend" datasource="#getEBMDsn()#">
                SELECT 	*
                FROM	simplexprogramdata.portal_link_c10_p1_unicafiles
                WHERE	Status_int = 0
                ORDER BY PKID_bi asc
                LIMIT #variables.SMSPerThread#
            </cfquery>

            <!---if no unsent record found then exit the loop else start calling EBM API--->
            <cfif qRecordsToSend.recordcount EQ 0>
                <cfbreak>
            <cfelse>

                <!---Mark thread as currently in use--->
                <cfquery name="qEngageThread" datasource="#getEBMDsn()#">
                    UPDATE	simplexprogramdata.portal_link_c10_p1_threads
                    SET		IsRunning_int = 1,
                            LastRan_dt = now()
                    WHERE	ThreadId_int = #qAvailableThreads.ThreadId_int#
                </cfquery>

                <!--- update status of records i.e mark as "in queue" (Status_int: 0=not processed, 1=in queue, 2=processed, 3=exported--->
                <cfquery name="qUpdateSTatus" datasource="#getEBMDsn()#">
                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
                    SET		Status_int = 1
                    WHERE PKID_bi in (#valuelist(qRecordsToSend.PKID_bi,",")#)
                </cfquery>

                <!---call EBM api in thread to make the processing faster--->
                <cfthread name="fplthr#qAvailableThreads.ThreadId_int#" ThisThreadId="#qAvailableThreads.ThreadId_int#" ThisThreadQuery="#qRecordsToSend#" action="run">

                    <cfloop query="attributes.ThisThreadQuery">

                        <cftry>

                            <cfset smsnumber = trim(attributes.ThisThreadQuery.PrimaryPhoneNumber_vch)>

                            <!---if in whitelist or if whitelist is not enforced, call api else log this as error--->
                            <cfif NOT getEnforceWhiteList() OR  listFindNoCase(getSMSWhiteList(),smsnumber)>

                                <cfif isnumeric(smsnumber) AND trim(attributes.ThisThreadQuery.TextMessage_vch) NEQ "">

                                    <cfhttp url="#getEBMApiUrlBase()#/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >

                                       <!--- Required components --->

                                       <!--- By default EBM API will return json or XML --->
                                       <cfhttpparam name="Accept" type="header" value="application/json" />
                                       <cfhttpparam type="header" name="datetime" value="#datetime#" />
                                       <cfhttpparam type="header" name="authorization" value="#authorization#" />

                                       <!--- Batch Id controls which pre-defined campaign to run --->
                                       <cfhttpparam type="formfield" name="inpBatchId" value="#getBatchId()#" />

                                       <!--- 1=voice 2=email 3=SMS--->
                                       <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />

                                       <!--- Contact string--->
                                       <cfhttpparam type="formfield" name="inpContactString" value="#smsnumber#" />

                                       <!--- Optional Components --->

                                       <!--- Custom data element for PDC Batch Id 1135 --->
                                       <cfhttpparam type="formfield" name="inpCustomSMS" value="#cleanHighASCiiChar(attributes.ThisThreadQuery.TextMessage_vch)#" />

                                    </cfhttp>


	                                <cfset resultArray = deserializeJSON(returnStruct.Filecontent)>

	                                <!---RXRESULTCODE will be positive for success and negative for failiure--->
	                                <cfif extractAPIResultEle(resultArray,"RXRESULTCODE") EQ 1>
	                                    <!---update with distribution id so we can query for response later on for each sms transaction--->
	                                    <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
	                                        UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
	                                        SET		DTSID_bi = #extractAPIResultEle(resultArray,"DISTRIBUTIONID")#,
			                                        RequestUUID_vch = '#extractAPIResultEle(resultArray,"REQUESTUUID")#',
	                                                ResponseMessage_vch = 'Success',
	                                                ResponseMessageTimestamp_dt = now(),
	                                                Status_int = 2,  <!---processed--->
	                                                statusTimeStamp_dt = now()
	                                        WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
	                                    </cfquery>
	                                <cfelse>
	                                    <!---upadte with faliure--->
	                                    <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
	                                        UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
	                                        SET		DTSID_bi = -1,
			                                        RequestUUID_vch = '#extractAPIResultEle(resultArray,"REQUESTUUID")#',
	                                                ResponseMessage_vch = '#extractAPIResultEle(resultArray,"MESSAGE")# #extractAPIResultEle(resultArray,"ERRMESSAGE")#',
	                                                ResponseMessageTimestamp_dt = now(),
	                                                Status_int = 2, <!---processed--->
	                                                statusTimeStamp_dt = now()
	                                        WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
	                                    </cfquery>
	                                </cfif>


                                <cfelse>

									<!--- log activity --->
									<cfset logFPLMsg(	msg="PKID_bi=#attributes.ThisThreadQuery.PKID_bi#  PrimaryPhoneNumber_vch=#attributes.ThisThreadQuery.PrimaryPhoneNumber_vch# TextMessage_vch:#attributes.ThisThreadQuery.TextMessage_vch#",
														error=1)>

	                                <!---upadte with faliure--->
	                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
	                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
	                                    SET		DTSID_bi = -1,
			                                    RequestUUID_vch = '-1',
	                                            ResponseMessage_vch = 'Invalid phone number or txt msg',
	                                            ResponseMessageTimestamp_dt = now(),
	                                            Status_int = 2, <!---processed--->
	                                            statusTimeStamp_dt = now()
	                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
	                                </cfquery>

                                </cfif>

                           <cfelse> <!---if not in whitelist--->

                                <!---upadte with faliure--->
                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
                                    SET		DTSID_bi = -1,
											RequestUUID_vch = '-1',
                                            ResponseMessage_vch = 'Phone number not in whitelist',
                                            ResponseMessageTimestamp_dt = now(),
                                            Status_int = 2, <!---processed--->
                                            statusTimeStamp_dt = now()
                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
                                </cfquery>

                            </cfif>

                            <cfcatch>
                                <!---If there is exception, mark sms msg as failed and move on--->
                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unicafiles
                                    SET		DTSID_bi = -1,
											RequestUUID_vch = '-1',
                                            ResponseMessage_vch = 'Failed',
                                            ResponseMessageTimestamp_dt = now(),
                                            Status_int = 2, <!---processed--->
                                            statusTimeStamp_dt = now()
                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
                                </cfquery>

                                <cfset SubjectLine = "FPL ERROR - UNICA - Error sending sms">
                                <cfset ENA_Message = "Error in processing file: #ThisThreadQuery.InFileName_vch# and PKID: #ThisThreadQuery.PKID_bi#">
                    			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                            </cfcatch>

                            <cffinally>
                               <!--- exception or not please release the thread--->
                                <cfquery name="qReleaseThread" datasource="#getEBMDsn()#">
                                    UPDATE	simplexprogramdata.portal_link_c10_p1_threads
                                    SET		IsRunning_int = 0,
                                            LastRan_dt = now()
                                    WHERE	ThreadId_int = #attributes.ThisThreadId#
                                </cfquery>
                            </cffinally>

                        </cftry>

                    </cfloop>

                </cfthread>

            </cfif> <!---end of no records found condition--->

        </cfloop>

        <cfreturn SerializeJSON(sReturn)>

    </cffunction>

	<!--- function to export SMS resposne data to FTP site --->
	<cffunction name="exportData" access="remote" output="true" returnformat="JSON">

		<cfset sReturn = getReturnStruct()>

		<cfset strColumns = "transaction_id|channel|pfp_id|ReasonCode|ReasonDescription|timestamp1|timestamp2|timestamp3|account_no|premise|primary_phone_number|secondary_phone_number|campaigncode|email_address|custom1|custom2|custom3|custom4|custom5">


		<!---get all the records that we have to export i.e. where ResponseExportTimeStamp is null--->
		<cfquery name="qExport" datasource="#getEBMDsn()#">
		SELECT TRANSID_vch as transaction_id,
				channel_vch as channel,
		       pfpid_bi as pfp_id,
		       CASE
		         WHEN dtsid_bi > 0 THEN 4
		         ELSE 5
		       END                         AS ReasonCode,
		       CASE
		         WHEN dtsid_bi > 0 THEN 'Delivered'
		         ELSE 'Failed'
		       END                         AS ReasonDescription,
		       DATE_ADD(timestamp1_dt,INTERVAL 3 HOUR) as timestamp1,
		       DATE_ADD(responsemessagetimestamp_dt,INTERVAL 3 HOUR) AS timestamp2,
		       DATE_ADD(now(),INTERVAL 3 HOUR) as timestamp3,
		       accountno_vch as account_no,
		       premise_vch as premise,
		       primaryphonenumber_vch as primary_phone_number,
		       secondaryphonenumber_vch as secondary_phone_number,
		       campaigncode_vch as campaigncode,
		       emailaddress_vch as email_address,
		       custom1_vch as custom1,
		       custom2_vch as custom2,
		       custom3_vch as custom3,
		       custom4_vch as custom4,
		       custom5_vch as custom5,
		       InFileName_vch,
		       PKID_bi
		FROM   	simplexprogramdata.portal_link_c10_p1_unicafiles U
		WHERE	status_int = 2
				and isProd_ti = <cfif isProd()>1<cfelse>0</cfif>
		ORDER BY InFileName_vch asc, PKID_bi asc
		</cfquery>

		<!---Array to return filenames and number of records exported --->
        <cfset aFiles = arraynew(2)>
		<cfset counter = 1>

		<cfoutput query="qExport" group="InFileName_vch" groupcasesensitive="no">

			<!---get records for one file at a time--->
		    <cfquery name="qOneFile" dbtype="query">
		    	SELECT 	transaction_id,channel,pfp_id,ReasonCode,ReasonDescription,timestamp1,timestamp2,timestamp3,account_no,
		        		premise,primary_phone_number,secondary_phone_number,campaigncode,email_address,custom1,custom2,custom3,custom4,custom5, pkid_bi
		        FROM 	qExport
		        WHERE	InFileName_vch = '#qExport.InFileName_vch#'
		        ORDER BY PKID_bi asc
		    </cfquery>

		    <cftry>

				<!---generate file--->
		        <cfset strCSV = CSVFormat(qOneFile,"",strColumns)>

		        <!---save file to out folder--->
		        <cffile action="write" file="#getRemoteOutFolder()#\#qExport.InFileName_vch#" output="#strCSV#" addnewline="no">


		        <cfif qOneFile.recordcount GT 0>

		            <!---Update flag in table so we dont end up exporting again --->
		            <cfquery name="qUpdateFlag" datasource="#getEBMDsn()#">
		                UPDATE	simplexprogramdata.portal_link_c10_p1_unicafiles
		                SET		Status_int = 3,
		                        StatusTimeStamp_dt = now(),
		                        ExportTimeStamp_dt = now()
		                WHERE	pkid_bi IN (#valuelist(qOneFile.pkid_bi)#)
		            </cfquery>

		        </cfif>

                <!---add file name etc to array that will be returned --->
                <cfset aFiles[counter][1] = qExport.InFileName_vch>
                <cfset aFiles[counter][2] = qOneFile.recordcount>
				<cfset counter = counter + 1>


				<cfcatch type="any">

		            <cfset SubjectLine = "FPL ERROR - UNICA - Error Exporting file #qExport.InFileName_vch#">
		            <cfset ENA_Message = "Error while exporting SMS results to out folder">
           			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                    <!---set error in struct--->
                    <cfset sReturn.ErrMsg = cfcatch.Message>
                    <cfset sReturn.ErrCode = -1>

		        </cfcatch>

			</cftry>

		</cfoutput>

		<cfset sReturn.Files = aFiles>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="SMS_EXP", serviceData = serializeJSON(sReturn))>

		<cfreturn SerializeJSON(sReturn)>

	</cffunction>

</cfcomponent>