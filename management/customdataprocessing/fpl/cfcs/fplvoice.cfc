<!---
  --- fplvoice
  --- --------
  ---
  --- author: ahasan
  --- date:   4/3/15
  --- Voice service component
  --->
<cfcomponent  accessors="yes" extends="fpl" persistent="yes">



	<!--- init all properties/values --->
	<cffunction name="init" access="public" returntype="any">

    	<cfset super.init(argumentCollection=arguments)>

		<!--- SMS per thread --->
    	<cfif isdefined("arguments.VoicePerThread")>
	    	<cfset setVoicePerThread(arguments.VoicePerThread)>
		</cfif>

		<cfreturn this>
	</cffunction>

    <!---function to import Voice data files from unica to EBM table simplexprogramdata.portal_link_c10_p1_unica_voice_files --->
	<cffunction name="importVoiceFiles" access="remote" output="false" returnformat="JSON" >

        <cfset sReturn = getReturnStruct()>

    	<!---get list of files from the server--->
	    <cfdirectory action="list" directory="#getRemoteInFolder()#" name="qFiles" filter="#getVoiceFilePattern()#" sort="DATELASTMODIFIED ASC">

		<!---if files found, move to local folder for processing and rename original files--->
        <cfset aFiles = arraynew(2)>

        <cfloop query="qFiles">

            <cftry>

		        <cfquery name="qData" datasource="#getEBMDsn()#" result="queryResults">
		            LOAD DATA LOCAL INFILE  '#replacenocase(getRemoteInFolder(),"\","\\","all")#\\#qFiles.Name#'
		            INTO TABLE simplexprogramdata.portal_link_c10_p1_unica_voice_files
		            FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES
		            (TRANSID_vch,Channel_vch,PFPID_bi,AccountNo_vch,Premise_vch,TextMessage_vch,VoiceScriptIdentifier_vch,PrimaryPhoneNumber_vch,CellPhoneFlag1_ch,SecondaryPhoneNumber_vch,CellPhoneFlag2_ch,
		            CampaignCode_vch,CampaignName_vch, @ts1 ,WholeName_vch,PremAddrComplete_vch,PremCity_vch,PremState_vch,PremZip_vch,EmailAddress_vch,
		            Custom1_vch,Custom2_vch,Custom3_vch,Custom4_vch,Custom5_vch,Custom6_vch,Custom7_vch,Custom8_vch,Custom9_vch,Custom10_vch,Custom11_vch,Custom12_vch,Custom13_vch,Custom14_vch,Custom15_vch)
		              SET InFileName_vch = '#qFiles.Name#',
		                timestamp1_dt = DATE_ADD(STR_TO_DATE(@ts1,'%m/%d/%Y %T'),INTERVAL -3 HOUR),  <!--- substract 3 hours to translate time to PST from EST --->
		                status_int = 0,
		                statusTimeStamp_dt = now(),
		                importTimeStamp_dt = now(),
		                isProd_ti = <cfif isProd()>1<cfelse>0</cfif>;
		        </cfquery>

                <!---add file name etc to array that will be returned --->
                <cfset aFiles[qFiles.currentRow][1] = qFiles.Name>
                <cfset aFiles[qFiles.currentRow][2] = queryResults.recordcount>

                <!---move file to local processed folder--->
                <cffile action="move" source="#getRemoteInFolder()#\#qFiles.name#" destination="#getRemoteInFolder()#\processed\#qFiles.name#">

                <cfcatch type="any">

                    <!---move file to local error folder--->
                    <cffile action="move" source="#getRemoteInFolder()#\#qFiles.name#" destination="#getRemoteInFolder()#\error\#qFiles.name#">

                    <cfset SubjectLine = "FPL ERROR - UNICA VOICE - Error processing file #qFiles.name#">
                    <cfset ENA_Message = "Error in processing voice file #qFiles.name#. File moved to error folder (#getRemoteInFolder()#\error\)">
                    <cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                    <!---set error in struct--->
                    <cfset sReturn.ErrMsg = cfcatch.Message>
                    <cfset sReturn.ErrCode = -1>

                </cfcatch>

            </cftry>

        </cfloop>

        <cfset sReturn.Files = aFiles>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="VOICE_IMP", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>

    </cffunction>

    <!---function to import Voice data files from unica to EBM table simplexprogramdata.portal_link_c10_p1_unica_voice_files --->
	<cffunction name="sendCalls" access="remote" output="true" returnformat="JSON" >

        <cfset sReturn = getReturnStruct()>

	    <!---Verb must match that of request type--->
		<cfset local.sign = generateSignature("POST") />
        <cfset datetime = local.sign.DATE>
        <cfset signature = local.sign.SIGNATURE>
        <cfset authorization = getAccessKey() & ":" & signature>


		<!--- Get threads that aren't running. --->
        <cfquery name="qAvailableThreads" datasource="#getEBMDsn()#">
	        SELECT	ThreadId_int
	        FROM	simplexprogramdata.portal_link_c10_p1_threads
	        WHERE	IsRunning_int = 0
					AND
					ThreadType_ch = 'V'
	        ORDER BY ThreadId_int
        </cfquery>

       	<cfloop query="qAvailableThreads">

            <!---get top VoicePerThread records to send--->
            <cfquery name="qRecordsToSend" datasource="#getEBMDsn()#">
                SELECT 	v.*, m.batchId_bi, m.batchcode_vch
                FROM	simplexprogramdata.portal_link_c10_p1_unica_voice_files v
						left join simplexprogramdata.portal_link_c10_p1_voice_script_identifiers m on v.VoiceScriptIdentifier_vch = m.VoiceScriptIdentifier_vch
                WHERE	Status_int = 0
						<!--- AND v.VoiceScriptIdentifier_vch = '1515' --->
                ORDER BY PKID_bi asc
                LIMIT #variables.VoicePerThread#
            </cfquery>

            <!---if no unsent record found then exit the loop else start calling EBM API--->
            <cfif qRecordsToSend.recordcount EQ 0>
                <cfbreak>
            <cfelse>

                <!---Mark thread as currently in use--->
                <cfquery name="qEngageThread" datasource="#getEBMDsn()#">
                    UPDATE	simplexprogramdata.portal_link_c10_p1_threads
                    SET		IsRunning_int = 1,
                            LastRan_dt = now()
                    WHERE	ThreadId_int = #qAvailableThreads.ThreadId_int#
                </cfquery>

                <!--- update status of records i.e mark as "in queue" (Status_int: 0=not processed, 1=in queue, 2=processed, 3=exported--->
                <cfquery name="qUpdateSTatus" datasource="#getEBMDsn()#">
                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
                    SET		Status_int = 1
                    WHERE PKID_bi in (#valuelist(qRecordsToSend.PKID_bi,",")#)
                </cfquery>

                <!---call EBM api in thread to make the processing faster--->
                <cfthread name="fplthr#qAvailableThreads.ThreadId_int#" ThisThreadId="#qAvailableThreads.ThreadId_int#" ThisThreadQuery="#qRecordsToSend#" action="run">

                    <cfloop query="attributes.ThisThreadQuery">

                        <cftry>

							<cfif isnumeric(attributes.ThisThreadQuery.batchId_bi)>

								<!--- get the number --->
	                            <cfset Voicenumber = trim(attributes.ThisThreadQuery.PrimaryPhoneNumber_vch)>
								<cfset thisNum = "P">

								<!--- if number is invalid, get secondary number --->
								<cfif NOT isnumeric(Voicenumber) OR len(Voicenumber) NEQ 10>
									<cfset Voicenumber = trim(attributes.ThisThreadQuery.SecondaryPhoneNumber_vch)>
									<cfset thisNum = "S">
								</cfif>

	                            <!---if in whitelist or of whitelist is not enforced , call api else log this as error--->
	                            <cfif  NOT getEnforceWhiteList() OR  listFindNoCase(getVoiceWhiteList(),Voicenumber)>

	                                <cfif isnumeric(Voicenumber) and len(Voicenumber) EQ 10>

	                                	<cfquery name="qCurrRow" dbtype="query">
		                                	SELECT * FROM attributes.ThisThreadQuery WHERE PKID_BI = #attributes.ThisThreadQuery.PKID_bi#
	                                	</cfquery>

										<cfif attributes.ThisThreadQuery.batchcode_vch EQ "O">
											<cfset thisControlSTring = application.outage.getControlString(qCurrRow)>
										<cfelseif attributes.ThisThreadQuery.batchcode_vch EQ "M">
											<cfset thisControlSTring = application.maintenance.getControlString(qCurrRow)>
										<cfelseif attributes.ThisThreadQuery.batchcode_vch EQ "ME">
											<cfset thisControlSTring = application.meas.getControlString(qCurrRow)>
										<cfelse>
											<cfset thisControlSTring = "">
										</cfif>


				                        <cfhttp url="#getEBMApiUrlBase()#/ebm/pdc/addtorealtime" method="POST" result="returnStruct" >

				                           <!--- By default EBM API will return json or XML --->
				                           <cfhttpparam name="Accept" type="header" value="application/json" />
				                           <cfhttpparam type="header" name="datetime" value="#datetime#" />
				                           <cfhttpparam type="header" name="authorization" value="#authorization#" />

				                           <!--- Batch Id controls which pre-defined campaign to run --->
				                           <!---
				                           FPL campaign = 1546
				                           MEAS compaign = 1515
				                            --->
				                           <cfhttpparam type="formfield" name="inpBatchId" value="#attributes.ThisThreadQuery.batchId_bi#" />

				                           <!--- 1=voice 2=email 3=SMS--->
				                           <cfhttpparam type="formfield" name="inpContactTypeId" value="1" />

				                           <!--- Contact string--->
				                           <cfhttpparam type="formfield" name="inpContactString" value="#Voicenumber#" />

				                           <cfif thisControlSTring NEQ "">
					                           <cfhttpparam type="formfield" name="inpXMLControlString" value="#thisControlSTring#" />
										   </cfif>

				                        </cfhttp>


	 	                                <cfset resultArray = deserializeJSON(returnStruct.Filecontent)>

	<!---	                                <CFSAVECONTENT Variable="VVV">
			                                <CFDUMP VAR="#resultArray#">
			                                <CFDUMP VAR="#returnStruct.Filecontent#">
		                                </CFSAVECONTENT> --->

		                                <!---RXRESULTCODE will be positive for success and negative for failiure--->
		                                <cfif extractAPIResultEle(resultArray,"RXRESULTCODE") EQ 1>
		                                    <!---update with distribution id so we can query for response later on for each Voice transaction--->
		                                    <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
		                                        UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
		                                        SET		DTSID_bi = #extractAPIResultEle(resultArray,"DISTRIBUTIONID")#,
				                                        RequestUUID_vch = '#extractAPIResultEle(resultArray,"REQUESTUUID")#',
		                                                ResponseMessage_vch = 'Success',
		                                                ResponseMessageTimestamp_dt = now(),
		                                                Status_int = 2,  <!---processed--->
		                                                statusTimeStamp_dt = now()
		                                        WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
		                                    </cfquery>
		                                <cfelse>
		                                    <!---upadte with faliure--->
		                                    <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
		                                        UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
		                                        SET		DTSID_bi = -1,
				                                        RequestUUID_vch = '#extractAPIResultEle(resultArray,"REQUESTUUID")#',
		                                                ResponseMessage_vch = '#extractAPIResultEle(resultArray,"MESSAGE")# #extractAPIResultEle(resultArray,"ERRMESSAGE")#',
		                                                ResponseMessageTimestamp_dt = now(),
		                                                Status_int = 2, <!---processed--->
		                                                statusTimeStamp_dt = now()
		                                        WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
		                                    </cfquery>
		                                </cfif>

	                                <cfelse>

	                                    <cflog
	                                        text = "PKID_bi=#attributes.ThisThreadQuery.PKID_bi#  PrimaryPhoneNumber_vch=#attributes.ThisThreadQuery.PrimaryPhoneNumber_vch# TextMessage_vch:#attributes.ThisThreadQuery.TextMessage_vch#"
	                                        application = "yes"
	                                        file = "FPL_API_Call_Error"
	                                        type = "warning">

		                                <!---upadte with faliure--->
		                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
		                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
		                                    SET		DTSID_bi = -1,
				                                    RequestUUID_vch = '-1',
		                                            ResponseMessage_vch = 'Invalid Phone Number(s)',
		                                            ResponseMessageTimestamp_dt = now(),
		                                            Status_int = 2, <!---processed--->
		                                            statusTimeStamp_dt = now()
		                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
		                                </cfquery>

	                                </cfif>

	                            <cfelse> <!---if not in whitelist--->

	                                <!---upadte with faliure--->
	                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
	                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
	                                    SET		DTSID_bi = -1,
			                                    RequestUUID_vch = '-1',
	                                            ResponseMessage_vch = 'Phone number not in whitelist',
	                                            ResponseMessageTimestamp_dt = now(),
	                                            Status_int = 2, <!---processed--->
	                                            statusTimeStamp_dt = now()
	                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
	                                </cfquery>

	                            </cfif>

							<cfelse>

                                <!---upadte with faliure--->
                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
                                    SET		DTSID_bi = -1,
		                                    RequestUUID_vch = '-1',
                                            ResponseMessage_vch = 'Invalid/Missing Voice Identifier',
                                            ResponseMessageTimestamp_dt = now(),
                                            Status_int = 2, <!---processed--->
                                            statusTimeStamp_dt = now()
                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
                                </cfquery>

							</cfif>

                            <cfcatch>
                                <!---If there is exception, mark Voice msg as failed and move on--->
                                <cfquery name="qrySuccess" datasource="#getEBMDsn()#">
                                    UPDATE 	simplexprogramdata.portal_link_c10_p1_unica_voice_files
                                    SET		DTSID_bi = -1,
											RequestUUID_vch = '-1',
                                            ResponseMessage_vch = 'Failed',
                                            ResponseMessageTimestamp_dt = now(),
                                            Status_int = 2, <!---processed--->
                                            statusTimeStamp_dt = now()
                                    WHERE	PKID_bi =  #attributes.ThisThreadQuery.PKID_bi#
                                </cfquery>

                                <cfset SubjectLine = "FPL ERROR - UNICA VOICE - Error sending Voice">
                                <cfset ENA_Message = "Error in processing file: #ThisThreadQuery.InFileName_vch# and PKID: #ThisThreadQuery.PKID_bi#">
                    			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                            </cfcatch>

                            <cffinally>
                               <!--- exception or not please release the thread--->
                                <cfquery name="qReleaseThread" datasource="#getEBMDsn()#">
                                    UPDATE	simplexprogramdata.portal_link_c10_p1_threads
                                    SET		IsRunning_int = 0,
                                            LastRan_dt = now()
                                    WHERE	ThreadId_int = #attributes.ThisThreadId#
                                </cfquery>
                            </cffinally>

                        </cftry>

                    </cfloop>

                </cfthread>

            </cfif> <!---end of no records found condition--->

        </cfloop>

        <cfreturn SerializeJSON(sReturn)>

    </cffunction>

	<!--- function to export Voice resposne data to FTP site --->
	<cffunction name="exportData" access="remote" output="true" returnformat="JSON">

		<cfset sReturn = getReturnStruct()>

		<cfset strColumns = "transaction_id|channel|pfp_id|ReasonCode|ReasonDescription|timestamp1|timestamp2|timestamp3|account_no|premise|primary_phone_number|secondary_phone_number|campaigncode|email_address|custom1|custom2|custom3|custom4|custom5|custom6|custom7|custom8|custom9|custom10|custom11|custom12|custom13|custom14|custom15">


		<!---get all the records that are at least 1 hour 15 min old and have not been exporeted yet--->
		<cfquery name="qExport" datasource="#getEBMDsn()#">
			SELECT TRANSID_vch as transaction_id,
					channel_vch as channel,
					pfpid_bi as pfp_id,
					'0' AS ReasonCode,
					'' AS ReasonDescription,
					DATE_ADD(timestamp1_dt,INTERVAL 3 HOUR) as timestamp1,
					DATE_ADD(responsemessagetimestamp_dt,INTERVAL 3 HOUR) AS timestamp2,
					DATE_ADD(now(),INTERVAL 3 HOUR) as timestamp3,
					accountno_vch as account_no,
					premise_vch as premise,
					primaryphonenumber_vch as primary_phone_number,
					secondaryphonenumber_vch as secondary_phone_number,
					campaigncode_vch as campaigncode,
					emailaddress_vch as email_address,
					custom1_vch as custom1,
					custom2_vch as custom2,
					custom3_vch as custom3,
					custom4_vch as custom4,
					custom5_vch as custom5,
					custom6_vch as custom6,
					custom7_vch as custom7,
					custom8_vch as custom8,
					custom9_vch as custom9,
					custom10_vch as custom10,
					custom11_vch as custom11,
					custom12_vch as custom12,
					custom13_vch as custom13,
					custom14_vch as custom14,
					custom15_vch as custom15,
					InFileName_vch,
					PKID_bi,
					RequestUUID_vch,
					status_int
			FROM   	simplexprogramdata.portal_link_c10_p1_unica_voice_files U
			WHERE	status_int <> 3
					and U.StatusTimeStamp_dt < date_add(now(), interval -65 MINUTE)
					and isProd_ti = <cfif isProd()>1<cfelse>0</cfif>
			ORDER BY InFileName_vch asc, PKID_bi asc
		</cfquery>

		<!---Array to return filenames and number of records exported --->
        <cfset aFiles = arraynew(2)>
		<cfset counter = 1>

		<cfoutput group="InFileName_vch" query="qExport">

			<!---get records for one file at a time--->
		    <cfquery name="qOneFile" dbtype="query">
		    	SELECT 	transaction_id,channel,pfp_id,ReasonCode,ReasonDescription,timestamp1,timestamp2,timestamp3,account_no,
		        		premise,primary_phone_number,secondary_phone_number,campaigncode,email_address,custom1,custom2,custom3,custom4,custom5,custom6,custom7,custom8,custom9,custom10,custom11,custom12,custom13,custom14,custom15, pkid_bi,RequestUUID_vch,status_int
		        FROM 	qExport
		        WHERE	InFileName_vch = '#qExport.InFileName_vch#'
		        ORDER BY PKID_bi asc
		    </cfquery>

		    <cftry>

			   <!---  lets make sure that all the records are processed. If there is any 0 or 1 status, dont process this file yet --->
			   <cfif listFindNoCase(valuelist(qOneFile.status_int),0) EQ 0 AND listFindNoCase(valuelist(qOneFile.status_int),1) EQ 0 >

		 			<cfloop query="qOneFile">

						<cfif qOneFile.RequestUUID_vch NEQ "-1">
						    <cfquery name="qCR" datasource="#getEBMDsn()#">
							    SELECT CallResult_int
							    FROM simplexresults.contactresults
							    WHERE DTS_UUID_vch = '#qOneFile.RequestUUID_vch#'
								ORDER BY CallResultTS_dt desc
								limit 1
							</cfquery>

							<cfset CRCode = qCR.CallResult_int>

						<cfelse>

							<cfset CRCode = 6>

						</cfif>

						<cfswitch expression="#CRCode#">
							<cfcase value="6"> <!--- Error --->
								<cfset responseCode = 6>
								<cfset response = "Call Error">
							</cfcase>
							<cfcase value="3,5"> <!--- live --->
								<cfset responseCode = 1>
								<cfset response = "Live Answer">
							</cfcase>
							<cfcase value="4"> <!--- Answering Machine --->
								<cfset responseCode = 2>
								<cfset response = "Answering Machine">
							</cfcase>
							<cfcase value="7"> <!--- Busy --->
								<cfset responseCode = 3>
								<cfset response = "Busy">
							</cfcase>
							<cfcase value="9"> <!--- Fax --->
								<cfset responseCode = 4>
								<cfset response = "Fax">
							</cfcase>
							<cfcase value="10"> <!--- No Ans --->
								<cfset responseCode = 5>
								<cfset response = "No Answer">
							</cfcase>
							<cfdefaultcase> <!--- Unknown --->
								<cfset responseCode = 7>
								<cfset response = "Unknown">
							</cfdefaultcase>

						</cfswitch>


						<cfset qOneFile.ReasonCode = responseCode>
						<cfset qOneFile.ReasonDescription = response>

					</cfloop>


					<!---generate file--->
			        <cfset strCSV = CSVFormat(qOneFile,"",strColumns)>

			        <!---save file to out folder--->
			        <cffile action="write" file="#getRemoteOutFolder()#\#qExport.InFileName_vch#" output="#strCSV#" addnewline="no">

			        <cfif qOneFile.recordcount GT 0>

			            <!---Update flag in table so we dont end up exporting again --->
			            <cfquery name="qUpdateFlag" datasource="#getEBMDsn()#">
			                UPDATE	simplexprogramdata.portal_link_c10_p1_unica_voice_files
			                SET		Status_int = 3,
			                        StatusTimeStamp_dt = now(),
			                        ExportTimeStamp_dt = now()
			                WHERE	pkid_bi IN (#valuelist(qOneFile.pkid_bi)#)
			            </cfquery>

			        </cfif>

	                <!---add file name etc to array that will be returned --->
	                <cfset aFiles[counter][1] = qExport.InFileName_vch>
	                <cfset aFiles[counter][2] = qOneFile.recordcount>
					<cfset counter = counter + 1>

				</cfif> <!--- end if there is any unprocessed record check --->

				<cfcatch type="any">

		            <cfset SubjectLine = "FPL ERROR - UNICA VOICE - Error Exporting file #qExport.InFileName_vch#">
		            <cfset ENA_Message = "Error while exporting VOICE results to out folder">
           			<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                    <!---set error in struct--->
                    <cfset sReturn.ErrMsg = cfcatch.Message>
                    <cfset sReturn.ErrCode = -1>

		        </cfcatch>

			</cftry>

		</cfoutput>

		<cfset sReturn.Files = aFiles>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="VOICE_EXP", serviceData = serializeJSON(sReturn))>

		<cfreturn SerializeJSON(sReturn)>

	</cffunction>




</cfcomponent>
