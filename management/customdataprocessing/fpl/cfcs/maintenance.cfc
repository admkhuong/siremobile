<!---
  --- maintenance
  --- -----------
  ---
  --- author: ahasan
  --- date:   5/7/15
  --->
<cfcomponent accessors="true" output="false" persistent="true">

	<cfproperty name="maintenanceLibId" type="numeric" default="3">

	<cfproperty name="TTSLibId" type="numeric" default="1">
	<cfproperty name="TTSLibIdSpan" type="numeric" default="3">

	<cfproperty name="TTSLibName" type="string" default="VW Paul">
	<cfproperty name="TTSLibNameSpan" type="string" default="IVONA OEM NextUp OpenSAPI miguel22">

	<cfproperty name="pauseEleId" type="numeric" default="24">

	<cfproperty name="commonEleIdEng" type="numeric" default="8">
	<cfproperty name="commonEleIdSpan" type="numeric" default="9">

    <cfproperty name="singleDigitEleIdEng" type="numeric" default="19">
    <cfproperty name="doubleDigitEleIdEng" type="numeric" default="12">

    <cfproperty name="singleDigitEleIdSpan" type="numeric" default="11">
    <cfproperty name="doubleDigitEleIdSpan" type="numeric" default="13">

    <cfproperty name="100DataIdEng" type="numeric" default="103">
    <cfproperty name="1000DataIdEng" type="numeric" default="105">

    <cfproperty name="100DataIdSpan" type="numeric" default="103">
    <cfproperty name="1000DataIdSpan" type="numeric" default="106">

    <cfproperty name="andDataIdEng" type="numeric" default="102">
    <cfproperty name="AMDataIdEng" type="numeric" default="101">
    <cfproperty name="PMDataIdEng" type="numeric" default="104">

    <cfproperty name="andDataIdSpan" type="numeric" default="102">
    <cfproperty name="AMDataIdSpan" type="numeric" default="101">
    <cfproperty name="PMDataIdSpan" type="numeric" default="104">

    <cfproperty name="DaysEleIdEng" type="numeric" default="20">
    <cfproperty name="MonthsEleIdEng" type="numeric" default="22">

    <cfproperty name="DaysEleIdSpan" type="numeric" default="21">
    <cfproperty name="MonthsEleIdSpan" type="numeric" default="23">

	<cfproperty name="outageCCDString" type="string" default="<CCD CID='8002263545' ESI='-14' DRD='0' RMin='2' PTL='1' PTM='1'>0</CCD>">

	<!--- init all properties/values --->
	<cffunction name="init" access="public" returntype="any">


		<cfreturn this>
	</cffunction>

	<!--- cleanup a phonenumber --->
	<cffunction name="cleanupPhoneNumber" access="private" output="false">
		<cfargument name="inpNumber" required="true">

		<cfreturn reReplaceNoCase(arguments.inpNumber,"[^0-9]","","all")>

	</cffunction>

	<!--- induce space after each char/number so TTS wont read a number as 4 million 45 thousands....... instead reads it one number a time. --->
	<cffunction name="induceSpace" access="private" output="false">
		<cfargument name="sourceString" type="string" required="true">

		<cfset var thisString = "">

		<cfloop index="i" from="1" to="#len(arguments.sourceString)#">
			<cfset thisString = thisString & mid(arguments.sourceString,i,1) & " ">
		</cfloop>

		<cfreturn thisString>
	</cffunction>

	<!--- generate XML for single digit numbers --->
	<cffunction name="getSingleDigitXML" access="public" output="false">
		<cfargument name="sourceNumber" type="string" required="false" default="">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfif isnumeric(sourceNumber)>

			<cfloop index="i" from="1" to="#len(arguments.sourceNumber)#">

				<!--- digit --->
				<cfif arguments.language EQ "E">
					<cfset thisXML = thisXML & "<ELE ID='#getSingleDigitEleIdEng()#'>#int(MID(sourceNumber, i, 1))+1#</ELE>">
				<cfelse>
					<cfset thisXML = thisXML & "<ELE ID='#getSingleDigitEleIdSpan()#'>#int(MID(sourceNumber, i, 1))+1#</ELE>">
				</cfif>

				<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">

			</cfloop>

		<cfelse>

			<cfif arguments.language EQ "E">
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#induceSpace(arguments.sourceNumber)#</ELE>">
			<cfelse>
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#induceSpace(arguments.sourceNumber)#</ELE>">
			</cfif>

		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!--- generate XML for double digit numbers --->
	<cffunction name="getDoubleDigitXML" access="public" output="false">
		<cfargument name="sourceNumber" type="string" required="false" default="">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfif isnumeric(arguments.sourceNumber)>

			<cfswitch expression="#len(arguments.sourceNumber)#">
				<cfcase value="1,2">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(arguments.sourceNumber)+1#</ELE>">
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(arguments.sourceNumber)+1#</ELE>">
					</cfif>
				</cfcase>
				<cfcase value="3">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(getAndDataIdEng())#</ELE>"> <!--- and --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(getAndDataIdSpan())#</ELE>"> <!--- and --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="4">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfif int(mid(arguments.sourceNumber,2,1)) GT 0>
							<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,2,1))+1#</ELE>"> <!--- 100th number --->
							<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
							<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
							<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						</cfif>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfif int(mid(arguments.sourceNumber,2,1)) GT 0>
							<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,2,1))+1#</ELE>"> <!--- 100th number --->
							<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
							<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
							<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						</cfif>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="5">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,2))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,3,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,2))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,3,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="6">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,2,2))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,3,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))+1#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,2,2))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,3,1))+1#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))+1#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfdefaultcase>
						<cfset thisXML = getSingleDigitXML(arguments.sourceNumber,arguments.language)>
				</cfdefaultcase>
			</cfswitch>

		<cfelse>

			<cfif arguments.language EQ "E">
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.sourceNumber#</ELE>">
			<cfelse>
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.sourceNumber#</ELE>">
			</cfif>

		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!---
	The Est_Rest_Time has two formats:
	1.	If the estimated restoration time is the same day (today), then it�s hh:mm am/pm with leading zeroes removed for the hh. For example 1:45 pm.
	2.	If the estimated restoration time is for a future date (tomorrow or further) then it is in the format mm/dd hh:mm am/pm with leading zeros removed from the mm, dd, and hh. For example 12/5 10:00 am, or 1/25 8:00 pm.
	 --->
	<cffunction name="getDateTimeXML" access="public" output="false">
		<cfargument name="dateTimeString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">
		<cfargument name="formatType" type="string" required="false" default="DT"> <!--- DT=datetime, D=date, T=time --->

		<cfset var thisXML = "">
		<cfset var arrDateTime = "">

		<cfif isDate(arguments.dateTimeString)>

<!--- 			<cfset arrDateTime = listToArray(arguments.dateTimeString," ",true)	>

			<cfif arrayLen(arrDateTime) EQ 1>	<!--- it was just date mm/dd --->
				<cfset thisXML = getDateXML(arrDateTime[1],arguments.language)>
			<cfelseif arrayLen(arrDateTime) EQ 2>	<!--- it was just time i.e hh:mm am --->
				<cfset thisXML = getTimeXML(arrDateTime[1] & " " & arrDateTime[2],arguments.language)>
			<cfelseif arrayLen(arrDateTime) EQ 3>	<!--- it was date and time i.e. format mm/dd/yyyy hh:mm am --->
				<cfset thisXML = getDateXML(arrDateTime[1],arguments.language)>
				<cfset thisXML = thisXML & getTimeXML(arrDateTime[2] & " " & arrDateTime[3],arguments.language)>
			</cfif> --->

			<cfif arguments.formatType EQ "D">
				<cfset thisXML = getDateXML(dateformat(arguments.dateTimeString,'mm/dd/yyyy'),arguments.language)>
			<cfelseif arguments.formatType EQ "T">
				<cfset thisXML = getTimeXML(timeFormat(arguments.formatType,'hh:mm tt'),arguments.language)>
			<cfelse>
				<cfset thisXML = getDateXML(dateformat(arguments.dateTimeString,'mm/dd/yyyy'),arguments.language)>
				<cfset thisXML = thisXML & getTimeXML(timeFormat(arguments.formatType,'hh:mm tt'),arguments.language)>
			</cfif>

		<cfelse>

			<cfif arguments.language EQ "E">
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.dateTimeString#</ELE>">
			<cfelse>
				<cfset thisXML = "<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.dateTimeString#</ELE>">
			</cfif>

		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!--- get XML for date --->
	<cffunction name="getDateXML" access="public" output="false">
		<cfargument name="dateString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfset var aDT = listToArray(arguments.dateString,'/',false)>

		<cfif len(dateString)>
			<cfif arguments.language EQ "E">
				<cfset thisXML = thisXML & "<ELE ID='#getMonthsEleIdEng()#'>#int(aDT[1])#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getDaysEleIdEng()#'>#int(aDT[2])#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfif arraylen(aDT) EQ 3>
					<cfset thisXML = thisXML & getDoubleDigitXML(aDT[3],'E')>
					<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				</cfif>
			<cfelse>
<!--- 				<cfset thisXML = thisXML & "<ELE ID='#getMonthsEleIdSpan()#'>#int(aDT[1])#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getDaysEleIdSpan()#'>#int(aDT[2])#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfif arraylen(aDT) EQ 3>
					<cfset thisXML = thisXML & getDoubleDigitXML(aDT[3],'S')>
					<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				</cfif> --->
				<cfset thisXML = thisXML & "<ELE ID='#getDaysEleIdSpan()#'>#int(aDT[2])#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
 				<cfset thisXML = thisXML & "<ELE ID='#getMonthsEleIdSpan()#'>#int(aDT[1])#</ELE>">
				<cfset thisXML = thisXML & "<ELE  DESC='del' ID='#getCommonEleIdSpan()#'>8</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfif arraylen(aDT) EQ 3>
					<cfset thisXML = thisXML & getDoubleDigitXML(aDT[3],'S')>
					<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				</cfif>
			</cfif>
		</cfif>


		<cfreturn thisXML>

	</cffunction>

	<!--- get XML for time --->
	<cffunction name="getTimeXML" access="public" output="false">
		<cfargument name="timeString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">
		<cfset var arrTime = listToArray(arguments.timeString," ",true)>   <!--- first element of array has hh:mm and second element has am or pm --->

		<cfif arguments.language EQ "E">

			<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],1,':')),arguments.language) >
			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

			<cfif int(listgetat(arrTime[1],2,':')) GT 0>
				<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],2,':')),arguments.language) >
			</cfif>

			<cfif ucase(arrTime[2]) EQ "AM">
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#getAMDataIdEng()#</ELE>">
			<cfelse>
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#getPMDataIdEng()#</ELE>">
			</cfif>

			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

		<cfelse>

			<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],1,':')),arguments.language) >
			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

			<cfif int(listgetat(arrTime[1],2,':')) GT 0>
				<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],2,':')),arguments.language) >
			</cfif>

			<cfif ucase(arrTime[2]) EQ "AM">
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#getAMDataIdSpan()#</ELE>">
			<cfelse>
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#getPMDataIdSpan()#</ELE>">
			</cfif>

			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->


		</cfif>


		<cfreturn thisXML>

	</cffunction>

	<!--- get numeric part of the street address to generate dynamic elements for "address starting with.." --->
	<cffunction name="getNumericAddress" access="public" output="false">
		<cfargument name="alphaNumericAddress" required="true" type="string">

		<cfset var numericAddress = "">

		<cfset var structNum = REFind("^[0-9]+[ ]*", arguments.alphaNumericAddress,1,"true")>
		<cfset var strPos = structNum['POS'][1]>
		<cfset var strLen = structNum['LEN'][1]>

		<cfif strPos GT 0>
			<cfset numericAddress = trim(mid(arguments.alphaNumericAddress,strPos,strLen)) >
		<cfelse>
			<cfset numericAddress = "">
		</cfif>

		<cfreturn numericAddress>

	</cffunction>

	<!--- get XML for common elements --->
	<cffunction name="getCommonElementXML" access="public" output="false">
		<cfargument name="ele" required="true" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfswitch expression="#ucase(arguments.ele)#">
			<cfcase value="FPL.COM">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='visit fpl dot com' ID='#getCommonEleIdEng()#'>6</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE  DESC='tambin puede visitar F P L punto com barra diagonal outage para ver actualizaciones' ID='#getCommonEleIdSpan()#'>6</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="OPTIN">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE  DESC='opt in msg' ID='#getCommonEleIdEng()#'>4</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE  DESC='para aceptar recibir mensajes...diagonal preferences' ID='#getCommonEleIdSpan()#'>3</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="REPEAT">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='repeat msg' ID='#getCommonEleIdEng()#'>5</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='para escuchar este mensaje de nuevo, presione hash' ID='#getCommonEleIdSpan()#'>4</ELE>">
				</cfif>
				<cfset thisXML = "">
			</cfcase>
			<cfcase value="HELLO">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='hello this is Florida Power and Light at 18004688243' ID='#getCommonEleIdEng()#'>2</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='hola es Florida Power and Light en el 18004688243' ID='#getCommonEleIdSpan()#'>2</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="ALTLANGUAGE">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='press 2 for english' ID='#getCommonEleIdEng()#'>1</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='para espanol' ID='#getCommonEleIdSpan()#'>5</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="THANKYOUGOODBYE">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='thankyou good bye' ID='#getCommonEleIdEng()#'>3</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='gracias adios' ID='#getCommonEleIdSpan()#'>1</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="THANKYOU">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='thankyou' ID='#getCommonEleIdEng()#'>7</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='gracias' ID='#getCommonEleIdSpan()#'>11</ELE>">
				</cfif>
			</cfcase>
		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

	<!--- method to deal with specific case of Uno and Una in TTS --->
	<cffunction name="FixSpanish" access="remote" output="false">
		<cfargument name="strSpanish" type="string" default="">

		<cfreturn REreplaceNoCase(arguments.strSpanish,'\b1[ ]+Hora','Una hora','all')>
	</cffunction>

	<cffunction name="getControlString" access="remote" output="false">
		<cfargument name="qData" type="query" default="">

		<cfset var controlString = "">
		<cfset var primaryNumber = "">
		<cfset var secondaryNumber = "">


		<cfif listfindnocase("1599,160102,160106,160109,160111,160113,160115,160117,160119,160121",arguments.qData.batchId_bi)><!---  call to begin with english language (English control string) --->

			<cfsavecontent variable="controlString">
			<cfoutput>
			<DM BS='0' DSUID='377' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
			<ELE ID='0'>0</ELE>
			</DM>
			<RXSS>
				<ELE BS='0' CK1='2' CK2='3' CK5='3' CP='' DESC='' LINK='-1' QID='1' RQ='' RXT='24' X='620.5' Y='94'/>
				<!--- English Live --->
				<ELE BS='1' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='-13,3' CK3='-13' CK4='(-13,2)(3,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='60' CP='0' DESC='' DI='1' DS='#getMaintenanceLibId()#' DSE='15' DSUID='377' LINK='0' QID='2' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >This message will repeat</ELE> --->
					<ELE DESC='this message will repeat' ID='8' >8</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
				</ELE>
				<!--- English Answering Machine --->
			    <ELE BS='1' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='0' DS='#getMaintenanceLibId()#' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='725' Y='250' >
					#stichDynamicMessage(arguments.qData,'AMENG')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >This message will repeat</ELE> --->
					<ELE DESC='this message will repeat' ID='8' >8</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'AMENG')#
			    </ELE>
			    <!--- Spanish Live --->
				<ELE BS='1' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='-13,2' CK3='-13' CK4='(-13,4)(2,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='60' CP='0' DESC='' DI='1' DS='#getMaintenanceLibId()#' DSE='15' DSUID='377' LINK='0' QID='4' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#' >Este mensaje se repetira</ELE> --->
					<ELE DESC='este mensaje se repetira' ID='9' >10</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
				</ELE>
			</RXSS>
			<CCD CID='8002263545' ESI='-14' DRD='2' RMin='30' PTL='1' PTM='1' ESC1='#qData.PrimaryPhoneNumber_vch#' ESC2='#qData.PrimaryPhoneNumber_vch#' ESC3='' ESC4='' ESC5='' >0</CCD>
			<!--- POVM='1' --->
			<!--- #getOutageCCDString()# --->
			</cfoutput>
			</cfsavecontent>

		</cfif>

		<cfif listfindnocase("1600,160103,160107,160110,160112,160114,160116,160118,160120,160122",arguments.qData.batchId_bi)> <!--- call to begin with spanish language (spanish control string) --->

			<cfsavecontent variable="controlString">
			<cfoutput>
			<DM BS='0' DSUID='377' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
			<ELE ID='0'>0</ELE>
			</DM>
			<RXSS>
				<ELE BS='0' CK1='2' CK2='3' CK5='3' CP='' DESC='' LINK='-1' QID='1' RQ='' RXT='24' X='620.5' Y='94'/>
				<!--- Spanish Live --->
				<ELE BS='1' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='-13,2' CK3='-13' CK4='(-13,2)(2,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='60' CP='0' DESC='' DI='1' DS='#getMaintenanceLibId()#' DSE='15' DSUID='377' LINK='0' QID='2' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#' >Este mensaje se repetira</ELE> --->
					<ELE DESC='este mensaje se repetira' ID='9' >10</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
				</ELE>
				<!--- Spanish Answering Machine --->
			    <ELE BS='1' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='0' DS='#getMaintenanceLibId()#' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='725' Y='250' >
					#stichDynamicMessage(arguments.qData,'AMSPAN')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#' >Este mensaje se repetira</ELE> --->
					<ELE DESC='este mensaje se repetira' ID='9' >10</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'AMSPAN')#
			    </ELE>
			    <!--- English Live --->
				<ELE BS='1' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='-13,3' CK3='-13' CK4='(-13,4)(3,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='60' CP='0' DESC='' DI='1' DS='#getMaintenanceLibId()#' DSE='15' DSUID='377' LINK='0' QID='4' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
					<ELE DESC='half second pause' ID='24' >1</ELE>
					<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >This message will repeat</ELE> --->
					<ELE DESC='this message will repeat' ID='8' >8</ELE>
					<ELE DESC='half second pause' ID='24' >1</ELE>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
				</ELE>
			</RXSS>
			<CCD CID='8002263545' ESI='-14' DRD='2' RMin='30' PTL='1' PTM='1' ESC1='#qData.PrimaryPhoneNumber_vch#' ESC2='#qData.PrimaryPhoneNumber_vch#' ESC3='' ESC4='' ESC5='' >0</CCD>
			<!--- POVM='1' --->
			<!--- #getOutageCCDString()# --->
			</cfoutput>
			</cfsavecontent>

		</cfif>


		<cfreturn controlString>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic --->
	<cffunction name="stichDynamicMessage" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfswitch expression="#arguments.qData.batchId_bi#">
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1599=89, 1600=99
custom1: language option
custom2: address
custom3: memo/phone number
custom4: future date
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1599,1600">
				<cfset numericAddress = getNumericAddress(arguments.qData.Custom2_vch)>
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
				        <ELE DESC='we are calling to...address starting with' ID='1' >4</ELE>
				        #getSingleDigitXML(numericAddress,'E')#
				        <ELE DESC='is in need of permanent r...contractor can call us at' ID='1' >1</ELE>
				        #getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom3_vch),'E')#
				        <ELE DESC='to schedule an appointmen...airs must be completed by' ID='1' >3</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'E','D')#
				        <ELE DESC='to avoid disconnection of your electric service.' ID='1' >2</ELE>
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
					        <ELE DESC='Thank you in advance for taking action to ensure the needed repairs are completed soon' ID='1' >6</ELE>
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='estamos llamando para inf...ireccion que comienza con' ID='28' >2</ELE>
				        #getSingleDigitXML(numericAddress,'S')#
				        <ELE DESC='requiere reparaciones per...icista pueden llamarnos a' ID='28' >3</ELE>
				        #getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom3_vch),'S')#
						<ELE DESC='para programar una cita p...ben completarse antes del' ID='28' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'S','D')#
						<ELE DESC='para evitar la desconexion de su servicio electrico' ID='28' >7</ELE>
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
					        <ELE DESC='am le agradecemos por...se completen pronto' ID='28' >9</ELE>
					        #getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160102=90, 160103=100
custom1: language option
custom2: address
custom3: future date
custom4: contact name
cusrom5: contact phone
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160102,160103">
				<cfset numericAddress = getNumericAddress(arguments.qData.Custom2_vch)>
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
				        <ELE DESC='we are calling again to r...the address starting with' ID='2' >6</ELE>
				        #getSingleDigitXML(numericAddress,'E')#
				        <ELE DESC='is in need of permanent r...airs must be completed by' ID='2' >3</ELE>
				        #getDateTimeXML(arguments.qData.Custom3_vch,'E','D')#
				        <ELE DESC='to continue your electric...epair the meter enclosure' ID='2' >4</ELE>
						<ELE DESC='if you have already made ...n appointment please call' ID='2' >5</ELE>
        				<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom4_vch#</ELE>
				        <ELE DESC='at' ID='2' >2</ELE>
   						#getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom5_vch),'E')#
        				<ELE DESC='as soon as possible to discuss the matter.Thank you' ID='2' >1</ELE>
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
				        <ELE DESC='estamos llamando de nuevo...ireccion que comienza con' ID='29' >2</ELE>
				        #getSingleDigitXML(numericAddress,'S')#
				        <ELE DESC='requiere reparaciones per...ben finalizarse antes del' ID='29' >4</ELE>
				        #getDateTimeXML(arguments.qData.Custom3_vch,'S','D')#
						<ELE DESC='para continuar recibiendo...del metrocontador' ID='29' >8</ELE>
						<ELE DESC='si ya ha hecho las repara...na cita por favor llame a' ID='29' >9</ELE>
						<!--- using english TTS for just CONTACT NAME on FPL request --->
        				<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'>#arguments.qData.Custom4_vch#</ELE>
				        <ELE DESC='al' ID='29' >10</ELE>
   						#getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom5_vch),'S')#
        				<ELE DESC='lo antes posible para hablar al respecto.gracias' ID='29' >11</ELE>
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160106=91, 160107=101
custom1: language option
custom2: contact name
custom3: contact phone
custom4: work order number /memo4
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160106,160107">
				<cfset numericAddress = getNumericAddress(arguments.qData.Custom2_vch)>
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
				        <ELE DESC='we apologize for any inconvenience...please call' ID='3' >7</ELE>
           				<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='at' ID='3' >3</ELE>
						#getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom3_vch),'E')#
        				<ELE DESC='and refer to work request' ID='3' >2</ELE>
						#getSingleDigitXML(arguments.qData.Custom4_vch,'E')#
						#getCommonElementXML('THANKYOU','E')#
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='nos disculpamos por...intente mover el' ID='30' >8</ELE>
						<!--- using english TTS for just CONTACT NAME on FPL request --->
           				<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'>#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='al' ID='30' >6</ELE>
						#getSingleDigitXML(cleanupPhoneNumber(arguments.qData.Custom3_vch),'S')#
						<ELE DESC='e indique la solicitud de trabajo' ID='30' >11</ELE>
						#getSingleDigitXML(arguments.qData.Custom4_vch,'S')#
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160109=92, 160110=102
custom1: language option
custom2: contact name
custom3: contact phone
custom4: work order number / memo1
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160109,160110">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='we completed permanent...seven business days' ID='4' >2</ELE>
						<cfif  arguments.msgType EQ "AMENG">
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						<ELE DESC='for questions call' ID='4' >3</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='at' ID='4'>4</ELE>
						#getSingleDigitXML(arguments.qData.Custom3_vch,'E')#
						<ELE DESC='and refer to work request' ID='4' >5</ELE>
						#getSingleDigitXML(arguments.qData.Custom4_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom4_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='hemos completado las...siete dias habiles' ID='31' >11</ELE>
						<cfif  arguments.msgType EQ "AMSPAN">
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						<ELE DESC='si tiene preguntas llame a' ID='31' >12</ELE>
						<!--- using english TTS for just CONTACT NAME on FPL request --->
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='al' ID='31' >4</ELE>
						#getSingleDigitXML(arguments.qData.Custom3_vch,'S')#
						<ELE DESC='e indique la solicitud de trabajo' ID='31' >7</ELE>
						#getSingleDigitXML(arguments.qData.Custom4_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom4_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160111=93, 160112=103
custom1: language option
custom2: work order number / memo1
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160111,160112">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='as part of our ongoing wo...and refer to work request' ID='5' >4</ELE>
						#getSingleDigitXML(arguments.qData.Custom2_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='como parte de...solicitud de trabajo' ID='32' >4</ELE>
						#getSingleDigitXML(arguments.qData.Custom2_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom2_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160113=94, 160114=104
custom1: language option
custom2: future date
custom3: memo1 hh:mm:ss
custom4: memo2 4 hours 30 minutes
custom5: contractor name
custom6: memo3 hours/minutes in english 4 Houras, 30 Minutos
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160113,160114">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='to provide you with safe...temporary power outage on' ID='6' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom2_vch,'E','D')#
						<ELE DESC='at' ID='6' >5</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom3_vch#</ELE>
						<ELE DESC='for' ID='6' >7</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom4_vch#</ELE>
						<ELE DESC='please understand this...questions please call' ID='6' >2</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom5_vch#</ELE>
						<ELE DESC='at' ID='6' >5</ELE>
						#getSingleDigitXML(arguments.qData.Custom6_vch,'E')#
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							<ELE DESC='am thanks for your patience' ID='6' >9</ELE>
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='para proporcionarle un...servicio electrico para el' ID='33' >2</ELE>
						#getDateTimeXML(arguments.qData.Custom2_vch,'S','D')#
						<ELE DESC='hora' ID='33' >9</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom3_vch#</ELE>
						<ELE DESC='por' ID='33' >12</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#fixSpanish(arguments.qData.Custom7_vch)#</ELE>
						<ELE DESC='por favor tenga...favor llame a' ID='33' >11</ELE>
						<!--- using english TTS for just CONTACT NAME on FPL request --->
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'>#arguments.qData.Custom5_vch#</ELE>
						<ELE DESC='al' ID='33' >5</ELE>
						#getSingleDigitXML(arguments.qData.Custom6_vch,'S')#
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							<ELE DESC='am gracias por su paciencia' ID='33' >7</ELE>
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160115=95, 160116=105
custom1: language option
custom2: future date
custom3: memo1 hh:mm:ss
custom4: memo2 4 hours 30 minutes
custom5: memo4 2344AA
custom6: memo3 hours/minutes in english 4 Houras, 30 Minutos
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160115,160116">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='to provide you with safe,...temporary power outage on' ID='7' >2</ELE>
						#getDateTimeXML(arguments.qData.Custom2_vch,'E','D')#
						<ELE DESC='at' ID='7' >3</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom3_vch#</ELE>
						<ELE DESC='for' ID='7' >4</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom4_vch#</ELE>
						<ELE DESC='please understand this is...and refer to workrequest' ID='7' >1</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom5_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='para proporcionarle un...servicio electrico para el' ID='34' >17</ELE>
						#getDateTimeXML(arguments.qData.Custom2_vch,'S','D')#
						<ELE DESC='hora' ID='34' >9</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom3_vch#</ELE>
						<ELE DESC='por' ID='34' >13</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#fixSpanish(arguments.qData.Custom6_vch)#</ELE>
						<ELE DESC='por favor tenga...solicitud de trabajo' ID='34' >16</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom5_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160117=96, 160118=106
custom1: language option
custom2: memo1 TTS such as: provide an additional source of power for the area
custom3: memo3 mm/dd/yyyy
custom4: memo4 mm/dd/yyyy
custom5: memo5 reference number 2344AA
custom6: CONTACT_NAME
custom7: CONTACT_PHONE
custom8: memo6 TTS SPANISH for memo1
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160117,160118">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='a project is scheduled in...ghts on this project will' ID='25' >2</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='work is scheduled for' ID='25' >3</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E','D')#
						<ELE DESC='through' ID='25' >11</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'E','D')#
						<ELE DESC='for questions call' ID='25' >5</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom6_vch#</ELE>
						<ELE DESC='at' ID='25' >6</ELE>
						#getSingleDigitXML(arguments.qData.Custom7_vch,'E')#
						<ELE DESC='and refer to work request' ID='25' >9</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom5_vch#</ELE> --->
						<ELE DESC='am thanks for your patience' ID='25' >13</ELE>
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='un proytecto esta...este proytecto sera' ID='35' >8</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom8_vch#</ELE>
						<ELE DESC='el trabajo comenzara el' ID='35' >9</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S','D')#
						<ELE DESC='hasta el' ID='35' >10</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'S','D')#
						<ELE DESC='si tiene preguntas llame a' ID='35' >12</ELE>
						<!--- using english TTS for just CONTACT NAME on FPL request --->
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'>#arguments.qData.Custom6_vch#</ELE>
						<ELE DESC='al' ID='35' >6</ELE>
						#getSingleDigitXML(arguments.qData.Custom7_vch,'S')#
						<ELE DESC='e indique la solicitud de trabajo' ID='35' >13</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom5_vch#</ELE> --->
						<ELE DESC='gracias por su paciencia' ID='35' >15</ELE>
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160119=97, 160120=107
custom1: language option
custom2: memo1 TTS such as: provide an additional source of power for the area
custom3: memo3 mm/dd/yyyy
custom4: memo4 mm/dd/yyyy
custom5: memo5 reference number 2344AA
custom6: memo4 TTS SPANISH
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160119,160120">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='an fpl approved contracto...line in your neighborhood' ID='26' >8</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE>
						<ELE DESC='work is scheduled for' ID='26' >11</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E','D')#
						<ELE DESC='through' ID='26' >10</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'E','D')#
						<ELE DESC='for questions call 180069...and refer to work request' ID='26' >9</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom5_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='pronto comenzaremos un...en su vecindario' ID='36' >9</ELE>
						<ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom6_vch#</ELE>
						<ELE DESC='el trabajo comenzara el' ID='36' >10</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S','D')#
						<ELE DESC='hasta el' ID='36' >12</ELE>
						#getDateTimeXML(arguments.qData.Custom4_vch,'S','D')#
						<ELE DESC='si tiene preguntas...solicitud de trabajo' ID='36' >13</ELE>
						#getSingleDigitXML(arguments.qData.Custom5_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom5_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160121=98, 160122=108
custom1: language option
custom2: memo1 reference number
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160121,160122">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='in order to provide you w...and refer to work request' ID='27' >1</ELE>
						#getSingleDigitXML(arguments.qData.Custom2_vch,'E')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibId()#' RXVNAME='#getTTSLibName()#'  >#arguments.qData.Custom2_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVEENG">
					        #getCommonElementXML('OPTIN','E')#
							#getCommonElementXML('REPEAT','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif (isnumeric(arguments.qData.Custom1_vch) AND int(arguments.qData.Custom1_vch) EQ 1) AND arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='para poder proporcionarle...solicitud de trabajo' ID='37' >2</ELE>
						#getSingleDigitXML(arguments.qData.Custom2_vch,'S')#
						<!--- <ELE DESC='' ID='TTS' RXVID='#getTTSLibIdSpan()#' RXVNAME='#getTTSLibNameSpan()#'  >#arguments.qData.Custom2_vch#</ELE> --->
				        <cfif  arguments.msgType EQ "LIVESPAN">
					        #getCommonElementXML('OPTIN','S')#
							#getCommonElementXML('REPEAT','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

</cfcomponent>
