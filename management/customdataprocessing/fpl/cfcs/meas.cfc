<!---
  --- meas
  --- ----
  ---
  --- author: ahasan
  --- date:   5/7/15
  --->
<cfcomponent accessors="false" output="false" persistent="true">

	<!--- init all properties/values --->
	<cffunction name="init" access="public" returntype="any">

		<cfreturn this>
	</cffunction>

	<cffunction name="getControlString" access="public" output="false">
		<cfargument name="qData" type="query" default="">

		<cfset var controlString = "">
		<cfset var primaryNumber = "">
		<cfset var secondaryNumber = "">


		<cfif isnumeric(arguments.qData.PrimaryPhoneNumber_vch) AND len(arguments.qData.PrimaryPhoneNumber_vch) EQ 10>
			<cfset primaryNumber = arguments.qData.PrimaryPhoneNumber_vch>

			<cfif trim(arguments.qData.Custom1_vch) NEQ "">
				<cfset primaryNumber = "#primaryNumber#P2X#arguments.qData.Custom1_vch#">
			</cfif>
		</cfif>

		<cfif isnumeric(arguments.qData.SecondaryPhoneNumber_vch) AND len(arguments.qData.SecondaryPhoneNumber_vch) EQ 10>
			<!--- if primary number is empty, then secondary number becomes primary --->
			<cfif primaryNumber EQ "">

				<cfset primaryNumber = arguments.qData.SecondaryPhoneNumber_vch>

				<cfif trim(arguments.qData.Custom2_vch) NEQ "">
					<cfset primaryNumber = "#primaryNumber#P2X#arguments.qData.Custom2_vch#">
				</cfif>

			<cfelse>

				<cfset secondaryNumber = arguments.qData.SecondaryPhoneNumber_vch>

				<cfif trim(arguments.qData.Custom2_vch) NEQ "">
					<cfset secondaryNumber = "#secondaryNumber#P2X#arguments.qData.Custom2_vch#">
				</cfif>

			</cfif>
		</cfif>

		<cfsavecontent variable="controlString">
		<cfoutput>
		<DM BS='0' DSUID='377' Desc='Description Not Specified' LIB='0' MT='1' PT='12' >
		<ELE ID='0'>0</ELE>
		</DM>
		<RXSS X='0' Y='0'>
		<ELE BS='0' CK1='2' CK2='3' CK5='-1' DESC='' LINK='-1' CP='' RQ='' QID='1' RXT='24' X='644.5' Y='138'></ELE>
		<!--- Spanish flag 1=start with spanish, 0 or else=start with english --->
		<cfif isnumeric(arguments.qData.Custom3_vch) AND int(arguments.qData.Custom3_vch) EQ 1>
			<ELE BS='0' CK1='10' CK2='-13,2' CK3='-13' CK4='(-13,2)(2,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CK10='' CK11='' CK12='' CK13='' DESC='' DI='3' DS='1' DSE='1' DSUID='377' LINK='0' CP='0' RQ='0' QID='2' RXT='2' X='495' Y='290' >0</ELE>
			<ELE BS='0' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='5' DS='1' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='765.5' Y='292' >0</ELE>
			<ELE BS='0' CK1='10' CK2='-13,3' CK3='-13' CK4='(-13,4)(3,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CK10='' CK11='' CK12='' CK13='' DESC='' DI='2' DS='1' DSE='1' DSUID='377' LINK='0' CP='0' RQ='0' QID='4' RXT='2' X='495' Y='290' >0</ELE>
		<cfelse>
			<ELE BS='0' CK1='10' CK2='-13,3' CK3='-13' CK4='(-13,2)(3,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CK10='' CK11='' CK12='' CK13='' DESC='' DI='2' DS='1' DSE='1' DSUID='377' LINK='0' CP='0' RQ='0' QID='2' RXT='2' X='495' Y='290' >0</ELE>
			<ELE BS='0' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='4' DS='1' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='765.5' Y='292' >0</ELE>
			<ELE BS='0' CK1='10' CK2='-13,2' CK3='-13' CK4='(-13,4)(2,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CK10='' CK11='' CK12='' CK13='' DESC='' DI='3' DS='1' DSE='1' DSUID='377' LINK='0' CP='0' RQ='0' QID='4' RXT='2' X='495' Y='290' >0</ELE>
		</cfif>
		</RXSS>
		<!--- different logic for escalation and redial in case of absence or presense of secondary number --->
		<cfif secondaryNumber EQ "" OR secondaryNumber EQ primaryNumber>
			<!--- <CCD CID='8884881624' ESI='-14' FileSeq='1' UserSpecData='' DRD='3' RMin='120' PTL='1' PTM='1' SCDPL='0' LMLA='1' RMB='0' RMF='' RMNA='' RMO='' SIM='' SSVMD='23' SSVMDIVR='26' POVM='' POVMSTR='' MMLS='' IRC='' MIRC='' ESC1='#primaryNumber#' ESC2='#primaryNumber#' ESC3='#primaryNumber#' ESC4='' ESC5='' CRNML='' CRNMR='' >0</CCD> --->
			<CCD CID='8884881624' ESI='-14' DRD='2' RMin='120' PTL='1' PTM='1' ESC1='#primaryNumber#' ESC2='#primaryNumber#' ESC3='' ESC4='' ESC5='' >0</CCD>
		<cfelse>
			<!--- <CCD CID='8884881624' ESI='-14' FileSeq='1' UserSpecData='' DRD='5' RMin='60' PTL='1' PTM='1' SCDPL='0' LMLA='1' RMB='0' RMF='' RMNA='' RMO='' SIM='' SSVMD='23' SSVMDIVR='26' POVM='' POVMSTR='' MMLS='' IRC='' MIRC='' ESC1='#secondaryNumber#' ESC2='#primaryNumber#' ESC3='#secondaryNumber#' ESC4='#primaryNumber#' ESC5='#secondaryNumber#' CRNML='' CRNMR='' >0</CCD> --->
			<CCD CID='8884881624' ESI='-14' DRD='5' RMin='60' PTL='1' PTM='1' ESC1='#secondaryNumber#' ESC2='#primaryNumber#' ESC3='#secondaryNumber#' ESC4='#primaryNumber#' ESC5='#secondaryNumber#' >0</CCD>
		</cfif>
		</cfoutput>
		</cfsavecontent>

		<cfreturn controlString>

	</cffunction>

</cfcomponent>