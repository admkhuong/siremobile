<!---
  --- nco
  --- ---
  ---
  --- author: ahasan
  --- date:   4/8/15
  --->
<cfcomponent accessors="true" output="true" persistent="true" extends="fpl">

	<!--- change time to eastern while sending records to FPL and NCO --->

	<!--- init all properties/values --->
	<cffunction name="init" access="public" returntype="any">

    	<cfset super.init(argumentCollection=arguments)>

		<cfreturn this>
	</cffunction>

	<!--- this method gets file from NCO (ftp), process them and add to table and then send any updates to FPL --->
	<cffunction name="getFilesFromNCO" access="remote" output="false">
		<cfset sReturn = getReturnStruct()>

		<!--- get and process files from NCO --->
		<cfset sReturn.importNCOFiles = DeserializeJSON(importNCOFiles())>
		<cfset sReturn.processNCOFiles = DeserializeJSON(processNCOFiles())>
		<cfset sReturn.sendTOFPL = DeserializeJSON(sendTOFPL())>

		<cfreturn SerializeJSON(sReturn)>

	</cffunction>

    <!---function to import NCO data files from unica to EBM table simplexprogramdata.portal_link_c10_p1_ncofiles --->
	<cffunction name="importNCOFiles" access="remote" output="false" returnformat="JSON" >

        <cfset sReturn = getReturnStruct()>

    	<!---get list of files from the server--->
	    <cfdirectory action="list" directory="#getremoteInFolderNCO()#" name="qFiles" filter="#getNCOFilePattern()#" sort="DATELASTMODIFIED ASC">


		<!---if files found, move to local folder for processing and rename original files--->
        <cfset aFiles = arraynew(2)>

        <cfloop query="qFiles">

            <cftry>

		        <cfquery name="qData" datasource="#getEBMDsn()#" result="queryResults">
		            LOAD DATA LOCAL INFILE  '#replacenocase(getremoteInFolderNCO(),"\","\\","all")#\\#qFiles.Name#'
		            INTO TABLE simplexprogramdata.portal_link_c10_p1_ncofiles
		            FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 0 LINES
		            (CustomerId_vch,PhoneNo_vch,@ts1,StatusCode_ch,@ts2,@ts3)
		              SET InFileName_vch = '#qFiles.Name#',
		                StatusOfferDate_dt = DATE_ADD(STR_TO_DATE(@ts1,'%m%d%Y'),INTERVAL -3 HOUR),
 		                StatusActionDate_dt = DATE_ADD(STR_TO_DATE(CONCAT(@ts2,' ',left(@ts3,2),':',mid(@ts3,3,2),':',right(@ts3,2)),'%m%d%Y %T'),INTERVAL -3 HOUR),
		                importTimeStamp_dt = now(),
		                StatusTimeStamp_dt = now();
		        </cfquery>

                <!---add file name etc to array that will be returned --->
                <cfset aFiles[qFiles.currentRow][1] = qFiles.Name>
                <cfset aFiles[qFiles.currentRow][2] = queryResults.recordcount>

                <!---move file to local processed folder--->
                <cffile action="move" source="#getremoteInFolderNCO()#\#qFiles.name#" destination="#getremoteInFolderNCO()#\processed\#qFiles.name#">

                <cfcatch type="any">

                    <!---move file to local error folder--->
                    <cffile action="move" source="#getremoteInFolderNCO()#\#qFiles.name#" destination="#getremoteInFolderNCO()#\error\#qFiles.name#">

                    <cfset SubjectLine = "FPL ERROR - NCO - Error processing file #qFiles.name#">
                    <cfset ENA_Message = "Error in importing NCO file #qFiles.name#. File moved to error folder (#getRemoteInFolder()#\error\)">
                    <cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

                    <!---set error in struct--->
                    <cfset sReturn.ErrMsg = cfcatch.Message>
                    <cfset sReturn.ErrCode = -1>

                </cfcatch>

            </cftry>

        </cfloop>

        <cfset sReturn.Files = aFiles>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="IMP_NCO_FILES", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>

    </cffunction>

	<!--- process all the NCO files i.e
	look for all record swith status = 0
	update accounts table for all these records
	make op-in or opt-out entires for all these records in EBM opt table--->
	<cffunction name="processNCOFiles" access="remote" output="false" returnformat="JSON" >

		<cfset var oFPlOpt = createObject('component','fplopt')>
		<cfset var oFPLAccount = createObject('component','fplaccount')>

        <cfset sReturn = getReturnStruct()>

		<!---get records to process --->
		<cfquery name="qUpdatedRecords" datasource="#getEBMDsn()#">
			SELECT 	*
			FROM	simplexprogramdata.portal_link_c10_p1_ncofiles
			WHERE	StatusId_int = 0
			ORDER BY InFileName_vch, PKId_bi
		</cfquery>

		<cfset sReturn.TotalRecords = qUpdatedRecords.recordcount>

		<cfset successCount = 0>

		<cfloop query="qUpdatedRecords">

			<cftransaction>

				<cftry>

					<!---set the var for collection notices--->
					<cfswitch expression="#qUpdatedRecords.StatusCode_ch#">
						<cfcase value="O,T"> <!---Opt in--->

							<cfset liCollection = 1>

							<!--- see if we need to initiate double optin --->
							<cfset s = structnew()>
							<cfset s.inpContactString = qUpdatedRecords.PhoneNo_vch>
							<cfset s.inpOptInSource = "NCO">
							<cfset s.inpOptInDate = qUpdatedRecords.StatusActionDate_dt>
							<cfset oFPlOpt.addOptInEntry(argumentCollection=s)>

						</cfcase>
						<cfcase value="X,V"> <!--- Opt out--->

							<cfset liCollection = 0>

							<cfset s = structnew()>
							<cfset s.inpContactString = qUpdatedRecords.PhoneNo_vch>
							<cfset s.inpOptOutSource = "NCO">
							<cfset s.inpOptOutDate = qUpdatedRecords.StatusActionDate_dt>
							<cfset oFPlOpt.addOptOutEntry(argumentCollection=s)>

						</cfcase>
						<cfcase value="D"> <!---Opt down--->
							<cfset liCollection = 0>
						</cfcase>
						<cfdefaultcase>
							<cfset liCollection = 0>
						</cfdefaultcase>
					</cfswitch>

					<!---INSERT or UPDATE NCO data into accounts table--->
			        <cfset oFPLAccount.mergeAccount(AccountId = qUpdatedRecords.CustomerId_vch,
						        								ContactString = qUpdatedRecords.PhoneNo_vch,
						        								Collection = liCollection,
						        								SendToFPL = 1,
						        								SendToFPLStatus = qUpdatedRecords.StatusCode_ch)>

					<!---Update record status in NCO import table so its not processed again--->
					<cfquery name="qNCORecord" datasource="#getEBMDsn()#">
						UPDATE 	simplexprogramdata.portal_link_c10_p1_ncofiles
						SET		StatusId_int = 1,
								StatusTimeStamp_dt = now()
						WHERE	PKID_bi = #qUpdatedRecords.PKID_bi#
					</cfquery>

					<cftransaction action="commit" />

					<cfset successCount = successCount + 1>

					<cfcatch>
						<cftransaction action="rollback">

	                    <cfset SubjectLine = "FPL ERROR - NCO - Error updating accounts">
	                    <cfset ENA_Message = "Error in processing file: #qUpdatedRecords.InFileName_vch# and PKID: #qUpdatedRecords.PKID_bi#">
	                    <cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

	                    <!---set error in struct--->
	                    <cfset sReturn.ErrMsg = cfcatch.Message>
	                    <cfset sReturn.ErrCode = -1>
					</cfcatch>

				</cftry>

			</cftransaction>

		</cfloop>

        <cfset sReturn.ModifiedSuccessfully = successCount>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="PROCESS_NCO_FILES", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>

	</cffunction>

	<!--- function to send new NCO updates to FPL. Look for records with status id=1 and send them to FPL
	using FPL's service and set the status id = 2fac' --->
	<cffunction name="sendTOFPL" access="remote" output="true" returnformat="JSON">
		<cfargument name="fromTimeStamp" default="#getVar('SENT_TO_FPL')#">

		<cfset var sReturn = getReturnStruct()>
		<cfset var oOpt = createObject('component','fplopt')>

		<cftry>

			<!--- get all Updated records --->
			<cfquery name="qUpdatedRecords" datasource="#getEBMDsn()#">
				SELECT 	*
				FROM 	simplexprogramdata.portal_link_c10_p1_ncofiles
				where  	StatusId_int = 1
						<!--- AND StatusTimeStamp_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now() --->
			</cfquery>


			<cfif qUpdatedRecords.recordcount GT 0>

				<cfloop query="qUpdatedRecords">

					<!--- post the updates to FPL. Call service with diff
					parameters depending on opt-in or opt-out --->
					<cfif qUpdatedRecords.StatusCode_ch EQ "O" OR  qUpdatedRecords.StatusCode_ch EQ "T">
						<cfset temp = oOpt.postToFPL(contactString=qUpdatedRecords.PHONENO_VCH,
														accountNo = qUpdatedRecords.CUSTOMERID_VCH,
														optIn=true,
														optDate=qUpdatedRecords.STATUSACTIONDATE_DT)>
					<cfelseif qUpdatedRecords.StatusCode_ch EQ "X" OR  qUpdatedRecords.StatusCode_ch EQ "V" >
						<cfset temp = oOpt.postToFPL(contactString=qUpdatedRecords.PHONENO_VCH,
														accountNo = qUpdatedRecords.CUSTOMERID_VCH,
														optIn=false)>
					</cfif>

				</cfloop>

				<!--- update nco files table --->
				<cfquery name="qUpdateRecords" datasource="#getEBMDsn()#">
					UPDATE	simplexprogramdata.portal_link_c10_p1_ncofiles
					SET		StatusId_int = 2,
							StatusTimeStamp_dt = now()
					WHERE	PKID_bi in (#valuelist(qUpdatedRecords.PKID_bi)#)
				</cfquery>

			</cfif>

			<!--- update last check timestamp in var table so next time we check new optins from here on--->
			<cfset setVar(VarName='SENT_TO_FPL',VarValue="#dateformat(now(),'yyyy-mm-dd')# #timeformat(now(),'HH:mm:ss')#")>

			<cfset sReturn.RecCount = qUpdatedRecords.Recordcount>


			<cfcatch>

				<cfset SubjectLine = "FPL ERROR - SEND_NCO_TO_FPL - Error while sending NCO updates to FPL on server #CGI.SERVER_NAME#">
				<cfset ENA_Message = "Error while checking sending NCO data to FPL at timestamp #arguments.fromTimeStamp#">
				<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

				<!---set error in struct--->
				<cfset sReturn.ErrMsg = cfcatch.Message>
				<cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="SEND_NCO_TO_FPL", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>
	</cffunction>

	<!--- function to look for records with SendToNCO flag turned on and send updates to FPL using the service they are hosting --->
	<cffunction name="sendTONCO" access="remote" output="true">
		<cfargument name="fromTimeStamp" default="#getVar('SENT_TO_NCO')#">

		<cfset var sReturn = getReturnStruct()>

		<cfset var strColumns = "AccountId_vch|PhoneNo_vch|OfferDate|StatusCode|StatusDate|StatusTime">

		<cftry>

			<!--- get all Updated records --->
			<cfquery name="qUpdatedRecords" datasource="#getEBMDsn()#">
				SELECT 	acc.AccountId_vch,
						acc.PhoneNo_vch,
				        DATE_FORMAT(date_add(now(),INTERVAL 3 HOUR),'%m%d%Y') as OfferDate,
				        CASE
							WHEN acc.SendToNCOStatus_vch IN ('IN','UC','OSC') AND acc.Collection_int =1 THEN 'O'
				            WHEN acc.SendToNCOStatus_vch = 'OUT' THEN 'X'
				            WHEN acc.SendToNCOStatus_vch IN ('UC','OSC','IN') AND acc.Collection_int = 0 THEN 'D'
				            ELSE 'U'
						END as StatusCode,
						DATE_FORMAT(date_add(IFNULL(acc.OptTimeStampToNCO_dt,UpdateTimeStamptoNCO_dt),INTERVAL 3 HOUR),'%m%d%Y') as StatusDate,
						DATE_FORMAT(date_add(IFNULL(acc.OptTimeStampToNCO_dt,UpdateTimeStamptoNCO_dt),INTERVAL 3 HOUR),'%H%i%s') as StatusTime,
				        acc.PKID_bi
				FROM	simplexprogramdata.portal_link_c10_p1_accounts acc
				<!--- where  	SendToNCO_int = 1	AND
						UpdateTimeStampToNCO_dt between <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.fromTimeStamp#"> AND now() --->
				where	<cfif hour(now()) LT 21 >  	<!--- NCO wants cumulative results from 12 midnight (EST) for each day --->
							<cfset previousDay = dateformat(dateAdd("d",-1,now()),"yyyy-mm-dd") & " 21:00:00">
							UpdateTimeStampToNCO_dt between '#previousDay#' AND now()
						<cfelse>
							UpdateTimeStampToNCO_dt between '#dateformat(now(),"yyyy-mm-dd")# 21:00:00' AND now()
						</cfif>
				ORDER BY PKID_bi asc
			</cfquery>

			<cfif qUpdatedRecords.recordcount GT 0>

				<!---generate file--->
		        <cfset strCSV = CSVFormat(qUpdatedRecords,"",strColumns,false,true)>

		        <!---save file to out folder--->
		        <cffile action="write" file="#getRemoteOutFolderNCO()#\MB_#dateformat(now(),'mmddyyyy')#_#timeformat(now(),'HHmmss')#.csv" output="#strCSV#" addnewline="no">

				<!--- update nco files table --->
				<cfquery name="qUpdateRecords" datasource="#getEBMDsn()#">
					UPDATE	simplexprogramdata.portal_link_c10_p1_accounts
					SET		SendToNCO_int = 0
					WHERE	PKID_bi in (#valuelist(qUpdatedRecords.PKID_bi)#)
				</cfquery>

			</cfif>

			<!--- update last check timestamp in var table so next time we check new optins from here on--->
			<cfset setVar(VarName='SENT_TO_NCO',VarValue="#dateformat(now(),'yyyy-mm-dd')# #timeformat(now(),'HH:mm:ss')#")>

			<cfset sReturn.RecCount = qUpdatedRecords.Recordcount>


			<cfcatch>

				<cfset SubjectLine = "FPL ERROR - SEND_FPL_TO_NCO - Error while sending FPL updates to NCO on server #CGI.SERVER_NAME#">
				<cfset ENA_Message = "Error while checking sending FPL data to NCO at timestamp #arguments.fromTimeStamp#">
				<cfset sendErrorNotification(ErrSubject = SubjectLine, ErrMsg = ENA_Message)>

				<!---set error in struct--->
				<cfset sReturn.ErrMsg = cfcatch.Message>
				<cfset sReturn.ErrCode = -1>

			</cfcatch>

		</cftry>

		<!---Lot it into table--->
		<cfset logServiceCall(serviceType="SEND_FPL_TO_NCO", serviceData = serializeJSON(sReturn))>

        <cfreturn SerializeJSON(sReturn)>
	</cffunction>

</cfcomponent>