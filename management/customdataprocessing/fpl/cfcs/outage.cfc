<!---
  --- outage
  --- ------
  ---
  --- author: ahasan
  --- date:   5/7/15
  --->
<cfcomponent accessors="true" output="false" persistent="true">

	<cfproperty name="fplLIbId" type="numeric" default="2">
	<cfproperty name="pauseEleId" type="numeric" default="2">

	<cfproperty name="commonEleIdEng" type="numeric" default="16">
	<cfproperty name="commonEleIdSpan" type="numeric" default="25">

    <cfproperty name="singleDigitEleIdEng" type="numeric" default="1">
    <cfproperty name="doubleDigitEleIdEng" type="numeric" default="3">
    <cfproperty name="singleDigitEleIdSpan" type="numeric" default="5">
    <cfproperty name="doubleDigitEleIdSpan" type="numeric" default="4">

    <cfproperty name="100DataIdEng" type="numeric" default="101">
    <cfproperty name="1000DataIdEng" type="numeric" default="100">
    <cfproperty name="100DataIdSpan" type="numeric" default="101">
    <cfproperty name="1000DataIdSpan" type="numeric" default="102">

    <cfproperty name="andDataIdEng" type="numeric" default="102">
    <cfproperty name="AMDataIdEng" type="numeric" default="103">
    <cfproperty name="PMDataIdEng" type="numeric" default="104">
    <cfproperty name="andDataIdSpan" type="numeric" default="103">
    <cfproperty name="AMDataIdSpan" type="numeric" default="104">
    <cfproperty name="PMDataIdSpan" type="numeric" default="105">


    <cfproperty name="DaysEleIdEng" type="numeric" default="10">
    <cfproperty name="MonthsEleIdEng" type="numeric" default="12">
    <cfproperty name="DaysEleIdSpan" type="numeric" default="23">
    <cfproperty name="MonthsEleIdSpan" type="numeric" default="24">

	<cfproperty name="outageCCDString" type="string" default="<CCD CID='8002263545' ESI='-14' DRD='1' RMin='2' PTL='1' PTM='1'>0</CCD>">


	<!--- init all properties/values --->
	<cffunction name="init" access="public" returntype="any">

		<cfreturn this>
	</cffunction>

	<!--- generate XML for single digit numbers --->
	<cffunction name="getSingleDigitXML" access="public" output="false">
		<cfargument name="sourceNumber" type="string" required="false" default="">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfif isnumeric(sourceNumber)>

			<cfloop index="i" from="1" to="#len(arguments.sourceNumber)#">

				<!--- digit --->
				<cfif arguments.language EQ "E">
					<cfset thisXML = thisXML & "<ELE ID='#getSingleDigitEleIdEng()#'>#int(MID(sourceNumber, i, 1))#</ELE>">
				<cfelse>
					<cfset thisXML = thisXML & "<ELE ID='#getSingleDigitEleIdSpan()#'>#int(MID(sourceNumber, i, 1))#</ELE>">
				</cfif>

				<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">

			</cfloop>

		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!--- generate XML for double digit numbers --->
	<cffunction name="getDoubleDigitXML" access="public" output="false">
		<cfargument name="sourceNumber" type="string" required="false" default="">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfif isnumeric(arguments.sourceNumber)>

			<cfswitch expression="#len(arguments.sourceNumber)#">
				<cfcase value="1,2">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#arguments.sourceNumber#</ELE>">
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#arguments.sourceNumber#</ELE>">
					</cfif>
				</cfcase>
				<cfcase value="3">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(getAndDataIdEng())#</ELE>"> <!--- and --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(getAndDataIdSpan())#</ELE>"> <!--- and --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="4">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,2,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,2,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="5">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,2))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,3,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,2))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,3,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfcase value="6">
					<cfif arguments.language EQ "E">
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,2,2))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get1000DataIdEng())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(mid(arguments.sourceNumber,3,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(get100DataIdEng())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					<cfelse>
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(left(arguments.sourceNumber,1))#</ELE>"> <!--- 1000th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,2,2))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get1000DataIdSpan())#</ELE>"> <!--- thousand --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(mid(arguments.sourceNumber,3,1))#</ELE>"> <!--- 100th number --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(get100DataIdSpan())#</ELE>"> <!--- hundred --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
						<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#int(right(arguments.sourceNumber,2))#</ELE>"> <!--- 10th numbers --->
						<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
					</cfif>
				</cfcase>
				<cfdefaultcase>
						<cfset thisXML = getSingleDigitXML(arguments.sourceNumber,arguments.language)>
				</cfdefaultcase>
			</cfswitch>

		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!---
	The Est_Rest_Time has two formats:
	1.	If the estimated restoration time is the same day (today), then it�s hh:mm am/pm with leading zeroes removed for the hh. For example 1:45 pm.
	2.	If the estimated restoration time is for a future date (tomorrow or further) then it is in the format mm/dd hh:mm am/pm with leading zeros removed from the mm, dd, and hh. For example 12/5 10:00 am, or 1/25 8:00 pm.
	 --->
	<cffunction name="getDateTimeXML" access="public" output="false">
		<cfargument name="dateTimeString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">
		<cfset var arrDateTime = listToArray(arguments.dateTimeString," ",true)>

		<cfif arrayLen(arrDateTime) EQ 1>	<!--- it was just date mm/dd --->
			<cfset thisXML = getDateXML(arrDateTime[1],arguments.language)>
		<cfelseif arrayLen(arrDateTime) EQ 2>	<!--- it was just time i.e hh:mm am --->
			<cfset thisXML = getTimeXML(arrDateTime[1] & " " & arrDateTime[2],arguments.language)>
		<cfelseif arrayLen(arrDateTime) EQ 3>	<!--- it was date and time i.e. format mm/dd/yyyy hh:mm am --->
			<cfset thisXML = getDateXML(arrDateTime[1],arguments.language)>
			<cfset thisXML = thisXML & getTimeXML(arrDateTime[2] & " " & arrDateTime[3],arguments.language)>
		</cfif>

		<cfreturn thisXML>

	</cffunction>

	<!--- get XML for date --->
	<cffunction name="getDateXML" access="public" output="false">
		<cfargument name="dateString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfif len(dateString)>
			<cfif arguments.language EQ "E">
				<cfset thisXML = thisXML & "<ELE ID='#getMonthsEleIdEng()#'>#int(listgetat(dateString,1,'/'))#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getDaysEleIdEng()#'>#int(listgetat(dateString,2,'/'))#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
			<cfelse>
				<cfset thisXML = thisXML & "<ELE ID='#getMonthsEleIdSpan()#'>#int(listgetat(dateString,1,'/'))#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
				<cfset thisXML = thisXML & "<ELE ID='#getDaysEleIdSpan()#'>#int(listgetat(dateString,2,'/'))#</ELE>">
				<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->
			</cfif>
		</cfif>


		<cfreturn thisXML>

	</cffunction>

	<!--- get XML for time --->
	<cffunction name="getTimeXML" access="public" output="false">
		<cfargument name="timeString" required="yes" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">
		<cfset var arrTime = listToArray(arguments.timeString," ",true)>   <!--- first element of array has hh:mm and second element has am or pm --->

		<cfif arguments.language EQ "E">

			<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],1,':')),arguments.language) >
			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

			<cfif int(listgetat(arrTime[1],2,':')) GT 0>
				<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],2,':')),arguments.language) >
			</cfif>

			<cfif ucase(arrTime[2]) EQ "AM">
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#getAMDataIdEng()#</ELE>">
			<cfelse>
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdEng()#'>#getPMDataIdEng()#</ELE>">
			</cfif>

			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

		<cfelse>

			<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],1,':')),arguments.language) >
			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->

			<cfif int(listgetat(arrTime[1],2,':')) GT 0>
				<cfset thisXML = thisXML & getDoubleDigitXML(int(listgetat(arrTime[1],2,':')),arguments.language) >
			</cfif>

			<cfif ucase(arrTime[2]) EQ "AM">
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#getAMDataIdSpan()#</ELE>">
			<cfelse>
				<cfset thisXML = thisXML & "<ELE ID='#getDoubleDigitEleIdSpan()#'>#getPMDataIdSpan()#</ELE>">
			</cfif>

			<cfset thisXML = thisXML & "<ELE ID='#getPauseEleId()#'>4</ELE>">	<!--- eighth second pause --->


		</cfif>


		<cfreturn thisXML>

	</cffunction>

	<!--- get numeric part of the street address to generate dynamic elements for "address starting with.." --->
	<cffunction name="getNumericAddress" access="public" output="false">
		<cfargument name="alphaNumericAddress" required="true" type="string">

		<cfset var numericAddress = "">

		<cfset var structNum = REFind("^[0-9]+[ ]*", arguments.alphaNumericAddress,1,"true")>
		<cfset var strPos = structNum['POS'][1]>
		<cfset var strLen = structNum['LEN'][1]>

		<cfif strPos GT 0>
			<cfset numericAddress = trim(mid(arguments.alphaNumericAddress,strPos,strLen)) >
		<cfelse>
			<cfset numericAddress = "">
		</cfif>

		<cfreturn numericAddress>

	</cffunction>

	<!--- get XML for common elements --->
	<cffunction name="getCommonElementXML" access="public" output="false">
		<cfargument name="ele" required="true" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfswitch expression="#ucase(arguments.ele)#">
			<cfcase value="FPL.COM">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='visit fpl dot com' ID='#getCommonEleIdEng()#'>3</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE  DESC='tambin puede visitar F P L punto com barra diagonal outage para ver actualizaciones' ID='#getCommonEleIdSpan()#'>3</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="OPTIN">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE  DESC='opt in msg' ID='#getCommonEleIdEng()#'>2</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE  DESC='para aceptar recibir mensajes...diagonal preferences' ID='#getCommonEleIdSpan()#'>2</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="REPEAT">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='repeat msg' ID='#getCommonEleIdEng()#'>1</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='para escuchar este mensaje de nuevo, presione hash' ID='#getCommonEleIdSpan()#'>7</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="HELLO">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='hello this is Florida Power and Light at 18004688243' ID='#getCommonEleIdEng()#'>4</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='hola es Florida Power and Light en el 18004688243' ID='#getCommonEleIdSpan()#'>4</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="ALTLANGUAGE">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='press 2 for english' ID='#getCommonEleIdEng()#'>5</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='para espanol' ID='#getCommonEleIdSpan()#'>5</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="THANKYOU">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='thankyou good bye' ID='#getCommonEleIdEng()#'>6</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='gracias adios' ID='#getCommonEleIdSpan()#'>6</ELE>">
				</cfif>
			</cfcase>
		</cfswitch>

		<cfreturn thisXML>

	</cffunction>


	<!--- get XML for Cause elements --->
	<cffunction name="getCauseXML" access="public" output="false">
		<cfargument name="ele" required="true" type="string">
		<cfargument name="language" type="string" required="false" default="E">

		<cfset var thisXML = "">

		<cfswitch expression="#ucase(arguments.ele)#">
			<cfcase value="0">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='0' ID='21' >1</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='0' ID='22' >1</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="1">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='1' ID='21' >2</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='1' ID='22' >2</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="2">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='2' ID='21' >3</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='2' ID='22' >3</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="3">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='3' ID='21' >4</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='3' ID='22' >4</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="5">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='5' ID='21' >5</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='5' ID='22' >5</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="7">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='7' ID='21' >6</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='7' ID='22' >6</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="13">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='13' ID='21' >7</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='13' ID='22' >7</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="20">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='20' ID='21' >8</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='20' ID='22' >8</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="40">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='40' ID='21' >9</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='40' ID='22' >9</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="41">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='41' ID='21' >10</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='41' ID='22' >10</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="46">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='46' ID='21' >11</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='46' ID='22' >11</ELE>">
				</cfif>
			</cfcase>
			<cfcase value="79">
				<cfif arguments.language EQ "E">
					<cfset thisXML = "<ELE DESC='79' ID='21' >12</ELE>">
				<cfelse>
					<cfset thisXML = "<ELE DESC='79' ID='22' >12</ELE>">
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>


	<cffunction name="getControlString" access="public" output="false">
		<cfargument name="qData" type="query" default="">

		<cfset var controlString = "">
		<cfset var primaryNumber = "">
		<cfset var secondaryNumber = "">

		<cfif listfindnocase("1546,1569,1571,1573,1575,1577,1579",arguments.qData.batchId_bi)><!---  call to begin with english language (English control string) --->

			<cfsavecontent variable="controlString">
			<cfoutput>
			<DM BS='0' DSUID='377' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
			<ELE ID='0'>0</ELE>
			</DM>
			<RXSS>
				<ELE BS='0' CK1='2' CK2='3' CK5='3' CP='' DESC='' LINK='-1' QID='1' RQ='' RXT='24' X='620.5' Y='94'/>
				<!--- English Live --->
				<ELE BS='1' CK1='10' CK10='' CK11='' CK12='' CK13='' CK2='-13,3' CK3='-13' CK4='(-13,2)(3,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CP='0' DESC='' DI='1' DS='2' DSE='15' DSUID='377' LINK='0' QID='2' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
				</ELE>
				<!--- English Answering Machine --->
			    <ELE BS='1' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='0' DS='2' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='725' Y='250' >
					#stichDynamicMessage(arguments.qData,'AMENG')#
			    </ELE>
			    <!--- Spanish Live --->
				<ELE BS='1' CK1='10' CK10='' CK11='' CK12='' CK13='' CK2='-13' CK3='-13,2' CK4='(-13,4)(2,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CP='0' DESC='' DI='1' DS='2' DSE='15' DSUID='377' LINK='0' QID='4' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
				</ELE>
			</RXSS>
			#getOutageCCDString()#
			</cfoutput>
			</cfsavecontent>

		</cfif>

		<cfif listfindnocase("1568,1570,1572,1574,1576,1578,1580",arguments.qData.batchId_bi)> <!--- call to begin with spanish language (spanish control string) --->

			<cfsavecontent variable="controlString">
			<cfoutput>
			<DM BS='0' DSUID='377' Desc='Description Not Specified' LIB='0' MT='1' PT='12'>
			<ELE ID='0'>0</ELE>
			</DM>
			<RXSS>
				<ELE BS='0' CK1='2' CK2='3' CK5='3' CP='' DESC='' LINK='-1' QID='1' RQ='' RXT='24' X='620.5' Y='94'/>
				<!--- Spanish Live --->
				<ELE BS='1' CK1='10' CK10='' CK11='' CK12='' CK13='' CK2='-13,2' CK3='-13' CK4='(-13,2)(2,4)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CP='0' DESC='' DI='1' DS='2' DSE='15' DSUID='377' LINK='0' QID='2' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVESPAN')#
				</ELE>
				<!--- Spanish Answering Machine --->
			    <ELE BS='1' CK1='0' CK5='-1' CP='' RQ='' DESC='' DI='0' DS='2' DSE='1' DSUID='377' LINK='0' QID='3' RXT='1' X='725' Y='250' >
					#stichDynamicMessage(arguments.qData,'AMSPAN')#
			    </ELE>
			    <!--- English Live --->
				<ELE BS='1' CK1='10' CK10='' CK11='' CK12='' CK13='' CK2='-13' CK3='-13,3' CK4='(-13,4)(3,2)' CK5='-1' CK6='-1' CK7='-1' CK8='5' CK9='30' CP='0' DESC='' DI='1' DS='2' DSE='15' DSUID='377' LINK='0' QID='4' RQ='0' RXT='2' X='493.5' Y='218'>
					#stichDynamicMessage(arguments.qData,'LIVEENG')#
				</ELE>
			</RXSS>
			#getOutageCCDString()#
			</cfoutput>
			</cfsavecontent>

		</cfif>


		<cfreturn controlString>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic --->
	<cffunction name="stichDynamicMessage" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">

		<cfset thisXML = stichDynamicMessageP1(arguments.qData,arguments.msgType)>
		<cfset thisXML = thisXML & stichDynamicMessageP2(arguments.qData,arguments.msgType)>
		<cfset thisXML = thisXML & stichDynamicMessageP3(arguments.qData,arguments.msgType)>
		<cfset thisXML = thisXML & stichDynamicMessageP4(arguments.qData,arguments.msgType)>
		<cfset thisXML = thisXML & stichDynamicMessageP5(arguments.qData,arguments.msgType)>


<!---

		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1546 english, 1568 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1546,1568">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='power is out at the address starting with' ID='15'>1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<cfif int(arguments.qData.Custom2_vch) EQ 1> <!--- If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
							<ELE DESC='nd you are the only customer affected in the area at this' ID='15'>7</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0 > <!--- UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
							<ELE DESC='and its affecting' ID='15'>2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='customers in the area' ID='15'>3</ELE>
						</cfif>
						<ELE DESC='we expact to have power back on by' ID='15'>4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
						<ELE DESC='please understand this initial estimate...when we have new information' ID='15'>5</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='hay una interrupcin en la direccin que comienza con' ID='26' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<cfif int(arguments.qData.Custom2_vch) EQ 1> <!--- If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time. --->
							<ELE DESC='esta interrupcin comenz a las' ID='26'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
							<ELE DESC='y usted es el unico cliente afectado en el rea en este momento' ID='26'>7</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0 > <!--- UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='esta interrupcin comenz a las' ID='26'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
							<ELE DESC='y est afectando a' ID='26' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
							<ELE DESC='clientes en el rea' ID='26' >3</ELE>
						</cfif>
						<ELE DESC='esperamos restablecer el servicio elctrico a las' ID='26' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
						<ELE DESC='tenga en cuenta que este estimado...tengamos informacin nueva' ID='26' >5</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1569 english, 1570 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1569,1570">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='power is currently out to more than one of your accounts near the address starting with' ID='17' >1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
							<ELE DESC='and its affecting' ID='17' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='customers in the area' ID='17' >3</ELE>
						</cfif>
						<ELE DESC='we expect to have power back on by' ID='17' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
						<ELE DESC='please understand this initial estimate...when we have new information' ID='17' >5</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='actualmente hay una interrupcin en ms de una de sus cuentas cerca de la direccin que comienza con' ID='27' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='esta interrupcin comenz a las' ID='27'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
							<ELE DESC='y est afectando a' ID='27' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
							<ELE DESC='clientes en el rea' ID='27' >3</ELE>
						</cfif>
						<ELE DESC='esperamos restablecer el servicio elctrico a las' ID='27' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
						<ELE DESC='tenga en cuenta que este estimado...informacin nueva' ID='27' >5</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1571 english, 1572 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1571,1572">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
				        <ELE DESC='power is out at the address starting with' ID='28' >1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<!---
							UNICA: If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time.
							UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE.
						 --->
						<cfif int(arguments.qData.Custom2_vch) EQ 1 >
					        <ELE DESC='this outage started at' ID='28' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
					        <ELE DESC='and you are the only customer affected in the area at the moment' ID='28' >6</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
					        <ELE DESC='this outage started at' ID='28' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
					        <ELE DESC='and its affecting' ID='28' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
					        <ELE DESC='customers in the area' ID='28' >3</ELE>
						</cfif>
				        <ELE DESC='an estimated restoration time is under investigation. We will call you again when we have new information' ID='28' >4</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
        				<ELE DESC='hay una interrupcin en la direccin que comienza con' ID='29' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<!---
							UNICA: If CUST_AFFECTED equals 1, replace the 2nd sentence with this:   Esta interrupci�n comenz� a las POWER_OFF_DATE y usted es el �nico cliente afectado en el �rea en este momento.
							UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  Esta interrupci�n comenz� a las POWER_OFF_DATE.
						 --->
						<cfif int(arguments.qData.Custom2_vch) EQ 1 >
					        <ELE DESC='esta interrupcin comenz a las' ID='29' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
					        <ELE DESC='y usted es el unico cliente afectado en el rea en este' ID='29' >6</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
					        <ELE DESC='esta interrupcin comenz a las' ID='29' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
					        <ELE DESC='y est afectando a' ID='29' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
        					<ELE DESC='clientes en el rea' ID='29' >3</ELE>
						</cfif>
				        <ELE DESC='el tiempo de restablecimiento estimado...Recibir otra llamada de actualizacin cuando tengamos informacin nueva' ID='29' >4</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1573 english, 1574 Spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1573,1574">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
					        <ELE DESC='power is currently out to more than one of your accounts near the address starting with' ID='30' >1</ELE>
					        #getSingleDigitXML(numericAddress,'E')#
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
								<ELE DESC='this outage started at' ID='30' >5</ELE>
								#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
						        <ELE DESC='and its affecting' ID='30' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers in the area' ID='30' >3</ELE>
							</cfif>
					        <ELE DESC='an estimated restoration time is under investigation. We will call you again when we have new information' ID='30' >4</ELE>
							#getCommonElementXML('FPL.COM','E')#
							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
					        <ELE DESC='hay una interrupcin en ms de una de sus cuentas cerca de la direccin que comienza con' ID='31' >2</ELE>
					        #getSingleDigitXML(numericAddress,'S')#
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
								<ELE DESC='esta interrupcin comenz a las' ID='31' >6</ELE>
								#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
							<cfelse>
						        <ELE DESC='y est afectando a' ID='31' >3</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes en el rea' ID='31' >4</ELE>
							</cfif>
					        <ELE DESC='el tiempo de restablecimiento estimado...Recibir otra llamada de actualizacin cuando tengamos informacin nueva' ID='31' >5</ELE>
							#getCommonElementXML('FPL.COM','S')#
							#getCommonElementXML('OPTIN','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1575 english, 1576 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1575,1576">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='A restoration specialist is en route to investigate the outage at' ID='32' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='32' >6</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration specialist is en route to investigate the outage near' ID='32' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='32' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='32' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='we expect power to be back on by' ID='32' >3</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='please know this estimate may change as we work to restore your power. we will call you again when we have new information.' ID='32' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1>
					        	<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion en - at' ID='33' >8</ELE>
								#getSingleDigitXML(numericAddress,'S')#
								<ELE DESC='usted es el nico cliente afectado' ID='33' >9</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta cerca de - near' ID='33' >7</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta afectando a' ID='33' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes cerca de la direccion que comienza con' ID='33' >3</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							</cfif>
					        <ELE DESC='esperamos restablecer el servicio electrico a las' ID='33' >4</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'S')#
					        <ELE DESC='sepa que este estimado puede cambiar mientras...cuando tengamos informacion nueva' ID='33' >5</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1577 english, 1578 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1577,1578">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							 #getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1 OR int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='A restoration specialist is en route to investigate the outage near' ID='34' >5</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='34' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='34' >2</ELE>
							</cfif>
							#getSingleDigitXML(numericAddress,'E')#
					        <ELE DESC='Please know this estimate may change as we work to restore your power. We expect power to be back on by' ID='34' >3</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='We will call you again when we have new information.' ID='34' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							 #getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1 OR int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='un especialista de restablecimiento est en ruta para investigar la interrupcin cerca a' ID='50' >7</ELE>
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta afectando a' ID='50' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes cerca de la direccion que comienza con' ID='50' >3</ELE>
							</cfif>
							#getSingleDigitXML(numericAddress,'S')#
					        <ELE DESC='sepa que este estimado puede cambiar mientras trabajamos para restablecer su servicio electrico' ID='50' >5</ELE>
					        <ELE DESC='esperamos restablecer el servicio electrico a las' ID='50' >4</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'S')#
					        <ELE DESC='recibira otra llamada de actualizacion cuando tengamos informacion nueva' ID='50' >6</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1579 english, 1580 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1579,1580">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='A restoration specialist is en route to investigate the outage near' ID='35' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='A restoration specialist is en route to investigate the outage at' ID='35' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='35' >6</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='35' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='35' >2</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='An estimated restoration time is under investigation. We will call you again when we have new information.' ID='35' >7</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta cerca de - near' ID='64' >7</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion en - at' ID='64' >6</ELE>
								#getSingleDigitXML(numericAddress,'S')#
								<ELE DESC='usted es el nico cliente afectado' ID='64' >8</ELE>
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento est en ruta para investigar la interrupcin que est afectando a' ID='64' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes  cerca de la direccin que comienza con' ID='64' >3</ELE>
						        #getSingleDigitXML(numericAddress,'S')#
							</cfif>
					        <ELE DESC='el tiempo de restablecimiento estimado est bajo investigacin' ID='64' >4</ELE>
					        <ELE DESC='recibira otra llamada de actualizacion cuando tengamos informacion nueva' ID='64' >5</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1593 english, 1594 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1593,1594">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) LT 1>
						        <ELE DESC='A restoration specialist is en route to investigate the outage near' ID='36' >4</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='36' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='36' >2</ELE>
							</cfif>
					        #getSingleDigitXML(numericAddress,'E')#
					        <ELE DESC='An estimated restoration time is under investigation. We will call you when we have new information.' ID='36' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1595 english, 1596 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1595,1596">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='A restoration specialist is in the area to investigate the outage at' ID='37' >5</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
						        <ELE DESC='Youre the only customer affected at this time.' ID='37' >6</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
						        <ELE DESC='A restoration soecialist is in the area to investigate the outage near' ID='37' >7</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							<cfelse>
						        <ELE DESC='A restoration specialist is in the area to investigate the outage that is affecting' ID='37' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='37' >2</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='We expect to have power back on by' ID='37' >8</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='Please know this estimate may change as we work to restore your power. We will call you again when we have new information.' ID='37' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
1597 english, 1598 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1597,1598">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='38' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='38' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='38' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect to have power back on by' ID='38' >6</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate... we have new information.' ID='38' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160239 english, 160240 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160239,160240">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 >
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='39' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration specialist ...investigate the outage at' ID='39' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time.' ID='37' >6</ELE>
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='39' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='39' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ... we have new information.' ID='39' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160241 english, 160242 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160241,160242">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1 >
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='40' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='40' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='40' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ... we have new information.' ID='40' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160243 english, 160244 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160243,160244">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='we're sending additional resources to restore power near' ID='41' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='41' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1 >
								<ELE DESC='we're sending additional resources to restore your power at' ID='41' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='you're the only customer affected at this time.' ID='41' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='41' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='we're sending additional ... this outage is affecting' ID='41' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='41' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='41' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know the estimate ...n we have new information' ID='41' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160245 english, 160246 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160245,160246">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources to restore power near' ID='42' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='42' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='we're sending additional ... this outage is affecting' ID='42' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='42' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='42' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='42' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160247 english, 160248 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160247,160248">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='were sending additional resources to restore the power near' ID='43' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='43' >6</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources  to restore your power at' ID='43' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='43' >5</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='43' >6</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='were sending additional r... this outage is affecting' ID='43' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='43' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ...n we have new information' ID='43' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160249 english, 160250 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160249,160250">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources to restore the power near' ID='44' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='44' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='were sending additional r... this outage is affecting' ID='44' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='44' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='44' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160251 english, 160252 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160251,160252">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='45' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='45' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<CFELSEif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en route to investigate the outage at' ID='45' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='45' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='45' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='45' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='45' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power back on by' ID='45' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							 <ELE DESC='please know this estimate...n we have new information' ID='45' >4</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160253 english, 160254 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160253,160254">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='46' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='46' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='46' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='46' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='46' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='46' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160255 english, 160256 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160255,160256">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='47' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='47' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en route to investigate the outage at' ID='47' >6</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='32' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='47' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='47' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='47' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='47' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160257 english, 160258 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160257,160258">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='48' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='48' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='48' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='48' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='48' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160259 english, 160260 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->

			<cfcase value="160259,160260">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='49' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='49' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...investigate the outage at' ID='49' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='49' >5</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='49' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='49' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power to be back on by' ID='49' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='49' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160261 english, 160262 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160261,160262">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='51' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='51' >5</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='51' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power to be back on by' ID='51' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='51' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160263 english, 160264 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->

			<cfcase value="160263,160264">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='52' >3</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='52' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...investigate the outage at' ID='52' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='52' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='52' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='52' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='52' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160265 english, 160266 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160265,160266">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='53' >3</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='53' >4</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='53' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='53' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160267 english, 160268 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160267,160268">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='54' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='power should be on if not...tage again at fpl dot com' ID='54' >2</ELE>

							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160284 english, 160285 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160284,160285">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='55' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='power should be on if not...tage again at fpl dot com' ID='55' >2</ELE>

							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160286 english, 160287 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160286,160287">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='the outage at the address starting with' ID='56' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='was caused by' ID='56' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='56' >3</ELE>
							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160288 english, 160289 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160288,160289">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='the outage near the address starting with' ID='57' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='was caused by' ID='57' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='57' >3</ELE>
							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160290 english, 160291 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160290,160291">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='58' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='if not try resetting the ...tage again at fpl dot com' ID='58' >2</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160292 english, 160293 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160292,160293">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='59' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='59' >2</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160294 english, 160295 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160294,160295">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were unable to locate or ...the address starting with' ID='60' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='and need additional infor...u please call 18004688243' ID='60' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160296 english, 160297 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160296,160297">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were unable to locate or ...the address starting with' ID='61' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='and need additional infor...u please call 18004688243' ID='61' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160298 english, 160299 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160298,160299">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='62' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='62' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='we expect power to be back on by' ID='62' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please understand this es... slash outage for updates' ID='62' >4</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160300 english, 160301 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160300,160301">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='63' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='63' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='we expect power to be back on by' ID='63' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please understand this es... slash outage for updates' ID='63' >4</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160302 english, 160303 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160302,160303">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='65' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='65' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='an estimated restoration ... slash outage for updates' ID='65' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160304 english, 160305 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160304,160305">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='66' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='66' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='an estimated restoration ... slash outage for updates' ID='65' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch> --->

		<cfreturn thisXML>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic (Script 1 to 10) --->
	<cffunction name="stichDynamicMessageP1" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1546 english, 1568 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1546,1568">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='power is out at the address starting with' ID='15'>1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<cfif int(arguments.qData.Custom2_vch) EQ 1> <!--- If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
							<ELE DESC='nd you are the only customer affected in the area at this' ID='15'>7</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0 > <!--- UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
							<ELE DESC='and its affecting' ID='15'>2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='customers in the area' ID='15'>3</ELE>
						</cfif>
						<ELE DESC='we expact to have power back on by' ID='15'>4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
						<ELE DESC='please understand this initial estimate...when we have new information' ID='15'>5</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='hay una interrupcin en la direccin que comienza con' ID='26' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<cfif int(arguments.qData.Custom2_vch) EQ 1> <!--- If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time. --->
							<ELE DESC='esta interrupcin comenz a las' ID='26'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
							<ELE DESC='y usted es el unico cliente afectado en el rea en este momento' ID='26'>7</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0 > <!--- UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='esta interrupcin comenz a las' ID='26'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
							<ELE DESC='y est afectando a' ID='26' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
							<ELE DESC='clientes en el rea' ID='26' >3</ELE>
						</cfif>
						<ELE DESC='esperamos restablecer el servicio elctrico a las' ID='26' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
						<ELE DESC='tenga en cuenta que este estimado...tengamos informacin nueva' ID='26' >5</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1569 english, 1570 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1569,1570">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
						<ELE DESC='power is currently out to more than one of your accounts near the address starting with' ID='17' >1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='this outage started at' ID='15'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
							<ELE DESC='and its affecting' ID='17' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='customers in the area' ID='17' >3</ELE>
						</cfif>
						<ELE DESC='we expect to have power back on by' ID='17' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
						<ELE DESC='please understand this initial estimate...when we have new information' ID='17' >5</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
						<ELE DESC='actualmente hay una interrupcin en ms de una de sus cuentas cerca de la direccin que comienza con' ID='27' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
							<ELE DESC='esta interrupcin comenz a las' ID='27'>6</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
							<ELE DESC='y est afectando a' ID='27' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
							<ELE DESC='clientes en el rea' ID='27' >3</ELE>
						</cfif>
						<ELE DESC='esperamos restablecer el servicio elctrico a las' ID='27' >4</ELE>
						#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
						<ELE DESC='tenga en cuenta que este estimado...informacin nueva' ID='27' >5</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1571 english, 1572 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1571,1572">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('ALTLANGUAGE','S')#
						</cfif>
				        <ELE DESC='power is out at the address starting with' ID='28' >1</ELE>
						#getSingleDigitXML(numericAddress,'E')#
						<!---
							UNICA: If CUST_AFFECTED equals 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE and you're the only customer affected in the area at this time.
							UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE.
						 --->
						<cfif int(arguments.qData.Custom2_vch) EQ 1 >
					        <ELE DESC='this outage started at' ID='28' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
					        <ELE DESC='and you are the only customer affected in the area at the moment' ID='28' >6</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
					        <ELE DESC='this outage started at' ID='28' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'E')#
						<cfelse>
					        <ELE DESC='and its affecting' ID='28' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
					        <ELE DESC='customers in the area' ID='28' >3</ELE>
						</cfif>
				        <ELE DESC='an estimated restoration time is under investigation. We will call you again when we have new information' ID='28' >4</ELE>
						#getCommonElementXML('FPL.COM','E')#
						#getCommonElementXML('OPTIN','E')#
						<cfif arguments.msgType EQ "LIVEENG">
							#getCommonElementXML('REPEAT','E')#
						<cfelse>
							#getCommonElementXML('THANKYOU','E')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						#getCommonElementXML('HELLO','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('ALTLANGUAGE','E')#
						</cfif>
        				<ELE DESC='hay una interrupcin en la direccin que comienza con' ID='29' >1</ELE>
						#getSingleDigitXML(numericAddress,'S')#
						<!---
							UNICA: If CUST_AFFECTED equals 1, replace the 2nd sentence with this:   Esta interrupci�n comenz� a las POWER_OFF_DATE y usted es el �nico cliente afectado en el �rea en este momento.
							UNICA: If CUST_AFFECTED equals 0, replace the 2nd sentence with this:  Esta interrupci�n comenz� a las POWER_OFF_DATE.
						 --->
						<cfif int(arguments.qData.Custom2_vch) EQ 1 >
					        <ELE DESC='esta interrupcin comenz a las' ID='29' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
					        <ELE DESC='y usted es el unico cliente afectado en el rea en este' ID='29' >6</ELE>
						<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
					        <ELE DESC='esta interrupcin comenz a las' ID='29' >5</ELE>
							#getDateTimeXML(arguments.qData.Custom4_vch,'S')#
						<cfelse>
					        <ELE DESC='y est afectando a' ID='29' >2</ELE>
							#getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
        					<ELE DESC='clientes en el rea' ID='29' >3</ELE>
						</cfif>
				        <ELE DESC='el tiempo de restablecimiento estimado...Recibir otra llamada de actualizacin cuando tengamos informacin nueva' ID='29' >4</ELE>
						#getCommonElementXML('FPL.COM','S')#
						#getCommonElementXML('OPTIN','S')#
						<cfif arguments.msgType EQ "LIVESPAN">
							#getCommonElementXML('REPEAT','S')#
						<cfelse>
							#getCommonElementXML('THANKYOU','S')#
						</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1573 english, 1574 Spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1573,1574">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
					        <ELE DESC='power is currently out to more than one of your accounts near the address starting with' ID='30' >1</ELE>
					        #getSingleDigitXML(numericAddress,'E')#
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
								<ELE DESC='this outage started at' ID='30' >5</ELE>
								#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
						        <ELE DESC='and its affecting' ID='30' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers in the area' ID='30' >3</ELE>
							</cfif>
					        <ELE DESC='an estimated restoration time is under investigation. We will call you again when we have new information' ID='30' >4</ELE>
							#getCommonElementXML('FPL.COM','E')#
							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
					        <ELE DESC='hay una interrupcin en ms de una de sus cuentas cerca de la direccin que comienza con' ID='31' >2</ELE>
					        #getSingleDigitXML(numericAddress,'S')#
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1 > <!--- UNICA: If CUST_AFFECTED equals 0 or 1, replace the 2nd sentence with this:  This outage started at POWER_OFF_DATE. --->
								<ELE DESC='esta interrupcin comenz a las' ID='31' >6</ELE>
								#getDateTimeXML(arguments.qData.Custom3_vch,'S')#
							<cfelse>
						        <ELE DESC='y est afectando a' ID='31' >3</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes en el rea' ID='31' >4</ELE>
							</cfif>
					        <ELE DESC='el tiempo de restablecimiento estimado...Recibir otra llamada de actualizacin cuando tengamos informacin nueva' ID='31' >5</ELE>
							#getCommonElementXML('FPL.COM','S')#
							#getCommonElementXML('OPTIN','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1575 english, 1576 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1575,1576">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='A restoration specialist is en route to investigate the outage at' ID='32' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='32' >6</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration specialist is en route to investigate the outage near' ID='32' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='32' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='32' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='we expect power to be back on by' ID='32' >3</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='please know this estimate may change as we work to restore your power. we will call you again when we have new information.' ID='32' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1>
					        	<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion en - at' ID='33' >8</ELE>
								#getSingleDigitXML(numericAddress,'S')#
								<ELE DESC='usted es el nico cliente afectado' ID='33' >9</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta cerca de - near' ID='33' >7</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta afectando a' ID='33' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes cerca de la direccion que comienza con' ID='33' >3</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							</cfif>
					        <ELE DESC='esperamos restablecer el servicio electrico a las' ID='33' >4</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'S')#
					        <ELE DESC='sepa que este estimado puede cambiar mientras...cuando tengamos informacion nueva' ID='33' >5</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1577 english, 1578 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1577,1578">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							 #getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1 OR int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='A restoration specialist is en route to investigate the outage near' ID='34' >5</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='34' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='34' >2</ELE>
							</cfif>
							#getSingleDigitXML(numericAddress,'E')#
					        <ELE DESC='Please know this estimate may change as we work to restore your power. We expect power to be back on by' ID='34' >3</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='We will call you again when we have new information.' ID='34' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							 #getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 1 OR int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='un especialista de restablecimiento est en ruta para investigar la interrupcin cerca a' ID='50' >7</ELE>
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta afectando a' ID='50' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes cerca de la direccion que comienza con' ID='50' >3</ELE>
							</cfif>
							#getSingleDigitXML(numericAddress,'S')#
					        <ELE DESC='sepa que este estimado puede cambiar mientras trabajamos para restablecer su servicio electrico' ID='50' >5</ELE>
					        <ELE DESC='esperamos restablecer el servicio electrico a las' ID='50' >4</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'S')#
					        <ELE DESC='recibira otra llamada de actualizacion cuando tengamos informacion nueva' ID='50' >6</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1579 english, 1580 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1579,1580">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='A restoration specialist is en route to investigate the outage near' ID='35' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='A restoration specialist is en route to investigate the outage at' ID='35' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='35' >6</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='35' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='35' >2</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='An estimated restoration time is under investigation. We will call you again when we have new information.' ID='35' >7</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','S')#
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('ALTLANGUAGE','E')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion que esta cerca de - near' ID='64' >7</ELE>
								#getSingleDigitXML(numericAddress,'S')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
						        <ELE DESC='un especialista de restablecimiento esta en ruta para investigar la interrupcion en - at' ID='64' >6</ELE>
								#getSingleDigitXML(numericAddress,'S')#
								<ELE DESC='usted es el nico cliente afectado' ID='64' >8</ELE>
							<cfelse>
						        <ELE DESC='un especialista de restablecimiento est en ruta para investigar la interrupcin que est afectando a' ID='64' >2</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'S')#
						        <ELE DESC='clientes  cerca de la direccin que comienza con' ID='64' >3</ELE>
						        #getSingleDigitXML(numericAddress,'S')#
							</cfif>
					        <ELE DESC='el tiempo de restablecimiento estimado est bajo investigacin' ID='64' >4</ELE>
					        <ELE DESC='recibira otra llamada de actualizacion cuando tengamos informacion nueva' ID='64' >5</ELE>
							<cfif arguments.msgType EQ "LIVESPAN">
								#getCommonElementXML('REPEAT','S')#
							<cfelse>
								#getCommonElementXML('THANKYOU','S')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1593 english, 1594 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1593,1594">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) LT 1>
						        <ELE DESC='A restoration specialist is en route to investigate the outage near' ID='36' >4</ELE>
							<cfelse>
						        <ELE DESC='A restoration specialist is en route to investigate the outage affecting' ID='36' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='36' >2</ELE>
							</cfif>
					        #getSingleDigitXML(numericAddress,'E')#
					        <ELE DESC='An estimated restoration time is under investigation. We will call you when we have new information.' ID='36' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>
						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>
<!---
==========================================================================================================================================================
==========================================================================================================================================================
1595 english, 1596 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1595,1596">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
						        <ELE DESC='A restoration specialist is in the area to investigate the outage at' ID='37' >5</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
						        <ELE DESC='Youre the only customer affected at this time.' ID='37' >6</ELE>
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
						        <ELE DESC='A restoration soecialist is in the area to investigate the outage near' ID='37' >7</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							<cfelse>
						        <ELE DESC='A restoration specialist is in the area to investigate the outage that is affecting' ID='37' >1</ELE>
						        #getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
						        <ELE DESC='customers near the address starting with' ID='37' >2</ELE>
						        #getSingleDigitXML(numericAddress,'E')#
							</cfif>
					        <ELE DESC='We expect to have power back on by' ID='37' >8</ELE>
					        #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
					        <ELE DESC='Please know this estimate may change as we work to restore your power. We will call you again when we have new information.' ID='37' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
1597 english, 1598 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="1597,1598">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='38' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='38' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='38' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect to have power back on by' ID='38' >6</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate... we have new information.' ID='38' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic (Script 11 to 20) --->
	<cffunction name="stichDynamicMessageP2" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160239 english, 160240 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160239,160240">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 >
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='39' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration specialist ...investigate the outage at' ID='39' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time.' ID='37' >6</ELE>
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='39' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='39' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ... we have new information.' ID='39' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160241 english, 160242 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160241,160242">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1 >
								<ELE DESC='a restoration specialist ...vestigate the outage near' ID='40' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							<cfelse>
								<ELE DESC='a restoration specialist ... outage that is affecting' ID='40' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='40' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ... we have new information.' ID='40' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160243 english, 160244 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160243,160244">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='we're sending additional resources to restore power near' ID='41' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='41' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1 >
								<ELE DESC='we're sending additional resources to restore your power at' ID='41' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='you're the only customer affected at this time.' ID='41' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='41' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='we're sending additional ... this outage is affecting' ID='41' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='41' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='41' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know the estimate ...n we have new information' ID='41' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160245 english, 160246 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160245,160246">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources to restore power near' ID='42' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='42' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='we're sending additional ... this outage is affecting' ID='42' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='42' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='42' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='42' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160247 english, 160248 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160247,160248">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='were sending additional resources to restore the power near' ID='43' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='43' >6</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources  to restore your power at' ID='43' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='43' >5</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='43' >6</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='were sending additional r... this outage is affecting' ID='43' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='43' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='an estimated restoration ...n we have new information' ID='43' >3</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160249 english, 160250 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160249,160250">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR  int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='were sending additional resources to restore the power near' ID='44' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='44' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='were sending additional r... this outage is affecting' ID='44' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='44' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='44' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160251 english, 160252 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160251,160252">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='45' >8</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='45' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<CFELSEif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en route to investigate the outage at' ID='45' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='45' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='45' >7</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='45' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='45' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power back on by' ID='45' >3</ELE>
							 #getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							 <ELE DESC='please know this estimate...n we have new information' ID='45' >4</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160253 english, 160254 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160253,160254">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>
							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='46' >5</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='46' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='46' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='46' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>
							<ELE DESC='we expect power back on by' ID='46' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='46' >4</ELE>
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160255 english, 160256 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160255,160256">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='47' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='47' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en route to investigate the outage at' ID='47' >6</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='Youre the only customer affected at this time' ID='32' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='47' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='47' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='47' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='47' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160257 english, 160258 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160257,160258">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is en ...vestigate the outage near' ID='48' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='48' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is en ...gate the outage affecting' ID='48' >1</ELE>
								#getDoubleDigitXML(arguments.qData.Custom2_vch,'E')#
								<ELE DESC='customers near the address starting with' ID='48' >2</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='48' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic (Script 21 to 30) --->
	<cffunction name="stichDynamicMessageP3" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160259 english, 160260 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->

			<cfcase value="160259,160260">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='49' >7</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='49' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...investigate the outage at' ID='49' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='49' >5</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='49' >6</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='49' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power to be back on by' ID='49' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='49' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160261 english, 160262 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160261,160262">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='51' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='51' >5</ELE>
								#getCauseXML(arguments.qData.Custom4_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='51' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='we expect power to be back on by' ID='51' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='51' >3</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160263 english, 160264 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->

			<cfcase value="160263,160264">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='52' >3</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='52' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelseif int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...investigate the outage at' ID='52' >4</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='youre the only customer affected at this time' ID='52' >6</ELE>
								<ELE DESC='our investigation shows that the outage was caused by' ID='52' >5</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='52' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='52' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160265 english, 160266 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160265,160266">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<cfif int(arguments.qData.Custom2_vch) EQ 0 OR int(arguments.qData.Custom2_vch) EQ 1>
								<ELE DESC='a restoration crew is in ...vestigate the outage near' ID='53' >3</ELE>
								#getSingleDigitXML(numericAddress,'E')#
								<ELE DESC='our investigation shows that the outage was caused by' ID='53' >4</ELE>
								#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<cfelse>
								<ELE DESC='a restoration crew is in ...the address starting with' ID='53' >1</ELE>
								#getSingleDigitXML(numericAddress,'E')#
							</cfif>

							<ELE DESC='an estimated restoration ...n we have new information' ID='53' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160267 english, 160268 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160267,160268">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='54' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='power should be on if not...tage again at fpl dot com' ID='54' >2</ELE>

							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160284 english, 160285 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160284,160285">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='55' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='power should be on if not...tage again at fpl dot com' ID='55' >2</ELE>

							#getCommonElementXML('OPTIN','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160286 english, 160287 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160286,160287">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='the outage at the address starting with' ID='56' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='was caused by' ID='56' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='56' >3</ELE>
							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160288 english, 160289 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160288,160289">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='the outage near the address starting with' ID='57' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='was caused by' ID='57' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='57' >3</ELE>
							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160290 english, 160291 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160290,160291">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='58' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='if not try resetting the ...tage again at fpl dot com' ID='58' >2</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160292 english, 160293 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160292,160293">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='we are calling with an up...the address starting with' ID='59' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='your power should be on i...tage again at fpl dot com' ID='59' >2</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic (Script 31 to 40) --->
	<cffunction name="stichDynamicMessageP4" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160294 english, 160295 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160294,160295">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were unable to locate or ...the address starting with' ID='60' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='and need additional infor...u please call 18004688243' ID='60' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>



<!---
==========================================================================================================================================================
==========================================================================================================================================================
160296 english, 160297 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160296,160297">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were unable to locate or ...the address starting with' ID='61' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='and need additional infor...u please call 18004688243' ID='61' >2</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160298 english, 160299 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160298,160299">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='62' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='62' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='we expect power to be back on by' ID='62' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please understand this es... slash outage for updates' ID='62' >4</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160300 english, 160301 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160300,160301">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='63' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='63' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='we expect power to be back on by' ID='63' >3</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please understand this es... slash outage for updates' ID='63' >4</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160302 english, 160303 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160302,160303">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='65' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='65' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='an estimated restoration ... slash outage for updates' ID='65' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160304 english, 160305 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160304,160305">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were upgrading the electr...the address starting with' ID='66' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='this power outage is scheduled to start at approximately' ID='66' >2</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='an estimated restoration ... slash outage for updates' ID='65' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160306 english, 160307 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160306,160307">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='67' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='67' >2</ELE>
							#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='and will take longer to repair' ID='67' >3</ELE>
							<ELE DESC='we now expect to have power back on by' ID='67' >4</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='67' >5</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160308 english, 160309 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160308,160309">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='68' >6</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='68' >2</ELE>
							#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='and will take longer to repair' ID='68' >3</ELE>
							<ELE DESC='we now expect to have power back on by' ID='68' >4</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='68' >5</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160310 english, 160311 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160310,160311">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='69' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='69' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='and will take longer to repair' ID='69' >3</ELE>
							<ELE DESC='an estimated restoration ...n we have new information' ID='69' >4</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160312 english, 160313 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160312,160313">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='70' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='70' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='and will take longer to repair' ID='70' >3</ELE>
							<ELE DESC='an estimated restoration ...n we have new information' ID='70' >4</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>

	<!--- function to generate actual messages by stiching the data ids  and applying business logic (Script 41 to 50) --->
	<cffunction name="stichDynamicMessageP5" access="private" output="false">
		<cfargument name="qData" type="query" default="">
		<cfargument name="msgType" type="string" default="LIVEENG"> <!--- LIVEENG, AMENG, AMSPAN, LIVESPAN --->

		<cfset var  thisXML = "">
		<cfset var numericAddress = "">


		<cfset numericAddress = getNumericAddress(arguments.qData.Custom1_vch)>

		<cfswitch expression="#arguments.qData.batchId_bi#">
<!---
==========================================================================================================================================================
==========================================================================================================================================================
160314 english, 160315 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160314,160315">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='71' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='71' >2</ELE>
							#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='and will take less time than expected to repair' ID='71' >3</ELE>
							<ELE DESC='we now expect to have power back on by' ID='71' >4</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='71' >5</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

<!---
==========================================================================================================================================================
==========================================================================================================================================================
160316 english, 160317 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160316,160317">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='72' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='72' >2</ELE>
							#getCauseXML(arguments.qData.Custom3_vch,'E')#
							<ELE DESC='and will take less time than expected to repair' ID='72' >3</ELE>
							<ELE DESC='we now expect to have power back on by' ID='72' >4</ELE>
							#getDateTimeXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='please know this estimate...n we have new information' ID='72' >5</ELE>

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160318 english, 160319 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160318,160319">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='73' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='73' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='and will take less time t... slash outage for updates' ID='73' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>


<!---
==========================================================================================================================================================
==========================================================================================================================================================
160320 english, 160321 spanish
==========================================================================================================================================================
==========================================================================================================================================================
--->
			<cfcase value="160320,160321">
				<cfif arguments.msgType EQ "AMENG" OR arguments.msgType EQ "LIVEENG">
					<cfsavecontent variable="thisXML">
						<cfoutput>
							#getCommonElementXML('HELLO','E')#
							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('ALTLANGUAGE','S')#
							</cfif>

							<ELE DESC='were still working to res...the address starting with' ID='74' >1</ELE>
							#getSingleDigitXML(numericAddress,'E')#
							<ELE DESC='our investigation shows the outage was caused by' ID='74' >2</ELE>
							#getCauseXML(arguments.qData.Custom2_vch,'E')#
							<ELE DESC='and will take less time t... slash outage for updates' ID='74' >3</ELE>

							#getCommonElementXML('OPTIN','E')#

							<cfif arguments.msgType EQ "LIVEENG">
								#getCommonElementXML('REPEAT','E')#
							<cfelse>
								#getCommonElementXML('THANKYOU','E')#
							</cfif>
						</cfoutput>
					</cfsavecontent>
				<cfelseif arguments.msgType EQ "AMSPAN" OR arguments.msgType EQ "LIVESPAN">
					<cfsavecontent variable="thisXML">
						<cfoutput>

						</cfoutput>
					</cfsavecontent>
				</cfif>
			</cfcase>

		</cfswitch>

		<cfreturn thisXML>

	</cffunction>



</cfcomponent>
