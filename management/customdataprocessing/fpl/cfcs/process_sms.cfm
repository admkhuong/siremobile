<!--- This file is just for cron job --->

<cfset mm = minute(now())>

<cfset o = CreateObject('component','fplsms').init()>

<!--- run file import every 6 min --->
<cfif mm mod 6 EQ 0>
	<cfset temp = o.importSMSFiles()>
</cfif>

<!--- export results every 6 min --->
<cfif mm mod 6 EQ 0>
	<cfset temp = o.exportData()>
</cfif>

<!--- send sms every 2 min if there is any in database --->
<cfif mm mod 2 EQ 0>
	<cfset temp = o.sendSMS()>
</cfif>