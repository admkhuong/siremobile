<!--- This file is just for cron job --->

<cfset mm = minute(now())>

<cfset o = CreateObject('component','fplvoice').init()>


<!--- run file import every 6 min --->
<cfif mm mod 10 EQ 0>
	<cfset temp = o.importVoiceFiles()>
</cfif>

<!--- export results every 10 min --->
<cfif mm mod 10 EQ 0>
	<cfset temp = o.exportData()>
</cfif>

<!--- send sms every 5 min if there is any in database --->
<cfif mm mod 2 EQ 0>
	<cfset temp = o.sendCalls()>
</cfif>