<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Load Monthly Reference Data</title>
</head>

<body>

<!---

CREATE TABLE `simplexprogramdata`.`portal_link_c8_p1_monthly_ref` (
`PKId_int` INT NOT NULL AUTO_INCREMENT,
`MemberId_vch` VARCHAR(255) NULL,
`DateOfBirth_dt` DATETIME NULL,
`INNLimit_vch` VARCHAR(45) NULL,
`OONLimit_vch` VARCHAR(45) NULL,
`INNBal_vch` VARCHAR(45) NULL,
`OONBal_vch` VARCHAR(45) NULL,
`AsOfDate_dt` DATETIME NULL,
PRIMARY KEY (`PKId_int`),
INDEX `IDX_Member` (`MemberId_vch` ASC));

--->


<cfset FileToImport = "" />

-- rename old table for archive purposes? or delete? Log transactions?

<!--- Bulk Insert Monthly Reference File--->

<!--- Run manually from desktop mySQLWorkbench for now --->
LOAD DATA LOCAL INFILE  '/Users/JLP/Desktop/uhc_deductible_monthly_sample.txt'
INTO TABLE simplexprogramdata.portal_link_c8_p1_monthly_ref 
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
(MemberId_vch, @date1, INNLimit_vch, OONLimit_vch, INNBal_vch, OONBal_vch, @date2) 
SET DateOfBirth_dt = STR_TO_DATE(@date1,'%Y%m%d'), AsOfDate_dt = STR_TO_DATE(@date2,'%Y%m%d')





LOAD DATA LOCAL INFILE  '/Users/JLP/Desktop/uhc_deductible_monthly_sample.txt'
INTO TABLE simplexprogramdata.portal_link_c8_p1_monthly_ref 
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
(MemberId_vch, @date1, INNLimit_vch, OONLimit_vch, INNBal_vch, OONBal_vch) 
SET DateOfBirth_dt = STR_TO_DATE(@date1,'%Y%m%d')




LOAD DATA LOCAL INFILE  '/Users/JLP/Desktop/uhc_deductible_monthly_sample.txt'
INTO TABLE simplexprogramdata.portal_link_c8_p1_monthly_ref 
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
(MemberId_vch, @date1, INNLimit_vch, OONLimit_vch, INNBal_vch, OONBal_vch, @date2) 
SET DateOfBirth_dt = STR_TO_DATE(@date1,'%Y%m%d'), AsOfDate_dt = STR_TO_DATE(@date2,'%Y%m%d')


-- wells_fargo_accumulator_r141010_test.txt


LOAD DATA LOCAL INFILE  '/Users/JLP/Desktop/wells_fargo_accumulator_r141010_test.txt'
INTO TABLE simplexprogramdata.portal_link_c8_p1_monthly_ref 
FIELDS TERMINATED BY '!' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
(MemberId_vch, @date1, INNLimit_vch, OONLimit_vch, INNBal_vch, OONBal_vch, @date2) 
SET DateOfBirth_dt = STR_TO_DATE(@date1,'%Y%m%d'), AsOfDate_dt = STR_TO_DATE(@date2,'%Y%m%d')



LOAD DATA LOCAL INFILE  '/Users/JLP/Desktop/wells_fargo_accumulator_r141215.txt'
INTO TABLE simplexprogramdata.portal_link_c8_p1_monthly_ref 
FIELDS TERMINATED BY '!' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
(MemberId_vch, @date1, INNLimit_vch, OONLimit_vch, INNBal_vch, OONBal_vch, @date2) 
SET DateOfBirth_dt = STR_TO_DATE(@date1,'%Y%m%d'), AsOfDate_dt = STR_TO_DATE(@date2,'%Y%m%d')





SELECT * FROM simplexprogramdata.portal_link_c8_p1_monthly_ref

SELECT COUNT(*) FROM simplexprogramdata.portal_link_c8_p1_monthly_ref

SELECT COUNT(*) FROM simplexprogramdata.portal_link_c8_p1_monthly_ref WHERE LastSent_dt IS NULL


SELECT COUNT(*) FROM simplexprogramdata.portal_link_c8_p1_monthly_ref WHERE LastSent_dt IS NOT NULL

SELECT COUNT(*) FROM simplexprogramdata.portal_link_c8_p1_monthly_ref



SELECT `portal_link_c8_p1`.`PKId_int`,
    `portal_link_c8_p1`.`MemberId_vch`,
    `portal_link_c8_p1`.`DateOfBirth_dt`,
    `portal_link_c8_p1`.`Status_int`,
    `portal_link_c8_p1`.`FirstName_vch`,
    `portal_link_c8_p1`.`LastName_vch`,
    `portal_link_c8_p1`.`LastSent_dt`,
    `portal_link_c8_p1`.`ContactString_vch`,
    `portal_link_c8_p1`.`ContactTypeId_int`,
    `portal_link_c8_p1`.`LastSentDeductableIn_vch`,
    `portal_link_c8_p1`.`LastSentDeductableInMet_vch`,
    `portal_link_c8_p1`.`LastSentDeductableOut_vch`,
    `portal_link_c8_p1`.`LastSentDeductableOutMet_vch`,
    `portal_link_c8_p1`.`LastSentAsOfDate_dt`,
    `portal_link_c8_p1`.`ProgramNotes_vch`,
    `portal_link_c8_p1`.`LastAgentUserId_int`
FROM `simplexprogramdata`.`portal_link_c8_p1`;



SELECT 
        DISTINCT oi.ContactString_vch,
        p1.MemberId_vch,
        p1.DateOfBirth_dt,
        MAX(oi.OptIn_dt) AS OptIn_dt
    FROM 
        simplelists.optinout AS oi 
        left outer join simplexprogramdata.portal_link_c8_p1 p1
            ON p1.ContactString_vch = oi.ContactString_vch
        left outer join simpleobjects.useraccount  AS ua
            ON p1.LastAgentUserId_int = ua.userid_int
    WHERE 
        OptIn_dt IS NOT NULL 
    AND 
        OptOut_dt IS NULL 
    AND 
        ShortCode_vch = '244687'  
    AND 
        oi.Batchid_bi = '1254'
    AND 
        oi.ContactString_vch <> '9494000553'  
	AND
        ua.emailaddress_vch <> 'mary.tester.uhg@mb.com'
   AND
        OptIn_dt > '2014-09-01 00:00:00'
    AND
        OptIn_dt < '2014-10-07 00:00:00'
	GROUP BY
		oi.ContactString_vch,
        p1.MemberId_vch,
        p1.DateOfBirth_dt
    ORDER BY 
        oi.OptIn_dt	DESC



SELECT 
        DISTINCT oi.ContactString_vch
    FROM 
        simplelists.optinout AS oi 
        left outer join simplexprogramdata.portal_link_c8_p1 p1
            ON p1.ContactString_vch = oi.ContactString_vch
        left outer join simpleobjects.useraccount  AS ua
            ON p1.LastAgentUserId_int = ua.userid_int
    WHERE 
        OptIn_dt IS NOT NULL 
    AND 
        OptOut_dt IS NULL 
    AND 
        ShortCode_vch = '244687'  
    AND 
        oi.Batchid_bi = '1254'
    AND 
       oi.ContactString_vch <> '9494000553' 
	AND
        ua.emailaddress_vch <> 'mary.tester.uhg@mb.com'
    AND
        OptIn_dt > '2014-09-01 00:00:00'
    AND
        OptIn_dt < '2014-10-07 00:00:00'
  
    ORDER BY 
        oi.OptIn_dt	DESC








</body>
</html>