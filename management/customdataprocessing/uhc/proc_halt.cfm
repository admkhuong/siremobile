<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Process Monthly</title>
</head>

<body>

<cfset ProdDB = "Bishop" />
<cfset DefaultTimeZone = 31 />

<cfset CountFound = 0 />
<cfset CountNotFound = 0 />
<cfset NotSentLog = "" />


<cfparam name="inpLimitAmount" default="0" />

<cfoutput>


<!---
Hi Jeff & Tani–
 
I have great news here.
 
The programming team was able to add in the plan limits, so we will be providing all data for the text after  all. There will be no need to pull from previously entered input from the agents as I stated yesterday.
 
Here is a sample mockup of the data (the ‘!’ will be replaced by ‘|’):
 
601352677!19931010!000003072!00000307200!00000295200!00000295200!20140923
001887784!19950214!000003072!00000307200!00000307200!00000307200!20140923
521694668!19870505!000003072!00000307200!00000306928!00000306928!20140923
 
Record definition
1st – Member ID
2nd – Member DOB (CCYYMMDD)
3rd  -INN Deductible Limit (11 bytes , 9 before decimal and last 2 after decimal)
4th  - OON Deductible Limit (11 bytes , 9 before decimal and last 2 after decimal)
5th  - INN Deductible balance  - after deducting INN YTD deductions from INN Limit (11 bytes , 9 before decimal and last 2 after decimal)
6th  - OON Deductible balance  - after deducting OON YTD deductions from OON Limit (11 bytes , 9 before decimal and last 2 after decimal)
7th  - Processed date (CCYYMMDD format)
 
Jeff – can you use this to start programming the processing of this information?
 
Thanks! Sam
 
 
 
 
 Questions:
 
 	How much data in monthly file? 
		Answer - 500K records - all of Wells Fargo
	How far back in the double opt in do you eant to send data? Someone could have just got data 2 days ago....
		Answer - 5 days


--->



<!--- Read all currently double opted in to program batch id 1254 contacts   --->
<cfquery name="getContacts" datasource="#ProdDB#">
   SELECT 
        DISTINCT oi.ContactString_vch
    FROM 
        simplelists.optinout AS oi 
        left outer join simplexprogramdata.portal_link_c8_p1 p1
            ON p1.ContactString_vch = oi.ContactString_vch
        left outer join simpleobjects.useraccount  AS ua
            ON p1.LastAgentUserId_int = ua.userid_int
    WHERE 
        OptIn_dt IS NOT NULL 
    AND 
        OptOut_dt IS NULL 
    AND 
        ShortCode_vch = '244687'  
    AND 
        oi.Batchid_bi = '1254'
    <!---AND 
        oi.ContactString_vch <> '9494000553'--->
       <!--- oi.ContactString_vch = '9494000553'  --->
    
    <!--- Testing JLP Only --->
    <!---AND 
        oi.ContactString_vch = '9494000553'--->
        
        
    AND
        OptIn_dt > '2014-09-01 00:00:00'
    AND
        OptIn_dt < '2014-12-15 00:00:00'
        
    <!--- Not a testing agent entry --->    
    AND
        ua.emailaddress_vch <> 'mary.tester.uhg@mb.com'	
        
    <!--- Not agent opted out --->             
    AND
		p1.status_int <> 5    
        
    ORDER BY 
        oi.OptIn_dt	DESC
    
    <cfif inpLimitAmount GT 0>
    	LIMIT #inpLimitAmount#
    </cfif>    
        
</cfquery>	

Records Found = #getContacts.RecordCount#

<HR />

<!--- Do this once - separated for quicker looping where needed --->
<cfinclude template="authsetup.cfm" />
                        
    <cfloop query="getContacts">
        
        <!--- Lookup each member id for contact information in agent interface table --->
                           
			<cfset VerboseDebug = 0 />
            <cfset inpBatchId = "1413" />
            <cfset inpContactString = '#TRIM(getContacts.ContactString_vch)#' />
            <cfset inpSkipLocalUserDNCCheck = "0" />
                
            ***** Success
            <BR/>
            Contact String (#TRIM(getContacts.ContactString_vch)#)                    
            <BR />
           
            
            <!--- Process it --->
                                        
                <!--- Post to API --->
                <!---<cfinclude template="sendsinglesms.cfm" />--->
                  
                <!--- security - sanity - do not sent twice per file load  --->          
                <!--- Update record as processed Date - prevent multiple sends on page loads  --->                    
                  
        <HR/>
        
        <!--- For testing - just output to screen --->
        
        <!--- security - sanity - check last sent not in last 5 days --->
        <!--- security - sanity - check as of date is not older than 7 days --->
       
    </cfloop>

<HR />

CountNotFound = #CountNotFound#
<BR />

CountFound = #CountFound#
<BR />

NotSentLog = <BR />#NotSentLog#
<BR />

</cfoutput>





</body>
</html>
