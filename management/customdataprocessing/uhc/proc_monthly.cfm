<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Process Monthly</title>
</head>

<body>

<cfset ProdDB = "Bishop" />
<cfset DefaultTimeZone = 31 />

<cfset CountFound = 0 />
<cfset CountNotFound = 0 />
<cfset NotSentLog = "" />


<cfparam name="inpLimitAmount" default="0" />

<cfoutput>


<!---
Hi Jeff & Tani–
 
I have great news here.
 
The programming team was able to add in the plan limits, so we will be providing all data for the text after  all. There will be no need to pull from previously entered input from the agents as I stated yesterday.
 
Here is a sample mockup of the data (the ‘!’ will be replaced by ‘|’):
 
601352677!19931010!000003072!00000307200!00000295200!00000295200!20140923
001887784!19950214!000003072!00000307200!00000307200!00000307200!20140923
521694668!19870505!000003072!00000307200!00000306928!00000306928!20140923
 
Record definition
1st – Member ID
2nd – Member DOB (CCYYMMDD)
3rd  -INN Deductible Limit (11 bytes , 9 before decimal and last 2 after decimal)
4th  - OON Deductible Limit (11 bytes , 9 before decimal and last 2 after decimal)
5th  - INN Deductible balance  - after deducting INN YTD deductions from INN Limit (11 bytes , 9 before decimal and last 2 after decimal)
6th  - OON Deductible balance  - after deducting OON YTD deductions from OON Limit (11 bytes , 9 before decimal and last 2 after decimal)
7th  - Processed date (CCYYMMDD format)
 
Jeff – can you use this to start programming the processing of this information?
 
Thanks! Sam
 
 
 
 
 Questions:
 
 	How much data in monthly file? 
		Answer - 500K records - all of Wells Fargo
	How far back in the double opt in do you eant to send data? Someone could have just got data 2 days ago....
		Answer - 5 days


--->



<!--- Read all currently double opted in to program batch id 1254 contacts   --->
<cfquery name="getContacts" datasource="#ProdDB#">
   SELECT 
        DISTINCT oi.ContactString_vch
    FROM 
        simplelists.optinout AS oi 
        left outer join simplexprogramdata.portal_link_c8_p1 p1
            ON p1.ContactString_vch = oi.ContactString_vch
        left outer join simpleobjects.useraccount  AS ua
            ON p1.LastAgentUserId_int = ua.userid_int
    WHERE 
        OptIn_dt IS NOT NULL 
    AND 
        OptOut_dt IS NULL 
    AND 
        ShortCode_vch = '244687'  
    AND 
        oi.Batchid_bi = '1254'
    <!---AND 
        oi.ContactString_vch <> '9494000553'--->
       <!--- oi.ContactString_vch = '9494000553'  --->
    AND
        OptIn_dt > '2014-09-01 00:00:00'
    AND
        OptIn_dt < '2014-11-15 00:00:00'
    <!---AND
        ua.emailaddress_vch <> 'mary.tester.uhg@mb.com'	--->
    ORDER BY 
        oi.OptIn_dt	DESC
    
    <cfif inpLimitAmount GT 0>
    	LIMIT #inpLimitAmount#
    </cfif>    
        
</cfquery>	

Records Found = #getContacts.RecordCount#

<HR />

<!--- Do this once - separated for quicker looping where needed --->
<cfinclude template="authsetup.cfm" />
                        
    <cfloop query="getContacts">
        
        <!--- Lookup each member id for contact information in agent interface table --->
        
        <cfquery name="getContactsDetails" datasource="#ProdDB#">
           SELECT DISTINCT        
                MemberId_vch,
                DateOfBirth_dt
            FROM 
               simplexprogramdata.portal_link_c8_p1 
            WHERE 
                ContactString_vch = '#TRIM(getContacts.ContactString_vch)#'
            LIMIT 10  <!--- Limit possible matches --->
        </cfquery>	
        
                
        <cfloop query="getContactsDetails">
             <!--- Presume monthly data dump has already been imported --->
    
                <!--- Match for current message data lookup in monthly provided data dump --->
                <cfquery name="getRefData" datasource="#ProdDB#">
                   SELECT 
                        PKId_int,
                        MemberId_vch,
                        DateOfBirth_dt,
                        INNLimit_vch,
                        OONLimit_vch,
                        INNBal_vch,
                        OONBal_vch,
                        AsOfDate_dt
                    FROM 
                        simplexprogramdata.portal_link_c8_p1_monthly_ref 
                    WHERE 
                        MemberId_vch = '#TRIM(getContactsDetails.MemberId_vch)#'
                    <!---AND 
                        DateOfBirth_dt = '#TRIM(getContactsDetails.DateOfBirth_dt)#'--->
                    AND
                    	LastSent_dt IS NULL    
                    Limit 1
                    
                 </cfquery>	
                
                
                
                <cfif getRefData.RecordCount GT 0>
                    
                    <cfset VerboseDebug = 0 />
					<cfset inpBatchId = "1255" />
                    <cfset inpContactString = '#TRIM(getContacts.ContactString_vch)#' />
                    <cfset inpLastSentDeductableIn = "#Left(getRefData.INNLimit_vch, Len(getRefData.INNLimit_vch)-2) * 1#">
                    <cfset inpLastSentDeductableInMet = "#Left(getRefData.INNBal_vch, Len(getRefData.INNBal_vch)-2) * 1#">
                    <cfset inpLastSentDeductableOut = "#Left(getRefData.OONLimit_vch, Len(getRefData.OONLimit_vch)-2) * 1#">
                    <cfset inpLastSentDeductableOutMet = "#Left(getRefData.OONBal_vch, Len(getRefData.OONBal_vch)-2)  * 1#">
                    <cfset inpLastSentAsOfDate = "#dateformat(getRefData.AsOfDate_dt,'yyyy-mm-dd')#" >
                    <cfset inpSkipLocalUserDNCCheck = "0" />
						
                        
                        
                    ***** Success
                    <BR/>
                    Contact String (#TRIM(getContacts.ContactString_vch)#)                    
                    <BR />
                    Member(#TRIM(getContactsDetails.MemberId_vch)#)
                    <BR />
                    DOB(#TRIM(getContactsDetails.DateOfBirth_dt)#) #getRefData.INNLimit_vch#
                    <BR />
                    INNLimit(#inpLastSentDeductableIn#)
                    <BR />
                    OONLimit(#inpLastSentDeductableOut#)
                    <BR />
                    INNBal(#inpLastSentDeductableInMet#)
                    <BR />
                    OONBal(#inpLastSentDeductableOutMet#)
                    <BR />
                    As of date (#dateformat(getRefData.AsOfDate_dt,'yyyy-mm-dd')#)
                    <BR/>
                    
                    <!--- Process it --->
                    				
												
						<!--- Post to API --->
						<!---<cfinclude template="sendsinglesms.cfm" />--->
			              
                        <!--- security - sanity - do not sent twice per file load  --->          
						<!--- Update record as processed Date - prevent multiple sends on page loads  --->                    
                    	<cfquery name="UpdateRefData" datasource="#ProdDB#">
                           	UPDATE
                           		simplexprogramdata.portal_link_c8_p1_monthly_ref
                           	SET
                           		LastSent_dt = NOW()                                 
                            WHERE 
                                MemberId_vch = '#TRIM(getContactsDetails.MemberId_vch)#'
                           <!--- AND 
                                DateOfBirth_dt = '#TRIM(getContactsDetails.DateOfBirth_dt)#'--->
                            AND
                                LastSent_dt IS NULL                                
                         </cfquery>	                 
                 
                    <!--- Found a match - exit --->
                    
                    <cfset CountFound = CountFound + 1 />
                    
                    <cfbreak>
                    
                <cfelse>
                
                	<cfset NotSentLog = NotSentLog & "#TRIM(getContacts.ContactString_vch)#|#TRIM(getContactsDetails.MemberId_vch)#|#dateformat(getContactsDetails.DateOfBirth_dt,'yyyy-mm-dd')#<BR/>" />
                
                    <BR/>
                    	<cfset CountNotFound = CountNotFound + 1 />
                    
                    	No reference records found for Member(#TRIM(getContactsDetails.MemberId_vch)#) and DOB(#dateformat(getContactsDetails.DateOfBirth_dt,'yyyy-mm-dd')#)
                    <BR/>
                
                </cfif>
                
                <HR/>
                
                <!--- For testing - just output to screen --->
                
                <!--- security - sanity - check last sent not in last 5 days --->
                <!--- security - sanity - check as of date is not older than 7 days --->
            
        </cfloop>
                
    
    </cfloop>

<HR />

CountNotFound = #CountNotFound#
<BR />

CountFound = #CountFound#
<BR />

NotSentLog = <BR />#NotSentLog#
<BR />

</cfoutput>





</body>
</html>
