<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-store, no-cache, must-revalidate">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title></title>

<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link TYPE="text/css" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <link TYPE="text/css" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/BabbleSphere/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/RXForm.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/rxform2.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/main.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/babblesphere.css" />
    
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery-1.5.1.min.js"></script>
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
    
	<script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 

 	<script src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/ValidationRegEx.js"></script>
    
    <script src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/grid.locale-en.js" TYPE="text/javascript"></script>
	<script src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.jqGrid.min.Light.RXMod.js" TYPE="text/javascript"></script>
 
    <script src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.print.js" TYPE="text/javascript"></script>
    
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/fisheye-iutil.min.js"></script>
    
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/css/BB.ui.jqgrid.css" />
    
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery.jeditable.js"></script>
    
 
 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
<script TYPE="text/javascript">
	var CurrSitePath = '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>';
</script>

<!--- Enable hover functionality ---> 
<script TYPE="text/javascript">
$(function(){

});
</script>


</cfoutput>
 
 
<script TYPE="text/javascript">


	
</script> 


