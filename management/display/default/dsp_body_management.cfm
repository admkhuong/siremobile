<cfparam name="Session.USERID" default="0">
<script>
	
</script>
<script type="text/javascript">
        $(document).ready(function() {
        wait(500);

            $(".signin").click(function(e) {
                e.preventDefault();
                $("fieldset#signin_menu").toggle();
				$("#BSCRightHome").toggleClass("loginBody");
                $(".signin").toggleClass("menu-open");
			});

            $("fieldset#signin_menu").mouseup(function() {
                return false
            });
            $(document).mouseup(function(e) {
                if($(e.target).parent("a.signin").length==0) {
					$("#BSCRightHome").removeClass("loginBody");
                    $(".signin").removeClass("menu-open");
                    $("fieldset#signin_menu").hide();					
                }
            });   
            
        $("#LoginLink").click(function() {DoLogin();});
		$("#ForgotPassword").click(function() {return false;});
		
		$("#loadingDlg").hide();
		$("#loadingDlgLogin").hide();
			
	    });
		function DoLogin()
			{		
				try
				{
					<!--- Call LOCALOUTPUT javascript validations first --->
					if(ValidateInput($("#inpUserID").val()) && ValidateInput($("#inpPassword").val()))
					{
						currentUserId=$("#inpUserID").val();
						<!--- Let the user know somthing is processing --->
														
						$.getJSON( '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { 
							inpUserID : $("#inpUserID").val(), 
							inpPassword : $("#inpPassword").val()
							}, 
							<!--- Default return function for authentication - Async call back --->
							function(d) 
							{
								<!--- Alert if failure --->
										
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{																									
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{							
											CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
											
											if(CurrRXResultCode > 0)
											{
												<!--- Start the timeout count down --->
												CurrentTimeLeft = <cfoutput>#TimeOutSeconds#</cfoutput>;
												
												CountDown();
												window.location.href="<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/account/home";
												
												<!--- Kill modal login dialog --->								
												//$('#container').hide();
												//$('#BSHeadLogin').show();		
												//$('#BSContentWide').show();																		
											}
											else
											{
												<!--- Unsuccessful Login --->
												jAlert("Error.", d.DATA.REASONMSG[0]);									
											}
										}
										else
										{<!--- Invalid structure returned --->	
											// Do something	
											jAlert("Error.", "general CF Error.");						
										}
									}
									else
									{<!--- No result returned --->
										// Do something									
									}
							} );					
						
					}
					else
					{
							alert('LOCALOUTPUT Validation Not OK');
						//	var msg = document.getElementById("winLogon_title");
						// 	msg.innerHTML = "Input Invalid!";
					}
				}
				catch(e)
				{
					alert(e)
				}
					
					
			}	    
function submitenter(myfield,e)
	{
		var keycode;
		if (window.event) keycode = window.event.keyCode;
		else if (e) keycode = e.which;
		else return true;

		if (keycode == 13)
   		{
        DoLogin();
        return false;
        }
        else
        return true;
}
function wait(msecs)
	{
		var start = new Date().getTime();
		var cur = start
		while(cur - start < msecs)
		{
			cur = new Date().getTime();
		}
	}

</script>
<!--- Most java and css goes before here --->
</head>



<!--- Start the main body --->
<body >

<div id="BSHeader">

	<div id="BSHeadCenter">
    	<div id="BSHeadLogo">
    		<a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home" style="text-decoration:none;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/MBIcon1Web.png"></a>
            <BR>
            <h4>Management Console</h4>
        </div>
         <div id="container">
              <div id="topnav" class="topnav"><a href="login" class="signin"><span>Sign in</span></a></div>
              
              
                <fieldset id="signin_menu" style="width:253px !important">
                <div id="BSHeadLogin_2">
               
                <div style="float:right;margin-right: 15px;">
                <form id="frmLogon" name="frmLogon" method="post">
                  <label for="username">Username or email</label>
                  <input TYPE="text" id="inpUserID" name="inpUserID" label="User Id" title="username" onKeyPress="return submitenter(this,event)" />
                  </p>
                  <p>
                    <label for="password">Password</label>
                    <input TYPE="password" id="inpPassword" name="inpPassword" label="Password" onKeyPress="return submitenter(this,event)"/>
                  </p>
                  <p class="remember">
                    <input TYPE="checkbox" id="inpRememberMe" name="inpRememberMe" value="1" />
                    <label for="remember">Remember me</label>
                    <span style="float:right"><input class="signin_submit" id="LoginLink" value="Sign in" tabindex="6" type="button"></span>
                    <div id="loadingDlgLogin" style="display:inline; float:right">
                        <img class="loadingDlgLogin" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">
                    </div>
                  </p>
                  
                  <p class="forgot"> <a href="#" id="resend_password_link">Forgot your password?</a> </p>
                  <p class="forgot-username"> <A id="forgot_username_link" title="If you remember your password, try logging in with your email" href="#">Forgot your username?</A> </p>
                </form>
                </div>
              	</fieldset>
            </div>
			<div id="BSHeadLogin">
        		<div style="display:block; text-align:right;">
				<cfset currentURL="#CGI.SCRIPT_NAME#">
				<cfif currentURL CONTAINS "management/account/home">
					<a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home'>My Account</a> | <a href='<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/act_logout.cfm'>Logout</a>
					<cfelse>
					<a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home'>My Account</a> | <a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout.cfm'>Logout</a>
				</cfif>
       			</div>
            	<div id="sessionAccountInfoContainer" style="text-align:right;">You are currently logged in as <cfoutput>#Session.EmailAddress#</cfoutput></div>  
        	</div>
        </div>
        
    </div>

</div>

<div id="BSContentWide">

      
    <div id="BSCLeft" align="left">


