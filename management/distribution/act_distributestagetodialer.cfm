<cfparam name="AllowDuplicates" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0">
<cfparam name="inpUserId" default="0">
<cfparam name="inpDialString" default="0">
<cfparam name="inpDialString2" default="">
<cfparam name="XMLControlString_vch" default="XXX">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="LocalityId_int" default="">
<cfparam name="TimeZone_ti" default="">
<cfparam name="RedialCount_int" default="0">
<cfparam name="ServerId_si" default="0">
<cfparam name="inpDialerIPAddr" default="0.0.0.0">

<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="3001-01-01 00:00:00">
<cfparam name="ourReply" default="0,500">

<cfparam name="CurrResult" default="0">
<cfparam name="CurrResultDist" default="1">
<cfparam name="isCoupled" default="1">
<cfparam name="InvalidRecordCount" default="0">

<cfset OutPutTempFileLocal = "C:\\StageDTSProcessing\\#NameOfTempFileForThisProcess#.txt">
<cfset OutPutTempFile = "\\#inpDialerIPAddr#\rxtestTemp\#NameOfTempFileForThisProcess#.txt">
<cfset OutPutTempFilemySQLFormat = "\\\\#inpDialerIPAddr#\\rxtestTemp\\#NameOfTempFileForThisProcess#.txt">

<cftry>
	<cfquery name="CountStageToFile1" datasource="#Session.DBSourceEBM#">
		SELECT
			COUNT(*) AS TOTALCOUNT
		FROM
			simplequeue.#nameoftempfileforthisprocess#
	</cfquery>

	<cfif CountStageToFile1.TOTALCOUNT GT 0>
    
    	<!--- Another stop on the prevent multiple erroneous contacts from going out --->
    	<cfinclude template="act_PreProductionPushDuplicateCheck.cfm">
        
        <!--- Another stop on the prevent multiple erroneous contacts from going out --->
    	<cfinclude template="act_PreProductionPushCheckXMLControlString.cfm">
        
		<!--- Update script libraries --->
        <cfinclude template="act_PushStagedAudioII.cfm"> 
        
        <!--- Update schedule options --->
        <cfinclude template="act_PushStagedScheduleOptions.cfm"> 
        
        
        <cfset UpdateStatusOK = 0>
                       
         <!--- Squash deadlocking issues? --->
        <cfloop from="1" to="3" step="1" index="DeadlockIndexDSD">
            <cftry>
            
            	<!---
				  UPDATE 
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = 3, <!--- 1 = Queued 2= Queued to go on on RXDialer, 3=Now On Dialer 4=Extracted--->
                        Queued_DialerIP_vch = '#inpDialerIPAddr#',
                        Queue_dt = NOW() 
                    WHERE
                        DTSId_int IN (SELECT DQDTSId_int FROM simplequeue.#nameoftempfileforthisprocess# WHERE CleanFlag_ti = 1)
                    AND
                        DTSStatusType_ti = 2  <!--- Be VERY careful here - this queries index usage is pick Changing to <3 instead of =2 make it run 12+ minutes versus under 20 seconds --->
              	--->
                
                <!--- Update Dial Queue Table Status to Queued - This will take care of any freshly processed crashed tables --->
                <cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">                                
               		UPDATE
                        simplequeue.contactqueue AS T1
                    JOIN
                        simplequeue.#nameoftempfileforthisprocess# AS T2 ON T2.DTS_UUID_vch = T1.DTS_UUID_vch AND T2.CleanFlag_ti = 1
                    SET                        
                        DTSStatusType_ti = 3, <!--- 1 = Queued 2= Queued to go on on RXDialer, 3=Now On Dialer 4=Extracted--->
                        Queued_DialerIP_vch = '#inpDialerIPAddr#',
                        Queue_dt = NOW() 
                    WHERE
                        T1.DTSStatusType_ti = 2
                </cfquery>    
                
                <cfset UpdateStatusOK = 1>
                
                <cfbreak>
                
                <cfcatch type="any">
                
                    <cfif findnocase("Deadlock", cfcatch.detail) GT 0 > 
                    
                        <!---
                        <cfthread
                            action="sleep"
                            duration="#(2 * 1000)#"
                        />--->                            
                        
                        <cfscript> 
                            thread = CreateObject("java","java.lang.Thread"); 
                            thread.sleep(3000); // CF will now sleep for 5 seconds 
                        </cfscript> 

                    <cfelse>
                    	
                        <!--- other errors - break from loop--->                        
                        <cfbreak>
                    
                    </cfif>
                
                </cfcatch>
            
            </cftry>
        
        </cfloop>        
        
        
        <!--- One more try - non-squashed --->
		<cfif UpdateStatusOK EQ 0>
        
            <!--- Update Dial Queue Table Status to Queued - This will take care of any freshly processed crashed tables --->
          	<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
               UPDATE
                    simplequeue.contactqueue AS T1
                JOIN
                    simplequeue.#nameoftempfileforthisprocess# AS T2 ON T2.DTS_UUID_vch = T1.DTS_UUID_vch AND T2.CleanFlag_ti = 1
                SET
                    DTSStatusType_ti = 3, <!--- 1 = Queued 2= Queued to go on on RXDialer, 3=Now On Dialer 4=Extracted--->
                    Queued_DialerIP_vch = '#inpDialerIPAddr#',
                    Queue_dt = NOW() 
                WHERE
                    T1.DTSStatusType_ti = 2
              </cfquery>    
        
        </cfif>
                
                
		<cfquery name="StageToFile" datasource="#Session.DBSourceEBM#">
			SELECT
				DialString1_vch,
				DialString2_vch,
				TimeZone_ti,
				CurrentRedialCount_ti,
				XMLControlString_vch,
				BatchId_bi,
                UserId_int,
                CampaignTypeId_int
			FROM
				 simplequeue.#nameoftempfileforthisprocess#
			WHERE
				  CleanFlag_ti = 1
            AND 
            	CampaignTypeId_int > 29   <!--- One off call campaign type 0--->
		</cfquery>

		<!--- Create the file --->
		<cffile action="write" file="#OutPutTempFileLocal#" output="" addnewline="no" nameconflict="overwrite">

	<!---	old one had comma at the end ?!? - broke when switch to inno DB
		output="0,#StageToFile.BatchId_bi#,1,#StageToFile.TimeZone_ti#,#StageToFile.CurrentRedialCount_ti#,#StageToFile.UserId_int#,#inpCampaignId#,#StageToFile.CampaignTypeId_int#,0,0,#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#,#StageToFile.DialString1_vch#,#StageToFile.DialString2_vch#,SimpleXStageWEBToFileToDTS,""#StageToFile.XMLControlString_vch#"","
	--->
       
		<cfloop query="StageToFile">
			<cffile action="Append"
				file="#OutPutTempFileLocal#"
				output="0,#StageToFile.BatchId_bi#,1,#StageToFile.TimeZone_ti#,#StageToFile.CurrentRedialCount_ti#,#StageToFile.UserId_int#,#inpCampaignId#,#StageToFile.CampaignTypeId_int#,0,0,#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#,#StageToFile.DialString1_vch#,#StageToFile.DialString2_vch#,SimpleXStageWEBToFileToDTS,""#StageToFile.XMLControlString_vch#"","
				addNewLine = "YES">
		</cfloop>

	    <!--- Copy file to dialer --->
		<cffile action="copy" source="#OutPutTempFileLocal#" destination="#OutPutTempFile#" nameconflict="overwrite">

		<!--- Import to DTS on target dialer --->
		<cfquery name="InsertIntoRemoteDialerDTS" datasource="#inpDialerIPAddr#">
			LOAD DATA INFILE '#OutPutTempFilemySQLFormat#'
			INTO TABLE DistributedToSend.DTS
			FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n'
		</cfquery>

		<cfquery name="StageToFile" datasource="#Session.DBSourceEBM#">
			DROP TABLE simplequeue.#nameoftempfileforthisprocess#
		</cfquery>
	<cfelse>
		 <cfquery name="StageToFile" datasource="#Session.DBSourceEBM#">
			 DROP TABLE simplequeue.#nameoftempfileforthisprocess#
		 </cfquery>
	</cfif>
<cfcatch TYPE="any">
	<cfset ENA_Message = "ERROR on act_DistributionStageToDialer.cfm PAGE File - <P>#NameOfTempFileForThisProcess# table Processing Error.">
	<cfset InvalidRecordCount = InvalidRecordCount + 1>
	<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
</cfcatch> <!--- End Main Catch DTS --->
</cftry> <!--- End Main Try DTS --->