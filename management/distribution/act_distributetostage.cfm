<cfparam name="AllowDuplicates" default="0">
<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0">
<cfparam name="inpUserId" default="0">
<cfparam name="inpDialString" default="0">
<cfparam name="inpDialString2" default="">
<cfparam name="XMLControlString_vch" default="XXX">
<cfparam name="UserSpecifiedData_vch" default="">
<cfparam name="FileSeqNumber_int" default="0">
<cfparam name="LocalityId_int" default="0">
<cfparam name="TimeZone_ti" default="0">
<cfparam name="inpCurrUUID" default="">
<cfparam name="inpTypeMask_ti" default="0">
<cfparam name="RedialCount_int" default="0">
<cfparam name="ServerId_si" default="0">
<cfparam name="inpDialerIPAddr" default="0.0.0.0">
<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="3001-01-01 00:00:00">
<cfparam name="ourReply" default="0,500">
<cfparam name="CurrResult" default="0">
<cfparam name="CurrResultDist" default="0">
<cfparam name="isCoupled" default="1">
<cfparam name="CurrDialID" default="0">
<cfparam name="InvalidRecordCount" default="0">
<cfparam name="inpLibId" default="-1">
<cfparam name="inpEleId" default="-1">
<cfparam name="inpScriptId" default="-1">
<cfparam name="inpPushSkip" default="0">
<cfparam name="inpDQDTSId" default="-1">



<cfset CurrResultDist = 0>

<!--- Set schedule date based on input parameters (time zone?) --->
<cfset inpScheduled_dt = DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')>

<!---
<cfset OutSQLStr = "	INSERT INTO simplequeue.#nameoftempfileforthisprocess#		(	DialString1_vch,	DialString2_vch,	TimeZone_ti,	CurrentRedialCount_ti,	XMLControlString_vch,	FileSeqNumber_int,	UserSpecifiedData_vch,	BatchId_bi,	CleanFlag_ti	) VALUES (			'#inpDialString#',						'#inpDialString2#',						#TimeZone_ti#,			#RedialCount_int#,			'#trim(XMLControlString_vch)#',			#FileSeqNumber_int#, 					'#UserSpecifiedData_vch#',				#INPBATCHID#,							1									)">
<cfif VerboseDebug GT 0><cfoutput>#OutSQLStr# <BR /></cfoutput></cfif>
--->

<cftry>
	<cfquery name="InsertToStage" datasource="#Session.DBSourceEBM#">
		INSERT INTO simplequeue.#nameoftempfileforthisprocess#				
		(
			DialString1_vch,
			DialString2_vch,
			TimeZone_ti,
            TypeMask_ti,
			CurrentRedialCount_ti,
			XMLControlString_vch,
			FileSeqNumber_int,
			UserSpecifiedData_vch,
			BatchId_bi,
			CleanFlag_ti,
            UserId_int,
        	PushLibrary_int,
    	    PushElement_int,
	        PushScript_int,
            PushSkip_int,
            DQDTSId_int,
            DTS_UUID_vch,
            CampaignTypeId_int
            
		)
		VALUES
		(
			'#inpDialString#',			<!--- Dial String --->
			'#inpDialString2#',			<!--- LAT String - Now DTS_UUID --->
			#TimeZone_ti#,
            #inpTypeMask_ti#, 
			#RedialCount_int#,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#trim(XMLControlString_vch)#">,   <!--- 			'#trim(XMLControlString_vch)#', --->
			#FileSeqNumber_int#, 		<!--- File it came from --->
			'#UserSpecifiedData_vch#',	<!--- unique transaction Id --->
			#INPBATCHID#,				<!--- Batch ID --->
			1,							<!--- 1 = OK to Distribute in Bulk 0 = duplicate--->
            #inpUserId#,
            #inpLibId#,
            #inpEleId#,
            #inpScriptId#,
            #inpPushSkip#,
            #inpDQDTSId#,
            '#inpCurrUUID#',
            #inpCampaignTypeId#           
		)
	</cfquery>

	<cfset CurrResultDist ="1">
<cfcatch TYPE="any"><!--- Add number to list --->
	<cfset CurrResultDist ="-2">
	
	<!--- Just a duplicate issue - log and move on  --->
	<cfif CurrResult neq -2>
		<!--- Log and notify of error --->
		<cfset ENA_Message = "Error while trying to auto-distribute SimpleX dial: ">
		<cfset ENA_Message = ENA_Message & "#nameoftempfileforthisprocess# #INPBATCHID#,1,#TimeZone_ti#,#RedialCount_int#,#inpUserId#,#inpCampaignId#,#inpCampaignTypeId#,0,-1,'#inpScheduled_dt#','#inpDialString#','#inpDialString2#','Special System Distribution','#trim(XMLControlString_vch)#')">
		<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
		<cfset InvalidRecordCount = InvalidRecordCount + 1>
		<cfset CurrResultDist ="-3">
		<cfset CurrResult= -3>
	</cfif>
</cfcatch>
</cftry> <!--- Add number to list ---> 