

<!--- Check for Bad XML here --->

                       
                                                
                        <!--- Clean Flag to bad XML scrubbing Rule --->
                        <cfquery name="UpdateForBadXMLTT3" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplequeue.#nameoftempfileforthisprocess# AS T1
                            SET 
                                T1.CleanFlag_ti = 8
                            WHERE 
                                LENGTH(T1.XMLControlString_vch) < 20
                            OR
                                T1.XMLControlString_vch NOT LIKE "%ESI='#ESIID#'%"
                            AND 
                                T1.CleanFlag_ti = 1	                                  	
                        </cfquery>
        
						<!---  OLD query causing table scan locks in IN clause 												   
						   UPDATE 
								simplequeue.contactqueue AS T1
							SET
								DTSStatusType_ti = 6                                  
							WHERE 
								T1.DTS_UUID_vch IN (SELECT T2.DTS_UUID_vch FROM simplequeue.#nameoftempfileforthisprocess# AS T2 WHERE T2.DTS_UUID_vch = T1.DTS_UUID_vch AND CleanFlag_ti = 8)
							AND
								DTSStatusType_ti < 4      
                         --->
                                                
    	                <!--- 1 = Queued 2= Queued on RXDialer, 3=? 4=Extracted--->  
                        <cfquery name="UpdateForBadXMLDQ" datasource="#Session.DBSourceEBM#">                            
                            UPDATE
                                simplequeue.contactqueue AS T1
                            JOIN
                                simplequeue.#nameoftempfileforthisprocess# AS T2 ON T2.DTS_UUID_vch = T1.DTS_UUID_vch AND T2.CleanFlag_ti = 8
                            SET
                                T1.DTSStatusType_ti = 6
                            WHERE
                                T1.DTSStatusType_ti < 4
                        </cfquery>       
                            
                                    
                        <!--- Write to master results table  --->
                        <cfquery name="InsertToCDL" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplexresults.contactresults
                                (	
                                    RXCallDetailId_int,
                                    DTSID_int,
                                    BatchId_bi,
                                    PhoneId_int,
                                    TotalObjectTime_int,
                                    TotalCallTimeLiveTransfer_int,
                                    TotalCallTime_int,
                                    TotalConnectTime_int,
                                    ReplayTotalCallTime_int,
                                    TotalAnswerTime_int,
                                    UserSpecifiedLineNumber_si,
                                    NumberOfHoursToRescheduleRedial_si,
                                    TransferStatusId_ti,
                                    IsHangUpDetected_ti,
                                    IsOptOut_ti,
                                    IsMaxRedialsReached_ti,
                                    IsRescheduled_ti,
                                    CallResult_int,
                                    SixSecondBilling_int,
                                    SystemBilling_int,
                                    RXCDLStartTime_dt,
                                    CallStartTime_dt,
                                    CallEndTime_dt,
                                    CallStartTimeLiveTransfer_dt,
                                    CallEndTimeLiveTransfer_dt,
                                    CallResultTS_dt,
                                    PlayFileStartTime_dt,
                                    PlayFileEndTime_dt,
                                    HangUpDetectedTS_dt,
                                    Created_dt,
                                    DialerName_vch,
                                    CurrTS_vch,
                                    CurrVoice_vch,
                                    CurrTSLiveTransfer_vch,
                                    CurrVoiceLiveTransfer_vch,
                                    CurrCDP_vch,
                                    CurrCDPLiveTransfer_vch,
                                    ContactString_vch,
                                    RedialNumber_int,
                                    TimeZone_ti,
                                    MessageDelivered_si,
                                    SingleResponseSurvey_si,
                                    XMLResultStr_vch,
                                    XMLControlString_vch,
                                    IsOptIn_ti,
                                    ActualCost_int
                                )
                                SELECT
                                    0, <!---#ExtractFromDialer.RXCallDetailId_int#,--->
                                    0, <!---#ExtractFromDialer.DTSID_int#,--->
                                    BatchId_bi, <!--- #ExtractFromDialer.BatchId_bi#,--->
                                    0, <!---#ExtractFromDialer.PhoneId_int#,--->
                                    0, <!---#ExtractFromDialer.TotalObjectTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalCallTimeLiveTransfer_int#,--->
                                    0, <!---#ExtractFromDialer.TotalCallTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalConnectTime_int#,--->
                                    0, <!---#ExtractFromDialer.ReplayTotalCallTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalAnswerTime_int#,--->
                                    0, <!---#ExtractFromDialer.UserSpecifiedLineNumber_si#,--->
                                    0, <!---#ExtractFromDialer.NumberOfHoursToRescheduleRedial_si#,--->
                                    0, <!---#ExtractFromDialer.TransferStatusId_ti#,--->
                                    0, <!---#ExtractFromDialer.IsHangUpDetected_ti#,--->
                                    0, <!---#ExtractFromDialer.IsOptOut_ti#,--->
                                    0, <!---#ExtractFromDialer.IsMaxRedialsReached_ti#,--->
                                    0, <!---#ExtractFromDialer.IsRescheduled_ti#,--->
                                    503, <!---#ExtractFromDialer.CallResult_int#,--->
                                    0, <!---#ExtractFromDialer.SixSecondBilling_int#,--->
                                    0, <!---#ExtractFromDialer.SystemBilling_int#,--->
                                    NOW(), <!---'#ExtractFromDialer.RXCDLStartTime_dt#',--->
                                    NOW(),<!---'#ExtractFromDialer.CallStartTime_dt#',--->
                                    NOW(),<!---'#ExtractFromDialer.CallEndTime_dt#',--->
                                    '1900-01-01 00:00:00', <!--- '#ExtractFromDialer.CallStartTimeLiveTransfer_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.CallEndTimeLiveTransfer_dt#',--->
                                    NOW(), <!---'#ExtractFromDialer.CallResultTS_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileStartTime_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileEndTime_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.HangUpDetectedTS_dt#',--->
                                    NOW(), <!---'#ExtractFromDialer.Created_dt#',--->
                                    NULL, <!---'#ExtractFromDialer.DialerName_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrTS_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrVoice_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrTSLiveTransfer_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrVoiceLiveTransfer_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrCDP_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrCDPLiveTransfer_vch#',--->
                                    DialString1_vch, <!---'#ExtractFromDialer.DialString_vch#',--->
                                    CurrentRedialCount_ti, <!---#ExtractFromDialer.RedialNumber_int#,--->
                                    TimeZone_ti, <!---#ExtractFromDialer.TimeZone_ti#,--->
                                    0, <!---#ExtractFromDialer.MessageDelivered_si#,--->
                                    0, <!---#ExtractFromDialer.SingleResponseSurvey_si#,--->
                                    '', <!---'#ExtractFromDialer.XMLResultStr_vch#',--->
                                    XMLControlString_vch,
                                    0, <!---#ExtractFromDialer.IsOptIn_ti#,--->
                                    0 <!---#CurrentBillingAmount#--->
                                FROM
                                    simplequeue.#nameoftempfileforthisprocess#   
                                WHERE
                                    CleanFlag_ti = 8                                                               
                        </cfquery>                        
                                                   
                        
                        <!--- Get End time --->
                        <cfset EndTimeCheckDuplicateUUIDs = NOW()>
                        
                        
                      

                       
                       
                        
