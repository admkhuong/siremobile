

<!--- Check Duplicate UUIDs here --->


    <!--- Get start time--->
						<cfset StartTimeCheckDuplicateUUIDs = NOW()>
                       
                       
                        <!--- check for Duplicate UUID's coming back from extractor --->
                        <!--- Check for wether duplicates allowed or other BR's here--->                           
                        <!--- Clean Flag to Duplicate scrubbing Rule against already returned results--->
                        <cfquery name="UpdateForDuplicatesTT3" datasource="#Session.DBSourceEBM#">
                            UPDATE 
                                simplequeue.#nameoftempfileforthisprocess# AS T1
                            SET 
                                T1.CleanFlag_ti = 6
                            WHERE 
                                T1.DTS_UUID_vch IN (SELECT T2.DTS_UUID_vch FROM simplexresults.contactresults AS T2 WHERE T2.DTS_UUID_vch = T1.DTS_UUID_vch)
                            AND 
                                T1.CleanFlag_ti = 1	                                  	
                        </cfquery>
                        
                        <!---
						
						 UPDATE 
                                simplequeue.contactqueue AS T1
                            SET
                                DTSStatusType_ti = 4                                  
                            WHERE 
                                T1.DTS_UUID_vch IN (SELECT T2.DTS_UUID_vch FROM simplequeue.#nameoftempfileforthisprocess# AS T2 WHERE T2.DTS_UUID_vch = T1.DTS_UUID_vch AND CleanFlag_ti = 6)
                            AND
                                DTSStatusType_ti < 4    
						
						--->
                       <!--- 1 = Queued 2= Queued on RXDialer, 3=? 4=Extracted--->  
                        <cfquery name="UpdateForDuplicatesDQ" datasource="#Session.DBSourceEBM#">   
                            UPDATE
                                simplequeue.contactqueue AS T1
                            JOIN
                                simplequeue.#nameoftempfileforthisprocess# AS T2 ON T2.DTS_UUID_vch = T1.DTS_UUID_vch AND T2.CleanFlag_ti = 6
                            SET
                                T1.DTSStatusType_ti = 4
                            WHERE
                                T1.DTSStatusType_ti < 4
                        </cfquery>       
                            
                                    
                        <!--- Write to master results table  --->
                        <cfquery name="InsertToCDL" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplexresults.contactresults
                                (	
                                    RXCallDetailId_int,
                                    DTSID_int,
                                    BatchId_bi,
                                    PhoneId_int,
                                    TotalObjectTime_int,
                                    TotalCallTimeLiveTransfer_int,
                                    TotalCallTime_int,
                                    TotalConnectTime_int,
                                    ReplayTotalCallTime_int,
                                    TotalAnswerTime_int,
                                    UserSpecifiedLineNumber_si,
                                    NumberOfHoursToRescheduleRedial_si,
                                    TransferStatusId_ti,
                                    IsHangUpDetected_ti,
                                    IsOptOut_ti,
                                    IsMaxRedialsReached_ti,
                                    IsRescheduled_ti,
                                    CallResult_int,
                                    SixSecondBilling_int,
                                    SystemBilling_int,
                                    RXCDLStartTime_dt,
                                    CallStartTime_dt,
                                    CallEndTime_dt,
                                    CallStartTimeLiveTransfer_dt,
                                    CallEndTimeLiveTransfer_dt,
                                    CallResultTS_dt,
                                    PlayFileStartTime_dt,
                                    PlayFileEndTime_dt,
                                    HangUpDetectedTS_dt,
                                    Created_dt,
                                    DialerName_vch,
                                    CurrTS_vch,
                                    CurrVoice_vch,
                                    CurrTSLiveTransfer_vch,
                                    CurrVoiceLiveTransfer_vch,
                                    CurrCDP_vch,
                                    CurrCDPLiveTransfer_vch,
                                    ContactString_vch,
                                    RedialNumber_int,
                                    TimeZone_ti,
                                    MessageDelivered_si,
                                    SingleResponseSurvey_si,
                                    XMLResultStr_vch,
                                    XMLControlString_vch,
                                    IsOptIn_ti,
                                    ActualCost_int
                                )
                                SELECT
                                    0, <!---#ExtractFromDialer.RXCallDetailId_int#,--->
                                    0, <!---#ExtractFromDialer.DTSID_int#,--->
                                    BatchId_bi, <!--- #ExtractFromDialer.BatchId_bi#,--->
                                    0, <!---#ExtractFromDialer.PhoneId_int#,--->
                                    0, <!---#ExtractFromDialer.TotalObjectTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalCallTimeLiveTransfer_int#,--->
                                    0, <!---#ExtractFromDialer.TotalCallTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalConnectTime_int#,--->
                                    0, <!---#ExtractFromDialer.ReplayTotalCallTime_int#,--->
                                    0, <!---#ExtractFromDialer.TotalAnswerTime_int#,--->
                                    0, <!---#ExtractFromDialer.UserSpecifiedLineNumber_si#,--->
                                    0, <!---#ExtractFromDialer.NumberOfHoursToRescheduleRedial_si#,--->
                                    0, <!---#ExtractFromDialer.TransferStatusId_ti#,--->
                                    0, <!---#ExtractFromDialer.IsHangUpDetected_ti#,--->
                                    0, <!---#ExtractFromDialer.IsOptOut_ti#,--->
                                    0, <!---#ExtractFromDialer.IsMaxRedialsReached_ti#,--->
                                    0, <!---#ExtractFromDialer.IsRescheduled_ti#,--->
                                    502, <!---#ExtractFromDialer.CallResult_int#,--->
                                    0, <!---#ExtractFromDialer.SixSecondBilling_int#,--->
                                    0, <!---#ExtractFromDialer.SystemBilling_int#,--->
                                    NOW(), <!---'#ExtractFromDialer.RXCDLStartTime_dt#',--->
                                    NOW(),<!---'#ExtractFromDialer.CallStartTime_dt#',--->
                                    NOW(),<!---'#ExtractFromDialer.CallEndTime_dt#',--->
                                    '1900-01-01 00:00:00', <!--- '#ExtractFromDialer.CallStartTimeLiveTransfer_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.CallEndTimeLiveTransfer_dt#',--->
                                    NOW(), <!---'#ExtractFromDialer.CallResultTS_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileStartTime_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileEndTime_dt#',--->
                                    '1900-01-01 00:00:00', <!---'#ExtractFromDialer.HangUpDetectedTS_dt#',--->
                                    NOW(), <!---'#ExtractFromDialer.Created_dt#',--->
                                    NULL, <!---'#ExtractFromDialer.DialerName_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrTS_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrVoice_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrTSLiveTransfer_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrVoiceLiveTransfer_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrCDP_vch#',--->
                                    NULL, <!---'#ExtractFromDialer.CurrCDPLiveTransfer_vch#',--->
                                    DialString1_vch, <!---'#ExtractFromDialer.DialString_vch#',--->
                                    CurrentRedialCount_ti, <!---#ExtractFromDialer.RedialNumber_int#,--->
                                    TimeZone_ti, <!---#ExtractFromDialer.TimeZone_ti#,--->
                                    0, <!---#ExtractFromDialer.MessageDelivered_si#,--->
                                    0, <!---#ExtractFromDialer.SingleResponseSurvey_si#,--->
                                    '', <!---'#ExtractFromDialer.XMLResultStr_vch#',--->
                                    XMLControlString_vch,
                                    0, <!---#ExtractFromDialer.IsOptIn_ti#,--->
                                    0 <!---#CurrentBillingAmount#--->
                                FROM
                                    simplequeue.#nameoftempfileforthisprocess#   
                                WHERE
                                    CleanFlag_ti = 6                                                               
                        </cfquery>                        
                                                   
                        
                        <!--- Get End time --->
                        <cfset EndTimeCheckDuplicateUUIDs = NOW()>
                        
                        
                        <!--- Log counts along with --->
                        <!--- Report Results to Table --->
                        <cftry>
                            <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                                INSERT INTO simplequeue.simplexprod_log
                                (
                                    LogType_int,
                                    Distribution_dt,                                        
                                    START_DT,
                                    End_dt
                                )
                                VALUES
                                (
                                    35,
                                    NOW(),                                       
                                    '#LSDateFormat(StartTimeCheckDuplicateUUIDs, 'yyyy-mm-dd')# #LSTimeFormat(StartTimeCheckDuplicateUUIDs, 'HH:mm:ss')#',
                                    '#LSDateFormat(EndTimeCheckDuplicateUUIDs, 'yyyy-mm-dd')# #LSTimeFormat(EndTimeCheckDuplicateUUIDs, 'HH:mm:ss')#'
                                )
                            </cfquery>
                        <cfcatch TYPE="any">
                            <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
                            <cfset ErrorNumber = 105>
                            <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
                        </cfcatch>
                        </cftry>






<!--- Duplicate checks within Batch --->    
					    <cfset StartTimeCheckDuplicateBatchIDs = NOW()>
		
		<!--- Remove duplicates of already distributed from Stage.--->
		<!--- Retrieve unique Batch IDs to loop through; prevents bleed-through. --->

						<cfquery name="getDistinctBatchIdsDupeProcessing" datasource="#Session.DBSourceEBM#">
                            SELECT DISTINCT
                                BatchId_bi
                            FROM
                                simplequeue.#nameoftempfileforthisprocess#
                        </cfquery>
                
                        <cfloop query="getDistinctBatchIdsDupeProcessing">
                           
                           	<!--- Check for wether duplicates allowed or other BR's here--->                           
                           	<cfquery name="getDistinctBatchIdsDupeProcessingFlag" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    AllowDuplicates_ti
                                FROM
                                    simpleobjects.batch
                                WHERE
                                	BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi# 
                           	</cfquery>                           
                           
                           	<!--- Allow advanced users to override this value--->
                           	<cfif getDistinctBatchIdsDupeProcessingFlag.AllowDuplicates_ti EQ 0>
                              
                                <!--- Clean Flag to Duplicate scrubbing Rule against already distibuted--->
                                <cfquery name="UpdateForDuplicatesTT" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplequeue.#nameoftempfileforthisprocess# AS T1
                                    SET 
                                        CleanFlag_ti = 3
                                    WHERE 
                                        DialString1_vch IN (SELECT ContactString_vch FROM simplequeue.contactqueue AS T2 WHERE T2.BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi# AND T2.DTSStatusType_ti > 2 AND T2.ContactString_vch = T1.DialString1_vch AND T2.TypeMask_ti = T1.TypeMask_ti)
                                    AND 
                                        CleanFlag_ti = 1		
                                    AND     
                                        BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi#
                                </cfquery>
                                
                                <!--- Clean Flag to Duplicate scrubbing Rule against already returned results--->
                                <cfquery name="UpdateForDuplicatesTT2" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplequeue.#nameoftempfileforthisprocess# AS T1
                                    SET 
                                        T1.CleanFlag_ti = 3
                                    WHERE 
                                        T1.DialString1_vch IN (SELECT T2.ContactString_vch FROM simplexresults.contactresults AS T2 WHERE BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi# AND T2.ContactString_vch = T1.DialString1_vch)
                                    AND 
                                        T1.CleanFlag_ti = 1	
                                    AND     
                                        BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi#    	
                                </cfquery>
                                
                                 <!--- Clean Flag to Duplicate scrubbing Rule against already returned results--->
                                <cfquery name="UpdateForDuplicatesTT3" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplequeue.#nameoftempfileforthisprocess# AS T1
                                    SET 
                                        T1.CleanFlag_ti = 3
                                    WHERE 
                                        T1.DialString1_vch IN (SELECT T2.ContactString_vch FROM simplexresults.contactresults AS T2 WHERE BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi# AND T2.DTS_UUID_vch = T1.DTS_UUID_vch)
                                    AND 
                                        T1.CleanFlag_ti = 1	
                                    AND     
                                        BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi#    	
                                </cfquery>
                                                            
                                <!--- 1 = Queued 2= Queued on RXDialer, 3=? 4=Extracted--->  
                                <cfquery name="UpdateForDuplicatesDQ" datasource="#Session.DBSourceEBM#">                            
                                    UPDATE 
                                        simplequeue.contactqueue
                                    SET
                                        DTSStatusType_ti = 4                                  
                                    WHERE
                                        BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi#
                                    AND    
                                        ContactString_vch IN (SELECT DialString1_vch FROM simplequeue.#nameoftempfileforthisprocess# WHERE BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi# AND CleanFlag_ti = 3)
                                    AND
                                        DTSStatusType_ti < 4                                                       
                                </cfquery>              
                              
                                                  
                                <!--- Write to our master log first (if coupled only) --->
                                <cfquery name="InsertToCDL" datasource="#Session.DBSourceEBM#">
                                    INSERT INTO simplexresults.contactresults
                                        (	
                                            RXCallDetailId_int,
                                            DTSID_int,
                                            BatchId_bi,
                                            PhoneId_int,
                                            TotalObjectTime_int,
                                            TotalCallTimeLiveTransfer_int,
                                            TotalCallTime_int,
                                            TotalConnectTime_int,
                                            ReplayTotalCallTime_int,
                                            TotalAnswerTime_int,
                                            UserSpecifiedLineNumber_si,
                                            NumberOfHoursToRescheduleRedial_si,
                                            TransferStatusId_ti,
                                            IsHangUpDetected_ti,
                                            IsOptOut_ti,
                                            IsMaxRedialsReached_ti,
                                            IsRescheduled_ti,
                                            CallResult_int,
                                            SixSecondBilling_int,
                                            SystemBilling_int,
                                            RXCDLStartTime_dt,
                                            CallStartTime_dt,
                                            CallEndTime_dt,
                                            CallStartTimeLiveTransfer_dt,
                                            CallEndTimeLiveTransfer_dt,
                                            CallResultTS_dt,
                                            PlayFileStartTime_dt,
                                            PlayFileEndTime_dt,
                                            HangUpDetectedTS_dt,
                                            Created_dt,
                                            DialerName_vch,
                                            CurrTS_vch,
                                            CurrVoice_vch,
                                            CurrTSLiveTransfer_vch,
                                            CurrVoiceLiveTransfer_vch,
                                            CurrCDP_vch,
                                            CurrCDPLiveTransfer_vch,
                                            ContactString_vch,
                                            RedialNumber_int,
                                            TimeZone_ti,
                                            MessageDelivered_si,
                                            SingleResponseSurvey_si,
                                            XMLResultStr_vch,
                                            XMLControlString_vch,
                                            IsOptIn_ti,
                                            ActualCost_int
                                        )
                                        SELECT
                                            0, <!---#ExtractFromDialer.RXCallDetailId_int#,--->
                                            0, <!---#ExtractFromDialer.DTSID_int#,--->
                                            #getDistinctBatchIdsDupeProcessing.BatchId_bi#, <!--- #ExtractFromDialer.BatchId_bi#,--->
                                            0, <!---#ExtractFromDialer.PhoneId_int#,--->
                                            0, <!---#ExtractFromDialer.TotalObjectTime_int#,--->
                                            0, <!---#ExtractFromDialer.TotalCallTimeLiveTransfer_int#,--->
                                            0, <!---#ExtractFromDialer.TotalCallTime_int#,--->
                                            0, <!---#ExtractFromDialer.TotalConnectTime_int#,--->
                                            0, <!---#ExtractFromDialer.ReplayTotalCallTime_int#,--->
                                            0, <!---#ExtractFromDialer.TotalAnswerTime_int#,--->
                                            0, <!---#ExtractFromDialer.UserSpecifiedLineNumber_si#,--->
                                            0, <!---#ExtractFromDialer.NumberOfHoursToRescheduleRedial_si#,--->
                                            0, <!---#ExtractFromDialer.TransferStatusId_ti#,--->
                                            0, <!---#ExtractFromDialer.IsHangUpDetected_ti#,--->
                                            0, <!---#ExtractFromDialer.IsOptOut_ti#,--->
                                            0, <!---#ExtractFromDialer.IsMaxRedialsReached_ti#,--->
                                            0, <!---#ExtractFromDialer.IsRescheduled_ti#,--->
                                            501, <!---#ExtractFromDialer.CallResult_int#,--->
                                            0, <!---#ExtractFromDialer.SixSecondBilling_int#,--->
                                            0, <!---#ExtractFromDialer.SystemBilling_int#,--->
                                            NOW(), <!---'#ExtractFromDialer.RXCDLStartTime_dt#',--->
                                            NOW(),<!---'#ExtractFromDialer.CallStartTime_dt#',--->
                                            NOW(),<!---'#ExtractFromDialer.CallEndTime_dt#',--->
                                            '1900-01-01 00:00:00', <!--- '#ExtractFromDialer.CallStartTimeLiveTransfer_dt#',--->
                                            '1900-01-01 00:00:00', <!---'#ExtractFromDialer.CallEndTimeLiveTransfer_dt#',--->
                                            NOW(), <!---'#ExtractFromDialer.CallResultTS_dt#',--->
                                            '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileStartTime_dt#',--->
                                            '1900-01-01 00:00:00', <!---'#ExtractFromDialer.PlayFileEndTime_dt#',--->
                                            '1900-01-01 00:00:00', <!---'#ExtractFromDialer.HangUpDetectedTS_dt#',--->
                                            NOW(), <!---'#ExtractFromDialer.Created_dt#',--->
                                            NULL, <!---'#ExtractFromDialer.DialerName_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrTS_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrVoice_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrTSLiveTransfer_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrVoiceLiveTransfer_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrCDP_vch#',--->
                                            NULL, <!---'#ExtractFromDialer.CurrCDPLiveTransfer_vch#',--->
                                            DialString1_vch, <!---'#ExtractFromDialer.DialString_vch#',--->
                                            CurrentRedialCount_ti, <!---#ExtractFromDialer.RedialNumber_int#,--->
                                            TimeZone_ti, <!---#ExtractFromDialer.TimeZone_ti#,--->
                                            0, <!---#ExtractFromDialer.MessageDelivered_si#,--->
                                            0, <!---#ExtractFromDialer.SingleResponseSurvey_si#,--->
                                            '', <!---'#ExtractFromDialer.XMLResultStr_vch#',--->
                                            XMLControlString_vch,
                                            0, <!---#ExtractFromDialer.IsOptIn_ti#,--->
                                            0 <!---#CurrentBillingAmount#--->
                                        FROM
                                            simplequeue.#nameoftempfileforthisprocess#   
                                        WHERE
                                            CleanFlag_ti = 3
                                        AND
                                            BatchId_bi = #getDistinctBatchIdsDupeProcessing.BatchId_bi#                                
                                </cfquery>
                            
                            </cfif><!--- Allow advanced users to override this value--->
                            
                        </cfloop>
                       
                       	<!--- Get End time --->
                        <cfset EndTimeCheckDuplicateBatchIDs = NOW()>
                        
                        <!--- Log counts along with --->
                        <!--- Report Results to Table --->
                        <cftry>
                            <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                                INSERT INTO simplequeue.simplexprod_log
                                (
                                    LogType_int,
                                    Distribution_dt,                                        
                                    START_DT,
                                    End_dt
                                )
                                VALUES
                                (
                                    36,
                                    NOW(),                                       
                                    '#LSDateFormat(StartTimeCheckDuplicateBatchIDs, 'yyyy-mm-dd')# #LSTimeFormat(StartTimeCheckDuplicateBatchIDs, 'HH:mm:ss')#',
                                    '#LSDateFormat(EndTimeCheckDuplicateBatchIDs, 'yyyy-mm-dd')# #LSTimeFormat(EndTimeCheckDuplicateBatchIDs, 'HH:mm:ss')#'
                                )
                            </cfquery>
                        <cfcatch TYPE="any">
                            <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
                            <cfset ErrorNumber = 105>
                            <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
                        </cfcatch>
                        </cftry>
                        
