<cfsetting requesttimeout="3600">

<cfparam name="inpDialerIPAddr" default="0.0.0.0">
<cfparam name="VerboseDebug" default="0">

<cfparam name="inpForceUpdate" default="0">

<cfparam name="ProcessedAudioFilesVerifiedUpToDate" default="0">
<cfparam name="ProcessedAudioFilesUpdated" default="0">
<cfparam name="ProcessedAudioFilesBlocked" default="0">
<cfparam name="ProcessedAudioFilesErrors" default="0">
<cfparam name="ProcessedAudioFilesMainFileNotExists" default="0">
<cfparam name="CreationCount" default="0">

<cfparam name="NameOfTempFileForThisProcess" default="Testing1"> 

<!---
 dev.telespeech.com/devjlp/EBM_DEV/management/distribution/act_PushStagedAudioII.cfm?VerboseDebug=1&inpDialerIPAddr=10.11.100.104 
--->


<cfset StartTimePushStagedAudio = NOW()>


<!--- Set here only so if changes can be quickly updated --->
<!---<cfinclude template="../Distribution/data_ScriptPaths.cfm">--->
               
<!---
    <!--- For testing area ...--->
    <cfset PSABatchList = QueryNew("BatchId_bi")>
    
    <cfset QueryAddRow(PSABatchList) />
    <cfset QuerySetCell(PSABatchList, "BatchId_bi", "124") /> 
    
    <cfset QueryAddRow(PSABatchList) />
    <cfset QuerySetCell(PSABatchList, "BatchId_bi", "121") /> 
    
    <cfset QueryAddRow(PSABatchList) />
    <cfset QuerySetCell(PSABatchList, "BatchId_bi", "124") /> 
    
    <cfquery dbTYPE="query" name="getDistinctBatchData">
        SELECT DISTINCT
           BatchId_bi
        FROM 
            PSABatchList   
        ORDER BY
            BatchId_bi ASC       
<!---       WHERE
        	PushLibrary_int = 0
      	AND
            PushElement_int = 0
        AND
            PushScript_int = 0
        AND
            PushSkip_int = 0	<!--- Allow program to optionally specify skipping library push for large pre-pushed programs --->
			
--->
    </cfquery>
    --->
        
    <cfif VerboseDebug gt 0>
	    <cfoutput>
            rxdsLocalWritePath = (#rxdsLocalWritePath#) <BR>
        </cfoutput>    
    </cfif>
       
    
	<!--- Temp location to store found libraries--->
   	<cfset PSAdataout = QueryNew("UserId, LibId, BatchId_bi")>   
                      
    <!--- Distribute all non-specifc script/element/library libraries--->
    <cfquery name="getDistinctBatchData" datasource="#Session.DBSourceEBM#">
        SELECT DISTINCT
           BatchId_bi
        FROM
            simplequeue.#nameoftempfileforthisprocess#
        WHERE
        	PushLibrary_int < 1
      	AND
            PushElement_int < 1
        AND
            PushScript_int < 1
        AND
            PushSkip_int < 1            
    </cfquery>

    <cfif VerboseDebug gt 0>
	    <cfoutput>
            Distinct Batches Found = (#getDistinctBatchData.RecordCount#) <BR>
        </cfoutput>    
    </cfif>
                            
    <cfloop query="getDistinctBatchData">
        
        <cfset PSACurrBatch = "#getDistinctBatchData.BatchId_bi#">
        <cfset PSACurrLib = "">
		<cfset PSACurrUser = "">    
        
        
        <!--- Get User for current batch--->
        <cfquery name="getPSAUserId" datasource="#Session.DBSourceEBM#">
            SELECT             
                UserId_int
            FROM
                simpleobjects.batch
            WHERE
                BatchId_bi = #getDistinctBatchData.BatchId_bi#
        </cfquery>
        
        <cfif getPSAUserId.RecordCount GT 0><cfset PSACurrUser = "#getPSAUserId.UserId_int#"></cfif>
    
	    <cfif VerboseDebug gt 0>
        	<cfoutput>
	            PSACurrUser = #PSACurrUser# <BR />
            </cfoutput>    
        </cfif>
        
        <!--- Get Libraries for current batch--->    
        
         <!--- Publish - use new rxds mp3 logic --->
        <cfinvoke 
             component="#Session.ManagementCFCPath#.publish"
             method="GetSCRIPTLIBs"
             returnvariable="RetValLibs">
                <cfinvokeargument name="INPBATCHID" value="#PSACurrBatch#"/>
        </cfinvoke>
		
		
		<cfif VerboseDebug gt 0>
			<cfoutput>
				<cfdump var="#RetValLibs#">
			</cfoutput>
		</cfif>
		

		<cfloop query="RetValLibs">
        	
            <cfif RetValLibs.SCRIPTLIB GT 0>
				<cfset PSACurrLib = "#RetValLibs.SCRIPTLIB#">
    		<cfelse>
	            <cfset PSACurrLib = "">
            </cfif>
            
			<cfif VerboseDebug gt 0>
            	<cfoutput>
    		        PSACurrLib = #PSACurrLib# <BR>
                </cfoutput>
		    </cfif>
        
			<cfif PSACurrUser NEQ "" AND PSACurrLib NEQ "">        
                
                <!--- Add row to temp table--->
                <cfset QueryAddRow(PSAdataout) />
                      
                <!--- Add User info --->
                <cfset QuerySetCell(PSAdataout, "UserId", "#PSACurrUser#") /> 
            
                <!--- Add Lib info --->
                <cfset QuerySetCell(PSAdataout, "LibId", "#PSACurrLib#") /> 
                
                <!--- Add Lib info --->
                <cfset QuerySetCell(PSAdataout, "BatchId_bi", "#getDistinctBatchData.BatchId_bi#") /> 
                
             </cfif>     
         
        </cfloop>        
    
    </cfloop>
    
    
    <!--- Get the DISTINCT Libraries / Elements --->    
    <cfquery dbTYPE="query" name="GetLibs">
        SELECT DISTINCT
            UserId,
            LibId
        FROM 
            PSAdataout   
        ORDER BY
            UserId, LibId ASC       
    </cfquery>
        
    <cfif VerboseDebug gt 0>
    	<cfoutput>
        	Distinct Libraries Found = (#GetLibs.RecordCount#) <BR>
        </cfoutput>
    </cfif>
  
          
    <!--- Loop over each Lib--->     
    <cfloop query="GetLibs">
    
        <cfif VerboseDebug gt 0>
        	<cfoutput>
                Library = #GetLibs.LibId# <BR>
                User = #GetLibs.UserId# <BR />
                #Session.ManagementCFCPath#.publish <BR />
			</cfoutput>
        </cfif>
        
  
        <cftry>
            <!--- Does User Directory Exist --->
            <cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#">
            
            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                <cfset CreationCount = CreationCount + 1> 
                
                <!--- Still doesn't exist - check your access settings --->
                <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                    <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                </cfif>
                
            <cfelse>
            
                <cfset ExistsCount = ExistsCount + 1> 
            
            </cfif>
            
            <!--- Does Lib Directory Exist --->
			<cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#\L#GetLibs.LibId#">
            
            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrRemoteUserPath#">
            
                <cfset CreationCount = CreationCount + 1> 
                
                <!--- Still doesn't exist - check your access settings --->
                <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                    <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                </cfif>
                
            <cfelse>
            
                <cfset ExistsCount = ExistsCount + 1> 
            
            </cfif>
                    
        <cfcatch type="any">
	        <!--- Just squash for now - will error out gracefully later on in individual scripts--->
            <cfif VerboseDebug gt 0> 
				<cfoutput>
                    Error creating remote User/LIB directory <BR />   
                    \\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#  <BR />
                    \\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#\L#GetLibs.LibId# <BR />                                     
                    <cfdump var="#cfcatch#">
                </cfoutput>    
            </cfif>
        </cfcatch>
        
        </cftry>
        
                          
        <!--- **** DON'T do this here - messses with logic to just check element directory for changes --->
      <!---  	<cfinvoke 
             component="#Session.ManagementCFCPath#.publish"
             method="ValidateRemotePaths"
             returnvariable="RetValAudioPaths">
                <cfinvokeargument name="INPLIBID" value="#GetLibs.LibId#"/>
                <cfinvokeargument name="INPDIALERIPADDR" value="#inpDialerIPAddr#"/>
                <cfinvokeargument name="INPUSERID" value="#GetLibs.UserId#"/>
            </cfinvoke>
            
            <cfif VerboseDebug gt 0>
				<cfoutput>
                	Script Paths  - #RetValAudioPaths.MESSAGE# <BR>
				</cfoutput>
            </cfif>
	  --->
            
        <!--- Get first script --->
        <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
            SELECT                           
                DSEId_int                           
            FROM
                rxds.dselement
            WHERE
                Active_int = 1
                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetLibs.LibId#">
                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetLibs.UserId#">
            ORDER BY
                DSEId_int ASC
        </cfquery>  
        
        <!--- Loop Elements Here --->            
        <cfloop query="GetElements">
        
			<cfif VerboseDebug gt 0>
            	<cfoutput>
                	Element = #GetElements.DSEId_int# <BR>                
                </cfoutput>
            </cfif>

			<!--- \rxds_#GetLibs.UserId#_#GetLibs.LibId#_#GetElements.DSEId_int#_#GetFirstScript.DataId_int#.mp3 --->            
            
           	<!--- Get current side last modified date --->                                
			<cfset MainfilePath = "#rxdsLocalWritePath#\U#GetLibs.UserId#\L#GetLibs.LibId#\E#GetElements.DSEId_int#">
                         
            <cfset MainPathFileExists = DirectoryExists(MainfilePath)>
            <cfif MainPathFileExists> 
                <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
            <cfelse>
                 <cfset MainfileDate = '2000-01-01 00:00:01'>
            </cfif>
            
            <!--- Get remote last modified date --->
            <cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#\L#GetLibs.LibId#\E#GetElements.DSEId_int#">
            <cfset RemotePathFileExists = DirectoryExists(RemotefilePath)>
            
            <cfif RemotePathFileExists> 
                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
            <cfelse>
            
	            <cftry>
					<cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#GetLibs.UserId#\L#GetLibs.LibId#\E#GetElements.DSEId_int#">
                    
					<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

                        <cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
                    <cfelse>
                    
                        <cfset ExistsCount = ExistsCount + 1> 
                    
                    </cfif>
                  
                    
                <cfcatch type="any">
            
                	<!--- Just squash for now - will error out gracefully later on in individual scripts--->
            		<cfif VerboseDebug gt 0> 
                    	Error creating remote ELE directory <BR />                        
                        <cfdump var="#cfcatch#">
                    </cfif>
                    
                </cfcatch>
                
                </cftry>
        
                <cfset RemotefileDate = '1900-01-01 00:00:00'>
            </cfif>
                                                        
            <cfif VerboseDebug gt 0>   
            	<cfoutput>         	                         
                    MainfileDate = #MainfileDate# <BR>       
                    RemotefileDate = #RemotefileDate# <BR>     
                    <cfdump var="#MainfileDate#">
                    <cfdump var="#RemotefileDate#">
                    DateCompare(MainfileDate,RemotefileDate) = #DateCompare(MainfileDate,RemotefileDate)# <BR />           
				</cfoutput>
            </cfif>                
            
            <!--- *** NOTE - What about case where single file has been updated/deleted - then need force update to fix or delete ELE dir--->
            <!--- Performance enhancement - only update scripts if element directory is out of date --->
            <cfif DateCompare(MainfileDate,RemotefileDate) NEQ 0 OR inpForceUpdate GT 0 >
        
        		<cfset CurrScriptDistributionErrors = 0>
                
                <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DATAID_INT,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                DESC_VCH
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetLibs.LibId#">
                                AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetElements.DSEId_int#">
                                AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetLibs.UserId#">
                            ORDER BY
                                DATAID_INT ASC
                </cfquery>  
            
                <!--- Loop Scripts here --->
                <cfloop query="GetScripts">
                                
                    <cftry>
                        
                        <cfif VerboseDebug gt 0>
                        	<cfoutput>
                            	Script = #GetScripts.DATAID_INT# Desc = #GetScripts.DESC_VCH# <BR> 
                                
                                cfinvokeargument name="INPUSERID" value="#GetLibs.UserId#" <BR> 
                                cfinvokeargument name="INPLIBID" value="#GetLibs.LibId#" <BR> 
                                cfinvokeargument name="INPNEXTELEID" value="#GetElements.DSEId_int#" <BR> 
                                cfinvokeargument name="INPNEXTDATAID" value="#GetScripts.DATAID_INT#" <BR> 
                                cfinvokeargument name="INPDIALERIPADDR" value="#inpDialerIPAddr#" <BR> 
                                cfinvokeargument name="inpForceUpdate" value="0"  <BR>  
                                               
                           	</cfoutput>
                        </cfif>
               
                        <!--- Publish - use new rxds mp3 logic --->
                        <cfinvoke 
                             component="#Session.ManagementCFCPath#.publish"
                             method="ValidateRemoteScriptData"
                             returnvariable="RetValAudio">
                                <cfinvokeargument name="INPUSERID" value="#GetLibs.UserId#"/>
                                <cfinvokeargument name="INPLIBID" value="#GetLibs.LibId#"/>
                                <cfinvokeargument name="INPNEXTELEID" value="#GetElements.DSEId_int#"/>
                                <cfinvokeargument name="INPNEXTDATAID" value="#GetScripts.DATAID_INT#"/>
                                <cfinvokeargument name="INPDIALERIPADDR" value="#inpDialerIPAddr#"/>
                                <cfinvokeargument name="inpForceUpdate" value="0"/>            
                        </cfinvoke>
                        
                        <!--- <cfif RetValAudio.>--->
                       
                        <cfswitch expression="#RetValAudio.RXRESULTCODE#">
                            <cfcase value="1"><cfset ProcessedAudioFilesUpdated = ProcessedAudioFilesUpdated + 1></cfcase>
                            <cfcase value="2"><cfset ProcessedAudioFilesUpdated = ProcessedAudioFilesUpdated + 1></cfcase>
                            
                            <cfcase value="3"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                            <cfcase value="4"><cfset ProcessedAudioFilesVerifiedUpToDate = ProcessedAudioFilesVerifiedUpToDate + 1></cfcase>
                       
                            <cfcase value="5"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                            <cfcase value="6"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                       
                            <cfcase value="-4">
                                <cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1>
                                <cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
                                <cfthrow MESSAGE="Invalid Data Specified" type="any" detail="Data specified in the request is invalid." errorcode="-4"> 
                            </cfcase>
                       
                            <!---
                            <cfcase value="-10"><cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1></cfcase>
                            <cfcase value="-2"><cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1></cfcase>
                            --->
                            
                            <cfdefaultcase>
								<cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
								<cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1>
                                
                                <cfif VerboseDebug gt 0>
									<cfoutput>
                                        CurrScriptDistributionErrors = #CurrScriptDistributionErrors# <BR />
                                        RetValAudio = #RetValAudio.MESSAGE# <BR>
                                        <cfdump var="#RetValAudio#">
                                        <cfflush>
                                    </cfoutput>
                                </cfif>          
                                 
                                 
                                <cfthrow MESSAGE="#RetValAudio.MESSAGE#" type="any" detail="RetValAudio.RXRESULTCODE = (#RetValAudio.RXRESULTCODE#) Data specified in the request is invalid." errorcode="-4"> 
                            </cfdefaultcase>
                       
                        </cfswitch>
                        
                        <cfif VerboseDebug gt 0>
                        	<cfoutput>
                                CurrScriptDistributionErrors = #CurrScriptDistributionErrors# <BR />
                                RetValAudio = #RetValAudio.MESSAGE# <BR>
                                <!---<cfdump var="#RetValAudio#">--->
                                <cfflush>
                            </cfoutput>
                        </cfif>          
                                 
                             
                    <cfcatch type="any" >
                
                     
                     	<cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
                     
                        <cfif VerboseDebug gt 0>
                            <cfoutput>Updateing Dial Queue to Hold for this record type due to file push issues = UserId_int (#GetLibs.UserId#) - PushLibrary_int (#GetLibs.LibId#) - PushElement_int (#GetElements.DSEId_int#) -  PushScript_int (#GetScripts.DATAID_INT#)<BR></cfoutput>
                        </cfif>
                                
                         <cfquery dbTYPE="query" name="GetBatchesFromStage">
                            SELECT DISTINCT
                                BatchId_bi
                            FROM 
                                PSAdataout   
                            WHERE
                            	UserId = #GetLibs.UserId#
                            AND
                             	LibId = #GetLibs.LibId#
                        </cfquery>        
                                
                        <cfloop query="GetBatchesFromStage">        
                                
							<!--- Update Simple Queue---> 
                            <cfquery name="SELECTDataToRemoveAudioSection" datasource="#Session.DBSourceEBM#">
                                UPDATE
                                    simplequeue.contactqueue
                                SET
                                    DTSStatusType_ti = 100
                                WHERE
                                    DTSID_int IN
                                (            
                                SELECT
                                    DQDTSId_int
                                FROM
                                    simplequeue.#nameoftempfileforthisprocess#
                                WHERE
                                    UserId_int = #GetLibs.UserId#
                                AND
                                    BatchId_bi = #GetBatchesFromStage.BatchId_bi#              
                                 )
                            </cfquery>
                            
                            <!--- Update current temp table--->        
                            <cfquery name="RemovceQueuedData" datasource="#Session.DBSourceEBM#">
                                DELETE FROM
                                    simplequeue.#nameoftempfileforthisprocess#
                                WHERE
                                    UserId_int = #GetLibs.UserId#
                                AND
                                    BatchId_bi = #GetBatchesFromStage.BatchId_bi# 
                            </cfquery>                

						</cfloop>
                    
                        <cfset ENA_Message ="SimpleX Error Notice. Updateing Dial Queue to Hold - UserId_int (#GetLibs.UserId#) - PushLibrary_int (#GetLibs.LibId#) - PushElement_int (#GetElements.DSEId_int#) -  PushScript_int (#GetScripts.DATAID_INT#)">
						<cfset ErrorNumber = 102>
                        <cfinclude template="../NOC/rep_EscalationNotificationActions.cfm">
                    
                    </cfcatch>
                        
                </cftry>
        
                </cfloop> <!--- Loop Scripts here --->
			
				<!--- if all is distributed OK then update to same as source --->      
                <cfif CurrScriptDistributionErrors EQ 0>
                          
                    <cftry>
           
                        <!--- Set time on RXDialer to be same as time on main script directory--->             
                        <cfscript>
                            FileSetLastModified(RemotefilePath, "#LSDateFormat(MainfileDate, 'yyyy-mm-dd')# #LSTimeFormat(MainfileDate, 'HH:mm:ss.l')#"); 
                        </cfscript>
                        
                        <cfif VerboseDebug gt 0>   
                            <cfoutput>         	                                                        
                                RemotefileDate FileSetLastModified #MainfileDate# <BR>                
                            </cfoutput>
                        </cfif>           
                    
                                
                    <cfcatch type="any">
                    
                        <cfset ENA_Message ="Error Setting File\Folder Last Modified Datetime - by Batch XML">
                        <cfset ErrorNumber = 103>
                        <cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
                                
                        <cfif VerboseDebug gt 0>   
                            <cfoutput>         	                                                        
                                ERROR trying to RemotefileDate FileSetLastModified "#LSDateFormat(MainfileDate, 'yyyy-mm-dd')# #LSTimeFormat(MainfileDate, 'HH:mm:ss.l')#" <BR>                
                            </cfoutput>
                        </cfif>
                    
                    </cfcatch>            
                    </cftry>
               
               <cfelse>
               	
	                <cfif VerboseDebug gt 0>   
						<cfoutput>         	                                                        
                            WARNING: There were (#CurrScriptDistributionErrors#) Script Distributon Errors so no directory timestamp sync. <BR>                
                        </cfoutput>
                    </cfif>
               
               </cfif>
               
                    
                
            <cfelse>
				<cfif VerboseDebug gt 0>
                    <cfoutput>Remote Ele Directory is up to date = UserId_int (#GetLibs.UserId#) - PushLibrary_int (#GetLibs.LibId#) - PushElement_int (#GetElements.DSEId_int#)<BR></cfoutput>
                </cfif>

			</cfif> <!--- Only update if needed --->
                       
        </cfloop> <!--- Loop Elements Here ---> 
    
    </cfloop> <!--- Loop over each Lib---> 


<!---
**********************************************************************************************************************************************************

Distribute specified script libraries\elements\scripts

**********************************************************************************************************************************************************
--->
        
    <!--- Distribute Specific script libraries--->    
    <cfquery name="getDistinctAudioDataII" datasource="#Session.DBSourceEBM#">
        SELECT DISTINCT
            UserId_int,
            PushLibrary_int,
            PushElement_int,
            PushScript_int
        FROM
            simplequeue.#nameoftempfileforthisprocess#
        WHERE
            (
                PushLibrary_int > 0
            OR
                PushElement_int > 0
            OR
                PushScript_int > -1
            )
            AND
                PushSkip_int = 0 
    </cfquery>
                          
                      
    <cfloop query="getDistinctAudioDataII">

		
        <cfif getDistinctAudioDataII.PushScript_int LT 1 AND getDistinctAudioDataII.PushElement_int LT 1>
            
            <!--- Get first script --->
            <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                SELECT                           
                    DSEId_int                           
                FROM
                    rxds.dselement
                WHERE
                    Active_int = 1
                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getDistinctAudioDataII.PushLibrary_int#">
                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getDistinctAudioDataII.UserId_int#">
                ORDER BY
                    DSEId_int ASC
            </cfquery>  

		<cfelse>
        	
			<cfset GetElements = QueryNew("DSEId_int")>   
             
            <cfset QueryAddRow(GetElements) />
            <cfset QuerySetCell(GetElements, "DSEId_int", "#getDistinctAudioDataII.PushElement_int#") /> 
                   
        </cfif>
        
        <!--- Loop Elements Here --->            
        <cfloop query="GetElements">
        
			<cfif VerboseDebug gt 0>
            	<cfoutput>
                	Element = #GetElements.DSEId_int# <BR>                
                </cfoutput>
            </cfif>

			<!--- \rxds_#getDistinctAudioDataII.UserId_int#_#getDistinctAudioDataII.PushLibrary_int#_#GetElements.DSEId_int#_#GetFirstScript.DataId_int#.mp3 --->            
            
           	<!--- Get current side last modified date --->                                
			<cfset MainfilePath = "#rxdsLocalWritePath#\U#getDistinctAudioDataII.UserId_int#\L#getDistinctAudioDataII.PushLibrary_int#\E#GetElements.DSEId_int#">
                         
            <cfset MainPathFileExists = DirectoryExists(MainfilePath)>
            <cfif MainPathFileExists> 
                <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
            <cfelse>
                 <cfset MainfileDate = '2000-01-01 00:00:01'>
            </cfif>
            
            <!--- Get remote last modified date --->
            <cfset RemotefilePath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#getDistinctAudioDataII.UserId_int#\L#getDistinctAudioDataII.PushLibrary_int#\E#GetElements.DSEId_int#">
            <cfset RemotePathFileExists = DirectoryExists(RemotefilePath)>
            
            <cfif RemotePathFileExists> 
                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
            <cfelse>
            
	            <cftry>
					<cfset CurrRemoteUserPath = "\\#INPDIALERIPADDR#\#rxdsRemoteRXDialerPath#\U#getDistinctAudioDataII.UserId_int#\L#getDistinctAudioDataII.PushLibrary_int#\E#GetElements.DSEId_int#">
                    
					<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

                        <cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
                    <cfelse>
                    
                        <cfset ExistsCount = ExistsCount + 1> 
                    
                    </cfif>
                  
                    
                <cfcatch type="any">
                	<!--- Just squash for now - will error out gracefully later on in individual scripts--->
                </cfcatch>
                
                </cftry>
        
                <cfset RemotefileDate = '1900-01-01 00:00:00'>
            </cfif>
                                                        
            <cfif VerboseDebug gt 0>   
            	<cfoutput>         	                         
                    MainfileDate = #MainfileDate# <BR>       
                    RemotefileDate = #RemotefileDate# <BR>     
                    <cfdump var="#MainfileDate#">
                    <cfdump var="#RemotefileDate#">
                    DateCompare(MainfileDate,RemotefileDate) = #DateCompare(MainfileDate,RemotefileDate)# <BR />           
				</cfoutput>
            </cfif>                

            
            <!--- *** NOTE - What about case where single file has been updated/deleted - then need force update to fix or delete ELE dir--->
            <!--- Performance enhancement - only update scripts if element directory is out of date --->
            <cfif DateCompare(MainfileDate,RemotefileDate) NEQ 0 OR inpForceUpdate GT 0 >
        
        		<cfset CurrScriptDistributionErrors = 0>
                
                <cfif getDistinctAudioDataII.PushScript_int LT 1 AND getDistinctAudioDataII.PushElement_int LT 1>
                
                    <cfquery name="GetScripts" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DATAID_INT,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    DESC_VCH
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getDistinctAudioDataII.PushLibrary_int#">
                                    AND DSEId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getDistinctAudioDataII.PushElement_int#">
                                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getDistinctAudioDataII.UserId_int#">
                                ORDER BY
                                    DATAID_INT ASC
                    </cfquery>  

				<cfelse>
        	
					<cfset GetScripts = QueryNew("DATAID_INT, DESC_VCH")>   
                     
                    <cfset QueryAddRow(GetScripts) />
                    <cfset QuerySetCell(GetScripts, "DATAID_INT", "#getDistinctAudioDataII.PushScript_int#") /> 
                    <cfset QuerySetCell(GetScripts, "DESC_VCH", "Push Script Specified") /> 
                           
                </cfif>
                
                <!--- Loop Scripts here --->
                <cfloop query="GetScripts">
                                
                    <cftry>
                        
                        <cfif VerboseDebug gt 0>
                        	<cfoutput>
                            	Script = #GetScripts.DATAID_INT# Desc = #GetScripts.DESC_VCH# <BR>                
                           	</cfoutput>
                        </cfif>
               
                        <!--- Publish - use new rxds mp3 logic --->
                        <cfinvoke 
                             component="#Session.ManagementCFCPath#.publish"
                             method="ValidateRemoteScriptData"
                             returnvariable="RetValAudio">
                                <cfinvokeargument name="INPUSERID" value="#getDistinctAudioDataII.UserId_int#"/>
                                <cfinvokeargument name="INPLIBID" value="#getDistinctAudioDataII.PushLibrary_int#"/>
                                <cfinvokeargument name="INPNEXTELEID" value="#GetElements.DSEId_int#"/>
                                <cfinvokeargument name="INPNEXTDATAID" value="#GetScripts.DATAID_INT#"/>
                                <cfinvokeargument name="INPDIALERIPADDR" value="#inpDialerIPAddr#"/>
                                <cfinvokeargument name="inpForceUpdate" value="0"/>            
                        </cfinvoke>
                        
                        <!--- <cfif RetValAudio.>--->
                       
                        <cfswitch expression="#RetValAudio.RXRESULTCODE#">
                            <cfcase value="1"><cfset ProcessedAudioFilesUpdated = ProcessedAudioFilesUpdated + 1></cfcase>
                            <cfcase value="2"><cfset ProcessedAudioFilesUpdated = ProcessedAudioFilesUpdated + 1></cfcase>
                            
                            <cfcase value="3"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                            <cfcase value="4"><cfset ProcessedAudioFilesVerifiedUpToDate = ProcessedAudioFilesVerifiedUpToDate + 1></cfcase>
                       
                            <cfcase value="5"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                            <cfcase value="6"><cfset ProcessedAudioFilesMainFileNotExists = ProcessedAudioFilesMainFileNotExists + 1></cfcase>
                       
                            <cfcase value="-4">
                                <cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1>
                                <cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
                                <cfthrow MESSAGE="Invalid Data Specified" type="any" detail="Data specified in the request is invalid." errorcode="-4"> 
                            </cfcase>
                       
                            <!---
                            <cfcase value="-10"><cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1></cfcase>
                            <cfcase value="-2"><cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1></cfcase>
                            --->
                            
                            <cfdefaultcase>
								<cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
								<cfset ProcessedAudioFilesErrors = ProcessedAudioFilesErrors + 1>
                                <cfthrow MESSAGE="#RetValAudio.MESSAGE#" type="any" detail="Data specified in the request is invalid." errorcode="-4"> 
                            </cfdefaultcase>
                       
                        </cfswitch>
                        
                        <cfif VerboseDebug gt 0>
                        	<cfoutput>
                            	CurrScriptDistributionErrors = #CurrScriptDistributionErrors# <BR />
                                RetValAudio = #RetValAudio.MESSAGE# <BR>
                               <!--- <cfdump var="#RetValAudio#"> <BR />--->
                                <cfflush>
                            </cfoutput>
                        </cfif>          
                                 
                             
                    <cfcatch type="any" >
                
                     
                     	<cfset CurrScriptDistributionErrors = CurrScriptDistributionErrors + 1>
                     
                        <cfif VerboseDebug gt 0>
                            <cfoutput>Updating Dial Queue to Hold for this record type due to file push issues = UserId_int (#getDistinctAudioDataII.UserId_int#) - PushLibrary_int (#getDistinctAudioDataII.PushLibrary_int#) - PushElement_int (#GetElements.DSEId_int#) -  PushScript_int (#GetScripts.DATAID_INT#) - Message = #cfcatch.MESSAGE#<BR></cfoutput>
                        </cfif>
                                
                        <!--- Update Simple Queue---> 
                        <cfquery name="SELECTDataToRemoveAudioSection" datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplequeue.contactqueue
                            SET
                                DTSStatusType_ti = 100
                            WHERE
                                DTSID_int IN
                            (            
                            SELECT
                                DQDTSId_int
                            FROM
                                simplequeue.#nameoftempfileforthisprocess#
                            WHERE
                                UserId_int = #getDistinctAudioDataII.UserId_int#
                            AND
                                PushLibrary_int = #getDistinctAudioDataII.PushLibrary_int#
                            AND 	
                                PushElement_int = #GetElements.DSEId_int#
                            AND
                                PushScript_int = #GetScripts.DATAID_INT#                
                             )
                        </cfquery>
                        
                        <!--- Update current temp table--->        
                        <cfquery name="RemovceQueuedData" datasource="#Session.DBSourceEBM#">
                            DELETE FROM
                                simplequeue.#nameoftempfileforthisprocess#
                            WHERE
                                UserId_int = #getDistinctAudioDataII.UserId_int#
                            AND
                                PushLibrary_int = #getDistinctAudioDataII.PushLibrary_int#
                            AND 	
                                PushElement_int = #GetElements.DSEId_int#
                            AND
                                PushScript_int = #GetScripts.DATAID_INT#
                        </cfquery>                
                    
                        <cfset ENA_Message ="SimpleX Error Notice. Updateing Dial Queue to Hold - UserId_int (#getDistinctAudioDataII.UserId_int#) - PushLibrary_int (#getDistinctAudioDataII.PushLibrary_int#) - PushElement_int (#GetElements.DSEId_int#) -  PushScript_int (#GetScripts.DATAID_INT#) - Message = #cfcatch.MESSAGE#">
                        <cfset ErrorNumber = 104>
                        <cfinclude template="../NOC/rep_EscalationNotificationActions.cfm">
                    
                    </cfcatch>
                        
                </cftry>
        
                </cfloop> <!--- Loop Scripts here --->
			
				<!--- if all is distributed OK then update to same as source --->      
                <cfif CurrScriptDistributionErrors EQ 0>
                          
                    <cftry>
           
                        <!--- Set time on RXDialer to be same as time on main script directory--->             
                        <cfscript>
                            FileSetLastModified(RemotefilePath, "#LSDateFormat(MainfileDate, 'yyyy-mm-dd')# #LSTimeFormat(MainfileDate, 'HH:mm:ss.l')#"); 
                        </cfscript>
                        
                        <cfif VerboseDebug gt 0>   
                            <cfoutput>         	                                                        
                                RemotefileDate FileSetLastModified #MainfileDate# <BR>                
                            </cfoutput>
                        </cfif>           
                    
                                
                    <cfcatch type="any">
                    
                        <cfset ENA_Message ="Error Setting File\Folder Last Modified Datetime - By Specified Object">
                        <cfset ErrorNumber = 101>
                        <cfinclude template="../NOC/act_EscalationNotificationActions.cfm">                        
                                
                        <cfif VerboseDebug gt 0>   
                            <cfoutput>         	                                                        
                                ERROR trying to RemotefileDate FileSetLastModified "#LSDateFormat(MainfileDate, 'yyyy-mm-dd')# #LSTimeFormat(MainfileDate, 'HH:mm:ss.l')#" <BR>                
                            </cfoutput>
                        </cfif>
                    
                    </cfcatch>            
                    </cftry>
               
               <cfelse>
               	
	                <cfif VerboseDebug gt 0>   
						<cfoutput>         	                                                        
                            WARNING: There were (#CurrScriptDistributionErrors#) Script Distributon Errors so no directory timestamp sync. <BR>                
                        </cfoutput>
                    </cfif>
               
               </cfif>
               
                    
                
            <cfelse>
				<cfif VerboseDebug gt 0>
                    <cfoutput>Remote Ele Directory is up to date = UserId_int (#getDistinctAudioDataII.UserId_int#) - PushLibrary_int (#getDistinctAudioDataII.PushLibrary_int#) - PushElement_int (#GetElements.DSEId_int#)<BR></cfoutput>
                </cfif>

			</cfif> <!--- Only update if needed --->
                       
        </cfloop> <!--- Loop Elements Here ---> 
                    
    </cfloop>  <!--- Loop specific libraries--->   


  	<!--- Get End time --->
	<cfset EndTimePushStagedAudio = NOW()>
    
    <!--- Log counts along with --->
    <!--- Report Results to Table --->
    <cftry>
        <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.simplexprod_log
            (
                LogType_int,
                Distribution_dt,                                        
                START_DT,
                End_dt
            )
            VALUES
            (
                38,
                NOW(),                                       
                '#LSDateFormat(StartTimePushStagedAudio, 'yyyy-mm-dd')# #LSTimeFormat(StartTimePushStagedAudio, 'HH:mm:ss')#',
                '#LSDateFormat(EndTimePushStagedAudio, 'yyyy-mm-dd')# #LSTimeFormat(EndTimePushStagedAudio, 'HH:mm:ss')#'
            )
        </cfquery>
    <cfcatch TYPE="any">
        <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
        <cfset ErrorNumber = 105>
        <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    </cfcatch>
    </cftry>