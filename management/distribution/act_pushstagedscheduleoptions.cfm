

<cfparam name="inpDialerIPAddr" default="0.0.0.0">

<cfquery name="getDistinctBatchIds" datasource="#Session.DBSourceEBM#">
    SELECT DISTINCT
        BatchId_bi
    FROM
        simplequeue.#nameoftempfileforthisprocess#
</cfquery>
                        
                        
<cfloop query="getDistinctBatchIds">

	<!--- Update schedule options for each batch Id --->
    <cfinvoke 
     component="#Session.ManagementCFCPath#.publish"
     method="UpdateScheduleOptionsRemoteRXDialer"
     returnvariable="RetValSchedule">
        <cfinvokeargument name="INPBATCHID" value="#getDistinctBatchIds.BatchId_bi#"/>
        <cfinvokeargument name="inpDialerIPAddr" value="#inpDialerIPAddr#"/>
    </cfinvoke>
    
    <cfif VerboseDebug gt 0>
        RetValSchedule = <cfdump var="#RetValSchedule#"> <BR>
    </cfif>
            
            
</cfloop>          