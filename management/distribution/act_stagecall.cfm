<cfparam name="inpDialerIPAddr" default="0.0.0.0"><!---  10.4.0.64 --->
<cfparam name="INPBATCHID" default="0">
<cfparam name="inpListId" default="0">
<cfparam name="inpUserId" default="0">
<cfparam name="inpCampaignId" default="0">
<cfparam name="inpCampaignTypeId" default="0">
<cfparam name="inpScheduled_dt" default="3001-01-01 00:00:00">
<cfparam name="inpDialString" default="9999999999">
<cfparam name="inpDialString2" default="">
<cfparam name="inpTimeDelay" default="0">
<cfparam name="ourReply" default="1,1">
<cfparam name="inpInvalidNumber" default="0">
<cfparam name="inpTransferNumber" default="">
<cfparam name="VerboseDebug" default="0">
<cfparam name="isCoupled" default="1">

<cfoutput>
	<cfset ourReply = "1,1" >

	<!--- Remove () and - --->
	<cfset OriginalinpDialString = inpDialString>
	<cfset inpDialString = Replace(inpDialString,')', '')>
	<cfset inpDialString = Replace(inpDialString,'(', '')>
	<cfset inpDialString = Replace(inpDialString,'-', '')>

	<!--- Set schedule date based on input parameters (time zone?) --->
	<cfset inpScheduled_dt = DateFormat(DateAdd('n',inpTimeDelay,Now()),'yyyy-mm-dd') & " " & TimeFormat(DateAdd('n',inpTimeDelay,Now()),'HH:mm:ss')>

	<!--- Add To Phone List - Returns CurrResult and TimeZone--->
	<cfset CurrResult = "0">
	<cfinclude template="act_InsertPhoneNumberIntoList.cfm">

	<cfif VerboseDebug gt 0>
		Curr Phone Insert Result = #CurrResult# <BR>
		inpDialString = #inpDialString# <BR>
		CurrResult = #CurrResult# <BR>
		TimeZone_ti = #TimeZone_ti# <BR>
		inpTimeDelay = #inpTimeDelay# <BR>
		inpScheduled_dt = #inpScheduled_dt# <BR>
		NOW() = #NOW()# <BR>
		INPBATCHID = #INPBATCHID#<BR>
	</cfif>

	<cfif TimeZone_ti gt 0>
		 <cfinclude template="act_DistributeToStage.cfm">
	<cfelse>
		<cfset inpInvalidNumber=1>
	</cfif>
</cfoutput>