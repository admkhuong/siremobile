<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Test Path Access</title>
</head>

<cfoutput>
	<script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery-1.4.4.min.js"></script>
    <script TYPE="text/javascript" src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
</cfoutput>
   
   

<cfparam name="GO" default="0">
<cfparam name="inpDialerIPAddr" default="10.11.0.165">
<cfparam name="rxdsRemoteRXDialerPath" default="DynamicLibraries">


<script type="text/javascript">
	$(document).ready(function()
	{				
		$("#GoTest").bind('click', function()
		{							
			window.location = "dsp_testremotefileaccess.cfm?inpDialerIPAddr=" + encodeURI($("#RemoteIP").val()) + "&rxdsRemoteRXDialerPath=" + encodeURI($("#RemotePath").val()) + "&GO=1";
			
		});	
					
	});
    
</script>


 
    
<body>

<cfoutput>

IP Address <BR />
<input type="text" id="RemoteIP" name="inpDialerIPAddr" value="#inpDialerIPAddr#" />

<BR />

Remote file Path <BR />
<input type="text" id="RemotePath" name="rxdsRemoteRXDialerPath" value="#rxdsRemoteRXDialerPath#" />


<div style="margin-top:40px;">
	<a href="##" id="GoTest">Go!</a>
</div>


<cfif GO GT 0>

			<!--- Does Lib Directory Exist --->
			<cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\UTEST303">

			CurrRemoteUserPath = #CurrRemoteUserPath# <BR />
            
            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                
                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                <cfdirectory action="create" directory="#CurrRemoteUserPath#">
                                            
                <!--- Still doesn't exist - check your access settings --->
                <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                    <cfthrow MESSAGE="Unable to create remote user script library directory " TYPE="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                </cfif>
                
            <cfelse>
            
               #CurrRemoteUserPath# Exists already
            
            </cfif>


</cfif>

</cfoutput>


</body>
</html>