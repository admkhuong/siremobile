

REQUIREMENTS FOR RUNNING THE SERVLET

==============================================================

1) In order to run the programs, you will have to download "javax.servlet.api-3.0.1.jar" and add it to the Library of Netbeans IDE after placing it inside the "src" folder. 

2) You would have to enter the servlets details in "web.xml" for each Java programs.