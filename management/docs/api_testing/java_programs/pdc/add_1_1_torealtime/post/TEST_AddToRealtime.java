//import com.google.gson.Gson;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
//import java.net.URL;
//import java.net.URLConnection;
//import java.net.URLEncoder;
//import java.sql.Date;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.TimeZone;
//import javax.crypto.Mac;
//import javax.crypto.spec.SecretKeySpec;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.crypto.Mac;
//import org.apache.commons.codec.binary.Hex;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
////import java.net.URL;
//import java.security.SecureRandom;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.KeyManager;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSession;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//
//@WebServlet("/TEST_AddToRealtime")
//public class TEST_AddToRealtime extends HttpServlet {
//    private static final long serialVersionUID = 1L;
//       
//   
//    public TEST_AddToRealtime() {
//        super();
//        
//    }
//
//    
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        
//        response.setContentType("text/html;charset=UTF-8");
//        
//        PrintWriter out = response.getWriter();
//     
//      
//        try {
//            out.println("<!DOCTYPE html>");  // HTML 5
//            out.println("<html><head>");
//            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
//            out.println(new Gson().toJson(Add_To_RealTime(/*"cpp test - new"*/)).replace("\\",""));
//            out.println("<head><title>Add Request To RealTime</title></head>");
//            out.println("<body>");
//            out.println("<h3>Request Added To RealTime</h3>");
//            out.println("</body></html>");
//        } 	
//        catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(TEST_AddToRealtime.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (KeyManagementException ex) {
//            Logger.getLogger(TEST_AddToRealtime.class.getName()).log(Level.SEVERE, null, ex);
//        }        finally {
//            out.close();  
//        }
//    }
//    
//    public static Object Add_To_RealTime(/*String cppDesc*/) throws IOException, NoSuchAlgorithmException, KeyManagementException {
//        String accessKey = "FDE3568B165B1C510591";
//        String secretKey = "CAd9fbd60f81e66EAFd79455607e89631eAA41/0";
//        String uRLCppList = "https://ebmdevii.messagebroadcast.com/webservice/ebm/PDC/AddToRealTime";
//        String method = "POST";
//        java.util.Date currentTime = new java.util.Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
//        
//        
//
//        // Give it to me in GMT time.
//        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//        String dateTimeString = sdf.format(currentTime);
//                
//        String signature = generateSignature(method, secretKey, dateTimeString);
//        String authorization = accessKey + ":" + signature;
//        Map<String, String> params = new HashMap<String, String>();
//        //params.put("cppListDesc", cppDesc);
//        
//        String[] result = sendHttpRequest(uRLCppList, "POST", params, dateTimeString, authorization); 
//        
//        return result;
//    }
//    
//    
//    public static String[] sendHttpRequest(String requestUrl, String method, Map<String, String> params, String dateTimeString, String authorization) throws IOException, NoSuchAlgorithmException, KeyManagementException {
//        List<String> response = new ArrayList<String>();
//            
//        StringBuffer requestParams = new StringBuffer();
//        
//        if (params != null && params.size() > 0) {
//            Iterator<String> paramIterator = params.keySet().iterator();
//            while (paramIterator.hasNext()) {
//                String key = paramIterator.next();
//                String value = params.get(key);
//                requestParams.append(URLEncoder.encode(key, "UTF-8"));
//                requestParams.append("=").append(URLEncoder.encode(value, "UTF-8"));
//                requestParams.append("&");
//            }
//        }
//        // configure the SSLContext with a TrustManager
//        SSLContext ctx = SSLContext.getInstance("TLS");
//        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
//        SSLContext.setDefault(ctx);
//        
//        
//        URL url = new URL(requestUrl);
//        HttpsURLConnection urlConn = (HttpsURLConnection)url.openConnection();
//        urlConn.setRequestProperty("accept", "application/json");
//        urlConn.setRequestProperty("datetime", dateTimeString);
//        urlConn.setRequestProperty("authorization", authorization);
//        
//        urlConn.setHostnameVerifier(new HostnameVerifier() {
//            @Override
//            public boolean verify(String arg0, SSLSession arg1) {
//                return true;
//            }
//        });
//       
//        System.out.println(urlConn.getResponseCode());
//        urlConn.disconnect();
//        urlConn.setUseCaches(false);
//        
//        // the request will return a response
//        urlConn.setDoInput(true);
//        
//        if ("POST".equals(method)) {
//            // set request method to POST
//            urlConn.setDoOutput(true);
//        } else {
//            // set request method to GET
//            urlConn.setDoOutput(false);
//        }
//        
//        if ("POST".equals(method) && params != null && params.size() > 0) {
//            OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream());
//            writer.write(requestParams.toString());
//            writer.flush();  
//        }
//        
//        // reads response, store line by line in an array of Strings
//        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream())); 
//         
//        String line = "";
//        while ((line = reader.readLine()) != null) {
//            response.add(line);
//        }
//        
//        reader.close();
//        
//        return (String[]) response.toArray(new String[0]);
//    }
//     
//    public static String generateSignature(String method, String secretKey, String dateTimeString) {		
//        
//        String cs = String.format("%s\n\n\n%s\n\n\n", method, dateTimeString);
//        String signature = createSignature(cs, secretKey);
//        
//        return  signature;
//    }
//    
//    public static String createSignature(String stringIn, String scretKey) {
//    
//        String fixedData = stringIn.replace('\n', (char)10);
//        // Calculate the hash of the information
//        String digest = hmacSha1(scretKey, fixedData);
//        
//        return digest;
//    } 
//    
//    private static class DefaultTrustManager implements X509TrustManager {
//
//        @Override
//        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
//
//        @Override
//        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
//
//        @Override
//        public X509Certificate[] getAcceptedIssuers() {
//            return null;
//        }
//    }
//    
//    
//    public static String hmacSha1(String key, String value) {
//        try {
//            // Get an hmac_sha1 key from the raw key bytes
//            byte[] keyBytes = key.getBytes("iso-8859-1");           
//            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
//
//            // Get an hmac_sha1 Mac instance and initialize with the signing key
//            Mac mac = Mac.getInstance("HmacSHA1");
//            mac.init(signingKey);
//
//            // Compute the hmac on input data bytes
//            byte[] rawHmac = mac.doFinal(value.getBytes("iso-8859-1"));
//
//            //  Covert array of Hex bytes to a String
//            return Base64.encode(rawHmac);
//            
//            //return new String(hexBytes, "UTF-8");
//        } 
//        catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//}




// ORIGINAL WORKING CODE WITHOUT ANY SSL LOGIC
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.crypto.Mac;
import org.apache.commons.codec.binary.Hex;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;


@WebServlet("/TEST_AddToRealtime")
public class TEST_AddToRealtime extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
   
    public TEST_AddToRealtime() {
        super();
        
    }

    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.setContentType("text/html;charset=UTF-8");
        // Allocate a output writer to write the response message into the network socket
        PrintWriter out = response.getWriter();
     
       // Write the response message, in an HTML page
        try {
            out.println("<!DOCTYPE html>");  // HTML 5
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println(new Gson().toJson(Add_To_RealTime("cpp test - new")));
            out.println("<head><title>Add Request To RealTime</title></head>");
            out.println("<body>");
            out.println("<h3>Request Added To RealTime</h3>");
            out.println("</body></html>");
        } 	
        finally {
            out.close();  // Always close the output writer
        }
    }
    
    public static Object Add_To_RealTime(String cppDesc) throws IOException {
        String accessKey = "FDE3568B165B1C510591";
        String secretKey = "CAd9fbd60f81e66EAFd79455607e89631eAA41/0";
        String uRLCppList = "http://mbdemodev.messagebroadcast.com/webservice/ebm/PDC/AddToRealTime";
        String method = "POST";
        java.util.Date currentTime = new java.util.Date();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        
        

        // Give it to me in GMT time.
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String dateTimeString = sdf.format(currentTime);
                
        String signature = generateSignature(method, secretKey, dateTimeString);
        String authorization = accessKey + ":" + signature;
        Map<String, String> params = new HashMap<String, String>();
        params.put("cppListDesc", cppDesc);
        
        String[] result = sendHttpRequest(uRLCppList, "POST", params, dateTimeString, authorization); 
        
        return result;
    }
    
    
    public static String[] sendHttpRequest(String requestUrl, String method, Map<String, String> params, String dateTimeString, String authorization) throws IOException {
        List<String> response = new ArrayList<String>();
            
        StringBuffer requestParams = new StringBuffer();
        
        if (params != null && params.size() > 0) {
            Iterator<String> paramIterator = params.keySet().iterator();
            while (paramIterator.hasNext()) {
                String key = paramIterator.next();
                String value = params.get(key);
                requestParams.append(URLEncoder.encode(key, "UTF-8"));
                requestParams.append("=").append(URLEncoder.encode(value, "UTF-8"));
                requestParams.append("&");
            }
        }
        
        URL url = new URL(requestUrl);
        URLConnection urlConn = url.openConnection();
        urlConn.setRequestProperty("accept", "application/json");
        urlConn.setRequestProperty("datetime", dateTimeString);
        urlConn.setRequestProperty("authorization", authorization);
       
        urlConn.setUseCaches(false);
        
        // the request will return a response
        urlConn.setDoInput(true);
        
        if ("POST".equals(method)) {
            // set request method to POST
            urlConn.setDoOutput(true);
        } else {
            // set request method to GET
            urlConn.setDoOutput(false);
        }
        
        if ("POST".equals(method) && params != null && params.size() > 0) {
            OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream());
            writer.write(requestParams.toString());
            writer.flush();  
        }
        
        // reads response, store line by line in an array of Strings
        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream())); 
         
        String line = "";
        while ((line = reader.readLine()) != null) {
            response.add(line);
        }
        
        reader.close();
        
        return (String[]) response.toArray(new String[0]);
    }
     
    public static String generateSignature(String method, String secretKey, String dateTimeString) {		
        
        String cs = String.format("%s\n\n\n%s\n\n\n", method, dateTimeString);
        String signature = createSignature(cs, secretKey);
        
        return  signature;
    }
    
    public static String createSignature(String stringIn, String scretKey) {
    
        String fixedData = stringIn.replace('\n', (char)10);
        // Calculate the hash of the information
        String digest = hmacSha1(scretKey, fixedData);
        
        return digest;
    } 
    
    
    public static String hmacSha1(String key, String value) {
        try {
            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = key.getBytes("iso-8859-1");           
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(value.getBytes("iso-8859-1"));

            //  Covert array of Hex bytes to a String
            return Base64.encode(rawHmac);
            
            //return new String(hexBytes, "UTF-8");
        } 
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}