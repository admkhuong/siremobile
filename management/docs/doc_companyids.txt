For the Company linked accounts I am thinking along the lines of the following…

In the application.cfc  I've added a new variable for tracking who is really logged in but is sharing a master Company UserID – SESSION.CompanyUserId
<cfparam name="SESSION.CompanyUserId" default="#Session.USERID#">

UserID's are still globally unique to the system but the main Session.USERID is the company account everything is linked to.

Permissions should then be linked to the SESSION.CompanyUserId and interface features that are turned off or greyed out are based on the SESSION.CompanyUserId

A normal single user is one where the SESSION.CompanyUserId is the same as the Session.USERID.

The login processing page(s) will need to be updated to check the UserAccount table to see if there is a CompanyAccountId_int associated with it that is different than the current record's UserId_int. IF so then the SESSION.CompanyUserId is the current login infos UserId_int and the main Session.USERID would then be the CompanyAccountId_int

An extra field needs to be handled in the cookies section to store and recall the SESSION.CompanyUserId.

We will need a master management interface that is capable of linking existing users to a CompanyAccountId_int.

The master account will need to be able to add new user accounts based on its own UserID_int as the CompanyAccountId_int.


Does this make sense?
