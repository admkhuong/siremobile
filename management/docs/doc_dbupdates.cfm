<p><strong>JLP Update 2016-01-10</strong></p>
<p>CREATE TABLE `sms`.`carrierlist` (<br />
`PKId_int` INT NOT NULL AUTO_INCREMENT,<br />
`OperatorId_int` INT NULL,<br />
`StandardRate_ti` TINYINT NULL,<br />
`MMS_ti` TINYINT NULL,<br />
`FTEU_ti` TINYINT NULL,<br />
`Download_ti` TINYINT NULL,<br />
`Country_vch` VARCHAR(255) NULL,<br />
`Operator_vch` VARCHAR(255) NULL,<br />
`OperatorAlias_vch` VARCHAR(255) NULL,<br />
PRIMARY KEY (`PKId_int`),<br />
INDEX `IDX_OperatorId` (`OperatorId_int` ASC),<br />
INDEX `IDX_SupportComombo` (`StandardRate_ti` ASC, `FTEU_ti` ASC, `MMS_ti` ASC, `Download_ti` ASC));</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2015-12-05</strong></p>
<p>CREATE TABLE  `simplequeue`.`eventapp` (<br />
`PKId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`BatchId_bi` bigint(20) NOT NULL,<br />
`Status_int` int(11) NOT NULL,<br />
`Event_date` date NOT NULL,<br />
`Created_dt` datetime NOT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
PRIMARY KEY (`PKId_bi`),<br />
UNIQUE KEY `IDX_TodaysStatus` (`BatchId_bi`,`Status_int`,`Event_date`,`ContactString_vch`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2015-08-24</strong></p>
<p>ALTER TABLE `simplequeue`.`sessionire` <br />
ADD COLUMN `DeliveryReceipt_ti` TINYINT NULL DEFAULT 0 AFTER `Status_ti`;</p>
<p><strong>JLP Update 2015-06-24</strong></p>
<p>ALTER TABLE `simplequeue`.`sessionire` <br />
    ADD INDEX `idx_batchid_created_dt_sessionState` (`BatchId_bi` ASC, `Created_dt` ASC, `SessionState_int` ASC);</p>
<p>ALTER TABLE `simpleobjects`.`systemalertscontacts` <br />
ADD COLUMN `ErrorNotify_int` TINYINT NULL DEFAULT '0' AFTER `Desc_vch`;</p>
<p><strong>JLP Update 2015-05-18</strong></p>
<p>CREATE TABLE simplexprogramdata.`sampleapilog` (<br />
`PKId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`UserId_int` int(11) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`CGI_vch` text,<br />
PRIMARY KEY (`PKId_int`),<br />
KEY `IDX_Combo` (`Created_dt`,`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2015-05-18</strong></p>
<p>CREATE TABLE contactresults.`ireresults` (<br />
`IREResultsId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`BatchId_bi` bigint(20) DEFAULT NULL,<br />
`UserId_int` int(11) DEFAULT NULL,<br />
`CPId_int` int(11) DEFAULT NULL,<br />
`QID_int` int(11) DEFAULT NULL,<br />
`IREType_int` int(11) DEFAULT NULL,<br />
`CarrierId_int` int(11) DEFAULT NULL,<br />
`Increments_ti` tinyint(4) DEFAULT NULL,<br />
`IRESessionId_bi` bigint(20) DEFAULT NULL,<br />
`MasterRXCallDetailId_bi` bigint(20) DEFAULT NULL,<br />
`moInboundQueueId_bi` bigint(20) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
`Response_vch` varchar(1000) DEFAULT NULL,<br />
`ShortCode_vch` varchar(45) DEFAULT NULL,<br />
PRIMARY KEY (`IREResultsId_bi`),<br />
KEY `IDX_Combo` (`BatchId_bi`,`Created_dt`,`Response_vch`(160),`ContactString_vch`),<br />
KEY `rply_index1` (`BatchId_bi`,`ContactString_vch`,`Created_dt`),<br />
KEY `IDX_Contact_Created` (`Created_dt`,`ContactString_vch`),<br />
KEY `IDX_Created_User_IRETYPE` (`Created_dt`,`UserId_int`,`IREType_int`),<br />
KEY `IDX_Created_BatchId_IRETYPE` (`Created_dt`,`BatchId_bi`,`IREType_int`),<br />
KEY `IDX_Contact2` (`ContactString_vch`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=400000309 DEFAULT CHARSET=latin1;</p>
<p><strong>JLP Update 2015-05-04</strong></p>
<p>CREATE TABLE simplequeue.`sessionire` (<br />
`SessionId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`SessionState_int` int(11) DEFAULT '0',<br />
`CSC_vch` varchar(45) DEFAULT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
`APIRequestJSON_vch` text,<br />
`LastCP_int` int(11) DEFAULT NULL,<br />
`XMLResultString_vch` text,<br />
`BatchId_bi` bigint(20) DEFAULT NULL,<br />
`UserId_int` int(11) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
`Status_ti` tinyint(4) DEFAULT '1',<br />
PRIMARY KEY (`SessionId_bi`),<br />
KEY `IDX_Combo` (`SessionState_int`,`CSC_vch`,`ContactString_vch`,`SessionId_bi`,`BatchId_bi`,`UserId_int`),<br />
KEY `idx_session_int` (`SessionId_bi`,`SessionState_int`),<br />
KEY `idx_date_vch` (`ContactString_vch`,`Created_dt`,`SessionState_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=373 DEFAULT CHARSET=latin1;</p>
<p>ALTER TABLE `simplequeue`.`smsmtfailurequeue` <br />
ADD INDEX `IDX_ComboII` (`Created_dt` ASC, `ShortCode_vch` ASC, `BatchId_bi` ASC);</p>
<p><strong>JLP Update 2015-04-27</strong></p>
<p>CREATE TABLE `simplelists`.`cpp_log` (<br />
`PKId_bi` BIGINT NOT NULL AUTO_INCREMENT,<br />
`ContactString_vch` VARCHAR(255) NULL,<br />
`CPPID_vch` VARCHAR(512) NULL,<br />
`CPP_UUID_vch` VARCHAR(36) NULL,<br />
`Source_vch` VARCHAR(255) NULL,<br />
`Created_dt` VARCHAR(45) NULL,<br />
`Event_vch` VARCHAR(255) NULL,<br />
PRIMARY KEY (`PKId_bi`));</p>
<p><strong>JLP Update 2015-04-24</strong></p>
<p>ALTER TABLE `simplelists`.`customerpreferenceportal` <br />
CHANGE COLUMN `customhtml_txt` `customhtml_txt` LONGTEXT NULL DEFAULT NULL ;</p>
<p><strong>JLP Update 2015-04-03</strong></p>
<p>ALTER TABLE `simpleobjects`.`cron_tracking` <br />
    ADD COLUMN `RunningLocationIP_vch` VARCHAR(255) NULL AFTER `JSONData_vch`,<br />
ADD COLUMN `RunningLocationDesc_vch` TEXT NULL AFTER `RunningLocationIP_vch`;</p>
<p><strong>JLP Update 2015-03-31</strong></p>
<p>SELECT * FROM simpleobjects.cron_tracking;CREATE TABLE `cron_tracking` (<br />
`PKId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`CronName_vch` varchar(255) DEFAULT NULL,<br />
`LastStart_dt` datetime DEFAULT NULL,<br />
`LastFinish_dt` datetime DEFAULT NULL,<br />
`IsRunning_ti` tinyint(4) DEFAULT '0',<br />
`QuickData1_vch` varchar(255) DEFAULT NULL,<br />
`QuickData2_vch` varchar(255) DEFAULT NULL,<br />
`QuickData3_vch` varchar(255) DEFAULT NULL,<br />
`QuickData4_vch` varchar(255) DEFAULT NULL,<br />
`QucikData5_vch` varchar(255) DEFAULT NULL,<br />
`JSONData_vch` longtext,<br />
`Desc_vch` text,<br />
PRIMARY KEY (`PKId_int`),<br />
UNIQUE KEY `IDX_CronName` (`CronName_vch`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE `metadata_errors` (<br />
`ErrorId_int` int(11) NOT NULL,<br />
`ErrorDesc_vch` text,<br />
`TroubleShootingTips_vch` text,<br />
PRIMARY KEY (`ErrorId_int`),<br />
UNIQUE KEY `ErrorId_int_UNIQUE` (`ErrorId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE `metadata_logentries` (<br />
`LogId_int` int(11) NOT NULL,<br />
`Desc_vch` text,<br />
`TroubleShootingTips_vch` text,<br />
PRIMARY KEY (`LogId_int`),<br />
UNIQUE KEY `LogId_int_UNIQUE` (`LogId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1; </p>
<p><strong>JLP Update 2015-03-09</strong></p>
<p>ALTER TABLE `simpleobjects`.`userreportingpref` <br />
ADD COLUMN `CustomDataJSON_vch` TEXT NULL AFTER `CustomData5_vch`;</p>
<p><br />
    CREATE TABLE simplequeue.`smsduplicatemolog` (<br />
`SMSDuplicateMOLogId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`ContactString_vch` varchar(255) NOT NULL,<br />
`CarrierId_vch` varchar(255) NOT NULL,<br />
`ShortCode_vch` varchar(255) NOT NULL,<br />
`Keyword_vch` varchar(1024) DEFAULT NULL,<br />
`TransactionId_vch` varchar(45) DEFAULT NULL,<br />
`QuestionTimeOutQID_int` int(11) DEFAULT '0',<br />
`Time_dt` datetime DEFAULT NULL,<br />
`Scheduled_dt` datetime NOT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`Status_ti` tinyint(4) DEFAULT NULL,<br />
`CustomServiceId1_vch` varchar(255) DEFAULT NULL,<br />
`CustomServiceId2_vch` varchar(255) DEFAULT NULL,<br />
`RawData_vch` text,<br />
PRIMARY KEY (`SMSDuplicateMOLogId_bi`),<br />
KEY `IDX_COMBO_Created_CS_Keyword` (`Created_dt`,`ContactString_vch`,`Keyword_vch`(160)) <br />
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2015-02-03</strong></p>
<p>DROP Table `simplequeue`.`mtfailurequeue` </p>
<p>CREATE TABLE `simplequeue`.`smsmtfailurequeue` (<br />
`moInboundQueueId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`Status_ti` tinyint(4) UNSIGNED NULL DEFAULT NULL,<br />
`AggregatorId_int` int(11) NOT NULL,<br />
`SMPPErrorCode_int` int(11) DEFAULT NULL, <br />
`ControlPoint_int` INT NULL,<br />
    `BatchId_bi` BIGINT NULL,<br />
    `Failure_dt` datetime DEFAULT NULL,<br />
    `Created_dt` datetime DEFAULT NULL,<br />
    `ScheduledRetry_dt` datetime DEFAULT NULL,<br />
    `ContactString_vch` varchar(255) NOT NULL,<br />
    `ShortCode_vch` varchar(255) NOT NULL,<br />
    `TransactionId_vch` varchar(45) DEFAULT NULL,<br />
    `RawRequestData_vch` text,<br />
    PRIMARY KEY (`moInboundQueueId_bi`),<br />
    KEY `IDX_ANALYTICS` (`ContactString_vch`,`Status_ti`,`ShortCode_vch`),<br />
    KEY `IDX_COMBO_Created_CS` (`Created_dt`,`ContactString_vch`),<br />
    KEY `IDX_Status_ti` (`Status_ti`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p><strong>JLP Update 2015-01-29</strong></p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
ADD COLUMN `ShortCode_vch` VARCHAR(255) NULL AFTER `Time_vch`;</p>
<p><strong>JLP Update 2015-01-28</strong></p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
    ADD COLUMN `Status_ti` TINYINT NULL DEFAULT 0 AFTER `SMSDispositionId_int`;</p>
<p><strong>JLP Update 2015-01-26</strong></p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD COLUMN `DeliveryReceipt_ti` TINYINT NULL DEFAULT 0 AFTER `EMSFlag_int`;</p>
<p>ALTER TABLE `sms`.`shortcode` <br />
ADD COLUMN `DeliveryReceipt_ti` TINYINT NULL DEFAULT 0 AFTER `PreferredAggregator_int`;</p>
<p><strong>JLP Update 2015-01-23</strong></p>
<p>CREATE TABLE simplequeue.`moconcatbuffer` (<br />
`PKId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`ReferenceNumber_int` int(11) DEFAULT NULL,<br />
`TotalParts_int` int(11) DEFAULT NULL,<br />
`PartNumber_int` int(11) DEFAULT NULL,<br />
`Status_int` int(11) DEFAULT '1',<br />
`SurveyResultsId_bi` bigint(20) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
`ShortCode_vch` varchar(255) DEFAULT NULL,<br />
`Content_vch` varchar(512) DEFAULT NULL,<br />
PRIMARY KEY (`PKId_int`),<br />
KEY `IDX_Combo1` (`Created_dt`,`ContactString_vch`,`ReferenceNumber_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2015-01-09</strong><br />
    <br />
    CREATE TABLE simplequeue.`mtfailurequeue` (<br />
`moInboundQueueId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`ContactString_vch` varchar(255) NOT NULL,<br />
`AggregatorId_int` int(11) NOT NULL,<br />
`ShortCode_vch` varchar(255) NOT NULL,<br />
`TransactionId_vch` varchar(45) DEFAULT NULL,<br />
`Failure_dt` datetime DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`ScheduledRetry_dt` datetime DEFAULT NULL,<br />
`Status_ti` tinyint(4) DEFAULT NULL,<br />
`RawRequestData_vch` text,<br />
PRIMARY KEY (`moInboundQueueId_bi`),<br />
KEY `IDX_ANALYTICS` (`ContactString_vch`,`Status_ti`,`ShortCode_vch`),<br />
KEY `IDX_COMBO_Created_CS` (`Created_dt`,`ContactString_vch`),<br />
KEY `IDX_Status_ti` (`Status_ti`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;</p>
<p><strong>Seta Update 2015-01-08</strong></p>
<p>ALTER TABLE `simpleobjects`.`useraccount`<br />
    ADD COLUMN `ChangePasswordLink_vch` VARCHAR(255) NULL,<br />
	ADD COLUMN `validPasswordLink_ti` TINYINT(4) NULL DEFAULT '0';
</p>

<p><strong>JLP Update 2014-12-24</strong></p>
<p>CREATE TABLE `simpleobjects`.`batch_cdf_ui_store` (<br />
`PKID_int` BIGINT NOT NULL,<br />
`BatchId_bi` BIGINT NULL,<br />
`LastPreviewJSON_vch` LONGTEXT NULL,<br />
`Created_dt` DATETIME NULL,<br />
PRIMARY KEY (`PKID_int`));</p>
<p>ALTER TABLE `simpleobjects`.`batch_cdf_ui_store` <br />
ADD INDEX `IDX_Batch` (`BatchId_bi` ASC, `PKID_int` ASC);</p>

<p><strong>JLP Update 2014-12-19</strong></p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
CHANGE COLUMN `RealTimeFlag_int` `RealTimeFlag_int` TINYINT(4) NULL DEFAULT '1' ;</p>
<p><strong>JLP Update 2014-12-11</strong></p>
<p>ALTER TABLE `sms`.`smsshare` <br />
ADD COLUMN `Content_vch` VARCHAR(640) NOT NULL AFTER `BatchId_bi`;</p>
<p>ALTER TABLE `sms`.`smsshare` <br />
    DROP INDEX `IDX_Combo_Keyword_CSC` ,<br />
ADD INDEX `IDX_Combo_Keyword_CSC` (`Keyword_vch` ASC, `ShortCode_vch` ASC, `BatchId_bi` ASC);</p>
<p><strong>JLP Update 2014-12-10</strong></p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
ADD COLUMN `DistributionProcessId_int` TINYINT NULL DEFAULT 0 AFTER `ProcTime_int`;</p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
    DROP INDEX `IDXComboDistribution` ,<br />
ADD INDEX `IDXComboDistribution` (`DTSStatusType_ti` ASC, `DTSId_int` ASC, `Scheduled_dt` ASC, `DistributionProcessId_int` ASC);</p>
<p>ALTER TABLE `simplexfulfillment`.`device` <br />
ADD COLUMN `DistributionProcessIdList_vch` VARCHAR(255) NULL DEFAULT '(0)' AFTER `DeviceType_int`;</p>
<p>&nbsp;</p>
<p>CREATE TABLE `sms`.`smsshare` (<br />
`PKId_int` BIGINT NOT NULL AUTO_INCREMENT,<br />
`ShortCode_vch` VARCHAR(45) NOT NULL,<br />
`Keyword_vch` VARCHAR(45) NOT NULL,<br />
`BatchId_bi` VARCHAR(45) NOT NULL,<br />
PRIMARY KEY (`PKId_int`),<br />
INDEX `IDX_Combo_Keyword_CSC` (`Keyword_vch` ASC, `ShortCode_vch` ASC),<br />
INDEX `IDX_Batch` (`BatchId_bi` ASC));</p>
<p>&nbsp;</p>
<p><strong>Seta Update 2014-11-03</strong></p>
<p>ALTER TABLE `simpleobjects`.`cannedresponse`
    CHANGE COLUMN `Approved_bt` `Approved_bt` BIT(1) NULL DEFAULT NULL COMMENT '			' ,
    ADD COLUMN `company_int` INT(11) NOT NULL AFTER `Approved_bt`;
</p>
<!------>
<p><strong>JLP Update 2014-10-22</strong></p>
<p>ALTER TABLE `simplebilling`.`authorizepayment` <br />
    CHANGE COLUMN `PayPalExecuteLink` `PayPalExecuteLink_vch` VARCHAR(1024) NULL DEFAULT NULL ;</p>
<p><strong>JLP Update 2014-10-09</strong></p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1_monthly_ref` <br />
    ADD COLUMN `LastSent_dt` DATETIME NULL DEFAULT NULL AFTER `AsOfDate_dt`,<br />
ADD INDEX `IDX_MemberSearch` (`MemberId_vch` ASC, `DateOfBirth_dt` ASC, `LastSent_dt` ASC);</p>
<p><strong>JLP Update 2014-10-07</strong></p>
<p>CREATE TABLE `simplexprogramdata`.`portal_link_c8_p1_monthly_ref` (<br />
`PKId_int` INT NOT NULL AUTO_INCREMENT,<br />
`MemberId_vch` VARCHAR(255) NULL,<br />
`DateOfBirth_dt` DATETIME NULL,<br />
`INNLimit_vch` VARCHAR(45) NULL,<br />
`OONLimit_vch` VARCHAR(45) NULL,<br />
`INNBal_vch` VARCHAR(45) NULL,<br />
`OONBal_vch` VARCHAR(45) NULL,<br />
`AsOfDate_dt` DATETIME NULL,<br />
PRIMARY KEY (`PKId_int`),<br />
INDEX `IDX_Member` (`MemberId_vch` ASC));</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-10-06</strong></p>
<p>ALTER TABLE `simplequeue`.`apptqueue` <br />
    ADD COLUMN `Confirmation_int` TINYINT NULL DEFAULT 0 AFTER `Desc_vch`,<br />
ADD INDEX `IDX_Confirm` (`Batchid_bi` ASC, `Confirmation_int` ASC, `active_int` ASC);</p>
<p>&nbsp;</p>
<p>ALTER TABLE `simplequeue`.`apptqueue` <br />
    ADD INDEX `IDX_ConfirmResponse` (`Batchid_bi` ASC, `ContactTypeId_int` ASC, `active_int` ASC, `ContactString_vch` ASC, `Start_dt` ASC);</p>
<p><strong>JLP Update 2014-09-25</strong></p>
<p>ALTER TABLE `simplexfulfillment`.`device` <br />
ADD COLUMN `DeviceType_int` TINYINT NULL DEFAULT 0 AFTER `Updated_dt`;</p>
<p>CREATE TABLE `simpleobjects`.`simplekeys_log` (<br />
    `pkid_int` INT NOT NULL AUTO_INCREMENT,<br />
    `SimpleKey_vch` VARCHAR(45) NULL,<br />
    `Created_dt` DATETIME NULL,<br />
    `CGIData_vch` TEXT NULL,<br />
PRIMARY KEY (`pkid_int`));</p>
<p><strong>JLP Update 2014-09-17</strong></p>
<p>CREATE TABLE `simpleobjects`.`threadcontrol_smsqueue` (<br />
    `ThreadId_int` INT NOT NULL,<br />
    `IsRunning_bit` TINYINT NULL DEFAULT 0,<br />
    `LastRan_dt` DATETIME NULL,<br />
    PRIMARY KEY (`ThreadId_int`),<br />
    INDEX `IDX_DT` (`LastRan_dt` ASC),<br />
    INDEX `IDX_IsRunning` (`IsRunning_bit` ASC),<br />
INDEX `IDX_Combo_IsRunning_LastRan` (`IsRunning_bit` ASC, `LastRan_dt` ASC));</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-09-17</strong></p>
<p>CREATE TABLE simpleobjects.`simplekeys` (<br />
`PKId_int` int(11) NOT NULL AUTO_INCREMENT,<br />
`SimpleKey_vch` varchar(45) DEFAULT NULL,<br />
`FirstName_vch` varchar(1024) DEFAULT NULL,<br />
`LastName_vch` varchar(1024) DEFAULT NULL,<br />
`SignedNDA_int` tinyint(4) DEFAULT '0',<br />
`CompanyName_vch` varchar(1024) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`LastLogIn_dt` datetime DEFAULT NULL,<br />
`LogInCount_int` int(11) DEFAULT '0',<br />
PRIMARY KEY (`PKId_int`),<br />
UNIQUE KEY `UC_SimpleKey` (`SimpleKey_vch`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-08-25</strong></p>
<p>CREATE TABLE simpleobjects.`email_templates` (<br />
 `pkId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`UserId_int` int(11) NOT NULL,<br />
`Desc_vch` text,<br />
`Template_vch` text,<br />
`Active_int` tinyint(4) DEFAULT '1',<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`pkId_int`),<br />
KEY `IDX_Combo_UserID_Desc` (`UserId_int`,`Desc_vch`(500)),<br />
KEY `IDX_UserId` (`UserId_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;</p>
<p><strong>JLP Update 2014-08-19</strong></p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1` <br />
ADD COLUMN `DateOfBirth_dt` DATETIME NULL AFTER `MemberId_vch`;</p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1` <br />
    DROP INDEX `IDX_MemberId` ,<br />
ADD INDEX `IDX_MemberId` (`MemberId_vch` ASC, `DateOfBirth_dt` ASC);</p>
<p><strong>JLP Update 2014-08-12</strong></p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1` <br />
ADD COLUMN `LastAgentUserId_int` INT NULL AFTER `ProgramNotes_vch`;</p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1` <br />
CHANGE COLUMN `PKId_int` `PKId_int` BIGINT(20) NOT NULL AUTO_INCREMENT ;</p>
<p><strong>JLP Update 2014-08-11</strong></p>
<p>ALTER TABLE `simpleobjects`.`companyagenttools` <br />
ADD COLUMN `AllowAllUsers_int` INT NULL DEFAULT 1 AFTER `Desc_vch`;</p>
<p>ALTER TABLE `simpleobjects`.`companyagenttoolsuserlinks` <br />
RENAME TO  `simpleobjects`.`companyagenttoolsuserblocklinks` ;</p>
<p>CREATE TABLE `simplexprogramdata`.`portal_link_c8_p1` (<br />
`PKId_int` BIGINT NOT NULL,<br />
`MemberId_vch` VARCHAR(255) NULL,<br />
`Status_int` INT NULL,<br />
`FirstName_vch` VARCHAR(1024) NULL,<br />
`LastName_vch` VARCHAR(1024) NULL,<br />
`LastSent_dt` DATETIME NULL,<br />
`ContactString_vch` VARCHAR(1024) NULL,<br />
`ContactTypeId_int` INT NULL,<br />
`LastSentDeductableIn_vch` VARCHAR(45) NULL,<br />
`LastSentDeductableInMet_vch` VARCHAR(45) NULL,<br />
`LastSentDeductableOut_vch` VARCHAR(45) NULL,<br />
`LastSentDeductableOutMet_vch` VARCHAR(45) NULL,<br />
`LastSentAsOfDate_dt` DATETIME NULL,<br />
`ProgramNotes_vch` TEXT NULL,<br />
PRIMARY KEY (`pkid_int`));</p>
<p>ALTER TABLE `simplexprogramdata`.`portal_link_c8_p1` <br />
ADD INDEX `IDX_MemberId` (`MemberId_vch` ASC);</p>
<p><strong>JLP Update 2014-08-08</strong></p>
<p>CREATE TABLE `simpleobjects`.`companyagenttoolsuserlinks` (<br />
`PKId_int` INT NOT NULL AUTO_INCREMENT,<br />
`AgentToolPKId_int` INT NULL,<br />
`UserId_int` INT NULL,<br />
`Created_dt` DATETIME NULL,<br />
PRIMARY KEY (`PKId_int`));</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-08-07</strong></p>
<p>ALTER TABLE `simpleobjects`.`companyagenttools` <br />
    ADD COLUMN `Active_int` INT NULL DEFAULT 1 AFTER `LinkDesc_vch`;</p>
<p>ALTER TABLE `simpleobjects`.`companyagenttools` <br />
    CHANGE COLUMN `LinkDesc_vch` `Desc_vch` TEXT NULL DEFAULT NULL ,<br />
    ADD COLUMN `Created_dt` DATETIME NULL AFTER `Active_int`,<br />
ADD COLUMN `Modified_dt` DATETIME NULL AFTER `Created_dt`;</p>
<p><strong>JLP Update 2014-07-30</strong></p>
<p>CREATE TABLE `simpleobjects`.`companyagenttools` (<br />
    `pkid_int` INT NOT NULL AUTO_INCREMENT,<br />
    `CompanyId_int` INT NULL,<br />
    `LinkTitle_vch` VARCHAR(2048) NULL,<br />
    `Link_vch` VARCHAR(2048) NULL,<br />
    `LinkDesc_vch` TEXT NULL,<br />
    PRIMARY KEY (`pkid_int`),<br />
    INDEX `IDX_Company` (`CompanyId_int` ASC),<br />
    INDEX `IDX_Combo` (`CompanyId_int` ASC, `LinkTitle_vch` ASC));</p>
<p>&nbsp;</p>
<p>ALTER TABLE `simpleobjects`.`companyaccount` <br />
    ADD COLUMN `LinkDirUUID_vch` VARCHAR(255) NULL AFTER `SharedAccountUserId_int`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-07-15</strong></p>
<p>2 New Data Sources for reproting in the paths.cfm used in reporting section. Work is still in progress.</p>
<p>    EBMReportsRealTime- Points to real time slave<br />
EBMReportsArchive - points to a large archive server</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-07-06</strong></p>
<p>ALTER TABLE `simplelists`.`customerpreferenceportal` <br />
ADD COLUMN `VerificationBatchId_bi` BIGINT NULL DEFAULT 0 AFTER `customhtml_txt`;</p>
<p>&nbsp;</p>
<p><strong>Seta Update 2014-06-30</strong></p>
ALTER TABLE `simplelists`.`customdefinedfields` <br />
ADD COLUMN `type_field` TINYINT(1) NOT NULL DEFAULT 0 AFTER `Created_dt`;<br />
<p>&nbsp;</p>
CREATE TABLE `simplelists`.`default_value_custom_defined_fields` (<br />
`id` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`CdfId_int` int(11) NOT NULL,<br />
`default_value` varchar(255) NOT NULL,<br />
PRIMARY KEY (`id`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;<br />
<p>&nbsp;</p>
<p><strong>JLP Update 2014-06-25</strong></p>
<p>ALTER TABLE `simpleobjects`.`useraccount` <br />
    ADD COLUMN `MFAContactString_vch` VARCHAR(255) NULL DEFAULT NULL AFTER `HauInvitationCode_vch`,<br />
ADD COLUMN `MFAContactType_ti` TINYINT NULL DEFAULT 1 AFTER `MFAContacStringt_vch`,<br />
ADD COLUMN `MFAEXT_vch`  VARCHAR(255) NULL DEFAULT NULL AFTER `MFAContactType_ti`<br />
    ADD COLUMN `MFAEnabled_ti` TINYINT NULL DEFAULT 1 AFTER `MFAEXT_vch`;<br />
</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-06-23</strong></p>
<p>ALTER TABLE simplexresults.apitracking <br />
ADD INDEX `IDX_User` (`UserId_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>SETA Update 2014-06-20</strong></p>
<p>CREATE TABLE simplelists.`cpp_cdf_html` (<br />
`cppCdfHtmlId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`title` text,<br />
`description` text,<br />
`CPP_UUID_vch` varchar(36) NOT NULL,<br />
PRIMARY KEY (`cppCdfHtmlId_bi`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;</p>

<p> CREATE TABLE simplelists.`cpp_cdf_groups` (<br />
    `id` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `CdfId_int` int(11) NOT NULL,<br />
    `cppCdfHtmlId_bi` bigint(20) NOT NULL,<br />
    PRIMARY KEY (`id`)<br />
    )ENGINE=InnoDB AUTO_INCREMENT=1421 DEFAULT CHARSET=latin1;</p>
<p><strong>JLP Update 2014-06-09</strong></p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` <br />
ADD INDEX `IDX_ProcessQ2` (`Scheduled_dt` ASC, `Status_ti` ASC, `moInboundQueueId_bi` ASC);</p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` <br />
DROP INDEX `IDX_ProcessQ` ;</p>
<p><strong>JLP Update 2014-06-01</strong></p>
<p>ALTER TABLE `simplebilling`.`billing` <br />
CHANGE COLUMN `Balance_int` `Balance_int` FLOAT(14,3) NOT NULL DEFAULT '0.000' ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-29</strong></p>
<p>ALTER TABLE `simplexfulfillment`.`device` <br />
CHANGE COLUMN `Enabled_dt` `Enabled_int` TINYINT(4) NULL DEFAULT '0' ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-26</strong></p>
<p><br />
    CREATE DATABASE simplexinbound<br />
</p>
<p>CREATE TABLE simplexinbound.`genericinboundsources` (<br />
    `InboundId_int` int(10) unsigned NOT NULL auto_increment,<br />
    `PhoneNumber_vch` varchar(50) default NULL,<br />
    `Desc_vch` varchar(255) default NULL,<br />
    `Start_dt` datetime default NULL,<br />
    `End_dt` datetime default NULL,<br />
    `Modified_dt` datetime default NULL,<br />
    `Active_int` int(11) default NULL,<br />
    `UserId_int` int(11) default '0',<br />
    `BatchId_bi` bigint(20) default '0',<br />
    KEY `XPKGenericInboundSource` (`InboundId_int`)<br />
) ENGINE=innoDB DEFAULT CHARSET=latin1</p>
<p><strong>JLP Update 2014-05-20</strong></p>
<p>ALTER TABLE `simplexresults`.`surveyresults` <br />
ADD COLUMN `ShortCode_vch` VARCHAR(45) NULL AFTER `Response_vch`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-18</strong></p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
ADD INDEX `IDXComboDistribution` (`DTSStatusType_ti` ASC, `DTSId_int` ASC, `Scheduled_dt` ASC, `TypeMask_ti` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-12</strong></p>
<p>ALTER TABLE `simpleobjects`.`systemalertscontacts` <br />
ADD COLUMN `UploadDebug_int` TINYINT NULL DEFAULT 0 AFTER `NewUserNotice_int`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-11</strong> - New Appointment system</p>
<p>CREATE TABLE simplequeue.`apptqueue` (<br />
`pkid_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`UserId_int` int(11) DEFAULT NULL,<br />
`Batchid_bi` bigint(20) DEFAULT NULL,<br />
`ContactId_bi` varchar(45) DEFAULT NULL,<br />
`ContactTypeId_int` tinyint(4) DEFAULT NULL,<br />
`active_int` tinyint(4) DEFAULT '1',<br />
`AllDayFlag_int` tinyint(4) DEFAULT '0',<br />
`Created_dt` varchar(45) DEFAULT NULL,<br />
`Updated_dt` varchar(45) DEFAULT NULL,<br />
`Start_dt` datetime DEFAULT NULL,<br />
`End_dt` datetime DEFAULT NULL,<br />
`Duration_int` varchar(45) DEFAULT NULL,<br />
`ContactString_vch` varchar(255) DEFAULT NULL,<br />
`SystemDesc_vch` varchar(255) DEFAULT NULL,<br />
`Desc_vch` text,<br />
PRIMARY KEY (`pkid_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-08</strong></p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` <br />
ADD INDEX `IDX_Combo_Creates_Contact` (`ContactString_vch` ASC, `Created_dt` ASC);</p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` <br />
ADD INDEX `IDX_ContactString` (`ContactString_vch` ASC);</p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
ADD INDEX `IDX_Combo_Batch_Scheduled_Status` (`BatchId_bi` ASC, `Scheduled_dt` ASC, `DTSStatusType_ti` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-07</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
    ADD INDEX `IDX_CDLDT_BATCH_CALLRESULT` (`RXCDLStartTime_dt` ASC, `BatchId_bi` ASC, `CallResult_int` ASC);</p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
ADD INDEX `IDX_COMBO_CS_CDLDT_BATCH_CALLRESULT` (`RXCDLStartTime_dt` ASC, `BatchId_bi` ASC, `CallResult_int` ASC, `ContactString_vch` ASC);</p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
ADD INDEX `IDX_Combo_Batch_User` (`BatchId_bi` ASC, `UserId_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-05-06</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
    ADD INDEX `IDX_BATCH_CDLDT` (`RXCDLStartTime_dt` ASC, `BatchId_bi` ASC);</p>
<p><strong><br />
JLP Update 2014-04-29</strong></p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
ADD COLUMN `DEFAULTCID_VCH` VARCHAR(15) NULL AFTER `ContactFilter_vch`;</p>
<p>ALTER TABLE `simplexfulfillment`.`device` <br />
ADD COLUMN `LastLookCount_int` INT(11) NULL DEFAULT 0 AFTER `ErrorCount_int`,<br />
ADD COLUMN `Updated_dt` DATETIME NULL AFTER `LastLookCount_int`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-04-21</strong></p>
<p>DELIMITER $$<br />
CREATE DEFINER=`EBMSQLUser`@`%` PROCEDURE `usp_sg_ins_emailv3`(IN `nDtsid` BIGINT, IN `sUUID` VARCHAR(200), IN `sAttempt` INT, IN `sCategory` INT, IN `sEmail` VARCHAR(200), IN `sEvent` VARCHAR(200), IN `sResponse` VARCHAR(500), IN `sSmtpid` VARCHAR(200), IN `dSgTimestamp` DATETIME, IN `sType` VARCHAR(200), IN `sReason` VARCHAR(500), IN `sStatus` VARCHAR(100)</p>
<p>, IN `sIncomingIP_vch` VARCHAR(50))<br />
    BEGIN</p>
<p> INSERT INTO simplexresults.contactresults_email<br />
    (<br />
    DTSID_int, <br />
    DTS_UUID_vch,<br />
    Attempt_int,<br />
    BatchId_bi,<br />
    Email_vch, <br />
    Event_vch, <br />
    Response_vch,<br />
    Smtpid_vch,<br />
    sgTimestamp_dt,<br />
    Type_vch,<br />
    Reason_vch,<br />
    Status_vch,<br />
    IncomingIP_vch,<br />
    mbTimestamp_dt<br />
    )<br />
    VALUES<br />
    (<br />
    nDtsid,<br />
    sUUID,<br />
    sAttempt,<br />
    sCategory,<br />
    sEmail,<br />
    sEvent,<br />
    sResponse,<br />
    sSmtpid,<br />
    dSgTimestamp,<br />
    sType,<br />
    sReason,<br />
    sStatus,<br />
    sIncomingIP_vch,<br />
    CURRENT_TIMESTAMP()<br />
    );</p>
<p>END$$<br />
    DELIMITER ;</p>
<p>&nbsp;</p>
<p>CREATE TABLE simplexresults.`contactresults_email` (<br />
    `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `DTSID_int` int(10) unsigned DEFAULT NULL,<br />
    `DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
    `Attempt_int` int(11) DEFAULT NULL,<br />
    `BatchId_bi` bigint(20) DEFAULT NULL,<br />
    `Email_vch` varchar(50) DEFAULT NULL,<br />
    `Event_vch` varchar(50) DEFAULT NULL,<br />
    `Response_vch` varchar(800) DEFAULT NULL,<br />
    `Smtpid_vch` varchar(200) DEFAULT NULL,<br />
    `sgTimestamp_dt` datetime DEFAULT NULL,<br />
    `Type_vch` varchar(200) DEFAULT NULL,<br />
    `Reason_vch` varchar(500) DEFAULT NULL,<br />
    `Status_vch` varchar(100) DEFAULT NULL,<br />
    `mbTimestamp_dt` datetime DEFAULT NULL,<br />
    `IncomingIP_vch` varchar(50) DEFAULT NULL,<br />
    `read_bit` bit(1) DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_bi`),<br />
    KEY `IDX_mbTimestamp` (`mbTimestamp_dt`),<br />
    KEY `IDX_BatchId_bi` (`BatchId_bi`),<br />
    KEY `IDX_Event_vch` (`Event_vch`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_clients` (<br />
    `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `accountID_vch` varchar(10) DEFAULT NULL,<br />
    `aol_int` int(11) DEFAULT NULL,<br />
    `android_Phone_int` int(11) DEFAULT NULL,<br />
    `androidTablet_int` int(11) DEFAULT NULL,<br />
    `apple_mail_int` int(11) DEFAULT NULL,<br />
    `blackberry_int` int(11) DEFAULT NULL,<br />
    `Eudora_int` int(11) DEFAULT NULL,<br />
    `gMail_int` int(11) DEFAULT NULL,<br />
    `Hotmail_int` int(11) DEFAULT NULL,<br />
    `lotus_notes_int` int(11) DEFAULT NULL,<br />
    `other_int` int(11) DEFAULT NULL,<br />
    `other_webmail_int` int(11) DEFAULT NULL,<br />
    `Outlook_int` int(11) DEFAULT NULL,<br />
    `Postbox_int` int(11) DEFAULT NULL,<br />
    `sparrow_int` int(11) DEFAULT NULL,<br />
    `thunderbird_int` int(11) DEFAULT NULL,<br />
    `windowsLiveMail_int` int(11) DEFAULT NULL,<br />
    `yahoo_int` int(11) DEFAULT NULL,<br />
    `iPad_int` int(11) DEFAULT NULL,<br />
    `iphone_int` int(11) DEFAULT NULL,<br />
    `iPod_int` int(11) DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_bi`),<br />
    KEY `IDX_date_dt` (`date_dt`),<br />
    KEY `IDX_gmail_int` (`gMail_int`),<br />
    KEY `IDX_iphone_int` (`iphone_int`),<br />
    KEY `IDx_applemail_int` (`apple_mail_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_devices` (<br />
    `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `accountID_vch` varchar(10) DEFAULT NULL,<br />
    `open_webMail_int` int(11) DEFAULT NULL,<br />
    `open_phone_int` int(11) DEFAULT NULL,<br />
    `open_tablet_int` int(11) DEFAULT NULL,<br />
    `open_desktop_int` int(11) DEFAULT NULL,<br />
    `open_other_int` int(11) DEFAULT NULL,<br />
    `unique_webMail_int` int(11) DEFAULT NULL,<br />
    `unique_phone_int` int(11) DEFAULT NULL,<br />
    `unique_tablet_int` int(11) DEFAULT NULL,<br />
    `unique_other_int` int(11) DEFAULT NULL,<br />
    `unique_desktop_int` int(11) DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_bi`),<br />
    KEY `IDX_date_dt` (`date_dt`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_global` (<br />
    `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `accountId_vch` varchar(10) DEFAULT NULL,<br />
    `delivered_int` int(11) DEFAULT NULL,<br />
    `unique_open_int` int(11) DEFAULT NULL,<br />
    `spamreport_int` int(11) DEFAULT NULL,<br />
    `drop_int` int(11) DEFAULT NULL,<br />
    `request_int` int(11) DEFAULT NULL,<br />
    `bounce_int` int(11) DEFAULT NULL,<br />
    `deferred_int` int(11) DEFAULT NULL,<br />
    `processed_int` int(11) DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    `open_int` int(11) DEFAULT NULL,<br />
    `blocked_int` int(11) DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_bi`),<br />
    KEY `IDX_date_dt` (`date_dt`),<br />
    KEY `IDX_open_int` (`open_int`),<br />
    KEY `IDX_delivered_int` (`delivered_int`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_isps` (<br />
    `TableID_int` int(11) NOT NULL AUTO_INCREMENT,<br />
    `accountID_vch` varchar(10) DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    `blocked_int` int(11) DEFAULT NULL,<br />
    `bounce_int` int(11) DEFAULT NULL,<br />
    `deferred_int` int(11) DEFAULT NULL,<br />
    `delivered_int` int(11) DEFAULT NULL,<br />
    `drop_int` int(11) DEFAULT NULL,<br />
    `open_int` int(11) DEFAULT NULL,<br />
    `processed_int` int(11) DEFAULT NULL,<br />
    `request_int` int(11) DEFAULT NULL,<br />
    `spamreport_int` int(11) DEFAULT NULL,<br />
    `uniqueopen_int` int(11) DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_int`),<br />
    KEY `IDX_DATE_DT` (`date_dt`),<br />
    KEY `IDX_open_int` (`open_int`),<br />
    KEY `IDX_processed_int` (`processed_int`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_isps_uniqueopen` (<br />
    `TableID_int` int(11) NOT NULL AUTO_INCREMENT,<br />
    `accountID_vch` varchar(10) DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    `aol_int` int(11) DEFAULT NULL,<br />
    `android_phone_int` int(11) DEFAULT NULL,<br />
    `applemail_int` int(11) DEFAULT NULL,<br />
    `blackberry_int` int(11) DEFAULT NULL,<br />
    `eudora_int` int(11) DEFAULT NULL,<br />
    `gmail_int` int(11) DEFAULT NULL,<br />
    `hotmail_int` int(11) DEFAULT NULL,<br />
    `lotusnotes_int` int(11) DEFAULT NULL,<br />
    `other_int` int(11) DEFAULT NULL,<br />
    `other_webmail_int` int(11) DEFAULT NULL,<br />
    `outlook_int` int(11) DEFAULT NULL,<br />
    `postbox_int` int(11) DEFAULT NULL,<br />
    `sparrow_int` int(11) DEFAULT NULL,<br />
    `thunderbird_int` int(11) DEFAULT NULL,<br />
    `windows_livemail_int` int(11) DEFAULT NULL,<br />
    `yahoo_int` int(11) DEFAULT NULL,<br />
    `iPad_int` int(11) DEFAULT NULL,<br />
    `iPhone_int` int(11) DEFAULT NULL,<br />
    `iPod_int` int(11) DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_int`)<br />
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p>CREATE TABLE simplexresults.`contactresults_email_account_summary_uniqueclients` (<br />
    `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `accountID_vch` varchar(45) DEFAULT NULL,<br />
    `aol_int` int(11) DEFAULT NULL,<br />
    `android_phone_int` int(11) DEFAULT NULL,<br />
    `android_tablet_int` int(11) DEFAULT NULL,<br />
    `apple_mail_int` int(11) DEFAULT NULL,<br />
    `blackberry_int` int(11) DEFAULT NULL,<br />
    `gmail_int` int(11) DEFAULT NULL,<br />
    `hotmail_int` int(11) DEFAULT NULL,<br />
    `lotus_notes_int` int(11) DEFAULT NULL,<br />
    `other_int` int(11) DEFAULT NULL,<br />
    `other_webmail_int` int(11) DEFAULT NULL,<br />
    `outlook_int` int(11) DEFAULT NULL,<br />
    `postbox_int` int(11) DEFAULT NULL,<br />
    `sparrow_int` int(11) DEFAULT NULL,<br />
    `thunderbird_int` int(11) DEFAULT NULL,<br />
    `windows_livemail_int` int(11) DEFAULT NULL,<br />
    `yahoo_int` int(11) DEFAULT NULL,<br />
    `ipad_int` int(11) DEFAULT NULL,<br />
    `iphone_int` int(11) DEFAULT NULL,<br />
    `ipod_int` int(11) DEFAULT NULL,<br />
    `date_dt` datetime DEFAULT NULL,<br />
    `startdate_dt` datetime DEFAULT NULL,<br />
    `enddate_dt` datetime DEFAULT NULL,<br />
    PRIMARY KEY (`TableID_bi`),<br />
    KEY `IDX_DATE_DT` (`date_dt`),<br />
    KEY `IDX_applemail_int` (`apple_mail_int`),<br />
    KEY `IDX_gmail_int` (`gmail_int`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;</p>
<p><strong>JLP Update 2014-04-18</strong></p>
<p>ALTER TABLE `simplexresults`.`surveyresults` <br />
ADD INDEX `IDX_Contact_Created` (`Created_dt` ASC, `ContactString_vch` ASC);</p>
<p>ALTER TABLE `simplexresults`.`surveyresults` <br />
ADD INDEX `IDX_Contact2` (`ContactString_vch` ASC);</p>
<p>ALTER TABLE `simplebilling`.`authorizepayment` <br />
 ADD COLUMN `PayPalTransactionId_vch` VARCHAR(255) NULL AFTER `customerPaymentProfileID_int`,<br />
    ADD COLUMN `PayPalApprovalLink_vch` VARCHAR(1024) NULL AFTER `PayPalTransactionId_vch`,<br />
    ADD COLUMN `PayPalExecuteLink_vch` VARCHAR(1024) NULL AFTER `PayPalApprovalLink_vch`,<br />
ADD COLUMN `PayPalSelfLink_vch` VARCHAR(1024) NULL AFTER `PayPalExecuteLink`,<br />
ADD COLUMN `PayPalComplete_int` TINYINT NULL DEFAULT 0 AFTER `PayPalSelfLink_vch,<br />
ADD COLUMN `CreditsAddAmount_int` INT(10) NULL DEFAULT '0' AFTER `PayPalComplete_int`,<br />
ADD COLUMN `PayPalExecuteDetails_vch` TEXT NULL AFTER `CreditsAddAmount_int`;</p>
<p>ALTER TABLE `simplebilling`.`authorizepayment` <br />
ADD INDEX `IDX_AuthLink` (`PayPalApprovalLink_vch` ASC);</p>
<p>&nbsp;</p>
<p><strong>Seta Update 2014-04-18</strong></p>
<pre>
Create database setatest;
use setatest;

CREATE TABLE `setatest`.`useraccount` (
  `userid_int` INT NOT NULL AUTO_INCREMENT,
  `username_vch` VARCHAR(50) NOT NULL,
  `password_vch` BLOB NOT NULL,
  `created_dt` DATETIME NULL,
  `role_int` INT NOT NULL,
  `FirstName_vch` VARCHAR(100) NULL,
  `LastName_vch` VARCHAR(100) NULL,
  PRIMARY KEY (`userid_int`));


INSERT INTO setatest.useraccount (username_vch, password_vch, created_dt, role_int, FirstName_vch, LastName_vch) values
(
	'admin', AES_ENCRYPT('123456', 'SetatestEncryptKey'), NOW(), 1, 'Administrator', 'Seta'
);

INSERT INTO setatest.useraccount (username_vch, password_vch, created_dt, role_int, FirstName_vch, LastName_vch) values
(
	'jeff', AES_ENCRYPT('123456', 'SetatestEncryptKey'), NOW(), 2, 'Jeff', 'Peterson'
);

INSERT INTO setatest.useraccount (username_vch, password_vch, created_dt, role_int, FirstName_vch, LastName_vch) values
(
	'manhhd', AES_ENCRYPT('123456', 'SetatestEncryptKey'), NOW(), 2, 'Manh', 'Hoang'
);

INSERT INTO setatest.useraccount (username_vch, password_vch, created_dt, role_int, FirstName_vch, LastName_vch) values
(
	'trungnv', AES_ENCRYPT('123456', 'SetatestEncryptKey'), NOW(), 2, 'Trung', 'Nguyen'
);	
</pre>

<p><strong>Seta Update 2014-04-15</strong></p>
<pre>CREATE  TABLE `simplelists`.`CustomDefinedFields` (
  `CdfId_int` INT NOT NULL AUTO_INCREMENT ,
  `CdfName_vch` VARCHAR(255) NOT NULL ,
  `UserId_int` INT NOT NULL ,
  `CdfDefaultValue_vch` VARCHAR(255) NULL ,
  `Created_dt` DATETIME NULL ,
  PRIMARY KEY (`CdfId_int`) );
</pre>
<p>&nbsp;</p>
<p>Below insert command will migrate old data(VariableName_vch in table simplelists.contactvariable) into simplelists.customdefinedfields as new cdf records</p>
<p>&nbsp;</p>
<pre>
<s>INSERT INTO simplelists.customdefinedfields(
	CdfName_vch,
	UserId_int,
	CdfDefaultValue_vch,
	Created_dt
)
SELECT Distinct
VariableName_vch
variable,
userid_int,
'',
NOW()
FROM simplelists.contactvariable;<br /></s></pre>
<p><br />
    INSERT INTO simplelists.customdefinedfields(<br />
CdfName_vch,<br />
UserId_int,<br />
CdfDefaultValue_vch,<br />
Created_dt<br />
)<br />
SELECT Distinct<br />
VariableName_vch,<br />
userid_int,<br />
'',<br />
NOW()<br />
FROM simplelists.contactvariable<br />
WHERE<br />
UserId_int IS NOT NULL</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Below update command will update data of cdfId in simplelists.contactvariable as matched cdfId_int in simplelists.customdefinedfields</p>
<p>&nbsp;</p>
<pre>
UPDATE 
	simplelists.contactvariable 
JOIN 
	simplelists.customdefinedfields
ON 
	simplelists.contactvariable.VariableName_vch = simplelists.customdefinedfields.cdfName_vch
AND
	simplelists.contactvariable.userId_int = simplelists.customdefinedfields.userId_int
SET 
	simplelists.contactvariable.cdfid_int = simplelists.customdefinedfields.cdfid_int
</pre>
<p>&nbsp;</p>
<p>ALTER TABLE `simplelists`.`contactvariable` ADD COLUMN `CdfId_int` INT NULL  AFTER `Created_dt` ;</p>
<pre>
USE `simplelists`;
DROP procedure IF EXISTS `sp_addcdf`;

DELIMITER $$
USE `simplelists`$$
CREATE PROCEDURE `sp_addcdf`( IN inpDesc Varchar(100),
IN inpDefault Varchar(5000),
IN inpUserId INTEGER(11),
IN inpCdfId INTEGER(11)
)
BEGIN

INSERT INTO
simplelists.contactvariable
SELECT
NULL,
UserId_int,
ContactId_bi,
LEFT(inpDesc,100),
LEFT(inpDefault, 5000),
NOW(),
inpCdfId
FROM
simplelists.contactlist
WHERE
UserId_int = inpUserId;
END$$
DELIMITER ;
</pre>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-04-11</strong></p>
<p>CREATE TABLE `dashboard_templates` (<br />
`pkId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
`UserId_int` int(11) NOT NULL,<br />
`Desc_vch` text,<br />
`Template_vch` text,<br />
`Active_int` tinyint(4) DEFAULT '1',<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`pk_Id_int`),<br />
KEY `IDX_Combo_UserID_Desc` (`UserId_int`,`Desc_vch`(500)),<br />
KEY `IDX_UserId` (`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-04-08</strong></p>
<p>ALTER TABLE `simpleobjects`.`userreportingpref` <br />
ADD COLUMN `ClassInfo_vch` TEXT NULL AFTER `CustomData5_vch`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-04-03</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
ADD INDEX `IDX_Combo_CSC_SurveyState` (`SMSCSC_vch` ASC, `SMSSurveyState_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-04-02</strong></p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
ADD COLUMN `ContactFilter_vch` TEXT NULL DEFAULT NULL AFTER `RealTimeFlag_int`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-31</strong></p>
<p>ALTER TABLE `simpleobjects`.`userreportingpref` <br />
    ADD COLUMN `CustomData4_vch` TEXT NULL DEFAULT NULL AFTER `CustomData3_vch`,<br />
ADD COLUMN `CustomData5_vch` TEXT NULL DEFAULT NULL AFTER `CustomData4_vch`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-22</strong></p>
<p>CREATE TABLE simpleobjects.`systemalertscontacts` (<br />
    `pkId_int` int(11) NOT NULL AUTO_INCREMENT,<br />
    `ContactAddress_vch` varchar(1024) DEFAULT NULL,<br />
    `ContactType_int` int(11) DEFAULT '0',<br />
    `EMSNotice_int` tinyint(4) DEFAULT '0',<br />
    `NewUserNotice_int` tinyint(4) DEFAULT '0',<br />
    `BalanceAdjustNotice_int` tinyint(4) DEFAULT '0',<br />
    `Desc_vch` varchar(2024) DEFAULT NULL,<br />
    PRIMARY KEY (`pkId_int`),<br />
    KEY `IDX_COMBO_ContactType_EMS` (`ContactType_int`,`EMSNotice_int`),<br />
    KEY `IDX_COMBO_ContactType_NewUser` (`ContactType_int`,`NewUserNotice_int`),<br />
    KEY `IDX_COMBO_ContactType_Bal` (`BalanceAdjustNotice_int`,`ContactType_int`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-21</strong></p>
<p><br />
    For creating of datasource for EBM report:<br />
    CREATE DATABASE `simplexreports`<br />
    <br />
    Table Creation:<br />
    CREATE TABLE `simplexreports`.`batchquestions` (<br />
`pkId_int` int(11) NOT NULL AUTO_INCREMENT,<br />
`batchId_int` int(11) DEFAULT NULL,<br />
`keyword_vch` varchar(45) DEFAULT NULL,<br />
`optionCount_int` int(11) DEFAULT NULL,<br />
`quesId_int` int(11) DEFAULT NULL,<br />
`text_vch` varchar(8000) DEFAULT NULL,<br />
`type` varchar(45) DEFAULT NULL,<br />
`gid_int` int(11) DEFAULT NULL,<br />
`iType_vch` varchar(45) DEFAULT NULL,<br />
PRIMARY KEY (`pkId_int`),<br />
KEY `rply_index` (`batchId_int`,`quesId_int`,`optionCount_int`,`text_vch`(767))<br />
) ENGINE=InnoDB AUTO_INCREMENT=14170 DEFAULT CHARSET=latin1<br />
<br />
CREATE TABLE `simplexreports`.`questionsoptions` (<br />
`pkId_int` int(11) NOT NULL AUTO_INCREMENT,<br />
`optionNum_int` int(11) DEFAULT NULL,<br />
`text_vch` varchar(8000) DEFAULT NULL,<br />
`batchid_bi` bigint(20) DEFAULT NULL,<br />
`quesId_int` int(11) DEFAULT NULL,<br />
PRIMARY KEY (`pkId_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=3725 DEFAULT CHARSET=latin1<br />
<br />
CREATE INDEX qoption_batch_index<br />
on simplexreports.questionsoptions(batchid_bi)<br />
</p>
<p><strong>JLP Update 2014-03-18</strong></p>
<p><br />
    -- Used for storage of custom program data <br />
create database simplexprogramdata;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-17</strong></p>
<p>CREATE TABLE `simpleobjects`.`useremailtarget` (<br />
`UserId_int` INT NOT NULL,<br />
`UserName_vch` VARCHAR(1024) NULL,<br />
`Password_vch` VARCHAR(1024) NULL,<br />
`RemoteAddress_vch` VARCHAR(1024) NULL,<br />
`RemotePort_vch` VARCHAR(45) NULL,<br />
`TLSFlag_ti` TINYINT NULL,<br />
`SSLFlag_ti` TINYINT NULL,<br />
PRIMARY KEY (`UserId_int`));</p>
<p>CREATE TABLE `simpleobjects`.`usersystemlimits` (<br />
    `UserId_int` INT NOT NULL,<br />
    `MaxEMSPerSend_int` INT NULL DEFAULT 100000,<br />
    `MaxEMSAudioFileLength_int` INT NULL DEFAULT 180,<br />
PRIMARY KEY (`inpUserId_int`));</p>
<p>CREATE TABLE simplexresults.apitracking (<br />
`APITrackingId_int` bigint(11) NOT NULL AUTO_INCREMENT,<br />
`EventId_int` int(11) DEFAULT '0',<br />
`UserId_int` int(11) DEFAULT '0',<br />
`Created_dt` datetime DEFAULT NULL,<br />
`Subject_vch` text,<br />
`Message_vch` text,<br />
`TroubleShootingTips_vch` text,<br />
`Output_vch` text, <br />
`DebugStr_vch` text, <br />
`DataSource_vch` varchar(2048),<br />
`Host_vch` varchar(2048) DEFAULT NULL,<br />
`Referer_vch` varchar(2048) DEFAULT NULL,<br />
`UserAgent_vch` varchar(2048) DEFAULT NULL,<br />
`Path_vch` varchar(2048) DEFAULT NULL,<br />
`QueryString_vch` text,<br />
PRIMARY KEY (`APITrackingId_int`),<br />
KEY `IDX_Combo_ErrorNumber_Created` (`EventId_int`,`Created_dt`),<br />
KEY `IDX_Combo_ErrorNumber_Created_User` (`EventId_int`,`Created_dt`, `UserId_int`),<br />
KEY `IDX_EventId_int` (`EventId_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-13</strong></p>
<p>    ALTER TABLE `simplequeue`.`errorlogs` <br />
    ADD INDEX `IDX_Combo_ErrorNumber_Created` (`ErrorNumber_int` ASC, `Created_dt` ASC),<br />
ADD INDEX `IDX_ErrorNumber` (`ErrorNumber_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-12</strong></p>
<p>CREATE TABLE simplelists.`optinout` (<br />
    `OptId_int` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `UserId_int` int(11) DEFAULT NULL,<br />
    `ContactString_vch` varchar(255) DEFAULT NULL,<br />
    `OptOut_dt` datetime DEFAULT NULL,<br />
    `OptIn_dt` datetime DEFAULT NULL,<br />
    `OptOutSource_vch` varchar(255) DEFAULT NULL,<br />
    `OptInSource_vch` varchar(255) DEFAULT NULL,<br />
    `ShortCode_vch` varchar(255) DEFAULT NULL,<br />
    `FromAddress_vch` varchar(255) DEFAULT NULL,<br />
    `CallerId_vch` varchar(20) DEFAULT NULL,<br />
    `BatchId_bi` bigint(20) DEFAULT '0',<br />
    PRIMARY KEY (`OptId_int`),<br />
    KEY `IDX_Combo_Contact_CSC_In_Out` (`OptOut_dt`,`OptIn_dt`,`ShortCode_vch`,`ContactString_vch`),<br />
    KEY `IDX_Combo_Contact_User_CSC_In_Out` (`UserId_int`,`OptOut_dt`,`OptIn_dt`,`ShortCode_vch`,`ContactString_vch`),<br />
    KEY `IDX_Combo_Contact_FromAddress_In_out` (`ContactString_vch`,`OptOut_dt`,`OptIn_dt`,`FromAddress_vch`),<br />
    KEY `IDX_Combo_Contact_User_In_Out` (`ContactString_vch`,`UserId_int`,`OptInSource_vch`,`OptOutSource_vch`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-07</strong></p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD COLUMN `EMSFlag_int` TINYINT NULL DEFAULT 0 AFTER `ToolTip_vch`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-05</strong></p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
ADD COLUMN `RealTimeFlag_int` TINYINT NULL DEFAULT 0 AFTER `EMS_Flag_int`;</p>
<p>ALTER TABLE `simplexfulfillment`.`device` <br />
ADD COLUMN `ErrorCount_int` INT NULL DEFAULT 0 AFTER `Modified_dt`;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-03-04</strong> - Redo - fulfillment devices table</p>
<p>&nbsp;</p>
<p>DROP TABLE simplexfulfillment.device;</p>
<p>CREATE TABLE simplexfulfillment.`device` (<br />
`DeviceId_int` int(11) NOT NULL AUTO_INCREMENT,<br />
`IP_vch` varchar(255) NOT NULL,<br />
`Desc_vch` varchar(2048) NOT NULL DEFAULT 'EBM RXDialer',<br />
`SystemLoad_int` int(11) NOT NULL DEFAULT '2500',<br />
`VoiceEnabled_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`WebServiceEnabled_int` tinyint(4) DEFAULT '0',<br />
`EmailEnabled_int` tinyint(4) DEFAULT '0',<br />
`ScriptRecordEnabled_int` tinyint(4) DEFAULT '0',<br />
`Position_int` int(11) DEFAULT '1',<br />
`RealTime_int` tinyint(4) DEFAULT '0',<br />
`Enabled_dt` tinyint(4) DEFAULT '0',<br />
`Modified_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`DeviceId_int`),<br />
KEY `IDX_Position` (`Position_int`),<br />
KEY `IDX_Combo_enabled_tele_email_web_realtime` (`VoiceEnabled_int`,`WebServiceEnabled_int`,`EmailEnabled_int`,`Enabled_dt`,`RealTime_int`),<br />
KEY `IDX_Combo_enabled_tele_realtime` (`RealTime_int`,`Enabled_dt`,`VoiceEnabled_int`),<br />
KEY `IDX_Combo_enabled_web_realtime` (`RealTime_int`,`Enabled_dt`,`WebServiceEnabled_int`),<br />
KEY `IDX_Combo_enabled_email_realtime` (`EmailEnabled_int`,`RealTime_int`,`Enabled_dt`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-02-28</strong></p>
<p>-- somehow a dev db had wrong column name - update to ControlKey_int incase you have wrong value.</p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
    CHANGE COLUMN `ControlPoint_int` `ControlKey_int` INT(11) NULL DEFAULT '0' ;</p>
<p><strong>JLP Update 2014-02-26 - Index maintanence</strong></p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
ADD INDEX `IDX_Combo_User_status` (`UserId_int` ASC, `DTSStatusType_ti` ASC);</p>
<p>ALTER TABLE `simpleobjects`.`securitycredentials` <br />
ADD INDEX `IDX_Combo_access_active` (`AccessKey_vch` ASC, `Active_ti` ASC);</p>
<p>ALTER TABLE `simpleobjects`.`securitycredentials` <br />
    CHANGE COLUMN `Active_ti` `Active_ti` TINYINT(3) UNSIGNED NOT NULL AFTER `Created_dt`,<br />
ADD COLUMN `SecretName_vch` VARCHAR(255) NULL AFTER `SecretKey_vch`;</p>
<p><strong>JLP Update 2014-02-25 - Index maintanence</strong></p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
ADD INDEX `IDX_ContactString` (`ContactString_vch` ASC);</p>
<p><strong>JLP Update 2014-02-24 - Index maintanence</strong></p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD INDEX `idx_isdefault` (`IsDefault_bit` ASC);</p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD INDEX `idx_combo_keyword_scid` (`ShortCodeId_int` ASC, `Keyword_vch` ASC);</p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD INDEX `IDX_COMBO_scrid_active` (`ShortCodeRequestId_int` ASC, `Active_int` ASC);</p>
<p>ALTER TABLE `sms`.`shortcoderequest` <br />
ADD INDEX `IDX_COMBO_scid_scrid` (`ShortCodeRequestId_int` ASC, `ShortCodeId_int` ASC);</p>
<p>ALTER TABLE `sms`.`shortcode` <br />
ADD INDEX `IDX_COMBO_scid_sc` (`ShortCodeId_int` ASC, `ShortCode_vch` ASC);</p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD INDEX `IDX_COMBO_scr_sc_keyword_active` (`Active_int` ASC, `ShortCodeRequestId_int` ASC, `ShortCodeId_int` ASC, Keyword_vch ASC);</p>
<p>ALTER TABLE `sms`.`keyword` <br />
ADD INDEX `IDX_COMBO_keyword_active` (`Keyword_vch` ASC, `Active_int` ASC);</p>
<p>ALTER TABLE `sms`.`shortcode` <br />
ADD INDEX `IDX_SC` (`ShortCode_vch` ASC);</p>
<p>ALTER TABLE `simplelists`.`contactvariable` <br />
ADD INDEX `IDX_ContactOnly` (`ContactId_bi` ASC);</p>
<p>ALTER TABLE `simplelists`.`contactlist` <br />
ADD INDEX `IDX_Combo_user_contactid` (`ContactId_bi` ASC, `UserId_int` ASC);</p>
<p>truncate table simplexresults.smsmtabusetracking</p>
<p>ALTER TABLE `simplexresults`.`smsmtabusetracking` <br />
    ADD INDEX `IDX_Combo_Contact_trans_messpart` (`ContactString_vch` ASC, `TransactionId_vch` ASC, `MessagePart_int` ASC),<br />
    ADD INDEX `IDX_Combo_contact_messpart_created` (`ContactString_vch` ASC, `Created_dt` ASC, `MessagePart_int` ASC);</p>
<p>ALTER TABLE `simplequeue`.`contactqueue` <br />
    ADD INDEX `IDX_COMBO_contact_sc_dtsstatus` (`DTSStatusType_ti` ASC, `ContactString_vch` ASC, `ShortCode_vch` ASC);</p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
ADD INDEX `IDX_COMBO_contactstring_smssurveystate_shortcode_batch` (`ContactString_vch` ASC, `SMSSurveyState_int` ASC, SMSCSC_vch ASC, Batchid_bi ASC);</p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
ADD INDEX `IDX_COMBO_contactstring_smssurveystate` (`ContactString_vch` ASC, `SMSSurveyState_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-02-11</strong></p>
<p>CREATE INDEX batchId_index<br />
ON sms.keyword (BatchId_bi)</p>
<p>CREATE INDEX IDX_COMBO_dt_cs_batchId<br />
ON simplexresults.contactresults (BatchId_bi,RXCDLStartTime_dt,ContactString_vch)</p>
<p><strong>JLP Update 2014-02-10</strong></p>
<p>CREATE INDEX rply_index1<br />
ON simplexresults.surveyresults (batchId_bi,contactString_vch,created_dt)</p>
<p><strong>JLP Update 2014-02-06</strong></p>
<p>-- Redundant Index  <br />
    ALTER TABLE `simplexresults`.`contactresults` <br />
DROP INDEX `contactresults_idx1` ;</p>
<p>-- Add Index <br />
    ALTER TABLE `simplexresults`.`contactresults` <br />
ADD INDEX `IDX_Combo_Date_CSC_SMSResult` (`RXCDLStartTime_dt` ASC, `SMSResult_int` ASC, `SMSCSC_vch` ASC);</p>
<p><strong>JLP Update 2014-02-05</strong></p>
<p>ALTER TABLE `simpleobjects`.`batch` <br />
    ADD COLUMN `EMS_Flag_int` TINYINT NULL DEFAULT 0 AFTER `ContactIsApplyFilter`,<br />
ADD INDEX `IDX_EMSFlag` (`EMS_Flag_int` ASC);</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2014-02-04</strong></p>
<p>
CREATE TABLE simpleobjects.`userreportingquadrentpref` (
  `UserReportingPref_id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) NOT NULL DEFAULT '0',
  `BatchListId_vch` text,
  `ChartQuantity_int` int(11) DEFAULT '4',
  `DateUpdated_dt` datetime NOT NULL,
  PRIMARY KEY (`UserReportingPref_id`),
  KEY `IDX_userreportingpref_int` (`UserId_int`,`BatchListId_vch`(255))
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;
</p>




<p><strong>Seta Update 2014-01-15</strong></p>
<p>ALTER TABLE `simpleobjects`.`cannedresponse`</p>
<p>ADD COLUMN `Approved_bt` BIT(1) NULL  AFTER `Public_bt` ,</p>
<p>CHANGE COLUMN `Active_bi` `Public_bt` BIT(1) NULL DEFAULT NULL  ;</p> 
<br/>
<br/>

<p><strong>Seta Update 2014-01-13</strong></p>
<p>ALTER TABLE `simplelists`.`customerpreferenceportal`</p>
<p>CHANGE COLUMN `StepSetup_vch` `StepSetup_vch` VARCHAR(500) NULL DEFAULT '2,3,4'  ;</p> 
<br/>
<br/>

<p><strong>Seta Update 2014-01-15</strong></p>
<pre>
CREATE TABLE `simplelists`.`cpp_stepdata` (
  `stepDataID_int` int(11) NOT NULL AUTO_INCREMENT,
  `CPP_UUID_vch` varchar(45) DEFAULT NULL,
  `title_vch` text,
  `description_vch` text,
  `step_type_ti` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`stepDataID_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='store title and description of step in cpp'

</pre>

<p><strong>JLP Update 2014-01-10</strong></p>
<p>CREATE TABLE `simplelists`.`cpp_customhtml` (</p>
<p>`customHtmlId_bi` bigint(20) NOT NULL AUTO_INCREMENT ,</p>
<p>`customHtml_txt` TEXT NULL ,</p>
<p>`CPP_UUID_vch` VARCHAR(36) NOT NULL,</p>
<p>PRIMARY KEY (`customHtmlId_bi`) );</p>
<p>&nbsp;</p>

<p><strong>Seta Update 2014-01-08</strong></p>
<p>ALTER TABLE `simpleobjects`.`cannedresponse`</p>
 <p>ADD COLUMN `AgentId_int` INT NULL DEFAULT NULL  AFTER `BatchId_bi` , ADD COLUMN `Active_bi` BIT NULL DEFAULT NULL  AFTER `AgentId_int` ;</p>
<br/>
<br/>

<p><strong>JLP Update 2013-12-17</strong></p>
<p>    Create database simplexfulfillment</p>
<p>CREATE TABLE `simplexfulfillment`.`device` (<br />
`DeviceId_int` INT NOT NULL AUTO_INCREMENT,<br />
`IP_vch` VARCHAR(255) NOT NULL,<br />
`Desc_vch` VARCHAR(2048) NOT NULL,<br />
`SystemLoad_int` INT NOT NULL DEFAULT 100,<br />
`TeleCommEnabled_int` TINYINT NOT NULL DEFAULT 0,<br />
`Position_int` INT NULL,<br />
PRIMARY KEY (`DeviceId_int`),<br />
INDEX `IDX_Position` (`Position_int` ASC));</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-12-13</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` <br />
ADD COLUMN `ControlPoint_int` INT(11) NULL DEFAULT '0' AFTER `APIRequestJSON_vch`;</p>
<p>--- ALTER TABLE `simplexresults`.`contactresults` CHANGE COLUMN `ControlKey_int` `ControlPoint_int` INT(11) NULL DEFAULT '0'  ;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Seta Update 2013-12-09</strong></p>
<p>create a new table called "LanguagePreference" to store cpp preference</p>
CREATE TABLE simplelists. `cpp_languagepreference` (
  `LanguagePreferenceId_int` int(11) NOT NULL AUTO_INCREMENT,
  `CPP_UUID_vch` varchar(36) DEFAULT NULL,
  `LanguagePreference_vch` varchar(255) DEFAULT NULL,
  `Order_int` int(11) DEFAULT NULL,
  PRIMARY KEY (`LanguagePreferenceId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1$$
<br/>
<p>Create a new column called "LanguagePreference_vch" in contactlist table</p>
ALTER TABLE `simplelists`.`contactlist` ADD COLUMN `LanguagePreference_vch` VARCHAR(255) NULL DEFAULT NULL  AFTER `CompanyUserId_int` ;


<p>&nbsp;</p>
<p><strong>JLP Update 2013-12-08</strong></p>
<p>Add Stored procedure to speed up adding custom variable on large list sizes</p>
<p>USE `simplelists`;<br />
DROP procedure IF EXISTS `sp_addcdf`;</p>
<p>DELIMITER $$<br />
  USE `simplelists`$$<br />
  CREATE PROCEDURE `sp_addcdf` 	(	IN inpDesc Varchar(100),<br />
  IN inpDefault Varchar(5000),<br />
  IN inpUserId INTEGER(11)<br />
  )<br />
  BEGIN</p>
<p> INSERT INTO <br />
  simplelists.contactvariable<br />
  SELECT<br />
  NULL,<br />
  UserId_int,<br />
  ContactId_bi,<br />
  LEFT(inpDesc,100),<br />
  LEFT(inpDefault, 5000), <br />
  NOW()<br />
  FROM<br />
  simplelists.contactlist<br />
  WHERE<br />
  UserId_int = inpUserId; <br />
  END</p>
<p>$$</p>
<p>DELIMITER ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-11-07</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` ADD COLUMN `APIRequestJSON_vch` VARCHAR(2048) NULL DEFAULT NULL  AFTER `SMSMTPostResultCode_vch` ;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-10-31<br />
    </strong><br />
    delimiter $$</p>
<p>CREATE TABLE sms.smsdip (<br />
    `ContactString_vch` varchar(255) DEFAULT NULL,<br />
    `Type_vch` varchar(50) DEFAULT NULL,<br />
    `OCN_int` int(11) DEFAULT NULL,<br />
    `OverallOCN_int` bigint(20) DEFAULT NULL,<br />
    `DBA_vch` varchar(512) DEFAULT NULL,<br />
    `CName_vch` varchar(512) DEFAULT NULL,<br />
    `HoldCo_vch` varchar(512) DEFAULT NULL,<br />
    `Management_vch` varchar(512) DEFAULT NULL,<br />
    `SMSMail_vch` varchar(512) DEFAULT NULL,<br />
    `Mblox_int` int(11) DEFAULT NULL,<br />
    `Date_dt` datetime DEFAULT NULL,<br />
    `CommonName_vch` varchar(512) DEFAULT NULL<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$ </p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-10-22</strong></p>
<p> ALTER TABLE `simplexresults`.`smsdisposition` ADD COLUMN `AggregatorId_int` INT NULL DEFAULT 0  AFTER `RawData_vch` ;</p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
ADD INDEX `IDX_CreatedAggregator` (`AggregatorId_int` ASC, `Created_dt` ASC) ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-10-17</strong></p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` CHANGE COLUMN `Keyword_vch` `Keyword_vch` VARCHAR(1024) NULL DEFAULT NULL  ;<br />
    <br />
ALTER TABLE `simplexresults`.`surveyresults` CHANGE COLUMN `Response_vch` `Response_vch` VARCHAR(1000) NULL DEFAULT NULL  ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-10-15</strong></p>
<p>In EBM I prefer the Note_vch column to be named Desc_vch - just a preference - please update in Seta DB and Code</p>
<p>ALTER TABLE `simplelists`.`grouplist` CHANGE COLUMN `Note_vch` `Desc_vch` VARCHAR(1000) NULL DEFAULT NULL  ;</p>
<p>&nbsp;</p>
<p><strong><s>Seta update on 2013-10-14</s></strong></p>
<pre>
<s>ALTER TABLE `rxds`.`scriptdata` 
ADD COLUMN `RemoteId_int` INT NULL AFTER `REPORTABUSECOUNT_INT`,
ADD COLUMN `RemoteGroupId_bi` BIGINT NULL AFTER `RemoteId_int`;</s>
</pre>

<p><strong>Seta update on 2013-10-10</strong></p>
<pre>
ALTER TABLE `simplelists`.`grouplist` 
ADD COLUMN `Note_vch` VARCHAR(1000) NULL AFTER `remoteId_int`;
</pre>
<p><strong>JLP Update 2013-10-08</strong></p>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` ADD INDEX `IDX_Keywords` (`Keyword_vch` ASC, `ShortCode_vch` ASC, `Created_dt` ASC) ;</p>
<p>&nbsp;</p>
<p><strong>Seta Update 2013-10-08</strong></p>
<p>CREATE  TABLE `simpleobjects`.`hauinvitationcodebatch` (</p>
<p>`Hauinvitationcode_vch` VARCHAR(255) NOT NULL ,</p>
<p>`BatchId_bi` BIGINT NOT NULL ,</p>
<p>PRIMARY KEY (`Hauinvitationcode_vch`, `BatchId_bi`) );</p>
<br />
<p><strong>JLP Update 2013-10-06</strong></p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
ADD INDEX `IDX_CreatedSubscriber` (`Created_dt` ASC, `SubscriberNumber_vch` ASC) ;<br />
</p>
<p><strong>Seta Update 2013-10-03</strong></p>
<pre>
	ALTER TABLE `simplelists`.`grouplist` 
	ADD COLUMN `remoteId_int` INT(11) NULL AFTER `Active_int`;
</pre>
<p><strong>JLP Update 2013-10-02</strong></p>
<p>ALTER TABLE `sms`.`shortcode` ADD COLUMN `PreferredAggregator_int` TINYINT NULL DEFAULT 1  AFTER `CustomServiceId5_vch` ;</p>
<p>&nbsp;</p>
<p><strong>SETA Update 2013-09-28</strong></p>
<p>ALTER TABLE `simpleobjects`.`cannedresponse` ADD COLUMN `BatchId_bi` BIGINT NULL DEFAULT NULL  AFTER `Updated_dt` ;</p><br />
<p>ALTER TABLE `simplexresults`.`conversationmessage` ADD COLUMN `BatchId_bi` BIGINT NULL DEFAULT NULL  AFTER `SessionLocked_ti` ;</p><br />

<p><strong>JLP Update 2013-09-26 - Reporting cleanup</strong></p>
<p>ALTER TABLE `simpleobjects`.`userreportingpref` ADD COLUMN `customdata1_vch` TEXT NULL  AFTER `ReportType_vch` , ADD COLUMN `customdata2_vch` TEXT NULL  AFTER `customdata1_vch` , ADD COLUMN `customdata3_vch` TEXT NULL  AFTER `customdata2_vch` ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-09-13 - Reporting cleanup</strong></p>
<p>ALTER TABLE `simplequeue`.`contactqueue` ADD COLUMN `ProcTime_int` INT NULL DEFAULT 0  AFTER `ShortCode_vch` ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-09-4 - Reporting cleanup</strong></p>
<p>ALTER TABLE `simpleobjects`.`userreportingpref` ADD COLUMN `ReportType_vch` VARCHAR(45) NULL DEFAULT 'CHART'  AFTER `ReportId_int` ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-09-4 - general Cleanup</strong></p>
<p>ALTER TABLE `simplelists`.`contactlist` <br />
ADD INDEX `IDX_UserId` (`UserId_int` ASC) ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update 2013-08-30 - More tracking updates</strong></p>
<p>INSERT INTO `simplexresults`.`smssurveystates` (`SMSSurveyState_int`, `Desc_vch`) VALUES (8, 'Interactive Campaign Stop Request');<br />
  INSERT INTO `simplexresults`.`smssurveystates` (`SMSSurveyState_int`, `Desc_vch`) VALUES (3, 'Waiting for next Response Interval');<br />
INSERT INTO `simplexresults`.`smssurveystates` (`SMSSurveyState_int`, `Desc_vch`) VALUES (4, 'Complete');</p>
<p>UPDATE `simplexresults`.`smssurveystates` SET `Desc_vch`='SMSSURVEYSTATE_INTERVAL_HOLD' WHERE `SMSSurveyState_int`='2';<br />
  <br />
ALTER TABLE `simplexresults`.`smssurveystates` ADD COLUMN `DescSmall_vch` VARCHAR(255) NULL  AFTER `Desc_vch` ;</p>
<p>UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='NOTASURVEY' WHERE `SMSSurveyState_int`='0';<br />
  UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='RUNNING' WHERE `SMSSurveyState_int`='1';<br />
  UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='INTERVAL_HOLD' WHERE `SMSSurveyState_int`='2';<br />
  UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='RESPONSEINTERVAL' WHERE `SMSSurveyState_int`='3';<br />
  UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='COMPLETE' WHERE `SMSSurveyState_int`='4';<br />
UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='CANCELLED' WHERE `SMSSurveyState_int`='5';<br />
UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='EXPIRED' WHERE `SMSSurveyState_int`='6';<br />
UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='TERMINATED' WHERE `SMSSurveyState_int`='7';<br />
UPDATE `simplexresults`.`smssurveystates` SET `DescSmall_vch`='STOPPED' WHERE `SMSSurveyState_int`='8';<br />
</p>
<p><strong>SETA Update 2013-08-28 Add babble user id (BS to EBM integration)</strong></p>
<p><s>
	ALTER TABLE `simplelists`.`grouplist` <br/>
	ADD COLUMN `babbleUserId_int` INT NULL AFTER `Active_int`;</s><br/>
</p>
<b>Add more column session lock</b>
<p>ALTER TABLE `simplexresults`.`conversationmessage` ADD COLUMN `SessionLocked_ti` TINYINT NULL  AFTER `SessionCode_vch` ;</p>


<p><strong>JLP Update 2013-08-23 Reproting update</strong></p>
<p>drop table `simpleobjects`.`userreportingpref`;</p>
<p>CREATE TABLE `simpleobjects`.`userreportingpref` (<br />
    `UserReportingPref_id` int(11) NOT NULL AUTO_INCREMENT,<br />
    `UserId_int` int(11) NOT NULL DEFAULT '0',<br />
    `BatchListId_vch` text NOT NULL,<br />
    `QuadarantId_int` int(11) NOT NULL DEFAULT '0',<br />
`ReportName_vch` varchar(255) DEFAULT NULL,<br />
`DateUpdated_dt` datetime NOT NULL,<br />
    PRIMARY KEY (`UserReportingPref_id`),<br />
    KEY `IDX_userreportingpref_int` (`UserId_int`,`QuadarantId_int`, `BatchListId_vch` (255))<br />
) ENGINE=innoDB</p>
<p>&nbsp;</p>
<p><strong>Seta VN  Update 2013-08-21 create simpleobjects.cannedresponse table</strong></p>
<p>
CREATE  TABLE `simpleobjects`.`cannedresponse` (<br/>
  `CannedResponseId_int` INT NOT NULL AUTO_INCREMENT ,<br/>
  `Title_vch` VARCHAR(255) NULL ,<br/>
  `Response_vch` VARCHAR(255) NULL ,<br/>
  `OwnerId_int` INT NULL,<br/>
  `OwnerType_ti` TINYINT NULL,<br/>
  `Created_dt` DATETIME NULL,<br/>
  `Updated_dt` DATETIME NULL,<br/>
  PRIMARY KEY (`CannedResponseId_int`) );<br/>
</p>
<p><strong>Seta VN  Update 2013-08-14 create simpleobjects.question table</strong></p>
CREATE TABLE `simpleobjects`.`haucategory` (<br/>
  `haucategoryId_int` int(11) NOT NULL AUTO_INCREMENT,<br/>
  `Title_vch` varchar(255) DEFAULT NULL,<br/>
  `Description_vch` varchar(1000) DEFAULT NULL,<br/>
  `Order_int` int(11) DEFAULT NULL,<br/>
  `Created_dt` datetime DEFAULT NULL,<br/>
  `Update_dt` datetime DEFAULT NULL,<br/>
  `OwnerId_int` int(11) DEFAULT NULL,<br/>
  `OwnerType_ti` tinyint(4) DEFAULT NULL,<br/>
  PRIMARY KEY (`haucategoryId_int`)<br/>
);<br/>


<p><strong>Seta VN  Update 2013-08-13 create simpleobjects.question table</strong></p>
<p>CREATE TABLE `simpleobjects`.`question` (<br />
    `QuestionId_int` INT NULL ,<br />
    `Question_vch` VARCHAR(255) NULL ,<br />
    `Answer_vch` VARCHAR(255) NULL ,<br />
    `CategoryId_int` INT NULL ,<br />
    `Created_dt` DATETIME NULL,<br />
    `Update_dt` DATETIME NULL,<br />
    PRIMARY KEY (`QuestionId_int`) );<br/>
  ALTER TABLE `simpleobjects`.`question` CHANGE COLUMN `QuestionId_int` `QuestionId_int` INT(11) NOT NULL AUTO_INCREMENT ;<br/>
</p>

<p><strong>Seta VN  Update 2013-08-10 alter simpleobjects.hausetting table</strong></p>
<p>
	ALTER TABLE `simpleobjects`.`hausettings` ADD COLUMN `OwnerId_int` INT NOT NULL DEFAULT 0  AFTER `AutoRouteReturningRequest_bt` , ADD COLUMN `OwnerType_ti` TINYINT NOT NULL DEFAULT 0  AFTER `OwnerId_int` ;

</p>

<p><strong>JLP Update 2013-08-01 - More tracking updates</strong></p>
<p>delimiter $$</p>
<p>CREATE TABLE `surveyresults` (<br />
    `SurveyResultsId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `BatchId_bi` bigint(20) DEFAULT NULL,<br />
    `CPId_int` int(11) DEFAULT NULL,<br />
    `QID_int` int(11) DEFAULT NULL,<br />
    `Created_dt` datetime DEFAULT NULL,<br />
    `ContactString_vch` varchar(255) DEFAULT NULL,<br />
    `Response_vch` varchar(512) DEFAULT NULL,<br />
    PRIMARY KEY (`SurveyResultsId_bi`),<br />
    KEY `IDX_Combo` (`BatchId_bi`,`Created_dt`,`Response_vch`,`ContactString_vch`)<br />
    ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1$$</p>
<p><strong>JLP Update 2013-07-31 - More tracking updates</strong></p>
<p>ALTER TABLE `simplexresults`.`contactresults` ADD COLUMN `SMSMTPostResultCode_vch` VARCHAR(255) NULL DEFAULT '0'  AFTER `SMSTrackingTwo_bi` ;</p>
<p>delimiter $$<br />
    CREATE TABLE `smsmtabusetracking` (<br />
    `SMSMTAbuseTrackingId_bi` bigint(20) NOT NULL AUTO_INCREMENT,<br />
    `ContactString_vch` varchar(255) DEFAULT NULL,<br />
    `TransactionId_vch` varchar(255) DEFAULT NULL,<br />
    `MessagePart_int` int(11) DEFAULT NULL,<br />
    `Created_dt` datetime DEFAULT NULL,<br />
    PRIMARY KEY (`SMSMTAbuseTrackingId_bi`),<br />
    KEY `IDX_Combo` (`ContactString_vch`,`Created_dt`,`TransactionId_vch`,`MessagePart_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1$$</p>
<p><strong>JLP Update 2013-07-30 - More tracking updates</strong><br />
</p>
<p>ALTER TABLE `sms`.`shortcode` ADD COLUMN `CustomServiceId3_vch` VARCHAR(255) NULL  AFTER `CustomServiceId2_vch` , ADD COLUMN `CustomServiceId4_vch` VARCHAR(255) NULL  AFTER `CustomServiceId3_vch` , ADD COLUMN `CustomServiceId5_vch` VARCHAR(255) NULL  AFTER `CustomServiceId4_vch` ;</p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
    ADD INDEX `IDX_Subscriber` (`SubscriberNumber_vch` ASC) ;<br />
</p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` <br />
    ADD INDEX `IDX_CreatedReasonCombo` (`Created_dt` ASC, `Reason_vch` ASC) ;</p>
<p>ALTER TABLE `simplexresults`.`smsdisposition` ADD COLUMN `RawData_vch` TEXT NULL  AFTER `Created_dt` ;</p>
<p><br />
    CREATE TABLE `simplexresults`.`smbloxdispositioncodes` (<br />
    `ReasonCode_vch` VARCHAR(512) NULL ,<br />
    `Status_vch` VARCHAR(512) NULL ,<br />
    `Reason_vch` VARCHAR(512) NULL ,<br />
    `Type_vch` VARCHAR(512) NULL ,<br />
    `Details_vch` TEXT NULL ,<br />
PRIMARY KEY (`ReasonCode_vch`) );</p>
<p>CREATE  TABLE `simplexresults`.`smsdisposition` (<br />
`SMSDispositionId_int` BIGINT NOT NULL ,<br />
`SubscriberNumber_vch` VARCHAR(512) NULL ,<br />
`Status_vch` VARCHAR(512) NULL ,<br />
`MsgReference_vch` VARCHAR(512) NULL ,<br />
`Reason_vch` VARCHAR(512) NULL ,<br />
`nSMSGlobalMessageId_vch` VARCHAR(512) NULL ,<br />
`SequenceNumber_vch` VARCHAR(512) NULL ,<br />
`SecondarySequenceNumber_vch` VARCHAR(512) NULL ,<br />
`Time_vch`  VARCHAR(512) NULL ,<br />
`Created_dt` DATETIME NULL ,<br />
PRIMARY KEY (`SMSDispositionId_int`) );</p>
<p><br />
    Save the data below to a file called MBLOXDispo.csv and then import with<br />
    <br />
    LOAD DATA LOCAL INFILE '/Users/JLP/Downloads/mbloxdispo.csv' INTO TABLE `simplexresults`.`smbloxdispositioncodes` <br />
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '&quot;'<br />
    LINES TERMINATED BY '\n'<br />
    (<br />
    `ReasonCode_vch`,<br />
    `Status_vch`,<br />
    `Reason_vch`,<br />
    `Type_vch`,<br />
`Details_vch`</p>
<p>1,Buffered,Phone related,Intermediate,Intermediate state notification that the message has not yet been delivered due to a phone related problem but is being retried.<br />
    2,Buffered,Deliverer related: message within carrier,Intermediate,Used to indicate that the message has not yet been delivered due to some carrier related problem but is being retried within the network.<br />
    3,Acked,Accepted by carrier,Intermediate,&quot;Used to indicate that the message has been accepted by the carrier. For certain carriers, this may be interpreted that the carrier has reported successful billing of the subscriber.&quot;<br />
    4,Delivered,Delivered to mobile device,Permanent,The message was delivered.<br />
    5,Failed,Message failed - detailed reason unknown,Unknown,The message has been confirmed as undelivered but no detailed information related to the failure is known.<br />
    6,Unknown,This carrier does not provide handset status,Unknown,mBlox cannot determine whether this message has been delivered or has failed due to lack of final delivery state information from the carrier.<br />
    7,Buffered,Credit related - message may be being retried,Intermediate,The message has not yet been delivered due to insufficient subscriber credit but is being retried within the network.<br />
    8,Failed,Message expired within the carrier and failure reason is unknown,Check Carrier matrix,Used when a message expired (could not be delivered within the life time of the message) within the carrier SMSC but is not associated with a reason for failure.<br />
    20,Failed,Permanent carrier error,Permanent,&quot;Used when a message in its current form is undeliverable or request is invalid. This is often triggered as a result of an Invalid Originator (Shortcode) ,Product ID or VZW Program ID&quot;<br />
    21,Failed,Credit related: message has been retried by carrier,Temporary,Only occurs where the carrier accepts the message before performing the subscriber credit check. If there is insufficient credit then the carrier will retry the message until the subscriber tops up or the message expires. If the message expires and the last failure reason is related to credit then this error code will be used.<br />
    22,Failed,Credit related: message has NOT been retried by carrier,Temporary/Permanent on T-mobile &amp; AT&amp;T,&quot;Only occurs where the carrier performs the subscriber credit check before accepting the message and rejects messages if there are insufficient funds available. For Virgin Mobile USA subscribers only, the client should respond with the following message ???You do not have enough funds to participate in this program. Please visit www.virginmobileusa.com to top up your account.???&quot;<br />
    23,Failed,Absent subscriber permanent,Permanent,Used when the message is undeliverable due to an incorrect / invalid / blacklisted / or permanently barred MSISDN for this carrier (Blocked). This MSISDN should be removed from the client???s database and not be used again for message submissions to this carrier.<br />
    24,Failed,Absent subscriber temporary,Temporary,Used when the message is undeliverable due to an incorrect / invalid / blacklisted / or temporarily barred MSISDN for this carrier.<br />
    25,Failed,Carrier network failure,Temporary/Permanent on InterOp/ACG/MetroPCS/Cricket,&quot;Used when the message has failed due to a temporary condition in the carrier network. This could be related to the SS7 layer, SMSC or gateway. This error code is not Retryable on some carriers.&quot;<br />
    26,Failed,Phone related error,Temporary,&quot;Used when a message has failed due to a temporary phone related error, e.g. SIM card full, SME busy, memory exceeded etc. This does not mean the phone is unable to receive this type of message/content (refer to error code 27).&quot;<br />
    27,Failed,Permanent phone related error,Permanent,Used when a handset is permanently incompatible or unable to receive this type of message (e.g. MMS compatibility).<br />
    29,Failed,Content or parameter related error,Permanent,Used when a specific content or parameter is missing or not permitted on the network/short code.<br />
    30,Failed,Subscriber spend limit exceeded,Temporary/(Permanent on T-Mobile),Used when message fails or is rejected because the subscriber has reached the predetermined spend limit for the current billing period.<br />
    31,Failed,Subscriber unable to be billed,Temporary(Permanent on AT&amp;T and T-Mobile),&quot;Used when the MSISDN is for a valid subscriber on the carrier but the message fails or is rejected because the subscriber is unable to be billed, e.g. the subscriber account is suspended (either voluntarily or involuntarily), the subscriber is not enabled for bill-to-phone services, the subscriber is not eligible for bill-to-phone services, etc.&quot;<br />
    33,Failed,Age verification failure,Permanent,End user failed age verification checks for the requested content.<br />
    37,Failed,Subscription or Purchase related failure/Duplicate request,Permanent,Used where an ownership agreement (Subscription) or an initial purchase does not exist.<br />
    38,Failed,Invalid subscription operation,Permanent,Used where the carrier platform supports subscription management and an operation has failed because it is invalid e.g. it is not supported on the particular subscription.<br />
    40,Failed,Message delivered - billing failed,Permanent,Used in the rare occurrence of a message being delivered but the billing event failing due to a temporary problem within the operator network.<br />
    41,Failed,Payment authorization Failure,Permanent,Carrier could not authorize payment for the transaction.<br />
    43,Failed,Message was rejected because the MSISDN is blacklisted,Permanent,MSISDN has been rejected because the carrier has blacklisted the number. Please REMOVE this number from database as it is a deactivated number and will not be billable. (implemented as part of carrier specific blacklisting programs)<br />
    46,Failed,Can't send message to Shortcode,Temporary,Can???t send message to Shortcode<br />
    48,Failed,Message was blocked because the content rating of the subscriber is lower than that of PSMS application,Permanent,&quot;Used specifically for Verizon Content Filtering, and applicable to PSMS. The client must cancel the subscriber???s subscription. No other action is required of client, since mBlox will respond to the subscriber with an informative free SMS MT on the client???s behalf&quot;<br />
    49,Failed,Service Denied for or by this subscriber,Permanent,&quot;Either the carrier or the end user has blocked third party content to this number. Applicable to both premium and standard rate programs. Client must cancel all of the subscriptions for the subscriber and remove the MSISDN from their database. No other action is required of client, since mBlox will respond to the subscriber with an informative free SMS MT on the client???s behalf.&quot;<br />
    50,Failed,Invalid WAP Push,Permanent,Used specifically for Verizon WAP Push messages. Returned when the WAP Push binary message could not be fully decoded by the mBlox platform because of one or more invalid elements.<br />
    51,Failed,Content could not be retrieved,Temporary,Used specifically for Verizon WAP Push messages. Returned when the client???s server could not be accessed to retrieve the content.<br />
    52,Failed,Content file not available,Permanent,Used specifically for Verizon WAP Push messages. Returned when the specific content file was not found on the client???s server. Typically this occurs where the content format requested for the specific mobile device is not supported.<br />
    53,Failed,Message was bloacked because the content rating of the subscriber is lower than that of the PSMS application,Permanent,&quot;The client must cancel the subscriber???s subscription. No other action is required of client, since mBlox will respond to the subscriber with an informative free SMS MT on the client???s behalf&quot;<br />
    54,Failed,Indeterminate Age Verification,Permanent,This transaction requires the end user???s age to be verified and the carrier could not perform that action.<br />
    55,Failed,Subscription failed because it was a duplicate subscription,Permanent,This code is returned after an attempt to set-up a duplicate subscription.<br />
    56,Failed,&quot;Duplicate purchase, opt-in re-sent&quot;,Permanent,The request is a duplicate of an active purchase request that has not yet been confirmed by the end-user. The opt-in has been re-sent to the subscriber. The client must not retry<br />
    59,Buffered,Message is delivered but not billed yet,Intermediate,The message was delivered but has not been billed yet. The client should not try to re-send the purchase request<br />
    70,Failed,Opt-in/purchase Expired,Permanent,This code is returned after an Opt-In request that was sent to the subscriber times out (12 hours). The client must not retry until the subscriber requests to Opt-In to the same campaign again<br />
    71,Failed,Message failed due to missing or invalid parameter,Permanent,The message is missing a key piece of data. Clients should add this missing piece of data and then retry the message. The message will fail if it???s simply retried as it is.<br />
    72,Failed,&quot;Message failed because of incorrect, missing or invalid carrier configuration settings or because the carrier took administrative action to delete the message&quot;,Permanent,The carrier or mBlox are not ready to process the message due to some missing/invalid configuration settings in the carrier network. This is also used if the carrier took manual actions to remove the message from their system for any reason.<br />
    75,Failed,Refund denied,Permanent,Used where the carrier platform supports refunds on subscription cancellation and the refund requestwas denied.<br />
    78,Failed,Invalid subscription operation,Temporary,Used where the carrier platform supports subscription management and an operation has failed but can be retried.<br />
    82,Failed,Subscription failed due to internal error,Temporary,Subscription failed due to internal system error<br />
    87,Failed,Indicates that the subscriber requested their account be bloacked from sending or receiving messages from premium short code,Permanent,Sprint Premium MDN shortcode blocking<br />
    88,Failed,Indicates that the subscriber requested their account be blocked from sending or receiving messages from non premium short code.,Permanent,Sprint Non-premium MDN shortcode blocking<br />
    89,Failed,Message failed because the subscriber has requested to block the Originating Shortcode from sending messages to the particular MDN,Permanent,Verizon Shortcode blocking by subscriber<br />
    120,Failed,Message failed because Subscriber has Shortcode blocked for Premium Transactions,Permanent,Billing charge rejected as the subscriber is blocked for premium short code<br />
121,Failed,&quot;Insufficient Funds, Subscription failed to bill&quot;,Permanent,Used where carrier requires clients to remove the subscription requiring the end user to re-opt in when funds are available</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Seta VN  Update 2013-07-29 alter simpleobjects.useraccount table</strong></p>

<p>
	ALTER TABLE `simpleobjects`.`hauinvitationcode` CHANGE COLUMN `HauInvitationCode` `HauInvitationCode_vch` VARCHAR(255) NOT NULL
, DROP PRIMARY KEY
, ADD PRIMARY KEY (`HauInvitationCode_vch`) ;	
</p>

<p>
	ALTER TABLE `simpleobjects`.`useraccount` CHANGE COLUMN `InviteId_int` `HauInvitationCode_vch` VARCHAR(255) NULL DEFAULT NULL  ;
</p>

<br/>
<p><strong>Seta VN  Update 2013-07-26 alter simplexresults.conversationmessage and simpleobjects.useraccount table</strong></p>
<p>
	ALTER TABLE `simplexresults`.`conversationmessage` ADD COLUMN `SessionCode_vch` VARCHAR(255) NULL DEFAULT NULL  AFTER `EndTime_dt` ;
</p>

<p>	
	ALTER TABLE `simpleobjects`.`useraccount` ADD COLUMN `InviteId_int` INT NULL DEFAULT NULL  AFTER `DefaultCID_vch` ;
</p>

<p><strong>Seta VN  Update 2013-07-25 create hauinvitationcode table</strong></p>
<p>
	CREATE  TABLE `simpleobjects`.`hauinvitationcode` (
  `HauInvitationCode` VARCHAR(255) NOT NULL ,
  `Created_dt` DATETIME NOT NULL ,
  `InviteUserId_int` INT NOT NULL ,
  `EmailAddress_vch` VARCHAR(255) NOT NULL ,
  `Status_ti` TINYINT NOT NULL ,
  `BatchId_bi` BIGINT NOT NULL,
  `UserTypeId_ti` TINYINT NOT NULL, 
  PRIMARY KEY (`HauInvitationCode`) );	
</p>

<p><strong>Seta VN  Update 2013-07-22 alter primary key for conversationcontactresult </strong></p>
	<p>ALTER TABLE `simplexresults`.`conversationcontactresult` CHANGE COLUMN `SessionId_bi` `SessionId_bi` BIGINT(20) NULL  , CHANGE COLUMN `MasterRXCallDetailId_int` `MasterRXCallDetailId_int` INT(11) NULL  
, ADD PRIMARY KEY (`SessionId_bi`, `MasterRXCallDetailId_int`) ;</p>

<p><strong>JP Update - 2013-07-21 - need to expand limit of XMLControlString_vch</strong><br />
  ALTER TABLE `simpleobjects`.`batch` CHANGE COLUMN `XMLControlString_vch` `XMLControlString_vch` LONGTEXT NULL DEFAULT NULL  ;<br />
  ALTER TABLE `simpleobjects`.`history` CHANGE COLUMN `XMLControlString_vch` `XMLControlString_vch` LONGTEXT NOT NULL  ;<br />
ALTER TABLE `simplexresults`.`contactresults` CHANGE COLUMN `XMLControlString_vch` `XMLControlString_vch` LONGTEXT NULL DEFAULT NULL  ;<br />
ALTER TABLE `simplequeue`.`contactqueue` CHANGE COLUMN `XMLControlString_vch` `XMLControlString_vch` LONGTEXT NULL DEFAULT NULL  ;<br />
Anywhere else I might be forgeting? Watch out for it.<br />
See http://coldfusion9.blogspot.com/2010/05/enable-long-text-retrieval-in.html
</p>
<p>&nbsp;</p>
<p><strong>Seta VN  Update 2013-07-19 - create table hausettings and add default value</strong></p>
<br/>
<p>
	CREATE  TABLE `simpleobjects`.`hausettings` (

  `HauSettingId_int` INT NOT NULL AUTO_INCREMENT ,

  `AutoRouteIncomingRequest_bt` BIT NULL ,

  `AllowSesstionTransfer_bt` BIT NOT NULL ,

  `MaximumSessionsPerAgent_bt` BIT NOT NULL ,

  `MaximumSessionsPerAgent_int` INT NULL ,

  `AllowCannedResponses_bt` BIT NOT NULL ,

  `AllowAgnetCreatedCannedResponses_bt` BIT NOT NULL ,

  `AutoRouteReturningRequest_bt` BIT NOT NULL ,

  PRIMARY KEY (`HauSettingId_int`) );
</p>
<br/>
<p>
  INSERT INTO `simpleobjects`.`hausettings` (`AutoRouteIncomingRequest_bt`, `AllowSesstionTransfer_bt`, `MaximumSessionsPerAgent_bt`, `MaximumSessionsPerAgent_int`, `AllowCannedResponses_bt`, `AllowAgnetCreatedCannedResponses_bt`, `AutoRouteReturningRequest_bt`) VALUES (0, 0, 1, '3', 1, 0, 0);
</p>

<p><strong>Seta VN  Update 2013-07-18 - alter table conversationmessage and create ConversationContactResult</strong></p>
<p>ALTER TABLE `simplexresults`.`conversationmessage` ADD COLUMN `StartTime_dt` DATETIME NOT NULL  AFTER `ContactString_vch` , ADD COLUMN `EndTime_dt` DATETIME NULL  AFTER `StartTime_dt` ;<br />
</p>
<br/>

<p>
	CREATE  TABLE `simpleobjects`.`products` (
  `Product_bi` BIGINT NOT NULL ,
  `Name_vch` VARCHAR(255) NOT NULL ,
  `SKU_vch` VARCHAR(245) NOT NULL ,
  `Description_vch` VARCHAR(1500) NULL ,
  `Aisle_vch` VARCHAR(255) NULL ,
  `Side_vch` VARCHAR(255) NULL ,
  `XY_vch` VARCHAR(255) NULL ,
  `Created_dt` DATETIME NULL ,
  PRIMARY KEY (`Product_bi`) );
ALTER TABLE `simpleobjects`.`products` CHANGE COLUMN `Product_bi` `Product_bi` BIGINT(20) NOT NULL AUTO_INCREMENT  ;
ALTER TABLE `simpleobjects`.`products` CHANGE COLUMN `Product_bi` `ProductId_bi` BIGINT(20) NOT NULL AUTO_INCREMENT 

, DROP PRIMARY KEY 
, ADD PRIMARY KEY (`ProductId_bi`) ;	
	
</p>



<p>
CREATE  TABLE `simplexresults`.`conversationcontactresult` (
  `SessionId_bi` BIGINT NOT NULL ,
  `MasterRXCallDetailId_int` INT NOT NULL );	
<br/>
</p>
<p><strong>JLP Update 2013-07-16 - add index</strong></p>
<p>ALTER TABLE `simpleobjects`.`history`add INDEX `IDX_Batchid` (`BatchId_bi` ASC) ;<br />
</p>
<p><strong>SETA Update on 2013-07-12 - creat table</strong></p>
<p>CREATE  TABLE `simplexresults`.`conversationmessage` (
  `SessionId_bi` BIGINT NOT NULL AUTO_INCREMENT ,
  `ShortCode_vch` VARCHAR(255) NOT NULL ,
  `OwnerId_int` INT NOT NULL ,
  `ContactString_vch` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`SessionId_bi`) );
<p>
<p><strong>JLP Update 2013-07-11 - SMS Rename column</strong>
<p>ALTER TABLE `simplequeue`.`moinboundqueue` CHANGE COLUMN `QuestionTimeOutRQ_int` `QuestionTimeOutQID_int` INT(11) NULL DEFAULT '0'  ;</p>
<p>&nbsp;</p>
<p><strong>JLP Updatye 2013-07-10 - SMS Interactive Mods</strong></p>
<p> ALTER TABLE `simplequeue`.`moinboundqueue` ADD COLUMN `QuestionTimeOutRQ_int` INT NULL DEFAULT 0  AFTER `TransactionId_vch` ;<br />
</p>
<p><strong>JLP Update 2013-07-08 - SMS Opt out tracking</strong><br />
    <br />
    CREATE TABLE `smsstoprequests` (<br />
    `ShortCode_vch` varchar(255) NOT NULL,<br />
    `ContactString_vch` varchar(200) NOT NULL,<br />
    `Created_dt` datetime NOT NULL,<br />
    PRIMARY KEY (`ShortCode_vch`,`ContactString_vch`,`Created_dt`)<br />
) ENGINE=InnoDB DEFAULT</p>
<p>ALTER TABLE `simplequeue`.`contactqueue` ADD COLUMN `ShortCode_vch` VARCHAR(255) NULL DEFAULT NULL  AFTER `LastUpdated_dt` <br />
, ADD INDEX `IDX_SMSSTOP` (`ShortCode_vch` ASC, `TypeMask_ti` ASC, `ContactString_vch` ASC, `DTSStatusType_ti` ASC) ;</p>
<p>&nbsp;</p>
<p><strong>Seta VN Update 2013-07-05 - CREATE  TABLE CONTACTTERMINATE</strong><br />
    CREATE  TABLE `simplexresults`.`contactterminate` (`ContactTerminateId_int` INT NOT NULL AUTO_INCREMENT ,`ContactString_vch` VARCHAR(255) NOT NULL, `ShortCode_vch` VARCHAR(255) NOT NULL,  `BatchID_bi` BIGINT NOT NULL , `Terminated_bit` BIT NOT NULL DEFAULT 0 , PRIMARY KEY (`ContactTerminateId_int`) );
</p>


<p><strong>JLP Update 2013-06-30 - SMS Queue processing</strong><br />
  ALTER TABLE `simplequeue`.`moinboundqueue` ADD INDEX `IDX_ANALYTICS` (`ContactString_vch` ASC, `Status_ti` ASC, `ShortCode_vch` ASC) ;<br />
</p>
<p><strong>JLP Update 2013-06-29 - MBLOX Tracking</strong><br />
  CREATE  TABLE `sms`.`MBLOXTracking` ( `MBLOXId_bi` BIGINT NOT NULL AUTO_INCREMENT , `Created_dt` DATETIME NULL , PRIMARY KEY (`MBLOXId_bi`) ;</p>
<p>ALTER TABLE `simplexresults`.`contactresults` ADD COLUMN `SMSTrackingOne_bi` BIGINT NULL DEFAULT 0  AFTER `SMSCSC_vch` , ADD COLUMN `SMSTrackingTwo_bi` BIGINT NULL DEFAULT 0  AFTER `SMSTrackingOne_bi` ;</p>
<p>&nbsp;</p>
<p><strong>JLP Update on 2013-06-28 - SMS Flow Updates</strong><br />
    ALTER TABLE `simplequeue`.`moinboundqueue` ADD COLUMN `Scheduled_dt` DATETIME NOT NULL  AFTER `Time_dt` <br />
    , ADD INDEX `IDX_ProcessQ` (`Status_ti` ASC, `Scheduled_dt` ASC) ;</p>
<p>ALTER TABLE `simplexresults`.`contactresults` ADD COLUMN `SMSSequence_ti` TINYINT NULL DEFAULT 0  AFTER `SMSSurveyState_int` ;</p>
<p>&nbsp;</p>
<p><strong>SETA Update on 2013-06-25- Add new reporting column</strong></p>
ALTER TABLE `simpleobjects`.`userreportingpref` ADD COLUMN `ReportXML_vch` TEXT NULL  AFTER `DateUpdated_dt`; <br/>


<p><strong>SETA Update on 2013-06-20 - Rename column</strong></p>
 ALTER TABLE `simplequeue`.`moinboundqueue` CHANGE COLUMN `CarierId_vch` `CarrierId_vch` VARCHAR(255) NOT NULL;<br/>
 
<p><br />
ALTER TABLE `sms`.`shortcoderequest` ADD COLUMN `Proc
