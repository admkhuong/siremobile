CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: 10.26.0.40    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'simplelists'
--
/*!50003 DROP FUNCTION IF EXISTS `regex_replace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 FUNCTION `regex_replace`(pattern VARCHAR(1000),replacement VARCHAR(1000),original VARCHAR(1000)) RETURNS varchar(1000) CHARSET latin1
    DETERMINISTIC
BEGIN 
 DECLARE temp VARCHAR(1000); 
 DECLARE ch VARCHAR(1); 
 DECLARE i INT;
 SET i = 1;
 SET temp = '';
 IF original REGEXP pattern THEN 
  loop_label: LOOP 
   IF i>CHAR_LENGTH(original) THEN
    LEAVE loop_label;  
   END IF;
   SET ch = SUBSTRING(original,i,1);
   IF NOT ch REGEXP pattern THEN
    SET temp = CONCAT(temp,ch);
   ELSE
    SET temp = CONCAT(temp,replacement);
   END IF;
   SET i=i+1;
  END LOOP;
 ELSE
  SET temp = original;
 END IF;
 RETURN temp;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltemail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltemail`(
					IN ea Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @ea = ea;
					SET @AltNum = altnum;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @EDupFlag = CONCAT('EaltDupFlag',ea);
					SET @EFlag = CONCAT('EaltFlag',ea);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT SimpleLists.ContactList.ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltemailpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltemailpre`(
					IN ea Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11)
					)
BEGIN
					
					SET @ea = ea;
					SET @AltNum = altnum;
					set @datatablename = datatablename;
					SET @EDupFlag = CONCAT('EaltDupFlag',ea);
					SET @EFlag = CONCAT('EaltFlag',ea);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltfax` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltfax`(
                    IN fa Varchar(50),
					IN altnum INTEGER(11),
                    IN datatablename Varchar(50),
                    IN Optin_int INTEGER(11),
                    IN DoubleOptin_int INTEGER(11),
                    IN ContactOptin_int INTEGER(11),
                    IN UserId INTEGER(11)
                    )
BEGIN
                    
                    SET @fa = fa;
					SET @AltNum = altnum;
                    SET @FTimeZone = CONCAT('FaltTimeZone',fa);
                    SET @FCity = CONCAT('FaltCity',fa);
                    SET @FState = CONCAT('FaltState',fa);
                    SET @FCellFlag = CONCAT('FaltCellFlag',fa);
                    set @datatablename = datatablename;
                    SET @UserId = UserId;
                    SET @FDupFlag = CONCAT('FaltDupFlag',fa);
                    SET @FFlag = CONCAT('FaltFlag',fa);
                    SET @Optin = Optin_int;
                    SET @DoubleOptin = DoubleOptin_int;
                    SET @ContactOptin = ContactOptin_int;
                    
                    SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT SimpleLists.ContactList.ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
                    
                    PREPARE stmt FROM @sql_text;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                    
                    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltfaxpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltfaxpre`(
					IN fa Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @fa = fa;
					SET @AltNum = altnum;
                    SET @FTimeZone = CONCAT('FaltTimeZone',fa);
                    SET @FCity = CONCAT('FaltCity',fa);
                    SET @FState = CONCAT('FaltState',fa);
                    SET @FCellFlag = CONCAT('FaltCellFlag',fa);
                    set @datatablename = datatablename;
                    SET @UserId = UserId;
                    SET @FDupFlag = CONCAT('FaltDupFlag',fa);
                    SET @FFlag = CONCAT('FaltFlag',fa);
                    SET @Optin = Optin_int;
                    SET @DoubleOptin = DoubleOptin_int;
                    SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltmobile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltmobile`(
IN ma Varchar(50),
IN altnum INTEGER(11),
IN datatablename Varchar(50),
IN Optin_int INTEGER(11),
IN DoubleOptin_int INTEGER(11),
IN ContactOptin_int INTEGER(11),
IN UserId INTEGER(11)
)
BEGIN
 
SET @ma = ma;
SET @AltNum = altnum;
SET @MTimeZone = CONCAT('MaltTimeZone',ma);
SET @MCity = CONCAT('MaltCity',ma);
SET @MState = CONCAT('MaltState',ma);
SET @MCellFlag = CONCAT('MaltCellFlag',ma);
set @datatablename = datatablename;
SET @UserId = UserId;
SET @MDupFlag = CONCAT('MaltDupFlag',ma);
SET @MFlag = CONCAT('MaltFlag',ma);
SET @Optin = Optin_int;
SET @DoubleOptin = DoubleOptin_int;
SET @ContactOptin = ContactOptin_int;
 
SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT SimpleLists.ContactList.ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
 
PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltmobilepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltmobilepre`(
IN ma Varchar(50),
IN altnum INTEGER(11),
IN datatablename Varchar(50),
IN Optin_int INTEGER(11),
IN DoubleOptin_int INTEGER(11),
IN ContactOptin_int INTEGER(11),
IN UserId INTEGER(11)
)
BEGIN

SET @ma = ma;
SET @AltNum = altnum;
SET @MTimeZone = CONCAT('MaltTimeZone',ma);
SET @MCity = CONCAT('MaltCity',ma);
SET @MState = CONCAT('MaltState',ma);
SET @MCellFlag = CONCAT('MaltCellFlag',ma);
set @datatablename = datatablename;
SET @UserId = UserId;
SET @MDupFlag = CONCAT('MaltDupFlag',ma);
SET @MFlag = CONCAT('MaltFlag',ma);
SET @Optin = Optin_int;
SET @DoubleOptin = DoubleOptin_int;
SET @ContactOptin = ContactOptin_int;

SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0');

PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltvoice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltvoice`(
					IN va Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @va = va;
					SET @AltNum = altnum;
					SET @LTimeZone = CONCAT('LaltTimeZone',va);
					SET @LCity = CONCAT('LaltCity',va);
					SET @LState = CONCAT('LaltState',va);
					SET @LCellFlag = CONCAT('LaltCellFlag',va);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @LDupFlag = CONCAT('LaltDupFlag',va);
					SET @LFlag = CONCAT('LaltFlag',va);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT SimpleLists.ContactList.ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addaltvoicepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addaltvoicepre`(
					IN va Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @va = va;
					SET @AltNum = altnum;
					SET @LTimeZone = CONCAT('LaltTimeZone',va);
					SET @LCity = CONCAT('LaltCity',va);
					SET @LState = CONCAT('LaltState',va);
					SET @LCellFlag = CONCAT('LaltCellFlag',va);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @LDupFlag = CONCAT('LaltDupFlag',va);
					SET @LFlag = CONCAT('LaltFlag',va);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int,AltNum_int)SELECT ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,',',@AltNum,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addcustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addcustomer`(
		IN datatablename Varchar(50),
		IN Identifier Varchar(50),
		IN Company Varchar(500),
		IN Fname Varchar(90),
		IN LName Varchar(90),
		IN Address Varchar(250),
		IN Suite Varchar(100),
		IN City Varchar(100),
		IN State Varchar(50),
		IN ZipCode Varchar(12),
		IN Country Varchar(100),
		IN UserName Varchar(300),
		IN Password Varchar(300),
		IN UserId INTEGER(11)
		)
BEGIN
		
		SET @DataStr = CONCAT('UserId_int,DataTrack_vch');
		SET @vDataStr = CONCAT(UserId,',DataTrack_vch');
		
		IF LENGTH(Identifier) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',UserDefinedKey_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Identifier);
		END IF;
		IF LENGTH(Company) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Company_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Company);
		END IF;
		IF LENGTH(Fname) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',FirstName_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Fname);
		END IF;
		IF LENGTH(LName) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',LastName_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',LName);
		END IF;
		IF LENGTH(Address) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Address_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Address);
		END IF;
		IF LENGTH(Suite) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Address1_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Suite);
		END IF;
		IF LENGTH(City) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',City_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',City);
		END IF;
		IF LENGTH(State) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',State_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',State);
		END IF;
		IF LENGTH(ZipCode) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',ZipCode_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',ZipCode);
		END IF;
		IF LENGTH(Country) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Country_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Country);
		END IF;
		IF LENGTH(UserName) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',UserName_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',UserName);
		END IF;
		IF LENGTH(Password) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Password_vch');
			SET @vDataStr = CONCAT(@vDataStr,',',Password);
		END IF;
		
		SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactList(',@DataStr,') SELECT 
		',@vDataStr,' FROM SimpleXUploadStage.',datatablename,' WHERE ContactPreExist_int = 0');
		
		PREPARE stmt FROM @sql_text;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		 
		END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addemail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addemail`(
IN ea Varchar(50),
IN datatablename Varchar(50),
IN Optin_int INTEGER(11),
IN DoubleOptin_int INTEGER(11),
IN ContactOptin_int INTEGER(11),
IN UserId INTEGER(11)
)
BEGIN

SET @ea = ea;
set @datatablename = datatablename;
SET @UserId = UserId;
SET @EDupFlag = CONCAT('EDupFlag',ea);
SET @EFlag = CONCAT('EFlag',ea);
SET @Optin = Optin_int;
SET @DoubleOptin = DoubleOptin_int;
SET @ContactOptin = ContactOptin_int;

SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT SimpleLists.ContactList.ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);

PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addemailpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addemailpre`(
IN ea Varchar(50),
IN datatablename Varchar(50),
IN Optin_int INTEGER(11),
IN DoubleOptin_int INTEGER(11),
IN ContactOptin_int INTEGER(11),
IN UserId INTEGER(11)
)
BEGIN

SET @ea = ea;
set @datatablename = datatablename;
SET @UserId = UserId;
SET @EDupFlag = CONCAT('EDupFlag',ea);
SET @EFlag = CONCAT('EFlag',ea);
SET @Optin = Optin_int;
SET @DoubleOptin = DoubleOptin_int;
SET @ContactOptin = ContactOptin_int;

SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT ContactId_bi,',@ea,',2,',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@EFlag,' = 0 and ',@EDupFlag,' = 0');

PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addfax` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addfax`(
                    IN fa Varchar(50),
                    IN datatablename Varchar(50),
                    IN Optin_int INTEGER(11),
                    IN DoubleOptin_int INTEGER(11),
                    IN ContactOptin_int INTEGER(11),
                    IN UserId INTEGER(11)
                    )
BEGIN
                    
                    SET @fa = fa;
                    SET @FTimeZone = CONCAT('FTimeZone',fa);
                    SET @FCity = CONCAT('FCity',fa);
                    SET @FState = CONCAT('FState',fa);
                    SET @FCellFlag = CONCAT('FCellFlag',fa);
                    set @datatablename = datatablename;
                    SET @UserId = UserId;
                    SET @FDupFlag = CONCAT('FDupFlag',fa);
                    SET @FFlag = CONCAT('FFlag',fa);
                    SET @Optin = Optin_int;
                    SET @DoubleOptin = DoubleOptin_int;
                    SET @ContactOptin = ContactOptin_int;
                    
                    SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT SimpleLists.ContactList.ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
                    
                    PREPARE stmt FROM @sql_text;
                    EXECUTE stmt;
                    DEALLOCATE PREPARE stmt;
                    
                    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addfaxpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addfaxpre`(
					IN fa Varchar(50),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @fa = fa;
                    SET @FTimeZone = CONCAT('FTimeZone',fa);
                    SET @FCity = CONCAT('FCity',fa);
                    SET @FState = CONCAT('FState',fa);
                    SET @FCellFlag = CONCAT('FCellFlag',fa);
                    set @datatablename = datatablename;
                    SET @UserId = UserId;
                    SET @FDupFlag = CONCAT('FDupFlag',fa);
                    SET @FFlag = CONCAT('FFlag',fa);
                    SET @Optin = Optin_int;
                    SET @DoubleOptin = DoubleOptin_int;
                    SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT ContactId_bi,',@fa,',4,',@FTimeZone,',',@FCity,',',@FState,',',@FCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@FFlag,' = 0 and ',@FDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addmobile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addmobile`(
					IN ma Varchar(50),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @ma = ma;
					SET @MTimeZone = CONCAT('MTimeZone',ma);
					SET @MCity = CONCAT('MCity',ma);
					SET @MState = CONCAT('MState',ma);
					SET @MCellFlag = CONCAT('MCellFlag',ma);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @MDupFlag = CONCAT('MDupFlag',ma);
					SET @MFlag = CONCAT('MFlag',ma);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					 
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT SimpleLists.ContactList.ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addmobilepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addmobilepre`(
					IN ma Varchar(50),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @ma = ma;
					SET @MTimeZone = CONCAT('MTimeZone',ma);
					SET @MCity = CONCAT('MCity',ma);
					SET @MState = CONCAT('MState',ma);
					SET @MCellFlag = CONCAT('MCellFlag',ma);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @MDupFlag = CONCAT('MDupFlag',ma);
					SET @MFlag = CONCAT('MFlag',ma);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT ContactId_bi,',@ma,',3,',@MTimeZone,',',@MCity,',',@MState,',',@MCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@MFlag,' = 0 and ',@MDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addtogroup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addtogroup`(
IN DataTrack Varchar(300),
IN currentlist INTEGER(11),
IN UserId INTEGER(11)
)
BEGIN

SET @sql_text = CONCAT('INSERT INTO simplelists.groupcontactlist(ContactAddressId_bi,GroupId_bi) SELECT SimpleLists.contactstring.ContactAddressId_bi,',currentlist,' FROM SimpleLists.contactstring inner join SimpleLists.contactlist on SimpleLists.contactstring.ContactId_bi = SimpleLists.contactlist.ContactId_bi WHERE left(SimpleLists.contactlist.DataTrack_vch,12) = ',DataTrack,' and SimpleLists.contactlist.UserId_int = ',UserId,' and SimpleLists.contactstring.ContactAddressId_bi not in (Select ContactAddressId_bi from simplelists.groupcontactlist where GroupId_bi = ',currentlist,')');

PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addUDvars` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addUDvars`(
					IN thisvarname Varchar(300),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @varname = thisvarname;
					SET @TVarName = CONCAT('"',thisvarname,'"');
					set @datatablename = datatablename;
					SET @UserId = UserId;
					
					SET @sql_text = CONCAT('INSERT INTO simplelists.contactvariable(ContactId_bi,VariableName_vch,VariableValue_vch) SELECT SimpleLists.ContactList.ContactId_bi,',@TVarName,',SimpleXUploadStage.',@datatablename,'.',@varname,' FROM	SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE SimpleLists.ContactList.UserId_int = ',@UserId);
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addvoice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addvoice`(
					IN va Varchar(50),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @va = va;
					SET @LTimeZone = CONCAT('LTimeZone',va);
					SET @LCity = CONCAT('LCity',va);
					SET @LState = CONCAT('LState',va);
					SET @LCellFlag = CONCAT('LCellFlag',va);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @LDupFlag = CONCAT('LDupFlag',va);
					SET @LFlag = CONCAT('LFlag',va);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT SimpleLists.ContactList.ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' inner join SimpleLists.ContactList on SimpleXUploadStage.',@datatablename,'.DataTrack_vch = SimpleLists.ContactList.DataTrack_vch WHERE ContactPreExist_int = 0 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0 and SimpleLists.ContactList.UserId_int = ',@UserId);
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addvoicepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `addvoicepre`(
					IN va Varchar(50),
					IN datatablename Varchar(50),
					IN Optin_int INTEGER(11),
					IN DoubleOptin_int INTEGER(11),
					IN ContactOptin_int INTEGER(11),
					IN UserId INTEGER(11)
					)
BEGIN
					
					SET @va = va;
					SET @LTimeZone = CONCAT('LTimeZone',va);
					SET @LCity = CONCAT('LCity',va);
					SET @LState = CONCAT('LState',va);
					SET @LCellFlag = CONCAT('LCellFlag',va);
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @LDupFlag = CONCAT('LDupFlag',va);
					SET @LFlag = CONCAT('LFlag',va);
					SET @Optin = Optin_int;
					SET @DoubleOptin = DoubleOptin_int;
					SET @ContactOptin = ContactOptin_int;
					
					SET @sql_text = CONCAT('INSERT INTO SimpleLists.ContactString(ContactId_bi,ContactString_vch,ContactType_int,TimeZone_int,City_vch,State_vch,CellFlag_int,OptIn_int,DoubleOptin_int,ContactOptin_int)SELECT ContactId_bi,',@va,',1,',@LTimeZone,',',@LCity,',',@LState,',',@LCellFlag,',',@Optin,',',@DoubleOptin,',',@ContactOptin,' FROM SimpleXUploadStage.',@datatablename,' WHERE ContactPreExist_int = 1 and ',@LFlag,' = 0 and ',@LDupFlag,' = 0');
					
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upaltemailpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upaltemailpre`(
					IN ea Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @ea = ea;
					SET @AltNum = altnum;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @EDupFlag = CONCAT('EaltDupFlag',ea);
					SET @EFlag = CONCAT('EaltFlag',ea);
					
					SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@ea,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@EFlag,' = 2 and ntd.',@EDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 2');
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upaltfaxpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upaltfaxpre`(
					IN fa Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @fa = fa;
					SET @AltNum = altnum;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @FDupFlag = CONCAT('FaltDupFlag',fa);
					SET @FFlag = CONCAT('FaltFlag',fa);
					
					SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@fa,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@FFlag,' = 2 and ntd.',@FDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 4');
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upaltmobilepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upaltmobilepre`(
IN ma Varchar(50),
IN altnum INTEGER(11),
IN datatablename Varchar(50),
IN UserId INTEGER(11)
)
BEGIN
 
SET @ma = ma;
SET @AltNum = altnum;
set @datatablename = datatablename;
SET @UserId = UserId;
SET @MDupFlag = CONCAT('MaltDupFlag',ma);
SET @MFlag = CONCAT('MaltFlag',ma);

SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@ma,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@MFlag,' = 2 and ntd.',@MDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 3');
 
PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upaltvoicepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upaltvoicepre`(
					IN va Varchar(50),
					IN altnum INTEGER(11),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @va = va;
					SET @AltNum = altnum;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @LDupFlag = CONCAT('LaltDupFlag',va);
					SET @LFlag = CONCAT('LaltFlag',va);
					
					SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@va,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = ',@AltNum,' WHERE	ntd.',@LFlag,' = 2 and ntd.',@LDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 1');
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatecustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `updatecustomer`(
		IN datatablename Varchar(50),
		IN Identifier Varchar(50),
		IN Company Varchar(500),
		IN Fname Varchar(90),
		IN LName Varchar(90),
		IN Address Varchar(250),
		IN Suite Varchar(100),
		IN City Varchar(100),
		IN State Varchar(50),
		IN ZipCode Varchar(12),
		IN Country Varchar(100),
		IN UserName Varchar(300),
		IN Password Varchar(300),
		IN UserId INTEGER(11)
		)
BEGIN
		
		SET @DataStr = CONCAT('SimpleLists.ContactList.DataTrack_vch = SimpleXUploadStage.',datatablename,'.DataTrack_vch');
		
		IF LENGTH(Identifier) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',UserDefinedKey_vch = ',Identifier);
		END IF;
		IF LENGTH(Company) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Company_vch = ',Company);
		END IF;
		IF LENGTH(Fname) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',FirstName_vch = ',Fname);
		END IF;
		IF LENGTH(LName) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',LastName_vch = ',LName);
		END IF;
		IF LENGTH(Address) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Address_vch = ',Address);
		END IF;
		IF LENGTH(Suite) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Address1_vch = ',Suite);
		END IF;
		IF LENGTH(City) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',City_vch = ',City);
		END IF;
		IF LENGTH(State) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',State_vch = ',State);
		END IF;
		IF LENGTH(ZipCode) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',ZipCode_vch = ',ZipCode);
		END IF;
		IF LENGTH(Country) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Country_vch = ',Country);
		END IF;
		IF LENGTH(UserName) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',UserName_vch = ',UserName);
		END IF;
		IF LENGTH(Password) > 0 THEN
			SET @DataStr = CONCAT(@DataStr,',Password_vch = ',Password);
		END IF;
		
		
		SET @sql_text = CONCAT('UPDATE SimpleLists.ContactList join SimpleXUploadStage.',datatablename,' on SimpleLists.ContactList.ContactId_bi = SimpleXUploadStage.',datatablename,'.ContactId_bi SET ',@DataStr,' WHERE SimpleXUploadStage.',datatablename,'.ContactPreExist_int = 1');
		
		
		PREPARE stmt FROM @sql_text;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		 
		END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upemailpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upemailpre`(
					IN ea Varchar(50),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @ea = ea;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @EDupFlag = CONCAT('EDupFlag',ea);
					SET @EFlag = CONCAT('EFlag',ea);
					
					SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@ea,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@EFlag,' = 2 and ntd.',@EDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 2');
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upfaxpre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upfaxpre`(
					IN fa Varchar(50),
					IN datatablename Varchar(50),
					IN UserId INTEGER(11)
					)
BEGIN
					 
					SET @fa = fa;
					set @datatablename = datatablename;
					SET @UserId = UserId;
					SET @FDupFlag = CONCAT('FDupFlag',fa);
					SET @FFlag = CONCAT('FFlag',fa);
					
					SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@fa,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@FFlag,' = 2 and ntd.',@FDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 4');
					 
					PREPARE stmt FROM @sql_text;
					EXECUTE stmt;
					DEALLOCATE PREPARE stmt;
					 
					END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upmobilepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upmobilepre`(
IN ma Varchar(50),
IN datatablename Varchar(50),
IN UserId INTEGER(11)
)
BEGIN
 
SET @ma = ma;
set @datatablename = datatablename;
SET @UserId = UserId;
SET @MDupFlag = CONCAT('MDupFlag',ma);
SET @MFlag = CONCAT('MFlag',ma);

SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@ma,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@MFlag,' = 2 and ntd.',@MDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 3');
 
PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upvoicepre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`EBMSQLAdmin`@`%`*/ /*!50003 PROCEDURE `upvoicepre`(
IN va Varchar(50),
IN datatablename Varchar(50),
IN UserId INTEGER(11)
)
BEGIN
 
SET @va = va;
set @datatablename = datatablename;
SET @UserId = UserId;
SET @LDupFlag = CONCAT('LDupFlag',va);
SET @LFlag = CONCAT('LFlag',va);

SET @sql_text = CONCAT('UPDATE SimpleXUploadStage.',@datatablename,' as ntd INNER JOIN SimpleLists.ContactString as otd ON (ntd.',@va,' = otd.ContactString_vch) INNER JOIN SimpleLists.ContactList as otcl ON (otd.ContactId_bi = otcl.ContactId_bi) SET otd.AltNum_int = 0 WHERE	ntd.',@LFlag,' = 2 and ntd.',@LDupFlag,' = 0 and otcl.UserId_int = ',@UserId,' and otd.ContactType_int = 1');
 
PREPARE stmt FROM @sql_text;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-09 13:35:42
