# Config MB notification server

---

System: Ubuntu 12.04 x64

All commands run as root.

## Add PPA for latest Node.js version

`apt-get install python-software-properties`

`add-apt-repository ppa:chris-lea/node.js`

`apt-get update`

`apt-get install nodejs`

### Check Node.js version
`node -v`

`npm -v`


## Config Notification Service
### Install Forever Globally
[Forever](https://github.com/nodejitsu/forever) is a useful tool for running a Node.js process with monitoring; it can be set to restart a failed process, and has a few other helpful features along the same lines. In this post you'll find a couple of exceedingly simple scripts for running a Node.js process as a service on Ubuntu using Forever.

`npm –g install forever`
### Make upstart script to start Node.js application on system startup
[Upstart](http://upstart.ubuntu.com/) is an alternative to using standard issue init scripts and is installed by default on Ubuntu. A script resides in the /etc/init folder and has a .conf file extension, e.g. /etc/init/mb-notification.conf.

Most upstart scripts are set to automatically start and stop for specific run levels in the same way as init scripts. Here is an example Upstart script for a Node.js service running under Forever:

`vim /etc/init/mb-notification.conf`
#### Paste this content below and change env variables by your source path:
```
#!upstart

description "MB Notification Server Upstart Script"

start on startup
stop on shutdown

expect fork

env NODE_BIN_DIR="/usr/lib/node_modules/forever/bin"
env NODE_PATH="/usr/lib/node_modules"

# Change this source path
env APPLICATION_DIRECTORY="/srv/nodeapp/mb"

# Change this file if needed
env APPLICATION_START="server.js"

# Change this file if needed
env LOG="/var/log/mb-notification.log"

script
    PATH=$NODE_BIN_DIR:$PATH
    exec forever --sourceDir $APPLICATION_DIRECTORY -a -l $LOG \
         --minUptime 5000 --spinSleepTime 2000 start $APPLICATION_START
end script

pre-stop script
    PATH=$NODE_BIN_DIR:$PATH
    exec forever stop $APPLICATION_START >> $LOG
end script
```

#### Note: Please change three variables to your path
```
env APPLICATION_DIRECTORY="/srv/nodeapp/mb"
env APPLICATION_START="server.js"
env LOG="/var/log/mb-notification.log"
```

### Make init script

`touch /etc/init.d/mb-notification`

`chmod a+x /etc/init.d/mb-notification`

`vim /etc/init.d/mb-notification`


#### Paste this content below and change env variables by your source path:
```
#!/bin/bash

NAME="MB Notification Server"
NODE_BIN_DIR="/usr/lib/node_modules/forever/bin"
NODE_PATH="/usr/lib/node_modules/"

# Change this source path
APPLICATION_DIRECTORY="/srv/nodeapp/mb"

 # Change this file if needed
APPLICATION_START="server.js"

# Change this file if needed
PIDFILE=/var/run/mb-notification.pid

# Change this file if needed
LOGFILE=/var/log/mb-notification.log

PATH=$NODE_BIN_DIR:$PATH
export NODE_PATH=$NODE_PATH

start() {
    echo "Starting $NAME"
    forever --pidFile $PIDFILE --sourceDir $APPLICATION_DIRECTORY \
        -a -l $LOGFILE --minUptime 5000 --spinSleepTime 2000 \
        start $APPLICATION_START &
    RETVAL=$?
}

stop() {
    if [ -f $PIDFILE ]; then
        echo "Shutting down $NAME"
        forever stop $APPLICATION_START
        rm -f $PIDFILE
        RETVAL=$?
    else
        echo "$NAME is not running."
        RETVAL=0
    fi
}

restart() {
    echo "Restarting $NAME"
    stop
    start
}

status() {
    echo "Status for $NAME:"
    forever list
    RETVAL=$?
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        restart
        ;;
    *)
        echo "Usage: {start|stop|status|restart}"
        exit 1
        ;;
esac
exit $RETVAL
```

#### Note: Please change four variables to your path
```
APPLICATION_DIRECTORY="/srv/nodeapp/mb"
APPLICATION_START="server.js"
PIDFILE="/var/run/mb-notification.pid"
LOGFILE="/var/log/mb-notification.log"
```

### Update the system service definitions

`update-rc.d mb-notification defaults`

### Commands to handle service:
```
service mb-notification start
service mb-notification status
service mb-notification restart
service mb-notification stop
```

