








Storage in the Batch XMLControlString Dynamic Message <DM> tag with Message Type of four (MT=4) 

Sample:
<DM BS='0' DESC='Description Not Specified' DSUID='10' LIB='0' MT='4' PT='12'><ELE BS='0' CK1='test 555' CK10=' ' CK11=' ' CK12=' ' CK13=' ' CK14=' ' CK2=' ' CK4='555' CK5=' ' CK6=' ' CK7=' ' CK8=' ' CK9=' ' CP=' ' DESC='Description Not Specified' DSUID='10' LINK=' ' QID='1' RQ=' ' rxt='20' X='0' Y='0'>0</ELE></DM>

When Distributing through Session/Distribution.cfc AddFiltersToQueue method call

Use includes for Dynamic and Static Processing to convert to RXDialer ready XMLControlString.
Includes/LoadQueueEMail_Dynamic.cfm
Includes/LoadQueueSMS_Static.cfm

When sending to RXDialer XMLControlString Dynamic Message <DM> tag with Message Type of one (MT=1) and Play Type of 1 (PT=1) so no telecomn resources are called by default. 

Part of processing calls the method ReadDM_MT4XML in mcidtoolsii.cfc to translate DM in batch storage to a DM that the RXDailer can read for web service call.

See:
http://techwiki.messagebroadcast.com/Campaign_Type_30/MCID_19_-_Web_Service_Call


When Processing Dial Queue in management section under act_TransferQueue.cfm run  UUID processing so we can track individual requests























XML format for SMS URL to send below:


<cfhttp url="http://xml3.us.mblox.com:8180/send" method="post" resolveurl="no" throwonerror="yes">
                <cfhttpparam type="XML" name="XMLDoc" value="#inpMFASMSDoc#">           
</cfhttp>



<?xml version=""1.0"" ?> 
<NotificationRequest Version=""3.4""> 
 <NotificationHeader> 
   <PartnerName>MsgBroadcastMT</PartnerName>   <!--- Should be constant --->
   <PartnerPassword>Qa3PEwe8</PartnerPassword> <!--- Should be constant --->
</NotificationHeader> 
 <NotificationList BatchID=""#uid#""> <!---variable --->
                <Notification SequenceNumber=""1"" MessageType=""SMS""> 
                <Message>#Message Variavble up to 160 char#   ---sampleJPMorgan Alerts: Your code is #text1# and can be used for the next #info1# hrs. Visit www.retireonline.com for questions.</Message>  <!---variable --->
                <Profile>32258</Profile> <!--- Might change if we start sending a carrier code i.e. 33113 (AT&T) --->
                <SenderID Type=""Shortcode"">69675</SenderID> <!---variable --->
                <Tariff>0</Tariff> 
                <Subscriber> 
                                <SubscriberNumber>1#smsnumber#</SubscriberNumber> <!---variable number to send to--->
                </Subscriber>
                <ServiceId>27702</ServiceId> <!---variable based on shortcode --->
                </Notification>
</NotificationList>
</NotificationRequest>



