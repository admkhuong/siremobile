<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Audio Files</title>
</head>

<body>
<h1>Audio Files
</h1>
<p>&nbsp;</p>
<p>Audio files are stored in Library format on a shared file on a media server. This is defined in Paths.cfm on each web server.</p>
<p>&nbsp;</p>
<h3>Steps to start a new file:</h3>
<p>&nbsp;</p>
<p>DB Prep<br />
  Log in as a UserId<br />
  Create or Select a Library entry in the DB<br />
  Create or Select an Element entry in the DB<br />
Create a new Script entry in the DB<br />
All objects start at one for each user and sub element - Libs start at one - ELEs start at one for each LIB - Scripts Start at one for each ELE<br />
</p>
<p>File System Prep: Each Webserver has a global path variable for this location<br />
Verify or Create User Id folder on media server - format is UXXX where XXX is the UserId<br />
Verify or Library Id folder on media server - format is LXXX where XXX is the Library ID from DB prep steps<br />
Verify or Create User Id folder on media server</p>
<p>Web Steps<br />
  User gives new file a descriptive name
  or Systems gives a default based on Date Time<br />
  User selects file to upload on computer - see rxdsWebProcessingPath for destination<br />
  or<br />
  User records file using Flash tool<br />
  or<br />
User calls in to inbound number to record script - via CFTE<br />
or<br />
BabbleSphere like Mobile App<br />
  File is trasfered to buffer area on web server - see rxdsWebProcessingPath<br />
File is validated proper type and format - no viruses or other malware<br />
File is converted into system .mp3 format 128KBPs - Using SOX tool on web server<br />
Volume is Normalized - see latest BabbleSphere app for code fix<br />
File is copied to File System Prep Directory - File name is rxds_UID_LIB_ELE_SCRIPTID.mp3</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Distribution to Fulfillment Devices</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> <br />
</p>
<p>&nbsp;</p>
<h3>Commen Paths</h3>
<p>&lt;!--- Use UNC paths for directory exists checks - shared path wont work for some reason ---&gt;<br />
  &lt;cfset rxdsLocalWritePath = &quot;\\10.0.1.114\SimpleXScripts&quot;&gt;<br />
  &lt;!---&lt;cfset rxdsWebReadPath = &quot;SimpleXScripts&quot;&gt;---&gt;<br />
  &lt;cfset rxdsLegacyReadPath = &quot;\\10.0.1.10\DynaScript&quot;&gt;<br />
&lt;cfset rxdsWebProcessingPath = &quot;C:\Temp_script&quot;&gt;</p>
<p>&lt;!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr ---&gt;<br />
  &lt;!--- Note: Current RXDialers all should have a shared path DynamicLibraries ---&gt;<br />
&lt;cfset rxdsRemoteRXDialerPath = &quot;\DynamicLibraries&quot;&gt;</p>
<p>&nbsp; </p>
<h3>Audio Conversion - SOX</h3>
<p>Audio conversion is done using open source SOX2 open source api</p>
<p><a href="libsox.pdf">libsox</a><br />
</p>
<p><a href="sox.pdf">sox</a></p>
<p><a href="soxformat.pdf">soxformat</a></p>
<p><a href="soxi.pdf">soxi</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Database</h3>
<p>All DB structures are in the rxds database</p>
<p>&nbsp;</p>
<p>Library ID is DSId_int<br />
  CREATE TABLE `dynamicscript` (<br />
`DSId_int` int(11) NOT NULL DEFAULT '0',<br />
`UserId_int` int(11) NOT NULL DEFAULT '0',<br />
`Active_int` int(11) DEFAULT '1',<br />
`Desc_vch` text,<br />
PRIMARY KEY (`DSId_int`,`UserId_int`),<br />
UNIQUE KEY `UC_DSId_int` (`DSId_int`,`UserId_int`),<br />
KEY `Active_int` (`Active_int`),<br />
KEY `UserId_int` (`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$<br />
</p>
<p><br />
  CREATE TABLE `dselement` (<br />
`DSEId_int` int(11) NOT NULL DEFAULT '0',<br />
`DSId_int` int(11) NOT NULL DEFAULT '0',<br />
`UserId_int` int(11) NOT NULL DEFAULT '0',<br />
`Active_int` int(11) DEFAULT '1',<br />
`Desc_vch` text,<br />
PRIMARY KEY (`DSEId_int`,`DSId_int`,`UserId_int`),<br />
KEY `Active_int` (`Active_int`),<br />
KEY `DSId_int` (`DSId_int`),<br />
KEY `DSEId_int` (`DSEId_int`),<br />
KEY `UserId_int` (`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p></p>
<p>CREATE TABLE `scriptdata` (<br />
`DataId_int` int(11) NOT NULL DEFAULT '0',<br />
`DSEId_int` int(11) NOT NULL DEFAULT '0',<br />
`DSId_int` int(11) NOT NULL DEFAULT '0',<br />
`UserId_int` int(11) NOT NULL DEFAULT '0',<br />
`StatusId_int` int(11) DEFAULT NULL,<br />
`AltId_vch` varchar(255) NOT NULL DEFAULT '',<br />
`Length_int` int(11) DEFAULT '0',<br />
`Format_int` int(11) DEFAULT '0',<br />
`Active_int` int(11) DEFAULT '1',<br />
`Desc_vch` text,<br />
`TTS_vch` text,<br />
`AccessLevel_int` int(11) NOT NULL DEFAULT '0',<br />
`categories_vch` varchar(255) DEFAULT NULL,<br />
`votesUp_int` int(11) DEFAULT '0',<br />
`votesDown_int` int(11) DEFAULT '0',<br />
`Created_dt` datetime NOT NULL,<br />
`viewCount_int` int(11) DEFAULT '0',<br />
`scriptFacebookId_vch` varchar(100) DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`DataId_int`,`DSEId_int`,`DSId_int`,`UserId_int`),<br />
KEY `DataId_int` (`DataId_int`),<br />
KEY `DSEId_int` (`DSEId_int`),<br />
KEY `DSId_int` (`DSId_int`),<br />
KEY `UserId_int` (`UserId_int`),<br />
KEY `StatusId_int` (`StatusId_int`),<br />
KEY `Active_int` (`Active_int`),<br />
KEY `IDX_AccessLevel_int` (`AccessLevel_int`),<br />
KEY `UserId_int_index` (`UserId_int`),<br />
KEY `AccessLevel_int_index` (`AccessLevel_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp; </p>
<p>&nbsp;</p>
<p>Every user has their own Audio Directory based on UID</p>
<p>Library</p>
<p>Element</p>
<p>Data ID</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Scripts.CFC</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Sections in EBM</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>No direct access to Audio </h3>
<p>&lt;cfparam name=&quot;inpUserId&quot; default=&quot;0&quot;&gt;<br />
  &lt;cfparam name=&quot;inpLibId&quot; default=&quot;0&quot;&gt;<br />
  &lt;cfparam name=&quot;inpEleId&quot; default=&quot;0&quot;&gt;<br />
&lt;cfparam name=&quot;inpScriptId&quot; default=&quot;0&quot;&gt;</p>
<p>&lt;!---&lt;cfinclude template=&quot;../scripts/data_ScriptPaths.cfm&quot;&gt;---&gt; </p>
<p>&lt;!--- Validate authorized current session user can listen to input users request ---&gt;</p>
<p>&lt;cfset MainLibFile = &quot;#rxdsLocalWritePath#\U#inpUserId#\L#inpLibId#\E#inpEleId#\rxds_#inpUserId#_#inpLibId#_#inpEleId#_#inpScriptId#.mp3&quot;&gt; <br />
  &lt;!---&lt;cfset MainLibFile = &quot;#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/#rxdsWebReadPath#\U#inpUserId#\L#inpLibId#\E#inpEleId#\rxds_#inpUserId#_#inpLibId#_#inpEleId#_#inpScriptId#.mp3&quot;&gt; <br />
  &lt;cfheader name=&quot;Content-Disposition&quot; value=&quot;attachment;filename=TestFile.mp3&quot;&gt;<br />
  &lt;cfcontent TYPE=&quot;application/mp3&quot; file=&quot;#MainLibFile#&quot;&gt;&lt;/cfcontent&gt; <br />
  ---&gt; </p>
<p>&lt;Cfif session.userid neq inpUserId&gt; <br />
  &lt;cfquery name=&quot;updateView&quot; datasource=&quot;#Session.DBSourceEBM#&quot;&gt;<br />
  Update rxds.scriptData<br />
  set viewcount_int = viewcount_int+1<br />
  where userid_int = #inpUserId# and DATAID_INT=#inpScriptId# <br />
  &lt;/cfquery&gt;<br />
  &lt;/Cfif&gt;</p>
<p>&lt;cfheader name=&quot;Content-Disposition&quot; value=&quot;inline; filename=TestFile.mp3&quot;&gt;<br />
  &lt;cfcontent file=&quot;#MainLibFile#&quot;&gt;&lt;/cfcontent&gt; </p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>