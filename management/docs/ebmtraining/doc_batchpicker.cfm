<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Batch Picker</title>
</head>

<body>
<h1>Batch Picker</h1>
<p>&nbsp;</p>
<p>A standard EBM dialog for picking a Campaign/BatchID. Can either select with single click and then choose select button or Double click on a batch. </p>
<p>This is for selection only - no editing here for now</p>
<p>Provides basic filtering and ordering for searching for a particular batch id</p>
<p>&nbsp;</p>
<h3>To use:</h3>
<p>Call with a JQuery Dialog:</p>
<p>If optionally leaving list in last select state - dont destroy on close - you can reopen in last state but must kill on exiting calling page/dialog. If you do this move the following lines into your close top page method.</p>
<p>CreateBatchPickerDialog.dialog('destroy');<br />
CreateBatchPickerDialog.remove();<br />
CreateBatchPickerDialog = 0;</p>
<p>&nbsp;</p>
<h3>Optional Callback function:</h3>
<p>If an extra URL param is passed in (inpCallBackUpdateBatchId) you can define a local function to execute that recieves a BatchID passed into it.</p>
<p>&nbsp;</p>
<h3>Sample Code:</h3>
<p>&lt;!--- Global so popup can refernece it to close it---&gt;<br />
  var CreateBatchPickerDialog = 0;<br />
  <br />
  function BatchPickerDialog()<br />
  { <br />
  var $loading = $('&lt;img src=&quot;&lt;cfoutput&gt;#rootUrl#/#PublicPath#&lt;/cfoutput&gt;/images/loading-small.gif&quot; width=&quot;16&quot; height=&quot;16&quot;&gt;...Preparing');<br />
  <br />
  var ParamStr = '?inpCallBackUpdateBatchId=UpdateLocalBatchData';<br />
  <br />
  if(typeof(inpBRDialString) != &quot;undefined&quot; &amp;&amp; inpBRDialString != &quot;&quot;)<br />
  { <br />
  &lt;!---//ParamStr = '?inpBRDialString=' + encodeURIComponent(inpBRDialString);<br />
  <br />
  //if(typeof(inpNotes) != &quot;undefined&quot; &amp;&amp; inpNotes != &quot;&quot;) <br />
  //	ParamStr = ParamStr + '&amp;inpNotes=' + encodeURIComponent(inpNotes);---&gt;<br />
  <br />
  }<br />
  <br />
  &lt;!--- reuse dialog data ---&gt;<br />
  if(CreateBatchPickerDialog != 0)<br />
  { <br />
  <br />
  CreateBatchPickerDialog.dialog('open');<br />
  return false; <br />
  <br />
  &lt;!---CreateBatchPickerDialog.remove();<br />
  CreateBatchPickerDialog = 0;---&gt;<br />
  <br />
  }<br />
  <br />
  CreateBatchPickerDialog = $('&lt;div&gt;&lt;/div&gt;').append($loading.clone());<br />
  <br />
  CreateBatchPickerDialog<br />
  .load('&lt;cfoutput&gt;#rootUrl#/#SessionPath#&lt;/cfoutput&gt;/Batch/dsp_BatchPicker.cfm' + ParamStr)<br />
  .dialog({<br />
  show: {effect: &quot;fade&quot;, duration: 500},<br />
  hide: {effect: &quot;fold&quot;, duration: 500},<br />
  modal : true,<br />
  title: 'Choose a Campaign',<br />
  width: 700,<br />
  height: 'auto',<br />
  position: 'top', <br />
  closeOnEscape: false, <br />
  dialogClass: 'noTitleStuff' <br />
});</p>
<p> CreateBatchPickerDialog.dialog('open');</p>
<p> return false; <br />
  }</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Callback function that you define - can be anything you want it to be</h3>
<h3>Sample:</h3>
<p><br />
  function UpdateLocalBatchData(inpBatchId)<br />
  { <br />
$(&quot;#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole&quot;).val(inpBatchId); <br />
<br />
LastScriptIdASMG = $(&quot;#addgroupToQueueConsoleDiv #inpBatchID_LCABTQConsole&quot;).val();<br />
<br />
var inpDoRules = 0; <br />
if ($(&quot;#addgroupToQueueConsoleDiv input[name='inpDoRules']:checked&quot;).val() == 'DoRules')<br />
inpDoRules = 1; <br />
<br />
if(LastScriptIdASMG &gt; 0)<br />
{ <br />
GetCurrentSchedule();<br />
<br />
&lt;!--- If apply rules is checked than adjust counts based on batch rules ---&gt;<br />
if(inpDoRules)<br />
{<br />
GetGroupToQueueCounts_Phone(LastScriptIdASMG);<br />
GetGroupToQueueCounts_eMail(LastScriptIdASMG);<br />
GetGroupToQueueCounts_SMS(LastScriptIdASMG);<br />
}<br />
<br />
// $(&quot;#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton&quot;).show();<br />
}<br />
else<br />
{<br />
// $(&quot;#addgroupToQueueConsoleDiv #addgroupToQueueConsoleButton&quot;).hide(); <br />
}<br />
<br />
}<br />
</p>
</body>
</html>
