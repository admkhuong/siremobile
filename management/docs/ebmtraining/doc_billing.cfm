<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billing</title>
</head>

<body>
<h1>Billing</h1>
<p>&nbsp;</p>
<p>Each user account has a balance stored in the   simplebilling.billing database.table. The billing is always decremented when extracting results from the fulfillment device. Billing is either incremented as a result of user charging with credit card or through admin tools.</p>
<h3>&nbsp;</h3>
<h3>Balance_int -</h3>
<p>Is a float value representing US dollars up to three decimal places max </p>
<p>&nbsp; </p>
<p>During distribution EBM sites need to check totals in queue and not allow queueing of more data if estimated totals exceed balance. This is done in Billing.CFC under ValidateBilling Method</p>
<h3>&nbsp;</h3>
<p>During extraction - act_CalculateBilling.cfm is called.A switch stement will handle the various billing types.</p>
<p>Each userID has their own entry in billing table. </p>
<p>When creating a new user account EBM sites will need to create associated billing tables.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Security:</h3>
<p> It is important that no one can  access this without proper credentials. All credentials and location data needs  to be stored as part of transaction log including IP Address, UserId, Timestamp  and any other data available to identify the user making the adjustment</p>
<p>Managers log in with new Co-Brand ID - 5 is EBM Manager</p>
<p>&nbsp;</p>
<p>
<h3>Billing Types - RateType_int</h3>
1 - Is based on connect time. Increment1_int value is used to determine total increments. Rate1_int is multiplied by the Ceiling (Round UP) of the CEILING(CurrTotalConnectTime_int / CurrIncrement1) <br />
2 - Every result is charge 1 increment. Balance is debited by 1.00 * rate. Period<br />
3 - Per message left flat rate. For connected calls only Balance is debited by 1.00 * rate. Period<br />
4 - Based on value passed through system in CCD of MMLS. For connected calls only - will bill based on length of message<br />
5 - 
Based on value passed through system in CCD of MMLS. For all attempts - will bill based on length of message<br />
Note: MMLS is just a multipler - if 30 second increments a one minute message will have an MMLS of 2
<p> <br />
</p>
<p>&nbsp;</p>
<h3>DATA<br />
</h3>
<p>CREATE TABLE `billing` (<br />
`UserId_int` int(11) NOT NULL,<br />
`Balance_int` float(10,3) NOT NULL DEFAULT '0.000',<br />
`RateType_int` int(4) NOT NULL DEFAULT '1',<br />
`Rate1_int` float(10,3) NOT NULL DEFAULT '0.050',<br />
`Rate2_int` float(10,3) NOT NULL DEFAULT '0.050',<br />
`Rate3_int` float(10,3) NOT NULL DEFAULT '0.050',<br />
`Increment1_int` int(4) NOT NULL DEFAULT '30',<br />
`Increment2_int` int(4) NOT NULL DEFAULT '30',<br />
`Increment3_int` int(4) NOT NULL DEFAULT '30',<br />
PRIMARY KEY (`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Actors:</h3><p>System Admin – only a top level  system admin should have access to this<br />
  System User – An EBM system user  will not be able to just add balance but must either rely on System Admin OR we  will be adding credit card processing at a later date.Data is stored in  simplebilling.billing database.table<br />
  All transactions need to be  logged in the simplebilling.transactionlog – use the EventData_vch to store  additional transaction data in XML format.Security:<br />
  It is important that no one can  access this without proper credentials. All credentials and location data needs  to be stored as part of transaction log including IP Address, UserId, Timestamp  and any other data available to identify the user making the adjustment
</p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Billing Transaction Log:</p>
<p>EBM Admins will need a tool to  view their own accounts history – they should be limited to only their own  account information and not be able to see everyone else’s data.<br />
  System Admins We will need a  viewer to backtrack history for both the entire system or list of user ids<br />
  Internal Balance Update is just  one Event_vch – there will be other events based on credit card transactions  later.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Adding - Modifing - Removing<br />
  Billing Types
</h3>
<p>Make changes in two places<br />
  Session/CFC/Billing.cfc - ValidateBilling method<br />
and<br /> 
Management/processing/act_CalculateBilling.cfm</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Project:</p>
<p>All Coding Standards.</p>
<p>Mangement Tools</p>
<p>Select User - filter by name, company, userid, login, other....<br />
Edit User - Change Name info, address, billing type<br />
Make balance Changes
</p>
<p>&nbsp;</p>
<p>Document how to add a new billing type - what pages need editing, what entries in the database need to be added.<br />
  New table to track billing types: BillingTypes table</p>
<p>Add BiltingType_int column to simplebilling..billing table</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Use Case 1:</p>
<p>Create new Account - use billing type CIG</p>
<p>Charge up account for CIG demo usages</p>
<p>Create new billing type ala Chase CIG</p>
<p>Run an internal Test campaign - min ten calls - </p>
<p>generate Reports</p>
<p></p>
<p>Use Case 2:</p>
<p>Ato logs in. Finds user. Types filters - Date Range and /or Batch ID. generates report for billing. Exports to send to customer.</p>
<p>Break down to include:</p>
<p>All Channels<br />
  Option to break down by Batch<br />
  Option to summarize all Batches<br />
  Export to XLS, PDF
</p>
<p>Our (MB) information that needs to be in report<br />
Customer Information that needs to be in report.</p>
<p>Reports to look for descrepencies - More dials than queuued - elimnate erroneous duplicates </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Sample CIG - careful - this is real data<br />
  http://casperb101.mb/ChaseProduction/reports/dsp_billingSV.cfm<br />
\\10.18.1.230\c$\CasperSite\ChaseProduction\Reports</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>