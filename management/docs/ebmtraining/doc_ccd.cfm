<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Call Control Data - CCD
</h1>
<p>&nbsp;</p>
<div id="pageText">
  <p> </p>
  <p>Sample string in ColdFusion<br />
    <br />
    &lt;cfset CurrMessageXMLCCD = &quot;&lt;CCD FileSeq='&quot; &amp;   XMLFormat(&quot;0&quot;) &amp; &quot;' UserSpecData='&quot; &amp;  XMLFormat(uniqueId) &amp;   &quot;'&quot; &amp; &quot; CID='8662651727'&quot; &amp; &quot; DRD='1' RMin='1' AID='&quot; &amp;   XMLFormat(&quot;MFA JP Morgan 3&quot;) &amp; &quot;'&gt;0&lt;/CCD&gt;&quot;&gt;<br />
    <br />
    <br />
    XML Tag is &lt;CCD&gt;0&lt;/CCD&gt; - 0 is just a placeholder<br />
    <br />
    FileSeq = used to help identidy which datafeed for a given dial -   used for daily/weekly/monthly campaigns that reuse the same Batch info -   default is &quot;&quot; blank<br />
    UserSpecData = place to carry over customer data used in reporting and extracts - default is &quot;&quot; blank<br />
    CID = ten to 15 digit caller ID that is broadcast for that call - default is &quot;&quot; blank<br />
    DRD = number of aumated redial attempts valid values are from 1 to 5 - default is 0<br />
    RMin = number of minutes to schedule in advance the next redial in the local dialers DTS table - default is 1440<br />
    PTL = 1 or 0 - Play to live answers - default is 1 - LMLA can and does override these values<br />
    PTM = 1 or 0 - Play to message machines - default is 1 - LMLA can and does override these values<br />
    SCDPL = Skip call detection pause length - default is 0 - if greater   than 0 all calls will skip call detection and be treated as live answers   if the call does not changup before this pause length is finished - in   miliseconds</p>
  <p>LMLA - Leave Message Last Attempt - overrides the PTL/PTM option to   allow skipping messages until the last attempted redial - WARNING - if   you do not define this it will default to 1 which will override any PTM   or PTLs already defined if the curretn redial is also the final dial.</p>
  <p><br />
    RMB - time in minutes to schedule a redial for Busy - overrides   RedialMin - A value of -1 or less will cause redial logic to halt on   this call result type<br />
    RMF - time in minutes to schedule a redial for   FAX - overrides RedialMin - A value of -1 or less will cause redial   logic to halt on this call result type<br />
    RMNA - time in minutes to   schedule a redial for No Answer - overrides RedialMin - A value of -1 or   less will cause redial logic to halt on this call result type<br />
    RMO -   time in minutes to schedule a redial for Other - overrides RedialMin - A   value of -1 or less will cause redial logic to halt on this call result   type</p>
  <p><br />
    IRC - Increment (MAX) Redail Count -</p>
  <p>ESI - Extraction System ID - a negative number if not system 1 -   allows multiple extrators to pull from same dialer. Call result status   will be 1 or the alternate system Id until pulled - pulled calls status   will still be greater than 1 and cleared after three days.</p>
  <p> </p>
  <p>Version 24.0.0.18 and above<br />
    {</p>
  <p>SSVMD value - Second Stage Voice Mail Detection delay is a value from   1 to 100 representing 100 millisecond incrments 25 = 2.5 seconds- any   value outside 1-100 is ignored and it will default to reg settings on   local RXDialer</p>
  <p>SSVMDIVR flag this is a value from 1 to 100 representing 100   millisecond incrments 25 = 2.5 seconds any value outside 1-100 is   ignored and it will default to 2.6 seconds.</p>
  <p>}</p>
  <p> </p>
  <p>Version 24.0.0.33 and above - Super advanced case user</p>
  <p>POVM - Play Over Voice Mail - Will play POVMSTR as digits over a   voicemail to force it to skip to recording faster. 0 or blank is off -   only applies if not using SCDL and PTM is set to true. Use 1 for turning   on this option.</p>
  <p>POVMSTR - This is the string that will be pulsed to DTMF when a   voicemail is detetected - default is # if this is left blank - use * and   # as valid keys also.</p>
  <p> </p>
  <p> </p>
  <p>&lt;style type=&quot;text/css&quot;&gt;]]&gt;&lt;/style&gt;</p>
  <p>IRC=’ L,M,B,F,N,O’<br />
    <br />
    Allows   specified list of call results codes to increase the CCD’s DRD amount up   by one. The DRD as specified in the XMLControlString_vch is replaced   (on the RXDialer DTS table only) with one that is one increment higher.<br />
    <br />
    Default is not specified and this option is ignored – defaults to all previous behaviors<br />
    <br />
    Only has meaning if DRD is greater than 0<br />
    <br />
    Cannot increment beyond 6 no matter what<br />
    <br />
    L,M,B,F,N,O – Upper or lower case OK<br />
    <br />
    (L)ive<br />
    (M)achine<br />
    (B)usy<br />
    (F)ax<br />
    (N)o Answer<br />
    (O)there<br />
    <br />
    In conjunction with the other CCD redial codes can be used to do all sorts of crazy stuff – BE CAREFUL!<br />
  </p>
  <p>MIRC -  </p>
  <p>Absolute maximum number of times to increment   DRD with IRC logic - if it starts at 4 then a MIRC='1' will stop redials   at 5 if one of the IRC map entries is hit and will stop at 4 if no IRC   map entry is hit.</p>
  <p> </p>
  <p>Version 22.0.0.25 or higher<br />
    <br />
    Automated escalation logic<br />
    There is no ESC0 as the initial call will always go to the initial dial string<br />
    ESC1 - if not blank &quot;&quot; will switch out the specified dial string for the DTS dial string - the next call will go to this number<br />
    ESC2 - if not blank &quot;&quot; will switch out the specified dial string for the DTS dial string - the next call will go to this number<br />
    ESC3 - if not blank &quot;&quot; will switch out the specified dial string for the DTS dial string - the next call will go to this number<br />
    ESC4 - if not blank &quot;&quot; will switch out the specified dial string for the DTS dial string - the next call will go to this number<br />
    ESC5 - if not blank &quot;&quot; will switch out the specified dial string for the DTS dial string - the next call will go to this number<br />
    <br />
    Can be used in several fashions - must be built into original calls CCD string.<br />
    <br />
    Can be used with CRNML to dial up to six unique numbers trying to get a specific call result or a specific response (CRNML)<br />
    Can be used to dial two numbers up to three times each trying to get a specific call result or specific response (CRNMR)<br />
    <br />
    CRNMR - Continue Redial No Match Response<br />
    Looks for a specified response in the XMLResultString - else will treat call's redial logic as no message left yet.<br />
    You must escape your &lt;, &gt;, and / to keep your XML valid</p>
  <p> </p>
  <p> <br />
    SAMPLE - if you are looking for QID 2 = 1;<br />
    &lt;Q ID='2'&gt;0&lt;/Q&gt; = &amp;lt;Q ID=&amp;apos;2&amp;apos;&amp;gt;1&amp;lt;<br />
    CRNML='&amp;lt;Q ID=&amp;apos;2&amp;apos;&amp;gt;1&amp;lt;'          </p>
  <p> </p>
  <p> <br />
    &lt;DM LIB='1281' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
    &lt;RXSS&gt;<br />
    &lt;ELE QID='1' RXT='11' BS='0' CK1='123456' CK5='2'&gt;1&lt;/ELE&gt;<br />
    &lt;ELE QID='2' RXT='1' BS='1' CK5='30'&gt;&lt;ELE ID='TTS' RXVID='5' RXBR='8'&gt;Joe Blow&lt;/ELE&gt;0&lt;/ELE&gt;<br />
    &lt;ELE QID='30' RXT='1' BS='0' DS='1281' DSE='1' DI='1' CK1='3' CK5='31'&gt;1&lt;/ELE&gt;<br />
    &lt;ELE QID='31' RXT='11' BS='0' CK1='987654321' CK5='-1'&gt;1&lt;/ELE&gt;<br />
    &lt;/RXSS&gt;<br />
    &lt;DM LIB='1281' MT='2' PT='1'&gt;&lt;ELE ID='1'&gt;2&lt;/ELE&gt;&lt;/DM&gt;<br />
    &lt;CCD CID='7777777777' DRD='1' RMin='1' PTL='1' PTM='1'   ESC1='9494000553' CRNMR='&amp;lt;Q   ID=&amp;apos;2&amp;apos;&amp;gt;1&amp;lt;'&gt;0&lt;/CCD&gt;</p>
  <p> </p>
  <p> </p>
  <p> </p>
  <p><br />
    New upper limit for automated redials is 6 max attempts – any value   higher than this will not redial – if you try to use these options on a   redial attempt higher than 6 than options like LMLA will still not   leave a message on the final attempt.</p>
  <p> </p>
  <p> </p>
  Non-specified values should default to old behavior for existing campaigns
  <p> </p>
  <p>Please test to ensure desired behavior – requires RXDialer Version 20.0.0.16 or higher</p>
  <p>  </p>
  <p><br />
    There is now a new campaign type 30 option in the CCD string - Skip Call Detection Pause Length (SCDPL)<br />
    <br />
    This is the amount in milliseconds to wait after connection before just playing the message.<br />
    <br />
    Call results will be either 3 – always assumes live<br />
    Or<br />
    29 – consumer hung up before pause is finished<br />
    <br />
    Good for demos and excessively noisy environments – will result in partials for answering machines though<br />
    <br />
    See sample below<br />
    <br />
    <br />
    &lt;DM LIB='0' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
    &lt;RXSS&gt;<br />
    &lt;ELE QID='1' RXT='2' BS='0' DS='0' UID='91' CAMPID='946' GSID='2'   CK1='3' CK2='1,-6' CK3='-100' CK4='(1,4),(-6,2)' CK5='-1' CK6='-1'   CK7='4'&gt;0&lt;/ELE&gt;<br />
    &lt;ELE QID='2' RXT='5' BS='0' DS='0' UID='91' CAMPID='946' GSID='3' CK5='3'&gt;0&lt;/ELE&gt;<br />
    &lt;ELE QID='3' RXT='1' BS='0' DS='0' UID='91' CAMPID='946' GSID='3' CK1='100' CK5='-1'&gt;0&lt;/ELE&gt;<br />
    &lt;ELE QID='4' RXT='4' BS='0' DS='0' UID='91' CAMPID='946' GSID='2' CK1='9494283111' CK2='1' CK3='100'&gt;0&lt;/ELE&gt;<br />
    &lt;/RXSS&gt;<br />
    &lt;DM LIB='0' MT='2' PT='1'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
    &lt;CCDFileSeq='0' UserSpecData='0' CID='9999999999' DRD='4' RMin='30' PTL='1' PTM='1' SCDPL='2000'&gt;0&lt;/CCD&gt;</p>
</div>
<p>&nbsp;</p>
</body>
</html>