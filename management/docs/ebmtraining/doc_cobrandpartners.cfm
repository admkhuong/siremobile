<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>CoBrand Partners</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Description</h3>
<p>Allows multiple web sites to have there own unique set of user accounts and still exist within the EBM system structures. Without a cobrand ID the email address used for user accounts would not be allow in multiple systems.</p>
<p>&nbsp;</p>
<p>Default System is 1 or NULL for EBM - any system</p>
<p>&nbsp;</p>
<p>To add a new site:<br />
Create a CBPId_int in cobrandpartner table - manual enter for now as there will not be that many<br />
Use 
AND CBPID_int = X in all of your account login and access queries.</p>
<p>Special Case EBM -  AND (CBPID_int = 1 OR CBPID_int IS NULL)      </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Data</h3>
<p>CREATE TABLE `cobrandpartner` (<br />
`Id_int` int(11) NOT NULL,<br />
`Desc_vch` varchar(512) DEFAULT NULL,<br />
PRIMARY KEY (`Id_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>