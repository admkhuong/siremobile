<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Collaboration</h1>
<p>&nbsp;</p>
<p>A place where external system users can specify the dynamics of a campaign. This form acts as a digitally signed approval form to needed to be authorized before running a campaign.</p>
<p>&nbsp;</p>
<p>The collaboration tool is accessed for each batch through the toolbar above the stage.</p>
<p><img src="ColabSample4.jpg" alt="8" width="140" height="223" /></p>
<p>&nbsp;</p>
<p>Not all accounts will uses this - just typically the big corporate accounts.</p>
<p><img src="ColabSample2.jpg" alt="6" width="688" height="418" /></p>
<p>&nbsp;</p>
<p>Changes that are saved are stored historically and are associated with the User account that is currently logged into the Company level account.</p>
<p>&nbsp;</p>
<p>A new Company level UserAccount (CompanyAccountId_int) may want customized collaboration forms for use by all users.</p>
<p>A. Different companies will want different fields available.</p>
<p>B. When starting a new Company Level Account - the system administrator will need to be able to selectively enable or disable certain fields. Even when disabled the fields will still be in the DB but empty. Optional accounts will be top level groups - the ones in dark blue - and sub-level fields.</p>
<p>Each Coulumn is a  table in the DB and each sub-item is a field itn the table. Tables are stored in the SimpleObjects database and table names are prefaced with PM_</p>
<p>C. When starting a new Company Level Account - the system administrator will need to be able to selectively set as Required certain fields. When disabled the fields will still be in the DB but empty and not be required..</p>
<p>&nbsp;</p>
<p>So while one account may see:<img src="ColabSample1.jpg" alt="5" width="1543" height="1166" /></p>
<p>A differnt account may only see:<img src="ColabSample3.jpg" alt="7" width="1543" height="1166" /></p>
</body>
</html>