<style type="text/css">
.warning {
	color: #F00;
}
</style>
<p class="warning">***************************************************<br />
  Do NOT edit here anymore - moved to Session Help Section<br />
*************************************************** </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p></p>
<p>&nbsp;</p>
<p>New Call Result Numbers<br />
  BR Range</p>
<p>Call Results and codes -Most codes get grouped into OTHER catagory - otherwise it might be TMI - too much information<br />
  -3	Problem with line routing<br />
  -2	Result Not Set Never got a result<br />
  -1	Severe Error<br />
  1	Cadence Break<br />
  2	Loop Current - Analog only<br />
  3	Positive Voice Detection<br />
  4	Positive Answering Machine Detected<br />
  5	Unknown But Assume Live<br />
  6	CO Intercept<br />
  7	Busy<br />
  8	Call Error<br />
  9	Fax Machine<br />
  10	No Answer<br />
  11	No Dialtone<br />
  12	No Ringback<br />
  13	Channel Stopped<br />
  14	Ring time + AnsMachine Message exceeded 90 seconds<br />
  15	Unable to allocate resource<br />
  20	MakeCall Failed<br />
  21	Failed media Detect<br />
  22	Failed Set Event Mask<br />
  23	Failed GCEV_ERROR While waiting for connection<br />
  24	Disconnected for unknown reson<br />
  25	Call disconnected by remote network<br />
  26	GCEV_ERROR<br />
  27	GCEV_TASKFAIL<br />
  28	Old style default<br />
  40	Answered but real quick hangup<br />
  42	CO Intercept <br />
  45	Invalid GCRN - makecall must have failed<br />
  46	Legacy Other<br />
  47	CO Intercept Type II<br />
  48	No Answer Type II<br />
  49	Should Not Get Here - Other<br />
  50	ISDN Connected but failure to get more information <br />
  -50	Invalid XML<br />
  75	Invalid AE Combo - unable to get Time Zone information<br />
  76	eMail Only on RXDialer Line</p>
<p>500 - <br />
  501 - Duplicate detected during distribution - already distributed for this batch<br />
  502 - Duplicate based on UUID<br />
  503 - Bad XML Prior to Distribution<br />
</p>
<p>&nbsp;</p>
<p><br />
delimiter $$</p>
<p>CREATE TABLE simplexresults.`contactresults` (<br />
  `MasterRXCallDetailId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,<br />
  `RXCallDetailId_int` int(11) unsigned NOT NULL,<br />
  `CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',<br />
  `DTSID_int` int(10) unsigned DEFAULT NULL,<br />
  `BatchId_bi` bigint(20) DEFAULT NULL,<br />
  `PhoneId_int` int(11) DEFAULT NULL,<br />
  `TotalObjectTime_int` int(11) DEFAULT NULL,<br />
  `TotalCallTimeLiveTransfer_int` int(11) DEFAULT NULL,<br />
  `TotalCallTime_int` int(11) DEFAULT NULL,<br />
  `TotalConnectTime_int` int(11) DEFAULT '0',<br />
  `ReplayTotalCallTime_int` int(11) DEFAULT NULL,<br />
  `SixSecondBilling_int` int(11) DEFAULT NULL,<br />
  `SystemBilling_int` int(11) DEFAULT NULL,<br />
  `TotalAnswerTime_int` int(11) DEFAULT NULL,<br />
  `CallResult_int` int(11) DEFAULT NULL,<br />
  `RedialNumber_int` int(11) DEFAULT NULL,<br />
  `MessageDelivered_si` smallint(6) DEFAULT NULL,<br />
  `SingleResponseSurvey_si` smallint(6) DEFAULT NULL,<br />
  `UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,<br />
  `NumberOfHoursToRescheduleRedial_si` smallint(6) DEFAULT NULL,<br />
  `TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,<br />
  `TransferStatusId_ti` tinyint(4) DEFAULT NULL,<br />
  `IsHangUpDetected_ti` tinyint(4) DEFAULT NULL,<br />
  `IsOptOut_ti` tinyint(4) DEFAULT NULL,<br />
  `IsOptIn_ti` tinyint(4) DEFAULT '0',<br />
  `IsMaxRedialsReached_ti` tinyint(4) DEFAULT NULL,<br />
  `IsRescheduled_ti` tinyint(4) DEFAULT NULL,<br />
  `RXCDLStartTime_dt` datetime DEFAULT NULL,<br />
  `CallStartTime_dt` datetime DEFAULT NULL,<br />
  `CallEndTime_dt` datetime DEFAULT NULL,<br />
  `CallStartTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
  `CallEndTimeLiveTransfer_dt` datetime DEFAULT NULL,<br />
  `CallResultTS_dt` datetime DEFAULT NULL,<br />
  `PlayFileStartTime_dt` datetime DEFAULT NULL,<br />
  `PlayFileEndTime_dt` datetime DEFAULT NULL,<br />
  `HangUpDetectedTS_dt` datetime DEFAULT NULL,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
  `DialerName_vch` varchar(255) DEFAULT NULL,<br />
  `DialerIP_vch` varchar(50) DEFAULT NULL,<br />
  `CurrTS_vch` varchar(255) DEFAULT NULL,<br />
  `CurrVoice_vch` varchar(255) DEFAULT NULL,<br />
  `CurrTSLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
  `CurrVoiceLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
  `CurrCDP_vch` varchar(255) DEFAULT NULL,<br />
  `CurrCDPLiveTransfer_vch` varchar(255) DEFAULT NULL,<br />
  `ContactString_vch` varchar(255) DEFAULT NULL,<br />
  `ContactTypeId_int` int(11) DEFAULT NULL,<br />
  `XMLResultStr_vch` text,<br />
  `XMLControlString_vch` text,<br />
  `FileSeqNumber_int` int(11) DEFAULT NULL,<br />
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,<br />
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',<br />
  `dial6_vch` varchar(10) DEFAULT NULL,<br />
  `MainMessageLengthSeconds_int` int(11) DEFAULT '0',<br />
  PRIMARY KEY (`MasterRXCallDetailId_int`),<br />
  KEY `XPKRXCallDetails` (`MasterRXCallDetailId_int`),<br />
  KEY `IDX_ContactString_vch` (`ContactString_vch`),<br />
  KEY `IDX_DTSID_int` (`DTSID_int`),<br />
  KEY `IDX_CallResult_int` (`CallResult_int`),<br />
  KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),<br />
  KEY `IDX_BatchId_bi` (`BatchId_bi`),<br />
  KEY `IDX_Dial6` (`dial6_vch`)<br />
  ) ENGINE=InnoDB AUTO_INCREMENT=3984 DEFAULT CHARSET=latin1$$</p>
<p></p>
<p>t</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
