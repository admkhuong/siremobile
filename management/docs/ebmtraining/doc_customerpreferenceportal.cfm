<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Preference Portal</title>
</head>

<body>
<h1>Customer Preference Portal (CPP)</h1>
<p>&nbsp;</p>
<h3>Overview</h3>
<p>The Customer Preference Portal is a place where a company's customers can log in and provide feedback on how they wish to be communicated with.</p>
<p>EBM User can generate HTML code to embed in their site to allow customers to control their own contact preferences.</p>
<p>Remote customers can then modify their  preferences either within remote customer site or through  a special EBM site.</p>
<p>For web based CPP access the end user must have a primary eMail Address</p>
<p>The CPP is for contacts who are on the RXMultiList  under an EBM User’s account to be able to access and control how they want the  EBM User to communicate with them. To facilitate this the EBM user creates list  of message types (These are jst GroupId’s in EBM see `simplelists`.`simplephonelistgroups`)  that a remote contact can select which of their contacts strings they wish to  be used and in what order to attempt them in.</p>
<p>This will be primarily a stand-alone web site for a contact that is on an EBM  users RXMultiList can access and set preferences without having to log in as an  EBM user. Security is to limit access only to the contacts linked list of  contacts – only those entries in the RXMultiList that have the same  emailAddress in the CPPID_vch field are eligible  to be managed by a remote user. If a remote user adds a new contact string then  the CPPID_vch is filled in with the one used to authenticate access.</p>
<p>&nbsp;</p>
<p><strong>WARNING:</strong>Do not put critical organizational groups into a publi setting - the public can remove themselves from a group and you may not have any other way to tell where the contact originated from or where it may be a a part of by default.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h1>TODOs</h1>
<p>Customizable Apps to specific UUID. Download app for publishing.</p>
<p>Load montioring usage of public API's - resource allocation and security</p>
<p>&nbsp;</p>
<h3>Google apps account</h3>
<p>  ContactPreferencePortal.com<br />
  Google apps for <br />
  admin@contactpreferenceportal.com<br />
support@contactpreferenceportal.com<br />
dEF!1048</p>
<p>&nbsp;</p>
<h3>Password Recovery or new or first time use</h3>
<p>email is through the Google Apps email account from  support@contactpreferenceportal.com</p>
<p>&nbsp;</p>
<h3>Why?</h3>
<p>Casual Customers that do not have an established account can still interact with the company.<br />
  Covert Prospects, Shopper<br />
Track entries through CPP - coupon codes</p>
<p>Table Tents, Signage, Print media area ll ways to peak interest in  the program. Once they are in you gain the ability to market directly to email, SMS, Phone.<br />
  QR Codes, SMS, Web
</p>
<p>Casual Customers do not want to be annoyed - retain interest with compelling offers. Coupons, Special offers, Secret Sales, New Inventory, Limited Inventory.....</p>
<p>Look at our features and details -&gt; See list in code</p>
<h3>Duplicates</h3>
<p>All RXMultiList entries are unique by - PRIMARY KEY (`UserId_int`,`ContactString_vch`,`SourceKey_vch`,`ContactTypeId_int`),</p>
<p>For the CPP the SourceKey_vch is the same as the current users's CPPID which is their email address by default.</p>
<p>Multiple entries are possible for the same contact string - but only one entry per contact string per CPPID</p>
<p>All contacts strings can belong to multiple groups.</p>
<p><strong>WARNING: </strong>Do not put critical organizational groups into a publi setting - the public can remove themselves from a group and you may not have any other way to tell where the contact originated from or where it may be a a part of by default.</p>
<p>&nbsp;</p>
<h3>Data Structures</h3>
<p>Customer Preferences are stored in the simplelists.customerpreferenceportal database.table</p>
<p><br />
  CREATE TABLE `customerpreferenceportal` (<br />
`CPP_UUID_vch` varchar(36) NOT NULL,<br />
`UserId_int` int(11) NOT NULL,<br />
`Desc_vch` varchar(2048) DEFAULT NULL,<br />
`PrimaryLink_vch` varchar(1024) DEFAULT NULL,<br />
`Active_int` int(4) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`LastModified_dt` datetime DEFAULT NULL,<br />
`CPP_Template_vch` text,<br />
`GroupId_int` int(11) DEFAULT NULL,<br />
`SystemPassword_vch` blob,<br />
PRIMARY KEY (`CPP_UUID_vch`),<br />
UNIQUE KEY `CPP_UUID_vch_UNIQUE` (`CPP_UUID_vch`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$<br />
</p>
<p>&nbsp;</p>
<p>Password is stored for each CPPID encrypted with mySQL AES encryption using the SESSION.EncryptionKey_UserDB<br />
  ... 
  AND Password_vch = AES_ENCRYPT(&lt;CFQUERYPARAM CFSQLTYPE=&quot;CF_SQL_VARCHAR&quot; VALUE=&quot;#inpPassword#&quot;&gt;, '#APPLICATION.EncryptionKey_UserDB#')</p>
<p>&nbsp;</p>
<p>Unique Consumers are linked together by secondary eMail Address in RXMultiList Table<br />
  Consumer manage  their account through the primary eMail address and can specify a password or use the one generated for them
</p>
<p>Alter table - <br />
  new eMail Address for CPP login stored in CPPID_vch - can be same as ContactString - used to track a unique user for CPP purposes.<br />
  new password for CPP login
  - CPPPWD_vch<br />
  new priority order flag - 
CPPPriority_vch</p>
<p>ALTER TABLE `simplelists`.`rxmultilist` ADD COLUMN `CPPID_vch` VARCHAR(1024) NULL  AFTER `FirstName_vch` , ADD COLUMN `CPPPWD_vch` BLOB NULL  AFTER `CPPID_vch` , ADD COLUMN `CPPPriority_vch` INT(4) NULL  AFTER `CPPPWD_vch` ;</p>
<p>All contact strings are related under a given EBM UserID_int by their CPPID_vch. The CPPID_vch is alwyas a valid email address.</p>
<p>&nbsp;</p>
<h3>Group Types</h3>
<p>EBM User Defined - can be used to gather lists of opted in customers for particualr campaigns</p>
<p><s>Each CPP has a list of Group type the consumer may sign up for. Not all CPPs will offer multiple types and may only inlude one type by default if noe are specified during setup. Group types are just a comma seperated list of GroupId_int's - grouplist_vch in customerpreferenceportal</s></p>
<p>Now CPP allows just one group to join - cleans up complexity - If want to join more than one group then UI can make multiple API calls or a special API to pass a list of groups too.</p>
<p>UI Page to give samples:<br />
  Outages<br />
  New Products<br />
  Discount Offers and Coupons<br /> 
Appointment Confirmation<br /> 
Usage Alerts
</p>
<p>&nbsp;</p>
<h3>EBM UI</h3>
<p>Grid List of active Customer Preference Portals (CPP)<br />
Option to Add new CPP - on creation assign a new UUID using mySQL UUID() method - reference this UUID<br />
Option to Edit existing CPP 
  <br />
Option to rename existing CPP<br />
update last modified date when changes  are made to CPP object<br />
WYSIWYG TineMCE editor to define custom HTML content for Consumer side of CPP - stored in CPP_Template_vch</p>
<p>CPP Editor Dialog:<br /> 
  Display list of existing Group types (GroupId)<br />
  Display total count for a Group type<br />
  Add new Group type - each Group type is related to specific GroupId as consumers add ContactStrings to a given Group type the same contact strings in the main RXMultiList are linked to that GroupID. An existing GoupId may be linked to a CPP or a new GroupID may be defined - see add/manage group methods in existing MultiLists.cfc</p>
<p>Reporting: <br />
  each Report should have a date range option<br />
  each report should be able to focus on 1 or more groups<br />
  How many on CPP lists Groups?<br />
  How many have logged in and made changes?<br />
  How many have multiple contact strings related to?<br />
Average number of contact strings related to each other - 3 is a good number to target.<br />
Group Picker Dialog - 
Ability  to select one or more groups for reporting preferences - see how Main Reports does it for Batch picker</p>
<p>Ability to filter a given contact list by people who have logged in and set their preferences  - look in DB for contact strings within a GroupId that also have defined CPPPWD_vch</p>
<p>&nbsp;</p>
<h3>Consumer  UI</h3>
<p>The consumer UI is a public facing web site where the user passes in or enters a CPP UUID to start. <br />
  Based on UUID differing content is display - &quot;Welcome to XYZ Company's Customer Preference Portal&quot;
</p>
<p>Some of the custom fields (like Company Name) in the Consumer UI can pull data from the simpleobjects.useraccount table by getting the CPPUID and matching to the EBM System UserID_int associated with it.</p>
<p>The Consumer then uses their  email address to login.</p>
<p>Consumer can request  a password be sent to them the first time they try to manage   their preferences. If this is the case system needs to create a UUID as a temporary password and then email it to the customer for verification. </p>
<p>Reset password - I Forgot Option.</p>
<p>Option to Require Multi-Factor Authentication every time - passwords only good for one session and are reset to null on first use.</p>
<p>If a new UUID is generated then a verification email must be sent to allow the user to re-log in.</p>
<p>If a new UID is generated all CPPPWD_vch fields need to be updated in RXMultiList that contain a matching CPPID_vch for a given EBM UserID_int. There is only one password at a time and any changes must be made across  all of a contacts related contactstrings. Remember all contact strings are related under a given EBM UserID_int by their CPPID_vch. The CPPID_vch is alwyas a valid email address.</p>
<p>  Consumer can go to EBM public section to manage preferences<br />
  Consumer can go through AJAX HTML plugin code on an EBM User's Company site section to manage preferences</p>
<p>Consumer must login to manage CPP<br />
  Consumer can set their password first time they log in or continuously request a new one through email each time they log in.<br />
  Consumer can add/remove/change contact strings related to the master eMailAddress in the RXMultiList<br /> 
  Consumer can order in priority their contact strings - see new field in RXMultiList for 
CPPPriority_vch</p>
<p>Consumer is shown list of possible Groups to subscribe to:<br />
  Each Group can have differing levels of subscriptions - some may want voice and email - others may only want email. Under a list of each Group the customer may request all or only certain contact strings.
</p>
<p>If there is only one Group then the display should only show a list of contact strings.</p>
<p>Consumer can specify one or more Voice phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked</p>
<p>Consumer can specify one or more SMS phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
</p>
<p>Consumer can specify one or more eMail phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
</p>
<p>Password is stored for each CPPID encrypted with mySQL AES encryption using the SESSION.EncryptionKey_UserDB<br />
  ... 
AND Password_vch = AES_ENCRYPT(&lt;CFQUERYPARAM CFSQLTYPE=&quot;CF_SQL_VARCHAR&quot; VALUE=&quot;#inpPassword#&quot;&gt;, '#APPLICATION.EncryptionKey_UserDB#')</p>
<p>&nbsp;</p>
<h3>Embeded UI</h3>
<p>Authentication to work like in EBMAPI.cfc where a company can set a password for the CPP that only they can use internally as pasrt of AJAX calls into our system. Customer would not leave their site but storage of contact information would be in EBM structures.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4>Sample embeded Consumer UI</h4>
<p><img src="CPPSample1.jpg" width="821" height="1573" /></p>
<p>&nbsp;</p>
<h3>API</h3>
<p>&nbsp;</p>
<p>The data will be stored in the new DB column created with above alter table statement.<br />
  The data is optional but must be NULL or '' by default if not provided<br />
  Text area to enter values<br />
  Allow entry of single range or multiple ranges separated by commas<br />
  Allow newlines - I will strip them out on load from DB<br />
Ranges will be expect to follow the format xxx.xxx.xxx.xxx-xxx.xxx.xxx.xxx</p>
<p>Single IPs will still need to be entered as range with both ends of the range being equal<br />
  204.201.12.9-204.201.12.9</p>
<p>A list might look like<br />
  204.201.12.0-204.201.12.234,<br />
  62.200.109.14-62.200.109.14,<br />
  197.12.34.200-197.12.34.220</p>
<p>Or Could be just a single range<br />
  204.201.12.49-204.201.12.100</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Also:</p>
<p>The EBM user will need to be able to set a password for the CPP API access also. If no password is set then no API access will be granted. <br />
  Password entry will need to force strong passwords - <br />
  8 or more characters - up to 255<br />
  Require numbers<br />
  Require at least one upper and lower case characters<br />
  Require at least one special character<br />
  Maybe give them a password strength meter <br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
  Marketing Material Source - Us<br />
  http://ebmcloud.com/public/ebm/tools/cpp/about_cpp.cfm<br />
  http://dev.telespeech.com/jimp/hammertime/signup1.cfm<br />
  http://aig.mbwebportal.com/<br />
</p>
<p>Static Version 1<br />
  https://ebm.messagebroadcast.com/session/account/home</p>
<p>https://contactpreferenceportal.com/public/CPP/signin.cfm?inpCPPUUID=1362757355<br />
</p>
<p>Tarball V2 - internal<br />
  http://dev.telespeech.com/devjlp/EBM_DEV/session/account/home<br />
</p>
<p>Current following strict coding standards (on GitHub )<br />
  http://www.eventbasedmessaging.tk/devjlp/NEW_EBM/session/marketing/cpp/cpp.cfm</p>
<p>Documentation:<br />
  http://dev.telespeech.com/DEVJLP/EBM_DEV/management/Docs/EBMTraining/doc_CustomerPreferencePortal.cfm</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Competition<br />
  http://www.mypreferences.com/<br />
  http://www.nuance.com/for-business/by-solution/customer-service-solutions/solutions-services/outbound-contact-center-solutions/nuance-notification-hub/index.htm<br />
</p>
<p>&nbsp;</p>
<p></p>
<p></p>
<p></p>
<p></p>
</body>
</html>