<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Dynamic Data</h1>
<p>&nbsp;</p>
<p>Custom Elements are another important concept for  understanding how EBM is supposed to work: Let me know if this still does not  make sense. Everyone on the team needs to understand this concept.</p>
<p>&nbsp;</p>
<h3>Data Driven Messages</h3>
<p>  Whether it is Voice, eMail, SMS, or other IP based  messaging, there are many situations where unique customer information needs to  be incorporated in customized message content.</p>
<p>This is done in EBM by direct data replacement or through  control flow switching logic.</p>
<p>1. Data is uploaded into the simplexlists.rxmultilist using  ETL tools whithing the EBM system. This data can be modified individually using  list tools<br />
  2. Content is created and stored in XMLControlString_vch in  the simplexobjects.batch table. This content can contain special tags  {%Field_Name%} that will be replaced on distribution.<br />
3. While populating simplexqueue.contactqueue customized  XMLControlStrings_vch are created for each message based on the master in  XMLControlString_vch in the simplexobjects.batch table. Anywhere a custom data  string is defined {%Field_Name%} , it is replaced with the value of the  corresponding Field_Name&lsquo;s data for each contact string the message(s) is/are  sent to. The custom data fields can be used as part of switch statements or as  direct replacements for text values in TTS strings. Custom data files can also  be used as input into CONV objects – things like account balance and dates that  need to be unique to each message.</p>
<p>&nbsp;</p>
<h3>How Data Gets In:</h3>
<p>  Contact Strings can be entered individually<br />
  <img src="doc_DynamicData_clip_image001.jpg" alt="1" width="624" height="316" /><br />
  And<br />
  <img src="doc_DynamicData_clip_image002.jpg" alt="2" width="624" height="377" /><br />
  <img src="doc_DynamicData_clip_image003.jpg" alt="3" width="624" height="751" /></p>
<p>Or<br />
  Through file Uploads using ETL tools<br />
<img src="doc_DynamicData_clip_image004.jpg" alt="4" width="624" height="406" /></p>
<p>&nbsp;</p>
<p>1. Data is uploaded into the simplexlists.rxmultilist using  ETL tools whithing the EBM system. This data can be modified individually using  list tools<br />
  2. Content is created and stored in XMLControlString_vch in  the simplexobjects.batch table. This content can contain special tags {%Field_Name%}  that will be replaced on distribution.<br />
  3. While populating simplexqueue.contactqueue customized  XMLControlStrings_vch are created for each message based on the master in  XMLControlString_vch in the simplexobjects.batch table. Anywhere a custom data  string is defined {%Field_Name%} , it is replaced with the value of the  corresponding Field_Name&lsquo;s data for each contact string the message(s) is/are  sent to. The custom data fields can be used as part of switch statements or as  direct replacements for text values in TTS strings.</p>
<p>&nbsp;</p>
<h3>Queued Values</h3>
<p>While data from the user is replaced in the XMLControlString prior to inserting into the queue, certain fields are best left for swap out when moving from the queue to distribution&gt;</p>
<p>This is done in management/distribution/act_TransferQueue.cfm</p>
<p>Any text that matches  a specific string <br />
REPLACETHISUUID - this will be replaced with the UUID that was generated while inserting into the Queue table.<br />
REPLACETHISCONTACTSTRING- this will be replaced with the Contactstring that was used while inserting into the Queue table.<br />
REPLACETHISBATCHIDSTRING- this will be replaced with the BatchID that was used while inserting into the Queue table.<br />
REPLACETHISDQDTSIDSTRING- this will be replaced with the DTSID that was generated while inserting into the Queue table.<br />
</p>
<p>&lt;!--- UUID ---&gt; <br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch, &quot;REPLACETHISUUID&quot;, &quot;#inpCurrUUID#&quot;, &quot;ALL&quot;)&gt;<br />
  <br />
  &lt;!--- Contact String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch, &quot;REPLACETHISCONTACTSTRING&quot;, &quot;#inpDialString#&quot;, &quot;ALL&quot;)&gt;<br />
  <br />
  &lt;!--- Batch String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch, &quot;REPLACETHISBATCHIDSTRING&quot;, &quot;#INPBATCHID#&quot;, &quot;ALL&quot;)&gt;<br />
  <br />
  &lt;!--- Distrubution Queue DTS Id String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch, &quot;REPLACETHISDQDTSIDSTRING&quot;, &quot;#inpDQDTSId#&quot;, &quot;ALL&quot;)&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>