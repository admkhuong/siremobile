<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
  Install git<br />
  Setup Git - per Github instructions<br />
Test connection</p>
<p>Clone Repository<br />
  git clone git@github.com:MESSAGEBROADCAST/Event-Based-Messaging</p>
<p>Clone will pull entire project down<br />
  Switch to project directory<br />
  cd Event-Based-Messaging<br />
  <br />
  checkout desired branch<br />
  git checkout WebDevelopment<br />
  <br />
  Verify your are in correct branch - * next to current branch<br />
  git branch </p>
<p>Use the set-upstream arg:<br />
  git branch --set-upstream WebDevelopment origin/WebDevelopment<br />
  Running the above command updates your .git/config file correctly and even verifies with this output:<br />
</p>
<p>To add/track any new files<br />
  git add .</p>
<p>to commit changes to current branch<br />
  git commit -m 'your message and notes here' -a<br />
  <br />
  to push to current branch<br />
  git push<br />
  <br />
</p>
</body>
</html>