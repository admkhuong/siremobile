<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p><strong>Sofwares to install :</strong><br />
  1) GIT [<a title="http://code.google.com/p/msysgit/downloads/list?can=3" href="http://code.google.com/p/msysgit/downloads/list?can=3" rel="external nofollow" target="_blank">http://code.google.com/p/msysgit/downloads/list?can=3</a>] [necessary, below two are optional]<br />
  2) TortoiseGit [<a title="http://code.google.com/p/tortoisegit/downloads/list" href="http://code.google.com/p/tortoisegit/downloads/list" rel="external nofollow" target="_blank">http://code.google.com/p/tortoisegit/downloads/list</a>]<br />
  3) GitWeaver [<a title="https://github.com/ChrisMcKee/gitweaver/downloads" href="https://github.com/ChrisMcKee/gitweaver/downloads" rel="external nofollow" target="_blank">https://github.com/ChrisMcKee/gitweaver/downloads</a>]</p>
<p> </p>
<p><br />
  <strong>Configuring Git on your machine:</strong><br />
  Open Git Bash<br />
  1) $ git config --global user.name &quot;Deepak Yadav&quot;<br />
  2) $ git config --global user.email <a title="mailto:dyadav@messagebroadcast.com" href="mailto:dyadav@messagebroadcast.com" rel="external nofollow" target="_blank">dyadav@messagebroadcast.com</a><br />
  3) $ git config --global merge.tool c:/examdiff/examdiff.exe [Not necessary]<br />
  <br />
  [generating ssh key on your machine and putting that on github]<br />
  4) $ cd ~/.ssh</p>
<p><br />
  5) $ ssh-keygen -t rsa -C &quot;<a title="mailto:your_email@youremail.com" href="mailto:your_email@youremail.com" rel="external nofollow" target="_blank">your_email@youremail.com</a>&quot;<br />
  [press 'enter' for 'enter file where you want to save the key', and for 'enter passphrase']<br />
  6) Add your SSH key to GitHub.<br />
  On the GitHub site Click &ldquo;Account Settings&rdquo; &gt; Click &ldquo;SSH Public Keys&rdquo; &gt; Click &ldquo;Add another public key&rdquo;<br />
  <br />
  Test:<br />
  $ ssh -T <a title="mailto:git@github.com" href="mailto:git@github.com" rel="external nofollow" target="_blank">git@github.com</a><br />
  [Press enter if it asks to conitnue, or it will show the success message]<br />
  <br />
  7) Set your GitHub token.<br />
  Some tools connect to GitHub without SSH. To use these tools properly you need to find and configure your API Token.<br />
  On the GitHub site Click &ldquo;Account Settings&rdquo; &gt; Click &ldquo;Account Admin.&rdquo; and copy your 'token' and paste in Git bash using 'shift+insert'<br />
  8) $ git config --global github.user <em>your_username</em><br />
  9) $ git config --global github.token 0123456789yourf0123456789</p>
<p>Reference: <a title="http://help.github.com/win-set-up-git/" href="http://help.github.com/win-set-up-git/" rel="external nofollow" target="_blank">http://help.github.com/win-set-up-git/</a><br />
  <em>---Configuring Finished</em>------</p>
<p><br />
  <br />
  <strong>Putting existing project on git (2 ways):</strong><br />
  <br />
  <strong>1) Using Git bash (preffered for projects &gt; 2 MB size)</strong><br />
  1) cd Y:\DEV3.TELESPEECH\DEVJLP\EBM_dev [point git to the location of folder to be put on github]<br />
  2) git init<br />
  3) touch README<br />
  4) git add README<br />
  5) git commit -m 'first commit'<br />
  [Committing all changes in a repository<br />
  git commit -a]<br />
  6) git remote add origin <a title="mailto:git@github.com" href="mailto:git@github.com" rel="external nofollow" target="_blank">git@github.com</a>:MESSAGEBROADCAST/EBM.git<br />
  7) git push -u origin master<br />
  <br />
  If the project is big (or for some other reasons), the above comit and push commands dont push everything in github.<br />
  So we have to do it using below commands:<br />
  8) git add . [adds all the unversioned files in current directory and subdirectories]<br />
  9) git commit -m 'message for commit' [commits the added files from above command]<br />
  10) git push -u origin master<br />
  <br />
  <strong>2) Using Tortoise Git GUI</strong> [Only for small projects. It hangs and dont work while committing big projects]<br />
  1) Right click on folder, click 'git create repository here', click 'ok' without checking the checkbox(if it appears).<br />
  2) Right click, choose 'Git commit -&gt; master'.<br />
  3) Write comment and click on 'sign in', then select the unversioned file you want to commit.<br />
  4) Then select push right after committing.<br />
</p>
<p><strong><U>Connecting with Dreamweaver (2 steps)</U><br />
  First:: </strong><em>public-private key generation</em><br />
  1) Go to 'start menu' -&gt; 'all programs' -&gt; 'tortoiseGit' -&gt; 'Putty gen'<br />
  2) Click on 'generate', move your mouse for randomness on free area.<br />
  3) Copy 'public key' on top and paste it into your github account.<br />
  'Account setting'[on top right] -&gt; SSH Public Key[on left menu] -&gt; 'add another key'<br />
  4) Click 'Save private key' on your computer(we have to use that in dreamweaver)<br />
  <br />
  <strong>Second:: </strong><em>Configuring git in dreamweaver[You should have gitWeaver, a dreamweaver extesntion for git to start below steps]:</em><br />
  1) Right click on the file you want to commit. and click 'commit'.<br />
  2) click Sign.<br />
  3) after successful commit, click on 'Push' right away.<br />
  4) Go to destination -&gt; remote -&gt; click 'manage' [for the first time]<br />
  5) Add a new remote under 'Git'. Name the remote(example: MB_EBM_dev), put the url of git (example: <a title="mailto:git@github.com" href="mailto:git@github.com" rel="external nofollow" target="_blank">git@github.com</a>:MESSAGEBROADCAST/EBM.git), browse to your private key genrated by 'Key generator' in above steps.<br />
  6) Click 'Apply' -&gt; 'Ok'.<br />
  7) Chose newly created remote 'MB_EBM_dev' and click 'OK'<br />
  8) <strong>To pull changes</strong><strong>:</strong> right click on project and click 'pull', then choose the right remote to pull.<br />
</p>
<p><strong>How to pull project from Github to our webserver:</strong><br />
  1) Open Git bash,<br />
  2) cd c:/abc [path where you want to clone your repository from github]</p>
<p>[to get whole project for first time: git clone <a title="mailto:git@github.com" href="mailto:git@github.com" rel="external nofollow" target="_blank">git@github.com</a>:dyadav/testp.git]<br />
  3) git pull <a title="mailto:git@github.com" href="mailto:git@github.com" rel="external nofollow" target="_blank">git@github.com</a>:MESSAGEBROADCAST/BabbleSphere.git<br />
</p>
</body>
</html>