<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Life Cycle Events
</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Overview</h3>
<p>This is a tool to help enterprise clients  track LCE’s - In customer relationship management (CRM), customer life cycle  events (LCE) are a term used to describe the progression of steps a customer  goes through when considering, purchasing, using, and maintaining loyalty to a  product or service. Here is a matrix that breaks the customer life cycle into  five distinct steps: reach, acquisition, conversion, retention, and loyalty. In  layman's terms, this means getting a potential customer's attention, teaching  them what you have to offer, turning them into a paying customer, and then  keeping them as a loyal customer whose satisfaction with the product or service  urges other customers to join the cycle. The customer life cycle is often  depicted by an ellipse, representing the fact that customer retention truly is  a cycle and the goal of effective CRM is to get the customer to move through  the cycle again and again.</p>
<p>&nbsp;</p>
<p>Tasks:</p>
<p>Please review and understand existing code first<br />
  All DB Structures are in SimpleObjects and  begin with </p>
<p>LCEList  - Each user account has their own list of Life  Cycle events list and they all start at 1 for each user - Add-edit-delete<br />
  LCEItems  - Each list contains one or more items in a  map - Add-edit-delete<br />
  LCELinkInternalContent - Each item is  mapped backed to one or more Campaigns/BatchIDs . Add-edit-delete <br />
  LCELinkExternalContent  - Optionally each item can be mapped to other  objects such as whitepapers and ROI calculators – these objects don’t exist yet</p>
<p>These two tables do not exist yet.<br />
  LCEWhitePapers - Tool to track white papers  – Add, Edit, Delete<br />
  LCEROI – Tool to track ROI calculators –  Add, edit, delete</p>
<p>Batch Picker – give UI an ability to choose  one or more batches – something similar already existing in the reporting tools  for Campaign/batches</p>
<p>Graphics and images are just place holders  now. New images and colors will be needed to make this look more professional.  Google search on CRM tools for ideas.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Data Structures</h3>
<p>CREATE TABLE `lcelist` (<br />
  `LCEListId_int` int(11) NOT NULL,<br />
  `UserId_int` int(11) NOT NULL,<br />
  `Desc_vch` text,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  `LastUpdated_dt` datetime DEFAULT NULL,<br />
  `Active_int` int(4) DEFAULT '1',<br />
  PRIMARY KEY (`LCEListId_int`,`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>CREATE TABLE `lceitems` (<br />
  `LCEListId_int` int(11) NOT NULL,<br />
  `LCEItemId_int` int(11) NOT NULL,<br />
  `UserId_int` int(11) NOT NULL,<br />
  `Desc_vch` text,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  `LastUpdated_dt` datetime DEFAULT NULL,<br />
  `Active_int` int(4) DEFAULT '1',<br />
  PRIMARY KEY (`LCEListId_int`,`LCEItemId_int`,`UserId_int`),<br />
  KEY  `IDX_Active_int` (`Active_int`)<br />
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>CREATE TABLE `lcelinksinteralcontent` (<br />
  `LCEListId_int` int(11) NOT NULL,<br />
  `LCEItemId_int` int(11) NOT NULL,<br />
  `UserId_int` int(11) NOT NULL,<br />
  `BatchId_bi` int(11) NOT NULL,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`LCEItemId_int`,`UserId_int`,`BatchId_bi`)<br />
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>CREATE TABLE `lcelinksexternalcontent` (<br />
  `LCEListId_int` int(11) NOT NULL,<br />
  `LCEItemId_int` int(11) NOT NULL,<br />
  `UserId_int` int(11) NOT NULL,<br />
  `BatchId_bi` int(11) NOT NULL,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  `ProgramInfo_vch` text,<br />
  `Start_dt` datetime DEFAULT NULL,<br />
  `End_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`LCEItemId_int`,`UserId_int`,`BatchId_bi`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>