<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>MCID Control Keys</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>A basic voice  campaign is made up of the following elements which dictate what the XML  Control String will play when the call goes out.</p>
<ul>
  <li><strong>Dynamic Message</strong></li>
  <li><strong>Message Type</strong></li>
  <li><strong>Script Library</strong></li>
  <li><strong>Play Logic (Play Type)</strong></li>
  <li><strong>Call Control Data</strong></li>
</ul>
<p><strong>&nbsp;</strong></p>
<p><strong>Dynamic Message</strong><br />
  A Dynamic Message  is a message that is made up of multiple text to speech (TTS) attributes that  could include unique message identifiers specific to the call recipient&rsquo;s  personal informational needs.  </p>
<p><strong>Message Type</strong><br />
  A message type  signifies the purpose of the message. Some examples of standard message types  are:</p>
<ul>
  <li>Statement</li>
  <li>Interactive Response</li>
  <li>Data Capture</li>
  <li>Audio Capture</li>
  <li>Live Agent Transfer (LAT)</li>
  <li>Opt Out</li>
</ul>
<p><strong>Script Library</strong> <br />
  As previously  stated, a script library contains all of the recordings necessary to complete  the voice campaign program build.</p>
<p><strong>Play Logic (Play Type)</strong><br />
  When a campaign  is launched and calls are dialed the MB systems detects whether or not the call  placed is received by a live person, answering machine, or other which could  include a disconnected number recording. Play logic is the set of parameters  that the client and Account Manager determine will make the outcome of a  campaign successful keeping in mind percentage allocation to how calls placed  are received. </p>
<p><strong>CCD (Call Control Data)</strong><br />
  Contains information needed to enable  a call such as:</p>
<ul>
  <li>Batch ID</li>
  <li>Campaign Type</li>
  <li>Script Library</li>
  <li>XML Control String</li>
  <ul>
    <li>Voice  XML Control Strings</li>
    <li>MCID  XML Control Strings</li>
  </ul>
</ul>
<p><strong>Steps  for Launching a Basic Voice Campaign</strong><br />
  The following are the due diligence  steps that are to be followed between the Account Manager and client in order  to launch a voice campaign.</p>
<ul>
  <li>Identify  the required message components (e.g. Greeting, Message, Purpose of Call,  Desired Action by Call Recipient, etc.).</li>
  <li>Assign  Message Component Identifications (MCIDs)</li>
  <li>Identify  necessary scripts to support the message components</li>
  <ul>
    <li>There  are generally recognized phrases and message compliant verbiage that are  necessary for voice campaigns.</li>
  </ul>
  <li>Submit  and gain formal client approval</li>
  <li>Submit  and gain formal legal counsel approval</li>
  <li>Outsource  voice talent recording</li>
  <li>Create  Script Library for programming </li>
</ul>
<p><strong>User Spec Data</strong><br />
  User specific  data is customized data supplied by the client to add to the campaign XML  string for post-campaign export. The data will then be used for analysis and reporting.  This data is contained within the <em>Call  Control Data</em> or <em>CCD. </em>Data options  would include the following codes and definitions: </p>
<div align="center">
  <table border="1" cellspacing="0" cellpadding="0">
    <tr>
      <td width="618" colspan="3"><p align="center"><strong>&nbsp;</strong></p></td>
    </tr>
    <tr>
      <td width="172"><p align="center"><strong>Acronyms</strong></p></td>
      <td width="172"><p align="center"><strong>Name</strong></p></td>
      <td width="274"><p align="center"><strong>Description</strong></p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>BS</p></td>
      <td width="172" valign="top"><p>Built    Script</p></td>
      <td width="274" valign="top"><p>A    recorded script that will be used for a voice campaign</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>CID</p></td>
      <td width="172" valign="top"><p>Caller    Identification</p></td>
      <td width="274" valign="top"><p>The    telephone number that is transmitted and displayed to the recipient of a    phone call</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>CK</p></td>
      <td width="172" valign="top"><p>Control    Key</p></td>
      <td width="274" valign="top"><p>Attributes    allocated to an MCID</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>DM    LIB </p></td>
      <td width="172" valign="top"><p>Dynascript    Library</p></td>
      <td width="274" valign="top"><p>Denotes    a recording library located in Dynascript</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>DRD</p></td>
      <td width="172" valign="top"><p>Do    Redial</p></td>
      <td width="274" valign="top"><p>Perform    redial for campaign</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>DS</p></td>
      <td width="172" valign="top"><p>Dynamic    Script Library</p></td>
      <td width="274" valign="top"><p>Denotes    script located in Dynascript</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>DSE</p></td>
      <td width="172" valign="top"><p>Dynamic    Script Element</p></td>
      <td width="274" valign="top"><p>Denotes    an MCID recording in Dynascript</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>ELE    ID</p></td>
      <td width="172" valign="top"><p>Element    Identification</p></td>
      <td width="274" valign="top"><p>A    number assigned to a recording portion of an MCID. Sequential recordings are    organized with element identification numbers. </p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>ELE    QID</p></td>
      <td width="172" valign="top"><p>Element    Question ID</p></td>
      <td width="274" valign="top"><p>A    number assigned to a question within a call survey programmed for a client.    The ELE QID organizes survey questions and is used for outcome reporting.</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>MT</p></td>
      <td width="172" valign="top"><p>Message    Type</p></td>
      <td width="274" valign="top"><p>Identifies    the purpose of the message</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>FileSeq</p></td>
      <td width="172" valign="top"><p>File    Sequence</p></td>
      <td width="274" valign="top"><p>Used    to help identify the datafeed for daily, weekly, monthly campaigns that reuse    the same batch information.</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>MT=1</p></td>
      <td width="172" valign="top"><p>Live    Message Type</p></td>
      <td width="274" valign="top"><p>Message    Statement is meant to be received by a live recipient </p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>MT=2</p></td>
      <td width="172" valign="top"><p>Voicemail    Message Type</p></td>
      <td width="274" valign="top"><p>Message    Statement is meant for receipt by a voicemail</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>PT=1</p></td>
      <td width="172" valign="top"><p>Play    Type</p></td>
      <td width="274" valign="top"><p>Identifies    recipient of call</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>PTL</p></td>
      <td width="172" valign="top"><p>Play    to Live</p></td>
      <td width="274" valign="top"><p>Recipient    of call is to be a live person</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>PTM</p></td>
      <td width="172" valign="top"><p>Play    to Machine</p></td>
      <td width="274" valign="top"><p>Recipient    of call is to be a voicemail</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>RXSS</p></td>
      <td width="172" valign="top"><p>Reaction    X Survey System</p></td>
      <td width="274" valign="top"><p>Contains    all required information to run a batch</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>rxt</p></td>
      <td width="172" valign="top"><p>Reaction    X Type</p></td>
      <td width="274" valign="top"><p>An    rxtype is an MCID. It is used as a programming type label and denotes each    step in the deployment of a call.</p></td>
    </tr>
    <tr>
      <td width="172" valign="top"><p>UserSpecData</p></td>
      <td width="172" valign="top"><p>User    Specific Data</p></td>
      <td width="274" valign="top"><p>Stored    customer data that is exported for reporting</p></td>
    </tr>
  </table>
</div>

<div>
<p></p>
<p></p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>As of RXDialer version 24.0.0.1</p>
<p>Added RQ for Report Question - set RQ='1' or greater than 0 in the MCID string to add the reult will also have this identifier.</p>
<p>Added CPfor Check Point - set CP='1' or greater than 0 in the MCID string to add the reult will also have this identifier.</p>
<p>Watch old reporting that looks for only &lt;Q ID='X'&gt; Logic....</p>
<p><br />
</p>
<p><br />
</p>
<p>// RX Question Type rxtId[]<br />
  // 1 = Statement - no IVR options<br />
  // RXSSCK1[CurrMCID] = DTMFs To Ignore before continuing on<br />
  // RXSSCK2[CurrMCID] =<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  <br />
  // 2 = IVR 1-9 with option to repeat (-13 is #) (-6 is *) and (-1 is no response)<br />
  //       Hold - just a special instance of IVR<br />
  <br />
  // RXSSCK1[CurrMCID] = How many retries allowed<br />
  // RXSSCK2[CurrMCID] = Valid Responses (1,2,3 ect) (-13 is #) (-6 is *) and (-1 is no response (NR))<br />
  // RXSSCK3[CurrMCID] = Repeat Key (-13 is #) (-6 is *)   (-1 is NR) - will allow * and # to specified directly - if -1 is   specified then a valid audio file must be played as part of CK7<br />
  // RXSSCK4[CurrMCID] = Answer MAP -   '(1,x),(2,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(-13,x),(-6,x),(-1,x)'<br />
  // RXSSCK5[CurrMCID] = Next QID - no match   // Was Opt   Out Option - Enclosed in () so -13 and 1 dont both return positive<br />
  // RXSSCK6[CurrMCID] = MCID - No Entry Message<br />
  // RXSSCK7[CurrMCID] = MCID - Invalid Response Message<br />
  // RXSSCK8[CurrMCID] = Max number of no Response - default is 5 &lt;Q ID='CURRMCID'&gt;TMNR&lt;/Q&gt;<br />
  // RXSSCK9[CurrMCID] = MaxDigitDelay - MIN 1 MAX 150   --- Max pause after audio file finishes reading question while waiting   for input - 100ms increments 30=3 seconds and 25=2.5 seconds - RX   version 23.0.0.4 and up<br />
  <br />
</p>
<p><br />
              // 3 = Record a response - with erase and re-record options<br />
            // Results stored in   </p>
<p>&nbsp;</p>
<p>           // 3 = Record a response - with erase and re-record options</p>
<p>// Setup answer file        </p>
<p>  // Results stored in mediaServerPath\Results\UserId\BatchId<br />
  if(RXSSCK13[CurrMCID].GetLength() &gt; 0)<br />
  SurveyResultsFilePath.Format(&quot;%s\\Results\\%d\\%s\\SurveyResults_%d_%s_%d_%s.wav&quot;,   mediaServerPath, UserId, BatchId, CurrMCID, inpPhoneStr,   CurrentRedialCount,RXSSCK13[CurrMCID]);<br />
  else<br />
  SurveyResultsFilePath.Format(&quot;%s\\Results\\%d\\%s\\SurveyResults_%d_%s_%d.wav&quot;,   mediaServerPath, UserId, BatchId, CurrMCID, inpPhoneStr,   CurrentRedialCount);<br />
</p>
<p><br />
  // Results stored in   mediaServerPath\Results\UserId\BatchId\SurveyResults_CurrQID_inpPhoneStr_CurrentRedialCount.wav&quot;</p>
<p> </p>
<p><br />
  // Answer 0=no recording/error/hang-up  1=saved    2=declined</p>
<p>Ver 20.0.0.7 and up for latest features<br />
  // RXSSCK1[CurrMCID] = How many re-records allowed - specify 1 to skip any<br />
  // RXSSCK2[CurrMCID] = How long to record in seconds<br />
  // RXSSCK3[CurrMCID] = MCID - At the tone please record   your answer... - specify 0 or leave blank to skip this message<br />
  // RXSSCK4[CurrMCID] = MCID - Your message exceeded the   maximum length - specify 0 or leave blank to skip this message<br />
  // RXSSCK5[CurrMCID] = MCID - Play your message will sound   like... - specify 0 or leave blank to skip this message<br />
  // RXSSCK6[CurrMCID] = MCID - To erase, save, repeat press 1,2,3... only can be skipped for CK1=1<br />
  // RXSSCK7[CurrMCID] = MCID - Your message has been saved - specify 0 or leave blank to skip this message<br />
  // RXSSCK8[CurrMCID] = Next QID<br />
  // RXSSCK9[CurrMCID] = Accept Key - default 1 if blank<br />
  // RXSSCK10[CurrMCID] = Erase Key - default 2 if blank<br />
  // RXSSCK11[CurrMCID] = Repeat Key - default 3 if blank<br />
  // RXSSCK12[CurrMCID] = Output file format (default if 0   or blank)=Linear PCM - 1=GSM610 Microsoft - 2=G.726 - 3=G.711 MuLaw -   4=ADPCM Dialogic - NOTE: Append option only works with 0 or blank -   Linear PCM - 5 = mp3(if MP3 then default extension is .MP3)<br />
  // RXSSCK13[CurrMCID] = String to append to recorded   results file name  - if not blank adds an _XXX to end of file name  //   RXSSCK14[CurrMCID] = Append Key - default 4 if blank - NOTE: Append   option only works with 0 or blank - Linear PCM<br />
  //   RXSSCK15[CurrMCID] = MCID - Append version of At the tone please record   your answer... - specify 0 or leave blank to skip this - NOTE: Append   option only works with 0 or blank - Linear PCM</p>
<p>               // RXSSCK16[CurrMCID] = UNC Path to copy file too - optional - Specify &quot;PBX&quot; to use RXDialer Reg Path for PBX<br />
  // RXSSCK17[CurrMCID] = New File Name if copy to UNC   path specified - if blank just default recording name - Special case if   &quot;PBX&quot; - will auto create a new message in PBX box using 16,19,20<br />
  Special keywords will be replaced dynamically<br />
  // Replace USERID<br />
  // Replace BATCHID<br />
  // Replace INPADDRESS<br />
  // Replace QID<br />
  // Replace REDIALNUM</p>
<p><br />
  // RXSSCK18[CurrMCID] -- not used now - was user id<br />
  // RXSSCK19[CurrMCID] PBX Account Number - Phone Number -   only if 16 is special case of &quot;PBX&quot; - - Can also use CDS strings<br />
  // RXSSCK20[CurrMCID] PBX Extension - only if 16 is special case of &quot;PBX&quot; - Can also use CDS strings   <br />
  // RXSSCK21[CurrMCID] - Do file analysis - anything   greater than one will do this. File will remain in final location as set   by other control keys but additional analysis values will be stored in   the reults string under the current QID.</p>
<p> </p>
<p><br />
  <br />
  <br />
  // 4 = LAT<br />
  // RXSSCK1[CurrMCID] = LAT number<br />
  // RXSSCK2[CurrMCID] = Play Transferring Message -   Transfer message is specified by the QID's DS, DSE, and DI WARNING -   invalid values here will cause transfer to fail<br />
  // RXSSCK3[CurrMCID] = DTMFs To Ignore before continuing on past transfer message<br />
  // RXSSCK4[CurrMCID] = LATCallerIdNumber<br />
  // RXSSCK5[CurrMCID] = MCID for the Hold while transfering Message<br />
  // RXSSCK6[CurrMCID] = MAX timeout in seconds for LAT -   Just hangup if this value exceeded or infinite if this value is -100   -&gt; NULL,0 or Less defaults to 14400 - Four hours<br />
  // RXSSCK7[CurrMCID] = MCID for the &quot;Whisper Greeting&quot; Message - will   auto continue bridge on keypress - will auto continue bridge when   message completes<br />
  // RXSSCK8[CurrMCID] = 0 = off 1 = Call Recording on --- 0 is default if blank<br />
  // Final recording path will be stored on media Server   (Reg setting on RXDialer) under the dir of the LATNumber called   <br />
  // The final file name is formated as   FinalFileName.Format(&quot;CallRec_%d_%s_%s_%s&quot;, inpRXLD1,   CurrTimeStamp.Format(_T(&quot;%Y%m%d%H%M%S&quot;)), PhoneStrLiveTransfer,    LATCallerIdNumber);<br />
  // BuffOutStr.Format(&quot;&lt;Q   ID='CallRECFileName'&gt;%s&lt;/Q&gt;&quot;, FinalFileName) is added to result   string for call<br />
  // NOTE: No default ring back will be heard if you use any of the following options:<br />
  // Whisper Greeting<br />
  // Hold Music<br />
  // IVR Navigation<br />
  // Call Recording<br />
  // Added NOTE - You can fake it by playing hold music that sounds like ringing   </p>
<p>                  // More Notes: When original outbound leg of call hangs up first during   a bridged call then isHangupDetected = 1 - when LAT leg of bridge hangs   up isHangUpDetected = 2</p>
<p>// more notes If original CFTE/LAT MCID   string did not include a CK4 for the Caller ID to be broadcast on the   LAT leg. Default behavior is to broadcast the number of the original   outbound leg as the CID for the LAT leg. When adding extension   navigation logic, the caller ID is now containing invalid characters.   Some telecom can handle this while others cannot and will just terminate   the call. The solution is when dialing a number with extension   information to be sure to specify a CK4 for MCID type 4 without the   extension information.</p>
<p><br />
  // 5 = Opt Out<br />
  // RXSSCK1[CurrMCID] =<br />
  // RXSSCK2[CurrMCID] =<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  <br />
  // 6 = IVR multi string - press pound or five second pause to complete entry<br />
  // RXSSCK1[CurrMCID] = Max number of digits to retrieve<br />
  // RXSSCK2[CurrMCID] = Place to store in array of   previous digit strings 1-10 for now - retrieve in other MCIDs as CDS1 or   CDS2 ... CDS10<br />
  // RXSSCK3[CurrMCID] = Max   Inter-Digit Delay in seconds - default is 5 seconds if this number is   not between 1 and 29<br />
  // RXSSCK4[CurrMCID] = Response   Map - Optional - full string match -   '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
  // RXSSCK5[CurrMCID] = Next QID - no match<br />
  // RXSSCK6[CurrMCID] = <s>any value besides &quot;&quot; skip script read- just capture digits   </s>For consistancy this is now changed to &quot;&quot; is equivalent to &quot;0&quot; and and   positive integer will cuase this to skip the reading of the current   script and just capture digits.<br />
  // RXSSCK7[CurrMCID] = Minimum Digits required or skip   to CK8 - CK7 and 8 must be greater than 0 for this to take affect<br />
  // RXSSCK8[CurrMCID] = QID to goto next if not minumum   digits - CK7 and 8 must be greater than 0 for this to take affect<br />
  // RXSSCK9[CurrMCID] = MCID - No Entry Message<br />
  // RXSSCK10[CurrMCID] = MCID - Invalid Response / No Match Message<br />
  // RXSSCK11[CurrMCID] = Max number of retries - Max times this MCID will be allowed to repeat - default is 0<br />
  // RXSSCK12[CurrMCID] = Max number of no Response - default is 5 &lt;Q ID='CURRMCID'&gt;TMNR&lt;/Q&gt;<br />
  <br />
  <br />
  <br />
  // 7 = Opt In - 10 digit string<br />
  // RXSSCK1[CurrMCID] = SP1 - MCID for - Thank-you. You are calling from XXX-XXX-XXXX.<br />
  // RXSSCK2[CurrMCID] = SB2 - MCID for - If This Is Your Number   <br />
  // RXSSCK3[CurrMCID] = SB3 - MCID for - Number Added to opt in list<br />
  // RXSSCK4[CurrMCID] = SB4 - MCID for - Enter Phone Number<br />
  // RXSSCK5[CurrMCID] = SB5 - MCID for - You Entered Number<br />
  // RXSSCK6[CurrMCID] = SB6 - MCID for - Invalid Response<br />
  // RXSSCK7[CurrMCID] = Zero start MCID path ID - the next 9 need to be 1-9 in order<br />
  // RXSSCK8[CurrMCID] = Next QID<br />
  <br />
  // OR...<br />
  // IF RXSSCK7[CurrMCID] = -100<br />
  // THEN<br />
  // RXSSCK9[CurrMCID] - Master Script library ID<br />
  // RXSSCK10[CurrMCID] - Two digit element ID<br />
  // RXSSCK11[CurrMCID] - Three digit element ID<br />
  <br />
  // RXSSCK12[CurrMCID] = Dumb ass people want a   different message on the second loop for &quot;If This Is Your Number&quot; -   other alternative is to build as full IVR<br />
  <br />
  // Result string contains opt in number - &quot;&lt;Q ID='CurrMCID'&gt;OptInSelected&lt;/Q&gt;&quot;<br />
  // For reporting purposes IsOptIn flag = 1 if same   number called and IsOptIn flag = 2 if different number is entered</p>
<p _cke_saved_style="margin-left: 6em;">// OR...<br />
  // IF RXSSCK13[CurrMCID] != &quot;&quot; - Value to store - Special case for CDS1-10 and (ANI) AND (IBD) for inbound dialed<br />
  // THEN<br />
  // Just opt in and go to next RXSSCK8[CurrMCID] = Next QID<br />
// Adds the value of RXSSCK13[CurrMCID] to the XMLResultString under   the current QID - Value to store - Special case for CDS1-10 and (ANI) AND (IBD) for inbound dialed</p>
<p><br />
  <span _cke_saved_style="color:rgb(255,0,0);">Deprecated - Use CCD instead now</span><br />
  // 8 = Find me Follow Me FMFM<br />
  // RXSSCK1[CurrMCID] = Comma seperated list of numbers to contact<br />
  // RXSSCK2[CurrMCID] = 1 = Call til message left, 2 = Call for response, 3 = call all numbers  <br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] =<br />
  // RXSSCK6[CurrMCID] =<br />
  // RXSSCK7[CurrMCID] =<br />
  // RXSSCK8[CurrMCID] = Next QID<br />
  // Note: Still a work in progress - justs skips to next MCID for now<br />
  <br />
  // 9 = Read out previous/specified digit strings 1-10 - no IVR options<br />
  // RXSSCK1[CurrMCID] = Number of digits to read - used for things like right 4 digits - numeric<br />
  // RXSSCK2[CurrMCID] = Place to get data in array of previous digit strings 1-10 for now<br />
  // RXSSCK3[CurrMCID] = Zero start MCID path ID - the next 9 need to be 0-9 in order<br />
  // RXSSCK4[CurrMCID] = Specific Digit Sring to read -   any value here negates the CDS in CK2 - Special case the word &quot;(ANI)&quot; will   be replace with the number dialed for outbound and the number called   from for inbound AND (IBD) for inbound dialed<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  <br />
  <br />
  // 10 = SQL Query String - INSERT, UPDATE or SELECT (1,0)<br />
  // RXSSCK1[CurrMCID] = SQL Query String<br />
  // RXSSCK2[CurrMCID] = Make Hash Upper (only aplies when referencing CDSHA1S[x] and CDMD5S[x] arrays)<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] = Response Map - Optional - full   string match -   '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
// RXSSCK5[CurrMCID] = Next QID<br />
// RXSSCK6[CurrMCID] = Database connection name - most of the time IP Address<br />
  // RXSSCK7[CurrMCID] = NOT REALLY  USED - SECURITY   PROBLEMS - Database User name - make sure dialer DB loginis smae as   remote db login for best practices<br />
//   RXSSCK8[CurrMCID] = NOT REALLY  USED - SECURITY PROBLEMS - Database   password - make sure dialer DB loginis smae as remote db loginfor best   practices</p>
<p></p>
<p><br />
  // Note: use (CASE WHEN COUNT(*) &gt; 0 THEN 1 ELSE 0   END) instead of (COUNT(*)) so the results can be mapped back to answer   map - if the count can be greater than 1 you would need an infinite   answer map<br />
  // can use Special case for CDS1-10,    CDSHA1S1-10,  CDMD5S1-10, , CDSHA256S1-10, and (ANI) AND (IBD) for inbound dialed where the CDS's are   previously capture strings and CDSHA256S SHA1S and MD5S are hassed   versions of previouly captured strings<br />
  // Hashes are made with additonal XString added to end of original text as specified in RXDialer Registry<br />
  // Use Reg Tool in RXDialer to set key as it will be a hashed value itself in the registry<br />
  <br />
</p>
<p><br />
</p>
<p><br />
  // 11 = Play DTMFs - no IVR options<br />
  // RXSSCK1[CurrMCID] = String of DTMFs to play - '123456789*#'<br />
  // RXSSCK2[CurrMCID] =<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  <br />
  // 12 = Strategic Pause - line still listens for hangups but will puase for specified number of milli-seconds<br />
  // RXSSCK1[CurrMCID] = number of milli-seconds to pause for - 1 to 1000000 (1000 seconds max)<br />
  // RXSSCK2[CurrMCID] =<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  // Note: only good for first leg of call - if part of LAT use X999P999 logic instead</p>
<p>           // 13 = Email                <br />
  // RXSSCK1[CurrMCID] = Async Flag 1 = Async 0 or blank   for Syncronouse - Async releases line faster - good for inbound - not as   critical for outbound<br />
  // RXSSCK2[CurrMCID] = MessageWarning;                        <br />
  // RXSSCK3[CurrMCID] = MessageText;        <br />
  // RXSSCK4[CurrMCID] = MessageHTML;                            <br />
  // RXSSCK5[CurrMCID] = MessageFromUser;                        <br />
  // RXSSCK6[CurrMCID] = MessageToUser;    ; seperated list of email addresses;    <br />
  // RXSSCK7[CurrMCID] = MessageCCUser;     ; seperated list of email addresses;<br />
  // RXSSCK8[CurrMCID] = MessageSubject;    <br />
  // RXSSCK9[CurrMCID] = MessageCreationDate;    <br />
  // RXSSCK10[CurrMCID] = ConnFromUser;    <br />
  // RXSSCK11[CurrMCID] = ConnFromPassword;    <br />
  // RXSSCK12[CurrMCID] = AttachmentPath;    <br />
  // RXSSCK13[CurrMCID] = SMTP Server IP Address - if blank use local dialers Reg settings<br />
  // RXSSCK14[CurrMCID] = Optional UUID - if not   specified will be a combination of BatchID and Dialer specific DTS id<br />
  // RXSSCK15[CurrMCID] = Next QID<br />
  // RXSSCK16[CurrMCID] = Additional Headers - Multiple new headers  allowed but if you specify anything here each header has to be  terminated by \r\n Format is HeaderName:HeaderData\r\n<br />
    <br />
    // 14 = Store IGData for inbound calls<br />
    // RXSSCK1[CurrMCID] = IGData ID 1 - 10<br />
    // RXSSCK2[CurrMCID] = Value to store - Special case for CDS1-10 and (ANI) AND (IBD) for inbound dialed<br />
    // RXSSCK3[CurrMCID] =<br />
    // RXSSCK4[CurrMCID] =<br />
    // RXSSCK5[CurrMCID] = Next QID<br />
    <br />
    // NOTE:<br />
    // IGData1 is usually Option Selected<br />
    // IGData2 is usually CallerIdString;<br />
    // IGData3 is usually Opt Out Number<br />
    // IGData4 is usually LAT number<br />
    // IGData5 is usually LAT Call Time</p>
<p>// 15 = Play from absolute path<br />
  // RXSSCK1[CurrMCID] = DTMFs To Ignore before continuing on <br />
  // RXSSCK2[CurrMCID] = String for absolute path<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  <br />
  // 16 = Switch based on last response to a previous MCID<br />
  // RXSSCK1[CurrMCID] = Previous MCID to check<br />
  // RXSSCK2[CurrMCID] =<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] = Response Map - Optional - full   string match -   '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
  // RXSSCK5[CurrMCID] = Next QID if no match found</p>
<p><br />
</p>
<p>      // 17 = Switchout MCIDs based on SQL query - Ignores CCD changes -   Justs switches out MCID - only generates one path - no live/machine   options<br />
  // RXSSCK1[CurrMCID] = SQL Query String - MUST return a valid XML MCID string<br />
  // RXSSCK2[CurrMCID] = Start QID of new MCID<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] =<br />
  // RXSSCK5[CurrMCID] = Next QID if Blank -<br />
  // NOTE - Query Results must contain an XML string of MCIDs surrounded by &lt;RXSS&gt;&lt;/RXSS&gt; to work<br />
  // RXSSCK6[CurrMCID] = Database connection name - most of the time IP Address<br />
 // RXSSCK7[CurrMCID] = NOT REALLY  USED - SECURITY   PROBLEMS - Database User name - make sure dialer DB loginis smae as   remote db login for best practices<br />
 //   RXSSCK8[CurrMCID] = NOT REALLY  USED - SECURITY PROBLEMS - Database   password - make sure dialer DB loginis smae as remote db loginfor best   practices</p>
<p><br />
  Sample rxt 17 usage<br />
  <br />
  &lt;DM LIB='1274' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;RXSS&gt;<br />
  &lt;ELE QID='1' RXT='6' BS='0' DS='1274' DSE='12' DI='1' CK1='10'   CK2='1' CK3='5' CK4='(#,2),(-1,2),(*,2)' CK5='2'&gt;1&lt;/ELE&gt;<br />
  &lt;ELE QID='2' RXT='17' BS='0' DS='1274' DSE='12' DI='1' CK1='SELECT   XMLOut_txt FROM test.table2' CK2='1' CK5='-1'&gt;1&lt;/ELE&gt;<br />
  &lt;/RXSS&gt;<br />
  &lt;CCD CID='7777777777' IRC='F,B,N,O' MIRC='2' DRD='2' RMin='1' PTL='1' PTM='1'&gt;0&lt;/CCD&gt;<br />
  <br />
  SELECT XMLOut_txt FROM test.table2 returns<br />
  &lt;RXSS&gt;&lt;ELE QID='1' RXT='6' BS='0' DS='1274' DSE='12' DI='1'   CK1='10' CK2='1' CK3='5' CK4='(#,-1),(-1,-1),(*,-1)'   CK5='-1'&gt;1&lt;/ELE&gt;&lt;/RXSS&gt;<br />
  <br />
  The result string will contain an indicator for when a MCID is switched &lt;Q ID='NEWMCID'&gt;OK&lt;/Q&gt;<br />
  &lt;Q ID='1'&gt;12&lt;/Q&gt;&lt;Q ID='NEWMCID'&gt;OK&lt;/Q&gt;&lt;Q ID='1'&gt;12&lt;/Q&gt;<br />
  <br />
  OR is Invalid - Invalid secondary MCIDs will halt process.<br />
  &lt;Q ID='1'&gt;123&lt;/Q&gt;&lt;Q ID='NEWMCID'&gt;FAILED&lt;/Q&gt;<br />
  <br />
  OR is Blank<br />
  &lt;Q ID='1'&gt;111&lt;/Q&gt;&lt;Q ID='NEWMCID'&gt;BLANK&lt;/Q&gt;</p>
<p>*************************************************************************************************************************************<br />
  // 18 = ASR - Automatic Speech Recognition - Assumes CSP available<br />
  **************************************************************************************************************************************<br />
  // RXSSCK1[CurrMCID] = How many retries allowed<br />
  // RXSSCK2[CurrMCID] = Valid Responses (1,2,3 ect) (-13 is #) (-6 is *) and (-1 is no response)<br />
  // RXSSCK3[CurrMCID] = Repeat Key (-13 is #) (-6 is *)<br />
  // RXSSCK4[CurrMCID] = Answer MAP -   '(1,x),(2,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(-13,x),(-6,x),(-1,x)'   1-12 are resereved any number 13 and up may be used<br />
  // RXSSCK5[CurrMCID] = Next QID - no match   // Was Opt Out Option -   Enclosed in () so -13 and 1 dont both return positive<br />
  // RXSSCK6[CurrMCID] = MCID - No Entry Message<br />
  // RXSSCK7[CurrMCID] = MCID - Invalid Response Message<br />
  // RXSSCK8[CurrMCID] = Custom rules string   CK8='(string, value)(string, value)' pairs ( ) and , are reserved   characters<br />
  // RXSSCK9[CurrMCID] = Curr_TapLength - default=128<br />
  // RXSSCK10[CurrMCID] = Curr_Threshold - Min energy (dB) needed to bargein with - negative values default=-40<br />
  // RXSSCK11[CurrMCID] = Curr_SignalToNoiseRatio - default=-12<br />
  // RXSSCK12[CurrMCID] = Flag to use premade IVR options for ASR - default=0 0=off and 1=on<br />
  // RXSSCK13[CurrMCID] =  MaxDigitDelay - MIN 1 MAX 150   --- Max pause after audio file finishes reading question while waiting   for input - 100ms increments 30=3 seconds and 25=2.5 seconds - RX   version 24.0.0.24 and up<br />
  <br />
</p>
<p><br />
  Notes -</p>
<p>if answer is not specified in CK2 then will return -1<br />
  for CK4 Answer Map - return values 1-12 are resereved - any number 13 and up may be used</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p>**************************************************************************************************************************************<br />
  // 19 =  Web Service - ver 24.0.0.10 and up<br />
  **************************************************************************************************************************************<br />
  // RXSSCK1[CurrMCID] = POST or GET - default is GET<br />
  // RXSSCK2[CurrMCID] = Content type - default is text/xml<br />
  // RXSSCK3[CurrMCID] = Port - default is 80<br />
  // RXSSCK4[CurrMCID] = Response Map - Optional -   partial string match so be careful -   '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'    - escape  commas and parentesis with {&quot;&amp;lp;&quot;, &quot;(&quot;} {&quot;&amp;rp;&quot;,   &quot;)&quot;} {&quot;&amp;com;'&quot;, &quot;,&quot;}<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  // RXSSCK6[CurrMCID] = Domain<br />
  // RXSSCK7[CurrMCID] = Directory/File<br />
  // RXSSCK8[CurrMCID] = Additional Content to send - XML, SOAP, ETC<br />
  // Escape (&quot;&amp;lt;&quot;, &quot;&lt;&quot;) (&quot;&amp;gt;&quot;, &quot;&gt;&quot;)   (&quot;&amp;apos;&quot;, &quot;'&quot;) (&quot;CHR(13)&quot;, CHAR(13)) Carrige Return) (&quot;CHR(10)&quot;,   CHAR(10)) Line Feed) (&quot;&amp;quot;&quot;, &quot; double quote) (&quot;&amp;amp;&quot;,   &quot;&amp;&quot;)                <br />
  // Continue on in survey<br />
  http://techwiki.messagebroadcast.com/Campaign_Type_30/MCID_19_-_Web_Service_Call</p>
<p> <br />
  <br />
  // 20 Dynamically create and read a date from a SQL query<br />
  // RXSSCK1[CurrMCID] = SQL Query String - must   return a date time value - Date format must be consistant will work with   any Microsoft &quot;C&quot; OLEDateTime format but prefer 2009-01-01 00:00:00   CK1='SELECT DATE(&amp;#x27;CDS1&amp;#x27;)'                   <br />
  // RXSSCK2[CurrMCID] = Next QID if invalid Date Returned by SQL<br />
  // RXSSCK3[CurrMCID] =<br />
  // RXSSCK4[CurrMCID] = TTS Voice ID GT 0 Just do TTS<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  // RXSSCK6[CurrMCID] = GT 0 Include Day of Week <br />
  // RXSSCK7[CurrMCID] = GT 0 then include time<br />
  // RXSSCK8[CurrMCID] = ELE Year<br />
  // RXSSCK9[CurrMCID] = ELE two digit library<br />
  // RXSSCK10[CurrMCID] = ELE Month<br />
  // RXSSCK11[CurrMCID] = ELE Day of week<br />
  // RXSSCK12[CurrMCID] = ELE DSTime words<br />
  // RXSSCK13[CurrMCID] = ELE Day of Month 12th, 31st, etc<br />
// RXSSCK14[CurrMCID] = TTS Bit Rate - only has meaning if CK4 is greater than 0</p>
<p><br />
</p>
<p _cke_saved_style="margin-left: 40px;">// 21 - Switch on data logic<br />
  // RXSSCK1 - a tag representing a data field {%CustomField1%}<br />
  //  Add sub XML elements for each case<br />
  // Type 1 - &lt;VAL='X'&gt;Y&lt;/VAL&gt;<br />
  // Type 2 -  &lt;VAL='X' TAG='Z' &gt;Y&lt;/VAL&gt;</p>
<p _cke_saved_style="margin-left: 40px;"><br />
</p>
<p _cke_saved_style="margin-left: 40px;">// 22 - Switch MCID based on current call state</p>
<p _cke_saved_style="margin-left: 80px;">// RXSSCK1[CurrMCID] = QID to go to next if Live or Inbound<br />
  // RXSSCK2[CurrMCID] = QID to go to next if Machine<br />
  // RXSSCK5[CurrMCID] = QID to go to next if default</p>
<p _cke_saved_style="margin-left: 40px;"><br />
</p>
<p>**************************************************************************************************************************************<br />
  //  =  SMS Storage DM MT=4  - ver 24.0.0.10 and up<br />
  **************************************************************************************************************************************<br />
  // RXSSCK1[CurrMCID] = MT<br />
  // RXSSCK2[CurrMCID] = MO HELP<br />
  // RXSSCK3[CurrMCID] = MO STOP<br />
  // RXSSCK4[CurrMCID] = Short Code<br />
  // RXSSCK5[CurrMCID] = Next QID<br />
  // RXSSCK6[CurrMCID] =<br />
  // RXSSCK7[CurrMCID] =<br />
  // RXSSCK8[CurrMCID] =<br />
</p>
<p>&nbsp;</p>
<p align="center"><img src="doc_MCIDS_clip_image001.gif" alt="" width="627" height="2" />Basic Programming for Voice Campaigns<br />
</p>
<p>To make your  first phone call the following components are required to launch a call:</p>
<ul>
  <li><strong>Batch  ID</strong></li>
  <li><strong>Campaign  Type</strong></li>
  <li><strong>A  Script Library</strong></li>
  <li><strong>An XML Control String</strong></li>
</ul>
<p><strong>Batch ID</strong><br />
  A Batch ID is a unique  alphanumeric label given to an individual campaign to reference the parameters  within the campaign. This would include assigned telephone numbers, dialing  schedule, call control data, etc.</p>
<p><strong>Campaign Type 30 (Play Type 12)</strong><br />
  All inbound and  outbound campaigns can be built around this MCID type. Most commonly used  template to build client voice campaigns.</p>
<p><strong>Script Library</strong><br />
A Script Library  is a compilation of each recorded message that makes up an entire program  and/or campaign (e.g. Live Message, Voicemail, Spanish, Dynamic Elements). The  Script Library is laid out as individual MCID(s) each containing a message and  data ID number that are to be used for programming. </p>
<p><strong>XML Control String</strong><br />
  The XML Control String is programming  code that dictates to the dialer what is to be performed within a call. The XML  Control String contains the following information:</p>
<ul>
  <li>IVR  Options</li>
  <li>LAT</li>
  <li>Message  Delivery Logic</li>
  <li>Dynamic  Elements</li>
  <li>Caller  ID(s)</li>
  <li>Redial  Logic</li>
</ul>
<p><strong>MCID (Message Component Identifier)</strong> – Every message recording contains  unique components that are identified by an MCID number. Each message contains  a minimum of two required MCID(s):</p>
<ul>
  <li>Live</li>
  <li>Voicemail</li>
</ul>
<p>Each  of the MCID(s) have Standard Message Component Types (MC Type) that carry attributes  for the MCID. The following is a list of those attributes:</p>
<ul>
  <li>Statement</li>
</ul>
<p>EX: Greeting</p>
<ul>
  <li>Interactive  Response</li>
</ul>
<p>EX: Invalid Response Message</p>
<ul>
  <li>Data  Capture (Alpha and/or Numeric)</li>
</ul>
<p>EX: &ldquo;To enter a different phone number…&rdquo;</p>
<ul>
  <li>Audio  Capture</li>
</ul>
<p>EX: &ldquo;At the tone please record your response…&rdquo;</p>
<ul>
  <li>Live  Agent Transfer (LAT)</li>
  <li>Opt  Out</li>
</ul>
<p><strong><em>MCID Control Key codes and definitions  are represented in the following table:</em></strong></p>
<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="638" colspan="3"><p align="center"><strong>MCID    TYPES</strong></p></td>
  </tr>
  <tr>
    <td width="86"><p align="center"><strong>Types</strong></p></td>
    <td width="151"><p align="center"><strong>Definition</strong></p></td>
    <td width="401"><p align="center"><strong>Control    Keys for each MCID Type</strong></p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 1</strong></p></td>
    <td width="151" valign="top"><p>Statement</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= DTMFs To    Ignore before continuing on<br />
      <strong>CK5</strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 2 </strong></p></td>
    <td width="151" valign="top"><p>IVR    1-9</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= How many    retries allowed<br />
      <strong>CK2 </strong>= Valid Responses (1,2,3 ect)    (-13 is #) (-6 is *) and (-1 is no response)<br />
      <strong>CK3</strong>= Repeat    Key (-13 is #) (-6 is *)<br />
      <strong>CK4 </strong>= Answer MAP    (1,x),(2,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(-13,x),(-6,x),(-1,x)'<br />
      <strong>CK5</strong>= Next QID - no    match&nbsp;&nbsp; // Was Opt Out Option - Enclosed in () so -13 and 1 dont    both return positive<br />
      <strong>CK6 </strong>= MCID - No Entry Message<br />
      <strong>CK7</strong>= MCID - Invalid Response    Message<br />
      <strong>CK8</strong>= Max number of no Response -    default is 5 &lt;Q ID='CURRMCID'&gt;TMNR&lt;/Q&gt;<br />
      <strong>CK9</strong> = MaxDigitDelay - MIN 1 MAX    150 --- Max pause after audio file finishes reading question while waiting    for input - 100ms increments 30=3 seconds and 25=2.5 seconds - RX version    23.0.0.4 and up </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 3</strong></p></td>
    <td width="151" valign="top"><p>Record    a Response (with Erase and Re-Record Option)</p></td>
    <td width="401" valign="top"><p>  // Setup answer file   name and default location     <br />
      </p>
      <p>  // Results stored in mediaServerPath\Results\UserId\BatchId<br />
        if(RXSSCK13[CurrMCID].GetLength() &gt; 0)<br />
        SurveyResultsFilePath.Format(&quot;%s\\Results\\%d\\%s\\SurveyResults_%d_%s_%d_%s.wav&quot;,   mediaServerPath, UserId, BatchId, CurrMCID, inpPhoneStr,   CurrentRedialCount,RXSSCK13[CurrMCID]);<br />
        </p>
      <p>else<br />
        </p>
      <p>SurveyResultsFilePath.Format(&quot;%s\\Results\\%d\\%s\\SurveyResults_%d_%s_%d.wav&quot;,   mediaServerPath, UserId, BatchId, CurrMCID, inpPhoneStr,   CurrentRedialCount);<br />
      </p>
<p>&nbsp;</p>
      <p><strong>CK1</strong>= How many re-records allowed - specify 1 to skip    any  CK2 = How long to record in    seconds<br />
        <strong>CK3</strong>=  At the tone please record your answer... -    specify 0 or leave blank to skip this message<br />
        <strong>CK4 </strong>= Your message exceeded the    maximum length - specify 0 or leave blank to skip this message<br />
        <strong>CK5</strong>= Play your message will sound    like... - specify 0 or leave blank to skip this message<br />
        <strong>CK6</strong> = To erase, save, repeat press    1,2,3... only can be skipped for CK1=1<br />
        <strong>CK7</strong> = Your message has been saved    - specify 0 or leave blank to skip this message<br />
        <strong>CK8 </strong>= Next QID<br />
        <strong>CK9</strong> = Accept Key - default 1 if    blank<br />
        <strong>CK10</strong> = Erase Key - default 2 if    blank<br />
        <strong>CK11 </strong>= Repeat Key - default 3 if    blank<br />
        <strong>CK12</strong> = Output file format (default    if 0 or blank)=Linear PCM - 1=GSM610 Microsoft - 2=G.726 - 3=G.711 MuLaw - 4=ADPCM    Dialogic  NOTE: Append option only works with 0 or blank - Linear PCM<br />
        <strong>CK13</strong> = String to append to recorded results file    name&nbsp; - if not blank adds an _XXX to end of file name<br />
        <strong>CK14</strong> = Append Key - default 4 if blank - NOTE: Append option only works with 0 or blank - Linear PCM<br />
        <strong>CK15</strong> =  MCID - Append version of At the   tone please record your answer... - specify 0 or leave blank to skip   this - NOTE: Append option only works with 0 or blank - Linear PCM<br />
        // RXSSCK16[CurrMCID] = UNC Path to copy file too - optional - Specify &quot;PBX&quot; to use RXDialer Reg Path for PBX<br />
        // RXSSCK17[CurrMCID] = New File Name if copy to UNC path specified - if blank just default recording name - Special case if &quot;PBX&quot; - will auto create a new message in PBX box using 16,18,19,20<br />
        // RXSSCK18[CurrMCID] PBX user Id - only if 16 is special case of &quot;PBX&quot; - Can also use CDS strings<br />
        // RXSSCK19[CurrMCID] PBX Account Number - Phone Number - only if 16 is special case of &quot;PBX&quot; - - Can also use CDS strings<br />
      // RXSSCK20[CurrMCID] PBX Extension - only if 16 is special case of &quot;PBX&quot; - Can also use CDS strings</p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 4</strong></p></td>
    <td width="151" valign="top"><p>LAT</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= LAT    number<br />
      <strong>CK2</strong> = Play Transferring Message<br />
      <strong>CK3</strong> = DTMFs To Ignore before    continuing on past transfer message<br />
      <strong>CK4</strong>= LATCallerIdNumber<br />
      <strong>CK5</strong> = MCID for the Hold while    transfering Message<br />
      <strong>CK6</strong> = MAX timeout in seconds for    LAT - Just hangup if this value exceeded or infinite if this value is -100    -&gt; NULL,0 or Less defaults to 14400 - Four hours<br />
      <strong>CK7 </strong>= MCID for the &quot;Whisper    Greeting&quot; Message - will auto continue bridge on keypress - will auto    continue bridge when message completes<br />
      <strong>CK8</strong>= 0 = off 1 = Call Recording on    --- 0 is default if blank </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 5</strong></p></td>
    <td width="151" valign="top"><p>Opt    Out</p></td>
    <td width="401" valign="top"><p><strong>CK5</strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 6</strong></p></td>
    <td width="151" valign="top"><p>IVR    multi-string</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= Max    number of digits to retrieve<br />
      <strong>CK2 </strong>= Place to store in array of    previous digit strings 1-10 for now - retrieve in other MCIDs as CDS1 or CDS2    ... CDS10<br />
      <strong>CK3 </strong>= Max Inter-Digit Delay in    seconds - default is 5 seconds if this number is not between 1 and 29<br />
      <strong>CK4 </strong>= Response Map - Optional -    full string match -    '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
      <strong>CK5</strong>= Next QID    - no match<br />
      <strong>CK6 </strong>=<s>any value besides &quot;&quot; skip script read- just capture digits   </s>For consistancy this is now changed to &quot;&quot; is equivalent to &quot;0&quot; and and   positive integer will cuase this to skip the reading of the current   script and just capture digits.<br />
      <strong>CK7 </strong>= Minimum    Digits required or skip to CK8 - CK7 and 8 must be greater than 0 for this to    take affect<br />
      <strong>CK8</strong> = QID to go to next if not    minimum digits - CK7 and 8 must be greater than 0 for this to take affect<br />
      <strong>CK9</strong> = MCID - No Entry Message<br />
      <strong>CK10</strong> = MCID - Invalid Response /    No Match Message<br />
      <strong>CK11 </strong>= Max number of retries - Max    times this MCID will be allowed to repeat - default is 0<br />
      <strong>CK12</strong>= Max number of no Response -    default is 5 &lt;Q ID='CURRMCID'&gt;TMNR&lt;/Q&gt; </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 7</strong></p></td>
    <td width="151" valign="top"><p>Opt    In – 10 digit string</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= SP1 - MCID for - Thank-you. You are calling    from XXX-XXX-XXXX.<br />
      <strong>CK2</strong>= SB2 - MCID for - If This Is    Your Number&nbsp;&nbsp;&nbsp;<br />
      <strong>CK3</strong>= SB3 - MCID for - Number Added    to opt in list<br />
      <strong>CK4</strong> = SB4 - MCID for - Enter Phone    Number<br />
      <strong>CK5 </strong>= SB5 - MCID for - You Entered    Number<br />
      <strong>CK6 </strong>= SB6 - MCID for - Invalid Response<br />
      <strong>CK7 </strong>= Zero start MCID path ID -    the next 9 need to be 1-9 in order<br />
      <strong>CK8</strong> = Next QID<br />
      OR...<br />
      <strong>CK7</strong>= -100&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; // THEN<br />
      <strong>CK9 </strong>- Master Script library ID<br />
      <strong>CK10</strong> - Two digit element ID<br />
      <strong>CK11</strong>- Three digit element ID<br />
      <strong>CK12 </strong>= Dumb ass people want a    different message on the second loop for &quot;If This Is Your Number&quot; -    other alternative is to build as full IVR </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 8</strong></p></td>
    <td width="151" valign="top"><p>Find    Me Follow Me (FMFM)</p></td>
    <td width="401" valign="top"><p><strong>CK1 </strong>= Comma    seperated list of numbers to contact<br />
      <strong>CK2 </strong>= 1 = Call til message left, 2    = Call for response, 3 = call all number<br />
      <strong>CK8 </strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 9</strong></p></td>
    <td width="151" valign="top"><p>Read    Out Previous Digit String (No IVR Option)</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= Number of    digits to read - used for things like right 4 digits – numeric<br />
      <strong>CK2</strong> = Place to    get data in array of previous digit strings 1-10 for now<br />
      <strong>CK3</strong> = Zero start MCID path ID -    the next 9 need to be 0-9 in order<br />
      <strong>CK4</strong> = Specific Digit Sring to read    - any value here negates the CDS in CK2 - Special case the word    &quot;(ANI)&quot; will be replace with the number dialed for outbound and the    number called from for inbound AND (IBD) for inbound dialed<br />
      <strong>CK5 </strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 10</strong></p></td>
    <td width="151" valign="top"><p>SQL    Query String</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong> = SQL    Query String<br />
      <strong>CK4 </strong>= Response Map - Optional -    full string match -    '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
      <strong>CK5</strong>= Next QID </p>
      <p>CK6 = Database connection name - most of the time IP Address<br />
CK7 = NOT REALLY  USED - SECURITY   PROBLEMS - Database User name - make sure dialer DB loginis smae as   remote db login for best practices<br />
CK8 = NOT REALLY  USED - SECURITY PROBLEMS - Database   password - make sure dialer DB loginis smae as remote db loginfor best   practices</p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 11</strong></p></td>
    <td width="151" valign="top"><p>Play    DTMF(s) Dual Tone Multi-frequency</p></td>
    <td width="401" valign="top"><p><strong>CK1 </strong>= String    of DTMFs to play - '123456789*#'<br />
      <strong>CK5</strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 12</strong></p></td>
    <td width="151" valign="top"><p>Strategic    Pause</p></td>
    <td width="401" valign="top"><p><strong>CK1 </strong>= number    of milli-seconds to pause for - 1 to 1000000 (1000 seconds max)<br />
      <strong>CK5 </strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 13</strong></p></td>
    <td width="151" valign="top"><p>Email</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong> = Async    Flag 1 = Async 0 or blank for Syncronouse - Async releases line faster - good    for inbound - not as critical for outbound<br />
      <strong>CK2</strong>= MessageWarning;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;<br />
      <strong>CK3</strong>= MessageText;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;<br />
      <strong>CK4</strong> =    MessageHTML;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    &nbsp;<br />
      <strong>CK5</strong> = MessageFromUser;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp; &nbsp;<br />
      <strong>CK6</strong>=    MessageToUser;&nbsp;&nbsp;&nbsp; ; seperated list of email addresses;&nbsp;&nbsp;    &nbsp;<br />
      <strong>CK7</strong>= MessageCCUser;&nbsp;&nbsp;    &nbsp; ; seperated list of email addresses;<br />
      <strong>CK8</strong> = MessageSubject;&nbsp;&nbsp;    &nbsp;<br />
      <strong>CK9</strong> =    MessageCreationDate;&nbsp;&nbsp; &nbsp;<br />
      <strong>CK10</strong> = ConnFromUser;&nbsp;&nbsp;    &nbsp;<br />
      <strong>CK11</strong> =    ConnFromPassword;&nbsp;&nbsp; &nbsp;<br />
      <strong>CK12</strong>= AttachmentPath;&nbsp;&nbsp;    &nbsp;<br />
      <strong>CK13</strong> = SMTP Server IP Address - if    blank use local dialers Reg settings<br />
      <strong>CK15</strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 14</strong></p></td>
    <td width="151" valign="top"><p>Store    IGData for Inbound Calls</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= IGData ID    1 - 10<br />
      <strong>CK2</strong> = Value to store - Special    case for CDS1-10 and (ANI) AND (IBD) for inbound dialed<br />
      <strong>CK5</strong> = Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 15</strong></p></td>
    <td width="151" valign="top"><p>Play    From Absolute Path</p></td>
    <td width="401" valign="top"><p><strong>CK1 </strong>= DTMFs To    Ignore before continuing on&nbsp;<br />
      <strong>CK2</strong>= String for absolute path<br />
      <strong>CK5</strong>= Next QID </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 16</strong></p></td>
    <td width="151" valign="top"><p>Switch    Based on Previous Responses</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= Previous    MCID to check<br />
      <strong>CK4</strong>= Response Map - Optional -    full string match -    '(123,x),(34552,x),(3,x),(4,x),(5,x),(6,x),(7,x),(8,x),(9,x),(#,x),(*,x),(-1,x)'<br />
      <strong>CK5</strong>= Next QID if no match found </p></td>
  </tr>
  <tr>
    <td width="86" valign="top"><p><strong>Type 17</strong></p></td>
    <td width="151" valign="top"><p>Switch    MCID Based on SQL Query</p></td>
    <td width="401" valign="top"><p><strong>CK1</strong>= SQL Query    String - MUST return a valid XML MCID string<br />
      <strong>CK2</strong> = Start QID of new MCID<br />
      <strong>CK5</strong> = Next QID    if Blank - </p>
      <p>CK6 = Database connection name - most of the time IP Address<br />
CK7 = NOT REALLY  USED - SECURITY   PROBLEMS - Database User name - make sure dialer DB loginis smae as   remote db login for best practices<br />
CK8 = NOT REALLY  USED - SECURITY PROBLEMS - Database   password - make sure dialer DB loginis smae as remote db loginfor best   practices</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp; </p>

</div>


</body>
</html>