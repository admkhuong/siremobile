<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Object Mix Ins</h1>
<p>&nbsp;</p>
<p>A new type of template where the staeting MCID QID is the next largest avaialble. Properly written all MCID QID references within individual MCIDs will be based on beginning QID + offset.</p>
<p>&nbsp;</p>
<p>maxQID - is auto calculated by the append call.<br />
maxSID - is auto calculated by the append call.</p>
<p>&nbsp;</p>
<h3>Strings Required in definition file to have any affect</h3>
<p>ObjMixXML - will be appended to the RXSS list of ELE's</p>
<p>ObjMixStageXML - will be appended to STAGE list of ELE's for descriptions.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Usage</h3>
<p>mcidtoolsii.cfc?method=AddNewQIDMix will add the XML defined within the given file name to the given BatchId<br />
  data:  { INPBATCHID :   &lt;cfoutput&gt;#INPBATCHID#&lt;/cfoutput&gt;, inpObjMixInclude : inpObjMixInclude}<br />
</p>
<p>New Object Mix In Definitions are kept in files under the Session\MCID\ObjMix\ Directory. Each file name is included in the call as a parameter passed in without the .cfm extension as inpObjMixInclude. Just need the name of the file. To call inc_NewRecSet.cfm then inpObjMixInclude='inc_NewRecSet'</p>
All QIDs start at maxQID+1
<p>&nbsp; </p>
<h3>Sample<br />
</h3>
<p>&lt;cfset ObjMixXML = &quot;&lt;ELE BS='1' CK1='4' CK10='-1' CK11='0' CK12='5' CK2='1' CK3='5' CK4='(-1,#maxQID + 1#)' CK5='#maxQID + 2#' CK6='0' CK7='0' CK8='-1' CK9='-1' DESC='Please enter your mailbox number' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 1#' RXT='6' X='150' Y='40'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Please enter your mailbox number&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='4' CK10='-1' CK11='0' CK12='5' CK2='2' CK3='5' CK4='(-1,#maxQID + 3#)' CK5='#maxQID + 4#' CK6='0' CK7='0' CK8='-1' CK9='-1' DESC='Please enter your PIN code' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 3#' RXT='6' X='450' Y='40'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Please enter your PIN code&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='0' CK1='SELECT CASE WHEN COUNT(*) &amp;amp;gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = &amp;amp;apos;(IBD)&amp;amp;apos; AND Extension_vch = &amp;amp;apos;CDS1&amp;amp;apos;' CK2='0' CK3='' CK4='(1,#maxQID + 2#),(0,#maxQID + 3#)' CK5='4' CK6='PBX' CK7='' CK8='' DESC='Description Not Specified' DSUID='10' LINK='4' QID='#maxQID + 2#' RXT='10' X='150' Y='199'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='#maxQID + 1#' DESC='That extension can not be found' DI='0' DS='0' DSE='0' DSUID='10' LINK='1' QID='#maxQID + 4#' RXT='1' X='250' Y='199'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;That extension can not be found&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='0' CK1='SELECT CASE WHEN COUNT(*) &amp;amp;gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = &amp;amp;apos;(IBD)&amp;amp;apos; AND Extension_vch = &amp;amp;apos;CDS1&amp;amp;apos; AND Password_vch = &amp;amp;apos;CDS2&amp;amp;apos;' CK2='0' CK3='' CK4='(1,#maxQID + 6#),(0,#maxQID + 5#)' CK5='#maxQID + 6#' CK6='PBX' CK7='' CK8='' DESC='Validate PIN' DSUID='10' LINK='6' QID='#maxQID + 5#' RXT='10' X='450' Y='190'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='3' DESC='That is an invalid PIN' DI='0' DS='0' DSE='0' DSUID='10' LINK='3' QID='#maxQID + 6#' RXT='1' X='550' Y='190'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;That is an invalid PIN&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='0' CK1='SELECT COUNT(*) FROM PBX.PBX_Messages WHERE AccountId_vch = &amp;amp;apos;9999999999&amp;amp;apos; AND MessageState_int = 1' CK2='0' CK3='3' CK4='' CK5='8' CK6='PBX' CK7='' CK8='' DESC='Get Message Counts' DSUID='10' LINK='8' QID='#maxQID + 7#' RXT='10' X='150' Y='420'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='9' DESC='You have ' DI='0' DS='0' DSE='0' DSUID='10' LINK='9' QID='#maxQID + 8#' RXT='1' X='250' Y='390'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;You have &lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='0' CK1='' CK2='3' CK3='10' CK4='' CK5='20' CK6='0' DESC='Description Not Specified' DSUID='10' LINK='20' QID='#maxQID + 9#' RXT='9' X='400' Y='390'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Zero' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 10#' RXT='1' X='0' Y='600'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Zero&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='one' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 11#' RXT='1' X='0' Y='691'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;One&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='two' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 12#' RXT='1' X='0' Y='781'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Two&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Three' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 13#' RXT='1' X='0' Y='871'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Three&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Four' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 14#' RXT='1' X='0' Y='960'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Four&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Five' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 15#' RXT='1' X='0' Y='1050'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Five&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Six' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 16#' RXT='1' X='0' Y='1141'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Six&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Seven' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 17#' RXT='1' X='0' Y='1230'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Seven&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Eight' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 18#' RXT='1' X='0' Y='1320'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Eight&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='-1' DESC='Nine' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 19#' RXT='1' X='0' Y='1410'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Nine&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK5='#maxQID + 21#' DESC='Messages in your mailbox' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='#maxQID + 20#' RXT='1' X='500' Y='390'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;Messages in your mailbox&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  &lt;ELE BS='1' CK1='0' CK10='' CK11='0' CK12='5' CK2='(*)' CK3='' CK4='(*,7),(-1,#maxQID + 21#)' CK5='7' CK6='' CK7='' CK8='5' CK9='30' CP='0' DESC='This is a long winded place holder for voicemail review' DI='0' DS='0' DSE='0' DSUID='10' LINK='7' QID='#maxQID + 21#' RQ='0' RXT='2' X='260' Y='600'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='2'&gt;This is a long winded place holder for voicemail review&lt;/ELE&gt;<br />
  &lt;/ELE&gt; <br />
&quot;&gt; </p>
<p>&lt;cfset ObjMixStageXML = &quot;<br />
  &lt;ELE DESC='Get a valid extension' ID='#maxSID + 1#' STYLE='' X='140' Y='0'/&gt;<br />
  &lt;ELE DESC='Validate PIN Code' ID='#maxSID + 2#' STYLE='' X='440' Y='0'/&gt;<br />
  &lt;ELE DESC='Read out number of Messages' ID='#maxSID + 3#' STYLE='width: 275px; height: 26px; top: 0px; left: 0px;' X='140' Y='350'/&gt;<br />
  &lt;ELE DESC='0-9 TTS' ID='#maxSID + 4#' STYLE='width: 137px; height: 17px; top: 0px; left: 0px;' X='0' Y='560'/&gt;<br />
  &quot;&gt; <br />
</p>
</body>
</html>