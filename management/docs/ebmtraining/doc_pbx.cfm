<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PBX</title>
</head>

<body>
<h1>Private Branch Exchange (PBX)
</h1>
<p>&nbsp;</p>
<h3>Data Structures</h3>
<p>Main DB - PBX</p>
<p>Create database PBX</p>
<p>&nbsp;</p>
<p>CREATE TABLE `pbx` (<br />
  `AccountId_vch` varchar(50) NOT NULL,<br />
  `Extension_vch` varchar(50) NOT NULL,<br />
  `Password_vch` varchar(255) NOT NULL,<br />
  `LATNumber_vch` varchar(50) DEFAULT NULL,<br />
  `TimeZone_int` int(11) DEFAULT NULL,<br />
  `GreetingTypeId_int` int(11) DEFAULT NULL,<br />
  `BoxTypeId_int` int(11) DEFAULT NULL,<br />
  `FindMeList_vch` varchar(8000) DEFAULT NULL,<br />
  `EmailList_vch` varchar(8000) DEFAULT NULL,<br />
  `SMSList_vch` varchar(8000) DEFAULT NULL,<br />
  `AtachedToeMail_int` int(11) DEFAULT NULL,<br />
  `PlayEnvelope_int` int(11) DEFAULT NULL,<br />
  `PlayCID_int` int(11) DEFAULT NULL,<br />
  `MaxRerecords_int` int(11) DEFAULT '10',<br />
  `MaxMessageLengthSeconds_int` int(11) DEFAULT '120',<br />
  `RecordBridgedCallAsync_int` int(11) DEFAULT NULL,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`Extension_vch`,`AccountId_vch`),<br />
  KEY `Password_vch` (`Password_vch`),<br />
  KEY `Extension_vch` (`Extension_vch`),<br />
  KEY `AccountId_vch` (`AccountId_vch`)<br />
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1$$<br />
</p>
<p>&nbsp;</p>
<p>CREATE TABLE `pbx_messages` (<br />
  `MessageId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,<br />
  `AccountId_vch` varchar(50) NOT NULL,<br />
  `MessageTypeId_int` int(11) DEFAULT NULL,<br />
  `Extension_vch` varchar(50) NOT NULL,<br />
  `CIDNumber_vch` varchar(50) NOT NULL,<br />
  `Urgent_int` int(11) DEFAULT NULL,<br />
  `MessageState_int` int(11) DEFAULT NULL,<br />
  `MessageDelivered_dt` datetime DEFAULT NULL,<br />
  `MessageLastReviewed_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`MessageId_int`),<br />
  KEY `Extension_vch` (`Extension_vch`),<br />
  KEY `AccountId_vch` (`AccountId_vch`),<br />
  KEY `MessageState_int` (`MessageState_int`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1$$</p>
<p><br />
  CREATE TABLE `phonenumbermanager` (<br />
`PhoneNumber_vch` varchar(50) NOT NULL,<br />
`UserId_int` int(11) NOT NULL,<br />
`SystemList_vch` varchar(8000) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`PhoneNumber_vch`),<br />
KEY `UserId_int` (`UserId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p></p>
<p>NEW: RXDialer - Fulfillment Device REG settings for the PBX database access<br />
 PBXUser;<br />
PBXPassword;<br />
 PBXConnection;<br />
PBXDatabaseName;<br />
PBXPath</p>
<p>(IBD) is the CCD CID for outbound calls and is the Inbound number dialed for inbound calls.</p>
<h3>Authentication</h3>
<p>After call is connected - Inbound/Outbound/LAT a user is authenticated with a variable length PIN entered via keypad through IVR.</p>
<p>rxt6 Multi String Capture<br />
  then<br />
  rxt10 SQL Query
  <br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>For rxt 3 - Record a message there are now PBX options</p>
<p>IF CK16 = &quot;PBX&quot; then uses PBXPath on RXDialer</p>
<p>IF CK17 = &quot;PBX&quot; then<br />
  CK19 is the Account ID or Phone Number associated with this mailbox - use (IBD) and (ANI) to get on the fly<br />
CK20 is the extension. Default extension is 0 if none is specified. Use CDS to get earlier collected digits<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>VM Navigation</h3>
<h4>Top level</h4>
<p>Start with reading how many outstanding VMs there are<br />
  New VMs<br />
Old VMs  <br />
  Press 1 to hear new VMs<br />
  Press X for admin options
</p>
<p>&nbsp;</p>
<h1>MCID Programming and Setup</h1>
<p>Liberal use of rxt 10 - SQL<br />
</p>
<h3> Validate Extension</h3>
<p>SELECT CASE WHEN COUNT(*) &gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = '(IBD)' AND Extension_vch = 'CDS1'</p>
<h3>Get Count New Messages</h3>
<p>SELECT COUNT(*) FROM PBX.PBX_Messages WHERE AccountId_vch = '(IBD)' AND MessageState_int = 1 AND Extension_vch = 'CDS1'</p>
<p>SELECT CASE WHEN COUNT(*) &gt; 0 THEN 1 ELSE 0 END FROM PBX.PBX WHERE AccountId_vch = '(IBD)' AND Extension_vch = 'CDS1' AND Password_vch = 'CDS2'</p>
<h3>Get next VM</h3>
<p>SELECT MessageId_int FROM PBX.PBX_Messages WHERE AccountId_vch = '(IBD)' AND Extension_vch = 'CDS1' AND MessageId_int &gt; CAST('CDS4' as DECIMAL) ORDER BY MessageId_int ASC LIMIT 1</p>
<p>Use CAST('CDS4' as decimal) in case CDSX is undefined in mySQL CAST('' as decimal) evaluates to 0<br />
LIMIT 1 is important - MCID SQL will always return will return first column / last row in result set.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4>After Message Plays Option</h4>
<p>Go Next<br />
Delete<br />
Save<br />
Forward<br />
Mark<br />
Play again<br />
</p>
<p>&nbsp;</p>
<h3>Admin Options<br />
</h3>
<p>&nbsp;</p>
<p>You can Authenticated based on  inbound number <br />
  <br />
  SELECT <br />
CASE WHEN COUNT(*) &gt; 0 THEN 1 ELSE 0 END<br />
FROM PBX.PBX<br />
  WHERE<br />
  AccountId_vch = '9999999999'<br />
  AND<br />
  Extension_vch = '1111'<br />
  AND<br />
Password_vch = '1111'</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Message States - MessageState_int</h3>
<p>1 = New Message<br />
  2 = Read Message - Old Message<br />
  3 = Saved Message<br />
10 = Logical Deleted Message</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Data Structures</h3>
<p>Main DB - PBX</p>
<p>Create database PBX</p>
<p>delimiter $$<br />
delimiter $$</p>
<p>CREATE TABLE `pbx` (<br />
  `AccountId_vch` varchar(50) NOT NULL,<br />
  `Extension_vch` varchar(50) NOT NULL,<br />
  `Password_vch` varchar(255) NOT NULL,<br />
  `LATNumber_vch` varchar(50) DEFAULT NULL,<br />
  `TimeZone_int` int(11) DEFAULT NULL,<br />
  `GreetingTypeId_int` int(11) DEFAULT NULL,<br />
  `BoxTypeId_int` int(11) DEFAULT NULL,<br />
  `FindMeList_vch` varchar(8000) DEFAULT NULL,<br />
  `EmailList_vch` varchar(8000) DEFAULT NULL,<br />
  `SMSList_vch` varchar(8000) DEFAULT NULL,<br />
  `AtachedToeMail_int` int(11) DEFAULT NULL,<br />
  `PlayEnvelope_int` int(11) DEFAULT NULL,<br />
  `PlayCID_int` int(11) DEFAULT NULL,<br />
  `MaxRerecords_int` int(11) DEFAULT '10',<br />
  `MaxMessageLengthSeconds_int` int(11) DEFAULT '120',<br />
  `RecordBridgedCallAsync_int` int(11) DEFAULT NULL,<br />
  `Created_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`Extension_vch`,`AccountId_vch`),<br />
  KEY `Password_vch` (`Password_vch`),<br />
  KEY `Extension_vch` (`Extension_vch`),<br />
  KEY `AccountId_vch` (`AccountId_vch`)<br />
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1$$<br />
</p>
<p>delimiter $$</p>
<p>CREATE TABLE `pbx_messages` (<br />
  `MessageId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,<br />
  `AccountId_vch` varchar(50) NOT NULL,<br />
  `MessageTypeId_int` int(11) DEFAULT NULL,<br />
  `Extension_vch` varchar(50) NOT NULL,<br />
  `CIDNumber_vch` varchar(50) NOT NULL,<br />
  `Urgent_int` int(11) DEFAULT NULL,<br />
  `MessageState_int` int(11) DEFAULT NULL,<br />
  `MessageDelivered_dt` datetime DEFAULT NULL,<br />
  `MessageLastReviewed_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`MessageId_int`),<br />
  KEY `Extension_vch` (`Extension_vch`),<br />
  KEY `AccountId_vch` (`AccountId_vch`),<br />
  KEY `MessageState_int` (`MessageState_int`)<br />
  ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>