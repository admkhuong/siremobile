<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.red {
	color: #F00;
}
</style>
</head>

<body>
<h1 class="red">This file is deprecated see latest version in Session/Help/Developers</h1>
<h1>&nbsp;</h1>
<h1>Recorded Responses</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Needs:</p>
<p>Cleanup delete archive old data so drives dont get full</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Pull Recorded Responses out of SimpleXResults:</h3>
<p>Once audio is pulled </p>
<p>RecordedResultPulled_int</p>
<p>ALTER TABLE `simplexresults`.`contactresults` ADD COLUMN `RecordedResultPulled_int` INT(1) NOT NULL DEFAULT 0 AFTER `MainMessageLengthSeconds_int`, ADD INDEX `IDX_RecordedResultPulled_int` (`RecordedResultPulled_int` ASC) ;</p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Design:</p>
<p>&nbsp;</p>
<p>Surveys are completed through normal MCID processes - additional heatinx info in results strings<br />
EBM to have a set of tools for both review and transcription of recorded audio.</p>
<p>&nbsp;</p>
<h3> Management: </h3>
<p>New Process:</p>
<p>A Process that runs in scheduled tasks (act_TransferResponses.cfm) that loops through all already extracted results and:</p>
<p>a. Pulls audio files to centralized media server location (TBD) - File names in Final media Server: <br />
b. Pre calculates any additoanl feileds<br />
c. Populates new table
</p>
<p>&nbsp;</p>
<h3>Session\RecordedResponses</h3>
<p>Display section through here.</p>
<p>Tools/RecordedResponses Tab<br /> 
First Screen lists all batches - review recordings by clicking magnifying glass<br />
First level Popup - just lists recordings - Select a recording to review<br />
Second Level popup</p>
<p>Type notes<br />
  Type Transcriptions<br />
  Play audio - HTML5 tools<br />
  Display HeatIDX info<br />
  Length<br />
  Last Reviewed Date<br />
  History?<br />
  Links to orignal customer information from RXMultiList<br />
Integrated callback - with integrated call back history<br />
Track what is sent for transcription<br />
  <br />
  <br />
</p>
<p>???</p>
<p><br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
</p>
<h3>Session\CFC RecordedResponses.cfc</h3>
<p>All interactions with data and files through here</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Data</h3>
<p>simplexrecordedresults.transcriptionfilelog</p>
<p>CREATE TABLE `transcriptionfilelog` (<br />
`TFL_int` int(10) unsigned NOT NULL AUTO_INCREMENT,<br />
`MasterRXCallDetailId_int` int(11) unsigned NOT NULL,<br />
`RedialNumber_int` int(11) DEFAULT NULL,<br />
`DTS_UUID_vch` varchar(36) DEFAULT NULL,<br />
`BatchId_bi` bigint(20) DEFAULT NULL,<br />
`UserId_int` int(10) unsigned DEFAULT NULL,<br />
`XMLResultStr_vch` text,<br />
`Original_FileName_vch` varchar(1024) DEFAULT NULL,<br />
`TransCo_FileName_vch` varchar(1024) DEFAULT NULL,<br />
`Customer_FileName_vch` varchar(1024) DEFAULT NULL,<br />
`IsInFileBlank_ti` tinyint(1) DEFAULT NULL,<br />
`TranscribeAuth_ti` tinyint(1) DEFAULT NULL,<br />
`DialerIP_vch` varchar(50) DEFAULT NULL,<br />
`InitialDate_dt` datetime DEFAULT NULL,<br />
`OutDateTransCo_dt` datetime DEFAULT NULL,<br />
`OutDateCustsomer_dt` datetime DEFAULT NULL,<br />
`Reserved_dt` datetime DEFAULT NULL,<br />
`Reviewed_dt` datetime DEFAULT NULL,<br />
`ReviewNotes_vch` text,<br />
`ReviewedBy_vch` varchar(1024) DEFAULT NULL,<br />
`Transcription_vch` text,<br />
`HeatIndex_int` int(11) DEFAULT NULL,<br />
`AdjustedHeatIndex_int` int(11) DEFAULT NULL,<br />
`LastModified_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`TFL_int`),<br />
KEY `IDX_BatchId_bi` (`BatchId_bi`),<br />
KEY `IDX_UserId_int` (`UserId_int`),<br />
KEY `IDX_MasterRXCallDetailId_int` (`MasterRXCallDetailId_int`),<br />
KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),<br />
KEY `IDX_InitialDate_dt` (`InitialDate_dt`),<br />
KEY `IDX_OutDateTransCo_dt` (`OutDateTransCo_dt`),<br />
KEY `IDX_OutDateCustsomer_dt` (`OutDateCustsomer_dt`),<br />
KEY `IDX_Reserved_dt` (`Reserved_dt`),<br />
KEY `IDX_Reviewed_dt` (`Reviewed_dt`)<br />
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1$$</p>
<p>;</p>
<p><strong>Table for managing transcription locks</strong></p>
<p>simplexrecordedresponses.recordedresponses_lock</p>
<p>CREATE TABLE `recordedresponses_lock` (<br />
  `Id_int` int(11) NOT NULL,<br />
  `MasterRXCallDetailId_int` int(11) DEFAULT NULL,<br />
  `UserId_int` int(11) DEFAULT NULL,<br />
  `LockStart_dt` datetime DEFAULT NULL,<br />
  `LockEnd_dt` datetime DEFAULT NULL,<br />
  PRIMARY KEY (`Id_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1</p>
<p></p>
</body>
</html>