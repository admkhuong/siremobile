<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p>Keys for identifying questions for reporting and looking for   check points. If key values are greater than 0 they will carry through   to the result string.</p>
<p>Can be used for &quot;standard&quot; reporting.<br />
  Can be used for randomizing survey questions but still tracking with response goes to which question.<br />
  Can use one or both or none<br />
  Default behavior is the same but if you use these keys then no more   searching results for &lt;Q ID='X'&gt; with the close bracket. Search   instead with XML parsers, regular expressions, SQL wildcards or just   &quot;&lt;Q ID='X'&quot; - no close bracket</p>
<p> </p>
<p>RQ='1' - default is blank - must be greater than 0 to work</p>
<p>CP='1' - default is blank - must be greater than 0 to work</p>
<p>MCID String<br />
  &lt;DM LIB='711' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;RXSS&gt;<br />
  &lt;ELE QID='1' RXT='2' BS='0' DS='711'  DSE='1' DI='1' CK1='3'   CK2='1,-6' CK3='-6' CK4='(1,2)' CK5='-1' CK6='-1'   CK7='-1'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE QID='2' RXT='11' BS='0' CK1='123456' CK5='-1'&gt;0&lt;/ELE&gt;<br />
  &lt;/RXSS&gt;<br />
  &lt;DM LIB='711' MT='2' PT='1'&gt;&lt;ELE ID='1'&gt;2&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1'&gt;0&lt;/CCD&gt;</p>
<p>Result String<br />
  &lt;Q ID='1'&gt;-1&lt;/Q&gt;<br />
  <br />
  <br />
  MCID String<br />
  &lt;DM LIB='711' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;RXSS&gt;<br />
  &lt;ELE QID='1' RXT='2' BS='0' DS='711'  DSE='1' DI='1' CK1='3'   CK2='1,-6' CK3='-6' CK4='(1,2)' CK5='-1' CK6='-1' CK7='-1' RQ='1'   CP='5'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE QID='2' RXT='11' BS='0' CK1='123456' CK5='-1'&gt;0&lt;/ELE&gt;<br />
  &lt;/RXSS&gt;<br />
  &lt;DM LIB='711' MT='2' PT='1'&gt;&lt;ELE ID='1'&gt;2&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1'&gt;0&lt;/CCD&gt;<br />
  <br />
  Result String<br />
  &lt;Q ID='1' RQ='1' CP='5'&gt;-1&lt;/Q&gt;<br />
  <br />
  <br />
  MCID String<br />
  &lt;DM LIB='711' MT='1' PT='12'&gt;&lt;ELE ID='0'&gt;0&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;RXSS&gt;<br />
  &lt;ELE QID='1' RXT='2' BS='0' DS='711'  DSE='1' DI='1' CK1='3'   CK2='1,-6' CK3='-6' CK4='(1,2)' CK5='-1' CK6='-1' CK7='-1'   RQ='1'&gt;0&lt;/ELE&gt;<br />
  &lt;ELE QID='2' RXT='11' BS='0' CK1='123456' CK5='-1'&gt;0&lt;/ELE&gt;<br />
  &lt;/RXSS&gt;<br />
  &lt;DM LIB='711' MT='2' PT='1'&gt;&lt;ELE ID='1'&gt;2&lt;/ELE&gt;&lt;/DM&gt;<br />
  &lt;CCD FileSeq='1' UserSpecData='0' CID='9999999999' DRD='1' RMin='1' PTL='1' PTM='1'&gt;0&lt;/CCD&gt;<br />
  <br />
  Result String<br />
  &lt;Q ID='1' RQ='1'&gt;-1&lt;/Q&gt;</p>
</body>
</html>