<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.warning {
	color: #F00;
}
</style>
</head>

<body>
<h1 class="warning">***************************************************<br />
Do NOT edit here anymore - moved to Session Help Section<br />
***************************************************
</h1>
<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1>&nbsp;</h1>
<h1>RXMultiList</h1>
<p>&nbsp;</p>
<h3>Overview</h3>
<p>The RXMultilist is a place for an EBM user to store and manage lists of contact strings. Contact strings can be of several types. Phone numbers, eMail addresses, SMS numbers, and other ways to contact a person.</p>
<p>&nbsp;</p>
<h3>Structure</h3>
<p>All customer contact data is stored in the simplelists.rxmultilist table.</p>
<p><br />
  CREATE TABLE `rxmultilist` (<br />
`UserId_int` int(11) NOT NULL,<br />
`ContactTypeId_int` int(4) NOT NULL,<br />
`TimeZone_int` smallint(6) NOT NULL,<br />
`CellFlag_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`OptInFlag_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`SourceKey_int` int(11) DEFAULT NULL,<br />
`Created_dt` datetime DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
`LastAccess_dt` datetime DEFAULT NULL,<br />
`CustomField1_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField2_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField3_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField4_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField5_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField6_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField7_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField8_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField9_int` decimal(11,4) DEFAULT NULL,<br />
`CustomField10_int` decimal(11,4) DEFAULT NULL,<br />
`UniqueCustomer_UUID_vch` varchar(36) DEFAULT NULL,<br />
`ContactString_vch` varchar(255) NOT NULL DEFAULT '',<br />
`LocationKey1_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey2_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey3_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey4_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey5_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey6_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey7_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey8_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey9_vch` varchar(255) DEFAULT NULL,<br />
`LocationKey10_vch` varchar(255) DEFAULT NULL,<br />
`SourceKey_vch` varchar(255) NOT NULL DEFAULT '',<br />
`grouplist_vch` varchar(512) NOT NULL DEFAULT '0,',<br />
`CustomField1_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField2_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField3_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField4_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField5_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField6_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField7_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField8_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField9_vch` varchar(2048) DEFAULT NULL,<br />
`CustomField10_vch` varchar(2048) DEFAULT NULL,<br />
`UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,<br />
`SourceString_vch` text,<br />
`LastName_vch` varchar(255) DEFAULT NULL,<br />
`FirstName_vch` varchar(255) DEFAULT NULL,<br />
`CPPID_vch` varchar(1024) DEFAULT NULL,<br />
`CPPPWD_vch` blob,<br />
`CPPPriority_vch` int(4) DEFAULT NULL,<br />
 `CPPContactTimePref0_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`CPPContactTimePref1_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`CPPContactTimePref2_int` tinyint(4) NOT NULL DEFAULT '0',<br />
`CPPContactTimePref3_int` tinyint(4) NOT NULL DEFAULT '0',<br />
  PRIMARY KEY (`UserId_int`,`ContactString_vch`,`SourceKey_vch`,`ContactTypeId_int`),<br />
  KEY `IDX_OptInFlag_int` (`OptInFlag_int`),<br />
  KEY `IDX_grouplist_vch` (`grouplist_vch`),<br />
  KEY `IDX_ContactTypeId_int` (`ContactTypeId_int`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p>ALTER TABLE  `simplelists`.`rxmultilist` ADD COLUMN `socialToken_vch` VARCHAR(2048) NULL  DEFAULT NULL  AFTER `FirstName_vch`</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>One of the main point of the list is to import data needed for customizing XMLControlString</p>
<p>&nbsp;</p>
<h3>The UserID</h3>
<p>All data is part of the one main list associated with the top level UserID</p>
<p>&nbsp;</p>
<h3>Contact Type</h3>
<p>ContactTypeId_int is used for</p>
<p>1 = Voice - this will be a phone number string<br />
2 = eMail - this will be an email address<br />
3 = SMS - this will be an SMS phone number<br />
4 = Facebook Token
<br />
5 - Other<br />
6 - Other again<br />
.<br />
.<br />
.<br />
</p>
<p>&nbsp;</p>
<h3>Time Zone</h3>
<p>TimeZone_int is used by the fulfillment devices to adjust scheduled campaigns to individual contacts. Mellissa data is used in a phone number is prvided.</p>
<p>UI lets indiviual contact strings get updated</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>SourceKey_int - user defined number for the source of this<br />
  SourceString_vch - is a user defined description of the source for this data  <br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Location Keys</h3>
<p>Users can store whatever they want in each of these fields<br />
By Convention</p>
<p>LocationKey1_vch is City<br />
LocationKey2_vch is State<br />
LocationKey3_vch is Zip</p>
<p><br />
</p>
<h3>Numeric Fields</h3>
<p>Custom numeric strings wiht up to 4 decimal places<br />
  CustomField1_int<br />
</p>
<p>&nbsp;</p>
<h3>Groups</h3>
<p>Groups are a way to maintain and organize separate lists of contact strings within a RXMultiList.</p>
<p>simplelists`.`simplephonelistgroups</p>
<p>All - is Group Id 0<br />
  Current UID </p>
<p>&nbsp;</p>
<h3>Usage</h3>
<p>An EBM users uses the filters under contacts to first specify thepeople they want to send messages to. Then the EBM user selects a message (BatchID). The Distribution.CFC contains a method that will combine these into the Queue for fulfillment. Dynamic processing of custom XMLControl Strings is possible by using custom fields within the RXMultiList.</p>
<p>Advanced Users Only - It is not always nessasary to push data into the List first if you have a process to push to Queue directly</p>
<p>&nbsp;</p>
<h3>ETL Tools</h3>
<p>Either manual create ETL XML or use EBM Enterprise GUI to generate ETL XML. ETL XML is passed into </p>
<p>../Session/Lists/MultiContacts/act_AsyncWritePhoneListFile.cfm does the processing.<br />
../Session/Lists/MultiContacts/Transformations/... contains files used by act_AsyncWritePhoneListFile to do the work</p>
<p>&nbsp;</p>
<p>Simple 1 field per line text file</p>
<p>&lt;FD ID='1' DESC='Phone Number'&gt;&lt;LINK LinkTarget='1'&gt;&lt;/LINK&gt;&lt;/FD&gt;</p>
<p>Sample XML String for 16 fields, Comma Seperated file format</p>
<p>&lt;FD ID='1' DESC='ID - GUID'&gt;&lt;LINK LinkTarget='201' StartX='853' StartY='95' StopX='90' StopY='368' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='2' DESC='Alert Type'&gt;&lt;LINK LinkTarget='202' StartX='853' StartY='127' StopX='90' StopY='397' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='3' DESC='Device Address - Phone Number'&gt;&lt;LINK LinkTarget='1' StartX='640' StartY='159' StopX='-80' StopY='38' Quadrant='1' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='4' DESC='Site'&gt;&lt;LINK LinkTarget='307' StartX='640' StartY='191' StopX='-80' StopY='542' Quadrant='1' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='5' DESC='Account Number'&gt;&lt;LINK LinkTarget='308' StartX='640' StartY='223' StopX='-80' StopY='571' Quadrant='1' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='6' DESC='Threshold'&gt;&lt;LINK LinkTarget='203' StartX='853' StartY='255' StopX='90' StopY='429' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='7' DESC='Variable Character Data 1'&gt;&lt;LINK LinkTarget='204' StartX='853' StartY='287' StopX='90' StopY='455' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='8' DESC='Variable Character Data 2'&gt;&lt;LINK LinkTarget='205' StartX='853' StartY='319' StopX='90' StopY='484' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='9' DESC='Date Data 1'&gt;&lt;LINK LinkTarget='206' StartX='853' StartY='351' StopX='90' StopY='513' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='10' DESC='Variable Numeric Data 1'&gt;&lt;LINK LinkTarget='101' StartX='853' StartY='383' StopX='90' StopY='38' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='11' DESC='Security Flag'&gt;&lt;LINK LinkTarget='102' StartX='853' StartY='415' StopX='90' StopY='67' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='12' DESC='Post Date'&gt;&lt;LINK LinkTarget='207' StartX='853' StartY='447' StopX='90' StopY='542' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='13' DESC='Variable Character Data 4'&gt;&lt;LINK LinkTarget='208' StartX='853' StartY='479' StopX='90' StopY='571' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='14' DESC='Variable Character Data 5'&gt;&lt;LINK LinkTarget='209' StartX='853' StartY='511' StopX='90' StopY='600' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='15' DESC='Time Zone'&gt;&lt;LINK LinkTarget='301' StartX='640' StartY='543' StopX='-80' StopY='368' Quadrant='1' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
  &lt;FD ID='16' DESC='Variable Numeric Data 2'&gt;&lt;LINK LinkTarget='103' StartX='853' StartY='575' StopX='90' StopY='96' Quadrant='2' &gt;&lt;/LINK&gt;&lt;/FD&gt;<br />
</p>
<p></p>
<p></p>
<p>&nbsp;</p>
<h3>Upload Monitor</h3>
<p>simplelists.simplephonelistuploadmonitor</p>
<p>Table to track what stage of the upload is currently happening. Also stores last error information if any.</p>
<p>&nbsp;</p>
<p>User Preferences for Best Time to Contact</p>
<p>&nbsp;</p>
<p> `CPPContactTimePref0_int` tinyint(4) NOT NULL DEFAULT '0', - Anytime -user can define actual values - default 24 Hours<br />
  `CPPContactTimePref1_int` tinyint(4) NOT NULL DEFAULT '0', - Morning - user can define actual values - default is 8AM-12PM<br />
  `CPPContactTimePref2_int` tinyint(4) NOT NULL DEFAULT '0', - Afternoon- user can define actual values - default is 12PM-5PM<br />
`CPPContactTimePref3_int` tinyint(4) NOT NULL DEFAULT '0',- Evening- user can define actual values - default is 5PM - 9PM</p>
<p>&nbsp;</p>
<p>ALTER TABLE `simplelists`.`rxmultilist` ADD COLUMN `CPPContactTimePref0_int` TINYINT NOT NULL DEFAULT 0 AFTER `CPPPriority_vch` , ADD COLUMN `CPPContactTimePref1_int` TINYINT NOT NULL DEFAULT 0 AFTER `CPPContactTimePref0_int` , ADD COLUMN `CPPContactTimePref2_int` TINYINT NOT NULL DEFAULT 0 AFTER `CPPContactTimePref1_int` , ADD COLUMN `CPPContactTimePref3_int` TINYINT NOT NULL DEFAULT 0 AFTER `CPPContactTimePref2_int` ,  ADD COLUMN `CPPFrequencyFlag_int` TINYINT NOT NULL DEFAULT '0'  AFTER `CPPContactTimePref3_int`</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>