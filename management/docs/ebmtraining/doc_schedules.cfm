<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Schedules</h1>
<p>&nbsp;</p>
<h3>Overview</h3>
<p>All fullfillment is controlled by schedules.</p>
<p>The main storage location for schedules is in simpleobjects.scheduleoptions. When distributing to the fullfillment device the automated process will automatically update the remote schedule to match the main schedule</p>
<p>Every Batch has its own schedule.</p>
<p>There are two types:</p>
<p>Single - all start and stop times are the same regwardless of day of week. Particular days of week can be enabled and disabled directly. DynamicScheduleDayOfWeek_ti = 0.</p>
<p>Dynamic - there is one schedule optional for each day of week with Sunday being DynamicScheduleDayOfWeek_ti=1 and Saturday being DynamicScheduleDayOfWeek_ti=7</p>
<p>If Single Schedule is chosen only the DynamicScheduleDayOfWeek_ti of 0 is used - all other dynamic schedules will be erased - DynamicScheduleDayOfWeek_ti &gt; 0 will be deleted</p>
<p>If a Dynamic Schedule has not been saved yet then default is disabled. </p>
<p>Saviving at least one Dynamic day will erase default schedule of DynamicScheduleDayOfWeek_ti=0.</p>
<p></p>
<p>&nbsp;</p>
<h3>Data Structures</h3>
<p>  CREATE TABLE `scheduleoptions` (<br />
`BatchId_bi` bigint(20) NOT NULL DEFAULT '0',<br />
`DynamicScheduleDayOfWeek_ti` tinyint(4) NOT NULL DEFAULT '0',<br />
`Enabled_ti` tinyint(4) DEFAULT '0',<br />
`StartHour_ti` tinyint(3) unsigned DEFAULT '9',<br />
`EndHour_ti` tinyint(3) unsigned DEFAULT '17',<br />
`StartMinute_ti` tinyint(3) NOT NULL DEFAULT '0',<br />
`EndMinute_ti` tinyint(3) NOT NULL DEFAULT '0',<br />
`BlackoutStartHour_ti` tinyint(4) DEFAULT '0',<br />
`BlackoutEndHour_ti` tinyint(4) DEFAULT '0',<br />
`Sunday_ti` tinyint(4) DEFAULT '0',<br />
`Monday_ti` tinyint(4) DEFAULT '0',<br />
`Tuesday_ti` tinyint(4) DEFAULT '0',<br />
`Wednesday_ti` tinyint(4) DEFAULT '0',<br />
`Thursday_ti` tinyint(4) DEFAULT '0',<br />
`Friday_ti` tinyint(4) DEFAULT '0',<br />
`Saturday_ti` tinyint(4) DEFAULT '0',<br />
`LoopLimit_int` int(10) unsigned DEFAULT '100',<br />
`LastSent_dt` datetime DEFAULT NULL,<br />
`Stop_dt` datetime DEFAULT NULL,<br />
`Start_dt` datetime DEFAULT NULL,<br />
`LastUpdated_dt` datetime DEFAULT NULL,<br />
PRIMARY KEY (`BatchId_bi`,`DynamicScheduleDayOfWeek_ti`)<br />
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Schedule Updates</h3>
<p>Whenever the schedule is updated in the main EBM - all fullfillment devices that currently have items distributed to them will also be updated. </p>
<p>Any automated redials still on these fulfillment devices will follow the new schedule rules</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Old Bug Fixed</h3>
<p>RXDialer would auto disable any callcontrol.scheduleoptions entry where no calls have been made in more than 30 days - base on LastSent_dt. This is to keep the DTS processesor from looking at too many old batch ids once they are finished running. When a new call was distributed but the master schedule had not changed the RXDialer would still remain disabled even though the master schedule is still enabled. I fixed this by auto updating remote schedules even if no changes have been made if the last time it was update3d remotely was over 7 days ago.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Todos - Dev</p>
<p>If any Dynamic schedules are defined then </p>
<p>Copy from drop down to copy other day of week.</p>
<p>If a schedule is changed on the main EBM site then the Queue is searched for RXDialers that have data scheduled to go out. - make option to adjust already running schedules? Warn if already running?</p>
<p>Add logic on update schedules to check for remote data still running. Use Ajax to check and give loacl EBM user feedback. Total Count - How many fulfillment devices?</p>
<p>&nbsp;</p>
<p>Need to adjust extract logic to update Queue only if call is complete - automated redials need to have scheduled state in queue left as distributed.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>