<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Script Recording Remote<br />
  (Old Click to Record Tool)</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Requirements</h3>
<p>Requires RXDialer 25.0.0.4 or Higher<br />
  RXDialer needs full read/write acces to the rxds file library
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Usage</h3>
<p>Using rxt Type 3 with special formating options</p>
<p> CK16 - Special Logic if CK16='SIMPLEXLIB'</p>
<p>Pass in CK17 - User_Lib_Ele_Script - Allow use of CDS's</p>
<p>Pass In CK18 - AutoFilled with rxdsLocalWritePath - based on whatever is current system local script library. Relys on Webserver setup - user deoes not need to specify.<br />
  Dev area rxdsLocalWritePath - &quot;\\10.0.1.114\SimpleXScripts&quot;<br />
Prod - rxdsLocalWritePath - \\10.26.0.60\SimpleXScripts  <br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>User Id<br />
  Lib Id<br />
  ELE Id<br />
  Script Id<br />
</p>
<p>Convert current file to phone res wav from mps<br />
  Play file<br />
  Option to record<br />
  Record file<br />
  save file<br />
  convert to mp3 for final storage<br />
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>CK1 needs to be greater than 2 - this is so the intial recording can be script and you can then be dropped down into &quot;Your message will sound like...&quot;</p>
</body>
</html>