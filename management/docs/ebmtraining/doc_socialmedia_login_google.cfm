<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p align="center"><strong>Google+ EBM social media guidline</strong></p>
<ul>
  <li> Configuration - Google APP ID </li>
  <li>Access this link: <a href="https://code.google.com/apis/console/#project:952961036705:access">https://code.google.com/apis/console/#project:952961036705:access</a></li>
  <li>Click on <u>Create another client ID, </u></li>
</ul>
<p><img src="doc_Socialmedia_Login_Google_clip_image002.jpg" alt="" width="623" height="317" border="0" /></p>
<ul>
  <li>A form will be displayed as below</li>
</ul>
<p><img src="doc_Socialmedia_Login_Google_clip_image004.jpg" alt="" width="623" height="306" border="0" /></p>
<ul>
  <li>Chose Web application for Application Type</li>
  <li>You need to input <strong>EBM domain</strong> into 2 textboxes following above image</li>
  <li>Google will generate a <strong>client ID</strong> for your application</li>
</ul>
<p><img src="doc_Socialmedia_Login_Google_clip_image006.jpg" alt="" width="623" height="108" border="0" /></p>
<ul>
  <li>Open file &ldquo;devjlp\EBM_DEV\Public\paths.cfm&rdquo;  and change content of this line for Google Client  ID</li>
</ul>
<p>&lt;cfset APPLICATION.GoogleClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' /&gt;</p>
<p>&nbsp;</p>
</body>
</html>