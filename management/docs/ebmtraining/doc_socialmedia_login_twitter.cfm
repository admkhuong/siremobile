<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<p><strong>How to configure twitter application for EBM</strong><br />
  Flow these steps to  configure twitter application:</p>
<ul>
  <li>Create twitter  application</li>
  <li>Get twitter  application information and configure on EBM</li>
</ul>
<p><strong>&nbsp;</strong></p>
<ul>
  <li><strong>Create twitter application</strong></li>
  <li>Go to <a href="https://dev.twitter.com/">https://dev.twitter.com/</a></li>
</ul>
<p><img src="doc_Socialmedia_Twitter_clip_image002.jpg" alt="" width="520" height="300" border="0" /> <br />
  <img src="doc_Socialmedia_Twitter_clip_image004.jpg" alt="" width="472" height="281" border="0" /> <br />
  After creating twitter  application, go to <strong>settings </strong>tab to  set app permission <strong>read and write</strong><br />
  <img src="doc_Socialmedia_Twitter_clip_image006.jpg" alt="" width="564" height="673" border="0" /></p>
<ul>
  <li><strong>Get twitter application information and configure on  EBM</strong></li>
  <li>Go to &ldquo;<strong>Details</strong>&rdquo;, click to &ldquo;<strong>Create my access token</strong>&rdquo; button to  generate access token string</li>
</ul>
<p><img src="doc_Socialmedia_Twitter_clip_image008.jpg" alt="" width="624" height="555" border="0" /></p>
<p><img src="doc_Socialmedia_Twitter_clip_image010.jpg" alt="" width="624" height="617" border="0" /></p>
<ul>
  <li>Copy &ldquo;<em>Consumer Key</em>&rdquo;, &ldquo;<em>Consumer secret</em>&rdquo;, <em>&ldquo;Access  token&rdquo;, &ldquo;Access token secret&rdquo;</em></li>
  <li>Open Open file  &ldquo;devjlp\EBM_DEV\Public\paths.cfm&rdquo; and file &ldquo;devjlp\EBM_DEV\Session\paths.cfm&rdquo; </li>
</ul>
<p>&lt;!---  Twitter application ---&gt; <br />
  &lt;cfset APPLICATION.TwitterAccessToken = 'your  access token' /&gt; <br />
  &lt;cfset APPLICATION.TwitterAccessTokenSecret = '<em>your access token secret</em>' /&gt; <br />
  &lt;cfset APPLICATION.TwitterConsumerkey = '<em>your</em> <em>Consumer  Key</em> ' /&gt; <br />
  &lt;cfset APPLICATION.TwitterConsumersecret = <em>'your Consumer secret</em>' /&gt; </p>
</body>
</html>