<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>Surveys - Simplified Survey UI</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Running questions and answers</h3>
<p>[6:30:53 AM] Hung Hoxuan: hi Jeff<br />
  [6:34:48 AM] Hung Hoxuan: team is waiting for you for discussion<br />
  [6:34:54 AM] Hung Hoxuan: please let me know when you are online<br />
  [8:09:41 AM] jpijeff: ???<br />
  [8:15:33 AM] Hung Hoxuan: do you have time now<br />
  [8:16:09 AM] jpijeff: yrs<br />
  [8:16:11 AM] Hung Hoxuan: one of our developers want to chat with you to clarify some concerns<br />
  [8:16:12 AM] jpijeff: yes<br />
  [8:16:15 AM] Hung Hoxuan: ok let me add him<br />
  [8:16:24 AM] *** Hung Hoxuan added Le Tien Trang ***<br />
  [8:16:55 AM] Le Tien Trang: Hi Jeff,<br />
  [8:17:08 AM] jpijeff: Hello<br />
  [8:17:39 AM] Le Tien Trang: I have some questions about Survey tools<br />
  [8:17:45 AM] jpijeff: OK<br />
  [8:18:33 AM] Le Tien Trang: about UUID<br />
  [8:19:08 AM] Le Tien Trang: I can set text {%UUID%} in link Survey Page<br />
  [8:19:31 AM] Le Tien Trang: Which UUID we get and push it to form  Email Content Editor?<br />
  [8:22:01 AM] jpijeff: For text replacement item changed after inserting  into the Queue table you would not use  {%UUID%}  but instead use REPLACETHISUUID with no special characters around it.<br />
  [8:22:46 AM] jpijeff: see the bottom of the ebmsit/management/docs/ebmtraining/doc_DynamicData.cfm page for more details<br />
  [8:24:04 AM] jpijeff: the UUID id not created until the messageing request is inserted into the simplequeue.contactqueue table. When inserting into the table during distribution I use the mySQL function UUID() to generate a unique value.<br />
  [8:24:19 AM] Le Tien Trang: Yes<br />
  [8:24:44 AM] jpijeff: Then when pushing from the queue to a fulfillment device, I then do the final text replacements.<br />
  [8:25:47 AM] jpijeff: Example: https://Surveyportal.EventBasedMessageing.com?surveyID=REPLACETHISUUID<br />
  [8:26:17 AM] Le Tien Trang: https://Surveyportal.EventBasedMessageing.com?BatchID=123&amp;surveyID=REPLACETHISUUID<br />
  [8:26:56 AM] jpijeff: would be replaced with https://Surveyportal.EventBasedMessageing.com?surveyID=34333377-66ee-11e1-b220-0025648bc9c4 in the actual message<br />
  [8:28:04 AM] Le Tien Trang: BatchID will determine which survey email is sent<br />
  [8:28:56 AM] jpijeff: With the UUID you should not need to know the Batch ID in able to determine the survey.<br />
  [8:29:49 AM] jpijeff: You determine the survey based on the values in the XMLControlString_vch found in the same record of the queue table that has the matching UUID<br />
  [8:30:25 AM] Le Tien Trang: ok<br />
  [8:31:01 AM] Le Tien Trang: that is customize case<br />
  [8:31:14 AM] jpijeff: IF you still somehow need the BatchID it is also in this same record in the queue table but everthing you need to process the survey should already be in the XMLControlString<br />
  [8:32:25 AM] jpijeff: Even if all surveys are identical - presuming expiration dates and everthing are all the same there will still be an XMLControlString for each record in the queue - each with an unique UUID<br />
  [8:33:04 AM] Le Tien Trang: in case using UUID in RxmultiList table will create multi connections (1-n)<br />
  [8:34:03 AM] jpijeff: The UUID in the RXMultilist has nothing to do with the UUID in the queue table.<br />
[8:34:53 AM | Edited 8:35:07 AM] Le Tien Trang: [8:34 AM] jpijeff: </p>
<p>&lt;&lt;&lt; queue tableis this contactqueue table?<br />
  [8:35:04 AM] jpijeff: Each survey request regardless of who and how they are sent are done through the queue table<br />
  [8:35:38 AM] jpijeff: yes I use the term queue as shorthand for simplequeue.contactqueue table<br />
  [8:35:56 AM] Le Tien Trang: okie, thanks<br />
  [8:36:56 AM] Le Tien Trang: another question:<br />
  you said IVR survey was completed<br />
  [8:37:25 AM] Le Tien Trang: Is it different with voice survey?<br />
  [8:37:52 AM] jpijeff: You can use the MCID tools to draw an IVR survey on the Stage. IVR stands for Interactive Voice Response. This is the same as Voice.<br />
  [8:38:45 AM] jpijeff: &lt;RXSS&gt;&lt;/RXSS&gt; stuff IS the Voice<br />
  [8:38:55 AM] Le Tien Trang: okie I see<br />
  [8:40:07 AM] jpijeff: Instead of making the user drag and drop the items on the stage and configure each one of the Voice survey parts, the Survey UI should automatically generate the correct structure for each question using TTS by default.<br />
  [8:41:12 AM] jpijeff: IF done correctly there is no need for all of the Enterprise tools from the full EBM site, but a more advanced user COULD choose to modify a survey this way also.<br />
  [8:42:09 AM] jpijeff: Just like the &lt;RXSSEM&gt; part is generated by the Survey<br />
  [8:42:22 AM] jpijeff: UI the &lt;RXSS&gt; part needs to be generated too.<br />
  [8:43:35 AM] Le Tien Trang: You don't want EBM UI using MCID<br />
  [8:44:06 AM] jpijeff: ???<br />
  [8:44:49 AM] jpijeff: Properly formed &lt;RXSS&gt; XML should be interchangeable between full EBM UI and new Surveys UI<br />
  [8:45:28 AM] jpijeff: The Survey UI just limits the amount of choices a simple user will need to make<br />
  [8:46:02 AM | Edited 8:46:21 AM] Le Tien Trang: You don't want EBM user use MCID?<br />
  [8:46:45 AM] jpijeff: I use the term EBM for the whole enterprise wide site that does everthing<br />
  [8:47:29 AM] jpijeff: I use the term Survey UI to refer to the simplified survey portal that may exist independently from the rest of the site.<br />
  [8:47:59 AM] jpijeff: The XML generated by the Survey UI should be fully compatible with the full EBM site.<br />
  [8:48:11 AM] Le Tien Trang: on survey management page will create RXSS tag?<br />
  [8:48:53 AM] jpijeff: The survey UI will be limited to a subset of possible &lt;RXSS&gt; MCID values<br />
  [8:49:18 AM] jpijeff: Yes the Survey UI will create the RXSS tag<br />
  [8:50:02 AM] jpijeff: The EBM site user would then be able to later view and edit the RXSS tag generated by the Survey UI<br />
  [8:51:42 AM] Le Tien Trang: All tags in RXSS generated by survey tool are default values<br />
  [8:52:25 AM] jpijeff: The EBM site user would have avaialable more sofisticated options that would not be avialable to the Survey UI. The EBM user would have to be knowlegable and careful not to create or use options that would break the Survey UI because the Survey UI would not know how to deal with some of the more advanced options.<br />
  [8:53:01 AM] jpijeff: The &quot;default&quot; values would be determined by the needs of the Survey UI.<br />
  [8:55:07 AM | Edited 8:55:45 AM] Le Tien Trang: please provide us some samples of &quot;default&quot; values<br />
  [8:57:45 AM] jpijeff: You would build these default values by building each qustion using the existing MCID tools. The more common rxt's the Survey UI will use will be RXT='2' for single response IVR , RXT='6' for multi string IVR and RXT='3' to record a response.<br />
  [8:58:24 AM] jpijeff: You can look at what you need to change based on each question type.<br />
  [8:59:53 AM] Le Tien Trang: so how to  know a survey need what MICD object?<br />
  [9:00:45 AM] jpijeff: That is where you need to understand what the current EBM site does.<br />
  [9:01:09 AM | Edited 9:01:28 AM] Le Tien Trang: because you said voice survey would be created automatic by creating survey without MCID tool<br />
  [9:02:25 AM] jpijeff: The Survey UI tool would do the automatic creating - the programmer will stilll need to generate the valid XML code to do this. Each question type will have a specific &lt;RXSS&gt; &lt;ELE&gt; structure.<br />
  [9:05:22 AM] Le Tien Trang: so I will create MCID objects in MCID tools then copy  the XML to voice Survey belong to each question<br />
  [9:06:58 AM] jpijeff: YOu will need to programatically switch out the text as spcified by the Survey UI user<br />
  [9:09:15 AM] Le Tien Trang: please clarify more for me<br />
  [9:09:33 AM] jpijeff: You will also need to change options for IVR collection based on what the Survey User specifies.<br />
  [9:12:08 AM] jpijeff: Clarify - If the survey user chooses the question - &quot;What is your faviorite color?&quot; the the TTS audio in the ELE will need to be changed to match the wording of the question.<br />
  [9:12:10 AM] jpijeff:  &lt;ELE BS='1' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='' CK5='-1' CK6='' CK7='' CK8='5' CK9='30' CP='0' DESC='What is your faviorite color?' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RQ='0' RXT='2' X='700' Y='439'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='1'&gt;What is your faviorite color?&lt;/ELE&gt;<br />
  &lt;/ELE&gt;<br />
  [9:12:49 AM] jpijeff: If there are three possible responses then the response map in CK2 will need to be populated accordingly<br />
  [9:15:07 AM] jpijeff: Assume the choise are 1=ORANGE 2=GREEN and 3=RED then the TTS string would need to read out &quot;What is your faviorite color? Press 1 for ORANGE, Press 2 for GREEN, Press 3 for RED.&quot;<br />
  [9:16:01 AM | Removed 9:16:08 AM] Le Tien Trang: This message has been removed.<br />
  [9:17:45 AM] jpijeff: Assuming that this question is QID='4' like the example above and the survey respondent choose 3 for RED then the result string will contain &lt;Q ID='4'&gt;3&lt;/Q&gt;<br />
  [9:18:37 AM] jpijeff: If the QID is defiuned with CP's and RQ's the the result string would contain this information as well<br />
  [9:19:14 AM] jpijeff: Assume CP='5' and RQ='4' then the result would be &lt;Q ID='4' CP='5' RQ='4'&gt;3&lt;/Q&gt;<br />
  [9:20:01 AM] jpijeff: Survey reports could then answer questions like... How many people like red? What percentage of respondents like Red? How many people made it to question 4 in the survey?<br />
  [9:20:53 AM] Le Tien Trang: [9:12 AM] jpijeff: </p>
<p>&lt;&lt;&lt;  &lt;ELE BS='1' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='' CK5='-1' CK6='' CK7='' CK8='5' CK9='30' CP='0' DESC='What is your faviorite color?' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='4' RQ='0' RXT='2' X='700' Y='439'&gt;<br />
  &lt;ELE ID='TTS' RXBR='16' RXVID='1'&gt;What is your faviorite color?&lt;/ELE&gt;<br />
  &lt;/ELE&gt;so above xml snippet is created by MCID tool or voice survey?<br />
  [9:22:42 AM] jpijeff: The above snippet is contstrined to the requirements of the definition of an XML control string. The same string could be generated by wither the EBM sites drag and drop interface OR by the new Survey UI.<br />
  [9:29:01 AM] Le Tien Trang: I know  &quot;EBM sites drag and drop interface&quot; but how to create that xml in new Survey UI<br />
  [9:29:21 AM] Le Tien Trang: please suggest me a solution<br />
  [9:31:07 AM] jpijeff: Based on question types you will need to know which rxt's to use. You would have pre-generatred XML ELE templates in your code. YOu would customize the templates based on the state of the Survey UI.<br />
  [9:33:31 AM] jpijeff: You will need to be familiar with the possible rxt's. You will need to be familiar with the rules of rxts. YOu will need to know what is valid XML and what is not valid XML. We went trhough all of these items as part of building the MCID drag and drop tool.<br />
  [9:37:12 AM] Le Tien Trang: so I will implement following step:<br />
  - Input question to survey tool<br />
  - Then above snippet xml would be generated. But I don't know which rxt is used for this question<br />
  [9:42:10 AM] jpijeff: I can only suggest practice building Voice surveys using the EBM SIte and the MCID drag and drop tool. You can then look at the XML generated foir hints. I sent over several docs in the past relating to the different rxts. Who on the team is most familair with &lt;RXSS&gt; structiures?<br />
  [9:44:15 AM] Le Tien Trang: I have strong knowlege about &lt;RXSS&gt; structiures and how to create it in MCID tool<br />
  [9:44:46 AM] Le Tien Trang: but I don't know to create it in survey tool<br />
  [9:45:06 AM] jpijeff: ??? Hmmmmm<br />
  [9:49:19 AM] jpijeff: The survey tool does not show &lt;RXSS&gt; - just the questions and answers. Start with a simple question type and see if you can manually generate correct &lt;RXSSS&gt; in Parralle as you generate &lt;RXSSEM&gt; for the simple queston type. Once you can do this go on to next hardest question type.<br />
  [9:50:24 AM | Edited 9:50:57 AM] Le Tien Trang: so a the question type will determine rxt?<br />
  [9:51:20 AM] jpijeff: Yes each question type will probably generate differing rxts structures<br />
  [9:51:29 AM] Le Tien Trang: okie<br />
  [9:51:32 AM] Le Tien Trang: :)<br />
  [9:52:01 AM | Edited 9:52:15 AM] Le Tien Trang: that is the problem I confused<br />
  [9:54:24 AM] Le Tien Trang: if you aren't busy, I will have some more questions about report survey<br />
  [9:54:38 AM] jpijeff: ok<br />
  [9:56:46 AM] Le Tien Trang: in the CallResult table, data will update or insert after answering<br />
  [9:58:03 AM] Le Tien Trang: becauce answering by mail and voice are different<br />
  [9:58:07 AM] jpijeff: The data will usally only insert. For the rarer case someone comes back later to finish a survey the data will need to be updated.<br />
  [9:59:12 AM] jpijeff: On an outbound call the survey can only be answered all at once. For an inbound call the survey can be completed later. FOr email the survey can be completed later also.<br />
  [10:01:26 AM] Le Tien Trang: okie<br />
  [10:01:58 AM] Le Tien Trang: the last question that I want to ask you about CPP<br />
  [10:02:08 AM] jpijeff: OK<br />
  [10:03:13 AM] Le Tien Trang: In consumer UI, you said consumer can go to EBM public section to manage preferences<br />
  [10:04:10 AM | Edited 10:04:19 AM] Le Tien Trang: is &quot;manage preferences&quot;  link to access EBM session?<br />
  [10:05:56 AM] jpijeff: The consumer needs to be able to access a public site without lgging into the EBM site. The consumer will still need to authenticate themselves based on email address and CPP Id. The consumer will then only be able to update their contact preferences based on the current CPP Id options<br />
  [10:06:14 AM] jpijeff: The consumer will not be directly logged into any EBM session<br />
  [10:07:35 AM] jpijeff: The consumer will not see or be aware of anything to do with EBM site. The site will be reached through dsome generic DNS entry. The site will need to be able work without all the EBM site session tools being available.<br />
  [10:14:00 AM | Edited 10:14:14 AM] Le Tien Trang: in previous email you said &quot;consumer must login to manage CPP&quot;<br />
  [10:15:25 AM] jpijeff: have you seenthe doc_CusomerPreferencePortal.cfm?<br />
  [10:15:36 AM] Le Tien Trang: what is &quot;manage CPP&quot; in Consumer UI?<br />
  [10:17:06 AM] jpijeff: There is a Consumer UI section in the doc_CusomerPreferencePortal.cfm document. It even has a sample of an integrated UI for an Utility company<br />
  [10:17:23 AM] jpijeff: DO you have access to the doc_CusomerPreferencePortal.cfm?<br />
  [10:17:32 AM] Le Tien Trang: yes I do<br />
  [10:18:56 AM] jpijeff: The consumer UI is a public facing web site where the user passes in or enters a CPP UUID to start. <br />
  Based on UUID differing content is display - &quot;Welcome to XYZ Company's Customer Preference Portal&quot; </p>
<p>Some of the custom fields (like Company Name) in the Consumer UI can pull data from the simpleobjects.useraccount table by getting the CPPUID and matching to the EBM System UserID_int associated with it.</p>
<p>The Consumer then uses their email address to login.</p>
<p>Consumer can request a password be sent to them the first time they try to manage their preferences. If this is the case system needs to create a UUID as a temporary password and then email it to the customer for verification. </p>
<p>Reset password - I Forgot Option.</p>
<p>Option to Require Multi-Factor Authentication every time - passwords only good for one session and are reset to null on first use.</p>
<p>If a new UUID is generated then a verification email must be sent to allow the user to re-log in.</p>
<p>If a new UID is generated all CPPPWD_vch fields need to be updated in RXMultiList that contain a matching CPPID_vch for a given EBM UserID_int. There is only one password at a time and any changes must be made across all of a contacts related contactstrings. Remember all contact strings are related under a given EBM UserID_int by their CPPID_vch. The CPPID_vch is alwyas a valid email address.</p>
<p>Consumer can go to EBM public section to manage preferences<br />
  Consumer can go through AJAX HTML plugin code on an EBM User's Company site section to manage preferences</p>
<p>Consumer must login to manage CPP<br />
  Consumer can set their password first time they log in or continuously request a new one through email each time they log in.<br />
  Consumer can add/remove/change contact strings related to the master eMailAddress in the RXMultiList<br />
  Consumer can order in priority their contact strings - see new field in RXMultiList for CPPPriority_vch</p>
<p>Consumer is shown list of possible Groups to subscribe to:<br />
  Each Group can have differing levels of subscriptions - some may want voice and email - others may only want email. Under a list of each Group the customer may request all or only certain contact strings. </p>
<p>If there is only one Group then the display should only show a list of contact strings.</p>
<p>Consumer can specify one or more Voice phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked</p>
<p>Consumer can specify one or more SMS phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
</p>
<p>Consumer can specify one or more eMail phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
</p>
<p>Password is stored for each CPPID encrypted with mySQL AES encryption using the SESSION.EncryptionKey_UserDB<br />
  ... AND Password_vch = AES_ENCRYPT(&lt;CFQUERYPARAM CFSQLTYPE=&quot;CF_SQL_VARCHAR&quot; VALUE=&quot;#inpPassword#&quot;&gt;, '#APPLICATION.EncryptionKey_UserDB#')<br />
  [10:19:20 AM] Le Tien Trang: Consumer can set their password first time they log in or continuously request a new one through email each time they log in.<br />
  [10:19:28 AM] Le Tien Trang: :)<br />
  [10:19:53 AM] jpijeff: The main tasks are:<br />
  [10:19:56 AM] jpijeff: [10:18 AM] jpijeff: </p>
<p>&lt;&lt;&lt; Consumer is shown list of possible Groups to subscribe to:<br />
  Each Group can have differing levels of subscriptions - some may want voice and email - others may only want email. Under a list of each Group the customer may request all or only certain contact strings. </p>
<p>If there is only one Group then the display should only show a list of contact strings.</p>
<p>Consumer can specify one or more Voice phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked</p>
<p>Consumer can specify one or more SMS phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
</p>
<p>Consumer can specify one or more eMail phone numbers ordered by contact preference - if they are already on main MultiList they are automatically Grouped and Linked<br />
  [10:20:25 AM] Le Tien Trang: okie Jeff<br />
  [10:20:42 AM] Le Tien Trang: thanks so much for you support<br />
  [10:20:49 AM] jpijeff: Good for today?<br />
  [10:21:10 AM] jpijeff: I have another meeting in 10 minutes...<br />
  [10:21:19 AM] Le Tien Trang: okie<br />
  [10:22:03 AM] Le Tien Trang: good bye<br />
  [10:22:28 AM] jpijeff: Good night!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Dear Jeffery  Peterson,</p>
<p>Thank you so  much your remind me. I am sorry about the current progress. At now, I have the  depth joining into all tasks, I promise the best performance as your  expectation as soon.</p>
<p>Thanks!<br />
  Best regards,<br />
  Long Nguyen</p>
<p><strong>From:</strong> Jeff Peterson <a href="mailto:[mailto:JPeterson@messagebroadcast.com]">[mailto:JPeterson@messagebroadcast.com]</a> <br />
  <strong>Sent:</strong> Thursday, March 08, 2012 1:01 AM<br />
  <strong>To:</strong> 'Long Nguyen Huu'<br />
  <strong>Cc:</strong> 'Bruce Watanabe'; 'SeanPower SeanPower'; 'Steve Ho (SETA)';  'messagebroadcast@setacinq.com.vn'<br />
  <strong>Subject:</strong> RE: Questions about Survey Tools<br />
  <strong>Importance:</strong> High</p>
<p>Long,</p>
<p>No. For two months of high priority work this really does not  meet my expectations. <br />
  Even after one month of explaining task I would have expected at  least a prototype where I could actually run through the whole Simplified  Process Flow not just the first stage.</p>
<p>See my other answers below.</p>
<p>&nbsp;</p>
<p><strong>Jeffery Lee Peterson</strong><br />
  <img border="0" width="166" height="37" src="doc_SurveyUI_clip_image001.jpg" alt="Description: cid:image001.jpg@01C97BDC.B32381C0" /><br />
  <strong>Intelligent, Adaptable, Customizable Messaging and Notification</strong> <br />
  MessageBroadcast<br />
  4685 MacArthur Ct., Ste 250, Newport Beach CA &nbsp;92660<u><br />
  </u><a href="http://www.messagebroadcast.com" title="blocked::http://www.messagebroadcast.com/ http://www.messagebroadcast.com/">www.messagebroadcast.com</a><strong> </strong><br />
  <strong>Office</strong>: <em>949-428-4899</em><br />
  <strong>Mobile</strong>: <em>949-400-0553</em><br />
  <strong>email</strong><em>:</em><em> <a href="mailto:jpeterson@messagebroadcast.com">jpeterson@messagebroadcast.com</a></em><br />
  <strong>Trouble Reporting 24x7</strong>: <em>949-428-3111</em> <em>press 0</em></p>
<p><strong>From:</strong> Long Nguyen Huu <a href="mailto:[mailto:longnh6176@setacinq.com.vn]">[mailto:longnh6176@setacinq.com.vn]</a> <br />
  <strong>Sent:</strong> Wednesday, March 07, 2012 3:11 AM<br />
  <strong>To:</strong> Jeff Peterson<br />
  <strong>Cc:</strong> 'Bruce Watanabe'; 'SeanPower SeanPower'; 'Steve Ho (SETA)'; <a href="mailto:messagebroadcast@setacinq.com.vn">messagebroadcast@setacinq.com.vn</a><br />
  <strong>Subject:</strong> Questions about Survey Tools</p>
<p>Dear Jeffery  Peterson,</p>
<p>Please review  all the questions below and then give us your detail comments about it as soon  as possible.</p>
<p>1. We have  already completed create Survey Section. You can see the video attached or  check it on EBM site under Survey Management tab<br />
  &nbsp;&nbsp; àIf it met your requirement or not just let  we know.<br />
  No. For two months of high priority work this really does not  meet my expectations.<br />
  How does this work with voice? Auto generate MCID&rsquo;s? TTS by  default? Edit with recordings?<br />
  Default graphics is just place holder graphic grabbed from other  place holder.<br />
  While technically good at generating questions and answers there  is not enough &nbsp;visual cues as to what the user is expected to do. This  tool is not for technical people but to be used by regular business/marketing  people to generate surveys.<br />
  The batch picker is just a list of meaningless (to the UI user)  Batch Ids - I have been looking for a better batch picker across multiple  projects (surveys, reporting portal) – I just went ahead and created on myself  yesterday. See Session/Batch/dsp_BatchPicker.cfm and the Docs for it under  management/Docs/EBMTraining/doc_BatchPicker.cfm<br />
  Why must an existing batch be chosen first? While it may be  optional to choose an existing Batch – a new batch should be created by default  without any additional UI clicks. An optional link/button to allow user to  choose an existing batch. Give visual feedback once a batch is chosen.  Description as well as ID.</p>
<p>&nbsp;</p>
<p>2.Create Survey  section is finished. We added 5 types Survey question <br />
  -One  Selection(group radio selection, one selection/one row and user can only choose  one)<br />
  -Multi  Selection(group checkbox and user can choose many)<br />
  -Comment(just  let user input their comment/feedback)<br />
  -Rating  scale(group radio selection, all selection per one row and user can only choose  one)<br />
  -Matrix of  choose(many selection for many row but user can choose only one selection per  one row)<br />
  àDo you need more  type of questions? If yes, Can you explain for us?<br />
  To get a basic site up quickly, these are enough questions to  start.<br />
  For the future:<br />
  What other web survey companies have you looked at? <br />
  What are other web survey companies offering for question  structure?<br />
  What sample surveys do you see in daily life? Over here I get  survey offers almost daily. How about samples I sent over previously?</p>
<p>&nbsp;</p>
<p>3.As you said before  the Survey need to integrated back into main EBM drag and drop interface.<br />
  àWill we&nbsp;  integrated Survey into MCID Tools by add a new Survey object in COMMS section(left menu)?<br />
  Survey  Questions can be editable through a Survey object by drag drop it into MCID  main stage.<br />
  If user click  to Survey object a popup will be show extract like when user edit Survey  question under Survey Management tab on EBM site .<br />
  It will be show  like that:</p>
<p><img src="doc_SurveyUI_clip_image003.jpg" alt="" width="504" height="429" border="0" /></p>
<p>&nbsp;</p>
<p>àIs it OK if we  implement it? <br />
  To get a basic site up quickly, &nbsp;Yes that will be fine for  now.<br />
  In future how will questions be represented on stage? Each  question should be a stage object that can be individually edited/orders/removed/added.</p>
<p>&nbsp;</p>
<p>4. About send  Survey to customer&rsquo;s email address<br />
  We know the EBM  user selects or uploads a list of targets for the survey into the  simplelists.rxmultilists table.<br />
  Email  invitations can be sent with personalized content</p>
<p>After edit  Survey question on MCID main stage ,user edit Email invitation.<br />
  àHow can we send  a link Survey with UUID &nbsp;to combo of customer email address? – see my answer to question 5 below  RE: dynamic content and Queued values </p>
<p>We suggest we  can do that when user click a [Take Survey] button on Email Content Editor then  a link will be generate like image below</p>
<ul>
  <li>Is it OK?</li>
  <li>This would be better if just another type of dynamic content  that can be inserted from the <u>Insert Dynamic Content</u> link</li>
</ul>
<p><img src="doc_SurveyUI_clip_image005.jpg" alt="" width="1175" height="555" border="0" /></p>
<p>&nbsp;</p>
<p>5.We have been a little confuse about custom element<br />
  You wrote that  &ldquo;Requests are sent out via email and voice through the simplequeue.contactqueue table  using a customized XMLControlString&nbsp; stored here for each request. UUIDs  are assigned during this step. If custom elements are used in the survey  content, data from the&nbsp; simplelists.rxmultilists is used in combination  with the master batch XMLControlString&nbsp; stored in simpleobjects.batch. If  the survey is not customized in any way then the XMLControlString&nbsp; stored  in the simplequeue.contactqueue matches the XMLControlString in the  simpleobjects.batch table. This is a key component of how the EBM system works.  Everyone on the team needs to understand this for anything to make sense&rdquo;.</p>
<p>àCan you explain  this sentences &ldquo;If custom elements are used in the survey content&rdquo; and &ldquo; If the  survey is not customized in any way&rdquo;?</p>
<p>You need to fully understand management/Docs/EBMTraining/doc_DynamicData.cfm  for this to make sense. I added a new section for dealing with Queued data.</p>
<p>Queued Values</p>
<p>While data from the user is replaced in the XMLControlString  prior to inserting into the queue, certain fields are best left for swap out  when moving from the queue to distribution&gt;</p>
<p>This is done in management/distribution/act_TransferQueue.cfm</p>
<p>Any text that matches a specific string <br />
  REPLACETHISUUID - this will be replaced with the UUID that was  generated while inserting into the Queue table.<br />
  REPLACETHISCONTACTSTRING- this will be replaced with the  Contactstring that was used while inserting into the Queue table.<br />
  REPLACETHISBATCHIDSTRING- this will be replaced with the BatchID  that was used while inserting into the Queue table.<br />
  REPLACETHISDQDTSIDSTRING- this will be replaced with the DTSID  that was generated while inserting into the Queue table.</p>
<p>&nbsp;</p>
<p>&lt;!--- UUID ---&gt; <br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch,  &quot;REPLACETHISUUID&quot;, &quot;#inpCurrUUID#&quot;, &quot;ALL&quot;)&gt;</p>
<p>&lt;!--- Contact String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch,  &quot;REPLACETHISCONTACTSTRING&quot;, &quot;#inpDialString#&quot;,  &quot;ALL&quot;)&gt;</p>
<p>&lt;!--- Batch String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch,  &quot;REPLACETHISBATCHIDSTRING&quot;, &quot;#INPBATCHID#&quot;,  &quot;ALL&quot;)&gt;</p>
<p>&lt;!--- Distrubution Queue DTS Id String ---&gt;<br />
  &lt;cfset XMLControlString_vch = REPLACE(XMLControlString_vch,  &quot;REPLACETHISDQDTSIDSTRING&quot;, &quot;#inpDQDTSId#&quot;,  &quot;ALL&quot;)&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>We understand  that the UUID is used to lookup which questions are to be asked and UUID can  get from simplelists.rxmultilists or simplequeue.contactqueue table</p>
<p>Two way we can  get UUID – no only one way  to get UUID for surveys a. <br />
  a. <br />
  -simpleobjects.batch(batchid,xmlcontrolstring_vch)<br />
  - simplequeue.contactqueue  (BatchId,DTS_UUID_vch)<br />
  b.<br />
  -simpleobjects.batch(batchid,userid,xmlcontrolstring_vch)<br />
  -simplelists.rxmultilists(userid,uniquecustomer_uuid_vch) </p>
<p>In case (a)  each UUID in contactqueue table map to one XMLControlString(contain Survey).That  easy for us get UUID in this table</p>
<p>But in case (b)  each UUID in RXMultiLists map to many XMLControlString <br />
  àWhich UUID we  get and push it to form &nbsp;Email Content Editor? <br />
  – See my  earlier notes on UUID above </p>
<p>6.Collect  responses and save to database section <br />
  About survey  response site<br />
  àWhere is Survey folder  place? Could it be outside Public folder?<br />
  This should be a standalone folder under the public site. This  folder should be publishable to any web site, but work as stand alone under EBM  site. No need for EBM user id to use but just need valid survey <br />
  UUID to take survey.</p>
<p>7. You  wrote - &ldquo;Analyse the responses % for each  response to each question, Partial completed, % complete, and other reports…&rdquo; <br />
  àWill we analyse  reponses for each Survey or for group of Survey? -  Both<br />
  àWhat about other  reports? Can you clarify this task? – Google is  your friend. What do other survey sites offer for reports? Beyond the most  basic and obvious reporting examples I am looking for you guys to give me  sample surveys to pick from. </p>
<p>&nbsp;</p>
<p>Thank you so much!<br />
  Best regards,<br />
  Long Nguyen</p>
<p>&nbsp;</p>
<p><strong>From:</strong> Jeff Peterson <a href="mailto:[mailto:JPeterson@messagebroadcast.com]">[mailto:JPeterson@messagebroadcast.com]</a> <br />
  <strong>Sent:</strong> Wednesday, February 22, 2012 12:15 AM<br />
  <strong>To:</strong> 'Long Nguyen Huu'<br />
  <strong>Cc:</strong> 'Steve Ho (SETA)'; 'bruce@seta-international.com'; 'seanpower@seta-international.com';  'messagebroadcast@setacinq.com.vn'<br />
  <strong>Subject:</strong> RE: Questions about Survey Tools</p>
<p>Long,</p>
<p>&nbsp;</p>
<p>No. I still do not see the need for the simpleobjects.survey  table. While this might be easier/quicker it does not integrate well into the  overall EBM API.<br />
  &nbsp;&nbsp;&nbsp; -SurveyId_int – This is  covered with a Batch ID – we can either add a field to the simpleobjects.batch  or do a search for Batches XMLControlString_vch that contains a &lt;RXSSEM&gt;  object.<br />
  &nbsp;&nbsp;&nbsp; -Description_vch – This can be  stored in the top level &lt;  RXSSEM DESC=&rsquo;This is a title here!&rsquo;&gt; similar to what we do for other objects  in the XMLControlString OR just use the top level Batch Description. <br />
  &nbsp;&nbsp; &nbsp;-QuestionXML_vch – These are  just &lt;Q&gt; elements under the &lt; RXSSEM&gt; tag <br />
  &nbsp;&nbsp;&nbsp; -ValidDay_int(how long a survey can be valid  for since EBM user create that survey) – Can be another tag under the &lt; RXSSEM&gt; element. &lt;EXP  =&rsquo;2012-02-28 00:00:00&rsquo;&gt; or can be a specific date relative to when sent to  the simplequeue.contactqueue table &lt;EXP =&rsquo;3 DAYS&rsquo;&gt; <br />
  &nbsp;&nbsp;&nbsp; -EmailInvitationContent_vch(contains user  email content and a link ) – Use the same structure as the DM MT=&rsquo;3&rsquo; or  if you really need a separate one mimic the top level DM MT=&rsquo;3&rsquo; underneath the &lt; RXSSEM&gt; </p>
<p>The simplified survey UI is meant as a standalone addition to  the EBM. The XML will need to be editable in the near future within the EBM but  not required to use the EBM to create. If we follow the logic I have described  then this should be no problem and this can be done similar to what I did with the  Rules Objects for a new set of RXSSEM objects.</p>
<p>1St – create simplified UI that generates the XML as  we have discussed – this should be done first and in the near future. The  simplified UI will make most of the decisions for the user and will automatically  generate the backend XML with default values that make sense to the flow of the  survey.</p>
<p>2cd – After survey is fully working we can worry about  integrating back into main EBM drag and drop interface. Based on how I have  laid it out this should only take a week.</p>
<p>&nbsp;</p>
<p><strong>Jeffery Lee Peterson</strong><br />
  <img border="0" width="166" height="37" src="doc_SurveyUI_clip_image001_0000.jpg" alt="Description: Description: Description: Description: cid:image001.jpg@01C97BDC.B32381C0" /><br />
  <strong>Intelligent, Adaptable, Customizable Messaging and Notification</strong> <br />
  MessageBroadcast<br />
  4685 MacArthur Ct., Ste 250, Newport Beach CA &nbsp;92660<u><br />
  </u><a href="http://www.messagebroadcast.com" title="blocked::http://www.messagebroadcast.com/ http://www.messagebroadcast.com/">www.messagebroadcast.com</a><strong> </strong><br />
  <strong>Office</strong>: <em>949-428-4899</em><br />
  <strong>Mobile</strong>: <em>949-400-0553</em><br />
  <strong>email</strong><em>:</em><a href="mailto:jpeterson@messagebroadcast.com"><em>jpeterson@messagebroadcast.com</em></a><br />
  <strong>Trouble Reporting 24x7</strong>: <em>949-428-3111</em> <em>press 0</em></p>
<p><strong>From:</strong> Long Nguyen Huu <a href="mailto:[mailto:longnh6176@setacinq.com.vn]">[mailto:longnh6176@setacinq.com.vn]</a> <br />
  <strong>Sent:</strong> Tuesday, February 21, 2012 3:28 AM<br />
  <strong>To:</strong> Jeff Peterson<br />
  <strong>Cc:</strong> 'Steve Ho (SETA)'; <a href="mailto:bruce@seta-international.com">bruce@seta-international.com</a>; <a href="mailto:seanpower@seta-international.com">seanpower@seta-international.com</a>; <a href="mailto:messagebroadcast@setacinq.com.vn">messagebroadcast@setacinq.com.vn</a><br />
  <strong>Subject:</strong> RE: Questions about Survey Tools</p>
<p>Dear Jeff Peterson,</p>
<p>Thank for your answers before. We clear any more about Survey  but we still need your help to answer any more questions before implement it.<br />
  Please review our questions below:</p>
<p>Base on previous answers, We create simpleobjects.survey table store  general survey info include:<br />
  &nbsp;&nbsp;&nbsp; -SurveyId_int<br />
  &nbsp;&nbsp;&nbsp; -Description_vch<br />
  &nbsp;&nbsp; &nbsp;-QuestionXML_vch<br />
  &nbsp;&nbsp;&nbsp; -ValidDay_int(how long a survey can be valid  for since EBM user create that survey)<br />
  &nbsp;&nbsp;&nbsp; -EmailInvitationContent_vch(contains user  email content and a link )</p>
<p>In image attached file, after user clicks on [Survey] button  &nbsp;a dialog will be appear then let user choose one of Surveys that are  already defined before.</p>
<p><strong>1)Before add  a Survey to XMLControlString_vch</strong></p>
<p><strong>&lt;DM  BS='0' DSUID='10' Desc='Description Not Specified' LIB='0' MT='1' PT='12'&gt;</strong><br />
  <strong>&lt;ELE  ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM  BS='0' DSUID='10' Desc='Description Not Specified' LIB='0' MT='2' PT='12'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM  BS='0' DESC='Description Not Specified' DSUID='10' LIB='0' MT='3' PT='12'  X='100' Y='450'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' CK2=''  CK3='' CK4='&rsquo;</strong><br />
  <strong>CK5=' CK6=''  CK7='' CK8=' CK9='' DESC=&rsquo;' DSUID='10' LINK='0' QID='1' RXT='13' X='0'  Y='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;RULES&gt;</strong><br />
  <strong>&lt;/RULES&gt;</strong><br />
  <strong>&lt;RXSS  X='100' Y='129'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='0'  CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0' DESC='Have many times in the last  year have you used our Survey in the last year? For never press 0 For once  press 1 For twice press 2 for thrice press 3 for more than three times press 4'  DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RQ='0' RXT='2' X='100'  Y='249'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='0'  CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0' DESC='How did you first hear about  our web site? For Television Press 1 For Social Sites Press 2 For Word of Mouth  press 3 ' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RQ='0' RXT='2'  X='210' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='1' CK10='' CK11='' CK12='' CK13='' CK2='60' CK3='0' CK4='0'  CK5='0' CK6='' CK7='0' CK8='-1' CK9='' DESC='How do you fell about UI of this  Survey?' DSUID='10' LINK='-1' QID='3' RXT='3' X='320' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/RXSS&gt;</strong><br />
  <strong>&lt;CCD  CID='7777777777' ESI='-12'&gt;0&lt;/CCD&gt;</strong></p>
<p><strong>2</strong><strong>)After that </strong><br />
  -CK4 of Email  will be replace with EmailInvitationContent_vch of Survey<br />
  &nbsp;  XMLControlString_vch will be like that:</p>
<p><strong>&lt;DM  BS='0' DSUID='10' Desc='Description Not Specified' LIB='0' MT='1' PT='12'&gt;</strong><br />
  <strong>&lt;ELE  ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM  BS='0' DSUID='10' Desc='Description Not Specified' LIB='0' MT='2' PT='12'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM  BS='0' DESC='Description Not Specified' DSUID='10' LIB='0' MT='3' PT='12'  X='100' Y='450'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='' CK12='' CK13='' CK14='' CK15='-1' CK2=''  CK3='' CK4='</strong><br />
  &nbsp;&lt;p&gt; We are  conducting a survey, and your response would be appreciated. &lt;/p&gt;<br />
  &nbsp;&nbsp;  &lt;p&gt;Click on the following link to begin.  http:EBMSurveySystem.com?SID=00000000001&lt;/p&gt;<br />
  &nbsp;&lt;p&gt;  This link is uniquely tied to this survey and your email address.  &nbsp;&lt;p&gt;<br />
  &nbsp;&lt;p&gt;  Please do not forward this message.&nbsp; &nbsp;&nbsp;&lt;p&gt;<br />
  &nbsp;&lt;p&gt; We  very much appreciate your completing this survey. &nbsp;&lt;p&gt;<br />
  &nbsp;&lt;p&gt;  Thanks for your participation!&nbsp;&nbsp; &nbsp;&lt;p&gt;&rsquo;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp; </strong><br />
  <strong>CK5= CK6=''  CK7='' CK8='Please take our survey' CK9='' DESC='Simple Survey Invitation'  DSUID='10' LINK='0' QID='1' RXT='13' X='0' Y='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;RULES&gt;</strong><br />
  <strong>&lt;/RULES&gt;</strong><br />
  <strong>&lt;RXSS  X='100' Y='129'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='0'  CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0' DESC='Have many times in the last  year have you used our Survey in the last year? For never press 0 For once  press 1 For twice press 2 for thrice press 3 for more than three times press 4'  DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='1' RQ='0' RXT='2' X='100'  Y='249'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='0' CK10='' CK11='0' CK12='5' CK2='()' CK3='' CK4='0'  CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0' DESC='How did you first hear about  our web site? For Television Press 1 For Social Sites Press 2 For Word of Mouth  press 3 ' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1' QID='2' RQ='0' RXT='2'  X='210' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp;  &lt;ELE BS='0' CK1='1' CK10='' CK11='' CK12='' CK13='' CK2='60' CK3='0' CK4='0'  CK5='0' CK6='' CK7='0' CK8='-1' CK9='' DESC='How do you fell about UI of this  Survey?' DSUID='10' LINK='-1' QID='3' RXT='3' X='320' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/RXSS&gt;</strong><br />
  &lt; RXSSEM&gt;<br />
  &lt;Q ID=''1&quot; TYPE=&quot;OneSelection&quot; TEXT=&quot;Have  many times in the last year have you used our Survey in the last year  &quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;1&quot;  TEXT=&quot;ZERO&quot; &gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;2&quot; TEXT  =&quot;ONE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;3&quot; TEXT  =&quot;TWICE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;4&quot; TEXT  =&quot;THREE OR  MORE&quot;&gt;&lt;/OPTION&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
  &lt;/Q&gt;<br />
  &lt;Q ID=''2&quot; TYPE=&quot;MultipleSelection&quot; TEXT=&quot;How  did you first hear about our web site&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;1&quot;  TEXT=&quot;Television&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION ID=&quot;2&quot;  TEXT=&quot;Social Sites(Facebook,Twiter)&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp; &lt;OPTION&nbsp; ID=&quot;3&quot;  TEXT=&quot;World of mouth&quot;&gt;&lt;/OPTION&gt;<br />
  &lt;/Q&gt;<br />
  &lt;Q ID=&quot;3&quot; TYPE=&quot;Comment&quot; TEXT=&quot;How do  you fell about UI of this Survey&quot;&gt;&lt;/Q&gt;<br />
  &lt; RXSSEM&gt;<br />
  <strong>&lt;CCD  CID='7777777777' ESI='-12' </strong>SEXP=&rsquo;10&rsquo;<strong>&gt;0&lt;/CCD&gt;</strong></p>
<p>If you agree with our idea, we will implement it soon.<br />
  For the progress of project is better than, Please answer about  it as soon as possible!</p>
<p>Thank you so much!<br />
  Best regards,<br />
  Long Nguyen</p>
<p><strong>From:</strong> Jeff Peterson <a href="mailto:[mailto:JPeterson@messagebroadcast.com]">[mailto:JPeterson@messagebroadcast.com]</a> <br />
  <strong>Sent:</strong> Tuesday, February 21, 2012 1:40 AM<br />
  <strong>To:</strong> 'Long Nguyen Huu'<br />
  <strong>Cc:</strong> 'Steve Ho (SETA)'; 'bruce@seta-international.com';  'seanpower@seta-international.com'; 'messagebroadcast@setacinq.com.vn'<br />
  <strong>Subject:</strong> RE: Questions about Survey Tools</p>
<p>Long,</p>
<p>&nbsp;</p>
<p>See my additional detailed answers below embedded in your  original questions.</p>
<p>&nbsp;</p>
<p>Simplified Process Flow:</p>
<ul>
  <li>EBM user creates a survey using the simple UI and stores the  questions and answer choices in the XMLControlString in the simpleobjects.batch  table</li>
  <li>The EBM user selects or uploads a list of targets for the survey  into the simplelists.rxmultilists table. Targets can be both email addresses  and/or &nbsp;phone numbers.</li>
  <li>Requests are sent out via  email and voice through the simplequeue.contactqueue table using a customized  XMLControlString &nbsp;stored here for each request. UUIDs are assigned during  this step. If custom elements are used in the survey content, data from the  &nbsp;simplelists.rxmultilists is used in combination with the master batch  XMLControlString &nbsp;stored in simpleobjects.batch. If the survey is not  customized in any way then the XMLControlString &nbsp;stored in the  simplequeue.contactqueue matches the XMLControlString in the simpleobjects.batch  table. <strong><em>This is a key component of how the EBM system works. Everyone on  the team needs to understand this for anything to make sense.</em></strong></li>
  <li>Voice and email Request fulfillment is taken care of  asynchronously by the RXDialer equipment on our side. </li>
</ul>
<p>&nbsp;</p>
<ul>
  <li>When an end user clicks a link in their survey invitation email  they are taken to a site to answer the questions. The UUID is part of the email  link in the original invitation. If a user gets to the survey site without a  UUID, they are asked to enter one as provided in the original request. </li>
</ul>
<ul>
  <li>The UUID is used to lookup which questions are to be asked from  the customized XMControlString in the simplequeue.contactqueue. Logic can be used  to verify if survey request has expired yet. Logic can also be used to check if  this is a continuation of a previously started survey and navigate accordingly.</li>
</ul>
<p>&nbsp;</p>
<ul>
  <li>Voice and eMail Survey answers are stored in the  XMLResultStr_vch in the simpleresults.callresults table.</li>
</ul>
<ul>
  <li>EBM user then later returns to look at reporting for ongoing  survey campaigns.</li>
</ul>
<p>&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Sample Master XML Control String with eMail survey questions  embedded and invitation email defined</strong></p>
<p><strong>&lt;DM BS='0' DSUID='10' Desc='Description Not Specified'  LIB='0' MT='1' PT='12'&gt;</strong><br />
  <strong>&lt;ELE ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM BS='0' DSUID='10' Desc='Description Not Specified'  LIB='0' MT='2' PT='12'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;ELE ID='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;DM BS='0' DESC='Description Not Specified' DSUID='10'  LIB='0' MT='3' PT='12' X='100' Y='450'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;ELE BS='0' CK1='0' CK10='' CK11=''  CK12='' CK13='' CK14='' CK15='-1' CK2='' CK3='' CK4='&lt;p&gt;Dear {%FirstName_vch%}&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;&amp;nbsp;&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;Your opinion matters to  us:&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;&amp;nbsp;&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;Lick on the following link to begin.  http:EBMSurveySystem.com?SID=00000000001&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;&amp;nbsp;&lt;/p&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;p&gt;&amp;nbsp;&lt;/p&gt;'  CK5='NoReply@MBCampaign.com' CK6='' CK7='' CK8='Please take our survey' CK9=''  DESC='Simple Survey Invitation' DSUID='10' LINK='0' QID='1' RXT='13' X='0'  Y='0'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/DM&gt;</strong><br />
  <strong>&lt;RULES&gt;</strong><br />
  <strong>&lt;/RULES&gt;</strong><br />
  <strong>&lt;RXSS X='100' Y='129'&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;ELE BS='0' CK1='0' CK10='' CK11='0'  CK12='5' CK2='()' CK3='' CK4='0' CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0'  DESC='Have many times in the last year have you used our Survey in the last  year? For never press 0 For once press 1 For twice press 2 for thrice press 3  for more than three times press 4' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1'  QID='1' RQ='0' RXT='2' X='100' Y='249'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;ELE BS='0' CK1='0' CK10='' CK11='0'  CK12='5' CK2='()' CK3='' CK4='0' CK5='-1' CK6='' CK7='' CK8='5' CK9='' CP='0' DESC='How  did you first hear about our web site? For Television Press 1 For Social Sites  Press 2 For Word of Mouth press 3 ' DI='0' DS='0' DSE='0' DSUID='10' LINK='-1'  QID='2' RQ='0' RXT='2' X='210' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&nbsp;&nbsp;&nbsp; &lt;ELE BS='0' CK1='1' CK10='' CK11='' CK12=''  CK13='' CK2='60' CK3='0' CK4='0' CK5='0' CK6='' CK7='0' CK8='-1' CK9=''  DESC='How do you fell about UI of this Survey?' DSUID='10' LINK='-1' QID='3'  RXT='3' X='320' Y='240'&gt;0&lt;/ELE&gt;</strong><br />
  <strong>&lt;/RXSS&gt;</strong><br />
  &lt;Questions&gt;<br />
  &lt;Q ID=''1&quot;  TYPE=&quot;OneSelection&quot; TEXT=&quot;Have many times in the last year have  you used our Survey in the last year &quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;1&quot; TEXT=&quot;ZERO&quot; &gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;2&quot; TEXT =&quot;ONE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;3&quot; TEXT =&quot;TWICE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;4&quot; TEXT =&quot;THREE OR  MORE&quot;&gt;&lt;/OPTION&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
  &lt;/Q&gt;<br />
  &lt;Q ID=''2&quot;  TYPE=&quot;MultipleSelection&quot; TEXT=&quot;How did you first hear about our  web site&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;1&quot; TEXT=&quot;Television&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;2&quot; TEXT=&quot;Social  Sites(Facebook,Twiter)&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION&nbsp; ID=&quot;3&quot; TEXT=&quot;World of mouth&quot;&gt;&lt;/OPTION&gt;<br />
  &lt;/Q&gt;<br />
  &lt;Q ID=&quot;3&quot;  TYPE=&quot;Comment&quot; TEXT=&quot;How do you fell about UI of this  Survey&quot;&gt;&lt;/Q&gt;<br />
  &lt;Questions&gt;<br />
  <strong>&lt;CCD CID='7777777777' ESI='-12'&gt;0&lt;/CCD&gt;</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Even when using the simple UI to create the surveys should  remain fully editable from the advanced UI. Not the use of rxt 2 for the IVR  questions and the rxt 3 for the free form recorded reponse.<br />
  <img border="0" width="562" height="766" src="doc_SurveyUI_clip_image007.jpg" alt="Description: Description: Description: cid:image002.png@01CCFBA1.9C74F9C0" /></p>
<p>Email invitations can be sent with personalized content<br />
  <img border="0" width="1240" height="677" src="doc_SurveyUI_clip_image009.jpg" alt="Description: Description: Description: cid:image003.png@01CCFBA1.9C74F9C0" /></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Jeffery Lee Peterson</strong><br />
  <img border="0" width="166" height="37" src="doc_SurveyUI_clip_image001_0001.jpg" alt="Description: Description: Description: Description: cid:image001.jpg@01C97BDC.B32381C0" /><br />
  <strong>Intelligent, Adaptable, Customizable Messaging and Notification</strong> <br />
  MessageBroadcast<br />
  4685 MacArthur Ct., Ste 250, Newport Beach CA &nbsp;92660<u><br />
  </u><a href="http://www.messagebroadcast.com" title="blocked::http://www.messagebroadcast.com/ http://www.messagebroadcast.com/">www.messagebroadcast.com</a><strong> </strong><br />
  <strong>Office</strong>: <em>949-428-4899</em><br />
  <strong>Mobile</strong>: <em>949-400-0553</em><br />
  <strong>email</strong><em>:</em><a href="mailto:jpeterson@messagebroadcast.com"><em>jpeterson@messagebroadcast.com</em></a><br />
  <strong>Trouble Reporting 24x7</strong>: <em>949-428-3111</em> <em>press 0</em></p>
<p><strong>From:</strong> Long Nguyen Huu <a href="mailto:[mailto:longnh6176@setacinq.com.vn]">[mailto:longnh6176@setacinq.com.vn]</a> <br />
  <strong>Sent:</strong> Friday, February 17, 2012 12:13 AM<br />
  <strong>To:</strong> Jeff Peterson<br />
  <strong>Cc:</strong> 'Steve Ho (SETA)'; <a href="mailto:bruce@seta-international.com">bruce@seta-international.com</a>; <a href="mailto:seanpower@seta-international.com">seanpower@seta-international.com</a>; <a href="mailto:messagebroadcast@setacinq.com.vn">messagebroadcast@setacinq.com.vn</a><br />
  <strong>Subject:</strong> Questions about Survey Tools</p>
<p>Dear Jeff Peterson,</p>
<p>We are developing Survey Tools. Please review all the questions  below and then give us your detail comments about it.</p>
<p>1. We created the simple UI for Survey Tools. Please review the  video attached.</p>
<p>In the latest mail you said &nbsp;we should store Survey  information in the Batch XMLControlString.<br />
  àDid you mean we should store all surveys and questions that  are created  by user in that column but data store in Batch XMLControlString is generated by  MCID Tools?<br />
  We don&rsquo;t understand because Survey Section separated with MCID  Tools.</p>
<p>Answer JLP: For the example below the XML generated for the email  survey would be stored under the same XMLControlString used for the MCID tools.  If we were to take it further and this were both a Voice/IVR AND an eMail  Survey the XMLControlString would contain both the &lt;RXSS&gt; and the  &lt;Questions&gt; tags.</p>
<p>In a more complex example the questions could include custom data  fields from the RXMultiLists just like the Voice/IVR MCID also can. Reference  to Names, Places , Dates, Dollar amounts could then be inserted into survey at  distribution time</p>
<p>&nbsp;</p>
<p>2. In our opinion, we designed database store Survey &nbsp;like  image attached</p>
<p>Survey table: store basic information for each survey is created  by internal user (Question_XML &nbsp;&nbsp;store all questons as XML String)</p>
<p>Survey_User_Email table: If delivery survey via Email this table  store UID and status of it.</p>
<p>simpleaccount.useraccount: already exists. </p>
<p><strong>We have some questions about </strong><strong>additional survey tools</strong><strong>:</strong><strong> </strong><br />
  1.We design database above for store Email Survey.<br />
  As your answer below User sends survey request to Customers via Voice/IVR  and email via the existing EBM system<br />
  Final result Voice/IVR and SMS survey store &nbsp;in  simpleresults.callresults &nbsp;XMLResultStrs column </p>
<p>Step 3 below &nbsp;Run surveys by web, email, SMS or IVR/voice <br />
  +On SETA side we current work on create survey and send a link with  UID to email&rsquo;s user. Is that right? <br />
  Answer JLP: Yes - Another step would to be to design an email  invitation using the other EBM tools. Store invitation structure under a &lt;DM  MT=&rsquo;3&rsquo;&gt; email Object</p>
<p>&nbsp;</p>
<p>+With Email Survey. which table we get customer&rsquo;s  email address? <br />
  In design database survey above we suggest the table  simpleaccount.useraccount<br />
  Answer JLP: No – email address targets are defined in the  simplelists.rxmultilists table NOT the EBM user Account that created the  survey.</p>
<p>2. After add some questions(See video above) will generate XML  like that <br />
  Answer JLP: Can you change the &lt;Questions&gt; tag to  &lt;RXSSEM&gt;</p>
<p>&lt;Questions&gt;<br />
  &lt;Q ID=''1&quot;  TYPE=&quot;OneSelection&quot; TEXT=&quot;Have many times in the last year have  you used our Survey in the last year &quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;1&quot; TEXT=&quot;ZERO&quot; &gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;2&quot; TEXT =&quot;ONE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;3&quot; TEXT =&quot;TWICE&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;4&quot; TEXT =&quot;THREE OR  MORE&quot;&gt;&lt;/OPTION&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
  &lt;/Q&gt;<br />
  &lt;Q ID=''2&quot;  TYPE=&quot;MultipleSelection&quot; TEXT=&quot;How did you first hear about our  web site&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;1&quot; TEXT=&quot;Television&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION ID=&quot;2&quot; TEXT=&quot;Social  Sites(Facebook,Twiter)&quot;&gt;&lt;/OPTION&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;  &lt;OPTION&nbsp; ID=&quot;3&quot; TEXT=&quot;World of  mouth&quot;&gt;&lt;/OPTION&gt;<br />
  &lt;/Q&gt;<br />
  &lt;Q ID=&quot;3&quot;  TYPE=&quot;Comment&quot; TEXT=&quot;How do you fell about UI of this  Survey&quot;&gt;&lt;/Q&gt;<br />
  &lt;Questions&gt;</p>
<p>With Email channel:<br />
  When user answer &nbsp;xml string will generate like that <br />
  &lt;Response Channel=&quot;Email&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''1&quot; CP=&quot;1&quot;&gt;4&lt;/Q&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''2&quot; CP=&quot;1&quot;&gt;1,3&lt;/Q&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=&quot;3&quot; CP=&quot;0&quot;&gt;&lt;/Q&gt;<br />
  &lt;/Response&gt;</p>
<p>CP:check point use for check questions answer or not<br />
  Answer JLP: No – currently we only store responses for fully  answered questions. CheckPoint CP is used to look for completion points. Some  surveys have additional optional questions that can be safely ignored or the  survey can have questions that jump around based on previous reposnes. CP is  used to make sure a certain point of the survey is reached. </p>
<p>If only the first two questions are answered than the response would  look like: <br />
  &lt;Response Channel=&quot;Email&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''1&quot; CP=&quot;1&quot;&gt;4&lt;/Q&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''2&quot; CP=&quot;2&quot;&gt;1,3&lt;/Q&gt;<br />
  &lt;/Response&gt;<br />
  If the survey respondent comes back later the response string can be  searched for where they left off and then continue the survey from there.</p>
<p>&nbsp;</p>
<p>As we discussed before all responses will be stored in  simpleresults.callresults XMLResultStrs column.<br />
  Ask:  Is that OK if we store questions and answers like that?<br />
  Answer JLP: Yes – in case of multichannel survey - Please add indicator  for email responses like below using a response type key RT=&rsquo;1&rsquo; for Voice and  RT=&rsquo;3&rsquo; for email<br />
  &lt;Response Channel=&quot;Email&quot;&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''1&quot; RT=&rsquo;3&rsquo; CP=&quot;1&quot;&gt;4&lt;/Q&gt;<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &lt;Q ID=''2&quot; RT=&rsquo;3&rsquo; CP=&quot;2&quot;&gt;1,3&lt;/Q&gt;<br />
  &lt;/Response&gt;</p>
<p>&nbsp;</p>
<p>Please let us know if it met your requirement or not?</p>
<p>Thank you so much!<br />
  Best regards,<br />
  Long Nguyen</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>From:</strong> Jeff Peterson <a href="mailto:[mailto:JPeterson@messagebroadcast.com]">[mailto:JPeterson@messagebroadcast.com]</a> <br />
  <strong>Sent:</strong> Thursday, February 02, 2012 1:22 AM<br />
  <strong>To:</strong> 'Vu Manh Cuong'<br />
  <strong>Cc:</strong> 'Lam Tran'; 'messagebroadcast@setacinq.com.vn';  'longnh6176@setacinq.com.vn'; 'bruce@seta-international.com'<br />
  <strong>Subject:</strong> RE: Let us get to know you!</p>
<p>Vu,</p>
<p>Why the hold up? Short of writing the pages myself I find it  hard to be any more clear. This has been on Task list since at least 12-7-2011  - I thought we had this worked out previously when you sent me screen shots of  proposed UI to which I responded with detailed answers. With up to six people that  by now should be familiar with EBM structures I am expecting a sample of at  least a simple solution by now. Part of that solution would be a separate sub  web site for collecting web responses. See my notes below for each step.</p>
<p>Jeff,</p>
<p>We are still confused about additional survey tools.<br />
  In my opinion, these are steps to let surveys run:<br />
  1) Create surveys by survey tools  – Based on screen shots we have discussed previously, build a tool to  let users build their own surveys. Capability to build simple surveys first and  then we can add more complexity later. The key is to get something useful  running right away. The GUI would just generate an XMLControlString stored in  the Batch that could be viewed in EBM content editor, but I am looking for a  dedicated tool just to build surveys. Can be just another tab on the EBM site.<br />
  2) Schedule to start surveys –  Scheduling for outbound invites is already done within the EBM system – For  advanced scheduling we need the ability to allow or disallow responses based on  rules from how long ago a survey was sent out or a given survey ID may expire  on a certain date. If the survey is no longer available just give the user a  thank you but you are too late message. <br />
  3) Run surveys by web, email, SMS or IVR/voice – EBM can already do IVR voice surveys.  Email/Web (email a link/UID respond via web) are related and would be next. We  do SMS internally now but will work on SMS surveys for EBM after the other  channels are complete.<br />
  4) Collect responses and save to database &nbsp;- users would receive invites from EBM  messaging site and respond to survey response site. Response site would track  responses based on unique survey ID. The unique survy ID would be generated  when the request is sent out and stored in either the Queue table or the  Results table. Responses would be stored in the XMLResult String in the result  table.<br />
  5) Analyse the responses - % for  each response to each question, Partial completed, % complete, and other  reports…</p>
<p>What are exactly your system having?<br />
  Please let us know what steps above.<br />
  I think SETA team are working with step #1 and #5, is  that right? – wrong - Seta team should  be working on all aspects </p>
<p>For your below reference, do you want to make tools to  create surveys like that? – The  reference below is just a simple example – see previous reference from JDPowers  for more complex sample.<br />
  To translate that kind of surveys to SMS or IVR/voice, we  need back-end engines. – all  results are posted simpleresults table in the XMLResultString<br />
  Does your system support these engines? I don&rsquo;t want to integrate into other engines –  I want a standalone engine to work within the frame work of EBM<br />
  If yes, how we link the engines with survey tools, for  example API or data transfer template? </p>
<p>Use Case 1 – keep it simple</p>
<p>User – EBM system user<br />
  Customer – Person taking survey</p>
<p>User creates simple three question survey similar to Chilis  survey below – new UI in EBM – BatchID unique to each survey <br />
  User sets business rules on how long a survey can be valid for  since initial request – can be infinite or 1 or more days – this data point can  be stored in the CCD string under a new tag of SEXP=&rsquo;X&rsquo; where X is the number  of days the survey is valid for. Use SEXP=&rsquo;0&rsquo;, SEXP=&rsquo;&rsquo; &nbsp;or no SEXP for  infinite<br />
  Maybe use another DM MT type similar to the MT type 3 I use now  for email content for storage of HTML markup to surround actual survey  questions<br />
  User creates invitation email and Voice content for survey  requests<br />
  User loads data for contacts in EBM multilist - via the existing  EBM system<br />
  User sends survey request to Customers via Voice/IVR and email  via the existing EBM system<br />
  Requests are stored in Queue table with unique IDs – we may need  a new field for survey ID<br />
  User view results and analytics through EBM</p>
<p>Voice/IVR Section<br />
  Calls are schedule<br />
  Calls go out<br />
  Results are stored in simpleresults under the XMLResultString –  there are sample non-survey reports built into the current EBM system – we need  new reports relating to survey results – see previous email attached referring  to how results are stored</p>
<p>e-mail section<br />
  Customer is sent link with UID – can be combo of email address  and batch ID or a complete UUID<br />
  Customer follows link to web site you create (can just be a subfolder  under the public EBM site for now but can be published anywhere later) – UID is  in link or user is given opportunity to enter UID info<br />
  System checks UID against business rules<br />
  Common BR&rsquo;s: - more can be added later but let&rsquo;s keep it simple  for now<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Has Customer already completed survey – if so thank them for previous  response<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Has Customer waited too long and survey is expired (not all surveys have to  expire but it is a common feature) – thank them for trying but time&nbsp; limit  has been reached – tell them how much time they had in original invite as well  as in the sorry your too late message.<br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Has Customer previously started but not completed survey – if so continue  where left off &nbsp;<br />
  Collect Customer responses and store results in simpleresults  under the XMLResultString <br />
  Allow Customer to go back in survey and change responses until  survey is completed</p>
<p>A common more complex survey would ask different questions based  on previous responses &nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>From:</strong> Jeff Peterson <a href="mailto:[mailto:JPeterson@messagebroadcast.com]">[mailto:JPeterson@messagebroadcast.com]</a> <br />
  <strong>Sent:</strong> Wednesday, February 01, 2012 11:05 AM<br />
  <strong>To:</strong> 'Vu Manh Cuong'<br />
  <strong>Cc:</strong> 'Lam Tran'; 'messagebroadcast@setacinq.com.vn';  'longnh6176@setacinq.com.vn'<br />
  <strong>Subject:</strong> FW: Let us get to know you!</p>
<p>Vu,</p>
<p>&nbsp;</p>
<p>A real easy survey example:</p>
<p>A three question survey to tells us more about yourself in  exchange for better coupons. This would work well in SMS, eMail, or IVR/Voice.</p>
<p><a href="http://chilis.fbmta.com/members/UpdateProfile.aspx?_Theme=23622320199&amp;Member=2dd3ad5eac2743ba9f797b4f439b8433&amp;pref_ts=true">http://chilis.fbmta.com/members/UpdateProfile.aspx?_Theme=23622320199&amp;Member=2dd3ad5eac2743ba9f797b4f439b8433&amp;pref_ts=true</a></p>
<p>&nbsp;</p>
<p><strong>From:</strong> Chili's <a href="mailto:[mailto:ChilisCorp@Chilis.fbmta.com]">[mailto:ChilisCorp@Chilis.fbmta.com]</a> <br />
  <strong>Sent:</strong> Tuesday, January 31, 2012 5:52 PM<br />
  <strong>To:</strong> <a href="mailto:jpijeff@hotmail.com">jpijeff@hotmail.com</a><br />
  <strong>Subject:</strong> Let us get to know you!</p>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td><p><img border="0" width="389" height="175" src="doc_SurveyUI_clip_image010.gif" alt="Description: Description: Description: We Want to Know About You!" /></p></td>
    <td><p><a href="http://chilis.fbmta.com/a/0/23651848699/23622722635/default.aspx" target="_blank"><img border="0" width="211" height="175" src="doc_SurveyUI_clip_image011.gif" alt="Description: Description: Description: Chili's®" /></a></p></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td width="1"><p><img border="0" width="83" height="40" src="doc_SurveyUI_clip_image012.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229180779.gif" /></p></td>
    <td><table border="0" cellspacing="0" cellpadding="0" width="176">
      <tr>
        <td><p><br />
          <strong>J, </strong></p></td>
      </tr>
    </table></td>
    <td width="1"><p><img border="0" width="341" height="40" src="doc_SurveyUI_clip_image013.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229181263.gif" /></p></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td><p><img border="0" width="83" height="144" src="doc_SurveyUI_clip_image014.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229520924.gif" /></p></td>
    <td><p><a href="http://chilis.fbmta.com/a/0/23651848699/23622722636/default.aspx?_u=aHR0cDovL0NoaWxpcy5mYm10YS5jb20vbWVtYmVycy9VcGRhdGVQcm9maWxlLmFzcHg_X1RoZW1lPTIzNjIyMzIwMTk5Jk1lbWJlcj0yZGQzYWQ1ZWFjMjc0M2JhOWY3OTdiNGY0MzliODQzMyZwcmVmX3RzPXRydWU1" target="_blank"><img border="0" width="517" height="144" src="doc_SurveyUI_clip_image015.gif" alt="Description: Description: Description: As a valued Chili's Email Club member, we want to know you better so we can send you the information and offers best for you!  Please answer these 3 short questions now." /></a></p></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td width="1"><p><img border="0" width="83" height="68" src="doc_SurveyUI_clip_image016.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229521908.gif" /></p></td>
    <td width="1"><p><a href="http://chilis.fbmta.com/a/0/23651848699/23622722636/default.aspx?_u=aHR0cDovL0NoaWxpcy5mYm10YS5jb20vbWVtYmVycy9VcGRhdGVQcm9maWxlLmFzcHg_X1RoZW1lPTIzNjIyMzIwMTk5Jk1lbWJlcj0yZGQzYWQ1ZWFjMjc0M2JhOWY3OTdiNGY0MzliODQzMyZwcmVmX3RzPXRydWU1" target="_blank"><img border="0" width="363" height="68" src="doc_SurveyUI_clip_image017.gif" alt="Description: Description: Description: TELL US MORE &gt;" /></a></p></td>
    <td width="1"><p><img border="0" width="154" height="68" src="doc_SurveyUI_clip_image018.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229523011.gif" /></p></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td><p><img border="0" width="600" height="102" src="doc_SurveyUI_clip_image019.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/shared/images/23622320195/23622320195_20111229523500.gif" /></p></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td width="600"><table border="0" cellspacing="0" cellpadding="0" width="600">
      <tr>
        <td><p align="center"><br />
          <a href="http://Chilis.fbmta.com/a/0/23651848699/23622722637/default.aspx?_u=aHR0cDovL0NoaWxpcy5mYm10YS5jb20vbWVtYmVycy9VcGRhdGVQcm9maWxlLmFzcHg_X1RoZW1lPTIzNjIyMzIwMTk5Jk1lbWJlcj0yZGQzYWQ1ZWFjMjc0M2JhOWY3OTdiNGY0MzliODQzMyZwcmVmX3RzPXRydWU1">Tell      Us More</a></p></td>
      </tr>
    </table></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr>
    <td><p align="center"><br />
      <a href="http://Chilis.fbmta.com/a/0/23651848699/23622722635/default.aspx">Visit    Chilis.com</a> &nbsp;&nbsp;|&nbsp; <a href="http://Chilis.fbmta.com/a/0/23651848699/23622722638/default.aspx">Find A    Chili's</a>&nbsp;&nbsp;|&nbsp; <a href="http://Chilis.fbmta.com/a/0/23651848699/23622722639/default.aspx">Order    Online</a><br />
      <a href="http://Chilis.fbmta.com/a/0/23651848699/23622722640/default.aspx">¿Prefieres    español?</a>&nbsp;&nbsp;|&nbsp; <a href="http://Chilis.fbmta.com/a/1/23651848699/23622722641/default.aspx?Member=2dd3ad5eac2743ba9f797b4f439b8433">Update    Your Profile</a>&nbsp;&nbsp;|&nbsp; <a href="http://Chilis.fbmta.com/a/1/23651848699/23622722642/default.aspx?_u=aHR0cDovL0NoaWxpcy5mYm10YS5jb20vbWVtYmVycy9VbnN1YnNjcmliZS5hc3B4P01lbWJlcj0yZGQzYWQ1ZWFjMjc0M2JhOWY3OTdiNGY0MzliODQzMyZNYWlsaW5nPTIzNjIyMzU2MjQ00">Unsubscribe</a></p></td>
  </tr>
  <tr>
    <td><div align="center">
      <hr size="2" width="100%" noshade="noshade" align="center" />
    </div>
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td><p><a href="http://chilis.fbmta.com/a/0/23651848699/23622722643/default.aspx"><img border="0" width="115" height="39" src="doc_SurveyUI_clip_image020.gif" alt="Description: Description: Description: Powered by Fishbowl" /></a></p></td>
          <td><p>This      email was sent because you joined Chili's email club in one of our      restaurants, online or at one of our events. Your email address will not be      shared with anyone. You can take your name out of Chili's email club at any      time by clicking the unsubscribe link on this email and you will be removed      from our list immediately. Chili's - A Brinker International Brand, 6820      LBJ Freeway, Dallas, TX 75240.</p></td>
        </tr>
      </table>
      <p>To ensure    delivery, add <a href="mailto:ChilisCorp@Chilis.fbmta.com">ChilisCorp@Chilis.fbmta.com</a> to your address    book.</p></td>
  </tr>
</table>
<p><img border="0" width="32" height="1" src="doc_SurveyUI_clip_image021.gif" alt="Description: Description: Description: http://Chilis.fbmta.com/a/23651848699/23622356244/default.gif" /></p>
</body>
</html>