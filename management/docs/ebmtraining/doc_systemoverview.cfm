<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<h1>System Overview
</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>The main point of the EBM system is to allow a user to login, design, fulfill, and report on all aspects of communications with consumers. Includinf Voice, eMail, SMS, IP, IM, Social media, TTY, FAX, and anything else we decide to support.</p>
<h3>&nbsp;</h3>
<p>1. User authenticates and establishes identity - The UserID.</p>
<p>2. Data is uploaded into the simplexlists.rxmultilist using  ETL tools whithing the EBM system. This data can be modified individually using  list tools<br />
  <br />
3. Content is created and stored in XMLControlString_vch in  the simplexobjects.batch table. This content can contain special tags  {%Field_Name%} that will be replaced on distribution.<br />
<br />
4. Each unique request for a fulfillment message is done through an  entry in the simplequeue.contactqueue. The contactqueue can be populated with contact data directly OR by filtering data from the RXMultiList</p>
<p>Note: While populating simplexqueue.contactqueue customized  XMLControlStrings_vch are created for each message based on the master in  XMLControlString_vch in the simplexobjects.batch table. Anywhere a custom data  string is defined {%Field_Name%} , it is replaced with the value of the  corresponding Field_Name&lsquo;s data for each contact string the message(s) is/are  sent to. The custom data fields can be used as part of switch statements or as  direct replacements for text values in TTS strings. Custom data files can also  be used as input into CONV objects – things like account balance and dates that  need to be unique to each message. </p>
<p>5. Backend processes automatically fulfill and return results to the simplexresulsts.calldetails table.</p>
<p>6. Reporting and Analytics take place on the results.</p>
<p></p>
<p></p>
<h3>UserAccount</h3>
<p>All interactions are tied back to a single User Account. In the case of Company level accounts there may be several sub accounts but still only one top level account.</p>
<h3><br />
Content</h3>
<p>Content is defined using UI tools and is then stored in the master XMLControlString_vch as part of a simpleobjects.batch object.</p>
<p>Content is created and stored in XMLControlString_vch in  the simplexobjects.batch table. This content can contain special tags  {%Field_Name%} that will be replaced on distribution.</p>
<h3><br />
Contacts</h3>
<p>Contact targets are entered using UI tools and then are stored in simplelists.rxmultilist</p>
<p>Data is uploaded into the simplexlists.rxmultilist using  ETL tools whithing the EBM system. This data can be modified individually using  list tools</p>
<h3><br />
Fulfillment</h3>
<p>Fulfillment requestes are loaded into the simplequeue.contactqueue</p>
<p>While populating simplexqueue.contactqueue customized  XMLControlStrings_vch are created for each message based on the master in  XMLControlString_vch in the simplexobjects.batch table. Anywhere a custom data  string is defined {%Field_Name%} , it is replaced with the value of the  corresponding Field_Name&lsquo;s data for each contact string the message(s) is/are  sent to. The custom data fields can be used as part of switch statements or as  direct replacements for text values in TTS strings. Custom data files can also  be used as input into CONV objects – things like account balance and dates that  need to be unique to each message.</p>
<h3><br />
Schedules</h3>
<p>All fullfillment is controlled by schedules.</p>
<p>The main storage location for schedules is in simpleobjects.scheduleoptions. When distributing to the fullfillment device the automated process will automatically update the remote schedule to match the main schedule</p>
<p>Every Batch has its own schedule.</p>
<p>There are two types:</p>
<p>Single - all start and stop times are the same regwardless of day of week. Particular days of week can be enabled and disabled directly. DynamicScheduleDayOfWeek_ti = 0.</p>
<p>Dynamic - there is one schedule optional for each day of week with Sunday being DynamicScheduleDayOfWeek_ti=1 and Saturday being DynamicScheduleDayOfWeek_ti=7</p>
<p>If Single Schedule is chosen only the DynamicScheduleDayOfWeek_ti of 0 is used - all other dynamic schedules will be erased - DynamicScheduleDayOfWeek_ti &gt; 0 will be deleted</p>
<p>If a Dynamic Schedule has not been saved yet then default is disabled. </p>
<p>Saviving at least one Dynamic day will erase default schedule of DynamicScheduleDayOfWeek_ti=0.</p>
<p>&nbsp;</p>
<h3>Distribution Processing</h3>
<p>Using the coldfusion task scheduler and a file in management section<br />
http://EBMManagementDEVII.mb:8503/management/processing/act_Processcontactqueue.cfm?VerboseDebug=0</p>
<p><img src="Distribution.jpg" width="882" height="592" alt="5" /></p>
<p>&nbsp;</p>
<h3>Extraction</h3>
<p><img src="Extraction.jpg" width="864" height="577" alt="5" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Reporting</h3>
<p>Online reporting portal<br />
  Batch list Picker
    <br />
    Date Range<br />
    Quad quadrant design lets user customize their own reports<br />
Export tools for final dispositions</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Billing</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Major Sections<br />
  <br />
  Public – Session - Management</p>
<p>Security and the Session<br />
  The User Account – Company vs Standalone<br />
  The RXMultiList – ETL Tools<br />
  The contactqueue<br />
  Billing<br />
  Aduio Library<br />
  Dynamic Schedules<br />
  Rules<br />
  Communications Objects</p>
<p>XMLControlString_vch – Master and Custom<br />
  eMail<br />
  SMS<br />
  Reporting<br />
  Automated processes<br />
  Distribution<br />
  Extraction<br />
  Coding Standards<br />
  The Task List<br />
  API’s<br />
  In session vs standalone calls<br />
  DNC lists</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>