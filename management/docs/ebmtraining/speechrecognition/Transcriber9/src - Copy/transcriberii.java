/*
 * Copyright 1999-2004 Carnegie Mellon University.
 * Portions Copyright 2004 Sun Microsystems, Inc.
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 *
 */


import edu.cmu.sphinx.frontend.util.AudioFileDataSource;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Lattice;
import edu.cmu.sphinx.result.LatticeOptimizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.net.URL;

// For MP# to wav conversion
import javazoom.jl.converter.Converter;


// for wav output data format conversion
import javax.sound.sampled.AudioFileFormat; 
import javax.sound.sampled.AudioFormat; 
import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 


/** A simple example that shows how to transcribe a continuous audio file that has multiple utterances in it. */
public class TranscriberII {


    public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
       
       	String inpFileName = "K:\\JLP Eclipse\\JLP Eclipse Projects\\Transcriber9\\src\\BabbleSphere_705_1_1_8.mp3";
           
       	String outVal = TranscribeMe("config.xml", inpFileName);
                
    }
    
    
    public static String TestMe(String inpA, int inpB)
    {
    	
    	return "This is a test";
    }
    
    
    
    public static String TranscribeMe(String inpConfigPath, String inpPath)
    {

    	String RetVal = "Default No Text Found";
    	String DebugStr = "Start";
    	String DebugStr2 = "Start2";
    	
    	String inpFileName = inpPath;
    	String outFileName = inpFileName + ".wav";
    	String outFileNameLoRes = inpFileName + "_lowres.wav";
    	
    	if(inpConfigPath == "")
    		inpConfigPath = "config.xml";
    	
    	try
    	{
    	
	        URL audioURL;        
	     	
	    	try
	    	{
	    		       		        	
	    		System.out.println("inpFileName=" + inpFileName);
	    		System.out.println("outFileName=" + outFileName);
	    		System.out.println("outFileNameLoRes=" + outFileNameLoRes);
	    		     		
	    		// Java sound api does not natively support .mp3 format so convert from mp3 to wav first.
	    		Converter myConverter = new Converter();           		
	    		// This class/method does not support specifing the dat format for the output wav file but instead relys on base mp3 files format to make a determination on "best" output data format
	    		myConverter.convert(inpFileName, outFileName);
	    		
	    		// Define desired output format to convert wav file to
	    		AudioFormat outDataFormat = new AudioFormat((float) 16000.0, (int) 16, (int) 1, true, false);
	    		
	    		// Open wave file in whatever format it currently is
				File soundFile = new File(outFileName);
				if (!soundFile.exists()) 
				{ 
				    System.err.println("Wave file not found: " + outFileName);
				    return "Wave file not found: " + outFileName;
				} 
	    	        
	    	    // Get a java AudioInputStream object from the opened wav file    
		        AudioInputStream audioInputStream = null;
		        try
		        { 
		            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
		        }
		        catch (UnsupportedAudioFileException e1)
		        { 
		            e1.printStackTrace();
		            return "UnsupportedAudioFileException";
		        }
		        catch (IOException e1)
		        { 
		            e1.printStackTrace();
		            return "IOException";
		        } 
		 
		        DebugStr2 = "About to make lo res" + AudioSystem.isConversionSupported(outDataFormat, audioInputStream.getFormat());
		        
		        // If target format conversion is supported
		        if (AudioSystem.isConversionSupported(outDataFormat, audioInputStream.getFormat())) 
		        {   		        			        		
		        	AudioInputStream lowResAIS = null;     
		        	lowResAIS = AudioSystem.getAudioInputStream(outDataFormat, audioInputStream);
		         	  
		        	// AudioFileFormat.Type targetFileType = AudioFileFormat.Type.WAVE;      	        
		        	AudioSystem.write(lowResAIS, AudioFileFormat.Type.WAVE,  new File(outFileNameLoRes));
		        	
		        	DebugStr2 = "Made lo res OK";
		        }
		                      
	    	}
	    	catch(Exception error)
	    	{
	    		 System.out.println("Error converting file Level A - " + error.getMessage());
	    		 RetVal = "Error converting file Level A - " + error.getMessage();
	    		 return RetVal; 
	    	}
	        	
	//         	audioURL = TranscriberII.class.getResource("K:\\JLP Eclipse\\JLP Eclipse Projects\\Transcriber9\\bin\\BabbleSphere_705_1_1_8.wav");
	//         	audioURL = TranscriberII.class.getResource("BabbleSphere_705_1_1_8.wav");
	//        	audioURL = TranscriberII.class.getResource("10001-90210-01803.wav");
	//         	audioURL = TranscriberII.class.getResource("file:./src/DSSD_1_2578_1.wav");
	//         	audioURL = TranscriberII.class.getResource("DSSD_1_2578_1.wav");
	//         	audioURL = TranscriberII.class.getResource("DSSD_1_2578_2.wav");
	    	   	audioURL = new File(outFileNameLoRes).toURI().toURL(); 
	//        	audioURL = TranscriberII.class.getResource("BabbleSphere_705_1_1_8.mp3_lowres.wav");
	     	
	  
	
	    	DebugStr = "config.xml";
	    	   	
	        //URL configURL = TranscriberII.class.getResource("hub4.config2.xml");
	        URL configURL = TranscriberII.class.getResource("config.xml");
	
	        System.out.println("Create CM");
	        
	        ConfigurationManager cm = new ConfigurationManager(configURL);
	        Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
	
	        /* allocate the resource necessary for the recognizer */
	        recognizer.allocate();
	
	        System.out.println("recognizer.allocate");	        
	        DebugStr = "recognizer.allocate";
	        
	        // configure the audio input for the recognizer
	        AudioFileDataSource dataSource = (AudioFileDataSource) cm.lookup("audioFileDataSource");
	        dataSource.setAudioFile(audioURL, null);
	
	        DebugStr = "setAudioFile audioURL";
	        
	        // Loop until last utterance in the audio file has been decoded, in which case the recognizer will return null.
	        Result result;
	        
	        System.out.println("Result");
	        DebugStr = "Result";
	        
	        
	        DebugStr = "R0";
	        
	        while ((result = recognizer.recognize())!= null) {
	
	        		DebugStr = DebugStr + ",1";
	        	
	                String resultText = result.getBestResultNoFiller();
	                System.out.println(resultText);
	                
	                DebugStr = DebugStr + ",2";
	                
	                RetVal = RetVal + resultText;
	                
	                resultText = result.getBestPronunciationResult();
	                System.out.println(resultText);
	         
	                /*
	                Lattice lattice = new Lattice(result);
	                LatticeOptimizer optimizer = new LatticeOptimizer(lattice);
	                optimizer.optimize();
	                lattice.dumpAllPaths();
	                */
	                
	           //     resultText = result.getTimedBestResult(true, true);
	           //     System.out.println(resultText);
	                              
	        }
	        
	        // Remove staged .wav file
    	}
    	catch(Exception error)
    	{
    		 System.out.println("Error converting file Level B - " + DebugStr + " - " + DebugStr2 + " - " + error.getMessage());
    		 RetVal = "Error converting file Level B - " + DebugStr + " - " + DebugStr2 + " - " + error.getMessage() + " - " + outFileNameLoRes;
    	}
    	
    	System.out.println(DebugStr);
    	System.out.println(DebugStr2);
    	
    	return RetVal;
    }
    
    
    
    
    
}
