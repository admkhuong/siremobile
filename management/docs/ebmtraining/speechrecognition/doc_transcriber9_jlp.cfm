<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transcriber 9</title>
</head>

<body>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Build the project </p>
<p>The &quot;Gold&quot; is in the configuration of the</p>
<p>The config file path is specified in the initialization of the java class object.<br />
  Files and directories needed to run<br />
	cmudict.hub4.06d.dict - file<br />
language_model.arpaformat.DMP - file<br />
hub4opensrc.cd_continuous_8gau - dir<br />
</p>
<p>&nbsp;</p>
<p>Creating the JAR file<br />
  Export the running class into a jar file - only need class data</p>
<p>Installing the JAR file on Coldfusion</p>
<p>Copy the .jar file to the lib diectory in your coldfusion install. c$\ColdFusion8\wwwroot\WEB-INF\lib</p>
<p>Additional jars needed to run</p>
<p>transcriber9.jar<br />
  jl1.0.1.jar<br />
  js.jar
  <br />
  jsapi.jar  <br />
  jsapi-1.0-base.jar  <br />
  sphinx4.jar  <br />
  tags.jar  <br />
  WSJ_8gau_13dCep_16k_40mel_130Hz_6800Hz.jar<br />
</p>
<p>&nbsp;</p>
<p>Possible Error:<br />
java.lang.UnsupportedClassVersionError: TranscriberII : Unsupported major.minor version 51.0  </p>
<p>Make sure the class is compiled with Eclipse project settings where the java is comapatible with the Coldfusion's java version. In this case the code was compiled using 1.7 but Coldfusion 8 the Jar file was installed on only supported 1.6. </p>
<p>&nbsp;</p>
<h3>Update Latest Java</h3>
<p>Install Latest java version - download the correct version for your machine at java site</p>
<p>JRun stores startup parameters in a file named jvm.config located in the cf_root/runtime/bin or jrun_root/bin <br />
</p>
<p>The jvm.config has an argument called &quot;java.home.&quot; The &quot;java.home&quot; argument accepts a value of the path to your JRE (Java Runtime Environment). For example: java.home=C:/CFusionMX7/runtime/jre</p>
<p>When you change your JVM, this argument must reflect the path to your new JRE. For example: java.home=C:/j2sdk1.4.2_11/jre</p>
<p></p>
<p>&nbsp; </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Under Project Settings<br />
Under Java Compiler - change Compiler compliance level back to 1.6. You can still use 1.7 JRE for testing so you dont need to install older JRE</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Setup extra memeory for transcriptions</p>
<p>&nbsp;</p>
<h3>Set actual resource paths in config.xml file(s)</h3>
<p>value=&quot;file:/C:/SimpleXScripts/transcriber9/cmudict.hub4.06d.dict&quot; /&gt;<br />
value=&quot;file:/C:/cds/transcriber9/cmudict.hub4.06d.dict&quot; /&gt;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>To test:</p>
<p>First instantiate the object<br />
  &lt;cfset jarvar = createObject(&quot;java&quot;,&quot;TranscriberII&quot;)&gt;<br />
</p>
<p>Then Set the config path<br />
  &lt;cfset inpConfigPath = &quot;C:/cds/webplayground/config.xml&quot;&gt;<br />
  <br />
  Then call the TranscribeMe method passing in config and file paths<br />
  &lt;cfset outVal2 = jarvar.TranscribeMe(inpConfigPath, &quot;C:/cds/webplayground/BabbleSphere_705_1_1_8.mp3&quot;)&gt;<br />
</p>
<p></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>