


<cfparam name="HowManyDaysOldToDrop" default="30">
<cfparam name="VerboseDebug" default="1">

<!---

http://dev.telespeech.com/devjlp/EBM_DEV/management/maintenance/act_cleanupDataImportStageTables.cfm?VerboseDebug=1&HowManyDaysOldToDrop=30#

--->

<cftry>

	<cfif VerboseDebug GT 0><cfoutput>Page start #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#  <BR /></cfoutput></cfif>
    
    <cfif VerboseDebug GT 0><cfoutput>HowManyDaysOldToDrop = (#HowManyDaysOldToDrop#) <br /></cfoutput></cfif>
            
    <!--- Get description for each group--->
    <cfquery name="GetTablesToDrop" datasource="#Session.DBSourceEBM#">                                                                                                   
        SELECT 
            table_name
        FROM
            INFORMATION_SCHEMA.TABLES
        WHERE
            table_schema = 'simplelists'
        AND
            table_name LIKE 'simplephonelist_stage_%'
        AND 
        	<cfif HowManyDaysOldToDrop GT 0>
	            Create_time < DATE_ADD(NOW(), INTERVAL -#HowManyDaysOldToDrop# DAY)                
            <cfelse>
            	 Create_time < DATE_ADD(NOW(), INTERVAL -1 HOUR)
            </cfif>
    </cfquery>  
    
    <cfif VerboseDebug GT 0><cfoutput>Total Tables Found = (#GetTablesToDrop.RecordCount#) <br /></cfoutput></cfif>
    
    <cfloop query="GetTablesToDrop">
		<!--- Drop each table --->
        <cfquery name="DropTable" datasource="#Session.DBSourceEBM#">                                                                                                   
           DROP TABLE simplexuploadstage.#gettablestodrop.table_name#   
        </cfquery>  
	
	    <cfif VerboseDebug GT 0><cfoutput>Table #GetTablesToDrop.table_name# Dropped <BR /></cfoutput></cfif>
    </cfloop>
	
	<!--- Log for NOC --->
	<cfquery name="UpdateLogData" datasource="#Session.DBSourceEBM#">
        INSERT INTO simplequeue.simplexprod_log
        (
            LogType_int,
            CountProcessed_int,            
            START_DT,
            End_dt
        )
        VALUES
        (
            501,
            #GetTablesToDrop.RecordCount#,          
            NOW(),         
            NOW()
        )
    </cfquery>

	<cfif VerboseDebug GT 0><cfoutput>Page finished #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR /></cfoutput></cfif>

<cfcatch TYPE="any">
    <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem cleaning up data import disposition stage tables.">
    <cfset ErrorNumber = 105>
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
</cfcatch>
</cftry>