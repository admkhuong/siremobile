<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
<title>EBM MAnagement</title>
</head>

<cfset PublicPath = "public">
<cfset SessionPath = "management">
<cfset LOCALPROTOCOL = "http">

<Style TYPE="text/css">
	.ui-tabs .ui-tabs-panel { display: block; border-width: 0 1px 1px 1px; padding: 0.5em; background: none; }
</style>
 
<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/BabbleSphere/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/rxform2.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/babblesphere.css" />
    
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.5.1.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
    
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 

 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
 
 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
<script TYPE="text/javascript">
	var CurrSitePath = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>';
	var currentUserId = "";
</script>

</cfoutput>



<!--- Enable hover functionality ---> 
<script TYPE="text/javascript">


	$(function(){
		$('.ui-state-default').hover(
			function(){ $(this).addClass('ui-state-hover'); }, 
			function(){ $(this).removeClass('ui-state-hover'); }
		);
		
		
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"<div id='loadingDlg' style='display:inline;'><img class='loadingDlgRegisterNewAccount' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif' width='20' height='20'></div>" );
				}
			}
		});



		$("#RegisterNewAccountForm #RegisterNewAccountButton").click( function() { RegisterNewAccount(); return false;  }); 	
			
			  
		$("#inpNAPassword").keyup( function(event){  
			 PasswordStrength($(this).val());
		} );
		  
			  
		$("#LoginLink").click(function() {DoLogin();});
		$("#ForgotPassword").click(function() {return false;});
		
		$("#loadingDlg").hide();
		$("#loadingDlgLogin").hide();
		
			  
			  
	});


	function DoLogin()
	{		
		<!--- hide login button --->
		$("#LoginLink").hide();
		
		<!--- Show processing notice --->
		$("#loadingDlgLogin").show();
				
		try
		{
			<!--- Call LOCALOUTPUT javascript validations first --->
			if(ValidateInput($("#frmLogon #inpUserID").val()) && ValidateInput($("#frmLogon #inpPassword").val()))
			{
				currentUserId=$("#frmLogon #inpUserID").val();
				<!--- Let the user know somthing is processing --->
				// $("#CurrentrxtXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');
												
				$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
				{ 
					inpUserID : $("#frmLogon #inpUserID").val(), 
					inpPassword : $("#frmLogon #inpPassword").val()
				}, 
					<!--- Default return function for authentication - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																					
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										<!--- Navigate tp sesion Home --->
										window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
									}
									else
									{
										<!--- Unsuccessful Login --->
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.REASONMSG[0]) != "undefined")
										{	 
											jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
					}		
										else
										{
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
										}
										
									}
								}
								else
								{<!--- Invalid structure returned --->	
									// Do something		
									
									jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
						
								}
							}
							else
							{<!--- No result returned --->
								// Do something		
								
								jAlertOK("Error while trying to login.\n" + "No response form remote server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
							
							}
					} );
					
									
				
			}
			else
			{					
				jAlertOK("Error while trying to login.\n" + "Invalid input." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
			}
		}
		catch(e)
		{
			jAlertOK("Error while trying to login.\n" + e , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
		}
			
			
	}
	
</script>


<body>
<div id="BSHeader">

	<div id="BSHeadCenter">
    	<div id="BSHeadLogo">
    		<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/HeaderLogo1Web.png" />
        </div>
        
        <div id="BSHeadLogin">
        
       		 
        	<form id="frmLogon" name="frmLogon" >
            	<table border="0" cellpadding="0" cellspacing="0">
                	<tr>
                    	<td align="left" nowrap>
                        	User Id
                        </td>
                        
                        <td align="left" nowrap>
                        	Password
                        </td>       
                        
                        <td>&nbsp;
                        	
                        </td>         			
                	</tr>
                    
                    <tr>
                    	<td align="left" nowrap>
                        	<input TYPE="text" id="inpUserID" name="inpUserID" label="User Id" />                			
                        </td>
                    
                    	<td align="left" nowrap>
                        	<input TYPE="password" id="inpPassword" name="inpPassword" label="Password" />
                        </td>         
                        
                        <td width="40px">
                            <div id="loadingDlgLogin" style="display:inline;">
                                <img class="loadingDlgLogin" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">
                            </div>

                        	<a id="LoginLink">Login</a>                        
                        </td>           
                    </tr>
                    
                    <tr>
                    	<td align="left" nowrap>&nbsp;
                        	
                        </td>
                        
                        <td align="left" nowrap>                        
                        	<a id="ForgotPassword">Forgot your password?</a>
                        </td>       
                        
                        <td>&nbsp;
                        	
                        </td>         			
                	</tr>
                    
                    	
                </table>
            </form>
        
        </div>
    </div>

</div>

<div id="BSContent">

	<div id="BSCLeft">
    	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/BSLogo1Web.png" />
    </div>
      
    <div id="BSCRight" align="center">
            
            
         <div id="BSCHomeContent">
         
        <!--- 	<div id="HomeContentLeft" style="display:inline;">
            	Ways people are using babbleSphere
            </div>
            
            
            <div id="HomeContentRight" style="display:inline;">
	             Babble lets your voice be heard.            
            </div>--->
            
                  
         </div>               
      
    
    </div>


</div>


<div id="BSFooter">


    <div id="BSFooterCenter">
    	<hr />	
        
        <div id="FooterMenu">    
            <a>About</a>
            .
            <a href="FindUs.cfm">Find Us</a>
            .
            <a>Privacy</a>
            .
            <a>Terms of Use</a>
        </div>
        
        <div id="FooterNotice">
        	BabbleSphere &copy; 2011       
        </div>
        
	</div>
</div>
</body>
</html>