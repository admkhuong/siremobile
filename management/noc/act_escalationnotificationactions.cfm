<cfparam name="Email" default="1">
<cfparam name="ENA_Message" default="No MESSAGE Specified">
<cfparam name="SubjectLine" default="Error Notification">
<cfparam name="TroubleShootingTips" default="No Troubleshooting Tips Specified">
<cfparam name="ErrorNumber" default="0">
<cfparam name="AlertType" default="1">

<cfparam name="cfcatch.TYPE" default="">
<cfparam name="cfcatch.MESSAGE" default="">
<cfparam name="cfcatch.detail" default="">
<cfparam name="CGI.HTTP_HOST" default="">
<cfparam name="CGI.HTTP_REFERER" default="">
<cfparam name="CGI.HTTP_USER_AGENT" default="">
<cfparam name="CGI.PATH_TRANSLATED" default="">
<cfparam name="CGI.QUERY_STRING" default="">

<cfparam name="SupportEMailFrom" default="support@siremobile.com" />
<cfparam name="SupportEMailServer" default="smtp.gmail.com" />
<cfparam name="SupportEMailUserName" default="support@siremobile.com" />
<cfparam name="SupportEMailPassword" default="SF927$tyir3" />
<cfparam name="SupportEMailPort" default="465" />
<cfparam name="SupportEMailUseSSL" default="true" />

<cfparam name="TargetDataSource" default="#Session.DBSourceEBM#">

<cfif RIGHT(SubjectLine,8) NEQ "- DEL408">
	<cfset SubjectLine = SubjectLine & " - DEL408">
</cfif>

<cfset ErrorLogInsertedOK = 1>

<cftry>

	<cfquery name="InsertToErrorLog" datasource="#TargetDataSource#">
        INSERT INTO simplequeue.errorlogs
        (
        	ErrorNumber_int,
            Created_dt,
            Subject_vch,
            Message_vch,
            TroubleShootingTips_vch,
            CatchType_vch,
            CatchMessage_vch,
            CatchDetail_vch,
            Host_vch,
            Referer_vch,
            UserAgent_vch,
            Path_vch,
            QueryString_vch
        )
        VALUES
        (
        	#ErrorNumber#,
            NOW(),
            '#REPLACE(SubjectLine, "'", "''")#',   
            '#REPLACE(ENA_Message, "'", "''")#',   
            '#REPLACE(TroubleShootingTips, "'", "''")#',     
            '#REPLACE(cfcatch.TYPE, "'", "''")#', 
            '#REPLACE(cfcatch.MESSAGE, "'", "''")#',
            '#REPLACE(cfcatch.detail, "'", "''")#',
            '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
        );
	</cfquery>

<cfcatch type="any"><cfset ErrorLogInsertedOK = 0></cfcatch>

</cftry>


<cfoutput>
		<cfset CONTACTLIST = "support@siremobile.com;" >
		
        <cfsavecontent variable="CGIDump">
        
        <cfif Email eq 1>
            <cfmail to="#CONTACTLIST#" subject="Sire Management #SubjectLine#" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
            
                #ENA_Message#
    
                <cfif IsDefined("cfcatch.TYPE") AND Len(Trim(cfcatch.TYPE)) GT 0 >
                cfcatch.TYPE
                -------------------------
                #cfcatch.TYPE#
                -------------------------
                </cfif>
                
                <cfif IsDefined("cfcatch.MESSAGE") AND Len(Trim(cfcatch.MESSAGE)) GT 0 >
                cfcatch.MESSAGE
                -------------------------
                #cfcatch.MESSAGE#
                -------------------------
                </cfif>
                
                <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
                cfcatch.detail
                -------------------------
                #cfcatch.detail#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
                CGI.HTTP_HOST
                -------------------------
                #CGI.HTTP_HOST#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
                CGI.HTTP_REFERER
                -------------------------
                #CGI.HTTP_REFERER#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
                CGI.HTTP_USER_AGENT
                -------------------------
                #CGI.HTTP_USER_AGENT#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
                CGI.PATH_TRANSLATED
                -------------------------
                #CGI.PATH_TRANSLATED#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
                CGI.QUERY_STRING
                -------------------------
                #CGI.QUERY_STRING#
                -------------------------
                </cfif>
    
            </cfmail> 
		<cfelse>
        	<!---<cfmail to="pnguyen@messagebroadcast.com" from="IT@messagebroadcast.com" subject="#SubjectLine#">			
            
                #ENA_Message#
    
                <cfif IsDefined("cfcatch.TYPE") AND Len(Trim(cfcatch.TYPE)) GT 0 >
                cfcatch.TYPE
                -------------------------
                #cfcatch.TYPE#
                -------------------------
                </cfif>
                
                <cfif IsDefined("cfcatch.MESSAGE") AND Len(Trim(cfcatch.MESSAGE)) GT 0 >
                cfcatch.MESSAGE
                -------------------------
                #cfcatch.MESSAGE#
                -------------------------
                </cfif>
                
                <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
                cfcatch.detail
                -------------------------
                #cfcatch.detail#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
                CGI.HTTP_HOST
                -------------------------
                #CGI.HTTP_HOST#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
                CGI.HTTP_REFERER
                -------------------------
                #CGI.HTTP_REFERER#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
                CGI.HTTP_USER_AGENT
                -------------------------
                #CGI.HTTP_USER_AGENT#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
                CGI.PATH_TRANSLATED
                -------------------------
                #CGI.PATH_TRANSLATED#
                -------------------------
                </cfif>
                
                <cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
                CGI.QUERY_STRING
                -------------------------
                #CGI.QUERY_STRING#
                -------------------------
                </cfif>
    
            </cfmail> --->
		</cfif>        
        </cfsavecontent>
        
        <!--- <cftry>
            <cfquery datasource="#Session.generalAlertsDBSource#">
                INSERT INTO [Alerts].[dbo].[Daily_Alerts]
                   (ServerIP_vch
                   ,ENASubject_vch
                   ,ENAMessage_vch
                   ,CGIDump_txt
                   ,HighPriority_bit
                   ,AlertType_si
                   ,To_vch)
             VALUES
                   ('10.11.0.75'
                   ,'#SubjectLine#'
                   ,'#ENA_Message#'
                   ,'#CGIDump#'
                   ,0
                   ,#AlertType#
                   ,'#CONTACTLIST#')
            </cfquery>
        <cfcatch>
          	<cfmail to="#CONTACTLIST#" subject="EBM Management Global Alerts DB Insert Error #SubjectLine#" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                Failed to post alert to database:
                cfcatch.Message: <cfif IsDefined("cfcatch.message") AND Len(Trim(cfcatch.message)) GT 0>#cfcatch.Message#<cfelse>Unknown</cfif>
                cfcatch.Detail: <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >#cfcatch.Detail#<cfelse>Unknown</cfif>
                ---
                #ENA_Message#
                #CGIDump#
            </cfmail>
        </cfcatch>
        </cftry>--->
    
    	
        
        
</cfoutput>