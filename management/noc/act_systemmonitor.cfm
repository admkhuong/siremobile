<cfparam name = "THRESHOLD" default = "500" />
<cfparam name = "INTERVAL" default = "HOUR" />
<cfparam name = "DBSOURCE" default = "Bishop" />
<cfinclude template="../../public/paths.cfm" >

<cfset TargetDataSource = "#DBSOURCE#" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>System Monitor Alert</title>
</head>

<body>
<cftry>


	<cfset LastDeviceIP = "" />
    
	<!--- Get Devices--->
    <cfquery name="GetDevices" datasource="#TargetDataSource#" >                            
        SELECT
            IP_Vch
        FROM
            simplexfulfillment.device                                  
        WHERE
            Enabled_int = 1    
        ORDER BY
            Position_int ASC, DeviceId_int ASC                       
    </cfquery>        
                    
    <cfloop query="GetDevices">
        
        <cfset LastDeviceIP = "#GetDevices.IP_Vch#" />
        
        <cfoutput>
        	LastDeviceIP = #LastDeviceIP#
            <BR />
        </cfoutput>
        
        <cfquery name="q_check_DTS" datasource="#GetDevices.IP_Vch#">           
                SELECT
                	COUNT(*) AS TotalCount
				FROM
                    DistributedToSend.dts dts JOIN
                    CallControl.ScheduleOptions so ON (so.BatchId_bi = dts.BatchId_bi)
				WHERE
                    dts.DTSStatusType_ti = 1                   
                    AND Scheduled_dt < NOW()
                    AND 
                       (so.stop_dt < NOW() OR so.enabled_ti = 0)                
        </cfquery>
                
        <cfif q_check_DTS.TotalCount GT 250>
        
        	<cfset ENA_Message ="Alert - SimpleX Monitor Alert - #DBSOURCE# #GetDevices.IP_Vch# has too many ineligable queued calls" >
            <cfset ErrorNumber = 1001>
            <cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">
        
        </cfif>     
        
         <cfoutput>
        	TotalCount = #q_check_DTS.TotalCount#
            <BR />
        </cfoutput>   
        
    </cfloop>            
	    
    
    <cfset ENA_Message ="System Monitor - #DBSOURCE# - GetDevices.RecordCount = #GetDevices.RecordCount# ">
	<cfset ErrorNumber = 0>
    <cfset AlertType = 3>
    <cfset SubjectLine = "SimpleX Monitor #DBSOURCE# Completed OK">
    <cfset Email=0>
    <cfinclude template="act_EscalationNotificationActions.cfm">
            
                
            
	<cfcatch type="any">
			<cfset ENA_Message ="System Monitor - #DBSOURCE# - Catch ">
            <cfset ErrorNumber = 0>
            <cfset AlertType = 3>
            <cfset SubjectLine = "POSTED - SimpleX #DBSOURCE# SimpleX Monitor Alert Failed">
            <cfset Email=0>
            <cfinclude template="act_EscalationNotificationActions.cfm">
            
            <cfdump var="#cfcatch#" />
	</cfcatch>

</cftry>   


<!--- Watch for too much MO Queue Scheduled intervals building up too much --->
<!--- 7:00 AM until 8:00 PM Only for now--->

<cfif HOUR(NOW()) GT 7 AND HOUR(NOW()) LT 20 >
    <cftry>
        
        <!--- Get Devices--->
        <cfquery name="GetCountOldIntervals" datasource="#TargetDataSource#" >     
            SELECT
                COUNT(*) AS TOTALCOUNT
            FROM
                simplequeue.moinboundqueue
            WHERE
                Scheduled_dt <  DATE_SUB(NOW(),INTERVAL 2 HOUR)
            AND
                status_ti = 1                   
        </cfquery>        
                                   
            <cfif GetCountOldIntervals.TotalCount GT 250>
            
                <cfset ENA_Message ="Alert - SimpleX Monitor Alert - #DBSOURCE# simplequeue.moinboundqueue has too many unproccessed intervals queued up." >
                <cfset ErrorNumber = 10002>
                <cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">
            
            </cfif>     
            
             <cfoutput>
                GetCountOldIntervals TOTALCOUNT = #GetCountOldIntervals.TOTALCOUNT#
                <BR />
            </cfoutput>   
            
        
        <cfset ENA_Message ="System Monitor - #DBSOURCE# - GetCountOldIntervals.TOTALCOUNT = #GetCountOldIntervals.TOTALCOUNT# ">
        <cfset ErrorNumber = 0>
        <cfset AlertType = 3>
        <cfset SubjectLine = "SimpleX Monitor #DBSOURCE# GetCountOldIntervals Check Completed OK">
        <cfset Email=0>
        <cfinclude template="act_EscalationNotificationActions.cfm">
                               
        <cfcatch type="any">
                <cfset ENA_Message ="System Monitor - #DBSOURCE# - Catch ">
                <cfset ErrorNumber = 0>
                <cfset AlertType = 3>
                <cfset SubjectLine = "POSTED - SimpleX #DBSOURCE# SimpleX Monitor Alert Failed">
                <cfset Email=0>
                <cfinclude template="act_EscalationNotificationActions.cfm">
                
                <cfdump var="#cfcatch#" />
        </cfcatch>
    
    </cftry>   

<cfelse>

 	<cfset ENA_Message ="System Monitor - #DBSOURCE# - GetCountOldIntervals.TOTALCOUNT = Check Skipped - After hours  ">
	<cfset ErrorNumber = 0>
    <cfset AlertType = 3>
    <cfset SubjectLine = "SimpleX Monitor #DBSOURCE# GetCountOldIntervals Check Skipped - After hours ">
    <cfset Email=0>
    <cfinclude template="act_EscalationNotificationActions.cfm">
    
</cfif>

Done.

</body>
</html>