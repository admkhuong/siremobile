<cfparam name = "THRESHOLD" default = "500" />
<cfparam name = "INTERVAL" default = "HOUR" />
<cfparam name = "DBSOURCE" default = "Bishop" />
<cfinclude template="../../public/paths.cfm" >

<cfset TargetDataSource = "#DBSOURCE#" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Threshold Alert</title>
</head>

<body>
<cftry>

	<cfquery name="q_check_contactqueue" datasource="#DBSOURCE#">
		SELECT COUNT(*) AS threshold
        FROM simplequeue.contactqueue
        WHERE Scheduled_dt > DATE_SUB(NOW(), INTERVAL 1 #INTERVAL#)
    </cfquery>
    
    <cfif isdefined("q_check_contactqueue.recordcount") >
    	<cfif q_check_contactqueue.threshold gte THRESHOLD>
    		<cfset ENA_Message ="Alert - SimpleX Threshold Alert - #DBSOURCE# contactqueue insertion exceeds threshold (#THRESHOLD#/#INTERVAL#), total in last hour: #q_check_contactqueue.threshold#">
            <cfset ErrorNumber = 1001>
            <!---<cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">--->
            <cfinclude template="act_EscalationNotificationActions.cfm">
        <cfelse>
    	
			<cfset ENA_Message ="Statistics - SimpleX Threshold Alert - #DBSOURCE# contactqueue insertion total in last hour: #q_check_contactqueue.threshold#">
            <cfset ErrorNumber = 0>
            <cfset AlertType = 3>
            <cfset SubjectLine = "POSTED - SimpleX #DBSOURCE# contactqueue Threshold: #q_check_contactqueue.threshold# - OK">
            <cfset Email=0>
            <cfinclude template="act_EscalationNotificationActions.cfm">
            
    	</cfif>
    	
    	
    </cfif>
    
	<cfcatch type="any"><cfset ErrorLogInsertedOK = 0></cfcatch>

</cftry>   

<cftry> 

    <cfquery name="q_check_contactresults" datasource="#DBSOURCE#">
		SELECT COUNT(*) AS threshold
        FROM simplexresults.contactresults
        WHERE RXCDLStartTime_dt > DATE_SUB(NOW(), INTERVAL 1 #INTERVAL#)
    </cfquery>
    
    <cfif isdefined("q_check_contactresults.recordcount") >
    	<cfif q_check_contactresults.threshold gte THRESHOLD>
    		<cfset ENA_Message ="Alert - SimpleX Threshold Alert - #DBSOURCE# Contactresults insertion exceeds threshold (#THRESHOLD#/#INTERVAL#), total in last hour: #q_check_contactresults.threshold#">
            <cfset ErrorNumber = 1002>
            <!---<cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">--->
            <cfinclude template="act_EscalationNotificationActions.cfm">
        <cfelse>
			<cfset ENA_Message ="Statistics - SimpleX Threshold Alert - #DBSOURCE# Contactresults insertion total in last hour: #q_check_contactresults.threshold#">
            <cfset ErrorNumber = 0>
            <cfset AlertType = 3>            
            <cfset SubjectLine = "POSTED - SimpleX #DBSOURCE# contactresults Threshold: #q_check_contactresults.threshold# - OK">
            <cfset Email=0>            
            <cfinclude template="act_EscalationNotificationActions.cfm">
            
    	</cfif>
    	
    </cfif>
    
    <cfcatch type="any"><cfset ErrorLogInsertedOK = 0></cfcatch>

</cftry>

Done.


<!--- Check for any stuck threads in SMS Queue processing - need to kill via Coldfusion to fix if there are --->
<cftry> 

    <cfquery name="q_check_SMSQueueMTProcessing" datasource="#DBSOURCE#">		
        SELECT
            COUNT(*) AS threshold
        FROM 
            simpleobjects.threadcontrol_smsqueue
        WHERE
            IsRunning_bit = 1
        AND
            TIMESTAMPDIFF(MINUTE,LastRan_dt,NOW()) > 40
    </cfquery>
    
    <cfif isdefined("q_check_SMSQueueMTProcessing.recordcount") >
    	<cfif q_check_SMSQueueMTProcessing.threshold GT 0>
    		<cfset ENA_Message ="Alert - SimpleX Threshold Alert - #DBSOURCE# simpleobjects.threadcontrol_smsqueue processing thread exceeds threshold (40 Minutes) ">
            <cfset ErrorNumber = 1002>
            <!---<cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">--->
            <cfinclude template="act_EscalationNotificationActionsHighPriority.cfm">
        <cfelse>
			<cfset ENA_Message ="Statistics - SimpleX Threshold Alert - #DBSOURCE# simpleobjects.threadcontrol_smsqueue processing thread OK (40 Minutes)">
            <cfset ErrorNumber = 0>
            <cfset AlertType = 3>            
            <cfset SubjectLine = "POSTED - SimpleX #DBSOURCE# simpleobjects.threadcontrol_smsqueue processing Threshold: - OK">
            <cfset Email=0>            
            <cfinclude template="act_EscalationNotificationActions.cfm">
            
    	</cfif>
    	
    </cfif>
    
    <cfcatch type="any"><cfset ErrorLogInsertedOK = 0></cfcatch>

</cftry>






</body>
</html>