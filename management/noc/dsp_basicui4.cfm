

<cfparam name="STARTHOUR_TI" TYPE="string" default="9">
<cfparam name="ENDHOUR_TI" TYPE="string" default="17">
<cfparam name="START_DT" TYPE="string" default="#LSDateFormat(NOW(), 'yyyy-mm-dd')#">
<cfparam name="STOP_DT" TYPE="string" default="#LSDateFormat(dateadd('d', 30, NOW()), 'yyyy-mm-dd')#">
<cfparam name="LOOPLIMIT_INT" TYPE="string" default="200">


<cfoutput>

    <div style="clear:both; width:500px; padding:5px; height: 235px; min-height:235px;">
    
        <div style="float:left; min-width:245px; height: 235px; min-height:235px;">
            <label title="Calander date queued calls are eligible to start going out.">Start Date</label>
            <div TYPE="text" name="START_DT" id="START_DT" value="#LSDateFormat(START_DT, 'yyyy-mm-dd')#" class="ui-corner-all" style="width:145px;" title="Calander date queued calls are eligible to start going out."/>
        </div>
        
        <div style="float:right; min-width:245px; height: 235px; min-height:235px;"> 
            <label title="Calander date queued calls are stopped from going out. Calls not completed by this date will be removed from the call queue.">Stop Date</label>
            <div TYPE="text" name="STOP_DT" id="STOP_DT" value="#LSDateFormat(STOP_DT, 'yyyy-mm-dd')#" class="ui-corner-all" style="width:145px;" title="Calander date queued calls are stopped from going out. Calls not completed by this date will be removed from the call queue."/> 
        </div>    
    </div>
    
    <div  id="HourOptions" style="clear:both; width:500px; padding:5px; height: 45px; min-height:45px;" >		
        <div style="float:left; min-width:245px;">
            <label style="display:block; width:130px; text-align:left; margin:0px 0 2px 5px;" title="Calls will not start until the time LOCALOUTPUT to the phone number is the start hour or later.">Start Hour</label>
            <select name="STARTHOUR_TI" id="STARTHOUR_TI" size="1" style="width:90px;" title="Calls will not start until the time LOCALOUTPUT to the phone number is the start hour or later."> 
            
                <cfif Session.AfterHours GT 0>
                        <option value="0" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# LTE 0>selected="selected"</cfif>>NONE</option>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #STARTHOUR_TI# EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #STARTHOUR_TI# EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
            </select>    
        </div>
                    
        <div style="float:right; min-width:245px;"> 
            <label title="Calls still in queue after this time will carry over until tommorow's start hour.">End Hour</label>
            <select name="ENDHOUR_TI" id="ENDHOUR_TI" size="1" style="width:90px;" title="Calls still in queue after this time will carry over until tommorow's start hour."> 
            
                <cfif Session.AfterHours GT 0>
                        <option value="0" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# LTE 0>selected="selected"</cfif>>NONE</option>
                        <option value="1" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 1>selected="selected"</cfif>>1 AM</option>
                        <option value="2" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 2>selected="selected"</cfif>>2 AM</option>
                        <option value="3" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 3>selected="selected"</cfif>>3 AM</option>
                        <option value="4" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 4>selected="selected"</cfif>>4 AM</option>
                        <option value="5" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 5>selected="selected"</cfif>>5 AM</option>
                        <option value="6" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 6>selected="selected"</cfif>>6 AM</option>
                        <option value="7" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 7>selected="selected"</cfif>>7 AM</option>
                        <option value="8" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 8>selected="selected"</cfif>>8 AM</option>
                    </cfif>
                    
                    <option value="9" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 9>selected="selected"</cfif>>9 AM</option>
                    <option value="10" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 10>selected="selected"</cfif>>10 AM</option>
                    <option value="11" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 11>selected="selected"</cfif>>11 AM</option>
                    <option value="12" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 12>selected="selected"</cfif>>12 PM </option>
                    <option value="13" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 13>selected="selected"</cfif>>1  PM</option>
                    <option value="14" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 14>selected="selected"</cfif>>2  PM</option>
                    <option value="15" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 15>selected="selected"</cfif>>3  PM</option>
                    <option value="16" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 16>selected="selected"</cfif>>4  PM</option>
                    <option value="17" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 17>selected="selected"</cfif>>5  PM</option>
                    <option value="18" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 18>selected="selected"</cfif>>6  PM</option>
                    <option value="19" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 19>selected="selected"</cfif>>7  PM</option>
                    <option value="20" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 20>selected="selected"</cfif>>8  PM</option>
                    <option value="21" class="HourSelectOption" <cfif #ENDHOUR_TI# EQ 21>selected="selected"</cfif>>9  PM</option>
                    
                    <cfif Session.AfterHours GT 0>
                        <option value="22" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 22>selected="selected"</cfif>>10 PM</option>
                        <option value="23" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 23>selected="selected"</cfif>>11 PM</option>
                        <option value="24" class="HourSelectOptionOutOfRange" <cfif #ENDHOUR_TI# EQ 24>selected="selected"</cfif>>12 AM</option>
                    </cfif>
            </select>
        </div>    
    </div>
    
</cfoutput>    