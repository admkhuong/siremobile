<cfparam name="is1080p" default="1">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EBM NOC</title>

<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/MB/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/RXForm.css" />
    
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.4.4.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
    
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 

 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
 
 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
<script TYPE="text/javascript">
	var CurrSitePath = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>';
</script>


</cfoutput>

<!---

Each Section

Stats
State
Errors


New Batches for today Count
New Audio Uploads Count
New Accounts




How many in Queue?
Count last hour?


Last Distro Start Time
Last Distro End Time
Last Distro Step Time



Last Updated - page numbers


Total Runs since midnight
expect total since midnight


Last 5/10/100





Last Updated Stats


--->

<script>


$(function() 
{
	
	$("#TestMe").click(function() { LoadQueueStats();  });
	
	LoadQueueStats();
	LoadEBMStats();
	LoadFulfillmentStats();
	LoadExtractionStats();
	
});
	

function LoadQueueStats()
{
	  $("#NOCQueueLoading").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/monitor.cfc?method=GetPublishQueueStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#NOCQueueLoading").hide(); <!--- Reload NOC stats in XXXX milisecods--->	setTimeout('LoadQueueStats()', 120000);<!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{						
							if(typeof(d.DATA.LASTUPDATESTATS[0]) != "undefined")								
								$("#DFM_Queue_Stats #LASTUPDATESTATS").html(d.DATA.LASTUPDATESTATS[0]);
							else
								$("#DFM_Queue_Stats #LASTUPDATESTATS").html('');
								
							if(typeof(d.DATA.LASTSTART[0]) != "undefined")								
								$("#DFM_Queue_Stats #LASTSTART").html(d.DATA.LASTSTART[0]);
							else
								$("#DFM_Queue_Stats #LASTSTART").html('unknown');
								
							if(typeof(d.DATA.LASTEND[0]) != "undefined")								
								$("#DFM_Queue_Stats #LASTEND").html(d.DATA.LASTEND[0]);
							else
								$("#DFM_Queue_Stats #LASTEND").html('unknown');	
								
							if(typeof(d.DATA.RUNCOUNT[0]) != "undefined")								
								$("#DFM_Queue_Stats #RUNCOUNT").html(d.DATA.RUNCOUNT[0]);
							else
								$("#DFM_Queue_Stats #RUNCOUNT").html('unknown');										
																						     
					}
																								
					$("#NOCQueueLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadQueueStats()', 120000);
			
				}
				else
				{<!--- Invalid structure returned --->	
					$("#NOCQueueLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadQueueStats()', 120000);
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#NOCQueueLoading").hide();
				<!--- Reload NOC stats in XXXX milisecods--->
				setTimeout('LoadQueueStats()', 120000);
			}				
		} 		
				
	});

	return false;
	
}


function LoadEBMStats()
{	
	  $("#NOCEBMLoading").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/monitor.cfc?method=GetEBMStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#NOCEBMLoading").hide(); <!--- Reload NOC stats in XXXX milisecods---> setTimeout('LoadEBMStats()', 300000);<!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
						
						if(typeof(d.DATA.LASTUPDATESTATS[0]) != "undefined")								
							$("#DFM_EBM_Stats #LASTUPDATESTATS").html(d.DATA.LASTUPDATESTATS[0]);
						else
							$("#DFM_EBM_Stats #LASTUPDATESTATS").html('');
															
						if(typeof(d.DATA.USERCOUNT[0]) != "undefined")								
							$("#DFM_EBM_Stats #USERCOUNT").html(d.DATA.USERCOUNT[0]);
						else
							$("#DFM_EBM_Stats #USERCOUNT").html('unknown');
							
						if(typeof(d.DATA.USERSLOGINLAST30[0]) != "undefined")								
							$("#DFM_EBM_Stats #USERSLOGINLAST30").html(d.DATA.USERSLOGINLAST30[0]);
						else
							$("#DFM_EBM_Stats #USERSLOGINLAST30").html('unknown');	
							
						if(typeof(d.DATA.USERSREGLAST30[0]) != "undefined")								
							$("#DFM_EBM_Stats #USERSREGLAST30").html(d.DATA.USERSREGLAST30[0]);
						else
							$("#DFM_EBM_Stats #USERSREGLAST30").html('unknown');	
																						     
					}
																								
					$("#NOCEBMLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadEBMStats()', 300000);
			
				}
				else
				{<!--- Invalid structure returned --->	
					$("#NOCEBMLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadEBMStats()', 300000);
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#NOCEBMLoading").hide();
				<!--- Reload NOC stats in XXXX milisecods--->
				setTimeout('LoadEBMStats()', 300000);
			}				
		} 		
				
	});

	return false;
}


function LoadFulfillmentStats()
{
	  $("#NOCFulfillmentLoading").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/monitor.cfc?method=GetFulfillmentStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) { $("#NOCFulfillmentLoading").hide(); <!--- Reload NOC stats in XXXX milisecods---> setTimeout('LoadFulfillmentStats()', 300000); <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
					
						if(typeof(d.DATA.LASTUPDATESTATS[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #LASTUPDATESTATS").html(d.DATA.LASTUPDATESTATS[0]);
						else
							$("#DFM_Fulfillment_Stats #LASTUPDATESTATS").html('');
							
						if(typeof(d.DATA.UNIQUERXDIALERCOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #UNIQUERXDIALERCOUNT").html(d.DATA.UNIQUERXDIALERCOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #UNIQUERXDIALERCOUNT").html('unknown');	
															
						if(typeof(d.DATA.DTSQUEUEDELLIGIBLECOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #DTSQUEUEDELLIGIBLECOUNT").html(d.DATA.DTSQUEUEDELLIGIBLECOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #DTSQUEUEDELLIGIBLECOUNT").html('unknown');
							
						if(typeof(d.DATA.DTSQUEUEDCOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #DTSQUEUEDCOUNT").html(d.DATA.DTSQUEUEDCOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #DTSQUEUEDCOUNT").html('unknown');	
							
						if(typeof(d.DATA.DTSPROCCOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #DTSPROCCOUNT").html(d.DATA.DTSPROCCOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #DTSPROCCOUNT").html('unknown');	
							
						if(typeof(d.DATA.CALLRESULTQUEUECOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #CALLRESULTQUEUECOUNT").html(d.DATA.CALLRESULTQUEUECOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #CALLRESULTQUEUECOUNT").html('unknown');	
							
						if(typeof(d.DATA.CALLRESULTCOMPLETECOUNT[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #CALLRESULTCOMPLETECOUNT").html(d.DATA.CALLRESULTCOMPLETECOUNT[0]);
						else
							$("#DFM_Fulfillment_Stats #CALLRESULTCOMPLETECOUNT").html('unknown');		
																						     
					}
																								
					$("#NOCFulfillmentLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadFulfillmentStats()', 300000);
				}
				else
				{<!--- Invalid structure returned --->	
					$("#NOCFulfillmentLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadFulfillmentStats()', 300000);
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#NOCFulfillmentLoading").hide();
				<!--- Reload NOC stats in XXXX milisecods--->
				setTimeout('LoadFulfillmentStats()', 300000);				
			}				
		} 		
				
	});



	return false;
}
	
	
function LoadExtractionStats()
{ 
	  $("#NOCQueueLoading").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/cfc/monitor.cfc?method=GetExtractionStats&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#NOCExtractionLoading").hide(); 	<!--- Reload NOC stats in XXXX milisecods---> setTimeout('LoadExtractionStats()', 120000); <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{						
							if(typeof(d.DATA.LASTUPDATESTATS[0]) != "undefined")								
								$("#DFM_Extraction_Stats #LASTUPDATESTATS").html(d.DATA.LASTUPDATESTATS[0]);
							else
								$("#DFM_Extraction_Stats #LASTUPDATESTATS").html('');
								
							if(typeof(d.DATA.LASTSTART[0]) != "undefined")								
								$("#DFM_Extraction_Stats #LASTSTART").html(d.DATA.LASTSTART[0]);
							else
								$("#DFM_Extraction_Stats #LASTSTART").html('unknown');
								
							if(typeof(d.DATA.LASTEND[0]) != "undefined")								
								$("#DFM_Extraction_Stats #LASTEND").html(d.DATA.LASTEND[0]);
							else
								$("#DFM_Extraction_Stats #LASTEND").html('unknown');	
								
							if(typeof(d.DATA.RUNCOUNT[0]) != "undefined")								
								$("#DFM_Extraction_Stats #RUNCOUNT").html(d.DATA.RUNCOUNT[0]);
							else
								$("#DFM_Extraction_Stats #RUNCOUNT").html('unknown');										
																						     
					}
																								
					$("#NOCExtractionLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadExtractionStats()', 120000);
			
				}
				else
				{<!--- Invalid structure returned --->	
					$("#NOCExtractionLoading").hide();
					<!--- Reload NOC stats in XXXX milisecods--->
					setTimeout('LoadExtractionStats()', 120000);
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#NOCExtractionLoading").hide();
				<!--- Reload NOC stats in XXXX milisecods--->
				setTimeout('LoadExtractionStats()', 120000);
			}				
		} 		
				
	});

	return false;
}


</script>


<style>

	body
	{
		margin:0;
		padding:0;
		background-image: url("../../public/css/pepper-grinder/images/ui-bg_fine-grain_15_eceadf_60x60.png");
		<cfif is1080p GT 0>padding-left:160px;</cfif>	
		<cfif is1080p GT 0>padding-top:90px;</cfif>		
	}

	
	#DFM_Main
	{
		background-image: url("../../public/images/NOC2_Web1600x900.png");
		background-repeat:no-repeat;		
		min-width:1600px;
		min-height:1000px;
	}


	#DFM_Main h1
	{
		font-size:12px;
		font-weight:bold;	
		display:inline;
		padding-right:10px;	
		min-width: 100px;
		width: 100px;
		
		
	}

	#DFM_Main BR
	{ 			
		line-height:12px;		
	}
	
	#DFM_Main p
	{ 	
		font-size:12px;
		font-weight:normal;	
	}
	
	#DFM_Queue
	{
		position:absolute;		
		<cfif is1080p GT 0>top:105px; <cfelse> top:15px;	</cfif>
		<cfif is1080p GT 0>left:775px; <cfelse> left:615px;	</cfif>		
		min-width:250px;
		min-height:100px;
		
	}
	
	.NOCLoading
	{
		position:absolute;
		right:20px;
		top:2px;		
	}

	#DFM_Queue_Ani
	{
		position:absolute;		
		<cfif is1080p GT 0>top:535px; <cfelse> top:445px;	</cfif>
		<cfif is1080p GT 0>left:680px; <cfelse> left:520px;	</cfif>
	}
	
	#DFM_Queue_Ani_LeftArrow
	{
		position:absolute;		
		<cfif is1080p GT 0>top:890px; <cfelse> top:800px;	</cfif>
		<cfif is1080p GT 0>left:300px; <cfelse> left:140px;	</cfif>
	}
	
	#DFM_Queue_Stats
	{
		background-color: #FFFFFF;
		position:absolute;		
		top:5px;
		left:5px;
		min-width:350px;
		width: 350px;
		min-height:400px;
		border: #000 2px solid;
		box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, 0.5);
		border-radius: 20px 20px 20px 20px;
		overflow:hidden;
		padding: 5px 5px 5px 5px;
	}
	
	#DFM_EBM
	{
		position:absolute;		
		<cfif is1080p GT 0>top:105px; <cfelse> top:15px;	</cfif>
		<cfif is1080p GT 0>left:175px; <cfelse> left:15px;	</cfif>
		min-width:250px;
		min-height:100px;
	}
		
	#DFM_EBM_Stats
	{
		background-color: #FFFFFF;
		position:absolute;		
		top:5px;
		left:5px;		
		min-width:300px;
		width: 300px;
		min-height:690px;
		border: #000 2px solid;
		box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, 0.5);
		border-radius: 20px 20px 20px 20px;
		overflow:hidden;
		padding: 5px 5px 5px 5px;
	}
	
	#DFM_Extraction
	{
		position:absolute;		
		<cfif is1080p GT 0>top:685px; <cfelse> top:595px;	</cfif>
		<cfif is1080p GT 0>left:548px; <cfelse> left:388px;	</cfif>		
		min-width:250px;
		min-height:100px;
	}
		
	#DFM_Extraction_Stats
	{
		background-color: #FFFFFF;
		position:absolute;		
		top:5px;
		left:5px;	
		min-width:798px;
		width: 798px;
		min-height:200px;
		border: #000 2px solid;
		box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, 0.5);
		border-radius: 20px 20px 20px 20px;
		overflow:hidden;
		padding: 5px 5px 5px 5px;
	}
	
	#DFM_Fulfillment
	{
		position:absolute;		
		<cfif is1080p GT 0>top:105px; <cfelse> top:15px;	</cfif>
		<cfif is1080p GT 0>left:1172px; <cfelse> left:1012px;	</cfif>
		min-width:450px;
		min-height:200px;
	}
			
	#DFM_Fulfillment_Stats
	{
		background-color: #FFFFFF;
		position:absolute;		
		top:5px;
		left:5px;	
		min-width:550px;
		width: 550px;
		min-height:330px;
		border: #000 2px solid;
		box-shadow: 20px 20px 10px -10px rgba(88, 88, 88, 0.5);
		border-radius: 20px 20px 20px 20px;
		overflow:hidden;
		padding: 5px 5px 5px 5px;
	}
	
	.NOCValue1
	{
		font-size:12px;
		font-weight:normal;	
		display:inline;
		width: 150px;
		position:absolute;		
		left:110px;	
	}
	
	.NOCValue3
	{
		font-size:12px;
		font-weight:normal;	
		display:inline;
		width: 150px;
		position:absolute;		
		left:200px;	
	}
	
	.NOCValue4
	{
		font-size:12px;
		font-weight:normal;	
		display:inline;
		width: 150px;
		position:absolute;		
		left:200px;	
	}
	
	.NOCHeader1
	{
		font-size:12px;
		font-weight:bold;	
		display:inline;
		min-width:200px;
		width: 200px;
		padding-right:10px;			
		overflow:hidden;
		position:absolute;		
		left:5px;	
		
	}
	
	.NOCHeader2
	{
		font-size:12px;
		font-weight:bold;
		font-style:italic;
		display:inline;
		min-width:200px;
		width: 200px;
		padding-right:10px;			
		overflow:hidden;
		position:absolute;		
		left:5px;	
		
	}
	
	.NOCHeader3
	{
		font-size:12px;
		font-weight:bold;	
		display:inline;
		min-width:200px;
		width: 200px;
		padding-right:10px;			
		overflow:hidden;
		position:absolute;		
		left:5px;	
		
	}
	
	
</style>

</head>

<body>


    
    <div id="DFM_Main">
            
           
            <img id="DFM_Queue_Ani" src="../../public/images/QueueArrow_Web.gif"></img>
            <img id="DFM_Queue_Ani_LeftArrow" src="../../public/images/QueueArrowLeft_Web.gif"></img>
            
                    
            <!--- EBM System --->
            <div id="DFM_EBM">
                  
                 <div id="DFM_EBM_Stats">	
	                 
    	            <div id="NOCEBMLoading" class="NOCLoading">
                        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                    </div>
                    
                    <div class="NOCHeader1">EBM System</div><div id="LASTUPDATESTATS" class="NOCValue3"></div>
                    <BR />
                    <HR />
                    
                    <div class="NOCHeader2">Statistics</div>
                    <BR />
                    <BR />
                    
                    <div class="NOCHeader3">Users</div><div id="USERCOUNT" class="NOCValue3"></div>
                    <BR />
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    <div class="NOCHeader3">New Users Last 30 Days</div><div id="USERSREGLAST30" class="NOCValue3"></div>                
                	<BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                    <div class="NOCHeader3">Count Logged In Last 30 Days</div><div id="USERSLOGINLAST30" class="NOCValue3"></div>
                    <BR />
                    
                </div>	
            
            </div>
           
            <!--- Queue --->
            <div id="DFM_Queue">
                      
                <div id="DFM_Queue_Stats">
            
	                <div id="NOCQueueLoading" class="NOCLoading">
                        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                    </div>
                
                    <div class="NOCHeader1">Queue</div><div id="LASTUPDATESTATS" class="NOCValue3"></div>
                    <BR />
                    <HR />
                    
                    <div class="NOCHeader2">Statistics</div>
                    <BR />
                    <BR />
                             
                    <div class="NOCHeader2">Processing</div>
                    <BR />
                    <BR />
                    
                    <div class="NOCHeader1">Start</div><div id="LASTSTART" class="NOCValue1"></div>
                    <BR />
                    <div class="NOCHeader1">Finish</div><div id="LASTEND" class="NOCValue1"></div>
                    <BR />
                    <div class="NOCHeader1">Runs</div><div id="RUNCOUNT" class="NOCValue1"></div>                
                
                </div>	
            
            </div>
             
            <!--- Extraction --->
            <div id="DFM_Extraction">
               
    
                <div id="DFM_Extraction_Stats">
                
                 	<div id="NOCExtractionLoading" class="NOCLoading">
                        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                    </div>
                    
                    <div class="NOCHeader1">Extraction</div><div id="LASTUPDATESTATS" class="NOCValue3"></div>
                    <BR />
                    <HR />
                    
                    <div class="NOCHeader2">Statistics</div>
                    <BR />
                    <BR />
                          
                    <div class="NOCHeader2">Processing</div>
                    <BR />
                    <div class="NOCHeader1">Start</div><div id="LASTSTART" class="NOCValue1"></div>
                    <BR />
                    <div class="NOCHeader1">Finish</div><div id="LASTEND" class="NOCValue1"></div>
                    <BR />
                    <div class="NOCHeader1">Runs</div><div id="RUNCOUNT" class="NOCValue1"></div>                      
                
                </div>	
            
            </div>
      
            <!--- Fulfillment --->
            <div id="DFM_Fulfillment">
               
    
                <div id="DFM_Fulfillment_Stats">
                
	                <div id="NOCFulfillmentLoading" class="NOCLoading">
                        <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                    </div>
                    
                    <div class="NOCHeader1">Fulfillment</div><div id="LASTUPDATESTATS" class="NOCValue3"></div>
                    <BR />
                    <HR />
                    
                    <div class="NOCHeader2">Statistics</div>
                    <BR />
                    <BR />
                                                    
                    <div class="NOCHeader1">Total RXDialers</div><div id="UNIQUERXDIALERCOUNT" class="NOCValue4"></div>
                    <BR />      
                    <div class="NOCHeader1">Elligible to Run on RXDialer</div><div id="DTSQUEUEDELLIGIBLECOUNT" class="NOCValue4"></div>
                    <BR />
                    <div class="NOCHeader1">Queued on RXDialer</div><div id="DTSQUEUEDCOUNT" class="NOCValue4"></div>
                    <BR />
                    <div class="NOCHeader1">Inprocess on RXDialer</div><div id="DTSPROCCOUNT" class="NOCValue4"></div>                
                    <BR />
                    <div class="NOCHeader1">Complete Waiting for Extraction</div><div id="CALLRESULTQUEUECOUNT" class="NOCValue4"></div>
                    <BR />
                    <div class="NOCHeader1">Complete and Extracted</div><div id="CALLRESULTCOMPLETECOUNT" class="NOCValue4"></div>                
                
                </div>	
            
            </div>
                        
      
                        
            
                
    <!---    <a id="TestMe" style="position:absolute; top:950px; left:1400px;">Test</a>--->
    
    </div>


</body>
</html>
