<cfparam name="is1080p" default="1">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EBM Test Services</title>

<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/MB/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/rxform2.css" />
    
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.4.4.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
    
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 

 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
 
 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
<script TYPE="text/javascript">
	var CurrSitePath = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>';
</script>


</cfoutput>


<script>

$(function() 
{
	$("#TestServicesDiv #loadingDlgTestServices").hide();
	$("#TestServicesButton").click(function() { QueueSingleCall();  });
	
	
	$("#ValidateBillingButton").click(function() { ValidateBilling();  });
	
	
	$("#GetTZ").click(function() { GetTZInfo();  });
	
});
	
	
	
function QueueSingleCall()
{
	  $("#loadingDlgTestServices").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/EBMAPI.cfc?method=AddInputToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { inpDialString : $("#TestServicesDiv #inpDialString").val(), 
	  		   INPBATCHID : $("#TestServicesDiv #INPBATCHID").val(),
    		   inpChannelMask : $("#TestServicesDiv #inpChannelMask").val(),
			   inpServicePassword : $("#TestServicesDiv #inpServicePassword").val(), 
			   FirstName_vch : $("#TestServicesDiv #FirstName_vch").val(),
			   LastName_vch : $("#TestServicesDiv #LastName_vch").val(),			   
			   ContactStringVoice_vch : $("#TestServicesDiv #ContactStringVoice_vch").val(), 
			   ContactStringSMS_vch : $("#TestServicesDiv #ContactStringSMS_vch").val(), 
			   ContactStringEMail_vch : $("#TestServicesDiv #ContactStringEMail_vch").val(), 
			   inpValidateScheduleActive : $("#TestServicesDiv #inpValidateScheduleActive").val(),			   
			   inpScheduleOffsetSeconds : $("#TestServicesDiv #inpScheduleOffsetSeconds").val(),
			   CustomField1_vch : $("#TestServicesDiv #CustomField1_vch").val(), 
			   CustomField2_vch : $("#TestServicesDiv #CustomField2_vch").val(), 
			   CustomField3_vch : $("#TestServicesDiv #CustomField3_vch").val(), 
			   CustomField4_vch : $("#TestServicesDiv #CustomField4_vch").val(), 
			   CustomField5_vch : $("#TestServicesDiv #CustomField5_vch").val(), 
			   CustomField6_vch : $("#TestServicesDiv #CustomField6_vch").val(), 
			   CustomField7_vch : $("#TestServicesDiv #CustomField7_vch").val(), 
			   CustomField8_vch : $("#TestServicesDiv #CustomField8_vch").val(), 
			   CustomField9_vch : $("#TestServicesDiv #CustomField9_vch").val(), 
			   CustomField10_vch : $("#TestServicesDiv #CustomField10_vch").val(),
			   CustomField1_int : $("#TestServicesDiv #CustomField1_int").val(), 
			   CustomField2_int : $("#TestServicesDiv #CustomField2_int").val(), 
			   CustomField3_int : $("#TestServicesDiv #CustomField3_int").val(), 
			   CustomField4_int : $("#TestServicesDiv #CustomField4_int").val(), 
			   CustomField5_int : $("#TestServicesDiv #CustomField5_int").val(), 
			   CustomField6_int : $("#TestServicesDiv #CustomField6_int").val(), 
			   CustomField7_int : $("#TestServicesDiv #CustomField7_int").val(), 
			   CustomField8_int : $("#TestServicesDiv #CustomField8_int").val(), 
			   CustomField9_int : $("#TestServicesDiv #CustomField9_int").val(), 
			   CustomField10_int : $("#TestServicesDiv #CustomField10_int").val()
		   },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#loadingDlgTestServices").hide(); <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
					
				<!---		if(typeof(d.DATA.LASTUPDATESTATS[0]) != "undefined")								
							$("#DFM_Fulfillment_Stats #LASTUPDATESTATS").html(d.DATA.LASTUPDATESTATS[0]);
						else
							$("#DFM_Fulfillment_Stats #LASTUPDATESTATS").html('');--->
													
																						     
					}
																								
					$("#loadingDlgTestServices").hide();
	
				}
				else
				{<!--- Invalid structure returned --->	
					$("#loadingDlgTestServices").hide();
					
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#loadingDlgTestServices").hide();
				
			}				
		} 		
				
	});

	return false;
}


function ValidateBilling()
{
	  $("#loadingDlgTestServices").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/Billing.cfc?method=ValidateBilling&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { inpUserId : $("#TestServicesDiv #inpUserId").val(), inpCount : $("#TestServicesDiv #inpCount").val() },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#loadingDlgTestServices").hide(); <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
																							
																						     
					}
																								
					$("#loadingDlgTestServices").hide();
	
				}
				else
				{<!--- Invalid structure returned --->	
					$("#loadingDlgTestServices").hide();
					
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#loadingDlgTestServices").hide();
				
			}				
		} 		
				
	});

	return false;
}


function GetTZInfo()
{
	  $("#loadingDlgTestServices").show();		
			
	  $.ajax({
	  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/EBMAPI.cfc?method=GetTZFromContactString&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
	  dataType: 'json',
	  type: 'POST',
	  data:  { INPCONTACTSTRING : $("#TestServicesDiv #inpDialString").val() },					  
	  error: function(XMLHttpRequest, textStatus, errorThrown) {$("#loadingDlgTestServices").hide(); <!---console.log(textStatus, errorThrown);--->},					  
	  success:
		  
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
			
			// alert(d);
																						
			<!--- Get row 1 of results if exisits--->
			if (d.ROWCOUNT > 0) 
			{																									
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{		
																							
																						     
					}
																								
					$("#loadingDlgTestServices").hide();
	
				}
				else
				{<!--- Invalid structure returned --->	
					$("#loadingDlgTestServices").hide();
					
				}
			}
			else
			{<!--- No result returned --->
				<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
				jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				$("#loadingDlgTestServices").hide();
				
			}				
		} 		
				
	});

	return false;
}

   
</script> 

</head>


<style>

.Body_Sand
	{	
		background-color:#FFF;
		background-image: url(../../public/css/pepper-grinder/images/ui-bg_fine-grain_15_eceadf_60x60.png);
		margin:5px;
		padding:10px;		
	}


</style>


<body class="Body_Sand">



<cfoutput>

        
<div id='TestServicesDiv' class="RXFormContainerX rxform2" style="background-color:##FFF;">

<form id="TestServicesForm" name="TestServicesForm" action="" method="POST">

    <table border="1">         
	    <tr>
    	    <td valign="top">
    
            <table>
                <tr>
                    <td>
                        <label>inpDialString</label>
                        <input TYPE="text" name="inpDialString" id="inpDialString" size="255" class="ui-corner-all" value="9494284899" /> 
                    </td>
                </tr>
                   
                <tr>
                    <td>   
                        <label>ContactStringVoice_vch</label>
                        <input TYPE="text" name="ContactStringVoice_vch" id="ContactStringVoice_vch" size="255" class="ui-corner-all" value="9494284899" /> 
                    </td>
                </tr>
				<tr>
                    <td>                   
                        <label>ContactStringSMS_vch</label>
                        <input TYPE="text" name="ContactStringSMS_vch" id="ContactStringSMS_vch" size="255" class="ui-corner-all" value="9494284899" /> 
                      </td>
                </tr>

				<tr>
                    <td>
                        <label>ContactStringEMail_vch</label>
                        <input TYPE="text" name="ContactStringEMail_vch" id="ContactStringEMail_vch" size="255" class="ui-corner-all" value="jpeterson@messageBroadcast.com" /> 
                     </td>
                </tr>
                
                <tr>
                    <td>                                  
 	                   <label>FirstName_vch</label>
    	                <input TYPE="text" name="FirstName_vch" id="FirstName_vch" size="255" class="ui-corner-all" value="XXX" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                                  
 	                   <label>LastName_vch</label>
    	                <input TYPE="text" name="LastName_vch" id="LastName_vch" size="255" class="ui-corner-all" value="XXX" /> 
                    </td>
                </tr>
                

				<tr>
                    <td>
                        <label>INPBATCHID</label>
                        <input TYPE="text" name="INPBATCHID" id="INPBATCHID" size="255" class="ui-corner-all" value="192" /> 
                      </td>
                </tr>
				
                <tr>
                    <td>
                      	<select id="inpChannelMask" name="inpChannelMask" style="width:200px;">        
                            <option value="0" class="ScriptSelectOption" selected="selected">All Channels</option> 
                            <option value="1" class="ScriptSelectOption">Voice</option>
                            <option value="2" class="ScriptSelectOption">e-Mail</option>
                            <option value="3" class="ScriptSelectOption">SMS</option>                              
                   		</select>
		            </td>
                </tr>
        
                <tr>
                    <td>                   
                        <label>inpServicePassword</label>
                        <input TYPE="text" name="inpServicePassword" id="inpServicePassword" size="255" class="ui-corner-all" value="XXX" /> 
                    </td>
                </tr>
                
		        <tr>
                    <td>
                        <label>inpUserId</label>
                        <input TYPE="text" name="inpUserId" id="inpUserId" size="255" class="ui-corner-all" value="10" /> 
                      </td>
                </tr>
                   
				<tr>
                    <td>                    
                    	<label>inpCount</label>
                    	<input TYPE="text" name="inpCount" id="inpCount" size="255" class="ui-corner-all" value="1" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <label>inpValidateScheduleActive</label>
                        <input TYPE="text" name="inpValidateScheduleActive" id="inpValidateScheduleActive" size="255" class="ui-corner-all" value="0" /> 
                    </td>
                </tr>
                
                 <tr>
                    <td>
                        <label>Offset in Seconds</label>
                        <input TYPE="text" name="inpScheduleOffsetSeconds" id="inpScheduleOffsetSeconds" size="255" class="ui-corner-all" value="0" /> 
                    </td>
                </tr>
                                           
                 
            </table>
            
       </td>
	
       <td valign="top">
            
            <table>
            
                <tr>
                    <td>                
                        <label>CustomField1_vch</label>
                        <input TYPE="text" name="CustomField1_vch" id="CustomField1_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                   
                <tr>
                    <td>   
                        <label>CustomField2_vch</label>
                        <input TYPE="text" name="CustomField2_vch" id="CustomField2_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                   
                <tr>
                    <td>                  
                        <label>CustomField3_vch</label>
                        <input TYPE="text" name="CustomField3_vch" id="CustomField3_vch" size="255" class="ui-corner-all" value="" />           
                    </td>
                </tr>
                   
                <tr>
                    <td>                  
                        <label>CustomField4_vch</label>
                        <input TYPE="text" name="CustomField4_vch" id="CustomField4_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                           
                        <label>CustomField5_vch</label>
                        <input TYPE="text" name="CustomField5_vch" id="CustomField5_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField6_vch</label>
                        <input TYPE="text" name="CustomField6_vch" id="CustomField6_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField7_vch</label>
                        <input TYPE="text" name="CustomField7_vch" id="CustomField7_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField8_vch</label>
                        <input TYPE="text" name="CustomField8_vch" id="CustomField8_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField9_vch</label>
                        <input TYPE="text" name="CustomField9_vch" id="CustomField9_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField10_vch</label>
                        <input TYPE="text" name="CustomField10_vch" id="CustomField10_vch" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
            
            </table>                

		</td>
        
        <td valign="top">
            
            <table>
            
                <tr>
                    <td>                
                        <label>CustomField1_int</label>
                        <input TYPE="text" name="CustomField1_int" id="CustomField1_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                   
                <tr>
                    <td>   
                        <label>CustomField2_int</label>
                        <input TYPE="text" name="CustomField2_int" id="CustomField2_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                   
                <tr>
                    <td>                  
                        <label>CustomField3_int</label>
                        <input TYPE="text" name="CustomField3_int" id="CustomField3_int" size="255" class="ui-corner-all" value="" />           
                    </td>
                </tr>
                   
                <tr>
                    <td>                  
                        <label>CustomField4_int</label>
                        <input TYPE="text" name="CustomField4_int" id="CustomField4_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                           
                        <label>CustomField5_int</label>
                        <input TYPE="text" name="CustomField5_int" id="CustomField5_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField6_int</label>
                        <input TYPE="text" name="CustomField6_int" id="CustomField6_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField7_int</label>
                        <input TYPE="text" name="CustomField7_int" id="CustomField7_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField8_int</label>
                        <input TYPE="text" name="CustomField8_int" id="CustomField8_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField9_int</label>
                        <input TYPE="text" name="CustomField9_int" id="CustomField9_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
                
                <tr>
                    <td>                
                        <label>CustomField10_int</label>
                        <input TYPE="text" name="CustomField10_int" id="CustomField10_int" size="255" class="ui-corner-all" value="" /> 
                    </td>
                </tr>
            
            </table>                

		</td>

	</tr>

</table>


<br />                
            
               <button id="TestServicesButton" TYPE="button" class="ui-corner-all">Test Single Queue</button>
                    <BR />
                    <button id="ValidateBillingButton" TYPE="button" class="ui-corner-all">Validate Billing Elligble</button>
                    <BR />
                    <button id="GetTZ" TYPE="button" class="ui-corner-all">Get Time zone Info</button>
                    <BR />  
                    <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                    
                        
        <div id="loadingDlgTestServices" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</cfoutput>
</div>

</body>
</html>