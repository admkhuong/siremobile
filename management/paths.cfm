<cfsetting showdebugoutput="no">

	<cfif cgi.server_port_secure eq '1'>
         <cfset LocalProtocol="https">
     <cfelse>
          <cfset LocalProtocol="http">
     </cfif>	
	
	<cfset LocalPort = "">
<cfset stepUndoRedo = 30 >
<cfset chartReportingType = 3> <!---1 for fusion chart, 2 for highchart,3 for amChart--->

<cfset serverSocket = "#LocalProtocol#://localhost">
<cfset serverSocketPort = "2014">
<cfset serverSocketOn = 1>
<cfset clientTimeEndSession = 5400>

<!--- setup max time record (second) --->
<cfset emsMaxRecordDuration = 180>

<!--- Default branding file--->
<cfset BrandingFile = "branding/ebmbrand.cfm">

<!--- Default URLS --->
<cfset BrandingAbout = "info_about">
<cfset BrandingCareer = "info_career">
<cfset BrandingCompliance = "info_compliance">
<cfset BrandingTerms = "info_terms">
<cfset BrandingPrivacy = "info_privacy">
<cfset BrandingPrivacy = "hiw_pricing">

<cfset BrandingLOLink = "" />


<!---<cfset BrandingFile = "branding/mbbrand.cfm">--->

<!--- Paypal REST Integration --->
<cfset PayPalRESTURL = "https://api.sandbox.paypal.com" />
<cfset PayPalClientId = "" />
<cfset PayPalSecretKey = "" /> 
<cfset EBMAPIPath = "" /> 


<!--- Email Whitelisting service defaults --->
<cfset DefaultgetUserEmailSubaccountInfo = StructNew() />
<cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "" />
<cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "" />    
    
    <cfset BrandingFile = "branding/sirebrand.cfm">
    <cfset BrandingAbout = "sireabout.cfm">
     	   
    <cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">
    
    <cfset AAUDomain = "#LocalProtocol#://ebmii.messagebroadcast.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">
	
    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="Session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->
	
    <cfset DBSourceEBM = "Bishop"/> 
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/> 
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/> 
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    <cfparam name="Session.ManagementCFCPath" default="Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="Session.Rules.cfc">
	
	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/> 
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/> 
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/> 
	<cfparam name="SITE_VERIFY_URL" default="https://www.google.com/recaptcha/api/siteverify"/>
    <!---
	<cfparam name="PRIVATEKEY" default="6LekKwATAAAAADUMEvLa6dulNpxdXvjo_Tahd9Yq"/> 
	<cfparam name="PUBLICKEY" default="6LekKwATAAAAAEijqjiEMRIZGG9h6O8NooygPzcy"/>
    --->
    <cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/> 
    <cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>

    <cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>
	
    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "Public.monkehTweet.com.coldfumonkeh.monkehTweet" />	

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-14"/>
    
    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

	<!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />
    	
	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/EBMMESS'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F'>
	<cfset APPLICATION.Facebook_AppID = '413297832034676'>
	<cfset APPLICATION.Facebook_AppSecret = '55b461c58e5f647e5ccc294c99c7fc4c'>
    
    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />  
        
    <cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />
	            
    <!--- Path Structure to files 
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->
    
    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "\\10.11.0.80\SimpleXScripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">
    
    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">
    
    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe"> 
    <cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" /> 
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">
	
    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >
    
    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\EBMWebsites\\Log">
    
    <!--- application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";--->    
    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";
    
        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)
        
        
        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";
        
        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {
            
        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>
    
 	<!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "#GetTempDirectory()#">
	<cfset PhoneListDBLocalWritePath = "/var/lib/mysql/upload"> <!--- no trailing slash - no blanks - The SFTP command will scp the file here on upload --->
	<cfset CppPdfExportWritePath = "C://CppPdfExport">
    
    <!--- Linux DB SFTP site for bulk insert files to DB --->
	<cfset DBSFTPHostName = "10.25.0.200">  <!--- Needs to be DNS for failover --->
	<cfset DBSFTPUserName = "ebmsshremote">
    <cfset DBSFTPPassword = "mbW1110!S"> 
    <!--- To retrieve from console of logged in session ->  ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub--->
    <cfset DBSFTPFingerPrint = "89:a8:2d:11:42:3b:1f:9e:59:f5:7e:27:0b:19:6a:bc">  <!--- Needs to be copied to all failover DBs by Network Admins --->
    <cfset DBSFTPOutDir = "/var/lib/mysql/upload/">  <!--- must end in / if not blank - The SFTP command will scp the file here on upload - The SFTP path is relative to the SFTP login and is usually differnt then the DB Local path used for LOAD FILE --->
  
	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "C:/Temp_script/tts">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://dev.ebmui.com/cpp'>
    <cfset CppRedirectDomainPublic = '#LocalProtocol#://siremobile.com/cpp'>
	
	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">
     
    <cfset serverSocket = "http://localhost">
	<cfset serverSocketPort = "2014">
    <cfset serverSocketOn = 0>
    
    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />
    
    <cfset PayPalRESTURL = "https://api.paypal.com" />
    <cfset PayPalClientId = "AQYk3xAFH-CjllG-uKGvsqDyMuWAe95ZBfRwF7KBC1b0TshyoxRE7hRTsBxz" />
    <cfset PayPalSecretKey = "EGyajhCW4B6_8Pr-GtObck92S5POyHAQop_EKlz2q785txuR0sZLe2Vh1u_Q" /> 	    
    
    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" /> <!--- <cfset EBMAPIPath = "ebmapi.com" /> ---> 
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" /> 
    
    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "ebm02" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "6QMx9GqWSLY" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "smtp.sendgrid.net" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "587" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "1" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "0" />
	
    
    
    <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME##LocalPort#">
	
    