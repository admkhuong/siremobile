<cfparam name="CurrTotalConnectTime_int" default="0">
<cfparam name="CurrRateType" default="1">
<cfparam name="CurrRate1" default="0.100">
<cfparam name="CurrRate2" default="0.100">
<cfparam name="CurrRate3" default="0.100">
<cfparam name="CurrIncrement1" default="30">
<cfparam name="CurrIncrement2" default="30">
<cfparam name="CurrIncrement3" default="30">
<cfparam name="CurrentBillingAmount" default="0">
<cfparam name="CurrentMessageDeliveredFlag" default="0">
<cfparam name="CurrentMMLS" default="1">
<!--- 
Voice = 1
SMS = 2
eMail = 3
--->
<cfparam name="CurrentMessageType" default="1">


<!--- Calculate Billing--->
<cfswitch expression="#CurrRateType#">

    <!---SimpleX -  Rate 1 at Incrment 1--->
    <cfcase value="1">
    
    	
    	<cfif CurrentMessageType EQ 2>
    		<cfset CurrentBillingAmount = 1 />
    	<cfelseif CurrentMessageType EQ 3>
        	<cfset CurrentBillingAmount = 2 />    	
        <cfelseif CurrTotalConnectTime_int GT 0>
        	<cfset CurrNumberofIncrements = CEILING(CurrTotalConnectTime_int / CurrIncrement1) >    
            <cfset CurrentBillingAmount = LSNumberFormat(CurrNumberofIncrements * 3 )>                     
        <cfelse>
        	<!--- Dont charge for non-connected voice calls --->
	        <cfset CurrentBillingAmount = 0>
        </cfif>
    </cfcase>
    
    <!---SimpleX -  Rate 2 at 1 per unit--->
    <cfcase value="2">
        	<cfset CurrNumberofIncrements = 1 >    
            <cfset CurrentBillingAmount = CurrRate1>                             
    </cfcase>
    
    <!---SimpleX -  Per Message Left - Flat Rate - Rate 1 at 1 per unit - Limit length of recording --->
    <cfcase value="3">
    		<cfif CurrentMessageDeliveredFlag GT 0>    
				<cfset CurrNumberofIncrements = 1 >    
                <cfset CurrentBillingAmount = CurrRate1>
            </cfif>                                 
    </cfcase>
         
    <!---SimpleX -  Per connected call - Rate 1 at Incerment 1 times MMLS specified number of increments - uses MMLS --->
    <cfcase value="4">
    	 <cfif CurrTotalConnectTime_int GT 0>
        	<cfset CurrNumberofIncrements = CurrentMMLS >    
            <cfset CurrentBillingAmount = LSNumberFormat(CurrNumberofIncrements * CurrRate1 , "0.0000")>                     
        <cfelse>
	        <cfset CurrentBillingAmount = 0>
        </cfif>                             
    </cfcase>    
    
	<!---SimpleX -  Per attempt - Rate 1 at Increment 1 - uses MMLS --->
    <cfcase value="5">
        <cfset CurrNumberofIncrements = CurrentMMLS >    
        <cfset CurrentBillingAmount = LSNumberFormat(CurrNumberofIncrements * CurrRate1 , "0.0000")>                     
    </cfcase>
    
    <cfdefaultcase>    
    	<cfset CurrNumberofIncrements = 1 >    
        <cfset CurrentBillingAmount = CurrRate1>  
    </cfdefaultcase>

</cfswitch> 


            