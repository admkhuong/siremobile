<cfsetting RequestTimeout="1200" >
<cfparam name="VerboseDebug" default="1">
<cfparam name = "inpDataSource" default = "Bishop" />
<cfinclude template="../../public/paths.cfm" >

<cfset DBSourceEBM = "#inpDataSource#" />





<!--- Daily tasks to to be run on a schedule --->



<!--- Truncate SMS Abuse Table --->
<cftry>        

	<cfset StartTruncate_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">

    <cfquery name="TruncateDailySMSAbuseTable" datasource="#DBSourceEBM#">
    
    	TRUNCATE TABLE simplexresults.smsmtabusetracking
    
    </cfquery>
    
    <!--- Report Results to Table --->
    <cftry>
        <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.simplexprod_log
            (
                LogType_int,
                CountDistributed_int,
                CountDistributedRegular_int,
                CountDistributedSecurity_int,
                Distribution_dt,
                CountProcessed_int,
                CountSuppressed_int,
                CountDuplicate_int,
                CountInvalid_int,
                CountDialersDistributedTo_int,
                ElligibleDialers_int,
                CountDialersSkippedTooBusy_int,
                START_DT,
                End_dt
            )
            VALUES
            (
                5001,
                1,
                1,
                0,
                NOW(),
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                '#StartTruncate_dt#',
                '#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#'
            )
        </cfquery>
    <cfcatch TYPE="any">
        <cfoutput>
        	<cfif VerboseDebug gt 0><cfdump var="#cfcatch#"></cfif> 
		</cfoutput>
    
    </cfcatch>
    </cftry>


	<cfoutput>
        <cfif VerboseDebug gt 0>TRUNCATE TABLE simplexresults.smsmtabusetracking Completed OK<BR></cfif> 
	</cfoutput>
    
                
<cfcatch type="any">


    <cfset ENA_Message = "">
    <cfset SubjectLine = "EBM - Daily Task - Failed to truncate simplexresults.smsmtabusetracking">
    <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
    <cfset ErrorNumber="5001">
                                      
    <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
        INSERT INTO simplequeue.errorlogs
        (
            ErrorNumber_int,
            Created_dt,
            Subject_vch,
            Message_vch,
            TroubleShootingTips_vch,
            CatchType_vch,
            CatchMessage_vch,
            CatchDetail_vch,
            Host_vch,
            Referer_vch,
            UserAgent_vch,
            Path_vch,
            QueryString_vch
        )
        VALUES
        (
            #ErrorNumber#,
            NOW(),
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.TYPE)#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.MESSAGE)#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(cfcatch.detail)#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERE)#">, 
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
        )
    </cfquery>
                    

</cfcatch>
    
</cftry>    


<cfoutput>
        <cfif VerboseDebug gt 0>Daily tasks Completed OK<BR></cfif> 
	</cfoutput>

