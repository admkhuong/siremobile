<cfparam name="inpMaxExtractAmount" default="5">
<cfparam name="VerboseDebug" default="0">
<cfparam name="ServerId_si" default="0">
<cfparam name="inpDialerIPAddr" default="0.0.0.0">

<cfinclude template="../../public/paths.cfm" >

<cfset DistirbutionCount = 0>

<cfset ENA_sub = "general">

<cfset LastBatchId = -100>

<cfset CurrRate1 = 0.10>
<cfset CurrRateType = 1>
<cfset CurrRate1 = 0.10>
<cfset CurrRate2 = 0.10>
<cfset CurrRate3 = 0.10>                    
<cfset CurrIncrement1 = 30>
<cfset CurrIncrement2 = 30>
<cfset CurrIncrement3 = 30>

<cfoutput>

Start Time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
<cfflush>

<!--- Loop over each dialer in array --->
<cfif inpDialerIPAddr NEQ "0.0.0.0">

	<!--- Main Dialer Try --->
	<cftry>

		<!--- Get results from current dialer --->
		<cfquery name="ExtractFromDialer" datasource="#inpDialerIPAddr#">
			SELECT
				RXCallDetailId_int,
                DTSID_int,
				CallDetailsStatusId_ti,
				BatchId_bi,
				PhoneId_int,
				TotalObjectTime_int,
				TotalCallTimeLiveTransfer_int,
				TotalCallTime_int,
				TotalAnswerTime_int,
				UserSpecifiedLineNumber_si,
				NumberOfHoursToRescheduleRedial_si,
				TransferStatusId_ti,
				IsHangUpDetected_ti,
				IsOptOut_ti,
				IsMaxRedialsReached_ti,
				IsRescheduled_ti,
				CallResult_int,
				SixSecondBilling_int,
				SystemBilling_int,
				RXCDLStartTime_dt,
				CallStartTime_dt,
				CallEndTime_dt,
				CallStartTimeLiveTransfer_dt,
				CallEndTimeLiveTransfer_dt,
				CallResultTS_dt,
				PlayFileStartTime_dt,
				PlayFileEndTime_dt,
				HangUpDetectedTS_dt,
				Created_dt,
				DialerName_vch,
				CurrTS_vch,
				CurrVoice_vch,
				CurrTSLiveTransfer_vch,
				CurrVoiceLiveTransfer_vch,
				CurrCDP_vch,
				CurrCDPLiveTransfer_vch,
				DialString_vch,
				RedialNumber_int,
				TimeZone_ti,
				MessageDelivered_si,
				SingleResponseSurvey_si,
				ReplayTotalCallTime_int,
				XMLResultStr_vch,
				TotalConnectTime_int,
				IsOptIn_ti,
				FileSeqNumber_int,
				UserSpecifiedData_vch,
                XMLControlString_vch,
                DTS_UUID_vch,
                MainMessageLengthSeconds_int                
			FROM
				CallDetail.rxcalldetails
			WHERE			
				<!--- Active Dialers --->
				CallDetailsStatusId_ti = #ESIID#
			ORDER BY
				RXCallDetailId_int
			LIMIT
				#inpMaxExtractAmount#						
		</cfquery>

		<cfif VerboseDebug gt 0 AND inpMaxExtractAmount LT 10>
			 <cfdump var="#ExtractFromDialer#">
		</cfif>

		<cfloop query="ExtractFromDialer">		
           
			<!--- Update CallDetails as Pulling --->
            <cfquery name="UpdateDialerCR" datasource="#inpDialerIPAddr#">	
                UPDATE CallDetail.rxcalldetails SET
                    CallDetailsStatusId_ti = 7
                WHERE
                    RXCallDetailId_int = #ExtractFromDialer.RXCallDetailId_int#
            </cfquery>		
        
            <!--- Get Current Users Rate info--->
        	<cfif LastBatchId NEQ ExtractFromDialer.BatchId_bi>
            	<cfset LastBatchId = ExtractFromDialer.BatchId_bi>
            
            	<!--- Do user lookup--->
                <cfquery name="GetUser" datasource="#Session.DBSourceEBM#">
                	SELECT
                    	UserId_int 
                    FROM
                    	simpleobjects.batch
                    WHERE
                    	BatchId_bi = #ExtractFromDialer.BatchId_bi#
                </cfquery>
                
                <cfif GetUser.RecordCount GT 0>
                    <cfset CurrUser = GetUser.UserId_int>
                <cfelse>
	                <cfset CurrUser = 0>
                </cfif>
                
                <!--- Warning if user id is still 0--->
                
                <!--- Do billing lookup --->            
                <cfquery name="GetUserBillingInfo" datasource="#Session.DBSourceEBM#">
                	SELECT                    
                        RateType_int,
                    	Rate1_int,
                        Rate2_int,
                        Rate3_int,
                        Increment1_int,
                        Increment2_int,
                        Increment3_int,
                        UnlimitedBalance_ti
                    FROM
                    	simplebilling.billing 
                    WHERE
                    	UserId_int = #CurrUser#
                </cfquery>
                
                <cfif GetUserBillingInfo.RecordCount GT 0>
                    <cfset CurrRateType = GetUserBillingInfo.RateType_int>
					<cfset CurrRate1 = GetUserBillingInfo.Rate1_int>
                    <cfset CurrRate2 = GetUserBillingInfo.Rate2_int>
                    <cfset CurrRate3 = GetUserBillingInfo.Rate3_int>                    
                    <cfset CurrIncrement1 = GetUserBillingInfo.Increment1_int>
                    <cfset CurrIncrement2 = GetUserBillingInfo.Increment2_int>
                    <cfset CurrIncrement3 = GetUserBillingInfo.Increment3_int>
                                      
                <cfelse>
	                <cfset CurrRate1 = 0.10>
                    <cfset CurrRateType = 1>
					<cfset CurrRate1 = 0.10>
                    <cfset CurrRate2 = 0.10>
                    <cfset CurrRate3 = 0.10>                    
                    <cfset CurrIncrement1 = 30>
                    <cfset CurrIncrement2 = 30>
                    <cfset CurrIncrement3 = 30>   
                    
                    <cfif VerboseDebug gt 0>Using Default Billing Rates - none found. <BR></cfif>
                                     
                </cfif>
                        
            </cfif>
	                
            <!--- Calculate current cost of call --->			 
            <cfset CurrentBillingAmount = 0>
            
            <!--- Web Service calls are now (as 25.0.0.19) in millisecond increments --->
            <cfif ExtractFromDialer.CallResult_int EQ 76> 
            	<cfset CurrTotalConnectTime_int = ExtractFromDialer.TotalConnectTime_int / 1000>
			<cfelse>
            	<cfset CurrTotalConnectTime_int = ExtractFromDialer.TotalConnectTime_int>
            </cfif>
            
            
            <cfswitch expression="#ExtractFromDialer.CallResult_int#">
            	
                <!--- email --->
                <cfcase value="75">
                	<cfset CurrentMessageType = 2 />
                </cfcase>
                
                <!--- SMS --->
                <cfcase value="76">
                	<cfset CurrentMessageType = 3 />
                </cfcase>
                
                <cfdefaultcase>
                
	                <cfset CurrentMessageType = 1 />
                
                </cfdefaultcase>
            
            </cfswitch>
            

            <cfset CurrentMessageDeliveredFlag = ExtractFromDialer.MessageDelivered_si>
            <cfset CurrentMMLS = ExtractFromDialer.MainMessageLengthSeconds_int>
            
            <cfinclude template="act_CalculateBilling.cfm">
           
           	<cfif VerboseDebug gt 0>
				<cfoutput>
                    CurrRateType = #CurrRateType# <BR>
                    CurrRate1 = #CurrRate1# <BR>
                    CurrRate2 = #CurrRate2# <BR>
                    CurrRate3 = #CurrRate3# <BR>
                    CurrIncrement1 = #CurrIncrement1# <BR>
                    CurrIncrement2 = #CurrIncrement2# <BR>
                    CurrIncrement3 = #CurrIncrement3# <BR>     
                    CurrUser = #CurrUser# <BR>   
                    CurrTotalConnectTime_int = #CurrTotalConnectTime_int# <BR>
                    CurrentBillingAmount = #CurrentBillingAmount# <BR>    
                    CurrentMessageDeliveredFlag = #CurrentMessageDeliveredFlag# <BR>   
                    CurrentMMLS = #CurrentMMLS# <BR>                    
                </cfoutput>
            </cfif>
                     
            <!--- Write to our master log first (if coupled only) --->
            <cfquery name="InsertToCDL" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.contactresults
                    (	
	                    RXCallDetailId_int,
                        DTSID_int,
                        BatchId_bi,
                        PhoneId_int,
                        TotalObjectTime_int,
                        TotalCallTimeLiveTransfer_int,
                        TotalCallTime_int,
                        TotalConnectTime_int,
                        ReplayTotalCallTime_int,
                        TotalAnswerTime_int,
                        UserSpecifiedLineNumber_si,
                        NumberOfHoursToRescheduleRedial_si,
                        TransferStatusId_ti,
                        IsHangUpDetected_ti,
                        IsOptOut_ti,
                        IsMaxRedialsReached_ti,
                        IsRescheduled_ti,
                        CallResult_int,
                        SixSecondBilling_int,
                        SystemBilling_int,
                        RXCDLStartTime_dt,
                        CallStartTime_dt,
                        CallEndTime_dt,
                        CallStartTimeLiveTransfer_dt,
                        CallEndTimeLiveTransfer_dt,
                        CallResultTS_dt,
                        PlayFileStartTime_dt,
                        PlayFileEndTime_dt,
                        HangUpDetectedTS_dt,
                        Created_dt,
                        DialerName_vch,
                        DialerIP_vch,
                        CurrTS_vch,
                        CurrVoice_vch,
                        CurrTSLiveTransfer_vch,
                        CurrVoiceLiveTransfer_vch,
                        CurrCDP_vch,
                        CurrCDPLiveTransfer_vch,
                        ContactString_vch,
                        RedialNumber_int,
                        TimeZone_ti,
                        MessageDelivered_si,
                        SingleResponseSurvey_si,
                        XMLResultStr_vch,
                        IsOptIn_ti,
                        ActualCost_int,
                        DTS_UUID_vch,
                        XMLControlString_vch,
                        UserSpecifiedData_vch,
                        FileSeqNumber_int,
                        MainMessageLengthSeconds_int
                    )
                    VALUES
                    (
                    	#ExtractFromDialer.RXCallDetailId_int#,
                        #ExtractFromDialer.DTSID_int#,
                        #ExtractFromDialer.BatchId_bi#,
                        #ExtractFromDialer.PhoneId_int#,
                        #ExtractFromDialer.TotalObjectTime_int#,
                        #ExtractFromDialer.TotalCallTimeLiveTransfer_int#,
                        #ExtractFromDialer.TotalCallTime_int#,
                        #ExtractFromDialer.TotalConnectTime_int#,
                        #ExtractFromDialer.ReplayTotalCallTime_int#,
                        #ExtractFromDialer.TotalAnswerTime_int#,
                        #ExtractFromDialer.UserSpecifiedLineNumber_si#,
                        #ExtractFromDialer.NumberOfHoursToRescheduleRedial_si#,
                        #ExtractFromDialer.TransferStatusId_ti#,
                        #ExtractFromDialer.IsHangUpDetected_ti#,
                        #ExtractFromDialer.IsOptOut_ti#,
                        #ExtractFromDialer.IsMaxRedialsReached_ti#,
                        #ExtractFromDialer.IsRescheduled_ti#,
                        #ExtractFromDialer.CallResult_int#,
                        #ExtractFromDialer.SixSecondBilling_int#,
                        #ExtractFromDialer.SystemBilling_int#,
                        '#ExtractFromDialer.RXCDLStartTime_dt#',
                        '#ExtractFromDialer.CallStartTime_dt#',
                        '#ExtractFromDialer.CallEndTime_dt#',
                        '#ExtractFromDialer.CallStartTimeLiveTransfer_dt#',
                        '#ExtractFromDialer.CallEndTimeLiveTransfer_dt#',
                        '#ExtractFromDialer.CallResultTS_dt#',
                        '#ExtractFromDialer.PlayFileStartTime_dt#',
                        '#ExtractFromDialer.PlayFileEndTime_dt#',
                        '#ExtractFromDialer.HangUpDetectedTS_dt#',
                        '#ExtractFromDialer.Created_dt#',
                        '#ExtractFromDialer.DialerName_vch#',
                        '#inpDialerIPAddr#',
                        '#ExtractFromDialer.CurrTS_vch#',
                        '#ExtractFromDialer.CurrVoice_vch#',
                        '#ExtractFromDialer.CurrTSLiveTransfer_vch#',
                        '#ExtractFromDialer.CurrVoiceLiveTransfer_vch#',
                        '#ExtractFromDialer.CurrCDP_vch#',
                        '#ExtractFromDialer.CurrCDPLiveTransfer_vch#',
                        '#ExtractFromDialer.DialString_vch#',
                        #ExtractFromDialer.RedialNumber_int#,
                        #ExtractFromDialer.TimeZone_ti#,
                        #ExtractFromDialer.MessageDelivered_si#,
                        #ExtractFromDialer.SingleResponseSurvey_si#,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ExtractFromDialer.XMLResultStr_vch#">,
                        #ExtractFromDialer.IsOptIn_ti#,
                        #CurrentBillingAmount#,
                        '#ExtractFromDialer.DTS_UUID_vch#',
                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ExtractFromDialer.XMLControlString_vch#">,
                         <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ExtractFromDialer.UserSpecifiedData_vch#">,
                         <cfif ExtractFromDialer.FileSeqNumber_int NEQ "">#ExtractFromDialer.FileSeqNumber_int#<cfelse>NULL</cfif>,
                        #ExtractFromDialer.MainMessageLengthSeconds_int#
                        
                    )
            </cfquery>
                
      		<!--- Update contactqueue--->            
            <cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simplequeue.contactqueue
                SET
                    DTSStatusType_ti = 5, <!--- 1 = Queued 2= Queued to go on RXDialer, 3=Moved to RXDialer, 4=Extracted 5=Extracted--->
                    ActualCost_int = #CurrentBillingAmount#
                WHERE
                	ContactString_vch = '#ExtractFromDialer.DialString_vch#'
                AND
                	BatchId_bi = #ExtractFromDialer.BatchId_bi#
                AND 
                	Queued_DialerIP_vch = '#inpDialerIPAddr#'    
                AND
                    DTSStatusType_ti IN (2,3,4)
                AND
                	TypeMask_ti = #CurrentMessageType#    
            </cfquery>     
            
            <!--- Update Billing--->  
            <cfquery name="UpdateBalance" datasource="#Session.DBSourceEBM#">                                                                                                   
                UPDATE
                    simplebilling.billing
                 SET                         	  
                    Balance_int = Balance_int - #CurrentBillingAmount#                            
                WHERE                
                    UserId_int = #CurrUser#                                                                                                                                                           
            </cfquery>  
              
			
			<!--- Update CallDetails as Pulled --->
			<cfquery name="UpdateDialerCR" datasource="#inpDialerIPAddr#">	
				UPDATE CallDetail.rxcalldetails SET
					CallDetailsStatusId_ti = #ABS(ESIID)#
				WHERE
					RXCallDetailId_int = #ExtractFromDialer.RXCallDetailId_int#
			</cfquery>

		</cfloop>			

		Record Count = #ExtractFromDialer.RecordCount# <BR />
		<cfset DistirbutionCount = ExtractFromDialer.RecordCount>
	<cfcatch>
		<cfset ENA_Message = "ERROR on act_Extractor.cfm PAGE File - #ENA_sub# Error. Dialer IP = #inpDialerIPAddr#">
		<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
	</cfcatch>
	</cftry>

	<cfif VerboseDebug gt 0 AND inpMaxExtractAmount LT 10>
		<HR>
	</cfif>
</cfif>

End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
<cfflush>

</cfoutput>