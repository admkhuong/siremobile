
<cftry>
	<cfquery name="InsertToMailTracking" datasource="#Session.DBSourceEBM#">
		INSERT INTO simplexresults.rxmaildetails				
		(
			DTS_UUID_vch,
			BatchId_bi,
			MessageToUser_vch,
            Created_dt,
            LASTUPDATED_DT,
            XMLControlString_vch,
            FileSeqNumber_int,
            UserSpecifiedData_vch
		)
		VALUES
		(
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCurrUUID#">,
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDialString#">,
            NOW(),
            NOW(),
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#FileSeqNumber_int#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserSpecifiedData_vch#">
		)
	</cfquery>
    
    
    <!---
		delimiter $$

CREATE TABLE `rxmaildetails` (
  `DTS_UUID_vch` varchar(36) NOT NULL,
  `CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',
  `DTSID_int` int(11) unsigned DEFAULT NULL,
  `BatchId_bi` int(14) DEFAULT NULL,
  `LinkId_vch` varchar(512) DEFAULT NULL,
  `TotalObjectTime_int` int(11) DEFAULT NULL,
  `TotalCallTime_int` int(11) DEFAULT NULL,
  `SystemBilling_int` int(11) DEFAULT '1',
  `MailResult_int` int(11) DEFAULT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `InitialDeliveryStatus_si` smallint(6) DEFAULT NULL,
  `InitialDelivery_vch` text,
  `FinalDeliveryStatus_si` smallint(6) DEFAULT NULL,
  `FinalDeliveryText_vch` text,
  `UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,
  `TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,
  `IsOptOut_int` int(11) DEFAULT '0',
  `LastOptOut_dt` datetime DEFAULT NULL,
  `IsOptIn_int` int(11) DEFAULT '0',
  `LastOptIn_dt` datetime DEFAULT NULL,
  `ReadCount_int` int(11) DEFAULT '0',
  `LastRead_dt` datetime DEFAULT NULL,
  `IsRescheduled_ti` tinyint(4) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LASTUPDATED_DT` datetime DEFAULT NULL,
  `DialerName_vch` varchar(255) DEFAULT NULL,
  `MessageWarning_vch` text,
  `MessageText_vch` text,
  `MessageHTML_vch` text,
  `ConnFromUser_vch` varchar(2048) DEFAULT NULL,
  `MessageFromUser_vch` varchar(2048) DEFAULT NULL,
  `MessageToUser_vch` varchar(2048) DEFAULT NULL,
  `MessageCCUser_vch` varchar(2048) DEFAULT NULL,
  `MessageSubject_vch` varchar(2048) DEFAULT NULL,
  `SMTPMessageCreationDate_vch` varchar(255) DEFAULT NULL,
  `AttachmentPath_vch` varchar(2048) DEFAULT NULL,
  `SMTPServer_vch` varchar(50) DEFAULT NULL,
  `XMLControlString_vch` text,
  `XMLResultStr_vch` text,
  `FileSeqNumber_int` int(11) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`DTS_UUID_vch`),
  KEY `IDX_MessageToUser_vch` (`MessageToUser_vch`(767)),
  KEY `IDXDTSID_int` (`DTSID_int`),
  KEY `IDXLinkId_vch` (`LinkId_vch`),
  KEY `IDX_CallDetailsStatusId_ti` (`CallDetailsStatusId_ti`),
  KEY `IDX_RXCDLStartTime_dt` (`RXCDLStartTime_dt`),
  KEY `IDX_BatchID_bi` (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$
	
	--->

<cfcatch TYPE="any"><!--- Add number to list --->


	
		<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into eMail tracking log.">
        <cfset ErrorNumber = 108>
		<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
	    
    
</cfcatch>
</cftry> <!--- Add number to list ---> 