<cfsetting RequestTimeout="1800" >


<!--- Run once a minute --->
<!--- *** Critical to monitor for stuck threads   --->

<!--- ***NOTE - Must stop process and wait for all threads to clear before adding or removing threads or duplicate processing will result --->

<cfparam name="inpAmountToProcess" default = "250">
<cfparam name="VerboseDebug" default="1">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">


<cfinclude template="Constants_Processing.cfm" >
<cfinclude template="paths.cfm" >


<!--- Start timing test --->
<cfset StartQueueItUp_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">


 <cfif VerboseDebug gt 0>
 
 	StartQueueItUp_dt = <cfoutput>#StartQueueItUp_dt#</cfoutput>
 	<BR>
 
 </cfif>
 
<!--- Get all currently enabled threads. --->
<cfquery name="getThreads" datasource="#Session.DBSourceEBM#">
	SELECT	
    	ThreadId_int,
    	IsRunning_bit
	FROM
    	simpleobjects.threadcontrol_smsqueue
</cfquery>

<!--- This is the number of threads to run. --->
<cfset inpMod = getThreads.RecordCount>

<!--- *** Critical to monitor for stuck threads   --->

<!--- Get threads that aren't running. --->
<cfquery name="getNewThreads" dbtype="query">
	SELECT	
    	ThreadId_int
    FROM
    	getThreads
    WHERE
    	IsRunning_bit = 0
</cfquery>

<!--- Loop to create threads--->
<cfloop query="getNewThreads">
	<!--- Set pageIndex for thread. --->
	<cfset pageIndex = getNewThreads.ThreadId_int - 1>
 
	<!--- Main thread processing here. --->
    <cfthread name="thr#pageIndex#" threadIndex="#pageIndex#" action="run" inpMod=#inpMod# VerboseDebug=#VerboseDebug# DoModValue=#DoModValue# >
    
    	<!--- Update thread so it won't run again --->
    	<cftry>
        	<cfquery datasource="#Session.DBSourceEBM#">
            	UPDATE	
                	simpleobjects.threadcontrol_smsqueue
                SET		
                	IsRunning_bit = 1,
                	LastRan_dt = NOW()
                WHERE	
                	ThreadId_int = #threadIndex+1#
            </cfquery>
        <cfcatch>
        	<cfset ENA_Message ="EBM SMS Queue Processing - Problem Updating (Reserve) ThreadControl #getNewThreads.Threadid_int#.">
            <cfset ErrorNumber = 105>
    		<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
            <cfbreak>
        </cfcatch>
        </cftry>
        
        <!--- Mod remainder for thread control. --->
        <cfset DoModValue = threadIndex>
        
        <cftry>
			<!--- *** Look into upgrade to stored procedure in following template--->
            <cfinclude template="act_processsmsqueue.cfm" />
        <cfcatch>
        	<cfset ENA_Message ="EBM SMS Queue Processing - Problem in act_processsmsqueue ThreadControl #getNewThreads.Threadid_int#.">
            <cfset ErrorNumber = 106>
    		<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
                                     
                <cftry>
                
                    <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            1068,
                            NOW(),
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("act_kickoffthreadsforprocessingsmsqueue.cfm", "'", "''")#',     
                            '#REPLACE(cfcatch.TYPE, "'", "''")#', 
                            '#REPLACE(cfcatch.MESSAGE, "'", "''")#',
                            '#REPLACE(cfcatch.detail, "'", "''")#',
                            '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
                        );
                    </cfquery>
                
                <cfcatch type="any">
                
                </cfcatch>
                
                </cftry>
                
        </cfcatch>
        </cftry>
        
    	<cftry>
			<!--- Release thread. --->
            <cfquery datasource="#Session.DBSourceEBM#">
                UPDATE	
                    simpleobjects.threadcontrol_smsqueue
                SET
                    IsRunning_bit = 0,
                    LastRan_dt = NOW()
                WHERE
                    ThreadId_int = #threadIndex+1#
            </cfquery>
        
        <cfcatch>
        	<cfset ENA_Message ="EBM SMS Queue Processing - Problem Updating (Release) ThreadControl #getNewThreads.Threadid_int#.">
            <cfset ErrorNumber = 106>
    		<cfinclude template="../noc/act_escalationnotificationactionshighpriority.cfm">
            <cfbreak>
        </cfcatch>
        </cftry>
        
    </cfthread> 
</cfloop>







<!--- CFThread only - If you dont time out a http post CF can go nuts and thread is unstoppable short of restarting coldfusion --->


















