<cfsetting RequestTimeout="1800" >
<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpMaxDTSCountsPerDialer" default = "250">
<cfparam name="Session.DBSourceEBM" default="Bishop"/>
<cfparam name="VerboseDebug" default="1">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDistributionProcessId" default="0">


<!---

Yes this is complex, but will spread DTS accross multiple dialer via multiple threads
DTSId_int MOD #DeviceCount*inpMod# = #CurrDialerArrayIndex# + #DeviceCount*DoModValue# 

Y=Number of Devices
A=Device Index - 0 to (MaxDevicess-1)
X=Number of Threads
B=Thread Index - 0 to (MaxThreads-1)

Sample - Let us Assume: 
Y = 3
X = 4

Then

Mod 3*4 = 0 + (3*0)  
Mod 3*4 = 1 + (3*0) 
Mod 3*4 = 2 + (3*0)
Mod 3*4 = 0 + (3*1)
Mod 3*4 = 1 + (3*1)
Mod 3*4 = 2 + (3*1)
Mod 3*4 = 0 + (3*2)
Mod 3*4 = 1 + (3*2)
Mod 3*4 = 2 + (3*2)
Mod 3*4 = 0 + (3*3)
Mod 3*4 = 1 + (3*3)
Mod 3*4 = 2 + (3*3)

--->


<!--- Running Totals - QIU stands for Queue It Up --->
<cfset QIUProcessedRecordCount = 0>
<cfset QIUDistributionCount = 0>
<cfset QIUInvalidTZRecordCount = 0>
<cfset QIUTotalDialersDistributedTo = 0>
<cfset QIUTotalDialersSkippedForTooBusy = 0>
<cfset InvalidRecordCount = 0>

<!--- Defaults in case no records processed --->
<cfset ProcessedRecordCount = 0>
<cfset DistributionCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset ProcessedAudioFilesVerifiedUpToDate = 0>
<cfset ProcessedAudioFilesUpdated = 0>
<cfset ProcessedAudioFilesErrors = 0>
<cfset ProcessedAudioFilesMainFileNotExists = 0>

<cfoutput>

<cfset StartQueueItUp_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">


<cfif VerboseDebug gt 0>StartQueueItUp_dt = #StartQueueItUp_dt#<BR><cfflush></cfif> 


<!--- Push script files to remote dialer --->

<!--- Load call into temp table --->

<!--- Push temp table to remote DTS - limit to schedule limit --->

<!--- Push schedule to remote dialer --->


<!--- Purge old stuff on Dialers that will never run --->

<!--- Clean from Queue calls that are past their stop dates --->


<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->
<!---  --->



<!---<cftry>--->

 <!--- Report Results to Table --->
	<cftry>
		<cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.simplexprod_log
			(
				LogType_int,
				CountDistributed_int,
				CountDistributedRegular_int,
				CountDistributedSecurity_int,
				Distribution_dt,
				CountProcessed_int,
				CountSuppressed_int,
				CountDuplicate_int,
				CountInvalid_int,
				CountDialersDistributedTo_int,
				ElligibleDialers_int,
				CountDialersSkippedTooBusy_int,
				START_DT,
				End_dt
			)
			VALUES
			(
				10,
				0,
				0,
				0,
				NOW(),
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				NOW(),
				NULL
			)
		</cfquery>
	<cfcatch TYPE="any">
		<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
        <cfset ErrorNumber = 105>
		<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
	</cfcatch>
	</cftry>
    
	<cftry>
    
    
	    <cfset DeviceCount = 0 />
        
<!--- Get Devices--->
        <cfquery name="GetDevices" datasource="#Session.DBSourceEBM#" >                            
            SELECT
                IP_Vch,
                DeviceId_int,
                SystemLoad_int
            FROM
                simplexfulfillment.device                                  
            WHERE
                RealTime_int = 0    <!--- Dont overload real time devices with normal traffic --->
            AND
                Enabled_int = 1    
            AND	
            	DistributionProcessIdList_vch LIKE '%(#inpDistributionProcessId#)%'    
            
           <!--- 
		   <cfif ListContains(INPCONTACTTYPELIST, "1") GT 0>
                AND VoiceEnabled_int = 1
            </cfif>  
            
            <cfif ListContains(INPCONTACTTYPELIST, "2") GT 0>
                AND WebEnabled_int = 1
            </cfif>  
            
            <cfif ListContains(INPCONTACTTYPELIST, "3") GT 0>
                AND EmailEnabled_int = 1
            </cfif> 
			            
            <cfif inpScriptRecord GT 0>
                AND ScriptRecordEnabled_int = 1
            </cfif>     
            
			--->
                            
            ORDER BY
                RAND()  <!--- Effectivly randomizes the order of the results so no one device is responsible for the same data ranges --->                       
        </cfquery>        
                       
        <!--- Get Device Count ---> 
        <cfset DeviceCount = GetDevices.RecordCount />
                          
        <!--- Validat there are actual devices --->
        <cfif DeviceCount EQ 0>
             <cfthrow MESSAGE="No devices found. inpDistributionProcessId=#inpDistributionProcessId#" TYPE="Any" detail="Check fulfillment device DB." errorcode="-2">                   
        </cfif>
                      
           
                
<!--- Loop over each batch--->

		<cfset CurrDialerArrayIndex = 0/>
        
		<!--- Loop over each dialer in array --->
		<cfloop query="GetDevices">
		
        	<cfset inpDialerIPAddr = #GetDevices.IP_Vch# />
			<cfset ServerId_si = #GetDevices.DeviceId_int# />
            <cfset SystemLoad = #GetDevices.SystemLoad_int# />
            
            
            <!--- Report Results to Table --->
            <cftry>
                <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.simplexprod_log
                    (
                        LogType_int,
                        CountDistributed_int,
                        CountDistributedRegular_int,
                        CountDistributedSecurity_int,
                        Distribution_dt,
                        CountProcessed_int,
                        CountSuppressed_int,
                        CountDuplicate_int,
                        CountInvalid_int,
                        CountDialersDistributedTo_int,
                        ElligibleDialers_int,
                        CountDialersSkippedTooBusy_int,
                        START_DT,
                        End_dt,
                        DialerIPAddr_vch,
                        ServerId_int,
                        MaxDTSCountsPerDialer_int
                    )
                    VALUES
                    (
                        30,
                        0,
                        0,
                        0,
                        NOW(),
                        0,
                        0,
                        0,
                        0,
                        0,
                        #DeviceCount#,
                        0,
                        NOW(),
                        NULL,
                        '#inpDialerIPAddr#',
                        #ServerId_si#,
                        #inpMaxDTSCountsPerDialer#
                    )
                </cfquery>
            <cfcatch TYPE="any">
                <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
                <cfset ErrorNumber = 105>
                <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
                
                <cfif VerboseDebug GT 0>
                    ENA_Message = #ENA_Message#
                    <BR />
                    ErrorNumber = #ErrorNumber#
                    <BR />
                    <cfdump var="#cfcatch#" />
                </cfif>
        
            </cfcatch>
            </cftry>

            <cfif VerboseDebug gt 0>
                inpDialerIPAddr(#CurrDialerArrayIndex#) = #inpDialerIPAddr# <BR>
                ServerId_si(#CurrDialerArrayIndex#) = #ServerId_si# <BR>
            </cfif>


			<!--- Check dialer load OK --->
            <!--- 31 = PST, Relative dialer (mySQL) --->
            <cfquery name="GetDTSCounts" datasource="#inpDialerIPAddr#" timeout="60">
                SELECT
                    COUNT(*) AS TOTALCOUNT
                FROM
                    DistributedToSend.dts dts JOIN
                    CallControl.ScheduleOptions so ON (so.BatchId_bi = dts.BatchId_bi)
                WHERE
                    dts.DTSStatusType_ti = 1
                    AND so.enabled_ti = 1
                    AND so.STARTHOUR_TI <= (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                    AND so.ENDHOUR_TI > (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                    AND #DateFormat(NOW(),"dddd")#_ti = 1 
                    AND Scheduled_dt < NOW()  
                   	AND NOW() BETWEEN so.Start_dt and so.Stop_dt                 
            </cfquery>
            
            <!--- Update Last Look Count --->
			<cfquery name="UpdateLastLookCount" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simplexfulfillment.device
				SET
					LastLookCount_int = #GetDTSCounts.TOTALCOUNT#,
                    Updated_dt = NOW()
				WHERE
					DeviceId_int = #ServerId_si#
			</cfquery>
    
		    <cfset outErrorOnScheduleUpdate = 0>
        		            
            <!--- Dialer Ready for more --->
            <cfif GetDTSCounts.TOTALCOUNT LT SystemLoad>
			    <cfset CurrDialersSkippedForTooBusy = 0>
            
                <!--- Validate Dialer is still active --->
                <cfif outErrorOnScheduleUpdate EQ 0>
                    <!--- Distribute inpNumberOfDialsToRead number of dials to dialer inpDialerIPAddr --->
                    <cfinclude template="act_TransferQueue.cfm">
                </cfif>
            <cfelse>
                <!--- Dialer too busy this loop --->
                <cfif VerboseDebug GT 0>#inpDialerIPAddr# too busy for more dials: Count = #GetDTSCounts.TOTALCOUNT#. Limit is #SystemLoad#<BR /></cfif>
                <cfset QIUTotalDialersSkippedForTooBusy = QIUTotalDialersSkippedForTooBusy + 1>
                <cfset CurrDialersSkippedForTooBusy = 1>
            </cfif>
                                                
            <cfset QIUTotalDialersDistributedTo = QIUTotalDialersDistributedTo + 1>
            <cfset QIUProcessedRecordCount = QIUProcessedRecordCount + ProcessedRecordCount>
            <cfset QIUDistributionCount = QIUDistributionCount + DistributionCount>
            <cfset QIUInvalidTZRecordCount = QIUInvalidTZRecordCount + InvalidTZRecordCount>

			<!--- Report Results to Table --->
            <cftry>
                <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                    INSERT INTO simplequeue.simplexprod_log
                    (
                        LogType_int,
                        CountDistributed_int,
                        CountDistributedRegular_int,
                        CountDistributedSecurity_int,
                        Distribution_dt,
                        CountProcessed_int,
                        CountSuppressed_int,
                        CountDuplicate_int,
                        CountInvalid_int,
                        CountDialersDistributedTo_int,
                        ElligibleDialers_int,
                        CountDialersSkippedTooBusy_int,
                        START_DT,
                        End_dt,
                        DialerIPAddr_vch,
                        ServerId_int,
                        CountElligibleOnDialer_int,
                        MaxDTSCountsPerDialer_int
                    )
                    VALUES
                    (
                        40,
                        #DistributionCount#,
                        0,
                        0,
                        NOW(),
                        #ProcessedRecordCount#,
                        0,
                        0,
                        0,
                        1,
                        #DeviceCount#,
                        #CurrDialersSkippedForTooBusy#,
                        NULL,
                        NOW(),
                        '#inpDialerIPAddr#',
                        #ServerId_si#,
                        #GetDTSCounts.TOTALCOUNT#,
                        #SystemLoad#
                    )
                </cfquery>
            <cfcatch TYPE="any">
                <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log II.">
                <cfset ErrorNumber = 105>
                <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
            
				<cfif VerboseDebug GT 0>
                    ENA_Message = #ENA_Message#
                    <BR />
                    ErrorNumber = #ErrorNumber#
                    <BR />
                    <cfdump var="#cfcatch#" />                    
                </cfif>
            
            </cfcatch>
        
            </cftry>
            
            <!--- Mod calculation expect this counter to be 0 based --->
            <cfset CurrDialerArrayIndex = CurrDialerArrayIndex + 1 />	
            
		</cfloop>

<!---
	<cfcatch TYPE="any">
    		
        <cfif VerboseDebug GT 0>
        	<cfdump var="#cfcatch#">
        </cfif>
    
		<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem getting Dialer Info.">
        <cfset ErrorNumber = 106>
		<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
	</cfcatch>
	</cftry>

--->

		<!--- Report Results to Email --->
        <cftry>
            <cfif InvalidRecordCount GT 0>
                <cfset ENA_Message ="SimpleX Error Notice. #InvalidRecordCount# errors logged between #LSTimeFormat(StartQueueItUp_dt, 'HH:mm:ss')# and #LSTimeFormat(Now(), 'HH:mm:ss')#.">
                <cfset ErrorNumber = 107>
                <cfinclude template="../NOC/rep_EscalationNotificationActions.cfm">
            </cfif>
    
        <cfcatch TYPE="any">
            <!--- Squash email errors --->
        </cfcatch>
        </cftry>
    
    
    
        
        <!--- Report Results to Table --->
        <cftry>
            <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplequeue.simplexprod_log
                (
                    LogType_int,
                    CountDistributed_int,
                    CountDistributedRegular_int,
                    CountDistributedSecurity_int,
                    Distribution_dt,
                    CountProcessed_int,
                    CountSuppressed_int,
                    CountDuplicate_int,
                    CountInvalid_int,
                    CountDialersDistributedTo_int,
                    ElligibleDialers_int,
                    CountDialersSkippedTooBusy_int,
                    START_DT,
                    End_dt
                )
                VALUES
                (
                    20,
                    #QIUDistributionCount#,
                    0,
                    0,
                    NOW(),
                    #QIUProcessedRecordCount#,
                    0,
                    0,
                    #QIUInvalidTZRecordCount#,
                    #QIUTotalDialersDistributedTo#,
                    #DeviceCount#,
                    #QIUTotalDialersSkippedForTooBusy#,
                    '#StartQueueItUp_dt#',
                    '#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#'
                )
            </cfquery>
        <cfcatch TYPE="any">
            <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log III.">
            <cfset ErrorNumber = 105>
            <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
            
            <cfif VerboseDebug GT 0>
                ENA_Message = #ENA_Message#
                <BR />
                ErrorNumber = #ErrorNumber#
                <BR />
                <cfdump var="#cfcatch#" />
            </cfif>
        
        </cfcatch>
        </cftry>
                
    <cfcatch TYPE="any">
        <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem during outer loop processing.">
        <cfset ErrorNumber = 125>
        <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
        
        <cfif VerboseDebug GT 0>
        	ENA_Message = #ENA_Message#
            <BR />
            ErrorNumber = #ErrorNumber#
            <BR />
            <cfdump var="#cfcatch#" />
        </cfif>
    </cfcatch>
    </cftry>
    
    
        
    <cfif VerboseDebug GT 0>
    
	<!---	<cfset SessionPath = "devjlp/SimpleFramedX/public">
		<cfset LOCALPROTOCOL = "http">
--->

    	<!--- include main jquery and CSS here - paths are based upon application settings --->
		<cfoutput>
            
            <!---<link TYPE="text/css" href="#rootUrl#/#SessionPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" />--->
      <!---      <link TYPE="text/css" href="#rootUrl#/#SessionPath#/css/Aristo/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
            <link TYPE="text/css" href="#rootUrl#/#SessionPath#/css/rxdsmenu.css" rel="stylesheet" /> 
            <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#SessionPath#/css/RXForm.css" />
            
            <script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/jquery-1.4.4.min.js"></script>
            <script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
            
            <script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/jquery.alerts.js"></script>
            <link TYPE="text/css" href="#rootUrl#/#SessionPath#/css/jquery.alerts.css" rel="stylesheet" /> 
        
            <script src="#rootUrl#/#SessionPath#/js/ValidationRegEx.js"></script>
			--->
         
            <!--- script for timout remiders and popups --->
            <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 <!---        
        <script TYPE="text/javascript">
            var CurrSitePath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
        </script>
        --->
        </cfoutput>

  <!---  
		<script>
        
			$(function()
			{	
				   
					<!--- Kill the new dialog --->
					$("#ProcesscontactqueueDiv #Cancel").click( function() 
						{
								$("#loadingDlgProcessQueue").hide();					
								ProcessCallQueueDialog.remove(); 
								return false;
						}); 		
					  
				   
				  
					
					$("#loadingDlgProcessQueue").hide();	
			
				  
			} );
        
        </script>--->
    
	 
        <div id='ProcesscontactqueueDiv' class="RXForm">
        
                <button id="Cancel" TYPE="button">Done</button>
            <!---    
                <div id="loadingDlgProcessQueue" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/devjlp/EBM_DEV/public</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>--->
       
        </div>
    
    
    </cfif>
    
</cfoutput>    
    
    
    
    
    
    
    