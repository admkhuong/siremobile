<cfparam name="inpMaxExtractAmount" default = "5">
<cfparam name="Session.DBSourceEBM" default="Bishop"/>
<cfparam name="VerboseDebug" default="1">
<cfparam name="inpRealTimeFlag" default="0">

<cfoutput>

<cfset StartExtractItOut_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">

<cfif VerboseDebug gt 0>StartExtractItOut_dt = #StartExtractItOut_dt#<BR><cfflush></cfif> 

<!--- Running Totals - QIU stands for Queue It Up --->
<cfset QIUProcessedRecordCount = 0>
<cfset QIUDistributionCount = 0>
<cfset QIUInvalidTZRecordCount = 0>
<cfset QIUTotalDialersDistributedTo = 0>
<cfset QIUTotalDialersSkippedForTooBusy = 0>
<cfset InvalidRecordCount = 0>

<!--- Defaults in case no records processed --->
<cfset ProcessedRecordCount = 0>
<cfset DistributionCount = 0>
<cfset InvalidTZRecordCount = 0>

<cfset ErrorNumber = 105/>

<cftry>

<!--- Loop over each batch--->


        
        <!--- Report Results to Table --->
        <cftry>
            <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplequeue.simplexprod_log
                (
                    LogType_int,
                    Distribution_dt,				
                    ElligibleDialers_int,
                    CountDialersSkippedTooBusy_int,
                    START_DT,
                    End_dt
                )
                VALUES
                (
                    110,
                    NOW(),				
                    0,
                    #QIUTotalDialersSkippedForTooBusy#,
                    NOW(),
                    NULL
                )
            </cfquery>
        <cfcatch TYPE="any">
            <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into extraction log.">
            <cfset ErrorNumber = 105/>
            <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
            
            <cfif VerboseDebug GT 0>
                ENA_Message = #ENA_Message#
                <BR />
                ErrorNumber = #ErrorNumber#
                <BR />
                <cfdump var="#cfcatch#" />
            </cfif>
                
        </cfcatch>
        </cftry>
        

<cfset DeviceCount = 0 />
        
<!--- Get Devices--->
        <cfquery name="GetDevices" datasource="#Session.DBSourceEBM#" >                            
            SELECT
                IP_Vch,
                DeviceId_int,
                SystemLoad_int
            FROM
                simplexfulfillment.device                                  
            WHERE
                RealTime_int = #inpRealTimeFlag#    <!--- Dont overload real time devices with normal traffic --->
            AND
                Enabled_int = 1    
            
           <!--- 
		   <cfif ListContains(INPCONTACTTYPELIST, "1") GT 0>
                AND VoiceEnabled_int = 1
            </cfif>  
            
            <cfif ListContains(INPCONTACTTYPELIST, "2") GT 0>
                AND WebEnabled_int = 1
            </cfif>  
            
            <cfif ListContains(INPCONTACTTYPELIST, "3") GT 0>
                AND EmailEnabled_int = 1
            </cfif> 
			            
            <cfif inpScriptRecord GT 0>
                AND ScriptRecordEnabled_int = 1
            </cfif>     
            
			--->
                            
            ORDER BY
                Position_int ASC, DeviceId_int ASC                       
        </cfquery>        
                       
        <!--- Get Device Count ---> 
        <cfset DeviceCount = GetDevices.RecordCount />
                          
        <!--- Validat there are actual devices --->
        <cfif DeviceCount EQ 0>
             <cfthrow MESSAGE="No realtime devices found." TYPE="Any" detail="Check fulfillment device DB." errorcode="-2">                   
        </cfif>
                      
           
                
<!--- Loop over each batch--->

		<cfset CurrDialerArrayIndex = 0/>
        
		<!--- Loop over each dialer in array --->
		<cfloop query="GetDevices">
		
        	<cfset inpDialerIPAddr = #GetDevices.IP_Vch# />
			<cfset ServerId_si = #GetDevices.DeviceId_int# />
            <cfset SystemLoad = #GetDevices.SystemLoad_int# />

            <cfif VerboseDebug gt 0>
                inpDialerIPAddr(#CurrDialerArrayIndex#) = #inpDialerIPAddr# <BR>
                ServerId_si(#CurrDialerArrayIndex#) = #ServerId_si# <BR>
            </cfif>
            
            <cfinclude template="act_Extractor.cfm">
            
			<cfset QIUTotalDialersDistributedTo = QIUTotalDialersDistributedTo + 1>
            <cfset QIUProcessedRecordCount = QIUProcessedRecordCount + ProcessedRecordCount>
            <cfset QIUDistributionCount = QIUDistributionCount + DistirbutionCount>
            <cfset QIUInvalidTZRecordCount = QIUInvalidTZRecordCount + InvalidTZRecordCount>
                
             	   
			<!--- Mod calculation expect this counter to be 0 based --->
            <cfset CurrDialerArrayIndex = CurrDialerArrayIndex + 1 />	
            
		</cfloop>

	<cfcatch TYPE="any">
		<cfset ENA_Message ="Error - SimpleX Extraction It Up - Problem Extraction Loop.">
        <cfset ErrorNumber = 105/>
		<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
        
		<cfif VerboseDebug GT 0>
            ENA_Message = #ENA_Message#
            <BR />
            ErrorNumber = #ErrorNumber#
            <BR />
            <cfdump var="#cfcatch#" />
        </cfif>
            
	</cfcatch>
	</cftry>

	<!--- Report Results to Email --->
	<cftry>
		<cfif InvalidRecordCount GT 0>
			<cfset ENA_Message ="SimpleX Error Notice. #InvalidRecordCount# errors logged between #LSTimeFormat(StartExtractItOut_dt, 'HH:mm:ss')# and #LSTimeFormat(Now(), 'HH:mm:ss')#.">
			<cfinclude template="../NOC/rep_EscalationNotificationActions.cfm">
		</cfif>

	<cfcatch TYPE="any">
		<!--- Squash email errors --->
	</cfcatch>
	</cftry>
    
    
    
    
    <!--- Report Results to Table --->
	<cftry>
		<cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.simplexprod_log
			(
				LogType_int,
				CountDistributed_int,
				CountDistributedRegular_int,
				CountDistributedSecurity_int,
				Distribution_dt,
				CountProcessed_int,
				CountSuppressed_int,
				CountDuplicate_int,
				CountInvalid_int,
				CountDialersDistributedTo_int,
				ElligibleDialers_int,
				CountDialersSkippedTooBusy_int,
				START_DT,
				End_dt
			)
			VALUES
			(
				120,
				#QIUDistributionCount#,
				0,
				0,
				NOW(),
				#QIUProcessedRecordCount#,
				0,
				0,
				#QIUInvalidTZRecordCount#,
				#QIUTotalDialersDistributedTo#,
				#DeviceCount#,
				#QIUTotalDialersSkippedForTooBusy#,
				'#StartExtractItOut_dt#',
				'#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#'
			)
		</cfquery>
	<cfcatch TYPE="any">
		<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into extraction log.">
        <cfset ErrorNumber = 105/>
		<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
        <cfif VerboseDebug GT 0>
            ENA_Message = #ENA_Message#
            <BR />
            ErrorNumber = #ErrorNumber#
            <BR />
            <cfdump var="#cfcatch#" />
        </cfif>
            
	</cfcatch>
	</cftry>
    
  
  </cfoutput>
    
    <cfif VerboseDebug GT 0>
    
		<cfset DisplayLocalSessionPath = "DEVJLP\SimpleFramedX\public">
		<cfset DisplayLocalSessionPath = "DEVJLP/EBM_DEV/public">
		<cfset LOCALPROTOCOL = "http">
 
        <div id='ProcessExtractCallResultsDiv' class="RXForm">
        
                <button id="Cancel" TYPE="button">Done</button>
                
            <!---    <div id="loadingDlgProcessQueue" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#LocalServerDirPath#/public</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>--->
       
        </div>
    
    
    </cfif>
    
    
    
    
    
    
    
    
    