<CFSETTING EnableCFOutputOnly = "yes">
<cfsetting RequestTimeout="1200" >

<cfinclude template="Constants_Processing.cfm" >
<cfinclude template="paths.cfm" >


<cfparam name="inpAmountToProcess" default = "10">
<cfparam name="VerboseDebug" default="1">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">

<cfparam name="inpDistributionProcessId" default="0">

<cfset ErrorCount = 0>
<cfset LastErrorDetails = ''>


<cfset StartQueueItUp_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">
<cfset SMSQCountProcessed = 0>
<cfset SMSQCountSent = 0>

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  
  
  
<!---<cfset objCommon = CreateObject("component", "cscnopath")>--->

<!--- Report Results to Table --->
<cftry>

    
    <cfoutput>
        <cfif VerboseDebug gt 0>StartQueueItUp_dt = #StartQueueItUp_dt#<BR></cfif> 
	</cfoutput>


    <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
        INSERT INTO simplequeue.simplexprod_log
        (
            LogType_int,
            CountDistributed_int,
            CountDistributedRegular_int,
            CountDistributedSecurity_int,
            Distribution_dt,
            CountProcessed_int,
            CountSuppressed_int,
            CountDuplicate_int,
            CountInvalid_int,
            CountDialersDistributedTo_int,
            ElligibleDialers_int,
            CountDialersSkippedTooBusy_int,
            START_DT,
            End_dt
        )
        VALUES
        (
            #LOG_SMSSTARTPROCESSESING + DoModValue#,
            0,
            0,
            0,
            NOW(),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            NOW(),
            NULL
        )
    </cfquery>
<cfcatch TYPE="any">
    <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
    <cfset ErrorNumber = 105>
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
</cfcatch>
</cftry>


<cftry>


<!---
	Build out as processesing app or send to RXDialer?

--->
    
    <!--- Check batch schedules !!!???  
	
		When process the first time and not eligible - reload and specify batch info in table
	
	--->
         
    <!--- Loop over each batch--->
    
    <cfset tickBegin = GetTickCount() />
    
    <!--- Read from MO Inbound queue--->
    <cfquery name="ProcessSMSQueue" datasource="#DBSourceEBM#">
        SELECT
	        moInboundQueueId_bi,
            ContactString_vch,
            CarrierId_vch,
            ShortCode_vch,
            Keyword_vch,
            TransactionId_vch,
            Time_dt,
            Created_dt,
            Status_ti,
            CustomServiceId1_vch,
            RawData_vch,
            QuestionTimeOutQID_int,
            SessionId_bi
        FROM
            simplequeue.moinboundqueue 
        WHERE
            Status_ti = #SMSQCODE_READYTOPROCESS#
		AND
            Scheduled_dt < NOW()    
        AND
        	moInboundQueueId_bi MOD #inpMod# = #DoModValue#      
        ORDER BY 
              moInboundQueueId_bi ASC 
        LIMIT #inpAmountToProcess#       
    </cfquery>
    
    <cfif VerboseDebug gt 0>
    	<cfoutput>
       		<!--- Calculate final result --->
			<cfset tickEnd = GetTickCount()>
            <cfset testTime = tickEnd - tickBegin>
                                    
	    	-ProcessSMSQueue<BR /><cfdump var="#ProcessSMSQueue#">
            <BR />QueryTime = #testTime#
        </cfoutput>
    </cfif>
    
    
	<!--- Update based on query select --->        
   	<!--- this query runs full table scan - bad news in speed better as loop?
   		<cfif ProcessSMSQueue.RecordCount GT 0>
            <cfquery name="UpdateProcessSMSQueueData" datasource="#DBSourceEBM#">
                UPDATE 
                    simplequeue.moinboundqueue
                SET
                    Status_ti = 99  <!--- 1 = Queued --->
                WHERE
                    moInboundQueueId_bi IN (#ValueList(ProcessSMSQueue.moInboundQueueId_bi,',')#)
            </cfquery>
        </cfif>
	--->
                             
    <cfloop query="ProcessSMSQueue">
    
    	<cfset SMSQCountProcessed = SMSQCountProcessed + 1>
    
	    <cfif VerboseDebug gt 0>
			<cfset tickBegin = GetTickCount() />
        </cfif>
        
        <cfset inpPreferredAggregator = "" />
        <cfswitch expression="#ProcessSMSQueue.CarrierId_vch#" >
                
        	<cfcase value="ATT">
            	<cfset inpPreferredAggregator = "2" />
            </cfcase>	
        
	        <cfdefaultcase>
        		<cfset inpPreferredAggregator = "" />	
    	    </cfdefaultcase>
        
        </cfswitch>        
            
		<!--- Update to processing --->        
        <cfinvoke                    
             component="cscnopath" 
             method="UpdateQueueNextResponseSMSStatus"
             returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                <cfinvokeargument name="inpQueueState" value="#SMSQCODE_INPROCESS#"/>                        
                <!---<cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>--->
        </cfinvoke>     
    
        <!--- Verify update OK - Be paranoid - DB's can crash badly and we *DO NOT* want to blast anyone --->
		<cfif VerboseDebug gt 0>
        	<cfoutput>
            
            	<!--- Calculate final result --->
                <cfset tickEnd = GetTickCount()>
                <cfset testTime = tickEnd - tickBegin>
                    
            	#DBSourceEBM#
                <BR />
                #ProcessSMSQueue.moInboundQueueId_bi#
                <BR />
                #SMSQCODE_INPROCESS#
                <BR />UpdateTime = #testTime#    
           
            	<!---RetValUpdateQueueNextResponseSMSStatus<BR /><cfdump var="#RetValUpdateQueueNextResponseSMSStatus#">--->
            </cfoutput>           
    	</cfif>
    	
        <!--- All is well - then send --->
    	<cfif RetValUpdateQueueNextResponseSMSStatus.RXRESULTCODE GT 0>    
        	
            <cfif VerboseDebug gt 0>
				<cfset tickBegin = GetTickCount() />
            </cfif>
            
            <!--- Process next response --->
            <cfinvoke                    
                 component="cscnopath" 
                 method="ProcessNextResponseSMS"
                 returnvariable="RetValProcessNextResponseSMS">                         
                    <cfinvokeargument name="inpContactString" value="#ProcessSMSQueue.ContactString_vch#"/>
                    <cfinvokeargument name="inpCarrier" value="#ProcessSMSQueue.CarrierId_vch#"/>
                    <cfinvokeargument name="inpShortCode" value="#ProcessSMSQueue.ShortCode_vch#"/>
                    <cfinvokeargument name="inpKeyword" value="#ProcessSMSQueue.Keyword_vch#"/>
                    <cfinvokeargument name="inpTransactionId" value="#ProcessSMSQueue.TransactionId_vch#"/>
                    <cfinvokeargument name="inpPreferredAggregator" value="#inpPreferredAggregator#"/>
                    <cfinvokeargument name="inpTime" value="#ProcessSMSQueue.Time_dt#"/>
                    <cfinvokeargument name="inpServiceId" value="#ProcessSMSQueue.CustomServiceId1_vch#"/>
                    <cfinvokeargument name="inpXMLDATA" value="#ProcessSMSQueue.RawData_vch#"/>
                    <cfinvokeargument name="inpOverRideInterval" value="1"/>  
                    <cfinvokeargument name="inpTimeOutNextQID" value="#ProcessSMSQueue.QuestionTimeOutQID_int#"/>
					<cfinvokeargument name="inpIRESessionId" value="#ProcessSMSQueue.SessionId_bi#"/>
                    <!---<cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>  --->                  
            </cfinvoke>  
            
           
		    <cfif VerboseDebug gt 0>
            	<cfoutput>
             		
                    <!--- Calculate final result --->
                    <cfset tickEnd = GetTickCount()>
                    <cfset testTime = tickEnd - tickBegin>
             
                	RetValProcessNextResponseSMS<BR /><cfdump var="#RetValProcessNextResponseSMS#"> 
                    <BR />testTime = #testTime#    
                               
                </cfoutput>                
            </cfif>    
			 
          
          	<cfset SMSQCountSent = SMSQCountSent + 1>
          
          	<cfif RetValProcessNextResponseSMS.RXRESULTCODE GT 0> 
          
				<!--- Update to processed --->        
                <cfinvoke                    
                     component="cscnopath" 
                     method="UpdateQueueNextResponseSMSStatus"
                     returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                        <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_PROCESSED#"/>                        
                        <!---<cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>--->
                </cfinvoke>  
                
            <cfelse>
            
	            <cfset ErrorCount = ErrorCount + 1>
	
    			<!--- Update to Error- This could be partially processed by this point so do not re-try --->        
                <cfinvoke                    
                     component="cscnopath" 
                     method="UpdateQueueNextResponseSMSStatus"
                     returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                        <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                        <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR_POSSIBLE_PROCESSED#"/>                        
                        <!---<cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>--->
                </cfinvoke>   
                
                <!--- Log errors --->
                <cfset ENA_Message ="SimpleX Error Notice. SMS Queue  Process next response Error - ProcessNextResponseSMS - #RetValProcessNextResponseSMS.MESSAGE# #RetValProcessNextResponseSMS.ERRMESSAGE#">
				<cfset ErrorNumber = ALERTPROCERROR_NEXTRESPONSEFAILED>
                <cfinclude template="../NOC/rep_EscalationNotificationActions.cfm">
        
				<cfset LastErrorDetails = SerializeJSON(RetValProcessNextResponseSMS) />

            </cfif>
        
        <cfelse> <!--- All is well - then send --->
        
			<!--- Update to Error --->        
            <cfinvoke                    
                 component="cscnopath" 
                 method="UpdateQueueNextResponseSMSStatus"
                 returnvariable="RetValUpdateQueueNextResponseSMSStatus">                         
                    <cfinvokeargument name="inpQueueId" value="#ProcessSMSQueue.moInboundQueueId_bi#"/>
                    <cfinvokeargument name="inpQueueState" value="#SMSQCODE_ERROR#"/>                        
                    <!---<cfinvokeargument name="inpDBSourceEBM" value="#DBSourceEBM#"/>--->
            </cfinvoke>          
            
            <cfset ErrorCount = ErrorCount + 1>            
            <cfset LastErrorDetails = SerializeJSON(RetValUpdateQueueNextResponseSMSStatus) />
        
        </cfif> <!--- All is well - then send --->
    
                          
    </cfloop>    
    
    
    <cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
       
       
     <!--- Report Results to Table --->          
    <cfquery name="AddProductionLogEntry" datasource="#Session.DBSourceEBM#">    
        INSERT INTO simplequeue.sire_sms_hold_queue_proc_log
        (        
            LogType_int,
            DistributionProcessId_int,
            Check_dt,
            RunTimeMS_int,
            Start_dt,
            End_dt,
            CountDistributed_int,
            CountInvalid_int,
            CountProcessed_int,
            CountSuppressed_int,
            CountDuplicate_int,
            CountErrors_int,
            inpMod_int,
            DoModValue_int,
            inpAmountToProcess_int            
        )
		VALUES
        (
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="50">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDistributionProcessId#">,
            NOW(),
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TotalProcTime#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerStartTime#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerEndTime#">,
        	<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCountSent#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SMSQCountProcessed#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ErrorCount#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMod#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DoModValue#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAmountToProcess#">
        
        )
    </cfquery>
    
      

<cfcatch TYPE="any">
    
    <cfoutput> 
    
    	#cfcatch.Detail# 
		<BR />
        #cfcatch.Message#
        <BR />
        #cfcatch.Type#
        <br />
        
	
	</cfoutput>  
    
	<cfif VerboseDebug gt 0>
        <cfoutput><cfdump var="#cfcatch#"></cfoutput>  
                      
    </cfif>
        
            
	<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Processing SMS Queue.">
    <cfset ErrorNumber = ALERTPROCERROR_GENERALQUEUEPROCERROR>
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
</cfcatch>
</cftry>

<cftry>
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	102,
	            NOW(),
	            'act_processsmsqueue - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>


<!---

<!--- Report Results to Table --->
<cftry>
	<cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
		INSERT INTO simplequeue.simplexprod_log
		(
			LogType_int,
			CountDistributed_int,
			CountDistributedRegular_int,
			CountDistributedSecurity_int,
			Distribution_dt,
			CountProcessed_int,
			CountSuppressed_int,
			CountDuplicate_int,
			CountInvalid_int,
			CountDialersDistributedTo_int,
			ElligibleDialers_int,
			CountDialersSkippedTooBusy_int,
			START_DT,
			End_dt
		)
		VALUES
		(
			#LOG_SMSENDPROCESSESING + DoModValue#,
			#SMSQCountProcessed#,
			#SMSQCountSent#,
			0,
			NOW(),
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			'#StartQueueItUp_dt#',
			'#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#'
		)
	</cfquery>
<cfcatch TYPE="any">
	<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
	<cfset ErrorNumber = 105>
	<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
</cfcatch>
</cftry>--->
    
    
    