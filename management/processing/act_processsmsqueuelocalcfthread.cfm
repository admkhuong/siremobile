<CFSETTING EnableCFOutputOnly = "yes">
<cfsetting RequestTimeout="1200" >

<cfinclude template="Constants_Processing.cfm" >
<cfinclude template="paths.cfm" >

<cfparam name="inpAmountToProcess" default = "10">
<cfparam name="inpMaxThreadCount" default = "10">
<cfparam name="VerboseDebug" default="1">
<cfparam name="inpMod" default="1">
<cfparam name="inpWebServicePath" default="http://ebmmo.ebm.internal">
<cfparam name="DoModValue" default="0">

<cfset CronNameLocal = "act_processsmsqueuelocalcfthread_#DoModValue#" />
<cfset StartQueueItUp_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">
<cfset SMSQCountProcessed = 0>
<cfset SMSQCountSent = 0>
<cfset SleepCounter = 0 />
<cfset DelayCounter = 0 />
<cfset ToLongToReserveNextThread = 0 />
<cfset VARIABLES.ThreadErrorCount = 0 />

<!--- Sample cron job commands --->
<!---

Config processing server both IIS and CF
http://www.cfwhisperer.com/post.cfm/important-threading-information-for-coldfusion-iis-and-legacy-odbc
 
File should be {drive}:\JRun4\lib\wsconfig\1\jrun_iis6_wildcard.ini File name might be slightly different.
example: C:\ColdFusion9\runtime\lib\wsconfig\1\jrun_iis6_wildcard.ini


Two threads sample - each thread is multithreaded
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=0
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=1

Single thread sample
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=3000&inpMaxThreadCount=50&inpMod=1&DoModValue=0

--->



<!--- Validate not already active --->

<cftry>
    
    <!--- Look at DB for page active or page crash --->    
    <cfquery name="GetCronData" datasource="#DBSourceEBM#">
        SELECT
	        cron_tracking.PKId_int,
            cron_tracking.CronName_vch,
            cron_tracking.LastStart_dt,
            cron_tracking.LastFinish_dt,
            cron_tracking.IsRunning_ti,
            cron_tracking.QuickData1_vch,
            cron_tracking.QuickData2_vch,
            cron_tracking.QuickData3_vch,
            cron_tracking.QuickData4_vch,
            cron_tracking.QucikData5_vch,
            cron_tracking.JSONData_vch
        FROM
            simpleobjects.cron_tracking 
        WHERE
            CronName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#CronNameLocal#"> 		   
    </cfquery>
    
    <!--- Alert if process does not exist in queue --->
	<cfif GetCronData.RecordCount EQ 0>
        <cfthrow message="GetCronData - No Record Found" type="" errorcode="-1">
    </cfif>
	
    <!--- Alert if already active - calculate how long last one active has been running --->
    <cfif GetCronData.IsRunning_ti GT 0>
    
    	<cfif DateCompare(GetCronData.LastStart_dt, DateAdd('n', -15, Now()), "n") EQ 1 >
        	
            <cfif VerboseDebug gt 0>
    			<cfoutput>        
                    <BR />
                    GetCronData.LastStart_dt = #GetCronData.LastStart_dt#
                    <BR />
                    DateAdd('n', -15, Now()) = #DateAdd('n', -15, Now())#
                    <BR />
                    DateCompare(GetCronData.LastStart_dt, DateAdd('n', -15, Now()), "n") = #DateCompare(GetCronData.LastStart_dt, DateAdd('n', -15, Now()), "n")#
                    <BR />                    	
                    <b>Will auto retry in a few minutes based on auto scheduled task</b>
                    <BR />
                </cfoutput>
            </cfif>
            
            <!--- Will auto retry in a few minutes based on schedule --->
            <cfabort />
        
        <cfelse>
    
			<cfif VerboseDebug gt 0>
    			<cfoutput>        
                    <BR />
                    GetCronData.LastStart_dt = #GetCronData.LastStart_dt#
                    <BR />
                    DateAdd('n', -15, Now()) = #DateAdd('n', -15, Now())#
                    <BR />
                    DateCompare(GetCronData.LastStart_dt, DateAdd('n', -15, Now()), "n") = #DateCompare(GetCronData.LastStart_dt, DateAdd('n', -15, Now()), "n")#
                    <BR />
                    <b>Already active - Aborting request</b>
                    <BR />
                </cfoutput>
            </cfif>
                    
	    	<cfthrow message="GetCronData - Already Active" type="" errorcode="-1">
        </cfif>
        
    </cfif>
    
	<!--- flag as active   --->
    <cfquery name="UpdateCronStatus" datasource="#DBSourceEBM#">
        UPDATE
        	simpleobjects.cron_tracking
        SET
            IsRunning_ti = 1,
           	LastStart_dt = NOW()
        WHERE
            CronName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#CronNameLocal#"> 		   
    </cfquery>
    
<cfcatch TYPE="any">
    <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Starting SMS Queue Processing">
    <cfset ErrorNumber = 1> <!--- Get this from new error numbering log --->
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
    <cfabort />
    
</cfcatch>
</cftry>


<!--- Report Start to Log Table --->
<cftry>

    
    <cfoutput>
        <cfif VerboseDebug gt 0>StartQueueItUp_dt = #StartQueueItUp_dt#<BR></cfif> 
	</cfoutput>


    <cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
        INSERT INTO simplequeue.simplexprod_log
        (
            LogType_int,
            CountDistributed_int,
            CountDistributedRegular_int,
            CountDistributedSecurity_int,
            Distribution_dt,
            CountProcessed_int,
            CountSuppressed_int,
            CountDuplicate_int,
            CountInvalid_int,
            CountDialersDistributedTo_int,
            ElligibleDialers_int,
            CountDialersSkippedTooBusy_int,
            START_DT,
            End_dt
        )
        VALUES
        (
            #LOG_SMSSTARTPROCESSESING + DoModValue#,
            0,
            0,
            0,
            NOW(),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            NOW(),
            NULL
        )
    </cfquery>
<cfcatch TYPE="any">
    <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
    <cfset ErrorNumber = 105>
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
</cfcatch>
</cftry>


<!--- Main Processing--->
<cftry>

    <cfset tickBeginPage = GetTickCount() />
    
    <!--- Read from MO Inbound queue--->
    <cfquery name="ProcessSMSQueue" datasource="#DBSourceEBM#">
        SELECT
	        moInboundQueueId_bi,
            ContactString_vch,
            CarrierId_vch,
            ShortCode_vch,
            Keyword_vch,
            TransactionId_vch,
            Time_dt,
            Created_dt,
            Status_ti,
            CustomServiceId1_vch,
            RawData_vch,
            QuestionTimeOutQID_int
        FROM
            simplequeue.moinboundqueue 
        WHERE
            Status_ti = #SMSQCODE_READYTOPROCESS#
		AND
            Scheduled_dt < NOW()    
        AND
        	moInboundQueueId_bi MOD #inpMod# = #DoModValue#      
        ORDER BY 
              moInboundQueueId_bi ASC 
        LIMIT #inpAmountToProcess#       
    </cfquery>
    
    <cfif VerboseDebug gt 0>
    	<cfoutput>
       		<!--- Calculate final result --->
			<cfset tickEnd = GetTickCount()>
            <cfset testTime = tickEnd - tickBeginPage>
                                    
	    	-ProcessSMSQueue<BR /><cfdump var="#ProcessSMSQueue#">
            <BR />QueryTime = #testTime#
        </cfoutput>
    </cfif>
    
    <cfset LocalPageThreadList = "">
    
        
    <cfloop from="1" to="#inpMaxThreadCount#" step="1" index="CurrThreadId">
    	<cfthread name="thrlcf#CurrThreadId#" action="run">
        	<!--- Establishes thread objects for checking state --->
            
        </cfthread>   
        
       	<cfset LocalPageThreadList = listAppend(LocalPageThreadList, "thrlcf#CurrThreadId#")>
    
    </cfloop>
                   
                   
    <cfset tickBeginLoop = GetTickCount() />
    <cfset tickLastThreadStart = GetTickCount() />
    
    <cfset ToLongToReserveNextThread = "0" />                            
    <cfloop query="ProcessSMSQueue">
    
    	<cfset SMSQCountProcessed = SMSQCountProcessed + 1>
        
        
        <!--- Reserve thread or sleep --->
        <!--- Alarm if it takes too long to reserve --->
        <!--- *** TODO DO This!!!! Reserve in DB? Lock at application layer in case multiple starts? --->
    
    
    	<!--- Wait for a thread to open up - once open then --->
    	<!--- http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-6f29.html --->
    	<!--- Look for open thread and then use it or sleep and then loop --->
    	
        <cfset NextThreadToUse = "" />
       
       	<!--- Exit out of outer loop if this alert has been triggered --->
       	<cfif ToLongToReserveNextThread GT 0 >
        	<cfbreak />
        </cfif>
        
        <!--- Look for too many thread errors. Log, Alert, and force Loop exit if too many errors exist--->
        <cflock scope="REQUEST" timeout="20" type="Exclusive"> 
			<cfif VARIABLES.ThreadErrorCount GT inpMaxThreadCount >
            
            
            <cfset ENA_Message ="SimpleX MO Queue Processing - Problem Processing SMS Queue - Too many thread errors">
    			<cfset ErrorNumber = "10006">
    			<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
    			<cftry>
                        
                    <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("#CronNameLocal#", "'", "''")#',     
                            '#REPLACE("", "'", "''")#', 
                            '#REPLACE("", "'", "''")#',
                            '#REPLACE("", "'", "''")#',
                            '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
                        );
                    </cfquery>
                
                <cfcatch type="any">
                
                </cfcatch>
                
                </cftry>
                        
                <!--- Break out of main loop --->        
				<cfbreak />
            
            </cfif>             
        </cflock>
                        
        <!--- Loop looking for a thread --->
        <cfloop condition="#LEN(NextThreadToUse)# EQ 0 AND ToLongToReserveNextThread EQ 0">
        
        	<!--- Loop over each active thread looking for one that is finished --->
	        <cfloop index = "CurrThreadIdStatusCheck" list = "#LocalPageThreadList#"> 
            
            	<cfif cfthread["#CurrThreadIdStatusCheck#"].Status EQ "COMPLETED" OR  cfthread["#CurrThreadIdStatusCheck#"].Status EQ "TERMINATED">
                	
                    <cfif VerboseDebug gt 0>
						<cfoutput>
                            List before "#LocalPageThreadList#"
                            <BR />
                        </cfoutput>
                    </cfif>
					
					<!--- Remove Old Thread From List --->                    
                    <cfset LocalPageThreadList = ListDeleteAt( LocalPageThreadList, ListFind(LocalPageThreadList,CurrThreadIdStatusCheck)) />
					
                    <!--- Each thread is uniquly named - uses moInboundQueueId_bi so it should not be possible too threads are processing the same moInboundQueueId_bi--->
					<cfset NextThreadToUse = "thrlcf#ProcessSMSQueue.moInboundQueueId_bi#" />
                    
                    <!--- Add new thread to list --->
                    <cfset LocalPageThreadList = listAppend(LocalPageThreadList, NextThreadToUse)>
                    
                    <cfif VerboseDebug gt 0>
						<cfoutput>
                            List After "#LocalPageThreadList#"
                            <BR />
                        </cfoutput>
                    
						<cfoutput>
                            #cfthread["#CurrThreadIdStatusCheck#"].name#
                            <BR />
                            #cfthread["#CurrThreadIdStatusCheck#"].Elapsedtime#
                            <BR />
                            #cfthread["#CurrThreadIdStatusCheck#"].Starttime#
                            <BR />
                            #cfthread["#CurrThreadIdStatusCheck#"].output#
                            <BR />
                        </cfoutput>
                    
                    </cfif>
                    
                    <!--- If a thread is found stop looping over running threads looking for one --->
                    <cfbreak />
                
                </cfif>                
                       
            </cfloop>
            
        	<!--- Check for excessive queue times  --->
            
            <cfset tickEnd = GetTickCount()>
            <cfset testTime = tickEnd - tickLastThreadStart>
            
            <cfif testTime GT 9000>            
            	<!--- Send alarm if couter gets too high ??? --->
            	<!--- *** TODO - break out of loop(s) --->
               <cfset DelayCounter = DelayCounter + 1 />
            </cfif>
            
            <!--- If it takes too long to reserve a thread - Log, Alert, and exit loop(s)--->
            <cfif testTime GT 60000>
            
            	<!--- Send alarm --- Change to 60000--->
            	<!--- *** TODO - break out of loop(s) --->                
               
				<cfset ToLongToReserveNextThread = 60000> 
               
               	<cfset ENA_Message ="SimpleX MO Queue Processing - Problem Processing SMS Queue - Too long to reserve thread">
    			<cfset ErrorNumber = "10005">
    			<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
    			<cftry>
                        
                    <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                        INSERT INTO simplequeue.errorlogs
                        (
                            ErrorNumber_int,
                            Created_dt,
                            Subject_vch,
                            Message_vch,
                            TroubleShootingTips_vch,
                            CatchType_vch,
                            CatchMessage_vch,
                            CatchDetail_vch,
                            Host_vch,
                            Referer_vch,
                            UserAgent_vch,
                            Path_vch,
                            QueryString_vch
                        )
                        VALUES
                        (
                            #ErrorNumber#,
                            NOW(),
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                            '#REPLACE("#CronNameLocal#", "'", "''")#',     
                            '#REPLACE("", "'", "''")#', 
                            '#REPLACE("", "'", "''")#',
                            '#REPLACE("", "'", "''")#',
                            '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
                            '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
                        );
                    </cfquery>
                
                <cfcatch type="any">
                
                </cfcatch>
                
                </cftry>
                
                <!--- Too many thread errors - time to leave --->        
				<cfbreak />
            
            </cfif>
            
            <!--- Sleep half a second waiting for next thread --->
            <cfif LEN(NextThreadToUse) EQ 0>
            
				<cfscript>
                    sleep(200);			
                </cfscript>            
            
            	<cfset SleepCounter = SleepCounter + 1 />
                
            </cfif>
                        
        </cfloop>
            
    	<cfif NextThreadToUse NEQ "">
    
    		<!--- Track when last thread was started --->
    		<cfset tickLastThreadStart = GetTickCount() />
    
			<!--- Main thread processing here. --->
            <cfthread name="#NextThreadToUse#" action="run"             
                VerboseDebug="#VerboseDebug#" 
                DoModValue="#DoModValue#" 
                TL_ThreadName = "#NextThreadToUse#"
                TL_CarrierId_vch="#ProcessSMSQueue.CarrierId_vch#" 
                TL_moInboundQueueId_bi="#ProcessSMSQueue.moInboundQueueId_bi#" 
                TL_ContactString_vch="#ProcessSMSQueue.ContactString_vch#" 
                TL_ShortCode_vch="#ProcessSMSQueue.ShortCode_vch#" 
                TL_Keyword_vch="#ProcessSMSQueue.Keyword_vch#"
                TL_TransactionId_vch="#ProcessSMSQueue.TransactionId_vch#" 
                TL_Time_dt="#ProcessSMSQueue.Time_dt#"
                TL_CustomServiceId1_vch="#ProcessSMSQueue.CustomServiceId1_vch#" 
                TL_RawData_vch="#ProcessSMSQueue.RawData_vch#" 
                TL_SMSQCODE_INPROCESS="#SMSQCODE_INPROCESS#"
                TL_SMSQCODE_PROCESSED="#SMSQCODE_PROCESSED#"
                TL_SMSQCODE_ERROR_POSSIBLE_PROCESSED="#SMSQCODE_ERROR_POSSIBLE_PROCESSED#"
                TL_SMSQCODE_ERROR="#SMSQCODE_ERROR#"
                TL_QuestionTimeOutQID_int="#ProcessSMSQueue.QuestionTimeOutQID_int#" 
                TL_inpPreferredAggregator=""
                TL_inpWebServicePath="#inpWebServicePath#" 
            >
            
                <cftry>      
                
                    <cfif VerboseDebug gt 0>
                    	<cfoutput>
							<!--- <cfset tickBeginThread = GetTickCount() /> --->
                            
                            <BR />
                            TL_moInboundQueueId_bi = #TL_moInboundQueueId_bi# 
                        </cfoutput>                    
                    </cfif>
            
                    <cfset TL_inpPreferredAggregator = "" />
                    <cfswitch expression="#TL_CarrierId_vch#" >
                            
                        <cfcase value="ATT">
                            <cfset TL_inpPreferredAggregator = "2" />
                        </cfcase>	
                    
                        <cfdefaultcase>
                            <cfset TL_inpPreferredAggregator = "" />	
                        </cfdefaultcase>
                    
                    </cfswitch>    
                    
                    <!--- Call Webservice to fulfill --  ebmui.com  -->  ebmmo.ebm.internal --->
                    <cfhttp url="#TL_inpWebServicePath#/ebmresponse/sms/ebmqueue/it" method="POST" result="returnStruct" timeout="60" throwonerror="yes">
                        <cfhttpparam type="formfield" name="inpCarrierId" value="#TL_CarrierId_vch#">
                        <cfhttpparam type="formfield" name="inpmoInboundQueueId" value="#TL_moInboundQueueId_bi#">
                        <cfhttpparam type="formfield" name="inpContactString" value="#TL_ContactString_vch#">
                        <cfhttpparam type="formfield" name="inpShortCode" value="#TL_ShortCode_vch#">
                        <cfhttpparam type="formfield" name="inpKeyword" value="#TL_Keyword_vch#">
                        <cfhttpparam type="formfield" name="inpTransactionId" value="#TL_TransactionId_vch#">
                        <cfhttpparam type="formfield" name="inpTime" value="#TL_Time_dt#">
                        <cfhttpparam type="formfield" name="inpCustomServiceId1" value="#TL_CustomServiceId1_vch#">
                        <cfhttpparam type="formfield" name="inpRawData" value="#TL_RawData_vch#">
                        <cfhttpparam type="formfield" name="inpQuestionTimeOutQID" value="#TL_QuestionTimeOutQID_int#">
                        <cfhttpparam type="formfield" name="inpPreferredAggregator" value="#TL_inpPreferredAggregator#">
                        <cfhttpparam type="formfield" name="inpQAMode" value="0">                        
                    </cfhttp>
                    
                    <cfif VerboseDebug gt 0>
                        <cfoutput>
                           
                            <BR />
                            returnStruct.Filecontent = #returnStruct.Filecontent#
                          	<BR />
                                                   
                            <!---RetValUpdateQueueNextResponseSMSStatus<BR /><cfdump var="#RetValUpdateQueueNextResponseSMSStatus#">--->
                        </cfoutput>           
                    </cfif>
                                    
                <cfcatch>
                
                	<cftry>
                    
                    	<cflock scope="REQUEST" timeout="20" type="Exclusive"> 
							<cfset VARIABLES.ThreadErrorCount = VARIABLES.ThreadErrorCount + 1> 
                		</cflock>
                
                    <cfcatch>
                    
                    </cfcatch>
                	</cftry>
                	
                    <cfset ENA_Message ="SimpleX MO Queue Processing - Problem in act_processsmsqueue ThreadControl #TL_ThreadName#.">
                    <cfset ErrorNumber = 10007>
                    <cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
                                             
                        <cftry>
                        
                            <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
                                INSERT INTO simplequeue.errorlogs
                                (
                                    ErrorNumber_int,
                                    Created_dt,
                                    Subject_vch,
                                    Message_vch,
                                    TroubleShootingTips_vch,
                                    CatchType_vch,
                                    CatchMessage_vch,
                                    CatchDetail_vch,
                                    Host_vch,
                                    Referer_vch,
                                    UserAgent_vch,
                                    Path_vch,
                                    QueryString_vch
                                )
                                VALUES
                                (
                                    #ErrorNumber#,
                                    NOW(),
                                    '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                                    '#REPLACE("SMS Queue MT Thread Processing Error", "'", "''")#',   
                                    '#REPLACE("act_kickoffthreadsforprocessingsmsqueue.cfm", "'", "''")#',     
                                    '#REPLACE(cfcatch.TYPE, "'", "''")#', 
                                    '#REPLACE(cfcatch.MESSAGE, "'", "''")#',
                                    '#REPLACE(cfcatch.detail, "'", "''")#',
                                    '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
                                    '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
                                    '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
                                    '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
                                    '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
                                );
                            </cfquery>
                        
                        <cfcatch type="any">
                        
                        </cfcatch>
                        
                        </cftry>
                        
                </cfcatch>
                </cftry>            
            
                <!--- Release thread --->
                
             </cfthread>         
		
        <cfelse>
        	
            <!--- Shutting down ... --->
        
        </cfif>
                                  
    </cfloop>    
          
    <cfthread action="join" name="#LocalPageThreadList#" timeout="30000"/>  
        
    <!--- Alarm any stuck threads.... --->
        
    <cfloop index = "CurrThreadIdStatusCheckFinal" list = "#LocalPageThreadList#"> 
            
		<cfif cfthread["#CurrThreadIdStatusCheckFinal#"].Status EQ "COMPLETED" OR  cfthread["#CurrThreadIdStatusCheckFinal#"].Status EQ "TERMINATED">
                        
            <cfif VerboseDebug gt 0>
				<cfoutput>
                    <HR />
                    Thread output
                    <BR />
                    #cfthread["#CurrThreadIdStatusCheckFinal#"].name#
                    <BR />
                    #cfthread["#CurrThreadIdStatusCheckFinal#"].Elapsedtime#
                    <BR />
                    #cfthread["#CurrThreadIdStatusCheckFinal#"].Starttime#
                    <BR />
                    #cfthread["#CurrThreadIdStatusCheckFinal#"].output#
                    <BR />
                        
				</cfoutput>
            </cfif>
        </cfif>                
        
    </cfloop>            
            
    <!--- Flag process as complete --->        

<cfcatch TYPE="any">
    
    <cfoutput> 
    
    	#cfcatch.Detail# 
		<BR />
        #cfcatch.Message#
        <BR />
        #cfcatch.Type#
        <br />
        	
	</cfoutput>  
    
	<cfif VerboseDebug gt 0>
        <cfoutput><cfdump var="#cfcatch#"></cfoutput>  
                      
    </cfif>
                    
	<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Processing SMS Queue.">
    <cfset ErrorNumber = ALERTPROCERROR_GENERALQUEUEPROCERROR>
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
</cfcatch>
</cftry>


<cftry>

	<cfset tickEndPage = GetTickCount()>
    <cfset testTimePage = tickEndPage - tickBeginPage>
        
	<!--- flag as in-active   --->
    <cfquery name="UpdateCronStatus" datasource="#DBSourceEBM#">
        UPDATE
        	simpleobjects.cron_tracking
        SET
            IsRunning_ti = 0,
           	LastFinish_dt = NOW(),            
            QuickData1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#testTimePage#">,
            QuickData2_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#SMSQCountProcessed#">,
            QuickData3_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE=""> 
        WHERE
            CronName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Varchar" VALUE="#CronNameLocal#"> 		   
    </cfquery>
    
<cfcatch TYPE="any">
    <cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Starting SMS Queue Processing">
    <cfset ErrorNumber = 1> <!--- Get this from new error numbering log --->
    <cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
    
    <cfabort />
    
</cfcatch>
</cftry>

<cfoutput>
	<BR />
	SleepCounter = #SleepCounter#
	<BR />
    DelayCounter = #DelayCounter#
    <BR />
    <BR />
    <BR />
</cfoutput>

<!--- Report Results to Table --->
<cftry>
	<cfquery name="UpdateFileData" datasource="#Session.DBSourceEBM#">
		INSERT INTO simplequeue.simplexprod_log
		(
			LogType_int,
			CountDistributed_int,
			CountDistributedRegular_int,
			CountDistributedSecurity_int,
			Distribution_dt,
			CountProcessed_int,
			CountSuppressed_int,
			CountDuplicate_int,
			CountInvalid_int,
			CountDialersDistributedTo_int,
			ElligibleDialers_int,
			CountDialersSkippedTooBusy_int,
			START_DT,
			End_dt
		)
		VALUES
		(
			#LOG_SMSENDPROCESSESING + DoModValue#,
			#SMSQCountProcessed#,
			#SMSQCountSent#,
			0,
			NOW(),
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			'#StartQueueItUp_dt#',
			'#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#'
		)
	</cfquery>
<cfcatch TYPE="any">
	<cfset ENA_Message ="Error - SimpleX Queue It Up - Problem Inserting into distribution log.">
	<cfset ErrorNumber = 105>
	<cfinclude template="../NOC/act_EscalationNotificationActionsHighPriority.cfm">
</cfcatch>
</cftry>
    
    
    