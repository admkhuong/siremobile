<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="CurrDialerArrayIndex" default="0">
<cfparam name="DeviceCount" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDistributionProcessId" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_processcontactqueue">
<cfparam name="inpVerboseDebug" default="0">
<cfparam name="inpDisableSend" default="0">

<cfset lockThreadName = inpDialerIPAddr & '_' & inpMod & '_' & DoModValue/>
<cfinclude template="Constants_Processing.cfm" >
<cfinclude template="/session/cfc/csc/constants.cfm">
<cfinclude template="paths.cfm" >

<!---
	Process queued up messages

	Sire version assumptions

	SMS only - no fulfilment device in the middle



	<!--- inpTypeMask 1=Voice 2=email 3=SMS--->


	Run multi threaded with inpMod and DoModValue


	Sample 10 threads

	inpMod = 10

	DoModValue = 0-9
	...	so 10 CRON jobs

	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9






	These are Status Flags to tell what state each entry in the Contact Queue is in.
	0=Paused
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Now being fulfilled
	4 = Blocked due BR rule (duplicates) on distribution
	5 = Complete by Process
	6 = Blocked due BR rule (bad XML) on distribution
	7 = SMS Stop request killed a queued message
	8 = Fulfilled by Real Time request
	100 - Queue hold due to distribution of audio error(s)
	There will be more status types as the system is built out.



--->

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />

<cfif inpVerboseDebug GT 0>
     <cfoutput>
          ServerStartTime = #ServerStartTime# <BR>
     </cfoutput>
</cfif>


<cfset TQCheckPoint = 0>
<cfset ProcessedRecordCount = 0>
<cfset SuppressedCount = 0>
<cfset DuplicateRecordCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset InvalidRecordCount = 0>
<cfset DistributionCount = 0>
<cfset DistributionCountRegular = 0>
<cfset DistributionCountSecurity = 0>
<cfset MaxInvalidRecords = 1>
<cfset ErrorCount = 0>
<cfset LastErrorDetails = ''>
<cfset getLockThread = 0>

<cftry>



         <cflock scope="SERVER" timeout="5" TYPE="exclusive">
             <cfset StructDelete(APPLICATION,lockThreadName)/>
         </cflock>


     <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
          <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
               <cfoutput> already run #lockThreadName#</cfoutput>
               <cfexit>
          <cfelse>
               <cfset APPLICATION['#lockThreadName#'] = 1>
               <cfset getLockThread = 1>
          </cfif>
     </cflock>

     <cfset ExitRecordLoop = 0>

     <!--- Record(s) post --->
     <cfif inpNumberOfDialsToRead GT 0>

          <cfquery name="GetElligibleMessages" datasource="#Session.DBSourceEBM#">
               SELECT
                    dts.DTSId_int,
                    dts.BatchId_bi,
                    dts.DTS_UUID_vch,
                    dts.TypeMask_ti,
                    dts.TimeZone_ti,
                    dts.CurrentRedialCount_ti,
                    dts.UserId_int,
                    dts.PushLibrary_int,
                    dts.PushElement_int,
                    dts.PushScript_int,
                    dts.PushSkip_int,
                    dts.CampaignTypeId_int,
                    dts.ContactString_vch,
                    dts.XMLControlString_vch,
                    dts.ShortCode_vch
               FROM
                    simplequeue.contactqueue dts JOIN
                    simpleobjects.scheduleoptions so ON (so.BatchId_bi = dts.BatchId_bi)
                    <!---FORCE INDEX (IDXComboDistribution)--->
               WHERE
                    dts.DTSStatusType_ti = 1
               AND
                    DTSId_int MOD #inpMod# = #DoModValue#
               AND
                    so.enabled_ti = 1
               AND
                    so.STARTHOUR_TI <= (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
               AND
                    so.ENDHOUR_TI > (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
               AND
                    so.#DateFormat(NOW(),"dddd")#_ti = 1
               AND
                    Scheduled_dt < NOW()
               AND
                    NOW() BETWEEN so.Start_dt and so.Stop_dt
               AND
                    dts.ContactType_int = 3 <!--- SMS is type 3 - only process SMS here --->
               AND
                    DistributionProcessId_int = #inpDistributionProcessId#
               ORDER BY
                    DTSId_int
               LIMIT
                    #inpNumberOfDialsToRead#
          </cfquery>


          <!--- Reset Defaults --->
          <cfset INPBATCHID = "0">
          <cfset inpDialString ="">
          <cfset inpDialString2 ="">
          <cfset TimeZone_ti = "">
          <cfset RedialCount_int = 0>
          <cfset XMLControlString_vch = "">
          <cfset FileSeqNumber_int = "0">
          <cfset UserSpecifiedData_vch = "">
          <cfset inpUserId = 0>
          <cfset inpCampaignTypeId = "30">
          <cfset inpLibId = -1>
          <cfset inpEleId = -1>
          <cfset inpScriptId = -1>
          <cfset inpPushSkip = 0>
          <cfset inpDQDTSId = -1>
          <cfset inpCurrUUID = "">

          <cfloop query="GetElligibleMessages">

               <!--- Reset Defaults --->
               <cfset CurrResult = 0>
               <cfset inpInvalidNumber = 0>


               <!--- Check for bad records TZ, Etc--->

               <cfif inpVerboseDebug GT 0>
                    <cfoutput>
                         inpDialString = #GetElligibleMessages.ContactString_vch# <BR>
                         TimeZone_ti = #GetElligibleMessages.TimeZone_ti# <BR>
                         NOW() = #NOW()# <BR>
                         INPBATCHID = #GetElligibleMessages.BatchId_bi#<BR>
                         inpTypeMask_ti = #GetElligibleMessages.TypeMask_ti#<BR />
                         XMLControlString_vch RAWDB = #HTMLCodeFormat(GetElligibleMessages.XMLControlString_vch)# <BR />
                    </cfoutput>
               </cfif>

               <!--- Ignore Time Zone for eMail and SMS --->

               <cfif GetElligibleMessages.TimeZone_ti LTE 0>
                    <!--- Default --->
                    <cfset GetElligibleMessages.TimeZone_ti = 31>
               </cfif>

               <!--- Replace CK14='UUID' with current queue tables UUID for Email--->

               <!--- UUID --->
               <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISUUID", "#GetElligibleMessages.DTS_UUID_vch#", "ALL")>

               <!--- Contact String --->
               <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISCONTACTSTRING", "#GetElligibleMessages.ContactString_vch#", "ALL")>

               <!--- Batch String --->
               <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISBATCHIDSTRING", "#GetElligibleMessages.BatchId_bi#", "ALL")>

               <!--- Distrubution Queue DTS Id String --->
               <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISDQDTSIDSTRING", "#GetElligibleMessages.DTSId_int#", "ALL")>

               <cfif inpVerboseDebug GT 0>
                    <cfoutput>
                         XMLControlString_vch After = #HTMLCodeFormat(GetElligibleMessages.XMLControlString_vch)# <BR />
                    </cfoutput>
               </cfif>


               <!--- Update Queue to in process (DTSStatusType_ti=2) --->
               <cfset inpDTSId = GetElligibleMessages.DTSId_int />
               <cfset inpDTSStatus = 2 />
               <cfinclude template="act_updatecontactqueuestatus.cfm" />




               <!--- Process Record Here --->
               <cftry>

                    <!--- ***TODO: Deduplication Checks? Validate XML?--->
                    <!---

                    <!--- Another stop on the prevent multiple erroneous contacts from going out --->
                    <cfinclude template="act_PreProductionPushDuplicateCheck.cfm">

                    <!--- Another stop on the prevent multiple erroneous contacts from going out --->
                    <cfinclude template="act_PreProductionPushCheckXMLControlString.cfm">

                    --->

                    <!--- Parse XML Control String --->
                    <!--- Validate DM PT=13 AND Each Q is RXT=19for WebService Call --->

                    <!--- Will need to check Web Service POST vs SMPP Post --->

                    <cfif inpVerboseDebug GT 0>
                         <cfoutput>
                              GetElligibleMessages.XMLControlString_vch = <code>#GetElligibleMessages.XMLControlString_vch#</code> <br />
                         </cfoutput>
                    </cfif>

                    <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>#GetElligibleMessages.XMLControlString_vch#</XMLControlStringDoc>") />

                    <!--- Read for all of the RXSS ELE tags of RXT=12 (Pause) or RXT=19 (Web Service call) --->
                    <cfset AllWebServicdAndDelays = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @RXT='12' or @RXT='19' or @RXT='25' ]") />

                    <cfloop array="#AllWebServicdAndDelays#" index="WSCQ">

                         <cfset CurrRXTType = WSCQ.XmlAttributes["rxt"] />

                         <cfswitch expression="#CurrRXTType#">


                              <!--- Send by Batch to new users on list only  --->
                              <cfcase value="35">

                              </cfcase>

                              <!--- Send by Batch to entire list  --->
                              <cfcase value="30">

                              </cfcase>

                              <!--- Send single by Batch  --->
                              <cfcase value="25">

                                   <cfif inpVerboseDebug GT 0>
                                        <cfoutput>
                                             Send by Batch  <br />
                                        </cfoutput>
                                   </cfif>

                                   <!--- Read the form data from the XML Controlstring --->
                                   <cfset inpFormData = "" />

                                   <cfif structKeyExists( WSCQ.XmlAttributes, "JSON" )>
                                        <cfset inpAPIRequestJSON = TRIM(WSCQ.XmlAttributes["JSON"]) />
                                   <cfelse>
                                        <cfset inpAPIRequestJSON = "" />
                                   </cfif>

                                   <!--- Process next response --->
                                   <!--- component="session.cfc.csc.csc"  --->
                                   <!--- Use cscnopath.cfc to bypass any session related redirects and other issues in real session dir --->
                                   <cfinvoke
                                       method="ProcessNextResponseSMS"
                                       returnvariable="RetValProcessNextResponseSMS"
                                       component="cscnopath" >
                                           <cfinvokeargument name="inpContactString" value="#GetElligibleMessages.ContactString_vch#"/>
                                           <cfinvokeargument name="inpShortCode" value="#GetElligibleMessages.ShortCode_vch#"/>
                                           <cfinvokeargument name="inpKeyword" value=""/>
                                           <cfinvokeargument name="inpBatchId" value="#GetElligibleMessages.BatchId_bi#"/>
                                           <cfinvokeargument name="inpOverrideAggregator" value="0"/>
                                           <cfinvokeargument name="inpAPIRequestJSON" value="#inpAPIRequestJSON#"/>
                                           <cfinvokeargument name="inpDBSourceEBM" value="#Session.DBSourceEBM#"/>
                                   </cfinvoke>

                              </cfcase>

                              <!--- ***TODO: implement this --->
                              <!--- Strategic Pause --->
                              <cfcase value="12">

                                   <cfif inpVerboseDebug GT 0>
                                        <cfoutput>
                                             Strategic Pause <br />
                                        </cfoutput>
                                   </cfif>

                              </cfcase>

                              <!--- Web Service Call  --->

                              <cfcase value="19">
                                   <!--- TEST WITH DELAY WHEN CALL WEB SERVICE --->
                                   <cfif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'sire.lc') >
                                        <cfset sleep(6000)/>
                                   </cfif>

                                   <cfset CurrVerb = WSCQ.XmlAttributes["CK1"] />
                                   <cfset CurrContentType = WSCQ.XmlAttributes["CK2"] />
                                   <cfset CurrPort = WSCQ.XmlAttributes["CK3"] />
                                   <cfset CurrDOM = WSCQ.XmlAttributes["CK6"] />
                                   <cfset CurrDir = WSCQ.XmlAttributes["CK7"] />
                                   <cfset CurrData = WSCQ.XmlAttributes["CK8"] />
                                   <cfset CurrAuth = WSCQ.XmlAttributes["CK9"] />
                                   <cfset CurrIREResultID = WSCQ.XmlAttributes["CK15"] />

                                   <cfif structKeyExists( WSCQ.XmlAttributes, "T64" )>
                                        <cfset CurrT64 = WSCQ.XmlAttributes["T64"] />
                                   <cfelse>
                                        <cfset CurrT64 = "0" />
                                   </cfif>

                                   <!--- If T64 GT 0 then decode CK8 before attempting to send --->
                                   <cfif CurrT64 GT 0>
                                        <cfset CurrData = ToString( ToBinary( CurrData ), "UTF-8" ) />
                                   </cfif>

                                   <cfif Find("&", CurrData) GT 0>
                                        <cfset CurrData = Replace(CurrData, "&amp;","&","ALL") />
                                        <cfset CurrData = Replace(CurrData, "&gt;",">","ALL") />
                                        <cfset CurrData = Replace(CurrData, "&lt;","<","ALL") />
                                        <cfset CurrData = Replace(CurrData, "&apos;","'","ALL") />
                                        <cfset CurrData = Replace(CurrData, "&quot;",'"',"ALL") />
                                        <cfset CurrData = Replace(CurrData, "&##63;","?","ALL") />
                                        <cfset CurrData = Replace(CurrData, "&amp;","&","ALL") />
                                   </cfif>

                                   <!--- Format newlines here--->
                                   <cfset CurrData = Replace(CurrData, "\n", "#chr(10)#", "ALL")>
                                   <cfset CurrDataE = ReplaceNoCase(CurrData, "<br />", "#chr(13)##chr(10)#", "ALL")>

                                   <cfif inpVerboseDebug GT 0>

                                        <cfset CurrQID = WSCQ.XmlAttributes["QID"] />

                                        <cfoutput>
                                             CurrQID = <b>#CurrQID#</b> <BR />
                                             CurrVerb = #CurrVerb# <BR />
                                             CurrContentType = #CurrContentType# <BR />
                                             CurrPort = #CurrPort# <BR />
                                             CurrDOM = #CurrDOM# <BR />
                                             CurrDir = #CurrDir# <BR />
                                             CurrData = <code>#CurrData#</code> <BR />
                                             <HR/>
                                        </cfoutput>
                                   </cfif>

                                   <cfif inpDisableSend EQ 0>

                                        <!--- Track each requests processing tiume to watch for errors on remote end --->
                                        <cfset intHTTPStartTime = GetTickCount() />

                                        <cfhttp url="#CurrDOM#/#CurrDir#" method="#CurrVerb#" resolveurl="no" throwonerror="no" result="RetVarAPIRequest" port="#CurrPort#" timeout="20" redirect="yes">

                                             <cfif CurrContentType EQ "text/html">

                                                  <cfhttpparam type="header" name="Content-Type" value="text/html" />

                                                  <cfhttpparam type="body" value="#TRIM(CurrData)#">

                                             <cfelseif CurrContentType EQ "text/xml" >

                                                  <cfhttpparam type="XML" name="XMLDoc" value="#TRIM(CurrData)#">

                                                  <cfif LEN(CurrAuth) GT 0>
                                                       <cfhttpparam type="header" name="Authorization" value="#CurrAuth#" />
                                                  </cfif>

                                             <cfelseif CurrContentType EQ "application/json">

                                                  <cfhttpparam type="header" name="Content-Type" value="application/json" />

                                                  <cfif LEN(CurrAuth) GT 0>
                                                       <cfhttpparam type="header" name="Authorization" value="#CurrAuth#" />
                                                  </cfif>

                                                  <cfhttpparam type="body" value="#TRIM(CurrData)#">

                                             <!--- Special case way to transfer form data into queue and still be able to store in CK8 key --->
                                             <cfelseif CurrContentType EQ "sire/form">

                                                  <cfhttpparam type="header" name="Content-Type" value="application/x-www-form-urlencoded" />

                                                  <cfif LEN(CurrAuth) GT 0>
                                                       <cfhttpparam type="header" name="Authorization" value="#CurrAuth#" />
                                                  </cfif>

                                                  <cfif isJson(CurrData)>

                                                       <!--- Loop over each top level JSON key and post its value as form data --->
                                                       <cfset CurrJSONData = deserializeJSON(CurrData) />

                                                       <cfloop list="#structKeyList(CurrJSONData)#" index="key">
                                                            <cfhttpparam type="formfield" name="#key#" value="#CurrJSONData[key]#" />
                                                       </cfloop>

                                                  </cfif>

                                             <cfelse>

                                                  <cfhttpparam type="header" name="Content-Type" value="text/plain" />
                                                  <cfhttpparam type="body" value="#TRIM(CurrData)#">

                                             </cfif>

                                        </cfhttp>

                                        <cfset PostResultCode = -1/>

                                        <!--- PDC Initial Response feedback for SMS --->
                                        <cfset PlatformResultOKFlag = 0 />
                                        <cfset PlatformResultMessage = '' />

                                        <cftry>

                                             <!--- Parse POST result code(s)--->
                                             <!--- Get System Result Code 0 is good everything else is an error --->
                                             <!--- Get System Result Code 0 is good everything else is an error --->
                                             <cfscript>
                                                  xmlDataString = URLDecode(RetVarAPIRequest.fileContent);
                                                  xmlDoc = XmlParse(xmlDataString);

                                                  selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/NotificationResultCode");
                                                  if(ArrayLen(selectedElements) GT 0)
                                                  {
                                                       PostResultCode = selectedElements[1].XmlText;
                                                  }

                                                  selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultCode");
                                                  if(ArrayLen(selectedElements) GT 0)
                                                  {
                                                       PostResultCode = selectedElements[1].XmlText;

                                                       if(PostResultCode EQ 0)
                                                       {
                                                            PlatformResultOKFlag = 1;
                                                       }
                                                       else
                                                       {
                                                            PlatformResultOKFlag = 0;
                                                       }
                                                  }
                                                  else
                                                  {
                                                       PlatformResultOKFlag = 0;
                                                  }

                                                  if(PlatformResultOKFlag EQ 0)
                                                  {
                                                       selectedElements = XmlSearch(xmlDoc, "/NotificationRequestResult/NotificationResultList/NotificationResult/SubscriberResult/SubscriberResultText");
                                                       if(ArrayLen(selectedElements) GT 0)
                                                       {
                                                            PlatformResultMessage = selectedElements[1].XmlText;
                                                       }
                                                  }

                                             </cfscript>

                                        <cfcatch>
                                             <cfset PostResultCode = -2 />
                                        </cfcatch>

                                        </cftry>

                                        <!---save batch transaction for get delivery report purpose --->
                                        <cfset inpSMSTrackingOne = 0/>
                                        <cftry>
                                             <cfscript>
                                                  xmlDataString1 = URLDecode(CurrData);
                                                  xmlDoc1 = XmlParse(xmlDataString1);

                                                  selectedElements = XmlSearch(xmlDoc1, "/NotificationRequest/NotificationList");

                                                  if(ArrayLen(selectedElements) GT 0)
                                                  {
                                                       inpSMSTrackingOne = selectedElements[1].XmlAttributes.BatchID;
                                                  }

                                             </cfscript>

                                        <cfcatch>

                                        </cfcatch>

                                        </cftry>

                                        <!--- Log this --->
                                        <cfset intHTTPTotalTime = (GetTickCount() - intHTTPStartTime) />

                                        <!--- Insert the ContactResult Record --->
                                        <cfinvoke component="cscnopath" method="AddContactResult"  returnvariable="RetVarAddContactResult">
                                             <cfinvokeargument name="INPBATCHID" value="#GetElligibleMessages.BatchId_bi#">
                                             <cfinvokeargument name="inpShortCode" value="QueueProcess">
                                             <cfinvokeargument name="inpContactResult" value="76">
                                             <cfinvokeargument name="inpContactString" value="#TRIM(GetElligibleMessages.ContactString_vch)#">
                                             <cfinvokeargument name="inpResultString" value="#RetVarAPIRequest.fileContent#">
                                             <cfinvokeargument name="inpSMSResult" value="#SMSRESULT_MTSENT#">
                                             <cfinvokeargument name="inpIRESESSIONSTATE" value="#IRESESSIONSTATE_NOTASURVEY#">
                                             <cfinvokeargument name="inpXmlControlString" value="#TRIM(CurrData)#">
                                             <cfinvokeargument name="inpSMSSequence" value="1">
                                             <cfinvokeargument name="inpSMSTrackingOne" value="#inpSMSTrackingOne#">
                                             <cfinvokeargument name="inpSMSTrackingTwo" value="0">
                                             <cfinvokeargument name="inpSMSMTPostResultCode" value="#PostResultCode#">
                                             <cfinvokeargument name="inpDTSID" value="#GetElligibleMessages.DTSId_int#">
                                             <cfinvokeargument name="inpControlPoint" value="0">
                                             <cfinvokeargument name="inpConnectTime" value="#intHTTPTotalTime#">
                                        </cfinvoke>


                                        <!--- Update IRE result with result of contactstring data if available from CK15 --->
                                        <cfif CurrIREResultID GT 0>

                                             <cfquery name="UpdateResultsTracking" datasource="#Session.DBSourceEBM#" >
                                                  UPDATE
                                                       simplexresults.ireresults
                                                  SET
                                                       MasterRXCallDetailId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#RetVarAddContactResult.MESSAGEID#">
                                                  WHERE
                                                       IREResultsId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#CurrIREResultID#">
                                             </cfquery>

                                        </cfif>

                                        <cfif inpVerboseDebug GT 0>
                                             <cfoutput>
                                                  intHTTPTotalTime = #intHTTPTotalTime# <br />

                                                  <cfdump var="#RetVarAPIRequest#" />

                                                  <hr />
                                             </cfoutput>
                                        </cfif>

                                   </cfif>

                              </cfcase>

                         </cfswitch>

                    </cfloop>

                    <!--- Update Queue to finished processing (DTSStatusType_ti=5) --->
                    <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                    <cfset inpDTSStatus = 5 />
                    <cfinclude template="act_updatecontactqueuestatus.cfm" />

               <cfcatch type="any">

                    <cfset ErrorCount = ErrorCount + 1>

                    <!--- Log error? --->
                    ********** ERROR on LOOP **********<BR>
                    <cfif inpVerboseDebug GT 0>
                         <cfoutput>
                              <cfdump var="#cfcatch#">
                         </cfoutput>
                    </cfif>

                    <!--- Update Queue to finished processing (DTSStatusType_ti=6) --->
                    <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                    <cfset inpDTSStatus = 6 />
                    <cfinclude template="act_updatecontactqueuestatus.cfm" />

                    <cfset LastErrorDetails = SerializeJSON(cfcatch) />

               </cfcatch>

               </cftry>

               <!--- Increment if TimeZone is bad. --->
               <cfif TimeZone_ti EQ "">
                    <cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>
               </cfif>

               <!--- Increment is duplicate record is found. --->
               <cfif CurrResult eq -2>
                    <cfset DuplicateRecordCount = DuplicateRecordCount + 1>
               </cfif>

               <!--- Increment distribution count. --->
               <cfset DistributionCount = DistributionCount + 1>

               <!--- Increment processed --->
               <cfset ProcessedRecordCount = ProcessedRecordCount + 1>

          </cfloop>

     </cfif>


     <cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
     <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

     <!--- Report Results to Table --->
     <cfquery name="AddProductionLogEntry" datasource="#Session.DBSourceEBM#">
          INSERT INTO simplequeue.sire_contact_queue_proc_log
          (
               LogType_int,
               DistributionProcessId_int,
               Check_dt,
               RunTimeMS_int,
               Start_dt,
               End_dt,
               CountDistributed_int,
               CountInvalid_int,
               CountProcessed_int,
               CountSuppressed_int,
               CountDuplicate_int,
               CountErrors_int,
               inpMod_int,
               DoModValue_int,
               inpNumberOfDialsToRead_int
          )
          VALUES
          (
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="40">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDistributionProcessId#">,
               NOW(),
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TotalProcTime#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerStartTime#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerEndTime#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ProcessedRecordCount#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ErrorCount#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMod#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DoModValue#">,
               <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#">
          )
     </cfquery>


<cfcatch TYPE="any"> <!--- Main Catch --->

	<cfset ErrorCount = ErrorCount + 1>

	********** ERROR on PAGE **********<BR>
    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>

	<cfset ENA_Message = "ERROR on #GetCurrentTemplatePath()# TQCheckPoint = #TQCheckPoint#">
	<cfset InvalidRecordCount = InvalidRecordCount + 1>
	<cfinclude template="..\NOC\act_EscalationNotificationActions.cfm">


</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try --->

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
</cfoutput>

<cftry>

	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_processcontactqueue - LastErrorDetails is Set - See Catch Detail for more info',
	            '',
	            '',
	            '',
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
	        );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>
