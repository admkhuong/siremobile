<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpDialerIPAddr" default="0.0.0.0">
<cfparam name="CurrDialerArrayIndex" default="0">
<cfparam name="DeviceCount" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDistributionProcessId" default="0">

<!---

Yes this is complex, but will spread DTS accross multiple dialer via multiple threads
DTSId_int MOD #DeviceCount*inpMod# = #CurrDialerArrayIndex# + #DeviceCount*DoModValue# 

Y=Number of Devices
A=Device Index - 0 to (MaxDevicess-1)
X=Number of Threads
B=Thread Index - 0 to (MaxThreads-1)



Sample - Let us Assume: 
Y = 3
X = 4

Then

Mod 3*4 = 0 + (3*0)  
Mod 3*4 = 1 + (3*0) 
Mod 3*4 = 2 + (3*0)
Mod 3*4 = 0 + (3*1)
Mod 3*4 = 1 + (3*1)
Mod 3*4 = 2 + (3*1)
Mod 3*4 = 0 + (3*2)
Mod 3*4 = 1 + (3*2)
Mod 3*4 = 2 + (3*2)
Mod 3*4 = 0 + (3*3)
Mod 3*4 = 1 + (3*3)
Mod 3*4 = 2 + (3*3)

--->

<cfset TQCheckPoint = 0>

<cfset ProcessedRecordCount = 0>
<cfset SuppressedCount = 0>
<cfset DuplicateRecordCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset DistributionCount = 0>
<cfset DistributionCountRegular = 0>
<cfset DistributionCountSecurity = 0>
<cfset MaxInvalidRecords = 1>

<cfoutput>

<cfset NameOfTempFileForThisProcess = "BLTemp_SimpleX_#CurrDialerArrayIndex#_#DoModValue#_#inpDistributionProcessId#">


<cftry>

	<cfset ExitRecordLoop = 0>

	<!--- Record(s) post --->
	<cfif inpNumberOfDialsToRead GT 0>
    
    
        <!--- Build Stage Table - Verify no other process is concurrently running. --->
		<!--- Check for existing List_Batch --->
		<cfquery name="CheckForListBatchTemp" datasource="#Session.DBSourceEBM#">
		        SELECT 
                	COUNT(*) AS DoesItExist
        		FROM 
                	Information_schema.tables 
        		WHERE 
        			table_name = '#NameOfTempFileForThisProcess#'
        		AND 
                	table_schema = 'SimpleQueue' 
		</cfquery>
        
		<cfif CheckForListBatchTemp.DoesItExist GT 0>
		        
        	<!--- Existing records? --->
			<cfquery name="CountStageToFileCreate" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(*) AS TOTALCOUNT
				FROM
					 simplequeue.#nameoftempfileforthisprocess# 
			</cfquery>
			
			<cfif CountStageToFileCreate.TOTALCOUNT GT 0>
				<!--- Distribute anything in table from prior to previous crash -- Drops table when finished--->
				<cfset ENA_Message = "Warning on act_DialDistributer.cfm - #NameOfTempFileForThisProcess# table already exists.">
				
                <cfif VerboseDebug gt 0>
					<cfoutput>
                        #ENA_Message#<BR>
                    </cfoutput>
                    <cfflush>
                </cfif>
            
				<!--- Doing nothing seems to work; add logic to prevent perma-lock. --->
				<cfset InvalidRecordCount = InvalidRecordCount + 1>
			<cfelse>
				<cfset ENA_Message = "Warning on act_DialDistributer.cfm - #NameOfTempFileForThisProcess# table already exists but is empty. Removing table.">
				
                <cfif VerboseDebug gt 0>
					<cfoutput>
                        #ENA_Message#<BR>
                    </cfoutput>
                    <cfflush>
                </cfif>
                
				<cfquery name="dropOldEmptyTable" datasource="#Session.DBSourceEBM#">
					DROP TABLE
						simplequeue.#nameoftempfileforthisprocess#
				</cfquery>
			</cfif>
			<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
		</cfif>
			
		<!--- Create One --->
		<cfquery name="CreateNewListBatchTemp" datasource="#Session.DBSourceEBM#" dbname="simplequeue">
        
        	<!---	USE SimpleQueue;--->
                
			<!--- Smaller the better... --->
			CREATE TABLE #NameOfTempFileForThisProcess# 
				(
					DialString1_vch		varchar(255)		NULL,
					DialString2_vch		varchar(255)		NULL,
                    DTS_UUID_vch        varchar(36) DEFAULT NULL,
					TimeZone_ti				integer(11) 	NULL,
                    TypeMask_ti				integer(4)      NULL,
					CurrentRedialCount_ti	integer	(11)	NULL,
					XMLControlString_vch	longtext		NULL,
					FileSeqNumber_int		integer	(11)	NULL,
					UserSpecifiedData_vch	varchar	(1000)	NULL,
					BatchId_bi				integer	(11)	NULL,
					CleanFlag_ti 			tinyint 		NULL,
                    UserId_int        integer	(11)	NULL,
                    PushLibrary_int	  integer	(11)	NULL,
                    PushElement_int   integer	(11)	NULL,
                    PushScript_int    integer	(11)	NULL,  
                    PushSkip_int	  integer	(4)	NULL,  
                    DQDTSId_int       integer	(11)	NULL,     
                    CampaignTypeId_int integer	(4)	NULL,   
                    KEY `IDX_CleanFlag_ti` (`CleanFlag_ti`),
  					KEY `IDX_DialString_RedialCount` (`DialString1_vch`, `CurrentRedialCount_ti`, `UserSpecifiedData_vch`, `FileSeqNumber_int`)
				)	ENGINE=InnoDB <!--- No MyISAM - Key length too long--->
		</cfquery>
	
		<cfif VerboseDebug gt 0>
			<cfoutput>
				Temp Table CREATED - #NameOfTempFileForThisProcess#<BR>
			</cfoutput>
			<cfflush>
		</cfif>
                    
        <cfquery name="GetElligibleCalls" datasource="#Session.DBSourceEBM#">            
                SELECT
                	DTSId_int,
                    BatchId_bi,
                    DTS_UUID_vch,
                    TypeMask_ti,
                    TimeZone_ti,
                    CurrentRedialCount_ti,
                    UserId_int,
                    PushLibrary_int,
                    PushElement_int,
                    PushScript_int,
                    PushSkip_int,
                    CampaignTypeId_int,
                    ContactString_vch,
                    XMLControlString_vch AS QXMLControlString_vch
            	FROM
                	simplequeue.contactqueue FORCE INDEX (IDXComboDistribution)
                WHERE
                	DTSStatusType_ti = 1   
                AND 
                	DTSId_int MOD #DeviceCount*inpMod# = #CurrDialerArrayIndex# + #DeviceCount*DoModValue#                  
                AND
                    Scheduled_dt < NOW()   
                    
                <!--- The IN statement is slowing it down - try without for now --->                         
                <!--- AND TypeMask_ti IN (1,2,3) --->            
                AND
                	DistributionProcessId_int = #inpDistributionProcessId#    
                ORDER BY
                	DTSId_int          
            	LIMIT 
                	#inpNumberOfDialsToRead#            
        </cfquery>
        
        <!--- Reset Defaults --->
		<cfset INPBATCHID = "0">
        <cfset inpDialString ="">
        <cfset inpDialString2 ="">
        <cfset TimeZone_ti = "">
        <cfset RedialCount_int = 0>
        <cfset XMLControlString_vch = "">
        <cfset FileSeqNumber_int = "0">
        <cfset UserSpecifiedData_vch = "">
        <cfset inpUserId = 0>
        <cfset inpCampaignTypeId = "30">
        <cfset inpLibId = -1>
        <cfset inpEleId = -1>
        <cfset inpScriptId = -1>
        <cfset inpPushSkip = 0>
        <cfset inpDQDTSId = -1>
        <cfset inpCurrUUID = "">
        
        
                    
        <cfloop query="GetElligibleCalls">
        
        	<!--- Reset Defaults --->
            <cfset INPBATCHID = "#GetElligibleCalls.BatchId_bi#">
            <cfset inpDialString = "#GetElligibleCalls.ContactString_vch#">
			<cfset inpDialString2 = "#GetElligibleCalls.DTS_UUID_vch#"> <!--- Legacy put UUID here for now - move to own column when legacy system is gone--->
            <cfset inpCurrUUID = "#GetElligibleCalls.DTS_UUID_vch#">
            <cfset inpUserId = "#GetElligibleCalls.UserId_int#">
            <cfset inpTypeMask_ti = "#GetElligibleCalls.TypeMask_ti#">
            <cfset TimeZone_ti = "#GetElligibleCalls.TimeZone_ti#">
            <cfset RedialCount_int = "#GetElligibleCalls.CurrentRedialCount_ti#">
            <cfset XMLControlString_vch = "#GetElligibleCalls.QXMLControlString_vch#">
            <cfset inpCampaignTypeId = "#GetElligibleCalls.CampaignTypeId_int#">
            <cfset FileSeqNumber_int = "0">
            <cfset UserSpecifiedData_vch = "">
            <cfset inpLibId = #GetElligibleCalls.PushLibrary_int#>
    	    <cfset inpEleId = #GetElligibleCalls.PushElement_int#>
	        <cfset inpScriptId = #GetElligibleCalls.PushScript_int#>
            <cfset inpPushSkip = #GetElligibleCalls.PushSkip_int#>
            <cfset inpDQDTSId = #GetElligibleCalls.DTSId_int#>
            
                       
            <cfset CurrResult = 0>
			<cfset inpInvalidNumber = 0>
            
        	<cfset inpBadRecord = 0>
            
        	<!--- Check for bad records TZ, Etc--->
        
        	<!--- Set schedule date based on input parameters (time zone?) --->
            <cfset inpTimeDelay = "0">
			<cfset inpScheduled_dt = DateFormat(DateAdd('n',inpTimeDelay,Now()),'yyyy-mm-dd') & " " & TimeFormat(DateAdd('n',inpTimeDelay,Now()),'HH:mm:ss')>

            <cfif VerboseDebug GT 0>
                Curr Phone Insert Result = #CurrResult# <BR>
                inpDialString = #inpDialString# <BR>
                CurrResult = #CurrResult# <BR>
                TimeZone_ti = #TimeZone_ti# <BR>
                inpScheduled_dt = #inpScheduled_dt# <BR>
                NOW() = #NOW()# <BR>
                INPBATCHID = #INPBATCHID#<BR>
                inpTypeMask_ti = #inpTypeMask_ti#<BR />
                XMLControlString_vch RAWDB = #HTMLCodeFormat(XMLControlString_vch)# <BR />
            </cfif>
                    
        	<!--- Dial if valid record  --->
        	<cfif inpBadRecord EQ 0>
        
        		<cfif inpTypeMask_ti EQ 1>
					<cfif TimeZone_ti GT 0>
                         <cfinclude template="..\Distribution\act_DistributeToStage.cfm">
                    <cfelse>
                        <cfset inpInvalidNumber=1>
                    </cfif>
				<cfelse>
                	<!--- Ignore Time Zone for eMail and SMS --->
                    
                    <cfif TimeZone_ti LTE 0>
                    	<!--- Default --->
                    	<cfset TimeZone_ti = 31>
                    </cfif>
                    
                    <!--- Replace CK14='UUID' with current queue tables UUID for Email--->
                    
                    <!--- UUID --->                                     
					<cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISUUID", "#inpCurrUUID#", "ALL")>
                    
                    <!--- Contact String --->
                    <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISCONTACTSTRING", "#inpDialString#", "ALL")>
                    
                    <!--- Batch String --->
                    <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISBATCHIDSTRING", "#INPBATCHID#", "ALL")>
                    
                    <!--- Distrubution Queue DTS Id String --->
                    <cfset XMLControlString_vch = REPLACE(XMLControlString_vch, "REPLACETHISDQDTSIDSTRING", "#inpDQDTSId#", "ALL")>
                    
                    <cfif VerboseDebug GT 0>
                           XMLControlString_vch After = #HTMLCodeFormat(XMLControlString_vch)# <BR />
                    </cfif>
                    
                    <!--- Insert email tracking by GUID --->
                    <!---<cfinclude template="act_InsertEMailTracking.cfm">--->
                    
                    <!--- inpTypeMask - 1=Voice 2=email 3=SMS--->
                    <cfif inpTypeMask_ti EQ 2>
                    
                    	<!--- Old Logic? Dont need for Send grid ? --->
	                    <!---<cfinclude template="act_InsertEMailTracking.cfm">--->
                    
                    </cfif>
					                    
	                <cfinclude template="..\Distribution\act_DistributeToStage.cfm">
                    
                    
                </cfif>    
                    
		    </cfif>
            
            
            <cfset UpdateStatusOK = 0>
            
            <!--- Squash deadlocking issues? --->
            <cfloop from="1" to="3" step="1" index="DeadlockIndex">
                <cftry>
                
                    <!--- Update Dial Queue Table Status to Queued --->
                    <cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplequeue.contactqueue
                        SET
                            DTSStatusType_ti = 2, <!--- 1 = Queued 2= Queued to go on RXDialer, 3=Now On RXDialer, 4=Extracted--->
                            Queued_DialerIP_vch = '#inpDialerIPAddr#',
                            Queue_dt = NOW() 
                        WHERE
                            DTSId_int = #GetElligibleCalls.DTSId_int#
                    </cfquery>
                    
                    <cfset UpdateStatusOK = 1>
                    
                    <cfbreak>
                    
                    <cfcatch type="any">
                    
                    	<cfif findnocase("Deadlock", cfcatch.detail) GT 0 > 
                        
                        	<!---
							<cfthread
                            	action="sleep"
                                duration="#(2 * 1000)#"
                            />--->                            
                            
							<cfscript> 
                                thread = CreateObject("java","java.lang.Thread"); 
                                thread.sleep(3000); // CF will now sleep for 5 seconds 
                            </cfscript> 
						
                        <cfelse>
                        	
                            <!--- other errors - break from loop--->
                        	<cfbreak>	                        	
                        
                        </cfif>
                    
                    </cfcatch>
                
                </cftry>
            
            </cfloop>
            
            <!--- One more try - non-squashed --->
            <cfif UpdateStatusOK EQ 0>
            
             		<!--- Update Dial Queue Table Status to Queued --->
                    <cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
                        UPDATE 
                            simplequeue.contactqueue
                        SET
                            DTSStatusType_ti = 2, <!--- 1 = Queued 2= Queued to go on RXDialer, 3=Now On RXDialer, 4=Extracted--->
                            Queued_DialerIP_vch = '#inpDialerIPAddr#',
                            Queue_dt = NOW() 
                        WHERE
                            DTSId_int = #GetElligibleCalls.DTSId_int#
                    </cfquery>
            
            </cfif>
                
                
            <!--- Dial if valid record  --->
			<cfif inpBadRecord EQ 0>
				<!---<cfinclude template="..\Distribution\act_DistributeToStage.cfm">--->
			</cfif>
            
            
            <!--- Increment if TimeZone is bad. --->
			<cfif TimeZone_ti EQ "">
				<cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>
			</cfif>

			<!--- Increment is duplicate record is found. --->
			<cfif CurrResult eq -2>
				<cfset DuplicateRecordCount = DuplicateRecordCount + 1>
			</cfif>

			<!--- Increment distribution count. --->
			<cfset DistributionCount = DistributionCount + 1>
            
            <!--- Increment processed --->
			<cfset ProcessedRecordCount = ProcessedRecordCount + 1>
            
        </cfloop>
        
        <cfinclude template="..\Distribution\act_DistributeStageToDialer.cfm">    
    
    </cfif>
    
    

<cfcatch TYPE="any"> <!--- Main Catch --->
	********** ERROR on PAGE **********<BR>
	<!--- Squash any additional cleanup errors --->
	<cftry>
		<cfquery name="GetStageToFileCount" datasource="#Session.DBSourceEBM#">
			  SELECT
				COUNT(*) AS TOTALCOUNT
			  FROM
				 simplequeue.#nameoftempfileforthisprocess#
		</cfquery>

		<cfif GetStageToFileCount.TOTALCOUNT EQ 0>
			<cfquery name="DropStageToFile" datasource="#Session.DBSourceEBM#">
				DROP TABLE simplequeue.#nameoftempfileforthisprocess#
			</cfquery>
		<cfelse>
			<cfinclude template="..\Distribution\act_DistributeStageToDialer.cfm">
		</cfif>
	<cfcatch TYPE="any">
		<!--- Do nothing --->
	</cfcatch>
	</cftry>

	<cfset ENA_Message = "ERROR on #GetCurrentTemplatePath()# TQCheckPoint = #TQCheckPoint#">
	<cfset InvalidRecordCount = InvalidRecordCount + 1>
	<cfinclude template="..\NOC\act_EscalationNotificationActions.cfm">

</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
<cfflush>

</cfoutput>    