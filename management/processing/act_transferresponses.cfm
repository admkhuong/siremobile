<cfparam name="VerboseDebug" default="0">


<cfinclude template="../../public/paths.cfm" >
<cfparam name="Session.DBSourceEBM" default="Bishop"/> 

<cfset MaxCDRID = 0>


<cfoutput>



<cfset StartTransferRecordedResponses_dt = "#LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')#">
<cfif VerboseDebug gt 0>StartTransferRecordedResponses_dt = #StartTransferRecordedResponses_dt#<BR><cfflush></cfif> 

	<!--- Get max record for this loop so concurrent extraction proccessing does not cause problems--->
	<cfquery name="getMax" datasource="#Session.DBSourceEBM#">
        SELECT
       		MAX(MasterRXCallDetailId_int) AS MaxCDRID
        FROM
        	simplexresults.contactresults
    </cfquery>

	<cfset MaxCDRID = getMax.MaxCDRID>

	<!--- Update records we dont need to process --->
	<cfquery name="UpdateIgnore" datasource="#Session.DBSourceEBM#">
    	UPDATE
	        simplexresults.contactresults
        SET
        	RecordedResultPulled_int = 15
        WHERE
        	(
            	XmlControlString_vch NOT LIKE '%RXT=''3''%'
            	OR CallResult_int <> 3
            	OR XMLResultStr_vch = 'NA'
            	OR DialerIP_vch IS NULL
            )
            AND RecordedResultPulled_int = 0
            AND MasterRXCallDetailId_int <= #MaxCDRID#
    </cfquery>
   

	<!--- Search simplexresults.contactresults for files to look for recorded responses--->
    <cfquery name="getRR" datasource="#Session.DBSourceEBM#">
       SELECT
            MasterRXCallDetailId_int
            ,BatchId_bi
            ,DialerIP_vch
            ,DTS_UUID_vch
            ,BatchId_bi
            ,RedialNumber_int
            ,ContactString_vch
            ,XmlResultStr_vch	<!--- ,REPLACE(XmlResultStr_vch,"=' ","='' ") AS XmlResultStr_vch --->    		<!--- Cleanup invalid XML? --->
            ,XmlControlString_vch	<!--- ,REPLACE(XmlControlString_vch,"=' ","='' ") AS XmlControlString_vch	--->	<!--- Cleanup invalid XML? --->
            ,RXCDLStartTime_dt
        FROM
            simplexresults.contactresults
        WHERE
            XmlControlString_vch LIKE '%RXT=''3''%'
            AND CallResult_int = 3
            AND XMLResultStr_vch <> 'NA'
            AND DialerIP_vch IS NOT NULL
            AND RecordedResultPulled_int = 0
            AND MasterRXCallDetailId_int <= #MaxCDRID#
        ORDER BY
        	MasterRXCallDetailId_int ASC    <!---ORDER BY BatchId_bi--->
    </cfquery>


	<cfloop query="getRR">

		<cftry>
        
        <!--- Once audio is pulled and processing is started --->
     	    <cfquery datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplexresults.contactresults
                SET
                    RecordedResultPulled_int = 1
                WHERE
                    MasterRXCallDetailId_int = #getRR.MasterRXCallDetailId_int#
            </cfquery>
                        
                        
            <cfif VerboseDebug gt 0>StartTransferRecordedResponses_dt = #StartTransferRecordedResponses_dt#<BR><cfflush></cfif> 
            
            
                            
        	<cfset MCIDText = "<root>#getRR.XmlControlString_vch#</root>">
            <cfif VerboseDebug gt 0>MCIDText = #HTMLCodeFormat(MCIDText)#<BR><cfflush></cfif> 
                            
			<cfset XmlStr = "<root>#getRR.XMLResultStr_vch#</root>">
            <cfif VerboseDebug gt 0>XmlStr = #HTMLCodeFormat(XmlStr)#<BR><cfflush></cfif> 
                        
            <cfset ELEQid = XMLSearch(MCIDText,"/root/RXSS/ELE[@RXT=3]")>
            
            <cfloop from="1" to="#ArrayLen(ELEQid)#" index="i">
	            <cfset NextTFLId = 0>
				<cfset FinalQID = ELEQid[1].XmlAttributes.QID>
        
                <cfset QIDValue = XMLSearch(XmlStr,"/root/Q[@ID=#FinalQID#]")>
        
                <cfif ArrayLen(QIDValue) GT 0>
                    <cfset FinalQIDValue = QIDValue[i].XmlText>
                    <cfif StructKeyExists(QIDValue[i].XmlAttributes,"ADJHEATIDX")>
                        <cfset AdjHeatIndex = QIDValue[i].XmlAttributes.ADJHEATIDX>
                    <cfelse>
                        <cfset AdjHeatIndex = 0>
                    </cfif>
                    
                    <cfif FinalQIDValue EQ 1>
                        <cfquery name="getUID" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                UserId_int
                            FROM 
                                simplequeue.contactqueue
                            WHERE 
                                DTS_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.DTS_UUID_vch#">
                        </cfquery>
        
                        <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#")>
                            <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserId_int#">
                        </cfif>
                        
                        <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#")>
                            <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#">
                        </cfif>
                        
                        <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#")>
                            <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#">
                        </cfif>
                        
                        
                        <!--- What if fulfillment has already converted to MP3 ???? --->
            
            			<cfset FileOnFulfillmentDevice = "\\#getRR.DialerIP_vch#\RXWaveFiles\RecordedResponses\Results\#getUID.UserId_int#\#getRR.BatchId_bi#\SurveyResults_#FinalQID#_#getRR.ContactString_vch#_#getRR.RedialNumber_int#.wav">	
                        <cfif VerboseDebug gt 0>FileOnFulfillmentDevice = #FileOnFulfillmentDevice#<BR><cfflush></cfif> 
                       
                       
                        <cfquery name="TFLInsert" datasource="#Session.DBSourceEBM#" result="TFLInsertResult" >
                            INSERT INTO
                                simplexrecordedresults.transcriptionfilelog
                            (MasterRXCallDetailId_int, DTS_UUID_vch, BatchId_bi, UserID_int, XMLResultStr_vch, Original_FileName_vch, TransCo_FileName_vch, DialerIP_vch, RXCDLStartTime_dt, AdjustedHeatIndex_int, LastModified_dt)
                            VALUES (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.MASTERRXCALLDETAILID_INT#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.DTS_UUID_vch#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.BatchId_bi#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUID.UserID_int#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.XMLResultStr_vch#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="SurveyResults_#FinalQID#_#getRR.ContactString_vch#_#getRR.RedialNumber_int#.wav">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.MasterRXCallDetailId_int#.wav">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.DialerIP_vch#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getRR.RXCDLStartTime_dt#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AdjHeatIndex#">
                                ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#NOW()#">
                            )
                        </cfquery>
                        
                        <cfset NextTFLId = TFLInsertResult.GENERATED_KEY>
                                                
                        <cfset FileRawInArchive = "#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#_#NextTFLId#.wav">
                        <cfif VerboseDebug gt 0>FileRawInArchive = #FileRawInArchive#<BR><cfflush></cfif> 
                                    
                        <cffile action="copy" source="#FileOnFulfillmentDevice#"
                            destination="#FileRawInArchive#">
                        
                        <!--- Convert to MP3 for less storage and transfer requirements--->
                        <cfexecute name="#SOXAudiopath#" 
                                arguments=" #FileRawInArchive# -C 128.3 #RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#_#NextTFLId#.mp3 loudness"
                                timeout="60"> 
                        </cfexecute>
                    
                        <cffile action="delete"
                            file="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#_#NextTFLId#.wav">
                    
                    </cfif>
                    
                    <!--- Once audio is pulled and processing is complete initially --->
                    <cfquery datasource="#Session.DBSourceEBM#">
                            UPDATE
                                simplexresults.contactresults
                            SET
                                RecordedResultPulled_int = 5
                            WHERE
                                MasterRXCallDetailId_int = #getRR.MasterRXCallDetailId_int#
                  	</cfquery>
            	</cfif>
            </cfloop>
      
        <cfcatch>
            <cfset ENAMessage = "ERROR on act_TransferResponses.cfm - #cfcatch.Message#, #cfcatch.Detail#">
            <cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
            <cfquery datasource="#Session.DBSourceEBM#">
                UPDATE
                    simplexresults.contactresults
                SET
                    RecordedResultPulled_int = -1
                WHERE
                    MasterRXCallDetailId_int = #getRR.MasterRXCallDetailId_int#
            </cfquery>
        </cfcatch>
        </cftry>

		<cfif VerboseDebug gt 0><HR /><cfflush></cfif> 

	</cfloop>

</cfoutput>


<!---
<cfoutput>
<cftry>
    <cfquery name="getRR" datasource="#Session.DBSourceEBM#">
       SELECT
       		MasterRXCallDetailId_int
            ,BatchId_bi
            ,DialerIP_vch
            ,DTS_UUID_vch
            ,BatchId_bi
            ,RedialNumber_int
            ,ContactString_vch
            ,REPLACE(XmlResultStr_vch,"=' ","='' ") AS XmlResultStr_vch
            ,REPLACE(XmlControlString_vch,"=' ","='' ") AS XmlControlString_vch
            ,RXCDLStartTime_dt
        FROM
        	simplexresults.contactresults
        WHERE
        	XmlControlString_vch LIKE '%RXT=''3''%'
            AND CallResult_int = 3
            AND XMLResultStr_vch <> 'NA'
            AND DialerIP_vch IS NOT NULL
            AND RecordedResultPulled_int = 0
    </cfquery>

    <cfloop query="getRR">
        <cfset MCIDText = "<root>#getRR.XmlControlString_vch#</root>">
        
        <cfset XmlStr = "<root>#getRR.XMLResultStr_vch#</root>">
        
        <cfset ELEQid = XMLSearch(MCIDText,"/root/RXSS/ELE[@RXT=3]")>
        
        <cfloop from="1" to="#ArrayLen(ELEQid)#" index="i">
			<cfset FinalQID = ELEQid[1].XmlAttributes.QID>
    
            <cfset QIDValue = XMLSearch(XmlStr,"/root/Q[@ID=#FinalQID#]")>
    
            <cfif ArrayLen(QIDValue) GT 0>
                <cfset FinalQIDValue = QIDValue[i].XmlText>
                <cfif StructKeyExists(QIDValue[i].XmlAttributes,"ADJHEATIDX")>
                    <cfset AdjHeatIndex = QIDValue[i].XmlAttributes.ADJHEATIDX>
                <cfelse>
                    <cfset AdjHeatIndex = 0>
                </cfif>
                
                <cfif FinalQIDValue EQ 1>
                    <cfquery name="getUID" datasource="#Session.DBSourceEBM#">
                        SELECT UserId_int
                        FROM simplequeue.contactqueue
                        WHERE DTS_UUID_vch = '#getRR.DTS_UUID_vch#'
                    </cfquery>
    
                    <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#")>
                        <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserId_int#">
                    </cfif>
                    
                    <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#")>
                        <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#">
                    </cfif>
                    
                    <cfif NOT DirectoryExists("#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#")>
                        <cfdirectory action="create" directory="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#">
                    </cfif>
                    
                    <cffile action="copy" source="\\#getRR.DialerIP_vch#\RXWaveFiles\RecordedResponses\Results\#getUID.UserId_int#\#getRR.BatchId_bi#\SurveyResults_#FinalQID#_#getRR.ContactString_vch#_#getRR.RedialNumber_int#.wav"
                        destination="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#.wav">
                    
                    <cfexecute name="#SOXAudiopath#" 
                            arguments=" #RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#.wav -C 128.3 #RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#.mp3 loudness"
                            timeout="60"> 
                    </cfexecute>
					
                    <cffile action="delete"
						file="#RRLocalFilePath#\RecordedResponses\U#getUID.UserID_int#\#getRR.BatchId_bi#\#DateFormat(RXCDLStartTime_dt,'yyyy_mm_dd')#\#getRR.MasterRXCallDetailId_int#.wav">
                    
                    <cfquery datasource="#Session.DBSourceEBM#">
                        INSERT INTO
                            simplexrecordedresults.transcriptionfilelog
                        (MasterRXCallDetailId_int, DTS_UUID_vch, BatchId_bi, UserID_int, XMLResultStr_vch, Original_FileName_vch, TransCo_FileName_vch, DialerIP_vch, InitialDate_dt, AdjustedHeatIndex_int, LastModified_dt)
                        VALUES (
                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.MASTERRXCALLDETAILID_INT#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.DTS_UUID_vch#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.BatchId_bi#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUID.UserID_int#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.XMLResultStr_vch#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="SurveyResults_#FinalQID#_#getRR.ContactString_vch#_#getRR.RedialNumber_int#.wav">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.MasterRXCallDetailId_int#.wav">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getRR.DialerIP_vch#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#getRR.RXCDLStartTime_dt#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AdjHeatIndex#">
                            ,<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#NOW()#">
                        )
                    </cfquery>
                </cfif>
                
                <cfquery datasource="#Session.DBSourceEBM#">
                	UPDATE
                        simplexresults.contactresults
                    SET
                    	RecordedResultPulled_int = 1
                    WHERE
                        MasterRXCallDetailId_int = #getRR.MasterRXCallDetailId_int#
                </cfquery>
            </cfif>
        </cfloop>
    </cfloop>
<cfcatch>
	<cfset ENAMessage = "ERROR on act_TransferResponses.cfm - #cfcatch.Message#, #cfcatch.Detail#">
	<cfinclude template="../NOC/act_EscalationNotificationActions.cfm">
    <cfquery datasource="#Session.DBSourceEBM#">
        UPDATE
            simplexresults.contactresults
        SET
            RecordedResultPulled_int = -1
        WHERE
            MasterRXCallDetailId_int = #getRR.MasterRXCallDetailId_int#
    </cfquery>
</cfcatch>
</cftry>
</cfoutput>

--->