


<!--- Logging codes --->
<cfset LOG_SMSSTARTPROCESSESING = 710>
<cfset LOG_SMSENDPROCESSESING = 720>


<!---
Log Types

10 - Start processing distribution
20 - End processing distribution
30 - Start RXDialer
35 - Time to Complete Duplicate UUID Checks
36 - Time to Complete Duplicate Batch Checks
38 - Push Staged Audio Times
40 - End RXDialer


110 - Extraction Process Started
120 - Extraction Process Completed

500 - Clean up tasks
501 - Data import clean up - Remove old staged import disposition tables


700's - SMS Processing

710 - Start processing distribution


 
--->
<!--- SMS Queue codes --->
<cfset SMSQCODE_READYTOPROCESS = 1>
<cfset SMSQCODE_PAUSED = 2>
<cfset SMSQCODE_INPROCESS = 3>
<cfset SMSQCODE_ERROR = 5>
<cfset SMSQCODE_ERROR_POSSIBLE_PROCESSED = 6>
<cfset SMSQCODE_PROCESSED = 10>
<cfset SMSQCODE_PROCESSED_ON_MO = 11>
<cfset SMSQCODE_INFORMATIONONLY = 100>




<!--- ProcessNextResponseSMS Failed--->
<cfset ALERTPROCERROR_NEXTRESPONSEFAILED = 210>
<!--- ProcessNextResponseSMS Failed--->
<cfset ALERTPROCERROR_GENERALQUEUEPROCERROR = 211>




