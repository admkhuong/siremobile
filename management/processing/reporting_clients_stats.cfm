<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced Client Statistics</title>
</head>

<body>

<cfparam name="startdate" default="#DateFormat(dateAdd('d',-1,now()), 'yyyy-mm-dd')#">
<cfparam name="enddate" default="#DateFormat(dateAdd('d',0,now()), 'yyyy-mm-dd')#">

 <cfset urladdress = "https://api.sendgrid.com/api/stats.getAdvanced.json?api_user=mbroadcast&api_key=L9v8kKrN&start_date=#URLEncodedFormat(startdate)#&end_date=#URLEncodedFormat(enddate)#&data_type=clients">  

<cfhttp url="#urladdress#" method="GET" resolveurl="Yes" throwonerror="Yes">

<cfoutput>
  This is just a string:<br />
  #CFHTTP.FileContent#<br />
</cfoutput>

<cfset cfData=DeserializeJSON(CFHTTP.FileContent)> 

This is object:<br />
<cfdump var="#cfData#">

<cfset arraylen = ArrayLen(cfdata)>

ArrayLength is : <cfdump var="#arraylen#"> 
 


 
 <cfquery datasource="10.11.0.130" name="qCoulmnInsert">
  INSERT INTO 
           simplexresults.contactresults_email_account_summary_clients  (aol_int,
           																 android_Phone_int,
                                                                         androidTablet_int,
                                                                         apple_mail_int,
                                                                         blackberry_int,
                                                                         Eudora_int,
                                                                         gMail_int,
                                                                         Hotmail_int,
                                                                         lotus_notes_int,
                                                                         other_int,
                                                                         other_webmail_int,
                                                                         Outlook_int,
                                                                         Postbox_int,
                                                                         sparrow_int,
                                                                         thunderbird_int,
                                                                         windowsLiveMail_int,
                                                                         yahoo_int,
                                                                         iPad_int,
                                                                         iphone_int,
                                                                         iPod_int,
                                                                        
                                                                        
                                                                        <!--- FOR DATES --->
                                                                        startdate_dt,
                                                                        enddate_dt,
                                                                        date_dt)
           
    VALUES
              <!--- loop through your array --->
             <cfloop from="1" to="#arrayLen(cfData)#" index="i">
             ( <!--- 1 --->
              <cfif structKeyExists(cfData[i], "open")>
              <!--- Just because the parent key open exists does not mean that it's chile will exist, hence checking for aol --->
              <cfif  structKeyExists(cfData[i].open,"aol")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["aol"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 2 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif  structKeyExists(cfData[i].open,"Android Phone")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Android Phone"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 3 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Android Tablet")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Android Tablet"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 4 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Apple Mail")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Apple Mail"]#">
               <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 5 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Blackberry")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Blackberry"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 6 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Eudora")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Eudora"]#">
               <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 7 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "gmail")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["gmail"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 8 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "hotmail")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["hotmail"]#">
             <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 9 --->
            <cfif structKeyExists(cfData[i], "open")>
            <cfif structKeyExists(cfData[i].open, "Lotus Notes")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Lotus Notes"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> , 
              
              <!--- 10 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Other")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Other"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 11 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Other Webmail")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Other Webmail"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
             <!--- 12 --->              
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Outlook")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Outlook"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 13 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Postbox")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Postbox"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 14 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Thunderbird")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Thunderbird"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 15 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Sparrow")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Sparrow"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 16 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Windows Live Mail")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Windows Live Mail"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 17 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "Yahoo")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["Yahoo"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 18 --->
              <cfif structKeyExists(cfData[i], "open")>
                 <cfif structKeyExists(cfData[i].open, "iPad")>
                   <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["iPad"]#">
                 <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 19 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "iPhone")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["iPhone"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
              <!--- 20 --->
              <cfif structKeyExists(cfData[i], "open")>
              <cfif structKeyExists(cfData[i].open, "iPod")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#cfData[i]["open"]["iPod"]#">
              <cfelse>
                 0
                 </cfif>
              <cfelse>
              0  
              </cfif> ,
              
                       
             
              
              
              
              
                            
              <!--- FOR DATES --->
              <cfif structKeyExists(cfData[i], "startdate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].startdate_dt#">
              <cfelse>
                NULL
              </cfif>, 
              
              
              
              <cfif structKeyExists(cfData[i], "enddate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].enddate_dt#">
              <cfelse>
                NULL
              </cfif>,
               
              
              
              <cfif structKeyExists(cfData[i], "date")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#cfData[i].date#">
              <cfelse>
                NULL
              </cfif> 
            
              )
              
              <cfif i neq arrayLen(cfData)>,</cfif>
          </cfloop>
</cfquery> 



    
    
    
    
  

</body>
</html>