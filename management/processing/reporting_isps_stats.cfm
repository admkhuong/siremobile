<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced ISPS Statistics</title>
</head>

<body>


<cfparam name="startdate" default="#DateFormat(dateAdd('d',-1,now()), 'yyyy-mm-dd')#">
<cfparam name="enddate" default="#DateFormat(dateAdd('d',0,now()), 'yyyy-mm-dd')#">

<cfset urladdress = "https://api.sendgrid.com/api/stats.getAdvanced.json?api_user=mbroadcast&api_key=L9v8kKrN&start_date=#URLEncodedFormat(startdate)#&end_date=#URLEncodedFormat(enddate)#&data_type=isps">

<cfhttp url="#urladdress#" method="GET" resolveurl="Yes" throwonerror="Yes">

<!--- <cfoutput>
  This is just a string:<br />
  #CFHTTP.FileContent#<br />
</cfoutput> --->

<cfset cfData=DeserializeJSON(CFHTTP.FileContent)> 

<!--- This is object:<br />
<cfdump var="#cfData#"> --->

<cfset arraylen = ArrayLen(cfdata)>

ArrayLength is : <cfdump var="#arraylen#"> 
 
<cfset totals = {}> 

<cfloop index="struct" array="#cfData#">
    <cfset totals[struct.date] = {blocked=0,bounce=0,deferred=0,delivered=0,drop=0,open=0,processed=0,request=0,spamreport=0,unique_open=0}>
     <cfloop item="key" collection="#struct#"> 
        <cfif isStruct(struct[key])>
            <cfset total = 0>
            <cfloop item="subkey" collection="#struct[key]#">
                <cfset total += struct[key][subkey]>
            </cfloop> 
            <cfset totals[struct.date][key] = total>
        </cfif>
    </cfloop>
</cfloop> 


<cfdump var="#totals#">


<!--- put all of the dates into an array --->
<cfset dateArray = structKeyArray(totals)>


<cfdump var="#dateArray#">

<!--- <cfset LoopArrayLen = arrayLen(dateArray)> --->


<!--- <cfabort> --->

<cfquery datasource="10.11.0.130" name="qCoulmnInsert">
INSERT INTO 
           simplexresults.contactresults_email_account_summary_isps  (
           																 blocked_int,
                                                                         bounce_int,
                                                                         deferred_int,
                                                                         delivered_int,
                                                                         drop_int,
                                                                         open_int,
                                                                         processed_int,
                                                                         request_int,
                                                                         spamreport_int,
                                                                         uniqueopen_int,
                                                                                                                                               
                                                                        
                                                                        <!--- FOR DATES --->
                                                                        startdate_dt,
                                                                        enddate_dt,
                                                                        date_dt
                                                                        )
           
  VALUES
              <!--- loop through your structure --->
             <cfloop from = "1" to="#arrayLen(dateArray)#" index="i">
             
             <!--- get current date --->
             
             <cfset theDate = dateArray[i]>
             
             <!--- append a comma in between value sets --->
             
             <cfif i gt 1>,</cfif>
             
             ( 
               
              <!--- 1 --->
              <!--- <cfif isStruct("blocked")> --->
             <cfif structKeyExists(totals[theDate], "blocked")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].blocked#">
              <cfelse>
                 NULL
              
              </cfif> ,
              <!--- </cfif> --->
              
              <!--- 2 --->
              <cfif structKeyExists(totals[theDate], "bounce")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].bounce#">
              <cfelse>
                 NULL
              </cfif> ,
              
             
              <!--- 3 --->
              <cfif structKeyExists(totals[theDate], "deferred")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].deferred#">
               <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 4 --->
              <cfif structKeyExists(totals[theDate], "delivered")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].delivered#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 5 --->
              <cfif structKeyExists(totals[theDate], "drop")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].drop#">
               <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 6 --->
              <cfif structKeyExists(totals[theDate], "open")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].open#">
               <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 7 --->
              <cfif structKeyExists(totals[theDate], "processed")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].processed#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 8 --->
              <cfif structKeyExists(totals[theDate], "request")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].request#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 9 --->
              <cfif structKeyExists(totals[theDate], "spamreport")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].spamreport#">
              <cfelse>
                 NULL
              </cfif> ,
              
              <!--- 10 --->
              <cfif structKeyExists(totals[theDate], "unique_open")>
                <cfqueryparam CFSQLTYPE="CF_SQL_INTEGER" value="#totals[theDate].unique_open#">
              <cfelse>
                 NULL
              </cfif> ,
              
             
              
                            
              <!--- FOR DATES --->
              <cfif structKeyExists(totals[theDate], "startdate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#totals[theDate].startdate_dt#">
              <cfelse>
                 NULL
              </cfif>, 
              
              
              
              <cfif structKeyExists(totals[theDate], "enddate_dt")>
                <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#totals[theDate].enddate_dt#">
              <cfelse>
                 NULL
              </cfif>,
               
              
              
              
              <cfqueryparam CFSQLTYPE="CF_SQL_DATE" value="#theDate#">
             
              
              
            
              )
              
             <!---  <cfif i neq arrayLen(cfData)>,</cfif> --->
          </cfloop>
          
          
</cfquery> 



    
    
    
    
    
    



     

</body>
</html>