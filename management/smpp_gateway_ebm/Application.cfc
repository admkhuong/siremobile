<cfcomponent>
	<cfset This.Sessionmanagement=true />
    <cfset This.Sessiontimeout="#createtimespan(0,0,60,0)#" />
    <cfset This.applicationtimeout="#createtimespan(1,0,0,0)#" />
    
    <cffunction name="OnApplicationStart" returntype="any">
    	<cfset APPLICATION.received = ArrayNew(1) />
        <cfset APPLICATION.permanentError = ArrayNEw(1) />
    </cffunction>
</cfcomponent>