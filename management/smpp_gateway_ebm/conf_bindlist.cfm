<!--- Server Name this is running on  - used for debugging - set server specific in conf_bindlist.cfm --->
<cfset LocalServerName = "10.25.0.87" />

<!--- Path to gateway log files --->
<cfset pathToFindLog = 'c:\ColdFusion9\logs\eventgateway.log'>

<!--- Build the array of possible SMPP Binds --->
<cfset BindArray = ArrayNew(1) />

<!--- Add all of your active binds here. Each server can be different --->
<!--- Is there a way to do this programmactically ???? --->
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.38.100-TX1") />
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.38.100-TX2") />
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.38.100-TX3") />
<cfset ArrayAppend(BindArray, "ATT-SMPP-209.183.38.100-TX4") />