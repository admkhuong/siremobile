<cfcomponent>

	<cffunction name="onIncomingMessage">
		<cfargument name="CFevent" type="struct" required="YES">
        
        <cftry>
		
			<cfset var data = CFevent.DATA />
            <cfset var UDHBuff = "" />
          		  
		  	<!---  
		  	<cfset var result = StructNew() />
            <cfset result.dataCoding = data.dataCoding />
            <cfset result.destAddress = data.destAddress />
            <cfset result.esmClass = data.esmClass />
            <cfset result.messageLength = data.messageLength />
            <cfset result.priority = data.priority />
            <cfset result.protocol = data.protocol />
            <cfset result.registeredDelivery = data.registeredDelivery />
            <cfset result.sourceAddress = data.sourceAddress />
            <cfset result.MESSAGE = data.MESSAGE />
            <cfset result.UDHString = "" />
            <cfset var tempMessage = data.MESSAGE />
            <cfset var receiptDetails = ['id:','sub:','dlvrd:','submit date:','done date:','stat:','err:','text:']/>
            <cfset var receiptDetailsCol = ['messageId','sub','dlvrd','submitDate','doneDate','stat','error','text']/>
            --->
            
            <!---
            
                Short Message Peer to Peer (SMPP) protocol 
            
            
                http://www.smstrade.de/pdf/smpp.pdf
            
                AT&T is the Short Message Service Centre (SMSC)
                
                We are the External Short Message Entities (ESME).
            
                The end users phone is an Short Message Entity (SME)
            
            --->
            
            
            <!---
            
            SEND CONCATENATED MESSAGES
    
            The maximum size of an SMS is 140 bytes. This equates to 160 plain text 7-bit characters (Latin-1 or GSM) or 70 Unicode (16-bit) characters. If you wish to send an SMS longer than this, it must be split into multiple parts. Each of these parts are sent as individual SMS messages to the handset, so in order for the handset to piece them together, you must configure a UDH (User Data Header) for each part of the long message.
            
            When a UDH is included in an SMS, it takes up 6 bytes in the SMS. This means that there is a slightly reduced capacity for the actual message content. The number of actual characters depends on the encoding, as follows:
            
            153 characters for 7-bit encoding (Latin-1)
            134 characters for 8-bit encoding (Binary)
            67 characters for 16-bit encoding (Unicode)
            
            USER DATA HEADER
    
            The UDH for a concatenated message should be configured according to the following format:
            
            :05:00:03:<A>:<B>:<C>
            
            Where <A>, <B> and <C> are variables which you should replace with appropriate hexadecimal values as described below. For concatenated messages, the UDH will always begin with ":05:00:03".
            
            Description of UDH hexadecimal values:
            
            Hexadecimal	Description
            05	Length of UDH - indicates that 5 bytes will follow
            00	Information Element Identifier (IEI) - indicates that the UDH is for a concatenated message
            03	Sub-header length - indicates that 3 bytes will follow
            <A>	Reference number - set the same for all parts of the long message
            <B>	Number of parts - indicates how many parts in message, ‘02’ for two parts, ‘03’ for three parts etc.
            <C>	Order of parts - indicates the order of the parts, ‘01’ for 1st part, ‘02’ for 2nd part etc.
                    
            
            --->
            
            
           <!--- Switch on ESM class DATA.esmClass ---> 
           <!---
           
                Normally a Binary value but CF/Java is converting to integer 	
                7 6 5 4 3 2 1 0 Meaning
           
                Message Mode (bits 1-0)
                x x x x x x x x not applicable - ignore bits 0 and 1
                Message Type (bits 5-2)
                x x 0 0 0 0 x x Default message Type (i.e. normal message)
                x x 0 0 0 1 x x Short Message contains SMSC Delivery Receipt
                x x 0 0 1 0 x x Short Message contains SME Delivery Acknowledgement
                x x 0 0 1 1 x x reserved
                x x 0 1 0 0 x x Short Message contains SME Manual/User Acknowledgment
                x x 0 1 0 1 x x reserved
                x x 0 1 1 0 x x Short Message contains Conversation Abort (Korean CDMA)
                x x 0 1 1 1 x x reserved
                x x 1 0 0 0 x x Short Message contains Intermediate Delivery Notification 
                       
           
                GSM Network Specific Features (bits 7-6)
           
                0 0 x x x x x x No specific features selected
                0 1 x x x x x x UDHI Indicator set - this is set for concatenated messages 
                1 0 x x x x x x Reply Path
                1 1 x x x x x x UDHI and Reply Path
    
                                
    
           --->
           
         	<!--- For debugging only --->
            <!---
			<cfset CFevent.EBMOUTEMSBM = BitMaskRead(data.esmClass, 2, 4) />
            <cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log4.txt" output="#SerializeJSON(CFevent)#" >
         	--->
         
           	<!--- For debugging only --->
            <!---
            <cfquery datasource="10.25.0.66">
            	insert into `attsmpp`.`smppresponse`(json_vch) values ( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SerializeJSON(CFevent)#"> )
            </cfquery>
            ---> 
           
           <!--- Add an extra value to pass to EBM for UDH header - Blank by default --->
           <cfset data.UDHString = "" />
           
           <!--- Start at 0 based position 2 and read to the left 4 binary digits - will be from 0 to 15 but not all are defined for ESM values --->
           <cfset data.BitResult52 = BitMaskRead(data.esmClass, 2, 4) />
           <!--- Start at 0 based position 6 and read to the single bit - will be 1 or 0  --->
           <cfset data.BitResult6 = BitMaskRead(data.esmClass, 6, 1) />
                  
           <!--- Only look at bits 5-2 to determine message type --->
           <cfswitch expression="#BitMaskRead(data.esmClass, 2, 4)#">
                
                <!--- x x 0 0 0 0 x x Default message Type (i.e. normal message) --->    	
                <cfcase value="0">
                
                    <!--- Check if UDH is set --->
                    <cfif BitMaskRead(data.esmClass, 6, 1) EQ 1>
                        
                        <!---
						
						
First SMS: 05 00 03 A6 02 01 .. bytes that make up the first part text ..
Second SMS: 05 00 03 A6 02 02 .. bytes that make up the second part text ..


						Bytes	Description
05	The UDHL, or length of the UDH. So the following five bytes are UDH
00	This is the IEI. This identifier says this is a concatenated message
03	This is the IEDL. It says that next 3 bytes are the data for this IE.
A6	The reference number of this concatenated message. Each part must have the same reference number.
02	There are two parts to this concatenated message.
01 or 02	This indicates whether the SMS is the first or second part.

OR
				
				
First SMS: 06 08 04 F4 2E 02 01 .. bytes that make up the first part text ..
Second SMS: 06 08 04 F4 2E 02 02 .. bytes that make up the second part text ..		
						Bytes	Description
06	The UDHL, or length of the UDH. So the following six bytes are UDH
08	This is the IEI. This identifier says this is a concatenated message with 16-bit reference
04	This is the IEDL. It says that next 4 bytes are the data for this IE.
F42E	The reference number of this concatenated message. Each part must have the same reference number.
02	There are two parts to this concatenated message.
01 or 02	This indicates whether the SMS is the first or second part.
--->

								<!--- Bug checking --->                      
                                <!--- Remove this section later on --->                        
                                <!--- Read binary header --->
								<cfset hexValue = binaryEncode( toBinary(toBase64( left(data.MESSAGE, 6) )), "hex" ) />
                                <cfset data.UDHStringOLD = right(hexValue, 12) />
                                <cfset data.UDHStringFullCFCF = hexValue />
                        
                        <!--- UDH processing 050003000000 is EBM default --->
                        <cfset data.UDHString = "050003000000"/>
                        <cfset data.MessageOriginal = data.MESSAGE/>
                       						                        
                        <cftry>
						
	                        <cfset var objString = createObject("Java", "java.lang.String").init(JavaCast("string", data.MESSAGE)) />
							
                            <!--- Look for unicode --->
                            <cfif data.dataCoding EQ "8">
	                            <cfset data.GETBYTESUDH = "UTF-16"/>
								<cfset var JavaByteArrayofMessage = objString.getBytes('UTF-16') />                               
    						<cfelse>
                            	<cfset var JavaByteArrayofMessage = objString.getBytes() />                                                                                                
                            </cfif>                            
                            
                            <cfset data.UDH1 = JavaByteArrayofMessage[1] />
							<cfset data.UDH2 = JavaByteArrayofMessage[2]  />
							<cfset data.UDH3 = JavaByteArrayofMessage[3]  />
							<cfset data.UDH4 = JavaByteArrayofMessage[4]  />
							<cfset data.UDH5 = JavaByteArrayofMessage[5]  />
							<cfset data.UDH6 = JavaByteArrayofMessage[6]  />
							<cfset data.UDH7 = JavaByteArrayofMessage[7]  />
							<cfset data.UDH8 = JavaByteArrayofMessage[8]  />
                           
                            <!--- convert to unsigned integer 0-255 or 00-FF in hex then create UDH string of 12 characters for EBM MO processing --->
                           
                            <!--- UDH Byte one--->
							<cfif data.UDH1 EQ 5>
	                            <cfset data.UDHString = "05"/>
                            <cfelseif data.UDH1 EQ 6>
	                            <cfset data.UDHString = "06"/>
                            <cfelse> <!--- Should not make it here but in case we do ... --->
                            
                            	<cfset UDHBuff = FormatBaseN(data.UDH1, 16) />
                            
                           		<cfif (Len( UDHBuff ) EQ 1)>
									<cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>

                            	<cfset data.UDHString = UDHBuff />
                            </cfif>
                            
                            <!--- UDH Byte two--->
                            <!--- Byte two should be 00 (8-bit indicator) or 08 (16-bit indicator) --->
                            <cfif data.UDH2 EQ 0>
	                            <cfset data.UDHString = data.UDHString & "00"/>
                            <cfelseif data.UDH2 EQ 8>
                            	<cfset data.UDHString = data.UDHString & "08"/>
                            <cfelse> <!--- Should not make it here but in case we do ... --->
                            
                            	<cfset UDHBuff = FormatBaseN(data.UDH2, 16) />
                            
                           		<cfif (Len( UDHBuff ) EQ 1)>
									<cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>

                            	<cfset data.UDHString = data.UDHString & UDHBuff />
                            </cfif>

							<!--- UDH Byte three--->
                            <cfif data.UDH3 EQ 3>
	                            <cfset data.UDHString = data.UDHString & "03"/>
                            <cfelseif data.UDH3 EQ 4>
                            	<cfset data.UDHString = data.UDHString & "04"/>
                            <cfelse> <!--- Should not make it here but in case we do ... --->
                            
                            	<cfset UDHBuff = FormatBaseN(data.UDH3, 16) />
                            
                           		<cfif (Len( UDHBuff ) EQ 1)>
									<cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>

                            	<cfset data.UDHString = data.UDHString & UDHBuff/>
                            </cfif>
							                           
                           <!---
						   			EBM MO processing takes time in to account so only expects 12 char UDH 
						   			Translate from 14 char to 12 char by only using the highest byte of two byte Id
						   --->
                           	
                            <!--- Look for five or six bytes following beginning of header---> 
                            <!--- UDH Byte four,five and six --->
                            <cfif data.UDH1 EQ 5>
                                                        
                                <!--- Convert signed -127 to +127 to 0 to 255--->
                            	<cfif data.UDH4 LT 0 >
                                	<cfset data.UDH4 = 127 + ABS(data.UDH4) />                                
                                </cfif>
                                
								<!--- Reference number 1 byte  --->
                                <cfset UDHBuff = FormatBaseN(data.UDH4, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
    
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                            
                            	<!---  Number of parts --->
								<cfset UDHBuff = FormatBaseN(data.UDH5, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
                                
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                                
                                <!--- Message part --->
                                <cfset UDHBuff = FormatBaseN(data.UDH6, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
                                
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                                
                                <!--- Remove the header from the final message string --->
		                        <cfset data.MESSAGE = RIGHT(data.MESSAGE, (LEN(data.MESSAGE) - 6) ) />
                            
                            <cfelse>
                            
                            	<!--- Convert signed -127 to +127 to 0 to 255--->
                            	<cfif data.UDH5 LT 0 >
                                	<cfset data.UDH5 = 127 + ABS(data.UDH5) />                                
                                </cfif>
                            
                            	<!--- Reference number 2 byte  --->
                                <cfset UDHBuff = FormatBaseN(data.UDH5, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
    
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                            
                            
	                            <cfset UDHBuff = FormatBaseN(data.UDH6, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
                                
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                                
                                <!--- Message part --->
                                <cfset UDHBuff = FormatBaseN(data.UDH7, 16) />
                                
                                <cfif (Len( UDHBuff ) EQ 1)>
                                    <cfset UDHBuff = ("0" & UDHBuff) />
                                </cfif>
                                
                                <cfset data.UDHString = data.UDHString & UDHBuff/>
                                
                                <!--- Remove the header from the final message string --->
		                        <cfset data.MESSAGE = RIGHT(data.MESSAGE, (LEN(data.MESSAGE) - 7) ) />
                            
                            </cfif>
                            
                            <!--- 
								Stupid consequence of untyped coldfusion is converting my UDH that contains an "E" in it to scientific notation
								
								0500030E0202 some how becomes 5.0003E207 WTF?!?
								
								I can almost forgive the . and the e but why 0202 become 0207 ?!?
								
								Cludge fix is to replace E with D
							--->
                            <cfset data.UDHString = ReplaceNoCase(data.UDHString,"e", "D", "ALL") />
                            							                        
                        <cfcatch type="any">
                              	 
                            <cfset data.UDHString = "000000000000"/>
                            <cfset data.UDHERROR = "#cfcatch.Message# #cfcatch.detail#"/>
                        
                        	<!--- Remove the header from the final message string --->
                        	<cfset data.MESSAGE = RIGHT(data.MESSAGE, (LEN(data.MESSAGE) - 6) ) />
                                         
                        </cfcatch>
                        </cftry> 
                                               
                    </cfif>
                    
                    <cftry>
                        
                        <!--- Development is in devebmmo.ebm.internal and Production is ebmmo.ebm.internal only --->                    
                        <cfthread name="thr#CreateUUID()#" action="run" incomingContent="#SerializeJSON(data)#">
                            <cfhttp url="http://ebmmo.ebm.internal/ebmresponse/sms/response288ii/mo" method="POST" result="returnStruct" timeout="60">
                                <cfhttpparam type="formfield" name="JSONDATA" value="#attributes.incomingContent#">	
                            </cfhttp>
                        </cfthread>
                        
                        <!--- Small delay to allow for actual processing as to not fake out remote server--->
                        <cfset sleep(300)>
                       
                    <cfcatch type="any">
                              	         
                    </cfcatch>
                    </cftry> 
                    
                </cfcase>
                
                <!--- x x 0 0 0 1 x x Short Message contains SMSC Delivery Receipt --->
                <cfcase value="1">
                
                	<!--- For debugging only --->
                    <!---
					<cfset CFevent.EBMOUTEMSBM = 1 />
            		<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log5.txt" output="#SerializeJSON(CFevent)#" >
					--->
         		                
                	<cftry>
                        
                        <!--- Development is in devebmmo.ebm.internal and Production is ebmmo.ebm.internal only --->                    
                        <cfthread name="thr#CreateUUID()#" action="run" incomingContent="#SerializeJSON(data)#">
                            <cfhttp url="http://ebmmo.ebm.internal/ebmresponse/sms/notificationservices288ii/mtstatus" method="POST" result="returnStruct" timeout="60">
                                <cfhttpparam type="formfield" name="JSONDATA" value="#attributes.incomingContent#">	
                            </cfhttp>
                        </cfthread>
                        
                        <!--- Small delay to allow for actual processing as to not fake out remote server--->
                        <cfset sleep(200)>
                       
                    <cfcatch type="any">
          	        
						<!--- For debugging only --->
                        <!---<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log6.txt" output="#SerializeJSON(CFCATCH)#" >--->
         		             
                    </cfcatch>
                    </cftry> 
                    
                </cfcase>
                
                <!--- x x 0 0 1 0 x x Short Message contains SME Delivery Acknowledgement --->
                <cfcase value="2">
                
                </cfcase>
               
                <!--- x x 0 0 1 1 x x reserved  --->
                <cfcase value="3">
                
                </cfcase>
                 
                <!---x x 0 1 0 0 x x Short Message contains SME Manual/User Acknowledgment --->
                <cfcase value="4">
                
                	<!--- For debugging only --->
                    <!---
					<cfset CFevent.EBMOUTEMSBM = 4 />
            		<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log5.txt" output="#SerializeJSON(CFevent)#" >
         			--->
                
                	<cftry>
                        
                        <!--- Development is in devebmmo.ebm.internal and Production is ebmmo.ebm.internal only --->                    
                        <cfthread name="thr#CreateUUID()#" action="run" incomingContent="#SerializeJSON(data)#">
                            <cfhttp url="http://ebmmo.ebm.internal/ebmresponse/sms/notificationservices288ii/mtstatus" method="POST" result="returnStruct" timeout="60">
                                <cfhttpparam type="formfield" name="JSONDATA" value="#attributes.incomingContent#">	
                            </cfhttp>
                        </cfthread>
                        
                        <!--- Small delay to allow for actual processing as to not fake out remote server--->
                        <cfset sleep(200)>
                       
                    <cfcatch type="any">
          	        
						<!--- For debugging only --->
                        <!---<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log6.txt" output="#SerializeJSON(CFCATCH)#" >--->
         		             
                    </cfcatch>
                    </cftry> 
                    
                
                </cfcase>
                
                <!--- x x 0 1 0 1 x x reserved --->
                <cfcase value="5">
                
                </cfcase>
           
                <!--- x x 0 1 1 0 x x Short Message contains Conversation Abort (Korean CDMA) --->
                <cfcase value="6">
                
                </cfcase>
                           
                <!--- x x 0 1 1 1 x x reserved --->
                <cfcase value="7">
                
                </cfcase>
                
                <!--- x x 1 0 0 0 x x Short Message contains Intermediate Delivery Notification  --->
                <cfcase value="8">
                
                	<cftry>
                        
                        <!--- Development is in devebmmo.ebm.internal and Production is ebmmo.ebm.internal only --->                    
                        <cfthread name="thr#CreateUUID()#" action="run" incomingContent="#SerializeJSON(data)#">
                            <cfhttp url="http://ebmmo.ebm.internal/ebmresponse/sms/notificationservices288ii/mtstatus" method="POST" result="returnStruct" timeout="60">
                                <cfhttpparam type="formfield" name="JSONDATA" value="#attributes.incomingContent#">	
                            </cfhttp>
                        </cfthread>
                        
                        <!--- Small delay to allow for actual processing as to not fake out remote server--->
                        <cfset sleep(200)>
                       
                    <cfcatch type="any">
          	        
						<!--- For debugging only --->
                        <!---<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log6.txt" output="#SerializeJSON(CFCATCH)#" >--->
         		             
                    </cfcatch>
                    </cftry> 
                    
                
                </cfcase>
                        
           </cfswitch>
                                  
			      
        <cfcatch type="any">
            <cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log1.txt" output="#SerializeJSON(cfcatch)#" >
        </cfcatch>
        </cftry>
	   
	</cffunction>

	

</cfcomponent>