<!--- This page will need more security if it is to be public - should be on private internal network only --->
<cfsetting enablecfoutputonly="yes">
<cfsetting showdebugoutput="no">

<!--- Path to SMPP gateway log to look for errors - below is default - set server specific in conf_bindlist.cfm --->
<cfset pathToFindLog = ''>

<!--- Server Name this is running on  - used for debugging - set server specific in conf_bindlist.cfm --->
<cfset LocalServerName = "10.25.0.86" />

<!--- For debugging only --->
<!---
<cffile action="append" file="#GetDirectoryFromPath(GetCurrentTemplatePath())#\log6.txt" output="cgi.content_type = #cgi.content_type# #ToString(getHTTPRequestData().content)#" >
--->            

<!--- This page will take standard SMPP params in a post JSON string as content and output JSON result data --->

<!--- Parse JSON for structured data --->
<cfif findnocase("application/json", cgi.content_type) GT 0>                      
	<cfset SendSMPPJSON = DeserializeJSON(ToString(getHTTPRequestData().content)) />  
<cfelse>
	<cfset SendSMPPJSON = StructNew() />
	<cfset SendSMPPJSON.shortMessage = "" />
    <cfset SendSMPPJSON.sourceAddress = "" />
    <cfset SendSMPPJSON.destAddress = "" />
    <cfset SendSMPPJSON.registeredDelivery = "0" />
    <cfset SendSMPPJSON.ApplicationRequestId = "" />       
    <cfset SendSMPPJSON.SarMsgRefNum = "0" /> 
	<cfset SendSMPPJSON.SarTotalSegments = "0" /> 
    <cfset SendSMPPJSON.SarSegmentSeqnum = "1" />
    <cfset SendSMPPJSON.PreferredBind = "" /> 
    <cfset SendSMPPJSON.dataCoding = "0" />                              
</cfif>     
                 
<!--- Store result data as structure - return as JSON --->
<cfset ATTSMPPDataOut = StructNew() />
<cfset ATTSMPPDataOut.MESSAGE = "Error - Default Data" />
<cfset ATTSMPPDataOut.BINARYRESULTCODE = 0 />
<cfset ATTSMPPDataOut.PROCTIME = 0 />
<cfset ATTSMPPDataOut.LocalServerName = LocalServerName />
<cfset ATTSMPPDataOut.BINDADDR = "Default" />
<cfset ATTSMPPDataOut.GATEWAYLOGLINE = "" />
<cfset ATTSMPPDataOut.GATEWAYLOGLINECOUNT = 0 />
<cfset ATTSMPPDataOut.msg = structNew() />

<!--- 1 if success, 0 if failure - check ATTSMPPDataOut.MESSAGE for failure resons--->
<cfset ATTSMPPDataOut.RESULTCODE = 0 />
<cfset ATTSMPPDataOut.ERRORCODE = 0 />
       	        
<cftry>

	<!--- Start time of request tracking --->
    <cfset intHTTPStartTime = GetTickCount() /> 
    
    <cfset msg = structNew() />
    
    <!--- Load bind list from file - every server is optionally different--->
	<cfinclude template="conf_bindlist.cfm" />
    
    <!--- Calculate numnber of binds --->
    <cfset NumberOfActiveBinds = ArrayLen(BindArray) />
    
    <!--- Get the next bind randomly --->
    <cfset CurrRndBind = RandRange(1, NumberOfActiveBinds, "SHA1PRNG") />
	<cfset ATTSMPPDataOut.BINDADDR = "#BindArray[CurrRndBind]#" />
    
    <!--- STOP! Do not allow prefered binds. Bad feature due to Load Balancer does not guarantee same server will get all of the same requests --->
    <!--- ***JLP New feature needed - do UDH and Concat on this side and not EBM side --->	
	<!---
	<cfif SendSMPPJSON.PreferredBind EQ "" >
    	<!--- Get the next bind randomly --->
        <cfset CurrRndBind = RandRange(1, NumberOfActiveBinds, "SHA1PRNG") />
	    <cfset ATTSMPPDataOut.BINDADDR = "#BindArray[CurrRndBind]#" />
    <cfelse>
	    <!--- Get the next bind requested --->
        <cfset ATTSMPPDataOut.BINDADDR = SendSMPPJSON.PreferredBind />
    </cfif>
	--->
               
    <cfif LEN(ToString(SendSMPPJSON.destAddress)) GT 0>
        
        <!--- To send static message to multiple recipeients - submitMulti use http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e3.html  --->
        <!--- http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-77e5.html --->
        <!--- I need to use ToString() because the underlying java and JSON Serialize/DeSerialize are converting phone numbers and short codes to integer which throw a java error when expecting string --->
        <cfset msg = structNew() />
        <cfset msg.command = "submit" />
        <cfset msg.shortMessage = ToString(SendSMPPJSON.shortMessage)>
        <cfset msg.destAddress = ToString(SendSMPPJSON.destAddress)>
        <cfset msg.sourceAddress = ToString(SendSMPPJSON.sourceAddress)>
        
        <!---
		SMPP standard:
			registeredDelivery = 0 – no delivery receipt requested
			registeredDelivery  = 1 - return delivery receipt on final state (i.e. delivered, expired, or rejected)
			registeredDelivery  = 2 - only return delivery receipt when final state is failed (expired or rejected)
		--->
        
        <cfif ListContains("0,1,2", "#SendSMPPJSON.registeredDelivery#") GT 0 >
        	<cfset msg.registeredDelivery = ToString(SendSMPPJSON.registeredDelivery) />
        <cfelse>
        	<cfset msg.registeredDelivery = ToString("0") />    
        </cfif>
        
        <!---
		
		The following optional fields do not have default values:

			alertOnMsgDelivery
			EsmClass
			priorityFlag
			smDefaultMsgId
			callbackNum
			ItsReplyType
			PrivacyIndicator
			SmsSignal
			callbackNumAtag
			ItsSessionInfo
			protocolId
			SourceAddrSubunit
			callbackNumPresInd
			LanguageIndicator
			registeredDelivery
			SourcePort
			dataCoding
			MoreMsgsToSend
			replaceIfPresent
			SourceSubaddress
			DestAddrSubunit
			MsMsgWaitFacilities
			SarMsgRefNum
			UserMessageReference
			DestinationPort
			MsValidity
			SarSegmentSeqnum
			UserResponseCode
			DestSubaddress
			NumberOfMessages
			SarTotalSegments
			UssdServiceOp
			DisplayTime
			PayloadType
			scheduleDeliveryTime
			validityPeriod		
		--->
                
        <!--- Check if this is a concatenated message --->
        <cfif SendSMPPJSON.SarTotalSegments GT 0> 
        
        	<!--- Get rid of JSON conversion causing 1.0 vs 1 --->
        	<cfset msg.SarMsgRefNum = ToString(javacast("int", SendSMPPJSON.SarMsgRefNum)) />
            <cfset msg.SarTotalSegments = ToString(javacast("int", SendSMPPJSON.SarTotalSegments)) />
            <cfset msg.SarSegmentSeqnum = ToString(javacast("int", SendSMPPJSON.SarSegmentSeqnum)) />
        </cfif>
                              
       	<!--- 
			<cfset param = structNew() />
			<cfset param.sequence_number = 4 />        
			<cfset param.optionalParameters = param />
		--->
        
        <!---
			x00 for SMSC default alphabet - the ISO 8859-1 Latin1
			character set is the default alphabet on AT&T SMSCs. ISO- 8859-1 is an 8 bit character set however it is converted into a 7- bit GSM characters by the SMSC before delivery to the mobile device.
			See GSM 03.38 for the GSM characters supported (including the extended characters). ISO 8859-1 characters that do not map directly to GSM 03.38 characters are either mapped to the closest proximity (e.g. capital O with a tilde will map to capital O) or if no equivalent it will map to an inverted question mark. See the character mapping in Appendix A.
			Also note that the GSM extended characters take the place of two characters when mapped, which reduces the maximum number of character that can be sent. See GSM Extended Character Set, Message Length, and Appendix A for more details.
			
			x03 for ISO 8859-1 Latin 1 – This is treated the same as x00. 
			
			x08 for UCS2 (ISO/IEC 10646) – ESME may encode the text
			using the 16 bit UCS2 character set however note that the message body will only support 70 characters. The SMSC will pass the UCS2 encoded characters directly to the mobile device without re-mapping. Not all character sets in UCS2 may be supported by the MA or the mobile handset. UCS2 requires 2 bytes per character therefore the message length field must be an even number otherwise the message will rejected with a status code of 0x01.
			If UCS2 characters are used in segmented messages, the 2 bytes comprising one UCS2 character must not be split between the segements.
			Use of any other DSC value must be approved by AT&T.
		--->
        <cfif StructKeyExists(SendSMPPJSON, "dataCoding") >
        
        	<!--- x08 for UCS2 (ISO/IEC 10646)  "Unicode" --->
        	<cfif SendSMPPJSON.dataCoding EQ "8">        
        		<cfset msg.dataCoding = ToString(javacast("int", SendSMPPJSON.dataCoding)) />
	        </cfif>
        
        </cfif>
        
        <!--- ***JLP Don't send if on Application level delay hold request --->
        
        <!--- Docs say sendGatewayMessage is ASYN - see http://help.adobe.com/en_US/ColdFusion/9.0/CFMLRef/WSc3ff6d0ea77859461172e0811cbec22c24-61ff.html --->	
        
        <cfset ATTSMPPDataOut.BINARYRESULTCODE = sendGatewayMessage(BindArray[CurrRndBind], msg) /> 
        
        <!--- Validate success or look for error codes (in logs?) --->
        <cfif LEN(ATTSMPPDataOut.BINARYRESULTCODE) GT 0 >
        
        	<!--- ***JLP Possible new EBM API for just logging sent MTs - Ones that go through RXDialer web service can not parse the BINARYRESULTCODE for matching to delivery receipts  --->
        	<cfset ATTSMPPDataOut.RESULTCODE = 1 />
            <cfset ATTSMPPDataOut.MESSAGE = "OK" />
        
        <cfelse>
        
	        <!--- Failed to send request --->
            <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - General Error - Gateway did not respond." />
            
        	<!--- Add more error handling here - check for pause, etc --->
            
            <!---Checking Error--->
            <cftry>            
            
            	<cfset line = "" />
                <cfset lines = "" />
                <cfset theFile = createObject("java","java.io.File").init(pathToFindLog) />
                
                <!--- Concurrency issues ??? should not be as this is read only --->
                <cfset raFile = createObject("java","java.io.RandomAccessFile").init(theFile,"r") />
                <cfset pos = theFile.length() />
                <cfset c = "" />
                <cfset total = 100 />
                
                <!--- Set file read to end of file --->                
                <cfset raFile.seek(pos-1) />
                
                <cfloop condition = "(listLen(line,chr(10)) LTE total) && pos GT -1">
                   
                   	<cfset c = raFile.read() />
                   
                   	<cfif c NEQ -1>
                   		<cfset line &= chr(c) />
                   	</cfif>        
                    
                    <cfset raFile.seek(pos--) />
                                                      
                </cfloop>                
               
                <!--- ***JLP How can/do we monitor too many open file handles/references? --->
                <!--- Close the file --->
                <cfset raFile.close() />	
                 
				<cfset line = reverse(line) />
                <cfset lines = listToArray(line, chr(10)) />
				
				<!--- Remove last blank line --->
            	<cfset arrayDeleteAt(lines,1) />                
            	                
                <!--- Read array backwards - read from bottom up - uop to configured limit of the array --->                	
                <cfloop index="i" from="#arrayLen(lines)#" to="1" step="-1" >
                    
                    <cfset ATTSMPPDataOut.GATEWAYLOGLINECOUNT = ATTSMPPDataOut.GATEWAYLOGLINECOUNT + 1 />
                    
                    <!--- Match errors to SMS destination--->                          
                    <cfif Find('#TRIM(msg.destAddress)#',lines[i])>
                    
	                    <!--- Put most common errors at top to limit search time --->
						<cfif Find('0x66',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "66" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RX_R_APPN Permanent - ESME Receiver Reject Message Error Code" />
                        	<cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
						<cfelseif Find('0x64',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "64" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RX_T_APPN ESME Receiver Temporary App Error Code." />                        
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif FindNoCase('0xa',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "10" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RINVSRCADR Invalid Source Address" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif FindNoCase('0xb',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "11" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RINVDST Invalid Destination Address" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif FindNoCase('0xd',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "13" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RBINDFAIL Bind Failed" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif Find('0x14',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "14" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RMSGQFUL Message Queue Full " />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif Find('0x15',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "15" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RINVSERTYP Invalid Service Type" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif Find('0x58',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "58" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RTHROTTLED Throttling error (ESME has exceeded allowed message limits)" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <!--- Search for 0x58 first then for 0x5 or will return false match --->
						<cfelseif Find('0x5',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "5" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RALYBND ESME already in Bound State" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->
                            <cfbreak/>
                        <cfelseif Find('0x45',lines[i])>
                            <cfset ATTSMPPDataOut.RESULTCODE = "45" />
                            <cfset ATTSMPPDataOut.MESSAGE = "Gateway Error - ESME_RSUBMITFAIL submit_sm or submit_multi failed" />
                            <cfset ATTSMPPDataOut.GATEWAYLOGLINE = lines[i] />
                            <!--- Record found - exit loop --->	
                            <cfbreak/>
                        </cfif>

                    </cfif>    
                
                </cfloop>
                    
            <cfcatch type="any">            
            
    	        <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
	            <cfset ATTSMPPDataOut.MESSAGE = "Error - General Error while reading error info. #SerializeJSON(CFCATCH)#" />
            </cfcatch>
            
            </cftry>            
        
        </cfif>
       
        <!--- Add time to send to result --->                 
        <cfset ATTSMPPDataOut.PROCTIME = (GetTickCount() - intHTTPStartTime) /> 
       
     <cfelse>
        
        <!--- Failed to send request --->
        <cfset ATTSMPPDataOut.RESULTCODE = 0 />                
        <cfset ATTSMPPDataOut.MESSAGE = "Error - Destination Address not specified" />
                 
    </cfif>
    
    <cfset ATTSMPPDataOut.msg = msg />
                       
<cfcatch type="any">
       
    <!--- Failed to send request --->
	<cfset ATTSMPPDataOut.RESULTCODE = 0 />                
    <cfset ATTSMPPDataOut.MESSAGE = "#cfcatch.ErrNumber# #cfcatch.Type# #cfcatch.Message# #cfcatch.Detail#" />

</cfcatch>

</cftry>

<!--- Output results as JSON --->
<cfoutput>#SerializeJSON(ATTSMPPDataOut)#</cfoutput>