CREATE DATABASE  IF NOT EXISTS `pbx` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pbx`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: pbx
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pbx`
--

DROP TABLE IF EXISTS `pbx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbx` (
  `AccountId_vch` varchar(50) NOT NULL,
  `Extension_vch` varchar(50) NOT NULL,
  `Password_vch` varchar(255) NOT NULL,
  `LATNumber_vch` varchar(50) DEFAULT NULL,
  `TimeZone_int` int(11) DEFAULT NULL,
  `GreetingTypeId_int` int(11) DEFAULT NULL,
  `BoxTypeId_int` int(11) DEFAULT NULL,
  `FindMeList_vch` varchar(8000) DEFAULT NULL,
  `EmailList_vch` varchar(8000) DEFAULT NULL,
  `SMSList_vch` varchar(8000) DEFAULT NULL,
  `AtachedToeMail_int` int(11) DEFAULT NULL,
  `PlayEnvelope_int` int(11) DEFAULT NULL,
  `PlayCID_int` int(11) DEFAULT NULL,
  `MaxRerecords_int` int(11) DEFAULT '10',
  `MaxMessageLengthSeconds_int` int(11) DEFAULT '120',
  `RecordBridgedCallAsync_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`Extension_vch`,`AccountId_vch`),
  KEY `Password_vch` (`Password_vch`),
  KEY `Extension_vch` (`Extension_vch`),
  KEY `AccountId_vch` (`AccountId_vch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:00
