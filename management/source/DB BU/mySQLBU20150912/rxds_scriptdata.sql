CREATE DATABASE  IF NOT EXISTS `rxds` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rxds`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: rxds
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `scriptdata`
--

DROP TABLE IF EXISTS `scriptdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scriptdata` (
  `DataId_int` int(11) NOT NULL DEFAULT '0',
  `DSEId_int` int(11) NOT NULL DEFAULT '0',
  `DSId_int` int(11) NOT NULL DEFAULT '0',
  `UserId_int` int(11) NOT NULL DEFAULT '0',
  `StatusId_int` int(11) DEFAULT NULL,
  `AltId_vch` varchar(255) NOT NULL DEFAULT '',
  `Length_int` int(11) DEFAULT '0',
  `Format_int` int(11) DEFAULT '0',
  `Active_int` int(11) DEFAULT '1',
  `Desc_vch` text,
  `TTS_vch` text,
  `AccessLevel_int` int(11) NOT NULL DEFAULT '0',
  `categories_vch` varchar(255) DEFAULT NULL,
  `votesUp_int` int(11) DEFAULT '0',
  `votesDown_int` int(11) DEFAULT '0',
  `Created_dt` datetime NOT NULL,
  `viewCount_int` int(11) DEFAULT '0',
  `scriptFacebookId_vch` varchar(100) DEFAULT NULL,
  `LastUpdated_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`DataId_int`,`DSEId_int`,`DSId_int`,`UserId_int`),
  KEY `DataId_int` (`DataId_int`),
  KEY `DSEId_int` (`DSEId_int`),
  KEY `DSId_int` (`DSId_int`),
  KEY `UserId_int` (`UserId_int`),
  KEY `StatusId_int` (`StatusId_int`),
  KEY `Active_int` (`Active_int`),
  KEY `IDX_AccessLevel_int` (`AccessLevel_int`),
  KEY `UserId_int_index` (`UserId_int`),
  KEY `AccessLevel_int_index` (`AccessLevel_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:40
