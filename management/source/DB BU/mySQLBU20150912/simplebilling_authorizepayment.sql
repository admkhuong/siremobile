CREATE DATABASE  IF NOT EXISTS `simplebilling` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplebilling`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplebilling
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorizepayment`
--

DROP TABLE IF EXISTS `authorizepayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorizepayment` (
  `idauthorizepayment` int(11) NOT NULL AUTO_INCREMENT,
  `userId_int` int(11) DEFAULT NULL,
  `nameOnCard_vch` varchar(100) DEFAULT NULL,
  `billingAddress_vch` varchar(300) DEFAULT NULL,
  `zip_vch` varchar(12) DEFAULT NULL,
  `ccn_vch` varchar(200) DEFAULT NULL,
  `amount_dec` decimal(8,2) DEFAULT NULL,
  `response_code_vch` varchar(100) DEFAULT NULL,
  `response_subcode_vch` varchar(100) DEFAULT NULL,
  `response_reason_code_vch` varchar(100) DEFAULT NULL,
  `response_reason_text_vch` varchar(500) DEFAULT NULL,
  `response_auth_code_vch` varchar(100) DEFAULT NULL,
  `response_avs_code_vch` varchar(100) DEFAULT NULL,
  `response_trans_id` varchar(150) DEFAULT NULL,
  `transactionDate_dt` datetime DEFAULT NULL,
  `transactionType_vch` varchar(500) DEFAULT NULL,
  `x_first_name` varchar(50) DEFAULT NULL,
  `x_last_name` varchar(50) DEFAULT NULL,
  `x_company` varchar(50) DEFAULT NULL,
  `x_address` varchar(60) DEFAULT NULL,
  `x_city` varchar(40) DEFAULT NULL,
  `x_state` varchar(40) DEFAULT NULL,
  `x_exp_date_mm` int(10) unsigned DEFAULT NULL,
  `x_exp_date_yyyy` int(10) unsigned DEFAULT NULL,
  `cctype_vch` varchar(20) DEFAULT NULL,
  `x_country` varchar(2) DEFAULT NULL,
  `x_cavv_response` varchar(255) DEFAULT NULL,
  `x_recurring_billing` int(10) unsigned DEFAULT NULL,
  `x_subscription_id` int(10) unsigned DEFAULT NULL,
  `x_subscription_paynum` int(10) unsigned DEFAULT NULL,
  `customerPaymentProfileID_int` int(10) unsigned DEFAULT NULL,
  `PayPalTransactionId_vch` varchar(255) DEFAULT NULL,
  `PayPalApprovalLink_vch` varchar(1024) DEFAULT NULL,
  `PayPalExecuteLink_vch` varchar(1024) DEFAULT NULL,
  `PayPalSelfLink_vch` varchar(1024) DEFAULT NULL,
  `PayPalComplete_int` tinyint(4) DEFAULT '0',
  `CreditsAddAmount_int` int(10) DEFAULT '0',
  `PayPalExecuteDetails_vch` text,
  PRIMARY KEY (`idauthorizepayment`),
  KEY `IDX_AuthLink` (`PayPalApprovalLink_vch`(767))
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:10
