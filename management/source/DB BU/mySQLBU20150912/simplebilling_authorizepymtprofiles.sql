CREATE DATABASE  IF NOT EXISTS `simplebilling` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplebilling`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplebilling
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorizepymtprofiles`
--

DROP TABLE IF EXISTS `authorizepymtprofiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorizepymtprofiles` (
  `customerPaymentProfileID_int` int(10) unsigned NOT NULL,
  `userid_int` int(10) unsigned NOT NULL,
  `x_first_name_vch` varchar(50) NOT NULL,
  `x_last_name_vch` varchar(50) NOT NULL,
  `x_company_vch` varchar(50) NOT NULL,
  `x_address_vch` varchar(60) NOT NULL,
  `x_city_vch` varchar(40) NOT NULL,
  `x_state_vch` varchar(40) NOT NULL,
  `zip_vch` varchar(12) NOT NULL,
  `x_country_vch` varchar(2) NOT NULL,
  `cctype_vch` varchar(20) NOT NULL,
  `ccn_vch` varchar(200) NOT NULL,
  `x_exp_date_mm_int` int(10) unsigned NOT NULL,
  `x_exp_date_yyyy_int` int(10) unsigned NOT NULL,
  `response_reason_text_vch` varchar(255) NOT NULL,
  `Active_int` int(10) unsigned NOT NULL DEFAULT '1',
  `Deleted_int` int(10) unsigned NOT NULL DEFAULT '0',
  `Added_dt` datetime NOT NULL,
  `response_trans_id_vch` varchar(150) NOT NULL,
  `IP_VCH` varchar(64) NOT NULL,
  `IsPrimary_int` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerPaymentProfileID_int`,`userid_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:09
