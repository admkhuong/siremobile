CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactlist`
--

DROP TABLE IF EXISTS `contactlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactlist` (
  `ContactId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL COMMENT 'This will be the user/company who is uploading the contact information',
  `Company_vch` varchar(500) DEFAULT NULL,
  `FirstName_vch` varchar(90) DEFAULT NULL,
  `LastName_vch` varchar(90) DEFAULT NULL,
  `Address_vch` varchar(250) DEFAULT NULL,
  `Address1_vch` varchar(100) DEFAULT NULL,
  `City_vch` varchar(100) DEFAULT NULL,
  `State_vch` varchar(50) DEFAULT NULL,
  `ZipCode_vch` varchar(20) DEFAULT NULL,
  `Country_vch` varchar(100) DEFAULT NULL,
  `UserName_vch` varchar(300) DEFAULT NULL COMMENT 'This should be an encrypted username using AES and HEX. ',
  `Password_vch` varchar(300) DEFAULT NULL COMMENT 'Encrypt password with same encryption key as username',
  `UserDefinedKey_vch` varchar(2048) DEFAULT NULL COMMENT 'User can upload their own unique identifier key for their contact',
  `Created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdated_dt` timestamp NULL DEFAULT NULL,
  `DataTrack_vch` varchar(30) DEFAULT NULL,
  `socialToken_vch` varchar(2048) DEFAULT NULL,
  `CPPID_vch` varchar(512) DEFAULT NULL,
  `CPPPWD_vch` blob,
  `CPP_UUID_vch` varchar(36) DEFAULT NULL,
  `CompanyUserId_int` int(11) NOT NULL DEFAULT '0',
  `LanguagePreference_vch` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ContactId_bi`),
  UNIQUE KEY `ContactId_bi_UNIQUE` (`ContactId_bi`),
  KEY `DataTrack` (`DataTrack_vch`,`UserName_vch`,`Password_vch`,`FirstName_vch`,`Address_vch`,`City_vch`,`UserDefinedKey_vch`(767),`UserId_int`),
  KEY `IDX_DT_UID` (`UserId_int`,`DataTrack_vch`),
  KEY `IDX_SocialToken` (`socialToken_vch`(767),`CPP_UUID_vch`,`UserId_int`),
  KEY `IDX_CPPID` (`CPPID_vch`,`UserId_int`,`CPP_UUID_vch`),
  KEY `IDX_UserId` (`UserId_int`),
  KEY `IDX_Combo_user_contactid` (`ContactId_bi`,`UserId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=71071 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:27
