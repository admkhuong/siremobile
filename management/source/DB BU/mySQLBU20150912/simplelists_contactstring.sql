CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactstring`
--

DROP TABLE IF EXISTS `contactstring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactstring` (
  `ContactAddressId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `ContactId_bi` bigint(20) DEFAULT NULL,
  `ContactString_vch` varchar(200) DEFAULT NULL,
  `OptIn_int` int(11) DEFAULT '0',
  `DoubleOptIn_int` int(11) DEFAULT '0',
  `OptOut_int` int(11) DEFAULT '0',
  `ContactOptIn_int` int(11) DEFAULT '0' COMMENT 'Mark This field as 1 if the Contact Themselves has opted in. Not uploaded by User.',
  `ContactType_int` int(11) DEFAULT '1',
  `TimeZone_int` int(11) DEFAULT '28',
  `City_vch` varchar(75) DEFAULT NULL,
  `State_vch` varchar(50) DEFAULT NULL,
  `OptIn_dt` datetime DEFAULT NULL,
  `OptInVerify_dt` datetime DEFAULT NULL,
  `OptOut_dt` datetime DEFAULT NULL,
  `CellFlag_int` int(11) DEFAULT '0',
  `Created_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastUpdated_dt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AltNum_int` int(11) DEFAULT '0',
  `UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`ContactAddressId_bi`),
  UNIQUE KEY `ContactAddressId_bi_UNIQUE` (`ContactAddressId_bi`),
  KEY `Cell` (`CellFlag_int`),
  KEY `ContactType` (`ContactType_int`,`City_vch`,`State_vch`,`TimeZone_int`,`ContactId_bi`,`ContactString_vch`),
  KEY `idx_contactid` (`ContactId_bi`,`ContactAddressId_bi`)
) ENGINE=InnoDB AUTO_INCREMENT=79258 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:25
