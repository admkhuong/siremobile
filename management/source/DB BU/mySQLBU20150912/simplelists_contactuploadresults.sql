CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactuploadresults`
--

DROP TABLE IF EXISTS `contactuploadresults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactuploadresults` (
  `ContactResultsId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL,
  `Created_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `TableName_vch` varchar(300) DEFAULT NULL,
  `GroupId_bi` bigint(20) DEFAULT NULL,
  `VoiceColumns_vch` varchar(300) DEFAULT NULL,
  `VoiceAltColumns_vch` varchar(300) DEFAULT NULL,
  `EmailColumns_vch` varchar(300) DEFAULT NULL,
  `EmailAltColumns_vch` varchar(300) DEFAULT NULL,
  `SMScolumns_vch` varchar(300) DEFAULT NULL,
  `SMSAltColumns_vch` varchar(300) DEFAULT NULL,
  `FaxColumns_vch` varchar(300) DEFAULT NULL,
  `FaxAltColumns_vch` varchar(300) DEFAULT NULL,
  `VariableColumns_vch` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ContactResultsId_bi`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:19
