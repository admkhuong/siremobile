CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customerpreferenceportal`
--

DROP TABLE IF EXISTS `customerpreferenceportal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerpreferenceportal` (
  `CPP_UUID_vch` varchar(36) NOT NULL,
  `UserId_int` int(11) NOT NULL,
  `Desc_vch` varchar(2048) DEFAULT NULL,
  `PrimaryLink_vch` varchar(1024) DEFAULT NULL,
  `Active_int` int(4) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LastModified_dt` datetime DEFAULT NULL,
  `CPP_Template_vch` text,
  `GroupId_int` int(11) NOT NULL DEFAULT '0',
  `IdentifyGroupId_int` int(11) DEFAULT '0',
  `SystemPassword_vch` blob,
  `CPPStyleTemplate_vch` text,
  `QRCode_blob` blob,
  `FacebookMsg_vch` varchar(2048) DEFAULT NULL,
  `TwitterMsg_vch` varchar(2048) DEFAULT NULL,
  `IPFilter_vch` text,
  `IFrameActive_int` int(11) DEFAULT '1',
  `ActiveApiAccess_int` int(11) DEFAULT '1',
  `Type_ti` tinyint(4) DEFAULT NULL,
  `MultiplePreference_ti` tinyint(4) DEFAULT '0',
  `UpdatePreference_ti` tinyint(4) DEFAULT '0',
  `VoiceMethod_ti` tinyint(4) DEFAULT '0',
  `SMSMethod_ti` tinyint(4) DEFAULT '0',
  `EmailMethod_ti` tinyint(4) DEFAULT '0',
  `IncludeLanguage_ti` tinyint(4) DEFAULT '0',
  `IncludeIdentity_ti` tinyint(4) DEFAULT '0',
  `StepSetup_vch` varchar(500) DEFAULT '2,3,4',
  `SetCustomValue_ti` tinyint(4) DEFAULT '0',
  `vanity_vch` varchar(255) DEFAULT NULL,
  `template_int` int(11) DEFAULT '0',
  `sizeType_int` int(11) DEFAULT '0',
  `width_int` int(11) DEFAULT '0',
  `height_int` int(11) DEFAULT '0',
  `displayType_ti` tinyint(4) DEFAULT '0',
  `domain_vch` varchar(255) DEFAULT NULL,
  `customhtml_txt` longtext,
  `VerificationBatchId_bi` bigint(20) DEFAULT '0',
  PRIMARY KEY (`CPP_UUID_vch`),
  UNIQUE KEY `CPP_UUID_vch_UNIQUE` (`CPP_UUID_vch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:08
