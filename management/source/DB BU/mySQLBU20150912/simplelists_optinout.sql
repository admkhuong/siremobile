CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `optinout`
--

DROP TABLE IF EXISTS `optinout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `optinout` (
  `OptId_int` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL,
  `ContactString_vch` varchar(255) DEFAULT NULL,
  `OptOut_dt` datetime DEFAULT NULL,
  `OptIn_dt` datetime DEFAULT NULL,
  `OptOutSource_vch` varchar(255) DEFAULT NULL,
  `OptInSource_vch` varchar(255) DEFAULT NULL,
  `ShortCode_vch` varchar(255) DEFAULT NULL,
  `FromAddress_vch` varchar(255) DEFAULT NULL,
  `CallerId_vch` varchar(20) DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT '0',
  PRIMARY KEY (`OptId_int`),
  KEY `IDX_Combo_Contact_CSC_In_Out` (`OptOut_dt`,`OptIn_dt`,`ShortCode_vch`,`ContactString_vch`),
  KEY `IDX_Combo_Contact_User_CSC_In_Out` (`UserId_int`,`OptOut_dt`,`OptIn_dt`,`ShortCode_vch`,`ContactString_vch`),
  KEY `IDX_Combo_Contact_FromAddress_In_out` (`ContactString_vch`,`OptOut_dt`,`OptIn_dt`,`FromAddress_vch`),
  KEY `IDX_Combo_Contact_User_In_Out` (`ContactString_vch`,`UserId_int`,`OptInSource_vch`,`OptOutSource_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=431330 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:09
