CREATE DATABASE  IF NOT EXISTS `simplelists` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplelists`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplelists
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rxmultilist`
--

DROP TABLE IF EXISTS `rxmultilist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rxmultilist` (
  `UserId_int` int(11) NOT NULL,
  `ContactTypeId_int` int(4) NOT NULL,
  `AutoId_int` int(11) NOT NULL AUTO_INCREMENT,
  `TimeZone_int` smallint(6) NOT NULL,
  `CellFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `OptInFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `SourceKey_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LastUpdated_dt` datetime DEFAULT NULL,
  `LastAccess_dt` datetime DEFAULT NULL,
  `CustomField1_int` decimal(11,4) DEFAULT NULL,
  `CustomField2_int` decimal(11,4) DEFAULT NULL,
  `CustomField3_int` decimal(11,4) DEFAULT NULL,
  `CustomField4_int` decimal(11,4) DEFAULT NULL,
  `CustomField5_int` decimal(11,4) DEFAULT NULL,
  `CustomField6_int` decimal(11,4) DEFAULT NULL,
  `CustomField7_int` decimal(11,4) DEFAULT NULL,
  `CustomField8_int` decimal(11,4) DEFAULT NULL,
  `CustomField9_int` decimal(11,4) DEFAULT NULL,
  `CustomField10_int` decimal(11,4) DEFAULT NULL,
  `UniqueCustomer_UUID_vch` varchar(36) DEFAULT NULL,
  `ContactString_vch` varchar(255) NOT NULL DEFAULT '',
  `LocationKey1_vch` varchar(255) DEFAULT NULL,
  `LocationKey2_vch` varchar(255) DEFAULT NULL,
  `LocationKey3_vch` varchar(255) DEFAULT NULL,
  `LocationKey4_vch` varchar(255) DEFAULT NULL,
  `LocationKey5_vch` varchar(255) DEFAULT NULL,
  `LocationKey6_vch` varchar(255) DEFAULT NULL,
  `LocationKey7_vch` varchar(255) DEFAULT NULL,
  `LocationKey8_vch` varchar(255) DEFAULT NULL,
  `LocationKey9_vch` varchar(255) DEFAULT NULL,
  `LocationKey10_vch` varchar(255) DEFAULT NULL,
  `SourceKey_vch` varchar(255) NOT NULL DEFAULT '',
  `GroupList_vch` varchar(512) NOT NULL DEFAULT '0,',
  `CustomField1_vch` varchar(2048) DEFAULT NULL,
  `CustomField2_vch` varchar(2048) DEFAULT NULL,
  `CustomField3_vch` varchar(2048) DEFAULT NULL,
  `CustomField4_vch` varchar(2048) DEFAULT NULL,
  `CustomField5_vch` varchar(2048) DEFAULT NULL,
  `CustomField6_vch` varchar(2048) DEFAULT NULL,
  `CustomField7_vch` varchar(2048) DEFAULT NULL,
  `CustomField8_vch` varchar(2048) DEFAULT NULL,
  `CustomField9_vch` varchar(2048) DEFAULT NULL,
  `CustomField10_vch` varchar(2048) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(2048) DEFAULT NULL,
  `SourceString_vch` text,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `socialToken_vch` varchar(2048) DEFAULT NULL,
  `CPPID_vch` varchar(512) NOT NULL DEFAULT '',
  `CPPPWD_vch` blob,
  `CPPPriority_vch` int(4) DEFAULT NULL,
  `CPPContactTimePref0_int` tinyint(4) NOT NULL DEFAULT '0',
  `CPPContactTimePref1_int` tinyint(4) NOT NULL DEFAULT '0',
  `CPPContactTimePref2_int` tinyint(4) NOT NULL DEFAULT '0',
  `CPPContactTimePref3_int` tinyint(4) NOT NULL DEFAULT '0',
  `CPPFrequencyFlag_int` tinyint(4) NOT NULL DEFAULT '0',
  `CPP_UUID_VCH` varchar(36) DEFAULT NULL COMMENT 'Cpp uuid',
  PRIMARY KEY (`UserId_int`,`ContactTypeId_int`,`ContactString_vch`,`SourceKey_vch`,`CPPID_vch`),
  UNIQUE KEY `IDX_AutoId` (`AutoId_int`),
  KEY `IDX_OptInFlag_int` (`OptInFlag_int`),
  KEY `IDX_GroupList_vch` (`GroupList_vch`),
  KEY `IDX_ContactTypeId_int` (`ContactTypeId_int`),
  KEY `IDX_Contact` (`ContactString_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=226436 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:07:20
