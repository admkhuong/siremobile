CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abcampaign`
--

DROP TABLE IF EXISTS `abcampaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abcampaign` (
  `ABCampaignId_bi` int(11) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL,
  `Desc_vch` text,
  `GroupId_bi` bigint(20) DEFAULT NULL,
  `TestPercentage_int` int(4) DEFAULT '10',
  `a_BatchId_bi` int(11) DEFAULT NULL,
  `a_Percentage_int` int(4) DEFAULT '0',
  `b_BatchId_bi` int(11) DEFAULT NULL,
  `b_Percentage_int` int(4) DEFAULT '0',
  `c_Batchid_bi` int(11) DEFAULT NULL,
  `c_Percentage_int` int(4) DEFAULT '0',
  `Created_dt` datetime DEFAULT NULL,
  `LastUpdated_dt` datetime DEFAULT NULL,
  `AutoSend_int` int(2) DEFAULT NULL,
  `EmailResponse_vch` varchar(255) DEFAULT NULL,
  `SMSResponse_vch` varchar(255) DEFAULT NULL,
  `VoiceResponse_vch` varchar(255) DEFAULT NULL,
  `Active_int` int(11) DEFAULT '1',
  PRIMARY KEY (`ABCampaignId_bi`),
  UNIQUE KEY `ABCampaignId_bi_UNIQUE` (`ABCampaignId_bi`),
  KEY `ActiveUserCombo_AB` (`UserId_int`,`Active_int`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:17
