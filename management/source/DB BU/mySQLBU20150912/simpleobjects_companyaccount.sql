CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `companyaccount`
--

DROP TABLE IF EXISTS `companyaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companyaccount` (
  `CompanyAccountId_int` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName_vch` text,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `EmailAddressVerified_vch` varchar(100) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `PrimaryPhoneStrVerified_ti` tinyint(4) NOT NULL DEFAULT '0',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Reactivate_int` tinyint(4) DEFAULT '0',
  `Manager_int` tinyint(4) DEFAULT '0',
  `UserIp_vch` varchar(255) DEFAULT NULL,
  `Balance_fl` float DEFAULT NULL,
  `RateType_int` int(4) DEFAULT '1',
  `Rate1_int` float DEFAULT '0.05',
  `Rate2_int` float DEFAULT '0.05',
  `Rate3_int` float DEFAULT '0.05',
  `ContactNamePoint_vch` varchar(255) DEFAULT '0',
  `ContactNumberPoint_vch` varchar(255) DEFAULT '0',
  `ContactEmailPoint_vch` varchar(255) DEFAULT '0',
  `UnlimitedBalance_ti` tinyint(4) DEFAULT '0',
  `DefaultCID_vch` varchar(15) DEFAULT NULL,
  `SharedAccountUserId_int` int(11) NOT NULL DEFAULT '0',
  `LinkDirUUID_vch` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CompanyAccountId_int`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:14
