CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_programoverview`
--

DROP TABLE IF EXISTS `pm_programoverview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_programoverview` (
  `BatchId_bi` int(11) NOT NULL,
  `PROJECTDESC_VCH` text,
  `TYPE_OF_CALL_VCH` varchar(100) DEFAULT NULL,
  `PLATFORM_ATT_INT` int(11) DEFAULT '0',
  `Precall_to_Email_int` int(11) DEFAULT '0',
  `Standalone_Call_int` int(11) DEFAULT '0',
  `PreCall_To_Direct_Mail_int` int(11) DEFAULT '0',
  `Postcall_To_Direct_Mail_int` int(11) DEFAULT '0',
  `Drive_To_Web_int` int(11) DEFAULT '0',
  `Informational_int` int(11) DEFAULT '0',
  `Other_int` int(11) DEFAULT '0',
  `Other_vch` varchar(1000) DEFAULT NULL,
  `AL` int(11) DEFAULT '0',
  `AK` int(11) DEFAULT '0',
  `AZ` int(11) DEFAULT '0',
  `AR` int(11) DEFAULT '0',
  `CA` int(11) DEFAULT '0',
  `CO` int(11) DEFAULT '0',
  `CT` int(11) DEFAULT '0',
  `DE` int(11) DEFAULT '0',
  `FL` int(11) DEFAULT '0',
  `GA` int(11) DEFAULT '0',
  `HI` int(11) DEFAULT '0',
  `ID` int(11) DEFAULT '0',
  `IL` int(11) DEFAULT '0',
  `nIN` int(11) DEFAULT '0',
  `IA` int(11) DEFAULT '0',
  `KS` int(11) DEFAULT '0',
  `KY` int(11) DEFAULT '0',
  `LA` int(11) DEFAULT '0',
  `ME` int(11) DEFAULT '0',
  `MD` int(11) DEFAULT '0',
  `MA` int(11) DEFAULT '0',
  `MI` int(11) DEFAULT '0',
  `MN` int(11) DEFAULT '0',
  `MS` int(11) DEFAULT '0',
  `MO` int(11) DEFAULT '0',
  `MT` int(11) DEFAULT '0',
  `NE` int(11) DEFAULT '0',
  `NV` int(11) DEFAULT '0',
  `NH` int(11) DEFAULT '0',
  `NJ` int(11) DEFAULT '0',
  `NM` int(11) DEFAULT '0',
  `NY` int(11) DEFAULT '0',
  `NC` int(11) DEFAULT '0',
  `ND` int(11) DEFAULT '0',
  `OH` int(11) DEFAULT '0',
  `OK` int(11) DEFAULT '0',
  `zOR` int(11) DEFAULT '0',
  `PA` int(11) DEFAULT '0',
  `RI` int(11) DEFAULT '0',
  `SC` int(11) DEFAULT '0',
  `SD` int(11) DEFAULT '0',
  `TN` int(11) DEFAULT '0',
  `TX` int(11) DEFAULT '0',
  `UT` int(11) DEFAULT '0',
  `VT` int(11) DEFAULT '0',
  `VA` int(11) DEFAULT '0',
  `WA` int(11) DEFAULT '0',
  `WV` int(11) DEFAULT '0',
  `WI` int(11) DEFAULT '0',
  `WY` int(11) DEFAULT '0',
  `Customer_Base_Con_int` int(11) DEFAULT NULL,
  `PLATFORM_MB_INT` int(11) DEFAULT '0',
  `PLATFORM_EGS_INT` int(11) DEFAULT '0',
  `Customer_Base_Bus_int` int(11) DEFAULT '0',
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:51
