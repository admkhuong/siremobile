CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_projectdatafeeds`
--

DROP TABLE IF EXISTS `pm_projectdatafeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_projectdatafeeds` (
  `BatchId_bi` bigint(20) NOT NULL,
  `Data_Requirements_vch` text,
  `IT_Interface_Signoff_vch` varchar(1000) DEFAULT NULL,
  `Call_Results_Processing_vch` varchar(1000) DEFAULT NULL,
  `Call_yes_vch` varchar(5000) DEFAULT NULL,
  `Data_Exchange_Develop_vch` varchar(1000) DEFAULT NULL,
  `Data_Source_vch` varchar(1000) DEFAULT NULL,
  `Do_Not_Scrub_int` int(11) DEFAULT '0',
  `State_DNC_List_int` int(11) DEFAULT '0',
  `Client_DNC_int` int(11) DEFAULT '0',
  `National_DNC_int` int(11) DEFAULT '0',
  `Company_Internal_list_int` int(11) DEFAULT '0',
  `Cell_Phone_List_int` int(11) DEFAULT '0',
  `Data_File_Transmission_vch` varchar(5000) DEFAULT NULL,
  `PDFNotes_vch` text,
  `Filter_Duplicate_Records_int` int(11) DEFAULT '0',
  `Filter_Within_file_int` int(11) DEFAULT '0',
  `Filter_Same_Day_int` int(11) DEFAULT '0',
  `Filter_X_Amount_Days_int` int(11) DEFAULT '0',
  `X_Days_Number_int` int(11) DEFAULT '0',
  `Do_Not_Dial_States_int` int(11) DEFAULT '0',
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:41
