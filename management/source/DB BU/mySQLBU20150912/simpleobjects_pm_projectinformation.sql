CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_projectinformation`
--

DROP TABLE IF EXISTS `pm_projectinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_projectinformation` (
  `BatchId_bi` bigint(20) NOT NULL,
  `ProjectLeadContact_vch` varchar(1024) DEFAULT NULL,
  `ProjectID_vch` varchar(300) DEFAULT NULL,
  `Version_vch` varchar(300) DEFAULT NULL,
  `Platform_Type_vch` varchar(45) DEFAULT NULL,
  `Project_Duration_vch` varchar(45) DEFAULT NULL,
  `Submission_Date_dt` datetime DEFAULT NULL,
  `Project_Start_Date_dt` datetime DEFAULT NULL,
  `Project_End_Date_dt` datetime DEFAULT NULL,
  `Prepared_By_vch` varchar(1000) DEFAULT NULL,
  `Email_Phone_vch` varchar(1000) DEFAULT NULL,
  `Script_Content_Contact_vch` varchar(1000) DEFAULT NULL,
  `Data_Feed_Contact_vch` varchar(1000) DEFAULT NULL,
  `MandP_Contact_vch` varchar(1000) DEFAULT NULL,
  `Project_Team_vch` varchar(1000) DEFAULT NULL,
  `Communications_Alert_vch` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:52
