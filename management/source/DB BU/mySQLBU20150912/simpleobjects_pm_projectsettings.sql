CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_projectsettings`
--

DROP TABLE IF EXISTS `pm_projectsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_projectsettings` (
  `BatchId_bi` bigint(20) NOT NULL,
  `MFDialingHours_vch` varchar(1000) DEFAULT NULL,
  `SaturdayDialingHours_vch` varchar(500) DEFAULT NULL,
  `SundayDialingHours_vch` varchar(500) DEFAULT NULL,
  `HolidayDialingHours_vch` varchar(500) DEFAULT NULL,
  `DailyCallVolumes_vch` varchar(1000) DEFAULT NULL,
  `ConsumerCallerID_vch` varchar(500) DEFAULT NULL,
  `BusinessCallerID_vch` varchar(500) DEFAULT NULL,
  `SpanishOption_vch` varchar(500) DEFAULT NULL,
  `AlternateNumberDialing_vch` varchar(1000) DEFAULT NULL,
  `PhoneNumToBeDialed_vch` varchar(1000) DEFAULT NULL,
  `RedialLogic_vch` varchar(1000) DEFAULT NULL,
  `RedialAttempts_vch` varchar(1000) DEFAULT NULL,
  `RolloverDialing_vch` varchar(45) DEFAULT NULL,
  `AnswerMachineStrategy_vch` varchar(1000) DEFAULT NULL,
  `MaxAttempts_int` int(11) DEFAULT '0',
  `RingCount_int` int(11) DEFAULT '0',
  `AnswerMachineWait_vch` varchar(1000) DEFAULT NULL,
  `BusyCallbackTime_vch` varchar(1000) DEFAULT NULL,
  `BCBTimeRedialTime_vch` varchar(500) DEFAULT NULL,
  `NoAnswerCallbackTime_vch` varchar(1000) DEFAULT NULL,
  `NACBTimeRedialTime_vch` varchar(500) DEFAULT NULL,
  `OtherFax_vch` varchar(500) DEFAULT NULL,
  `OFTimeRedialTime_vch` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:55
