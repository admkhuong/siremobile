CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_scriptstollfree`
--

DROP TABLE IF EXISTS `pm_scriptstollfree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_scriptstollfree` (
  `BatchId_bi` bigint(20) NOT NULL,
  `ALCTF_vch` varchar(20) DEFAULT NULL,
  `ALBTF_vch` varchar(20) DEFAULT NULL,
  `ALTF_vch` varchar(45) DEFAULT NULL,
  `AKTF_vch` varchar(45) DEFAULT NULL,
  `ARTF_vch` varchar(45) DEFAULT NULL,
  `AZTF_vch` varchar(45) DEFAULT NULL,
  `CATF_vch` varchar(45) DEFAULT NULL,
  `COTF_vch` varchar(45) DEFAULT NULL,
  `CTTF_vch` varchar(45) DEFAULT NULL,
  `DETF_vch` varchar(45) DEFAULT NULL,
  `FLTF_vch` varchar(45) DEFAULT NULL,
  `GATF_vch` varchar(45) DEFAULT NULL,
  `HITF_vch` varchar(45) DEFAULT NULL,
  `IDTF_vch` varchar(45) DEFAULT NULL,
  `ILTF_vch` varchar(45) DEFAULT NULL,
  `INTF_vch` varchar(45) DEFAULT NULL,
  `IATF_vch` varchar(45) DEFAULT NULL,
  `KSTF_vch` varchar(45) DEFAULT NULL,
  `KYTF_vch` varchar(45) DEFAULT NULL,
  `LATF_vch` varchar(45) DEFAULT NULL,
  `METF_vch` varchar(45) DEFAULT NULL,
  `MDTF_vch` varchar(45) DEFAULT NULL,
  `MATF_vch` varchar(45) DEFAULT NULL,
  `MITF_vch` varchar(45) DEFAULT NULL,
  `MNTF_vch` varchar(45) DEFAULT NULL,
  `MSTF_vch` varchar(45) DEFAULT NULL,
  `MOTF_vch` varchar(45) DEFAULT NULL,
  `MTTF_vch` varchar(45) DEFAULT NULL,
  `NETF_vch` varchar(45) DEFAULT NULL,
  `NVTF_vch` varchar(45) DEFAULT NULL,
  `NHTF_vch` varchar(45) DEFAULT NULL,
  `NJTF_vch` varchar(45) DEFAULT NULL,
  `NMTF_vch` varchar(45) DEFAULT NULL,
  `NYTF_vch` varchar(45) DEFAULT NULL,
  `NCTF_vch` varchar(45) DEFAULT NULL,
  `NDTF_vch` varchar(45) DEFAULT NULL,
  `OHTF_vch` varchar(45) DEFAULT NULL,
  `OKTF_vch` varchar(45) DEFAULT NULL,
  `ORTF_vch` varchar(45) DEFAULT NULL,
  `PATF_vch` varchar(45) DEFAULT NULL,
  `RITF_vch` varchar(45) DEFAULT NULL,
  `SCTF_vch` varchar(45) DEFAULT NULL,
  `SDTF_vch` varchar(45) DEFAULT NULL,
  `TNTF_vch` varchar(45) DEFAULT NULL,
  `TXTF_vch` varchar(45) DEFAULT NULL,
  `UTTF_vch` varchar(45) DEFAULT NULL,
  `VTTF_vch` varchar(45) DEFAULT NULL,
  `VATF_vch` varchar(45) DEFAULT NULL,
  `WATF_vch` varchar(45) DEFAULT NULL,
  `WVTF_vch` varchar(45) DEFAULT NULL,
  `WITF_vch` varchar(45) DEFAULT NULL,
  `WYTF_vch` varchar(45) DEFAULT NULL,
  `AKCTF_vch` varchar(45) DEFAULT NULL,
  `AKBTF_vch` varchar(45) DEFAULT NULL,
  `ARCTF_vch` varchar(45) DEFAULT NULL,
  `ARBTF_vch` varchar(45) DEFAULT NULL,
  `AZBTF_vch` varchar(45) DEFAULT NULL,
  `AZCTF_vch` varchar(45) DEFAULT NULL,
  `CABTF_vch` varchar(45) DEFAULT NULL,
  `CACTF_vch` varchar(45) DEFAULT NULL,
  `COCTF_vch` varchar(45) DEFAULT NULL,
  `COBTF_vch` varchar(45) DEFAULT NULL,
  `DEBTF_vch` varchar(45) DEFAULT NULL,
  `DECTF_vch` varchar(45) DEFAULT NULL,
  `CTBTF_vch` varchar(45) DEFAULT NULL,
  `CTCTF_vch` varchar(45) DEFAULT NULL,
  `FLBTF_vch` varchar(45) DEFAULT NULL,
  `FLCTF_vch` varchar(45) DEFAULT NULL,
  `GABTF_vch` varchar(45) DEFAULT NULL,
  `GACTF_vch` varchar(45) DEFAULT NULL,
  `HIBTF_vch` varchar(45) DEFAULT NULL,
  `HICTF_vch` varchar(45) DEFAULT NULL,
  `IDBTF_vch` varchar(45) DEFAULT NULL,
  `IDCTF_vch` varchar(45) DEFAULT NULL,
  `ILBTF_vch` varchar(45) DEFAULT NULL,
  `ILCTF_vch` varchar(45) DEFAULT NULL,
  `INBTF_vch` varchar(45) DEFAULT NULL,
  `INCTF_vch` varchar(45) DEFAULT NULL,
  `IABTF_vch` varchar(45) DEFAULT NULL,
  `IACTF_vch` varchar(45) DEFAULT NULL,
  `KSBTF_vch` varchar(45) DEFAULT NULL,
  `KSCTF_vch` varchar(45) DEFAULT NULL,
  `LABTF_vch` varchar(45) DEFAULT NULL,
  `LACTF_vch` varchar(45) DEFAULT NULL,
  `MEBTF_vch` varchar(45) DEFAULT NULL,
  `MECTF_vch` varchar(45) DEFAULT NULL,
  `MDBTF_vch` varchar(45) DEFAULT NULL,
  `MDCTF_vch` varchar(45) DEFAULT NULL,
  `MABTF_vch` varchar(45) DEFAULT NULL,
  `MACTF_vch` varchar(45) DEFAULT NULL,
  `MIBTF_vch` varchar(45) DEFAULT NULL,
  `MICTF_vch` varchar(45) DEFAULT NULL,
  `MNBTF_vch` varchar(45) DEFAULT NULL,
  `MNCTF_vch` varchar(45) DEFAULT NULL,
  `MSBTF_vch` varchar(45) DEFAULT NULL,
  `MSCTF_vch` varchar(45) DEFAULT NULL,
  `MOBTF_vch` varchar(45) DEFAULT NULL,
  `MOCTF_vch` varchar(45) DEFAULT NULL,
  `MTBTF_vch` varchar(45) DEFAULT NULL,
  `MTCTF_vch` varchar(45) DEFAULT NULL,
  `NEBTF_vch` varchar(45) DEFAULT NULL,
  `NECTF_vch` varchar(45) DEFAULT NULL,
  `NVBTF_vch` varchar(45) DEFAULT NULL,
  `NVCTF_vch` varchar(45) DEFAULT NULL,
  `NHBTF_vch` varchar(45) DEFAULT NULL,
  `NHCTF_vch` varchar(45) DEFAULT NULL,
  `NJBTF_vch` varchar(45) DEFAULT NULL,
  `NJCTF_vch` varchar(45) DEFAULT NULL,
  `NMBTF_vch` varchar(45) DEFAULT NULL,
  `NMCTF_vch` varchar(45) DEFAULT NULL,
  `NYBTF_vch` varchar(45) DEFAULT NULL,
  `NYCTF_vch` varchar(45) DEFAULT NULL,
  `NCBTF_vch` varchar(45) DEFAULT NULL,
  `NCCTF_vch` varchar(45) DEFAULT NULL,
  `NDBTF_vch` varchar(45) DEFAULT NULL,
  `NDCTF_vch` varchar(45) DEFAULT NULL,
  `OHBTF_vch` varchar(45) DEFAULT NULL,
  `OHCTF_vch` varchar(45) DEFAULT NULL,
  `OKBTF_vch` varchar(45) DEFAULT NULL,
  `OKCTF_vch` varchar(45) DEFAULT NULL,
  `ORBTF_vch` varchar(45) DEFAULT NULL,
  `ORCTF_vch` varchar(45) DEFAULT NULL,
  `PABTF_vch` varchar(45) DEFAULT NULL,
  `PACTF_vch` varchar(45) DEFAULT NULL,
  `RIBTF_vch` varchar(45) DEFAULT NULL,
  `RICTF_vch` varchar(45) DEFAULT NULL,
  `SCBTF_vch` varchar(45) DEFAULT NULL,
  `SCCTF_vch` varchar(45) DEFAULT NULL,
  `SDBTF_vch` varchar(45) DEFAULT NULL,
  `SDCTF_vch` varchar(45) DEFAULT NULL,
  `TNBTF_vch` varchar(45) DEFAULT NULL,
  `TNCTF_vch` varchar(45) DEFAULT NULL,
  `TXBTF_vch` varchar(45) DEFAULT NULL,
  `TXCTF_vch` varchar(45) DEFAULT NULL,
  `UTBTF_vch` varchar(45) DEFAULT NULL,
  `UTCTF_vch` varchar(45) DEFAULT NULL,
  `VTBTF_vch` varchar(45) DEFAULT NULL,
  `VTCTF_vch` varchar(45) DEFAULT NULL,
  `VABTF_vch` varchar(45) DEFAULT NULL,
  `VACTF_vch` varchar(45) DEFAULT NULL,
  `WABTF_vch` varchar(45) DEFAULT NULL,
  `WACTF_vch` varchar(45) DEFAULT NULL,
  `WVBTF_vch` varchar(45) DEFAULT NULL,
  `WVCTF_vch` varchar(45) DEFAULT NULL,
  `WIBTF_vch` varchar(45) DEFAULT NULL,
  `WICTF_vch` varchar(45) DEFAULT NULL,
  `WYBTF_vch` varchar(45) DEFAULT NULL,
  `WYCTF_vch` varchar(45) DEFAULT NULL,
  `KYBTF_vch` varchar(45) DEFAULT NULL,
  `KYCTF_vch` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:00
