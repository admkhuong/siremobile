CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pm_scriptstransfer`
--

DROP TABLE IF EXISTS `pm_scriptstransfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pm_scriptstransfer` (
  `BatchId_bi` bigint(20) NOT NULL,
  `ALBTN_vch` varchar(45) DEFAULT NULL,
  `ALCTN_vch` varchar(45) DEFAULT NULL,
  `AL_vch` varchar(45) DEFAULT NULL,
  `AK_vch` varchar(45) DEFAULT NULL,
  `AZ_vch` varchar(45) DEFAULT NULL,
  `AR_vch` varchar(45) DEFAULT NULL,
  `CA_vch` varchar(45) DEFAULT NULL,
  `CO_vch` varchar(45) DEFAULT NULL,
  `CT_vch` varchar(45) DEFAULT NULL,
  `DE_vch` varchar(45) DEFAULT NULL,
  `FL_vch` varchar(45) DEFAULT NULL,
  `GA_vch` varchar(45) DEFAULT NULL,
  `HI_vch` varchar(45) DEFAULT NULL,
  `ID_vch` varchar(45) DEFAULT NULL,
  `IL_vch` varchar(45) DEFAULT NULL,
  `IN_vch` varchar(45) DEFAULT NULL,
  `IA_vch` varchar(45) DEFAULT NULL,
  `KS_vch` varchar(45) DEFAULT NULL,
  `KY_vch` varchar(45) DEFAULT NULL,
  `LA_vch` varchar(45) DEFAULT NULL,
  `ME_vch` varchar(45) DEFAULT NULL,
  `MD_vch` varchar(45) DEFAULT NULL,
  `MA_vch` varchar(45) DEFAULT NULL,
  `MI_vch` varchar(45) DEFAULT NULL,
  `MN_vch` varchar(45) DEFAULT NULL,
  `MS_vch` varchar(45) DEFAULT NULL,
  `MO_vch` varchar(45) DEFAULT NULL,
  `MT_vch` varchar(45) DEFAULT NULL,
  `NE_vch` varchar(45) DEFAULT NULL,
  `NV_vch` varchar(45) DEFAULT NULL,
  `NH_vch` varchar(45) DEFAULT NULL,
  `NJ_vch` varchar(45) DEFAULT NULL,
  `NM_vch` varchar(45) DEFAULT NULL,
  `NY_vch` varchar(45) DEFAULT NULL,
  `NC_vch` varchar(45) DEFAULT NULL,
  `ND_vch` varchar(45) DEFAULT NULL,
  `OH_vch` varchar(45) DEFAULT NULL,
  `OK_vch` varchar(45) DEFAULT NULL,
  `OR_vch` varchar(45) DEFAULT NULL,
  `PA_vch` varchar(45) DEFAULT NULL,
  `RI_vch` varchar(45) DEFAULT NULL,
  `SC_vch` varchar(45) DEFAULT NULL,
  `SD_vch` varchar(45) DEFAULT NULL,
  `TN_vch` varchar(45) DEFAULT NULL,
  `TX_vch` varchar(45) DEFAULT NULL,
  `UT_vch` varchar(45) DEFAULT NULL,
  `VT_vch` varchar(45) DEFAULT NULL,
  `VA_vch` varchar(45) DEFAULT NULL,
  `WA_vch` varchar(45) DEFAULT NULL,
  `WV_vch` varchar(45) DEFAULT NULL,
  `WI_vch` varchar(45) DEFAULT NULL,
  `WY_vch` varchar(45) DEFAULT NULL,
  `AKBTN_vch` varchar(45) DEFAULT NULL,
  `AKCTN_vch` varchar(45) DEFAULT NULL,
  `AZBTN_vch` varchar(45) DEFAULT NULL,
  `AZCTN_vch` varchar(45) DEFAULT NULL,
  `ARBTN_vch` varchar(45) DEFAULT NULL,
  `ARCTN_vch` varchar(45) DEFAULT NULL,
  `CABTN_vch` varchar(45) DEFAULT NULL,
  `CACTN_vch` varchar(45) DEFAULT NULL,
  `COBTN_vch` varchar(45) DEFAULT NULL,
  `COCTN_vch` varchar(45) DEFAULT NULL,
  `CTBTN_vch` varchar(45) DEFAULT NULL,
  `CTCTN_vch` varchar(45) DEFAULT NULL,
  `DEBTN_vch` varchar(45) DEFAULT NULL,
  `DECTN_vch` varchar(45) DEFAULT NULL,
  `FLBTN_vch` varchar(45) DEFAULT NULL,
  `FLCTN_vch` varchar(45) DEFAULT NULL,
  `GABTN_vch` varchar(45) DEFAULT NULL,
  `GACTN_vch` varchar(45) DEFAULT NULL,
  `HIBTN_vch` varchar(45) DEFAULT NULL,
  `HICTN_vch` varchar(45) DEFAULT NULL,
  `IDBTN_vch` varchar(45) DEFAULT NULL,
  `IDCTN_vch` varchar(45) DEFAULT NULL,
  `ILBTN_vch` varchar(45) DEFAULT NULL,
  `ILCTN_vch` varchar(45) DEFAULT NULL,
  `INBTN_vch` varchar(45) DEFAULT NULL,
  `INCTN_vch` varchar(45) DEFAULT NULL,
  `IABTN_vch` varchar(45) DEFAULT NULL,
  `IACTN_vch` varchar(45) DEFAULT NULL,
  `KSBTN_vch` varchar(45) DEFAULT NULL,
  `KSCTN_vch` varchar(45) DEFAULT NULL,
  `KYBTN_vch` varchar(45) DEFAULT NULL,
  `KYCTN_vch` varchar(45) DEFAULT NULL,
  `LABTN_vch` varchar(45) DEFAULT NULL,
  `LACTN_vch` varchar(45) DEFAULT NULL,
  `MEBTN_vch` varchar(45) DEFAULT NULL,
  `MECTN_vch` varchar(45) DEFAULT NULL,
  `MDBTN_vch` varchar(45) DEFAULT NULL,
  `MDCTN_vch` varchar(45) DEFAULT NULL,
  `MABTN_vch` varchar(45) DEFAULT NULL,
  `MACTN_vch` varchar(45) DEFAULT NULL,
  `MIBTN_vch` varchar(45) DEFAULT NULL,
  `MICTN_vch` varchar(45) DEFAULT NULL,
  `MNBTN_vch` varchar(45) DEFAULT NULL,
  `MNCTN_vch` varchar(45) DEFAULT NULL,
  `MSBTN_vch` varchar(45) DEFAULT NULL,
  `MSCTN_vch` varchar(45) DEFAULT NULL,
  `MOBTN_vch` varchar(45) DEFAULT NULL,
  `MOCTN_vch` varchar(45) DEFAULT NULL,
  `MTBTN_vch` varchar(45) DEFAULT NULL,
  `MTCTN_vch` varchar(45) DEFAULT NULL,
  `NEBTN_vch` varchar(45) DEFAULT NULL,
  `NECTN_vch` varchar(45) DEFAULT NULL,
  `NVBTN_vch` varchar(45) DEFAULT NULL,
  `NVCTN_vch` varchar(45) DEFAULT NULL,
  `NHBTN_vch` varchar(45) DEFAULT NULL,
  `NHCTN_vch` varchar(45) DEFAULT NULL,
  `NJBTN_vch` varchar(45) DEFAULT NULL,
  `NJCTN_vch` varchar(45) DEFAULT NULL,
  `NMBTN_vch` varchar(45) DEFAULT NULL,
  `NMCTN_vch` varchar(45) DEFAULT NULL,
  `NYBTN_vch` varchar(45) DEFAULT NULL,
  `NYCTN_vch` varchar(45) DEFAULT NULL,
  `NCBTN_vch` varchar(45) DEFAULT NULL,
  `NCCTN_vch` varchar(45) DEFAULT NULL,
  `NDBTN_vch` varchar(45) DEFAULT NULL,
  `NDCTN_vch` varchar(45) DEFAULT NULL,
  `OHBTN_vch` varchar(45) DEFAULT NULL,
  `OHCTN_vch` varchar(45) DEFAULT NULL,
  `OKBTN_vch` varchar(45) DEFAULT NULL,
  `OKCTN_vch` varchar(45) DEFAULT NULL,
  `ORBTN_vch` varchar(45) DEFAULT NULL,
  `ORCTN_vch` varchar(45) DEFAULT NULL,
  `PABTN_vch` varchar(45) DEFAULT NULL,
  `PACTN_vch` varchar(45) DEFAULT NULL,
  `RIBTN_vch` varchar(45) DEFAULT NULL,
  `RICTN_vch` varchar(45) DEFAULT NULL,
  `SCBTN_vch` varchar(45) DEFAULT NULL,
  `SCCTN_vch` varchar(45) DEFAULT NULL,
  `SDBTN_vch` varchar(45) DEFAULT NULL,
  `SDCTN_vch` varchar(45) DEFAULT NULL,
  `TNBTN_vch` varchar(45) DEFAULT NULL,
  `TNCTN_vch` varchar(45) DEFAULT NULL,
  `TXBTN_vch` varchar(45) DEFAULT NULL,
  `TXCTN_vch` varchar(45) DEFAULT NULL,
  `UTBTN_vch` varchar(45) DEFAULT NULL,
  `UTCTN_vch` varchar(45) DEFAULT NULL,
  `VTBTN_vch` varchar(45) DEFAULT NULL,
  `VTCTN_vch` varchar(45) DEFAULT NULL,
  `VABTN_vch` varchar(45) DEFAULT NULL,
  `VACTN_vch` varchar(45) DEFAULT NULL,
  `WABTN_vch` varchar(45) DEFAULT NULL,
  `WACTN_vch` varchar(45) DEFAULT NULL,
  `WVBTN_vch` varchar(45) DEFAULT NULL,
  `WVCTN_vch` varchar(45) DEFAULT NULL,
  `WIBTN_vch` varchar(45) DEFAULT NULL,
  `WICTN_vch` varchar(45) DEFAULT NULL,
  `WYBTN_vch` varchar(45) DEFAULT NULL,
  `WYCTN_vch` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:42
