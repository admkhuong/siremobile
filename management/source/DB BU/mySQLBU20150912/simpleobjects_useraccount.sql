CREATE DATABASE  IF NOT EXISTS `simpleobjects` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simpleobjects`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simpleobjects
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `useraccount`
--

DROP TABLE IF EXISTS `useraccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount` (
  `UserId_int` int(11) NOT NULL AUTO_INCREMENT,
  `SOCIALMEDIAID_BI` bigint(20) DEFAULT '0',
  `CompanyAccountId_int` int(11) DEFAULT NULL,
  `UserName_vch` varchar(255) NOT NULL DEFAULT '',
  `Password_vch` blob NOT NULL,
  `AltUserName_vch` varchar(255) DEFAULT NULL,
  `AltPassword_vch` varchar(255) DEFAULT NULL,
  `UserLevel_int` int(11) NOT NULL DEFAULT '0',
  `Desc_vch` varchar(255) DEFAULT NULL,
  `ClientServicesReps_vch` varchar(2048) DEFAULT NULL,
  `PrimaryRep_vch` varchar(1024) DEFAULT NULL,
  `CSRNotes_vch` varchar(8000) DEFAULT NULL,
  `FirstName_vch` varchar(255) DEFAULT NULL,
  `LastName_vch` varchar(255) DEFAULT NULL,
  `Address1_vch` varchar(255) DEFAULT NULL,
  `Address2_vch` varchar(255) DEFAULT NULL,
  `City_vch` varchar(255) DEFAULT NULL,
  `State_vch` varchar(255) DEFAULT NULL,
  `Country_vch` varchar(255) DEFAULT NULL,
  `PostalCode_vch` varchar(255) DEFAULT NULL,
  `EmailAddress_vch` varchar(2000) DEFAULT NULL,
  `EmailAddressVerified_vch` varchar(100) DEFAULT NULL,
  `CompanyName_vch` varchar(255) DEFAULT NULL,
  `HomePhoneId_int` int(11) DEFAULT NULL,
  `WorkPhoneId_int` int(11) DEFAULT NULL,
  `AlternatePhoneId_int` int(11) DEFAULT NULL,
  `FaxPhoneId_int` int(11) DEFAULT NULL,
  `PrimaryPhoneStr_vch` varchar(255) NOT NULL DEFAULT '',
  `PrimaryPhoneStrVerified_ti` tinyint(4) NOT NULL DEFAULT '0',
  `HomePhoneStr_vch` varchar(255) DEFAULT NULL,
  `WorkPhoneStr_vch` varchar(255) DEFAULT NULL,
  `AlternatePhoneStr_vch` varchar(255) DEFAULT NULL,
  `FaxPhoneStr_vch` varchar(255) DEFAULT NULL,
  `CBPId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime NOT NULL,
  `LastLogIn_dt` datetime DEFAULT NULL,
  `Public_int` int(11) DEFAULT NULL,
  `Active_int` int(11) DEFAULT NULL,
  `Reactivate_int` tinyint(4) DEFAULT '0',
  `Manager_int` tinyint(4) DEFAULT '0',
  `UserIp_vch` varchar(255) DEFAULT NULL,
  `VerifyPasswordCode_vch` varchar(255) DEFAULT NULL,
  `Token_vch` varchar(128) DEFAULT NULL,
  `SocialToken_vch` text,
  `VerifiedDate_dt` datetime DEFAULT NULL,
  `registerStep_int` int(11) DEFAULT '0',
  `extensionWork_vch` varchar(45) DEFAULT NULL,
  `CreditCardVerified_int` int(11) DEFAULT NULL,
  `terms1_int` int(11) DEFAULT NULL,
  `terms2_int` int(11) DEFAULT NULL,
  `terms3_int` int(11) DEFAULT NULL,
  `terms4_int` int(11) DEFAULT NULL,
  `SignName_vch` varchar(150) DEFAULT NULL,
  `terms1Name_vch` varchar(100) DEFAULT NULL,
  `terms2Name_vch` varchar(100) DEFAULT NULL,
  `MailingAddress1_vch` varchar(255) DEFAULT NULL,
  `MailingAddress2_vch` varchar(255) DEFAULT NULL,
  `MailingCity_vch` varchar(255) DEFAULT NULL,
  `MailingState_vch` varchar(255) DEFAULT NULL,
  `MailingPostalCode_vch` varchar(255) DEFAULT NULL,
  `MailingCountry_vch` varchar(255) DEFAULT NULL,
  `FacebookUserName_vch` varchar(512) DEFAULT NULL,
  `TwitterUserName_vch` varchar(512) DEFAULT NULL,
  `GoogleUserName_vch` varchar(512) DEFAULT NULL,
  `SMSNOTIFICATIONS_INT` tinyint(3) unsigned DEFAULT NULL,
  `EmailNotifications_int` tinyint(3) unsigned DEFAULT NULL,
  `AvatarImg_blob` blob,
  `UseGravatarFlag_int` int(2) DEFAULT '0',
  `Role_int` int(11) DEFAULT NULL,
  `AndroidDeviceToken_vch` varchar(255) DEFAULT NULL,
  `IPhoneDeviceToken_vch` varchar(255) DEFAULT NULL,
  `Extension_vch` varchar(10) DEFAULT NULL,
  `CompanyUserId_int` int(11) DEFAULT NULL,
  `Position_vch` varchar(255) DEFAULT NULL,
  `Department_vch` varchar(255) DEFAULT NULL,
  `timezone_vch` varchar(100) DEFAULT NULL,
  `fund_limit` int(11) DEFAULT NULL,
  `fbuserid_bi` int(11) DEFAULT NULL,
  `SpendingLimit_ti` tinyint(4) DEFAULT '0',
  `DefaultCID_vch` varchar(15) DEFAULT NULL,
  `HauInvitationCode_vch` varchar(255) DEFAULT NULL,
  `MFAContactString_vch` varchar(255) DEFAULT NULL,
  `MFAContactType_ti` tinyint(4) DEFAULT '1',
  `MFAEXT_vch` varchar(45) DEFAULT NULL,
  `MFAEnabled_ti` tinyint(4) DEFAULT '1',
  `ChangePasswordLink_vch` varchar(255) DEFAULT NULL,
  `validPasswordLink_ti` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`UserId_int`),
  KEY `IDX_PrimaryPhoneStr_vch` (`PrimaryPhoneStr_vch`),
  KEY `IDX_fbuserid_bi` (`SOCIALMEDIAID_BI`),
  KEY `UserName_vch` (`UserName_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=410 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:40
