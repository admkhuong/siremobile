CREATE DATABASE  IF NOT EXISTS `simplequeue` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplequeue`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplequeue
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apptqueue`
--

DROP TABLE IF EXISTS `apptqueue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apptqueue` (
  `pkid_int` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserId_int` int(11) DEFAULT NULL,
  `Batchid_bi` bigint(20) DEFAULT NULL,
  `ContactId_bi` varchar(45) DEFAULT NULL,
  `ContactTypeId_int` tinyint(4) DEFAULT NULL,
  `active_int` tinyint(4) DEFAULT '1',
  `AllDayFlag_int` tinyint(4) DEFAULT '0',
  `Created_dt` varchar(45) DEFAULT NULL,
  `Updated_dt` varchar(45) DEFAULT NULL,
  `Start_dt` datetime DEFAULT NULL,
  `End_dt` datetime DEFAULT NULL,
  `Duration_int` varchar(45) DEFAULT NULL,
  `ContactString_vch` varchar(255) DEFAULT NULL,
  `SystemDesc_vch` varchar(255) DEFAULT NULL,
  `Desc_vch` text,
  `Confirmation_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`pkid_int`),
  KEY `IDX_Confirm` (`Batchid_bi`,`Confirmation_int`,`active_int`),
  KEY `IDX_ConfirmResponse` (`Batchid_bi`,`ContactTypeId_int`,`active_int`,`ContactString_vch`,`Start_dt`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:18
