CREATE DATABASE  IF NOT EXISTS `simplequeue` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplequeue`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplequeue
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactqueue`
--

DROP TABLE IF EXISTS `contactqueue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactqueue` (
  `DTSId_int` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,
  `BatchId_bi` bigint(20) unsigned DEFAULT NULL,
  `DTSStatusType_ti` tinyint(4) DEFAULT '0',
  `TypeMask_ti` tinyint(4) NOT NULL DEFAULT '1',
  `TimeZone_ti` tinyint(4) DEFAULT '0',
  `CurrentRedialCount_ti` tinyint(3) unsigned DEFAULT '0',
  `UserId_int` int(10) unsigned DEFAULT NULL,
  `PushLibrary_int` int(11) NOT NULL DEFAULT '0',
  `PushElement_int` int(11) NOT NULL DEFAULT '0',
  `PushScript_int` int(11) NOT NULL DEFAULT '0',
  `PushSkip_int` tinyint(4) NOT NULL DEFAULT '0',
  `EstimatedCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  `CampaignTypeId_int` int(11) DEFAULT NULL,
  `GroupId_int` int(11) NOT NULL DEFAULT '0',
  `Scheduled_dt` datetime DEFAULT NULL,
  `Queue_dt` datetime DEFAULT NULL,
  `Queued_DialerIP_vch` varchar(255) DEFAULT NULL,
  `DialString_vch` varchar(255) NOT NULL DEFAULT 'XXX',
  `Sender_vch` varchar(255) DEFAULT NULL,
  `XMLControlString_vch` longtext,
  `ContactString_vch` varchar(255) NOT NULL DEFAULT 'XXX',
  `LastUpdated_dt` datetime DEFAULT NULL,
  `ShortCode_vch` varchar(255) DEFAULT NULL,
  `ProcTime_int` int(11) DEFAULT '0',
  `DistributionProcessId_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`DTSId_int`),
  KEY `IDX_Batch_Status_TimeZone_Combo` (`BatchId_bi`,`DTSStatusType_ti`,`TimeZone_ti`),
  KEY `IDX_Scheduled` (`Scheduled_dt`),
  KEY `IDX_UserId_int` (`UserId_int`),
  KEY `IDX_Queued_DialerIP` (`Queued_DialerIP_vch`),
  KEY `IDX_DialString` (`DialString_vch`),
  KEY `IDX_BatchId` (`BatchId_bi`),
  KEY `IDX_DtsStatusType` (`DTSStatusType_ti`),
  KEY `IDX_TypeMask` (`TypeMask_ti`),
  KEY `IDX_LastUpdated_dt` (`LastUpdated_dt`),
  KEY `IDX_SMSSTOP` (`ShortCode_vch`,`TypeMask_ti`,`ContactString_vch`,`DTSStatusType_ti`),
  KEY `IDX_COMBO_contact_sc_dtsstatus` (`DTSStatusType_ti`,`ContactString_vch`,`ShortCode_vch`),
  KEY `IDX_ContactString` (`ContactString_vch`),
  KEY `IDX_Combo_User_status` (`UserId_int`,`DTSStatusType_ti`),
  KEY `IDX_Combo_Batch_Scheduled_Status` (`BatchId_bi`,`Scheduled_dt`,`DTSStatusType_ti`),
  KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),
  KEY `IDXComboDistribution` (`DTSStatusType_ti`,`DTSId_int`,`Scheduled_dt`,`DistributionProcessId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=66897752 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:14
