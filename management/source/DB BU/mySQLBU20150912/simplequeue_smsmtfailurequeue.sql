CREATE DATABASE  IF NOT EXISTS `simplequeue` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplequeue`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplequeue
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `smsmtfailurequeue`
--

DROP TABLE IF EXISTS `smsmtfailurequeue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smsmtfailurequeue` (
  `moInboundQueueId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `Status_ti` tinyint(4) unsigned DEFAULT NULL,
  `AggregatorId_int` int(11) NOT NULL,
  `SMPPErrorCode_int` int(11) DEFAULT NULL,
  `ControlPoint_int` int(11) DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `Failure_dt` datetime DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `ScheduledRetry_dt` datetime DEFAULT NULL,
  `ContactString_vch` varchar(255) NOT NULL,
  `ShortCode_vch` varchar(255) NOT NULL,
  `TransactionId_vch` varchar(45) DEFAULT NULL,
  `RawRequestData_vch` text,
  PRIMARY KEY (`moInboundQueueId_bi`),
  KEY `IDX_ANALYTICS` (`ContactString_vch`,`Status_ti`,`ShortCode_vch`),
  KEY `IDX_COMBO_Created_CS` (`Created_dt`,`ContactString_vch`),
  KEY `IDX_Status_ti` (`Status_ti`),
  KEY `IDX_ComboII` (`Created_dt`,`ShortCode_vch`,`BatchId_bi`)
) ENGINE=InnoDB AUTO_INCREMENT=213996 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:10
