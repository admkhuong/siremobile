CREATE DATABASE  IF NOT EXISTS `simplequeue` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplequeue`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplequeue
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `testing1`
--

DROP TABLE IF EXISTS `testing1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testing1` (
  `DialString1_vch` varchar(255) DEFAULT NULL,
  `DialString2_vch` varchar(255) DEFAULT NULL,
  `TimeZone_ti` int(11) DEFAULT NULL,
  `CurrentRedialCount_ti` int(11) DEFAULT NULL,
  `XMLControlString_vch` text,
  `FileSeqNumber_int` int(11) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,
  `BatchId_bi` int(11) DEFAULT NULL,
  `CleanFlag_ti` tinyint(4) DEFAULT NULL,
  `UserId_int` int(11) DEFAULT NULL,
  `PushLibrary_int` int(11) DEFAULT NULL,
  `PushElement_int` int(11) DEFAULT NULL,
  `PushScript_int` int(11) DEFAULT NULL,
  `PushSkip_int` int(4) DEFAULT NULL,
  `DQDTSId_int` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DQDTSId_int`),
  KEY `IDX_CleanFlag_ti` (`CleanFlag_ti`),
  KEY `IDX_DialString_RedialCount` (`DialString1_vch`,`CurrentRedialCount_ti`,`UserSpecifiedData_vch`(767),`FileSeqNumber_int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:21
