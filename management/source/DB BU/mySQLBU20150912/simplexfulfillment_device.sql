CREATE DATABASE  IF NOT EXISTS `simplexfulfillment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexfulfillment`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexfulfillment
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `DeviceId_int` int(11) NOT NULL AUTO_INCREMENT,
  `IP_vch` varchar(255) NOT NULL,
  `Desc_vch` varchar(2048) NOT NULL DEFAULT 'EBM RXDialer',
  `SystemLoad_int` int(11) NOT NULL DEFAULT '2500',
  `VoiceEnabled_int` tinyint(4) NOT NULL DEFAULT '0',
  `WebServiceEnabled_int` tinyint(4) DEFAULT '0',
  `EmailEnabled_int` tinyint(4) DEFAULT '0',
  `ScriptRecordEnabled_int` tinyint(4) DEFAULT '0',
  `EBMBlastEnabled_int` tinyint(4) DEFAULT '0',
  `Position_int` int(11) DEFAULT '1',
  `RealTime_int` tinyint(4) DEFAULT '0',
  `Enabled_int` tinyint(4) DEFAULT '0',
  `Modified_dt` datetime DEFAULT NULL,
  `ErrorCount_int` int(11) DEFAULT '0',
  `LastLookCount_int` int(11) DEFAULT '0',
  `Updated_dt` datetime DEFAULT NULL,
  `DeviceType_int` tinyint(4) DEFAULT '0',
  `DistributionProcessIdList_vch` varchar(255) DEFAULT '(0)',
  PRIMARY KEY (`DeviceId_int`),
  KEY `IDX_Position` (`Position_int`),
  KEY `IDX_Combo_enabled_tele_email_web_realtime` (`VoiceEnabled_int`,`WebServiceEnabled_int`,`EmailEnabled_int`,`Enabled_int`,`RealTime_int`),
  KEY `IDX_Combo_enabled_tele_realtime` (`RealTime_int`,`Enabled_int`,`VoiceEnabled_int`),
  KEY `IDX_Combo_enabled_web_realtime` (`RealTime_int`,`Enabled_int`,`WebServiceEnabled_int`),
  KEY `IDX_Combo_enabled_email_realtime` (`EmailEnabled_int`,`RealTime_int`,`Enabled_int`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:56:01
