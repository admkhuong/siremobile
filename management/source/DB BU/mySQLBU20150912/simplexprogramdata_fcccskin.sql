CREATE DATABASE  IF NOT EXISTS `simplexprogramdata` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexprogramdata`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexprogramdata
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fcccskin`
--

DROP TABLE IF EXISTS `fcccskin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fcccskin` (
  `FCCCSkinId_int` int(11) NOT NULL AUTO_INCREMENT,
  `ProcessedCountAM_int` int(11) DEFAULT '0',
  `ProcessedCountPM_int` int(11) DEFAULT '0',
  `TimeZone_int` int(11) DEFAULT '31',
  `Created_dt` datetime DEFAULT NULL,
  `Updated_dt` datetime DEFAULT NULL,
  `Invite_dt` datetime DEFAULT NULL,
  `CompletedAM_dt` datetime DEFAULT NULL,
  `CompletedPM_dt` datetime DEFAULT NULL,
  `OptOut_dt` datetime DEFAULT NULL,
  `DoubleOptInProcessed_dt` datetime DEFAULT NULL,
  `Program_vch` varchar(255) DEFAULT 'NA',
  `ContactString_vch` varchar(255) DEFAULT NULL,
  `AMSequenceString_vch` varchar(2048) DEFAULT NULL,
  `SFTPFileSource_vch` varchar(1024) DEFAULT 'NONE',
  `14DaySummarySent_int` tinyint(4) DEFAULT '0',
  `7DaySummarySent_int` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`FCCCSkinId_int`),
  UNIQUE KEY `IDX_ContactString` (`ContactString_vch`),
  KEY `IDX_Program` (`Program_vch`),
  KEY `IDX_Combo_Program_Complete` (`Program_vch`,`CompletedAM_dt`),
  KEY `IDX_Combo_Program_OptIn` (`DoubleOptInProcessed_dt`,`Program_vch`),
  KEY `IDX_Combo_Program_CompletePM` (`Program_vch`,`CompletedPM_dt`),
  KEY `IDX_Combo_Program_CompleteAM_OptIn_OptOut` (`OptOut_dt`,`CompletedAM_dt`,`DoubleOptInProcessed_dt`,`Program_vch`),
  KEY `IDX_Combo_Program_CompletePM_OptIn_OptOut` (`CompletedPM_dt`,`OptOut_dt`,`DoubleOptInProcessed_dt`,`Program_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:35
