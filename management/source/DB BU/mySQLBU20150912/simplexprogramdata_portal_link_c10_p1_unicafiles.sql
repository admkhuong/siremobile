CREATE DATABASE  IF NOT EXISTS `simplexprogramdata` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexprogramdata`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexprogramdata
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `portal_link_c10_p1_unicafiles`
--

DROP TABLE IF EXISTS `portal_link_c10_p1_unicafiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_link_c10_p1_unicafiles` (
  `PKId_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `InFileName_vch` varchar(100) NOT NULL,
  `ImportTimeStamp_dt` datetime DEFAULT NULL,
  `ExportTimeStamp_dt` datetime DEFAULT NULL,
  `Status_int` int(11) DEFAULT NULL COMMENT '0=not processed, 1=in queue, 2=processed, 3=exported',
  `StatusTimeStamp_dt` datetime DEFAULT NULL,
  `DTSID_bi` bigint(20) DEFAULT NULL,
  `RequestUUID_vch` varchar(40) DEFAULT NULL,
  `ResponseMessage_vch` varchar(255) DEFAULT NULL,
  `ResponseMessageTimestamp_dt` datetime DEFAULT NULL,
  `TRANSID_vch` varchar(50) NOT NULL,
  `Channel_vch` varchar(45) DEFAULT NULL,
  `PFPID_bi` bigint(20) NOT NULL,
  `AccountNo_vch` varchar(50) DEFAULT NULL,
  `Premise_vch` varchar(50) DEFAULT NULL,
  `TextMessage_vch` varchar(500) DEFAULT NULL,
  `VoiceScriptIdentifier_vch` varchar(50) DEFAULT NULL,
  `PrimaryPhoneNumber_vch` varchar(10) NOT NULL,
  `CellPhoneFlag1_ch` char(1) DEFAULT NULL,
  `SecondaryPhoneNumber_vch` varchar(10) DEFAULT NULL,
  `CellPhoneFlag2_ch` char(1) DEFAULT NULL,
  `CampaignCode_vch` varchar(50) DEFAULT NULL,
  `CampaignName_vch` varchar(500) DEFAULT NULL,
  `timestamp1_dt` datetime DEFAULT NULL,
  `WholeName_vch` varchar(100) DEFAULT NULL,
  `PremAddrComplete_vch` varchar(250) DEFAULT NULL,
  `PremCity_vch` varchar(100) DEFAULT NULL,
  `PremState_vch` varchar(50) DEFAULT NULL,
  `PremZip_vch` varchar(15) DEFAULT NULL,
  `EmailAddress_vch` varchar(250) DEFAULT NULL,
  `Custom1_vch` varchar(100) DEFAULT NULL,
  `Custom2_vch` varchar(100) DEFAULT NULL,
  `Custom3_vch` varchar(100) DEFAULT NULL,
  `Custom4_vch` varchar(100) DEFAULT NULL,
  `Custom5_vch` varchar(100) DEFAULT NULL,
  `IsProd_ti` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`PKId_bi`),
  KEY `IDX_FileName` (`InFileName_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=43681 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:37
