CREATE DATABASE  IF NOT EXISTS `simplexprogramdata` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexprogramdata`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexprogramdata
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `portal_link_c8_p1_monthly_ref`
--

DROP TABLE IF EXISTS `portal_link_c8_p1_monthly_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_link_c8_p1_monthly_ref` (
  `PKId_int` int(11) NOT NULL AUTO_INCREMENT,
  `MemberId_vch` varchar(255) DEFAULT NULL,
  `DateOfBirth_dt` datetime DEFAULT NULL,
  `INNLimit_vch` varchar(45) DEFAULT NULL,
  `OONLimit_vch` varchar(45) DEFAULT NULL,
  `INNBal_vch` varchar(45) DEFAULT NULL,
  `OONBal_vch` varchar(45) DEFAULT NULL,
  `AsOfDate_dt` datetime DEFAULT NULL,
  `LastSent_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`PKId_int`),
  KEY `IDX_Member` (`MemberId_vch`),
  KEY `IDX_MemberSearch` (`MemberId_vch`,`DateOfBirth_dt`,`LastSent_dt`)
) ENGINE=InnoDB AUTO_INCREMENT=765 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:55:42
