CREATE DATABASE  IF NOT EXISTS `simplexrecordedresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexrecordedresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexrecordedresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transcriptionfilelog`
--

DROP TABLE IF EXISTS `transcriptionfilelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transcriptionfilelog` (
  `TFLId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MasterRXCallDetailId_int` int(11) unsigned NOT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `UserId_int` int(10) unsigned DEFAULT NULL,
  `XMLResultStr_vch` text,
  `Original_FileName_vch` varchar(1024) DEFAULT NULL,
  `TransCo_FileName_vch` varchar(1024) DEFAULT NULL,
  `Customer_FileName_vch` varchar(1024) DEFAULT NULL,
  `IsInFileBlank_ti` tinyint(1) DEFAULT NULL,
  `TranscribeAuth_ti` tinyint(1) DEFAULT NULL,
  `DialerIP_vch` varchar(50) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `OutDateTransCo_dt` datetime DEFAULT NULL,
  `OutDateCustsomer_dt` datetime DEFAULT NULL,
  `Reserved_dt` datetime DEFAULT NULL,
  `Reviewed_dt` datetime DEFAULT NULL,
  `ReviewNotes_vch` text,
  `ReviewedBy_vch` varchar(1024) DEFAULT NULL,
  `Transcription_vch` text,
  `ASRTranscription_vch` text,
  `HeatIndex_int` int(11) DEFAULT NULL,
  `AdjustedHeatIndex_int` int(11) DEFAULT NULL,
  `LastModified_dt` datetime DEFAULT NULL,
  `ViewCount_int` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TFLId_int`),
  KEY `IDX_BatchId_bi` (`BatchId_bi`),
  KEY `IDX_UserId_int` (`UserId_int`),
  KEY `IDX_MasterRXCallDetailId_int` (`MasterRXCallDetailId_int`),
  KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),
  KEY `IDX_InitialDate_dt` (`RXCDLStartTime_dt`),
  KEY `IDX_OutDateTransCo_dt` (`OutDateTransCo_dt`),
  KEY `IDX_OutDateCustsomer_dt` (`OutDateCustsomer_dt`),
  KEY `IDX_Reserved_dt` (`Reserved_dt`),
  KEY `IDX_Reviewed_dt` (`Reviewed_dt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:54:15
