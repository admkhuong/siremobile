CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `callresults`
--

DROP TABLE IF EXISTS `callresults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callresults` (
  `MasterRXCallDetailId_int` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RXCallDetailId_int` int(11) unsigned NOT NULL,
  `CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',
  `DTSID_int` int(10) unsigned DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `PhoneId_int` int(11) DEFAULT NULL,
  `TotalObjectTime_int` int(11) DEFAULT NULL,
  `TotalCallTimeLiveTransfer_int` int(11) DEFAULT NULL,
  `TotalCallTime_int` int(11) DEFAULT NULL,
  `TotalConnectTime_int` int(11) DEFAULT '0',
  `ReplayTotalCallTime_int` int(11) DEFAULT NULL,
  `SixSecondBilling_int` int(11) DEFAULT NULL,
  `SystemBilling_int` int(11) DEFAULT NULL,
  `TotalAnswerTime_int` int(11) DEFAULT NULL,
  `CallResult_int` int(11) DEFAULT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `MessageDelivered_si` smallint(6) DEFAULT NULL,
  `SingleResponseSurvey_si` smallint(6) DEFAULT NULL,
  `UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,
  `NumberOfHoursToRescheduleRedial_si` smallint(6) DEFAULT NULL,
  `TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,
  `TransferStatusId_ti` tinyint(4) DEFAULT NULL,
  `IsHangUpDetected_ti` tinyint(4) DEFAULT NULL,
  `IsOptOut_ti` tinyint(4) DEFAULT NULL,
  `IsOptIn_ti` tinyint(4) DEFAULT '0',
  `IsMaxRedialsReached_ti` tinyint(4) DEFAULT NULL,
  `IsRescheduled_ti` tinyint(4) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `CallStartTime_dt` datetime DEFAULT NULL,
  `CallEndTime_dt` datetime DEFAULT NULL,
  `CallStartTimeLiveTransfer_dt` datetime DEFAULT NULL,
  `CallEndTimeLiveTransfer_dt` datetime DEFAULT NULL,
  `CallResultTS_dt` datetime DEFAULT NULL,
  `PlayFileStartTime_dt` datetime DEFAULT NULL,
  `PlayFileEndTime_dt` datetime DEFAULT NULL,
  `HangUpDetectedTS_dt` datetime DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,
  `DialerName_vch` varchar(255) DEFAULT NULL,
  `DialerIP_vch` varchar(50) DEFAULT NULL,
  `CurrTS_vch` varchar(255) DEFAULT NULL,
  `CurrVoice_vch` varchar(255) DEFAULT NULL,
  `CurrTSLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `CurrVoiceLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `CurrCDP_vch` varchar(255) DEFAULT NULL,
  `CurrCDPLiveTransfer_vch` varchar(255) DEFAULT NULL,
  `DialString_vch` varchar(255) DEFAULT NULL,
  `ContactTypeId_int` int(11) DEFAULT NULL,
  `XMLResultStr_vch` text,
  `XMLControlString_vch` text,
  `FileSeqNumber_int` int(11) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,
  `ActualCost_int` float(10,3) NOT NULL DEFAULT '0.000',
  `dial6_vch` varchar(10) DEFAULT NULL,
  `MainMessageLengthSeconds_int` int(11) DEFAULT '0',
  PRIMARY KEY (`MasterRXCallDetailId_int`),
  KEY `XPKRXCallDetails` (`MasterRXCallDetailId_int`),
  KEY `IDX_DialString` (`DialString_vch`),
  KEY `IDX_DTSID_int` (`DTSID_int`),
  KEY `IDX_CallResult_int` (`CallResult_int`),
  KEY `IDX_DTS_UUID_vch` (`DTS_UUID_vch`),
  KEY `IDX_BatchId_bi` (`BatchId_bi`),
  KEY `IDX_Dial6` (`dial6_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=4463 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:05:48
