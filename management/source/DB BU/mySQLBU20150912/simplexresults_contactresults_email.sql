CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactresults_email`
--

DROP TABLE IF EXISTS `contactresults_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactresults_email` (
  `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `DTSID_int` int(10) unsigned DEFAULT NULL,
  `DTS_UUID_vch` varchar(36) DEFAULT NULL,
  `Attempt_int` int(11) DEFAULT NULL,
  `BatchId_bi` bigint(20) DEFAULT NULL,
  `Email_vch` varchar(50) DEFAULT NULL,
  `Event_vch` varchar(50) DEFAULT NULL,
  `Response_vch` varchar(800) DEFAULT NULL,
  `Smtpid_vch` varchar(200) DEFAULT NULL,
  `sgTimestamp_dt` datetime DEFAULT NULL,
  `Type_vch` varchar(200) DEFAULT NULL,
  `Reason_vch` varchar(500) DEFAULT NULL,
  `Status_vch` varchar(100) DEFAULT NULL,
  `mbTimestamp_dt` datetime DEFAULT NULL,
  `IncomingIP_vch` varchar(50) DEFAULT NULL,
  `read_bit` bit(1) DEFAULT NULL,
  PRIMARY KEY (`TableID_bi`),
  KEY `IDX_mbTimestamp` (`mbTimestamp_dt`),
  KEY `IDX_BatchId_bi` (`BatchId_bi`),
  KEY `IDX_Event_vch` (`Event_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:05:42
