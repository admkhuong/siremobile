CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactresults_email_account_summary_devices`
--

DROP TABLE IF EXISTS `contactresults_email_account_summary_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactresults_email_account_summary_devices` (
  `TableID_bi` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountID_vch` varchar(10) DEFAULT NULL,
  `open_webMail_int` int(11) DEFAULT NULL,
  `open_phone_int` int(11) DEFAULT NULL,
  `open_tablet_int` int(11) DEFAULT NULL,
  `open_desktop_int` int(11) DEFAULT NULL,
  `open_other_int` int(11) DEFAULT NULL,
  `unique_webMail_int` int(11) DEFAULT NULL,
  `unique_phone_int` int(11) DEFAULT NULL,
  `unique_tablet_int` int(11) DEFAULT NULL,
  `unique_other_int` int(11) DEFAULT NULL,
  `unique_desktop_int` int(11) DEFAULT NULL,
  `startdate_dt` datetime DEFAULT NULL,
  `enddate_dt` datetime DEFAULT NULL,
  `date_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`TableID_bi`),
  KEY `IDX_date_dt` (`date_dt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:05:54
