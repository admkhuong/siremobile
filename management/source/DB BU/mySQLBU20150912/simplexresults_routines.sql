CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'simplexresults'
--
/*!50003 DROP PROCEDURE IF EXISTS `usp_sg_ins_emailv3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`EBMSQLUser`@`%` PROCEDURE `usp_sg_ins_emailv3`(IN `nDtsid` BIGINT, IN `sUUID` VARCHAR(200), IN `sAttempt` INT, IN `sCategory` INT, IN `sEmail` VARCHAR(200), IN `sEvent` VARCHAR(200), IN `sResponse` VARCHAR(500), IN `sSmtpid` VARCHAR(200), IN `dSgTimestamp` DATETIME, IN `sType` VARCHAR(200), IN `sReason` VARCHAR(500), IN `sStatus` VARCHAR(100)

, IN `sIncomingIP_vch` VARCHAR(50))
BEGIN

 INSERT INTO simplexresults.contactresults_email
            	(
                 DTSID_int, 
                 DTS_UUID_vch,
                 Attempt_int,
                 BatchId_bi,
                 Email_vch, 
                 Event_vch, 
                 Response_vch,
                 Smtpid_vch,
                 sgTimestamp_dt,
                 Type_vch,
                 Reason_vch,
                 Status_vch,
                 IncomingIP_vch,
                 mbTimestamp_dt
               )
            VALUES
            	(
				nDtsid,
                sUUID,
               	sAttempt,
               	sCategory,
               	sEmail,
                sEvent,
                sResponse,
                sSmtpid,
                dSgTimestamp,
                sType,
                sReason,
                sStatus,
                sIncomingIP_vch,
                CURRENT_TIMESTAMP()
                );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:01
