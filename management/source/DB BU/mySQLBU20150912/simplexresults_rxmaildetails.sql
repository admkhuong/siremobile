CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rxmaildetails`
--

DROP TABLE IF EXISTS `rxmaildetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rxmaildetails` (
  `DTS_UUID_vch` varchar(36) NOT NULL,
  `CallDetailsStatusId_ti` tinyint(4) DEFAULT '0',
  `DTSID_int` int(11) unsigned DEFAULT NULL,
  `BatchId_bi` int(14) DEFAULT NULL,
  `LinkId_vch` varchar(512) DEFAULT NULL,
  `TotalObjectTime_int` int(11) DEFAULT NULL,
  `TotalCallTime_int` int(11) DEFAULT NULL,
  `SystemBilling_int` int(11) DEFAULT '1',
  `MailResult_int` int(11) DEFAULT NULL,
  `RedialNumber_int` int(11) DEFAULT NULL,
  `InitialDeliveryStatus_si` smallint(6) DEFAULT NULL,
  `InitialDelivery_vch` text,
  `FinalDeliveryStatus_si` smallint(6) DEFAULT NULL,
  `FinalDeliveryText_vch` text,
  `UserSpecifiedLineNumber_si` smallint(6) DEFAULT NULL,
  `TimeZone_ti` tinyint(3) unsigned DEFAULT NULL,
  `IsOptOut_int` int(11) DEFAULT '0',
  `LastOptOut_dt` datetime DEFAULT NULL,
  `IsOptIn_int` int(11) DEFAULT '0',
  `LastOptIn_dt` datetime DEFAULT NULL,
  `ReadCount_int` int(11) DEFAULT '0',
  `LastRead_dt` datetime DEFAULT NULL,
  `IsRescheduled_ti` tinyint(4) DEFAULT NULL,
  `RXCDLStartTime_dt` datetime DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `LastUpdated_dt` datetime DEFAULT NULL,
  `DialerName_vch` varchar(255) DEFAULT NULL,
  `MessageWarning_vch` text,
  `MessageText_vch` text,
  `MessageHTML_vch` text,
  `ConnFromUser_vch` varchar(2048) DEFAULT NULL,
  `MessageFromUser_vch` varchar(2048) DEFAULT NULL,
  `MessageToUser_vch` varchar(2048) DEFAULT NULL,
  `MessageCCUser_vch` varchar(2048) DEFAULT NULL,
  `MessageSubject_vch` varchar(2048) DEFAULT NULL,
  `SMTPMessageCreationDate_vch` varchar(255) DEFAULT NULL,
  `AttachmentPath_vch` varchar(2048) DEFAULT NULL,
  `SMTPServer_vch` varchar(50) DEFAULT NULL,
  `XMLControlString_vch` text,
  `XMLResultStr_vch` text,
  `FileSeqNumber_int` int(11) DEFAULT NULL,
  `UserSpecifiedData_vch` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`DTS_UUID_vch`),
  KEY `IDX_MessageToUser_vch` (`MessageToUser_vch`(767)),
  KEY `IDXDTSID_int` (`DTSID_int`),
  KEY `IDXLinkId_vch` (`LinkId_vch`),
  KEY `IDX_CallDetailsStatusId_ti` (`CallDetailsStatusId_ti`),
  KEY `IDX_RXCDLStartTime_dt` (`RXCDLStartTime_dt`),
  KEY `IDX_BatchID_bi` (`BatchId_bi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:05:46
