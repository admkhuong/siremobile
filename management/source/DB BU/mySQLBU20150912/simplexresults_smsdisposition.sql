CREATE DATABASE  IF NOT EXISTS `simplexresults` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexresults`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexresults
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `smsdisposition`
--

DROP TABLE IF EXISTS `smsdisposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `smsdisposition` (
  `SMSDispositionId_int` bigint(20) NOT NULL AUTO_INCREMENT,
  `Status_ti` tinyint(4) DEFAULT '0',
  `SubscriberNumber_vch` varchar(512) DEFAULT NULL,
  `Status_vch` varchar(512) DEFAULT NULL,
  `MsgReference_vch` varchar(512) DEFAULT NULL,
  `Reason_vch` varchar(512) DEFAULT NULL,
  `nSMSGlobalMessageId_vch` varchar(512) DEFAULT NULL,
  `SequenceNumber_vch` varchar(512) DEFAULT NULL,
  `SecondarySequenceNumber_vch` varchar(512) DEFAULT NULL,
  `Time_vch` varchar(512) DEFAULT NULL,
  `ShortCode_vch` varchar(255) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `RawData_vch` text,
  `AggregatorId_int` int(11) DEFAULT '0',
  PRIMARY KEY (`SMSDispositionId_int`),
  KEY `IDX_Subscriber` (`SubscriberNumber_vch`),
  KEY `IDX_CreatedReasonCombo` (`Created_dt`,`Reason_vch`),
  KEY `IDX_CreatedSubscriber` (`Created_dt`,`SubscriberNumber_vch`),
  KEY `IDX_Aggregator` (`Created_dt`,`AggregatorId_int`)
) ENGINE=InnoDB AUTO_INCREMENT=14402063 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:05:52
