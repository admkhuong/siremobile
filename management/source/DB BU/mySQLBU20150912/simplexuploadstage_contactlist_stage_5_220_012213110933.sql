CREATE DATABASE  IF NOT EXISTS `simplexuploadstage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexuploadstage`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexuploadstage
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactlist_stage_5_220_012213110933`
--

DROP TABLE IF EXISTS `contactlist_stage_5_220_012213110933`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactlist_stage_5_220_012213110933` (
  `C1` varchar(50) DEFAULT '',
  `C2_3` varchar(255) DEFAULT '',
  `llkk` varchar(4000) DEFAULT '',
  `C4` varchar(255) DEFAULT '',
  `UID` int(11) NOT NULL AUTO_INCREMENT,
  `FFlagC4` int(11) NOT NULL DEFAULT '0',
  `FDupFlagC4` int(11) NOT NULL DEFAULT '0',
  `FTimeZoneC4` int(11) NOT NULL DEFAULT '0',
  `FCityC4` varchar(100) NOT NULL DEFAULT '',
  `FStateC4` varchar(2) NOT NULL DEFAULT '',
  `FCellFlagC4` int(11) DEFAULT '0',
  `LaltFlagC2_3` int(11) NOT NULL DEFAULT '0',
  `LaltDupFlagC2_3` int(11) NOT NULL DEFAULT '0',
  `LaltTimeZoneC2_3` int(11) NOT NULL DEFAULT '0',
  `LaltCityC2_3` varchar(100) NOT NULL DEFAULT '',
  `LaltStateC2_3` varchar(2) NOT NULL DEFAULT '',
  `LaltCellFlagC2_3` int(11) DEFAULT '0',
  `ContactPreExist_int` int(11) DEFAULT '0',
  `ContactId_bi` bigint(20) DEFAULT NULL,
  `DataTrack_vch` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`UID`),
  KEY `ContactPreExist_int` (`ContactPreExist_int`,`ContactId_bi`,`DataTrack_vch`,`FFlagC4`,`FDupFlagC4`,`LaltFlagC2_3`,`LaltDupFlagC2_3`),
  KEY `C2_3` (`C2_3`),
  KEY `C4` (`C4`)
) ENGINE=InnoDB AUTO_INCREMENT=5001 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:57:18
