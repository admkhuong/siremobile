CREATE DATABASE  IF NOT EXISTS `simplexuploadstage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simplexuploadstage`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: simplexuploadstage
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `simplephonelist_stage_9_288_030813093156`
--

DROP TABLE IF EXISTS `simplephonelist_stage_9_288_030813093156`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simplephonelist_stage_9_288_030813093156` (
  `column1` varchar(255) DEFAULT '',
  `column2` varchar(255) DEFAULT '',
  `column3` varchar(255) DEFAULT '',
  `column4` varchar(255) DEFAULT '',
  `FirstName_vch` varchar(255) DEFAULT '',
  `LastName_vch` varchar(255) DEFAULT '',
  `column7` varchar(255) DEFAULT '',
  `ContactString_vch` varchar(255) NOT NULL,
  `column9` varchar(255) DEFAULT '',
  `column10` varchar(255) DEFAULT '',
  `column11` varchar(255) DEFAULT '',
  `column12` varchar(255) DEFAULT '',
  `column13` varchar(255) DEFAULT '',
  `column14` varchar(255) DEFAULT '',
  `column15` varchar(255) DEFAULT '',
  `column16` varchar(255) DEFAULT '',
  `column17` varchar(255) DEFAULT '',
  `column18` varchar(255) DEFAULT '',
  `column19` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  8:57:53
