CREATE DATABASE  IF NOT EXISTS `sms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sms`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: 10.25.0.205    Database: sms
-- ------------------------------------------------------
-- Server version	5.5.39-36.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `keyword`
--

DROP TABLE IF EXISTS `keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keyword` (
  `KeywordId_int` int(11) NOT NULL AUTO_INCREMENT,
  `Keyword_vch` varchar(255) NOT NULL,
  `ShortCodeRequestId_int` int(11) DEFAULT NULL,
  `Created_dt` datetime DEFAULT NULL,
  `Response_vch` varchar(1000) NOT NULL,
  `IsDefault_bit` bit(1) DEFAULT b'0',
  `BatchId_bi` bigint(11) DEFAULT NULL,
  `Survey_int` int(4) DEFAULT NULL,
  `Active_int` int(4) DEFAULT '1',
  `ShortCodeId_int` int(11) DEFAULT NULL,
  `ToolTip_vch` varchar(1000) DEFAULT NULL,
  `EMSFlag_int` tinyint(4) DEFAULT '0',
  `DeliveryReceipt_ti` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`KeywordId_int`),
  KEY `batchId_index` (`BatchId_bi`),
  KEY `idx_isdefault` (`IsDefault_bit`),
  KEY `idx_combo_keyword_scid` (`ShortCodeId_int`,`Keyword_vch`),
  KEY `IDX_COMBO_scrid_active` (`ShortCodeRequestId_int`,`Active_int`),
  KEY `IDX_COMBO_keyword_active` (`Keyword_vch`,`Active_int`),
  KEY `IDX_COMBO_scr_sc_active` (`Active_int`,`ShortCodeRequestId_int`,`ShortCodeId_int`),
  KEY `IDX_COMBO_scr_sc_keyword_active` (`Active_int`,`ShortCodeRequestId_int`,`ShortCodeId_int`,`Keyword_vch`)
) ENGINE=InnoDB AUTO_INCREMENT=648 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-12  9:06:42
