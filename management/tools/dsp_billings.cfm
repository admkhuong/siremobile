<h4>List user for Billing</h4>
<script>
var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
	
$(function() {

	
<!---LoadSummaryInfo();--->
	
	jQuery("#listbilling").jqGrid({      
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=GetSimpleUserData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
		datatype: "json",
		height: 385,
		colNames:['User ID','User Name', 'CompanyAccountId','Option'],
		colModel:[			
			{name:'UserId_int',index:'UserId_int', width:100, editable:false, resizable:false},
			{name:'UserName_vch',index:'UserName_vch', width:300, editable:true, resizable:false},
			{name:'CompanyAccountId_int',index:'CompanyAccountId_int', width:200, editable:true, resizable:false},
			{name:'option',index:'option', width:70, editable:true, resizable:false}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
		toppager: true,
    	emptyrecords: "No User.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'UserId_int',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
		
		onSelectRow: function(UserId_int){

			},
		 loadComplete: function(data){ 		
		 	 
				$(".view_userListBatch").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_userListBatch").click( function() { ViewListBatch($(this).attr('rel')); } );		
		 		
				$(".view_userBalance").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_userBalance").click( function() { view_userBalance($(this).attr('rel')); } );	
				
				$('.viewTotalTime').click(function(){
					showTotalTimeDialog();
				});

				<!--- Bind all search functionality to new row --->
				$("#listbilling #search_notes").unbind();
				$("#listbilling #search_notes").keydown(function(event) { doSearchMCContent(arguments[0]||event,'search_notes') }); 
				
				<!--- Reset the focus to an inline filter box--->
				if(typeof(lastObj_MCContent) != "undefined" && lastObj_MCContent != "")
				{					
					<!--- Stupid IE not rendering yet so call again in 10 ms--->
					setTimeout(function() 
					{ 
					
						 var LocalFocus = $("#BatchListMCContent #" + lastObj_MCContent);
						 if (LocalFocus.setSelectionRange) 
						 {         
							 <!--- ... then use it        
							   (Doesn't work in IE)
							   Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh. 
							  ---> 
							   var len = $(LocalFocus).val().length * 2;         
							   LocalFocus.setSelectionRange(len, len);         
						 }        
						 else
  			          	 {
						     <!---... otherwise replace the contents with itself         
							   (Doesn't work in Google Chrome)    --->     
							  $(LocalFocus).focus(); 
							  var str = $(LocalFocus).val();
							  $(LocalFocus).val("");    
							  $(LocalFocus).val(str);        
					     }         
						 
						 <!--- Scroll to the bottom, in case we're in a tall textarea
						  (Necessary for Firefox and Google Chrome)--->
						 LocalFocus.scrollTop = 999999; 
						 
						
						lastObj_MCContent = undefined;
						$("#listbilling #" + lastObj_MCContent).focus(); 
					}, 10);
				}
			
				
   			} ,	
		<!---editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	--->
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default second column as ID
			  }	
	});

	$("#t_LevelList").height(65);

	$("#loading").hide();
		
});

<!--- Global so popup can refernece it to close it--->
	var CreateTotalTimeDialog = 0;
	function showTotalTimeDialog(){
			$.ajax({
				type:"GET",
				url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=GetTotalCallTimeMessage&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	            dataType: "json", 
	            async: false,
	            success: function(d2, textStatus, xhr) {
	            	    popupTotalCallTime(d2.ROWS);
	            }
			});
		return false;
	}

function popupTotalCallTime(callresult) {

		var tmp = '';
   	    for(var i=0;i < callresult.length;i++) {
   	    	mess = callresult[i][0];
   	    	timetotal = callresult[i][1];
   	    	tmp = tmp + "<li>billing type ("+mess+"):" + timetotal+"</li>";
   	    }
		if(CreateTotalTimeDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateTotalTimeDialog.remove();
			CreateTotalTimeDialog = 0;
			
		} 	
	var template = 
		"<div id='list_screen'><ul>"
		+tmp
		+"</ul>"
		+"</div>";	
		
		CreateTotalTimeDialog = $('<div>'+template+'</div>');
		
		CreateTotalTimeDialog
			.dialog({
				modal : true,
				title: 'Screen',
				width: 250,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close: function() { CreateTotalTimeDialog.remove(); CreateTotalTimeDialog = 0;},
				open: function() {
					
				}
			});
	
}
function gridReload(){
	 var grid = $("#listbilling");
	 grid.trigger("reloadGrid");
}	

<!--- Global so popup can refernece it to close it--->
var CreateViewListBatch = 0;
function ViewListBatch(UserId_int) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(UserId_int) != "undefined" && UserId_int != "")					
		ParamStr = '?UserId_int=' + encodeURIComponent(UserId_int);
	else
		UserId_int = 0;
	if ( CreateViewListBatch != 0 ) {
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateViewListBatch.remove();
		CreateViewListBatch = 0;
	}
	CreateViewListBatch = $('<div></div>').append($loading.clone());
	CreateViewListBatch
		<!--- .load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reports/dsp_ReportBilling.cfm' + ParamStr) --->
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Billing/dsp_invoices.cfm' + ParamStr)
		.dialog({
			modal : true,
			title: 'Reporting List Batch {' + UserId_int + '}',
			width: 700,
			minHeight: 400,
			height: 'auto',
			position: 'top' 
		});

		CreateViewListBatch.dialog('open');
}


var CreateViewListBatch = 0;
function view_userBalance(UserId_int) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(UserId_int) != "undefined" && UserId_int != "")					
		ParamStr = '?UserId_int=' + encodeURIComponent(UserId_int);
	else
		UserId_int = 0;
	if ( CreateViewListBatch != 0 ) {
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateViewListBatch.remove();
		CreateViewListBatch = 0;
	}
	CreateViewListBatch = $('<div></div>').append($loading.clone());
	CreateViewListBatch
		<!--- .load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reports/dsp_ReportBilling.cfm' + ParamStr) --->
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reports/dsp_ReportBilling.cfm' + ParamStr)
		.dialog({
			modal : true,
			title: 'Reporting List Batch {' + UserId_int + '}',
			width: 1200,
			minHeight: 400,
			height: 'auto',
			position: 'top' 
		});

		CreateViewListBatch.dialog('open');
}
function rx_AfterEditMCContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchListMCContent").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{							
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{								
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESC[0]) != "undefined")								
				{
					$("#listbilling").jqGrid('setRowData',rowid,{PermissionSection_vch:d.DATA.INPDESC[0]});
				}
				
			}
				
		}
		else
		{<!--- Invalid structure returned --->	
			
		}
	}
	else
	{<!--- No result returned --->
		
	}  
};

function DelBilling(UserId_int)
{
//	$("#loadingDlgAddBatchMCContent").show();	
		
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { UserId_int : UserId_int},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								gridReloadBatchesMCContent();										
								return false;
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Campaign has NOT beed deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
		//			$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
								
			} 		
			
		});
	
		return false;

	
	
}
</script>


<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->

<div >
	<table id="listbilling" align="center"></table>
</div>
<div id="pagerb"></div>



