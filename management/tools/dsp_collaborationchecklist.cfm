
<cfparam name="SESSION.CompanyUserId" default="#SESSION.USERID#">

<script>

var taskItemIds=0;
var localSelectedValues=new Array();
var lastsel;
var selectedTableName;
//var editedUserId=selectedUserId;
//var editedCompanyAccountId=selectedCompanyAccountId;
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#TableList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=getCollaborationTableList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		width:700,
		colNames:['Table Name','Description','Options'],
		colModel:[
			{name:'table_name',index:'table_name', width:150, editable:false},
			{name:'table_comment',index:'table_comment', width:150, editable:true},
			{name:'DisplayOptions',index:'DisplayOptions', width:50, editable:true,align:'center'},
		],
		rowNum:20,
	   	rowList:[5, 10, 20, 30],
		mtype: "POST",
		pager: jQuery('#pagerTaskList'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'table_name',
		viewrecords: true,
		sortorder: "asc",
		formatter: checkboxFormatter,
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		 loadComplete: function(data){
				//reloadUserTaskList();
				$(".edit_CheckList").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_CheckList").click( function() { EditCollaborationDialogCall($(this).attr('rel')); } );		
				return false;	 
   			},		
			onSelectRow: function(tableName,status){
				//alert(ItemId_int + '-' + status);
<!--- 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				} --->
				lastsel=tableName;
			},
			
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_TableList").height(55);
	$("#loading").hide();
		
});
	//remote existing element in array1 from array2]
	
	function getNeedInsertList(originalList,selectedList){
		var result=new Array();
		if(originalList.length > 0 && selectedList.length > 0){
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]*1,originalList) < 0){
					result.push(selectedList[i]*1);
				}
			}
		}else
		{
			return selectedList;
		}
		return result;
	}
	function getRequiredValueList(list){
		var result=new Array();
		if(list.length>0){
			for(i=0;i<list.length;i++){
				//var rowData = jQuery('#TaskList').getRowData(list[i]);
				//var temp= rowData['required'];
				var temp=getCellValue(jQuery('#TableList').getCell(list[i],'required'));
				result.push(temp);
				//result.push($('#' + list[i] + '_required').val());
			}
		}
		//alert(result);
		return result;
	}
	
	
	//get the not existing element from array 1 from array2
	function getNeedRemoveList(selectedList,originalList){
		var res=new Array();
		if(selectedList.length>0 && originalList.length>0){
			for(i=0;i<originalList.length;i++){
				if($.inArray(originalList[i]+"",selectedList) < 0){
					res.push(originalList[i]*1);
				}
			}
		}else{
			return originalList;
		}
		return res;
	}
	

	function reloadUserTaskList(){
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=getTaskListOfUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : lastsel }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							taskItemIds=d.DATA.TASKITEMS[0];
							requireds=d.DATA.REQUIRED[0];
							//set local selected values to array of original task list
							localSelectedValues=taskItemIds;
							//jQuery("#TaskList").resetSelection();
							if(taskItemIds.length > 0)
							{
								for(var i=0;i<=taskItemIds.length;i++){
									jQuery("#TableList").setSelection(taskItemIds[i],false);
									//$(#ur_checkbox_id).attr('disabled', true);
									jQuery("#TableList").setCell(taskItemIds[i],'required',requireds[i]);
								}
							}
	<!--- 									else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										} --->
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgLinkUserToCompany").hide();
			} );
	}
	
	
	var EditCollaborationDialog = 0;
	function EditCollaborationDialogCall(tableName){
		var selectingRow=jQuery("#TableList").getGridParam('selrow');
		
 		 if(selectingRow=='null'){
		 	jQuery("#TableList").setSelection(tableName,false);
		 	
		 }else{
		 	jQuery("#TableList").setSelection(selectingRow,false);
		 } 
		 
		selectedTableName=tableName;
		var $loadingLevel = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		if(EditCollaborationDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			EditCollaborationDialog.remove();
			EditCollaborationDialog = 0;
		}
		EditCollaborationDialog = $('<div></div>').append($loadingLevel.clone());
		EditCollaborationDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/tools/dsp_EditDefaultColumnList.cfm')
			.dialog({
				modal : true,
				title: 'Select default field to display on collaboration',
				close: function() {
					 EditCollaborationDialog.remove(); 
					 EditCollaborationDialog = 0; 
					 var grid = $("#FieldList");
					 grid.trigger("reloadGrid");
				},
				buttons:[
					{
						text:'Update',
						id:'UpdateCollaborationButton',
						click:function(){
							UpdateDefaultColumnList();
						}
					},
					{
						text: 'Cancel',
						click:function(){
							EditCollaborationDialog.remove(); 
							EditCollaborationDialog = 0;
						}
					}
				],				
				width: 700,
				position:'top',
				height: 400,
			});
	
		EditCollaborationDialog.dialog('open');
	
		return false;			
	}
	
		function diffArrays (A, B) {
		
		  var strA = ":" + A.join("::") + ":";
		  var strB = ":" +  B.join(":|:") + ":";
		
		  var reg = new RegExp("(" + strB + ")","gi");
		
		  var strDiff = strA.replace(reg,"").replace(/^:/,"").replace(/:$/,"");
		
		  var arrDiff = strDiff.split("::");
		
		  return arrDiff;
		}
		function checkboxFormatter(cellvalue, options, rowObject) {
		    cellvalue = cellvalue + "";
		    cellvalue = cellvalue.toLowerCase();
		    var bchk = cellvalue.search(/(false|0|no|off|n)/i) < 0 ? " checked='checked'" : "";
		    return '<input type="checkbox" onclick="clientSave(' + options.rowId + ','+ cellvalue + ');"' + bchk + ' value="' + cellvalue + '" offval="no" />';
		}
		function clientSave(rowId,value){

			if(value == 0 || value == 'undefined' || value === undefined){
				jQuery("#TableList").setCell(rowId,'required',1);
			}else{
				jQuery("#TableList").setCell(rowId,'required',0);
			}
		}
		function getCellValue(content) {
		  var k1, val = 0;
		  if(content==false) return 0;
		  k1 = content.indexOf(' value=') + 7;
		  val = content.substr(k1,3);
		  val = val.replace(/[^0-1]/g, '');
		  if(val=='undefined' || val=='') return 0;
		  return val;
		}
	
</script>

<div style="padding-left:40px"><table id="TableList" align="center"></table></div>
<div id="pagerTaskList"></div>