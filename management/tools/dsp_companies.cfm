<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
	<script>
		
var selectedCompanyAccountId;

		
$(function() {


	// Dock initialize
	$('#dockMCCompanyList').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_BabbleMCCompany',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
<!---LoadSummaryInfo();--->
	jQuery("#CompanyAccountList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/company.cfc?method=GetSimpleCompaniesData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['<font size="2"><b>Company Id</b></font>',
					'<font size="2"><b>Company Name</b></font>',
					'<font size="2"><b>Active</b></font>',
					'<font size="2"><b>Created Date</b></font>',
					'<font size="2"><b>Options</b></font>'],
		colModel:[
			{name:'CompanyAccountId_int',index:'CompanyAccountId_int', width:100, editable:false},
			{name:'CompanyName_vch',index:'CompanyName_vch', width:200, editable:true},
			{name:'Active_int',index:'Active_int', width:100, editable:false,align:'center'},
			{name:'Created_dt',index:'Created_dt', width:200, editable:true},
			{name:'DisplayOptions',index:'DisplayOptions', width:70, editable:false,align:'center'}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		//pager: jQuery('#pagerCompanyAccountList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'CompanyAccountId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(CompanyAccountId_int){
				selectedCompanyAccountId=CompanyAccountId_int;
				if(CompanyAccountId_int && CompanyAccountId_int!==lastsel){
					jQuery('#CompanyAccountList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=CompanyAccountId_int;
				}
			},
		 loadComplete: function(data){ 	
		 		 				 		  
				$(".deactive_Company").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".deactive_Company").click( function() { DeleteCompany($(this).attr('rel')); } );
				$(".edit_Company").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_Company").click( function() { UpdateCompanyAccountDialogCall(); } );
		 		
				$(".active_Company").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".active_Company").click( function() { ActivateCompanyAccount($(this).attr('rel'),1); } );
		 				 		
				currentEmail = <cfoutput>"#Session.EmailAddress#"</cfoutput>
				if(currentEmail == "unknown@unknown.com"){
					history.go(0);
				}		 		
   			},			
		
			
	//	editurl: '<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerCompanyAccountList',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 

	

	$("#loading").hide();
	
	$("#CompanyAccountList_toppager_left").html("<b><cfoutput>Company List</cfoutput></b><img id='refresh_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
		$("#refresh_List").click(function() { refreshGrid(); return false; });
		
});		

	var AddNewCompanyAccountDialog = 0;
	function AddNewCompanyAccountDialogCall(){

				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(AddNewCompanyAccountDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			AddNewCompanyAccountDialog.remove();
			AddNewCompanyAccountDialog = 0;
			
		}
						
		AddNewCompanyAccountDialog = $('<div></div>');

		AddNewCompanyAccountDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_AddCompanyAccount.cfm')
			.dialog({
				modal : true,
				title: 'Add Company Level Account',
				close: function() {
					 AddNewCompanyAccountDialog.remove(); 
					 AddNewCompanyAccountDialog = 0; 
					 var grid = $("#CompanyAccountList");
					 grid.trigger("reloadGrid");
				},
				width: 700,
				position:'top',
				height: 500,
			});
	
		AddNewCompanyAccountDialog.dialog('open');
		return false;
	}

	var UpdateCompanyAccountDialog = 0;
	function UpdateCompanyAccountDialogCall(){
<!---		
	if(typeof(lastsel) == "undefined")
	{
		jAlert("No company account selected.", "Select an account from the list and try again.");
		return;		
	}
	--->	
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(UpdateCompanyAccountDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateCompanyAccountDialog.remove();
			UpdateCompanyAccountDialog = 0;
			
		}
						
		UpdateCompanyAccountDialog = $('<div></div>');
			  $.ajax({
			    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=getUserRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  {inpUserId:<cfoutput>"#Session.USERID#"</cfoutput>},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];
								if(CurrRXResultCode > 0)
								{
									var userRole=	d.DATA.ROLE;
									if(userRole == 'SuperUser'){
										loadEditDialog();
									}else if(userRole == 'CompanyAdmin'){
										
										  $.ajax({
										    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
											url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=isBelongToACompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
											dataType: 'json',
											data:  {inpCompanyAccountId:selectedCompanyAccountId,inpUserId:<cfoutput>"#Session.USERID#"</cfoutput>},					  
											error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
											success:
											<!--- Default return function for Do CFTE Demo - Async call back --->
											function(d2, textStatus, xhr ) 
											{
												<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
												var d = eval('(' + xhr.responseText + ')');
													<!--- Get row 1 of results if exisits--->
													if (d.ROWCOUNT > 0)
													{
														<!--- Check if variable is part of JSON result string --->								
														if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
														{
															CurrRXResultCode = d.DATA.RXRESULTCODE[0];
															if(CurrRXResultCode > 0)
															{
																result= d.DATA.RES;
																if(result=='true' || result==true){
																	loadEditDialog();
																}else{
																	jAlert("You are not admin of this company","You don't have permission to edit this company's Task List");
																}
															}
														}
														else
														{<!--- Invalid structure returned --->	
															jAlert("You don't belong to any company","You don't have permission to edit this company's Task List");
														}
													}
													else
													{<!--- No result returned --->
														<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
														jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
													}
											} 		
										});
									}else{
										jAlert("You don't have permission to edit this company","Failed to load dialog");
									}
								}else{
									jAlert("You don't have permission to edit this company","Failed to load dialog");
								}
							}
							else
							{<!--- Invalid structure returned --->	
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});		

		return false;		
	}
	
	function DeleteCompany(companyAccountId)
	{
		var userRole='<cfoutput>#Session.UserRole#</cfoutput>';
		if(userRole != "SuperUser"){
			jAlert("You have no permission to delete company account","Failed");
			return;
		}
		
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "By deactivating company account,you would also remove its reference from appropriate user accounts !", "Confirmation", 
		function(result) { 
			if(!result){
				return;
			}else{	
			  $.ajax({
			    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/company.cfc?method=removeCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  { inpCompanyAccountId : companyAccountId},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{								
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Deleted", "Success!", function(result) 
																				{ 
																					
																				} );								
								}
																											
								$("#loading").hide();
						
							}
							else
							{<!--- Invalid structure returned --->	
								$("#loading").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						refreshGrid();
				}
			}); 
	
		 }  } );<!--- Close alert here --->
	
	
		return false; 
	}
	
	function ActivateCompanyAccount(companyAccountId,active)
	{
		var userRole='<cfoutput>#Session.UserRole#</cfoutput>';
		if(userRole != "SuperUser"){
			jAlert("You have no permission to active/deactive company account","Failed");
			return;
		}
		
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "Are you sure you want to activate this company account?", "Are you sure", 
		function(result) { 
			if(!result){
				return;
			}else{	
			  $.ajax({
			    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/company.cfc?method=activateCompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  { inpCompanyAccountId : companyAccountId, inpActive : active},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{								
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("activate", "Success!", function(result) 
																				{ 
																					
																				} );								
								}
																											
								$("#loading").hide();
						
							}
							else
							{<!--- Invalid structure returned --->	
								$("#loading").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						refreshGrid();
				}
			}); 
	
		 }  } );<!--- Close alert here --->
	
	
		return false; 
	}	
	
	
	function loadTaskListDialog(){
		UpdateCollaborationCheckListDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_CollaborationCheckList.cfm')
			.dialog({
				modal : true,
				title: 'Task List Tool',
				close: function() {
					 UpdateCollaborationCheckListDialog.remove(); UpdateCollaborationCheckListDialog = 0;
					 var grid = $("#TaskList");
					 grid.trigger("reloadGrid");
				},
				width: 751,
				position:'top',
				height: 463
			});
	
		UpdateCollaborationCheckListDialog.dialog('open');			
	}
	function loadEditDialog(){
		UpdateCompanyAccountDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_UpdateCompanyAccount1.cfm')
			.dialog({
				modal : true,
				title: 'Update Company Account',
				close: function() { UpdateCompanyAccountDialog.remove(); UpdateCompanyAccountDialog = 0; },
				width: 700,
				position:'top',
				height: 500,
			});
	
		UpdateCompanyAccountDialog.dialog('open');		
	}

	
	
	var UpdateCollaborationCheckListDialog =0;
	function UpdateCollaborationCheckListDialogCall(){
		if(typeof(lastsel) == "undefined")
		{
			jAlert("Please select one company to update", "Failure");
			return;		
		}

		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(UpdateCollaborationCheckListDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateCollaborationCheckListDialog.remove();
			UpdateCollaborationCheckListDialog = 0;
		}
						
		UpdateCollaborationCheckListDialog = $('<div></div>').append($loading.clone());
			  $.ajax({
			    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
				url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=getUserRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
				dataType: 'json',
				data:  {inpUserId:<cfoutput>"#Session.USERID#"</cfoutput>},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
				success:
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d2, textStatus, xhr ) 
				{
					<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
					var d = eval('(' + xhr.responseText + ')');
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];
								if(CurrRXResultCode > 0)
								{
									var userRole=	d.DATA.ROLE;
									if(userRole == 'SuperUser'){
										loadTaskListDialog();
									}else if(userRole == 'CompanyAdmin'){
										  $.ajax({
										    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
											url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=isBelongToACompanyAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
											dataType: 'json',
											data:  {inpCompanyAccountId:selectedCompanyAccountId,inpUserId:<cfoutput>"#Session.USERID#"</cfoutput>},					  
											error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
											success:
											<!--- Default return function for Do CFTE Demo - Async call back --->
											function(d2, textStatus, xhr ) 
											{
												<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
												var d = eval('(' + xhr.responseText + ')');
													<!--- Get row 1 of results if exisits--->
													if (d.ROWCOUNT > 0)
													{
														<!--- Check if variable is part of JSON result string --->								
														if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
														{
															CurrRXResultCode = d.DATA.RXRESULTCODE[0];
															if(CurrRXResultCode > 0)
															{
																result= d.DATA.RES;
																if(result=='true' || result==true){
																	loadTaskListDialog();
																}else{
																	jAlert("You are not admin of this company","You don't have permission to edit this company's Task List");
																}
															}
														}
														else
														{<!--- Invalid structure returned --->	
															jAlert("You don't belong to any company","You don't have permission to edit this company's Task List");
														}
													}
													else
													{<!--- No result returned --->
														<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
														jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
													}
											} 		
										});
									}else{
										console.log(d.DATA);
										jAlert("You don't have permission to edit this company","Failed to load dialog");
									}
								}else{
									jAlert("You don't have permission to edit this company","Failed to load dialog");
								}
							}
							else
							{<!--- Invalid structure returned --->	
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
				} 		
			});		

	
		return false;	
	}		
	//auto refresh grid
	function refreshGrid()
	{
	   var grid = $("#CompanyAccountList");
	   grid.trigger("reloadGrid");
	}

	$(document).ready(function() {
	refreshGrid();
	});	

	</script>
<cfoutput>
       
	  
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-CompanyList" >   
      
        <div style="table-layout:fixed;">
            <div id="pagerCompanyAccountList"></div>
            <div style="text-align:left;">
            <table id="CompanyAccountList"></table>
            </div>
        </div>
        
        
    </div>
        
     
	
</cfoutput>
<cfoutput>
	<div style="position:absolute; bottom:30px;">                           

 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockMCCompanyList" style="text-align:left;">
			<cfif isDefined("Session.userRole") and #Session.userRole# EQ "SuperUser">
            <div class="dock-container_BabbleMCCompany">
                <a class="dock-item BBMainMenu" href="##" onclick="AddNewCompanyAccountDialogCall(); return false;"><span>Add New Company Account</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Add new company account" /></a> 
            </div>
			</cfif>
            <div class="dock-container_BabbleMCCompany">
                <a class="dock-item BBMainMenu" href="##" onclick="UpdateCollaborationCheckListDialogCall(); return false;"><span>Update Tasklist</span><img src="../../public/images/dock/TaskList64x64II.png" alt="Update Tasklist" /></a> 
            </div>			
           
        </div>
</cfoutput>
	
</body>
</html>