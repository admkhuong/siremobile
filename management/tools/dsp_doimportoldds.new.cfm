
<cfsetting requesttimeout="7200">


<cfparam name="VerboseDebug" default="0" type="numeric">
<cfparam name="inpDialerIPAddr" default="0.0.0.0" type="string">
<cfparam name="inpLibId" default="0" type="numeric">
<cfparam name="inpUserId" default="0" type="numeric">
<cfparam name="inpOldLibId" default="0" type="numeric">
<cfparam name="inpForceUpdate" default="0">
<cfparam name="INPBATCHID" default="0" type="string">


<div id="loading">
<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">Processing...
</div>


<div id="DistributionLog" style=" border:solid; border-bottom-width:2px; height:500px; width:750px; overflow:auto; white-space:nowrap;">



<!--- Rewrite as AJAX version later if useful tool. Base on Scripts.cfc ValidateRemoteScriptData ValidateRemotePaths --->
<cfoutput>
inpLibId = #inpLibId# <BR />
inpUserID = #inpUserID# <BR />
inpOldLibId = #inpOldLibId# <BR />
</cfoutput>

<cfflush>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
            <cfset QuerySetCell(dataout, "inpNextEleId", "0") /> 
            <cfset QuerySetCell(dataout, "inpNextDataId", "0") /> 
            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "0") />  
            <cfset QuerySetCell(dataout, "message", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>                   
       
            <cftry> 
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<!---<cfinclude template="../../Session/Scripts/data_ScriptPaths.cfm">--->--->
                            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif inpUserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId)>
                    	<cfthrow message="Invalid Library Id Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                    
                                                                      
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            Desc_vch
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #inpUserID#
                            AND DSID_int = #inpLibId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                    
                                                         
                    <!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#\U#inpUserID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <cftry>
                        
							<!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                            <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                            <cfset CreationCount = CreationCount + 1> 
                            
                            <!--- Still doesn't exist - check your access settings --->
                            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                <cfthrow message="Unable to create remote user directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                            </cfif>
                        
                        <cfcatch type="any">
                        <cfoutput>#CurrRemoteUserPath# already exists</cfoutput> 
                        
                        </cfcatch>
                        
                        </cftry>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <cftry>
							<!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                            <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                            <cfset CreationCount = CreationCount + 1> 
                            
                            <!--- Still doesn't exist - check your access settings --->
                            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                <cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                            </cfif>

   						<cfcatch type="any">
                        <cfoutput>#CurrRemoteUserPath# already exists</cfoutput> 
                        
                        </cfcatch>
                        
                        </cftry>
                                                
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    
                    
                     
                    <!--- Get Legacy element(s) --->
                    <cfquery name="GetLegacyElements" datasource="rxds">
                        SELECT 
                            DSElementId_int,
                            Desc_vch                    
                        FROM
                            DynaScript.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = #inpOldLibId# 
                        ORDER BY
                            DSElementId_int ASC
                    </cfquery>
                    
                    
                    
                    <cfloop query="GetLegacyElements">
                    
                   	 	<cfoutput>GetLegacyElements.DSElementId_int = #GetLegacyElements.DSElementId_int# #GetLegacyElements.Desc_vch# <BR></cfoutput>
                   	 	<cfflush>
                        
                        
                        <!--- Add new Element --->
                        <!--- Convert or store as original? Convert on distro?--->
                         <!--- Assumes no Elements Created Yet - Add options if you want to change this --->
                   
                   
                   		<cftry>
							<!--- Add new element(s) --->
                            <cfquery name="AddNewElement" datasource="#Session.DBSourceEBM#">
                                INSERT INTO 
                                    rxds.dselement
                                (
                                    DSEId_int,
                                    DSID_int,
                                    UserId_int,
                                    Active_int,
                                    Desc_vch
                                )
                                VALUES
                                (
                                    #GetLegacyElements.DSElementId_int#,
                                    #inpLibId#,
                                    #inpUserID#,
                                    1,
                                    '#GetLegacyElements.Desc_vch#'                                
                                )
                            </cfquery>

						<cfcatch type="any">
                        <cfoutput>#GetLegacyElements.DSElementId_int# already exists</cfoutput> 
                        
                        </cfcatch>
                        
                        </cftry>
                                            
                    
    	                <!--- Does ELE Directory Exist --->
						<cfset CurrRemoteUserPath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#\E#GetLegacyElements.DSElementId_int#">
                        
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            
                            <cftry>
								<!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                <cfdirectory action="create" directory="#CurrRemoteUserPath#">
        
                                <cfset CreationCount = CreationCount + 1> 
                                
                                <!--- Still doesn't exist - check your access settings --->
                                <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                    <cfthrow message="Unable to create remote user script element directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                                </cfif>

                            <cfcatch type="any">
                            <cfoutput>#CurrRemoteUserPath# already exists</cfoutput> 
                            
                            </cfcatch>
                            
                            </cftry>    
                                
                            
                        <cfelse>
                        
                            <cfset ExistsCount = ExistsCount + 1> 
                        
                        </cfif>
                            
                            
                    	<!--- Get Legacy scripts --->
                        <cfquery name="GetLegacyScripts" datasource="rxds">
                            SELECT 
                                DataId_int,
                                DSElementId_int,
                                DSId_int,
                                StatusId_int,
                                AltId_vch,
                                Desc_vch
                            FROM
                                DynaScript.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = #inpOldLibId#
                                AND DSElementId_int = #GetLegacyElements.DSElementId_int#
                            ORDER BY
                                DataId_int ASC
                        </cfquery>  
                        
                        <cfloop query="GetLegacyScripts">
                        	
                            <cfset LegacyScriptPath = "">
                            <cfset MainfilePath = "">
                            
<!--- Legacy Storage Format \\10.0.1.10\DynaScript\LibId\DSSD_%s_%s_%s.wav - CurrScriptDataId, Lib, Element --->
    
                            <cfset LegacyScriptPath = "#rxdsLegacyReadPath#\#inpOldLibId#\DSSD_#GetLegacyScripts.DataId_int#_#inpOldLibId#_#GetLegacyElements.DSElementId_int#.wav">
                              
                            <cfset LegacyScriptPathFileExists = FileExists(LegacyScriptPath)>                               
                                  
                            <cfset MainfilePath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#\E#GetLegacyElements.DSElementId_int#\rxds_#inpUserID#_#inpLibId#_#GetLegacyElements.DSElementId_int#_#GetLegacyScripts.DataId_int#.wav">
                            
                            <cfset FinalFilePath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#\E#GetLegacyElements.DSElementId_int#\rxds_#inpUserID#_#inpLibId#_#GetLegacyElements.DSElementId_int#_#GetLegacyScripts.DataId_int#.mp3">
                           
                    
                    <cfoutput>
                            GetLegacyScripts = #GetLegacyScripts.Desc_vch# <BR>
                            LegacyScriptPath = #LegacyScriptPath# <BR>
                            MainfilePath = #MainfilePath# <BR> 
                            FinalFilePath = #FinalFilePath# <BR>                            
                   </cfoutput>
                   <cfflush>         
                          
							<cfif !FileExists(MainfilePath)>
                            
								<!--- Add new scripts(s) --->
                                <cfquery name="AddNewScripts" datasource="#Session.DBSourceEBM#">
                                    INSERT INTO 
                                        rxds.scriptdata
                                    (
                                        DataId_int,
                                        DSEId_int,
                                        DSID_int,
                                        UserId_int,
                                        Active_int,
                                        Desc_vch,
                                        StatusId_int,
                                        AltId_vch,
                                        Length_int,
                                        Format_int,
                                        Created_dt                                    
                                    )
                                    VALUES
                                    (
                                        #GetLegacyScripts.DataId_int#,
                                        #GetLegacyElements.DSElementId_int#,
                                        #inpLibId#,
                                        #inpUserID#,
                                        1,
                                        '#GetLegacyScripts.Desc_vch#',
                                        1,
                                        '#GetLegacyScripts.AltId_vch#',
                                        0,
                                        0,
                                        NOW()
                                    )
                                </cfquery>                           


        
                                <cfif LegacyScriptPathFileExists>
                                <!--- Copy file --->
                                    <cffile action="copy" source="#LegacyScriptPath#" destination="#MainfilePath#" nameconflict="overwrite" >                   File Copied OK<BR> 
                                 
                                    <cfif fileexists('#FinalFilePath#')>
                                        <cffile action="delete" file="#FinalFilePath#">
                                    </cfif>
                                    
                                    <cfexecute 	name="#SOXAudiopath#" 
                                            arguments='#MainfilePath# -C 128.3 #FinalFilePath#' 
                                            timeout="60"> 
                                    </cfexecute>
                                            
                                    <!---- END: conversion --->                                                    
                                 
                                    <!--- Convert file to mp3 --->	                             
                                 
                                <cfelse>
                                     <cfoutput>File #LegacyScriptPath# does not exist<BR> </cfoutput>
                                </cfif>
                            


							</cfif>
                            
                        
                             
                                                
                        </cfloop>      
                            
                    
                    </cfloop>
                                              
                         
	             </cfif>  
                 
                 
                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                <cfset QuerySetCell(dataout, "inpNextEleId", "0") /> 
                <cfset QuerySetCell(dataout, "inpNextDataId", "0") /> 
                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "0") />  
                <cfset QuerySetCell(dataout, "message", "Processed OK") />   
            
    
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                <cfset QuerySetCell(dataout, "inpNextEleId", "0") /> 
	            <cfset QuerySetCell(dataout, "inpNextDataId", "0") /> 
                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "0") /> 
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                                                                      
                                                         
            </cfcatch>
            
            </cftry>   

<cfoutput>		
            <cfdump var="#dataout#">     
</cfoutput>

</div>
    
                    
                    
                    
                    
                    
                    
                    <!--- 
                    
                    
                    
                    
                                        
                    <cfif inpNextEleId EQ 0><!--- What data to look for--->
                    	<!--- Get first element --->
                        <cfquery name="GetFirstElement" datasource="rxds">
                            SELECT 
                                DSEId_int,
                                Desc_vch                    
                            FROM
                                rxds.dselement
                            WHERE
                                Active_int = 1
                                AND DSID_int = #inpLibId#                                
                                AND UserId_int = #inpUserID#
                            ORDER BY
                                DSEId_int ASC
                            LIMIT 1            
                        </cfquery>
                        
                        <cfif GetFirstElement.RecordCount EQ 0><!--- Check Element Records --->
							
                            <!--- All still good? Warning Only? TTS Only? --->
							<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                            <cfset QuerySetCell(dataout, "inpNextEleId", "-1") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "-1") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                            <cfset QuerySetCell(dataout, "message", "WARNING - No Elements Found in Library ID (#inpLibId#) Library Name(#ValidateLibId.Desc_vch#") />
                            
                            
                        <cfelse><!--- Check Element Records --->
                        	
							<!--- Get first script --->
                            <cfquery name="GetFirstScript" datasource="rxds">
                                SELECT 
                                    DataId_int,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    Desc_vch
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#
                                    AND DSEId_int = #GetFirstElement.DSEId_int#
                                    AND UserId_int = #inpUserID#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
							<cfif GetFirstScript.RecordCount EQ 0><!--- Check Script Records --->
                                                            
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="rxds">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#                                
                                        AND UserId_int = #inpUserID#
                                        AND DSEId_int > #GetFirstElement.DSEId_int#
                                    ORDER BY
                                        DSEId_int ASC
                                    LIMIT 1              
                                </cfquery>
                                                                
                                <cfif GetNextElement.RecordCount EQ 0>
                                	<cfset inpNextEleId = -1>
                                    <cfset inpNextDataId = -1>
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                    <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                    <cfset QuerySetCell(dataout, "message", "WARNING - Finished - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#) - No more Elements found.")  />
                                
                                <cfelse>                                
	                                <cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset inpNextDataId = -1>
                                    
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 7) />
                                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                    <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                    <cfset QuerySetCell(dataout, "message", "WARNING - Moving on - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#")  />
                                
                                </cfif>
                                
                            <cfelse><!--- Check Script Records --->
                            	<!--- Get current side last modified date --->                                
                                <cfset MainfilePath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#\E#GetFirstElement.DSEId_int#\rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
							<!--- 	<cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                                <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
 --->
  
								<cfset MainPathFileExists = FileExists(MainfilePath)>
                                <cfif MainPathFileExists> 
                                    <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                                <cfelse>
                                     <cfset MainfileDate = '1901-01-01 00:00:00'>
                                </cfif>
                            
 
                                <!--- Get remote last modified date --->
                              	<cfset RemotefilePath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#inpUserID#\L#inpLibId#\E#GetFirstElement.DSEId_int#\rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
								<!--- <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                                <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())> --->
								<cfset RemotePathFileExists = FileExists(RemotefilePath)>
                                <cfif RemotePathFileExists> 
									<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                                <cfelse>
                                     <cfset RemotefileDate = '1900-01-01 00:00:00'>

                                </cfif>
                                
                                <!--- Start Result set Here --->
                                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>        
                                <cfset QueryAddRow(dataout) />                     	
                                
                                                        
                                <!--- if same dont update unless forced too --->
								<cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                               
                                    <cfif MainPathFileExists>
                                        
                                        <!--- Copy file --->
                                        <cffile action="copy" source="#MainfilePath#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                        
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                        
                                        <cfif RemotePathFileExists>                                        
                                        	<cfset QuerySetCell(dataout, "message", "OK - File Copied - rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />                             
                             			<cfelse>                                         
										 	<cfset QuerySetCell(dataout, "message", "OK - File Copied - rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                             			
                                        </cfif>
                             
                             
                                       
                                     <cfelse>
                                        
                                          <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                          <cfset QuerySetCell(dataout, "message", "WARNING - File Does not Exists - rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch#")  />
                                        
                                     </cfif>
                                     
                                     
                                <cfelse><!--- Date Compare --->
                                
                                    <!--- Skip file ---> 
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                    
                                    <cfif RemotePathFileExists> 
	                                    <cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                    <cfelse>                                    
                                     	<cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - rxds_#inpUserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                                     
                                    </cfif>
                                 
                                </cfif><!--- Date Compare --->
                            
                                
                                
                                <!--- Check for next script --->
                                <!--- Get Next script --->
                                <cfquery name="GetNextScript" datasource="rxds">
                                    SELECT 
                                        DataId_int                                        
                                    FROM
                                        rxds.scriptdata
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#
                                        AND DSEId_int = #GetFirstElement.DSEId_int#
                                        AND UserId_int = #inpUserID#
                                        AND DataId_int > #GetFirstScript.DataId_int#
                                    ORDER BY
                                        DataId_int ASC
                                    LIMIT 1              
                                </cfquery>                               		
                                                            
                                <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records II--->
                                	                                
                                    <!--- Move on - Get next element --->                              
                                    <cfquery name="GetNextElement" datasource="rxds">
                                        SELECT 
                                            DSEId_int,
                                            Desc_vch                    
                                        FROM
                                            rxds.dselement
                                        WHERE
                                            Active_int = 1
                                            AND DSID_int = #inpLibId#                                
                                            AND UserId_int = #inpUserID#
                                            AND DSEId_int > #GetFirstElement.DSEId_int#
                                        ORDER BY
                                            DSEId_int ASC            
                                        LIMIT 1  
                                    </cfquery>
                                    
                                    <cfif GetNextElement.RecordCount EQ 0>
                                        <cfset inpNextEleId = -1>
                                        <cfset inpNextDataId = -1>
                                    <cfelse>                                
                                        <cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                        <!--- Start at first script ID --->
                                        <cfset inpNextDataId = -1>
                                    </cfif>
                                                                    
                                <cfelse><!--- Check Script Records II --->
    	                            <cfset inpNextEleId = GetNextElement.DSEId_int>
	   								<cfset inpNextDataId = GetNextScript.DataId_int>
                                </cfif><!--- Check Script Records II --->       
                                
                                <!--- All still good --->
                           		<cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />       
                                <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") />  
                                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                                        
							</cfif><!--- Check Script Records --->

                        </cfif><!--- Check Element Records --->
                        
                    
                    <cfelseif inpNextEleId GT 0><!--- What data to look for--->
                    	<!--- Get as specified --->
                        <!--- Get script ---> <!--- Always start where you specify and work your way up --->
                        <cfquery name="GetScript" datasource="rxds">
                            SELECT 
                                DataId_int,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                Desc_vch
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = #inpLibId#
                                AND DSEId_int = #inpNextEleId#
                                AND UserId_int = #inpUserID#
                                AND DataId_int > #inpNextDataId# - 1
                            ORDER BY
                                DataId_int ASC
                            LIMIT 1              
                        </cfquery>      
                        
                       <cfif GetScript.RecordCount EQ 0><!--- Check Script Records III --->
                       
                       		<!--- Move on - Get next element --->                              
                            <cfquery name="GetNextElement" datasource="rxds">
                                SELECT 
                                    DSEId_int,
                                    Desc_vch                    
                                FROM
                                    rxds.dselement
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#                                
                                    AND UserId_int = #inpUserID#
                                    AND DSEId_int > #inpNextEleId#
                                ORDER BY
                                    DSEId_int ASC            
                                LIMIT 1  
                            </cfquery>
                            
                            <cfset LastinpNextEleId = inpNextEleId>
                            
                            <cfif GetNextElement.RecordCount EQ 0>
                                <cfset inpNextEleId = -1>
                                <cfset inpNextDataId = -1>
                            <cfelse>                                
                                <cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                <!--- Start at first script ID --->
                                <cfset inpNextDataId = -1>
                            </cfif>
                                                  
                            
                            <!--- All still good? Warning Only? TTS Only? --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                            <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                            <cfset QuerySetCell(dataout, "message", "WARNING - Moving on - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#LastinpNextEleId#)")  />
                                
                       
                       <cfelse><!--- Check Script Records III --->
                       <!--- Get current side last modified date --->                                
							<cfset MainfilePath = "#rxdsLocalWritePath#\U#inpUserID#\L#inpLibId#\E#inpNextEleId#\rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav">
                           <!---  <cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                            <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
                            --->     
                            <cfset MainPathFileExists = FileExists(MainfilePath)>
                            <cfif MainPathFileExists> 
                            	<cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                            <cfelse>
	                             <cfset MainfileDate = '1901-01-01 00:00:00'>
                            </cfif>
                                                        
                            <!--- Get remote last modified date --->
                            <cfset RemotefilePath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#\U#inpUserID#\L#inpLibId#\E#inpNextEleId#\rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav">
                          <!---   <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                            <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())>
                             --->
                            <cfset RemotePathFileExists = FileExists(RemotefilePath)>
                            <cfif RemotePathFileExists> 
                            	<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                            <cfelse>
	                             <cfset RemotefileDate = '1900-01-01 00:00:00'>
                            </cfif>
                             
                            <!--- Start Result set Here --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>                          
                            <cfset QueryAddRow(dataout) />   	
                            
                            <!--- if same dont update unless forced too --->
                            <cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                           
                           		<cfif MainPathFileExists>
                                    
                                    <!--- Copy file --->
                                    <cffile action="copy" source="#MainfilePath#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                    
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                    
                                    <cfif RemotePathFileExists> 
                                    	<cfset QuerySetCell(dataout, "message", "OK - File Copied - rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />			<cfelse>
                                       	<cfset QuerySetCell(dataout, "message", "OK - File Copied - rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />		                                     </cfif>
                                    
                                 <cfelse>
                                    
                                      <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                      <cfset QuerySetCell(dataout, "message", "WARNING - Main File Does not Exists - rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch#")  />
                                    
                                 </cfif>
                                 
                                 
                            <cfelse><!--- Date Compare --->
                            
                                <!--- Skip file ---> 
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                
								<cfif RemotePathFileExists>
	                                <cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                <cfelse>
									<cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - rxds_#inpUserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />
                                </cfif>
                             
                            </cfif><!--- Date Compare --->
                            
                            
                            <!--- Check for next script --->
                            <!--- Get Next script --->
                            <cfquery name="GetNextScript" datasource="rxds">
                                SELECT 
                                    DataId_int                                        
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#
                                    AND DSEId_int = #inpNextEleId#
                                    AND UserId_int = #inpUserID#
                                    AND DataId_int > #GetScript.DataId_int#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
                            <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records IV--->
                                                                
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="rxds">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#                                
                                        AND UserId_int = #inpUserID#
                                        AND DSEId_int > #inpNextEleId#
                                    ORDER BY
                                        DSEId_int ASC            
                                    LIMIT 1  
                                </cfquery>
                                
                                <cfif GetNextElement.RecordCount EQ 0>
	                                <!--- If no more data and no more elements then just exit --->
                                    <cfset inpNextEleId = -1>
                                    <cfset inpNextDataId = 0>
                                <cfelse>                                
                                    <cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset inpNextDataId = 0>
                                </cfif>
                                                                
                            <cfelse><!--- Check Script Records IV --->
                                <cfset inpNextEleId = inpNextEleId>
                                <cfset inpNextDataId = GetNextScript.DataId_int>
                            </cfif><!--- Check Script Records IV --->       
                            
                            <!--- All still good --->
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />       
                            <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                       
                       </cfif><!--- Check Script Records III --->
                    
                    <cfelse><!--- What data to look for--->
                    	<!--- No valid data found --->
                         	<cfthrow message="Invalid Data Specified" type="Any" detail="Data specified in the request is invalid." errorcode="-4">  
                    
                    </cfif><!--- What data to look for--->
                        
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 
		            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") /> 
                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->       --->  
                           
                     
<!--- 

<script type="text/javascript">

		
		alert('Yo!')
		$("#DistributionLog").append('Starting...' + '<br>');
		ValidateRemoteScriptPaths();
		
</script>

 --->