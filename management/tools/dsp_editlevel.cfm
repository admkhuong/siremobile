
<cfparam name="SESSION.CompanyUserId" default="#SESSION.USERID#">

<script>

var permissionIds=0;
var selectedLevelId=0;
var localSelectedValues=new Array();
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#PermissionList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=getPermissionListData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 200,
		colNames:['<font size="2"><b>Permission Id</b></font>','<font size="2"><b>Permission Section</b></font>'],
		colModel:[			
			{name:'PermissionId_int',index:'PermissionId_int', width:100, editable:false},
			{name:'PermissionSection_vch',index:'PermissionSection_vch', width:400, editable:true}
		],
		rowNum:500,
	   	rowList:[20,4,8,10,30],
		mtype: "POST",
		pager: jQuery('#pagerEditLevel'),
		toppager: false,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'PermissionId_int',
		//toolbar:[true,"both"],
		viewrecords: true,
		sortorder: "asc",
		
		multiselect: true,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		 loadComplete: function(data){
				reloadPermissionOfLevel();
				return false;	 
   			},		
			onSelectRow: function(PermissionId_int,status){
<!--- 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				} --->
			},   				
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerEditLevel',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_PermissionList").height(55);

	
	$("#update_permissions").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#update_permissions").click(function() { 
		var needUpdateList=new Array();
		var needRemoveList=new Array();
		needUpdateList=getNeedUpdateList(permissionIds,jQuery("#PermissionList").getGridParam('selarrrow'));
		needRemoveList=getNeedRemoveList(jQuery("#PermissionList").getGridParam('selarrrow'),permissionIds);
		UpdatePermissionOfLevel(selectedLevelId,needUpdateList,needRemoveList);
		//reloadPermissionOfLevel();
	});	

	$("#loading").hide();
		
});
	//remote existing element in array1 from array2]
	
	function getNeedUpdateList(originalList,selectedList){
		var result=new Array();
		if(originalList.length > 0 && selectedList.length > 0){
			for(i =0;i<selectedList.length;i++){
				if($.inArray(selectedList[i]*1,originalList) < 0){
					result.push(selectedList[i]*1);
				}
			}
		}else
		{
			return selectedList;
		}
		return result;
	}
	
	//get the not existing element from array 1 from array2
	function getNeedRemoveList(selectedList,originalList){
		var res=new Array();
		if(selectedList.length>0 && originalList.length>0){
			for(i=0;i<originalList.length;i++){
				if($.inArray(originalList[i]+"",selectedList) < 0){
					res.push(originalList[i]*1);
				}
			}
		}else{
			return originalList;
		}
		return res;
	}

	function UpdatePermissionOfLevel(inpLevelId,inpPermissions,RemoveList){
		//alert(jQuery("#PermissionList").getGridParam('selarrrow'));
		//jQuery("#PermissionList").setSelection(1,false);
		$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=updatePermissionOfLevel&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpLevelId : inpLevelId,inpPermissions : inpPermissions.join('~'),inpToRemovePermissions : RemoveList.join('~')}, 
		<!--- Default return function for Do CFTE Demo - Async call back --->
		function(d) 
		{
			<!--- Alert if failure --->
																						
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];
							
						
						if(CurrRXResultCode > 0)
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							//jAlert("Update Permissions sucessfully", "Success!", function(result) { //$("#loadingDlgLinkUserToCompany").hide();EditLevelDialog.remove();} );	
							$.jGrowl("Update Permissions sucessfully", { life:2000, position:"center", header:' Success' });																									
						}
						else
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Update failed.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
						}
					}
					else
					{<!--- Invalid structure returned --->	
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
				
				$("#loadingDlgLinkUserToCompany").hide();
		} );	
	return false;			
	}
	function reloadPermissionOfLevel(){
	 	selectedLevelId=lastsel-0;
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=getPermissionsOfLevel&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpLevelId : lastsel }, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{			
																
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							permissionIds=d.DATA.PERMISSIONS[0];
							//set local selected values to array of original permissions
							localSelectedValues=permissionIds;
							//jQuery("#PermissionList").resetSelection();
							if(permissionIds.length > 0)
							{
								for(var i=0;i<=permissionIds.length;i++){
									jQuery("#PermissionList").setSelection(permissionIds[i],false);
								}
							}
	<!--- 									else
										{
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
										} --->
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}

			} );		
	}

</script>



<BR />

<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->

<BR />

<div style="padding-left:40px"><table id="PermissionList" align="center"></table></div>
<div id="pagerEditLevel"></div>