
<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
}
#LevelList tr td
{
	text-align: center;
}


</style>


<script>

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
	
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#LevelList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/permission.cfc?method=getLevelListData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 385,
		colNames:['<font size="2"><b>UserLevel Id</b></font>',
					'<font size="2"><b>User Level</b></font>',
					'<font size="2"><b>Description</b></font>',
					'<font size="2"><b>Options &nbsp;&nbsp;&nbsp;</b></font>'],
		colModel:[			
			{name:'UserLevelId_int',index:'UserLevelId_int', width:200, editable:false},
			{name:'UserLevel',index:'UserLevel', width:200, editable:true},
			{name:'UserLevelDescription',index:'UserLevelDescription', width:200, editable:true},
			{name:'Options',index:'Options', width:100, editable:false, align:'right', sortable:false, resizable:false}			
		],
		rowNum:15,
	   	//rowList:[20,4,8,10,30],
		mtype: "POST",
		//pager: jQuery('#pagerLevelList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'UserLevelId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(UserLevelId_int){
				if(UserLevelId_int && UserLevelId_int!==lastsel){
					jQuery('#LevelList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=UserLevelId_int;
				}
			},
		 loadComplete: function(data){ 	
		 		 				 		  
				$(".del_Level").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_Level").click( function() { DeleteLevel($(this).attr('rel')); } );
				$(".edit_Level").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_Level").click( function() { editLevel(); } );
   			},			
			
	//	editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerLevelList',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_LevelList").height(65);

	
	
	$("#get_Billing").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#get_Billing").click(function() { CreateGetBalanceDialogCall(); });	
	
	$("#upd_Bal").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#upd_Bal").click(function() { CreateUpdateBalanceDialogCall(); });	
				
	$("#upd_Rate").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#upd_Rate").click(function() { CreateUpdateUserRate1DialogCall(); });

	$("#add_company_acc").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#add_company_acc").click(function() { AddNewCompanyAccountDialogCall(); });		

	$("#loadingLevel").hide();
		
});

function DeleteLevel(levelId)
{	
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "Are you sure?", "About to delete a Level", 
	function(result) { 
		if(!result){
			return;
		}else{	
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=deleteLevel&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpLevelId : lastsel},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Deleted", "Success!", function(result) 
																			{ 
																				
																			} );								
							}
																										
							$("#loadingLevel").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingLevel").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					gridReload();
			} 		
		}); 

	 }  } );<!--- Close alert here --->


	return false; 
}
	var EditLevelDialog = 0;
	function editLevel(userLevelId){
		var $loadingLevel = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		if(EditLevelDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			EditLevelDialog.remove();
			EditLevelDialog = 0;
		}
		EditLevelDialog = $('<div></div>').append($loadingLevel.clone());
		EditLevelDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/tools/dsp_EditLevel.cfm')
			.dialog({
				modal : true,
				title: 'Associate Permissions to Level',
				close: function() {
					 EditLevelDialog.remove(); 
					 EditLevelDialog = 0; 
					 var grid = $("#LevelList");
					 grid.trigger("reloadGrid");
				},
				width: 600,
				position:'top',
				height: 'auto',
				buttons:[
					{
						text:'Update',
						click:function(){
							var needUpdateList=new Array();
							var needRemoveList=new Array();
							needUpdateList=getNeedUpdateList(permissionIds,jQuery("#PermissionList").getGridParam('selarrrow'));
							needRemoveList=getNeedRemoveList(jQuery("#PermissionList").getGridParam('selarrrow'),permissionIds);
							UpdatePermissionOfLevel(selectedLevelId,needUpdateList,needRemoveList);							
						}
					},
					{
						text: 'Cancel',
						click:function(){
							EditLevelDialog.remove(); 
							EditLevelDialog = 0;
						}
					}
				]				
			});
	
		EditLevelDialog.dialog('open');
	
		return false;			
	}

function gridReload(){
	 var grid = $("#LevelList");
	 grid.trigger("reloadGrid");
}	

</script>





<BR />

<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->

<BR />


<table id="LevelList"></table>
<div id="pagerLevelList"></div>

