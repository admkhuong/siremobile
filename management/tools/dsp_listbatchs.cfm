<h4>List Batchs</h4>
<script>

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
	
$(function() {
<!---LoadSummaryInfo();--->
	
	jQuery("#listbilling").jqGrid({      
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/batch.cfc?method=getListBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialMediaFlag=1',
		datatype: "json",
		height: 385,
		colNames:['Master RXCall Detail', 'BatchId', 'Total Call Time', 'Message Delivered','CallStartTime','CallEndTime', 'Options'],
		colModel:[			
			{name:'MasterRXCallDetailId_int',index:'MasterRXCallDetailId_int', width:100, editable:false, resizable:false},
			{name:'BatchId_bi',index:'BatchId_bi', width:100, editable:true, resizable:false},
			{name:'TotalCallTime_int',index:'TotalCallTime_int', width:100, editable:true, resizable:false},
			{name:'MessageDelivered_si',index:'MessageDelivered_si', width:100, editable:true, resizable:false},
			{name:'CallStartTime',index:'CallStartTime', width:130, editable:true, resizable:false},
			{name:'CallEndTime',index:'CallEndTime', width:130, editable:true, resizable:false},
			{name:'Options',index:'Options', width:105, editable:false, align:'right', sortable:false, resizable:false}
		],
		rowNum:15,
	   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
		toppager: true,
    	emptyrecords: "No billing.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'BatchId_bi',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
		
		onSelectRow: function(MasterRXCallDetailId_int){
				if(UserId_int && MasterRXCallDetailId_int!==LastSelBatchesMCContent){
					$('#listbilling').jqGrid('restoreRow',LastSelBatchesMCContent);
					//$('#BatchListMCContent').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEditMCContent,'', '');
					LastSelBatchesMCContent=MasterRXCallDetailId_int;
				}
			},
		 loadComplete: function(data){ 		
		 	 
		 <!--- $(this).attr({"title":'Reports Viewer'}); --->
				$(".view_ReportBilling").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_ReportBilling").click( function() { ViewReportBilling($(this).attr('rel')); } );			
		  
			
				
				$(".del_billing").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_billing").click( function() { 
				
												$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
												$.alerts.cancelButton = '&nbsp;No&nbsp;';		
												
												var CurrREL = $(this).attr('rel');
												
												jConfirm( "Are you sure you want to delete this Billing?", "About to delete Billing.", function(result) { 
													if(result)
													{	
														DelBilling(CurrREL);
													}
													return false;																	
												});
																 
											} );
			

				<!--- Bind all search functionality to new row --->
				$("#BatchListMCContent #search_notes").unbind();
				$("#BatchListMCContent #search_notes").keydown(function(event) { doSearchMCContent(arguments[0]||event,'search_notes') }); 
				
				<!--- Reset the focus to an inline filter box--->
				if(typeof(lastObj_MCContent) != "undefined" && lastObj_MCContent != "")
				{					
					<!--- Stupid IE not rendering yet so call again in 10 ms--->
					setTimeout(function() 
					{ 
					
						 var LocalFocus = $("#BatchListMCContent #" + lastObj_MCContent);
						 if (LocalFocus.setSelectionRange) 
						 {         
							 <!--- ... then use it        
							   (Doesn't work in IE)
							   Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh. 
							  ---> 
							   var len = $(LocalFocus).val().length * 2;         
							   LocalFocus.setSelectionRange(len, len);         
						 }        
						 else
  			          	 {
						     <!---... otherwise replace the contents with itself         
							   (Doesn't work in Google Chrome)    --->     
							  $(LocalFocus).focus(); 
							  var str = $(LocalFocus).val();
							  $(LocalFocus).val("");    
							  $(LocalFocus).val(str);        
					     }         
						 
						 <!--- Scroll to the bottom, in case we're in a tall textarea
						  (Necessary for Firefox and Google Chrome)--->
						 LocalFocus.scrollTop = 999999; 
						 
						
						lastObj_MCContent = undefined;
						$("#BatchListMCContent #" + lastObj_MCContent).focus(); 
					}, 10);
				}
			
				
   			} ,	
		<!---editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	--->
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default second column as ID
			  }	
	});

	$("#t_LevelList").height(65);

	$("#loading").hide();
		
});

function gridReload(){
	 var grid = $("#listbilling");
	 grid.trigger("reloadGrid");
}	

<!--- Global so popup can refernece it to close it--->
var CreateViewReportBilling = 0;
function ViewReportBilling(UserId_int) {
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(UserId_int) != "undefined" && UserId_int != "")					
		ParamStr = '?UserId_int=' + encodeURIComponent(UserId_int);
	else
		UserId_int = 0;
	if ( CreateViewReportBilling != 0 ) {
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateViewReportBilling.remove();
		CreateViewReportBilling = 0;
	}
	CreateViewReportBilling = $('<div></div>').append($loading.clone());
	CreateViewReportBilling
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reports/dsp_ReportBilling.cfm' + ParamStr)
		.dialog({
			modal : true,
			title: 'Reporting Billing {' + UserId_int + '}',
			width: 1200,
			minHeight: 400,
			height: 'auto',
			position: 'top' 
		});

		CreateViewReportBilling.dialog('open');
}
function rx_AfterEditMCContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchListMCContent").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{							
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{								
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESC[0]) != "undefined")								
				{
					$("#listbilling").jqGrid('setRowData',rowid,{PermissionSection_vch:d.DATA.INPDESC[0]});
				}
				
			}
				
		}
		else
		{<!--- Invalid structure returned --->	
			
		}
	}
	else
	{<!--- No result returned --->
		
	}  
};

function DelBilling(UserId_int)
{
//	$("#loadingDlgAddBatchMCContent").show();	
		
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { UserId_int : UserId_int},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								gridReloadBatchesMCContent();										
								return false;
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Campaign has NOT beed deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
		//			$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
								
			} 		
			
		});
	
		return false;

	
	
}
</script>


<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->

<div >
	<table id="listbilling" align="center"></table>
</div>
<div id="pagerb"></div>



