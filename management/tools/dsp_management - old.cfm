







<script TYPE="text/javascript">
	
$(function() {


	$("#ProcessCallQueueCalls").click( function() { ProcessCallQueueDialogCall(); return false;  }); 
	
	$("#ExtractCallResults").click( function() { ProcessExtractCallResults(); return false;  }); 


	$("#RegisterNewAccountButton").click( function() { RegisterNewAccountDialogCall(); return false;  }); 	
	$("#ScheduleTestingButton").click( function() { ScheduleTestingDialogCall(); return false;  }); 	
	$("#addgroupToCallQueueButton").click( function() { addgroupToCallQueueDialogCall(); return false;  }); 	
	$("#AddSocialmediaToCallQueueButton").click( function() { addgroupToCallQueueDialogCall(1); return false;  }); 	
	$("#AddSingleToCallQueueButton").click( function() { return false;  }); 	
	
	$("#GetUserBalance").click( function() { CreateGetBalanceDialogCall(); return false;  }); 	
	$("#UpdateUserBalance").click( function() { CreateUpdateBalanceDialogCall(); return false;  }); 	



	$("#loadingDlgManagment").hide();
	
	
});


var ProcessCallQueueDialog = 0;

function ProcessCallQueueDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ProcessCallQueueDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ProcessCallQueueDialog.remove();
		ProcessCallQueueDialog = 0;
		
	}
					
	ProcessCallQueueDialog = $('<div></div>').append($loading.clone());
	
	ProcessCallQueueDialog
		.load('<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/devjlp/simpleframedx/management</cfoutput>/processing/act_processcontactqueue.cfm?verbosedebug=1')
		.dialog({
			modal : true,
			title: 'Publish Queued Calls To Dialers',
			close: function() { ProcessCallQueueDialog.remove(); ProcessCallQueueDialog = 0; },
			width: 600,
			position:'top',
			height: 'auto'
		});

	ProcessCallQueueDialog.dialog('open');

	return false;		
}	


var ProcessExtractCallResultsDialog = 0;
function ProcessExtractCallResults()
{				
	var $loading = $('<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ProcessExtractCallResultsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ProcessExtractCallResultsDialog.remove();
		ProcessExtractCallResultsDialog = 0;
		
	}
					
	ProcessExtractCallResultsDialog = $('<div></div>').append($loading.clone());
	
	ProcessExtractCallResultsDialog
		.load('<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/devjlp/simpleframedx/management</cfoutput>/processing/act_processextractionrxdialers.cfm?verbosedebug=1')
		.dialog({
			modal : true,
			title: 'Extract Call Results from RXDialers',
			close: function() { ProcessExtractCallResultsDialog.remove(); ProcessExtractCallResultsDialog = 0; },
			width: 600,
			position:'top',
			height: 'auto'
		});

	ProcessExtractCallResultsDialog.dialog('open');

	return false;		
}	




var addgroupToCallQueueDialog = 0;

function addgroupToCallQueueDialogCall(inpSocialmediaFlag)
{				
	var $loading = $('<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	var ParamStr = '';
						
	if(typeof(inpSocialmediaFlag) != "undefined" && inpSocialmediaFlag != "")
	{					
		ParamStr = '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/distribution/dsp_AddSocialmediaToQueue.cfm?inpSocialmediaFlag=1';
	}
	else
	{
		ParamStr = '<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/distribution/dsp_addgroupToQueue.cfm'; 
	}
	<!--- Erase any existing dialog data --->
	if(addgroupToCallQueueDialog != 0)
	{
		addgroupToCallQueueDialog.remove();
		addgroupToCallQueueDialog = 0;
	}
					
	addgroupToCallQueueDialog = $('<div></div>').append($loading.clone());
	
	
	addgroupToCallQueueDialog
		.load(ParamStr)
		.dialog({
			modal : true,
			title: 'Publish To Call Queue',
			width: 700,
			position:'top',
			height: 'auto'
		});

	addgroupToCallQueueDialog.dialog('open');

	return false;		
}	


var RegisterNewAccountDialog = 0;

function RegisterNewAccountDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(RegisterNewAccountDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		RegisterNewAccountDialog.remove();
		RegisterNewAccountDialog = 0;
		
	}
					
	RegisterNewAccountDialog = $('<div></div>').append($loading.clone());
	
	RegisterNewAccountDialog
		.load('<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/account/dsp_RegisterNewUser.cfm')
		.dialog({
			modal : true,
			title: 'Register a new user account.',
			close: function() { RegisterNewAccountDialog.remove(); RegisterNewAccountDialog = 0; },
			width: 500,
			position:'top',
			height: 'auto'
		});

	RegisterNewAccountDialog.dialog('open');

	return false;		
}


var ScheduleTestingDialog = 0;

function ScheduleTestingDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ScheduleTestingDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ScheduleTestingDialog.remove();
		ScheduleTestingDialog = 0;
		
	}
					
	ScheduleTestingDialog = $('<div></div>').append($loading.clone());
	
	ScheduleTestingDialog
		.load('<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/schedule/dsp_schedule.cfm')
		.dialog({
			modal : true,
			close: function() { ScheduleTestingDialog.remove(); ScheduleTestingDialog = 0; },
			title: 'Schedule Options',
			width: 350,
			position:'top',
			height: 'auto'
		});

	ScheduleTestingDialog.dialog('open');

	return false;		
}	

</script>

<div>
	
	<label>Management Tools</label>
	<BR />
	<a href="../management/users.cfm">Users Browser</a> <!--- Use list to manage users add, active, drop, change, retrieve, billing select--->
	<a href="../account/home">NOC Tools</a>
	<a id="ProcessCallQueueCalls">Publish Queued Calls To Dialers</a>         
    <a id="ExtractCallResults">Run Extractors</a>                        
			
	<br />
			
    <label>Testing Tools</label>
	<BR />
	<a href="../management/users.cfm">Publish Audio</a>

            	   
	<div id="loadingDlgManagment" style="display:inline;">
		<img class="LoadingDlgImg" src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
	</div>
    
      <label>Object Sections</label>
                <BR />
             
                <a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/lists/lists.cfm">Phone Lists</a>
                <a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/account/recording.cfm">Scripts</a>
                <a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#</cfoutput>/management/users.cfm">Users</a>    
                <a id="RegisterNewAccountButton">Register New User</a>
                <BR />
                <BR />
              
                <label>Distribution</label>
                <BR />
                <a id="AddSocialmediaToCallQueueButton">Add a Social media Group to the Call Queue</a>
                <a id="addgroupToCallQueueButton">Add a Group to the Call Queue</a>
                <a id="AddSingleToCallQueueButton">Add a Single Phone Number to the Call Queue</a>
                <a id="ScheduleTestingButton">Scheduling</a>
                <a id="ScheduleTestingButton">Add to Master DNC - UID 50</a>
        
                <br />
             	
	<label id="AddStatus" style="color:##933"></label>   

</div>