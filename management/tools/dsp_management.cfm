

<script TYPE="text/javascript">
	
$(function() {


	$("#ProcessCallQueueCalls").click( function() { ProcessCallQueueDialogCall(); return false;  }); 
	
	$("#ExtractCallResults").click( function() { ProcessExtractCallResults(); return false;  }); 


	$("#RegisterNewAccountButton").click( function() { RegisterNewAccountDialogCall(); return false;  }); 	
	$("#ScheduleTestingButton").click( function() { ScheduleTestingDialogCall(); return false;  }); 	
	
	
	$("#ImportOldrxds").click( function() { ImportOLDDS(); return false;  }); 	

	$("#loadingDlgManagment").hide();
	
	$("#LinkUserToCompany").click( function() { LinkUserToCompany(); return false;  });
	$("#EditDefaultCollaColumnList").click( function() { UpdateCollaDefaultListDialogCall(); return false;  });
	$("#switchPermissionManaged").click( function() { SwitchPermissionManagedDialogCall(); return false;  });
	$("#listuser").click( function() { listUserCompany(); return false;  });
	
		$("#taskList").click( function() { taskListTool(); return false;  });
	
	
	
});

var LinkUserToCompanyDialog = 0;
function LinkUserToCompany(){
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(LinkUserToCompanyDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			LinkUserToCompanyDialog.remove();
			LinkUserToCompanyDialog = 0;
			
		}
						
		LinkUserToCompanyDialog = $('<div></div>').append($loading.clone());

		LinkUserToCompanyDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_LinkUserToCompany.cfm')
			.dialog({
				modal : true,
				title: 'Link an User to Company Account',
				close: function() {
					 LinkUserToCompanyDialog.remove(); LinkUserToCompanyDialog = 0;
					 var grid = $("#UserList");
					 grid.trigger("reloadGrid");					  
				},
				width: 400,
				position:'top',
				height: 'auto'
			});
	
		LinkUserToCompanyDialog.dialog('open');
	
		return false;	
}

var listUserCompanyDialog = 0;
function listUserCompany(){
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(listUserCompanyDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			listUserCompanyDialog.remove();
			listUserCompanyDialog = 0;
			
		}
						
		listUserCompanyDialog = $('<div></div>').append($loading.clone());

		listUserCompanyDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/users/dsp_listUserFromCompany.cfm')
			.dialog({
				modal : true,
				title: 'List user from company',
				close: function() {
					 listUserCompanyDialog.remove(); 
					 listUserCompanyDialog = 0;
					 var grid = $("#UserList");
					 grid.trigger("reloadGrid");					  
				},
				width: 400,
				position:'top',
				height: 'auto'
			});
	
		listUserCompanyDialog.dialog('open');
	
		return false;	
}


var ImportOLDDSDialog = 0;
function ImportOLDDS()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ImportOLDDSDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ImportOLDDSDialog.remove();
		ImportOLDDSDialog = 0;
		
	}
					
	ImportOLDDSDialog = $('<div></div>').append($loading.clone());
	
	ImportOLDDSDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/tools/dsp_SelectimportOldDS.cfm?VerboseDebug=1')
		.dialog({
			modal : true,
			title: 'Import OLD RXDynascript',
			close: function() { ImportOLDDSDialog.remove(); ImportOLDDSDialog = 0; },
			width: 800,
			position:'top',
			height: 'auto'
		});

	ImportOLDDSDialog.dialog('open');

	return false;		
}	




var ProcessCallQueueDialog = 0;

function ProcessCallQueueDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ProcessCallQueueDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ProcessCallQueueDialog.remove();
		ProcessCallQueueDialog = 0;
		
	}
					
	ProcessCallQueueDialog = $('<div></div>').append($loading.clone());
	
	ProcessCallQueueDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/processing/act_Processcontactqueue.cfm?VerboseDebug=1')
		.dialog({
			modal : true,
			title: 'Publish Queued Calls To Dialers',
			close: function() { ProcessCallQueueDialog.remove(); ProcessCallQueueDialog = 0; },
			width: 600,
			position:'top',
			height: 'auto'
		});

	ProcessCallQueueDialog.dialog('open');

	return false;		
}	


var ProcessExtractCallResultsDialog = 0;
function ProcessExtractCallResults()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ProcessExtractCallResultsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ProcessExtractCallResultsDialog.remove();
		ProcessExtractCallResultsDialog = 0;
		
	}
					
	ProcessExtractCallResultsDialog = $('<div></div>').append($loading.clone());
	
	ProcessExtractCallResultsDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/processing/act_ProcessExtractionRXDialers.cfm?VerboseDebug=1')
		.dialog({
			modal : true,
			title: 'Extract Call Results from RXDialers',
			close: function() { ProcessExtractCallResultsDialog.remove(); ProcessExtractCallResultsDialog = 0; },
			width: 600,
			position:'top',
			height: 'auto'
		});

	ProcessExtractCallResultsDialog.dialog('open');

	return false;		
}	




var RegisterNewAccountDialog = 0;

function RegisterNewAccountDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(RegisterNewAccountDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		RegisterNewAccountDialog.remove();
		RegisterNewAccountDialog = 0;
		
	}
					
	RegisterNewAccountDialog = $('<div></div>').append($loading.clone());
	RegisterNewAccountDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management/</cfoutput>/account/dsp_RegisterNewUser.cfm')
		.dialog({
			modal : true,
			title: 'Register a new user account.',
			close: function() { RegisterNewAccountDialog.remove(); RegisterNewAccountDialog = 0; },
			width: 500,
			position:'top',
			height: 'auto',
			buttons:[
				{
					text:'Add',
					click:function(){
						RegisterNewAccount();
					}
				},
				{
					text: 'Cancel',
					click:function(){
						RegisterNewAccountDialog.remove(); 
						RegisterNewAccountDialog = 0;
					}
				}
			]			
		});

	RegisterNewAccountDialog.dialog('open');

	return false;		
}


var ScheduleTestingDialog = 0;

function ScheduleTestingDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(ScheduleTestingDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		ScheduleTestingDialog.remove();
		ScheduleTestingDialog = 0;
		
	}
					
	ScheduleTestingDialog = $('<div></div>').append($loading.clone());
	
	ScheduleTestingDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/schedule/dsp_schedule.cfm')
		.dialog({
			modal : true,
			close: function() { ScheduleTestingDialog.remove(); ScheduleTestingDialog = 0; },
			title: 'Schedule Options',
			width: 350,
			position:'top',
			height: 'auto'
		});

	ScheduleTestingDialog.dialog('open');

	return false;		
}	

	var UpdateCollaDefaultListDialog =0;
	function UpdateCollaDefaultListDialogCall(){
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(UpdateCollaDefaultListDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateCollaDefaultListDialog.remove();
			UpdateCollaDefaultListDialog = 0;
		}
		UpdateCollaDefaultListDialog = $('<div></div>').append($loading.clone());
		UpdateCollaDefaultListDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/tools/dsp_CollaborationCheckList.cfm')
			.dialog({
				modal : true,
				title: 'Select table to define default field list of Collaboration tool',
				close: function() {
					 UpdateCollaDefaultListDialog.remove(); UpdateCollaDefaultListDialog = 0;
					 var grid = $("#TableList");
					 grid.trigger("reloadGrid");
				},
				width: 751,
				position:'top',
				height: 463
			});
	
		UpdateCollaDefaultListDialog.dialog('open');
	}

	var SwitchPermissionManagedDialog =0;
	function SwitchPermissionManagedDialogCall(){
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');

		<!--- Erase any existing dialog data --->
		if(SwitchPermissionManagedDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			SwitchPermissionManagedDialog.remove();
			SwitchPermissionManagedDialog = 0;
		}
		SwitchPermissionManagedDialog = $('<div></div>').append($loading.clone());
		SwitchPermissionManagedDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/tools/dsp_SwitchPermissionManaged.cfm')
			.dialog({
				modal : true,
				title: 'On/Off managed option of permission',
				close: function() {
					 SwitchPermissionManagedDialog.remove(); SwitchPermissionManagedDialog = 0;
				},
				width: 751,
				position:'top',
				height: 463,
				buttons:[
					{
						text:'Update',
						click:function(){
							switchOnOff();
						}
					},
					{
						text: 'Cancel',
						click:function(){
							SwitchPermissionManagedDialog.remove(); 
							SwitchPermissionManagedDialog = 0;
						}
					}
				]			
				
			});
	
		SwitchPermissionManagedDialog.dialog('open');
	}
</script>

<div>
	
	<label>Management Tools</label>
	<BR />
	<a href="../account/home">NOC Tools</a>
	<a id="ProcessCallQueueCalls">Publish Queued Calls To Dialers</a>         
    <a id="ExtractCallResults">Run Extractors</a>  
    <a id="ImportOldrxds">Import Old RXDynascript Library</a>
	<cfif Session.UserRole EQ "SuperUser">
	<a id="LinkUserToCompany">Link user to company account</a>
	</cfif>
	<cfif Session.UserRole EQ "SuperUser">
	<a id="EditDefaultCollaColumnList">Define Default column list of Collaboration</a>
	</cfif>	
	<a id="listuser">List user to company</a>
	<br />
			
			
	<cfif Session.UserRole EQ "SuperUser">
		<a id="switchPermissionManaged">On/Off managed option of permission</a>
	</cfif>	
    <label>Testing Tools</label>
	<BR />
	<a href="../management/users.cfm">Publish Audio</a>

            	   
	<div id="loadingDlgManagment" style="display:inline;">
		<img class="LoadingDlgImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
	</div>
    
      <label>Object Sections</label>
                <BR />
                               <!--- <a id="RegisterNewAccountButton">Register New User</a> --->
                <BR />
                <BR />
              
                <label>Distribution</label>
                <BR />
                <a id="ScheduleTestingButton">Add to Master DNC - UID 50</a>
        
                <br />
             	
	<label id="AddStatus" style="color:##933"></label>   

</div>