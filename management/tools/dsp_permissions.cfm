
<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
}
#LevelList tr td
{
	text-align: center;
}


</style>
<script>

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
var grid = $("#PermissionList1");
	
$(function() {
	
		$('#dockPermission').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_Permission',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);	
<!---LoadSummaryInfo();--->
	
	jQuery("#PermissionList1").jqGrid({
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=getPermissionListData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 385,
		colNames:['<font size="2"><b>Id</b></font>','<font size="2"><b>Permission Section</b></font>','<font size="2"><b>Description</b></font>','<font size="2"><b>Options</b></font>'],
		colModel:[
			{name:'PermissionId_int',index:'PermissionId_int', width:50, editable:false},
			{name:'PermissionSection_vch',index:'PermissionSection_vch', width:300, editable:false},
			{name:'description',index:'description', width:200, editable:true},
			{name:'DisplayOptions',index:'DisplayOptions', width:55, editable:false,align:"center"}
		],
		rowNum:10,
	   	//rowList:[20,4,8,10,30],
		mtype: "POST",
		//pager: jQuery('#pagerPermissionList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'PermissionId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",
		multiselect: false,
		ondblClickRow: function(PermissionId_int){
			grid.setGridParam({editurl:'<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=updatePermission&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&id='+PermissionId_int});
			if(PermissionId_int==0){
				$('#PermissionList1').jqGrid('setRowData',PermissionId_int,{PermissionId_int:'',PermissionSection_vch:'',description:''});
			}
			$('#PermissionList1').jqGrid('editRow',PermissionId_int,true, '', '', '', '', afterEditPermissionContent,'', '');
			 
		},
		onSelectRow: function(PermissionId_int){
 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList1').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				}
			},
		afterInsertRow:function(PermissionId_int){
 				if(PermissionId_int && PermissionId_int!==lastsel){
					jQuery('#PermissionList1').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=PermissionId_int;
				}			
		},
		 loadComplete: function(data){
 				$(".del_Permission").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_Permission").click( function() { DeletePermission($(this).attr('rel')); } );
   			},			
			
		editurl: '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=updatePermission&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	//jQuery("#PermissionList1").jqGrid('navGrid','#pagerPermissionList');
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerPermissionList',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_LevelList").height(65);

	$("#t_PermissionList1").html(<cfinclude template="dsp_EditPermissionToolbar.cfm">);	


	$("#loadingPermission").hide();
		
});

function gridReload(){
	 
	 grid.trigger("reloadGrid");
}

function addNewPermission(){
	grid.setGridParam({editurl:'<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=updatePermission&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpAction=insert'});
		var DisplayOptions = "";
		//"<img class='edit_Row ListIconLinks Magnify-Purple-icon' rel='#permissionId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>";
		DisplayOptions = DisplayOptions + "<img class='del_Row ListIconLinks delete_16x16' rel='#permissionId#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>"       	;
		var myfirstrow = {PermissionId_int:0,PermissionSection_vch:"Edit than press Enter to save", description:"Edit than press Enter to save", DisplayOptions:DisplayOptions};
//		var top_rowid = $('#PermissionList1 #tbody:first-child #tr:first').attr('id');
		//jQuery("#PermissionList1").sortGrid("PermissionId_int",false);
	//	jQuery("#PermissionList1").setGridParam({sortname:'PermissionId_int'}).trigger('reloadGrid')
	//	var lastRowId=$("#PermissionList1").getDataIDs()[$("#PermissionList1").getDataIDs().length-1];
	//	alert(lastRowId);
		jQuery("#PermissionList1").addRowData("0", myfirstrow,'first');
		$('#PermissionList1').jqGrid('setRowData',0,{PermissionId_int:'',PermissionSection_vch:'',description:''});
		grid.jqGrid('getColProp','PermissionSection_vch').editable = true;
			$('#PermissionList1').jqGrid('editRow',0,true, '', '', '', '', afterEditPermissionContent,'', '');
}
function afterEditPermissionContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchListMCContent").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESC[0]) != "undefined" && d.DATA.INPDESC[0]!="")								
				{
						$("#PermissionList1").jqGrid('setRowData',rowid,{description:d.DATA.INPDESC[0]});
				}
				if(typeof(d.DATA.INPSECTION[0]) != "undefined" && d.DATA.INPSECTION[0] != "")								
				{
						$("#PermissionList1").jqGrid('setRowData',rowid,{PermissionSection_vch:d.DATA.INPSECTION[0]});
						$("#PermissionList1").jqGrid('setRowData',rowid,{PermissionId_int:d.DATA.INPPERMID[0]});
				}				
			}else if(CurrRXResultCode == -3){
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Permission Section Name already exist\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
			}else{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				$('#PermissionList1').jqGrid('delRowData',0);
				jAlert("Permission has not been updated\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) {} );
			}
		}
		else
		{<!--- Invalid structure returned --->	
			//alert('error1');
		}
	}
	else
	{<!--- No result returned --->
		//alert('error2');
	}  
	//gridReload();
	grid.jqGrid('getColProp','PermissionSection_vch').editable = false;
};

function DeletePermission(permissionId)
{	
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "Are you sure?", "About to delete permission from your list.", 
	function(result) { 
		if(!result){
			return;
		}else{	
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Session</cfoutput>/cfc/permission.cfc?method=deletePermission&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpPermissionId : permissionId},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Deleted", "Success!", function(result) 
																			{ 
																			} );								
							}
							$("#loadingPermission").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPermission").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					gridReload();
			}
					
		}); 

	 }  } );<!--- Close alert here --->


	return false; 
}

</script>





<BR />

<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->

<BR />

<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-CompanyList" >   
      
        <div style="table-layout:fixed; min-height:500px; height:500px;">
                
            <div id="pagerPermissionList"></div>
                           
            <div style="text-align:left;">
            <table id="PermissionList1" align="center"></table>
            </div>
        
        </div>

	</div>    
   
</div>
</cfoutput> 	
                          

 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockPermission" style="text-align:left;">
            <div class="dock-container_Permission">
                <a class="dock-item BBMainMenu" href="##" onclick="addNewPermission(); return false;"><span>Add New Permission Section</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Add new Permission Section" /></a> 
            </div>
           
        </div>