
<script>
		
var selectedROIId;

		
$(function() {


	// Dock initialize
	$('#dockLCEROI').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_LCEROI',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
<!---LoadSummaryInfo();--->
	<!--- jQuery("#lCEROIList").jqGrid({
	url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEROI.cfc?method=GetLCEROIData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['Company Id','Company Name','Options'],
		colModel:[
			{name:'CompanyAccountId_int',index:'CompanyAccountId_int', width:100, editable:false},
			{name:'CompanyName_vch',index:'CompanyName_vch', width:500, editable:true},			
			{name:'DisplayOptions',index:'DisplayOptions', width:70, editable:false}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		//pager: jQuery('#pagerCompanyAccountList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'CompanyAccountId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(CompanyAccountId_int){
				
			},
		 loadComplete: function(data){ 	
		 		 				 		  
					 		
   			},			
		
			
	//	editurl: '<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/simplelists.cfc?method=updatephonedata&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	}); --->
	

jQuery("#lCEROIList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEROI.cfc?method=GetLCEROIData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['Id', 'Company Id', 'Description', 'Number of Customers', 'Percent', 'Average Customer Profit', 'Total Profit', 'Options'],
		colModel:[
			{name:'LCEROIID_int', index:'LCEROIID_int', width: 100, editable: false},
			{name:'CompanyId_int', index: 'CompanyId_int', width: 150, editable: false},			
			{name:'Desc_vch', index: 'Desc_vch', width: 250, editable: false},			
			{name:'Customers_int', index: 'Customers_int', width: 150, editable: false},
			{name:'Percent_int', index: 'Percent_int', width: 150, editable: false},
			{name:'AveCustomerProfits_int', index: 'AveCustomerProfits_int', width: 250, editable: false},
			{name:'TotalProfits_int', index: 'TotalProfits_int', width: 150, editable: false},
			{name:'DisplayOptions', index: 'DisplayOptions', width: 70, editable: false}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'LCEROIId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(LCEROIID_int){
		},
		loadComplete: function(data){ 	
 			$(".del_LCEROI").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$(".del_LCEROI").click( function() { DeleteLCEROIDialog($(this).attr('rel')); } );
			$(".edit_LCEROI").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	$(".edit_LCEROI").click( function() { ShowEditLCEROIDialog(true, $(this).attr('rel') ); } );	
		},			
			
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		jsonReader: {
			root: "ROWS", //our data
			page: "PAGE", //current page
			total: "TOTAL", //total pages
			records:"RECORDS", //total records
			cell: "", //not used
			id: "0" //will default first column as ID
	  	}	
	});

	$("#loading").hide();
	
		
});		

	var editLCEROIDialog = 0;
	function ShowEditLCEROIDialog(isEditLCEROI, lCEROIID){

				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(editLCEROIDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			editLCEROIDialog.remove();
			editLCEROIDialog = 0;
			
		}
		var paramSTR = isEditLCEROI ? '?LCEROIID=' + lCEROIID : '';
		var dialogTitle = isEditLCEROI ? 'Edit LCE ROI' : 'Add new LCE ROI';
		editLCEROIDialog = $('<div></div>');

		editLCEROIDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_EditLCEROI.cfm' + paramSTR)
			.dialog({
				modal : true,
				title: dialogTitle,
				close: function() {
					 editLCEROIDialog.remove(); 
					 editLCEROIDialog = 0; 
					 var grid = $("#lCEROIList");
					 grid.trigger("reloadGrid");
				},
				width: 500,
				position:'top',
				height: 400,
			});
	
		editLCEROIDialog.dialog('open');
		return false;
	}

	
	function DeleteLCEROIDialog(lCEROIID)
	{	
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "Are you sure you want to delete selected LCE ROI?", "Confirmation", 
			function(result) { 
				$.getJSON('<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEROI.cfc?method=DeleteLCEROI&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { lCEROIID_int : lCEROIID }, 
				function(d) {
					<!--- Check if variable is part of JSON result string --->								
					if(d.DATA.RESULT == 'true')
					{
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("LCE ROI deleted", "Success", 
							function(result) 
									{});	
					}
					else
					{
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Cannot delete LCE ROI.\n"  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
					}
					
					$("#loading").hide(); 
					refreshLCEROIGrid();
				});	
			});
	
	
		return false; 
	}
	
	
	//auto refresh grid
	function refreshLCEROIGrid()
	{
	   var grid = $("#lCEROIList");
	   grid.trigger("reloadGrid");
	}

	$(document).ready(function() {
		refreshLCEROIGrid();
	});	

	</script>
<cfoutput>
       
	  
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-LCEROI" >   
      
        <div style="table-layout:fixed;">
            <div id="pagerLCEROIList"></div>
            <div style="text-align:left;">
            <table id="lCEROIList"></table>
            </div>
        </div>
        
        
    </div>
        
     
	
</cfoutput> 	
	<div style="position:absolute; bottom:30px;">                           
        <div id="dockLCEROI" style="text-align:left;">
            <div class="dock-container_LCEROI">
                <a class="dock-item BBMainMenu" href="##" onclick="ShowEditLCEROIDialog(false); return false;"><span>Add New LCE ROI</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Add new LCE ROI" /></a> 
            </div>
        </div>
