
<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
}
#LevelList tr td
{
	text-align: center;
}


</style>
<script>

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
var grid = $("#RoleList");
	
$(function() {
	
		$('#dockRoleList').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_LCEList',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);	
<!---LoadSummaryInfo();--->
	
	jQuery("#RoleList").jqGrid({
		url:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=getRolesData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 385,
		colNames:['<font size="2"><b>Role ID</b></font>',
				'<font size="2"><b>Role Name</b></font>',
				'<font size="2"><b>Description</b></font>',
				'<font size="2"><b>Options</b></font>'],
		colModel:[
			{name:'roleId_int',index:'roleId_int', width:50, editable:false},
			{name:'RoleName_vch',index:'RoleName_vch', width:300, editable:false},
			{name:'description_vch',index:'description_vch', width:200, editable:true},
			{name:'DisplayOptions',index:'DisplayOptions', width:55, editable:false, align:"center"}
		],
		rowNum:10,
	   	//rowList:[20,4,8,10,30],
		mtype: "POST",
		//pager: jQuery('#pagerPermissionList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'roleId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",
		multiselect: false,
		ondblClickRow: function(roleId_int){
			grid.setGridParam({editurl:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=updateRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&roleId_int='+roleId_int});
			if(roleId_int==0){
				$('#RoleList').jqGrid('setRowData',roleId_int,{roleId_int:'',RoleName_vch:'',description_vch:''});
			}
			$('#RoleList').jqGrid('editRow',roleId_int,true, '', '', '', '', afterEditContent,'', '');
			 lastsel=roleId_int;
		},
		onSelectRow: function(roleId_int){
 				if(roleId_int && roleId_int!==lastsel){
					jQuery('#RoleList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=roleId_int;
				}
			},
		afterInsertRow:function(roleId_int){
 				if(roleId_int && roleId_int!==lastsel){
					jQuery('#RoleList').jqGrid('restoreRow',lastsel);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=roleId_int;
				}
		},
		 loadComplete: function(data){
 				$(".del_Role").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_Role").click( function() { deleteRole($(this).attr('rel')); } );
   			},			
			
		editurl: '<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=updateRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->
		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	//jQuery("#PermissionList1").jqGrid('navGrid','#pagerPermissionList');
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerPermissionList',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_LevelList").height(65);

	$("#t_RoleList").html(<cfinclude template="dsp_EditPermissionToolbar.cfm">);	

	

	$("#upd_Bal").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#upd_Bal").click(function() { CreateUpdateBalanceDialogCall(); });	

	$("#upd_Rate").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#upd_Rate").click(function() { CreateUpdateUserRate1DialogCall(); });

	$("#add_company_acc").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#add_company_acc").click(function() { AddNewCompanyAccountDialogCall(); });		

	$("#loadingPermission").hide();
		
});

function gridReload(){
	 
	 grid.trigger("reloadGrid");
}

function addNewRole(){
	grid.setGridParam({editurl:'<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=updateRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpAction=insert'});
		var DisplayOptions = "";
		//"<img class='edit_Row ListIconLinks Magnify-Purple-icon' rel='#roleId#'  src='../../public/images/icons16x16/Magnify-Purple-icon.png' width='16' height='16'>";
		DisplayOptions = DisplayOptions + "<img class='del_Row ListIconLinks delete_16x16' rel='#roleId#'  src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'>";
		var myfirstrow = {RoleId_int:0,RoleName_vch:"Edit than press Enter to save", description_vch:"Edit than press Enter to save", DisplayOptions:DisplayOptions};
//		var top_rowid = $('#PermissionList1 #tbody:first-child #tr:first').attr('id');
		//jQuery("#PermissionList1").sortGrid("PermissionId_int",false);
	//	jQuery("#PermissionList1").setGridParam({sortname:'PermissionId_int'}).trigger('reloadGrid')
	//	var lastRowId=$("#PermissionList1").getDataIDs()[$("#PermissionList1").getDataIDs().length-1];
	//	alert(lastRowId);
		jQuery("#RoleList").addRowData("0", myfirstrow,'first');
		$('#RoleList').jqGrid('setRowData',0,{RoleId_int:'',RoleName_vch:'',description_vch:''});
		
		grid.jqGrid('getColProp','RoleName_vch').editable = true;
			$('#RoleList').jqGrid('editRow',0,true, '', '', '', '', afterEditContent,'', '');
}
function afterEditContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchListMCContent").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESCRIPTION[0]) != "undefined" && d.DATA.INPDESCRIPTION[0]!="")								
				{
						$("#RoleList").jqGrid('setRowData',rowid,{description_vch:d.DATA.INPDESCRIPTION[0]});
				}
				if(typeof(d.DATA.INPROLENAME[0]) != "undefined" && d.DATA.INPROLENAME[0]!="")								
				{
						$("#RoleList").jqGrid('setRowData',rowid,{RoleName_vch:d.DATA.INPROLENAME[0]});
						$("#RoleList").jqGrid('setRowData',rowid,{roleId_int:d.DATA.INPROLEID[0]});
				}
			}else if(CurrRXResultCode == -3){
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Role Name already exist\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
			}else{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				$('#RoleList').jqGrid('delRowData',0);
				jAlert("Role has not been updated\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) {} );
			}
		}
		else
		{<!--- Invalid structure returned --->	
			//alert('error1');
		}
	}
	else
	{<!--- No result returned --->
		//alert('error2');
	}  
	//gridReload();
	grid.jqGrid('getColProp','roleName_vch').editable = false;
};

function deleteRole(roleId)
{	
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "Are you sure?", "About to delete role from your list.", 
	function(result) { 
		if(!result){
			return;
		}else{	
		  $.ajax({
		    type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#LocalServerDirPath#Management</cfoutput>/cfc/management.cfc?method=deleteRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpRoleId : roleId},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Deleted", "Success!", function(result) 
																			{ 
																				
																			} );								
							}
																										
					
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					gridReload();
			} 		
					
		}); 

	 }  } );<!--- Close alert here --->


	return false; 
}

</script>





<BR />

<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->

<BR />

<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-CompanyList" >   
      
        <div style="table-layout:fixed; min-height:500px; height:500px;">
                
            <div id="pagerRoleList"></div>
                           
            <div style="text-align:left;">
            <table id="RoleList" align="center"></table>
            </div>
        
        </div>
			    <div style="position:absolute; bottom:80px;">                           

        <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
		
        <div id="dockRoleList" style="text-align:left;">
            <div class="dock-container_LCEList">
                <a class="dock-item" href="##" onclick="addNewRole(); return false;"><span>Add new</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Get Selected Users Balance" /></a> 
               
                <!--- Add a blank here to force menu to expand right - single item seems to expand left if not a second menu item--->
                <a class="dock-item BBMainMenu" href="##" onclick="return false;"><span></span><img src="../../public/images/dock/Spacer_Web_64x64.png" alt="" /></a>
             </div>
        </div>
        
	</div>
	</div>        

</cfoutput> 	



