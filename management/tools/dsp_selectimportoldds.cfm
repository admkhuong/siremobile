



<cfparam name="INPBATCHID" default="0" type="string">


<script type="text/javascript">


	$(function() {
		
		$("#CopyScriptLibraryButton").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } ); 
			
		$("#CopyScriptLibraryButton").click( function() { PublishScriptLibrary(); return false;  }); 
		
		$("#change").click(function() { SelectScriptDialog(1); });
		
	});

	var $DistributionDialog = 0;

	function PublishScriptLibrary()
	{			
		inpLibId= $("#inpLibId").val();
		inpOldLibId= $("#inpOldLibId").val();
		INPBATCHID= $("#INPBATCHID").val();
		inpDialerIPAddr= $("#inpDialerIPAddr").val();
		inpUserId= $("#inpUserId").val();
		
		
	
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof($("#inpLibId").val()) == 'undefined')
				inpLibId = 0;
			else
				inpLibId= $("#inpLibId").val();
				
		if(typeof(inpUserId) == 'undefined')
			if(typeof($("#inpUserId").val()) == 'undefined')
				inpUserId = 0;
			else
				inpUserId= $("#inpUserId").val();
				
		if(typeof(inpDialerIPAddr) == 'undefined')
			if(typeof($("#inpDialerIPAddr").val()) == 'undefined')
				inpDialerIPAddr = '0.0.0.0';
			else
				inpDialerIPAddr= $("#inpDialerIPAddr").val();
												
			var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
					

			$("#Linkgen").html('<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/tools/dsp_DoImportOldDS.cfm?inpLibId=' + inpLibId + '&inpUserId=' + inpUserId + '&INPBATCHID=' + INPBATCHID + '&inpOldLibId=' + inpOldLibId);		
			return false;						
									
			if($DistributionDialog != 0)
			{
				<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
				<!--- console.log($SelectScriptDialog); --->
				$DistributionDialog.remove();
				
			}
		
			$DistributionDialog = $('<div></div>').append($loading.clone());
			
			$DistributionDialog
				.load('<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/tools/dsp_DoImportOldDS.cfm?inpLibId=' + inpLibId + '&inpDialerIPAddr=' + inpDialerIPAddr + '&inpUserId=' + inpUserId + '&INPBATCHID=' + INPBATCHID + '&inpOldLibId=' + inpOldLibId)
				.dialog({
					modal : true,
					title: 'Publish',
					width: 800,
					height: 600
				});
	
			$DistributionDialog.dialog('open');
	
			return false;				
	}


	var $SelectScriptDialog = 0;
	
	function SelectScriptDialog(inpQID)
	{
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
												
						
		<!--- Either use input values from context menu or try to get from local form --->
		if(typeof(inpLibId) == 'undefined')
			if(typeof($("#inpLibId").val()) == 'undefined')
				inpLibId = 0;
			else
				inpLibId= + $("#inpLibId").val();
	
		<!--- These dont matter for Lib only selection --->
		inpEleId = 0;
		inpDataId = 0;
		
						
		if($SelectScriptDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			$SelectScriptDialog.remove();
			
		}
						
		$SelectScriptDialog = $('<div></div>').append($loading.clone());
		
		$SelectScriptDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/MCID/dsp_SelectScript.cfm?inpSelectLib=1&inpisDialog=1&inpLibId=' + inpLibId + '&inpEleId=' + inpEleId + '&inpDataId=' + inpDataId + '&inpQID=' + inpQID )
			.dialog({
				modal : true,
				title: 'Select Library',
				width: 800,
				height: 600
			});

		$SelectScriptDialog.dialog('open');

		return false;		
	}
	

</script>


<style>
/* ----------- stylized ----------- */
#PublishScriptFormDiv{

margin:0 5;
width:950px;
padding:5px;
height:800px;
}


#PublishScriptFormDiv p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #aaaaaa;
padding-bottom:10px;
}

#PublishScriptFormDiv label{
display:block;
font-weight:bold;
text-align:right;
width:400px;
float:left;
}

#PublishScriptFormDiv .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:400px;
}

#PublishScriptFormDiv input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
width:200px;
margin:2px 0 20px 10px;
display:block;
}



#PublishScriptFormDiv select{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
width:200px;
margin:2px 0 20px 10px;
display:block;
}

#PublishScriptFormDiv button{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aaaaaa;
margin:2px 0 20px 10px;
display:block;
}

</style>




<!--- Select Library --->

<!--- Select RXDialer --->


<!--- Go button --->
<cfoutput>

<div id="PublishScriptFormDiv">


<form name="PublishScriptForm" id="PublishScriptForm">
    
    <!--- Validate all numerics 0-9 --->
       
    <label>Target User ID
    <span class="small">User ID</span>
    </label>
    <input type="text" name="inpUserId" id="inpUserId" value="0" class="ui-corner-all"/> 
    
    <BR />
    <BR />
      
    <label>Target LibID
    <span class="small">Script Library ID</span>
    </label>
    <input type="text" name="inpLibId" id="inpLibId" value="0" class="ui-corner-all"/> 
    
    <ul id='rxdsmenu' class='rxdsmenu'> 		                                                  
	<li id='change' class='change ui-state-default ui-corner-all' style='display:inline;'><a style='display:inline;'>Change</a></li>                            
    </ul>
                           
    <label>RXOld Dynascript LibID
    <span class="small">Script Library ID</span>
    </label>
    <input type="text" name="inpOldLibId" id="inpOldLibId" value="0" class="ui-corner-all"/> 
                

    
 	<label>Copy to new Library Now
    <span class="small">Do Now</span>
    </label>
    
      
    
  <button id="CopyScriptLibraryButton" type="button" class="ui-state-default ui-corner-all">Copy Script Library</button>
  
  	<BR />
   	<BR />
	<BR />
   	<BR />
   	<BR />
    <BR />
    <BR />
   	<BR />
	<BR />
   	<BR />
   	<BR />
    <BR />
    <BR />
   	<BR />
	<BR />
   	<BR />
   	<BR />
    <BR />
                 
    <textarea id="Linkgen"></textarea>
	
</form>


  
</div> 




</cfoutput>









