<!--- Add a new Batch Form --->

<cfoutput>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
	
	<cfinclude template="../../public/paths.cfm" >
	<cfscript>
		 files = '\#ManagementPath#\defaultField.ini';
		 propUtil = createObject( 'component', '\#SessionPath#\cfc\PropertiesUtil' ).init( files );
	</cfscript>					
	<cfset isManaged=propUtil.getProperty("permission.managed")>
</cfoutput>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>


<script TYPE="text/javascript">

function switchOnOff(){
			$.getJSON( '<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/cfc/management.cfc?method=switchPermissionManaged&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
			{ 
				inpPermissionManaged : $('input[name=switch]:checked').val()
			},
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d)
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
<!--- 								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Permission Managed Switched " +$('input[name=switch]:checked').val(), "Success!", function(result) 
								{ 
									SwitchPermissionManagedDialog.remove();
								} ); --->
								$.jGrowl("Update sucessfully", { life:2000, position:"center", header:' Message'
									 });
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT beed added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
			} );		

		return false;
}
	$(function()
	{
		var isManaged= <cfoutput>"#isManaged#"</cfoutput>
		var $radios = $('input:radio[name=switch]');
		if(isManaged == "true" || isManaged== "on"){
			$radios.filter('[value=on]').attr('checked', true);
		}else{
			$radios.filter('[value=off]').attr('checked', true);
		}
	} );
</script

	

<cfoutput>

<div id='SwitchPermissionManagedDiv' class="RXForm">

<form id="SwitchPermissionManagedForm" name="SwitchPermissionManagedForm" action="" method="POST">
		<label>Permission Managed</label>
		        <br /> 
		<input type="radio" name="switch" value="on">On</input>
		<input type="radio" name="switch" value="off">Off</input>

</form>
</div>
<script type="text/javascript">

</script>
</cfoutput>
