
<script>
		
var selectedLCEWhitePapersId;

		
$(function() {


	// Dock initialize
	$('#dockLCEWhitePapersList').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_LCEWhitePapers',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
<!---LoadSummaryInfo();--->
	<!--- jQuery("#lCEWhitePapersList").jqGrid({
	url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEWhitePapers.cfc?method=GetLCEWhitePapersData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['Company Id','Company Name','Options'],
		colModel:[
			{name:'CompanyAccountId_int',index:'CompanyAccountId_int', width:100, editable:false},
			{name:'CompanyName_vch',index:'CompanyName_vch', width:500, editable:true},			
			{name:'DisplayOptions',index:'DisplayOptions', width:70, editable:false}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		//pager: jQuery('#pagerCompanyAccountList'),
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'CompanyAccountId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(CompanyAccountId_int){
				
			},
		 loadComplete: function(data){ 	
		 		 				 		  
					 		
   			},			
		
			
	//	editurl: '<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/simplelists.cfc?method=UpdatePhoneData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	}); --->
	

jQuery("#lCEWhitePapersList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/LCEWhitePapers.cfc?method=GetLCEWhitePapersData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:['Id', 'Content', 'Description', 'Options'],
		colModel:[
			{name:'LCEWhitePapersID_int', index:'LCEWhitePapersID_int', width:100, editable: false},
			{name:'Content', index: 'Content', width:250, editable: false},			
			{name:'Desc_vch', index: 'Desc_vch', width:250, editable: false},			
			{name:'DisplayOptions', index: 'DisplayOptions', width: 70, editable: false}
		],
		rowNum:20,
	   	rowList:[5,10,20,30],
		mtype: "POST",
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'LCEWhitePapersId_int',
		toolbar:[false,"both"],
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(LCEWhitePapersID_int){
		},
		loadComplete: function(data){ 	
 			$(".del_LCEWhitePapers").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$(".del_LCEWhitePapers").click( function() { DeleteLCEWhitePapersDialog($(this).attr('rel')); } );
			$(".edit_LCEWhitePapers").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	$(".edit_LCEWhitePapers").click( function() { ShowEditLCEWhitePapersDialog(true, $(this).attr('rel') ); } );	
		},			
			
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		jsonReader: {
			root: "ROWS", //our data
			page: "PAGE", //current page
			total: "TOTAL", //total pages
			records:"RECORDS", //total records
			cell: "", //not used
			id: "0" //will default first column as ID
	  	}	
	});

	$("#loading").hide();
	
		
});		

	var editLCEWhitePapersDialog = 0;
	function ShowEditLCEWhitePapersDialog(isEditLCEWhitePapers, lCEWhitePapersID){

				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(editLCEWhitePapersDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			editLCEWhitePapersDialog.remove();
			editLCEWhitePapersDialog = 0;
			
		}
		var paramSTR = isEditLCEWhitePapers ? '?LCEWhitePapersID=' + lCEWhitePapersID : '';
		var dialogTitle = isEditLCEWhitePapers ? 'Edit LCE White Papers' : 'Add new LCE White Papers';
		editLCEWhitePapersDialog = $('<div></div>');

		editLCEWhitePapersDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_AddLCEWhitePapers.cfm' + paramSTR)
			.dialog({
				modal : true,
				title: dialogTitle,
				close: function() {
					 editLCEWhitePapersDialog.remove(); 
					 editLCEWhitePapersDialog = 0; 
					 var grid = $("#lCEWhitePapersList");
					 grid.trigger("reloadGrid");
				},
				width: 500,
				position:'top',
				height: 200,
			});
	
		editLCEWhitePapersDialog.dialog('open');
		return false;
	}

	
	function DeleteLCEWhitePapersDialog(lCEWhitePapersID)
	{	
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( "Are you sure you want to delete selected LCE White Papers?", "Confirmation", 
			function(result) { 
				$.getJSON('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=DeleteLCEWhitePapers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { lCEWhitePapersID_int : lCEWhitePapersID }, 
				function(d) {
					<!--- Check if variable is part of JSON result string --->								
					if(d.DATA.RESULT == 'true')
					{
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("LCE White Papers deleted", "Success", 
							function(result) 
									{});	
					}
					else
					{
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Cannot delete LCE WhitePapers.\n"  + d.DATA.MESSAGE[0], "Failure!", function(result) { } );							
					}
					
					$("#loading").hide(); 
					refreshLCEWhitePapersGrid();
				});	
			});
	
	
		return false; 
	}
	
	
	//auto refresh grid
	function refreshLCEWhitePapersGrid()
	{
	   var grid = $("#lCEWhitePapersList");
	   grid.trigger("reloadGrid");
	}

	$(document).ready(function() {
		refreshLCEWhitePapersGrid();
	});	

	</script>
<cfoutput>
       
	  
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-LCEWhitePapers" >   
      
        <div style="table-layout:fixed;">
            <div id="pagerLCEWhitePapersList"></div>
            <div style="text-align:left;">
            <table id="lCEWhitePapersList"></table>
            </div>
        </div>
        
        
    </div>
        
     
	
</cfoutput> 	
	<div style="position:absolute; bottom:30px;">                           
        <div id="dockLCEWhitePapersList" style="text-align:left;">
            <div class="dock-container_LCEWhitePapers">
                <a class="dock-item BBMainMenu" href="##" onclick="ShowEditLCEWhitePapersDialog(false); return false;"><span>Add New LCE WhitePapers</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Add new LCE WhitePapers" /></a> 
            </div>
        </div>
