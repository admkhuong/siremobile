<script TYPE="text/javascript">


	function GetBalance()
	{					
		$("#loadingDlgGetBalance").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=GetBalanceAdmin&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#GetBalanceDiv #inpUserId").val(),inpCurrentUserId: "<cfoutput>#Session.USERID#</cfoutput>" }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								var Balance = 0;
								if (d.DATA.BALANCE[0]) {
									var Balance = formatCurrency(d.DATA.BALANCE[0]);
								}
								var Rate1 = 0;
								if (d.DATA.RATE1[0]) {
									var Rate1 = d.DATA.RATE1[0];
								}
								jAlert("User Balance is (" + Balance + ").\nUser Rate is (" + Rate1 + ").", "Success!", 
								function(result) 
									{ 
										$("#loadingDlgGetBalance").hide();
									} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgGetBalance").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{	
	
		$("#GetBalanceDiv #GetBalanceButton").click( function() { GetBalance(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#GetBalanceDiv #Cancel").click( function() 
			{
					$("#loadingDlgGetBalance").hide();					
					CreateGetBalanceDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#CurrUserId").html(lastsel);
		  
		  $("#GetBalanceForm #inpUserId").val(lastsel);
		  
		  $("#loadingDlgGetBalance").hide();	
	} );
		
		
</script>


<style>

#GetBalanceDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#GetBalanceDiv div{
display:inline;
border:none;
}

</style> 

<cfoutput>
        
<div id='GetBalanceDiv' class="RXForm">

<form id="GetBalanceForm" name="GetBalanceForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>User Id
        <span class="small" id="CurrUserId"></span>
        </label>
      
        <input TYPE="hidden" name="inpUserId" id="inpUserId" size="255" class="ui-corner-all" /> 
        
        <BR>
                
        <button id="GetBalanceButton" TYPE="button" class="ui-corner-all">Get Balance</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
        <div id="loadingDlgGetBalance" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</cfoutput>
</div>
























