<cfparam name="Session.DBSourceEBM" default="Bishop"/>
<script>
	function getUserList()
	{			
		var template="";
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=RetrieveUserIdListCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  
		{ 

			INPACTIVE : 1  
		}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.USERACCOUNTDATA[0]) != "undefined")
						{
							<!--- template += '<select TYPE="select" name="inpCompanyId" id="inpCompanyId" size="1" class="ui-corner-all" > '; --->
							template += '<ul>';
							for(i=0; i<d.DATA.USERACCOUNTDATA[0].length ; i++){
								var value=d.DATA.USERACCOUNTDATA[0][i][0];
								var text=d.DATA.USERACCOUNTDATA[0][i][1];
								<!--- template += '<option value="'+ value +'">'+ text + '</option>'; --->
								template += '<li>' + value+ ': ' + text + '</li>';
								
							};	
							template += '</ul>';	
							$("#CompanyListDiv").append(template);
							//var CompanyData = d.DATA.COMPANYDATA[0];	
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}	
		
</script>
<cfoutput>
	<div id="CompanyListDiv"/>
	
<script type="text/javascript">
	$(document).ready(function() {
		getUserList();
	});	
</script>
</cfoutput>