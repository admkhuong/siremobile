<script TYPE="text/javascript">


	function UpdateBalance()
	{					
		$("#loadingDlgUpdateBalance").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/billing.cfc?method=UpdateBalance&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#UpdateBalanceDiv #inpUserId").val(), inpAmountToAdd : $("#UpdateBalanceDiv #inpAmountToAdd").val(),inpCurrentUserId: "<cfoutput>#Session.USERID#</cfoutput>" }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Balance has beed updated. New Balance (" + d.DATA.BALANCE[0] + ").", "Success!", function(result) 
																			{ 
																				$("#loadingDlgUpdateBalance").hide();
																				
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Balance has NOT beed updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdateBalance").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{	
	
		$("#UpdateBalanceDiv #UpdateBalanceButton").click( function() { UpdateBalance(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#UpdateBalanceDiv #Cancel").click( function() 
			{
					$("#loadingDlgUpdateBalance").hide();					
					CreateUpdateBalanceDialog.remove(); 
					return false;
		  }); 	
		  
		  $("#CurrUserId").html(selectedUserId);
		  
		  $("#UpdateBalanceForm #inpUserId").val(selectedUserId);
		  
		  $("#loadingDlgUpdateBalance").hide();	
	} );
		
		
</script>


<style>

#UpdateBalanceDiv{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#UpdateBalanceDiv div{
display:inline;
border:none;
}

</style> 

<cfoutput>
        
<div id='UpdateBalanceDiv' class="RXForm">

<form id="UpdateBalanceForm" name="UpdateBalanceForm" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>User Id
        <span class="small" id="CurrUserId"></span>
        </label>
      
        <input TYPE="hidden" name="inpUserId" id="inpUserId" size="255" class="ui-corner-all" /> 
       
        <BR>
        
       	<label>Value to Add
        <span class="small">Can be negative</span>
        </label>
        <input TYPE="text" name="inpAmountToAdd" id="inpAmountToAdd" size="255" class="ui-corner-all" /> 
                
        <BR>
                
        <button id="UpdateBalanceButton" TYPE="button" class="ui-corner-all">Add</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
        <div id="loadingDlgUpdateBalance" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</cfoutput>
</div>
























