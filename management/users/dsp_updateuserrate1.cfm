<script TYPE="text/javascript">


	function UpdateUserRate1()
	{					
		$("#loadingDlgUpdateUserRate1").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/billing.cfc?method=UpdateRate1TenthPenny&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#UpdateUserRate1Div #inpUserId").val(), inpNewRate : $("#UpdateUserRate1Div #inpNewRate").val() }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Balance has beed updated.\nNew Balance is (" + formatCurrency(d.DATA.BALANCE[0]) + ").\nUser Rate is (" + d.DATA.RATE1[0] + ").", "Success!", function(result) 
																			{ 
																				$("#loadingDlgGetBalance").hide();
																				
																				
																			} );															
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Balance has NOT beed updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdateUserRate1").hide();
			} );		
	
		return false;

	}
	
	
	$(function()
	{	
	
		$("#UpdateUserRate1Div #UpdateUserRate1Button").click( function() { UpdateUserRate1(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#UpdateUserRate1Div #Cancel").click( function() 
			{								
				$("#loadingDlgUpdateUserRate1").hide();					
				CreateUpdateUserRate1Dialog.remove(); 
				return false;
		  }); 	
		  
		  $("#CurrUserId").html(selectedUserId);
		  
		  $("#UpdateUserRate1Form #inpUserId").val(selectedUserId);
		  
		  $("#loadingDlgUpdateUserRate1").hide();	
	} );
		
		
</script>


<style>

#UpdateUserRate1Div{
margin:0 5;
width:450px;
padding:5px;
border: none;
display:inline;
}


#UpdateUserRate1Div div{
display:inline;
border:none;
}

</style> 

<cfoutput>
        
<div id='UpdateUserRate1Div' class="RXForm">

<form id="UpdateUserRate1Form" name="UpdateUserRate1Form" action="" method="POST">

 <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="0" />

		<label>User Id
        <span class="small" id="CurrUserId"></span>
        </label>
      
        <input TYPE="hidden" name="inpUserId" id="inpUserId" size="255" class="ui-corner-all" /> 
       
        <BR>
        
       	<label>Value for new rate
        <span class="small">Max Resolution is to the Tenth of a penny $0.000</span>
        </label>
        <input TYPE="text" name="inpNewRate" id="inpNewRate" size="255" class="ui-corner-all" /> 
                
        <BR>
                
        <button id="UpdateUserRate1Button" TYPE="button" class="ui-corner-all">Add</button>
        <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
        <div id="loadingDlgUpdateUserRate1" style="display:inline;">
            <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
        </div>
</form>
</cfoutput>
</div>
























