<cfscript>
	 files = '\#SessionPath#\permissions.properties';
	 properties = createObject( 'component', '\#SessionPath#\CFC\PropertiesUtil' ).init( files );
	//writeOutput( 'key1 is ' & properties.getProperty( 'rxt1' ) );
</cfscript>
<cfset EditUserPerm="#properties.getProperty('permission.EditUser')#">
<cfset Session.roleData=structNew()/>
<cfset args=StructNew()>
<cfset args.inpUserId="#Session.USERID#">
<cfinvoke argumentcollection="#args#" method="getUserRole" component="#Session.SessionCFCPath#.permission" returnvariable="Session.roleData"/>

<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
}
#UserList tr td
{
	text-align: center;
}
#UserList .ui-state-highlight td:first-child{
	#background:url("../../public/images/Sign-Select-icon.png") no-repeat;
}

</style>
<script>
var currentEmail="";
var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
var selectedUserId;
var userIdForBilling;

$(function() {


	// Dock initialize
	$('#dockMCUserList').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_BabbleMCUsers',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);

<!---LoadSummaryInfo();--->
	
	
	jQuery("#UserList").jqGrid({
		url:'<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/users.cfc?method=GetUserDataWithCompanyName&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
		datatype: "json",
		height: 350,
		postData:{inpUserId:<cfoutput>#Session.USERID#</cfoutput>},
		colNames:[	'<font size="2"><b>User Id</b></font>',
					'<font size="2"><b>User Name</b></font>',
					'<font size="2"><b>Company Name</b></font>',
					'<font size="2"><b>Active</b></font>',
					'<font size="2"><b>Created Date</b></font>',
					'<font size="2"><b>User Level</b></font>',
					'<font size="2"><b>Role</b></font>'],
		colModel:[			
			{name:'UserId_int1',index:'UserId_int1', width:80, editable:false},
			{name:'UserName_vch',index:'UserName_vch', width:150, editable:true},
			{name:'CompanyName_vch',index:'CompanyName_vch', width:150, editable:true},
			{name:'Active_int',index:'Active_int', width:50, editable:true},
			{name:'Created_dt',index:'Created_dt', width:150, editable:true},
			{name:'UserLevel',index:'UserLevel', width:150, editable:true},
			{name:'RoleName_vch',index:'RoleName_vch', width:80, editable:true}
		],
		rowNum:15,
	   	rowList:[5,10,15,25],
		mtype: "POST",
		<!--- pager: jQuery('#pagerUserList'), --->
		toppager: true,
		pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'UserId_int',
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(UserId_int){ jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(UserId_int){
			selectedUserId=UserId_int;
				if(UserId_int && UserId_int!==lastsel){
					jQuery('#UserList').jqGrid('restoreRow',UserId_int);
					//jQuery('#UserList').jqGrid('editRow',UserId_int,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=UserId_int;
					
				}
			},
		loadComplete: function(){
			currentEmail = <cfoutput>"#Session.EmailAddress#"</cfoutput>
			if(currentEmail == "unknown@unknown.com"){
				//history.go(0);
			}
		},
			
		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default first column as ID
			  }	
	});
	
	 
//	 jQuery("#UserList").jqGrid('navGrid','#pagerUserList',{edit:false,edittext:"Edit Notes",add:false,addtext:"Add Phone",del:false,deltext:"Delete Phone",search:false, searchtext:"Add To Group",refresh:true},
//			{},
//			{},
//			{}	
//	 );
	 
	$("#t_UserList").height(30);

	$("#t_UserList").html(<cfinclude template="dsp_UserListToolbar.cfm">);
	


//	$("#add_company_acc").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
//	$("#add_company_acc").click(function() { AddNewCompanyAccountDialogCall(); });		

	
	
	
	$("#loading").hide();
		
});

	
function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));

	return (((sign)?'':'-') + '$' + num + '.' + cents);
}


var CreateGetBalanceDialog = 0;

function CreateGetBalanceDialogCall()
{
	
	if(typeof(lastsel) == "undefined")
	{
		jAlert("No user selected.", "Select a user from the list and try again.");
		return;		
	}
		
					
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(CreateGetBalanceDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateGetBalanceDialog.remove();
		CreateGetBalanceDialog = 0;
		
	}
					
	CreateGetBalanceDialog = $('<div></div>').append($loading.clone());
	
	CreateGetBalanceDialog
		.load('<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/users/dsp_GetUserBalance.cfm')
		.dialog({
			modal : true,
			title: 'Get User Billing Information',
			close: function() { CreateGetBalanceDialog.remove(); CreateGetBalanceDialog = 0; },
			width: 400,
			position:'top',
			height: 'auto'
		});

	CreateGetBalanceDialog.dialog('open');

	return false;		
}	



var CreateUpdateBalanceDialog = 0;

function CreateUpdateBalanceDialogCall()
{		
	if(typeof(selectedUserId) == "undefined")
	{
		jAlert("No user selected.", "Select a user from the list and try again.");
		return;		
	}
			
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	<!--- Erase any existing dialog data --->
	if(CreateUpdateBalanceDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateUpdateBalanceDialog.remove();
		CreateUpdateBalanceDialog = 0;
		
	}
					
	CreateUpdateBalanceDialog = $('<div></div>').append($loading.clone());
	
	CreateUpdateBalanceDialog
		.load('<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/users/dsp_UpdateUserBalance.cfm')
		.dialog({
			modal : true,
			title: 'Update User Balance',
			close: function() { CreateUpdateBalanceDialog.remove(); CreateUpdateBalanceDialog = 0; },
			width: 400,
			position:'top',
			height: 'auto'
		});

	CreateUpdateBalanceDialog.dialog('open');

	return false;		
}	


var CreateUpdateUserRate1Dialog = 0;

function CreateUpdateUserRate1DialogCall()
{	
		
	if(typeof(selectedUserId) == "undefined")
	{
		jAlert("No user selected.", "Select a user from the list and try again.");
		return;		
	}
			
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	<!--- Erase any existing dialog data --->
	if(CreateUpdateUserRate1Dialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateUpdateUserRate1Dialog.remove();
		CreateUpdateUserRate1Dialog = 0;
		
	}
					
	CreateUpdateUserRate1Dialog = $('<div></div>').append($loading.clone());
	
	CreateUpdateUserRate1Dialog
		.load('<cfoutput>#rootUrl#/#ManagementPath#</cfoutput>/users/dsp_UpdateUserRate1.cfm')
		.dialog({
			modal : true,
			title: 'Update User Rate',
			close: function() { CreateUpdateUserRate1Dialog.remove(); CreateUpdateUserRate1Dialog = 0; },
			width: 400,
			position:'top',
			height: 'auto'
		});

	CreateUpdateUserRate1Dialog.dialog('open');

	return false;		
}	
	var UpdateUserTaskListDialog =0;
	function UpdateUserTaskListDialogCall(){
			if(typeof(lastsel) == "undefined")
		{
			jAlert("No user selected.", "Select a user from the list and try again.");
			return;		
		}
				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(UpdateUserTaskListDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateUserTaskListDialog.remove();
			UpdateUserTaskListDialog = 0;
			
		}
						
		UpdateUserTaskListDialog = $('<div></div>').append($loading.clone());
		
		UpdateUserTaskListDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_TaskList.cfm')
			.dialog({
				modal : true,
				title: 'Task List Tool',
				close: function() {
					 UpdateUserTaskListDialog.remove(); UpdateUserTaskListDialog = 0;
					 var grid = $("#TaskList");
					 grid.trigger("reloadGrid");					  
				},
				width: 751,
				position:'top',
				height: 463
			});
	
		UpdateUserTaskListDialog.dialog('open');
	
		return false;	
	}




function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}
	

	

function doSearch(ev){
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReload,500)
}

function gridReload(){
	var UserId_mask = jQuery("#search_userid").val();
	var UserName_mask = jQuery("#search_username").val();
	var CurrGroup = jQuery("#inpGroupId").val(); 
	jQuery("#UserList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/users.cfc?method=GetSimpleUserData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&UserId_mask="+UserId_mask+"&UserName_mask="+UserName_mask+"&inpGroupId="+CurrGroup,page:1}).trigger("reloadGrid");
}

function enableAutosubmit(state){
	flAuto = state;
	gridReload();
	jQuery("#submitButton").attr("disabled",state);
}



function LoadSummaryInfo()
{
	$("#loading").show();		
		
		
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#MainCFCPath#</cfoutput>/simplelists.cfc?method=getsimpleliststatistics&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { },					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
				
				// alert(d);
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
								if(typeof(d.DATA.TOTALCOUNT[0]) != "undefined")								
									$("#TotalListCount").html(d.DATA.TOTALCOUNT[0])
								else
									$("#TotalListCount").html('Err')
							}
						
						
							for(x=0; x<d.ROWCOUNT; x++)
							{
							
								CurrRXResultCode = d.DATA.GROUPNAME[x];	
								
								if(d.DATA.GROUPNAME[x] != "")
								{																	
									if(typeof(d.DATA.TOTALCOUNT[0]) != "undefined")								
										$("#GroupSummary").append("Total " + d.DATA.GROUPNAME[x] + " Count " + d.DATA.GROUPCOUNT[x])										
								}
								
								<!--- Only show counts for top five groups --->
								if(x>5)
								{
									<!--- Indicate more to follow or put in a scroll bar --->
									break;
								}
							}
													
							$("#loading").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loading").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					
			} 		
					
		});


	return false;
}


	var UpdateUserAccountDialog = 0;
	function UpdateUserAccountDialogCall(){
	if(typeof(selectedUserId) == "undefined")
	{
		jAlert("No User account selected.", "Select an account from the list and try again.");
		return;		
	}
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		<!--- Erase any existing dialog data --->
		if(UpdateUserAccountDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateUserAccountDialog.remove();
			UpdateUserAccountDialog = 0;
			
		}	
		UpdateUserAccountDialog = $('<div></div>').append($loading.clone());
		UpdateUserAccountDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_UpdateUserAccount.cfm')
			.dialog({
				modal : true,
				title: 'Update User Account',
				close: function() {
					 UpdateUserAccountDialog.remove(); 
					 UpdateUserAccountDialog = 0; 
					 var grid = $("#UserList");
					 grid.trigger("reloadGrid");
				},
				width: 400,
				position:'top',
				height: 'auto',
				buttons:[
					{
						text:'Update',
						id:'UpdateUserAccountButton',
						disabled:true,
						click:function(){
							UpdateUserAccount();
						}
					},
					{
						text: 'Cancel',
						click:function(){
							UpdateUserAccountDialog.remove(); 
							UpdateUserAccountDialog = 0;
						}
					}
				]
			});
		UpdateUserAccountDialog.dialog('open');
		return false;
	}
	
	var UpdateUserRoleDialog = 0;
	function UpdateUserRoleDialogCall(){
<!---           <cfif NOT StructKeyExists(SESSION.accessRights,EditUserPerm) >
				jAlert("You don't have permission to edit User Account", "Failure");
				return;	
           </cfif> --->
<!--- 	if(typeof(lastsel) == "undefined")
	{
		jAlert("No User account selected.", "Select an account from the list and try again.");
		return;		
	} --->
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		<!--- Erase any existing dialog data --->
		if(UpdateUserRoleDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			UpdateUserRoleDialog.remove();
			UpdateUserRoleDialog = 0;
			
		}	
		UpdateUserRoleDialog = $('<div></div>').append($loading.clone());	
		UpdateUserRoleDialog
			.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management</cfoutput>/account/dsp_UpdateUserRole.cfm')
			.dialog({
				modal : true,
				title: 'Update User Role',
				close: function() {
					 UpdateUserRoleDialog.remove(); 
					 UpdateUserRoleDialog = 0; 
					 var grid = $("#UserList");
					 grid.trigger("reloadGrid");
				},
				width: 400,
				position:'top',
				height: 'auto',
				buttons:[
					{
						text:'Assign',
						click:function(){
							_UpdateUserRole();
						}
					},
					{
						text: 'Cancel',
						click:function(){
							UpdateUserRoleDialog.remove(); 
							UpdateUserRoleDialog = 0;
						}
					}
				]				
			});
	
		UpdateUserRoleDialog.dialog('open');
	
		return false;			
	}	
var RegisterNewAccountDialog = 0;

function RegisterNewAccountDialogCall()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(RegisterNewAccountDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		RegisterNewAccountDialog.remove();
		RegisterNewAccountDialog = 0;
		
	}
					
	RegisterNewAccountDialog = $('<div></div>').append($loading.clone());
	RegisterNewAccountDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management/</cfoutput>/account/dsp_RegisterNewUser.cfm')
		.dialog({
			modal : true,
			title: 'Register a new user account.',
			close: function() { RegisterNewAccountDialog.remove(); RegisterNewAccountDialog = 0; },
			width: 500,
			position:'top',
			height: 'auto',
			buttons:[
				{
					text:'Add',
					click:function(){
						RegisterNewAccount();
					}
				},
				{
					text: 'Cancel',
					click:function(){
						RegisterNewAccountDialog.remove(); 
						RegisterNewAccountDialog = 0;
					}
				}
			]				
		});

	RegisterNewAccountDialog.dialog('open');

	return false;		
}
var BillingTransactionDialog = 0;

function BillingTransactionDialogCall(userId)
{
	if(userId){
		userIdForBilling=userId;
		billingLogDialog='Personal billing transaction log';
	}else{
		userIdForBilling=jQuery("#UserList").getGridParam('selrow');
		if(!userIdForBilling){
			jAlert("No user was selected","Failed");
			return;
		}
		billingLogDialog='billing transaction log of '+userIdForBilling;
	}
	
	
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
						
	<!--- Erase any existing dialog data --->
	if(BillingTransactionDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		BillingTransactionDialog.remove();
		BillingTransactionDialog = 0;
	}
					
	BillingTransactionDialog = $('<div></div>').append($loading.clone());
	BillingTransactionDialog
		.load('<cfoutput>#rootUrl#/#LocalServerDirPath#management/</cfoutput>/account/dsp_BillingTransactionLog.cfm')
		.dialog({
			modal : true,
			title: billingLogDialog,
			close: function() { BillingTransactionDialog.remove(); BillingTransactionDialog = 0; },
			width: 890,
			position:'top',
			height: 'auto',
			buttons:[
				{
					text: 'Close',
					click:function(){
						BillingTransactionDialog.remove(); 
						BillingTransactionDialog = 0;
					}
				}
			]			
		});
	BillingTransactionDialog.dialog('open');

	return false;		
}
	

</script>






<!---<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>--->
<!---

<div>
	Total List count <span id='TotalListCount'>0</span>
</div>

<div id="GroupSummary">


</div>--->



<div id="pagerUserList"></div>
<table id="UserList"></table>


	   <div style="position:absolute; bottom:30px;">                           
 <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockMCUserList" style="text-align:left;">
            <div class="dock-container_BabbleMCUsers">
				<a class="dock-item BBMainMenu" href="##" onclick="RegisterNewAccountDialogCall(); return false;"><span>Add new User</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Register new user" /></a> 
				<a class="dock-item BBMainMenu" href="##" onclick="UpdateUserAccountDialogCall(); return false;"><span>Update User</span><img src="../../public/images/dock/EditGroup_Web_64x64.png" alt="Update User Account" /></a>

					<a disabled='true' class="dock-item BBMainMenu" href="##" onclick="UpdateUserRoleDialogCall(); return false;"><span>Update User's Role</span><img src="../../public/images/dock/Group_Web_64x64II.png" alt="Update User's Role" /></a>
				
                <a class="dock-item BBMainMenu" href="##" onclick="return false;"><span></span><img src="../../public/images/dock/Spacer_Web_64x64.png" alt="" /></a>				

                <a class="dock-item BBMainMenu" href="##" onclick="CreateGetBalanceDialogCall(); return false;"><span>Get Billing Info</span><img src="../../public/images/dock/BillingInfo64x64.png" alt="Get Selected Users Balance" /></a>
				
				<cfif isDefined("Session.UserRole") AND Session.UserRole EQ 'SuperUser'>
					<a class="dock-item BBMainMenu" href="##" onclick="BillingTransactionDialogCall(); return false;"><span>Get Billing Transaction Log</span><img src="../../public/images/dock/BillingInfo64x64.png" alt="Get Selected Users Balance" /></a> 
				<cfelse>
					<a class="dock-item BBMainMenu" href="##" onclick="BillingTransactionDialogCall('<cfoutput>#Session.USERID#</cfoutput>'); return false;"><span>Get Billing Transaction Log</span><img src="../../public/images/dock/BillingInfo64x64.png" alt="Get Selected Users Balance" /></a> 
				</cfif>

				<cfif isDefined("Session.UserRole") AND Session.UserRole EQ 'SuperUser'>
                	<a class="dock-item BBMainMenu" href="##" onclick="CreateUpdateBalanceDialogCall(); return false;"><span>Update Balance</span><img src="../../public/images/dock/updateBalance64x64.png" alt="Update Selected Users Balance" /></a> 
				<cfelse>
					<cfset alertMsg = "You don\'t have permission to edit user\'s balance">
					<a class="dock-item BBMainMenu" href="##" onclick="jAlert('<cfoutput>#alertMsg#</cfoutput>','Failed'); "><span>Update Balance</span><img src="../../public/images/dock/updateBalance64x64.png" alt="Update Selected Users Balance" /></a> 
				</cfif>
                
				<a class="dock-item BBMainMenu" href="##" onclick="CreateUpdateUserRate1DialogCall(); return false;"><span id="Show_TZ">Update Rate</span><img src="../../public/images/dock/SystemGears_Web_64x64II.png" alt="Update Selected Users Dial Rate" /></a> 



	            <!--- <a class="dock-item BBMainMenu" href="##" onclick="UpdateUserTaskListDialogCall(); return false;"><span>Update Task List</span><img src="../../public/images/dock/Ungroup_Web_64x64II.png" alt="Update User Task List" /></a> 
				<a class="dock-item BBMainMenu" href="##" onclick="UpdateCollaborationCheckListDialogCall(); return false;"><span>Update Task List</span><img src="../../public/images/dock/Ungroup_Web_64x64II.png" alt="Update User Task List" /></a>  --->
				
            </div>
           
        </div>
        
    </div>

