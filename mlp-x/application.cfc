<cfcomponent>
	<cfset this.name = "MLP-X-Application" />  <!--- RXMBEBMDemo ->  RXSireApplication --->
	
	<cfset this.sessionStorage  = "sessionstorage">
	<cfset this.sessionCluster = true>
	
	
	<cffunction name="onError"> 
	   <cfargument name="Exception" required=true/> 
	   <cfargument name="EventName" type="String" required=true/> 

	    <!--- Log all errors. ---> 
	    <cfset var message = ''>
	    <cfset var request_url = ''>
	    <cfset var messageDetail = ''>
	    <cfset var request_url = ''>
	    <cfset var http_referer = ''>
	    <cfset var http_user_agent = ''>

	    <cfif isStruct(Exception)>
	    	<cfset messageDetail = serializejson(Exception)>
	    </cfif>

	    <cfif structKeyExists(Exception, "message")>
	    	<cfset message = Exception.message>
	    </cfif>

	    <cfif structKeyExists(Exception, "Detail")>
	    	<cfset message &= ' '&Exception.Detail>
	    </cfif>
	    <!---
	    <cfif structKeyExists(Exception, "queryError")>
	    	<cfset message &= ' '&Exception.queryError>
	    </cfif>
	    --->
	    <cfif structKeyExists(cgi, "request_url")>
	    	<cfset request_url = cgi.request_url>
	    </cfif>

	    <cfif structKeyExists(cgi, "http_user_agent")>
	    	<cfset http_user_agent = cgi.http_user_agent>
	    </cfif>
	    
	    <cfif structKeyExists(cgi, "http_referer")>
	    	<cfset http_referer = cgi.http_referer>
	    </cfif>

	    <cftry>

	    	<cfquery name="addLog" datasource="Bishop">
	            INSERT INTO simpleobjects.sire_error_logs
	                (
	                    ErrorMessage_txt, 
	                    Created_dt, 
	                    UserIp_vch,
	                    RequestUrl_txt,
						UserId_int,
						ErrorDetail_txt,
						HttpUserAgent_txt,
						HttpReferer_txt
	                )
	            VALUES 
	            	(
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#message#">, 
						NOW(), 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">, 
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#request_url#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#messageDetail#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#http_user_agent#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#http_referer#">
					)                                        
	        </cfquery> 
		   
			<cfcatch>
			</cfcatch>
		</cftry>	
	    <!--- Display an error message if there is a page context. ---> 
<!--- 		<cflocation url= "/public/sire/pages/error-page" addToken = "false"/> --->
	    
	</cffunction>

</cfcomponent>