<!--- 
	
Note on .htaccess and the rules

https://stackoverflow.com/questions/16847027/mod-rewrite-rules-for-passing-parameters

Remember that RewriteRule only matches REUQEST_URI without a query string.

You just need QSA flag for this purpose:

RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^foo/?$ index.php?view=foo [L,QSA]
QSA - (Query String Append) is used for appending existing query string with the new ones.




RewriteEngine On
RewriteRule ^(index)/?$ index.cfm [L,QSA]
RewriteRule ^([A-Za-z0-9-]+)/?$ index.cfm?id=$1 [L,QSA]


	
	
	
 --->

<!--- Look for css and js files --->
<cfparam name="dirlink" default="lz/">
<!--- Optionally Pass in Hashed Contact string for Additional click tracking  --->
<cfparam name="h" default=""> 


<!--- Load Setting Defaults - Assume No Sessions here --->
<cfset DBSourceManagementRead = "bishop_read" />
<cfset DBSourceEBM = "bishop" />

<!--- id comes from the .htaccess redrect - anything after the slash  --->
<cfparam name="id" default="0">

<!--- Did we get a valid id and not the default 0? --->
<cfif id NEQ "0">

	<!--- Read target URL from DB - do strIght look up here as this will be a simple site with redirect -- Assume No Sessions here  --->	
    <cfquery name="LookupFullURL" datasource="#DBSourceManagementRead#">
        SELECT 
        	PKId_bi,
            TargetURL_vch,
            UniqueTargetId_int,
        	RedirectCode_int,
        	TargetURL_vch,
            DynamicData_vch,
            UserId_int
        FROM
            simplelists.url_shortner
        WHERE
            ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#id#">  
        AND
        	Active_int = 1	              
    </cfquery>       
             
    <cfif LookupFullURL.RECORDCOUNT GT 0>
	    	
	    <cfset NewTarget = TRIM(LookupFullURL.TargetURL_vch) />
	    
	    <!--- Repare poorly defined links to default to non-ssl http  --->
	    <cfif COMPARENOCASE(LEFT(NewTarget,4), "HTTP") NEQ 0>
		    
		    <cfset NewTarget = "http://" &  NewTarget />
		    
	    </cfif>    	    
	    
	    <!--- Append params if specified  --->
	    <cfif LEN(TRIM(LookupFullURL.DynamicData_vch)) GT 0>	
	
			<cfif LEFT(TRIM(LookupFullURL.DynamicData_vch), 1) EQ "?">
				
				<cfset NewTarget = NewTarget & LookupFullURL.DynamicData_vch />
				
			<cfelse>
				
				<cfset NewTarget = NewTarget & "?" & LookupFullURL.DynamicData_vch />
				
			</cfif>	
	
	    </cfif>
	    
	    <!--- Update click counter --->
	    <cftry>
	    	    
	    	<!--- Update entry into DB --->
            <cfquery name="UpdateShortURLData" datasource="#DBSourceEBM#">
               	UPDATE  
               		simplelists.url_shortner
			   	SET 	
	               ClickCount_int = ClickCount_int + 1,
				   LastClicked_dt = NOW()
               	WHERE
                    ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#id#">                 
                AND
                	Active_int = 1	              
            </cfquery>      
            
            
            <cfset inpContactString = h />
		
			<!--- Optional hashed contactstring --->
			<cfif LEN(h) GT 0>
				<!--- Reverse the Hash --->
				<!--- The defined SALT and the Alphabet must match source - leave hard coded for now but be careful on future usage --->
				<cfscript>
					hashids = new lz.cfc.Hashids('Brass Monkey', 1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!##%_-*');		
					retVal = hashids.decode(h, hashids.getAlphabet());	
				</cfscript>
				
				<cfif isArray(retVal) AND ArrayLen(retVal) GT 0>
					<cfset inpContactString = retVal[1] />
				</cfif>	
			
			
				<!--- For Short URLs - just store PKId_int as the ccpxdataid - ShortURLs now start at a billion 1,000,000,000 and should have a hard time crossing MLPs --->
				<!--- Add some basic logging for user to see how well their MLPs perform --->
	        	<cfinvoke method="LogMLPAccess" component="lz.cfc.mlp" returnvariable="RetVarLogMLPAccess">
					<cfinvokeargument name="ccpxDataId" value="#LookupFullURL.PKId_bi#">
					<cfinvokeargument name="MLPUserID" value="#LookupFullURL.UserId_int#">
					<cfinvokeargument name="inpContactString" value="#inpContactString#">
				</cfinvoke>	     
				
			</cfif>	
		
	    <cfcatch type="any">
	    
<!---
		    <cfdump var="#cfcatch#" />
			<cfabort /> 
--->	    
	    </cfcatch>
        </cftry> 
	    
	    <!--- In case target URL is changed or updated - erase the cache --->
	    <cfheader name="expires" value="#now()#"> 
		<cfheader name="pragma" value="no-cache"> 
		<cfheader name="cache-control" value="no-cache, no-store, must-revalidate"> 
	    
		<cflocation addToken="no" statusCode="#LookupFullURL.RedirectCode_int#" url="#NewTarget#" />  
	
	<cfelse>
	
		<!--- Look for active MLP --->
		
		<cfset dataout = {} />
		
		<cftry>
		   	
		   	<cfquery name="GetMLP" datasource="#DBSourceManagementRead#" >
	            SELECT 
	            	ccpxDataId_int
	            FROM	
	                simplelists.cppx_data
	            WHERE  	                
                    cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#id#">							                
	            AND    
		        	status_int = 1   
		    </cfquery>
							
	        <cfif GetMLP.RecordCount GT 0>
	        	
				<!--- Quick and dirty re-direct wont display OG and favicon resources correctly --->
				<!--- 	<cflocation addToken="no" url="lz/#id#" statusCode="302" />  --->					
				<!--- 	<script> location.href = <cfoutput>'lz/#id#'</cfoutput>; </script> --->

				<cfset inpURL = id /> 
				<cfinclude template="lz/index.cfm" />
				
				<!--- Exit processing without error being logged --->
				<cfexit> 
						  

	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Can not get this MLP data'>
	        </cfif>	

		    <cfcatch>
		    	<cfset dataout.MESSAGE = 'Can not get this MLP data'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    	

		    	<cfoutput>
			    	
			    	<cfdump var="#cfcatch#" />
			    	cfcatch.message = #cfcatch.message#	
			    	<br/>
			    	cfcatch.detail = #cfcatch.detail#	
		    	</cfoutput>

				    			    	
		    </cfcatch>    	
	    </cftry>    	
	    	    	    
    </cfif>        
        
</cfif>	

<!--- wont get here if re-direct is found but if it is not found....give the user a semi-useful error message --->
<link rel="icon" href="oops.png">
<title>Enterprise Application Integration URL Shortening service for Mobile marketing</title>

<body style="">


	<div style="margin: 2em; padding: 2em;">		
		
		
		<img src="oops.png" style="float: left; margin-right: 3em; " /> 
		
		<h1>WHOOPS!</h1>
		<h3>The page you requested was not found. We have a couple of guesses as to why. </h3>
		
		<ul>
			
			<li>If you typed the URL directly, please make sure the spelling is correct.</li>
			<li>If you clicked on a link to get here, the link is turned off or is outdated.</li>
			
		</ul>

		<p>MLP-X URL Shortening service for Mobile Web</p>					
	</div>

	

</body>




