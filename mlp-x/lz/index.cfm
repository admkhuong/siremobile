<cfsetting showdebugoutput = 0 />


<cfparam name="inpCPPID" default="0">
<cfparam name="inpUUID" default="">
<cfparam name="inpURL" default="">
<cfparam name="action" default="">
<cfparam name="isPreview" default="0">
<cfparam name="checkTerm" default="0">
<cfparam name="showSubmitBtn" default="0">
<cfparam name="checkTotalSubList" default="0">
<cfparam name="inpHDT" default="0"> <!--- Allow look up / review of historical changes --->
<cfparam name="h" default=""> <!--- Optionally Pass in Hashed Contact string for Additional click tracking  --->
<cfparam name="MLPDelayLoad" default="1"> <!--- 1= show, it 0 = ignore it Save certain scripts and code from only loading until on actual MLP - certain js libraries can alter the DOM in ways that get saved into MLP storage and can hose final display - Make available to javascript --->

<cfparam name="dirlink" default="">



<cfset isCPMLP = 0 />

<cfif isPreview EQ 1>
	<cfset inpActive = 0>
<cfelse>
	<cfset inpActive = 1>
</cfif>
<cfif action EQ "template">

	<cfinvoke component="cfc.mlp" method="ReadMLPTemplate" returnvariable="RetCPPXData">
		 <cfinvokeargument name="ccpxDataId" value="#inpURL#">
	</cfinvoke>

<cfelse>

	<cfif inpHDT GT 0> <!--- Special Allow review of historical data --->
		<cfinvoke component="cfc.mlp" method="ReadMLPHDT" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxURL" value="#inpURL#">
			 <cfinvokeargument name="inpHDT" value="#inpHDT#">
		</cfinvoke>
	<cfelseif isPreview EQ "1">
		<cfinvoke component="cfc.mlp" method="ReadMLP" returnvariable="RetCPPXData">
			 <cfinvokeargument name="ccpxURL" value="#inpURL#">
		</cfinvoke>
	<cfelse>


		<cfset inpContactString = h />

		<!--- Optional hashed contactstring --->
		<cfif LEN(h) GT 0>
			<!--- Reverse the Hash --->
			<!--- The defined SALT and the Alphabet must match source - leave hard coded for now but be careful on future usage --->
			<cfscript>
				hashids = new cfc.Hashids('Brass Monkey', 1, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!##%_-*');
				retVal = hashids.decode(h, hashids.getAlphabet());
			</cfscript>

			<cfif isArray(retVal) AND ArrayLen(retVal) GT 0>
				<cfset inpContactString = retVal[1] />
			</cfif>

		</cfif>

		<cfset isCPMLP = 1 />

		<cfinvoke component="cfc.mlp" method="ReadPublishedMLP" returnvariable="RetCPPXData">
			<cfinvokeargument name="ccpxURL" value="#inpURL#">
			<cfinvokeargument name="inpContactString" value="#inpContactString#">
		</cfinvoke>


		<cfset mlpContent = reFind('(<h1 class="mlp-body-msg">(.+)<p id="frequency-text">)', RetCPPXData.RAWCONTENT, 1, 'TRUE')/>

		<cfset getDesc="">
		<cftry>
			<cfset getDesc = Mid(RetCPPXData.RAWCONTENT, mlpContent.pos[1], mlpContent.len[1]).replace('<p id="frequency-text">', '')/>
		<cfcatch>

		</cfcatch>
		</cftry>


		<cfset getDesc = reReplace(getDesc, '(<[^>]*>)', ' ', 'ALL')/>
		<cfset getDesc = reReplace(getDesc, '#chr(34)#', '&##34', 'ALL')/>
		<cfset getDesc = reReplace(getDesc, '#chr(39)#', '&##39', 'ALL')/>
		<!--- <cfset getDesc =REReplace(HTMLCodeFormat(getDesc),'<pre>|</pre>','','all')> --->

		<cfinvoke component="cfc.mlp" method="GetUserOrgLogo" returnvariable="RetLogoURL">
			<cfinvokeargument name="inpUserId" value="#RetCPPXData.USERID#">
		</cfinvoke>

		<cfset imgogURL = RetLogoURL.IMGURL NEQ '' ? 'https://s3-us-west-1.amazonaws.com/siremobile/org_logo/#RetLogoURL.IMGURL#' : 'https://s3-us-west-1.amazonaws.com/siremobile/mlp_image/727D1D902034DF3AAA7B0BF758E5A476/cpg-mlp-default-logo.jpg'/>

		<cfset findMLPImageURL = REFind('(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)',RetCPPXData.RAWCONTENT,1,"TRUE")/>
        <!--- <cfdump var="#REFind('(<img style="max-height: 100%;" id="campaign-mlp-logo"([^>]+)>)',RetCPPXData.RAWCONTENT,1,"TRUE")#" abort="true"/> --->
        <!--- src="([^>]+)"> --->


        <cfset findMLPImageURL = REFind('src="([^>]+)"',RetCPPXData.RAWCONTENT,1,"TRUE")/>

		<cfset MLPImageURL=""/>
		<cftry>
			<cfset MLPImageURL = Mid(RetCPPXData.RAWCONTENT, findMLPImageURL.pos[1], findMLPImageURL.len[1]).replace('src="', '')/>
		<cfcatch>

		</cfcatch>
		</cftry>

        <cfset MLPImageURL = MLPImageURL.replace('"', '')/>

        <cfset previewMLPImg = Len(MLPImageURL) GT 0 ? MLPImageURL : imgogURL />
	</cfif>
</cfif>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>MLP</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0">

	<meta property="og:type" content="website" />

	<!--- repair Seta mistake - temporary fix --->
	<cfif FINDNOCASE("og:image", RetCPPXData.CUSTOMCSS) EQ 0 >

		<cfif isCPMLP EQ 1>
			<meta property="og:title" content="<cfoutput>#RetCPPXData.CPPNAME#</cfoutput>" />
			<meta property="og:description" content="<cfoutput>#getDesc#</cfoutput>" />
			<meta property="og:image" content="<cfoutput>#previewMLPImg#</cfoutput>" />
<!--- IF image is not this size these values screw up facebook - dont know why they are even here like this
			<meta property="og:image:width" content="1200" />
			<meta property="og:image:height" content="627" />
--->
			<meta property="og:url" content="<cfoutput>#cgi.request_url#</cfoutput>" />
		<cfelse>
			<meta property="og:title" content="<cfoutput>#RetCPPXData.CPPNAME#</cfoutput>" />
		</cfif>

		<meta property="twitter:card" content="summary" />
		<meta property="twitter:site" content="@sire_mobile" />
		<meta property="twitter:url" content="<cfoutput>#cgi.request_url#</cfoutput>" />

		<cfif isCPMLP EQ 1>
			<meta property="twitter:description" content="<cfoutput>#getDesc#</cfoutput>" />
			<meta property="twitter:image:src" content="<cfoutput>#previewMLPImg#</cfoutput>" />
			<meta property="twitter:title" content="<cfoutput>#RetCPPXData.CPPNAME#</cfoutput>" />
		<cfelse>
			<meta property="twitter:title" content="<cfoutput>#RetCPPXData.CPPNAME#</cfoutput>" />
		</cfif>

	</cfif>

	<link href="<cfoutput>#dirlink#</cfoutput>css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<cfoutput>#dirlink#</cfoutput>css/uikit3.css" rel="stylesheet" media="screen">
	<script src="<cfoutput>#dirlink#</cfoutput>js/jquery-2.1.4.min.js"></script>
	<script src="<cfoutput>#dirlink#</cfoutput>js/bootstrap.min.js"></script>
	<script src="<cfoutput>#dirlink#</cfoutput>js/select2.min.js"></script>

<!--- 	<link href="css/style.css" rel="stylesheet">  --->




	<!---
	<link href="/public/js/jquery-ui-1.11.4.custom/jquery-ui.theme.css" rel="stylesheet">
	<link href="css/validationEngine.jquery.css" rel="stylesheet">

	<link rel="stylesheet" href="css/font-awesome-4.5.0/css/font-awesome.min.css">

	<link href="css/select2.min.css" rel="stylesheet">
	<link href="css/select2-bootstrap.min.css" rel="stylesheet">
	--->
<!---
	<cfif 1 EQ 2>

		<!--- Support for Googles reCaptcha --->
		<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>


	</cfif>
--->

	<!--- Custom Sire MLP styles --->
	<style>


<!---
		body {
		  min-height: 100%;
		}
--->


		#MLPMain
		{

			<cfif RetCPPXData.CUSTOMCSS NEQ "" AND ISJSON(RetCPPXData.CUSTOMCSS)>

				<cfset CUSTOMCSS = deSerializeJSON(RetCPPXData.CUSTOMCSS) />


				<cfif !StructKeyExists(CUSTOMCSS, "backgroundcolor") >
					<cfset CUSTOMCSS.backgroundcolor = "##FFFFFF"/>
				</cfif>

				<cfif !StructKeyExists(CUSTOMCSS, "fontfamily") >
					<cfset CUSTOMCSS.fontfamily = "inherit" />
				</cfif>

				<cfif !StructKeyExists(CUSTOMCSS, "borderwidth") >
					<cfset CUSTOMCSS.borderwidth = "0px"/>
				</cfif>

				<cfif !StructKeyExists(CUSTOMCSS, "bordercolor") >
					<cfset CUSTOMCSS.bordercolor = "##FF0000"/>
				</cfif>

				<cfif !StructKeyExists(CUSTOMCSS, "borderstyle") >
					<cfset CUSTOMCSS.borderstyle = "none"/>
				</cfif>

				<cfif !StructKeyExists(CUSTOMCSS, "RAW") >
					<cfset CUSTOMCSS.RAW = ""/>
				</cfif>

				background-color: <cfoutput>#CUSTOMCSS.backgroundcolor#</cfoutput>;
				font-family: <cfoutput>#CUSTOMCSS.fontfamily#</cfoutput>;
				border-width: <cfoutput>#CUSTOMCSS.borderwidth#</cfoutput>;
				border-color: <cfoutput>#CUSTOMCSS.bordercolor#</cfoutput>;
				border-style: <cfoutput>#CUSTOMCSS.borderstyle#</cfoutput>;


			</cfif>

			 overflow: auto;

		}

		.select2-container--bootstrap {
		    display: block;
		    width: 100% !important;
		}


	</style>


	<!--- 1= show, it 0 = ignore it Save certain scripts and code from only loading until on actual MLP - certain js libraries can alter the DOM in ways that get saved into MLP storage and can hose final display - Make available to javascript --->
	<script type="text/javascript">
		var MLPDelayLoad = "<cfoutput>#MLPDelayLoad#</cfoutput>";
	</script>

	<!--- Keep legacy css for now.... --->
	<cfif RetCPPXData.CUSTOMCSS NEQ "" AND ISJSON(RetCPPXData.CUSTOMCSS)>
		<cfoutput>#CUSTOMCSS.RAW#</cfoutput>
	<cfelse>
		<cfoutput>#RetCPPXData.CUSTOMCSS#</cfoutput>
	</cfif>

	<!--- Styles that need to also be on main MLP-X page --->
	<style>

		<!--- https://www.w3schools.com/cssref/tryit.asp?filename=trycss_float_clear_overflow --->
		.clearfix::after {
		    content: "";
		    clear: both;
		    display: table;
		}

		.img-responsive {
		    display: block;
		    height: auto;
		    width: 100%;
		    padding: 0;
		}

		<!--- https://zellwk.com/blog/responsive-typography/ --->
		<!--- https://css-tricks.com/viewport-sized-typography/ --->
		#MLPMain h1, #main-stage-content h1 {
			font-size: 5.7vw;
		}

		#MLPMain h2, #main-stage-content h2 {
			font-size: 4.0vw;
		}

		#MLPMain h3, #main-stage-content h3 {
			font-size: 2.8vw;
		}

		#MLPMain h4, #main-stage-content h4 {
			font-size: 2.0vw;
		}

		#MLPMain h5, #main-stage-content h5 {
			font-size: 1.6vw;
		}

		#MLPMain p, #main-stage-content p {
			font-size: 2.5vw;
		}

		@media all and (min-width: 800px) {
		  #MLPMain h1, #main-stage-content h1 {
				font-size: 5.7vw;
			}

			#MLPMain h2, #main-stage-content h2 {
				font-size: 4.0vw;
			}

			#MLPMain h3, #main-stage-content h3 {
				font-size: 2.8vw;
			}

			#MLPMain h4, #main-stage-content h4 {
				font-size: 2.0vw;
			}

			#MLPMain h5, #main-stage-content h5 {
				font-size: 1.6vw;
			}

			#MLPMain p, #main-stage-content p {
				font-size: 2.5vw;
			}
		}

		@media all and (min-width: 1200px) {
		  #MLPMain h1, #main-stage-content h1 {
				font-size: 2.65vw;
			}

			#MLPMain h2, #main-stage-content h2 {
				font-size: 2.0vw;
			}

			#MLPMain h3, #main-stage-content h3 {
				font-size: 1.4vw;
			}

			#MLPMain h4, #main-stage-content h4 {
				font-size: 1.0vw;
			}

			#MLPMain h5, #main-stage-content h5 {
				font-size: 0.8vw;
			}

			#MLPMain p, #main-stage-content p {
				font-size: 1.2vw;
			}
		}

		<!--- An article titled “12 Little-Known CSS Facts”, published recently by SitePoint, mentions that <hr> can set its border-color to its parent's color if you specify  --->
		hr { border-color: inherit }

		.MLPMasterSectionContainer {
		    position: relative;
		}

		.section-arrow-top:before {
		    left: 50%;
		    -ms-transform: translateX(-50%);
		    transform: translateX(-50%);
		    border: solid transparent;
		    border-color: transparent;
		    content: "";
		    height: 0;
		    pointer-events: none;
		    position: absolute;
		    width: 0;
		    z-index: 1;
		    border-bottom-color: inherit !important;
		    border-width: 0 30px 20px !important;
		    bottom: 100%;
		}

		.section-arrow-bottom:before {
		    left: 50%;
		    -ms-transform: translateX(-50%);
		    transform: translateX(-50%);
		    border: solid transparent;
		    border-color: transparent;
		    content: "";
		    height: 0;
		    pointer-events: none;
		    position: absolute;
		    width: 0;
		    z-index: 1;
		    border-top-color: inherit !important;
		    border-width: 20px 30px 0 !important;
		    top: 100%;
		}

		.MLPEditable {
		    position: relative;
		    padding: 2em;
		}

		.campaign-mlp-keyword{
			word-break: break-all;
		}

		.campaign-mlp{
	        min-width: 273px !important;
	    }


	    @media screen and (max-width: 375px){
	            .campaign-mlp{
	            width: 273px !important;
	        }
	    }

	    @media screen and (min-width: 376px) and (max-width: 480px){
	        .campaign-mlp{
	            width: 312px !important;
	        }
	    }

	    .campaign-mlp-shortcode{
	    	word-break: break-word;
	    }

	    @media screen and (max-width: 640px){

		    #frequency-text{
		    	font-size: 13px !important;
		    }
		}

		@media screen and (max-width: 768px){
		    .campaign-mlp-body h2.campaign-mlp-keyword, .campaign-mlp-body span.campaign-mlp-shortcode, h1.mlp-body-msg{
		        font-size: 7vw !important;
		    }

		    .campaign-mlp-body h3, span.mlp-off-percent{
		        font-size: 5vw !important;
		    }

		}


		@media screen and (min-width: 641px){
			#frequency-text{
		    	font-size: 14px !important;
		    }
		}

		@media screen and (min-width: 769px){
			h2.campaign-mlp-keyword{
				font-size: 60px !important;
			}

			.campaign-mlp-body h1{
				font-size: 50px !important;
			}
		}
	</style>

</head>
<body>


	<!---
		<cfinvoke method="ServeAgencyAds" component="session.sire.portals.shopsharenetwork.agency" returnvariable="RetVarReadAgencyAds">
						<cfinvokeargument name="c" value="c">
						<cfinvokeargument name="u" value="522">
					</cfinvoke>


					<cfloop query="RetVarReadAgencyAds.QUERY">

						<li class="MLPXDesignHidden">
							<div class="agency-ad item-grid" data-attr-agency-ad-id="<cfoutput>#RetVarReadAgencyAds.QUERY.PKId_bi#</cfoutput>">

								<input type="hidden" id="Description" value="<cfoutput>#RetVarReadAgencyAds.QUERY.Desc_vch#</cfoutput>" />
								<input type="hidden" id="Expires" value="<cfoutput>#LSDateFormat(RetVarReadAgencyAds.QUERY.Expire_dt, 'yyyy-mm-dd')#</cfoutput>" />

							    <div class="ad-slide">
									<img src="<cfoutput>#RetVarReadAgencyAds.QUERY.ImageLinkOne_vch#</cfoutput>" class="ad-image" />
							    	<p class="ContentOne"><cfoutput>#RetVarReadAgencyAds.QUERY.ContentOne_vch#</cfoutput></p>

									<div class="ad-expires">Expires: <cfif LEN(TRIM(RetVarReadAgencyAds.QUERY.Expire_dt)) GT 0> <cfoutput>#LSDateFormat(RetVarReadAgencyAds.QUERY.Expire_dt, 'yyyy-mm-dd')# </cfoutput> <cfelse> N/A </cfif></div>
								</div>

						    </div>

					    </li>
					</cfloop>

	--->



	<!-- BODY SECTION
	=============================================================-->
<!---
	<div id="main">
		<div class="container-fluid page">
--->
			<div id="MLPMain">
				<input type="hidden" name="inpMLPXId" id="inpMLPXId" value="<cfoutput>#RetCPPXData.CPPID#</cfoutput>" />

			   	<cfif RetCPPXData.RXRESULTCODE EQ 1>
				   <cfoutput>#RetCPPXData.RAWCONTENT#</cfoutput>
			   	<cfelse> <!--- NOT FOUND CPP DATA --->
				   	<div class="col-md-12">
				   		<p class="text-center"> offline </p>
				   	</div>
		   		</cfif>
			</div>

<!---
		</div> <!--- END : container-fluid --->

	</div> <!--- END : main --->
--->

	<!---
	<script src="js/jquery-2.1.4.min.js"></script>
	--->

<!--- 	<script src="/public/js/jquery-ui-1.11.4.custom/jquery-ui.js"></script> --->
	<!---
	<script src="js/bootstrap.min.js"></script>
	<script src="js/retina.min.js"></script>

	<script src="js/jquery.validationEngine.en.js"></script>
	<script src="js/jquery.validationEngine.js"></script>

	<script src="js/bootbox.min.js"></script>
	<script src="js/jquery.mask.min.js"></script>
	<script src="js/select2.min.js"></script>
	<script src="js/mlp.js"></script>
	--->


</body>
</html>




<script type="text/javascript">


	$( function() {

		<!--- Make sure content is not editable in final published page --->
		$('body').find("*[contenteditable='true']").each(function() {
		    $(this).removeAttr("contenteditable");
		});

	});


	function LoadAgencyAds()
	{

		<!--- Save Keyword and Desc Data - validation on server side - will return error message if not valid --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '/session/sire/portals/shopsharenetwork/agency.cfc?method=ServeAgencyAds&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			async: true,
			data:
			{
				inpPKId : 0,
				c: '1',
				u: '522',
				inpFilterActive: $('#AutoPublish').is(':checked')

			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { <!--- No result returned ---> bootbox.alert("Error. No Response from the remote server. Check your connection and try again."); },
			success:
			<!--- Default return function for call back --->
			function(d)
			{
				<!--- RXRESULTCODE is 1 if everything is OK --->
				if (d.RXRESULTCODE == 1)
				{


					var AgencyAdTemplate = $('script[data-template="AgencyAdTemplate"]').text();

					$.each(d.QUERY.DATA.PKId_bi , function(i, val)
					{
						var NewObj = $(AgencyAdTemplate);

						NewObj.attr('data-attr-agency-ad-id', parseInt(d.QUERY.DATA.PKId_bi[i]));

						NewObj.find("img.ad-image").attr('src', d.QUERY.DATA.ImageLinkOne_vch[i]);

						if(d.QUERY.DATA.FORMATEDEXPIRE_DT[i].trim().length > 0)
							NewObj.find("div.ad-expires").html('Expires: ' + d.QUERY.DATA.FORMATEDEXPIRE_DT[i] );
						else
							NewObj.find("div.ad-expires").html('Expires: N/A');

						NewObj.find("p.ContentOne").html(d.QUERY.DATA.ContentOne_vch[i]);

						NewObj.find("#Description").val(d.QUERY.DATA.Desc_vch[i]);

						NewObj.find("#Expires").val(d.QUERY.DATA.FORMATEDEXPIRE_DT[i]);

						$('#AdContainer').append(NewObj);

					});


					$('.bxslider').bxSlider({
					  video: true,
					  useCSS: false,
					  pager: false
					});

				}
				else
				{
					<!--- No result returned - inform user --->

				}
			}

		});

	}

</script>
