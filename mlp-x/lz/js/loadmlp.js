// Public version of this should be minified and comments stripped out - maybe even obfuscate.
// http://www.danstools.com/javascript-obfuscate/index.php

// If you want to use JSON 3 for older browsers, you can load it conditionally: Now the standard window.JSON object is available to you no matter what browser a client is running.
window.JSON || document.write('<script src="//cdnjs.cloudflare.com/ajax/libs/json3/3.2.4/json3.min.js"><\/scr'+'ipt>');

// Build an inline function and call it - local vars remain available to asynchronous return values
!function prepareMLPFrame(w) {
		// Read values from last loaded script - dont use if script is async or defer
		
		// Get array of all scripts so far
		var scripts = document.getElementsByTagName( 'script' );
		
		// Get the last loaded script
		var me = scripts[ scripts.length - 1 ];

		// AJAX - read values for MLP - expiration, default height, etc
		var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function()
        {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
            {
                // alert(xmlHttp.responseText);
                
                var ifrm = document.createElement("iframe");
                // Handle targets content type
		        ifrm.setAttribute("src", ('https:' == document.location.protocol ? 'https://' : 'http://') + "mlp-x.com/" + me.getAttribute('data-mlp-id'));
		       //  i.style = "border: none;"
		        ifrm.style.width = me.getAttribute('data-mlp-w');
		        ifrm.style.height = me.getAttribute('data-mlp-h');
		        ifrm.frameborder = "0";
		        ifrm.style.border= "none";
		        
		        
		        // Since this is now loading asyncronously - use insertAfter instead of appendChild
		        // document.body.appendChild(ifrm);
		        
		        var obj = JSON.parse(xmlHttp.responseText);
		        
		        // console.log(obj);
		        
		        // Only add frame if MLP is not expired
		        if(parseInt(obj.EXPIRE) == 0 )
		        {
			         // target is what you want it to go after. Look for this elements parent.
				    var parent = me.parentNode;
				
				    // if the parents lastchild is the targetElement...
				    if (parent.lastChild == me) {
				        // add the newElement after the target element.
				        parent.appendChild(ifrm);
				    } else {
				        // else the target has siblings, insert the new element between the target and it's next sibling.
				        parent.insertBefore(ifrm, me.nextSibling);
				    }
			    
			    }        
            }
        }
        // Handle targets content type
        xmlHttp.open("post", ('https:' == document.location.protocol ? 'https://' : 'http://') + "dev.siremobile.com/testservice/js/checkmlp.cfm?inpMLPURLID=" + me.getAttribute('data-mlp-id')); 
        xmlHttp.send(''); 
	    
    }(window); 

 
   
    
    
    