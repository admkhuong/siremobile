$(function() {

	$("#cppForm").validationEngine({promptPosition: 'topLeft',scroll: false,focusFirstField : false});

	$(".sms-number").mask("(000)000-0000");
	$(".voice-number").mask("(000)000-0000");

	// ADD INPUT FIELDS
	$(document).on('click','.add-input-object',function(){
    	var cloneObject = $(this).parent().clone();
    	cloneObject.find('input').each(function( index ) {
		  
		});

    	$(this).parent().parent().append(cloneObject);
    	
    	var objectType = $(this).prev().prev().attr('name');

    	var listInputObject = $("input[name='"+objectType+"']");

    	
    	if(listInputObject.length > 1)
    	{
    		listInputObject.each(function( index ) {
		  		$(this).next('.remove-input-object').show();
			});
    	}
    		
	});

	$(document).on('click','.remove-input-object',function(){
    	
    	var objectType = $(this).prev().attr('name');
    	var listInputObject = $("input[name='"+objectType+"']");
    	
    	if(listInputObject.length > 1){
    		$(this).parent().remove();

    		if(listInputObject.length == 2)	{
	    		listInputObject.each(function( index ) {
			  		$(this).next('.remove-input-object').hide();
				});
	    	}
    	}
    		
	});	


	$(document).on('focus','input,select',function(){
			//$("#cppForm").validationEngine('hide');
			
  			$// (this).validationEngine('hide');
			var nextField = $(this).parents('.input-group').first().nextAll(':visible').first().find('input, select');
		
			setTimeout(function(){
				// nextField.validationEngine('hide');
			},1);
	});
	
	$('#btn-submit-cpp').click(function(e){
		if($("#cppForm").validationEngine('validate')){

			var voiceNumberObject = [];
			var smsNumberObject = [];
			var emailObject = [];
			var SubscriberList = [];
			
			// Get all the SMS Number Entries
			$('.sms-number').each(function(){
                var object = new Object();
                object = $(this).val();
                smsNumberObject.push(object);
            });
            
            // Get all the SMS Number Entries
			$('.voice-number').each(function(){
                var object = new Object();
                object = $(this).val();
                smsNumberObject.push(object);
            });
            
            // Get all the SMS Number Entries
			$('.email-address').each(function(){
                var object = new Object();
                object = $(this).val();
                smsNumberObject.push(object);
            });

			// Get all the SubscriberList Entries
			$('.Subscriber-list').each(function(){
                var object = new Object();
                object = $(this).val();
                SubscriberList.push(object);
            });
					          
			
			try
			{
				saveMLPXData(voiceNumberObject,smsNumberObject,emailObject,SubscriberList)	;
/*
				// CHECK TERM
				if(checkTerm == 1){
					var htmlTerms = $('#checkTermModal .modal-body').html();
					bootbox.dialog({
					    message: htmlTerms,
					    title: "Terms Of Service",
					    buttons: {
					        success: {
					            label: "Agree",
					            className: "btn btn-primary",
					            callback: function() {
					            	addToSubscriberList(voiceNumberObject,smsNumberObject,emailObject,SubscriberList)	
					            }
					        },
					        Cancel: {
					            label: "Cancel",
					            className: "btn btn-default",
					            callback: function() {
					            }
					        }
					    }
					});
				}
				else{
					saveMLPXData(voiceNumberObject,smsNumberObject,emailObject,SubscriberList)	
				}
*/
				
			}
			catch(ex)
			{
				bootbox.dialog({
				    message: "There is an error. Please try again later.",
				    title: "MLP-X Customer Preference Capture",
				    buttons: {
				        success: {
				            label: "OK",
				            className: "btn btn-success btn-success-custom",
				            callback: function() {}
				        }
				    }
				});
				$("#btn-submit-cpp").prop('disabled',false);			
				return false;
			}
		}
	});


});


function saveMLPXData(voiceNumberObject,smsNumberObject,emailObject,SubscriberList){
		$.ajax({
			type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
			url: 'cfc/mlp.cfc?method=saveMLPXData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data: {inpMLPXId:$('#inpMLPXId').val(),VoiceNumberObject:voiceNumberObject,SMSNumberObject:smsNumberObject,EmailObject:emailObject,SubscriberList:SubscriberList },
			beforeSend: function( xhr ) {
					$("#btn-submit-cpp").prop('disabled',true);
				},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$("#btn-submit-cpp").prop('disabled',false);
			},					  
			success:		
				function(d) 
				{
					$("#btn-submit-cpp").prop('disabled',false);
					if(d.RXRESULTCODE == 1){
						bootbox.dialog({
						    message: d.MESSAGE,
						    title: "MLP-X Customer Preference Capture",
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						return false;
					}else{

						bootbox.dialog({
						    message: d.MESSAGE+'<br/>'+d.ERRORDATA,
						    title: "MLP-X Customer Preference Capture",
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {
						            }
						        }
						    }
						});
					}
				} 		
		});
	}
