<div class="clearfix"> </div>

<!--- get --->
<section class="module parallax parallax-4 foucload">
    <div class="containerpara4">
    
		<div id="contact" class="get-in-touch">
			<div class="get-in-left">
				<h1>GET IN<span> TOUCH</span></h1>
				<p>What Can We Do To Help?</p>
						
			</div>
			<div class="get-in-right">
				<p><label> We are here to answer any questions you might have about our services. Feel free to call or email us, OR you can reach out to us using the contact form below and we will get back to you shortly.  </label></p>
					
			</div>
			<div class="clearfix"> </div>
		</div>
		
        <div class="contact">
			<div class="col-md-8 contact-left">
				<form id="infoForm" action="info">
					<input id="infoName" name="infoName" type="text" placeholder="Name" required>
					<input id="infoContact" name="infoContact" type="text" class="in" placeholder="Contact Address (eMail or Phone)" required>
					<textarea id="infoSubject" name="infoSubject" class="sub">Subject</textarea>
					<textarea id="infoMsg" name="infoMsg" class="msg">Message</textarea>
					<input type="submit"  name="" value="Send Message">
				</form>
			</div>
			<div class="col-md-4 contact-right">
                
            	<ul>
					<!---<li class="contact-dot"> <span>155 North 400 West, Suite 100</span><BR/><span>Salt Lake City, Utah 84103</span> </li>
					<li class="contact-call"> <span>(949) 394-5710</span></li>--->
                    <li class="contact-dot"> <span>4685 MacArthur Court, Suite 250</span><BR/><span>Newport Beach, CA 92660</span> </li>
					<li class="contact-call"> <span>(949) 400-0553</span></li>
					<li class="contact-msg"> <a href="mailto:info@sire.mobi">info@sire.mobi</a></li>
					<li class="contact-clock"> <span>Mo - Fr: 9am to 6pm Pacific</span></li>
				</ul>
				<BR>                
             	             
            </div>
			<div class="clearfix"> </div>
		</div>
	    </div>
</section>
<!--- //get --->
