<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Survey CSS 1 Demo</title>

 <cfinclude template="../../public/paths.cfm" >

<cfoutput>
	
    
	<style>		
		@import url('#rootUrl#/#PublicPath#/css/survey.css') all;
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css') all;
		@import url('#rootUrl#/#PublicPath#/css/utility.css') all;
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css') all;		
	</style>
      
</cfoutput>

<style>
   .ui-selecting { background: #FECA40; }
   .ui-selected { background: #F39814; }
   
	#TableSurveyQuestions { list-style: none; margin: 0; padding: 0; }
  	.ui-selecting { background: #FECA40; }
	.ui-selecting .handle { background: #FECA40; cursor:move;}
	.ui-selected { background: #F39814; }
	.ui-selected .handle { background: #F39814; cursor:move; }
	.handle {cursor:move; }
		
</style>


</head>

<body>


<div style="width:850px; margin-bottom:400px;" class="stand-alone-content" id="SurveyBuilderContainer">
        <div class="EBMDialog">
                                           
                 <div class="header">
                 	
                    
                 
                    <div class="header-text no-print">Interactive Campaign Builder (Demo Mode)</div>
                                                                          
                     
                     
                </div>
               
                <div style="width:750px;" class="inner-txt-box">
                          

                    <div id="SurveyContent">
                        <div style="float: left; width: 100%; padding-top: 5px;" id="AddQuestionSection">
                                                	
                         
                            
                            
                            
                            
                            </div>
                        <div style="color:#df0000;font-size: 16px;padding: 10px 0 0 15px; clear: both;" id="TitleTypeSurvey"></div>
                        <hr>
                        <div style="padding-top: 5px; padding-bottom: 5px;">
                            
                                <label>Delivery method SMS</label>
                            
                            
                        </div>
                        <hr>
                        
                        
                        
                         
                            <div id="pager1" class="pager no-print">
                                <span>Page 
                                    <span><strong>1</strong></span> 
                                </span>	
                               
                            </div>
                        
                        
                        <ul id="TableSurveyQuestions" class="ui-sortable ui-selectable">
                        	
                            
                        <li rel3="1" qid="2" rq="1" id="q_1" class="q_box SurveyQuestionBorder"> 
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="1" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="1">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="1" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="1" id="QLabel_1" class="q_contents ui-selectee" rq="1"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP1</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="1" rel3="SMS" rel2="2" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_2">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','2','SMS','1', '1'); return false;" rq="1" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_2">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">STATEMENT</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">{%INPFIRSTNAME%}, thanks for your recent social media contact with AT&amp;T. We'll text you shortly for some feedback about your experience. Survey texts are free.</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {159}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                </li><li rel3="2" qid="38" rq="2" id="q_2" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="2" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="2">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="2" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="2" id="QLabel_2" class="q_contents ui-selectee" rq="2"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP2</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="2" rel3="SMS" rel2="38" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_38">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','38','SMS','1', '2'); return false;" rq="2" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_38">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">How likely are you to recommend AT&amp;T services to a friend or family member - on a scale from 10 (definitely) to 1 (definitely not)?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {131}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="3" qid="9" rq="3" id="q_3" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="3" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="3">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="3" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="3" id="QLabel_3" class="q_contents ui-selectee" rq="3"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP3</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('9', 'SMS', 3 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_9">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','9','SMS','1', '3'); return false;" rq="3" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_9">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Q1 to Q2</div>
                            
                            
                            
                            
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_2" class="InfoHoverLink">CP<span class="">2</span>.</a> How likely are you to recommend AT&amp;T services to a friend or family member - on a scale from 10 (definitely) to 1 (definitely not)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {9,10,nine,ten,1 0} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_4" class="InfoHoverLink">CP<span class="">4</span>.</a> Great! How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_2" class="InfoHoverLink">CP<span class="">2</span>.</a> How likely are you to recommend AT&amp;T services to a friend or family member - on a scale from 10 (definitely) to 1 (definitely not)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,2,3,4,one,two,three,four} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_8" class="InfoHoverLink">CP<span class="">8</span>.</a> Sorry to hear. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_6" class="InfoHoverLink">CP<span class="">6</span>.</a> Thanks. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="4" qid="3" rq="4" id="q_4" class="q_box SurveyQuestionBorder"> 
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="4" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="4">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="4" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="4" id="QLabel_4" class="q_contents" rq="4"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP4</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="4" rel3="SMS" rel2="3" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_3">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','3','SMS','1', '4'); return false;" rq="4" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_3">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Great! How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {138}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                </li><li rel3="5" qid="25" rq="5" id="q_5" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="5" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="5">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="5" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="5" id="QLabel_5" class="q_contents ui-selectee" rq="5"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP5</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('25', 'SMS', 5 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_25">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','25','SMS','1', '5'); return false;" rq="5" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_25">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go To Branch (Q2 to Q3)</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_10" class="InfoHoverLink">CP<span class="">10</span>.</a> Q2 to Q3</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="6" qid="7" rq="6" id="q_6" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="6" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="6">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="6" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="6" id="QLabel_6" class="q_contents ui-selectee" rq="6"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP6</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="6" rel3="SMS" rel2="7" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_7">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','7','SMS','1', '6'); return false;" rq="6" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_7">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Thanks. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {136}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="7" qid="26" rq="7" id="q_7" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="7" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="7">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="7" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="7" id="QLabel_7" class="q_contents ui-selectee" rq="7"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP7</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('26', 'SMS', 7 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_26">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','26','SMS','1', '7'); return false;" rq="7" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_26">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go To Branch (Q2 to Q3)</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_10" class="InfoHoverLink">CP<span class="">10</span>.</a> Q2 to Q3</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="8" qid="8" rq="8" id="q_8" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="8" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="8">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="8" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="8" id="QLabel_8" class="q_contents ui-selectee" rq="8"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP8</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="8" rel3="SMS" rel2="8" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_8">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','8','SMS','1', '8'); return false;" rq="8" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_8">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Sorry to hear. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {143}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="9" qid="27" rq="9" id="q_9" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="9" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="9">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="9" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="9" id="QLabel_9" class="q_contents ui-selectee" rq="9"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP9</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('27', 'SMS', 9 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_27">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','27','SMS','1', '9'); return false;" rq="9" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_27">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go To Branch (Q2 to Q3)</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_10" class="InfoHoverLink">CP<span class="">10</span>.</a> Q2 to Q3</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="10" qid="21" rq="10" id="q_10" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="10" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="10">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="10" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="10" id="QLabel_10" class="q_contents ui-selectee" rq="10"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP10</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('21', 'SMS', 10 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_21">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','21','SMS','1', '10'); return false;" rq="10" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_21">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Q2 to Q3</div>
                            
                            
                            
                            
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_4" class="InfoHoverLink">CP<span class="">4</span>.</a> Great! How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {9,10,nine,ten,1 0} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_11" class="InfoHoverLink">CP<span class="">11</span>.</a> Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_6" class="InfoHoverLink">CP<span class="">6</span>.</a> Thanks. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {9,10,nine,ten,1 0} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_11" class="InfoHoverLink">CP<span class="">11</span>.</a> Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_8" class="InfoHoverLink">CP<span class="">8</span>.</a> Sorry to hear. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {9,10,nine,ten,1 0} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_11" class="InfoHoverLink">CP<span class="">11</span>.</a> Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_4" class="InfoHoverLink">CP<span class="">4</span>.</a> Great! How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,2,3,4,one,two,three,four} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_15" class="InfoHoverLink">CP<span class="">15</span>.</a> Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_6" class="InfoHoverLink">CP<span class="">6</span>.</a> Thanks. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,2,3,4,one,two,three,four} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_15" class="InfoHoverLink">CP<span class="">15</span>.</a> Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_8" class="InfoHoverLink">CP<span class="">8</span>.</a> Sorry to hear. How satisfied are you with the overall social media experience by {%INPREPNAME%} from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,2,3,4,one,two,three,four} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_15" class="InfoHoverLink">CP<span class="">15</span>.</a> Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_13" class="InfoHoverLink">CP<span class="">13</span>.</a> Thanks. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="11" qid="4" rq="11" id="q_11" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="11" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="11">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="11" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="11" id="QLabel_11" class="q_contents ui-selectee" rq="11"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP11</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="11" rel3="SMS" rel2="4" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_4">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','4','SMS','1', '11'); return false;" rq="11" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_4">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">ONESELECTION</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                            <div class="q_answer"> 
                                
                                
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>1. One 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>2. Two 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>3. Three 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>4. Four or more 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>5. Not Resolved 
                                                        
                                                        
                                                        </div>
                                                    
                                                        
                            </div> 
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {135}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="12" qid="42" rq="12" id="q_12" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="12" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="12">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="12" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="12" id="QLabel_12" class="q_contents ui-selectee" rq="12"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP12</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('42', 'SMS', 12 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_42">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','42','SMS','1', '12'); return false;" rq="12" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_42">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go to Comments Branch</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_16" class="InfoHoverLink">CP<span class="">16</span>.</a> Go to Comments</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="13" qid="40" rq="13" id="q_13" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="13" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="13">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="13" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="13" id="QLabel_13" class="q_contents ui-selectee" rq="13"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP13</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="13" rel3="SMS" rel2="40" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_40">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','40','SMS','1', '13'); return false;" rq="13" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_40">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">ONESELECTION</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Thanks. How many times did you have to contact AT&amp;T to resolve your request?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                            <div class="q_answer"> 
                                
                                
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>1. One 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>2. Two 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>3. Three 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>4. Four or more 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>5. Not Resolved 
                                                        
                                                        
                                                        </div>
                                                    
                                                        
                            </div> 
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {136}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="14" qid="36" rq="14" id="q_14" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="14" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="14">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="14" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="14" id="QLabel_14" class="q_contents ui-selectee" rq="14"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP14</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('36', 'SMS', 14 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_36">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','36','SMS','1', '14'); return false;" rq="14" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_36">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go to Comments Branch</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_16" class="InfoHoverLink">CP<span class="">16</span>.</a> Go to Comments</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="15" qid="14" rq="15" id="q_15" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="15" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="15">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="15" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="15" id="QLabel_15" class="q_contents ui-selectee" rq="15"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP15</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="15" rel3="SMS" rel2="14" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_14">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','14','SMS','1', '15'); return false;" rq="15" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_14">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">ONESELECTION</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                            <div class="q_answer"> 
                                
                                
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>1. One 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>2. Two 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>3. Three 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option white">
                                                            
                                                            
                                                                    <br>4. Four or more 
                                                        
                                                        
                                                        </div>
                                                    
                                                                                                
                                                        <div class="q_option grey">
                                                            
                                                            
                                                                    <br>5. Not Resolved 
                                                        
                                                        
                                                        </div>
                                                    
                                                        
                            </div> 
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {143}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="16" qid="48" rq="16" id="q_16" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="16" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="16">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="16" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="16" id="QLabel_16" class="q_contents ui-selectee" rq="16"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP16</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('48', 'SMS', 16 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_48">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','48','SMS','1', '16'); return false;" rq="16" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_48">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go to Comments</div>
                            
                            
                            
                            
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_11" class="InfoHoverLink">CP<span class="">11</span>.</a> Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,one} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_17" class="InfoHoverLink">CP<span class="">17</span>.</a> Excellent! Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_13" class="InfoHoverLink">CP<span class="">13</span>.</a> Thanks. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,one} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_17" class="InfoHoverLink">CP<span class="">17</span>.</a> Excellent! Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_15" class="InfoHoverLink">CP<span class="">15</span>.</a> Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {1,one} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_17" class="InfoHoverLink">CP<span class="">17</span>.</a> Excellent! Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_11" class="InfoHoverLink">CP<span class="">11</span>.</a> Great! How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {5,five} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_21" class="InfoHoverLink">CP<span class="">21</span>.</a> Sorry you were not satisfied. Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_13" class="InfoHoverLink">CP<span class="">13</span>.</a> Thanks. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {5,five} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_21" class="InfoHoverLink">CP<span class="">21</span>.</a> Sorry you were not satisfied. Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                
                                        <div ctype="RESPONSE" class="ConditionItemDisplay">
                                                                    
                                            
                                                            
                                                           <div class="InformationLabel">Key off Question:</div>
                                                           <div class="Information nosel"><a href="#QLabel_15" class="InfoHoverLink">CP<span class="">15</span>.</a> Sorry to hear. How many times did you have to contact AT&amp;T to resolve your request?</div>
                                                        
                                                        
                                                    <div class="InformationLabel">Condition:</div>
                                                    <div class="Information">Equals (=)</div>
                                                
                                                
                                                                       
                                                <div class="InformationLabel">Match Value(s):</div>
                                                
                                                                                    
                                                
                                                        <div class="Information">{} {5,five} </div>
                                                    
                                                        
                                                       <div class="InformationLabel">Question BRANCH jumps to when conditional matches:</div>
                                                       <div class="Information nosel"><a href="#QLabel_21" class="InfoHoverLink">CP<span class="">21</span>.</a> Sorry you were not satisfied. Please provide any additional feedback to help us fully understand your experience.</div>
                                                        
                                                                         
                                            
                                        </div>    
                                        
                                        <div class="clear row_padding_top"></div>
                                        
                                    
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_19" class="InfoHoverLink">CP<span class="">19</span>.</a> Thank you. Please provide any additional feedback to help us fully understand your experience.</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="17" qid="15" rq="17" id="q_17" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="17" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="17">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="17" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="17" id="QLabel_17" class="q_contents ui-selectee" rq="17"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP17</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="17" rel3="SMS" rel2="15" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_15">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','15','SMS','1', '17'); return false;" rq="17" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_15">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Excellent! Please provide any additional feedback to help us fully understand your experience.</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {94}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="18" qid="50" rq="18" id="q_18" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="18" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="18">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="18" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="18" id="QLabel_18" class="q_contents ui-selectee" rq="18"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP18</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('50', 'SMS', 18 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_50">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','50','SMS','1', '18'); return false;" rq="18" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_50">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go to End</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_22" class="InfoHoverLink">CP<span class="">22</span>.</a> Thanks again for your time. Your comments help us improve, such as offering flexible 24/7 service. Get it with the myAT&amp;T app - att.com/myattapp.</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="19" qid="44" rq="19" id="q_19" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="19" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="19">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="19" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="19" id="QLabel_19" class="q_contents ui-selectee" rq="19"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP19</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="19" rel3="SMS" rel2="44" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_44">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','44','SMS','1', '19'); return false;" rq="19" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_44">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Thank you. Please provide any additional feedback to help us fully understand your experience.</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {94}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="20" qid="51" rq="20" id="q_20" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="20" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="20">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="20" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="20" id="QLabel_20" class="q_contents ui-selectee" rq="20"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP20</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                         <button onclick="EditBranchLogic('51', 'SMS', 20 , $(this)); return false;" type="button" class="no-print ui-corner-all survey_builder_button edit_b" id="edit_quest_51">Edit Branch</button>                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','51','SMS','1', '20'); return false;" rq="20" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_51">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">BRANCH</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Go to End</div>
                            
                            
                            
                            
                                            
                                           <div class="InformationLabel">Question BRANCH jumps to when no conditions are matched or defined:</div>
                                           <div class="Information nosel"><a href="#QLabel_22" class="InfoHoverLink">CP<span class="">22</span>.</a> Thanks again for your time. Your comments help us improve, such as offering flexible 24/7 service. Get it with the myAT&amp;T app - att.com/myattapp.</div>
                                            
                                        
                            
                        </div> 
                        
                        
                        
                        
                    </div> 
                
                       
	                </li><li rel3="21" qid="46" rq="21" id="q_21" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="21" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="21">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="21" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="21" id="QLabel_21" class="q_contents ui-selectee" rq="21"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP21</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="21" rel3="SMS" rel2="46" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_46">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','46','SMS','1', '21'); return false;" rq="21" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_46">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">SHORTANSWER</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Sorry you were not satisfied. Please provide any additional feedback to help us fully understand your experience.</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {113}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li><li rel3="22" qid="6" rq="22" id="q_22" class="q_box SurveyQuestionBorder"> 
                
                        <div class="add_box handle no-print" style="display: none;">
                            <button inpbefore="0" inpafter="0" rel4="22" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q" rel24="22">Add Control Point</button>
                            <button inpbefore="0" inpafter="0" rel1="22" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline">Add Branch Logic</button>
                        </div>
                    
                    <div rel1="22" id="QLabel_22" class="q_contents ui-selectee" rq="22"> 
                    
                        <div class="q_actions"> 
                            <label><span class="label_ handle">CP22</span></label>
                            <span class="button_box no-print" style="display: none;">
                                
                                
                                    
                                        <button rel4="22" rel3="SMS" rel2="6" rel1="1488" type="button" class="no-print ui-corner-all survey_builder_button edit_q" id="edit_quest_6">Edit Control Point</button>  
                                        
                                    
                                    <button onclick="deleteQuestion(this,'1488','6','SMS','1', '22'); return false;" rq="22" type="button" class="no-print ui-corner-all survey_builder_button delete_q" id="delete_quest_6">Delete</button>
                                
                            </span>
                            
                            <span class="SurveyQuestionType">STATEMENT</span>
                            
                        </div> 
                        
                        
                        <div class="q_title">  
                        
                        	
                
                
                            <div class="SurveyQuestionText">Thanks again for your time. Your comments help us improve, such as offering flexible 24/7 service. Get it with the myAT&amp;T app - att.com/myattapp.</div>
                            
                            
                            
                            
                            
                        </div> 
                        
                        
                        
                        
                            <div style="float:right;">
                                
                                            Character Count {145}
                                        
                                
                            </div>
            
                        
                        
                    </div> 
                
                       
	                </li></ul>    
                        
                        
                            <div class="add_box handle no-print" style="display: none;">
                                <button inpbefore="0" inpafter="1" rel4="23" rel3="SMS" rel2="0" type="button" class="no-print ui-corner-all survey_builder_button add_button edit_q pageLevelAddQ " rel24="23">Add Control Point</button>
                                <button inpbefore="0" inpafter="1" rel1="23" type="button" class="no-print ui-corner-all survey_builder_button add_branch_button AddBranchLogicInline pageLevelAddBranch">Add Branch Logic</button>
                            </div>
                         
                            <div id="pager2" class="pager no-print">
                                <span>Page 
                                    <span><strong>1</strong></span> 
                                </span>			
                            </div>
                        
                        <hr>
                        <div style="padding-top: 5px">
                           
                            
                            
                            
                            
                            <div class="clear"></div>
                        </div>
                    </div>
                    
                    
                    <div style="height: 30px; width: 100%; display: block;" class="clear">
                        
                    </div>
              
                </div>
                
                <div style="clear:both"></div>
                <div style="clear:both"></div>
                
                                             
        </div>     
	</div>
    
    
</body>
</html>
