﻿<!DOCTYPE html>
<html>
<head>

<cfinclude template="../public/paths.cfm" >
<!---<cfinclude template="../public/header.cfm">--->

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

<title>Forgot Password</title>
	
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <!-- js -->
    <script src="js/jquery.min.js"></script>
  
    
    <!-- //js -->
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="SMS, Push and Web. Mobile Surveys, Keyword Response, Data Capture, Time Machine, Smart, Interactive, Response, Engine" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        $(function() 
        {
        
            $(".scroll").click(function(event){		
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
            
           $("#txtEmailOrUserName").keydown(function(){ 
			   	$(".error-message").html("");
				$("#txtEmailOrUserName").css("border-color","rgba(0, 0, 0, 0.3)");
			});
				
    
        });
          
			
				
    function RequestPasswordReset()
	{	
		var txtEmailOrUserName = $("#txtEmailOrUserName").val();
		if(txtEmailOrUserName!="")
		{
			var recaptcha = $("#recaptcha_response_field").val();
			if (recaptcha != "") {
				$.ajax({
					type: "POST",
					url:  '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/cfc/users.cfc?method=ResetPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  {
						inpUserName : $("#txtEmailOrUserName").val(),
						recaptcha_challenge_field : $("#recaptcha_challenge_field").val(), 
						recaptcha_response_field : $("#recaptcha_response_field").val()
						},										  
					error: function(XMLHttpRequest, textStatus, errorThrown)
					{
					 	<!---console.log(textStatus, errorThrown);--->
					},
					success:				  
					//Default return function for Do CFTE Demo - Async call back 
					function(d) 
					{												
						if(typeof(d.RXRESULTCODE) != "undefined")
						{
							if (d.RXRESULTCODE > 0) 
							{	 							
								//Navigate tp sesion Home
								jAlert("Check your email for a link with a special code needed to set your new password", "Request success!", function(result){
									window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/home.cfm";
								});
							}
							else
							{
								$(".error-message").html(d.MESSAGE);
								$("#txtEmailOrUserName").css("border-color","red");
								javascript:Recaptcha.reload();
//								jAlert(d.MESSAGE, "Request fail!", function(result){
//									javascript:Recaptcha.reload();
//								});
							}
						}
						else
						{
							jAlert("Error while resetting - please check your connection and try again later", "Request Fail!", function(result){
									javascript:Recaptcha.reload();
							});						
						}
					} 		
				});
			}
			else
			{	
				jAlert("Please enter captcha", "Warning Message!", function(result){
					javascript:Recaptcha.reload();
				});	
			}
		}
		else
		{
			jAlert("Please enter your email/username", "Warning Message!", function(result){
					javascript:Recaptcha.reload();
			});
		}		
		return false;				
	}	
            
            
    </script>
    <!-- start-smoth-scrolling -->


</head>
<body>

<!-- banner -->
	<div id="home" class="bannersmall header-unit" style="">
      
    
        <div class="container container-header">
    
           	<div class="head-logo">
                <a href="home">                
                <p>
                <font style="color:#0085c8; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font>
                <BR>
                <font style="color:#0085c8; font-size:22px; font-weight:400;">Smart</font> <font style="color:#ff9900; font-size:22px; font-weight:400;">Interactive Response Engine</font>
                </p>
                
                </a>
            </div>
   		 
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <ul class="nav1">
                        <li><a href="home" class="">Home</a></li>                       
                        <li><a href="#contact" class="scroll">Contact</a></li>
                    </ul>
                    <script> 
                               $( "span.menu" ).click(function() {
                                 $( "ul.nav1" ).slideToggle( 300, function() {
                                 // Animation complete.
                                  });
                                 });
                            
                    </script>

            </div>
    
            <div class="clearfix"> </div>
    
        </div>
                
	</div>
    
            
    <cfoutput>
	    
    		<div class="container">
                  
				<div id="inner-bg-m1" style="min-height: 450px">
        
					<div class="contentm1" id="forgetpassword_box">
						<div class="row-fluid">
						    <div class="dialog">
						        <div class="block">	            
									<p class="block-sub-heading">Enter your email or username below to reset your password:</p>
						            <div class="block-body">
						                <div id="login-form">
						                	<div class="login-row">
											<input class="login-input" type="text" id="txtEmailOrUserName" name="txtEmailOrUserName">
											</div>
											
											<div class="login-row">
												<div class="error-message"></div>
											</div>
											
											<div class="login-row" id="capthcha-row">
												<cfinvoke 
										             component="#PublicPath#.cfc.users"
										             method="render_reCAPTHCHA"
										             returnvariable="RetValRECaptchaData">
										             <cfinvokeargument name="ssl" value="true"/>
										             <cfinvokeargument name="theme" value="red"/>
										             <cfinvokeargument name="tabIndex" value="10"/> 
										        </cfinvoke>
											</div>
											
											<div class="login-footer">
												<button class="btn btn-primary pull-right loginbutton bluelogin small" onclick="RequestPasswordReset(); return false;" href="##">Submit</button>
											</div>
						                    <div class="clearfix"></div>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>

			</div>


	   <cfinclude template="contactsection.cfm" />
        
    </cfoutput>
    

<!--- footer --->
	<div class="footer">
	<div class="container">
		<p> &copy; 2015 <font style="color:#ffffff; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font> All rights reserved.</p>
	</div>
	</div>
<!--- footer --->
<!--- here stars scrolling icon --->
	<script type="text/javascript">
	 $(function() 
	{
		/*
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		*/
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
		$('.foucload').show();
		
		
		
	});
	</script>
<!--- //here ends scrolling icon --->
</body>
</html>