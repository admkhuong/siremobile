<!DOCTYPE html>
<html>
<head>

	<cfinclude template="../session/cfc/csc/constants.cfm">
    <cfinclude template="../public/paths.cfm" >
    
    
<cfif IsStruct(Session)>
	<cfif !StructKeyExists(Session, "USERID") >
		<cfset Session.USERID = 0 />
	</cfif>
</cfif>


<!--- Validate cookie and remember me option here--->

<cftry>

 	<cfparam name="Session.MFAISON" default="0">
    <cfparam name="Session.MFALENGTH" default="0"> 
    
    <!--- Decrypt out remember me cookie. --->
    <cfset LOCALOUTPUT.RememberMe = Decrypt(
		COOKIE.RememberMe,
		APPLICATION.EncryptionKey_Cookies,
		"cfmx_compat",
		"hex"
		) />
    <!---
	For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
	of list. Extract it from the list.
	--->
    <cfset CurrRememberMe = ListGetAt(
		LOCALOUTPUT.RememberMe,
		3,
		":"
		) />

    <!---
	Check to make sure this value is numeric, otherwise, it was not a valid value.
	--->
    <cfif IsNumeric( CurrRememberMe )>

        <!---
		We have successfully retreived the "remember me" ID from the user's cookie. Now, store
		that ID into the session as that is how we are tracking the logged-in status.
		--->
        <cfset Session.USERID = CurrRememberMe />
        <cfset SESSION.isAdmin = true>
        <cfset SESSION.loggedIn = "1" />
        
        <!--- *** What is this for ?!? --->
        <cfif StructKeyExists(cookie, "VOICEANSWERCOOKIE")>
            <cfset cookie.VOICEANSWERCOOKIE = null>
        </cfif>
    </cfif>

	<cfset Session.PrimaryPhone = ListGetAt(LOCALOUTPUT.RememberMe, 4, ":") />
    <cfset Session.at = ListGetAt(LOCALOUTPUT.RememberMe, 5, ":") />
    <cfset Session.facebook = ListGetAt(LOCALOUTPUT.RememberMe, 6, ":") />
    <cfset Session.fbuserid = ListGetAt(LOCALOUTPUT.RememberMe, 7, ":") />
    <cfset Session.EmailAddress = ListGetAt(LOCALOUTPUT.RememberMe, 8, ":") />
    <cfset Session.UserNAme = ListGetAt(LOCALOUTPUT.RememberMe, 10, ":") />
    <cfset SESSION.CompanyUserId = ListGetAt(LOCALOUTPUT.RememberMe, 11, ":") />
    <cfset Session.permissionManaged = ListGetAt(LOCALOUTPUT.RememberMe, 12, ":") />
    <cfset Session.companyId = ListGetAt(LOCALOUTPUT.RememberMe, 14, ":") />
    <cfset Session.MFAOK = ListGetAt(LOCALOUTPUT.RememberMe, 15, ":") /> 
    <cfset Session.MFAISON = ListGetAt(LOCALOUTPUT.RememberMe, 16, ":") /> 
    <cfset Session.MFALENGTH = ListGetAt(LOCALOUTPUT.RememberMe, 17, ":") /> 
            
    <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
        UPDATE
        	simpleobjects.useraccount
        SET
        	LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'
        WHERE
        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        AND
        	(CBPID_int = 1 OR CBPID_int IS NULL)
    </cfquery>


	<!--- Insetad of relocating back to session just let user know they are logged in --->
    <!---<cflocation addtoken="false" url="#rootUrl#/#SessionPath#/account/home">--->
    
    
    
    <!--- Catch any errors. --->
    <cfcatch TYPE="any">
        <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
    </cfcatch>
</cftry>


    
    <title>Home</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <!-- js -->
    
    
    <cfoutput>
	
		<!---<script src="js/jquery.min.js"></script>--->
        <script src="#rootUrl#/#PublicPath#/js/jquery-1.9.1.min.js"></script>
        <script src="js/responsiveslides.min.js"></script>
    
    	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
        
        <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">
        <!---<script src="#rootUrl#/#PublicPath#/js/jquery.cookie.js"></script>  --->
        
        <style>
			@import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;		
			
			<!--- Local overrides --->
			.ui-widget-overlay { background: ##222222 50% 50% repeat-x; opacity: .6;filter:Alpha(Opacity=60); }
			
			a:focus
			{
				outline:none;
				text-decoration:none;				
			}
									
		</style>
    
    </cfoutput>
    
    <!-- //js -->
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="SMS, Push and Web. Mobile Surveys, Keyword Response, Data Capture, Time Machine, Smart, Interactive, Response, Engine" />
    
	
	
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
	
        $(function() 
        {
        
            $(".scroll").click(function(event){		
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
            
           <!--- document.getElementById('BannerVideo').addEventListener('playing',onBannerVideoStarted,false);
            document.getElementById('BannerVideo').addEventListener('ended',onBannerVideoEnded,false);--->
			
			
			//callback handler for form submit
			$("#infoForm").submit(function(e)
			{
				var postData = $(this).serializeArray();
				var formURL = $(this).attr("action");
				
				$.ajax(
				{
					url : formURL,
					type: "POST",
					data : postData,
					success:function(data, textStatus, jqXHR) 
					{
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Thank you " + $('#infoName').val() + ", \nYour request for information has been sent.\n", "Thank You!!", function(result) { } );		
				
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
						//if fails      
					}
				});
				
				e.preventDefault(); //STOP default action			
			});


			$("#SignInTopLink, #SignInTopLinkBar").click(function(){

				<!---if($('#SignInContainer').css("display") == 'none' )
				{
					<!--- Two overlays because of staic header - 1 overlay wouldnt cut it z-index wise--->
					$("#overlay").toggle();
					$("#overlayheader").toggle();
					$("#SignInContainer").toggle("blind", {}, 250);
				}
				else
				{
					<!--- Details - wait for effect to finsh on toggle close before removing overlay--->
					$("#SignInContainer").toggle("blind", function(){$("#overlay").toggle(); $("#overlayheader").toggle();}, 250);
				}--->
				
			<!---	$("#overlay").toggle();
				$("#overlayheader").toggle();--->
								
				$( "#SignInContainer" ).dialog({
				 dialogClass: 'noTitleStuff',
				  resizable: false,
				  closeOnEscape: true,
				  width:473,
				  height:320,
				  modal: true,
				  close: 	function(event, ui)
							{
								<!---// $(this).destroy();
								$("#overlay").toggle();
								$("#overlayheader").toggle();--->
							},
				<!---  position: { my: "right top", at: "right bottom", of: $(this), collision: 'none'  }, --->
				  autoOpen: true
				});
			
			

			});
			
			$('#login_close').click(function(){
				
				$( "#SignInContainer" ).dialog( "close" );
				return false;
			});
					
			$('#ConfirmLogOffButton').click(function(){				
				
				if($("#DALD").is(':checked'))
					window.location = '<cfoutput>#rootUrl#/#SessionPath#/act_logout?DALD=1&LODest=#rootUrl#/pub/home</cfoutput>';
				else
					window.location = '<cfoutput>#rootUrl#/#SessionPath#/act_logout?DALD=0&LODest=#rootUrl#/pub/home</cfoutput>';
				
				
				$( "#dialog-confirm-logout" ).dialog( "close" );
			});
			
			$('#CancelLogOffButton').click(function(){
				
				$( "#dialog-confirm-logout" ).dialog( "close" );
				return false;
			});
				
				
			
			
			$("#logout, #logout2").click(function(){
							
				$("#overlay").toggle();
				$("#overlayheader").toggle();
								
				$( "#dialog-confirm-logout" ).dialog({
				  resizable: false,
				  closeOnEscape: true,
				  width:650,
				  <cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
					height:280,
				  <cfelse>
					height:200,
				  </cfif>
				  modal: true,
				  close: 	function(event, ui)
							{
								<!---// $(this).destroy();
								$("#overlay").toggle();
								$("#overlayheader").toggle();--->
							},
				  <!---position: { my: "right top", at: "right bottom", of: $(this), collision: 'none'  }, --->
				  autoOpen: true
				});
				
		
			});
		

			$('#SignInSectionTop').show();	
			
			
			
			$(".our-services-grid-link").click(function(){
											
				window.location = $(this).attr("rel1"); 
		
			});
			
			
			

    
        });
		
		
		function isValidRequiredField(fieldId){
			if($("#" + fieldId).val() == ''){
				$("#err_" + fieldId).show();
				return false;
			}else{
				$("#err_" + fieldId).hide();
				return true;
			}
		}
        
      
	  <!---  
        function onBannerVideoEnded(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }
        
        function onBannerVideoStarted(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }--->
            
            
            
    </script>
    <!-- start-smoth-scrolling -->


</head>

<style>

#video-container {
	position: absolute;
	top:0%;
	left:0%;
	height:100%;
	width:100%;
	overflow: hidden;
	z-index: 1;
}

.header-unit {
  height: 100%;
  border: 2px solid #000;
  border-right:none;
  border-left: none;
  position: relative;
  padding: 20px;
}

#video-container {
	position: absolute;
	top:0%;
	left:0%;
	height:100%;
	width:100%;
	overflow: hidden;
}

video {
	position:absolute;
	z-index:0;
	
<!---	background:transparent url('images/bannerfirstframe.jpg') no-repeat 0 0; 
   -webkit-background-size:cover; 
   -moz-background-size:cover; 
   -o-background-size:cover; 
   background-size:cover; --->
   
 min-height: 480px; 
 min-width: 854px; 
<!--- height: auto !important;
 width: auto !important; --->

<!---
I used the following CSS that worked for me. Tested on iPad mini with iOS 7.1

video {
 min-height: 100%; 
 min-width: 100%; 
 height: auto !important;
 width: auto !important; 
}
--->
	
	
}

video.fillWidth {
	width: 100%;
}

.container-header
{
	position: absolute;
	top:0%;
	left:0%;
	height:100%;
	width:100%;	
}

video::-webkit-media-controls {
    display:none !important;
}

@media only screen and (max-width: 530px)  {
	
	
video {
	   
 min-height: 100%; 
 min-width: 100%; 
 height: 100% !important;
 width: auto !important;
	
}
	
	}


#overlayheader {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: #000;
		filter:alpha(opacity=75);
		-moz-opacity:0.75;
		-khtml-opacity: 0.75;
		opacity: 0.35;
		z-index: 5;
		display:visible;
	}

.container-header
{
	z-index:10;		
}

</style>
	
<body>
<!-- banner -->
	<div id="home" class="banner header-unit" style="">
    
     	<div id="video-container">
        
        	<div id="overlayheader"></div>
                    
            <video id="BannerVideo" poster="images/bannerfirstframe.jpg" muted="muted" autoplay style="" class="fillWidth">
                <source type="video/mp4" src="images/video/24782510_480p.mp4"></source>
                <source type="video/webm" src="images/video/24782510_480p.webm"></source>
            </video>
                	        
        </div>
            
        <div class="container container-header">
    
           <!--- <div class="head-logo">
                <a href="#"><img src="images/sirelogoorangeweb75x114iii.png" alt=" " height="114" width="75"></a>
            </div>--->
    
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <ul class="nav1">
                        <li><a href="#home" class="scroll">Home</a></li>
                        <li><a href="#about" class="scroll">About Us</a></li>
                        <li><a href="#products" class="scroll">Products</a></li>
                        <li><a href="#demoOTT" class="scroll">Demo</a></li>
                        <li><a href="#pricing" class="scroll">Pricing</a></li>
                        <li><a href="#contact" class="scroll">Contact</a></li>
                    </ul>
                    <script> 
                               $( "span.menu" ).click(function() {
                                 $( "ul.nav1" ).slideToggle( 300, function() {
                                 // Animation complete.
                                  });
                                 });
                            
                    </script>

            </div>
    
            <div class="clearfix"> </div>
            
                      
    
            <div class="banner-info">
                <h2>Mobile Moments</h2>
                <h2>Interact with customers where they are, when they are ready.</h2>
                
                <!---<img id="BannerLogo" src="images/sirelogo6web274x128.png" alt=" " height="128" width="274" />--->
                
                
                <p>
                <font style="color:#0085c8; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font>
                <BR>
                <font style="color:#0085c8; font-size:22px; font-weight:400;">Smart</font> <font style="color:#ff9900; font-size:22px; font-weight:400;">Interactive Response Engine</font>
                
                
                </p>
                
               <!--- <div class="BannerIconImages">
                	<img id="BannerPhoneIcon" src="images/iphone6baseicon.png" alt=" "  width="30" height="60" style="margin-top:40px;"/>
                    <img id="BannerPhoneIcon" src="images/tabletweb71x100.png" alt=" "  width="71" height="100"/>
                </div>--->
                
                <div class="BannerFooter">                	                    
                   <em>SMS, Push and Web. Mobile Surveys, Keyword Response, Data Capture, Time Machine</em>
                </div>
                
                <cfoutput>
                <div id="SignInSectionTop" class="SignInSectionTop">
                
                	<ul class="nav2">
                
					<cfif Session.USERID GT 0>
                        <li><a id="InSessionAccount" class="" href="#rootUrl#/#SessionPath#/account/home"><i class="icon-enter"></i>My Account</a></li>
                        
                        <li><a href="##" class="" id="logout"><i class="icon-new-tab"></i>Log Out</a></li>
                        
                    <cfelse>
                        <li><a id="SignInTopLink" class="" href="##"><i class="icon-enter"></i>Sign In</a>            
                    
                        <li><a href="#rootUrl#/pub/signup" class=""><i class="icon-new-tab"></i>Sign Up</a></li>
                    
                    </cfif>
            
            		</ul>
                    
                </div>
                </cfoutput>
                
            
            </div>
    
        </div>
       
       
      	<!--- <div id="overlayheader"> </div>--->

        <div id="dialog-confirm-logout" title="Are you sure you want to log off?" style="display:none; z-index:1150;">
          
            <!--- Only offer if MFA is turned on AND phone has been specified --->
            <cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
                <p style="padding:25px;"></BR> <input type='checkbox' id='DALD' name='DALD' style="width:20px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px;  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);  margin-bottom: 8px; padding: 3px 8px;"> Deauthorize this device? May require MFA code at next log in.</p>
            </cfif>
            
            <div class="submit" style="padding:5px 25px; float:right; ">
                <a id="ConfirmLogOffButton" class="bluelogin small" href="##" style="margin-left:15px; margin-bottom:15px; color:##fff; font-size: 14px;">Yes, Log Off Now</a>
                <a id="CancelLogOffButton" class="bluelogin small" href="##" style="margin-left:15px; color:##fff; font-size: 14px;">Cancel</a>
            </div>
        
        </div> 
       
       	<div id="SignInContainer" class="awesome" style="z-index:1500;"><cfinclude template="signin.cfm"></div>
                
	</div>
    

<!-- banner -->
<!-- about -->
	<div id="about" class="about">
      
        <div>
       		<img src="images/bannerbottom.png" alt=" " width="100%" />
        </div>

      
        <div class="container">
            <div class="about-left">
                <h1>ABOUT <span>US</span></h1>
                <p>What We Do With Love</p>
                
                
            </div>
            <div class="about-right">
                <p><label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In a world demanding rich communications anywhere anytime on any device, our products enable fully interactive messaging and business communication for mobile and fixed access. At Sire, we provide mobile marketing tools and solutions that are designed for you to spend less time coding and more time finding creative ways to serve your customers. With our online Interactive Campaign Builder tools, Templates, or even using our APIs, everything is here that you need to get up and running fast.
                <BR/>
                <BR/>
                <em>Over 1 Billion Served!</em>
                <BR/>
                <BR/>
                Over the last 15 years our proven technology has been fully vetted in real world applications. Now we are making our experience and infrastructure available to you.
                <BR/>
                <div>
    	            <div style="float:left; margin-left:25px;"><img id="BannerPhoneIcon" src="images/30547034_s.jpg" alt=" "  width="450" height="276"/></div>
	                <div style='float:left; margin: 125px 0 0 25px; color: #666; font-family: "Lato",sans-serif; font-size: 18px; font-style: italic;'>Any Device - Any Time</div>
                </div>
    </label></p>
                
               <!--- <div>
                	<div style="float:right;"><div style="clear:both"></div><div class="bubble me">Hello!</div></div>--->
                           
            		
                
              <!---  </div>--->
                
               <!--- <div style="clear:both"></div><div class="bubble me">Hello!</div>
                <div style="clear:both"></div><div class="bubble you">SMS, Push and Web.<BR/>Mobile Surveys, Keyword Response,<BR/>Data Capture, Time Machine<BR/>and more...</div>
                    --->
                        
            </div>
                        
            <div class="clearfix"> </div>
                    
        </div>
	</div>
<!-- //about -->

 <!---   
    <!--- Sample app image  --->
<section class="module parallax parallax-3 foucload">
    <div class="containerpara">
    
		
	</div>
</section>


    <div class="clearfix"> </div>
 --->
    
    <div class="clearfix"> </div>
     
    <!-- our-services -->

	<div id="products" class="our-services foucload">
        <div class="container">
            <div class="our-services-left">
                <h1>OUR <span>PRODUCTS</span></h1>
                <p>What We Do Better</p>
                    
            </div>
            <div class="our-services-right">
                <p><label> Organizations are undergoing a seismic shift in how they communicate and deliver services to customers. Our products are engineered to help you reach them quickly in today's social, mobile, data-driven world with applications and services that are delivered at the speed of now. <!---We see being great at something as a starting point, not an endpoint.  Through continuous innovation and iteration, we take the things that work well and improve upon them in unexpected ways.---> <span></span></label></p>
                
            </div>
            <div class="clearfix"> </div>
            <BR/>
            <div class="our-services-grids">
                <div class="our-services-grid our-services-item our-services-grid-a our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/siresms</cfoutput>">
                    <div class="figure5 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                   	 <h3>SIRE SMS</h3>
                        <p>Simple OR Complex - <em>Compliant</em> SMS campaigns launched in minutes using shared pre-approved short codes. No code required.<span>
                        </span></p>                                                
                    </div>
                   
                    <div class="clearfix"> </div>
                </div>
                <div class="our-services-grid our-services-item our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/siredata</cfoutput>">
                    <div class="figure6 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                        <h3>Data</h3>
                        <p>Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With SIRE's Custom Data Fields, you don't have too. <span></span></p>
                    </div>
                    <div class="clearfix"> </div>
                  
                </div>
                <div class="clearfix"> </div>
                <div class="our-services-grid our-services-item our-services-grid-a our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/siremobileapps</cfoutput>">
                    <div class="figure7 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                        <h3>Mobile Applications</h3>
                        <p>Add interactive response to your App with Sire system.<span>
                        Save time on Design, Code, Database, Connectivity with added flexibility. <!---of beign able to change your app without---> </span></p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="our-services-grid our-services-item our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/siredashboards</cfoutput>">
                    <div class="figure9 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                        <h3>Reporting and Analytics</h3>
                        <p>High tech can be complex. Clarify your campaigns and data with reporting and analytics dashboards.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                
                <div id="our-services-api" class="our-services-grid our-services-item our-services-grid-a our-services-grid-bottom our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/simpleapi</cfoutput>">
                    <div class="figure8 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                        <h3>API</h3>
                        <p>The Simple API is a really easy way to add Smart Interactive Messaging to your App.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="our-services-grid our-services-item our-services-grid-bottom our-services-grid-link" rel1="<cfoutput>#rootUrl#/pub/sirecampaign</cfoutput>">
                    <div class="figure10 figure">
                    </div>
                    <div class="our-services-grid-text bubble me">
                        <h3>Tools for Campaign Management and Design</h3>
                        <p>Sleek easy to use application for account and campaign creation, management, and analytics.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>            
                
                
                <div class="clearfix"> </div>
            </div>
        </div>
	</div>
    <!-- our-services -->
    

<div style="clear:both;"></div>
    
<!--- Over the top demo ---> 
<div id="demoOTT" class=" foucload">

    <!---<div>
        <img src="images/bannerbottom.png" alt=" " width="100%" />
    </div>--->
    
    <div class="container">    
        <div class="plans-pricing">
            <div class="pricing-left">
                <h1>DEMO <span> INTERACTIONS</span></h1>
                <p>Over the Top</p>
                        
            </div>
            <div class="pricing-right">
                <p><label>SMS, Push, or Web Smart Interactive Response Engine</label></p>
                    
            </div>
            <div class="clearfix"> </div>
        </div>
	

	<cfset SA = 0 />
	<cfinclude template="demoire.cfm" />

	</div>

</div>
<!--- Over the top demo ---> 

  
    <cfinclude template="pricesection.cfm" />
    
     
    <div class="testimonials-top foucload">
<!--- Slider-starts-Here --->
			
             <script>
                
                $(function () {
                  // Slideshow 4
                  $("#slider2").responsiveSlides({
                    auto: true,
                    pager: false,
                    pause: true,
                    nav: false,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                      $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                      $('.events').append("<li>after event fired.</li>");
                    }
                  });
				  
				  <!--- BUG FIX IN - responsiveSlides with more than 9 slides get double char width - looks bad--->
				  $(".callbacks2_tabs a").html("0");
				             
                });
              </script>
        <!--//End-slider-script -->
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider2">
                
                <li>
						<div class="quote2">
							<div class="quote-fig">
								<img src="images/q.png" alt=" " />
							</div>
							<div class="quote-info">
								<p>DISRUPTIVE INNOVATION<BR/>can hurt, if you are not the one<BR>
                                 DOING THE DISRUPTING
									</p>
							</div>
							<div class="quote-link">
								<p><a href="http://www.amazon.com/Innovators-Dilemma-Revolutionary-Change-Business/dp/0062060244/ref=sr_1_1?s=books&ie=UTF8&qid=1424999787&sr=1-1&keywords=clay+christensen" target="_new">Clay Christensen</a></p>
							</div>
						</div>
					</li>
					<li>
						<div class="quote2">
							<div class="quote-fig">
								<img src="images/q.png" alt=" " />
							</div>
							<div class="quote-info">
								<p>In marketing you must choose between boredom, shouting and seduction. 
									<span>Which do you want?
										</span></p>
							</div>
							<div class="quote-link">
								<p><a href="http://www.amazon.com/Word-Mouse-Trends-Sell-Learn/dp/1451668406/ref=sr_1_3?s=books&ie=UTF8&qid=1424836254&sr=1-3&keywords=Marc+Ostrofsky" target="_new">Marc Ostrofsky</a></p>
							</div>
						</div>
					</li>
					<li>
						<div class="quote2">
							<div class="quote-fig">
								<img src="images/q.png" alt=" " />
							</div>
							<div class="quote-info">
								<p>Permission marketing turns strangers into friends and friends into loyal customers. It's not just about entertainment - it's about education.
								<span></span></p>
							</div>
							<div class="quote-link">
								<p><a href="http://www.amazon.com/Permission-Marketing-Turning-Strangers-Customers/dp/0684856360/ref=la_B000AP9EH0_1_6?s=books&ie=UTF8&qid=1424836065&sr=1-6" target="_new">Seth Godin</a></p>
							</div>
						</div>
					</li>
           
            </ul>
        </div>
	</div>
    

 <cfinclude template="contactsection.cfm" />


<!--- footer-top --->
	<div class="footer-top">
	<div class="container">
		<ul>
		<!---	<li><a href="#" class="facebook"> </a></li>
			<li><a href="#" class="twitter"> </a></li>
			<li><a href="#" class="in"> </a></li>
			<li><a href="#" class="p"> </a></li>
			<li><a href="#" class="google"> </a></li>
			<li><a href="#" class="dribble"> </a></li>
			<li><a href="#" class="in1"> </a></li>
		--->
		</ul>
	</div>
	</div>
<!--- //footer-top --->
<!--- footer --->
	<div class="footer">
	<div class="container">
		<p> &copy; 2015 <font style="color:#ffffff; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font> All rights reserved.</p>
	</div>
	</div>
<!--- footer --->
<!--- here stars scrolling icon --->
	<script type="text/javascript">
	 $(function() 
	{
		/*
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		*/
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
		$('.foucload').show();
		
		
		
	});
	</script>
<!--- //here ends scrolling icon --->
</body>
</html>


