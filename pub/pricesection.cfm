<div class="clearfix"> </div>

<!--- plans-pricing --->
        <div class="pricing foucload" id="pricing">
        
        <div>
       		<img src="images/bannerbottom.png" alt=" " width="100%" />
        </div>
        
        <!--- Pete townsend quote - I call that a Bargain - the best I ever hadddddddddd! --->
        <div class="container">
            <div class="plans-pricing">
                <div class="pricing-left">
                    <h1>PLANS & <span> PRICING</span></h1>
                    <p>Choose your plan</p>
                            
                </div>
                <div class="pricing-right">
                    <p><label> Break free with fast, simple tools and services to help your business with mobile messaging.</label></p>
                        
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="basic-plan">
                <div class="basic">
                    <div class="value">
                        <h4>PRO</h4>
                        <p><label>$</label> <span>25 </span>/ mo</p>
                    </div>
                    <ul>
                        <li>1 User Account</li>
                        <li>Application Control Only</li>
                        <li>Shared Shortcode Access</li>
                        <li>5 Keywords</li>
                        <li>Reports, Exports and Dashboards</li>
                        <li>First 1000 SMS Included</li>
                        <li>1.9 Cents per Msg after</li>
                    </ul>
                    <div class="started">
                        <a href="#">ORDER NOW</a>
                    </div>
                    <div class="para">
                        <p>Click for Details</p>
                    </div>
                </div>
                <div class="basic">
                    <div class="value">
                        <h4>SMB</h4>
                        <p><label>$</label> <span>250 </span>/ mo</p>
                    </div>
                    <ul>
                        <li>1 User Account</li>
                        <li>Application Control Only</li>
                        <li>Shared Shortcode Access</li>
                        <li>25 Keywords</li>
                        <li>Reports, Exports and Dashboards</li>
                        <li>First 10,000 SMS Included</li>
                        <li>1.5 Cents per Msg after</li>
                    </ul>
                    <div class="started">
                        <a href="#">ORDER NOW</a>
                    </div>
                    <div class="para">
                        <p>Click for Details</p>
                    </div>
                </div>
                
                <div class="NewlineInFloats"></div>
                
                <div class="basic">
                    <div class="value">
                        <h4>ENTERPRISE</h4>
                        <p><label>$</label><span>1000</span>/ mo</p>
                    </div>
                    <ul>
                        <li>1 Users</li>
                        <li>Application or API Control</li>
                        <li>Reports, Exports and Dashboards</li>
                        <li>Dedicated Short Code</li>
                        <li>Unlimited Keywords</li>
                        <li>First 50,000 SMS Included</li>
                        <li>0.9 Cents per Msg after</li>
                    </ul>
                    <div class="started">
                        <a href="#">ORDER NOW</a>
                    </div>
                    <div class="para">
                        <p>Click for Details</p>
                    </div>
                </div>
                <div class="basic">
                    <div class="value">
                        <h4>WHOLESALE</h4>
                        <p><label>$</label> <span>Call</span></p>
                    </div>
                    <ul>
                        <li>Unlimited User Accounts</li>
                        <li>Application or API Control</li>
                        <li>Reports, Exports and Dashboards</li>
                        <li>Dedicated Short Codes</li>
                        <li>500,000+ Msg per month</li>
                        <li>Volume Discounts</li>
                        <li>Tiered Pricing</li>
                    </ul>
                    <div class="started">
                        <a href="#">ORDER NOW</a>
                    </div>
                    <div class="para">
                        <p>Click for Details</p>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
    
        </div>
	</div>
    <!--- plans-pricing --->