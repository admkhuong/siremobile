<!---<cfinclude template="header.cfm">


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
		<title><cfoutput>#BrandFull#</cfoutput> - Sign In</title>
		
	</head>
	<body>--->
    
    <!--- Make code work so multiple log ins can exist on same page - for samples and sign up sections--->
    <cfparam name="SIID" default="0"/>
    
    <cfoutput>    
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
        
        <style TYPE="text/css">
            @import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css') all;
        </style>
	</cfoutput>

    		
    
		
			<script type="text/javascript" language="javascript">
				
				$(function() {		 
			 					   
				   	$("#<cfoutput>LoginLink_#SIID#</cfoutput>").click(function(){
  						  <cfoutput>doLogin_#SIID#</cfoutput>();
  					});
					
					$("#<cfoutput>LoginLinkMFA_#SIID#</cfoutput>").click(function(){
  						  <cfoutput>doLoginMFA_#SIID#</cfoutput>();
  					});
  							
				});
									
			
				function <cfoutput>doLogin_#SIID#</cfoutput>(){
					var isValid = true;
					if(!isValidRequiredField('<cfoutput>inpUserID_#SIID#</cfoutput>')){
						isValid = false;
					}
					if(!isValidRequiredField('<cfoutput>inpPassword_#SIID#</cfoutput>')){
						isValid = false;
					}
					if(!isValid){
						return false;
					}else{
						var userID = $("#<cfoutput>inpUserID_#SIID#</cfoutput>").val();
						var userPass = $("#<cfoutput>inpPassword_#SIID#</cfoutput>").val();
						// do login action
						try{
							<!---var RememberMeLocal = 0;
							if( $('#EBMSignInSection #inpRememberMe').is(':checked'))
								RememberMeLocal = 1;--->
								
								
							$.ajax({
							   type: "POST",
							   
							   <!--- HTTS for SSL --->
							   url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
							  							  
							   data:{
									inpUserID : userID, 
									inpPassword : encodeURI(userPass), 
									inpRememberMe : 1
							   },
							   dataType: "json", 
							   success: function(d) {
								  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.RXRESULTCODE) != "undefined")
									{					
										CurrRXResultCode = d.RXRESULTCODE;	
										
										<!---console.log(d);
																		
										alert('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home')--->
										
										if(CurrRXResultCode > 0)
										{
											<!---$.cookie("VOICEANSWERCOOKIE", null, { path : '/' })--->
											
											<!--- Check for MFA Requirement --->
											
											if(parseInt(d.MFAREQ) == 1)
											{
												if(d.LAST4.length == 4)
													$("#<cfoutput>LastFour_#SIID#</cfoutput>").html('Phone ******' + d.LAST4);
												
												<!--- Switch to second level of authentication --->	
												$(".LoginContainerMask").toggle();
												$(".MFAContainerMask").toggle();
												
												return false;												
											}
											
											<!--- Navigate tp sesion Home --->
											window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
										}
										else if(parseInt(CurrRXResultCode) == -2 )
										{
											jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );													
										}
										else
										{
											<!--- Unsuccessful Login --->
											<!--- Check if variable is part of JSON result string   d.CCDXMLString  --->								
											if(typeof(d.REASONMSG) != "undefined")
											{	 
												jAlert("Error while trying to login.\n", d.REASONMSG, function(result) { } );													
											}		
											else
											{
												jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );
											}
											
										}
									}
									else
									{<!--- Invalid structure returned --->														
										jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );
									}
									
							   }
						  });
						}
						catch(ex)
						{							
							jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );																		
						}
					}
				}
							
				
				<!--- Auto click Login button when press Enter key --->
				function <cfoutput>submitenter_#SIID#</cfoutput>(myfield,e) {
					var keycode;
					if (window.event) keycode = window.event.keyCode;
					else if (e) keycode = e.which;
					else return true;
									
					<!--- If the parent container is visible react to enter key press as login request  && $('#EBMSignInSection').parent().css("display") != "none"--->
					if (keycode == 13)
					{												
						<cfoutput>doLogin_#SIID#</cfoutput>();
						return false;
					}
					else
						return true;
				}
				
				<!--- Auto click Login button when press Enter key --->
				function <cfoutput>submitentermfa_#SIID#</cfoutput>(myfield,e) {
					var keycode;
					if (window.event) keycode = window.event.keyCode;
					else if (e) keycode = e.which;
					else return true;
									
					<!--- If the parent container is visible react to enter key press as login request  && $('#EBMSignInSection').parent().css("display") != "none"--->
					if (keycode == 13)
					{												
						<cfoutput>doLoginMFA_#SIID#</cfoutput>();
						return false;
					}
					else
						return true;
				}
				
	
				function <cfoutput>doLoginMFA_#SIID#</cfoutput>(){
					var isValid = true;
					if(!isValidRequiredField('<cfoutput>inpMFAID_#SIID#</cfoutput>')){
						isValid = false;
					}
					
					if(!isValid){
						return false;
					}else{
						var MFACode = $("#<cfoutput>inpMFAID_#SIID#</cfoutput>").val();
						
						try{
															
							$.ajax({
							   type: "POST",
							   <!--- HTTS for SSL --->
							   url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateMFA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
							   data:
							   {
									inpMFACode : $("#<cfoutput>inpMFAID_#SIID#</cfoutput>").val()
							   },
							   dataType: "json", 
							   success: function(d) {
								  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.RXRESULTCODE) != "undefined")
									{					
										CurrRXResultCode = d.RXRESULTCODE;	
										
										<!---console.log(d);
																		
										alert('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home')--->
										
										if(CurrRXResultCode > 0)
										{
											$.cookie("VOICEANSWERCOOKIE", null, { path : '/' })
											
											<!--- Check for MFA OK  --->
											
											// jAlert("testing", "MFA OK Result = " + d.MFAOK, function(result) { } );													
											
											if(parseInt(d.MFAOK) > 0 )
												<!--- Navigate tp sesion Home --->
												window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
											else
												jAlert("Error while trying to authenticate device.\n", d.REASONMSG, function(result) { } );	
										}
										else if(parseInt(CurrRXResultCode) == -2 )
										{
											jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );													
										}
										else
										{
											<!--- Unsuccessful Login --->
											<!--- Check if variable is part of JSON result string   d.CCDXMLString  --->								
											if(typeof(d.REASONMSG) != "undefined")
											{	 
												jAlert("Error while trying to authenticate device.\n", d.REASONMSG, function(result) { } );													
											}		
											else
											{
												jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );
											}
											
										}
									}
									else
									{<!--- Invalid structure returned --->														
										jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );
									}
									
							   }
						  });
						}
						catch(ex)
						{							
							jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );																		
						}
					}
				}
				
				
				function requestnewcode()
				{
					<!--- Switch to second level of authentication --->	
					$(".LoginContainerMask").toggle();
					$(".MFAContainerMask").toggle();					
				}
				
			</script>
	
     
     
             
               			
        <div id="EBMSignInSection<cfoutput>_#SIID#</cfoutput>" class="loginform cf"> 
        	
            <cfif SIID EQ 0>
	        	<a id="login_close" class="login_close">Close</a>
            </cfif>
             
            <div class="LoginHeader"><span>Log in to</span> <cfoutput>#BrandShort#</cfoutput> <span class="MFAContainerMask" style="display:none;">- Step 2</span></div>
             
            <form name="login" action="index_submit" autocomplete="on">
                <input id="inpSigninMethod" type="hidden" name="method" value="Signin" />
                
                <div>
                
                
                	<div class="LoginContainerMask">
                        <ul>
                            <li>
                                <label for="inpSigninUserID">Email</label>
                                <input type="email" id="<cfoutput>inpUserID_#SIID#</cfoutput>" name="<cfoutput>inpUserID_#SIID#</cfoutput>" placeholder="yourname@email.com" required onKeyPress="return <cfoutput>submitenter_#SIID#</cfoutput>(this,event)" autocomplete="on">
                            </li>
                            
                            <li>
                                <label for="inpSigninPassword">Password</label>
                                <input type="password" placeholder="password" required id="<cfoutput>inpPassword_#SIID#</cfoutput>" name="<cfoutput>inpPassword_#SIID#</cfoutput>" onKeyPress="return <cfoutput>submitenter_#SIID#</cfoutput>(this,event)">
                            </li>
                            
                            <!---
                             <li>
                                <label for="inpSigninPassword">Remember Me</label>
                                <input type="checkbox" required id="inpRememberMe" name="inpRememberMe">
                            </li>
                            --->
                            
                                                                            
                        </ul>
                    </div>
                    
                    <div class="MFAContainerMask" style="display:none;">
                    
                    	
                        
                		<ul>
                        	<li style="width:250px; padding-bottom:8px;">
                            	<h4 style="font-size:12px;">This account requires a second code to authorize each device. This code was sent to the phone number specified in your user settings.&nbsp;<span id="<cfoutput>LastFour_#SIID#</cfoutput>"></span></h4>
                            </li>
                        
                            <li>
                                <label for="inpMFAID">Authentication Code for This Device</label>
                                <input type="text" id="<cfoutput>inpMFAID_#SIID#</cfoutput>" name="<cfoutput>inpMFAID_#SIID#</cfoutput>" placeholder="Enter the code sent to you here" required onKeyPress="return <cfoutput>submitentermfa_#SIID#</cfoutput>(this,event)" autocomplete="on" style="width:250px;">
                            </li>                            
                         </ul>                
                	</div>
                
                
                </div>
                
                <div class="LoginContainerMask">
                    <div class="RememberMe"><!---Remember Me<input type="checkbox" required id="inpRememberMe" name="inpRememberMe" style="background:transparent;border:0">---> 
							<a href="forgotpassword">Forgot password?</a>
                        	<a href="javascript:;" class="loginbutton bluelogin small" id="<cfoutput>LoginLink_#SIID#</cfoutput>">Login</a>
                                      
                    </div>  
               	
                	<div id="signnew" class="signnew" align="center"><a href="signup">Not a user yet? Create your account here.</a> </div>               
                
                </div>
               
                                   
                <div class="MFAContainerMask" style="display:none;">
                	<div class="RememberMe">
                    <!--- Keep centered if included in middle of a page somewhere - dont apply to home pages--->                        
                        
                        <a href="javascript:;" class="loginbutton bluelogin small" id="<cfoutput>LoginLinkMFA_#SIID#</cfoutput>">Authorize</a>
                        
    				</div>
                    
				    <div id="RequestNewMFACode" class="signnew" align="center"><a href="javascript:;requestnewcode()">Need a new code? Request a new code by logging in again here.</a> </div>               
                
                
                </div>
                                 
            </form>
                      
           </div>
          
         
    <!---      
	</body>
</html>--->