<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<cfinclude template="../public/paths.cfm" >
<cfinclude template="../public/header.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

<title>Sign Up - <cfoutput>#BrandShort#</cfoutput></title>
		
		<cfoutput>
			<style type="text/css">
				<!---@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxform2.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/MB.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');--->
				@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
			</style>
		   <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
			<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>--->
		</cfoutput>
	    
        
        
<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }
	
	body
	{
		padding:0;
		border:none;
		height:100%;
		width:100%;
		min-width:1200px;	
		background: rgb(51,182,234); 
		background-size: 100% 100%;
		background-attachment: fixed; 	
		background-repeat:no-repeat;
	}

</style>

    <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					<!---$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}--->
					
					var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
					$img.bind( 'load', function()
					{
						$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
					});
					
					
					if( $img[0].width ){ $img.trigger( 'load' ); }
								 
					<!--- Preload images --->
					$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
												 
					$(window).scroll(function(){
						$('*[class^="prlx"]').each(function(r){
							var pos = $(this).offset().top;
							var scrolled = $(window).scrollTop();
							<!---$('*[class^="prlx"]').css('top', -(scrolled * 0.6) + 'px');		--->
							$('.prlx-1').css('top', -(scrolled * 0.6) + 'px');		
								
						});
					});
						
						
					$("#PreLoadIcon").fadeOut(); 
					$("#PreLoadMask").delay(350).fadeOut("slow");
		
				});
				
				<!--- Image preloader --->
				$.preloadImages = function() 
				{
					for (var i = 0; i < arguments.length; i++) 
					{
						$("<img />").attr("src", arguments[i]);				
					}
				}


				function isValidRequiredField(fieldId){
					
					if($("#" + fieldId).val() == ''){
						$("#err_" + fieldId).show();
						return false;
					}else{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				function isValidEmail(){
					var isValid = true;
					if($("#emailadd").val() == ''){
						isValid = false;
						$("#err_emailadd").show();
					}else{
						var filter = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if (!filter.test($("#emailadd").val())) {
							isValid = false;
							$("#err_emailadd").show();
						}else{
							$("#err_emailadd").hide();
						}
					}
					return isValid;
				}
				
				
				
				<!---// validate pass and confirm pass--->
				function isValidPassword(){
					var isvalid = true;
					$("#validateno1").css("color","#424242");
					$("#validateno2").css("color","#424242");
					$("#validateno3").css("color","#424242");
					$("#validateno4").css("color","#424242");
					$("#validateno5").css("color","#424242");
					$("#err_inpConfirmPassword2").css("display","none");
					
					if ($("#inpPasswordSignup").val().length < 8)
					{
						
						//jAlert("Password Validation Failure", 'Password must be at least 8 characters in length');
						$("#validateno1").css("color","red");
						isvalid = false;
					 					  
					}
					  
					var hasUpperCase = /[A-Z]/.test($("#inpPasswordSignup").val());
					if (!hasUpperCase)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 uppercase letter');
						$("#validateno3").css("color","red");
						isvalid = false;
					 					  
					}
										
					var hasLowerCase = /[a-z]/.test($("#inpPasswordSignup").val());
					if (!hasLowerCase)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 lowercase letter');
						$("#validateno4").css("color","red");
						isvalid = false;
					 					  
					}
					
					var hasNumbers = /\d/.test($("#inpPasswordSignup").val());
					if (!hasNumbers)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 number');
						$("#validateno2").css("color","red");
						isvalid = false;
					 					  
					}
										
					var hasNonalphas = /\W/.test($("#inpPasswordSignup").val());
					if (!hasNonalphas)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 special character');
						$("#validateno5").css("color","red");
						isvalid = false;
					 					  
					}
					
					
					if(!isValidRequiredField('inpPasswordSignup')){
		  				isvalid = false;
		  			}
		  			if(!isValidRequiredField('inpConfirmPassword')){
		  				isvalid = false;
		  			}
		  			if($("#inpPasswordSignup").val() != $("#inpConfirmPassword").val()){
						$("#err_inpConfirmPassword2").css("display","block");
		  				isvalid = false;
		  			}
					return isvalid;
				}
				<!---// validate form--->
				function isValidSignupForm(){
					var isValidForm = true;
					<!---<!---// account type--->
					if(!isValidAccountType()){
						isValidForm = false;
					}--->
					<!---// name--->
					if(!isValidRequiredField('fname')){
		  				isValidForm = false;
		  			}
					if(!isValidRequiredField('lname')){
		  				isValidForm = false;
		  			}
		  			<!---// email--->
		  			if(!isValidEmail()){
		  				isValidForm = false;
		  			}
		  			<!---// password--->
		  			if(!isValidPassword()){
		  				isValidForm = false;
		  			}
					return isValidForm;
				}
				<!---// Signup--->
				function signUp(){
					
					$('#SignUpButton').hide();			
					$("#loadingSignUp").css('visibility', 'visible');
			
					
					if(!isValidPassword())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					
					if(!isValidSignupForm())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					else
					{
						<!---// input--->
						var accountType = 'personal';
						var companyName = '';
						
						
						<!---	if($('#account_type_company').is(':checked')){
							accountType = 'company';
							companyName = $("#company_name").val();
						}else{
							accountType = 'personal';
						}--->
						
						
						var fname = $("#fname").val();
						var lname = $("#lname").val();
						var userEmail = $("#emailadd").val();
						
						var pass = $("#inpPasswordSignup").val();
						var confirmPass = $("#inpConfirmPassword").val();
						try{
							$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							data:  {
								INPMAINEMAIL: userEmail,
								INPCOMPANYNAME: companyName,
								INPTIMEZONE: 31,
								INPACCOUNTTYPE: accountType,
								INPNAPASSWORD: pass,
								INPFNAME: fname,
								INPLNAME: lname,
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) { $('#SignUpButton').show(); $("#loadingSignUp").css('visibility', 'hidden');},					  
							success:		
								function(d) 
								{
									if(d.RESULT != "SUCCESS"){
										jAlert(d.MESSAGE, 'Error');
										$('#SignUpButton').show();			
										$("#loadingSignUp").css('visibility', 'hidden');
										return false;
									}else{
										<!---// sign up successfull - go to the administrator page--->
										doLoginSignUp();
										<!---//$("#signup_box").html('');
										//var loginLink = '<a href="<cfoutput>#rootUrl#/#PublicPath#/home</cfoutput>">here</a>';
										//$("#signup_box").append('<h2 class="title message">Register successfully! Click ' + loginLink + ' to go login page.</h2>');--->
									}
								} 		
								
							});
						}catch(ex){
							jAlert('Signup Fail', 'Error');
							$('#SignUpButton').show();			
							$("#loadingSignUp").css('visibility', 'hidden');
							return false;
						}
					}
					return true;
				}
				
				function doLoginSignUp(){
					
						var userID = $("#emailadd").val();
						var userPass = $("#inpPasswordSignup").val();
						<!---// do login action--->
						try{
							var RememberMeLocal = 0;
							$.ajax({
						       type: "POST",
						       url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
						       data:{
						       		inpUserID : userID, 
									inpPassword : userPass, 
									inpRememberMe : RememberMeLocal
						       },
						       dataType: "json", 
						       success: function(d) {
						    	  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.RXRESULTCODE) != "undefined")
										{					
											CurrRXResultCode = d.RXRESULTCODE;	
											
											if(CurrRXResultCode > 0)
											{
												<!--- Navigate tp sesion Home --->
												window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home.cfm";
											}
											else if(parseInt(CurrRXResultCode) == -2 )
											{
												jAlertOK("Error while trying to login.\n" + d.REASONMSG , "Failure!", function(result) { <!---window.location.href="verifyEmail.cfm?email="+$("#frmLogon #inpUserID").val(); return false;---> } );												
											}
											else
											{
												<!--- Unsuccessful Login --->
												<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
												if(typeof(d.DATA.REASONMSG[0]) != "undefined")
												{	 
													jAlertOK("Error while trying to login.\n" + d.REASONMSG , "Failure!");
												}		
												else
												{
													jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
												}
												
											}
										}
										else
										{<!--- Invalid structure returned --->	
											
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
								
										}
									
						       }
						  });
						}catch(ex){
							jAlertOK('Have problem while login to system.', 'Warning');
						}
					}
			</script>
</head>
            
    <cfoutput>
	    <body>
    		<div id="main" align="center"> 
        
			<!---	<!--- EBM site footer included here --->
                <cfinclude template="act_ebmsiteheader.cfm">--->
                    
                 <div id="bannersection">
                    <div id="content" style="position:relative;">
                    
                    	<img style="margin:50px;" src="#rootUrl#/#publicPath#/images/m1/signupnote.png" height="200" width="212" />
                        
                       <!--- <div class="inner-main-hd" style="width:400px; margin-top:30px;">Sign up</div>
                        
                        <div style="clear:both;"></div>
                        
                        <div class="header-content">
                            ....          	                
                        </div>--->
                        
                     
                     
                    </div>
                </div>
          
        		<div id="inner-bg-m1" style="min-height: 450px">
        
					<div class="contentm1" id="signup_box">
						<form action="" name="sign_up" method="post" id="sign_up">
							<!---<div class='inp_box'>
								<label class='main_title title'><h2><span>1. Select Account Type</span></h2></label>
								<input name="account_type" id="account_type_personal" type="radio" 
										value="personal" checked="true" onchange="changeType('personal');">
								<span>Personal</span>
								<input name="account_type" id="account_type_company" type="radio" 
										value="company" onchange="changeType('company');">
								<span>Company</span>
								<label class='error' id='err_type'>You must choose one type of account.</label>
							</div>--->
                            
						<!---	<div class='inp_box hidden' id='company_box'>
								<label class='title'><span>Company Name</span></label><br />
								<input name="company_name" id="company_name" type="text" class="textfield half"><br />
								<label class='error' id='err_company_name'>This field is required.</label>
							</div>
						--->
                        
                        
							<div class='inp_box'>
								<label class='main_title title'><h2><span>Create <cfoutput>#BrandShort#</cfoutput> Account</span></h2></label>
								<div class="action_box">
									<a href="home.cfm"><span>or click here if you already have an existing account.</span></a>
								</div>
								<div class='inp_box'>
									<div class='half'>
										<label class='title'><span>First Name</span></label><br />
										<input name="fname" id="fname" type="text" 
												class="textfield"><br />
										<label class='error' id='err_fname'>This field is required.</label>
									</div>
									<div class='half padd-left'>
										<label class='title'><span>Last Name</span></label>
										<input name="lname" id="lname" type="text" 
												class="textfield">
										<label class='error' id='err_lname'>This field is required.</label>
									</div>
								</div>
								<div class='inp_box'>
								<label class='title'><span>Email Address</span></label><br />
								<input name="emailadd" id="emailadd" type="text" 
										class="textfield full"><br />
								<label class='error' id='err_emailadd'>Invalid Email.</label>
								</div>
								
                                <div style="clear:both"></div>
                                                                                              
                                <div style="float:left; width:300px;">                               								
                                    <div class='inp_box' style="margin-top:15px;">
                                        <label class="inTitle">Password</label><br />
                                        <input type="text" name="inpPasswordSignup" id="inpPasswordSignup" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpPassword">This field is required.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;" >
                                        <label class="inTitle">Confirm Password</label><br />
                                        <input type="text" name="inpConfirmPassword" id="inpConfirmPassword" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpConfirmPassword">Invalid confirm password.</label>
										<label class="error" id="err_inpConfirmPassword2">Two passwords you typed do not match.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;">
                                        <span id="SignUpButton"> <button type="button" class="ui-corner-all" onClick="signUp(); return false;">Sign up</button></span>
                                        <div id="loadingSignUp" style="float:right; display:inline; visibility:hidden; margin-right:5px;">Adding New Account...<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                                    </div>
                               	</div>
                                 
                                <div class='inp_box' style="float:left; width:500px; margin-left:30px;">
                                    <h2>Secure Password Requirements</h2>
                                        <ul style="margin-left:25px;">
                                            <li id="validateno1" class='title'>must be at least 8 characters in length</li>
                                            <li id="validateno2" class='title'>must have at least 1 number</li>
                                            <li id="validateno3" class='title'>must have at least 1 uppercase letter</li>
                                            <li id="validateno4" class='title'>must have at least 1 lower case letter</li>
                                            <li id="validateno5" class='title'>must have at least 1 special character</li>
                                        </ul>
                                    <BR />
                                   	<h4>A secure sample of one has been randomly generated for you.
                                   		<BR />
                                   		Feel free to use or replace with your own.  
                                    </h4>
                                   
                              	</div>
                                
                                
                                
							</div>
						</form>
					</div>
				</div>


		<div class="transpacer"></div>

    </div>
    </div>
    </body>
</cfoutput>
</html>


