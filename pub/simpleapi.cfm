








<!---

Up and running in minutes - Quicksart 







API options



User Name and Password
or
Public key and Private Key
or
VPN







--->
<!---
<cfoutput>

<p>http://sire.mobi/webservice/ebm/pdc/GetNextResponse</p>




</cfoutput>--->



<!DOCTYPE html>
<html>
<head>
    <title>Simple API Sample</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <!-- js -->
    <script src="js/jquery.min.js"></script>
  
    
    <!-- //js -->
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="SMS, Push and Web. Mobile Surveys, Keyword Response, Data Capture, Time Machine, Smart, Interactive, Response, Engine" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        $(function() 
        {
        
            $(".scroll").click(function(event){		
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
			
			
			
			$.each($('textarea[data-autoresize]'), function() {
				
				
				$(this).css('height','auto');
				$(this).height(this.scrollHeight);
		
						
			});

            
           <!--- document.getElementById('BannerVideo').addEventListener('playing',onBannerVideoStarted,false);
            document.getElementById('BannerVideo').addEventListener('ended',onBannerVideoEnded,false);--->
			
			
    
        });
        
      
	  <!---  
        function onBannerVideoEnded(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }
        
        function onBannerVideoStarted(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }--->
            
            
            
    </script>
    <!-- start-smoth-scrolling -->

<style>

	.SampleAPICode
	{
		background-color: #0085c8;
		border-radius: 8x;
	    box-shadow: 1px 1px 8px 0 #666;
		display: block;
		padding: 12px;
		color:#FFF;	
	}


	.autoResize
	{		
		width:100%;
		padding:10px;
		-moz-box-sizing: border-box;
    	box-sizing: border-box;
	    resize: none;	
		border-radius: 8x;
	    box-shadow: 1px 1px 8px 0 #666;
			
	}

</style>

</head>
<body>

<!-- banner -->
	    
    	<div id="home" class="bannersmall header-unit" style="">
      
    
        <div class="container container-header">
    
           	<div class="head-logo">
                <a href="home">                
                <p>
                <font style="color:#0085c8; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font>
                <BR>
                <font style="color:#0085c8; font-size:22px; font-weight:400;">Smart</font> <font style="color:#ff9900; font-size:22px; font-weight:400;">Interactive Response Engine</font>
                </p>
                
                </a>
            </div>
   		 
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <ul class="nav1">
                        <li><a href="home" class="">Home</a></li>
                       <!--- <li><a href="#about" class="scroll">About Us</a></li>
                        <li><a href="#products" class="scroll">Products</a></li>
                        <li><a href="#demoOTT" class="scroll">Demo</a></li>--->
                        <li><a href="#api" class="scroll">API</a></li>                        
                    </ul>
                    <script> 
                               $( "span.menu" ).click(function() {
                                 $( "ul.nav1" ).slideToggle( 300, function() {
                                 // Animation complete.
                                  });
                                 });
                            
                    </script>

            </div>
    
            <div class="clearfix"> </div>
    
        </div>
                
	</div>
    
    <!-- about -->
	<div id="about" class="about">
      
        <div>
       		<img src="images/bannerbottom.png" alt=" " width="100%" />
        </div>

      
        <div class="container">
            <div class="about-left">
                <h1>SIMPLE <span>API</span></h1>
                <p>Easy to use API</p>
            </div>
            <div class="about-right">
                <p>
                	<label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Simple API is a really easy way to add Smart Interactive Messaging to your App.
                    <BR/>
                    <BR/>
                    <BR/>
                    <BR/>
                    <BR/>                  
    				</label>
                </p>
            </div>
                        
            <div class="clearfix"> </div>
            
            Simple HTTP Calls/Methods can be used to Create, Update, Delete, or Read information on the server. To use a REST API, your application will make a HTTP Call and parse the response.</p>
                    
            <div class="SampleAPICode">
                
                <h3>Simple Sample</h3>                    
                
                <h3 style="display:inline;">REST:</h3>
                
                <div>http://sire.mobi/webservice/ebm/pdc/getnextresponse?inpContactString=9495551212&inpKeyword=Welcome&inpShortCode=DemoSire&authorization=A0DF875079D6267E05FC:demo.sire
                </div>                
               
            </div>
            
            <div>
            
            	<h4>Response</h4>            
                <textarea data-autoresize class="autoResize" readonly><?xml version="1.0" encoding="utf-8"?> 
<ROOT > 
    <LASTCP>0</LASTCP> 
    <LASTSURVEYRESULTID>308796638</LASTSURVEYRESULTID> 
    <BATCHID>1489</BATCHID> 
    <RESPONSE>Welcome to the <font style="color:#0085c8; font-family: 'Open Sans', sans-serif; font-weight:700;">S</font><font style="color:#ff9900; font-family: 'Open Sans', sans-serif; font-weight:700;">ire</font> interactive response engine demo. Try some of the sample interactions...</RESPONSE> 
    <RESPONSELENGTH>274</RESPONSELENGTH> 
    <RESPONSETYPE>TRAILER</RESPONSETYPE> 
    <LASTRQ>1</LASTRQ> 
</ROOT>
                </textarea>
                  
                  <BR>
                  
                  The response format can be either XML or JSON. If no specific format is request JSON will be returned by default. Most web browsers will request XML by default. 
                  
            </div>
                        
        </div>
	</div>
<!-- //about -->

				<BR/>
                <BR/>
                <BR/>
                <BR/>
                <BR/>
                <BR/>

<!--- footer --->
	<div class="footer">
        <div class="container">
            <p> &copy; 2015 <font style="color:#ffffff; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font> All rights reserved.</p>
        </div>
	</div>
            
    
<!--- footer --->
<!--- here stars scrolling icon --->
	<script type="text/javascript">
	 $(function() 
	{
		/*
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		*/
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
		$('.foucload').show();
		
		
		
	});
	</script>
<!--- //here ends scrolling icon --->
</body>
</html>