








<!---

Up and running in minutes - Quicksart 







API options



User Name and Password
or
Public key and Private Key
or
VPN







--->
<!---
<cfoutput>

<p>http://sire.mobi/webservice/ebm/pdc/GetNextResponse</p>




</cfoutput>--->



<!DOCTYPE html>
<html>
<head>
    <title>SMS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <!-- js -->
    <script src="js/jquery.min.js"></script>
  
    
    <!-- //js -->
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="SMS, Push and Web. Mobile Surveys, Keyword Response, Data Capture, Time Machine, Smart, Interactive, Response, Engine" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript">
        $(function() 
        {
        
            $(".scroll").click(function(event){		
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
            
           <!--- document.getElementById('BannerVideo').addEventListener('playing',onBannerVideoStarted,false);
            document.getElementById('BannerVideo').addEventListener('ended',onBannerVideoEnded,false);--->
			
			
    
        });
        
      
	  <!---  
        function onBannerVideoEnded(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }
        
        function onBannerVideoStarted(e) 
        {
                            
            if(!e) { e = window.event; }
            <!---// What you want to do after the event--->
            <!---$("#overlayheader").toggle();--->
            
        }--->
            
            
            
    </script>
    <!-- start-smoth-scrolling -->

<style>

	.SampleAPICode
	{
		background-color: #0085c8;
		border-radius: 8px;
		display: block;
		padding: 12px;
		color:#FFF;
	}


</style>

</head>
<body>

<!-- banner -->
	    
    	<div id="home" class="bannersmall header-unit" style="">
      
    
        <div class="container container-header">
    
           	<div class="head-logo">
                <a href="home">                
                <p>
                <font style="color:#0085c8; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font>
                <BR>
                <font style="color:#0085c8; font-size:22px; font-weight:400;">Smart</font> <font style="color:#ff9900; font-size:22px; font-weight:400;">Interactive Response Engine</font>
                </p>
                
                </a>
            </div>
   		 
            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt=" " /></span>
                    <ul class="nav1">
                        <li><a href="home" class="">Home</a></li>
                       <!--- <li><a href="#about" class="scroll">About Us</a></li>
                        <li><a href="#products" class="scroll">Products</a></li>
                        <li><a href="#demoOTT" class="scroll">Demo</a></li>--->
                        <li><a href="#siresms" class="scroll">SMS</a></li>                        
                    </ul>
                    <script> 
                               $( "span.menu" ).click(function() {
                                 $( "ul.nav1" ).slideToggle( 300, function() {
                                 // Animation complete.
                                  });
                                 });
                            
                    </script>

            </div>
    
            <div class="clearfix"> </div>
    
        </div>
                
	</div>
    

<div id="siresms" class="about">
      
        <div>
       		<img src="images/bannerbottom.png" alt=" " width="100%" />
        </div>

      
        <div class="container">
            <div class="about-left">
                <h1>SIRE <span>SMS</span></h1>
                <p>Easy to use SMS</p>
                
            </div>
            
           


   <div style="overflow: visible; width:100%; text-align:center;" class="large-12 columns sampleslide">
        <ul class="polaroids large-block-grid-4">
                    
                  
            <li>
                <a title="Add a Keyword" href="javascript: void(0)">
                    <img src="images/s_screen/s_keywordcreation.png" alt=" " />
                </a>
            </li>
            
            <li>
                <a title="Multiple Question Types" href="javascript: void(0)">
                    <img src="images/s_screen/s_singleselection.png" alt=" " />
                </a>
            </li>    
            
            <li>
                <a title="Campaign Builder" href="javascript: void(0)">
                    <img src="images/s_screen/s_interactivecampaignbuilder.png" alt=" "  />
                </a>
            </li> 
            
            <li>
                <a title="Advanced Interval Options" href="javascript: void(0)">
                    <img src="images/s_screen/s_intervalsample.png" alt=" "  />
                </a>
            </li>                         
           
            <div style="clear:both;"></div>
            
            <li>
                <a title="Interactive Campaigns" href="javascript: void(0)">
                    <img src="images/s_screen/s_interactivecampaignon.png" alt=" " />
                </a>
            </li>   
                       
            <li>
                <a title="Custom Data Fields" href="javascript: void(0)">
                    <img src="images/s_screen/s_cdfsamplecp.png" alt=" " />
                </a>
            </li>
           
            <li>
                <a title="Almighty Pie" href="javascript: void(0)">
                    <img src="images/s_screen/s_reportalmightypie.png" alt=" " />
                </a>
            </li>
             
            <li>
                <a title="XML Control Strings&#xa;If really WANT to code" href="javascript: void(0)">
                    <img src="images/s_screen/s_xmlcontrolstring.png" alt=" " />
                </a>
            </li>
            
            <div style="clear:both;"></div>
            
            <li>
                <a title="Branch Logic" href="javascript: void(0)">
                    <img src="images/s_screen/s_branch.png" alt=" " />
                </a>
            </li>
           
            <li>
                <a title="Dashboards" href="javascript: void(0)">
                    <img src="images/s_screen/s_reportdashboardsample.png" alt=" " />
                </a>
            </li>  
            
            <li>
                <a title="QA Tools" href="javascript: void(0)">
                    <img src="images/s_screen/s_qatools.png" alt=" " />
                </a>
            </li> 
            
            
             
      
        </ul>
	</div>
                        
            <div class="clearfix"> </div>
                    
        </div>
	</div>

				<BR/>
                <BR/>
                <BR/>
                <BR/>
                <BR/>
                <BR/>

<!--- footer --->
	<div class="footer">
	<div class="container">
		<p> &copy; 2015 <font style="color:#ffffff; font-size:48px; font-weight:700;">S</font><font style="color:#ff9900; font-size:48px; font-weight:700;">IRE</font> All rights reserved.</p>
	</div>
	</div>
<!--- footer --->
<!--- here stars scrolling icon --->
	<script type="text/javascript">
	 $(function() 
	{
		/*
		var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
		};
		*/
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
		$('.foucload').show();
		
		
		
	});
	</script>
<!--- //here ends scrolling icon --->
</body>
</html>