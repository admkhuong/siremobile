<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - SMS</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>
	 <script src="#rootUrl#/#publicPath#/js/jquery.powerzoom.js"></script>      
</cfoutput>




<style>


</style>
<script type="text/javascript" language="javascript">

	$(document).ready(function() {

		
  	});

</script>

</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
       <!--- *** JLP todo 
		   Add note about VoIP vs TDM and dedicated DSP's
	   ---> 	
                
        <!--- Messaging All Channels --->
       <!--- <div id="inner-bg">
            <div id="content">
               
               <div id="inner-right-img"> <img src="#rootUrl#/#publicPath#/images/hiw/sms_web.png" width="410" height="297" /></div>
               
               <div id="inner-box-left" align="center">
          
                    <div class="inner-main-hd">Short Message Service (SMS) </div>
                    <div class="inner-txt">

                        <p>Short Message Service (SMS) is a text messaging service component of phone, web, or mobile communication systems, using standardized communications protocols that allow the exchange of short text messages between fixed line or mobile phone devices. On December 3, 1992 Neil Papworth, a test engineer for Sema Group, sent the first text message, with the words "Merry Christmas" to Vodafone employee Richard Jarvis, but since mobile phones did not have the capability enabled yet, Jarvis was unable to respond.</p>
                        <BR />
                        <p>In 2012, 9.6 trillion SMS messages were sent. While SMS is still a growing market, traditional SMS are becoming increasingly challenged by alternative messaging services available on smartphones with data connections, especially in Western countries where these services are growing in popularity.</p>
                                                                    	  
            		</div>
            	</div>      
                  
         	</div>--->
           
            <div id="inner-bg-m1" style="padding-top:25px;"></div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                         
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Interactive Campaigns</h2>
                            <p>Session Management, Surveys, Marketing, Trickle, Educational and more...  </p>
                        </header>
                    </div>
                
                    <div class="hiw_Info" style="width:960px;  margin-top:15px; text-align:left;">
                        <h2 class="super">Trickle Campaign Sample</h2>
                        <br/>
                        <h3>
                            <h3>To help MasterCard develop a more engaging SMS campaign for underserved consumers, #BrandShort# has developed the following content to better provide relevant tips and guidance to respondents based on their unique characteristics. The content is designed to better understand who participants are and encouraging action-steps that would best meet their unique situations. Upon completion of the program, MasterCard will also be able to better assess the type of respondents using their program.</h3>
                            <BR />
                            <h3>Using EBM tools, this highly complex 4 week trickle campaign with over 342 control points was designed, built, tested, and launched in less than 2 weeks.</h3>
                        </h3>
                    </div>     
                
                <BR />
                
               	<img style="" src="#rootUrl#/#publicPath#/images/m1/mcprogram.png" />
                
                <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-keyword-simple.png" width="500" height="488" />
                
                <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-ic.png" width="857" height="1550" />
                
              </div>
                        
            </div>  
                     
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                    
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner-wide">
                            <h2>Keyword Management Tools</h2>
                            <p>Keywords, autoresponders, Filter Search, Block Duplicates, Interactive Campaigns </p>
                        </header>
                    </div>
                                        
                   <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-campaignmanager.png" width="927" height="1041" />
                   
                   <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-pending.png" width="728" height="381" />
                   
                   <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-updates.png" width="718" height="381" />
                        
            	</div>
                        
            </div>
                     
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                                            
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Personalization</h2>
                            <p>Power of Customer Experience</p>
                        </header>
                    </div>
                    
                   	<img src="#rootUrl#/#publicPath#/images/m1/personalizationcp2.png" width="750" height="172" />
                   
                   	<img src="#rootUrl#/#publicPath#/images/m1/personalizationcp6.png" width="750" height="172" />
                       
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                                            
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>QA Tools</h2>
                            <p>Online QA, Time Machine, Gateway Console</p>
                        </header>
                    </div>
                    
                    <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-qa.png" width="1070" height="1263" class="page-break" />
                        
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                                            
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Segmentation</h2>
                            <p>Target your campaign efforts</p>
                        </header>
                    </div>
                      
                    <div class="hiw_Info" style="width:800px; margin-bottom:10px; text-align:left;">
                    	
                    	<h3>
                        	<ul>
                                <li>
                                    4 Million Total Loyalty Card Customer Base
                                </li>
                                
                                <li>
                                    1 Million in West Region
                                </li>
                                
                                <li>
                                    200K have not transacted at Store in last 30 days
                                </li>
                                
                                <li>                                
                                    140K Females 18-35
                                </li>
                                
                                <li>
                                    90K are Signed up for Promotional Notifications via CPP for the SMS channel
                                </li>
                                
                                <li>
                                    Those 90K Customers get incentive coupon with a UUID code that is good on their next visit.
                                </li>                                
                            </ul>
                        </h3>
                    </div>
                    
                     <img src="#rootUrl#/#publicPath#/images/m1/segsample3.png" height="880" width="800" />
                                 
                </div>
                
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>User Management Tools</h2>
                            <p>Logging, Admin, etc</p>
                        </header>
                    </div>
                      
                   	<img src="#rootUrl#/#publicPath#/images/m1/usermanagementsmall.png" width="614" height="845" /> 
                        
            	</div>
                        
            </div>
            
           
           <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                    
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Common Short Code (CSC)</h2>
                            <p>What carrier has approved what content for what short codes.</p>
                        </header>
                    </div>
                    
                    <img src="#rootUrl#/#publicPath#/images/m1/sms/sms-ca.png" width="838" height="548" /> 
                        
            	</div>
                        
            </div>
            
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                    
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner-full">
                            <h2>Reporting and Customizable Dashboards</h2>
                            <p>Counts, Analysis, Monitoring and more...</p>
                        </header>
                    </div>
                        
                        <img src="#rootUrl#/#publicPath#/images/m1/dashboard-charts-half.png" width="618" height="353" /> 
                        
                        <img src="#rootUrl#/#publicPath#/images/m1/dashboard-half.png" width="828" height="653" /> 
            	</div>
                        
            </div>
            
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>List Tools</h2>
                            <p>Groups, Custom Data Fields (CDF), Filters, and more...  </p>
                        </header>
                    </div>
                    
                    <img src="#rootUrl#/#publicPath#/images/m1/list-list.png" width="1139" height="600" /> 
                        
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                    
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>CPP Tools</h2>
                            <p>Opt In, Double Opt In, Opt Out, Preference, CDF Capture</p>
                        </header>
                    </div>
                        
            	</div>
                    
                <img width="1055" height="1297" src="#rootUrl#/#publicPath#/images/m1/portaltoolslores.png">
                <BR />                
                <img style="margin-right: 250px;" src="#rootUrl#/#publicPath#/images/m1/cppmobilecombo.png" width="300" height="400" />
                <img style="" src="#rootUrl#/#publicPath#/images/m1/optinsms.png" width="265" height="300" />
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>AB Testing</h2>
                            <p>Find the right message for your audience.</p>
                        </header>
                    </div>
                    
                    <img width="1015" height="845" src="#rootUrl#/#publicPath#/images/m1/tools-ab.png">
                        
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Agent Tools</h2>
                            <p>SMS to Agent Chat Gateway</p>
                        </header>
                    </div>
                    
                    <img width="1134" height="587" src="#rootUrl#/#publicPath#/images/m1/tools-aau-settings.png">
                        
            	</div>
                        
            </div>
            
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>API Access</h2>
                            <p>Fulfill with just one API call</p>
                        </header>
                    </div>
                 
            		<div class="inner-txt-full">
                    	
                        <h3>Call Campaign via REST API from your app (Web, Mobile, or Desktop)</h3>
                        	
                     	<p><i>Updated on Wed, 2014-01-31 11:30</i></p>
						<p>&nbsp;</p>
                        <p>Allows the authenticating users to post a message to the queue for automated delivery.</p>
                        <p>&nbsp;</p>
                        <p>Returns a JSON structure when successful. Returns a string describing the failure condition when unsuccessful.</p>
                        <p>&nbsp;</p>
                        <p>Actions taken in this method are asynchronous and messages will be delivered based on Pre-Defined Campaign's schedule.</p>

                    	<p>&nbsp;</p>
                        <p>&nbsp;</p>
                    	<div>
                    	    <h2>Resource URL Samples</h2>
                    	    <div>http://#EBMAPIPath#/webservice/ebm/pdc/addtoqueue</div>
                            <div>http://#EBMAPIPath#/webservice/ebm/pdc/addtorealtime</div>
                   	    </div>
                        <p>&nbsp;</p>
                    	
                        <div class="field text field-doc-params">
                            <h2>Parameters</h2>
                            <div>
                                <div id="api-param-inpBatchId" class="parameter">
                                    <span class="param">inpBatchId 
                                        <span>required</span>
                                    </span>
                                    <p>The pre-defined campaign Id you created under your account.</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>864512</tt>
                                    </p>
                                </div>
                                <div id="api-param-inpContactString" class="parameter">
                                    <span class="param">inpContactString
                                    <span>required</span>
                                    </span>
                                    <p>The contact string - can be a Phone number , eMail address, or SMS number </p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>(555)812-1212</tt>
                                    OR
                                    <tt>JoeThompson@eventbasedmessaging.com</tt>
                                    </p>
                                    <p></p>
                                </div>
                                <div id="api-param-inpContactTypeId" class="parameter">
                                    <span class="param">inpContactTypeId
                                    <span>required</span>
                                    </span>
                                    <p>1=Phone number, 2=eMail, 3=SMS</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>1</tt>
                                    </p>
                                </div>
                          
                              	
                                <div id="api-param-unlimited" class="parameter">
                          		    <p>Unlimited custom data parameters are accepted via form data. These parameters can be used to control content, branching logic or other message paramters.</p>
                          			<p>&nbsp;</p>
                                    <span class="param">customFormField
                                    <span>optional</span>
                                    </span>
                                    <p>Strings or Numbers - not strongly typed but should evaluate to a number or string</p>
                                    <p>
                                     <strong>Example Values</strong>									
                                    :
                                    <tt>City</tt> OR <tt>Balance</tt> OR <tt>CallerId</tt> OR <tt>whatever your message needs.</tt>
                                    </p>
                                </div>
                            </div>
                            	
                          	<p>&nbsp;</p>
                          
                            <div class="fieldgroup group-example-request">
                                <h2>Example Request</h2>
                                <div>
                                
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST
                                        <span>Required</span>
                                        </span>
                                        <p>#EBMAPIPath#/webservice/ebm/pdc/addtoqueue</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST Data
                                        <span>Required</span>
                                        </span>
                                        <p>inpBatchId=864512&amp;inpContactString=5558121212&amp;inpContactTypeId=1</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Header Data
                                        <span>Required</span>
                                        </span>
                                        <p>type="header" name="Accept" value="application/json"</p>
                                        <p>type="header" name="datetime" value="YYYY-MM-DD HH:MM:SS"</p>
                                        <p>type="header" name="authorization" value="ecrypted(datetime)"</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Custom Data
                                        <span>Optional</span>
                                        </span>
                                        <p>type="form" name="inpCompany" value="WidgeCo"</p>
                                        <p>type="form" name="inpCity" value="Corona"</p>
                                        <p>type="form" name="inpState" value="CA"</p>
                                     </div>
                             
                            	</div>
                  	  		</div>
                            
                            
                    		<p>&nbsp;</p>
            		
                            <div>The EBM REST API calls use JSON as their return format, and the standard HTTP methods like GET, PUT, POST and DELETE 
                            <BR /><!---(see API documentation <a>here</a> for which methods are available for each resource)---></div>                                                       
           
           
                    	</div>
                        
                    </div>    
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Enterprise Tools</h2>
                            <p>Geo-Locations, Delivery Reciepts, Appointment Reminders, and more... </p>
                        </header>
                    </div>
                    
                    <img width="1205" height="629" src="#rootUrl#/#publicPath#/images/m1/tools-sample.png">
                        
            	</div>
                        
            </div>
            
            <div id="inner-bg-m1" style="">
            
                <div id="contentm1">
                          
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner-wide">
                            <h2>High-Level Short Code Example</h2>
                            <p>Think of a short code as similar to a domain name and a Keyword similar to a page.</p>
                        </header>
                    </div>
                    
                    <div id="inner-box-left" align="center">
                    
                        <div class="">
                            <div class="inner-row">
                                <h2>Step 1</h2>
                                <p>Mobile users sends the keyword "Tickets" to the short code "55555" from the mobile number (949)555-1212. This is an example of a Mobile Originated SMS message.</p>
                            </div>
                        </div>
                        
                        <BR />
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>Step 2</h2>
                                <p>An #BrandShort# server recieves the request. The #BrandShort# service parses the request and looks up actions related to the keyword "Tickets" relating to the short code "55555" </p>
                            </div>
                        </div>
                        
                        <BR />
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>Step 3</h2>
                                <p>An #BrandShort# server responds to (949) 555-1212 with a message with special deals on concert tickets from Sports City Tickets.</p>
                            </div>
                        </div>
                        
                        <BR />
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>Step 4</h2>
                                <p>(949) 555-1212 receives a message for deals on concert tickets from Sports City Tickets. This is an example of a Mobile Terminated SMS message.</p>
                            </div>
                        </div>
                    
                    </div>
                        
            	</div>
                        
            </div>
            
                                
               
           <div id="inner-bg-m1" style="">
             
          		<div id="contentm1" style="min-height:36px;">
                
                	<div class="section-title-content clearfix">
                        <header class="section-title-inner-wide">
                            <h2>Common Short Codes (CSC) FAQs</h2>
                            <p>More About Short Codes</p>
                        </header>
                    </div>
                    
                    <div id="inner-box-full" align="center">
                    
                        <div class="">
                            <div class="inner-row">
                                <h2>What Can I do with a Common Short Code?</h2>
                                <p>Common Short Codes present an unlimited array of contact possibilities all focused on increasing interaction with mobile consumers. Short codes take advantage of the exciting, rapidly growing success of text messaging in the U.S. Most CSC applications are two-way, meaning messages are both received from and sent to wireless subscribers, creating interactivity and an ongoing "conversation" between the end user and the application.
                                <BR /><BR />Today, countless television programs and marketing campaigns make use of CSCs, encouraging users to voice their opinions by sending a text message to the CSC on the screen. The packaging of a consumer good, for example, is likely to feature a promotion that requires one to send a text message to a csc. Sport scores and highlights are available real time as the action occurs; delivered to a mobile phone once the caller requests them via a text message to a csc.</p>
                            </div>
                        </div>
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>How Do I Lease a Common Short Code?</h2>
                                <p>There are three easy steps to obtain your own csc. First, from the CSCA website, find the short code you desire. If a specific code is not necessary, the system will assign a random code upon request. Next, apply for your code by submitting necessary registration data and wait for approval. Once approved and paid for, your CSC will be assigned to you for use.</p>
                            </div>
                        </div>
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>How Do I Implement My CSC?</h2>
                                <p>Once you have ordered a CSC, and completed the required campaign information, the next step is to implement it so it can be used in your mobile application. If you have strong, existing relationships with the wireless service providers, you may be able to complete the following implementation steps on your own by working directly with the wireless service providers.
                                <BR /><BR />Even though you are leasing a Common Short Code from the Registry, you still must negotiate activation of your Common Short Code with each of the Wireless Carriers. This is a separate agreement between you and the carrier for which neither the Registry nor the CSCA has any control. Failure to reach agreement with any Wireless Carrier to carry traffic through a leased CSC is not the responsibility of the Registry or the CSCA, nor will any refund be provided.</p>
                            </div>
                        </div>
                        
                        <div class="">
                            <div class="inner-row">
                                <h2>Approval</h2>
                                <p>Each participating wireless service provider must decide for itself whether or not to accept the routing of your CSC within their network. Wireless service providers will make their decision based on a number of criteria, including i) the content provider; ii) their experience in working with you and your partners; iii) the type of CSC application; iv) the amount and type of promotion of the CSC; v) the estimated message volumes; vi) the timing of your CSC; and vii) the number of other CSC applications in implementation.</p>
                            </div>
                        </div>
                    
                    
                        <div class="">
                            <div class="inner-row">
                                <h2>Contract</h2>
                                <p>In order to connect to a wireless service provider's network and begin sending message traffic related to your CSC; you must enter into an agreement with the wireless service provider. Working with Connectivity Aggregators that have existing contracts with the wireless service providers may facilitate the process.</p>
                            </div>
                        </div>
                                            
                        <div class="">
                            <div class="inner-row">
                                <h2>Connection</h2>
                                <p>Once the CSC has been accepted by the Registry, and you completed the required campaign information a connection between the wireless service provider's network and the CSC application must be implemented and tested. Depending upon your arrangement with the wireless service providers and the Connection Aggregators involved, this process can be sped up significantly by working with a Connection Aggregator who already has connectivity to your desired wireless service provider. Several connection aggregators allow you to connect to them and gain access to multiple wireless service providers. (Note: connection aggregators may offer different wireless service provider connections.)</p>
                            </div>
                        </div>
                    
               		 </div>
                
                </div>
                                                
         	</div> 
            
                           
           
        <!--- SMS --->       
         <div id="inner-bg-m1" style="">
             
          		<div id="contentm1" style="min-height:36px;">
                
                	<div class="section-title-content clearfix">
                        <header class="section-title-inner">
                            <h2>Glossary</h2>
                            <p>Key Terms</p>
                        </header>
                    </div>
         	               
            	    <div class="inner-txt-full" style="margin-bottom:20px;">
                         <div dir="ltr">
                             <table border="0" cellpadding="5px" cellspacing="5px;" >
                                 <colgroup>
                                     <col width="151" />
                                     <col width="673" />
                                 </colgroup>
                                 <tbody>
                                     <tr>
                                         <td><p dir="ltr">Term</p></td>
                                         <td><p dir="ltr">Definition</p></td>
                                     </tr>                                 
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Mobile Originated</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Short message Mobile Originated (SMS-MO)/ Point-to-Point: the ability of a network to transmit a Short Message sent by a mobile phone. The message can be sent to a phone or to a software application.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Mobile Terminated</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Short message Mobile Terminated (SMS-MT)/ Point-to-Point: the ability of a network to transmit a Short Message to a mobile phone. The message can be sent by phone or by a software application.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Keyword</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">A word or name used to distinguish a targeted message within a Short Code Service.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Gatekeeper</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Gatekeepers are responsible for call routing, billing, security (particularly user authentication and authorization), QoS reservations, and -depending on the implementation- any other functions that deal with executing user applications that control gateways.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Short Code or Common Short Code (CSC)</strong></p></td>
                                         <td><p dir="ltr">Short numeric numbers (typically 4-6 digits) to which text messages can be sent from a mobile phone. Wireless subscribers send text messages to common short codes with relevant keywords to access a wide variety of mobile content.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Queue</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Standard message queue that is part of the #BrandShort# system.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Message</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Bytes of data transmitted by the application or mobile phone.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Spam</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Mobile phone spam is a form of spamming directed at the text messaging or other mobile phone communications services.</p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Telcom Company</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">Telcom is a telecommunications network operator. </p></td>
                                     </tr>
                                     <tr>
                                         <td valign="top"><p dir="ltr"><strong>Aggregator</strong></p></td>
                                         <td style="padding-bottom:10px;"><p dir="ltr">There are a number companies offering SMS gateway services. These companies are also referred to as SMS aggregators. Some have direct connections to carriers and others don't. </p></td>
                                     </tr>
                                 </tbody>
                             </table>
                          </div>
                    
                	</div>   
          		
                </div>
                
         	</div> 
            
            
            <div id="inner-bg-m1" style="">
             
          		<div id="contentm1" style="min-height:36px;">
                
                	<div class="section-title-content clearfix">
                        <header class="section-title-inner-full">
                            <h2>XML driven message customization</h2>
                            <p>The possibilities are limitless.</p>
                        </header>
                    </div>
                                
                    <div class="inner-txt-full" style="margin-bottom:20px; margin-top:20px;">
                    
                        <p>&lt;DM BS='0' DSUID='236' Desc='Complex Survey Sample' LIB='0' MT='1' PT='12'&gt;<br />
                            &lt;ELE ID='0'&gt;0&lt;/ELE&gt;<br />
                            &lt;/DM&gt;<br />
                            &lt;RXSS X='100' Y='60'&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK13='' CK2='(2)' CK3='' CK4='(2,2)' CK5='2' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='1' DSUID='236' LINK='2' QID='1' RQ='0' RXT='2' X='240' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK5='3' DESC='We would like to ask' DI='1' DS='1' DSE='2' DSUID='236' LINK='3' QID='2' RXT='1' X='440' Y='60'&gt;<br />
                            &lt;ELE DESC='2.1a' ID='2'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='2.1b' ID='2'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1)' CK3='' CK4='(1,4)' CK5='4' CK6='3' CK7='8' CK8='5' CK9='30' DI='1' DS='1' DSE='3' DSUID='236' LINK='4' QID='3' RQ='0' RXT='2' X='700' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='5' CP='1' DESC='If at anytime' DI='1' DS='1' DSE='7' DSUID='236' LINK='5' QID='4' RXT='1' X='950' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,6),(9,6),(8,6),(7,6),(6,6),(5,6),(4,6),(3,6),(2,6),(1,6),(*,5)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='8' DSUID='236' LINK='8' QID='5' RQ='1' RXT='6' X='1250' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='10' DESC='In responding' DI='1' DS='1' DSE='9' DSUID='236' LINK='10' QID='6' RXT='1' X='1520' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Please press the ## key...' DI='1' DS='1' DSE='25' DSUID='236' LINK='-1' QID='7' RXT='1' X='1890' Y='60'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Thank you for participate' DI='1' DS='1' DSE='22' DSUID='236' LINK='-1' QID='8' RXT='1' X='1890' Y='190'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Invalid respond' DI='1' DS='1' DSE='23' DSUID='236' LINK='-1' QID='9' RXT='1' X='1890' Y='340'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK5='11' DESC='on [date]' DI='3' DS='25' DSE='2' DSUID='236' LINK='11' QID='10' RXT='1' X='40' Y='290'&gt;<br />
                            &lt;ELE DESC='on' ID='6'&gt;3&lt;/ELE&gt;<br />
                            &lt;ELE DESC='date' DK='21' ID='TTS' RXVID='1'&gt;{%CustomField1_vch%}&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,12),(9,13),(8,14),(7,15),(6,16),(5,17),(4,18),(3,19),(2,20),(1,21)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='10' DSUID='236' LINK='8' QID='11' RQ='2' RXT='6' X='240' Y='290'&gt;<br />
                            &lt;ELE DESC='20.1' ID='10'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,22),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='2' DS='1' DSE='11' DSUID='236' LINK='-1' QID='12' RQ='0' RXT='2' X='420' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1 Conerning the represetitives overall handleing of the call you selected...' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='10' ID='28'&gt;10&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='13' RQ='0' RXT='2' X='560' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='9' ID='28'&gt;9&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='14' RQ='0' RXT='2' X='700' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='8' ID='28'&gt;8&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='15' RQ='0' RXT='2' X='840' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='7' ID='28'&gt;7&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='16' RQ='0' RXT='2' X='980' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='6' ID='28'&gt;6&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='17' RQ='0' RXT='2' X='1120' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='5' ID='28'&gt;5&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='18' RQ='0' RXT='2' X='1250' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='4' ID='28'&gt;4&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='19' RQ='0' RXT='2' X='1390' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='3' ID='28'&gt;3&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='20' RQ='0' RXT='2' X='1530' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='2' ID='28'&gt;2&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='21' RQ='0' RXT='2' X='1670' Y='290'&gt;<br />
                            &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='1' ID='28'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='24' DESC='We are glad' DI='1' DS='1' DSE='12' DSUID='236' LINK='24' QID='22' RXT='1' X='50' Y='620'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='24' DESC='We are sorry' DI='1' DS='1' DSE='13' DSUID='236' LINK='24' QID='23' RXT='1' X='50' Y='890'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,25),(9,25),(8,25),(7,25),(6,25),(5,25),(4,25),(3,25),(2,25),(1,25)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='15' DSUID='236' LINK='8' QID='24' RQ='3' RXT='6' X='280' Y='750'&gt;<br />
                            &lt;ELE DESC='40.1' ID='15'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,26),(9,26),(8,26),(7,26),(6,26),(5,26),(4,26),(3,26),(2,26),(1,26)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='16' DSUID='236' LINK='8' QID='25' RQ='4' RXT='6' X='630' Y='750'&gt;<br />
                            &lt;ELE DESC='50.1' ID='16'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,27),(9,27),(8,27),(7,27),(6,27),(5,27),(4,27),(3,27),(2,27),(1,27)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' CP='2' DI='1' DS='1' DSE='17' DSUID='236' LINK='8' QID='26' RQ='5' RXT='6' X='990' Y='750'&gt;<br />
                            &lt;ELE DESC='60.1' ID='17'&gt;1&lt;/ELE&gt;<br />
                            &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                            &lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='11' CK4='(1,30)' CK5='28' DSUID='236' LINK='28' QID='27' RXT='16' X='1310' Y='750'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='11' CK4='(10,29),(9,29),(6,29),(5,29),(4,29),(3,29),(2,29),(1,29)' CK5='8' DSUID='236' LINK='8' QID='28' RXT='16' X='1600' Y='750'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,32),(2,31)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='20' DSUID='236' LINK='-1' QID='29' RQ='0' RXT='2' X='70' Y='1111'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='3' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,28),(2,28)' CK5='28' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='19' DSUID='236' LINK='28' QID='30' RQ='0' RXT='2' X='520' Y='1111'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='45' CK3='33' CK4='0' CK5='0' CK6='' CK7='0' CK8='8' CK9='-13' DSUID='236' LINK='8' QID='31' RQ='6' RXT='3' X='1020' Y='1121'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Thank you' DI='1' DS='1' DSE='22' DSUID='236' LINK='-1' QID='32' RXT='1' X='1570' Y='1151'&gt;0&lt;/ELE&gt;<br />
                            &lt;ELE BS='0' CK1='3' CK5='-1' DESC='After the tone' DI='1' DS='1' DSE='21' DSUID='236' LINK='-1' QID='33' RXT='1' X='1890' Y='1151'&gt;0&lt;/ELE&gt;<br />
                            &lt;/RXSS&gt;<br />
                            &lt;RULES/&gt;<br />
                            &lt;CCD CID='7777777777' ESI='-12'&gt;0&lt;/CCD&gt;<br />
                            &lt;STAGE h='1700' s='0.8999999999999999' w='3200'&gt;<br />
                            &lt;ELE DESC='Greeting' ID='1' STYLE='width: 164px; height: 28px; top: 0px; left: 0px;' X='230' Y='0'/&gt;<br />
                            &lt;ELE DESC='We would like to ask' ID='2' STYLE='width: 300px; height: 42px; top: 0px; left: 0px;' X='430' Y='0'/&gt;<br />
                            &lt;ELE DESC='If you are the person' ID='3' STYLE='width: 300px; height: 45px; top: 0px; left: 0px;' X='690' Y='0'/&gt;<br />
                            &lt;ELE DESC='If at anytime' ID='4' STYLE='width: 217px; height: 40px; top: 0px; left: 0px;' X='940' Y='0'/&gt;<br />
                            &lt;ELE DESC='If a friend' ID='5' STYLE='width: 300px; height: 48px; top: 0px; left: 0px;' X='1240' Y='0'/&gt;<br />
                            &lt;ELE DESC='In responding' ID='6' STYLE='width: 218px; height: 42px; top: 0px; left: 0px;' X='1510' Y='0'/&gt;<br />
                            &lt;ELE DESC='Please press the ## key...' ID='7' STYLE='width: 257px; height: 36px; top: 0px; left: 0px;' X='1880' Y='20'/&gt;<br />
                            &lt;ELE DESC='Thank you for participate' ID='8' STYLE='width: 256px; height: 36px; top: 0px; left: 0px;' X='1880' Y='150'/&gt;<br />
                            &lt;ELE DESC='You have selected an invalid response' ID='9' STYLE='width: 285px; height: 41px; top: 0px; left: 0px;' X='1880' Y='280'/&gt;<br />
                            &lt;ELE DESC='on [date]' ID='10' STYLE='width: 167px; height: 35px; top: 0px; left: 0px;' X='30' Y='240'/&gt;<br />
                            &lt;ELE DESC='Q1: overall rating' ID='11' STYLE='width: 195px; height: 27px; top: 0px; left: 0px;' X='200' Y='240'/&gt;<br />
                            &lt;ELE DESC='You selected 10 for Q1' ID='12' STYLE='width: 300px; height: 29px; top: 0px; left: 0px;' X='360' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 9' ID='13' STYLE='width: 300px; height: 34px; top: 0px; left: 0px;' X='550' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 8' ID='14' STYLE='width: 300px; height: 29px; top: 0px; left: 0px;' X='690' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 7' ID='15' STYLE='width: 300px; height: 33px; top: 0px; left: 0px;' X='830' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 6' ID='16' STYLE='width: 300px; height: 44px; top: 0px; left: 0px;' X='960' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 5' ID='17' STYLE='width: 300px; height: 47px; top: 0px; left: 0px;' X='1110' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 4' ID='18' STYLE='width: 300px; height: 45px; top: 0px; left: 0px;' X='1240' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 3' ID='19' STYLE='width: 157px; height: 34px; top: 0px; left: 0px;' X='1380' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 2' ID='20' STYLE='width: 213px; height: 43px; top: 0px; left: 0px;' X='1520' Y='240'/&gt;<br />
                            &lt;ELE DESC='Selected 1' ID='21' STYLE='width: 144px; height: 44px; top: 0px; left: 0px;' X='1660' Y='240'/&gt;<br />
                            &lt;ELE DESC='We are glad' ID='22' STYLE='width: 192px; height: 34px; top: 0px; left: 0px;' X='40' Y='570'/&gt;<br />
                            &lt;ELE DESC='We are sorry' ID='23' STYLE='width: 149px; height: 42px; top: 0px; left: 0px;' X='40' Y='850'/&gt;<br />
                            &lt;ELE DESC='Q2: courtesy rating' ID='24' STYLE='width: 212px; height: 34px; top: 0px; left: 0px;' X='230' Y='710'/&gt;<br />
                            &lt;ELE DESC='Q3: knowledge rating' ID='25' STYLE='width: 242px; height: 42px; top: 0px; left: 0px;' X='581.7000122070312' Y='676'/&gt;<br />
                            &lt;ELE DESC='Q4: time resolve rating' ID='26' STYLE='width: 300px; height: 34px; top: 0px; left: 0px;' X='930' Y='710'/&gt;<br />
                            &lt;ELE DESC='If Q1 response is 1, then go to 30 (call back)' ID='27' STYLE='width: 300px; height: 50px; top: 0px; left: 0px;' X='1220' Y='690'/&gt;<br />
                            &lt;ELE DESC='If Q1 response is other than 7 and 8, then go to 29 (leave a comment)' ID='28' STYLE='width: 300px; height: 69px; top: 0px; left: 0px;' X='1530' Y='690'/&gt;<br />
                            &lt;ELE DESC='If you would like to receive a call back' ID='29' STYLE='width: 300px; height: 62px; top: 0px; left: 0px;' X='425.5999755859375' Y='1030'/&gt;<br />
                            &lt;ELE DESC='If you would like to leave a comment' ID='30' STYLE='width: 300px; height: 62px; top: 0px; left: 0px;' X='50' Y='1050'/&gt;<br />
                            &lt;ELE DESC='Record a comment' ID='31' STYLE='width: 231px; height: 41px; top: 0px; left: 0px;' X='980' Y='1080'/&gt;<br />
                            &lt;ELE DESC='Thank you for participating' ID='32' STYLE='width: 267px; height: 43px; top: 0px; left: 0px;' X='1530' Y='1111'/&gt;<br />
                            &lt;ELE DESC='After the tone' ID='33' STYLE='width: 247px; height: 47px; top: 0px; left: 0px;' X='1880' Y='1111'/&gt;<br />
                            &lt;/STAGE&gt;</p>
                    </div>
                    
                </div>
                    
            </div>    
                             
        <div id="servicemainline"></div> 
        
      
                
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>