

<cfif IsStruct(Session)>
	<cfif !StructKeyExists(Session, "USERID") >
		<cfset Session.USERID = 0 />
	</cfif>
</cfif>


<!--- Validate cookie and remember me option here--->

<cftry>

 	<cfparam name="Session.MFAISON" default="0">
    <cfparam name="Session.MFALENGTH" default="0"> 
    
    <!--- Decrypt out remember me cookie. --->
    <cfset LOCALOUTPUT.RememberMe = Decrypt(
		COOKIE.RememberMe,
		APPLICATION.EncryptionKey_Cookies,
		"cfmx_compat",
		"hex"
		) />
    <!---
	For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
	of list. Extract it from the list.
	--->
    <cfset CurrRememberMe = ListGetAt(
		LOCALOUTPUT.RememberMe,
		3,
		":"
		) />

    <!---
	Check to make sure this value is numeric, otherwise, it was not a valid value.
	--->
    <cfif IsNumeric( CurrRememberMe )>

        <!---
		We have successfully retreived the "remember me" ID from the user's cookie. Now, store
		that ID into the session as that is how we are tracking the logged-in status.
		--->
        <cfset Session.USERID = CurrRememberMe />
        <cfset SESSION.isAdmin = true>
        <cfset SESSION.loggedIn = "1" />
        
        <!--- *** What is this for ?!? --->
        <cfif StructKeyExists(cookie, "VOICEANSWERCOOKIE")>
            <cfset cookie.VOICEANSWERCOOKIE = null>
        </cfif>
    </cfif>

	<cfset Session.PrimaryPhone = ListGetAt(LOCALOUTPUT.RememberMe, 4, ":") />
    <cfset Session.at = ListGetAt(LOCALOUTPUT.RememberMe, 5, ":") />
    <cfset Session.facebook = ListGetAt(LOCALOUTPUT.RememberMe, 6, ":") />
    <cfset Session.fbuserid = ListGetAt(LOCALOUTPUT.RememberMe, 7, ":") />
    <cfset Session.EmailAddress = ListGetAt(LOCALOUTPUT.RememberMe, 8, ":") />
    <cfset Session.UserNAme = ListGetAt(LOCALOUTPUT.RememberMe, 10, ":") />
    <cfset SESSION.CompanyUserId = ListGetAt(LOCALOUTPUT.RememberMe, 11, ":") />
    <cfset Session.permissionManaged = ListGetAt(LOCALOUTPUT.RememberMe, 12, ":") />
    <cfset Session.companyId = ListGetAt(LOCALOUTPUT.RememberMe, 14, ":") />
    <cfset Session.MFAOK = ListGetAt(LOCALOUTPUT.RememberMe, 15, ":") /> 
    <cfset Session.MFAISON = ListGetAt(LOCALOUTPUT.RememberMe, 16, ":") /> 
    <cfset Session.MFALENGTH = ListGetAt(LOCALOUTPUT.RememberMe, 17, ":") /> 
            
    <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
        UPDATE
        	simpleobjects.useraccount
        SET
        	LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'
        WHERE
        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        AND
        	(CBPID_int = 1 OR CBPID_int IS NULL)
    </cfquery>


	<!--- Insetad of relocating back to session just let user know they are logged in --->
    <!---<cflocation addtoken="false" url="#rootUrl#/#SessionPath#/account/home">--->
    
    
    
    <!--- Catch any errors. --->
    <cfcatch TYPE="any">
        <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
    </cfcatch>
</cftry>

<style>

   #SignInContainer
	{
		<!---position:relative;
		top:5px;
		right:390px;--->
		background-color: #FAFAFA;
		<!---border: 1px solid rgba(0, 0, 0, 0.3);
		border-radius: 5px 5px 5px 5px;
		box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2), 0 1px 0 0 rgba(250, 250, 250, 0.5) inset;--->

		margin: 0;
		padding: 10px 10px 10px 10px;

		width:450px;
		min-width:450px;
		max-width:450px;
		height:270px;
		min-height:270px;
		max-height:270px;

		display:none;
		text-align:left;
		z-index:10100;
	}

	#overlay {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 2000px;
		background-color: #000;
		filter:alpha(opacity=75);
		-moz-opacity:0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
		z-index: 1000;
		display:none;
	}
	
	#overlayheader {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: #000;
		filter:alpha(opacity=75);
		-moz-opacity:0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
		z-index: 1000;
		display:none;
	}
		
	.blueBG
	{
		background: #0085C6;
	}

	.whiteBG
	{
		background: #ffffff;
	}

	.lightgreyBG
	{
		background: #F8F8F8;
	}

	.mleftwi
	{
		text-align:left;
		margin-left:10px;		
	}
	
	.ui-dialog { z-index: 1400 !important ; padding:0 !important;}
	.ui-dialog-title {color: #FFF !important; font-size:14px;}
	
	.ui-widget-content 
	{
    	color: #444;
	}
	
	.ui-widget-content a, .ui-widget-content a:link, .ui-widget-content a:visited, .ui-widget-content a:active {
		color: #15436c;
		text-decoration: none;
	}

		.smalllogohome:hover
	{
		cursor:pointer;
		
	}
	
	.clearfix:after {
     visibility: hidden;
     display: block;
     font-size: 0;
     content: " ";
     clear: both;
     height: 0;
     }
	.clearfix { display: inline-block; }
	/* start commented backslash hack \*/
	* html .clearfix { height: 1%; }
	.clearfix { display: block; }

.group:after {
  content: "";
  display: table;
  clear: both;
}
	
	
	<!--- Change any styles for IE 8--->
	<cfif UseLegacyStrucutres > 	
        
	
	</cfif>	
</style>

<!--- Menu javascript --->



<script type = "text/javascript" >

	/* Copyright (C) 2001,2014 Jeffery Lee Peterson - All Rights Reserved
	 * You may NOT in any part use, distribute or modify this code under the
	 * terms of the EBM license
	 *
	 * You should have received a copy of the EBM license with
	 * this file. If not, please write to: jp@ebmui.com 
	 */

	var HeaderOpen = false;
	
	$(function() {

		<!--- Overlay for login--->
	<!---	var overlay = $('<div id="overlay"> </div>');
		overlay.appendTo(document.body);--->

		<!--- Init based on load--->
		TopMenuScroll(true);


		<!--- Animate / Colapse header on scroll --->
		$(window).scroll( function() {
									
			TopMenuScroll(false);
			
		});
		
		
		$("#SignInTopLink, #SignInTopLinkBar").click(function(){

			<!---if($('#SignInContainer').css("display") == 'none' )
			{
				<!--- Two overlays because of staic header - 1 overlay wouldnt cut it z-index wise--->
				$("#overlay").toggle();
				$("#overlayheader").toggle();
				$("#SignInContainer").toggle("blind", {}, 250);
			}
			else
			{
				<!--- Details - wait for effect to finsh on toggle close before removing overlay--->
				$("#SignInContainer").toggle("blind", function(){$("#overlay").toggle(); $("#overlayheader").toggle();}, 250);
			}--->
			
			$("#overlay").toggle();
			$("#overlayheader").toggle();
							
			$( "#SignInContainer" ).dialog({
			 dialogClass: 'noTitleStuff',
			  resizable: false,
			  closeOnEscape: true,
			  width:473,
			  height:300,
			  modal: true,
			  close: 	function(event, ui)
						{
							// $(this).destroy();
							$("#overlay").toggle();
							$("#overlayheader").toggle();
						},
	 		  position: { my: "right top", at: "right bottom", of: $(this), collision: 'none'  }, 
			  autoOpen: true
			});
			
			

		});
		
		$('#login_close').click(function(){
			
			$( "#SignInContainer" ).dialog( "close" );
			return false;
		});
				
		$('#ConfirmLogOffButton').click(function(){				
			
			if($("#DALD").is(':checked'))
				window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=1';
			else
				window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=0';
			
			
			$( "#dialog-confirm-logout" ).dialog( "close" );
		});
		
		$('#CancelLogOffButton').click(function(){
			
			$( "#dialog-confirm-logout" ).dialog( "close" );
			return false;
		});
			
			
			
		
		$("#logout, #logout2").click(function(){
						
			$("#overlay").toggle();
			$("#overlayheader").toggle();
							
			$( "#dialog-confirm-logout" ).dialog({
			  resizable: false,
			  closeOnEscape: true,
			  width:650,
			  <cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
			  	height:280,
			  <cfelse>
			  	height:200,
			  </cfif>
			  modal: true,
			  close: 	function(event, ui)
						{
							// $(this).destroy();
							$("#overlay").toggle();
							$("#overlayheader").toggle();
						},
	 		  position: { my: "right top", at: "right bottom", of: $(this), collision: 'none'  }, 
			  autoOpen: true
			});
			
	
		});


		// Main Navigation Mega Menu Functionality
		$('#header ul li a').click(function(){

			<!--- This stops menu move and hide and just goes to link--->
			if($(this).hasClass('ui-tabs-anchor') || $(this).hasClass('menulink') || $(this).hasClass('sbToggle') || $(this).hasClass('sbHolder') || $(this).hasClass('sbSelector') || $(this).hasClass('sbFocus'))
			{
				return true;
			}

			$('#header ul li').removeClass('active');
			$(this).parent().addClass('active');
			<!---$('#navigation .close-wrap').show();--->
			var currentTab = $(this).attr('rel');

			// if statement for class on li > a of main nav
			if($(this).hasClass('MegaMenuL')) {  
			
					$('#subnav-container').removeClass('short').addClass('tall').slideDown('fast').animate({'height':'150px',queue:false, duration: 'fast'});
					$('#FixedHeaderSpacerOpen').removeClass('short').addClass('tall').slideDown('fast').animate({'height':'150px',queue:false, duration: 'fast'});
										
				}
				else {
					
				
					$('#subnav-container').removeClass('tall').addClass('short').slideDown('fast').animate({'height':'150px', queue:false, duration: 'fast'});
					$('#FixedHeaderSpacerOpen').removeClass('short').addClass('tall').slideDown('fast').animate({'height':'150px',queue:false, duration: 'fast'});
					
				};

			// If Statement for opening/closing menus at tab
			if($(currentTab).hasClass('open')) {
				// Remove Active Class from Main Navigation Tab
				$('#header ul li').removeClass('active');
				// Remove class from any Open Tab
				$('#navigation .tabs').removeClass('open').fadeOut('fast').animate({queue:false, duration: 'fast'});
				// Close the Subnav Menu Container
				$('#subnav-container').slideUp('fast').animate({height:'0px', queue:false, duration: 'fast'});
				$('#FixedHeaderSpacerOpen').slideUp('fast').animate({height:'0px', queue:false, duration: 'fast'});
				// Hide the Close Button
				<!---$('#navigation .close-wrap').hide();--->
			}
			else {
				$('#navigation .tabs').removeClass('open').fadeOut('fast').animate({queue:false,duration: 'fast'});
				$(currentTab).addClass('open').fadeToggle('fast');
			};
			return false;
		});

		// Close button
		$('#navigation .close').click(function(){
			$(this).parent().hide();
			$('#header ul li').removeClass('active');
			$('#subnav-container').removeClass().addClass('closed').slideUp('fast').animate({'height':'0px', queue:false,duration: 'fast'});
			$('#FixedHeaderSpacerOpen').removeClass().addClass('closed').slideUp('fast').animate({'height':'0px', queue:false,duration: 'fast'});
			$('#navigation .tabs').removeClass('open').slideUp('fast').animate({queue:false, duration: 'fast'});
		});



		<!--- Menu sub containers --->
		$(this).click(function()
		{
			$("#subnav-container").slideUp('fast');
			$('#header ul li').removeClass('active');
			$('#subnav-container').removeClass().addClass('closed').slideUp('fast');
			$('#FixedHeaderSpacerOpen').removeClass().addClass('closed').slideUp('fast');
			$('#navigation .tabs').removeClass('open').slideUp('fast');
		});



	})
	
	function TopMenuScroll(IsInit)
	{
	
	
		<cfif !UseLegacyStrucutres > 	
	
			var value = $(this).scrollTop();
					
									
			if ( value > 10 )
			{
				
				if(HeaderOpen || IsInit)
				{
				
					$("#tollfree").hide();	
					$("#logolink").hide();			
					
					$('#navigationLeftMenu').animate({'top':'34px'},{queue:false,duration: 'fast'});	
					$('#ULContainer').animate({'margin-top':'-10px'},{queue:false,duration: 'fast'});
					$('#FixedHeaderSpacer').animate({'height':'0px'},{queue:false,duration: 'fast'});
					$('#StaticHeader').animate({'height':'36px'}, {queue:false,duration: 'fast'});
					$('#logotop').animate({'height':'0px'}, {easing: 'swing', queue:false, duration: 'fast'});	
					
					$('#SignInSectionTop').hide();	
					$('#SignInSectionTopBar').show();
					$('#smallLogoMenu').show();
					
					HeaderOpen = false;	
				}
			}
			else
			{
				if(!HeaderOpen || IsInit)
				{
					
					$("#tollfree").show();
					$("#logolink").show();		
					
					
					$('#navigationLeftMenu').animate({'top':'108px'},{queue:false,duration: 'fast'});
					$('#ULContainer').animate({'margin-top':'0px'},{queue:false,duration: 'fast'});
					$('#FixedHeaderSpacer').animate({'height':'108px'},{queue:false, duration: 'fast'});
					$('#StaticHeader').animate({'height':'108px'}, {queue:false,duration: 'fast'});
					$('#logotop').animate({'height':'75px'},{easing: 'swing',queue:false, duration: 'fast'});	
					
					$('#SignInSectionTop').show();	
					$('#SignInSectionTopBar').hide();
					$('#smallLogoMenu').hide();
					
					
					HeaderOpen = true;
				}
				
				
			}
			
		</cfif>	
		
	}
</script>
<!---
<cfif cgi.server_port_secure eq 0>
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-41345823-1', 'messagebroadcast.com');
      ga('send', 'pageview');
    </script>

<cfelse>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-41345823-1', 'messagebroadcast.com');
      ga('send', 'pageview');
    </script>

</cfif>--->


<cfoutput>

<div id="overlay"> </div>

<div id="StaticHeader" class="no-print" style="position:fixed; top:0px; width:100%; height:108px; background-color:##F8F8F8; z-index:1100; box-shadow: 0 2px 2px ##d1d1d1;">

	<div id="overlayheader"> </div>

 	<div id="dialog-confirm-logout" title="Are you sure you want to log off?" style="display:none; z-index:1150;">
      
    	<!--- Only offer if MFA is turned on AND phone has been specified --->
    	<cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
      		<p style="padding:25px;"></BR> <input type='checkbox' id='DALD' name='DALD' style="width:20px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px;  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);  margin-bottom: 8px; padding: 3px 8px;"> Deauthorize this device? May require MFA code at next log in.</p>
      	</cfif>
        
        <div class="submit" style="padding:5px 25px; float:right; ">
            <a id="ConfirmLogOffButton" class="bluelogin small" href="##" style="margin-left:15px; margin-bottom:15px; color:##fff; font-size: 14px;">Yes, Log Off Now</a>
            <a id="CancelLogOffButton" class="bluelogin small" href="##" style="margin-left:15px; color:##fff; font-size: 14px;">Cancel</a>
        </div>
    
    </div>

	<div id="SignInContainer" class="awesome" style="z-index:1500;"><cfinclude template="signin.cfm"></div>

    <div id="logotop" style="background-color:##F8F8F8;">
        <div id="logo" style="background-color:##F8F8F8;"><a id="logolink" href="#rootUrl#/#PublicPath#/home"><img src="#LogoImageLink#" style="border:none;" /></a></div>
       <!--- <div id="toll-icon"></div>--->
        <div>
            <div id="SignInSectionTop">
                
                <cfif Session.USERID GT 0>
	                <a id="InSessionAccount" class="BlueTextLink" href="#rootUrl#/#SessionPath#/account/home"><i class="icon-enter"></i>My Account</a>
                    
                    <a href="##" class="BlueTextLink" id="logout"><i class="icon-new-tab"></i>Log Out</a>
                    
                <cfelse>
    				<a id="SignInTopLink" class="BlueTextLink" href="##"><i class="icon-enter"></i>Sign In</a>            
                
	                <a href="#rootUrl#/#publicPath#/signup" class="BlueTextLink"><i class="icon-new-tab"></i>Sign Up</a>
                
                </cfif>
        
			</div>
           <!--- <div id="tollfree">949 428 4899</div>--->
        </div>
    </div>


    <div id="header" class="no-print group clearfix">
    
        <div id="navigation" class="group clearfix">    
            <div id="mainnavnav">    
                <div class="container group clearfix" style="height:36px;">
    
                    <div style="width:1270px; height:auto; text-align:left; margin-left: 200px; " class="group clearfix">
                    
                        <ul id="ULContainer" style="width:1270px; margin: 0 0 0 0;" class="group">
                        
                            <li class="smalllogohome" style="width:81px; height:20px; margin:8px 50px 8px 0px; display:inline;" onclick="window.location='#rootUrl#/#PublicPath#/home'; return true;"><img id="smallLogoMenu" style="display:none;" src="#LogoImageLinkSmall#" <!---width="81" height="20"---> /></li>
                            <li class=""><a class="MegaMenuS" href="##mainnavMessaging" rel="##mainnavMessaging">Messaging Services</a></li>
                            <li class=""><a class="MegaMenuS" href="##mainnavSub2" rel="##mainnavSub2">Trending Now</a></li>
                            <li class=""><a class="menulink" href="###BrandingPricing#" rel="###BrandingPricing#"  onclick="window.location='#rootUrl#/#PublicPath#/#BrandingPricing#'; return true;">pricing</a></li>
                            <li class=""><a class="MegaMenuS" href="##mainnavSub5" rel="##mainnavSub5">api</a></li>   
                            
                            <li style="">
                                <span id="SignInSectionTopBar" style="margin-left:5px;">
            
                                    <cfif Session.USERID GT 0>
                                        <a id="InSessionAccount" class="GreyTextLink menulink" href="#rootUrl#/#SessionPath#/account/home"><i class="icon-enter"></i>My Account</a>
                                        
                                        <a href="javascript:;" class="GreyTextLink menulink" id="logout2"><i class="icon-new-tab"></i>Log Out</a>
                                        
                                    <cfelse>
                                        <a id="SignInTopLinkBar" class="GreyTextLink menulink" href="javascript:;"><i class="icon-enter"></i>Sign In</a>            
                                    
                                        <a href="#rootUrl#/#publicPath#/signup" class="GreyTextLink menulink"><i class="icon-new-tab"></i>Sign Up</a>
                                    
                                    </cfif>
                                    
                                </span>
                            </li>
                                                                                  
                        </ul>
                       
                    </div>                    
                </div>                
            </div>
            
            <!--- Messaging  style="height:170px;" --->
            <div style="display: none; height: 0px;" class="closed tall" id="subnav-container"> 
                <div style="display: none;" id="mainnavMessaging" class="tabs clearfix" >
                    <div id="MessagingSolutions" class="container main-sub-nav clearfix" style="<!---width:1120px;--->" >
                        <ul class="clearfix">
                                                  
                            <!--- EAI Self Serve Inbound Outbound Sessions Surveys Data-Capture Alerts Warnings --->                     
                                    
                            <li class="sub three mleft">
                                
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_smc"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/voice_web2.png');" class="menuicon">&nbsp;</span><h4>Single-Channel<BR />Multi-Channel<BR/>Cross-Channel<BR/></h4></a>
                            </li>
                        
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_nas"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/sms_web2.png');" class="menuicon">&nbsp;</span><h4>Notifications<BR />Alerts and<BR />Surveys</h4></a>
                            </li>
                        
                            <li class="sub four mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_lis"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/agentfemale.png');" class="menuicon">&nbsp;</span><h4>Content<BR/>Management and<BR />Business Rules</h4></a>
                            </li>
                           
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_cpp"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/cppicon.png');" class="menuicon">&nbsp;</span><h4>Customer<BR/>Preference<BR />Portal Tools</h4></a>
                            </li>
                            
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_red"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/m1/dashboard3.png');" class="menuicon">&nbsp;</span><h4>Reports<BR />Exports and<BR />Dashboards</h4></a>
                            </li>
                            
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_eai"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/interactive1.png');" class="menuicon">&nbsp;</span><h4>Enterprise<BR />Application<BR />Integration</h4></a>
                            </li>
                           
                                                   
                        </ul>
                    </div>
                </div>
                
                <!--- / How it Works --->
                <div style="display: none;" id="mainnavSub2" class="tabs clearfix">
                    <div id="mainHowItWorks" class="container  main-sub-nav clearfix">
                        <ul class="clearfix">
                        
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_drip"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/trickle.png');" class="menuicon">&nbsp;</span><h4>Drip<BR />Marketing</h4></a>
                            </li>
                            
                            <li class="sub four mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_mfa"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/mfaicon.png');" class="menuicon">&nbsp;</span><h4>Multi<BR />Factor<BR/>Authentication</h4></a>
                            </li>
                             
                          	<li class="sub mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_customerexperience"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/cemicon.png');" class="menuicon">&nbsp;</span><h4>Customer<BR />Experience<BR/>Management</h4></a>
                            </li>
                        
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_cdf"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/cdficon.png');" class="menuicon">&nbsp;</span><h4>Custom<BR />Data<BR />Fields</h4></a>
                            </li>
                                                    
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_appt"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/appticon.png');" class="menuicon">&nbsp;</span><h4>Appointment<BR />Reminders</h4></a>
                            </li>
                            
                            <li class="sub three mleft">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_ems"><span style="cursor:pointer; background-image:URL('#rootUrl#/#PublicPath#/images/icons/emsicon.png');" class="menuicon">&nbsp;</span><h4>EMS<BR/> e-Messageing<BR />Service</h4></a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                              
                <!--- APIs --->
                <div style="display: none;" id="mainnavSub5" class="tabs clearfix">
                    <div id="APIsMenu" class="container  main-sub-nav clearfix">
                        <ul class="clearfix">
                        
                            <li class="sub three">
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_api" class="menulink"><span style="cursor:pointer;" class="icon">&nbsp;</span><h4>PDC One Call</h4></a>
                            </li>
                            <li class="sub two">								
                                <a class="menulink" href="#rootUrl#/#PublicPath#/hiw_api_document"><span style="cursor:pointer;" onclick="" class="icon">&nbsp;</span><h4>API Documentation</h4></a>
                            </li>
               
                        </ul>
                    </div>
                </div>
                
          </div>                 


        <!---<div style="<!---display: none;--->" class="close-wrap"></div>--->

    	
        </div><!--- navigation--->


	</div><!--- header--->


</div>

<!--- Fixed header spacer--->
<div id="FixedHeaderSpacer" class="no-print" style="min-height:108px;"></div>
<div id="FixedHeaderSpacerOpen" class="no-print" style="height:108px; display:none;"></div>


</cfoutput>