<!---
	Check to see if the page request is a POST or a GET.
	Based on this, we can figure out our target URL.
--->
<cfif (CGI.request_method EQ "get")>
 
	<!--- Get URL-based target url. --->
	<cfset strTargetURL = URL.ProxyURL />
 
	<!--- Delete target URL. --->
	<cfset StructDelete( URL, "ProxyURL" ) />
 
<cfelse>
 
	<!--- Get FORM-based target url. --->
	<cfset strTargetURL = FORM.ProxyURL />
 
		<!--- Delete target URL. --->
	<cfset StructDelete( FORM, "ProxyURL" ) />
 
</cfif>
 
 
<!---
	Remove any AJAX anit-caching that was used by jQuery. This
	is a random number meant to help ensure that GET URLs are
	not cached.
--->
<cfset StructDelete( URL, "_" ) />
 
 
 
<!---
	Make the proxy HTTP request using. When we do this, try to
	pass along all of the CGI information that was made by the
	original AJAX request.
--->
<cfhttp
	result="objRequest"
	url="#UrlDecode( strTargetURL )#"
	method="#CGI.request_method#"
	useragent="#CGI.http_user_agent#"
	timeout="15">
 
	<!--- Add the referer tht was passed-in. --->
	<cfhttpparam
		type="header"
		name="referer"
		value="#CGI.http_referer#"
		/>
 
	<!--- Pass along any URL values. --->
	<cfloop
		item="strKey"
		collection="#URL#">
 
		<cfhttpparam
			type="url"
			name="#LCase( strKey )#"
			value="#URL[ strKey ]#"
			/>
 
	</cfloop>
 
	<!--- Pass along any FORM values. --->
	<cfloop
		item="strKey"
		collection="#FORM#">
 
		<cfhttpparam
			type="formfield"
			name="#LCase( strKey )#"
			value="#FORM[ strKey ]#"
			/>
 
	</cfloop>
 
</cfhttp>
 
 
<!---
<!--- Debug most current request. --->
<cfset objDebug = {
	CGI = Duplicate( CGI ),
	URL = Duplicate( URL ),
	FORM = Duplicate( FORM ),
	Request = Duplicate( objRequest )
	} />
 
<!--- Output debug to file. --->
<cfdump
	var="#objDebug#"
	output="#ExpandPath( './ajax_prox_debug.htm' )#"
	format="HTML"
	/>
--->
 
 
<!---
	Get the content as a byte array (by converting it to binary,
	we can echo back the appropriate length as well as use it in
	the binary response stream.
--->
<cfset binResponse = ToBinary(
	ToBase64( objRequest.FileContent )
	) />
 
 
<!--- Echo back the response code. --->
<cfheader
	statuscode="#Val( objRequest.StatusCode )#"
	statustext="#ListRest( objRequest.StatusCode, ' ' )#"
	/>
 
<!--- Echo back response legnth. --->
<cfheader
	name="content-length"
	value="#ArrayLen( binResponse )#"
	/>
 
<!--- Echo back all response heaers. --->
<cfloop
	item="strKey"
	collection="#objRequest.ResponseHeader#">
 
	<!--- Check to see if this header is a simple value. --->
	<cfif IsSimpleValue( objRequest.ResponseHeader[ strKey ] )>
 
		<!--- Echo back header value. --->
		<cfheader
			name="#strKey#"
			value="#objRequest.ResponseHeader[ strKey ]#"
			/>
 
	</cfif>
 
</cfloop>
 
 
<!---
	Echo back content with the appropriate mime type. By using
	the Variable attribute, we will make sure that the content
	stream is reset and ONLY the given response will be returned.
--->
<cfcontent
	type="#objRequest.MimeType#"
	variable="#binResponse#"
	/>