<!--- Constants used in branding the site --->


<cfset BrandFull = "Event Based Messaging" />
<cfset BrandShort = "EBM" />
<cfset BrandCopyRightNotice = "Event Based Messaging" />

<cfset LogoImageLink = "#rootUrl#/#PublicPath#/images/ebm_i_main/logo-ebm-trans.png" />
<cfset LogoImageLinkSmall = "#rootUrl#/#publicPath#/images/m1/ebmlogotiny.png" />
<cfset LogoImageLink7 = "#rootUrl#/#PublicPath#/home7assets/images/m7/ebmlogo300websmall.png" />
<cfset LogoImageLinkSmall7 = "#rootUrl#/#publicPath#/home7assets/images/m7/ebmlogotiny.png" />

<!--- Default URLS --->
<cfset BrandingAbout = "ebmabout">
<cfset BrandingCareer = "info_career">
<cfset BrandingCompliance = "info_compliance">
<cfset BrandingTerms = "info_terms">
<cfset BrandingPrivacy = "info_privacy">
<cfset BrandingPricing = "hiw_pricing">
<cfset BrandingSupport = "hiw_support">
<cfset BrandingContact = "hiw_contact">

<!--- Contact Us --->
<cfset BrandingContactAddress = "<span>4533 MacArthur Court, Suite 1090</span><BR/><span>Newport Beach, CA 92660</span>">
<cfset BrandingContactPhone = "<span>(888) 747-4411</span>">
<cfset BrandingContactEmail = "<a href='mailto:info@ebmui.com'>info@ebmui.com</a>">



<cfsavecontent variable="BrandedCSSMods">
 
<style>

#logoMB 
{
    background: url("<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>/images/ebm_i_main/logo-ebm.png") no-repeat scroll 0 0 transparent;
}

</style>

</cfsavecontent>
