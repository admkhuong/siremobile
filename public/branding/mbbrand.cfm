<!--- Constants used in branding the site --->


<cfset BrandFull = "MessageBroadcast" />
<cfset BrandShort = "MB" />
<cfset BrandCopyRightNotice = "Event Based Messaging" />

<cfset LogoImageLink = "#rootUrl#/#PublicPath#/images/mb/mblogo145x50.png" />
<cfset LogoImageLinkSmall = "#rootUrl#/#PublicPath#/images/mb/mblogo165x50_small.png" />
<cfset LogoImageLink7 = "#rootUrl#/#PublicPath#/images/mb/mblogo145x50.png" />
<cfset LogoImageLinkSmall7 = "#rootUrl#/#PublicPath#/images/mb/mblogo165x50_small.png" />

<!--- Default URLS --->
<cfset BrandingAbout = "mbabout">
<cfset BrandingCareer = "info_career">
<cfset BrandingCompliance = "info_compliance">
<cfset BrandingTerms = "info_terms">
<cfset BrandingPrivacy = "info_privacy">
<cfset BrandingPricing = "hiw_pricingmb">
<cfset BrandingSupport = "hiw_support">
<cfset BrandingContact = "hiw_contactmb">

<!--- Contact Us --->
<cfset BrandingContactAddress = "<span>4685 MacArthur Court, Suite 250</span><BR/><span>Newport Beach, CA 92660</span>">
<cfset BrandingContactPhone = "<span>(949) 428-3111</span>">
<cfset BrandingContactEmail = "<a href='mailto:info@messagebroadcast.com'>info@messagebroadcast.com</a>">


<cfsavecontent variable="BrandedCSSMods">
<style>

#logoMB 
{
    background: url("<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>/images/mb/mblogo145x50.png") no-repeat scroll 0 0 transparent;
}

</style>
</cfsavecontent>

