<!--- Constants used in branding the site --->


<cfset BrandFull = "Smart Interactive Resppnse Engine" />
<cfset BrandShort = "Sire" />
<cfset BrandCopyRightNotice = "Sire" />

<cfset LogoImageLink = "#rootUrl#/pub/images/sirelogoii194x50web.png" />
<cfset LogoImageLinkSmall = "#rootUrl#/pub/images/sirelogo45x20web.png" />


<!--- Default URLS --->
<cfset BrandingAbout = "ebmabout">
<cfset BrandingCareer = "info_career">
<cfset BrandingCompliance = "info_compliance">
<cfset BrandingTerms = "info_terms">
<cfset BrandingPrivacy = "info_privacy">
<cfset BrandingPricing = "hiw_pricing">
<cfset BrandingSupport = "hiw_support">
<cfset BrandingContact = "hiw_contact">

<cfset BrandingLOLink = "#rootUrl#/pub/home" />



<cfsavecontent variable="BrandedCSSMods">
 
<style>

#logoMB 
{
    background: url("<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/pub</cfoutput>/images/sirelogoii194x50web.png") no-repeat scroll 0 0 transparent;
	width:194px !important;
}

</style>

</cfsavecontent>
