<cfcomponent output="false" >
	<cfinclude template="../paths.cfm" >
	
	
	<cffunction name="ReadFileContent" returnformat="json" access="remote" output="false">
		<cfargument name="path" required="yes" type="string"/>
		
		<cfset filePath = expandPath(path) />
		
		<cffile action = "read" 
	    file = "#filePath#"
	    variable = "Message">
		<cfreturn Message>

	</cffunction>	

	
	
</cfcomponent>