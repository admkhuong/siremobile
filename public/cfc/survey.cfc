<cfcomponent output="true">
<cffunction name="SaveAnswerSurvey" access="remote" output="false" hint="Add a new Answer to XMLResultStr_vch">
	 <cfargument name="BATCHID" TYPE="string" default="0"/>
	 <cfargument name="UUID" TYPE="string" default="0"/>
     <cfargument name="inpXML" TYPE="string" default="0"/>  
	
     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
			INSERT INTO simplexresults.contactresults (BatchId_bi, DTS_UUID_vch, Created_dt, XMLResultStr_vch)
			VALUES(
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#BATCHID#">, 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UUID#">, 
			Now(),
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpXML#">)	
	     </cfquery> 
	     <cfreturn 0> 
	</cffunction>
	</cfcomponent>