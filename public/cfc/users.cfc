

<cfcomponent>
	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
	<cfinclude template="../paths.cfm" >
	
	<!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 

	<cffunction name="AddUser" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <cffunction name="RetrieveUserInfo" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>
    
    <!---<cffunction name="ChangePassword" access="public" returntype="string">
		<cfargument name="myArgument" TYPE="string" required="yes">
		<cfset myResult="foo">
		<cfreturn myResult>
	</cffunction>--->
	
	<cffunction name="CheckLinkInValid" access="remote" output="false">
		<cfargument name="inpCode" type="string" required="yes" />
				
        <cfset dataout = {} />        
        <cftry>
			<!---validate verify code does match with userid--->						
            <cfquery name="GetUserId" datasource="#Session.DBSourceEBM#">
				SELECT 
                	UserId_int,
                	UserName_vch
				FROM 
                	simpleobjects.useraccount
				WHERE 
					VerifyPasswordCode_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpCode#">
                AND 
                    Active_int = 1
				LIMIT 1 
			</cfquery>
            <!---if verify code does not match with userid, return--->
			<cfif NOT GetUserId.RecordCount>
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "Your link was invalid! We'll be happy to send you another one when you're ready!" />
				<cfset dataout.ERRMESSAGE = "" />
				<cfreturn dataout>
			<cfelse>
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERNAME = GetUserId.UserName_vch />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />
			</cfif>
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="ChangePassword" access="remote" output="false">
		<cfargument name="inpCode" type="string" required="yes" />
		<cfargument name="inpPassword" type="string" required="yes" />
				
        <cfset dataout = {} />        
        <cftry>
			<!---validate verify code does match with userid--->						
            <cfquery name="GetUserId" datasource="#Session.DBSourceEBM#">
				SELECT 
                	UserId_int,
                	UserName_vch
				FROM 
                	simpleobjects.useraccount
				WHERE 
					VerifyPasswordCode_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpCode#">
                AND 
                    Active_int = 1
				LIMIT 1 
			</cfquery>
            <!---if verify code does not match with userid, return--->
			<cfif NOT GetUserId.RecordCount>
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "Your link was invalid! We'll be happy to send you another one when you're ready!" />
				<cfset dataout.ERRMESSAGE = "" />
				<cfreturn dataout>
			<cfelse>
				<!---if verify code does match with userid, update VerifyPasswordCode_vch is null and Password_vch--->
				<cfquery datasource="#Session.DBSourceEBM#">
					UPDATE
                    	simpleobjects.useraccount
					SET 
                    	VerifyPasswordCode_vch = NULL, 
                    	Password_vch = AES_ENCRYPT('#inpPassword#', '#RULE_ENCRYPT_PASS#') 
					WHERE 
						UserId_int = '#GetUserId.UserId_int#'
				</cfquery>
                
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.USERNAME = GetUserId.UserName_vch />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "" />
				<cfset dataout.ERRMESSAGE = "" />
			</cfif>
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="CancelChangePassword" access="remote" output="false">
		<cfargument name="inpCode" type="string" required="yes" />
				
        <cfset dataout = {} />        
        <cftry>
			<cfquery datasource="#Session.DBSourceEBM#">
				UPDATE
                	simpleobjects.useraccount
				SET 
                	VerifyPasswordCode_vch = NULL
				WHERE 
					VerifyPasswordCode_vch = '#inpCode#'
			</cfquery>
            
			<cfset dataout = {} />    
		    <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />
			<cfcatch type="any">
				<cfset dataout = {} />    
			    <cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>
        
    <!---<cffunction name="ResetPassword" access="remote" output="false">
		<cfargument name="inpUserName" TYPE="string" required="true">
		<cfargument name="inpEmail" Type="string" required="true">
		<cfargument name="inpValidationCode" Type="string" required="true">
		
		<cfset dataout="">
		
		<!--- validate association between username,email and validation code --->
		<cfquery name="validate" datasource="#Session.DBSourceEBM#">
			SELECT 
					count(*) as TOTALCOUNT
			FROM
					simpleobjects.useraccount
			WHERE
					userName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserName#">
			AND
					EmailAddress_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmail#">
			AND
					VerifyPasswordCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpValidationCode#">
		</cfquery>
		
		<cfif validate.TOTALCOUNT GT 0>
			<cftransaction>
			<!--- update user's password and reset validation code value to empty' --->
			
				<!--- generate new password --->
				<cfset newPassword="">
				<cfinvoke method="genRandomPassword" returnvariable="newPassword">
				<!--- Encrypt password before update to database --->
                <cfquery name="UpdatePassword" datasource="#Session.DBSourceEBM#">
					 UPDATE 
					 		simpleobjects.useraccount
					 SET
					 		Password_vch=AES_ENCRYPT('#newPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
					 		VerifyPasswordCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">
					 WHERE
							userName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserName#">
					 AND
							EmailAddress_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpEmail#">
					 AND
							VerifyPasswordCode_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpValidationCode#">
                </cfquery>
				
			<!--- Send password to email provided --->
					<cfset emailContent="Your new password is:" & #newPassword#>
					<cfmail 
					     server="smtp.gmail.com" 
					     username="messagebroadcastsystem@gmail.com" 
					     password="mbs123456" 
					     port="465" 
					     useSSL="true" 
					     type="html"
					     to="#inpEmail#" 
					     from="messagebroadcastsystem@gmail.com" 
					     subject="MessageBroadcast: Your new password">
					     #emailContent#
					</cfmail>
			
			</cftransaction>
			
		         <cfset dataout =  QueryNew("RXRESULTCODE, MAILTO,USERNAME,VALIDATIONCODE,TYPE, MESSAGE, ERRMESSAGE")>  
		         <cfset QueryAddRow(dataout) />
		         <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		         <cfset QuerySetCell(dataout, "MAILTO", "#inpEmail#") />
		         <cfset QuerySetCell(dataout, "USERNAME", "#inpUserName#") />
		         <cfset QuerySetCell(dataout, "VALIDATIONCODE", "#inpValidationCode#") />
		         <cfset QuerySetCell(dataout, "TYPE", "") />
		         <cfset QuerySetCell(dataout, "MESSAGE", "Send new password Successfully") />                
		         <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 			
		<cfelse>
	         <cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
	         <cfset QueryAddRow(dataout) />
	         <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	         <cfset QuerySetCell(dataout, "TYPE", "") />
	         <cfset QuerySetCell(dataout, "MESSAGE", "Wrong validation code") />                
	         <cfset QuerySetCell(dataout, "ERRMESSAGE", "Failed") /> 			
		</cfif>
		<cfreturn dataout>
	</cffunction>--->
    
  
    
    <!--- ************************************************************************************************************************* --->
    <!--- Add a new user account --->
    <!--- ************************************************************************************************************************* --->       
 	
        <cffunction name="RegisterNewAccount" access="remote" output="false" hint="Start a new user account">
	       	<cfargument name="INPNAUSERNAME" required="no" default="WebsiteUser">
	        <cfargument name="inpNAPassword" required="no" default="">
	        <cfargument name="INPMAINPHONE" required="yes" default="">
	        <cfargument name="INPMAINEMAIL" required="yes" default="">
	        <cfargument name="INPCOMPANYID" required="no" default=0>
	        <cfargument name="inpFacebookUser" required="no" default="0">
	        <cfargument name="inpAt" required="no" default="">
	        <cfargument name="inpFirstName" required="no" default="">
	        <cfargument name="inpLastName" required="no" default="">
        
  
        <cfset var dataout = '0' />    
        <cfset var sameIpLimit = 25 />
        <cfset var hoursLimit = 5 />
                                     
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
                           
       	<cfoutput>
         
            <cfset NextGroupId = -1>
        
        	<!--- Set default to error in case later processing goes bad --->
            <!--- Don't pass back password --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NEXTUSERID, INPNAUSERNAME, INPMAINPHONE, INPMAINEMAIL,INPCOMPANYID, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NEXTUSERID", "0") />     
            <cfset QuerySetCell(dataout, "INPNAUSERNAME", "#INPNAUSERNAME#") />     
            <cfset QuerySetCell(dataout, "INPMAINPHONE", "#INPMAINPHONE#") /> 
            <cfset QuerySetCell(dataout, "INPMAINEMAIL", "#INPMAINEMAIL#") />
			<cfset QuerySetCell(dataout, "INPCOMPANYID", "#INPCOMPANYID#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>
                
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif 1 EQ 1> <!--- Dont need a session to register --->
                
                
                	<cfset INPNAUSERNAME = TRIM(INPNAUSERNAME)>
                    <cfset inpNAPassword = TRIM(inpNAPassword)>
                    <cfset INPMAINPHONE = TRIM(INPMAINPHONE)>
                    <cfset INPMAINEMAIL = TRIM(INPMAINEMAIL)>
                    <cfset inpAt = TRIM(inpAt)>
            		
					<!--- Validate primary phone ---> 
                    
                    <!---Find and replace all non numerics except P X * #--->
                    <cfset INPMAINPHONE = REReplaceNoCase(INPMAINPHONE, "[^\d^\*^P^X^##]", "", "ALL")>
                    
                    <cfinvoke 
                     component="validation"
                     method="VldPhone10str"
                     returnvariable="safePhone">
                        <cfinvokeargument name="Input" value="#INPMAINPHONE#"/>
                    </cfinvoke>
            
                    <cfif safePhone NEQ true OR INPMAINPHONE EQ "">
                        <cfthrow MESSAGE="Invalid Phone Number - (XXX)-XXX-XXXX or XXXXXXXXXX - Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
            
                    <!--- Validate email address --->
                    <cfinvoke 
                     component="validation"
                     method="VldEmailAddress"
                     returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
                    </cfinvoke>
                    <cfdump var="#safeeMail#">
                    <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
                        <cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
                    
                    <!--- Verify does not already exist--->
					<!--- Get next Lib ID for current user --->               
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
						AND
                            CPBId_int = 1                                         
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
                        <cfthrow MESSAGE="Email already already in use! Try a different email." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif> 
                    
                    <!--- Verify phonenumber does not already exist--->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(*) AS TOTALCOUNT 
                        FROM
                            simpleobjects.useraccount
                        WHERE                
                            PrimaryPhoneStr_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINPHONE#">                                               
                    </cfquery>  
                    
                    <cfif VerifyUnique.TOTALCOUNT GT 0>
                        <cfthrow MESSAGE="Phonenumber already already in use! Try a different phonenumber." TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
                    
                    
                    <!--- check is user is registering from same ip address more then limited number of time --->
                    <cfquery name="checkLimit" datasource="#Session.DBSourceEBM#">
                    	SELECT
                        	count(*) as totalNumber
                        FROM
                        	simpleobjects.useraccount
                        WHERE
                        	userIp_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#"> 
                        AND
                            (UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(created_dt))/3600 < #hoursLimit#
                    </cfquery>
                    
                    <cfif checkLimit.totalNumber gte sameIpLimit>
                    	<cfthrow MESSAGE="Registering from same machine limit reached. Please wait for some time" TYPE="Any" extendedinfo="" errorcode="-6">
                    </cfif>
                    
                    <cfset randomStr = ''>
                    <cfloop index="i" from="1" to="25" step="1">
                        <cfset a = randrange(48,122)>
                        <cfif (#a# gt 57 and #a# lt 65) or (#a# gt 90 and #a# lt 97)>
                            <cfset randomStr = randomStr & a>
                        <cfelse>
                            <cfset randomStr = randomStr & chr(a)>
                        </cfif>
                    </cfloop>
                    

                    
                    <!--- END::check is user is registering from same ip address more then limited number of time --->
                    
                    
                    <!--- no need to verify username if registering from facebook --->
                    <!---<Cfif inpFacebookUser eq 0>--->
                    
						<!--- Cleanup SQL injection --->
                        <!--- Verify all numbers are actual numbers ---> 
                                                                                                   
                                            
                        <!--- Validate User Name --->                   
                        <cfinvoke 
                         component="validation"
                         method="VldInput"
                         returnvariable="safeName">
                            <cfinvokeargument name="Input" value="#INPNAUSERNAME#"/>
                        </cfinvoke>
                        
                        <cfif safeName NEQ true OR INPNAUSERNAME EQ "">
                            <cfthrow MESSAGE="Invalid login name - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                        </cfif>  
                        
                        <!--- Validate proper password---> 
                        <cfinvoke 
                         component="validation"
                         method="VldInput"
                         returnvariable="safePass">
                            <cfinvokeargument name="Input" value="#inpNAPassword#"/>
                        </cfinvoke>
                       
                        <cfif safePass NEQ true OR inpNAPassword EQ "">
                            <cfthrow MESSAGE="Invalid password - try another" TYPE="Any" extendedinfo="" errorcode="-6">
                        </cfif>  
                                                                 
                        <!--- Add record --->	                        
                        <cfquery name="AddUser" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simpleobjects.useraccount
                                (
                                UserName_vch, 
                                Password_vch, 
                                PrimaryPhoneStr_vch, 
                                EmailAddress_vch, 
                                Created_dt, 
                                UserIp_vch,
                                EmailAddressVerified_vch,
                                CompanyAccountId_int,
								CBPId_int
                                )
                            VALUES 
                            	(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPNAUSERNAME#">, 
								AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'), 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINPHONE#">, 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">, 
								NOW(), 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">, 
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#randomStr#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCOMPANYID#">,
                                1
								)                                        
                        </cfquery>     
                        <!--- Get next User ID for new user --->               
                        <cfquery name="GetNEXTUSERID" datasource="#Session.DBSourceEBM#">
                            SELECT
                                UserId_int,
								PrimaryPhoneStr_vch,
								SOCIALMEDIAID_BI,
								firstName_vch,
								lastName_vch,
								EmailAddress_vch,
								UserName_vch
                            FROM
                                simpleobjects.useraccount
                            WHERE                
                                EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#"> 
							AND
                                CBPId_int = 1                     
                        </cfquery>  
                        <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                           	INSERT INTO 
								simplebilling.billing
	                           	(
	                                UserId_int,
	                                Balance_int,
	                                RateType_int,
	                                Rate1_int,
	                                Rate2_int,
	                                Rate3_int,
	                                Increment1_int,
	                                Increment2_int,
	                                Increment3_int
	                             )
	                             VALUES
	                             (
	                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.UserId_int#">,
	                                100,
	                                4,
	                                1,
	                                1,
	                                1,
	                                30,
	                                30,
	                                30
	                                )
                            </cfquery>
                        
                    <cfset Session.USERID = GetNEXTUSERID.UserId_int>
					<cfset SESSION.CompanyUserId = GetNEXTUSERID.UserId_int>
					<cfset Session.PrimaryPhone = GetNEXTUSERID.PrimaryPhoneStr_vch>
                    <cfset Session.fbuserid = GetNEXTUSERID.SOCIALMEDIAID_BI>
					<cfset Session.EmailAddress = GetNEXTUSERID.EmailAddress_vch>
					<cfset Session.UserName = GetNEXTUSERID.UserName_vch>
                    
                    <cfset xtrainfo = "NADA">        
                    
				  <cfset strRememberMe = (
                            CreateUUID() & ":" &
                            CreateUUID() & ":" &
                            Session.USERID & ":" &
                            Session.PrimaryPhone & ":" &
                            Session.at & ":" &
                            Session.facebook & ":" &
                            Session.fbuserid & ":" &
                            Session.EmailAddress & ":" &
                            CreateUUID() & ":" &							
                            Session.UserName & ":" &
                            SESSION.CompanyUserId & ":" &
                            CreateUUID()														
                        ) />
                        
                    
                    <!--- Encrypt the value. --->
                    <cfset strRememberMe = Encrypt(
	                    strRememberMe,
	                    APPLICATION.EncryptionKey_Cookies,
	                    "cfmx_compat",
	                    "hex"
                    ) />
                     
                    
                    <!--- Store the cookie such that it never expires. --->
                    <cfcookie
	                    name="RememberMe"
	                    value="#strRememberMe#"
	                    expires="never"
                    />
                    
                    <cfset xtrainfo = "Cookie Set for Session.USERID = #Session.USERID# Session.PrimaryPhone = #Session.PrimaryPhone#">
                    
                    <!---<cfset simpleframedx.session.onsessionstart()>--->
                                                                                                         
                    			<cfset dataout =  QueryNew("RXRESULTCODE, NEXTUSERID, INPNAUSERNAME, INPMAINPHONE, INPMAINEMAIL,INPCOMPANYID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NEXTUSERID", "#GetNEXTUSERID.UserId_int#") />   
                    <cfset QuerySetCell(dataout, "INPNAUSERNAME", "#INPNAUSERNAME#") />     
                    <cfset QuerySetCell(dataout, "INPMAINPHONE", "#INPMAINPHONE#") /> 
                    <cfset QuerySetCell(dataout, "INPMAINEMAIL", "#INPMAINEMAIL#") />  
					<cfset QuerySetCell(dataout, "INPCOMPANYID", "#INPCOMPANYID#") />  					
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#Session.USERID# : #Session.PrimaryPhone# : #Session.fbuserid#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
            
                    <!--- Mail user their welcome aboard email --->			
                <cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, NEXTUSERID, INPNAUSERNAME, INPMAINPHONE, INPMAINEMAIL,INPCOMPANYID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NEXTUSERID", "0") />   
                    <cfset QuerySetCell(dataout, "INPNAUSERNAME", "#INPNAUSERNAME#") />     
					<cfset QuerySetCell(dataout, "INPMAINPHONE", "#INPMAINPHONE#") /> 
                    <cfset QuerySetCell(dataout, "INPMAINEMAIL", "#INPMAINEMAIL#") />  
					<cfset QuerySetCell(dataout, "INPCOMPANYID", "#INPCOMPANYID#") />  					
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Session Expired! Refresh page after logging back in.") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                           
                <cfcatch TYPE="any">
                    
					<cfset dataout =  QueryNew("RXRESULTCODE, NEXTUSERID, INPNAUSERNAME, INPMAINPHONE, INPMAINEMAIL,INPCOMPANYID, TYPE, MESSAGE, ERRMESSAGE")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
                    <cfset QuerySetCell(dataout, "NEXTUSERID", "0") />   
                    <cfset QuerySetCell(dataout, "INPNAUSERNAME", "#INPNAUSERNAME#") />     
                    <cfset QuerySetCell(dataout, "INPMAINPHONE", "#INPMAINPHONE#") /> 
                    <cfset QuerySetCell(dataout, "INPMAINEMAIL", "#INPMAINEMAIL#") /> 
					<cfset QuerySetCell(dataout, "INPCOMPANYID", "#INPCOMPANYID#") />     
                    <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                                
                </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="SendValidationCode" access="remote" output="false" hint="Send validation code for reset password to email of user">
		<cfargument name="inpUserName" default="" >
		<cfargument name="inpMailTo" default="">
		
		<cfset dataout="">
		<cfset validationCode="">
		
		<!--- Validate input UserName and input Email --->
        <cfquery name="validate" datasource="#Session.DBSourceEBM#">
			SELECT
					COUNT(*) AS TOTALCOUNT
			FROM
					simpleobjects.useraccount
			WHERE
					userName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserName#">
			AND
					EmailAddress_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMailTo#">
        </cfquery>
		
		<cfif validate.TOTALCOUNT GT 0>
			<!--- generate validation Code --->
			<cftransaction>
				<cfinvoke method="genValidationCode" returnvariable="validationCode"/>
				
				<!--- Save validation Code to database for later validation --->
				<cfquery name="saveValidationCode" datasource="#Session.DBSourceEBM#">
					UPDATE 
							simpleobjects.useraccount
					SET
							VerifyPasswordCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#validationCode#">
					WHERE
							userName_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUserName#">
					AND
							EmailAddress_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpMailTo#">
				</cfquery>
				
				<!--- Send validation Code to email provided --->
					<cfset emailContent="Your validation code is:" & #validationCode#>
					<cfmail 
					     server="smtp.gmail.com" 
					     username="messagebroadcastsystem@gmail.com" 
					     password="mbs123456" 
					     port="465" 
					     useSSL="true" 
					     type="html"
					     to="#inpMailTo#" 
					     from="messagebroadcastsystem@gmail.com" 
					     subject="Password reset validation code">
					     #emailContent#
					</cfmail>				
			</cftransaction>
		         <cfset dataout =  QueryNew("RXRESULTCODE, MAILTO,USERNAME,VALIDATIONCODE,TYPE, MESSAGE, ERRMESSAGE")>  
		         <cfset QueryAddRow(dataout) />
		         <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		         <cfset QuerySetCell(dataout, "MAILTO", "#inpMailTo#") />
		         <cfset QuerySetCell(dataout, "USERNAME", "#inpUserName#") />
		         <cfset QuerySetCell(dataout, "VALIDATIONCODE", "#validationCode#") />
		         <cfset QuerySetCell(dataout, "TYPE", "") />
		         <cfset QuerySetCell(dataout, "MESSAGE", "Send validation code Successfully") />                
		         <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 			
		<cfelse>
	         <cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
	         <cfset QueryAddRow(dataout) />
	         <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
	         <cfset QuerySetCell(dataout, "TYPE", "") />
	         <cfset QuerySetCell(dataout, "MESSAGE", "No legal useraccount/email provided") />                
	         <cfset QuerySetCell(dataout, "ERRMESSAGE", "Failed") /> 
		</cfif>
		

		
			<cfreturn dataout>		
	</cffunction>
	
	<cffunction name="genValidationCode" access="private" output="false" hint="generate validation code to send to user">
		<cfset chars = "0123456789abcdefghiklmnopqrstuvwxyz" />
		<cfset strLength = 6 />
		<cfset randout = "" />
		
		
		 <cfloop from="1" to="#strLength#" index="i">
	    	<cfset rnum = ceiling(rand() * len(chars)) / >
	     	<cfif rnum EQ 0 ><cfset rnum = 1 / ></cfif>
	    	<cfset randout = randout & mid(chars, rnum, 1) / >
		 </cfloop>
		 
		 <cfreturn randout/>

	</cffunction>
	
	<cffunction name="genRandomPassword" access="private" output="false" hint="generate random password with length = 8">
		<cfset chars = "0123456789abcdefghiklmnopqrstuvwxyz" />
		<cfset strLength = 8 />
		<cfset randout = "" />
		
		
		 <cfloop from="1" to="#strLength#" index="i">
	    	<cfset rnum = ceiling(rand() * len(chars)) / >
	     	<cfif rnum EQ 0 ><cfset rnum = 1 / ></cfif>
	    	<cfset randout = randout & mid(chars, rnum, 1) / >
		 </cfloop>
		 
		 <cfreturn randout/>

	</cffunction>	
	
	<cffunction name="render_reCAPTHCHA" hint="I display the reCAPTCHA form" output="true" returntype="void">
		<cfargument name="ssl" type="boolean" default="false" required="true" hint="Set true if form on ssl page to use secured version of reCAPTCHA API and avoid browser complaints. default false" />
		<cfargument name="theme" type="string" default="red" required="true" hint="red|white|blackgrass default red.  Changes look of reCAPTCHA form field." />
		<cfargument name="tabIndex" type="numeric" required="false" />
		
		<cfif ARGUMENTS.ssl>
			<cfset VARIABLES.apiURL = SSL_CHALLENGE_URL />
		<cfelse>
			<cfset VARIABLES.apiURL = CHALLENGE_URL />
		</cfif>
		
		<cfoutput>
		<script type="text/javascript" src="#VARIABLES.apiURL#/challenge?k=#PUBLICKEY#">
		</script>
		<noscript>
		   <iframe src="#VARIABLES.apiURL#/noscript?k=#PUBLICKEY#"
		       height="300" width="500" frameborder="0"></iframe><br>
		   <textarea name="recaptcha_challenge_field" rows="3" cols="40">
		   </textarea>
		   <input type="hidden" name="recaptcha_response_field"
		       value="manual_challenge">
		</noscript>
		</cfoutput>
	</cffunction>
	
	<cffunction name="ResetPassword" access="remote" output="false">
		<cfargument name="inpUserName" TYPE="string" required="yes" />
        <cfargument name="recaptcha_challenge_field" type="string" hint="Pass me the value of FORM.recaptcha_challenge_field" />
		<cfargument name="recaptcha_response_field" type="string" hint="Pass me the value of FORM.recaptcha_response_field" />
		<cfset var dataout = {} />
		<!--- Clean up to avoid SQL injection attack --->
		<cfset inpUserName = Trim(inpUserName) />		
		<cftry>		
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Unhandeled Exception Error - Support has been notified." />
			<cfset dataout.ERRMESSAGE = "Unhandeled Exception Error - Support has been notified." />
			<cfset RetValRECaptchaData = check_reCAPTHCHA(recaptcha_challenge_field = #recaptcha_challenge_field#, recaptcha_response_field = #recaptcha_response_field#) />
            
			<cfif RetValRECaptchaData NEQ true>
				<cfset dataout = {} />
	            <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.TYPE = "Any" />
	            <cfset dataout.MESSAGE = "reCAPTCHA data does not match." />
				<cfset dataout.ERRMESSAGE = "#RetValRECaptchaData#" />
				<cfreturn dataout>
            </cfif>
            
			<cfset dataout = DoResetPassword(inpUserName) />

        <cfcatch type="any">
        	<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>
    
	<cffunction name="DoResetPassword" access="remote" output="false">
		<cfargument name="inpUserName" TYPE="string" required="yes" />
		<cfset var dataout = {} />
		<!--- Clean up to avoid SQL injection attack --->
		<cfset inpUserName = Trim(inpUserName) />		
		<cftry>		
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Unhandeled Exception Error - Support has been notified." />
			<cfset dataout.ERRMESSAGE = "Unhandeled Exception Error - Support has been notified." />
            
            <!---get user account in useraccount table with username or email param---> 
            <cfquery name="GetUserId" datasource="#Session.DBSourceEBM#">
				SELECT 
                	UserId_int,
                    EmailAddress_vch,
                    UserName_vch 
				FROM 
                	simpleobjects.useraccount
				WHERE
                	USERNAME_VCH = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpUserName#">
                OR 
                	EmailAddress_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpUserName#">
                AND 
                    Active_int = 1
				LIMIT 1 
			</cfquery>
            <!---if not result, return--->
			<cfif GetUserId.RecordCount EQ 0>
				<cfset dataout = {} />
	            <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "Your email/username is not valid." />
				<cfset dataout.ERRMESSAGE = "Your email/username is not valid." />
				<cfreturn dataout>
			<cfelse>
			<!---if has user account match with username or email param,
			gen new VerifyPasswordCode, update to useraccount table and send mail to user--->    	
			<cfset verifyCode = Replace(CreateUUID(), "-", "", "All") />                            
            <cfquery datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.useraccount
                SET 
                    VerifyPasswordCode_vch = '#verifyCode#', 
                    validPasswordLink_ti = 0
                WHERE 
                    UserId_int = '#GetUserId.UserId_int#'
            </cfquery>
            
            <!--- Detail - Feature using mail account from BabbleSphere --->
            <!--- Mail user their welcome aboard email --->
            <cfmail server="smtp.gmail.com" username="#SupportEMailUserName#"
					password="#SupportEMailPassword#" port="#SupportEMailPort#" 
					useSSL="#SupportEMailUseSSL#" to="#TRIM(GetUserId.EmailAddress_vch)#"
					from="#SupportEMailFromNoReply#"
					subject="Email request reset password for #BrandShort# Account" type="html">
						
                <table width="75%" border="1px solid ##0075A1" style="border-collapse:collapse; padding:5px" cellpadding="5px" cellspacing="5px">
                    <tr>
                        <td style="background-color:##0075A1; color:white" valign="middle" colspan="2">
                            <span style="font-size:20px;  font-family:Arial, Helvetica, sans-serif; letter-spacing:1">#BrandShort# System Message</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:20px; font-size:15px; font-family:Verdana, geneva, sans-serif; color:##333; line-height:1.5; border:none">
                            <span style="font-size:26px; color:##0075A1; font-family:Script; font-weight:bold">
                                    Hi #left(GetUserId.EmailAddress_vch, find('@', GetUserId.EmailAddress_vch)-1)#,
                            </span>
                            <br />
                            This email is in response to a request to change your password. Your verification code is #verifyCode#<BR>
                            Please confirm this is a valid request by clicking this link (or copy the entire link and then paste into a browser navigation bar):
                            <br>
                            <a href="#rootUrl#/#publicPath#/changePassword.cfm?inpCode=#verifyCode# ">
                                #rootUrl#/#publicPath#/changePassword.cfm?inpCode=#verifyCode# 
                            </a>
                            <br />                            
							<p>
								Best,<br/>
								<span style="font-family:verdana; color:##999; font-size:16px">#BrandShort# Team</span>								
							</p>
                        </td>
                    </tr>
                </table>
            </cfmail>
            
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />			
        </cfif>

        <cfcatch type="any">
        	<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>
    
	<cffunction name="check_reCAPTHCHA" returntype="boolean" hint="I check the captcha">
		<cfargument name="recaptcha_challenge_field" type="string" hint="Pass me the value of FORM.recaptcha_challenge_field" />
		<cfargument name="recaptcha_response_field" type="string" hint="Pass me the value of FORM.recaptcha_response_field" />
		
		<cftry>
			<cfhttp url="#VERIFY_URL#" method="post" timeout="5" throwonerror="true">
				<cfhttpparam type="formfield" name="privatekey" value="#PRIVATEKEY#">
				<cfhttpparam type="formfield" name="remoteip" value="#cgi.REMOTE_ADDR#">
				<cfhttpparam type="formfield" name="challenge" value="#ARGUMENTS.recaptcha_challenge_field#">
				<cfhttpparam type="formfield" name="response" value="#ARGUMENTS.recaptcha_response_field#">
			</cfhttp>
		<cfcatch>
			<cfthrow  type="RECAPTCHA_NO_SERVICE"
				message="recaptcha: unable to contact recaptcha verification service on url '#VERIFY_URL#'">
		</cfcatch>
		</cftry>
	
		<cfset VARIABLES.aResponse = listToArray(cfhttp.fileContent, chr(10))>
	
		<cfif VARIABLES.aResponse[1] eq "false" and VARIABLES.aResponse[2] neq "incorrect-captcha-sol">
			<cfthrow type="RECAPTCHA_VERIFICATION_FAILURE"
				message="recaptcha: the verification service responded with error '#aResponse[2]#'. See http://recaptcha.net/apidocs/captcha/ for error meanings.">
		</cfif>
		
		<cfreturn VARIABLES.aResponse[1] />
	</cffunction>
	
</cfcomponent>