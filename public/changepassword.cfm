﻿<cfinclude template="paths.cfm" >



<cfif StructKeyExists(url,"inpCode")>
	<cfset inpCode = url['inpCode'] />
	<cfinvoke component="#PublicPath#.cfc.users" method="CheckLinkInValid" returnvariable="CheckPemission">
		<cfinvokeargument name="inpCode" value="#inpCode#">
	</cfinvoke>
	
	<cfif CheckPemission.RXRESULTCODE eq 1>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<cfinclude template="header.cfm">
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>
		
		<title>Forget Password - <cfoutput>#BrandShort#</cfoutput></title>
				
				<cfoutput>
					<style type="text/css">
						<!---@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/rxform2.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/MB.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');--->
						@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
					</style>
				   <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
				    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
					<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
				 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>--->
				</cfoutput>
			    
		        
		        
		<style>
			
			#bannerimage {
				background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
				border: 0 solid;
				margin-top: 5px;
				padding: 0;
				height:300px;
			}
			
			.header-content {
				color: #858585;
				font-size: 18px;
				padding-top: 10px;
				width: 480px;
				float:left;
				text-align:left;
			}
		
			.hiw_Info
			{
				color: #444;
			    font-size: 18px;
				text-align:left;	
			}
			
			h2.super 
			{
				color: #0085c8;
				font-size: 22px;
				font-weight: normal;
				padding-bottom: 8px;
			}
			
			.round
		    {
		        -moz-border-radius: 15px;
		        border-radius: 15px;
		        padding: 5px;
		        border: 2px solid #0085c8;
		    }
			
			body
			{
				padding:0;
				border:none;
				height:100%;
				width:100%;
				min-width:1200px;	
				background: rgb(51,182,234); 
				background-size: 100% 100%;
				background-attachment: fixed; 	
				background-repeat:no-repeat;
			}
			
			#forgetpassword_box{
				padding: 12px;
			    text-align: left;
			    width: 60%;
			}
			
			.error
			{
				color:red;
				font-size:12px;
			}
			.forgotPasswordForm
			{
				padding:10px;
				border: 1px solid #CCCCCC;
				text-align:left;
			}
			.forgotPasswordForm h1
			{
				font-size:40px;
				font-weight:normal;
				margin-top:10px;
				margin-bottom:5px;
			}
			.field{
				margin-top:5px;
				width:20%;
				font-weight:bold;
				float:left;
			}
			.editor
			{
				margin-top:5px;
				width:80%;
				float:left;
			}
			
			.editor input
			{
				height:25px;
				margin-bottom:5px;
				width:50%;
				float:left;
			}
			.btnSignup
			{
				float:right;
				margin-bottom:20px;
			}
			.clear
			{
				clear:both;
			}
			
			.bluelogin{
				margin-left:0px;
			}
		
		</style>
		
		    <script type="text/javascript" language="javascript">
						
			$(document).ready(function(){
				<!---$('#account_type_personal').click();
				if($('#account_type_company').is(':checked')){
		  			$("#company_name").show();
		  		}--->
				
				var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
				$img.bind( 'load', function()
				{
					$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
				});
				
				
				if( $img[0].width ){ $img.trigger( 'load' ); }
							 
				<!--- Preload images --->
				$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
											 
				$(window).scroll(function(){
					$('*[class^="prlx"]').each(function(r){
						var pos = $(this).offset().top;
						var scrolled = $(window).scrollTop();
						<!---$('*[class^="prlx"]').css('top', -(scrolled * 0.6) + 'px');		--->
						$('.prlx-1').css('top', -(scrolled * 0.6) + 'px');		
							
					});
				});
					
					
				$("#PreLoadIcon").fadeOut(); 
				$("#PreLoadMask").delay(350).fadeOut("slow");
		
			});
			
			<!--- Image preloader --->
			$.preloadImages = function() 
			{
				for (var i = 0; i < arguments.length; i++) 
				{
					$("<img />").attr("src", arguments[i]);				
				}
			}
			function CheckMatchPass()
			{
				var result = false;
				var minLength = 6; 
				var pass = $("#txtPassword").val();
				var confirmPass = $("#txtConfirmPassword").val();
				
				//if pass is valid min length
				if(pass.length >= minLength)
				{
					$("#validPassword").show();
					$("#unValidPassword").hide();
					$("#lblErrorPass").hide("slow");
					result = true;
				}
				else
				{
					$("#validPassword").hide();
					$("#unValidPassword").show();
					$("#lblErrorPass").show("slow");
					result = false
				}
				if(confirmPass.length > 0)
				{
					result = CheckMatchConfirmPass();
				}
				return result;
			}
			
			function CheckMatchConfirmPass()
			{
				var result = false;
				var minLength = 6; 
				var pass = $("#txtPassword").val();
				var confirmPass = $("#txtConfirmPassword").val();
				
				//if pass and confirmPass is valid min length
				if(pass.length >= minLength && confirmPass.length >= minLength)
				{
					$("#validPassword").show();
					$("#unValidPassword").hide();
					$("#lblErrorPass").hide("slow");
					
				 	//if pass is not match confirmPass 
					if(pass != confirmPass)
					{
						$("#validConfirmPassword").hide();
						$("#unValidConfirmPassword").show();
						$("#lblErrorConfirmPassword").hide("slow");
						$("#lblErrorConfirmPasswordNotMatchPass").show("slow");
						result = false;
					}
					else
					{
						$("#validConfirmPassword").show();
						$("#unValidConfirmPassword").hide();
						$("#lblErrorConfirmPassword").hide("slow");
						$("#lblErrorConfirmPasswordNotMatchPass").hide("slow");
						result = true;
					}		
				}
				else
				{
					//if pass is unvalid min length
					if (pass.length < minLength) 
					{
						$("#validPassword").hide();
						$("#unValidPassword").show();
						$("#lblErrorPass").show("slow");
						result = false;
					}
					//if confirmPass is unvalid min length
					if (confirmPass.length < minLength) 
					{
						$("#validConfirmPassword").hide();
						$("#unValidConfirmPassword").show();
						$("#lblErrorConfirmPassword").show("slow");
						$("#lblErrorConfirmPasswordNotMatchPass").hide("slow");
						result = false;
					}
					
				}
				return result;
			}
			function Validation()
			{
				var result = false;
				
				result = CheckMatchConfirmPass();
				return result; 
			}
			
			function ChangePassword()
			{
				var isvalid = Validation(); 
				if (isvalid == true) {
							
					$.ajax({
							type: "POST",
							url:  '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/cfc/users.cfc?method=ChangePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
							dataType: 'json',
							data:  {
								inpCode : $("#txtVerifyCode").val(),
								inpPassword : $("#txtPassword").val()},										  
							error: function(XMLHttpRequest, textStatus, errorThrown) {
								},					  
							success:					  
							//Default return function for Do CFTE Demo - Async call back
							function(d) 
							{												
								if(typeof(d.RXRESULTCODE) != "undefined")
								{	
									//<!--- Get row 1 of results if exisits--->
									if (d.RXRESULTCODE > 0) 
									{	
										jAlert("Your password has been reset successfully!", "Request success!", function(result){
											window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/home.cfm";
										});
										//doLogin(d.USERNAME, $("#txtPassword").val(), d.BATCHID);
									}
									else
									{
										jAlert(d.MESSAGE);
									}
								}
								else
								{
									//<!--- No result returned --->
									jAlert("Error while changing password - please check your connection and try again later.", "Change password Fail!");
								}
							} 		
						});
							
					return false;	
				}			
			}
			function doLogin(username, password, campaignCode){				
				$.ajax({
				   type: "POST",
				   url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
				  							  
				   data:{
						inpUserName : username, 
						inpPassword : password, 
						inpRememberMe : 0, 
						inpCampaignCode:campaignCode
				   },
				   dataType: "json", 
				   success: function(d) {
					  <!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{					
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/home.cfm";
								}
								else if(parseInt(CurrRXResultCode) == -2 )
								{
									$.jGrowl("Error while trying to login.\n" + "Invalid response from server." , { life:2000, position:"center", header:' Failure!' });
								}
								else
								{
									<!--- Unsuccessful Login --->
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
									if(typeof(d.DATA.REASONMSG[0]) != "undefined")
									{	 
										$.jGrowl("Error while trying to login.\n" + d.DATA.REASONMSG[0] , { life:2000, position:"center", header:' Failure!' });
									}		
									else
									{
										$.jGrowl("Error while trying to login.\n" + "Invalid response from server." , { life:2000, position:"center", header:' Failure!' });
									}
									
								}
							}
							else
							{<!--- Invalid structure returned --->	
								// Do something		
								$.jGrowl("Error while trying to login.\n" + "Invalid response from server." , { life:2000, position:"center", header:' Failure!' });
							}
						}
						else
						{<!--- No result returned --->
							// Do something		
							$.jGrowl("Error while trying to login.\n" + "Invalid response from server." , { life:2000, position:"center", header:' Failure!' });
						}
				   }
			  });
			}
			
			function CancelChangePassword(){
				$.ajax({
					type: "POST",
					url:  '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/cfc/users.cfc?method=CancelChangePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  {
						inpCode : $("#txtVerifyCode").val()
						},	
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						},					  
					success:					  
					//Default return function for Do CFTE Demo - Async call back
					function(d) 
					{												
						if(typeof(d.RXRESULTCODE) != "undefined")
						{	
							//<!--- Get row 1 of results if exisits--->
							if (d.RXRESULTCODE > 0) 
							{	
								window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/home.cfm";
								//doLogin(d.USERNAME, $("#txtPassword").val(), d.BATCHID);
							}
							else
							{
								jAlert(d.MESSAGE);
							}
						}
						else
						{
							//<!--- No result returned --->
							jAlert("Error while changing password - please check your connection and try again later.", "Change password Fail!");
						}
					} 		
				});
			}	
			</script>
		</head>
		            
		    <cfoutput>
			    <body>
		    		<div id="main" align="center"> 
		        
						<!--- EBM site footer included here --->
		                <cfinclude template="act_ebmsiteheader.cfm">
		                    
		                 <div id="bannersection">
		                    <div id="content" style="position:relative;">
		                        
		                        <div class="inner-main-hd" style="width:400px; margin-top:30px;">Change your password</div>
		                        
		                        <div style="clear:both;"></div>
		                        
		                        <div class="header-content">
		                            ....          	                
		                        </div>
		                    </div>
		                </div>
		          
						<div id="inner-bg-m1" style="min-height: 450px">
		        
							<div class="contentm1" id="forgetpassword_box">
								<div class="forgotPasswordForm">
									<div class="field" style="display:none;">
										Verify code
									</div>
									<div class="editor" style="display:none;">
										<input class="" type="text" id="txtVerifyCode" name="txtVerifyCode" value="#inpCode#"  disabled="true">
									</div>
									<!---<div class="clear"></div>
									<div class="field">
										Username
									</div>
									<div class="editor">
										<input class="" type="text" id="txtEmailOrUserName" name="txtEmailOrUserName" value="#inpUserName#" disabled="true">
									</div>		--->
									<div class="clear"></div>
									
									<div class="field" style="margin-top:20px;">
										Password
									</div>
									<div class="editor" style="margin-top:20px;">
										<input class="" type="password" id="txtPassword" name="txtPassword" onchange="CheckMatchPass();">
										<span id="validPassword" style="display:none;">
											<img src="#rootUrl#/#publicPath#/images/valid.png">
										</span>
										<span id="unValidPassword" style="display:none;">
											<img src="#rootUrl#/#publicPath#/images/unvalid.png">
										</span>
										<p class="error" id="lblErrorPass" style="display:none;">Min length of password is 6 char, please try another.</p>
									</div>
									<div class="clear"></div>
									
									<div class="field">
										Confirm
									</div>
									<div class="editor">
										<input class="" type="password" id="txtConfirmPassword" name="txtConfirmPassword" onchange="CheckMatchConfirmPass();">
										<span id="validConfirmPassword" style="display:none;">
											<img src="#rootUrl#/#publicPath#/images/valid.png">
										</span>
										<span id="unValidConfirmPassword" style="display:none;">
											<img src="#rootUrl#/#publicPath#/images/unvalid.png">
										</span>
										<p class="error" id="lblErrorConfirmPassword" style="display:none;">Min length of password is 6 char, please try another.</p>
										<p class="error" id="lblErrorConfirmPasswordNotMatchPass" style="display:none;">Confirm password does not match with password, please try another.</p>
									</div>
									<div class="clear"></div>
									<div align="center" style="margin-bottom:20px;">
										<a class="btnSignup filterButton loginbutton bluelogin small" onclick="CancelChangePassword(); return false;" href="##">Cancel</a>
										<a class="btnSignup filterButton loginbutton bluelogin small" onclick="ChangePassword(); return false;" href="##">Change Password</a>
									</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
	
	
			<div class="transpacer"></div>
	
	 		<!--- EBM site penultimate footer included here --->
	        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
	      
	        
	        <!--- EBM site footer included here --->
	        <cfinclude template="act_ebmsitefooter.cfm">
	    </div>
	    </div>
	    </body>
	</cfoutput>
	</html>
	<cfelse>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<cfinclude template="header.cfm">
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>
		
		<title>Forget Password - <cfoutput>#BrandShort#</cfoutput></title>
				
				<cfoutput>
					<style type="text/css">
						<!---@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/rxform2.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/MB.css');--->
					<!---	@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');--->
						@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
					</style>
				   <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
				    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
					<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
				 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>--->
				</cfoutput>
			    
		        
		        
		<style>
			
			#bannerimage {
				background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
				border: 0 solid;
				margin-top: 5px;
				padding: 0;
				height:300px;
			}
			
			.header-content {
				color: #858585;
				font-size: 18px;
				padding-top: 10px;
				width: 480px;
				float:left;
				text-align:left;
			}
		
			.hiw_Info
			{
				color: #444;
			    font-size: 18px;
				text-align:left;	
			}
			
			h2.super 
			{
				color: #0085c8;
				font-size: 22px;
				font-weight: normal;
				padding-bottom: 8px;
			}
			
			.round
		    {
		        -moz-border-radius: 15px;
		        border-radius: 15px;
		        padding: 5px;
		        border: 2px solid #0085c8;
		    }
			
			body
			{
				padding:0;
				border:none;
				height:100%;
				width:100%;
				min-width:1200px;	
				background: rgb(51,182,234); 
				background-size: 100% 100%;
				background-attachment: fixed; 	
				background-repeat:no-repeat;
			}
			
			#forgetpassword_box{
				padding: 12px;
			    text-align: left;
			    width: 60%;
			}
			
			.error
			{
				color:red;
				font-size:12px;
			}
			.forgotPasswordForm
			{
				padding:10px;
				border: 1px solid #CCCCCC;
				text-align:left;
			}
			.forgotPasswordForm h1
			{
				font-size:40px;
				font-weight:normal;
				margin-top:10px;
				margin-bottom:5px;
			}
			.field{
				margin-top:5px;
				width:20%;
				font-weight:bold;
				float:left;
			}
			.editor
			{
				margin-top:5px;
				width:80%;
				float:left;
			}
			
			.editor input
			{
				height:25px;
				margin-bottom:5px;
				width:50%;
				float:left;
			}
			.btnSignup
			{
				float:right;
				margin-bottom:20px;
			}
			.clear
			{
				clear:both;
			}
			
			.bluelogin{
				margin-left:0px;
			}
			
			#popup_container{
				margin-top:-270px!important;
			}
		
		</style>
		
		    <script type="text/javascript" language="javascript">
						
			$(document).ready(function(){
				
				<!---$('#account_type_personal').click();
				if($('#account_type_company').is(':checked')){
		  			$("#company_name").show();
		  		}--->
				
				var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
				$img.bind( 'load', function()
				{
					$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
				});
				
				
				if( $img[0].width ){ $img.trigger( 'load' ); }
							 
				<!--- Preload images --->
				$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
											 
				$(window).scroll(function(){
					$('*[class^="prlx"]').each(function(r){
						var pos = $(this).offset().top;
						var scrolled = $(window).scrollTop();
						<!---$('*[class^="prlx"]').css('top', -(scrolled * 0.6) + 'px');		--->
						$('.prlx-1').css('top', -(scrolled * 0.6) + 'px');		
							
					});
				});
					
					
				$("#PreLoadIcon").fadeOut(); 
				$("#PreLoadMask").delay(350).fadeOut("slow");
				
				jAlert("<cfoutput>#CheckPemission.MESSAGE#</cfoutput>", "Warning Message!", function(result){
					window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/forgotpassword.cfm";
				});
			});
			
			<!--- Image preloader --->
			$.preloadImages = function() 
			{
				for (var i = 0; i < arguments.length; i++) 
				{
					$("<img />").attr("src", arguments[i]);				
				}
			}
			</script>
		</head>
		            
		    <cfoutput>
			    <body>
		    		<div id="main" align="center"> 
		        
						<!--- EBM site footer included here --->
		                <cfinclude template="act_ebmsiteheader.cfm">
		                    
		                 <div id="bannersection">
		                    <div id="content" style="position:relative;">
		                        
		                        <div class="inner-main-hd" style="width:400px; margin-top:30px;">Change your password</div>
		                        
		                        <div style="clear:both;"></div>
		                        
		                        <div class="header-content">
		                            ....          	                
		                        </div>
		                    </div>
		                </div>
		          
						<div id="inner-bg-m1" style="min-height: 450px">
		        
							<div class="contentm1" id="forgetpassword_box">
								
							</div>
						</div>
		
		
				<div class="transpacer"></div>
		
		 		<!--- EBM site penultimate footer included here --->
		        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
		      
		        
		        <!--- EBM site footer included here --->
		        <cfinclude template="act_ebmsitefooter.cfm">
		    </div>
		    </div>
		    </body>
		</cfoutput>
		</html>
	</cfif>	
</cfif>


