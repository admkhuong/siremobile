<cfif isDefined("session.CPPUSERID")>
	<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
		SELECT u.UserId_int, u.UserName_vch, u.CompanyName_vch userCompanyName, c.CompanyName_vch companyName FROM simpleobjects.useraccount u LEFT JOIN simpleobjects.companyaccount c ON (u.CompanyAccountId_int = c.CompanyAccountId_int) WHERE UserId_int = 
		<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.CPPUSERID#">
	</cfquery>
	<cfif len(getUser.companyName)>
		<cfset welcomMsg = "Welcome to #getUser.companyName# Company's Customer Preference Portal" />
	<cfelseif len(getUser.userCompanyName)>
		<cfset welcomMsg = "Welcome to #getUser.userCompanyName# Company's Customer Preference Portal" />
	<cfelse>
		<cfset welcomMsg = "Welcome to #getUser.UserName_vch# Company's Customer Preference Portal" />
	</cfif>
	<cfoutput>
		<div id="topnav">
			#welcomMsg#
		</div>
	</cfoutput>
</cfif>