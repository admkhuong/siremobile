<cfcomponent output="false" hint="Handle the application.">

	<cfparam name="Session.accessRights" default="#StructNew()#"/>
	<!--- Set TimeOutSeconds for 3600 - 60 minutes for one hour timeout by default - change this if you change the application timeout --->
	<!--- Set up the application. --->
	<cfset THIS.Name = "Cpp" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
	<cfset THIS.SessionManagement = true />
	<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 0, 30, 0 ) />
	<cfset THIS.SetClientCookies = true />
	<cfset THIS.clientmanagement = false />
	<cfset THIS.setdomaincookies = false />
	<cfset THIS.setdomaincookies = false />
	<cfset THIS.loginstorage="session" />
	<!--- Define the page request properties. --->
	<cfsetting requesttimeout="120" showdebugoutput="false" enablecfoutputonly="false" />

	<cffunction name="OnRequestStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires at first part of page processing.">
		<!--- Define arguments. --->
		<cfargument name="TargetPage" TYPE="string" required="true" />
		
		<!--- Bug in CF8 and later - if using the onRequest method in the application.cfc then cfc requests all return empty --->
		<cfscript>          
			if (right(arguments.targetPage, 4) is ".cfc" ) {
			  structDelete(this, "onRequest");
			  structDelete(variables, "onRequest");
			 }
		</cfscript> 
		<!--- Return out. --->

		<cfset this.enforceSSLRequirement() />
        
           
       <!--- <cfset Session.CPPUUID = "">--->
        <cfinclude template="../paths.cfm" />
        <cfparam name="Session.DBSourceEBM" default="BISHOP"/>
               

		<cfreturn true />
	</cffunction>


	<cffunction name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Fires after pre page processing is complete.">
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			TYPE="string"
			required="true"
			 />
		<cfset DEBUG = "yes" />
		<cfinclude template="../paths.cfm" />
        <cfinclude template="../SocialApp.cfm" >
		<cfparam name="Session.DBSourceEBM" default="BISHOP"/>
		<!--- Set for one hour timeout by default - change this if you change the application timeout --->
		<cfset TimeOutSeconds = 3600 />
		<cfparam name="PageTitle" default="">
		<cfparam name="inpCPPUUID" default="0" />
        
		<cftry>
			<!--- Reset enitre session on request--->
	        <cfif inpCPPUUID EQ -1>
	            <cflock scope="session" timeout="10" type="exclusive">
	             	<cfscript>
	            		StructClear(Session);					
	            	</cfscript>
	                
	                <cfset inpCPPUUID = "0">
	            </cflock>
			</cfif>
		        
	        <!--- Details - Skip CPP signin if CPP already provided in URL --->
			<cfif not isdefined("Session.CPPUUID") and not find("index.cfm", ARGUMENTS.TargetPage) and not find("twitter_authorize.cfm", ARGUMENTS.TargetPage)>
	                      
	        	<cfif inpCPPUUID NEQ "0">
	            
	                <!--- Auto Repair Expired Session --->
	                <cfinvoke 
	                 component="cfc.cpp"
	                 method="EnterCpp"
	                 returnvariable="RetValCPPData">                         
	                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
	                </cfinvoke>  
	                
	                <!--- Check for success --->
	                <cfif RetValCPPData.RXRESULTCODE LT 1>
	                    <cfinclude template="/#PublicPath#/cpp/index.cfm" />  
	                <cfelse>
	                	<cfinclude template="/#PublicPath#/cpp/signin.cfm" /> 
	                </cfif>                    
	            	
	            <cfelse>
					<cfinclude template="/#PublicPath#/cpp/index.cfm" />            
	            </cfif>
	            
			<cfelseif not isdefined("Session.CONSUMEREMAIL") and not find("index.cfm", ARGUMENTS.TargetPage) and not find("signin.cfm", ARGUMENTS.TargetPage) and not find("twitter_authorize.cfm", ARGUMENTS.TargetPage)>
		       	<cfinclude template="/#PublicPath#/cpp/signin.cfm" />
		    <cfelse>
	        	
				<!--- If directed towards index and still have a good CPP UUID URL parameter--->
	        	<cfif inpCPPUUID NEQ "0" AND find("index.cfm", ARGUMENTS.TargetPage) >
	            
	                <!--- Auto Repair Expired Session --->
	                <cfinvoke 
	                 component="cfc.cpp"
	                 method="EnterCpp"
	                 returnvariable="RetValCPPData">                         
	                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
	                </cfinvoke>  
	                
	                <!--- Check for success --->
	                <cfif RetValCPPData.RXRESULTCODE LT 1>
	                    <cfinclude template="/#PublicPath#/cpp/index.cfm" />  
	                <cfelse>
	                	<cfinclude template="/#PublicPath#/cpp/signin.cfm" /> 
	                </cfif>                	
	            
	            <cfelse>
	                            
	            	<cfinclude template="#ARGUMENTS.TargetPage#" />
	                      
	            </cfif>
	            
			</cfif>
		<cfcatch>
			<cflog file="CPPErrorlog" type="error" text="#cfcatch.TYPE# - #cfcatch.MESSAGE# - #cfcatch.detail#">
		</cfcatch>
		</cftry>
        
		<!--- Return out. --->
		<cfreturn />
	</cffunction>


	<cffunction
		name="OnSessionEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the session is terminated.">
		<!--- Define arguments. --->
		<cfargument
			name="SessionScope"
			type="struct"
			required="true"
			 />
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			 />
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>

	<cffunction
		name="enforceSSLRequirement"
		access="public"
		returntype="void"
		output="false"
		hint="I check to see if the current request aligns properly with the current SSL connection.">
 		
   
 
		<!--- Definet the local scope. --->
		<cfset var local = {} />
 
		<!---
			Get the current directory in case we need to use
			this for SSL logic.
		--->
		<cfset local.directory = listLast(getDirectoryFromPath( cgi.script_name ), "\/"	) />
 
		<!---
			Get the current file in case we need to use this
			for SSL logic.
		--->
		<cfset local.template = listLast( cgi.script_name,"\/"	) />
 
		<!---
			Check to see if the current request is currently
			using an SSL connection.
		--->
		<cfset local.usingSSL = (cgi.https eq "on") />
 
		<!---
			By default, we are going to assume that the current
			request does NOT require an SSL connection (as this
			is usually in the minority of cases).
		--->
		<cfset local.requiresSSL = false />
 
		<!---
			Now, let's figure out if the current request requires
			an SSL connection. To do this, we can use any kind of
			url, form, directory, of file-based logic.
		--->
		<cfif 	(	
					FINDNOCASE("contactpreferenceportal.com", CGI.SERVER_NAME) AND
					(
				
						(UCASE(local.directory) eq "CPP") ||
						(UCASE(local.directory) eq "CFC") ||
						(UCASE(local.directory) eq "CONTACT") ||
						(UCASE(local.directory) eq "cfc") ||
						(
							structKeyExists( url, "action" ) &&
							(url.action eq "top-secret")
						)
					)
				)>
 
			<!---
				This request does require an SSL. Set the flag
				for use in further logic.
			--->
			<cfset local.requiresSSL = true />
 
		</cfif>
 
 
		<!---
			At this point, we know if we are currently using
			SSL or not and we know if the current request
			requires SSL. Let's use this information to see if
			we need to perform any redirect.
		--->
		<cfif (local.usingSSL eq local.requiresSSL)>
 
			<!---
				The current page is synched with the SSL
				requirements of the request (either is requires
				and IS using an SSL connection or it does not
				require and is NOT using an SSL connection).
				Just return out of the function - no further
				action is required.
			--->
			<cfreturn />
 
		<cfelseif local.requiresSSL>
 
			<!---
				At this point, we know that we need to do some
				sort of redirect, and because the requrest
				requires an SSL connection, we know we need to
				redirect to an HTTPS connection. Let's store the
				protocol for use in the following CFLocation.
			--->
			<cfset local.protocol = "https://" />
 
		<cfelse>
 
			<!---
				At this point, we know that we need to do some
				sort of redirect, and because the request does
				NOT requiere an SSL connection, we know we need
				to redirect to an HTTP connection. Let's store the
				protocol for use in the following CFLocation.
			--->
			<cfset local.protocol = "http://" />
 
		</cfif>
 
		<!---
			If we've made it this far, then we are redirecting
			the user to a different page based on the chosen
			protocol. Build the target URL.
		--->
		<cfset local.url = (
			local.protocol &
			cgi.server_name &
			cgi.script_name &
			"?" &
			cgi.query_string
			) />
 
		<!--- Redirect the use to the target connection. --->
		<cflocation
			url="#local.url#"
			addtoken="false"
			/>
 
		<!--- Return out (NOTE: we will never make it here). --->
		<cfreturn />
	</cffunction>
 




</cfcomponent>
