<cfcomponent>

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../paths.cfm" >
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0" />
	<cfparam name="Session.DBSourceEBM" default="Bishop" />
	<!--- ************************************************************************************************************************* --->
	<!--- Enter CPP --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="EnterCpp" access="remote" output="true" hint="Enter a CPP">
		<cfargument name="inpCPPUUID" required="yes" default="" />
		<cfscript>
			dataout = queryNew("SUCCESS, RXRESULTCODE, TYPE, MESSAGE");
			queryAddRow(dataout);
			querySetCell(dataout, "SUCCESS", false);
			querySetCell(dataout, "RXRESULTCODE", -1);
			querySetCell(dataout, "MESSAGE", "");
			
			inpCPPUUID = trim(inpCPPUUID);
			
			if (!len(inpCPPUUID)) {
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "RXRESULTCODE", -1);
				querySetCell(dataout, "MESSAGE", "Data is invalid!");
				return dataout;
			}
		</cfscript> 
		<cftry>
			<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, 
					GroupId_int, 
					CPP_Template_vch,
                    CPPStyleTemplate_vch 
				FROM 
					simplelists.customerpreferenceportal 
				WHERE 
					CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
					AND 
					Active_int = 1
			</cfquery>
			<cfif getUser.UserId_int GT 0>
				<cfset Session.CPPUUID = TRIM(inpCPPUUID) />
				<cfset Session.CPPUSERID = getUser.UserId_int />
				<cfset Session.CPPTemplate = trim(getUser.CPP_Template_vch) />
                <cfset Session.CPPStyleTemplate_vch = trim(getUser.CPPStyleTemplate_vch) />
                <cfset QuerySetCell(dataout, "SUCCESS", true) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "") />
			<cfelse>
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
				<cfset QuerySetCell(dataout, "TYPE", -1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />
				
			</cfif>
			<cfcatch TYPE="any">
				<cfset QuerySetCell(dataout, "SUCCESS", false) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- Coding Standards?--->	
	<cffunction name="Getgrouplist" access="public" output="true" returntype="query" hint="Get group list of CPP">
		<cfquery name="getGroupIds" datasource="#Session.DBSourceEBM#">
			SELECT grouplist_vch FROM simplelists.customerpreferenceportal WHERE CPP_UUID_vch = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUUID#">
		</cfquery>
		<cfset groupList = getGroupIds.grouplist_vch />
		<cfif Right(groupList, 1) eq ",">
			<cfset groupList = Left(groupList, len(groupList) - 1) />
		</cfif>
		<cfquery name="getGroups" datasource="#Session.DBSourceEBM#">
			SELECT GroupId_int id, Desc_vch name FROM simplelists.simplephonelistgroups WHERE UserId_int = 
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
			<cfif len(groupList)>
				AND GroupId_int IN (#groupList#)
			</cfif>
			ORDER BY Desc_vch
		</cfquery>
		<cfreturn getGroups />
	</cffunction>
     
    
    <!--- ************************************************************************************************************************* --->
    <!--- Get CPP group data --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPGroupData" access="remote" output="false" hint="Get CPP group data">
       	<cfargument name="inpCPPUUID" required="no" default="0">
        	
		<cfset var dataout = '0' />                                           
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->          
                           
       	<cfoutput>
                            
                            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
            <cfset QuerySetCell(dataout, "GROUPNAME", "No user defined Groups found.") />  
            <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "No user defined Groups found.") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
       
            <cftry>           
            	
                <!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
                <cfif inpCPPUUID NEQ "0">    
					<!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                      
                    <!--- Auto Repair Expired Session --->
                    <cfinvoke 
                     component="cpp"
                     method="EnterCpp"
                     returnvariable="RetValCPPData">                         
                        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                    </cfinvoke>  
                    
                    <!--- Check for success --->
                    <cfif RetValCPPData.RXRESULTCODE LT 1>
                        <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-7">
                    </cfif>
                </cfif>    
                                        
                                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.CPPUSERID GT 0>
                                                            
                    
                    <!--- Verify all numbers are actual numbers --->   
                   
	                <cfset LocalGroupIdList = Session.CPPgrouplist />
					<cfif Right(LocalGroupIdList, 1) eq ",">
                        <cfset LocalGroupIdList = Left(LocalGroupIdList, len(LocalGroupIdList) - 1) />
                    </cfif>
        
                    <!--- Get group counts --->
                    <cfquery name="GetGroups" datasource="#Session.DBSourceEBM#">                                                                                                   
                        SELECT                        	
                            GROUPID_INT,
                            DESC_VCH
                        FROM
                           simplelists.simplephonelistgroups
                        WHERE                
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                        <cfif LEN(LocalGroupIdList)>
                            AND GROUPID_INT IN (#LocalGroupIdList#)
                        </cfif>
            
                    </cfquery>  
                    
                    <!--- Start a new result set if there is more than one entry - all entris have total count plus unique group count --->
					                     
                    <cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>  
                    
					<cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "GROUPID", "0") /> 
                    <cfset QuerySetCell(dataout, "GROUPNAME", "Please select an area of interest") />  
                    <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />  
                          
                    <cfloop query="GetGroups">      
						<cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "GROUPID", "#GetGroups.GROUPID_INT#") /> 
                        <cfset QuerySetCell(dataout, "GROUPNAME", "#GetGroups.DESC_VCH#") />  
                        <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    </cfloop>
                                                                        
                <cfelse>
                
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
                                    
                </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>       
                
				<cfset dataout =  QueryNew("RXRESULTCODE, GROUPID, GROUPNAME, GROUPCOUNT, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
                <cfset QuerySetCell(dataout, "GROUPID", "-1") /> 
                <cfset QuerySetCell(dataout, "GROUPNAME", "") />  
                <cfset QuerySetCell(dataout, "GROUPCOUNT", "0") />                     
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
        
    <!--- ************************************************************************************************************************* --->
    <!--- Get detail data of CPP --->
    <!--- This method only requires a valid CPPUUID and CPPUserID in play  --->
    <!--- If not one already in play then restart session based on passed in inpCPPUUID --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="GetCPPDetailData" access="remote" output="true" hint="Get detail data of CPP">
		<cfargument name="inpCPPUUID" required="yes" default="" />
		<cfargument name="GroupId" required="yes" default="" />
				
		<!--- Set default to error in case later processing goes bad --->
		<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
        <cfset QueryAddRow(dataout) />
        <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
        <cfset QuerySetCell(dataout, "TYPE", "") />
		<cfset QuerySetCell(dataout, "MESSAGE", "No user message found.") />                
        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			
		<cftry>
			<cfif Session.CPPUSERID GT 0>
				<cfquery name="GetCPPMessage" datasource="#Session.DBSourceEBM#">
					SELECT 
						Desc_vch, 
						PrimaryLink_vch, 
						CPPMessage_vch
					FROM 
						simplelists.customerpreferenceportal 
					WHERE 
						CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCPPUUID)#"> 
						AND
							( grouplist_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE=",%#TRIM(GroupId)#%,"> 
							OR grouplist_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="%#TRIM(GroupId)#%,">)
						AND
						Active_int = 1
				</cfquery>
				<cfif GetCPPMessage.RecordCount GT 0>
					<cfset dataout =  QueryNew("RXRESULTCODE, DESC, PRIMARYLINK, CPPMESSAGE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		            <cfset QuerySetCell(dataout, "DESC", "#GetCPPMessage.Desc_vch#") /> 
		            <cfset QuerySetCell(dataout, "PRIMARYLINK", "#GetCPPMessage.PrimaryLink_vch#") /> 
		            <cfset QuerySetCell(dataout, "CPPMESSAGE", "#GetCPPMessage.CPPMessage_vch#") />
		            <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE", "") />                
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		            <cfset QuerySetCell(dataout, "TYPE", -1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "Invalid CPP UUID") />               
		            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				</cfif>
				
			<cfelse>
	                <cfthrow MESSAGE="Session Expired! Refresh page after logging back in." TYPE="Any" detail="" errorcode="-2">
            </cfif>          
                           
            <cfcatch TYPE="any">
            
	            <cfif cfcatch.ErrorCode EQ "">
					<cfset cfcatch.ErrorCode = -1>
                </cfif>   

				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>
	            <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.ErrorCode#) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
		
	</cffunction>
    


</cfcomponent>
