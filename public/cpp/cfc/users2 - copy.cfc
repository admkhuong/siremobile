<cfcomponent output="true">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
	<cfsetting showdebugoutput="no" />
	<cfinclude template="../../paths.cfm" >
	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0" />
	<cfparam name="Session.DBSourceEBM" default="Bishop" />

	<cffunction name="generatePassword" access="public" returntype="string" output="false" hint="generate a new password">
		<!--- Set up available lower case values. --->
		<cfset strLowerCaseAlpha = "abcdefghijklmnopqrstuvwxyz" />
		<!---
			Set up available upper case values. In this instance, we
			want the upper case to correspond to the lower case, so
			we are leveraging that character set.
			--->
		<cfset strUpperCaseAlpha = UCase( strLowerCaseAlpha ) />
		<!--- Set up available numbers. --->
		<cfset strNumbers = "0123456789" />
		<!--- Set up additional valid password chars. --->
		<cfset strOtherChars = "~!@##$%^&*" />
		<!---
			When selecting random value, we want to be able to easily
			choose from the entire set. To this effect, we are going
			to concatenate all the previous valid character sets.
			--->
		<cfset strAllValidChars = (
			strLowerCaseAlpha &
			strUpperCaseAlpha &
			strNumbers &
			strOtherChars
			) />
		<!---
			Create an array to contain the password ( think of a
			string as an array of character).
			--->
		<cfset arrPassword = ArrayNew(1) />
		<!---
			When creating a password, there are certain rules that we
			need to follow (as deemed by the business logic). That is,
			the password must:
			- must be exactly 8 characters in length
			- must have at least 1 number
			- must have at least 1 uppercase letter
			- must have at least 1 lower case letter
			--->
		<!--- Select the random number from our number set. --->
		<cfset arrPassword[1] = Mid(strNumbers, RandRange(1, Len(strNumbers)), 1) />
		<!--- Select the random letter from our lower case set. --->
		<cfset arrPassword[ 2 ] = Mid(strLowerCaseAlpha, RandRange(1, Len(strLowerCaseAlpha)), 1) />
		<!--- Select the random letter from our upper case set. --->
		<cfset arrPassword[ 3 ] = Mid(strUpperCaseAlpha, RandRange(1, Len(strUpperCaseAlpha)), 1) />
		<!---
			ASSERT: At this time, we have satisfied the character
			requirements of the password, but NOT the length
			requirement. In order to do that, we must add more
			random characters to make up a proper length.
			--->
		<!--- Create rest of the password. --->
		<cfloop index="intChar" from="#(ArrayLen(arrPassword) + 1)#" to="8" step="1">
			<!---
				Pick random value. For this character, we can choose
				from the entire set of valid characters.
				--->
			<cfset arrPassword[intChar] = Mid(strAllValidChars, RandRange( 1, Len(strAllValidChars)), 1) />
		</cfloop>
		<!---
			Now, we have an array that has the proper number of
			characters and fits the business rules. But, we don't
			always want the first three characters to be of the
			same order (by type). Therefore, let's use the Java
			Collections utility class to shuffle this array into
			a "random" order.
			If you are not comfortable using the Java class, you
			can create your own shuffle algorithm.
			--->
		<cfset CreateObject("java", "java.util.Collections").Shuffle(arrPassword) />
		<!---
			We now have a randomly shuffled array. Now, we just need
			to join all the characters into a single string. We can
			do this by converting the array to a list and then just
			providing no delimiters (empty string delimiter).
			--->
		<cfset strPassword = ArrayToList(arrPassword, "") />
		<cfreturn strPassword />
	</cffunction>


	<cffunction name="RequestNewPassword" access="remote" output="true" hint="Consumer request new password">
        <cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="no" default="2">
        <cfargument name="inpCPPUUID" required="no" default="0">
           
        
        <cfset var dataout = '0' />
        
		<cfscript>
			dataout = queryNew("RXRESULTCODE, SUCCESS, MESSAGE");
			queryAddRow(dataout);
			QuerySetCell(dataout, "RXRESULTCODE", -1);
			querySetCell(dataout, "SUCCESS", false);
			querySetCell(dataout, "MESSAGE", "");
			
			INPCONTACTSTRING = trim(INPCONTACTSTRING);
			
			if (!len(INPCONTACTSTRING)) {
				QuerySetCell(dataout, "RXRESULTCODE", -1);
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "MESSAGE", "eMail address is no valid!");
				return dataout;
			}
		</cfscript> 
		
        <!--- Source is the consumer email reference by default - this is used in insert if needed below to uniquly distinguish this entry --->
        <cfset INP_SOURCEKEY = TRIM(INPCONTACTSTRING)>
        
        <cftry>
        
        	
            
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
         	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpCPPUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp2"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! Valid CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
            
            
			<!--- Validate email address --->
			<cfinvoke 
				component="validation"
				method="VldEmailAddress"
				returnvariable="safeeMail"><cfinvokeargument name="Input" value="#TRIM(INPCONTACTSTRING)#"/></cfinvoke>
			<cfif safeeMail NEQ true OR TRIM(INPCONTACTSTRING) EQ "">
				<cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
            
            <!--- Validate session still in play - handle gracefully if not --->
            <cfif Session.CPPUSERID GT 0>
            
             	<!--- Check if on list yet--->
                <cfquery name="CheckIfOnList" datasource="#Session.DBSourceEBM#">
                    SELECT                        	
                        COUNT(*) AS TOTALCOUNT                                                     
                    FROM
                        simplelists.rxmultilist
                    WHERE                
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                    AND 
                        ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">         
                    <cfif INPCONTACTTYPEID GT 0>
                        AND ContactTypeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                    </cfif>                                                                         
                    AND
                        (
                            CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">   
                            OR
                            CPPID_vch = ""
                            OR
                            CPPID_vch IS NULL
                        )    
                </cfquery> 		
                                    
                <!--- If on list already for this EBM User account then just add to group - if not already in group --->            
                <cfif CheckIfOnList.TOTALCOUNT GT 0>     
                  
                    <!--- This adds CPP reference to existing contact string that does not already have reference --->
                    <!--- Password added after being generated later below--->
                    <cfquery name="UpdateListData" datasource="#Session.DBSourceEBM#" result="RES1">
                        UPDATE
                            simplelists.rxmultilist                                
                        SET 
                            CPPID_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">
                            AND ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">
                           AND
                           (
                                CPPID_vch = ""
                                OR
                                CPPID_vch IS NULL
                           )
                    </cfquery> 		  
                  
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                    <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />   
                    <cfset QuerySetCell(dataout, "TYPE", "") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Updated - Already on List - #RES1.RecordCount#") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                        
                <cfelse>
                    <!--- If not on list already for this EBM User account then just add to List AND Group - if not already in list ---> 
                    <!--- Adding here will allow duplicates by Source ID where Source ID by default is the TRIM(INPCONTACTSTRING)--->
                                        
                    <!--- Get time zone info ---> 
                    <cfset CurrTZ = 0>
                    
                    <!--- Get Locality info --->  
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">
                                                   
                    <!--- Default group--->
                    <cfset Currgrouplist = "0,">
                                                 
                    <!--- Set default to -1 --->         
                    <cfset NextContactListId = -1>       
                                    
                    <!--- Add record --->
                    <cftry>
                                                                                                                    
                        <cfquery name="AddToPhoneList" datasource="#Session.DBSourceEBM#">
                            INSERT INTO simplelists.rxmultilist
                                (
                                	CPPPWD_vch,
                                    CPPID_vch, 
                                    UserId_int, 
                                    Created_dt, 
                                    LASTUPDATED_DT, 
                                    LastAccess_dt, 
                                    ContactTypeId_int, 
                                    ContactString_vch, 
                                    SourceKey_vch, 
                                    TimeZone_int, 
                                    CellFlag_int, 
                                    UserSpecifiedData_vch, 
                                    grouplist_vch, 
                                    OptInFlag_int
                                )
                            VALUES 
                                (
                                    NULL, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">, 
                                    NOW(), 
                                    NOW(), 
                                    NOW(), 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INP_SOURCEKEY#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">, 
                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Currgrouplist#">,
                                     1
                                 )                                        
                        </cfquery>   
                        
                         <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                            
                            <!--- Get time zones, Localities, Cellular data --->
                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                UPDATE MelissaData.FONE AS fx JOIN
                                 MelissaData.CNTY AS cx ON
                                 (fx.FIPS = cx.FIPS) INNER JOIN
                                 simplelists.rxmultilist AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                SET 
                                  ded.TimeZone_int = (CASE cx.T_Z
                                  WHEN 14 THEN 37 
                                  WHEN 10 THEN 33 
                                  WHEN 9 THEN 32 
                                  WHEN 8 THEN 31 
                                  WHEN 7 THEN 30 
                                  WHEN 6 THEN 29 
                                  WHEN 5 THEN 28 
                                  WHEN 4 THEN 27  
                                 END),
                                 ded.LocationKey2_vch = 
                                 (CASE 
                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                  ELSE '' 
                                  END),
                                  ded.LocationKey1_vch = 
                                  (CASE 
                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                  ELSE '' 
                                  END),
                                  ded.CellFlag_int =  
                                  (CASE 
                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                  ELSE 0 
                                  END)                                                  
                                 WHERE
                                    cx.T_Z IS NOT NULL   
                                 AND
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.CPPUSERID#">                         
                                 AND    
                                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">
                                 AND 
                                    CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#"> 
                                    
                                    <!--- Limit to US or Canada?--->
                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
                            </cfquery>
                                                                
                      </cfif>                        
                        
                        <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "Inserted - not already on list") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                                                               
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->	
                        
                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                            
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
                        <cfelse>
                        
                            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                        
                        </cfif>
                        
                    </cfcatch>       
                    
                    </cftry>
                
                </cfif> <!--- CheckIfOnList - not found --->                    
                        
              <cfelse>
                
                    <cfthrow MESSAGE="Session Expired! No Valid CPP UUID Found." TYPE="Any" detail="" errorcode="-2">
                            
              </cfif>     
                        
			<!--- Set new password --->
			<cfset newPassword = generatePassword() />
			<cfquery name="updatePassword" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simplelists.rxmultilist 
				SET 
					CPPPWD_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#newPassword#">, '#APPLICATION.EncryptionKey_UserDB#') 
				WHERE 
					UserId_int = #Session.CPPUSERID# 
				AND 
					CPPID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">
			</cfquery>
            
            <!--- Detail - email address can't be from MessageBroadcast - EBM User will want it to come from their own email address--->
			<!--- Send email --->
			
            
            <cfmail server="smtp.gmail.com" username="support@contactpreferenceportal.com" password="dEF!1048" port="465" useSSL="true" to="#TRIM(INPCONTACTSTRING)#" from="noreply@contactpreferenceportal.com" subject="Email new password for the Contact Preference Portal">
				Your new MFA password to signin is: #newPassword# 
			</cfmail>
    
      <!---      
            <cfmail to="#TRIM(INPCONTACTSTRING)#" from="info@messagebroadcast.com" subject="Email new password for MessageBroadcast Account" type="html">
				Your new password to signin is: #newPassword#
			</cfmail>
            
            <cfmail server="smtp.gmail.com" username="messagebroadcastsystem@gmail.com" password="mbs123456" port="465" useSSL="true" to="#TRIM(INPCONTACTSTRING)#" from="info@messagebroadcast.com" subject="Email new password for MessageBroadcast Account">
				Your new password to signin is: #newPassword# 
			</cfmail>
	 --->
            
            
            
        <cfcatch TYPE="any">
            <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>  
            
            <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
        </cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>


	<!--- Detail - Use like instead of = for email CPPID Match (Case Sensitivity) --->
    <!--- Research - Is it faster to do UCASE compare or LIKE statement? - --->
	<cffunction name="Signin" access="remote" output="true" hint="Consumer signin by email">
		<cfargument name="inpSigninUserID" required="yes" default="" />
		<cfargument name="inpSigninPassword" required="yes" default="" />
		<cfargument name="resetPasswordEveryTime" required="no" default="0" />
        <cfargument name="inpCPPUUID" required="no" default="0">
                
        <cfset var dataout = '0' />
            
		<cftry>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
         	<!--- Based on inpCPPUUID Get the current CPP's EBM User ID--->
			<cfif inpCPPUUID NEQ "0">    
                <!--- session repair or context switch - by forcing CPP UUID the end user can move on to another CPP ---> 
                                  
                <!--- Auto Repair Expired Session --->
                <cfinvoke 
                 component="cpp2"
                 method="EnterCpp"
                 returnvariable="RetValCPPData">                         
                    <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                </cfinvoke>  
                
                <!--- Check for success --->
                <cfif RetValCPPData.RXRESULTCODE LT 1>
                    <cfthrow MESSAGE="Session Terminated! CPP UUID not found." TYPE="Any" detail="" errorcode="-2">
                </cfif>
            </cfif>    
            
            <!--- Data validation --->
            <cfif LEN(TRIM(inpSigninUserID)) EQ 0 >     
	            <cfthrow MESSAGE="CPP UUID must not be blank." TYPE="Any" detail="" errorcode="-3">            
            </cfif>
            
             <cfif LEN(TRIM(inpSigninPassword)) EQ 0 >     
	            <cfthrow MESSAGE="CPP password must not be blank." TYPE="Any" detail="" errorcode="-4">            
            </cfif>
            
            <!--- Look for User in List--->
			<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int
                    <!--- CF9 is not converting nicely from blob to string 
					 , AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninPassword)#">, '#APPLICATION.EncryptionKey_UserDB#') AS CPPPassword
					 --->
				FROM 
					simplelists.rxmultilist 
				WHERE 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
				AND 
					CPPID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninUserID)#">
				AND 
					CPPPWD_vch = AES_ENCRYPT( <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpSigninPassword)#">, '#APPLICATION.EncryptionKey_UserDB#') 
			</cfquery>
            
			<cfif getUser.RecordCount GT 0>
				<!--- Signin success --->
				<!--- Set session variables --->
				<!--- <cfset Session.CONSUMERUUID = getUser.UniqueCustomer_UUID_vch /> --->
				<cfset Session.CONSUMEREMAIL = TRIM(inpSigninUserID) />
				<cfset Session.resetPasswordEveryTime = resetPasswordEveryTime />
                <cfset Session.CPPPWD = inpSigninPassword />
                
				<!--- Update last access --->
				<cfquery name="updateLastAccess" datasource="#Session.DBSourceEBM#">
					UPDATE 
						simplelists.rxmultilist 
					SET 
						LastAccess_dt = NOW() 
                    
                    <!--- Optional reset password on log in - require MFA--->
                    <cfif  resetPasswordEveryTime GT 0>
                    	, CPPPWD_vch = ""
                    </cfif>   
                        
					WHERE 
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
					AND 
						CPPID_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CONSUMEREMAIL#">
				</cfquery>
                                             
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "TYPE", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "OK") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
                
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
                <cfset QuerySetCell(dataout, "TYPE", "") />
				<cfset QuerySetCell(dataout, "MESSAGE", "Invalid email or password") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
			</cfif>
            
            
        <cfcatch TYPE="any">
             <cfif cfcatch.errorcode EQ "">
                <cfset cfcatch.errorcode = -1>
            </cfif>       
            
            <cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", #cfcatch.errorcode#) />
            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            
        </cfcatch>
		</cftry>
        
		<cfreturn dataout />	
    </cffunction>


	<cffunction name="Signout" access="remote" output="true" hint="Consumer signout">
		<cfif (isDefined("Session.resetPasswordEveryTime") and Session.resetPasswordEveryTime)>
			<cfquery name="updatePassword" datasource="#Session.DBSourceEBM#">
				UPDATE simplelists.rxmultilist SET CPPPWD_vch = null WHERE UserId_int = 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CPPUSERID#">
				AND CPPID_vch LIKE 
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.CONSUMEREMAIL#">
			</cfquery>
		</cfif>
		<cfset StructClear(Session) />
		<cfreturn />
	</cffunction>

	<!--- ************************************************************************************************************************* --->
	<!--- Add a new user account --->
	<!--- ************************************************************************************************************************* --->

	<cffunction name="RegisterNewAccount" access="remote" output="true" hint="Start a new user account">
		<cfargument name="email" required="yes" default="" />
		<cfargument name="password" required="yes" default="" />
		<cfscript>
			dataout = queryNew("SUCCESS, MESSAGE");
			queryAddRow(dataout);
			querySetCell(dataout, "SUCCESS", true);
			querySetCell(dataout, "MESSAGE", "");
			
			email = trim(email);
			password = trim(password);
			
			if (!email.len() || !password.len()) {
				querySetCell(dataout, "SUCCESS", false);
				querySetCell(dataout, "MESSAGE", "Datas is invalid!");
				return dataout;
			}
		</cfscript> 
		<cftry>
			<!--- Validate email address --->
			<cfinvoke 
				component="validation"
				method="VldEmailAddress"
				returnvariable="safeeMail"><cfinvokeargument name="Input" value="#TRIM(email)#"/></cfinvoke>
			<cfif safeeMail NEQ true OR email EQ "">
				<cfthrow MESSAGE="Invalid eMail - From@somewehere.domain " TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
			<!--- Verify does not already exist--->
			<!--- Get next Lib ID for current user --->
			<cfquery name="VerifyUnique" datasource="#Session.DBSourceEBM#">
				SELECT 
					COUNT(*) AS TOTALCOUNT 
				FROM 
					simpleobjects.useraccount 
				WHERE 
					EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(email)#">
			</cfquery>
			<cfif VerifyUnique.TOTALCOUNT GT 0>
				<cfthrow MESSAGE="Email already already in use! Try a different email." TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
			<!--- Validate proper password--->
			<cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safePass"><cfinvokeargument name="Input" value="#password#"/></cfinvoke>
			<cfif safePass NEQ true OR password EQ "">
				<cfthrow MESSAGE="Invalid password - try another" TYPE="Any" extendedinfo="" errorcode="-6" />
			</cfif>
			<!--- Add record --->
			<cfquery name="AddUser" datasource="#Session.DBSourceEBM#">
				INSERT INTO 
					simplelists.rxmultilist 
					(UniqueCustomer_UUID_vch, CPPID_vch, CPPPWD_vch) 
				VALUES ( 
					Null, 
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(email)#">, 
					AES_ENCRYPT('#password#', '#APPLICATION.EncryptionKey_UserDB#') ) 
			</cfquery>
			<!--- Get next User ID for new user --->
			<cfquery name="GetNEXTUSERID" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int,
					PrimaryPhoneStr_vch,
					SOCIALMEDIAID_BI 
				FROM 
					simpleobjects.useraccount 
				WHERE 
					EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
			</cfquery>
			<cfset Session.USERID = GetNEXTUSERID.UserId_int />
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, NEXTUSERID, INPNAUSERNAME, INPMAINPHONE, INPMAINEMAIL,INPCOMPANYID, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
				<cfset QuerySetCell(dataout, "NEXTUSERID", "0") />
				<cfset QuerySetCell(dataout, "INPNAUSERNAME", "#INPNAUSERNAME#") />
				<cfset QuerySetCell(dataout, "INPMAINPHONE", "#INPMAINPHONE#") />
				<cfset QuerySetCell(dataout, "INPMAINEMAIL", "#INPMAINEMAIL#") />
				<cfset QuerySetCell(dataout, "INPCOMPANYID", "#INPCOMPANYID#") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

    
	<cffunction name="validateUsers" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's name and password match.">
		<cfargument name="inpUserID" TYPE="string" />
		<cfargument name="inpPassword" TYPE="string" />
		<cfargument name="inpRememberMe" TYPE="string" default="0" required="no" />
		<cfargument name="inpAt" TYPE="string" required="no" default="0" />
		<cfargument name="appId" TYPE="string" required="no" default="0" />
		<cfargument name="inpUUID" TYPE="string" required="no" default="0" />
		<cfargument name="appSecret" TYPE="string" required="no" default="0" />
		<cfargument name="facebook" TYPE="numeric" required="no" default="0" />
		<cfset var dataout = '0' />
		<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG") />
		<cfset QueryAddRow(dataout) />
		<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
		<cfset QuerySetCell(dataout, "REASONMSG", "") />
		<!--- 
			Positive is success
			1 = OK
			2 = 
			3 = 
			4 = 
			Negative is failure
			-1 = general failure
			-2 = CF Failure
			-3 = 
			--->
		<!--- Ensure that attempts to authenticate start with new credentials. --->
		<cflogout/>
		<cftry>

        	<cfset appSecret = "#APPLICATION.FacebookAppsecret#">
            <!---
            <cfif FINDNOCASE( "DEV_EBM.SETA.COM", "#CGI.SERVER_NAME#") GT 0 >		
                <!---<!--- tranglt facebook --->	
                var Facebook_URL = 'http://apps.facebook.com/ebmdevseta';
                var Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%ebmdevseta%2F';		
                <!---var Facebook_Appsecret = '<cfoutput>#APPLICATION.FacebookAppsecret#</cfoutput>';--->
                var Facebook_AppID = '<cfoutput>#APPLICATION.FacebookAppID#</cfoutput>'; <!--- babblesphere 196029783766423 ebmdev.seta 146749318786991 --->--->
            
                <cfset appSecret = "#APPLICATION.FacebookAppsecret#">
            
            <cfelseif FINDNOCASE( "DEV.TELESPEECH.COM", "#CGI.SERVER_NAME#") GT 0>
            
                <!---var Facebook_URL = 'http://apps.facebook.com/ebmcppdev';
                var Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%ebmcppdev%2F';		
                <!---var Facebook_Appsecret = '<cfoutput>4f8fa297f4e01960028db41912ceb48c</cfoutput>';--->
                var Facebook_AppID = '<cfoutput>239247492841662</cfoutput>';
                --->
                
                <cfset appSecret = "4f8fa297f4e01960028db41912ceb48c">
                
            <cfelseif FINDNOCASE( "EBM.MESSAGEBROADCAST.COM", "#CGI.SERVER_NAME#") GT 0>
            
            	<!---	var Facebook_URL = 'http://apps.facebook.com/EBMMESS';
                var Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F';		
                <!---var Facebook_Appsecret = '<cfoutput>55b461c58e5f647e5ccc294c99c7fc4c</cfoutput>';--->
                var Facebook_AppID = '<cfoutput>413297832034676</cfoutput>';
				--->
                
                <cfset appSecret = "55b461c58e5f647e5ccc294c99c7fc4c">
                    
            <cfelse> <!--- default to "CONTACTPREFERENCEPORTAL.COM"--->
            
                <!---var Facebook_URL = 'http://apps.facebook.com/contactpp';
                var Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%contactpp%2F';		
                <!---var Facebook_Appsecret = '<cfoutput>8b6eaf13f5d922efaad3525e47115b99</cfoutput>';--->
                var Facebook_AppID = '<cfoutput>309014592511192</cfoutput>';
				--->
                
                <cfset appSecret = "8b6eaf13f5d922efaad3525e47115b99">
                
            </cfif>
        	--->
        
        
			<!--- Check for too many tries this session and lockout for 5 min --->
			<cfset inpUserID = TRIM(inpUserID) />
			<cfset inpPassword = TRIM(inpPassword) />
			<!--- Server Level Input Protection      component="PremierIVR.model.Validation.validation" --->
			<cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safeName"><cfinvokeargument name="Input" value="#inpUserID#"/></cfinvoke>
			<cfinvoke 
				component="validation"
				method="VldInput"
				returnvariable="safePass"><cfinvokeargument name="Input" value="#inpPassword#"/></cfinvoke>
			<cfif safeName eq true and safePass eq true>
				<!--- No reference to current session variables - exisits in its on session/application scope --->
				<cfif inpAt neq 0>
					<cfhttp url="https://graph.facebook.com/me/?access_token=#inpAt#" result="myDetails" />
					<cfset pData = DeserializeJSON(myDetails.filecontent) />
					
					<!--- if login by 'connect with facebook button' --->

					<cfif inpUserID eq ''>
						<CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
							SELECT 
								UserId_int, 
								ContactTypeId_int,
								ContactString_vch
							FROM 
								simplelists.rxmultilist 
							WHERE 
								ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.id#"> 
						</CFQUERY>
						<!--- Build the obfuscated value. This will be a list in which the user ID is the middle value. --->
                            <cfset strRememberMe = (
								CreateUUID() & ":" &
								CreateUUID() & ":" &
								135 & ":" &
								inpAt & ":" &
								pData.id & ":" &
								pData.email & ":" &
								CreateUUID() & ":" &							
								pData.name & ":" &
								139 & ":" &
								CreateUUID()												
                            ) />
						<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#appId#&client_secret=#appSecret#&grant_type=fb_exchange_token&fb_exchange_token=#inpAt#" result="token_url" />
						<cfset accessContent = Right(token_url.filecontent,len(token_url.filecontent)-13) />
						<cfif find("&",accessContent)>
							<cfset accessContent = Left(accessContent,find("&",accessContent)-1)>
						</cfif>
						<cfif getUser.RecordCount GT 0>

							 <!--- Encrypt the value. --->
                            <cfset strRememberMe = Encrypt( strRememberMe, APPLICATION.EncryptionKey_Cookies, "cfmx_compat", "hex" ) />

							<cfcookie name="RememberMe" value="#strRememberMe#" expires="never" />

							<CFQUERY name="UpdateContact" datasource="#Session.DBSourceEBM#">
								UPDATE
									simplelists.rxmultilist
								SET
									socialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#accessContent#">,
									LastUpdated_dt = NOW(),
									LastAccess_dt = NOW()
								WHERE
									ContactTypeId_int = 4
								AND
									ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.id#">
								AND
									UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.UserId_int#">
									
							</CFQUERY>
						<cfelse>

							<CFQUERY name="writeUser" datasource="#Session.DBSourceEBM#">
								INSERT INTO simplelists.rxmultilist 
								(UserId_int, ContactTypeId_int, TimeZone_int, Created_dt,LastAccess_dt, ContactString_vch,LocationKey1_vch, LocationKey2_vch, LocationKey3_vch, SourceKey_vch,grouplist_vch, LastName_vch, FirstName_vch,socialToken_vch, CPPID_vch)

									SELECT 
									UserId_int, 4,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.timezone#">,
									NOW(),NOW(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.id#">,
									'','','',
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.email#">, 
									'0,',
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.first_name#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.last_name#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#accessContent#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.email#">
									FROM 
										simplelists.customerpreferenceportal
									WHERE 
										CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUUID#">
							</CFQUERY>

							<!--- <CFQUERY name="writeUser" datasource="#Session.DBSourceEBM#">
								INSERT INTO simplelists.rxmultilist 
								(UserId_int, ContactTypeId_int, TimeZone_int, Created_dt, ContactString_vch,LocationKey1_vch, LocationKey2_vch, LocationKey3_vch, SourceKey_vch,grouplist_vch, LastName_vch, FirstName_vch, CPPID_vch)
								VALUES (
									SELECT UserId_int
									FROM simplelists.customerpreferenceportal
									WHERE CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUUID#">,
									4,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.timezone#">,
									NOW(),
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.id#">,
									'','','',
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.email#">, 
									'0,',
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.first_name#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.last_name#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#pData.email#">
								)
							</CFQUERY> --->
							<cflogin>
							</cflogin>
							 <!--- Encrypt the value. --->
                            <cfset strRememberMe = Encrypt( strRememberMe, APPLICATION.EncryptionKey_Cookies, "cfmx_compat", "hex" ) />
							<cfcookie name="RememberMe" value="#strRememberMe#" expires="never" />
							
						</cfif>
						<!--- $token_url = "https://graph.facebook.com/oauth/access_token?client_id=".$appid."&client_secret=".$secret."&grant_type=fb_exchange_token&fb_exchange_token=".$token; --->
						
                        <cfset Session.UserName = #pData.name#>
						<cfset Session.CONSUMEREMAIL = TRIM(pData.email) />
						<cfset Session.resetPasswordEveryTime = 0 />
		                <cfset Session.CPPPWD = '' />
						<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG,FBUSERID, UserName, EmailAddress") />
						<cfset QueryAddRow(dataout) />
						<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "REASONMSG", "Login Success! ") />
						<cfset QuerySetCell(dataout, "FBUSERID", #pData.id#) />
						<cfset QuerySetCell(dataout, "UserName", #pData.name#) />
						<cfset QuerySetCell(dataout, "EmailAddress", #pData.email#) />
					</cfif>				
				</cfif>
			<cfelse>
				<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -4) />
				<cfset QuerySetCell(dataout, "REASONMSG", "Failed Server Level Validation") />
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
				<cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="validateContentTwitter" access="remote" output="true" hint="add contact into rxmultilist when signin by account twitter">
		<cfargument name="accessToken" TYPE="string" required="no" default="0" />
		<cfargument name="accessSecret" TYPE="string" required="no" default="0" />
		<cfargument name="userIdTwitter" TYPE="string" required="no" default="0" />
		<cfargument name="UserName" TYPE="string" required="no" default="0" />
		<cfargument name="email" TYPE="string" required="no" default="0" />
		<cfargument name="inpUUID" TYPE="string" required="no" default="0" />
		<cfset access = accessToken & '---' & accessSecret>
		<cfset dataout = ''>
		<cftry>
			<CFQUERY name="getUser" datasource="#Session.DBSourceEBM#">
				SELECT 
					UserId_int, 
					ContactTypeId_int,
					ContactString_vch
				FROM 
					simplelists.rxmultilist 
				WHERE 
					ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userIdTwitter#"> 
				AND
					ContactTypeId_int = 5
			</CFQUERY>
			<cfif getUser.RecordCount GT 0>
				<CFQUERY name="UpdateContact" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplelists.rxmultilist
					SET
						socialToken_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#access#">,
						LastUpdated_dt = NOW(),
						LastAccess_dt = NOW()
					WHERE
						ContactTypeId_int = 5
					AND
						ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userIdTwitter#">
					AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#getUser.UserId_int#">
						
				</CFQUERY>
			<cfelse>
				<CFQUERY name="writeUser" datasource="#Session.DBSourceEBM#">
					INSERT INTO simplelists.rxmultilist 
					(UserId_int, ContactTypeId_int, TimeZone_int, Created_dt,LastAccess_dt, ContactString_vch,LocationKey1_vch, LocationKey2_vch, LocationKey3_vch, SourceKey_vch,grouplist_vch, LastName_vch, FirstName_vch,socialToken_vch, CPPID_vch)
	
						SELECT 
						UserId_int, 5,
						0,
						NOW(),NOW(),
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userIdTwitter#">,
						'','','',
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#email#">, 
						'0,',
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserName#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UserName#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#access#">,
						<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#email#">
						FROM 
							simplelists.customerpreferenceportal
						WHERE 
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpUUID#">
				</CFQUERY>
			</cfif>

				<cfset Session.CPPPWD = 'TWITTER#generatePassword()#' />

				<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />

				<cfset QuerySetCell(dataout, "TYPE", 1) />
				<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				
			<cfcatch TYPE="any">
				<cfset dataout =  QueryNew("RXRESULTCODE, REASONMSG, TYPE, MESSAGE, ERRMESSAGE") />
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE", -5) />
				<cfset QuerySetCell(dataout, "REASONMSG", "general CF Error") />
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			</cfcatch>
		</cftry>
		<cfreturn dataout/>
	</cffunction>
</cfcomponent>
