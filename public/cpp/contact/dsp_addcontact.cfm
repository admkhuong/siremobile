
<cfoutput>

<div id='AddNewCPPListmainnavigator' class="RXForm">

	<div id="LeftMenu" style="float: left; width: 49%;">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Add a Contact to the List</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "contact-new-icon_web" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></div>

		<div style="margin: 10px 5px 10px 20px; line-height: 20px;">
         	<ul>
         		<li>Choose a contact type from the drop down list.</li>
                <li>Enter a contact string.</li>

                <li>Press "Add" button when ready.</li>
                <li>Press "Cancel" button when you do not want to Add contact.</li>
            </ul>
        </div>
        
	</div>
    
    <div id="RightStage" style="float: left; width: 49%;">

        <form id="AddCPPListForm" name="AddCPPListForm" action="" method="POST">
        
         	<input TYPE="hidden" name="INPUserId" id="INPUserId" value="#Session.cppuserid#" />
        	<input TYPE="hidden" name="INPCPPID" id="INPCPPID" value="#Session.consumeremail#" />
			<input TYPE="hidden" name="INPgrouplist" id="INPgrouplist" value="#Session.cppgrouplist#" />
			<br>
                <label>Group Type
                <span class="small"></span>
                </label>
				<select id="inpGroupType" name="inpGroupType" multiple="multiple">
				</select>
                <BR>	
                <label>Content type
                <span class="small"></span>
                </label>
				<select id="inpContactType" name="inpContacttype">
					<option value="1">Voice</option>
					<option value="2">E-mail</option>
					<option value="3">SMS</option>
				</select>
                <BR>
				<label>Contact String
                <span class="small"></span>
                </label>
                <input TYPE="text" name="inpContactString" id="inpContactString" size="265" class="ui-corner-all" /> 
                <BR> 
                <!--- <label>User Name
                <span class="small">Select username</span>
                </label>
				<select name="inpCPPListUserId" id="inpCPPListUserId" class="ui-corner-all" onchange="loaduser($(this).val())">
					<cfif GetUserData.RecordCount GT 0>
						<cfloop array="#GetUserData.USERACCOUNTDATA#" index="getUser">
								<option value="#getUser[1]#">#getUser[2]#</option>
						</cfloop>
					</cfif>
				</select>
                <input TYPE="text" name="inpCPPListUserId1" id="inpCPPListUserId1" size="265" class="ui-corner-all" onkeypress="return handleKeyPress(event);" disabled/> 
                <BR> --->
                
                <!--- <label>Primary Link
                <span class="small"></span>
                </label>
                <input TYPE="text" name="inpCPPListPrimary" id="inpCPPListPrimary" size="265" class="ui-corner-all" onkeypress="return handleKeyPress(event);"/> 
                <BR> 
                <label>Group List
                <span class="small">List group ID</span>
                </label>
                <input TYPE="text" name="inpCPPListGroup" id="inpCPPListGroup" size="265" class="ui-corner-all" onkeypress="return handleKeyPress(event);"/> 
                <BR> 
                <label>Password
                <span class="small"></span>
                </label>
                <input TYPE="password" name="inpCPPListPassword" id="inpCPPListPassword" size="265" class="ui-corner-all" onkeypress="return handleKeyPress(event);"/> 
                <BR>--->
                <button id="AddNewCPPListButton" TYPE="button" class="ui-corner-all">Add</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                        
                <div id="loadingDlgmainnavigator" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
        </form>
    
    </div>

</div>
<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="#rootUrl#/#PublicPath#/cpp/js/newcontact.js"></script>
<script>
$(function () {


});
</script>
</cfoutput>
