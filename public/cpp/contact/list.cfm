<cfparam name="CurrSelectedGroupId" default="0" />
<cfparam name="inpCPPUUID" default="0" />

<cfset RetValCPPContactData = "">
<cfset RetValCPPGroupData = "">


<!--- Details - This allows single group CPPs to skip drop down controls --->
<!--- Do Group Search here so results can be used in Javascript--->
<cfset GroupSelect = "">
<cfset GroupOptions = "">
<cfset ProgramDescription = "">
                                               
<cftry>
                    	
	<cfset GroupOptions = "">
    <cfset ProgramDescription = "">
    
    <cfinvoke 
     component="#cfcPath#.cpp"
     method="GetCPPGroupData"
     returnvariable="RetValCPPGroupData">                         
        <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
    </cfinvoke>  
                        
    <!--- Check for success --->
    <cfif RetValCPPGroupData.RXRESULTCODE LT 1>
        <cfthrow MESSAGE="RetValCPPGroupData failed to get groups." TYPE="Any" detail="" errorcode="-1">               
    </cfif>
        
    <cfif RetValCPPGroupData.RecordCount GT 2>
               
        <cfset ProgramDescription = "Choose an area of interest in the drop down menu just above.">
       
        <cfloop query="RetValCPPGroupData">
        
            <cfif CurrSelectedGroupId EQ RetValCPPGroupData.GROUPID> 	
                <cfset GroupOptions = GroupOptions & '<option value="' & RetValCPPGroupData.GROUPID & '" SELECTED class="">' & RetValCPPGroupData.GROUPNAME & '</option>'>
                <cfset ProgramDescription = RetValCPPGroupData.GROUPNAME>
            <cfelse>
                <cfset GroupOptions = GroupOptions & '<option value="' & RetValCPPGroupData.GROUPID & '" class="">' & RetValCPPGroupData.GROUPNAME & '</option>'>
            </cfif>
                                
        </cfloop>
        
        <cfset GroupSelect = "  <h3 style='width:350px;' align='left'>Choose an Area of Interest</h3>
                                <select name='inpCPPGroupId' id='inpCPPGroupId' style='width:600px; display:inline; margin-bottom:8px;' class=''>
                                    #GroupOptions#
                                </select> ">
        
    <cfelseif RetValCPPGroupData.RecordCount EQ 2 >
    	    
        <!--- Set current area of interest to choice two - Choice one is always choose by default --->
        <cfset CurrSelectedGroupId = RetValCPPGroupData.GROUPID[2]>                           
        <cfset ProgramDescription = RetValCPPGroupData.GROUPNAME[2]>
        
        <cfif CurrSelectedGroupId EQ "" OR CurrSelectedGroupId LT 0>
      		<cfthrow MESSAGE="RetValCPPGroupData failed to get groups." TYPE="Any" detail="" errorcode="-1">
        </cfif>
               
    <cfelse>
        <cfthrow MESSAGE="RetValCPPGroupData failed to get groups." TYPE="Any" detail="" errorcode="-1"> 
    </cfif>

	<cfcatch type="any">
		<cfset GroupSelect = "Error loading areas of interest to choose From - The system administrator has been notified. Please try again later. " >
	</cfcatch>                
</cftry>         

<cfif TRIM(Session.CPPStyleTemplate_vch) NEQ ""> 
	<cfset CPPStyleTemplate = TRIM(Session.CPPStyleTemplate_vch)>
<cfelse>	    
      <cfsavecontent variable="CPPStyleTemplate">
	    <cfinclude template="../css/cpp.css">
	  </cfsavecontent>
</cfif>                            
                            		

<!--- CSS Section--->
<cfsavecontent variable="cppCss">

	<!--- http://css-tricks.com/resolution-specific-stylesheets/ --->
		<style>
		
			<cfoutput>
				@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
	 		</cfoutput>
			
        </style>
    
    
    	<cfoutput>#CPPStyleTemplate#</cfoutput>	
    
        
	    <style media="screen" type="text/css">
	        	<!--- .CPPSocialLikeOption{
	                padding-top: 50px;
	                margin-bottom: 10px;
	                margin-right: 15px;
	                width: 100px;
	            }
	            
	            #CPPLikeBySocial{
	            padding-top: 10px;
	            } --->
	        
	            #fb-count {
					padding-top: 10px;
	                width: 79px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	            
	            #fb-no-count {
					padding-top: 10px;
	                width: 52px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	            
	            #t-count {
					padding-top: 10px;
	                width: 76px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	            
	            #t-no-count {
					padding-top: 10px;
	                width: 60px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	            
	            #gplus-count {
					padding-top: 10px;
	                width: 59px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	            
	            #gplus-no-count {
					padding-top: 10px;
	                width: 32px;
	                overflow: hidden;
	                height: 20px;
	                float:right;
	                display:inline;
	                margin-left: 15px;
	            }
	    
	    </style>
    
</cfsavecontent>

<cfsavecontent variable="cppJs">
	<cfoutput>
		<script type="text/javascript">
			/* Config */
			var rootUrl = "#rootUrl#";
			var publicPath = "#PublicPath#";
			var sessionPath = "#SessionPath#";	
		</script>
		
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.maskedinput-1.3.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.multiselect.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jgrowl.js"></script>
		<script src="https://connect.facebook.net/en_US/all.js"></script>
	</cfoutput> 
    
    
    <script type="text/javascript">
		var Facebook_URL = '<cfoutput>#APPLICATION.Facebook_URL#</cfoutput>';
		var Facebook_URL_PARAM = '<cfoutput>#APPLICATION.Facebook_URL_PARAM#</cfoutput>';		
		var Facebook_AppID = '<cfoutput>#APPLICATION.Facebook_AppID#</cfoutput>'; 
		
		$(function() {
	

			$("#inpCPPGroupId").change(function() 
			{ 
				
				var ParamStr = "";
		
				<cfoutput>							
					ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#&CurrSelectedGroupId=" + $("##inpCPPGroupId").val();
					window.location = "#rootUrl#/#PublicPath#/CPP/home" + ParamStr;
				</cfoutput>	
			});						
			
				
			$(".del_CPPContactRow").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$(".del_CPPContactRow").click( function() { DeleteCPPContact($(this).attr('rel'), $(this).attr('rel3')); } );
			
			
			<!--- Number of fields--->
			$("#CPPEnterContact").hide();	
					
			$("#CPPStartAdd").click(function(){
				
				$("#CPPEnterContact").show();
				
				return;
							
			});
					
					
			$("#CPPEnterContactOK").click(function(){
				
				EnrollCPPContact($("#CPPCurrentContactString").val(), $("input[name='CPPContactTypeSelect']:checked").val())
			});
			
			$("#CPPEnterContactCancel").click(function(){
				$("#CPPEnterContact").hide();
			});
			
			<!--- Allow current end user to sign out--->
			$("#CPPSignOut").click(function(){
				<!---FB.logout();--->
				var ParamStr = "";
	
				<cfoutput>							
					ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
					window.location = "#rootUrl#/#PublicPath#/CPP/signin.cfm" + ParamStr;
				</cfoutput>	
				
			});
			
			
			<!--- Set checked radio button in add contact display to first no display none class --->
			if( $(".CPPCRVOICE").css("display") == "none"  )			
				if( $(".CPPCRSMS").css("display") == "none"  )				
					if( $(".CPPCREMAIL").css("display") == "none"  )					
						; 
					else
						<!--- Check emails if turned on--->
						$('input:radio[name=CPPContactTypeSelect]').filter('[value=2]').attr('checked', true); 	
				else				
					<!--- Check SMS if turned on--->
					$('input:radio[name=CPPContactTypeSelect]').filter('[value=3]').attr('checked', true); 	
			else			
				<!--- Check phones if turned on--->
				$('input:radio[name=CPPContactTypeSelect]').filter('[value=1]').attr('checked', true); 
			
			
			<!--- Details - Change instructions based on desired contact type --->
			$("input[name='CPPContactTypeSelect']").change(function(){
										
				if ($("input[name='CPPContactTypeSelect']:checked").val() == '1')
				{
					$("#CPPEnterDesc").html('Enter your Phone Number here');			
				}
				else if ($("input[name='CPPContactTypeSelect']:checked").val() == '2')
				{
					$("#CPPEnterDesc").html('Enter your eMail Address here');	
				}
				else if ($("input[name='CPPContactTypeSelect']:checked").val() == '3')
				{
					$("#CPPEnterDesc").html('Enter your SMS Capable Phone Number here');	
				}
			<!---	else if ($("input[name='CPPContactTypeSelect']:checked").val() == '4')
				{
					$("#CPPEnterDesc").html('Enter your Facebook login name here');	
				}
				else if ($("input[name='CPPContactTypeSelect']:checked").val() == '5')
				{
					$("#CPPEnterDesc").html('Enter your Twitter login name here');	
				}--->
				
				
			});
			
			<!--- Details - Reset display based on cached radio button being checked still after page refresh--->
			if ($("input[name='CPPContactTypeSelect']:checked").val() == '1')
			{
				$("#CPPEnterDesc").html('Enter your Phone Number here');			
			}
			else if ($("input[name='CPPContactTypeSelect']:checked").val() == '2')
			{
				$("#CPPEnterDesc").html('Enter your eMail Address here');	
			}
			else if ($("input[name='CPPContactTypeSelect']:checked").val() == '3')
			{
				$("#CPPEnterDesc").html('Enter your SMS capable Phone Number here');	
			}
		<!---	else if ($("input[name='CPPContactTypeSelect']:checked").val() == '4')
			{
				$("#CPPEnterDesc").html('Enter your Facebook here');	
			}
			else if ($("input[name='CPPContactTypeSelect']:checked").val() == '5')
			{
				$("#CPPEnterDesc").html('Enter your Twitter login name here');	
			}--->
								
				
			$("#loadingDlgCPPPublicProcessing").hide();	
			
			$("#tab-panel-CPPPublic").toggleClass("ui-tabs-hide");
			
			<!--- Turned off until all is working for demo on Tues--->
			<!---bb_init_facebook();--->
			
			if( $(".CPPSocialLikeOption").css("display") != "none"  )
				bb_init_facebook();		
						
			
			<!--- 
			For getting CPP Default message to share on social network
			GetCPPDetailData();
			--->
		});
		
		
		
		
		function Left(str, n){
			if (n <= 0)
				return "";
			else if (n > String(str).length)
				return str;
			else
				return String(str).substring(0,n);
		}
		
		function Right(str, n){
			if (n <= 0)
			   return "";
			else if (n > String(str).length)
			   return str;
			else {
			   var iLen = String(str).length;
			   return String(str).substring(iLen, iLen - n);
			}
		}
					
		<!--- Feature --->	
		function DeleteCPPContact(inpContactString, inpContactType)
		{	
			<!--- Get currently selected phone strings --->	
			<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
			
			<!--- Dont go overboard on displaying too much info --->
			var displayList =  Left(inpContactString, 50);
			
			if(String(inpContactString).length > 50)
				displayList = displayList + " (...)";
		
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			$.alerts.confirm( "About to remove\n(" + displayList + ") \n\nAre you absolutely sure?", "About to remove contacts from the current program.", function(result) { if(!result){$("#loadingDlgCPPPublicProcessing").hide(); return;}else{	
				
				  $("#loadingDlgCPPPublicProcessing").show();		
						
				  $.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/contact.cfc?method=RemoveGroupContacts&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(inpContactString), INPCONTACTTYPEID: inpContactType, inpCPPUUID : <cfoutput>'#inpCPPUUID#'</cfoutput>, INPGROUPID : <cfoutput>'#CurrSelectedGroupId#'</cfoutput> },					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
					success:
					  
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d2, textStatus, xhr ) 
					{
						
						<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
						var d = eval('(' + xhr.responseText + ')');
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									<!--- If session Expired - redirect to sign in--->
									if(CurrRXResultCode == -2)
									{
										var ParamStr = "";
		
										<cfoutput>							
											ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
											window.location = "#rootUrl#/#PublicPath#/CPP/signin.cfm" + ParamStr;
										</cfoutput>										
									}							
									else								
									if(CurrRXResultCode == 1)
									{					
										<!--- Remove current row from list--->		
										
										<!--- If row count for current contact type is now 0 then add in a placeholder - details count--->	
										
										var ParamStr = "";
		
										<cfoutput>							
											ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#&CurrSelectedGroupId=" + $("##inpCPPGroupId").val();
											window.location = "#rootUrl#/#PublicPath#/CPP/home" + ParamStr;
										</cfoutput>	
									
									}
									else
									{
										$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });								
									}
																												
									$("#loadingDlgCPPPublicProcessing").hide();
							
								}
								else
								{<!--- Invalid structure returned --->	
									$("#loadingDlgCPPPublicProcessing").hide();
								}
							}
							else
							{<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}										
					} 		
							
				});
		
			 }  } );<!--- Close alert here --->
		
		
			return false;
		}
		
		<!--- Feature --->	
		function EnrollCPPContact(inpContactString, inpContactType)
		{	
			<!--- Get currently selected phone strings --->	
			<!---var inpContactString = $("#MCStringList").jqGrid('getGridParam','selarrrow');--->
			
			<!--- Dont go overboard on displaying too much info --->
			var displayList =  Left(inpContactString, 50);
			
			if(String(inpContactString).length > 50)
				displayList = displayList + " (...)";
				
				
				  $("#loadingDlgCPPPublicProcessing").show();		
						
				  $.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/contact.cfc?method=AddContactToGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(inpContactString), INPCONTACTTYPEID: inpContactType, inpCPPUUID : <cfoutput>'#inpCPPUUID#'</cfoutput>, INPGROUPID : <cfoutput>'#CurrSelectedGroupId#'</cfoutput> },					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
					success:
					  
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d2, textStatus, xhr ) 
					{
						
						<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
						var d = eval('(' + xhr.responseText + ')');
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									<!--- If session Expired - redirect to sign in--->
									if(CurrRXResultCode == -2)
									{
										var ParamStr = "";
		
										<cfoutput>							
											ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
											window.location = "#rootUrl#/#PublicPath#/CPP/signin.cfm" + ParamStr;
										</cfoutput>										
									}		
									else
									if(CurrRXResultCode == 1)
									{					
										<!--- refresh List --->
										
										var ParamStr = "";
		
										$("#CPPEnterContact").hide();	
				
										<!--- Clear value after OK--->
										$("#CPPCurrentContactString").val("");
									
										<cfoutput>							
											ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#&CurrSelectedGroupId=" + $("##inpCPPGroupId").val();
											window.location = "#rootUrl#/#PublicPath#/CPP/home" + ParamStr;
										</cfoutput>	
								
									}
									else
									{
										$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });								
									}
																												
									$("#loadingDlgCPPPublicProcessing").hide();
							
								}
								else
								{<!--- Invalid structure returned --->	
									$("#loadingDlgCPPPublicProcessing").hide();
								}
							}
							else
							{<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}										
					} 		
							
				});
				
			return false;
		} 
			
	function bb_init_facebook() {
		
		var curLoc = window.location;
      // Load the SDK Asynchronously
      <!----
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));
		--->
		FB.init({ 
			appId: Facebook_AppID,
			cookie: true,
			status: true,
			xfbml: true,
			/*channelUrl: 'http://www.babblesphere.com/Session/account/'*/
			/* Need channel to work with IE9 */
				/*channelUrl: 'http://www.babblesphere.com/channel.html'*/
			
			
			/* For MSB */
			channelUrl: 'http://babblesphere.seta:8500/BabbleSphere/channel.html'
			/* For SETA */
			/*channelUrl: 'http://babblesphere.seta:8500/BabbleSphere/channel.html'*/
	
		});
	}
	
	<!--- Get CPP message to share --->
	function GetCPPDetailData()
	{
		$.ajax({
			type: "POST",
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/cpp.cfc?method=GetCPPDetailData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  {inpCPPUUID : '<cfoutput>#inpCPPUUID#</cfoutput>', GroupId: $("#inpCPPGroupId").val()},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{	 																							
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = parseInt(d.DATA.RXRESULTCODE[0]);	
						
						if(CurrRXResultCode == 1)
						{					
							console.log(d.DATA.DESC[0]+"\n"+d.DATA.PRIMARYLINK[0]+"\n"+d.DATA.CPPMESSAGE[0]);//Test
						}
						else
						{
							console.log(d.DATA.MESSAGE[0]);	
						}
																									
					}
					else
					{<!--- Invalid structure returned --->	
						jAlert("Error.", "Invalid structure returned");
					}
				}
				else
				{<!--- No result returned --->
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
			} 		
		});
		
		return false;
	}
	
		
	<!--- Auto click Login button when press Enter key --->
	function submitenter(myfield,e) {
	  	var keycode;
	 	if (window.event) keycode = window.event.keyCode;
	  	else if (e) keycode = e.which;
	  	else return true;
	
		if (keycode == 13)
     	{
        	EnrollCPPContact($("#CPPCurrentContactString").val(), $("input[name='CPPContactTypeSelect']:checked").val());
        	return false;
        }
		else
			return true;
	}
	
	</script>
    
</cfsavecontent>


<!--- Main Content--->
<cfsavecontent variable="cppContent">
	<cfoutput>        
        
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)---> 
    <div id="tab-panel-CPPPublic" class="ui-tabs-hide" style="position:relative; overflow: hidden;">
                 
        <div id="CPPEndUserContainer" align="center">
        
          	<div id="CPPSelectGroupRow">#GroupSelect#</div>
                                                 
            
            <div id="CPPEndUserStage">
               
                <div style="position:relative;"> 
                    <div id="CPPEnterContact" align="center">
                       
                        <div id="CPPContactTypeSelectDiv" style="height:15px; min-height:50px; text-align:left; float:left;">
                            <h1>Preferred Contact Method</h1>  
                            <p>
                            <div class='CPPCRVOICE'><input name="CPPContactTypeSelect" type="radio" value="1"/> Phone</div>
                            <div class='CPPCRSMS'><input name="CPPContactTypeSelect" type="radio" value="3" /> SMS  </div>
                            <div class='CPPCREMAIL'><input name="CPPContactTypeSelect" type="radio" value="2" /> eMail  </div>
							<!---<BR />
                            <input name="CPPContactTypeSelect" type="radio" value="4"/> Facebook  <BR />
                            <input name="CPPContactTypeSelect" type="radio" value="5"/> Twitter --->
                            </p>
                        </div>       
                        
                        <div id="CPPContactTypeSelectDiv" style="height:15px; min-height:50px; text-align:left; float:right;">
                            <h1 id="CPPEnterDesc">Enter Your Phone Number Here</h1>
                            <p>
                            <input type="text" id="CPPCurrentContactString" name="CPPCurrentContactString" value="" class="ui-corner-all" style="width:350px; text-align:left;"  onKeyPress="return submitenter(this,event)"/>
                            </p>                                                                     
                            <button id="CPPEnterContactOK">OK</button> <button id="CPPEnterContactCancel">Cancel</button>
                        </div> 
                    </div>
                </div>
                                                  
                <div id="CPPGroupDescription" class="GradientBackground">
                	<h3 align="left">Current Area of Interest</h3>
                    <p align="left">#ProgramDescription#</p>    
                </div>
                
                <div id="CPPContactsList">
                
	                <div id="loadingDlgCPPPublicProcessing" style="display:inline; position:absolute; top:0px; left:0px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>


					<cfset ContactData = "">
                    <cfset ContactDataType1 = "">
                    <cfset ContactDataType2 = "">
                    <cfset ContactDataType3 = "">
                    
                    <cfset CountContactDataType1 = 0>
                    <cfset CountContactDataType2 = 0>
                    <cfset CountContactDataType3 = 0>
                    
                    <cftry>
                    
                        <cfif CurrSelectedGroupId GT 0>
                                                
                            <cfinvoke 
                             component="#cfcPath#.contact"
                             method="GetCPPContactData"
                             returnvariable="RetValCPPContactData">                         
                                <cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#"/>
                                <cfinvokeargument name="INPGROUPID" value="#CurrSelectedGroupId#"/>
                            </cfinvoke>  
                                                                                    
                            <cfif RetValCPPContactData.RXRESULTCODE LT 1>
                                
                                <cfthrow MESSAGE="RetValCPPContactData failed to get groups." TYPE="Any" detail="" errorcode="-1">               
                            
                            <cfelse>
                            
                                <cfloop query="RetValCPPContactData">
                                    
                                    <cfswitch expression="#RetValCPPContactData.CONTACTTYPEID#">
                                        
                                        <!--- Phone --->
                                        <cfcase value="1">
                                            <cfset CountContactDataType1 = CountContactDataType1 + 1>    
                                            
                                            <cfset ContactDataType1 = ContactDataType1 & "<div class='CPPContactRow CPPCRVOICE' rel1='#RetValCPPContactData.CONTACTSTRING#' rel2='#RetValCPPContactData.CONTACTTYPEID#'>" >											                                
                                            <cfset ContactDataType1 = ContactDataType1 & "<div class='CPPContact'>#RetValCPPContactData.CONTACTSTRING#</div>">
                                            
                                            <!--- Add a Delete Button for each row--->
                                            <cfset ContactDataType1 = ContactDataType1 & "<div class='CPPContactOptions'><img class='del_CPPContactRow ListIconLinks' rel='#RetValCPPContactData.CONTACTSTRING#' rel3='#RetValCPPContactData.CONTACTTYPEID#' src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'></div>">
                                            
                                            <cfset ContactDataType1 = ContactDataType1 & "</div>">                                        
                                                                                    
                                        </cfcase>
                                        
                                        <!--- eMail --->
                                        <cfcase value="2">
                                            <cfset CountContactDataType2 = CountContactDataType2 + 1>
                                            
											<cfset ContactDataType2 = ContactDataType2 & "<div class='CPPContactRow CPPCREMAIL' rel1='#RetValCPPContactData.CONTACTSTRING#' rel2='#RetValCPPContactData.CONTACTTYPEID#'>" >											                                
                                            <cfset ContactDataType2 = ContactDataType2 & "<div class='CPPContact'>#RetValCPPContactData.CONTACTSTRING#</div>">
                                            
                                            <!--- Add a Delete Button for each row--->
                                            <cfset ContactDataType2 = ContactDataType2 & "<div class='CPPContactOptions'><img class='del_CPPContactRow ListIconLinks' rel='#RetValCPPContactData.CONTACTSTRING#' rel3='#RetValCPPContactData.CONTACTTYPEID#' src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'></div>">
                                            
                                            <cfset ContactDataType2 = ContactDataType2 & "</div>">  
                                            
                                        </cfcase>
                                        
                                        <!--- SMS --->
                                        <cfcase value="3">
                                            <cfset CountContactDataType3 = CountContactDataType3 + 1>
                                            
                                            <cfset ContactDataType3 = ContactDataType3 & "<div class='CPPContactRow CPPCRSMS' rel1='#RetValCPPContactData.CONTACTSTRING#' rel2='#RetValCPPContactData.CONTACTTYPEID#'>" >											                                
                                            <cfset ContactDataType3 = ContactDataType3 & "<div class='CPPContact'>#RetValCPPContactData.CONTACTSTRING#</div>">
                                            
                                            <!--- Add a Delete Button for each row--->
                                            <cfset ContactDataType3 = ContactDataType3 & "<div class='CPPContactOptions'><img class='del_CPPContactRow ListIconLinks' rel='#RetValCPPContactData.CONTACTSTRING#' rel3='#RetValCPPContactData.CONTACTTYPEID#' src='../../public/images/icons16x16/delete_16x16.png' width='16' height='16'></div>">
                                            
                                            <cfset ContactDataType3 = ContactDataType3 & "</div>">  
                                                                            
                                        </cfcase>
                                                      
                                    </cfswitch>
                                                                                        
                                </cfloop>                            
                            
                            	<cfif CountContactDataType1 LT 1><cfset ContactDataType1 = "<div class='CPPCRVOICE'>none</div>"></cfif>
                                <cfif CountContactDataType2 LT 1><cfset ContactDataType2 = "<div class='CPPCREMAIL'>none</div>"></cfif>
                                <cfif CountContactDataType3 LT 1><cfset ContactDataType3 = "<div class='CPPCRSMS'>none</div>"></cfif>
                            
                            	<!--- Details - Plural or not --->
                            	<cfif CountContactDataType1 GT 1>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCRVOICE'><h4>Phones</h4></div>" & ContactDataType1>								
                                <cfelse>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCRVOICE'><h4>Phone</h4></div>" & ContactDataType1>								
                                </cfif>
                                
                                <cfif CountContactDataType2 GT 1>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCREMAIL'><h4>eMails</h4></div>" & ContactDataType2>								
                                <cfelse>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCREMAIL'><h4>eMail</h4></div>" & ContactDataType2>								
                                </cfif>
                                
                                <cfif CountContactDataType3 GT 1>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCRSMS'><h4>SMSs</h4></div>" & ContactDataType3>								
                                <cfelse>                                
                                	<cfset ContactData = ContactData & "<BR /><div class='CPPCRSMS'><h4>SMS</h4></div>" & ContactDataType3>								
                                </cfif>                                                                                  	
                            
                             </cfif>
                             
                        <cfelse>
                            
                           <cfset ContactData = "First - Choose an area of interest in the drop down menu just above.">                
                        
                        </cfif>
                    
                
	                <cfcatch type="any">
                    	Error loading contact data - The system administrator has been notified. Please try again later.                        
                    </cfcatch>                
                    </cftry>    
                    
                    <div id="CPPContactsHeader">
                    	<div style="float:left;">
                        	<h3 align="left" style="display:inline;">Contacts For: </h3>
                            <p><a id="CPPStartAdd">Add</a></p>
                        </div>
                        
                        <div style="float:left;">
	                        <p style="margin-left:10px;">#TRIM(Session.CONSUMEREMAIL)#</p>
                            <p style="margin-left:10px;"><a id="CPPSignOut">Sign Out</a></p>
                        </div>
                                        
                    </div>
                    
                   <!--- <BR />--->
                    
                    <div style="clear:both;">
	              
                  	  <p align="left">#ContactData#</p>

                   	</div>		
		            
                </div>
	            
				<cfset link = "#rootUrl#/#PublicPath#/cpp/home?inpCPPUUID=#inpCPPUUID#">
                <div id="CPPLikeBySocial">
                   
                    <!--- <div id="gplus-count">
						<g:plusone size="medium" href="#link#"></g:plusone>
                    </div> --->
                    
                    <div id="gplus-no-count">
						<g:plusone size="medium" annotation="none" href="#link#"></g:plusone>
                    </div>
                    
                    <!-- Place this render call where appropriate -->
                    <script type="text/javascript">
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script> 
					 
                    
                    <!--- <div id="t-count">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="#link#">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </div> --->
                    
                    <div id="t-no-count">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="#link#">Tweet</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>	
                    </div>
					
                     <!--- <div id="fb-count">
                        <iframe src="//www.facebook.com/plugins/like.php?href=#link#&amp;send=false&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px;" allowTransparency="true"></iframe>
                    </div> --->
                    
                    <div id="fb-no-count">
                        <iframe src="//www.facebook.com/plugins/like.php?href=#link#&amp;send=false&amp;layout="standard"&amp;show_faces=false&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px;" allowTransparency="true"></iframe>
                    </div>
                </div>
                        
            </div> <!--- CPPEndUserStage--->
                    
        </div>    <!---CPPEndUserContainer --->
        
	</div> <!--- anti FOUC div--->
    
    <!--- Debugging area - hide when done programming--->
<!--- 

    <BR />
    <BR />
    <BR />
    <BR />
    <BR />
	
	<cfdump var="#Session#">
    <cfdump var="#RetValCPPGroupData#">
    <cfdump var="#RetValCPPContactData#">
--->
    
	</cfoutput>
</cfsavecontent>


<!--- Insert Main content into defined OR default template--->
<cfsavecontent variable="cppFullContent">
	<cfoutput>
		<cfif len(Session.CPPTemplate)>
			#Session.CPPTemplate#
		<cfelse>
			<cfinclude template="../layout.cfm" />
		</cfif>
	</cfoutput>
</cfsavecontent>



<!--- Final combine and render of all outputs--->
<cfoutput>
	<cfset cppFullContent = Replace(cppFullContent, "</head>", "#cppCss#</head>") />
	<cfset cppFullContent = Replace(cppFullContent, "</body>", "#cppJs#</body>") />
	<cfset cppFullContent = Replace(cppFullContent, "{%cppContent%}", "#cppContent#") />
	#cppFullContent#
</cfoutput>

