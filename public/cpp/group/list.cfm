<cfquery name="getUser" datasource="#Session.DBSourceEBM#">
	SELECT UserId_int, UserName_vch FROM simpleobjects.useraccount WHERE UserId_int = 
	<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.USERID#">
</cfquery>
<cfif len(getUser.UserName_vch)>
<cfset welComeMsg = "Welcome #getUser.UserName_vch#" />
<cfelse>
<cfset welComeMsg = "Welcome #session.CONSUMEREMAIL#" />
</cfif>
<cfinvoke 
component="#cfcPath#.cpp"
method="Getgrouplist"
returnvariable="groupList"></cfinvoke>

<cfsavecontent variable="cppCss">
	<cfoutput>
		<link rel="stylesheet" type="text/css" media="all" href="#rootUrl#/#PublicPath#/js/jquery/css/cupertino/jquery-ui-1.8.17.custom.css" />
		<link rel="stylesheet" type="text/css" media="all" href="#rootUrl#/#PublicPath#/js/jquery/css/jquery.multiselect.css" />
		<link rel="stylesheet" type="text/css" media="all" href="#rootUrl#/#PublicPath#/js/jquery/css/jquery.jqgrid.css" />
		<link rel="stylesheet" type="text/css" media="all" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" />
		<link rel="stylesheet" type="text/css" media="all" href="#rootUrl#/#PublicPath#/cpp/css/style.css" />
	</cfoutput>
</cfsavecontent>


<cfsavecontent variable="cppContent">
	<cfoutput>
		<!--- View --->
		<div id="container" class="container">
			<div id="body" class="clf">
				<div id="sidebar">
					<div id="accordion">
						<h3 class="open">Your Account</h3>
						<div style="display: block;" class="content">
							<div style="padding: 0 0 10px 40px; margin-right: 10px; overflow: hidden">
								#welComeMsg#<br /><br />
								<a id="btnSignout" class="button" href="signout.cfm">Sign out</a>
							</div>
							<ul class="leftmenu">
								<li>
									<a href="#rootUrl#/#PublicPath#/cpp/embeded.cfm">Get embeded code</a>
								</li>
							</ul>
						</div>
						<h3 class="open">Group</h3>
						<div style="display: block;" class="content">
							<ul class="leftmenu">
								<li class="current"><a href="#rootUrl#/#PublicPath#/cpp/group/list.cfm">
										List
									</a></li>
							</ul>
						</div>
						<h3 class="open">Contact</h3>
						<div style="display: block;" class="content">
							<ul class="leftmenu">
								<li><a href="#rootUrl#/#PublicPath#/cpp/contact/list.cfm">
										All
									</a></li>
								<cfloop query="groupList">
									<li>
										<a href="#rootUrl#/#PublicPath#/cpp/contact/list.cfm?groupId=#groupList.id#">#groupList.name#</a>
									</li>
								</cfloop>
							</ul>
						</div>

					</div>
				</div>
				<div id="main">
					<div class="inner">
						<table id="groupList"><tbody><tr><td>&nbsp;</td></tr></tbody></table>
						<div id="pgrouplist"></div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="cppJs">
	<cfoutput>
		<script type="text/javascript">
			/* Config */
			var rootUrl = "#rootUrl#";
			var publicPath = "#PublicPath#";
			var sessionPath = "#SessionPath#";
		</script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.maskedinput-1.3.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.multiselect.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/i18n/grid.locale-en.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/cpp/js/general.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/cpp/js/group.js"></script>
	</cfoutput> 
</cfsavecontent>
			
<cfsavecontent variable="cppFullContent">
	<cfoutput>
		<cfif len(Session.CPPTemplate)>
			#Session.CPPTemplate#
		<cfelse>
			<cfinclude template="../layout.cfm" />
		</cfif>
	</cfoutput>
</cfsavecontent>

<cfoutput>
	<cfset cppFullContent = Replace(cppFullContent, "</head>", "#cppCss#</head>") />
	<cfset cppFullContent = Replace(cppFullContent, "</body>", "#cppJs#</body>") />
	<cfset cppFullContent = Replace(cppFullContent, "{%cppContent%}", "#cppContent#") />
	#cppFullContent#
</cfoutput>