<cfparam name="inpCPPUUID" default="0" />

<cfset StructClear(Session) />
<cfinclude template="../paths.cfm" />

<cfset message = ''>

<!---<cfif inpCPPUUID EQ -1>
	<cfif IsDefined("Cookie.UUID") is True> 
		<cfset inpCPPUUID = "#Cookie['UUID']#">
	</cfif>
</cfif>--->
<!---
<cfif inpCPPUUID NEQ 0>
	<cfinvoke 
		component="cfc.cpp"
		method="EnterCpp"
		returnvariable="dataout">
			<cfinvokeargument name="inpCPPUUID" value="#inpCPPUUID#">
	</cfinvoke>

	<cfif dataout.RXRESULTCODE GT 0>
		<script language="javascript">
			document.cookie = "inpCPPUUID=#inpCPPUUID#"; 
		</script>
		<cflocation url="#rootUrl#/#PublicPath#/cpp/signin.cfm">
		<!--- <cfheader name="Location" value="http://www.quackit.com/coldfusion/tutorial"> --->
	<cfelse>
		<!--- uuid not found --->
		<cfset message = 'UUID not found. Please input UUID!'>
	</cfif>
</cfif>
--->
<cfsavecontent variable="cppCss">

    <style>		
        <cfoutput>
            @import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
			@import url('#rootUrl#/#PublicPath#/cpp/css/cpp.css');
            @import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
            @import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
        </cfoutput>
    </style>	
        
</cfsavecontent>

<cfsavecontent variable="cppJs">
	<cfoutput>
		<script type="text/javascript">
			/* Config */
			var rootUrl = "#rootUrl#";
			var publicPath = "#PublicPath#";
			var sessionPath = "#SessionPath#";
			var INPUUID = '<cfoutput>#inpCPPUUID#</cfoutput>';
		</script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/cpp/js/general.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jgrowl.js"></script>
	</cfoutput>
    
     <!--- Details - if going to include custom JS - write as CFM so can document better during development - when stable make compacted version as pure .js--->
    <script type="text/javascript">
				
		$(function() {		
				
			$("#CPPPortalSignIn").click(function(){				
								
				CPPPortalSignIn();
				
			});	
						
		});
		
		<!--- Details - user input will teed trim before part of URL param--->
		function trim(s)
		{
			return rtrim(ltrim(s));
		}
		
		function ltrim(s)
		{
			var l=0;
			while(l < s.length && s[l] == ' ')
			{	l++; }
			return s.substring(l, s.length);
		}
		
		function rtrim(s)
		{
			var r=s.length -1;
			while(r > 0 && s[r] == ' ')
			{	r-=1;	}
			return s.substring(0, r+1);
		}


		
		function CPPPortalSignIn()
		{									
			$("#loadingDlgCPPPublicProcessing").show();		
					
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/cpp.cfc?method=EnterCpp&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  {inpCPPUUID : $("#inpCPPUUID").val() },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
					
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/index.cfm" + ParamStr;
								</cfoutput>										
							}
							else							
							if(CurrRXResultCode == 1)
							{					
								<!--- refresh List --->
								
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=" + trim($("##inpCPPUUID").val());
									window.location = "#rootUrl#/#PublicPath#/CPP/SignIn.cfm" + ParamStr;
								</cfoutput>	
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });								
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;
			
		}
		
    </script>  
    
</cfsavecontent>



<cfsavecontent variable="cppContent">
	<cfoutput>
		<div id="container" class="container">
			<div id="content" class="clf">
				<div id="divEnterCPP" class="rxform2 ui-corner-all">
			<!---		<form id="frmEnterCPP" name="frmEnterCPP" action="#rootUrl#/#PublicPath#/cpp/cfc/cpp.cfc?method=EnterCpp&amp;returnformat=json&amp;queryformat=column&amp;_cf_nodebug=true&amp;_cf_nocache=true" method="post">
			--->			<fieldset>
							<legend>Enter your CPP!&nbsp;&nbsp;</legend>
							<p class="red">#message#</p>
							<p>
								<label for="enterCpp_uuid">Your CPP UUID</label>
                                <!--- Detail - auto populate if provided--->
								<cfif inpCPPUUID NEQ "0">
									<input type="text" id="inpCPPUUID" name="inpCPPUUID" value="#inpCPPUUID#" />
                                <cfelse>
                                	<input type="text" id="inpCPPUUID" name="inpCPPUUID" value="" />
                                </cfif>
							</p>
						</fieldset>
						<button id="CPPPortalSignIn">Enter your CPP</button>
                        
						<span id="enterCppLoading" style="display:none;">
							<img alt="Loading" class="loadingDlgRegisterNewAccount" src="#rootUrl#/#PublicPath#/images/loading-small.gif" width="20" height="20" />
						</span>					
				</div>
			</div>
		</div>	
	</cfoutput>
</cfsavecontent>


<cfsavecontent variable="cppFullContent">
	<cfoutput>
		<cfinclude template="layout.cfm" />
	</cfoutput>
</cfsavecontent>

<cfoutput>
	<cfset cppFullContent = Replace(cppFullContent, "</head>", "#cppCss#</head>") />
	<cfset cppFullContent = Replace(cppFullContent, "</body>", "#cppJs#</body>") />
	<cfset cppFullContent = Replace(cppFullContent, "{%cppContent%}", "#cppContent#") />
	#cppFullContent#
</cfoutput>