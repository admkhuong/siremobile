function fmtContactType(cellvalue, options, rowObject) {
	// format the cellvalue to new format
	var contactType = [
		"Voice", "e-mail", "SMS"
	];
	return contactType[cellvalue - 1];
}

function beforeShowForm(form) {
	$('select[name="groups"]').multiselect({
		noneSelectedText: 'Select groups',
		selectedList: 1,
		minWidth: 160
	});
}
<!--- add new contact --->
var CreateAddContactDialog = 0;
function AddContact() {
	if(CreateAddContactDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		CreateAddContactDialog.remove();
		CreateAddContactDialog = 0;
		
	}
	var $loading = $(rootUrl + '/' + publicPath + '/images/loading-small.gif" width="16" height="16">...Preparing');
					
	var ParamStr = '';

	CreateAddContactDialog = $('<div></div>').append($loading.clone());
	CreateAddContactDialog
		.load(rootUrl + '/' + publicPath + '/cpp/contact/dsp_addContact.cfm' + ParamStr)
		.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
		    title: 'Add New Contact',
			close: function() {   $(this).dialog('destroy'); $(this).remove(); },
			width: 800,
			height: 'auto',
			position: 'top',
		    closeOnEscape: false/* , 
		    buttons:
			[
				{
					text: "Save",
					click: function(event)
					{
						 request_update_contact(); 
						 return false; 
					}
				},
				{
					text: "Cancel",
					click: function(event)
					{
						CreateAddContactDialog.remove();	
						 return false; 
					}
				}
			] */

		});
	
	CreateAddContactDialog.dialog('open');	
	return false;
}

	function DelContactList(contactType, contactString) {
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: rootUrl + '/' + publicPath + '/cpp/cfc/contact.cfc?method=DeleteContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				contactType : contactType,
				contactString : contactString
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									gridReloadContactList();									
									return false;
										
								}
								else
								{
						            $.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Contact has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!");
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");	
						}
				}
			});	
		return false;
	}
	var CreateeditContactListDialog = 0;
	function editContactList(ContactType,ContactString) {

		if(CreateeditContactListDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateeditContactListDialog.remove();
			CreateeditContactListDialog.dialog('destroy');
			CreateeditContactListDialog.remove();
			CreateeditContactListDialog = 0;
			
		}
		var $loading = $(rootUrl + '/' + publicPath + '/images/loading-small.gif" width="16" height="16">...Preparing');
						
		var ParamStr = '';
		var ParamStr = "?inpContactString=" + encodeURIComponent(ContactString) + "&inpContactType=" + encodeURIComponent(ContactType);
		
		CreateeditContactListDialog = $('<div></div>').append($loading.clone());
		CreateeditContactListDialog
			.load(rootUrl + '/' + sessionPath + '/lists/dsp_editRXMuliListDialog.cfm' + ParamStr)
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
			    title: 'Edit Contact { ' + ContactString + ' }',
				close: function() {   $(this).dialog('destroy'); $(this).remove(); },
				width: 800,
				height: 'auto',
				position: 'top',
			    closeOnEscape: false, 
			    buttons:
				[
					{
						text: "Save",
						click: function(event)
						{
							 request_update_contact(); 
								
							 return false; 
						}
					},
					{
						text: "Cancel",
						click: function(event)
						{
							CreateeditContactListDialog.remove();	
							 return false; 
						}
					}
				]

			});
		
		CreateeditContactListDialog.dialog('open');	
		return false;
	}
	function request_update_contact() {
		$("#cx_rxmultilist_loading").show();
		$.ajax({
			type: "POST",
			url: rootUrl + '/' + sessionPath + "/cfc/MultiLists.cfc?method=UpdateAllContactData&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		
			data: $("#cx_rxmultilist_form").serializeArray(),
			dataType: "json", 
			success: function(d2, textStatus, xhr) {
				$("#cx_rxmultilist_loading").hide();
				var d = eval('(' + xhr.responseText + ')');
				if (typeof(d.DATA) != "undefined")
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					<!--- Check if variable is part of JSON result string --->
					if (typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						var CurrRXResultCode = parseInt(d.DATA.RXRESULTCODE[0]);
						switch (CurrRXResultCode)
						{
						case 1:
							<!--- OK --->
							jAlert("Data updated.", "Success!", function(result) 
							{
								CreateeditContactListDialog.remove();
								
							});
							break;
						case 2:
							<!--- Session is expired --->
							jAlert("Error while updating data.", d.DATA.MESSAGE[0]);
							break;
						default:
							<!--- Error response from server --->
							jAlert("Error while updating data.\n"+d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0]);
						}
					}
					else
					{
						<!--- Invalid structure returned --->
						jAlert("Error while updating data.", "Invalid response from server.");
					}
				}
				else
				{
					<!--- No result returned --->
					jAlert("Error while updating data.", "No response form remote server.");
				}
			}
		});
	}
function gridReloadContactList(){
	
	var ParamStr = '';
	var CurrSORD =  $("#contactList").jqGrid('getGridParam','sord');
	var CurrSIDX =  $("#contactList").jqGrid('getGridParam','sidx');
	var CurrPage = $('#contactList').getGridParam('page'); 

			
	if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
		
	if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
	
	$("#contactList").jqGrid('setGridParam',{url:rootUrl + "/" + publicPath + "/cpp/cfc/contact.cfc?method=GetContactData&returnformat=json&queryformat=column&inpSocialmediaFlag=0&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:CurrPage}).trigger("reloadGrid");

}
$(function() {
	jQuery("#contactList").jqGrid({
		url: rootUrl + "/" + publicPath + "/cpp/cfc/contact.cfc?method=GetContactData&groupId=" + groupId + "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		datatype: "json",
		jsonReader: {
			root: "ROWS",
			page: "PAGE",
			total: "TOTAL",
			records: "RECORDS",
			cell: "", // not used
			id: "0" // will default second column as ID
		/*
		 * , repeatitems: true, cell: "cell", id: "id"
		 */
		},
		
		rowNum: 30,
		rowList: [
			10, 20, 30
		],
		colNames: [
			"Groups", "Type", "Contact string", "Option"
		],
		colModel: [
			{
				name: 'groups',
				index: 'groups',
				// width: 100,
				hidden: true,
				viewable: true,
				resizable: false,
				editable: true,
				edittype: "select",
				editoptions: {
					multiple: true,
					size: 5,
					value: jsvGroups,
					dataInit: function(el) {
						/*
						 * $(el).multiselect({ noneSelectedText: 'Select groups',
						 * selectedList: 1, minWidth: 300 });
						 */
					}
				},
				editrules: {
					edithidden: true
				}
			}, {
				name: 'type',
				index: 'type',
				width: 80,
				formatter: fmtContactType,
				//hidden: true,
				editable: true,
				edittype: "select",
				editoptions: {
					value: "1:Voice;2:e-mail;3:SMS"
				},
				editrules: {
					edithidden: true
				},
				stype: "select",
				searchoptions: {
					value: ":All;1:Voice;2:e-mail;3:SMS"
				}
			}, {
				name: 'contactString',
				index: 'contactString',
				width: 140,
				editable: true,
				editoptions: {
					dataInit: function(el) {
						// $(el).mask("(999) 999-9999");
					}
				},
				editrules: {
					required: true
				},
				searchoptions: {
					sopt: [
						"eq", "ne", "bw", "bn", "ew", "en", "cn", "nc"
					]
				}
			},
			{
				name: 'option',
				index: 'option',
				width: 80,
				align: 'center',
				search: false
			}
		/*
		 * , { name: 'action', width: 60, fixed: true, sortable: false, resize:
		 * false, formatter: 'actions', formatoptions: { editformbutton: true,
		 * editbutton: false, keys: true } }
		 */
		],
	//	pager: "#pContactList",
		autowidth: true,
		height: 'auto',
		emptyrecords: "No contacts...",
		viewrecords: true,
		toppager: true,
		viewrecords: true,
		pgbuttons: true,
		sortname: 'Type',
		grouping: true,
		groupingView: {
			groupField: [
				'type'
			],
			groupColumnShow: [
				true
			]
		},
		caption: "",
		editurl: rootUrl + "/" + publicPath + "/cpp/cfc/contact.cfc?method=jqGridAction&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		onSelectRow: function(id) {
			// jQuery("#contactList").jqGrid('editGridRow', id, {});
			/*
			 * if (id && id !== clLastSelect) {
			 * jQuery('#contactList').jqGrid('restoreRow', clLastSelect);
			 * jQuery('#contactList').jqGrid('editRow', id, true); clLastSelect = id; }
			 */
		},
		loadComplete: function(data){
			$(".edit_Rowcontact").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
			$(".edit_Rowcontact").click( function() {
				editContactList($(this).attr('rel'), $(this).attr('rel2'));
				return false;
			});
			$(".del_Rowcontact").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	 		$(".del_Rowcontact").click( function() { 
	 			
	 			$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
				$.alerts.cancelButton = '&nbsp;No&nbsp;';		
				CurrRel = $(this).attr('rel');
				CurrRel2 = $(this).attr('rel2');
				jConfirm( "Are you sure you want to delete this contact?", "About to delete Contact.", function(result) { 
					if(result)
					{	
						DelContactList(CurrRel,CurrRel2);
					}
					return false;																	
				});
	 		} );	
		}
	});
	/*jQuery("#contactList").jqGrid('navGrid', "#pContactList", {
		add: true,
		addtitle: "Add new contact",
		edit: false,
		edittitle: "Edit selected contact",
		del: false,
		search: false,
		deltitle: "Delete selected contact",
		alerttext: "Please, select contact"
	}, {
		beforeShowForm: beforeShowForm,
		editCaption: "Edit Contact"
	}, {
		beforeShowForm: beforeShowForm,
		addCaption: "Add Contact"
	}, {
		caption: "Delete Contact",
		msg: "Delete selected contact?"
	}, {
		multipleSearch: false
	}, {
		closeOnEscape: true
	});
	
	jQuery("#contactList").jqGrid('filterToolbar', {
		searchOnEnter: false
	});
	*/
	$('#dockBatchesLCEList').Fisheye({
				maxWidth: 30,
				items: 'a',
				itemsText: 'span',
				container: '.dock-container_LCEList',
				itemWidth: 50,
				proximity: 60,
				alignment : 'left',
				valign: 'bottom',
				halign : 'left'
			});
});