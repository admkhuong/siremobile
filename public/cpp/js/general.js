function noBack() {
	window.history.forward();
}

// $(document).ready(function() {
$(function() {
	$("a#btnSignout").click(function(e) {
		e.preventDefault();
		$.ajax({
			url: rootUrl + "/" + publicPath + "/cpp/cfc/users.cfc?method=signout&amp;returnformat=json&amp;queryformat=column&amp;_cf_nodebug=true&amp;_cf_nocache=true",
			success: function() {
				window.location = rootUrl + "/" + publicPath + "/cpp/index.cfm";
			}
		});
		return false;
	});
	
	/**
	 * Sidebar accordion
	 */
	jQuery('#accordion h3').click(function() {
		if (jQuery(this).hasClass('open')) {
			jQuery(this).removeClass('open');
			jQuery(this).next().slideUp('fast');
		} else {
			jQuery(this).addClass('open');
			jQuery(this).next().slideDown('fast');
		}
		return false;
	});
	
	/**
	 * Widget Box Toggle
	 */
	jQuery('.widgetbox h3, .widgetbox2 h3').hover(function() {
		jQuery(this).addClass('arrow');
		return false;
	}, function() {
		jQuery(this).removeClass('arrow');
		return false;
	});
	
	jQuery('.widgetbox h3, .widgetbox2 h3').toggle(function() {
		jQuery(this).next().slideUp('fast');
		jQuery(this).css({
			MozBorderRadius: '3px',
			WebkitBorderRadius: '3px',
			borderRadius: '3px'
		});
		return false;
	}, function() {
		jQuery(this).next().slideDown('fast');
		jQuery(this).css({
			MozBorderRadius: '3px 3px 0 0',
			WebkitBorderRadius: '3px 3px 0 0',
			borderRadius: '3px 3px 0 0'
		});
		return false;
	});
	
	$("textarea.selected").select();
	$("textarea.selected").click(function() {
		this.select();
	});
});