$(function() {
	jQuery("#groupList").jqGrid({
		url: rootUrl + "/" + publicPath + "/cpp/cfc/group.cfc?method=GetGroupData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		datatype: "json",
		jsonReader: {
			root: "ROWS",
			page: "PAGE",
			total: "TOTAL",
			records: "RECORDS",
			cell: "", // not used
			id: "0" // will default second column as ID
		/*
		 * , repeatitems: true, cell: "cell", id: "id"
		 */
		},
		autowidth: true,
		height: 'auto',
		viewrecords: true,
		rowNum: 30,
		rowList: [
			10, 20, 30
		],
		colNames: [
			'ID', 'Name', "Possible", 'Actions'
		],
		colModel: [
			{
				name: 'id',
				index: 'id',
				width: 50,
				search: false
			}, {
				name: 'name',
				index: 'name',
				width: 200,
				editable: true,
				editrules: {
					required: true
				},
				searchoptions: {
					sopt: [
						"eq", "ne", "bw", "bn", "ew", "en", "cn", "nc"
					]
				}
			}, {
				name: 'possible',
				index: 'possible',
				width: 200,
				search: false,
				formatter: "checkbox",
				formatoptions: {
					disabled: true
				},
				editable: true,
				edittype: 'checkbox',
				editoptions: {
					value: "1:0"
				}
			}, {
				name: 'action',
				width: 60,
				fixed: true,
				sortable: false,
				resize: false,
				search: false,
				formatter: 'actions',
				formatoptions: {
					// editformbutton: true,
					editbutton: true,
					keys: true,
					delOptions: {
						caption: "Delete",
						msg: "Are you sure you want to delete this group?",
						bSubmit: "Delete",
						bCancel: "Cancel"
					}
				}
			}
		],
		pager: "#pGroupList",
		viewrecords: true,
		sortname: 'name',
		/*
		 * grouping: true, groupingView: { groupField: [ 'possible' ],
		 * groupColumnShow: [ true ] },
		 */
		caption: "Groups",
		editurl: rootUrl + "/" + publicPath + "/cpp/cfc/group.cfc?method=jqGridAction&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true"
	/*
	 * , ondblClickRow: function(rowid, iRow, iCol, e) {
	 * jQuery("#contactList").jqGrid('editGridRow', id, {}); if (id && id !==
	 * clLastSelect) { jQuery('#contactList').jqGrid('restoreRow', clLastSelect);
	 * jQuery('#contactList').jqGrid('editRow', id, true); clLastSelect = id; } }
	 */
	});
	jQuery("#groupList").jqGrid('navGrid', "#pGroupList", {
		add: true,
		addtitle: "Add new group",
		edit: false,
		del: false
	}, {}, // use default settings for edit
	{
		addCaption: "Add Group",
    editCaption: "Edit Group"
	}, // use default settings for add
	{}, // delete instead that del:false we need this
	{
		multipleSearch: false
	}, // enable the advanced searching
	{
		closeOnEscape: true
	} // allow the view dialog to be closed when user press ESC key
	);
});