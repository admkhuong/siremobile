/**
 * Trang.Lee (trangunghoa@gmail.com)
 */
$(function () {
	ReloadMCContactsGroupData("inpGroupType", 1);
	
	
	
	$("#AddNewCPPListButton").click(function () {
		groups = $("#AddCPPListForm #inpGroupType").val();
		contactType = $("#AddCPPListForm #inpContactType").val();
		contactString = $("#AddCPPListForm #inpContactString").val();
		if (contactString) {
			addnewcontact(groups,contactType,contactString);
		} else {
			jAlert("Please input contact string.", "Failure.");
			$("#inpContactString").focus();
		}
		
		/*$.ajax({
			  type: "POST", 
			  url:  rootUrl + "/" + publicPath + "/cpp/cfc/contact.cfc?method=CreateContact&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
			  datatype: 'json',
			  data:  { 
				  type : contactType, 
				  groups : '0,', 
				  firstname : '',
				  lastname : '',
				  email : $("#AddCPPListForm #INPCPPID").val(),
				  ContactString : contactString
			  },
			  error: function(XMLHttpRequest, textStatus, errorThrown) {},
			  success:
					
					function(d2, textStatus, xhr ) 
					{
						var d = eval('(' + xhr.responseText + ')');
							if (d.ROWCOUNT > 0) 
							{	
								// alert(d.DATA.DEBUGVAR1[0])
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{   
										jAlert("123","1234");
											
									} else {
										 jAlert("Failure.", "Can't Save Survey to DB"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);
									}
								}
								else
								{
								 	 $.alerts.okButton = '&nbsp;OK&nbsp;';
									 jAlert("Failure.", "Can't Save Survey to DB"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0]);
								}
							}
							else
							{
							   alert("Unable save data to DB.Please check connection.");
							}

					}
		} );*/
		return false;
	});
	$("#Cancel").click(function () {
		CreateAddContactDialog.dialog('destroy');
		CreateAddContactDialog.remove();
		return false;
	});
});

	function addnewcontact(groups, contactType, contactString) {
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: rootUrl + '/' + publicPath + '/cpp/cfc/contact.cfc?method=CreateContact&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				type : contactType,
				groups: groups,
				firstname : '',
				firstname : '',
				ContactString : contactString
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									CreateAddContactDialog.dialog('destroy');
									CreateAddContactDialog.remove();
									$.jGrowl("Message.", 
								            { 
								             life:2000, 
								             position:"center",
								             header:'Contact add success!',
								             beforeClose:function(e,m,o){
								               
								             }
								            });
									gridReloadContactList();									
									return false;
										
								}
								else
								{
						            $.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Contact has NOT been create.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!");
								}
							}
							else
							{<!--- Invalid structure returned --->	
								
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");	
						}
				}
			});	
		return false;
	}
	
	var OldGroupName = "";

	function ReloadMCContactsGroupData(inpObjName, inpShowSystemGroups, inpCallBack)
	{
		$("#loadingPhoneList").show();		
		
		if(typeof(inpShowSystemGroups) == 'undefined')
			  inpShowSystemGroups = 0;
			
			  $.ajax({
			  url:  rootUrl + '/' + sessionPath + '/cfc/simplelists.cfc?method=GetGroupData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			  dataType: 'json',
			  data:  {inpShowSystemGroups : inpShowSystemGroups},					  
			  error: function(XMLHttpRequest, textStatus, errorThrown) { },					  
			  success:
				  
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
					
					// alert(d);
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{																									
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								LastGroupId_MCContacts = 0;
								var GroupOptions = "";
								
								for(x=0; x<d.ROWCOUNT; x++)
								{
									
									if(CurrRXResultCode > 0)
									{								
										if(LastGroupId_MCContacts == d.DATA.GROUPID[x])
											GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" SELECTED class="">' + d.DATA.GROUPNAME[x] + '</option>';
										else
											GroupOptions += '<option value="' + d.DATA.GROUPID[x] + '" class="">' + d.DATA.GROUPNAME[x] + '</option>';
									}							
								}
								
								if(CurrRXResultCode < 1)
								{
									GroupOptions += '<option value="' + d.DATA.GROUPID[0] + '" SELECTED class="">' + d.DATA.GROUPNAME[0] + '</option>';
								}
								
									
								<!--- Allow differnet html elements to get filled up--->	
								$("#" + inpObjName).html(GroupOptions);
									 				
								OldGroupName = $("#" + inpObjName + " option:selected").text();												
								$("#inpGroupType").multiselect({
								   selectedText: "# of # selected"
								});
								if(typeof(inpCallBack) != 'undefined')
			  						inpCallBack();
									
								$("#loadingPhoneList").hide();
						
							}
							else
							{<!--- Invalid structure returned --->	
								$("#loadingPhoneList").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
							jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						
				} 		
						
			});


		return false;
	}

