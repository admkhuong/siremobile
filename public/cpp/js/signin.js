// $(document).ready(function() {
$(function() {
	//noBack();
	
	$("#frmSignIn").validate({
		onfocusout: false,
		onkeyup: false,
		focusInvalid: false,
		focusCleanup: true,
		rules: {
			inpSigninUserID: "required",
			inpSigninPassword: "required"
		},
		submitHandler: function(form) {
			$("#inpSigninMethod").val("Signin");
			$(form).ajaxSubmit({
				dataType: 'json',
				beforeSubmit: function(formData, jqForm, options) {
					$("#signinLoading").show();
				},
				success: function(json, statusText, responseText) {
					$("#signinLoading").hide();
					if (json.ROWCOUNT) {
						if (!json.DATA.SUCCESS[0]) {
							jAlertOK(json.DATA.MESSAGE[0]);
						} else {
							window.location = rootUrl + "/" + publicPath + "/cpp/home";
						}
					}
				}
			});
		}
	});
	
	$("#btnRequestPassword").click(function(e) {
		if ($("#inpSigninUserID").valid()) {
			$("#inpSigninMethod").val("RequestNewPassword");
			$("#frmSignIn").ajaxSubmit({
				dataType: 'json',
				beforeSubmit: function(formData, jqForm, options) {
					$("#signinLoading").show();
				},
				success: function(json, statusText, responseText) {
					$("#signinLoading").hide();
					if (json.ROWCOUNT) {
						if (!json.DATA.SUCCESS[0]) {
							jAlertOK(json.DATA.MESSAGE[0]);
						} else {
							jAlertOK("A new password was sent to your email. Please check mail to get new password.");
						}
					}
				}
			});
		}
		return false;
	});
});