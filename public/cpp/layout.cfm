<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" itemscope itemtype="http://schema.org/Review">
	<head>
		<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
		<title>Customer Preference Portal</title>
		<!---
		<meta property="og:title" content="Like" />
		<meta property="og:type" content="author" />
		<meta property="og:url" content="http://ebmdev.seta/devjlp/EBM_DEV/public/cpp/home" />
		<meta property="og:image" content="" />
		<meta property="og:site_name" content="EBM" />
		<meta property="fb:admins" content="100002604088710" />
		--->
	</head>
	<body>
		<!---<cfinclude template="_header.cfm" />--->
		{%cppContent%}
		<!--- <cfinclude template="_footer.cfm" /> --->

	</body>
</html>