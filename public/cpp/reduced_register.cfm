<cfinclude template="paths.cfm" >

<cfsetting showdebugoutput="no">

<cfif find('public',cgi.HTTP_REFERER)>
	<cfset cfcPath = 'cfc/users.cfc'>
<cfelse>
	<cfset cfcPath = 'public/cfc/users.cfc'>
</cfif>

<Script TYPE="text/javascript">
	$("#RegisterNewAccountReducedForm #RegisterNewAccountReducedButton").click( function() { RegisterNewAccountReduced(); return false;  });
	$("#loadingDlgReduced").hide();
	
	function RegisterNewAccountReduced()
	{			
	
		$("#RegisterNewAccountReducedButton").show();		
		$("#loadingDlgReduced").show();
		<!--- if($("#RegisterNewAccountReducedForm #inpMainPhone").val() == "")
		{
			jAlertOK("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgReduced").hide(); return false; } );							
						
			return false;	
		} --->
		
		if($("#RegisterNewAccountReducedForm #inpMainEmail").val() == "")
		{				
			jAlertOK("You must input a valid email address.\n", "Failure!", function(result) { $("#loadingDlgReduced").hide(); return false; } );							
						
			return false;	
		}
		
		if (!$('#RegisterNewAccountReducedForm #acceptTerm').is(':checked'))
		{				
			jAlertOK("You must accept terms and conditions.\n", "Failure!", function(result) { $("#loadingDlgReduced").hide(); return false; } );							
			return false;											
		}
		
		$.ajax({
				type:"POST",
				url:'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cfc/users.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				data:{
					inpMainPhone : $("#RegisterNewAccountReducedForm #inpMainPhone").val(), 
					inpMainEmail : $("#RegisterNewAccountReducedForm #inpMainEmail").val(), 
					inpFacebookUser : $("#RegisterNewAccountReducedForm #facebookUser").val(),
					inpFirstName: $("#RegisterNewAccountReducedForm #inpFirstName").val(), 
					inpLastName: $("#RegisterNewAccountReducedForm #inpLastName").val(),  
					inpAt : encodeURIComponent($("#RegisterNewAccountReducedForm #at").val()),
					acceptAddFriend : $('#RegisterNewAccountReducedForm #acceptAddFriend').is(':checked') 
				},
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	var d = eval('(' + xhr.responseText + ')');
	            	if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{		
								<!---window.location.href="../session/account/home";--->							
								jAlertOK("User has been added.", "Success!", function(result) 
								{ 
									window.location.href="../session/account/home";<!------>
									//alert('here');
								} );
							}
							else
							{									
								<!--- CuongVM 2011.12.07 Modify: Show RegisterNewAccount button and focus PhoneNumber textbox when input invalid -Start --->
								<!--- jAlertOK("User has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } ); --->
								jAlertOK(" User has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0],
										 "Failure!",
										  function(result) {
														 	 $("#loadingDlgReduced").hide();
														 	 $("#RegisterNewAccountReducedForm #inpMainPhone").focus();
														 	 return false;
														   } );						
								
								return false;
								<!--- CuongVM 2011.12.07 Modify: Show RegisterNewAccount button and focus phone number textbox when input invalid -End --->
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#RegisterNewAccountReducedButton").hide();          		
	            }
			});
	
		return false;

	}
</script>


<cfoutput>
	
    <cfhttp url="https://graph.facebook.com/me/?access_token=#url.at#" result="myDetails" />
	<cfset pData = DeserializeJSON(myDetails.filecontent)>
	<div id="SignUpSection" class="rxform2 ui-corner-all" style="margin-left:0px !important; height:330px; padding-top:50px !important">
        <label>Sign up now!</label>
        <hr />
        <form id="RegisterNewAccountReducedForm" name="RegisterNewAccountReducedForm" action="" method="POST">

            <input TYPE="hidden" name="inpBatchId" id="inpBatchId" value="0" />
            <input TYPE="hidden" name="facebookUser" id="facebookUser" value="1" />
            <input TYPE="hidden" name="at" id="at" value="#url.at#" />
            
            <table border="0" cellpadding="0" cellspacing="0">	
                <tr>
                    <td align="right" nowrap><label>First Name</label></td>
                    <td align="left"><input TYPE="text" name="inpFirstName" id="inpFirstName" size="255" class="ui-corner-all" value="#pData.first_name#" /><td>         
                </tr>
                <tr>
                    
                    <td align="right" nowrap><label>Last Name</label></td>
                    <td align="left"><input TYPE="text" name="inpLastName" id="inpLastName" size="255" class="ui-corner-all" value="#pData.last_name#" /><td>         
                </tr>
                <tr>
                    <td align="right" nowrap valign="top"><label>Phone Number</label></td>
                    <td align="left"><input TYPE="text" name="inpMainPhone" id="inpMainPhone" size="255" class="ui-corner-all" />
                    	<span style="font-size:12px; color:##A13D69">[We need your phone number so that YOUR FRIENDS can call you. It will ONLY be visible to the friends selected by you.]</span>
                    <td>         
                </tr>
                <tr>
                    <td align="right" nowrap><label style="margin-top:5px">Email Address</label></td>
                    <td align="left" ><input TYPE="text" name="inpMainEmail" id="inpMainEmail"  style="margin-top:5px" size="255" class="ui-corner-all" value="#pData.email#" disabled="disabled" /></td>       
                </tr>
                <tr>
                    <td align="right" nowrap>
                        <label><input type="checkbox" name="acceptTerm" id="acceptTerm" style="width:10px; display:inline" ></label>
                    </td>
                    <td style="font-size:11px">
                        I accept <a href="dsp_TermsOfUse.cfm" target="_new" style="display:inline">terms & conditions</a>
                    <td>         
                </tr>
                <tr>
                    <td align="right" nowrap>
                        <label><input type="checkbox" name="acceptAddFriend" id="acceptAddFriend" style="width:10px; display:inline" checked="checked"></label>
                    </td>
                    <td style="font-size:11px">
                        Automatically add my facebook friends and they can call me using Babblesphere.
                    <td>         
                </tr>
                <Tr>
                	<td></td>
                	<td><br />
                    	<!---<input type="button" id="RegisterNewAccountReducedButton" class="ui-corner-all" value="Register for New Account" />--->
                        <button id="RegisterNewAccountReducedButton" class="ui-corner-all">Register for New Account</button><br />
                        <!---<span id="RegisterNewAccountReducedButton" class="ui-corner-all" style="font-size:12px; margin: 2px 0 5px 5px; padding:4px 2px; border:1px solid black; cursor:pointer">Register for New Account</span>--->
                        <div id="loadingDlgReduced" style="display:inline;">
                            <img class="loadingDlgReducedRegisterNewAccount" src="images/loading-small.gif" width="20" height="20">
                        </div>
                        
                    </td>
                </Tr>
            </table>            
            <!---<span class="required">*Note: all fields are required to get an account</span>--->
            <BR>
<!---            <button id="RegisterNewAccountReducedButton" TYPE="button" class="ui-corner-all" >Register for New Account</button>--->
            
        </form>
    </div>
    
</cfoutput>