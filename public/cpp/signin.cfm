<html itemscope itemtype="http://schema.org/link">
<head>
<cfoutput>
	<!-- START: Google+ TITLE, DESCRIPTION, AND IMAGE URL FOR POST -->
	<meta itemprop="name" content="Customer Preference Portal">
	<meta itemprop="description" content="With Customer Preference Portal, customers can stay engaged with highly personalized services. Reach out to customers through various means of communication to deliver real-time updates and deals.">
	<meta itemprop="image" content="#rootUrl#/#PublicPath#/cpp/images/CPPThumbs_265x265.png">
	<!-- END -->
</cfoutput>

<cfparam name="inpCPPUUID" default="0" />
<!--- <script type="text/javascript">
	window.onload = function () {	
		var inpCPPUUID = '<cfoutput>#inpCPPUUID#</cfoutput>';
		if (inpCPPUUID == 0) {
		<cfoutput> 
			window.location = "#rootUrl#/#PublicPath#/CPP/index.cfm";
		</cfoutput>	
		}
	}
</script> --->



<cfif TRIM(Session.CPPStyleTemplate_vch) NEQ ""> 
	<cfset CPPStyleTemplate = TRIM(Session.CPPStyleTemplate_vch)>
<cfelse>	    
    <!---<cffile action="read" variable="CPPStyleTemplate" file="#rootUrl#/#PublicPath#/cpp/css/cpp.css">--->
      
    <cfsavecontent variable="CPPStyleTemplate">
	   	<cfoutput>
	    <style type="text/css">
		    @import url('#rootUrl#/#PublicPath#/cpp/css/cpp.css');
		</style>
		</cfoutput>
    </cfsavecontent>
  
</cfif>    

<cfsavecontent variable="cppCss">
    
		<style>
		
			<cfoutput>
				@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
				@import url('#rootUrl#/#PublicPath#/css/jgrowl-theme.css');
	 		</cfoutput>
			
		<!---	#btnTwitter
			{
				border: 0px;
				background: url(../images/icons/twitter_sign_in_buttons.jpg) no-repeat left;
				width: 200px;
				height: 22px;
			}--->
<!---
			#sign option[value="facebook"]
			{
				padding: 10px 0px 10px 35px;
				background: url(../images/icons/fb-icon.gif) no-repeat 10px center;
			}
			#sign option[value="twitter"]
			{
				padding: 10px 0px 10px 35px;
				background: url(../images/icons/yahoo-icon.gif) no-repeat 10px center;
			}
			#sign option[value="google"]
			{
				padding: 10px 0px 10px 35px;
				background: url(../images/icons/google-icon.png) no-repeat 10px center;
			}


			.dropdown dd, .dropdown dt, .dropdown ul { margin:0px; padding:0px; }
			.dropdown dd { position:relative; }
			.dropdown a, .dropdown a:visited { color:#000000; text-decoration:none; outline:none;}
			.dropdown a:hover { color:#5d4617;}
			.dropdown dt a:hover, .dropdown dt a:focus { color:#5d4617; border: 1px solid #5d4617;}
			.dropdown dt a {background:#CCCCCC url(images/arrow_down.png) no-repeat scroll right center; display:block; padding-right:20px;
							border:1px solid #d4ca9a; width:150px;}
			.dropdown dt a span {cursor:pointer; display:block; padding:5px;}
			.dropdown dd ul { background:#e4dfcb none repeat scroll 0 0; border:1px solid #d4ca9a; color:#C5C0B0; display:none;
							  left:0px; padding:5px 0px; position:relative; top:2px; width:auto; min-width:170px;  max-width:170px;  width:170px; list-style:none;}
			.dropdown span.value { display:none;}
			.dropdown dd ul li a { padding:5px; display:block;}
			.dropdown dd ul li a:hover { background-color:#d0c9af;}
			
			.dropdown img.flag { border:none; vertical-align:middle; margin-left:10px; }
			
			
			--->
			
			
			<!---
			
			
			.dropdown dd, .dropdown dt, .dropdown ul { margin:0px; padding:0px; }
			.dropdown dd { position:relative; }
			.dropdown a, .dropdown a:visited { color:#FFFFFF; text-decoration:none; outline:none;}
			.dropdown a:hover { color:#5d4617;}
			.dropdown dt a:hover, .dropdown dt a:focus { color:#5d4617; border: 1px solid #5d4617;}
			.dropdown dt a {background:#e4dfcb url(images/arrow.png) no-repeat scroll right center; display:block; padding-right:20px;
							border:1px solid #d4ca9a; width:150px;}
			.dropdown dt a span {cursor:pointer; display:block; padding:5px;}
			.dropdown dd ul { background:#e4dfcb none repeat scroll 0 0; border:1px solid #d4ca9a; color:#C5C0B0; display:none;
							  left:0px; padding:5px 0px; position:absolute; top:2px; width:auto; min-width:170px; list-style:none;}
			.dropdown span.value { display:none;}
			.dropdown dd ul li a { padding:5px; display:block;}
			.dropdown dd ul li a:hover { background-color:#d0c9af;}
			
			.dropdown img.flag { border:none; vertical-align:middle; margin-left:10px; }
				
			
			--->
				

		</style>	
			
        <cfoutput>#CPPStyleTemplate#</cfoutput>	    
</cfsavecontent>

<cfsavecontent variable="cppJs">
	<CFFUNCTION NAME="MyDump">
	    <CFARGUMENT NAME="OBJ" Required="TRUE">
	    <CFDUMP VAR="#Obj#">
	</CFFUNCTION>
	<cfscript>
    application.objMonkehTweet = createObject('component',  
        '#cfcmonkehTweet#')  
        .init(  
            consumerKey         =   '#APPLICATION.Twitter_Consumerkey#',  
            consumerSecret      =   '#APPLICATION.Twitter_Consumersecret#',  
            oauthToken          =   '#APPLICATION.Twitter_AccessToken#',  
            oauthTokenSecret    =   '#APPLICATION.Twitter_Consumersecret#',  
            parseResults        =   true  
        ); 
        
        authStruct = application.objMonkehTweet;

	authStruct = application.objMonkehTweet.getAuthentication(callbackURL='#rootUrl#/#PublicPath#/cpp/twitter_authorize.cfm');
	
	//if (authStruct.success){
		//	Here, the returned information is being set into the session scope.
		//	You could also store these into a DB (if running an application for multiple users)
		session.oAuthToken		= authStruct.token;
		session.oAuthTokenSecret	= authStruct.token_secret;
		session.CPPUUID		= #inpCPPUUID#;
	//}
	
	</cfscript>
	<cfoutput>
		<script type="text/javascript">
			/* Config */
			var rootUrl = "#rootUrl#";
			var publicPath = "#PublicPath#";
			var sessionPath = "#SessionPath#";
		</script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
        <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jgrowl.js"></script>
        <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jgrowl-theme.js"></script>                
        <script src="https://apis.google.com/js/client.js?onload=GoogleLogin"></script>
		<script src="https://connect.facebook.net/en_US/all.js"></script>
     </cfoutput> 
        
    <!--- Details - if going to include custom JS - write as CFM so can document better during development - when stable make compacted version as pure .js--->
    <script type="text/javascript">
		var Facebook_URL = '<cfoutput>#APPLICATION.Facebook_URL#</cfoutput>';
		var Facebook_URL_PARAM = '<cfoutput>#APPLICATION.Facebook_URL_PARAM#</cfoutput>';		
		var Facebook_AppID = '<cfoutput>#APPLICATION.Facebook_AppID#</cfoutput>'; 
		<!---
		jpijeff@hotmail.com FB account
		dev.cpp
		App ID		
		239247492841662
		App Secret: 
		4f8fa297f4e01960028db41912ceb48c
		
		
Google OAuth
Product name:
 
Contact Preference Portal

Google account:
 support@ebmcloud.com  


Product logo:
https://contactpreferenceportal.com/public/ebm/images/EBMTriSwoosh105x103_WithShadowWeb.png 
Edit branding information... 

Client ID for web applications 
Client ID:
698138944340.apps.googleusercontent.com 
Email address:
698138944340@developer.gserviceaccount.com 
Client secret:
DVv3WRV-4QlTslQHxV-eH_z6 
Redirect URIs:
 https://www.contactpreferenceportal.com/oauth2callback  
JavaScript origins:
 https://www.contactpreferenceportal.com 




		--->
		$(function() {		
		
		
			var inpCPPUUID = "<cfoutput>#inpCPPUUID#</cfoutput>";
			$("#CPPSignIn").click(function(){								
				CPPSignIn();				
			});	
			
			$("#CPPRequestPassword").click(function(){								
				CPPRequestNewPassword();				
			});	
			
			$("#btnFacebook").click(function() {
				// console.log('yo fb');
				//FB.login(getToken());
				
				$.jGrowl("Watch for a possible popup to log in to Facebook, if you are not already logged in.", { life:2000, position:"center", header:' Message' });
				
				getToken();				
			});
			
			
			$("#btnTwitter").click(function() {
				//var twitter = twitter_redirect();
				<!---
				window.open('<cfoutput>#authStruct.authURL#</cfoutput>','TwitterLogin','width=400,height=200,left=100,top=100,screenX=100,screenY=100');
				--->
				window.location = "<cfoutput>#authStruct.authURL#</cfoutput>";
				
			});
			$("#btnGoogle").click(function () {
				
				$.jGrowl("Watch for a possible popup to log in to Google, if you are not already logged in.", { life:2000, position:"center", header:' Message' });
						
				GoogleLogin();
			});
			
			$("input[name='inpResetPasswordEveryTime']").change(function(){
											
				if ($("input[name='inpResetPasswordEveryTime']:checked").val() == '1')
				{
					$.jGrowl("Password will be changed on each successful login.", { life:2000, position:"center", header:' Message' });
				}
				else
				{
					$.jGrowl("Password is whatever was last sent to you. You may also request a new one at anytime.", { life:2000, position:"center", header:' Message' });
				}
			});
		
			
			<!--- Allow current end user to sign out--->
			$("#CPPPortalSignOut").click(function(){
				
				var ParamStr = "";
	
				<cfoutput>							
					ParamStr += "?inpCPPUUID=-1";
					window.location = "#rootUrl#/#PublicPath#/CPP/index.cfm" + ParamStr;
				</cfoutput>	
				
			});
			
			
			<!--- Allow current end user to sign out--->
			$("#WhatIsMFA").click(function(){
					
				jGrowlTheme('monocenterauto', 'MFA is...', '<BR/>You don&quot;t need an account.<BR/>You don&quot;t need to sign up.<BR/>Your email address is your primary identification.<BR/><BR/> This site uses Multi-Factor Authentication.<BR/>You just need to request/retrieve a password from your email address you provide to log in. <BR/>', '', 10000);
							
			});
			
			
			<!--- Log in from alternative source drop down --->
			
            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


           
			bb_init_facebook();
			
			
		});
		
		
		function CPPSignIn()
		{									
			$("#loadingDlgCPPPublicProcessing").show();		
					
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/Users.cfc?method=Signin&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { inpSigninUserID: $("#inpSigninUserID").val(), inpSigninPassword: $("#inpSigninPassword").val(), inpCPPUUID : <cfoutput>'#inpCPPUUID#'</cfoutput>, resetPasswordEveryTime : $("input[name='inpResetPasswordEveryTime']:checked").val() },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{
				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/index.cfm" + ParamStr;
								</cfoutput>										
							}
							else
							if(CurrRXResultCode == 1)
							{					
								<!--- refresh List --->
								
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/home" + ParamStr;
								</cfoutput>	
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });																
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;
			
		}
		
		function CPPRequestNewPassword()
		{
			$("#loadingDlgCPPPublicProcessing").show();		
					
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
			url:  '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/Users.cfc?method=RequestNewPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
			dataType: 'json',
			data:  { INPCONTACTSTRING: $("#inpSigninUserID").val(), INPCONTACTTYPEID: 2, inpCPPUUID : <cfoutput>'#inpCPPUUID#'</cfoutput>  },					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
			success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d2, textStatus, xhr ) 
			{				
				<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
				var d = eval('(' + xhr.responseText + ')');
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							<!--- If session Expired and no CPP detected - redirect to CPP entry in--->
							if(CurrRXResultCode == -2)
							{
								var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/index.cfm" + ParamStr;
								</cfoutput>										
							}	
							else							
							if(CurrRXResultCode == 1)
							{					
								<!--- refresh List --->
								
								<!---var ParamStr = "";
			
								<cfoutput>							
									ParamStr += "?inpCPPUUID=#TRIM(inpCPPUUID)#";
									window.location = "#rootUrl#/#PublicPath#/CPP/SignIn.cfm" + ParamStr;
								</cfoutput>	--->
								
								<!---$.jGrowl('A new password has been sent to the email address at ' + $("#inpSigninUserID").val() + ' \nThe message is from support@contactpreferenceportal.com. If you dont see it shortly, please check your junk folder(s).' , { life:10000, position:"center", header: ' Message'});--->
								
								jGrowlTheme('monocenter', 'Message', 'A new password has been emailed to:  ' + $("#inpSigninUserID").val(), '', 10000);
							
							}
							else
							{
								$.jGrowl(d.DATA.MESSAGE[0], { life:2000, position:"center", header:' Message' });									
							}
																										
							$("#loadingDlgCPPPublicProcessing").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingDlgCPPPublicProcessing").hide();
						}
					}
					else
					{<!--- No result returned --->
					
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentrxtXMLString").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}										
			} 		
					
			});
			
			return false;			
			
		}
		
	function authenticatePass(uid,access_token){
		$.ajax({
					type : "GET",
					async : false,
					url : "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/users.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					data : {
						inpUserID : '', 
						inpPassword : '', 
						inpAt : access_token,
						inpUUID : '<cfoutput>#inpCPPUUID#</cfoutput>',
						<!---appSecret : Facebook_Appsecret,--->
						appId : Facebook_AppID
					},
					dataType : "json",
					success : function (d) {
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{			
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									<!--- Navigate tp sesion Home --->
									var ParamStr = "?inpCPPUUID=<cfoutput>#TRIM(inpCPPUUID)#</cfoutput>";
									window.location.href="../cpp/home" + ParamStr;
								}
								else
								{
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
									if(typeof(d.DATA.REASONMSG[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined")
									{											
										jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] + d.DATA.MESSAGE[0] + d.DATA.ERRMESSAGE[0]);										
									}		
									else
									{
										jAlertOK("Error while trying to login.\n" + "Invalid response from server.");
									}
								}
							}
							else
							{<!--- Invalid structure returned --->	
								jAlertOK("Error while trying to login.\n" + "Invalid response from server." );
							}
						}
						else
						{<!--- No result returned --->
							jAlertOK("Error while trying to login.\n" + "No response form remote server.");
						}
							
						}
				});
	}
	
	<!--- get token facebook ---->
	function getToken()
	 {
		 //alert('here')

		 FB.getLoginStatus(function(r)
		 {//alert(response.status)

		 	response = r;
			if (response.status === 'connected') 
			{
				// the user is logged in and connected to your
				// app, and response.authResponse supplies
				// the user's ID, a valid access token, a signed
				// request, and the time the access token 
				// and signed request each expire

				var uid = response.authResponse.userID;
				access_token = response.authResponse.accessToken;
				
				authenticatePass(uid,access_token);
				
				
		  	} 
			else 
			{
				FB.login(function(response){
					if (response.status === 'connected') 
					{
						var uid = response.authResponse.userID;
						access_token = response.authResponse.accessToken;
						authenticatePass(uid,access_token);
				  	} 
				},{scope:'read_stream,publish_stream,email'});				
		  	}
		 	
		})
				
	 }
	 
	 
	<!--- Call back for login google---->
	function googleLoginCallBack(result){
		if(result != null){
			access_token = result.access_token;
			var pData = '';
				$.ajax({
		              type: "GET",
		              contentType: "application/json; charset=utf-8",
		              <!--- url: "https://www.googleapis.com/plus/v1/people/me?access_token=" + access_token, --->
		              url: "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token,
		              data: "{}",
		              dataType: "json",
		              async: false,
		              success: function (data) {
		                  pData = data;
		              }
		             });
		             
			$.ajax({
					type : "GET",
					async : false,
					url : "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/cfc/users.cfc?method=validateContactGoogle&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					data : {
						accessToken : access_token,
						accessSecret : '',
						userIdGoogle : pData.id,
						FirstName : pData.given_name,
						LastName : pData.family_name,
						email : pData.given_name,
						inpUUID : '<cfoutput>#inpCPPUUID#</cfoutput>'
					},
					dataType : "json",
					success : function (d) {
						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{			
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0)
								{
									<!--- Navigate tp sesion Home --->
									var ParamStr = "?inpCPPUUID=<cfoutput>#TRIM(inpCPPUUID)#</cfoutput>";
									window.location.href="../cpp/home" + ParamStr;
								}
								else
								{
									<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
									if(typeof(d.DATA.REASONMSG[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined" && typeof(d.DATA.MESSAGE[0]) != "undefined")
									{											
										jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] + d.DATA.MESSAGE[0] + d.DATA.ERRMESSAGE[0]);										
									}		
									else
									{
										jAlertOK("Error while trying to login.\n" + "Invalid response from server.");
									}
								}
							}
							else
							{<!--- Invalid structure returned --->	
								jAlertOK("Error while trying to login.\n" + "Invalid response from server." );
							}
						}
						else
						{<!--- No result returned --->
							jAlertOK("Error while trying to login.\n" + "No response form remote server.");
						}
							
						}
				});
		}else{
		 	var config = {
	            'client_id': '<cfoutput>#APPLICATION.Google_ClientId#</cfoutput>',
	            'scope': 'https://www.googleapis.com/auth/userinfo.profile',
				'immediate': false
	        };
	        gapi.auth.authorize(config, googleLoginCallBack);
		}
	}
    var access_token = 0;
    function GoogleLogin() {
        var config = {
            'client_id': '<cfoutput>#APPLICATION.Google_ClientId#</cfoutput>',
            'scope': 'https://www.googleapis.com/auth/userinfo.profile',
            'immediate': true  
        };
        gapi.auth.authorize(config, googleLoginCallBack);
    }

	var reduceReg = 0;
		
	function reduced_Register()
	{
		var loading = $('<img src="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		if(reduceReg != 0)
		{
			reduceReg.remove();
			reduceReg = 0;
		}
						
		reduceReg = $('<div></div>').append(loading.clone());
		
		reduceReg
			.load('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cpp/reduced_register.cfm?at='+access_token)
			.dialog({
				modal : true,
				title: 'Registration Form',
				width: 480,
				height: 400
			});
	
		reduceReg.dialog('open');
				
		return false;			
	}
	 
	function bb_init_facebook() {
		
		var curLoc = window.location;

		FB.init({ 
			appId: Facebook_AppID,
			cookie: true,
			status: true,
			xfbml: true,
			/*channelUrl: 'http://www.babblesphere.com/Session/account/'*/
			/* Need channel to work with IE9 */
				/*channelUrl: 'http://www.babblesphere.com/channel.html'*/
			
			
			/* For MSB */
			channelUrl: 'http://babblesphere.seta:8500/BabbleSphere/channel.html'
			/* For SETA */
			/*channelUrl: 'http://babblesphere.seta:8500/BabbleSphere/channel.html'*/
	
		});
	}
	
		
	<!--- Auto click Login button when press Enter key --->
	function submitenter(myfield,e) {
	  	var keycode;
	 	if (window.event) keycode = window.event.keyCode;
	  	else if (e) keycode = e.which;
	  	else return true;
	
		if (keycode == 13)
     	{
        	CPPSignIn();
        	return false;
        }
		else
			return true;
	}
		
    </script>    

</cfsavecontent>

<cfsavecontent variable="cppContent">
	<cfoutput>
		<div id="container" class="container" align="center">
			<div id="content" class="clf">
				<div id="CPPdivSignin" class="GradientBackground">
               
                 		<input type="hidden" id="inpCPPUUID" name="inpCPPUUID" value="#inpCPPUUID#" />
                        <input id="inpSigninMethod" type="hidden" name="method" value="Signin" />
						                                               
                        <p id="CPPSignInTitle">Sign In Using</p>
                        
                                                    
                        <a id="btnFacebook" href="javascript:void(0)" class="inline"><img class="flag imagebutton" src="images/facebook_32x32.png" alt="" /></a>
                        <a id="btnTwitter" href="javascript:void(0)" class="inline"><img class="flag imagebutton" src="images/twitter_32x32.png" alt="" /></a>                                    
                        <a id="btnGoogle" href="javascript:void(0)" class="inline"><img class="flag imagebutton" src="images/google_32x32.png" alt="" /></a>
                                
                       
                        <h2>----- or -----</h2>
                       
                        
                        <div id="CPPDivEmailSignin">
                            <p>
								<label for="inpSigninUserID">Email</label>
								<input type="text" id="inpSigninUserID" name="inpSigninUserID" onKeyPress="return submitenter(this,event)"/>
							</p>
							<p>
								<label for="inpSigninPassword">Password</label>
								<input type="password" id="inpSigninPassword" name="inpSigninPassword" onKeyPress="return submitenter(this,event)"/>
							</p>
						
                            <p style="text-align:right">
                                <button id="CPPSignIn">Sign In </button>
                                <span id="signinLoading" style="display:none;">
                                    <img alt="Loading" src="#rootUrl#/#PublicPath#/images/loading-small.gif" width="20" height="20" />
                                </span>
                           </p>                                                          
                      
                      	</div>

                        <div id="CPPRequestPasswordDiv">
                            <a id="CPPRequestPassword" style="display:inline;"><span class="nounderlineuntilhover">New User? Lost Password? Enter your email and request a password&nbsp;</span><span class="underlineuntilhover">here.</span></a>
                        </div>
                        				
				</div>
			</div>
		</div>
	</cfoutput>
</cfsavecontent>


<cfsavecontent variable="cppFullContent">
	<cfoutput>
		<cfif len(Session.CPPTemplate)>
			<script language="javascript">
				document.cookie = "UUID=#Session.CPPUUID#"; 
			</script>
			#Session.CPPTemplate#
		<cfelse>
			<cfinclude template="layout.cfm" />
		</cfif>
	</cfoutput>
</cfsavecontent>
<cfoutput>
	<cfset cppFullContent = Replace(cppFullContent, "</head>", "#cppCss##cppJs#</head>") />
	<cfset cppFullContent = Replace(cppFullContent, "</body>", "</body>") />
	<cfset cppFullContent = Replace(cppFullContent, "{%cppContent%}", "#cppContent#") />
	#cppFullContent#
</cfoutput>

</html>