<cftry>
	<cfscript>
		returnData	= application.objMonkehTweet.getAccessToken(  
			requestToken	= 	session.oAuthToken,
			requestSecret	= 	session.oAuthTokenSecret,
			verifier		=	url.oauth_verifier
		);
						
		if (returnData.success) {
		
			//Save these off to your database against your User so you can access their account in the future
			session['accessToken']	= returnData.token;
			session['accessSecret']	= returnData.token_secret;
		
			session['userIdTwitter']		= returnData.user_id;
			session['UserName']		= returnData.screen_name;
			session['CONSUMEREMAIL']= returnData.screen_name;
			session['resetPasswordEveryTime']= 0;
		}
	</cfscript>
	<cfcatch>
		<cflocation url="#rootUrl#/#PublicPath#/cpp/signin.cfm?inpCPPUUID=#session.CPPUUID#" addtoken="false" />
	</cfcatch>
</cftry>
<!---
	<cfajaxproxy cfc="devjlp.EBM_DEV.Public.cpp.cfc.users2" jsclassname="validateContentTwitter">
--->
    <cfinvoke 
     component="#cfcPath#.users"
     method="validateContentTwitter"
     returnvariable="dataout">    
		<cfinvokeargument name="accessToken" value="#returnData.token#"/>
		<cfinvokeargument name="accessSecret" value="#returnData.token_secret#"/>    
		<cfinvokeargument name="userIdTwitter" value="#returnData.user_id#"/> 
		<cfinvokeargument name="UserName" value="#returnData.screen_name#"/>    
		<cfinvokeargument name="email" value="#returnData.screen_name#"/>  
        <cfinvokeargument name="inpUUID" value="#session.CPPUUID#"/>
    </cfinvoke> 

<cfheader statuscode="301" statustext="Moved Permanently">
<cfheader name="Location" value="#rootUrl#/#PublicPath#/cpp/home?inpCPPUUID=#session.CPPUUID#">
<cfabort>
<cflocation url="#rootUrl#/#PublicPath#/cpp/home?inpCPPUUID=#session.CPPUUID#" addtoken="false" />
