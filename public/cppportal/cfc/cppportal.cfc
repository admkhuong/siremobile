<cfcomponent output="false">
	<cfinclude template="../../paths.cfm">
	<cffunction name="saveCppPortal" access="remote" output="true" hint="Get Preference By CPPUUID">
		<cfargument name="CPPUUID" required="yes" >
		<cfargument name="Preview" default="0" >
		<cfargument name="inpStep" default="1" >
		<cfargument name="billing1" default="">
		<cfargument name="billing2" default="">
		<cfargument name="billing3" default="">
		<cfargument name="emailAddress" default="">
		<cfargument name="PREFERENCECHECK" default="">
		<cfargument name="customFieldId" default="">
		<cfargument name="voiceCheck" default="0">
		<cfargument name="voiceText1" default="">
		<cfargument name="voiceText2" default="">
		<cfargument name="voiceText3" default="">
		<cfargument name="smsCheck" default="0">
		<cfargument name="smsText1" default="">
		<cfargument name="smsText2" default="">
		<cfargument name="smsText3" default="">
		<cfargument name="emailCheck" default="0">
		<cfargument name="emailText" default="">
		<cfargument name="language" default="1"> 
		<cfargument name="accepConditionForm" default="0">
		<cfset var dataout = '0' />  
		<cfset CPPUUID = trim(CPPUUID)>
		<cfset billing1 = trim(billing1)>
		<cfset billing2 = trim(billing2)>
		<cfset billing3 = trim(billing3)>
		<cfset voiceText1 = trim(voiceText1)>
		<cfset voiceText2 = trim(voiceText2)>
		<cfset voiceText3 = trim(voiceText3)>
		<cfset smsText1 = trim(smsText1)>
		<cfset smsText2 = trim(smsText2)>
		<cfset smsText3 = trim(smsText3)>
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE,  TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            <cftry>
				<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
					SELECT 
						c.UserId_int,
						c.CPP_UUID_vch,
						c.MultiplePreference_ti, 
						c.SetCustomValue_ti,
						c.IdentifyGroupId_int,
						c.UpdatePreference_ti, 
						c.VoiceMethod_ti, 
						c.SMSMethod_ti,
						c.EmailMethod_ti, 
						c.IncludeIdentity_ti,
						c.IncludeLanguage_ti, 
						c.StepSetup_vch,
						c.Type_ti
					FROM 
						simplelists.customerpreferenceportal c
					WHERE
						c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
				</cfquery>
				<cfif GetCPPSETUPQUERY.IncludeIdentity_ti EQ 0>
					<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,2))>
				</cfif>
				<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
					<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,4))>
				</cfif>
				
				<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 1>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							NOT IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				<cfelse>
					<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
						SELECT 
							count(*) AS COUNT
						FROM 
							simplelists.cpp_preference
						WHERE
							CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
						AND 
							IsNull(Value_vch)
						ORDER BY 
							Order_int 
					</cfquery>
				</cfif>
				
				<cfset var AccountID ='0'>
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 1)>	
					<cfif GetPreferenceQuery.COUNT EQ 0 OR  PREFERENCECHECK EQ ''>
						<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Please check at least one Contact Preference!<br>") /> 
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					</cfif>
				</cfif>
				<cfif GetCPPSETUPQUERY.IncludeIdentity_ti EQ 1 AND (GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 2))  >			
					<cfif GetCPPSETUPQUERY.IdentifyGroupId_int GT 0>
						<cfset var countFieldId = 1>
						<cfset AccountID = ''>
						<cfloop list="#customFieldId#" index="iFieldId"  >
							<cfquery name="GetIdentifyQuery" datasource="#Session.DBSourceEBM#">
								SELECT 
									identifyId_int,
									identifyGroupId_int,
									type_int,
									size_int
								FROM 
									simplelists.cpp_identify
								WHERE
									IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
								AND 
									identifyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iFieldId#">
							</cfquery>
							<cfset var customFieldIdVal = structfind(arguments,'CUSTOMFIELDVALUE_#iFieldId#')>
							<cfif len(customFieldIdVal) NEQ GetIdentifyQuery.size_int >
								<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Size of Account ID field #countFieldId# is #GetIdentifyQuery.size_int#!<br>") /> 
								<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
							</cfif>
							<cfif NOT isnumeric(customFieldIdVal) AND GetIdentifyQuery.type_int EQ 1 >
								<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Type of Account ID field #countFieldId# is numeric only!<br>") /> 
								<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
							</cfif>
							<cfif NOT refindNoCase('[a-z]+$',customFieldIdVal) AND GetIdentifyQuery.type_int EQ 2 >
								<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Type of Account ID field #countFieldId# is Letters Only!<br>") /> 
								<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
							</cfif>
							
							<cfset countFieldId++ >
							<cfset AccountID = AccountID & customFieldIdVal>
						</cfloop>
						
					<cfelseif GetCPPSETUPQUERY.IdentifyGroupId_int EQ -2>
						<cfset AccountID =  BILLING1 & BILLING2 & BILLING3>
						<cfif BILLING1 EQ '' OR BILLING2 EQ '' OR BILLING3 EQ '' OR NOT isvalid('telephone', BILLING1 & BILLING2 & BILLING3)>
							<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Billing Number is not a telephone!<br>") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
						
					<cfelseif GetCPPSETUPQUERY.IdentifyGroupId_int EQ -1>
						<cfset AccountID =  emailAddress>
						<cfif NOT isvalid('email',emailAddress)>
							<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE#  Email Addess invalid!<br>") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
				
				</cfif>
				<cfif GetCPPSETUPQUERY.Type_ti EQ 1 OR(GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep EQ 3)>	
				
					<cfif VOICECHECK EQ 0  AND EMAILCHECK EQ 0 AND SMSCHECK EQ 0 >
						<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Please check at least one Contact Method!<br>") /> 
						<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
					</cfif>
					
					<cfif VOICECHECK EQ 1 >
						<cfif VOICETEXT1 EQ '' OR VOICETEXT2 EQ '' OR VOICETEXT3 EQ '' 
									OR NOT isvalid('telephone',VOICETEXT1& VOICETEXT2 & VOICETEXT3)>
							<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Voice Number is not a telephone!<br>") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
					
					<cfif SMSCHECK EQ 1 >
						<cfif SMSTEXT1 EQ '' OR SMSTEXT2 EQ '' OR SMSTEXT3 EQ '' OR NOT isvalid('telephone', SMSTEXT1 & SMSTEXT2 & SMSTEXT3)>
							<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# SMS Number is not a telephone!<br>") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
					
					<cfif EMAILCHECK EQ 1 >
						<cfif NOT isvalid('email',emailText)>	
							<cfset QuerySetCell(dataout, "MESSAGE","#dataout.MESSAGE# Email Address invalid!<br>") /> 
							<cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
						</cfif>
					</cfif>
				</cfif>
				<cfif accepConditionForm EQ 1 AND(GetCPPSETUPQUERY.Type_ti EQ 1 OR (GetCPPSETUPQUERY.Type_ti EQ 2 AND inpStep GTE listlen(GetCPPSETUPQUERY.STEPSETUP_VCH) - 1 )) >
				
					<cfif dataout.RXRESULTCODE GT 0>
						<cfif Preview EQ 0>
						
							<cfset groupContactList = [0]>
							<cfset preferenceArr = []>
							
							<cfif PREFERENCECHECK NEQ ''>
								<cfloop list="#PREFERENCECHECK#" index="index">
									<cfquery name="SelectPreference" datasource="#Session.DBSourceEBM#">
										SELECT 
											preferenceId_int,
											CPP_UUID_vch,
											Value_vch,
											Desc_vch,
											Order_int,
											GroupId_int
										FROM
											simplelists.cpp_preference 
										WHERE
											preferenceId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#index#">
										ORDER BY 
											Order_int DESC
									</cfquery>
									<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 0>
										<cfset arrayAppend(preferenceArr,SelectPreference.desc_vch )>
									<cfelse>
										<cfset arrayAppend(preferenceArr,SelectPreference.value_vch )>
									</cfif>
									<cfif NOT arrayfindNoCase(groupContactList,SelectPreference.groupId_int )>
										<cfset  arrayAppend(groupContactList , SelectPreference.groupId_int )>
									</cfif>
								</cfloop>
							</cfif>
							
							<cfif VOICECHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkVoiceExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
								 	WHERE
										ContactTypeId_int = 1
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									<cfif AccountId EQ 0>
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">
									</cfif>
								</cfquery>
								<cfif checkVoiceExist.RecordCount  GT 0 >
									<cfloop array="#groupContactList#" index="iCheckVoice">
										<cfif listfind(checkVoiceExist.grouplist_vch, iCheckVoice) EQ 0>
											<cfset checkVoiceExist.grouplist_vch &= iCheckVoice & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkVoiceExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 1
										AND 
											UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
										AND 
											CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
										<cfif AccountId EQ 0>
											AND 
												ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">
										</cfif>
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											1,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#VOICETEXT1& VOICETEXT2 & VOICETEXT3#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
									<cfset SaveTimezone(
											inpContactString = "#VOICETEXT1& VOICETEXT2 & VOICETEXT3#",
											inpUserid = GetCPPSETUPQUERY.userId_int,
											inpContactType = 1,
											inpCppId = AccountID	
										)>
								</cfif>
							</cfif>
							
							<cfif SMSCHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkSMSExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
									WHERE 
										ContactTypeId_int = 3
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									<cfif AccountId EQ 0>
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">
									</cfif>
								</cfquery>
								<cfif checkSMSExist.RecordCount GT 0 >
									<cfloop array="#groupContactList#" index="iCheckSMS">
										<cfif listfind(checkSMSExist.grouplist_vch, iCheckSMS) EQ 0>
											<cfset checkSMSExist.grouplist_vch &= iCheckSMS & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkSMSExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 3
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									<cfif AccountId EQ 0>
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">
										</cfif>
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											3,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
									<cfset SaveTimezone(
										inpContactString = "#SMSTEXT1 & SMSTEXT2 & SMSTEXT3#",
										inpUserid = GetCPPSETUPQUERY.userId_int,
										inpContactType = 3,
										inpCppId = AccountID	
											)>
								</cfif>
							</cfif>
							
							<cfif EMAILCHECK EQ 1 >
								<cfquery datasource="#Session.DBSourceEBM#" name="checkEmailExist">
									SELECT 
										grouplist_vch
								 	FROM
								 		simplelists.rxmultilist
								 	WHERE
										ContactTypeId_int = 2
									AND 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
									AND 
										CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
									<cfif AccountId EQ 0>
										AND 
											ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">
									</cfif>
								</cfquery>
								<cfif checkEmailExist.RecordCount GT 0 >
									<cfloop array="#groupContactList#" index="iCheckEmail">
										<cfif listfind(checkEmailExist.grouplist_vch, iCheckEmail) EQ 0>
											<cfset checkEmailExist.grouplist_vch &= iCheckEmail & ','>
										</cfif>
									</cfloop>
									<cfquery datasource="#Session.DBSourceEBM#">
										UPDATE 
									 		simplelists.rxmultilist
									 	SET 
									 		grouplist_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#checkEmailExist.grouplist_vch#">
									 	WHERE
											ContactTypeId_int = 2
										AND 
											UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">
										AND 
											CPPID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">
										<cfif AccountId EQ 0>
											AND 
												ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">
										</cfif>
									</cfquery>
								<cfelse>
									<cfquery datasource="#Session.DBSourceEBM#">
										INSERT INTO 
										 	simplelists.rxmultilist(
										 		UserId_int,
										 		ContactTypeId_int,
										 		TimeZone_int,
										 		CPPID_vch,
										 		ContactString_vch,
										 		grouplist_vch,
										 		CustomField1_vch,
										 		CustomField2_int,
										 		Created_dt,
										 		cpp_uuid_vch
										 	)
										VALUES(
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.userId_int#">,
											2,
											0,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AccountID#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#emailText#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(groupContactList)#,">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arraytoList(preferenceArr)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#language#">,
											NOW(),
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
										)
									</cfquery>
								</cfif>
							</cfif>
						</cfif>
						<cfset QuerySetCell(dataout, "MESSAGE","Create Contact Success!") />
					</cfif>
	            </cfif>
                           
            <cfcatch TYPE="any">
				<cfdump var="#cfcatch#">
				<cfset dataout =  QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="SaveTimezone" access="remote" output="false" hint="Check save timezone">
		<cfargument name="inpContactString" required="yes" >
		<cfargument name="inpUserid" required="yes" >
		<cfargument name="inpContactType" required="yes" >
		<cfargument name="inpCppId" required="yes" >
		
		<cfif (inpContactType EQ 1 OR inpContactType EQ 3) >
	         <!--- Get time zones, Localities, Cellular data --->
	         <cfquery name="GetTZInfo" datasource="#DBSourceEBM#">
	             UPDATE MelissaData.FONE AS fx JOIN
	              MelissaData.CNTY AS cx ON
	              (fx.FIPS = cx.FIPS) INNER JOIN
	              simplelists.rxmultilist AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
	             SET 
	               ded.TimeZone_int = (CASE cx.T_Z
	               WHEN 14 THEN 37 
	               WHEN 10 THEN 33 
	               WHEN 9 THEN 32 
	               WHEN 8 THEN 31 
	               WHEN 7 THEN 30 
	               WHEN 6 THEN 29 
	               WHEN 5 THEN 28 
	               WHEN 4 THEN 27  
	              END),
	              ded.LocationKey2_vch = 
	              (CASE 
	               WHEN fx.STATE IS NOT NULL THEN fx.STATE
	               ELSE '' 
	               END),
	               ded.LocationKey1_vch = 
	               (CASE 
	               WHEN fx.CITY IS NOT NULL THEN fx.CITY
	               ELSE '' 
	               END),
	               ded.CellFlag_int =  
	               (CASE 
	               WHEN fx.Cell IS NOT NULL THEN fx.Cell
	               ELSE 0 
	               END)                                                  
	              WHERE
	                 cx.T_Z IS NOT NULL   
	              AND
	                 UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserid#">                         
	              AND    
	                 ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(inpContactString), 255)#">
	              AND 
	                 CPPID_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCppId)#"> 
	         </cfquery>
                                             
     	</cfif> 
	</cffunction>
</cfcomponent>