<cfparam name="CPPUUID" default="" >
<cfparam name="Preview" default="0" >
<cfparam name="STEP" default="1" >
<cfinclude template="../../paths.cfm" >
<cfscript>
     // redirect if mobile browser detected
     if(reFindNoCase("android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) > 0){
         location("../mobile/?CPPUUID=#CPPUUID#&Preview=#Preview#&STEP=#STEP#");
     }
 </cfscript>
<cfset CPPUUID = trim(CPPUUID)>

<cfset portalLeft = ''>
<cfset portalRight = ''>
<cftry>
	<cfquery name="GetCPPSETUPQUERY" datasource="#Session.DBSourceEBM#">
		SELECT 
			c.CPP_UUID_vch,
			c.CPP_Template_vch,
			c.MultiplePreference_ti, 
			c.SetCustomValue_ti,
			c.IdentifyGroupId_int,
			c.UpdatePreference_ti, 
			c.VoiceMethod_ti, 
			c.SMSMethod_ti,
			c.EmailMethod_ti, 
			c.IncludeLanguage_ti, 
			c.IncludeIdentity_ti,
			c.StepSetup_vch,
			c.Active_int,
			c.Type_ti,
			c.CPP_Template_vch,
			c.IFrameActive_int,
			c.template_int,
			t.customhtml
		FROM 
			simplelists.customerpreferenceportal c
		LEFT Join
			simplelists.cpptemplate t
		ON
			c.template_int = t.tempalteId_int
		WHERE
			c.CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
		
	</cfquery>
	<cfif GetCPPSETUPQUERY.Active_int NEQ 1 OR GetCPPSETUPQUERY.IFrameActive_int NEQ 1>
		This Customer Preference Portal is offline!
		<cfexit>
	</cfif>
	
	<cfset templateId = trim(GetCPPSETUPQUERY.template_int)>
	
	<cfinclude template="/#sessionPath#/marketing/cpp/template/edit/templateStyle.cfm">
	
	<cfoutput>
		<link rel="stylesheet" href="#rootUrl#/#PublicPath#/css/administrator/popup.css">
		<link rel="stylesheet" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css">
		
		<cfif reFindNoCase("android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0 >
			<link rel="stylesheet" href="#rootUrl#/#PublicPath#/cppPortal/css/mobile.css">
		</cfif>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		<link rel="stylesheet" href="#rootUrl#/#PublicPath#/cppPortal/css/default.css">
		
	</cfoutput>
	
	<cfif GetCPPSETUPQUERY.SetCustomValue_ti EQ 1>
		<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				preferenceId_int,
				desc_vch
			FROM 
				simplelists.cpp_preference
			WHERE
				CPP_UUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CPPUUID#">
			AND 
				NOT IsNull(Value_vch)
			ORDER BY 
				Order_int 
		</cfquery>
	<cfelse>
		<cfquery name="GetPreferenceQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				preferenceId_int,
				desc_vch
			FROM 
				simplelists.cpp_preference
			WHERE
				CPP_UUID_vch = #CPPUUID#
			AND 
				IsNull(Value_vch)
			ORDER BY 
				Order_int 
		</cfquery>
	</cfif>
	
	<cfif GetCPPSETUPQUERY.IdentifyGroupId_int GT 0>
		<cfquery name="GetIdentifyGroupQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				groupName_vch,
				hyphen_ti
			FROM 
				simplelists.cpp_identifygroup
			WHERE
				CPP_UUID_vch = #CPPUUID#
			AND 
				IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfquery>
		<cfquery name="GetIdentifyQuery" datasource="#Session.DBSourceEBM#">
			SELECT 
				size_int,
				identifyId_int
			FROM 
				simplelists.cpp_identify
			WHERE
				IdentifyGroupId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetCPPSETUPQUERY.IdentifyGroupId_int#">
		</cfquery>
	</cfif>
	
	<cfset leftStrLength = FINdNoCase( '{%portal%}', GetCPPSETUPQUERY.customhtml)>
	<cfif leftStrLength GT 0>
		<cfset portalLeft = left(GetCPPSETUPQUERY.customhtml, leftStrLength - 1)>
		<cfset portalRight = replaceNoCase(GetCPPSETUPQUERY.customhtml, portalLeft , '') >
		<cfset portalRight = replaceNoCase(portalRight, '{%portal%}' , '') >
	</cfif>
	
	<cfcatch TYPE="any">
		<cfoutput>#cfcatch.MESSAGE#</cfoutput>
		<cfexit>
    </cfcatch>
	
</cftry> 

<cfoutput>#portalLeft#</cfoutput>

<cfset i =1>
<cfform method="post" name="cppLaunchForm" id="cppLaunchForm">
	<cfinput type="hidden" name="inpStep" id="inpStep" value= "">
	<cfinput type="hidden" name="preview" id="preview" value= "0">
	<cfinput type="hidden" name="includeLanguage" id="includeLanguage" value= "#GetCPPSETUPQUERY.INCLUDELANGUAGE_TI#">
	<cfinput type="hidden" name="accepConditionForm" id="accepConditionForm">
	<cfif GetCPPSETUPQUERY.IncludeIdentity_ti EQ 0>
		<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,2))>
	</cfif>
	<cfif GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 0>
		<cfset GetCPPSETUPQUERY.StepSetup_vch = listdeleteAt(GetCPPSETUPQUERY.StepSetup_vch, listfind(GetCPPSETUPQUERY.StepSetup_vch,4))>
	</cfif>
	
	<div id="containerLaunch">
		<cfloop list="#GetCPPSETUPQUERY.StepSetup_vch#" index="index">
			<cfinput name="stepVal#index#" type="hidden" id="stepVal#i#" value ="#index#" >
			<div class = "cppPortalRow" <cfif GetCPPSETUPQUERY.Type_ti EQ 2 AND i NEQ STEP> style="display:none" </cfif> id="stepContent<cfoutput>#i#</cfoutput>">
				<cfif index eq 1>
					<h2 class="cppStepTitle" id="preTitle">Step <cfoutput>#i#</cfoutput>:<span> (Contact Preference)</span></h2>
					<div class="cppPortalDesc" id="preDesc">
						Please select which services you wish to stay informed about by checking the box next to the selected service.
					</div>
					<div class="cppPortalContent">
						<cfloop query="GetPreferenceQuery">
							<div class="cppPortalContentRow" id="preContent">
								<cfif GetCPPSETUPQUERY.MULTIPLEPREFERENCE_TI EQ 1>
									<cfinput type = "checkbox" name="preferenceCheck" value="#GetPreferenceQuery.preferenceId_int#" class="removeCheck">
								<cfelse>
									<cfinput type = "radio" name="preferenceCheck" value="#GetPreferenceQuery.preferenceId_int#" class="removeCheck">
								</cfif>
								<cfoutput>#GetPreferenceQuery.Desc_vch#</cfoutput>
							</div>
						</cfloop>
					</div>
					<cfset i++>
				</cfif>
				<cfif index eq 2 >
					<h2 class="cppStepTitle" id="accTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Account ID)</span></h2>
					<div class="cppPortalDesc" id="accDesc">
						Please enter the Billing phone number associated with your AT&T account.
					</div>
					<div class="cppPortalContent">
						<div class="cppPortalContentRow">
							<cfif GetCPPSETUPQUERY.IDENTIFYGROUPID_INT EQ -2>
								<label id="accLabel" >Billing Number*</label> <br>
								<cfinput type="text" name="billing1" class="removeText" size="4">
								- <cfinput type="text" name="billing2" class="removeText" size="4">
								- <cfinput type="text" name="billing3" class="removeText" size="4">
							<cfelseif GetCPPSETUPQUERY.IDENTIFYGROUPID_INT EQ -1>
								<label id="accLabel">Email Address*</label><br>
								<cfinput type="text" name="emailAddress" class="removeText emailText">
							<cfelse>
								<label id="accLabel"><cfoutput>#GetIdentifyGroupQuery.groupName_vch#</cfoutput>*</label><br>
								<cfset iIdentify = 0>
								<cfloop query="GetIdentifyQuery">
									<cfif iIdentify GT 0 AND GetIdentifyGroupQuery.hyphen_ti EQ 1>
										 - 
									</cfif>
									<cfinput type="text" size="15" maxlength="#GetIdentifyQuery.size_int#" name="customFieldValue_#GetIdentifyQuery.identifyId_int#" class="removeText">
									<cfinput type="hidden" name="customFieldId" value="#GetIdentifyQuery.identifyId_int#">
									<cfset iIdentify ++>
								</cfloop>
							</cfif>
							
						</div>
					</div>
					<cfset i++>
				</cfif>
				
				<cfif index eq 3>
					<h2 class="cppStepTitle" id="contactTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Contact Method)</span></h2>
					<div class="cppPortalDesc" id="contactDesc">
						<p>Please check the contact method you prefer to be contacted by for the above services.</p>
					</div>
					<div class="cppPortalContent">
						<cfif GetCPPSETUPQUERY.VOICEMETHOD_TI EQ 1>
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="voiceCheck" value="1" class="removeCheck">
								<label class="label VoiceNumberText" id="contactLabel" >Voice Number:</label>
								<input type="text" name="voiceText1" class="removeText" size="4">
								- <input type="text" name="voiceText2" class="removeText" size="4">
								- <input type="text" name="voiceText3" class="removeText" size="4">
							</div>
						</cfif>
						<cfif GetCPPSETUPQUERY.SMSMETHOD_TI EQ 1>
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="smsCheck" value="1" class="removeCheck">
								<label class="label SMSNumberText" id="contactLabel" >SMS Number:</label>
								<input type="text" name="smsText1" class="removeText" size="4">
								- <input type="text" name="smsText2" name="smsText2" class="removeText" size="4">
								- <input type="text" name="smsText3" class="removeText" size="4">
							</div>
						</cfif>
						<cfif GetCPPSETUPQUERY.EMAILMETHOD_TI EQ 1>
							<div class="cppPortalContentRow">
								<input type = "checkbox" name="emailCheck" value="1" class="removeCheck" >
								<label id="contactLabel" >Email Address:</label>
								<input type="text" name="emailText" class="removeText emailText">
							</div>
						</cfif>
					</div>
					<cfset i++>
				</cfif>
				
				<cfif index eq 4 AND GetCPPSETUPQUERY.INCLUDELANGUAGE_TI EQ 1 >
					<h2 class="cppStepTitle" id="langTitle" >Step <cfoutput>#i#</cfoutput>: <span>(Desired Language)</span></h2>
					<div class="cppPortalDesc" id="langDesc" >
						Please select the desired communication language you want your service announcements to be delivered.
					</div>
					<div class="cppPortalContent">
						<div class="cppPortalContentRow">
							<label id="langLabel" >Select language:</label><br/>
							<select name="language" id="langContent">
								<option value="1">English</option>
							</select>
							
						</div>
					</div>
					<cfset i++>
				</cfif>
			</div>
			
		</cfloop>
		<cfinput type="hidden" name="totalStep" id="totalStep" value="#listlen(GetCPPSETUPQUERY.StepSetup_vch)#">
		<cfinput type="hidden" name="CPPUUID" value="#CPPUUID#">
		<div class = "cppPortalSubmit">
			<input type = "button" value="Back" id ="cppBack" onclick="return false;" style="display:none" >
			<cfif GetCPPSETUPQUERY.Type_ti EQ 1  >
				<input type = "submit" value="Submit" id ="cppSubmit" onclick="return false;" >
			<cfelse>
				<input type = "submit" value="Next" id ="cppNext" onclick="nextStep(); return false;" >
			</cfif>
		</div>
	</div>
	
</cfform>

<cfinclude template="launchJs.cfm">

<cfoutput>#portalRight#</cfoutput>