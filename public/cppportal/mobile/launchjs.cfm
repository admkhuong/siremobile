<div id="overlay" class="web_dialog_overlay"></div>
<!--- Invite User Popup --->
<div id="dialog_termCondition" class="web_dialog" style="display: none;">
	<cfform action="" method="POST">
		<div class="dialog_content">
			<input type="checkbox" id="accepCondition" class="removeCheck" name="accepCondition" value = "1" >
			<span id="textCondition" >
				<cfif GetCPPSETUPQUERY.CPP_Template_vch EQ ''>
					<p>
					Yes, I want to receive alerts about my AT&T account, as well as occasional news alerting me to new services I can add to my accounts 
					or new ways I can save with AT&T. By checking off this box and click on "Submit" below, I am providing you - AT&T - my electronic signature
					authorizing you to deliver me that information or news by text to my mobile device, voice to my selected phone or e-mail to the e-mail address that i have provided. <br>
					I also agrre to the, <a href ="##">Terms & Conditions</a> (please click the link to review those). Msg & data rates may apply. You will be able to text "HELP" for help and "STOP" to unsubscibe.
					</p>
				<cfelse>
					<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>
				</cfif>
			</span>
			
		</div>
		<div class="dialog_content">
			<button  
				type="button" 
				class="somadesign1"
				onClick="savePortal(); return false;"	
			>SUBMIT</button>
			<button 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('termCondition');"	
			>CANCEL</button>
		</div>
	</cfform>
</div>

<script type="text/javascript">
	
	var CPP_Template_vch = '<cfoutput>#GetCPPSETUPQUERY.CPP_Template_vch#</cfoutput>';
	
	$('#cppSubmit').click(function(){
		if(CPP_Template_vch == ''){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{
			ShowDialog('termCondition');
			$('#accepConditionForm').val(0);
		}
		
	});
	var step = 1;
	
	$('#cppBack').click(function(){
		if(step >= $('#totalStep').val()){
			$('#cppNext').val('Next');
		}
		$('#stepContent'+step).hide();
		step -=1;
		$('#stepContent'+step).show();
		$('#cppNext').show();
		if(step == 1){
			$('#cppBack').hide();
		}
	});
	
	function nextStep(){
		$('#inpStep').val($('#stepVal'+step).val()); 
		
		
		$.ajax({
        	type: "POST",
           	url:"../cfc/cppPortal.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           	dataType: "json", 
           	data: $('#cppLaunchForm').serialize(),
           	success: function(d){
				if (d.ROWCOUNT > 0) 
				{													
				
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{
	           				$('#cppBack').show();
	           				if(step == $('#totalStep').val() ){
	           					if(CPP_Template_vch == ''){
									$('#accepConditionForm').val(1);
	           						savePortalAjax();
	           					}else{
	           						ShowDialog('termCondition');
	           						$('#accepConditionForm').val(0);
	           					}
	           				}else{
	           					$('#stepContent'+(step)).hide();
	           					step +=1;
	           					$('#stepContent'+step).show();
	           				}
	           				
	           			}else{
							jAlert(d.DATA.MESSAGE[0], "Failure!");		
						}
	           		}
	           	}
         	}
		});
		if(step + 1 == $('#totalStep').val()){
			$('#cppNext').val('Submit');
		}
	}
	
	function savePortal(){
		if($('#accepCondition').is(':checked')){
			$('#accepConditionForm').val(1);
			savePortalAjax();
		}else{
			jAlert('Please check the term conditon checkbox!', "Checking term condition !");	
			$('#accepConditionForm').val(0);
		}
	}
	
	function savePortalAjax(){
		$.ajax({
           type: "POST",
           url:"<cfoutput>#rootUrl#/#publicPath#/cppPortal</cfoutput>/cfc/cppPortal.cfc?method=saveCppPortal&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: $('#cppLaunchForm').serialize(),
           success: function(d){
           	if (d.ROWCOUNT > 0) 
			{													
			
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{
           				CloseDialog('termCondition');
           				$('.removeCheck').removeAttr('checked');
          				$('.removeText').val('');
          				$('#accepConditionForm').val(0);
          				jAlert('Create Contact Success!', "Success!",function(result){
           					if(result){
           						location.href ="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/cppportal/thankyou/?cppUUID=<cfoutput>#CPPUUID#</cfoutput>";
           					}
           				});
           			}else{
           				CloseDialog('termCondition');
						jAlert(d.DATA.MESSAGE[0], "Failure!");		
					}
           		}
           	}
           	$('#accepCondition').removeAttr('checked');
           }
  		});
	}
	
	function ShowDialog(dialog_id){
		$("#overlay").show();
	    $("#dialog_" + dialog_id).fadeIn(300);
	    $(".groupRequire").hide();
	}
	
	function CloseDialog(dialog_id){
		//clear text box data
		$("#inpGroupContactName").val("");
		$("#overlay").hide();
	    $("#dialog_" + dialog_id).fadeOut(300);
	    $(".groupRequire").hide();
	    $('#accepCondition').removeAttr('checked');
	}
	
</script>