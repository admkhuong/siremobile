<cfset DBSourceEBM = "Bishop"/> 
<cftry>
	<cfquery datasource="#DBSourceEBM#" name="cronInvatation">
		DELETE FROM
			simpleobjects.invitationcodes
		WHERE 
			TIMESTAMPDIFF(HOUR,Created_dt, NOW()) > 8 
		AND 
			status_ti = 1
	</cfquery>
<cfcatch type="database">
	<cflog text="#cfcatch.message#" file="cronInvitation" application="true" log="Scheduler">
</cfcatch>
</cftry>