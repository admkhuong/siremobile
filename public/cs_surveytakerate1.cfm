<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Case Study - Survey take rate improvment</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<style>
#ebmimage1 {
	width:970px;
	height:323px;
	padding:0;
	border:0px solid;
	text-align:center;
	background-image:url(../images/ebm_i_main/FPO-Header.png);
}
</style>
<script type="text/javascript" language="javascript">

$(document).ready(function() {

$('.hover-create').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/Create-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/create.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
}
);



$(".hover-create").click(function(){
$(this).hide();
});
$(".hover-create").click(function(){
$("#createclick").show();
$("#createclick1").hide();
$(".hover-create1").show();
$("#createclick2").hide();
$(".hover-create2").show();
$("p").show();
});
$("#createclick").click(function(){
$(".hover-create").show();
$("#createclick").hide();
$("p").hide(); 

});

});
</script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {

$('.hover-create2').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/Analyze-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/analyze.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}
);




$(".hover-create2").click(function(){
$(this).hide();

$("#createclick2").show();
$("#createclick1").hide();
$(".hover-create1").show();
$("#createclick").hide();
$(".hover-create").show();
$("p").show();
});


$("#createclick2").click(function(){
$(".hover-create2").show();

$("#createclick2").hide();
$("p").hide();
});
});
</script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {

$('.hover-create1').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/Collect-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/ebm_i_main/collect.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}
);




$(".hover-create1").click(function(){
$(this).hide();
$("#createclick1").show();

$("#createclick").hide();
$(".hover-create").show();
$("#createclick2").hide();
$(".hover-create2").show();
$("p").show();
});


$("#createclick1").click(function(){
$(".hover-create1").show();
$("#createclick1").hide();
$("p").hide();
});
});
</script>
</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
        <div id="inner-bg">
            <div id="content">
                <div id="inner-box" align="center">
                    <div class="inner-main-hd">Case Study: Dial strategy change to improve survey take rate.</div>
                    <div class="inner-txt">We all know customer satisfaction is essential to the survival of our businesses. How do we find out whether our customers are satisfied? The best way to find out whether your customers are satisfied is to ask them. </div>
                    <div class="phonemain">
                        <div id="phone-icon"></div>
                        <div class="phone-txt">
                            <div class="phoneheading">Survey Take Rate</div>
                            <br />
                            <div class="mobile-txt"> It's nice to think that most of your customers will happily take your survey; after all, the information is to benefit them, right? Unfortunately, some customers don't have the time; others will earmark it for later and forget about it; others will start it and get called away.

05-10% is about the average response rate (surveys completed, out of surveys offered) that most companies get on customer-satisfaction surveys. There are a few things you can do to increase this, however.

				 </div>
                            <div class="learnmore"><a href="#rootUrl#/#PublicPath#/">Learn More &gt;</a></div>
                        </div>
                    </div>
               
              <!---     
		    
		    Survey Take Rate
		    
		    Was
		    
		    Is now
		    
		    
		    
		    
		     <div class="phonemain">
                        <div id="mail2-icon"></div>
                        <div class="phone-txt">
                            <div class="phoneheading">Direct mail</div>
                            <div class="mobile-txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</div>
                            <div class="learnmore"><a href="http://www.designboxindia.com/Project/ebm/innerpage.html##">Learn More &gt;</a></div>
                        </div>
                    </div>--->
                </div>
                <div id="inner-left-img"> <img src="#rootUrl#/#PublicPath#/images/casestudies/happysurvey_web.png" /></div>
            </div>
        </div>
        <div id="servicemainline"></div>
         <div id="blue-box">
             <div id="blue-box-text">Dedicated Staffing Design Approval Extrqaction Transformation Loading and Reporting services Data Feeds Monthly Billing</div>
         </div>
         <div id="contentblue">
             <div id="plan-box">
                 <div id="plan-img">
                     <div class="plan-txt-hd">PLAN 1</div>
                 </div>
                 <div class="plan-txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem.</div>
             </div>
             <div id="datatable">
                 <div class="plan-line1"></div>
                 <div class="plan-line2"></div>
                 <div class="plan-line3"></div>
                 <div class="plan-line4"></div>
                 <div class="plan-line1"></div>
                 <div class="plan-line2"></div>
                 <div class="plan-line3"></div>
                 <div class="plan-line4"></div>
                 <div class="plan-line1"></div>
                 <div class="plan-line2"></div>
                 <div class="plan-line3"></div>
                 <div class="plan-line4"></div>
             </div>
         </div>
    
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>