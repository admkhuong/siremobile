define([ 
         "jquery",
         "http://i5.walmartimages.com/dfw/4ff9c6c9-ae15/k2-_4298cdf5-2370-4fc5-ac21-1bafa4e95695.v1.js"
       ], function($) {
	return {
		init : function($root) {
			var iframeContainer = $root.find('.pharmacyMainContent');
			
			//zipcode submission
			$(iframeContainer).find('#pharmSearch').submit(function(){
				var url = $(iframeContainer).find('#pharmSearch').attr('action'),
					zipCode = $(iframeContainer).find('#zipCode').val(),
					isValidZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode),
					queryString = window.parent.location.toString();
				
				queryString = queryString.split('?');
				
				if(queryString[1]){
					//if there is anything past the '?'...
					queryString = queryString[1];					
				}else{
					queryString = '';
				}
				
				if(isValidZip){
					url += '?' + queryString;
					//if there's already some variables, add them
					if($(queryString).length > 0){
						url += '&';
					}
					url += '&sfsearch_single_line_address=' + zipCode;
					window.parent.location.href = url;
				}else{
					alert('This is not a valid US zip code.');
				}
				return false;
			});
			
			$(iframeContainer).find('.findPharmacy .submitButton').click(function(){
				$(iframeContainer).find('#pharmSearch').trigger('submit');
			});
			
			//Tabs
			$(iframeContainer).find('.threeup .title').click(function(e){
				var content = $(this).parent().find('.content'),
					allBoxes = $(iframeContainer).find('.threeup .content'),
					allDisplays = $(iframeContainer).find('.threeup .clicked'),
					clickDisplay = $(this).find('.clicked');
				
				$(allBoxes).hide();
				$(allBoxes).removeClass('active');
				$(allDisplays).css('display', 'none');
				$(content).addClass('active').css('display', 'block');
				$(clickDisplay).css('display', 'block');
				
				var threeUpHeight = clickDisplay.height() + content.height() + 18;
				$(iframeContainer).find('.threeup').height(threeUpHeight);
				setHeight();
			});
			
			var iframeName = $root.find('head').attr('id');
			var iframe = $(window.parent.document).find('#' + iframeName);
			function setHeight(){
				var fullHeight = iframeContainer.height() + 50;
				iframe.height(fullHeight + 'px');
			}
			
			//Responsive image map
			iframeContainer.find('img[usemap]').rwdImageMaps(iframeContainer);
			
		}
	};
});
