define([ "jquery" ], function($) {
	return {

		
		setup: function setup(overlayWidth,overlayHeight,html,styles,closeButtonStyles,backingStyles){
			//this is a copy of the overlay file found here:
			//http://i5.walmartimages.com/dfw/4ff9c6c9-4b99/k2-_99e357ac-5db4-468b-9db5-1424f564c0e5.v1.js
			var outerPage = window.parent.document,
			outerBody = outerPage.body;

			var scale = 1;
			var windowHeight = window.parent.innerHeight * .96;

			if(overlayHeight >= windowHeight){
				scale = windowHeight / overlayHeight;
			}
	
			//Adding styles to the overlay
			var windowStyling = {
				'position' : 'fixed',
				'padding' : '10px',
				'background-color' : '#fff',
				'border' : '4px solid #007dc6',
				'border-radius' : '10px',
				'overflow' : 'hidden',
				'width' : overlayWidth + 'px',
				'height' : overlayHeight + 'px',
				'z-index' : '-10'
			};
			var backingStyling = {
				'position' : 'fixed',
				'width' : '100%',
				'top' : '0px',
				'z-index' : '5000',
				'background' : '#fff',
				'filter' : 'alpha(Opacity=60)',
				'opacity' : '.6'
			};
			var closeButtonStyling = {
				'position' : 'absolute',
				'top' : '-14px',
				'right' : '-14px',
				'width' : '32px',
				'height' : '32px',
				'padding' : '0',
				'border' : '0',
				'border-radius' : '50%',
				'background-color' : '#007dc6',
				'color' : '#fff',
				'cursor' : 'pointer',
				'font-size' : '20px',
				'font-weight' : '400',
				'line-height' : '30px',
				'font-family' : 'WalmartIcons',
				'text-align' : 'center'
			};
			
			var styleText = '';
			$.each(windowStyling,function(key,value){
				styleText += key + ': ' + value + '; ';
			});
			var customStyles = styles || {};
			$.each(customStyles,function(key,value){
				styleText += key + ': ' + value + '; ';
			});
						
			//adding the backing
			var backingStyleText = '';
			$.each(backingStyling,function(key,value){
				backingStyleText += key + ': ' + value + '; ';
			});
			var customBacking = backingStyles || {};
			$.each(customBacking,function(key,value){
				backingStyleText += key + ': ' + value + '; ';
			});

			$(outerBody).append('<div class="customOverlayBacking" style="' + backingStyleText + '" >');
			var overlayBacking = $(outerBody).find('.customOverlayBacking');
			
		
			//adding the overlay box
			$(outerBody).append('<div class="customOverlayWindow" style="position: fixed; z-index:5001;">');
			var overlayWindow = $(outerBody).find('.customOverlayWindow');
			sizeAndPosition();
			overlayWindow.append('<div class="windowContent" style="' + styleText + '">');
			
			var closeButtonStyleText = '';
			$.each(closeButtonStyling,function(key,value){
				closeButtonStyleText += key + ': ' + value + '; ';
			});
			var customButton = closeButtonStyles || {};
			$.each(customButton,function(key,value){
				closeButtonStyleText += key + ': ' + value + '; ';
			});

			overlayWindow.append('<div class="closeButton" style="' + closeButtonStyleText + '">x</div>');
			
			overlayWindow.find('.windowContent').html(html);
		
			
			//closing overlay
			overlayWindow.find('.closeButton').hover(function(){
				$(this).css('cursor','pointer');
			});
			overlayWindow.find('.closeButton').click(function(e){
				e.preventDefault();
				overlayBacking.remove();
				overlayWindow.remove();
				e.stopPropagation();
			});
			//click on backing to close
			overlayBacking.hover(function(){
				$(this).css('cursor','pointer');
			});
			overlayBacking.click(function(){
				overlayBacking.remove();
				overlayWindow.remove();
			});
		
			//maintaining during screen resize
			$(window.parent).resize(function(){
				sizeAndPosition();
			});
		
			//sizing and positioning the box
			function sizeAndPosition(){
				var width = $(window.parent).width(),
					height = $(window.parent).height(),
					left = (width - overlayWidth)/2,
					top = (height - overlayHeight)/2;
				
				$(overlayWindow).css('width', overlayWidth + 'px');
				$(overlayWindow).css('height', overlayHeight + 'px');
				$(overlayWindow).css('left', left + 'px');
				$(overlayWindow).css('top', top + 'px');
			
				overlayBacking.css('height', height + 'px');
				$(overlayWindow).css('transform','scale(' + scale + ')');
			}
		}

	
	

	}
});