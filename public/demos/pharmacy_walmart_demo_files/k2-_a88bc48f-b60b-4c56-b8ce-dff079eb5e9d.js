define([ 
		 "jquery", 
		 "http://i5.walmartimages.com/dfw/4ff9c6c9-f3bc/k2-_73942f7c-dea0-42fc-baee-e06e82c4d268.v1.js"
		], function($,overlay) {
	return {
		init : function($root) {
			var iframeContainer = $root.find('.pharmacyMainContent');
			
			var width = 800,
				height = 600,
				content = iframeContainer.find('#overlayContent').html(),
				styles = {'padding' : '30px' };
			
			iframeContainer.find('.overlayLink').click(function(e){
				e.preventDefault();
				overlay.setup(width, height, content, styles);
				e.stopPropagation();
				return false;
			});
		}
	};
});