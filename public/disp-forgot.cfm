<cfsilent>
 
	<!--- Param FORM values. --->
    <cfparam name="FORM.captcha" type="string" default="" />
    <cfparam name="FORM.captcha_check" type="string" default="" />
     
    <cftry>
        <cfparam name="FORM.submitted" type="numeric" default="0" />
         
        <cfcatch>
            <cfset FORM.submitted = 0 />
        </cfcatch>
    </cftry>
    <cfset session.emailcheckattempt = 0>
    <!--- Check to see if the form has been submitted. --->
    <cfif FORM.submitted and session.emailcheckattempt lt 4>
        <cftry>
                <!--- check for valid email submitted --->
                <cfset REStr = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$">
                <cfif len(trim(form.emailcheck)) neq 0 and REFind(REStr,form.emailcheck) eq 1>
                
					<!--- find user info with submitted email address --->
                    <cfquery name="getInfo" datasource="#DBSourceEBM#">
                        SELECT	
                            CAST(AES_DECRYPT(Password_vch, '#application.EncryptionKey_UserDB#') as CHAR) as npassword,
                            EmailAddress_vch
                        FROM	
                            simpleobjects.useraccount 
                        WHERE	
                            EmailAddress_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.emailcheck#" maxlength="150"> and
                            CBPId_int = 1
                    </cfquery>
                    <!--- If user found - email them their login password --->
                    <cfif getInfo.recordcount neq 0>
                    
                        <cfmail 
							to="#trim(getInfo.EmailAddress_vch)#" 
							from="PASP <it@messagebroadcast.com>" 
							subject="#BrandShort# login information" 
							server="smtp.gmail.com" 
						    username="messagebroadcastsystem@gmail.com" 
						    password="mbs123456" 
						    port="465" 
						    useSSL="true" 
							type="html"
							>
                            <cfoutput>
                            <div style="display:inline-block; width:500px;">
                                <div style="margin:15px 0 15px 10px; display:inline-block;">
                                Your login information is as follows:
                                </div>
                                <div style="margin:0 0 10px 10px; display:inline-block;">
                                username: #getInfo.EmailAddress_vch#<br />
                                password: #getInfo.npassword#
                                </div>
                                <div style="margin:15px 0 15px 10px; display:inline-block;">
                                Thank You
                                Messagebroadcast
                                </div>
                            </div>
                            </cfoutput>
                        </cfmail>
                        <cfset success = "Send successfull. You have been dispatched an email containing your login information.">
                        <cfset session.emailcheckattempt = 0>
                    <cfelse>
                        <cfset session.emailcheckattempt = incrementValue(session.emailcheckattempt)>
                        <cfset error = "Your email did not match any user in our system. Please check your email and submit again or register as a new customer.<br>You are only allowed 3 total attempts before being blocked.">
                    </cfif>
            	<cfelse>
                	<cfset session.emailcheckattempt = incrementValue(session.emailcheckattempt)>
                    <cfset error = "You must submit a valid email address.">
                </cfif>
            <!--- Catch any errors. --->
            <cfcatch type="any">
             	<cfmail from="tbayramkul@messagebroadcast.com" to="tbayramkul@messagebroadcast.com" type="html" subject="#BrandShort# Password Recovery Catch">
	            </cfmail>
            </cfcatch>
        </cftry>
    <cfelse>
    
    	<cfmail from="it@messagebroadcast.com" to="it@messagebroadcast.com" type="html" subject="#BrandShort# Password Recovery Abuse">
			A user has exceeded the allowed amount of attempts at password recovery.<br />
	    </cfmail>
    
	</cfif>
</cfsilent>


<div style="width:450; display:inline-block;">
	<cfif IsDefined("error")>
        <div>
            <cfoutput>#error#</cfoutput>
        </div>
	</cfif>
    <cfif IsDefined("success")>
		<div>
            <cfoutput>#success#</cfoutput>
        </div>
	</cfif>
	<cfoutput>
    <cfform action="" method="post">
    <input type="hidden" name="submitted" value="1" />

    <div id="forgot_box">
    	<div>
        	<div style="padding:5px;"><strong>Enter your email bellow and we'll send you password reset instructions.</strong></div>
        </div>
        <div>
            <div class="overlap">
                <cfinput type="text" name="emailcheck" id="emailcheck"  class="mainRegister" required="yes"  
					message="Please enter the email address you signed up with"
				 	tooltip="Please enter the email address you signed up with" style="width:370px;" autocomplete="off">
            	<input type="submit" value="Send Instructions" class='button'/>
			</div>
			<div>
				<a href="#rootUrl#/#PublicPath#/home"><< Back to login screen.</a>
			</div>
        </div>
    </div>
	<div class="signup_box">
		<span>Not a user yet?</span>&nbsp;
		<a href="signup.cfm"><span>Create your account here.</span></a>
	</div>
    </cfform>
    </cfoutput>
    
    <div id="con"></div>

    
</div>

