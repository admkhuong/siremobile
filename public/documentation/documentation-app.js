angular.module('Documentation', ['ngRoute', 'hljs'])
.config(function($routeProvider, hljsServiceProvider) {
	$routeProvider.when('/', {
		controller:'ListCtrl',
		templateUrl:'documentation/list.html'
	})
	.when('/api/:apiIndex', {
		controller:'ApiCtrl',
		templateUrl:'documentation/api.html'
	})
	.when('/api/:apiIndex/detail/:detailIndex', {
		controller:'DetailCtrl',
		templateUrl:'documentation/detail.html'
	})
	.otherwise({
		redirectTo:'/'
	});
	
	hljsServiceProvider.setOptions({
		// replace tab with 2 spaces
		tabReplace: '    '
	});
})
.controller('ListCtrl', function($scope, $rootScope, Apis) {
	if($rootScope.data) {
		$scope.apis = $rootScope.data;
	} else {
		Apis.getDataAsync(function(data) {
			$scope.apis = $rootScope.data = data;
		});
	}
	
	$rootScope.config.baseUri = $scope.rootPath;
    $scope.config = $rootScope.config;
})
.controller('ApiCtrl', function($scope, $rootScope, $routeParams, Apis) {
	if($rootScope.data) {
		$scope.apis = $rootScope.data;
	    $scope.currentCollection = $scope.apis[$routeParams.apiIndex];
	} else {
		Apis.getDataAsync(function(data) {
			$scope.apis = $rootScope.data = data;
		    $scope.currentCollection = $scope.apis[$routeParams.apiIndex];
		});
	}
	
	$rootScope.config.baseUri = $scope.rootPath;
    $scope.apiIndex = $routeParams.apiIndex;
    $scope.highlight = function (objStr) {
    	return objStr.toLowerCase();
    }
})
.controller('DetailCtrl', function($scope, $rootScope, $http, $parse, $routeParams, Apis) {
	if($rootScope.data) {
		$scope.apis = $rootScope.data;
	    $scope.currentApi = $scope.apis[$routeParams.apiIndex].data[$routeParams.detailIndex];
	} else {
		Apis.getDataAsync(function(data) {
			$scope.apis = $rootScope.data = data;
		    $scope.currentApi = $scope.apis[$routeParams.apiIndex].data[$routeParams.detailIndex];
		});
	}
	
	$rootScope.config.baseUri = $scope.rootPath;
    $scope.config = $rootScope.config;
    
    $scope.source = 'aloha';
    
    $scope.exampleCode = function(path, parentIdx, idx) {
    	//var url = '/testService/cfc/ebmService.cfc?method=ReadFileContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&path=';
    	//url+= './../../webservice/docs/data/' + path;
	//Start:Changes By Adarsh , Dec 05
	
	   var url = '/public/cfc/ebmService.cfc?method=ReadFileContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&path=';
    	url+= './../../webservice/docs/data/' + path;
	
	//End:Changes By Adarsh , Dec 05 	
		
		
		$http.post(url, {})
		.success(function(data) {
			data = data.replace(new RegExp('&lt;', 'g'), '<');
			data = data.replace(new RegExp('&gt;', 'g'), '>');
			$scope.currentApi.verbs[parentIdx].examplecodedata = JSON.parse(data).trim();
		});
    }
    
    $scope.prettyJSON = '';
    $scope.tabWidth = 4;
    
    var _lastGoodResult = '';
    $scope.toPrettyJSON = function (objStr, tabWidth) {
      try {
        var obj = $parse(objStr)({});
      }catch(e){
        // eat $parse error
        return _lastGoodResult;
      }
      
      var result = JSON.stringify(obj, null, Number(tabWidth));
      _lastGoodResult = result;
      
      return result;
    };
    
    $scope.highlight = function (objStr) {
    	return objStr.toLowerCase();
    }
    
    $scope.highlightVariable = function (objStr) {
    	return objStr;
    }
})
.run(function($rootScope) {
	$rootScope.config = {
    	providerName: 'EBM REST API',
        apiName: 'ebm'
    };
})
.factory('Apis', function($http) {
	return {
	    getDataAsync: function(callback) {
	      $http.get('documentation/data.txt').success(callback);
	    }
	  };
});