<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
</head>

<body>
    
<cfinclude template="paths.cfm" >

<Style TYPE="text/css">
	.ui-tabs .ui-tabs-panel { display: block; border-width: 0 1px 1px 1px; padding: 0.5em; background: none; }
</style>
 
<!--- include main jquery and CSS here - paths are based upon application settings --->
<cfoutput>
    
    <!---<link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/pepper-grinder/jquery-ui-1.8.custom.css" rel="stylesheet" /> --->
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/BabbleSphere/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/rxform2.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/babblesphere.css" />
    
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.5.1.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
    
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 

 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
 
 	<!--- script for timout remiders and popups --->
    <!--- <cfinclude template="/SitePath/js/SessionTimeoutMonitor.cfm"> --->
 
<script TYPE="text/javascript">
	var CurrSitePath = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>';
</script>

</cfoutput>



<!--- Enable hover functionality ---> 
<script TYPE="text/javascript">

	$(function(){
		$('.ui-state-default').hover(
			function(){ $(this).addClass('ui-state-hover'); }, 
			function(){ $(this).removeClass('ui-state-hover'); }
		);
		
		
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"<div id='loadingDlg' style='display:inline;'><img class='loadingDlgRegisterNewAccount' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif' width='20' height='20'></div>" );
				}
			}
		});



		$("#RegisterNewAccountForm #RegisterNewAccountButton").click( function() { RegisterNewAccount(); return false;  }); 	
			
			  
		$("#inpNAPassword").keyup( function(event){  
			 PasswordStrength($(this).val());
		} );
		  
			  
		$("#LoginLink").click(function() {DoLogin();});
		$("#ForgotPassword").click(function() {return false;});
		
		$("#loadingDlg").hide();
		$("#loadingDlgLogin").hide();
		
			  
			  
	});


	function DoLogin()
	{		
		<!--- hide login button --->
		$("#LoginLink").hide();
		
		<!--- Show processing notice --->
		$("#loadingDlgLogin").show();
				
		try
		{
			<!--- Call LOCALOUTPUT javascript validations first --->
			if(ValidateInput($("#frmLogon #inpUserID").val()) && ValidateInput($("#frmLogon #inpPassword").val()))
			{
				<!--- Let the user know somthing is processing --->
				// $("#CurrentrxtXMLString").html('<img src="images/loading-small.gif" width="32" height="32">');
												
				$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserID : $("#frmLogon #inpUserID").val(), inpPassword : $("#frmLogon #inpPassword").val()}, 
					<!--- Default return function for authentication - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																					
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										<!--- Navigate tp sesion Home --->
										window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
									}
									else
									{
										<!--- Unsuccessful Login --->
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.REASONMSG[0]) != "undefined")
										{	 
											jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
					}		
										else
										{
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
										}
										
									}
								}
								else
								{<!--- Invalid structure returned --->	
									// Do something		
									
									jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
						
								}
							}
							else
							{<!--- No result returned --->
								// Do something		
								
								jAlertOK("Error while trying to login.\n" + "No response form remote server." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
							
							}
					} );
					
									
				
			}
			else
			{					
				jAlertOK("Error while trying to login.\n" + "Invalid input." , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
			}
		}
		catch(e)
		{
			jAlertOK("Error while trying to login.\n" + e , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
		}
			
			
	}
	
		
	function RegisterNewAccount()
	{			
		$("#loadingDlgRegisterNewAccount").show();		
	
		if($("#inpNAPassword").val() != $("#inpNAPassword2").val())
		{				
			jAlertOK("Passwords dont match - please retype.\n", "Failure!", function(result) { $("#loadingDlg").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpNAUserName").val() == "")
		{				
			jAlertOK("You must input a login name.\n", "Failure!", function(result) { $("#loadingDlg").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpNAPassword").val() == "")
		{				
			jAlertOK("You must input a valid password.\n", "Failure!", function(result) { $("#loadingDlg").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpMainPhone").val() == "")
		{
			jAlertOK("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlg").hide(); return false; } );							
						
			return false;	
		}
		
		if($("#inpMainEmail").val() == "")
		{				
			jAlertOK("You must input a valid email address.\n", "Failure!", function(result) { $("#loadingDlg").hide(); return false; } );							
						
			return false;	
		}
	
		$.getJSON( '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/cfc/users.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpNAUserName : $("#RegisterNewAccountForm #inpNAUserName").val(), inpNAPassword : $("#RegisterNewAccountForm #inpNAPassword").val(), inpMainPhone : $("#RegisterNewAccountForm #inpMainPhone").val(), inpNAUserName : $("#RegisterNewAccountForm #inpNAUserName").val(), inpMainEmail : $("#RegisterNewAccountForm #inpMainEmail").val() }, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{									
								jAlertOK("User has been added.", "Success!", function(result) 
																			{ 
																				$("#loadingDlg").hide();
																			
																			} );								
							}
							else
							{									
								jAlertOK("User has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->							
						jAlertOK("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgRegisterNewAccount").hide();
			} );		
	
		return false;

	}
	
	function PasswordStrength(password)  
	{  
		var desc = new Array();  
		desc[0] = "No Blanks";  
		desc[1] = "Weak";  
		desc[2] = "Weak";  
		desc[3] = "OK";  
		desc[4] = "Good";  
		desc[5] = "Tough";  
		desc[6] = "Strong"; 
	
		var strength   = 0;
		
		if (password.length > 0) strength++;
		
		if (password.length > 6) strength++;
	
		if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) strength++;
		
		if (password.match(/\d+/)) strength++;
		
		if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) strength++;
		
		if (password.length > 9) strength++;
		
		$("#pwdesc").html(desc[strength]);
		
		$("#pwstrength").removeClass();
		$("#pwstrength").addClass("strength" + strength);
		
		switch(strength)
		{
			case 1:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
			
			case 2:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
			
			case 3:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
			
			case 4:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
			
			case 5:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
				
			case 6:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;
		
			default:
				$("#pwstrength").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				break;	
				
		}
	}

</script>


<body>
<div id="BSHeader">

	<div id="BSHeadCenter">
    	<div id="BSHeadLogo">
    		<img src="images/HeaderLogo1Web.png" />
        </div>
        
        <div id="BSHeadLogin">
        
     <!---  		 
        	<form id="frmLogon" name="frmLogon" >
            	<table border="0" cellpadding="0" cellspacing="0">
                	<tr>
                    	<td align="left" nowrap>
                        	User Id
                        </td>
                        
                        <td align="left" nowrap>
                        	Password
                        </td>       
                        
                        <td>&nbsp;
                        	
                        </td>         			
                	</tr>
                    
                    <tr>
                    	<td align="left" nowrap>
                        	<input TYPE="text" id="inpUserID" name="inpUserID" label="User Id" />                			
                        </td>
                    
                    	<td align="left" nowrap>
                        	<input TYPE="password" id="inpPassword" name="inpPassword" label="Password" />
                        </td>         
                        
                        <td width="40px">
                            <div id="loadingDlgLogin" style="display:inline;">
                                <img class="loadingDlgLogin" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">
                            </div>

                        	<a id="LoginLink">Login</a>                        
                        </td>           
                    </tr>
                    
                    <tr>
                    	<td align="left" nowrap>&nbsp;
                        	
                        </td>
                        
                        <td align="left" nowrap>                        
                        	<a id="ForgotPassword">Forgot your password?</a>
                        </td>       
                        
                        <td>&nbsp;
                        	
                        </td>         			
                	</tr>
                    
                    	
                </table>
            </form>--->
        
        </div>
    </div>

</div>

<div id="BSContent">

	<div id="BSCLeft">
    	<img src="images/BSLogo1Web.png" />
    </div>
      
    <div id="BSCRight" align="center">
            
            
         <div id="BSCHomeContent">