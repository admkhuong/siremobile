<cfsetting showdebugoutput="no"/>

<cfparam name="inpCS" default="jpijeff@gmail.com" />
<cfparam name="inpCST" default="2" />


<cfparam name="ElectricVehicleFlag" default="0" />
<cfparam name="ForwardForFulfillment" default="" />
<cfparam name="AlreadyBeenSentFlag" default="0" />
<cfparam name="CustomerName" default="John Q. Customer" />
<cfparam name="GraphSource_1" default="" />
   
<cfinclude template="../../paths.cfm" >

<cfsavecontent variable="outContent">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ENBREL Insurance Reverification</title>


<body bgcolor="#f2f2f2">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#f2f2f2" width="100%"><tr><td align="center">
<table border="0" cellspacing="2" cellpadding="0" width="574">
	<tr>
		<td align="left"><br />
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left"><font face="Arial, Helvetica, sans-serif" size="2" color="#333333" style="font-size:11px;">&nbsp;&nbsp;Check your insurance coverage <a style="color:#1ba4a4;" href="http://www.enbrel.com/check-ENBREL-insurance.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><font color="#1ba4a4">online</font></a>.</font></td>
					<td align="right"><font face="Arial, Helvetica, sans-serif" size="2" color="#333333" style="font-size:11px;">For support, call 1-888-436-2735. | <a style="color:#1ba4a4;" href="#"><font color="#1ba4a4">View Online</font></a>&nbsp;&nbsp;</font></td>
				</tr>
			</table>
			<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="14" alt="" border="0" style="display:block;" /></div>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left"><a href="http://www.enbrel.com/important-safety-information.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_nav_isi.gif" width="179" height="24" alt="Important Safety Information" border="0" style="display:block;" /></a></td>
					<td align="left"><a href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_pi.pdf"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_nav_pi.gif" width="148" height="24" alt="Prescribing Information" border="0" style="display:block;" /></a></td>
					<td align="left"><a href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_mg.pdf"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_nav_medGd.gif" width="119" height="24" alt="Medication Guide" border="0" style="display:block;" /></a></td>
					<td align="left"><a href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_piu.pdf"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_nav_instruc.gif" width="133" height="24" alt="Instructions for Use" border="0" style="display:block;" /></a></td>
				</tr>
			</table>
			<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="17" alt="" border="0" style="display:block;" /></div>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f2f2f2">
				<tr>
					<td align="left"><a style="color:#1ba4a4;" href="http://www.enbrel.com/index.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/logo_enbrel.gif" width="121" height="47" alt="Enbrel" border="0" style="display:block;" /></a></td>
					<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="14" height="1" alt="" border="0" style="display:block;" /></td>
                    <td valign="top" width="100%" style="line-height:14px;"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:11px; line-height:13px;">For adults with psoriatic arthritis, moderate to severe rheumatoid arthritis, ankylosing spondylitis, or chronic moderate to severe plaque psoriasis who are candidates for systemic therapy or phototherapy, or for children with moderately to severely active polyarticular juvenile idiopathic arthritis</font></td>
				</tr>
			</table>
            	<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="10" alt="" border="0" style="display:block;" /></div>
			<!--content-->
			<table border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="100%">
				<tr>
					<td align="center">
						<table border="0" cellspacing="0" cellpadding="0" width="514">
                        	<tr>
                            	<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="514" height="30" alt="" border="0" style="display:block;" /></td>
                            </tr>
							<tr>
								<td align="left">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<font face="Helvetica, Arial, sans-serif" size="5" color="#2e5c9f" style="font-size:30px; line-height:34px;"><strong>A new year is coming.<br/>Make sure your plan will still cover ENBREL.</strong></font>
												<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="18" alt="" border="0" style="display:block;" /></div>
												<font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">
													Changes to your insurance for 2014 could affect your prescription drug coverage. Take a few minutes today to verify that your plan will still cover ENBREL.
												</font>
												<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="18" alt="" border="0" style="display:block;" /></div>
												<div><a href="http://www.enbrel.com/check-ENBREL-insurance.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_visitEnbrelInsurance.gif" width="363" height="35" alt="" border="0" style="display:block;" /></a></div>
												
											</td>
											<td valign="top"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/calender_jan.gif" width="121" height="206" alt="" border="0" style="display:block;" /></td>
										</tr>
									</table>
								</td>
							</tr>
                            <tr>
                            	<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="514" height="30" alt="" border="0" style="display:block;" /></td>
                            </tr>
						</table>
					</td>
				</tr>
			</table>
			<!--Bullets-->
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
				<tr>
					<td align="left">
						<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="left">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td><img  width="570" height="9" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bg_shadow_white.gif" alt="" /></td>
										</tr>
									</table>
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
											<td height="30"><img width="1" height="30" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
										</tr>
										<tr>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
											<td><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px; font-weight:bold;"><strong>You can also verify your coverage by:</strong></font></td>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
										</tr>
										<tr>
											<td colspan="3" height="30"><img width="1" height="30" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
										</tr>
										<tr>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
											<td>
												<table cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td align="right" valign="top" width="36"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/arrow_bullet.gif" width="36" height="18" alt="" style="display:block;" /></td>
														<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
														<td valign="top"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">Contacting your pharmacy for more information</font></td>
													</tr>
													<tr><td width="10" height="20"><img width="10" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
													<tr>
														<td align="right" valign="top" width="36"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/arrow_bullet.gif" width="36" height="18" alt="" style="display:block;" /></td>
														<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
														<td valign="top"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">Contacting your insurance company to see how changes may affect your coverage</font></td>
													</tr>
													<tr><td width="10" height="20"><img width="10" height="20" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
													<tr>
														<td align="right" valign="top" width="36"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/arrow_bullet.gif" width="36" height="18" alt="" style="display:block;" /></td>
														<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
														<td valign="top"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;"><strong>Calling 1-888-4ENBREL (1-888-436-2735) and selecting<br />option #5</strong> to speak to an <em>ENBREL Support</em>&trade; insurance specialist, available from 8 <font size="2" style="font-size:12px;">AM</font> to 8 <font size="2" style="font-size:12px;">PM</font> Eastern time, Monday through Friday</font></td>
													</tr>
												</table>
											</td>
											<td width="30"><img width="30" height="1" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
										</tr>											
										<tr>
											<td colspan="3" height="30"><img width="1" height="30" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!--Check Mark Section-->
			<table cellspacing="0" cellpadding="0" bgcolor="#f2f2f2">
				<tr><td colspan="5" height="9" style="line-height:9px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bg_quotebox_top.gif" width="570" height="9" alt="" border="0" style="display:block;" /></td></tr>
				<tr>
					<td align="center">
						<table cellspacing="0" cellpadding="0" border="0">
								<tr>
                                <tr>
                                    <td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="514" height="30" alt="" border="0" style="display:block;" /></td>
                                </tr>
									<td align="center">
										<table cellspacing="0" cellpadding="0" border="0">
                                        	
											<tr><td colspan="3"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px; font-weight:bold;"><strong>Any of the following events could affect your prescription coverage:</strong></font></td></tr>
											<tr>
                                            <tr>
                                                <td colspan="3" height="30"><img width="1" height="30" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
                                            </tr>
												<td align="center">
													<table cellspacing="0" cellpadding="0" border="0" width="514">
														<tr>
															<td align="right" valign="top" width="40" height="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/check_bullet.gif" width="40" height="24" alt="" style="display:block;" /></td>
															<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
															<td align="left" valign="bottom"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">A change of insurance plans</font></td>
														</tr>
														<tr><td width="10" height="15"><img width="10" height="15" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
														<tr>
															<td align="right" valign="top" width="40" height="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/check_bullet.gif" width="40" height="24" alt="" style="display:block;" /></td>
															<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
															<td align="left" valign="bottom"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">New plan selection by your employer</font></td>
														</tr>
														<tr><td width="10" height="15"><img width="10" height="15" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
														<tr>
															<td align="right" valign="top" width="40" height="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/check_bullet.gif" width="40" height="24" alt="" style="display:block;" /></td>
															<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
															<td align="left" valign="bottom"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">A change in jobs resulting in new insurance coverage</font></td>
														</tr>
														<tr><td width="10" height="15"><img width="10" height="15" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
														<tr>
															<td align="right" valign="top" width="40" height="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/check_bullet.gif" width="40" height="24" alt="" style="display:block;" /></td>
															<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
															<td align="left" valign="bottom"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">A period of unemployment</font></td>
														</tr>
														<tr><td width="10" height="15"><img width="10" height="15" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td></tr>
														<tr>
															<td align="right" valign="top" width="40" height="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/check_bullet.gif" width="40" height="24" alt="" style="display:block;" /></td>
															<td width="8" valign="top"><img width="8" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" style="display:block;" alt="" /></td>
															<td align="left" valign="bottom"><font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">Eligibility for Medicare or Medicare Prescription Drug Coverage</font></td>
														</tr>
													</table>
												</td>
										  </tr>	
                                          <tr>
                                            <td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="514" height="30" alt="" border="0" style="display:block;" /></td>
                                        </tr>										
										</table>
								  </td>
								</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bg_quotebox_bot.gif" width="570" height="11" alt="" border="0" style="display:block;" /></td></tr>
			</table>
			
			
			<!--Breakout-->
			<table cellspacing="0" cellpadding="30" border="0" bgcolor="#ffffff" style="padding:30px;">
				<tr>
					<td align="left" style="margin:30px;">
						<table border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="100%">
							<tr>
								<td align="left">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td valign="top"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/headphone_girl.gif" width="134" height="115" alt="" border="0" style="display:block;" /></td>
														<td valign="top">
															<font face="Helvetica, Arial, sans-serif" size="5" color="#2e5c9f" style="font-size:30px;"><strong>Now eligible for Medicare?</strong></font>
															<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="18" alt="" border="0" style="display:block;" /></div>
															<font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">
															An <em>ENBREL Support</em>&trade; Medicare Solutions advocate can help you understand your benefits and help determine which plans may be best for you.<br/>
															<strong>Call 1-888-4ENBREL and select option #6.</strong></font>
															<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="18" alt="" border="0" style="display:block;" /></div>
															<font face="Helvetica, Arial, sans-serif" size="3" color="#333333" style="font-size:16px; line-height:22px;">You can also find information online.</font>
															<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="18" alt="" border="0" style="display:block;" /></div>
															<div><a href="http://www.enbrel.com/ENBREL-Medicare-Solutions.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/btn_review.gif" width="339" height="35" alt="" border="0" style="display:block;" /></a></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="100%" ><tr><td align="center"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/img_divider_solid.gif" width="515" height="22" alt="" border="0" /></td></tr></table>
			<!--ISI-->
			<table border="0" cellspacing="0" cellpadding="30" bgcolor="#ffffff" style="padding:30px;">
				<tr>
					<td align="left">
					<font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">
						<strong>Prescription Enbrel<sup>&reg;</sup> (etanercept) is taken by injection.</strong>
						<br /><br />
						<font color="#2e5c9f"><strong>IMPORTANT SAFETY INFORMATION</strong></font>
						<br /><br />
						<font color="#2e5c9f"><strong>What is the most important information I should know about ENBREL?</strong></font>
						<br /><br />
						ENBREL is a medicine that affects your immune system. ENBREL can lower the ability of your immune system to fight infections. Serious infections have happened in patients taking ENBREL. These infections include tuberculosis (TB) and infections caused by viruses, fungi, or bacteria that have spread throughout the body. Some patients have died from these infections. Your doctor should test you for TB before you take ENBREL and monitor you closely for TB before, during, and after ENBREL treatment, even if you have tested negative for TB.
						<br /><br />
						There have been some cases of unusual cancers reported in children and teenage patients who started using tumor necrosis factor (TNF) blockers before 18 years of age. Also, for children, teenagers, and adults taking TNF blockers, including ENBREL, the chances of getting lymphoma or other cancers may increase. Patients with RA or psoriasis may be more likely to get lymphoma.
						<br /><br />
						<font color="#2e5c9f"><strong>Before starting ENBREL, tell your doctor if you:</strong></font><br /><br />
					</font>
					<table border="0" cellspacing="5" cellpadding="0">
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td>
							<td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have any existing medical conditions</font></td></tr><tr valign="top"><td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td>
							<td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Are taking any medicines, including herbals</font></td></tr><tr valign="top"><td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td>
							<td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Think you have, are being treated for, have signs of, or are prone to infection. You should not start taking ENBREL if you have any kind of infection, unless your doctor says it is okay</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have any open cuts or sores</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have diabetes, HIV, or a weak immune system</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have TB or have been in close contact with someone who has had TB</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Were born in, lived in, or traveled to countries where there is more risk for getting TB. Ask your doctor if you are not sure</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Live, have lived in, or traveled to certain parts of the country (such as, the Ohio and Mississippi River valleys, or the Southwest) where there is a greater risk for certain kinds of fungal infections, such as histoplasmosis. These infections may develop or become more severe if you take ENBREL. If you don't know if these infections are common in the areas you've been to, ask your doctor</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have or have had hepatitis B</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have or have had heart failure </font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Develop symptoms such as persistent fever, bruising, bleeding, or paleness while taking ENBREL</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Use the medicine Kineret<sup>&reg;</sup> (anakinra), Orencia<sup>&reg;</sup> (abatacept), or Cytoxan<sup>&reg;</sup> (cyclophosphamide)</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Are taking anti-diabetic medicines</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have, have had, or develop a serious nervous disorder, seizures, any numbness or tingling, or a disease that affects your nervous system such as multiple sclerosis or Guillain-Barr&eacute; syndrome</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Are scheduled to have surgery</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have recently received or are scheduled for any vaccines. All vaccines should be brought up-to-date before starting ENBREL. Patients taking ENBREL should not receive live vaccines</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Are allergic to rubber or latex</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Are pregnant, planning to become pregnant, or breastfeeding</font></td>
						</tr>
						<tr valign="top">
							<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/bullet.gif" width="19" height="20" alt="" style="display:block;" /></td><td align="left"><font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">Have been around someone with chicken pox</font></td>
						</tr>
					</table>
					<!--Side Effects-->
					<font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:20px;">
						<br />
						<font color="#2e5c9f"><strong>What are the possible side effects of ENBREL?</strong></font>
						<br /><br />
						ENBREL can cause serious side effects including: New <strong>infections</strong> or worsening of infections you already have; <strong>hepatitis B</strong> can become active if you already have had it; <strong>nervous system problems</strong>, such as multiple sclerosis, seizures, or inflammation of the nerves of the eyes; <strong>blood problems</strong> (some fatal); new or worsening <strong>heart failure</strong>; new or worsening <strong>psoriasis; allergic reactions; autoimmune reactions</strong>, including a lupus-like syndrome and autoimmune hepatitis. 
						<br /><br />
						<strong>Common side effects include</strong>: Injection site reactions, upper respiratory infections (sinus infections), and headache.
						<br /><br />
						In a medical study of patients with JIA, infection, headache, abdominal pain, vomiting, and nausea occurred more frequently than in adults. The kinds of infections reported were generally mild and similar to those usually seen in children. Other serious adverse reactions were reported, including serious infection and depression/personality disorder.
						<br/><br/>
						These are not all the side effects with ENBREL. Tell your doctor about any side effect that bothers you or does not go away.
						<br /><br />
						If you have any questions about this information, be sure to discuss them with your doctor. You are encouraged to report negative side effects of prescription drugs to the FDA. Visit <a style="color:#1ba4a4;" href="http://www.fda.gov/medwatch"><strong><font color="#333333">www.fda.gov/medwatch</font></strong></a>, or call 1-800-FDA-1088.
						<br /><br />
						Please see accompanying <a style="color:#333333;" href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_pi.pdf"><strong><font color="#333333">Prescribing Information</font></strong></a> and <a style="color:#333333;" href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_mg.pdf"><strong><font color="#333333">Medication Guide</font></strong></a>. 
						<br/><br/>
						<font color="#2e5c9f"><strong>INDICATIONS</strong></font>
						
						<br /><br />
						<strong>Moderate to Severe Rheumatoid Arthritis (RA)</strong><br/>
						ENBREL is indicated for reducing signs and symptoms, keeping joint damage from getting worse, and improving physical function in patients with moderate to severe rheumatoid arthritis. ENBREL can be taken with methotrexate or used alone.
						<br/><br/>
						<strong>Moderately to Severely Active Polyarticular Juvenile Idiopathic Arthritis (JIA)</strong><br/>
						ENBREL is indicated for reducing signs and symptoms of moderately to severely active polyarticular juvenile idiopathic arthritis (JIA) in children ages 2 years and older.
						<br/><br/>
						<strong>Psoriatic Arthritis</strong><br/>
						ENBREL is indicated for reducing signs and symptoms, keeping joint damage from getting worse, and improving physical function in patients with psoriatic arthritis. ENBREL can be used in combination with methotrexate in patients who do not respond adequately to methotrexate alone.
						<br/><br/>
						<strong>Ankylosing Spondylitis (AS)</strong><br/>
						ENBREL is indicated for reducing signs and symptoms in patients with active ankylosing spondylitis.
						<br/><br/>
						<strong>Moderate to Severe Plaque Psoriasis</strong><br/>
						ENBREL is indicated for the treatment of adult patients (18 years or older) with chronic moderate to severe plaque psoriasis who are candidates for systemic therapy or phototherapy.
					</font>
					</td>
				</tr>
			</table>
			
			<!--Footer-->
			<table border="0" cellspacing="0" cellpadding="29" bgcolor="#f2f2f2">
				<tr>
					<td align="left">
						<font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:12px; line-height:16px;">
						To unsubscribe from this insurance verification email, please call 1-888-4ENBREL<br />(1-888-436-2735) or <a style="color:#333333;" href="https://preferences.amgen.com/email_optout.jsp"><font color="#333333">click here</font></a>. Please do not reply to this e-mail. This mailbox is not monitored.
						<br /><br />
						To ensure delivery of our e-mails, add <a style="color:#333333;text-decoration:none;cursor:default;" href="mailto:Amgen@amgen.epsln1.com"><font color="#333333">Amgen@amgen.epsln1.com</font></a> to your Address Book.
						</font>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr><td colspan="3" bgcolor="#cccccc" height="1" style="line-height:1px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="1" alt="" border="0" style="display:block;" /></td></tr>
				<tr>
					<td valign="top" width="62%" align="left">
						<div><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="100%" height="25" alt="" border="0" style="display:block;" /></div>
						<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="22" height="1" alt="" border="0" />
						<a href="http://www.enbrel.com/index.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/logo_enbrel.gif" alt="" border="0" /></a><br />
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="30" height="1" alt="" border="0" style="display:block;" /></td>
								<td align="left">
									<font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:14px; line-height:17px;">
									<strong>Call 1-888-4ENBREL</strong> (1-888-436-2735), 7 days a week, for your questions about ENBREL.
									<br /><br />
									</font>
									<font face="Helvetica, Arial, sans-serif" size="2" color="#333333" style="font-size:12px; line-height:20px;">
										<a style="color:#333333;" href="http://www.enbrel.com/important-safety-information.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><font color="#333333">Important Safety Information</font></a><br />
										<a style="color:#333333;" href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_pi.pdf"><font color="#333333">Prescribing Information</font></a><br />
										<a style="color:#333333;" href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_mg.pdf"><font color="#333333">Medication Guide</font></a><br />
										<a style="color:#333333;" href="http://pi.amgen.com/united_states/enbrel/derm/enbrel_piu.pdf"><font color="#333333">Instructions for Use</font></a><br />
										<a style="color:#333333;" href="http://www.enbrel.com/about-amgen.jspx?WT.z_co=A&WT.z_in=RA&WT.z_ch=EML&WT.z_ag=A_RA_EML&WT.mc_id=A_RA_EML_Q42013Reverification&WT.vr.rac_dc=EML&WT.vr.rac_pa=EnbrelEML&WT.vr.rac_mp=Reverification&WT.vr.rac=A_RA_EML_Q42013Reverification"><font color="#333333">About Amgen</font></a><br />
										<a style="color:#333333;" href="http://www.amgen.com/privacy/privacy_terms.html"><font color="#333333">Privacy Policy</font></a><br />
										<a style="color:#333333;" href="http://www.amgen.com/privacy/use.html"><font color="#333333">Terms of Use</font></a>
									</font>
								</td>
								<td align="left"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="20" height="1" alt="" border="0" style="display:block;" /></td>
							</tr>
						</table>
					</td>
					<td bgcolor="#cccccc" width="1"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="1" height="1" alt="" border="0" style="display:block;" /></td>
					<td valign="top">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="24"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="24" height="24" alt="" border="0" style="display:block;" /></td>
								<td align="left" valign="top">
									<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="120" height="38" alt="" border="0" style="display:block; line-height:38px; height:38px;" />
									<div><a href="http://www.amgen.com/" style="border:none; outline:none; text-decoration:none;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/logo_amgen.gif" alt="" width="106" border="0" style="display:block; border:none; outline:none;" /></a></div>
									<div style="line-height:8px; height:8px"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="120" height="8" alt="" border="0" style="display:block; line-height:8px; height:8px;" /></div>
									<font face="Helvetica, Arial, sans-serif" size="2" color="#424242" style="font-size:12px; line-height:20px;">
										Manufactured by Immunex Corporation, Thousand Oaks,<br />
										CA 91320.<br />
										Marketed by Amgen Inc.<br />
										&copy;2013 Amgen Inc.,<br />
										Thousand Oaks, CA 91320.<br />
										All rights reserved.<br />
										70492-R2-V1
									</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/email_samples/amgen/spacer.gif" width="120" height="38" alt="" border="0" style="display:block; line-height:38px; height:38px;" />
</td></tr></table>

</body>
</html>




</cfsavecontent>


<cfoutput>

	<cfif TRIM(ForwardForFulfillment) NEQ "">
        <pre name="code" class="html">#outContent#</pre>
    <cfelse>
        #outContent#
    </cfif>

</cfoutput>


<link type="text/css" rel="stylesheet" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/css/syntaxhighlighter.css"></link>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shcore.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushcsharp.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushxml.js"></script>
<script language="javascript">
dp.SyntaxHighlighter.ClipboardSwf = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/clipboard.swf';
dp.SyntaxHighlighter.HighlightAll('code');
</script>





