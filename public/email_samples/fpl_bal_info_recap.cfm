<cfsetting showdebugoutput="no"/>

<html xmlns:v=3D"urn:schemas-microsoft-com:vml" xmlns:o=3D"urn:schemas-micr=
osoft-com:office:office" xmlns:w=3D"urn:schemas-microsoft-com:office:word" =
xmlns:x=3D"urn:schemas-microsoft-com:office:excel" xmlns:p=3D"urn:schemas-m=
icrosoft-com:office:powerpoint" xmlns:a=3D"urn:schemas-microsoft-com:office=
:access" xmlns:dt=3D"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:s=3D"=
uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882" xmlns:rs=3D"urn:schemas-microsof=
t-com:rowset" xmlns:z=3D"#RowsetSchema" xmlns:b=3D"urn:schemas-microsoft-co=
m:office:publisher" xmlns:ss=3D"urn:schemas-microsoft-com:office:spreadshee=
t" xmlns:c=3D"urn:schemas-microsoft-com:office:component:spreadsheet" xmlns=
:odc=3D"urn:schemas-microsoft-com:office:odc" xmlns:oa=3D"urn:schemas-micro=
soft-com:office:activation" xmlns:html=3D"http://www.w3.org/TR/REC-html40" =
xmlns:q=3D"http://schemas.xmlsoap.org/soap/envelope/" xmlns:rtc=3D"http://m=
icrosoft.com/officenet/conferencing" xmlns:D=3D"DAV:" xmlns:Repl=3D"http://=
schemas.microsoft.com/repl/" xmlns:mt=3D"http://schemas.microsoft.com/share=
point/soap/meetings/" xmlns:x2=3D"http://schemas.microsoft.com/office/excel=
/2003/xml" xmlns:ppda=3D"http://www.passport.com/NameSpace.xsd" xmlns:ois=
=3D"http://schemas.microsoft.com/sharepoint/soap/ois/" xmlns:dir=3D"http://=
schemas.microsoft.com/sharepoint/soap/directory/" xmlns:ds=3D"http://www.w3=
.org/2000/09/xmldsig#" xmlns:dsp=3D"http://schemas.microsoft.com/sharepoint=
/dsp" xmlns:udc=3D"http://schemas.microsoft.com/data/udc" xmlns:xsd=3D"http=
://www.w3.org/2001/XMLSchema" xmlns:sub=3D"http://schemas.microsoft.com/sha=
repoint/soap/2002/1/alerts/" xmlns:ec=3D"http://www.w3.org/2001/04/xmlenc#"=
 xmlns:sp=3D"http://schemas.microsoft.com/sharepoint/" xmlns:sps=3D"http://=
schemas.microsoft.com/sharepoint/soap/" xmlns:xsi=3D"http://www.w3.org/2001=
/XMLSchema-instance" xmlns:udcs=3D"http://schemas.microsoft.com/data/udc/so=
ap" xmlns:udcxf=3D"http://schemas.microsoft.com/data/udc/xmlfile" xmlns:udc=
p2p=3D"http://schemas.microsoft.com/data/udc/parttopart" xmlns:wf=3D"http:/=
/schemas.microsoft.com/sharepoint/soap/workflow/" xmlns:dsss=3D"http://sche=
mas.microsoft.com/office/2006/digsig-setup" xmlns:dssi=3D"http://schemas.mi=
crosoft.com/office/2006/digsig" xmlns:mdssi=3D"http://schemas.openxmlformat=
s.org/package/2006/digital-signature" xmlns:mver=3D"http://schemas.openxmlf=
ormats.org/markup-compatibility/2006" xmlns:m=3D"http://schemas.microsoft.c=
om/office/2004/12/omml" xmlns:mrels=3D"http://schemas.openxmlformats.org/pa=
ckage/2006/relationships" xmlns:spwp=3D"http://microsoft.com/sharepoint/web=
partpages" xmlns:ex12t=3D"http://schemas.microsoft.com/exchange/services/20=
06/types" xmlns:ex12m=3D"http://schemas.microsoft.com/exchange/services/200=
6/messages" xmlns:pptsl=3D"http://schemas.microsoft.com/sharepoint/soap/Sli=
deLibrary/" xmlns:spsl=3D"http://microsoft.com/webservices/SharePointPortal=
Server/PublishedLinksService" xmlns:Z=3D"urn:schemas-microsoft-com:" xmlns:=
st=3D"&#1;" xmlns=3D"http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dus-ascii"=
>
<meta name=3D"generator" content=3D"Microsoft Word 14 (filtered medium)">
<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif]--><style><!--
/* Font Definitions */
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-priority:99;
	mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-priority:99;
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.EmailStyle20
	{mso-style-type:personal;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
span.EmailStyle21
	{mso-style-type:personal;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
span.EmailStyle22
	{mso-style-type:personal;
	font-family:"Calibri","sans-serif";
	color:#1F497D;}
span.EmailStyle23
	{mso-style-type:personal-reply;
	font-family:"Calibri","sans-serif";
	color:#1F497D;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-size:10.0pt;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
/* List Definitions */
@list l0
	{mso-list-id:1036082333;
	mso-list-type:hybrid;
	mso-list-template-ids:-1843080232 67698689 67698691 67698693 67698689 6769=
8691 67698693 67698689 67698691 67698693;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;
	font-family:Symbol;}
@list l0:level2
	{mso-level-tab-stop:1.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level3
	{mso-level-tab-stop:1.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level4
	{mso-level-tab-stop:2.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level5
	{mso-level-tab-stop:2.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level6
	{mso-level-tab-stop:3.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level7
	{mso-level-tab-stop:3.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level8
	{mso-level-tab-stop:4.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level9
	{mso-level-tab-stop:4.5in;
	mso-level-number-position:left;
	text-indent:-.25in;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext=3D"edit" spidmax=3D"1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext=3D"edit">
<o:idmap v:ext=3D"edit" data=3D"1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang=3D"EN-US" link=3D"blue" vlink=3D"purple">
<div class=3D"WordSection1">
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ca=
libri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></span><=
/p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;">Dear Valued Customer,<o:p></o:p></span></=
p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;">Thank you for calling us today in referen=
ce to your account balance. Below you will find a summary of your balance, =
information on how to make a payment and request additional
 time to pay (if needed). &nbsp;</span><span style=3D"font-size:11.0pt;font=
-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p></o:=
p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Account Information:&nbsp;&nbsp;
<o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"684" style=3D"width:513.0pt;margin-left:-1.15pt;border-coll=
apse:collapse">
<tbody>
<tr style=3D"height:16.5pt">
<td width=3D"165" valign=3D"bottom" style=3D"width:124.0pt;border-top:solid=
 windowtext 1.0pt;border-left:solid windowtext 1.0pt;border-bottom:none;bor=
der-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:16.5pt=
">
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;;color:white">Account Number:<o:p></o:p></span></b></p>
</td>
<td width=3D"178" valign=3D"bottom" style=3D"width:133.25pt;border-top:soli=
d windowtext 1.0pt;border-left:solid windowtext 1.0pt;border-bottom:none;bo=
rder-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:16.5p=
t">
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;;color:white">Customer Name:<o:p></o:p></span></b></p>
</td>
<td width=3D"168" valign=3D"bottom" style=3D"width:125.75pt;border-top:soli=
d windowtext 1.0pt;border-left:solid windowtext 1.0pt;border-bottom:none;bo=
rder-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:16.5p=
t">
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;;color:white">Service Address<o:p></o:p></span></b></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border-top:solid=
 windowtext 1.0pt;border-left:solid windowtext 1.0pt;border-bottom:none;bor=
der-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:16.5pt=
">
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;;color:white">Inquiry Date &amp; Time
<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:19.5pt">
<td width=3D"165" valign=3D"bottom" style=3D"width:124.0pt;border:solid win=
dowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">01190-44477<o:p></o:p></span></p>
</td>
<td width=3D"178" valign=3D"bottom" style=3D"width:133.25pt;border:solid wi=
ndowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">TIMOTHY A APWISCH<o:p></o:p></span></p>
</td>
<td width=3D"168" valign=3D"bottom" style=3D"width:125.75pt;border:solid wi=
ndowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">1000 GEORGE AVE<o:p></o:p></span></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border:solid win=
dowtext 1.0pt;border-left:none;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">11/12/2012; 2:29 P.M.<o:p></o:p></span></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Balance Information:
<o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"347" style=3D"width:260.0pt;margin-left:-1.15pt;border-coll=
apse:collapse">
<tbody>
<tr style=3D"height:15.0pt">
<td width=3D"143" valign=3D"bottom" style=3D"width:107.0pt;border:solid win=
dowtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.=
4pt;height:15.0pt">
</td>
<td width=3D"79" valign=3D"bottom" style=3D"width:59.0pt;border:solid windo=
wtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4p=
t;height:15.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Amount<o:p></o:p></span></b></p>
</td>
<td width=3D"125" valign=3D"bottom" style=3D"width:94.0pt;border:solid wind=
owtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:15.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Due Date<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:19.5pt">
<td width=3D"143" valign=3D"bottom" style=3D"width:107.0pt;border-top:none;=
border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;bor=
der-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:19.5pt=
">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Past Due Balance<o:p></o:p></span></b></p>
</td>
<td width=3D"79" nowrap=3D"" valign=3D"bottom" style=3D"width:59.0pt;border=
:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:=
19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$498.36<o:p></o:p></span></p>
</td>
<td width=3D"125" nowrap=3D"" valign=3D"bottom" style=3D"width:94.0pt;borde=
r-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-rig=
ht:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">See Payment Extension Section<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:19.5pt">
<td width=3D"143" valign=3D"bottom" style=3D"width:107.0pt;border-top:none;=
border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;bor=
der-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:19.5pt=
">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Current Balance<o:p></o:p></span></b></p>
</td>
<td width=3D"79" nowrap=3D"" valign=3D"bottom" style=3D"width:59.0pt;border=
:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:=
19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">169.28<o:p></o:p></span></p>
</td>
<td width=3D"125" nowrap=3D"" valign=3D"bottom" style=3D"width:94.0pt;borde=
r-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-rig=
ht:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">11/29/2012<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:19.5pt">
<td width=3D"143" valign=3D"bottom" style=3D"width:107.0pt;border-top:none;=
border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;bor=
der-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:19.5pt=
">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Total Balance<o:p></o:p></span></b></p>
</td>
<td width=3D"79" nowrap=3D"" valign=3D"bottom" style=3D"width:59.0pt;border=
:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:=
19.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$667.64<o:p></o:p></span></p>
</td>
<td style=3D"border:none;padding:0in 0in 0in 0in" width=3D"125">
<p class=3D"MsoNormal">&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Important Account Condition:
<o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:#6600CC"><o:p>&nbsp;</o:p></span>=
</b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"347" style=3D"width:260.0pt;margin-left:-1.15pt;border-coll=
apse:collapse">
<tbody>
<tr style=3D"height:15.0pt">
<td width=3D"143" valign=3D"bottom" style=3D"width:107.0pt;border:solid win=
dowtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.=
4pt;height:15.0pt">
</td>
<td width=3D"79" valign=3D"bottom" style=3D"width:59.0pt;border:solid windo=
wtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4p=
t;height:15.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Amount<o:p></o:p></span></b></p>
</td>
<td width=3D"125" valign=3D"bottom" style=3D"width:94.0pt;border:solid wind=
owtext 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:15.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Due Date<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:12.6pt">
<td width=3D"143" rowspan=3D"2" valign=3D"bottom" style=3D"width:107.0pt;bo=
rder-top:none;border-left:solid windowtext 1.0pt;border-bottom:solid window=
text 1.0pt;border-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt=
;height:12.6pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Payment Extension #1<o:p></o:p></span></b></p>
</td>
<td width=3D"79" nowrap=3D"" valign=3D"bottom" style=3D"width:59.0pt;border=
:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:=
12.6pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$299.00<o:p></o:p></span></p>
</td>
<td width=3D"125" nowrap=3D"" valign=3D"bottom" style=3D"width:94.0pt;borde=
r-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-rig=
ht:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:12.6pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">11/13/2012<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:12.55pt">
<td width=3D"79" nowrap=3D"" valign=3D"bottom" style=3D"width:59.0pt;border=
:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:=
12.55pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$199.36<o:p></o:p></span></p>
</td>
<td width=3D"125" nowrap=3D"" valign=3D"bottom" style=3D"width:94.0pt;borde=
r-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-rig=
ht:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:12.55pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">11/27/2012<o:p></o:p></span></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoListParagraph"><b><span style=3D"font-size:11.0pt;font-famil=
y:&quot;Arial&quot;,&quot;sans-serif&quot;;color:red"><o:p>&nbsp;</o:p></sp=
an></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Final Notice Information:
</span></b><b><span style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;=
,&quot;sans-serif&quot;;color:#6600CC"><o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D">Final Notice #1<o:p></=
o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"1147" style=3D"width:860.25pt;margin-left:-1.15pt;border-co=
llapse:collapse">
<tbody>
<tr style=3D"height:36.75pt">
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:36.75pt=
">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Issue Date<o:p></o:p></span></b></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Final Notice Amount<o:p></o:p></span></b></p>
</td>
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Amount Paid<o:p></o:p></span></b></p>
</td>
<td width=3D"188" valign=3D"bottom" style=3D"width:141.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Amount Due to Avoid Disconnection<o:p></o:p></span></b></p>
</td>
<td width=3D"153" valign=3D"bottom" style=3D"width:115.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Final Notice Past Due Date<o:p></o:p></span></b></p>
</td>
<td width=3D"299" valign=3D"bottom" style=3D"width:224.25pt;border:solid wi=
ndowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.=
4pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Status
<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:14.25pt">
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">10/11/2012<o:p></o:p></span></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$287.23<o:p></o:p></span></p>
</td>
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$0.00<o:p></o:p></span></p>
</td>
<td width=3D"188" valign=3D"bottom" style=3D"width:141.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$287.23<o:p></o:p></span></p>
</td>
<td width=3D"153" nowrap=3D"" valign=3D"bottom" style=3D"width:115.0pt;bord=
er-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-ri=
ght:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">10/18/2012<o:p></o:p></span></p>
</td>
<td width=3D"299" nowrap=3D"" valign=3D"bottom" style=3D"width:224.25pt;bor=
der-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-r=
ight:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Pending Due to Payment Extension<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:35.0pt">
<td width=3D"167" nowrap=3D"" valign=3D"bottom" style=3D"width:125.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:35.0pt">
<p class=3D"MsoNormal"><span style=3D"font-size:10.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:10.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
</td>
<td width=3D"173" nowrap=3D"" valign=3D"bottom" style=3D"width:130.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:35.0pt">
</td>
<td width=3D"167" nowrap=3D"" valign=3D"bottom" style=3D"width:125.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:35.0pt">
</td>
<td width=3D"188" nowrap=3D"" valign=3D"bottom" style=3D"width:141.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:35.0pt">
</td>
<td style=3D"border:none;padding:0in 0in 0in 0in" width=3D"452" colspan=3D"=
2">
<p class=3D"MsoNormal">&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D">Final Notice #2
</span></b><b><span style=3D"font-size:11.0pt;font-family:&quot;Calibri&quo=
t;,&quot;sans-serif&quot;;color:#6600CC"><o:p></o:p></span></b></p>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:#1F497D"><o:p>&nbsp;</o:p></span></b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"1147" style=3D"width:860.25pt;margin-left:-1.15pt;border-co=
llapse:collapse">
<tbody>
<tr style=3D"height:36.75pt">
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:36.75pt=
">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Issue Date<o:p></o:p></span></b></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Final Notice Amount<o:p></o:p></span></b></p>
</td>
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Amount Paid<o:p></o:p></span></b></p>
</td>
<td width=3D"188" valign=3D"bottom" style=3D"width:141.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Amount Due to Avoid Disconnection<o:p></o:p></span></b></p>
</td>
<td width=3D"153" valign=3D"bottom" style=3D"width:115.0pt;border:solid win=
dowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.4=
pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Final Notice Expiration Date<o:p></o:p></span></b></p>
</td>
<td width=3D"299" valign=3D"bottom" style=3D"width:224.25pt;border:solid wi=
ndowtext 1.0pt;border-left:none;background:#0066CC;padding:0in 5.4pt 0in 5.=
4pt;height:36.75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:white=
">Status<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:14.25pt">
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border:solid win=
dowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">10/31/2012<o:p></o:p></span></p>
</td>
<td width=3D"173" valign=3D"bottom" style=3D"width:130.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$211.13<o:p></o:p></span></p>
</td>
<td width=3D"167" valign=3D"bottom" style=3D"width:125.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$0.00<o:p></o:p></span></p>
</td>
<td width=3D"188" valign=3D"bottom" style=3D"width:141.0pt;border-top:none;=
border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid wi=
ndowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$211.13<o:p></o:p></span></p>
</td>
<td width=3D"153" nowrap=3D"" valign=3D"bottom" style=3D"width:115.0pt;bord=
er-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-ri=
ght:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">11/07/2012<o:p></o:p></span></p>
</td>
<td width=3D"299" nowrap=3D"" valign=3D"bottom" style=3D"width:224.25pt;bor=
der-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-r=
ight:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Pending Due to Payment Extension<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:12.75pt">
<td width=3D"167" nowrap=3D"" valign=3D"bottom" style=3D"width:125.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:12.75pt">
</td>
<td width=3D"173" nowrap=3D"" valign=3D"bottom" style=3D"width:130.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:12.75pt">
</td>
<td width=3D"167" nowrap=3D"" valign=3D"bottom" style=3D"width:125.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:12.75pt">
</td>
<td width=3D"188" nowrap=3D"" valign=3D"bottom" style=3D"width:141.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:12.75pt">
</td>
<td style=3D"border:none;padding:0in 0in 0in 0in" width=3D"452" colspan=3D"=
2">
<p class=3D"MsoNormal">&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:red"><o:p>&nbsp;</o:p></span></b>=
</p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:red">Total amount due ($287.23 &#=
43; $211.13)=3D $498.36<o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Detailed Balance Information:</span></b><b><span style=
=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">
<span style=3D"color:red"><o:p></o:p></span></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<p class=3D"MsoNormal"><img width=3D"667" height=3D"238" id=3D"Picture_x002=
0_1" src=3D"cid:image001.png@01CDC0E4.41653280"><b><u><span style=3D"font-s=
ize:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><o:p></o:p=
></span></u></b></p>
<p class=3D"MsoNormal"><b><u><span style=3D"font-size:11.0pt;font-family:&q=
uot;Arial&quot;,&quot;sans-serif&quot;"><o:p><span style=3D"text-decoration=
:none">&nbsp;</span></o:p></span></u></b></p>
<p class=3D"MsoNormal"><b><u><span style=3D"font-family:&quot;Arial&quot;,&=
quot;sans-serif&quot;"><o:p><span style=3D"text-decoration:none">&nbsp;</sp=
an></o:p></span></u></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-family:&quot;Arial&quot;,&quo=
t;sans-serif&quot;">Payment Options- How to make a payment and request more=
 time:<o:p></o:p></span></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></b></p>
<table class=3D"MsoNormalTable" border=3D"0" cellspacing=3D"0" cellpadding=
=3D"0" width=3D"1083" style=3D"width:812.0pt;margin-left:-.75pt;border-coll=
apse:collapse">
<tbody>
<tr style=3D"height:15.0pt">
<td width=3D"227" nowrap=3D"" valign=3D"bottom" style=3D"width:170.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:15.0pt">
</td>
<td width=3D"217" style=3D"width:163.0pt;border:solid windowtext 1.0pt;bord=
er-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:15.0pt"=
>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Automated Phone System<o:p></o:p></span></b></p>
</td>
<td width=3D"159" style=3D"width:119.0pt;border:solid windowtext 1.0pt;bord=
er-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:15.0pt"=
>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">*FPL.com
<o:p></o:p></span></b></p>
</td>
<td width=3D"100" style=3D"width:75.0pt;border:solid windowtext 1.0pt;borde=
r-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:15.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Cost
<o:p></o:p></span></b></p>
</td>
<td width=3D"380" style=3D"width:285.0pt;border:solid windowtext 1.0pt;bord=
er-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:15.0pt"=
>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><b><spa=
n style=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:white">Requirements
<o:p></o:p></span></b></p>
</td>
</tr>
<tr style=3D"height:45.0pt">
<td width=3D"227" style=3D"width:170.0pt;border:solid windowtext 1.0pt;bord=
er-right:none;background:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:45.0pt"=
>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:white">Checking Account<br>
FPL Pay-by-Phone and <br>
FPL Pay-on-Line<o:p></o:p></span></b></p>
</td>
<td width=3D"217" style=3D"width:163.0pt;border:solid windowtext 1.0pt;bord=
er-top:none;padding:0in 5.4pt 0in 5.4pt;height:45.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Call: 1-800-226-3545
<br>
English: option 1,2, 3 <br>
Spanish: option 3,1,2, 3<o:p></o:p></span></p>
</td>
<td width=3D"159" style=3D"width:119.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:45.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><u><spa=
n style=3D"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:blue"><a href=3D"http://www.fpl.com/residential/pay_online.shtm=
l">http://www.fpl.com/residential/pay_online.shtml</a>&nbsp;<o:p></o:p></sp=
an></u></p>
</td>
<td width=3D"100" style=3D"width:75.0pt;border-top:none;border-left:none;bo=
rder-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:45.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Free<o:p></o:p></span></p>
</td>
<td width=3D"380" style=3D"width:285.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:45.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">FPL Bill Account Number
<br>
Bank Routing Number <br>
Checking Account Number<o:p></o:p></span></p>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Important: Cash only accounts are not eligible to use this option<o:p><=
/o:p></span></p>
</td>
</tr>
<tr style=3D"height:85.5pt">
<td width=3D"227" style=3D"width:170.0pt;border-top:none;border-left:solid =
windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:none;bac=
kground:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:85.5pt">
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:white">Credit Card- Western Union=
 Speed Pay<o:p></o:p></span></b></p>
</td>
<td width=3D"217" style=3D"width:163.0pt;border:solid windowtext 1.0pt;bord=
er-top:none;padding:0in 5.4pt 0in 5.4pt;height:85.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Call: 1-800-979-3967<br>
English: stay on the line<br>
Spanish: option 2<o:p></o:p></span></p>
</td>
<td width=3D"159" style=3D"width:119.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:85.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><u><spa=
n style=3D"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:blue"><a href=3D"http://www.fpl.com/customer/paycard.shtml">htt=
p://www.fpl.com/customer/paycard.shtml</a>&nbsp;<o:p></o:p></span></u></p>
</td>
<td width=3D"100" style=3D"width:75.0pt;border-top:none;border-left:none;bo=
rder-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:85.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">$3.25
<o:p></o:p></span></p>
</td>
<td width=3D"380" style=3D"width:285.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:85.5pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">FPL Bill Account Number
<br>
Zip Code of Service Address<br>
Credit Card Billing Address<br>
<br>
Master Card, Discover, American Express&reg; <br>
Debit Cards: Accel, Star, NYCE, pulse<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:57.0pt">
<td width=3D"227" style=3D"width:170.0pt;border-top:none;border-left:solid =
windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:none;bac=
kground:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:57.0pt">
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:white">Authorized Pay Agent Locat=
or<br>
(In Person Payments) <o:p></o:p></span></b></p>
</td>
<td width=3D"217" style=3D"width:163.0pt;border:solid windowtext 1.0pt;bord=
er-top:none;padding:0in 5.4pt 0in 5.4pt;height:57.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Call: 1-800-226-3545
<br>
English: option 1, 2, 5,1<br>
Spanish: option 3, 1, 2, 5, 1<o:p></o:p></span></p>
</td>
<td width=3D"159" style=3D"width:119.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:57.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Not Available<o:p></o:p></span></p>
</td>
<td width=3D"100" style=3D"width:75.0pt;border-top:none;border-left:none;bo=
rder-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:57.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Free<o:p></o:p></span></p>
</td>
<td width=3D"380" style=3D"width:285.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:57.0pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Zip code of area where you would like to pay
<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:71.25pt">
<td width=3D"227" style=3D"width:170.0pt;border-top:none;border-left:solid =
windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;border-right:none;bac=
kground:#0066CC;padding:0in 5.4pt 0in 5.4pt;height:71.25pt">
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:white">To determine if your accou=
nt qualifies for additional time to pay
<o:p></o:p></span></b></p>
</td>
<td width=3D"217" style=3D"width:163.0pt;border:solid windowtext 1.0pt;bord=
er-top:none;padding:0in 5.4pt 0in 5.4pt;height:71.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Call: 1-800-226-3545<br>
English: option 1, 2, 1<br>
Spanish: option 3, 1, 2, 1<o:p></o:p></span></p>
</td>
<td width=3D"159" style=3D"width:119.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:71.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><u><spa=
n style=3D"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&=
quot;;color:blue"><a href=3D"https://app.fpl.com/collections/PextHtmlContro=
ller?command=3Dpextapplication">https://app.fpl.com/collections/PextHtmlCon=
troller?command=3Dpextapplication</a>&nbsp;<o:p></o:p></span></u></p>
</td>
<td width=3D"100" style=3D"width:75.0pt;border-top:none;border-left:none;bo=
rder-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padd=
ing:0in 5.4pt 0in 5.4pt;height:71.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Free<o:p></o:p></span></p>
</td>
<td width=3D"380" style=3D"width:285.0pt;border-top:none;border-left:none;b=
order-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;pad=
ding:0in 5.4pt 0in 5.4pt;height:71.25pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><span s=
tyle=3D"font-size:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quo=
t;">Eligibility is determined strictly by the system taking into account: p=
ayment history, length of time as FPL customer, past-due amount,
 timeliness of prior payments, and compliance with previous payment extensi=
ons<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:14.25pt">
<td width=3D"1083" colspan=3D"5" style=3D"width:812.0pt;padding:0in 5.4pt 0=
in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;">*Registering your FPL bill account number=
 is required before a payment can be submitted via the website. Click here =
to register:<o:p></o:p></span></p>
</td>
</tr>
<tr style=3D"height:14.25pt">
<td width=3D"1083" colspan=3D"5" style=3D"width:812.0pt;padding:0in 5.4pt 0=
in 5.4pt;height:14.25pt">
<p class=3D"MsoNormal"><u><span style=3D"font-size:10.0pt;font-family:&quot=
;Arial&quot;,&quot;sans-serif&quot;;color:blue"><a href=3D"https://app.fpl.=
com/eca/EcaController?command=3Ddisplayregistration">https://app.fpl.com/ec=
a/EcaController?command=3Ddisplayregistration</a><o:p></o:p></span></u></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<p class=3D"MsoNormal"><b><span style=3D"font-size:11.0pt;font-family:&quot=
;Calibri&quot;,&quot;sans-serif&quot;;color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></b></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;">Thank you for your business.<o:p></o:p></=
span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ca=
libri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"line-height:12.0pt"><span style=3D"font-siz=
e:11.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">FPL Customer=
 Care Center<b><o:p></o:p></b></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;font-family:&quot;Ca=
libri&quot;,&quot;sans-serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"text-autospace:none"><span style=3D"font-si=
ze:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><o:p>&nbs=
p;</o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:10.0pt;font-family:&quot;Ar=
ial&quot;,&quot;sans-serif&quot;;color:red">Please do not reply.</span><spa=
n style=3D"font-size:10.0pt;font-family:&quot;Calibri&quot;,&quot;sans-seri=
f&quot;;color:red">
</span><span style=3D"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;=
sans-serif&quot;;color:red">Replies to this message are undeliverable and w=
ill not reach the FPL Customer Care Center. If you need to contact us, plea=
se use
</span><span style=3D"color:red"><a href=3D"http://www.fpl.com/contact/inde=
x.shtml?wt.svl=3D3"><span style=3D"color:red">Contact Us</span></a>.
</span><span style=3D"font-size:11.0pt;font-family:&quot;Calibri&quot;,&quo=
t;sans-serif&quot;;color:red"><o:p></o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"font-size:11.0pt;color:red"><o:p>&nbs=
p;</o:p></span></p>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
</div>
</body>
</html>