<!--- Footer start --->

	<footer id="footer">
		<div class="container">
			<div class="row">

				<div class="col-sm-12">

					<ul class="social-links">
					<!---	<li><a href="index.html#" class="wow fadeInUp"><i class="fa fa-facebook"></i></a></li>
						<li><a href="index.html#" class="wow fadeInUp" data-wow-delay=".1s"><i class="fa fa-twitter"></i></a></li>
						<li><a href="index.html#" class="wow fadeInUp" data-wow-delay=".2s"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="index.html#" class="wow fadeInUp" data-wow-delay=".4s"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="index.html#" class="wow fadeInUp" data-wow-delay=".5s"><i class="fa fa-envelope"></i></a></li>--->
					</ul>
					
					<p class="heart">
                        We <span class="fa fa-heart fa-2x animated pulse"></span> what we do and it shows!
                    </p>
                    <p class="copyright" rel1="ReactionX, LLC">
                        All software, systems, and content is &copy; 2000, 2015 <!---| Images: <a href="https://unsplash.com/">Unsplash</a> & <a href="http://zoomwalls.com/">Zoomwalls</a>--->
					</p>

				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</footer>

	<!--- Footer end --->