﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

<title>Forget Password - <cfoutput>#BrandShort#</cfoutput></title>
		
		<cfoutput>
			<style type="text/css">
			<!---	@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxform2.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/MB.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');--->
				@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
			</style>
		   <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
			<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>--->
		</cfoutput>
	    
        
        
<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }
	
	body
	{
		padding:0;
		border:none;
		height:100%;
		width:100%;
		min-width:1200px;	
		background: rgb(51,182,234); 
		background-size: 100% 100%;
		background-attachment: fixed; 	
		background-repeat:no-repeat;
	}
	
	#forgetpassword_box{
		padding: 12px;
	    text-align: left;
	    width: 51%;
	}
	
	.bluelogin{
		margin-left:0px;
	}
	
	.error-message{
		color:red;
		font-size: 14px;
		margin-top:-20px;
	}
	
	.login-row{
		margin-top:18px;
	}

</style>

    <script type="text/javascript" language="javascript">
				
	$(document).ready(function(){
		<!---$('#account_type_personal').click();
		if($('#account_type_company').is(':checked')){
  			$("#company_name").show();
  		}--->
		
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );

		$img.bind( 'load', function()
		{
			$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
									 
		$(window).scroll(function(){
			$('*[class^="prlx"]').each(function(r){
				var pos = $(this).offset().top;
				var scrolled = $(window).scrollTop();
				<!---$('*[class^="prlx"]').css('top', -(scrolled * 0.6) + 'px');		--->
				$('.prlx-1').css('top', -(scrolled * 0.6) + 'px');		
					
			});
		});
			
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		$("#txtEmailOrUserName").keydown(function(){ 
		   	$(".error-message").html("");
			$("#txtEmailOrUserName").css("border-color","rgba(0, 0, 0, 0.3)");
		});

	});
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}
	function RequestPasswordReset()
	{	
		var txtEmailOrUserName = $("#txtEmailOrUserName").val();
		if(txtEmailOrUserName!="")
		{
			var recaptcha = $("#recaptcha_response_field").val();
			if (recaptcha != "") {
				$.ajax({
					type: "POST",
					url:  '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/cfc/users.cfc?method=ResetPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  {
						inpUserName : $("#txtEmailOrUserName").val(),
						recaptcha_challenge_field : $("#recaptcha_challenge_field").val(), 
						recaptcha_response_field : $("#recaptcha_response_field").val()
						},										  
					error: function(XMLHttpRequest, textStatus, errorThrown)
					{
					 	<!---console.log(textStatus, errorThrown);--->
					},
					success:				  
					//Default return function for Do CFTE Demo - Async call back 
					function(d) 
					{												
						if(typeof(d.RXRESULTCODE) != "undefined")
						{
							if (d.RXRESULTCODE > 0) 
							{	 							
								//Navigate tp sesion Home
								jAlert("Check your email for a link with a special code needed to set your new password", "Request success!", function(result){
									window.location.href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/home.cfm";
								});
							}
							else
							{
								$(".error-message").html(d.MESSAGE);
								$("#txtEmailOrUserName").css("border-color","red");
								javascript:Recaptcha.reload();
//								jAlert(d.MESSAGE, "Request fail!", function(result){
//									javascript:Recaptcha.reload();
//								});
							}
						}
						else
						{
							jAlert("Error while resetting - please check your connection and try again later", "Request Fail!", function(result){
									javascript:Recaptcha.reload();
							});						
						}
					} 		
				});
			}
			else
			{	
				jAlert("Please enter captcha", "Warning Message!", function(result){
					javascript:Recaptcha.reload();
				});	
			}
		}
		else
		{
			jAlert("Please enter your email/username", "Warning Message!", function(result){
					javascript:Recaptcha.reload();
			});
		}		
		return false;				
	}	
	</script>
</head>
            
    <cfoutput>
	    <body>
    		<div id="main" align="center"> 
        
				<!--- EBM site footer included here --->
                <cfinclude template="act_ebmsiteheader.cfm">
                    
                 <div id="bannersection">
                    <div id="content" style="position:relative;">
                        
                        <div class="inner-main-hd" style="width:400px; margin-top:30px;">Forgot your password?</div>
                        
                        <div style="clear:both;"></div>
                        
                        <div class="header-content">Reset it below.</div>
                    </div>
                </div>
          
				<div id="inner-bg-m1" style="min-height: 450px">
        
					<div class="contentm1" id="forgetpassword_box">
						<div class="row-fluid">
						    <div class="dialog">
						        <div class="block">	            
									<p class="block-sub-heading">Enter your email or username below to reset your password:</p>
						            <div class="block-body">
						                <div id="login-form">
						                	<div class="login-row">
											<input class="login-input" type="text" id="txtEmailOrUserName" name="txtEmailOrUserName">
											</div>
											
											<div class="login-row">
												<div class="error-message"></div>
											</div>
											
											<div class="login-row" id="capthcha-row">
												<cfinvoke 
										             component="#PublicPath#.cfc.users"
										             method="render_reCAPTHCHA"
										             returnvariable="RetValRECaptchaData">
										             <cfinvokeargument name="ssl" value="true"/>
										             <cfinvokeargument name="theme" value="red"/>
										             <cfinvokeargument name="tabIndex" value="10"/> 
										        </cfinvoke>
											</div>
											
											<div class="login-footer">
												<button class="btn btn-primary pull-right loginbutton bluelogin small" onclick="RequestPasswordReset(); return false;" href="##">Submit</button>
											</div>
						                    <div class="clearfix"></div>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>


		<div class="transpacer"></div>

 		<!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>


