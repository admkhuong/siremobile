<cfinclude template="paths.cfm" >

	<cfparam name="UseLegacy" default="0">
	<!--- I am only doing this to support old browsers for old agent instalations - you MUST verify any functionality you turn on actually worksd in old browser target --->
	<cfinvoke component="#LocalSessionDotPath#.cfc.browserDetect" method="browserDetect" returnvariable="browserDetect"></cfinvoke>

<cfoutput>

	<!--- Allow degrade to legacy strucutures if IE 9 or less add other degrades later if needed --->
    <cfset UseLegacyStrucutres = false />
    <cfif UseLegacy GT 0 OR FindNoCase("MSIE 6", browserDetect.BrowserName) GT 0 OR FindNoCase("MSIE 7", browserDetect.BrowserName) OR FindNoCase("MSIE 8", browserDetect.BrowserName) OR FindNoCase("MSIE 9", browserDetect.BrowserName) >
    	<cfset UseLegacyStrucutres = true />        	
    </cfif>     

	<script src="#rootUrl#/#PublicPath#/js/jquery-1.9.1.min.js"></script>
   <!--- <script src="#rootUrl#/#PublicPath#/js/jquery-migrate-1.1.0.min.js"></script>--->
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    <script src="#rootUrl#/#PublicPath#/js/jquery.cookie.js"></script>  
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    
	<!--- Main slider JS script file ---> 
    <!--- Create it with slider online build tool for better performance. --->
    <script src="#rootUrl#/#PublicPath#/js/royalslider/jquery.royalslider.min.js"></script>
    
    <link href='#rootUrl#/#PublicPath#/css/inlineform.css' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />
            
	<!--- http://stackoverflow.com/questions/4015816/why-is-font-face-throwing-a-404-error-on-woff-files --->
    <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ebmstyle_i_main.css" />
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/ebmmaster_i_main.css">
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/icomoon.css">
   
    <link rel="stylesheet" href="#rootUrl#/#PublicPath#/js/royalslider/royalslider.css">
    <link rel="stylesheet" href="#rootUrl#/#PublicPath#/js/royalslider/skins/ebm/rs-default.css">
 
	<style>
		
		@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css') all;
		@import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;		
		@import url('#rootUrl#/#PublicPath#/css/surveyprint.css') print;
	
	</style>

	<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/administrator/popup.css">
 
	     
    <!--- Google Analytics --->
    <cfif cgi.server_port_secure eq 0 AND FindNoCase("https", rootUrl) EQ 0 >
		
		<script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            
              ga('create', 'UA-41345823-2', 'auto');
              ga('send', 'pageview');
        
        </script>
    
    <cfelse>
    
        <script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga');
		
			 ga('create', 'UA-41345823-2', 'auto');
			 ga('send', 'pageview');
        </script>
            
    </cfif>

</cfoutput>

<!--- Load path specific images here--->
<style>

	body
	{
		padding:0;
		border:none;
		min-width:1200px;			
	}
	
	<!--- Background wrap and will still work modern ios safari--->
	#background_wrap 
	{
		z-index: -1;
		top: 0;
		left: 0;
		position: fixed;
		height: 100%;
		width: 100%;
		<!---	background-attachment: fixed;--->
		<!----webkit-background-size: 2650px 1440px;--->
		<!---min-height:2880px;--->
		background-image: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg');
		<!---
		<cfif UseLegacyStrucutres > 			
			
		<cfelse>
			position: fixed;
		</cfif>		
		--->		
		background-size: 100% 100%;
		background-repeat:no-repeat;
			
	}
	
	
	.ui-dialog .ui-dialog-titlebar 
	{
		background-image: url("../../../public/css/images/header.jpg");
		background-repeat: repeat-x;
		border-radius: 5px 5px 0 0;
		cursor: move;
		height: 100px;
	}
	
	body .ui-dialog .ui-dialog-titlebar 
	{
		color: #ffffff;
		font-family:Helvetica, Arial, sans-serif;
		font-size: 15px;
	}
	
	.ui-icon-closethick
	{
		display:none !important;	
		
	}
		
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		background: none;
		border: none;   
	}
	
	.ui-dialog .ui-dialog-titlebar-close 
	{
		height: 26px;
		margin: 0;
		padding: 0;
		position: absolute;
		right: 5px;	
		top: 0;
		width: 27px;
	}
	
	.ui-dialog .ui-dialog-title 
	{
		display: inline-block;
		float: left;
		font-size: 14px;
		margin: 70px 16px 0.2em 25px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		text-transform: uppercase;
	}

	.noTitleStuff .ui-dialog-titlebar {display:none}
	.noTitleStuff .ui-dialog {border:none !important;}

	
	#PreLoadIcon
	{
		background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/infinite-gif-preloader-grey.gif");
		background-position: center center;
		background-repeat: no-repeat;
		height: 200px;
		left: 50%;
		margin: -100px 0 0 -100px;
		position: absolute;
		top: 50%;
		width: 200px;			
	}


<cfif UseLegacyStrucutres > 	
        
		
		.LegacyHide
		{
			display:none !important;
		}
				
		
		.header-content {
			color: #333;
			
		}

	</cfif>	

</style>


<script type="text/javascript">
	
	/* Copyright (C) 2001,2014 Jeffery Lee Peterson - All Rights Reserved
	 * You may NOT in any part use, distribute or modify this code under the
	 * terms of the EBM license
	 *
	 * You should have received a copy of the EBM license with
	 * this file. If not, please write to: jp@ebmui.com 
	 */
	
	$(function() {		
	
			
		
	});
	
	function isValidRequiredField(fieldId){
		if($("#" + fieldId).val() == ''){
			$("#err_" + fieldId).show();
			return false;
		}else{
			$("#err_" + fieldId).hide();
			return true;
		}
	}
	
	
</script>

        