<cfoutput>

 <div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">CPP Overview</h3>

        
     <p>
        Customer Preference Portals (CPP) can be used to capture both customer preference data accross multiple channels. CPPs can be use to capture addtiional customer information into CDFs.
     </p>
            
     <ul>
    	<li>Via Online Tools</li>
        <li>Via REST API</li>
        <li>Via EAI</li>                   
    </ul>
    
    <p>New Portals up in running in less than an hour.</p>
    
    <p>Access via API, iFrame or Standalone URL. Vanity URLs available.</p> 
    
    <p>Mobile Optimized - Capture from Online or Mobile. Most Phones and Tablets Compatible</p>
    
    <p>
	   Ability to set multiple contact locations (Voice, SMS, e-Mail, other) By account and by preference.
    </p>
    
    <p>
       Audit history of all communications sent via the portal
    </p>
    
            
</div>      
        
<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Online #BrandShort# tools</h3>
            
     <p>
        Consumer directed Collection and Updating. Make the portal your own. Brand consitancy.
     </p>
     
     <img src="#rootUrl#/#publicPath#/help/images/loginscreencppsample.png" width="595" height="276" style="" />
     <BR>
     <img src="#rootUrl#/#publicPath#/help/images/cppsamplewithcdf.png" width="1067" height="905" style="" />
            
    <h5>Step 1 - Description</h5> 
    
    <img src="#rootUrl#/#publicPath#/help/images/cppstep1.png" width="812" height="395" style="" />  
    
    <p>Basic CPP definition</p>
    <p>
        Custom Name for each Portal - used for managing multiple portals.
    </p>
    
    <p>
        Generic URL for serving web forms for embedding in other sites.        
    </p>
    
    <p>
        Layout can be single page or succession of pages.            
    </p>
    
    <p>
        Optionally require WYSIWYG specified terms and conditions. When checked forces consumers to click a box to accept terms.
    </p>
    
    
    <h5>Step 2 - Modules</h5> 
    
    <p>
        Define Modules used within CPP
    </p>
    
    <ul>
    	<li><p>Login</p>
        <p>CPPs have three authentication options:</p>
        <p style="margin-left:10px;">User Defined Id. The user can sign up and authenticate with social media login or through email address.</p>
        <p style="margin-left:10px;">Unique account identifier from within a previously authendicated site. Unique account identifier is passed in with the protal request as an optional parameter.</p>
        <p style="margin-left:10px;">Open list capture. Allow people to authorize access to a CPP without any authentication.</p>
        	<img src="#rootUrl#/#publicPath#/help/images/loginmodule.png" width="488" height="106" style="" />          
        </li>
                
        <li><p>Custom HTML</p>
        <p>All modules can be surrounded with customizable HTML</p>
        	<img src="#rootUrl#/#publicPath#/help/images/customhtmlmodule.png" width="487" height="335" style="" />  
        </li>
        
        <li><p>CDF</p>
        	<p>One or more custom data fields (CDF) can be inserted into the CPP to capture additional consumer information. Samples include name, items of interest from a multi-choice list, prescription Id and number and more. </p>
        	<img src="#rootUrl#/#publicPath#/help/images/cdfmodule.png" width="487" height="282" style="" />  
        </li>
        
        <li><p>Opt In Verification</p>
        	<p>Additional messaging can be triggered to confirm the consumer's choices to opt in via the target device and channel.</p>
        	<img src="#rootUrl#/#publicPath#/help/images/optinverificationmodule.png" width="485" height="122" style="" />
            <BR>
            <img src="#rootUrl#/#publicPath#/help/images/campaignpicker.png" width="1199" height="656" style="" />  
            <BR>
            <img src="#rootUrl#/#publicPath#/help/images/optinsms.png" width="265" height="300" style="" /> 
        </li>
        
        <li><p>Contact Preferences</p>
        	<p>All preferences are stored in one or more unique lists per preference. Consumers can opt into multiple preferences at once or pick and choose. Add programs/communications as needed, with communication parameters</p>
        	<img src="#rootUrl#/#publicPath#/help/images/preferencesmodule.png" width="486" height="643" style="" />  
        </li>
        
        <li><p>Contact Methods</p>
        	<p>Choose which contact methods you want per portal. Limit by SMS, Voice, eMail or any cobination.</p>
        	<img src="#rootUrl#/#publicPath#/help/images/contactmethodsmodule.png" width="486" height="433" style="" />  
        </li>
        
        <li><p>Language Preferences</p>
        	<p>Allow consumer to to choose language preference.</p>
        	<img src="#rootUrl#/#publicPath#/help/images/languageprefmodule.png" width="487" height="357" style="" />  
        </li>
                       
    </ul>
        
        
</div>      
           
        
<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">API</h3>
        
    <p>
    	All Customer Preference Portals (CPP) features and several more can be accessed via REST APIs. CPPs can also be accessed in a hybrid approach, where a combination of APIs and online tools may be utilized.
    </p>
        
</div>   

    
<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Digital Signature</h3>
            
     <p>
        All updates to preferences are tracked and logged. Agent interface forms are available for looking up history.
     </p>
     
     <p>Terms and conditions are customizable to your specific needs.</p>
     <img src="#rootUrl#/#publicPath#/help/images/digitalsignature.png" width="513" height="272" style="" />
     
     <p>Terms and conditions require a user to click a check box. It is not enabled by default..</p>
     <img src="#rootUrl#/#publicPath#/help/images/digitalsig2.png" width="465" height="186" style="" />
          
     <p>Any time consumers update their prefences, they will be required to acknowledge terms and conditions.</p>
     <img src="#rootUrl#/#publicPath#/help/images/digitalsig3.png" width="1069" height="906" style="" />
           
</div>   
   
        
        
</cfoutput>
        
    
    