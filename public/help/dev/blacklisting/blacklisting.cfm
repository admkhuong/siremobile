<cfoutput>           
    <div id="inner-bg-m1" style="padding-bottom:25px; text-align:left;">
        <div style="display:inline; text-align:left;" >                              
        	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
        	<h2 style="line-height:32px; color: ##f8853b;">Black Listed Phone Number</h2>
        </div>
        <div style="padding-left:42px; padding-top:15px; width:700px;">
            <p style="font-weight:300;">
            	EBM database receive blacklisting phonenumber form telecoms everyday. Before sending SMS/Calls, system scrub data against this blacklisting databases. If system detect phone number in the EBM backlist database, then phone number will be added in opt-out list. Else, system will Proceed for sending SMS or Calls.
            </p>
            <img src="#rootUrl#/#publicPath#/help/dev/images/blacklist.jpg">                     
        </div>
        
        <div style="padding-left:42px; padding-top:15px; width:700px;">
        	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
            <h3 style="margin-left: 20px;">Initial Setup</h3>
        	
             <p style="font-weight:300;">
             	Step 1 - Download java Jar package from MBlox(<a href="https://customer.mblox.com" target="_blank">https://customer.mblox.com</a>). This pakage is used to consume secure webservice of MBlox.
             </p>   
             <p style="font-weight:300;">
             	Step 2 - Include Root file of java file at ColdFusion administrator.
             </p>
             <img src="#rootUrl#/#publicPath#/help/dev/images/blklstevn.jpg" />
             <p style="font-weight:300;">
             	Step 3 - Restart ColdFusion service.
             </p> 
             <p style="font-weight:300;">
             	Step 4 - Files from Mblox will be in XML format.
             </p>  
             <script src="https://gist.github.com/mb-atondon/53a0e47e1944cf0c34da.js"></script> 
              <p style="font-weight:300;">
             	Step 5 - Files get imported in EBM Database.
             </p>               
        </div>
        
        <div style="padding-left:42px; padding-top:15px; width:700px;">
        	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
            <h3 style="margin-left: 20px;">Coding Information</h3>
        	
            <p>Blacklisting - Table</p> 
            <script src="https://gist.github.com/mb-atondon/708bf5eea51b987fe062.js"></script> 
            
            <p>Get black listing number from MBlox</p> 
            <script src="https://gist.github.com/mb-atondon/d4a2d6d754749d320d81.js"></script>                    
        </div>
        
        <div style="padding-left:42px; padding-top:15px; width:700px;">
        	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
            <h3 style="margin-left: 20px;">Frequently Asked Questions</h3>
        	<p style="font-weight:bold;">
            	Q. How Often this EBM database get update from telecoms?
            </p>
            <p style="font-weight:300;">
            	EBM receive data Everyday.
            </p>
            <p style="font-weight:bold;">
            	Q. How many table do we have in EBM Blacklisting database?
            </p>
            <p style="font-weight:300;">
            	We have one table in it.
            </p>
            <p style="font-weight:bold;">
            	Q. How many Indexes do we have on the table?
            </p>
            <p style="font-weight:300;">
            	We have one index on 'phoneNumber_vch' column.
            </p>
            <p style="font-weight:bold;">
            	Q. Can we distinguish why a number is on this list? There is a difference between turned off and just changing service carriers. If a number is deactivated for fail to pay, does it get turned back on?
            </p>
            <p style="font-weight:300;">
            	'MSISDNEvent_vch' would be the column by which we can check activity of the phone number. Entries in column 'MDN Resume','MDN Deactivation','MDN Transfer','MDN Suspend'.
            </p> 
                                
        </div>
    </div>
</cfoutput>