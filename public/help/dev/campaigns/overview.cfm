

<cfoutput>

EBM offers two frameworks to create and launch campaigns
<p></p>
<ul>
	<li>e-Messaging Service (EMS)</li>
	<li>Enterprise Application Integration (EAI)</li>
</ul>
<p>Even though both the frameworks works off same XMLControlsString, EMS generates basic XML for you in background while in EAI, user designs his/her XMLControlString by using GUI and has more control over XMLControlString</p>
<div class="MajorSection">

    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">e-Messaging Service (EMS)</h3>

     <p>
        EMS is the simplest and quickest way to launch voice, sms or email campaign. To launch an EMS campaign you must
		<ul>
			<li>Have username/password to login to EBM</li>
			<li>Have credits to launch your campaign. You need
				<ul >
					<li>1 credit for each email</li>
					<li>2 credits for each sms</li>
					<li>3 credits for each voice call of upto 1 min in length</li>
				</ul>
			</li>
		</ul>
     </p>


     <h5>Create and Launch EMS</h5>

	<p>Login to EBM system by navigating to http://ebmui.com. Once you are into the system click EMS icon on user's homepage</p>

	<img src="#rootUrl#/#publicPath#/help/dev/images/campaign_homepage.jpg" />

     <p>Once you are on create EMS campaign page its as east as 1...2..3</p>

	<ol>
		<li>Create or select the contact group</li>
		<li>Create or select campaign content</li>
		<li>Click Launch Now button</li>
	</ol>
f
	<img src="#rootUrl#/#publicPath#/help/dev/images/campaign_ems.jpg" />

	Once Launch button is clicked, after goging through validation and rules applications, campaigns are sent to delivery queue. Most of the code dealing with EMS UI and logic is under <strong>/session/ems</strong> folder


</div>

<div class="MajorSection">
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
	<h3 style="margin-left: 20px;">Enterprise Application Integration (EAI)</h3>



</div>

</cfoutput>


<!---
            	<h1 align="left">EBM Campaign Flow</h1>

                <div id="contentm1">

                    <div class="section-title-content clearfix">
                        <header class="section-title-inner-wide">
                             <h2>EAI and EMS Campaign Flow</h2>
                             <p>Deep intergations into new and legacy systems</p>
                        </header>
                    </div>

                   <h3>When designing a messaging application, #BrandShort# helps you know where to put what types of data to share in the message customization / business rules, and likewise where to look for what types of data coming from other applications. </h3>
 --->

