               
                
<h1>&nbsp;</h1>
<p>I'm not perfect so if you see violations of my own standards feel free to fix as you go along, but I do expect minimal violations from all developers.
</p>


				 <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
                    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
                    <h3 style="margin-left: 20px;">Responsive Public Interfaces</h3>
                    
                    <h5>Rule</h5>    
                      
                    <p>
                        Target Mobile Browsers first - iPhone and Android browser on phones and Tablets a must.
                    </p>
                                              
                    <h5>Rule</h5>    
                      
                    <p>
                        I would rather see all public facing interfaces using the same core bootstrap - no multiple custom bootstrap configuration.
                    </p>
                    
                    <h5>Rule</h5>    
                      
                    <p>
                        No more jAlert - Replace with common bootbox.alert javascript library. It is easy to convert. See samples on site.
                    </p>
                  
                    <h5>Rule</h5>    
                      
                    <p>
                       All code will work on Linux or Windows versions of Coldfusion - what case sensitivity. Railo, and Macromedia must be supported.
                    </p>
                    
                    
                    <h5>Rule</h5>    
                      
                    <p>
                        Use Java where faster than CF - things like regular expression searches are a good example.
                    </p>
                    
                    <h5>Rule</h5>    
                      
                    <p>
                        No flash! Currently older script libraries still require it but no new deleopment in flash.
                    </p>
                    
                    <h5>Rule</h5>    
                      
                    <p>
                        Use mod-rewrite to hide underlying tech from the users. URLs shoudld not contain .cfm.                        
                    </p>
                    
                     <h5>Rule</h5>    
                      
                    <p>
                       Do NOT use CFFORM or other overly similarly bloated tags that already have jquery or bootstrap equivalent funtionality. This also helps in cross web server compatibility.
                    </p>
                    
                </div>
                
<p>&nbsp;</p>
<p>All major code sections should be commented so that other developers know what you are thinking - or in the case of maintaining older code what you were thinking in the first place.</p>
<p>I prefer that code is indented to match each section – liberal use of open and close brackets – even on conditional logic with one line of code.
  <br />
  Code brackets should match character positions on a line. Open and close brackets should be on a line by themselves – NOT open bracket on same line as starting command like some people use.<br />
</p>
<p>1 Tab = 4 spaces<br />
    <br />
</p>
<h3><br />
SQL    </h3>
<p>  I like my SQL to be consistant and easy to read – see sample
  All Queries will reference both the DB and the table name in dot(.) notation and not rely on default DB in data source definition.  </p>
<p>All SQL will be indented like sample
  All SQL commands will be in all CAPS
  All user input to DB needs to be wrapped in  
  &lt;cfqueryparams&gt; - no SQL injection holes.
<p>I used to manually replace single quotes with double quotes but now rely on
&lt;cfqueryparams&gt;  mixing old and new code might break XML strings with too many quotes.
<p> All SQL queries should load specific fields - no SELECT * queries please.
<p> Sample:<br />
&lt;cfquery name=&quot;getDistinctBatchIdsDupeProcessingFlag&quot; datasource=&quot;#Session.DBSourceEBM#&quot;&gt; <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SELECT <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AllowDuplicates_ti<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FROM<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;simpleobjects.batch<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WHERE<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BatchId_bi = &lt;CFQUERYPARAM CFSQLTYPE=&quot;CF_SQL_INTEGER&quot; VALUE=&quot;#INPBATCHID#&quot;&gt;<br />
&lt;/cfquery&gt; <br />
</p>


  
  As many queries (All of them) as possible should be in the CFC objects and acceseed through AJAX OR CFINVOKE.
<p> If we need to convert DB's (say go to Microsoft SQL vs mySQL) down the road, all the data calls are centralized.
<p>
<p> Note CF8 vs CF9 – CF9 auto converts all JSON return values to upper case. Sites written in CF8 expecting mix of upper and lower case will break.
<p> All CFC methods will need to add Session UID checks and error out accordingly.
<p> Remove all console.log calls in release versions – good for debugging but still breaks IE.
<p> I used to use .json JQuery calls but now prefer .ajax for better error handling and better clarity of parameters.
  
  All .ajax should use 'POST' option as opposed to 'GET'.
<p> Compress / Compact production JS when complete.
<p> Pages that begin with dsp_ use the site wide display templates as defined in the application.cfc.<br />
<p> Main DBSource is DBSourceEBM default="Bishop"
<p>
<h3>
  
  
  
  
  
  
  
  
  
  Exception Handleing</h3>
<p> All code blocks will be within either local exception handelers or within global exception handelers.
  
  In ColdFusion this is done within the CFTRY and CFCATCH Tags
  
  Liberal chaecking for bad parameters should use CFTHROW to exit gracefully.
  
  The end user should NEVER see generic ColdFusion server errors.
<p>
<p>
<p>When using a JavaScript switch statement on  a variable returned from an ajax call to a cfc - always surround variable with  parseInt()<br />
example: switch(d.DATA.currrxt[0]) becomes  switch(parseInt(d.DATA.currrxt[0]))
<p>
<p>
<h3>
Bloatware  </h3>
<p>Team members should ensure that they do not bring in  any bloat that is not used.
<p>In the interest of cleaning up I removed babblesphere.js – it  had about 15 legacy functions that were not used on new site.<br />
Seta was pushing this back into the project so they could have  access to one function - bb_center_popup
<p>If you run accross a file you suspect is no longer in use - rename it OLD_XXX where XXX is the original file name. If after a 30 days it does not break the site remove from the site.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>System Maintenance Check</h3>
<p>On all systems login pages (or other pages of critical nature such as payment systems), please add the following check to your logic.</p>
<p>If the value of systemup_int is 0 or unavailable then redirect user from login page to page displaying down for maintenance.</p>
<p>SELECT
  <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;systemup_int<br />
  FROM<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;simpleobjects.maintenance<br />
  WHERE<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mid_int = 1</p>
<p></p>
