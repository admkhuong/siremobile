<cfoutput>

 <div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Interval Queue Processing Overview</h3>

	<h5>simplequeue.moinboundqueue</h5>     
                 
    <p>Every inbound message (MO, OTT, Twitter, etc) that successfully comes into the EBM system will get logged here in this table. Events for future processing (EBM Intervals) will also be inserted here. 90 days of activity is stored here and anything older will be archived.
    </p>
    <p>Queries that add/delete/modify this table's data are all contained within the EBM Site...session/cfc/csc/csc.cfc and its asscociated cfincludes. Avoid code where entries are modfied via direct SQL vs using a pre-existing method. As always table names and database names are always lower case and are always case sensitive. Column names can be camel case and are not case sensitive.  
    </p>
    
    <p><script src="https://gist.github.com/jpijeff/1f23ee56643ed91498f9.js"></script></p>
        
    <p>Indexes are carefully monitored - do not just add/modify/delete without going through DBAs and the system architect first.</p>
               
    
    <h5>Column - moInboundQueueId_bi</h5> 
   	<p>Every entry will get a unique Id assigned to it. Note this is a bigint with autoincrement. </p>     

	<h5>Column - CarrierId_vch</h5>
	<p>For aggregators like MBlox and SyBase this is the field that captures where the carrier that the inbound message came from. This helps make it cheaper to route responses in both processing time and price.</p>
	    
    <h5>Column - ContactString_vch</h5>
	<p>This is the device address from which the incoming message came from. For SMS this is the mobile phone number. For Twitter this is the user screen name or user system Id. Sessions are tracked as a unique cobination of fields that include both ShortCode_vch and ContactString_vch</p>
	    
    <h5>Column - ShortCode_vch</h5>
	<p>This is the system address to track where specific message are coming from. For SMS this is the Common Short Code, for OTT and Twitter this might be an EBM system generated identifier. Sessions are tracked as a unique cobination of fields that include both ShortCode_vch and ContactString_vch</p>
	    
    <h5>Column - Keyword_vch</h5>
	<p>This can be one of two items. The text the end user sent, or EBM system generated events that will be identified as starting with "EBM -". INTERVALS, STATEMENTS, API, ETC can generate next steps in processing that are logged as an MO event. </p>
	    
    <h5>Column - TransactionId_vch</h5>
	<p>If the sending system provides a unique Id, this will be captured here. The EBM system will automatically dedupe against this Id. There is also more advanced dedupe logic to be outlined in another section.</p>
    
    <h5>Column - QuestionTimeOutQID_int</h5>
	<p>When an Interval Expires (becomes ready to process), this is the next Control Point (CP)</p>
    
    <h5>Column - Time_dt</h5>
	<p>Timestamp of the message as provided by the remote sender. Not all sources will provide this. Used for debugging and deduplication logic.</p>
    
    <h5>Column - Scheduled_dt</h5> 
    <p>This is when either the MO was processed or when the MO is schedule to be processed in the future (Interval). All times are Pacific. Calculations for a device offset using the EBM Interval GUI tools will automatically adjust for this.</p>   
      
    <h5>Column - Created_dt</h5>
	<p>This is when the entry was created in the DB. All times are Pacific.</p>
       
    <h5>Column - Status_ti</h5>    
    <p>As defined in the EBM Site...session/cfc/csc/constants.cfm page the status can be one of the following. Do not write code that just inserts number but use these constants instead.</p>

	<ul>
		<!--- SMS Queue codes Tiny Int 0-255 --->
        <li>SMSQCODE_READYTOPROCESS = 1</li>
        <li>SMSQCODE_PAUSED = 2</li>
        <li>SMSQCODE_INPROCESS = 3</li>
        <li>SMSQCODE_QA_TOOL_READYTOPROCESS = 4</li>
        <li>SMSQCODE_ERROR = 5</li>
        <li>SMSQCODE_ERROR_POSSIBLE_PROCESSED = 6</li>
        <li>SMSQCODE_STOP = 8</li>
        <li>SMSQCODE_PROCESSED = 10</li>
        <li>SMSQCODE_PROCESSED_ON_MO = 11</li>
        <li>SMSQCODE_PROCESSED_BY_TIMEMACHINE = 12</li>
        <li>SMSQCODE_PROCESSED_BY_RESPONSE = 14</li>
        <li>SMSQCODE_INFORMATIONONLY = 100</li>
        <!--- Furtue think for now--->
        <li>SMSQCODE_SECONDARYQUEUE = 200</li>
	</ul>   
   
    <h5>Column - CustomServiceId1_vch</h5>
	<p>Legacy - no longer used</p>
    
    <h5>Column - CustomServiceId2_vch</h5>
	<p>Legacy - no longer used</p>
    
    <h5>Column - RawData_vch</h5>
	<p>The raw request data. JSON or XML  or other serialized list of data sent with the incoming message.</p>
         
   
   
   
    <h5>The Interval Queue</h5>
    
    <p>Entries into the simplequeue.moinboundqueue table with a status_ti of 1 and a scheduled_dt in the future will continue processing at the next specified control point - QuestionTimeOutQID_int. </p>
    
    
    <h5>act_processsmsqueuelocalcfthread_0</h5>
    
    <p>An scheduled task will run every minute to check for elligible events to process. The process use a parameter (inpMaxThreadCount) specified number of threads to run. This new logic has less DB hits on the main thread and does not rely on Coldfusion limits to limit the number of threads.</p>
        
    <ul>
    	<li>http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=3000&inpMaxThreadCount=50&inpMod=1&DoModValue=0</li>
    	<li>Oldest, as defined by Scheduled_dt, that are ready to process queued events will process first.</li>
        <li>Current set up is MAX 90,000 intervals processed per hour</li>
        <li>The main processing task just loops over the max number of events and sends them to a load balanced farm for processing.</li>
    	<li>Post to http://ebmmo.ebm.internal/ebmresponse/sms/ebmqueue/it - this is on a loadbalanced farm of servers.</li>
        <li>Errors are written to simplequeue.errorlogs. Numeric Error Ids are specified in simplequeue.errorlogs.</li>
        <li>Processing box must have the number of threads configured high enough to support all running processes. Thuis is set in both Coldfusion Admin, Coldfusion Config Files, and web server limits ()IIS, Apache, etc </li>
        <li>Uses simpleobjects.cron_tracking table to limit only one running process per parameters.</li>
    </ul>
    
    <p><script src="https://gist.github.com/jpijeff/8b3ec92b8ce2beeb7462.js"></script></p>
    
    <h5>Single Process sample</h5>
    <p>http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=3000&inpMaxThreadCount=50&inpMod=1&DoModValue=0</p>
	  
    <h5>Two Process sample</h5>
    <p>http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=0</p>
    <p>http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=1</p>
            
    <h5>Config processing server both IIS and CF</h5>
    <p>http://www.cfwhisperer.com/post.cfm/important-threading-information-for-coldfusion-iis-and-legacy-odbc</p>
    <BR> 
    <p>File should be {drive}:\JRun4\lib\wsconfig\1\jrun_iis6_wildcard.ini File name might be slightly different.</p>
    <p>example: C:\ColdFusion9\runtime\lib\wsconfig\1\jrun_iis6_wildcard.ini</p>

    <h5>Still in Development - things to consider and do.</h5>
    
    <p>As alwys carefully understand source code before making any modifications.</p>
    <p>Currently the automated interval processing task is only scheduled to run 6:00 AM to 8:00 PM. It may go 24 hours if we trust it. The problem will be if too many tasks build up</p>
    <p>Add a simple key to the API call to prevent outside entities from abusing it.</p>
    
    <h5>Troubleshooting - What to do if it breaks - </h5>        
	
    <p>Verify the web server where the scheduled task is running. Look at both Coldfusion service and host server (Apache, IIS, etc)</p>
	<p>If the server is rebooted or crashes while running the automated process, go to the simpleobjects.cron_tracking table and clear the IsRunning_ti flag back to 0. Always verify the process is not running first via Coldfusion Monitor tools.</p>
	<p>The process can be split using inpMod=X where X is a value greater than 1 and an equivilent number of processes with same inpMod=X and with DoModValue from 0 to (X-1) </p>
    <p>Are the servers configured to handle the number of threads properly?</p>

</div>      
           
           
           
           
          <!--- 
		  
		  Document Control Points - provide link 
		  Document Dedupe processing - provide link 
		  
		 
		  
		  Alarms
		  
		  What to watch for
		  
		  Errors
		  
		  Restarting
		  
		  Watch for sending too fast errors on SMPP
		  
		  
SELECT
    COUNT(SMPPErrorCode_int), SMPPErrorCode_int
FROM
    simplequeue.smsmtfailurequeue
WHERE
    Created_dt > '2015-04-03 00:00:00'
GROUP BY
SMPPErrorCode_int
ORDER BY
SMPPErrorCode_int


		  
		  
		  Future think
		  
		  More servers?
		  More Processes
		  
		  Archive process will need to take into account interval programs over 90 days old
		  
		  
		  
Two threads sample - each thread is multithreaded
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=0
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=1250&inpMaxThreadCount=20&inpMod=2&DoModValue=1

Single thread sample
http://ebmmanagement.mb:8503/management/processing/act_ProcessSMSQueuelocalcfthread.cfm?VerboseDebug=0&DebugAPI=0&inpAmountToProcess=3000&inpMaxThreadCount=50&inpMod=1&DoModValue=0




		  
		  
		  --->
          
          
           
          
        
        
</cfoutput>
        
    
    