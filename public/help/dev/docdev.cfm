<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Help Center</title>

<cfinclude template="../../paths.cfm" >
<cfinclude template="../../header.cfm">
<cfinclude template="inc_validatesk.cfm" />

<cfoutput>

    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/jquery.tocify.css" />
    <script src="#rootUrl#/#PublicPath#/js/jquery.tocify.min.js"></script>

</cfoutput>


<style>


</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function()
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );

		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );
		});


		if( $img[0].width ){ $img.trigger( 'load' ); }

		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");

		$("#PreLoadIcon").fadeOut();
		$("#PreLoadMask").delay(350).fadeOut("slow");


		var toc = $("#navigationLeftMenu").tocify({ selectors: "h2, h3, h5" }).data("toc-tocify");

		<!--- Ajust auto scroll for TOC tool to account for header - 45 pixels --->
		toc.setOption("scrollTo", "45");
		toc.setOption("theme", "jqueryui");
		toc.setOption("extendPage", false);





  	});


	<!--- Image preloader --->
	$.preloadImages = function()
	{
		for (var i = 0; i < arguments.length; i++)
		{
			$("<img />").attr("src", arguments[i]);
		}
	}

</script>


<!--- help  and doc section style mods --->
<style>


	#LefttMenuFiller
	{
		position:absolute;
		top:0;
		left:0;
		background: #1E5799;
		width: 300px;
		height:100%;
	}

	#navigation #mainnavnav
	{
		background: #1E5799 !important;
	}

	#navigationLeftMenu
	{
		background: #1E5799;
		padding-top:3px;
	}

	#inner-bg-m1
	{
    	float: none;
   	}

	#navigationLeftMenu h2
	{
		line-height:30px;
	}

<!---	#navigationLeftMenu a
	{
		color: #07b4ee;
		text-decoration: none;
	}

	#navigationLeftMenu a:hover
	{
		color: #07b4ee;
		text-decoration: underline;
	}--->



	ul.hcpagelinks
	{
		list-style-image: url('<cfoutput>#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png</cfoutput>');
		padding-left: 16px;
		padding-top:15px;
	}

	ul.hcpagelinks li
	{
		padding-bottom:8px;
	}

	ul.hcpagelinks li a
	{
		color: #07b4ee;
		text-decoration: none;
	}

	ul.hcpagelinks li a:hover
	{
		color: #07b4ee;
		text-decoration: underline;
	}

	.well
	{
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
		margin-bottom: 20px;
		min-height: 20px;
		padding: 19px;
	}

	<!--- Overide EBM Public header stuff specifically for help--->
	#blue-box
	{
		margin-top: 0px;
	}

	.transpacer
	{
   		 clear: none;
   	}


	h3
	{
		color: #07b4ee;
		text-decoration: none;
		font-size: 16px;
	    font-weight: 200;
	}

	h5
	{
		color: #000;
		display: block;
		font-size: 17px;
		font-weight: normal;
		line-height: 17px;
		padding: 10px 0px;
		text-decoration: none;
	}

	p
	{
		font-weight:200;
		margin: 10px 0;
	}


	.transpacerDoc
	{
		clear: none;
	    min-height: 800px;
    	padding: 0 0;
    	position: relative;
    	width: 100%;
	}


	ul li
	{
		font-weight:200;
		margin-left:12px;
	}

	.MajorSection
	{
		padding-left:42px;
		padding-top:15px;
		width:900px;
		text-align:left;
	}

	<!---[data-unique="HelpCenter"] a
	{
		background-color: none !important;
		box-shadow: none !important;

	}--->

</style>

</head>
<cfoutput>
    <body>

    <div id="background_wrap"></div>

    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>


    <div id="HelpCenter" align="center" style="position:relative;">

       <!--- EBM site footer included here --->
       <cfinclude template="../../act_ebmsiteheader.cfm">


   		<div id="LefttMenuFiller" style=""></div>

        <div id="navigationLeftMenu" style=""></div>


        <div id="HelpStage" style="background-color:##f9f9f9; padding-left:325px;">

      		 <div id="inner-bg-m1" style="padding-bottom:25px; min-height:800px; text-align:left;">

       			<!--- cfinclude all of the docs to display here - make sure to follow h2,h3,h4 and <p> standards for format--->
           		<cfinclude template="startdev.cfm" />
                
                <!--- Coding Standards --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;"> Coding Standards</h2>

               		<cfinclude template="codingstandards.cfm">

                 </div>
                
                <cfinclude template="blacklisting/blacklisting.cfm" />

                <cfinclude template="xmlcontrolstring/xmlcontrolstringhome.cfm" />

            	<!--- CRON JOBS --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">Cron Jobs</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">Automated processing Jobs that keep the system running</p>

               		</div>

                  	<cfinclude template="cron/cronoverview.cfm">
                    <cfinclude template="cron/moinboundqueue.cfm">



                 </div>

                <!--- System Errors --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">System Errors</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">Errors will be logged in simplequeue.errorlogs</p>

               		</div>

                  	<cfinclude template="errors/errorsoverview.cfm">



                 </div>


                <!--- PROCCESSING FLOW --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">Processing Flow</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">Automated processing Jobs that keep the system running</p>

               		</div>

                  	<cfinclude template="processingflow/procflow.cfm">

                 </div>


				<!--- Reporting --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">Reporting and Analyitics</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">Reporting and Analytics is the discovery and communication of meaningful patterns in data. Messaging is especially valuable in areas rich with recorded information, analytics relies on the simultaneous application of statistics, computer programming and operations research to quantify performance. Analytics often favors data visualization (Reporting!) to communicate insight.</p>

               		</div>

                  	<cfinclude template="reporting/reportingoverview.cfm">



                 </div>



                <!--- CPP  --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">CPP</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;"></p>

               		</div>

                  	<cfinclude template="cpp/cppoverview.cfm">



                 </div>

                <BR />
                <HR />
                <BR />

                 <!--- PROCCESSING FLOW --->
				<div style="display:inline; text-align:left;" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">IVR Stage</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;">Drag N Drop tool for IVR flow</p>

               		</div>

                  	<cfinclude template="ivrstage/mainivrstage.cfm">

                 </div>


                <BR />
                <HR />
                <BR />

                <!--- SMS  --->
				<div style="display:inline; text-align:left; margin-top:25px; clear:both" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">SMS</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;"></p>

               		</div>

                  	<cfinclude template="sms/deliveryreceipts.cfm">
                    <cfinclude template="sms/admintasks.cfm">
                    <cfinclude template="sms/isopt2x.cfm">
                    <cfinclude template="sms/concatenatedsms.cfm">

                 </div>


				<!--- twitter  --->
				<div style="display:inline; text-align:left; margin-top:25px; clear:both" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">Twitter</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;"></p>

               		</div>

                  	<cfinclude template="twitter/dm.cfm">


                 </div>


				<!--- EBM Campaigns  --->
				<div style="display:inline; text-align:left; margin-top:25px; clear:both" >
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>

                    <h2 style="line-height:32px; color: ##f8853b;">EBM Campaigns</h2>

               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

		            	<p style="font-weight:300;"></p>

               		</div>

                  	<cfinclude template="campaigns/overview.cfm">


                 </div>

         	</div>


        </div>


      	<!--- Leaves a blank area at bottom of page--->
       	<div class="transpacerDoc"></div>

    <!---
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">


        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
	--->

    </div>
    </div>
    </body>
</cfoutput>
</html>











