
<!--- Validate Section Key --->
<cfparam name="sk" default="" />
<cfparam name="lg" default="0" />


<cfif lg GT 0>
	
   <!--- <cftry>--->
        <cfquery name="AddMemberInformation" datasource="#Session.DBSourceEBM#">
           INSERT INTO
            simpleobjects.simplekeys_log	 
            (
                PKId_int,
                SimpleKey_vch,
                Created_dt,
                CGIData_vch
                )
            VALUES
            (
                NULL,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(sk)#">,
                 NOW(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#serializeJSON(CGI)#">                                                       
            )   
            
        </cfquery>    

 <!---   <cfcatch type="any">
    
        <!--- Add collision detection?--->
        
    </cfcatch>
    
    </cftry>--->

</cfif>

<cfif Len(trim(sk)) NEQ 10>
	<!--- Re-direct to index--->
	<cflocation url="index?msg=#URLEncodedFormat('Invalid Key - Please Try Again (code=1)')#" />

<cfelse>
    
    <cfset TestSimpleKeyArr = ArrayNew( 1 ) />
	<cfset TestSimpleKey = "Invalid Key" />

	<!--- Get Mod Match Value --->
	<cfset ModMatch = MID(sk, 4, 1) />
    
    <!--- Convert string to an array---> 
    <cfset TestSimpleKeyArr = ListToArray(sk, "") />

	<!--- remove mod match value at position 4--->
	<cfset ArrayDeleteAt(TestSimpleKeyArr, 4) />
    
	<cfset SumValue = 0 />

	<cfloop
        index="intChar"
        from="1"
        to="9"
        step="1">
            
        <cfset SumValue = SumValue + Val( TestSimpleKeyArr[ intChar ] ) />                                    
    
    </cfloop>
    
   	<!--- Simple security but effective for such a minor task ---> 
    <cfif (SumValue MOD 10) NEQ ModMatch>                           
        <!--- Re-direct to index--->
		<cflocation url="index?msg=#URLEncodedFormat('Invalid Key - Please Try Again (code=2)')#" />          
    </cfif> 

</cfif>