<cfoutput>


<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Overview</h3>
                   
    <h5>IVR Stage Tool Overview</h5> 
   
   	<p>Build XMLControlString for IVR fulfillment</p>     
    <p></p>
    
    
    <h5>jsPlumb</h5> 
   
   	<p>javascript library for drag and drop objects</p>     
           
    
</div>      

        
<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">CCD</h3>
                   
    <h5>Call Control Data Overview</h5> 
   
   	<p>Part of XMLControlString for IVR fulfillment</p>     
    <p></p>
    
    
    <h5>ebm...session/campaign/stage/src/core/WorkSpace.js</h5> 
   
   	<p>How CCD is loaded from database.</p>     
    
    <p>ReadCCD: function(inpObj, _self)</p>
    <p>WriteCCDXML: function()</p>    
    <p>mcidservice.cfc</p>
        
    
</div>      
           
          
        
        
</cfoutput>
        
    
    