<cfoutput>

 <div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Processing Flow Overview</h3>

        
     <p>
        
     </p>
           
                
</div>      
        
<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Request Tracking</h3>
                   
    <h5>UUID</h5> 
   
   	<p>All requests inserted into the simplequeue.contactqueue get a UUID assigned</p>     
    <p>The UUID is 36 characters and is in a 8-4-4-4-12 standard format.</p>
    <p>All successful API requests will return this UUID in the result as REQUESTUUID. </p>
    <p>The UUID is tracked as the same in both simplequeue.contactqueue and simplexresults.contactresults and can be used to match results back to original requests. </p>
    <p>For one off Dynamic request the system uses java generated UUID. For bulk load staic requests the system relies on mySQL to generate each columns UUID.</p>
    <p>Distribution from simplequeue.contactqueue will ALWAYS pass the UUID to the fulfillment device, which will pass the UUID back to simplexresults.contactresults</p>
</div>      
           
          
        
        
</cfoutput>
        
    
    