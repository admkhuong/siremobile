
<cfoutput>


    
    <div class="MajorSection">
                    
        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
        <h3 style="margin-left: 20px;">Overview</h3>
    
         <p>
            Reporting, Analytics and the #BrandShort# Dashboard are used for the discovery and communication of meaningful patterns in data. Especially valuable in messaging which is rich with recorded information. Analytics relies on the simultaneous application of statistics, computer programming and operations research to quantify performance. Analytics often favors data visualization (Reports and Dashboards) to communicate insight.
         </p>
        
        
                    
    
       
        
        <h5>Requirements of a good report</h5>    
        
        <ul>
        
            <li>Easy to understand</li>
            <li>Online Analyitcal Processing (OLAP) is performed against the #Brandshort# slave.</li>
            <li>Folow SQL Coding standards so they are easy to read and debug.</li>
            <li>Indexes - Proper use of indexes. (DO NOT JUST ADD MORE INDEXES!)</li>
            <li>Reporting Library - </li>
            <li>Logical Groupings of Files - Like with Like</li>
            <li>http://www.amcharts.com/</li>
            <li>Returns in under 30 seconds</li>
            <li>100% Accurate</li>           
        </ul>
        
        
        <h5>Online Analyitcal Processing (OLAP)</h5>    
        
        <p>OLAP is performed against the #Brandshort# slave. The primary slave to the main EBM database will house 90 days worth ofrealtime data. The slave is on average 0 seconds behind the master.           
        </p>
        <p>            
            OLAP Slave Location: 10.25.0.201 
        </p>
        
        <p>From mySQL Workbench you can run SHOW SLAVE STATUS to see how up to date the slave is. Seconds_Behind_Master should be 0 if everything is OK.
        </p>
        
        <p>
        	datasource="#DBSourceEBMReportsRealTime#" is defined in the main paths.cfm document and points to the EBMReportsRealTime  datasource on the production web servers.
        </p>
        
        <h5>Sample Stage</5>           
        <p>        
   			 <img src="#rootUrl#/#publicPath#/help/dev/images/samplereport.png" width="auto" height="auto" style="" />     
        </p>
    
    </div>
    
    
   
    
    
    <!---
	
		Types of reports - Chart, Table, Graph, Download csv, pdf, word 
		Libray
		Permissions
		long running queries - kill them
		
		Company accounts and shared resources 
		Filters
		
		
		Future Think Features needed
		
		Templates
		
		Drag and Drop
		
		
		Batch List
		
		
	
	--->
    
    <div class="MajorSection">
                    
        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
        <h3 style="margin-left: 20px;">How to Build a Report</h3>
    
         <p>
           If you follow the structure of the #BrandShort# you will be able to easily integrate new reports within the overall framework. You will not need to mess with the framework. Just build your report and add it to the list. Reports are loaded into the dashboard via jquery drag and drop and AJax.           
         </p>
            
            <ul>
	            <li>FORM, CHART, TABLE report types. We may add more later but these are the basics.</li>
            	<li>Each Batch or Batch list is "remembered" - the last ran dashboard will still be there next time you go back</li>
                <li>Date Range Picker</li>
                <li>Reports the user has permissions to on on the left</li>
                <li>Templates</li>
                <li>Single batch or CSV list of Batch Ids</li>
                <li>Exporting for Table Data</li>
                <li>Resizable report objects</li>
                <li>Relocatable report objects</li>
                <li>Clear all option on stage</li>
                <li>Options for each report and stage are hover over options that dont display until the mouse is hovered over the title bar of the object</li>               
            </ul>    
	            
    
	    <h5>Before you begin...</h5>    
            
        <p>Authenticated Users must be logged in to access reports. Company users have access to reports on shared resources.</p>
        <p>All variables must be declared with var for use in multithreaded cfc</p>
        

        
        
        <!---Basic  Sample --->
        <h5>FORM Sample Report</h5>    
        <p>The Simplest Report</p>
        
        <p>        
   			 <img src="#rootUrl#/#publicPath#/help/dev/images/display_c_smsmt.png" width="auto" height="auto" style="" />     
        </p>
        
        <h5>Step 1 - Create a new method for your report.</h5>
        <p>Individual reports are located at <b>..session/cfc/inlcudes/reports/..logical grouping or stand alone file</b></p>
        <p>All report files are CFIncluded at the top of the main Reporting cfc at <b>..session/cfc/reporting.cfc</b></p>
        <p>Verify queires against actual data - use mySQL EXPLAIN</p>
        
        <p>Display_c_smsmt is a method defined in the SMS group of reports in the file at at <b>..session/cfc/inlcudes/reports/sms.cfm</b><BR />
        All methods must be system unique. The naming scheme is Display for display - type of output _c_ is a counter - smsmt is the object being counted<BR />
        Below is a well commented source code example for Display_c_smsmt</p>
        <script src="https://gist.github.com/jpijeff/0b1cedec83e9bc58780f.js"></script>
        
        
        <h5>Step 2 - Add your report to the Dashboard</h5>
        <p>The main reporting dashboard is located at <b>...session/reporting/reportbatch.cfm</b></p>
        
                 
        <p>Reports are added to the left menu as items in an unordered list.<p>
        <script src="https://gist.github.com/jpijeff/d14994e114ab9a05e82a.js"></script>
                    
        <p>Each item is a span of class="menuItem"</p>
        <p>Attribute rel1 is the name of the report. The reporting.cfc will use this to determine which method to use to generate the report. In this instance we are using rel1="Display_c_smsmt"</p>
        <p>Attributre rel2 is used to determine what type of report we are using. </p>
        <p>Attributre rel3 is used to determine what the inital size of report. There are a few coldfusion variables the predefine several cobinations.</p>
        <script src="https://gist.github.com/jpijeff/9dd83160c7667584b374.js"></script>
        
        
        <BR />
        <p>You can also enclose Reports in optional account level permissions</p>
        <script src="https://gist.github.com/jpijeff/49edd10a882b511feba0.js"></script>
        
        <!---
        
        <li>
        <span class="menuItem" rel1="Display_c_smsmt" rel2="FORM" rel3="#CounterTinyClassString#">
        SMS MT Counter
        </span>
        </li> 
        
        
        javascript is very complex - do not mess with it unless you KNOW what you are doing.
        
        --->
        
        <h5>Step 3 - Testing and QA</h5>
        <p>QA / Test QA / Test QA / Test </p>
        <p>Any customer facing report must be bullet proof.</p>
        
                                     
        <BR />
        
        <h5>CHART Sample</h5>
        <p>A more complex example using bar charts</p>
        
        <p>        
   			 <img src="#rootUrl#/#publicPath#/help/dev/images/mtcountbystate.png" width="auto" height="auto" style="" />     
        </p>
        
        <h5>Step 1 - Create a new method for your report.</h5>
        <p>Individual reports are located at <b>..session/cfc/inlcudes/reports/..logical grouping or stand alone file</b></p>
        <p>All report files are CFIncluded at the top of the main Reporting cfc at <b>..session/cfc/reporting.cfc</b></p>
        <p>Verify queires against actual data - use mySQL EXPLAIN</p>
        <p>Uses AMChart library objects located at ?<b>..session/lib/report/barchart.cfc</b></p>
        <p>MTCountByState is a method defined in the SMS group of reports in the file at at <b>..session/cfc/inlcudes/reports/sms.cfm</b><BR />
        All methods must be system unique.<BR />
        Below is a well commented source code example for MTCountByState</p>
        <script src="https://gist.github.com/jpijeff/a78da4e3a643b8127445.js"></script>
        
        
        <h5>Step 2 - Add your report to the Dashboard</h5>
        <p>The main reporting dashboard is located at <b>...session/reporting/reportbatch.cfm</b></p>
        
                 
        <p>Reports are added to the left menu as items in an unordered list.<p>
        <script src="https://gist.github.com/jpijeff/2ad0780389b8feb987d6.js"></script>
                    
        <p>Each item is a span of class="menuItem"</p>
        <p>Attribute rel1 is the name of the report. The reporting.cfc will use this to determine which method to use to generate the report. In this instance we are using rel1="MTCountByState"</p>
        <p>Attributre rel2 is used to determine what type of report we are using. In this case it is a chart so rel2="CHART" </p>
        <p>Attributre rel3 is used to determine what the inital size of report. There are a few coldfusion variables the predefine several cobinations. if rel3 is not defined then the default size will appear.</p>
        <script src="https://gist.github.com/jpijeff/9dd83160c7667584b374.js"></script>
        
        
        <BR />
        <p>You can also enclose Reports in optional account level permissions</p>
        <script src="https://gist.github.com/jpijeff/49edd10a882b511feba0.js"></script>
        
        
        
        
        
        <h5>Key reportbatch.cfm javascript functions</h5>
        <ul>
        
            <li>AdditionalHideTasks()</li>
            <li>AdditionalShowTasks()</li>
            <li>initDataTable()</li>
            <li>initComponents()</li>
            <li>loadingSingleChartData(id)</li>
            <li>finishedLoadingSingleChartData(id)</li>
            <li>loadingData()</li>
            <li>finishedLoadingData()</li>
            <li>convertDateToTimestamp(date)</li>
            <li>resizeChart()</li>
            <li>ReadDateBoundaries()</li>
            <li>RedrawChartWithNewData(inpDataURP, inpDataGC, inpChartPosition)</li>
            <li>InitDashboardDroppable()</li>
            <li>drawChart()</li>
            <li>RemoveDashboardObject(inpObj)</li>
            <li>SizeDashboardObject(inpObj, inpDir)</li>
            <li>AddSingleDashObj(inpDashObj)</li>
            <li>SaveCurrentAsTemplateDialog()</li>
            <li>LoadTemplate(inpId)</li>
            <li>ClearDashboard()</li>
            <li>LoadDashboardTemplateList()</li>
            <li>RemoveUserTemplate(inpId, inpObj)</li>
            
        </ul>    
         
        
        <h5>Key Reporting.cfc Methods</h5>
        <ul>
        
            <li>reporting.cfc?method= inpReportName   </li>
            <li>reporting.cfc?method=UpdateReportPreference</li>
            <li>reporting.cfc?method=UpdateDashboardObjectPosition</li>
            <li>reporting.cfc?method=GetChartByName</li>
            <li>reporting.cfc?method=GetDashBoardCount</li>
            <li>reporting.cfc?method=GetChartFromPreferences</li>
            <li>reporting.cfc?method=DeleteDashboardObj</li>
            <li>reporting.cfc?method=UpdateDashboardObjectClassInfo</li>
            <li>reporting.cfc?method=ReadTemplate</li>
            <li>reporting.cfc?method=ClearDashboard</li>
            <li>reporting.cfc?method=ReadTemplateList</li>
            <li>reporting.cfc?method=RemoveTemplate</li>
            
        </ul>    
        
       
        <h5>TABLE Sample</h5>
        <p>A more complex example using ... </p>
        
        
        <h5>CHART with FORM Sample</h5>
        <p>A very complex example using user input and Reset Options... </p>
                
        
    </div>
    
                  
                    
          
</cfoutput>