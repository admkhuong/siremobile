
<cfoutput>

<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Administrative Tasks</h3>
   
    <p>
       Backend magic or new features for admin control
    </p>
           
     
    <h5>Provision Shortcode</h5>
     
    <p>Only a super user account can provision new short codes on the EBM system.</p>
     
    <p>TBD: See Jeff to document</p>
     
          
    <p>Log on as Super User - do not store user name or password online anywhere</p>
    <img src="#rootUrl#/#publicPath#/help/images/welcomesu.png" width="120" height="26" style="" />
     
    <p>Open Admin Menu</p>
    <img src="#rootUrl#/#publicPath#/help/images/adminmenu.png" width="315" height="450" style="" />
     
    <p>Open Admin/SMS Menu</p>
    <img src="#rootUrl#/#publicPath#/help/images/adminsmsmenu.png" width="345" height="427" style="" />
          
    <p>Open Admin/SMS/Short Code Manager menu item</p>
    <img src="#rootUrl#/#publicPath#/help/images/scmanager.png" width="385" height="446" style="" />
     
    <p>This is the short code manager screen</p>
    <img src="#rootUrl#/#publicPath#/help/images/scmanagescreen.png" width="1158" height="729" style="" />
     
    <p>From the page you can:	
    <ul>
        <li>Add</li>
        <li>Modify</li>
        <li>Delete</li>
        <li>Set Public Keywords</li>
    </ul>
     
     <p>Modify</p>
     <img src="#rootUrl#/#publicPath#/help/images/scmodify.png" width="601" height="704" style="" />
     
     <p>Shoprt code data is stored in the sms.shortcode table</p>   
     <script src="https://gist.github.com/jpijeff/069664fc4700327fb889.js"></script>
          
     <p>Aggregators Table in the EBM</p>
     <script src="https://gist.github.com/jpijeff/069664fc4700327fb889.js"></script>
     
     <ul>
    	<li>1 - MBLOX - XML via HTTP(s) with all sorts of special service Ids per short code </li>
        <li>2 - AT&T SOAP - SOAP via HTTP(s) </li>
        <li>3 - ALLTEL - SMPP Gateway</li>  
        <li>4 - AT&T SMPP - AT&T SMPP Gateway</li>  
        <li>5 - MBLOX CANADA - XML via HTTP(s) with alternate URL for Canada with all sorts of special service Ids per short code </li>                   
     </ul>
          
    <BR/> 
    <h5>Switch the aggregator</h5>      
     
    <p>Switch the aggregator processing logic for a particular short code.</p>
     
    <p>AT&T wants to switch there aggregator connection point from a SOAP based URL post to a standards based SMPP connection.</p> 
     
    <p>Every short code in the EBM system is programmed to go to a default aggregator. When a new short code is provisioned on the EBM system</p>
         
    <p>Log on as Super User - do not store user name or password online anywhere</p>
    <img src="#rootUrl#/#publicPath#/help/images/welcomesu.png" width="120" height="26" style="" />
     
    <p>Open Admin Menu</p>
    <img src="#rootUrl#/#publicPath#/help/images/adminmenu.png" width="315" height="450" style="" />
     
    <p>Open Admin/SMS Menu</p>
    <img src="#rootUrl#/#publicPath#/help/images/adminsmsmenu.png" width="345" height="427" style="" />
          
    <p>Open Admin/SMS/Short Code Manager menu item</p>
    <img src="#rootUrl#/#publicPath#/help/images/scmanager.png" width="385" height="446" style="" />
     
    <p>This is the short code manager screen</p>
    <img src="#rootUrl#/#publicPath#/help/images/scmanagescreen.png" width="1158" height="729" style="" />
    
    <p>Choose Modify</p>
    <img src="#rootUrl#/#publicPath#/help/images/scmodify.png" width="601" height="704" style="" />
    <p>Save Changes</p> 
    <p><b>Note:</b>MBLox needs Custom Service Ids</p>
    
    <p>Choice is stored in the PreferredAggregator_int field of the sms.shortcode table</p>
    <script src="https://gist.github.com/jpijeff/069664fc4700327fb889.js"></script>
        
	<p>Aggregators Table in the EBM DB - Used to populate drop down list of choices</p>
    <script src="https://gist.github.com/jpijeff/0ca1fad1720d3966fcce.js"></script>
     
    <ul>
    	<li>1 - MBLOX </li>
        <li>2 - AT&T SOAP</li>
        <li>3 - ALLTEL</li>  
        <li>4 - AT&T SMPP</li>  
        <li>5 - MBLOX CANADA</li>                   
    </ul>
        
    <p>*special case - multi-homed short codes. On MO only we know which carrier the keyword came in from. If the shortcode is configured properly, the response will go back out on the aggrregation path the request came in on.</p>
    
    <BR/>     
    <h5>Switch the ShortCode</h5>      
    
    <p>Switch the Short code that a particular Batch/Keyword is going out on.</p>
    
    <p><b>NOTE:</b>All open sessions on old route will become invalid and will no longer work. Any active surveys are done at this point.</p>
    
    <p>Every keyword in the EBM system is programmed to go to a approved short code request.</p>
         
    <p>DB Tables</p> 
    <ul>
    	<li>sms.shortcode - Provisioned Short Code</li>
        <li>sms.shortcodeapproval - A user Account is granted access to the Short Code</li>
        <li>sms.keyword - A new keyword is added to the short code and assigned a Batch Id</li>                            
    </ul>
    
    <p>To change the short code a keyword is running on you will need to:</p>
    
     <ul>
    	<li><b>Step 1 - </b>Find it in the sms.keyword table. You will get a unique KeywordId_int</li>
        <li><b>Step 2 - </b>Verify the target Short code does not already have a keyword that matches the one you are trying to move to it. Active keywords must be unique.</li>
        <li><b>Step 3 - </b>find the ShortCodeRequestId_int for the new Short Code you wish to use for same user account. If one does not exist, it will need to be created. See section on getting a short code assigned to an account.</li>
        <li><b>Step 4 - </b>Using mySQLWorkbench on the Master DB (CAREFUL!) - Update the ShortCodeRequestId_int for the KeywordId_int</li>
        <li><b>Step 5 - </b>Test and verify</li>
    </ul>
     
    <BR> 
    <h5>Custom HELP STOP</h5>
    
    <p>A shared Short Code is one where more than one user account has access to create keywords on it.</p>
    
    <p>All Short Codes by default have a HELP and STOP keyword defined.</p>
    
    <p>By looking at the EBM Account where a particular device address (SMS Mobile Phone Number) last accessed a Short Code, then the EBM can respond with custom HELP STOP or other keyword messaging.</p>
    
    <p>DB Tables</p> 
    <ul>
    	<li>sms.smsshare - List of keywords and Batch Ids with custom messaging.</li>                                   
    </ul>
    
    <p>To add custom keyword responses you will need:</p>
    <ul>
 		<li>Short Code</li>
    	<li>Batch ID - every possible Batch Id will need to be programmed. this feature is not for thousands of Batch Ids</li>
        <li>Keyword - HELP, STOP or other Short Code Level defined responses</li>          
        <li>Content for each Keyword</li>                                 
    </ul>
    
    <p>Using mySQLWorkbench on the Master DB (CAREFUL!) - Add or update content for each Keyword/ShortCode/BatchId combination.</p>
    
    <p>To test you need to send a keyword to the ShortCode for a BatchID that has beenb defined already. Then send HELP/STOP/OR whatever keyword. You should get custom messages back. IT the last BatchId is NULL or one that is not defined then the default HELP/STOP messaging will be returned.</p>
    
    
    
        
            
</div>      

<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">NOC - SMS</h3>
   
    <p>
       Backend magic or new features for admin control
    </p>


	<h5>Monitor SMPP Failures</h5>
    
    
    <p>Count Batch Send OK vs Failures - watch for high percentage?</p>
  
    
    
    <script src="https://gist.github.com/jpijeff/0bfcb45d99a5a979a072.js"></script>
    
    <script src="https://gist.github.com/jpijeff/3bad37b493e065032f29.js"></script>
    
        
    
</div>






</cfoutput>