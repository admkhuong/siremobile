
<cfoutput>

<div class="MajorSection">

    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Concatenated SMS Messages</h3>
    <div>
        <p>Figuring out maximum character counts for standard <a href="http://en.wikipedia.org/wiki/SMS">SMSmessages</a> is really quite simple. However, the maximum character counts for <a href="http://en.wikipedia.org/wiki/Concatenated_SMS">concatenated SMS messages</a> is a bit more complicated. Throw<a href="http://en.wikipedia.org/wiki/Character_encodings" >character encodings</a> into the mix, and everything can become very muddled.</p>
        <p><span id="more-69449"> </span></p>
        <h5>Encodings</h5>
        <p>Languages which use a <a href="http://en.wikipedia.org/wiki/Latin_alphabet" >Latin-based alphabet</a> (such as English, Spanish, French, etc.) usually use phones supporting the <a href="http://en.wikipedia.org/wiki/GSM_03.38" >GSM character encoding</a>. The GSM character encoding uses 7 bits to represent each character (similar to <a href="http://en.wikipedia.org/wiki/ASCII">ASCII</a>). This contrasts with non-Latin-based alphabet languages (such as Chinese, Arabic, Sinhala, Mongolian, etc.) which usually use phones supporting <a href="http://en.wikipedia.org/wiki/Unicode">Unicode</a>. The specific character encoding utilized by these phones is usually <a href="http://en.wikipedia.org/wiki/UTF-16">UTF-16</a> or <a href="http://en.wikipedia.org/wiki/UTF-16">UCS-2</a>. Both UTF-16 andUCS-2 use 16 bits to represent each character. For the sake of simplicity, I will refer to the Latin-based alphabet and non-Latin-based alphabet languages in this post as &ldquo;GSM&rdquo; and &ldquo;Unicode&rdquo; languages respectively.</p>
        <h5>Standard SMS Messages</h5>
        <p>Standard SMS messages have a maximum payload of 140 bytes (1120 bits).</p>
        <p>Since GSM phones use a 7-bit character encoding, this allows a maximum of 160 characters per standard SMS message:</p>
        <pre>1120 bits / (7 bits/character) = 160 characters</pre>
        <p>For Unicode phones, which use a 16-bit character encoding, this allows a maximum of 70 characters per standard SMS message:</p>
        <pre>1120 bits / (16 bits/character) = 70 characters</pre>
        <h5>Concatenated SMS Messages</h5>
        <p>Things get a little bit more complex with concatenated SMS messages. Concatenated SMS messages allow a phone to send messages longer than 160 GSM characters. The sender creates their message as normal, but without the 140 byte limit. Behind the scenes, the phone detects the message length. If the message is less than or equal to 140 bytes, the phone sends a standard SMS message. However, if the message is greater than 140 bytes characters, the phone automatically divides the longer message into multiple, shorter SMS messages which are then transmitted to the recipient separately.</p>
        <p>The recipient&rsquo;s phone takes these multiple, shorter SMS messages and recombines them into the original message which was sent. Because the individual segments of the complete message need to be recombined in this way, this is referred to as &lsquo;concatenated SMS&rsquo;. In order to achieve this seamless delivery, additional information is added to each individual concatenated SMS message. This additional information, referred to as the user data header (UDH), provides identification and ordering information. For example, the UDH could relate the three individual concatenated SMS messages to each other, and indicate the order for recombination.</p>
        <p>The UDH takes up 6 bytes (48 bits) of a normal SMS message payload. This reduces the space for actual message data in concatenated SMSmessages:</p>
        <pre>1120 bits - 48 bits = 1072 bits</pre>
        <p>As a result, each individual concatenated SMS message can only contain 1072 bits of message data. This plays an important role in determining how many individual concatenated SMS messages will be sent based on the actual message data length.</p>
        <p><img src="#rootUrl#/#publicPath#/help/images/sms_2.png" alt="SMS payload diagram"></p>
        <p>Because GSM phones use a 7-bit character encoding, each individual concatenated SMS message can hold 153 characters:</p>
        <pre>1072 bits / (7 bits/character) = 153 characters</pre>
        <p>(<em>Note</em>: 153 characters * 7 bits/character = 1071 bits. However, the extra bit can&rsquo;t be used to represent a full character, so it is added as added as padding so that the actual 7-bit encoding data begins on a septet boundary—the 50th bit.)</p>
        <p>Unicode phones use a 16-bit character encoding, so each individual concatenated SMS message can hold 67 characters:</p>
        <pre>1072 bits / (16 bits/character) = 67 characters</pre>
        <h5>Character Count Thresholds</h5>
        <p>The character limits for individual concatenated SMS messages results in various thresholds for which additional individual concatenated SMSmessages will be required to support sending a larger overall message:</p>
        <p><strong>GSM encoding:</strong></p>
        <ul>
            <li>1 standard SMS message = up to 160 characters</li>
            <li>2 concatenated SMS messages = up to 306 characters</li>
            <li>3 concatenated SMS messages = up to 459 characters</li>
            <li>4 concatenated SMS messages = up to 612 characters</li>
            <li>5 concatenated SMS messages = up to 765 characters</li>
            <li>etc. (153 x number of individual concatenated SMS messages)</li>
        </ul>
        <p><strong>UTF-16 encoding:</strong></p>
        <ul>
            <li>1 standard SMS message = up to 70 characters</li>
            <li>2 concatenated SMS messages = up to 134 characters</li>
            <li>3 concatenated SMS messages = up to 201 characters</li>
            <li>4 concatenated SMS messages = up to 268 characters</li>
            <li>5 concatenated SMS messages = up to 335 characters</li>
            <li>etc. (67 x number of individual concatenated SMS messages)</li>
        </ul>
        <h5>Implications</h5>
        <p>These thresholds are an important consideration for a number of reasons including billing, and the programmatic interfacing with SMS gateways.</p>
        <p>Generally, telephone companies count individual concatenated SMSmessages separately even though they are being recombined at the phone into a single message. This means a GSM encoded message containing 180 characters could potentially invoke a charge for two SMSmessages, even if the sender/recipient only sees a single message.</p>
        <p>When interfacing with a telephone company&rsquo;s SMS gateway programmatically, there may be limits on the number of individual concatenated SMS messages which can sent as part of a single message. For example, Clickatell&rsquo;s documentation states that messages sent through their API should not contain more than 5 concatenated SMSsegments. This may require limiting the number of character input in a web application or service which sends SMS messages via an API in such a manner.</p>
        <p>While it may seem elementary, it is important to point out that SMSmessages are always in one particular encoding; i.e. fully GSM or fullyUTF-16. For example, a period character (&rdquo;.&rdquo;) takes up 7-bits in a GSM SMS message. The same character may exist in a Unicode SMS message, but takes up 16-bits, even it is representing the same character.</p>
    </div>

</div>

</cfoutput>