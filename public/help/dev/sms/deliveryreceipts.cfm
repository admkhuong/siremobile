

<cfoutput>

<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Delivery Recipts</h3>
   
     <p>
        Delivery Receipts can be requested at the API, Shortcode, or Keyword level. Short Code Receipt ON settings will override Keyword settings. API Receipt ON requests will only override Short Code or Keyword level settings if they are both off.
     </p>
           
     
     <h5>API Input</h5>      
     
     <p>Optional input for API calls or  is:<p> 
     <ul>
    	<li>0 - no delivery receipt requested </li>
        <li>1 - return delivery receipt on final state (i.e. delivered, expired, or rejected)</li>
        <li>2 - only return delivery receipt when final state is failed (expired or rejected)</li>                   
    </ul>
        
            
</div>      

</cfoutput>
