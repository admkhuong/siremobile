
<cfoutput>

<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">SMS IRE Is Double Opt In?</h3>
   
     <p>
       Branch on whether a batch has already captured a double opt in confirmation or not.
     </p>
           
     
     <h5>{%ISOPT2X%}</h5>      
     
     <p>Use the special case CDF of {%ISOPT2X%} in a Data Condition Branch. Be sure to take it down the path of an OPTIN question type. If you don't it will never detect as opt in. <p> 
     <ul>
    	<li>0 - not opted in on this batch Id yet </li>
        <li>1 - Opt in on this batch is current</li>
        <li>2 - Opt in is not on Current Batch but IS Opt In on Current Short Code</li>                   
    </ul>
        
            
</div>      

</cfoutput>
