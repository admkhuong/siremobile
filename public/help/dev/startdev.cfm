        <cfoutput>
        	
            
            
             <div id="inner-bg-m1" style="padding-bottom:25px; text-align:left;">
                
                <div style="display:inline; text-align:left;" >                              
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    
                    <h2 style="line-height:32px; color: ##f8853b;">The Help Center</h2>
                    
                </div>
                
                <div style="padding-left:42px; padding-top:15px; width:700px;">
                                       
                	<p style="font-weight:300;">As you are trained you will be expected to develope more documentation and help to improve the #BrandShort# public and developer help systems.</p>
               	                        
                </div>
                
            </div>
            
            
            
            <div style="display:inline; text-align:left;" >                              
                <img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                
                <h2 style="line-height:32px; color: ##f8853b;">System Rules</h2>
           
                <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
           
                    <p style="font-weight:300;">General system rules and standards for developers.
                    </p>	
                </div>
                
                <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
                    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
                    <h3 style="margin-left: 20px;">Primary Rules</h3>
                                              
                    <h5>Primary Rules ## 1</h5>    
                      
                    <p>
                        I would rather not see a message sent to a customer than they same customer getting either a wrong message or duplicate messages.
                    </p>
                    
                    
                    <h5>Primary Rules ## 2</h5>    
                      
                    <p>
                        I would rather not see a message sent to a customer than they same customer getting either a wrong message or duplicate messages.
                    </p>
                    
                    <h5>Primary Rules ## 3</h5>    
                      
                    <p>
                        I would rather not see a message sent to a customer than they same customer getting either a wrong message or duplicate messages.
                    </p>
                    
                </div>
                
            </div>
            
            
            
            <div style="display:inline; text-align:left;" >                              
                <img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                
                <h2 style="line-height:32px; color: ##f8853b;">Introduction to the help system</h2>
           
                <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
           
                    <p style="font-weight:300;">INTRO TEXT HERE ....
                    </p>	
                </div>
                
                <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
                    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
                    <h3 style="margin-left: 20px;">Help system overview</h3>
                                              
                    <h5>Introduction to the help system</h5>    
                              
                    <p>
                    There are two different types of help systems. One for the public and one for internal use only.
                    
                    <BR>
                        The public one is located at ...ebmsite/public/doc
                        
                    <BR>
                        The private one is located at ...ebmsite/public/help/dev/docdev and will require a dev key to use.
                    
                    </p>
                    
                    <p>
                    
                    </p>
                
                    <h5>How do I code a help page?</h5>
                    
                    <p>
                        Each logical section / topic is created in its own .cfm page. The page is then cfincluded in the main target document. With the use of standardized classes and structure, the pages will be auto included in the table of contents. This is accomplished with a jquery library called tocify see <a href="http://gregfranko.com/jquery.tocify.js">http://gregfranko.com/jquery.tocify.js</a>
                    
                    </p>
                    
                </div>
                
            </div>
            
            
            
            
          <!---   <div style="display:inline; text-align:left;" >                              
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    
                    <h2 style="line-height:32px; color: ##f8853b;">The Help Center</h2>
               
               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
               
		            	<p style="font-weight:300;">As you are trained you will be expected to develope more documentation and help to improve the #BrandShort# public and devloper help systems.</p>
               
               		</div>
                    
                    
                   
                                         
              </div>
              
              
                <div style="display:inline; text-align:left;" > 
                    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    <h2 style="line-height:32px; color: ##f8853b;">Primary Rules</h2>
                
                    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
                        <p style="font-weight:300;">System Rules</p>
                
                    </div>
                    
                    
                    <h3 style="margin-left: 20px;">Primary Rules</h3>
                                
                    <h5>Primary Rules ## 1</h5>    
                      
                    <p>
                        I would rather not see a message sent to a customer than they same customer getting either a wrong message or duplicate messages.
                    </p>
                 </div>   
                    
                    
                	
                    
                	<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">The Help System</h3>
                  
                  		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">

                            <h5>Introduction to the help system</h5>    
                              
                            <p>
                            There are two different types of help systems. One for the public and one for internal use only.
                            
                            <BR>
                                The public one is located at ...ebmsite/public/doc
                                
                            <BR>
                                The private one is located at ...ebmsite/public/help/dev/docdev and will require a dev key to use.
                            
                            </p>
                            
                            <p>
                            
                            </p>
                        
                            <h5>How do I code a help page?</h5>
                            
                            <p>
                                Each logical section / topic is created in its own .cfm page. The page is then cfincluded in the main target document. With the use of standardized classes and structure, the pages will be auto included in the table of contents. This is accomplished with a jquery library called tocify see <a href="http://gregfranko.com/jquery.tocify.js">http://gregfranko.com/jquery.tocify.js</a>
                            
                            </p>
                            
                        </div>
                            
                  
                	</div>
                     --->
              
                
                
                
                
                
                    
        
        
        </cfoutput>