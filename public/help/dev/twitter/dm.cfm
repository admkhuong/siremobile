

<cfoutput>

<div class="MajorSection">
                    
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">Direct Message</h3>
   
     <p>
        Aggregation for Twitter Direct Messages
     </p>
           
     
     <h5>Short Code</h5>      
     
     <p>The Short Code is the DisplayName for the twitter account that responds to Direct Messages</p> 
        
     <p>Service Ids are the keys that the app needs to communicate with the account via OAuth</p>       
     
     <ul>
    	
        <li>1 - Consumer Key (API Key)</li>
        <li>2 - Consumer Secret (API Secret)</li>   
        <li>3 - Access Token </li>                
        <li>4 - Access Token Secret </li>
    </ul>
        
</div>
    
<div class="MajorSection">        
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
	<h3 style="margin-left: 20px;">Notes:</h3>
   
    <p>
    	Aggregation for Twitter Direct Messages Notes
    </p>
           
    <ul>
        <li>Messages MUST be 140 characters or less</li>   
        
        <li>Links get replaced with bitly but if in concatenated message can break character length. Test your messages to be sure length is under 140</li> 
        
        <li>Twitter will shut you down as SPAM if you blast too fast or exceeed account limits</li>  
    </ul>
          
</div>      

</cfoutput>
