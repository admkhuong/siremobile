

<cfoutput>


    
    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                    
        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
        <h3 style="margin-left: 20px;">History</h3>
    
         <p>
            When changes are made to an XMLControlString via UI, each time the XMLControlstring is saved a copy of the latest entry is stored in the database. The table this is stored in is simpleobjects.history.
         </p>
            
    
        <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
        
            <h5>Table Structure</h5>    
            
            <p><script src="https://gist.github.com/jpijeff/218e58603b90057efa76.js"></script></p>
            
            
		    <h5>How do I use it?</h5>    
            
            <p>Entries to the history table are made through calls to the history.cfc object via the AddHistory method. You need to pass the new XMLControlString, the BatchId, and adescription of the event that caused the update. The user Id is automatically stroed from the currently logged in user. Good for tracking which user modified a shared resource. Not so good if everyone is logging in oth the same user account.           
            
            
            </p>
            Sample:
           
            <p>
            
            	<script src="https://gist.github.com/jpijeff/b1f4ace784cb6b975815.js"></script>
            </p>
            
        </div>
        
        
        
        
        
        <p>
        
        
        </p>
    
    </div>
                    
                    
          
</cfoutput>