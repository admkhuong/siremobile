
<cfoutput>

<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
        <h3 style="margin-left: 20px;">Email Bounce Management</h3>
       
<p><br>
    We're taking it back to the basics to define a term that is super important to email deliverability, but still seems to cause some mailers a bit of confusion: bounces. What's the difference between a hard bounce and a soft bounce?</p>
<p>On the EBM system, we probably position soft bounces a little differently. We look at soft bounces more as blocks-or short term issues. These blocks shouldn't be added to a suppression list. But, hard bounces (invalid email addresses and nondeliverables) should be. Read on for why.</p>

<h5>The Definition of an Email Bounce</h5>

<p>An email bounce signifies non-delivery of your email message. When this happens, the mailer will receive an automatic notification of the delivery failure. This failure originates from the recipient's mail server for a number of reasons (explained below).</p>

<h5>Contents of a Bounce Message</h5>

<p>Usually the bounce message will give you important information to help you identify the reason for the email delivery failure. This includes the following:</p>
<p>The time and date the message bounced<br>
    The mail server that bounced it<br>
    The RFC code and reason for the bounce<br>
    According to the RFC, hard bounces are depicted by a 5XX code and soft bounces by a 4XX code. However, not all ISPs adhere to that code consistently, so there could be exceptions to this rule.<br></p>

<h5>Soft Bounce (Block)  vs. Hard Bounce</h5>

<p>On the EBM system, there are two types of bounces that you can receive - a soft bounce/block or a hard bounce.</p>
<p>A soft bounce means that the email address was valid and the email message reached the recipient's mail server. However, it bounced back because:</p>
<p>The mailbox was full (the user is over their quota)<br>
    The server was down<br>
    The message was too large for the recipient's inbox<br>
    On the EBM system, we continue to attempt to send these messages for up to 72 hours until the message is delivered. If a message is continuously deferred for 72 hours, we convert these addresses to a block/deferral list.  (A deferral list a not a suppression list.)</p>
<p>A hard bounce occurs when the message has been permanently rejected either because:</p>
<p>The email address is  invalid<br>
    The email addresses doesn't exist<br>
    On the EBM system, we add these hard bounced addresses to a suppression list. What this means is that even if you send a message through us for that user, we will not even try to deliver to that address, because we know it's no longer good. Continuing to try to send to a known bad address will harm your reputation with the receiver, so we prevent that.</p>
<p>Bottom Line: Think of soft bounces as blocks that are a short term issue-you don't need to permanently take these addresses off of your list. However, hard bounces are either invalid or non-existent addresses that should be removed immediately.</p>

<h5>Reducing Your Bounces</h5>

<p>The best way to reduce the number of bounces is by following some key email deliverability best practices. This includes the following:</p>
<p>Maintain good list hygiene: Purge your list regularly of invalid emails and non-responders. High bounce rates can affect your sender reputation so keeping your lists clean will go a long way to achieving higher email delivery rates. On the EBM system, you can review and manage your bounce lists right from your dashboard. Here, you can purge blocks or hard bounces after a certain number of days and specify whether or not you want these bounce messages forwarded via email.<br>
    Use double opt-in: Send a confirmation email when users subscribe to your list. This way you can ensure that the user's email is not only valid, but that they in fact want to receive your email messages.<br>
    Monitor your email delivery: Track your email delivery rates by paying close attention to your bounce rates as well as your response rates. With regular monitoring, you can catch potential failures before they happen or before they do too much damage.<br>
    Achieving high email delivery rates can be a challenge, but not if you know the ropes.</p>

</div>


</cfoutput>




