<cfhtmlhead text = "<title>Help - Quick Look</title>" />

        <cfoutput>
        	
            
            <div id="inner-bg-m1" style="padding-bottom:25px;">
            
            	<h1 align="left">EBM Campaign Flow</h1>
                    
                <div id="contentm1">
                                            
                    <div class="section-title-content clearfix">
                        <header class="section-title-inner-wide">
                             <h2>EAI and EMS Campaign Flow</h2>
                             <p>Deep intergations into new and legacy systems</p>
                        </header>
                    </div>    
                    
                   <h3>When designing a messaging application, #BrandShort# helps you know where to put what types of data to share in the message customization / business rules, and likewise where to look for what types of data coming from other applications. </h3>
                                            
                   <div id="inner-right-img" style="margin:0px;"> <img src="#rootUrl#/#publicPath#/images/m1/genericcampaignflow.png" width="960" height="720"/></div>
                                 
                </div>
            </div>
        
        
        </cfoutput>