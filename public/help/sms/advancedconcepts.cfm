
<cfoutput>

<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">How do I include a previous answer...?</h3>
     
     <p>How do I include a previous answer in my next question/response?</p>
     
    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
    
        <p>    	    
           Use the special code {%INPPA=X%} where X is the physical Id (PID) of the previously asked question. If no answer is found, "BLANK" in all caps will be inserted in its place.
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/previousanserample1.png" width="599" height="404" style="" />
    
    </div>
   
</div>

   
</cfoutput>

    