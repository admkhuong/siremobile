
<cfoutput>

<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
    <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    <h3 style="margin-left: 20px;">How do I get a short code?</h3>
                  
                	
                    
     
    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
    
              
        <p>    	    
            Go to the left slide out menu. Select Campaings first.
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/mainmenucampaignlink.png" width="300" height="460" style="" />
    
    
    	<p>    	    
            Next select the SMS Campaign Management menu item. 
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/campaignssubmenusmscampaignmanagementlink.png" width="339" height="460" style="" />
    
    
    	<p>    	    
            There will be a box at the top of the page called Requests. Select the Pending Requests tab. Click on the Add button in the top left corner of the Pending Requests tab.  All requests must be approved and provisioned by a #BrandShort# system administrator and may take up to 24-48 hours.
 
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/newpendingrequest.png" width="717" height="343" style="" />
    
    
    	<p>    	    
            You can start a request to configure a short code you already own for use on the #BrandShort# system ...
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/addexistingshortcode.png" width="652" height="367" style="" />
    
    
    
   		 <p>    	    
            ... Or Reuest to use a shared short code on the #BrandShort# system.
        </p>
        
        <img src="#rootUrl#/#publicPath#/help/images/addsharedshortcode.png" width="649" height="509" style="" />
    
    
    
    </div>
   
</div>

   
</cfoutput>

    