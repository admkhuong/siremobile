		
         <cfoutput>
         
        
         	
                            
                <div style="display:inline; text-align:left;" >                              
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    
                    <h2 style="line-height:32px; color: ##f8853b;">SMS</h2>
               
               		<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
               
		            	<p style="font-weight:300;">Short Message Service, better known as SMS or text messaging, is a telecommunications service that allows the sending of short (160 characters or less) text-based messages between mobile phones. SMS is generally available on all wireless networks globally and can reach virtually all of the 4 billion mobile phones in use today, making it the world's most pervasive data communications technology.</p>
               
               		</div>
               
                	<div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">General SMS Information</h3>
                  
                  		<cfinclude template="generalsms.cfm">
                    
                	</div>
                    
                    
                    <cfinclude template="requestshortcode.cfm">
                    
                    <cfinclude template="advancedconcepts.cfm">
                                
                	                    
                    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">How do I create a keyword?</h3>
                  
                	</div>
                    
                    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">How do I create an interactive keyword?</h3>
                  
                	</div>
                    
                    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">How do I create a drip marketing campaign?</h3>
                  
                	</div>
                    
                    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">How do I personalize a SMS message?</h3>
                  
                	</div>
                    
                     <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                
	        	        <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
    		            <h3 style="margin-left: 20px;">How do I store a CDF?</h3>
                  
                	</div>
                		
                     
                </div>
                
                
               


                 <!---  <ul class="hcpagelinks" style="">
                    	<li><a href="doc?hf=quicklook" ><h3>General Information</a></h3></li>
                        <li><a href="doc?hf=quicklook" ><h3>How do I get a short code?</a></h3></li>
                        <li><a href="doc?hf=flow" ><h3>How do I create a keyword?</a></h3></li>
                        <li><a href="doc?hf=flow" ><h3>How do I create an interactive keyword?</a></h3></li>
                        <li><a href="doc?hf=flow" ><h3>How do I create a drip marketing campaign?</a></h3></li>
                        <li><a href="doc?hf=flow" ><h3>How do I personalize a SMS message?</a></h3></li>
                   </ul>--->
                  
                
                       
                
                
            <!---
			
			 <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
                  
                  
                  <h3 class="hcpagelinks">General Information</h3>
                    <p></p>
                    
                    <h3 class="hcpagelinks">How do I get a short code?</h3>
                    <p></p>
                    
                    <h3>How do I create a keyword?</h3>
                    <p></p>
                    
                    <h3>How do I create an interactive keyword?</h3>
                    <p></p>
                    
                    <h3>How do I create a drip marketing campaign?</h3>
                    <p></p>
                    
                    <h3><How do I personalize a SMS message?</h3>
                    <p></p>
                    
                  
                  
                  <img src="#rootUrl#/#publicPath#/help/images/icons/doc-blank-icon16x16.png" width="16" height="16" style="float:left;" />
                  <h3 style="margin-left: 20px;">General SMS Information</h3>
				  


			
			    <div style="padding-left:42px; padding-top:15px; width:700px;">
                                       
                	<p style="font-weight:300;">See the following examples for an overview of the #BrandShort# Core functions and features; See the foundations page for in-depth description of core concepts of the #BrandShort# hardware and software; the API page for information on extending and modifying the #BrandShort# software; and the links page for other documentation. Simple programs that demonstrate basic #BrandShort# tasks. These are for use in the #BrandShort# environment. To open them, click a link in the list below 
                    </p>	
               
			     <p>Talk about SMS</p>
                    
                    <h3 class="hcpagelinks">General Information</h3>
                    <p></p>
                    
                    <h3 class="hcpagelinks">How do I get a short code?</h3>
                    <p></p>
                    
                    <h3>How do I create a keyword?</h3>
                    <p></p>
                    
                    <h3>How do I create an interactive keyword?</h3>
                    <p></p>
                    
                    <h3>How do I create a drip marketing campaign?</h3>
                    <p></p>
                    
                    <h3><How do I personalize a SMS message?</h3>
                    <p></p>
					
					     
                    <ul class="hcpagelinks" style="">
                    	<li><a href="doc?hf=quicklook" >General Information</a></li>
                        <li><a href="doc?hf=quicklook" >How do I get a short code?</a></li>
                        <li><a href="doc?hf=flow" >How do I create a keyword?</a></li>
                        <li><a href="doc?hf=flow" >How do I create an interactive keyword?</a></li>
                        <li><a href="doc?hf=flow" >How do I create a drip marketing campaign?</a></li>
                        <li><a href="doc?hf=flow" >How do I personalize a SMS message?</a></li>
 	                  </ul>
                    
                </div>
                --->
            


 		</cfoutput>
 