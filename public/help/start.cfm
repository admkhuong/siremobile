        <cfoutput>
        	
            
            <div id="inner-bg-m1" style="padding-bottom:25px; text-align:left;">
                
                <div style="display:inline; text-align:left;" >                              
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    
                    <h2 style="line-height:32px; color: ##f8853b;">Common Answers</h2>
                    
                </div>
                
                <div style="padding-left:42px; padding-top:15px; width:700px;">
                                       
                	<p style="font-weight:300;">These answers to commonly asked questions are designed to provide a better understanding of #BrandShort#, our features, and our other technical tools.
                    </p>	
                    
                    <ul class="hcpagelinks" style="">
                    	<li><a href="doc?hf=quicklook" >How do I create an Account?</a></li>
                        <li><a href="doc?hf=flow" >How do I use #BrandShort# - Logical Flow</a></li>
                    </ul>
                    
                </div>
                
            </div>
            
            
             <div id="inner-bg-m1" style="padding-bottom:25px; text-align:left;">
                            
                <div style="display:inline; text-align:left;" >                              
                	<img src="#rootUrl#/#publicPath#/help/images/icons/doc-help-icon32x32.png" width="32" height="32" style="float:left; padding-right: 10px;"/>
                    
                    <h2 style="line-height:32px; color: ##f8853b;">Tutorials</h2>
                    
                </div>
                
                <div style="padding-left:42px; padding-top:15px; width:700px;">
                                       
                	<p style="font-weight:300;">See the following examples for an overview of the #BrandShort# Core functions and features; See the foundations page for in-depth description of core concepts of the #BrandShort# hardware and software; the API page for information on extending and modifying the #BrandShort# software; and the links page for other documentation. Simple programs that demonstrate basic #BrandShort# tasks. These are for use in the #BrandShort# environment. To open them, click a link in the list below 
                    </p>	
                    
                    <ul class="hcpagelinks" style="">
                    	<li><a href="doc?hf=quicklook" >How do I create an Account?</a></li>
                        <li><a href="doc?hf=flow" >How do I use #BrandShort# - Logical Flow</a></li>
                    </ul>
                    
                </div>
                
            </div>
            
        
        
        </cfoutput>