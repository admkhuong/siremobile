
<cfoutput>
    
    <div style="padding-left:42px; padding-top:15px; width:700px; text-align:left;">
    
        <h5>What is detection logic?</h5>  
        
             <p>When a call is automatically placed by our dialing platform, our system by default tries to determine if the call is being answered by an individual or an answering machine. The system uses multiple combinations of signal noise and timing. The final detected result is used by our telecomm system software and the message is delivered according to previously user defined parameters. Alternate message, LAT, etc.
            </p>
         
        
        <h5>What is your detection logic's specific values?</h5>
        
            <p>Our complete detection logic is proprietary.  It is based on multiple combinations of signal noise and timing. In general we have multiple stages of processing for multiple types of response. First level detection determines if a connection has been successfully made. First level detection can take as long as the system is setup to wait for a no answer condition (default is 50 seconds). Second level detection logic tries to determine if the call has been connected to a live person or a machine. Second level detection logic makes this determination in under 2 seconds. Third level detection logic will kick in based on second level logic detecting a machine. The third level logic will start playing the machine specific message in less than 2 seconds after the machine has begun recording. If a single frequency tone is detected of specific range and length we will make the determination to start playing the message sooner. For specifc scenarios (cell phones at rock concerts, call forwarding, etc)second and third stage call detection logic can be turned off completely.</p>
        
         
        
        <h5>They've got a complaint...</h5>
        
            <p>Whenever there is a problem it is best to get the phone number affected so IT can research the call detail logs. It is best to understand what is going on before trying to explain it to the customer. If you don't find your answer below, we can revisit the problem and add it to this sheet.
            </p>
        
        <h5>Why did I get mistakenly detected?</h5>
        
            <p>Under ideal conditions, our system is 98% accurate. What that means is for every 100 calls, 2 will be detected improperly and may or may not get a complete message. Less than ideal conditions can degrade this percentage to as low as 85%. This means for every 100 calls 15 will be detected improperly and may or may not get a complete message. Under high volume the number of errors increases. At 98% (again only in a ideal world) 1,000,000 calls will result in 20,000 people will be detected improperly and may or may not get a complete message.
            </p>
            
            <p>Ideal Conditions - Calls are made primarily to home phone numbers to live people or home answering machines.
            </p>
            
            <p>Less than Ideal - Calls are made primarily to business PBX's, cell phones, or other automated information systems.
            </p>
            
            <p>Worst Case - Calls are made primarily to cell phones at rock concerts. Bluetooth driving on the freeway. Speaker phones in noisy office environments.
            </p>
            
            <p> Custom Ringback tones - ISDN traces show normal connection regardless of ring back tones. 
            </p>
            
       <h5>Why did I get dead air?</h5>
            
            <p>Check for blank message? Wrong Script Box? - It has happened before.</p>
            
            <p>If a call result is detected as an answering machine AND the answer time was long then it is most likely due to excessive noise. The end user usually hangs up before the message starts actually to play.
            </p>
            
            <p>Mis-detected as other than live or machine? - The system is setup to only deliver messages to people or recording machines and not to Faxes or CO-Intercepts.
            </p>
            
            <p>Transient telecomm issues - remote signal noise, early disconnects, bad transmit channel etc.
            </p>
        
        <h5>Why did I only get the last part of the message?</h5>
        
             <p>This is called a "partial" condition. Partials can be caused by several factors. Common ones are voicemail systems with multiple IVR options before a message is recorded. If a call is detected as a Machine, the system tries to predict the best place to start playing the message. If the system is faked out, the message begins playing before the actual machine starts recording it. When the recording starts it is already partway into the message - hence partial.
            </p>
        
        <h5>Can you adjust my detection logic?</h5>
        
            <p>Although there are tunable parameters to the detection logic, switching in one direction tends to have a negative impact in the other. Leaning towards more voicemail accuracy instead of live accuracy results in more hang-ups and no message delivered. Leaning towards more live accuracy over voicemail results in more partial messages on machines. Right now, our current tuning is based on many millions of dials with feedback from many different call centers and program recipients with a "best practices" spread on the accuracy with the most messages delivered.
            </p>
            
            <p>We are tuned to best possible reults based on empirical data - we have dialed and mesaured results for many large campaigns.
            </p>
         
        
        <h5>What about numbers with extensions?</h5>  
        
            <p>IVR Navigation - Call detection logic is degraded over regular calls as there is not a "standard" extension navigation protocol. There is a slightly larger delay in processing after final answer before message is played (a few more milliseconds). Noisy environments may be mistaken for additional IVR processing
            </p>
         
        
        <h5>Technical Terms</h5>
        
        
            <p>Pre-Connect Analysis - First level detection</p>
            
            <p>Post Connect Analysis - Second and Third Level detection logic</p>
            
            <p>What is a Central Office (CO) intercept?</p>
            
            <p>When the remote called party is unable to be reached an  SIT tone and a reason can be given - all circuits are busy, number disconnected.</p>
            
            <p>SIT - Special Information Tones</p>
    
    
    </div>
    
</cfoutput>
