<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Appointment Remiders - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
	body
	{
		padding:0;
		border:none;
		height:100%;
		width:100%;
		min-width:1200px;	
		background: rgb(51,182,234); 
		background-size: 100% 100%;
		background-attachment: fixed; 	
		background-repeat:no-repeat;
	}
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
									 
		$(window).scroll(function(){
			$('*[class^="prlx"]').each(function(r){
				var pos = $(this).offset().top;
				var scrolled = $(window).scrollTop();
				<!---$('*[class^="prlx"]').css('top', -(scrolled * 0.6) + 'px');		--->
				$('.prlx-1').css('top', -(scrolled * 0.6) + 'px');		
					
			});
		});
			
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Appointment Reminders</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	....          	                
	            </div>
                
                <img style="position:absolute; top:0px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/callcenterheader.png" height="300" width="480" /> 
              
            </div>
        </div>
        
   		<div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>...</h2>
                         <p>...</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>
                        <h3>.</h3>
                        <BR />
                        <ul>
                        	<li>...</li>
                            <li>...</li>
                            <li>...</li>
                        </ul>
                    </h3>
                </div>     
                         
            </div>
        </div>
        
         <div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>...</h2>
                         <p>...</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>
                        <h3>.</h3>
                        <BR />
                        <ul>
                        	<li>...</li>
                            <li>...</li>
                            <li>...</li>
                        </ul>
                    </h3>
                </div>     
                         
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omnichannel engagement
                
            </div>
        </div>            
               
        <div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>...</h2>
                         <p>...</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>
                        <h3>.</h3>
                        <BR />
                        <ul>
                        	<li>...</li>
                            <li>...</li>
                            <li>...</li>
                        </ul>
                    </h3>
                </div>     
                         
            </div>
        </div>
                
        <div id="inner-bg-m1" style="padding-bottom:25px;padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>...</h2>
                         <p></p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
               
                    <ul>
                       
                       
                        <li></li>
                        <li></li>
                     
                       
                    </ul>
               
                  </div>    
                                        
             
                             
            </div>
        </div>
                        
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

