<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - API's</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
#ebmimage1 {
	width:970px;
	height:323px;
	padding:0;
	border:0px solid;
	text-align:center;
	background-image:url(../images/ebm_i_main/FPO-Header.png);
}
</style>
<script type="text/javascript" language="javascript">




	$(document).ready(function() {

		$('#PDCSurveyImg img').addpowerzoom({
		  defaultpower:3,
		  magnifiersize: [450, 250],
		  powerrange: [3, 3] <!---//Possible range: 2x to 10x magnification--->
		  });  
  	});


$(document).ready(function() {

$('.hover-create').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/Create-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/create.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
}
);



$(".hover-create").click(function(){
$(this).hide();
});
$(".hover-create").click(function(){
$("#createclick").show();
$("#createclick1").hide();
$(".hover-create1").show();
$("#createclick2").hide();
$(".hover-create2").show();
$("p").show();
});
$("#createclick").click(function(){
$(".hover-create").show();
$("#createclick").hide();
$("p").hide(); 

});

});
</script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {

$('.hover-create2').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/Analyze-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/analyze.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}
);




$(".hover-create2").click(function(){
$(this).hide();

$("#createclick2").show();
$("#createclick1").hide();
$(".hover-create1").show();
$("#createclick").hide();
$(".hover-create").show();
$("p").show();
});


$("#createclick2").click(function(){
$(".hover-create2").show();

$("#createclick2").hide();
$("p").hide();
});
});
</script>
<script type="text/javascript" language="javascript">

$(document).ready(function() {

$('.hover-create1').hover(
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/Collect-hover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}, 
function () {
$(this).css({"background-image":"<cfoutput>url(#rootUrl#/#publicPath#</cfoutput>/images/ebm_i_main/collect.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
}
);




$(".hover-create1").click(function(){
$(this).hide();
$("#createclick1").show();

$("#createclick").hide();
$(".hover-create").show();
$("#createclick2").hide();
$(".hover-create2").show();
$("p").show();
});


$("#createclick1").click(function(){
$(".hover-create1").show();
$("#createclick1").hide();
$("p").hide();
});
});
</script>
</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
        	<!---     
		    
				Marketing
				
					Automatically merge/purge against a single contact list for AB testing.
					Scientifcally determine the best message
					
				Screen Shot Create
				
				
				Screen shot manage	
					
				Verbiage on AB stuff	
				
	   		--->
                
                            <!--- Last piece of the puzzle --->
        <div id="inner-bg">
            <div id="content">
               
               <div id="inner-right-img"> <img src="#rootUrl#/#publicPath#/images/api/apppuzzle.jpg" /></div>
               
               <div id="inner-box-left" align="center">
          
                    <div class="inner-main-hd">Simplify</div>
                    <div class="inner-txt">Build and manage all of the messaging complexity using our simple web based tools. Deliver the messaging within your application with one simple API call. Need to change your messaging? Don't re-compile your app, just change your campaign. </div>
                    <div class="phonemain">
                        
                         <div class="inner-row">
                                            
                            
                            <div class="inner-span">
                                <div class="inner-headline">
                                    <h2>Simplified integration within all your applications.</h2>
                                    <p>Web, Mobile, Desktop, Embeded devices, or even your refirgerator. Deliver secure fast reliable messaging from any of your web connected applications.</p>
                                </div>
                            </div>
                
			                <div class="inner-span inner-offset1">
                                <div class="inner-headline">
                                <h2>View customer interactions through reporting and monitoring.</h2>
                                <p>Configurable reports based on messaging interactions. View messaging activity, or check on campaign success rates.</p>
                                </div>
                            </div>
                                    
                         </div>
                
                
                         <div class="inner-row">
                                            
                            
                            <div class="inner-span">
                                <div class="inner-headline">
                                    <h2>Engage using rich subscriber centric personalization.</h2>
                                    <p>View and edit profiles across active campaigns and take advantage of limitless, customizable segmentation data.</p>
                                </div>
                            </div>
                
			                <div class="inner-span inner-offset1">
                                <div class="inner-headline">
                                <h2>Rules based filters using S.M.A.R.T.&trade; technology.</h2>
                                <p>Segmented Messaging with Automated Response Tracking. Filter messaging according to demographics, location, preferences, behavior - or any criteria you specify. </p>
                                </div>
                            </div>
                                    
                         </div>
                
                    </div>
               
                </div>
               
               	
           
            </div>
        </div>
          
        <div id="servicemainline"></div>  
        
         <!--- All of this ... --->       
        <div id="inner-bg">
                        
            
           <div id="content" style="min-height:36px;">
           		<div class="inner-main-hd">Kick off all of this ...</div>
              	<div class="inner-txt-full" style="margin-bottom:20px;">With Pre-Defined Campaigns you can design highly customized messages and business rules that can be accessed with one simple API call.</div>
              
           </div>
            
            <div id="PDCSurveyImg"> <img src="#rootUrl#/#publicPath#/images/api/hiw_samplesurvey_web.png" height="539" width="900" /></div>
            
           <!---      
                 <div id="inner-content-list">
                     <ul>
                        <li>Complex Scheduling</li>
                        <li>Dynamic Content</li>
                        <li>Documentation</li>
                        <li>List Management</li>
                        <li>Compliance</li>
                        <li>Templates</li>
                        <li>Collaboration</li>
                        <li>Scripting Managment</li>
                        <li>DNC Scrubbing</li>
                        <li>Surveys</li>
                        <li>Reporting</li>
                        <li>... and more</li>
                     </ul>
                 </div>    --->
          
                         
        </div>
                        
        <div id="servicemainline"></div>
      
       
        <!--- For only one API call --->          
        <div id="inner-bg">
            <div id="content">
            
                         
               
          
                    <div class="inner-main-hd">... with just one API call</div>
            
            		<div class="inner-txt-full">
                    
                     	<p><i>Updated on Wed, 2013-01-31 11:30</i></p>
						<p>&nbsp;</p>
                        <p>Allows the authenticating users to post a message to the queue for automated delivery.</p>
                        <p>&nbsp;</p>
                        <p>Returns a JSON structure when successful. Returns a string describing the failure condition when unsuccessful.</p>
                        <p>&nbsp;</p>
                        <p>Actions taken in this method are asynchronous and messages will be delivered based on Pre-Defined Campaign's schedule.</p>

                    	<p>&nbsp;</p>
                        <p>&nbsp;</p>
                    	<div>
                    	    <h2>Resource URL</h2>
                    	    <div>https://ebmii.messagebroadcast.com/webservice/ebm/PDC/AddToQueue</div>
                   	    </div>
                        <p>&nbsp;</p>
                    	
                        <div class="field text field-doc-params">
                            <h2>Parameters</h2>
                            <div>
                                <div id="api-param-inpBatchId" class="parameter">
                                    <span class="param">inpBatchId 
                                        <span>required</span>
                                    </span>
                                    <p>The pre-defined campaign Id you created under your account.</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>864512</tt>
                                    </p>
                                </div>
                                <div id="api-param-inpContactString" class="parameter">
                                    <span class="param">inpContactString
                                    <span>required</span>
                                    </span>
                                    <p>The contact string - can be a Phone number , eMail address, or SMS number </p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>(555)812-1212</tt>
                                    OR
                                    <tt>JoeThompson@eventbasedmessaging.com</tt>
                                    </p>
                                    <p></p>
                                </div>
                                <div id="api-param-inpContactTypeId" class="parameter">
                                    <span class="param">inpContactTypeId
                                    <span>required</span>
                                    </span>
                                    <p>1=Phone number, 2=eMail,3=SMS</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>1</tt>
                                    </p>
                                </div>
                          
                              	
                                <div id="api-param-unlimited" class="parameter">
                          		    <p>Unlimited custom data parameters are accepted via form data. These parameters can be used to control content, branching logic or other message paramters.</p>
                          			<p>&nbsp;</p>
                                    <span class="param">customFormField
                                    <span>optional</span>
                                    </span>
                                    <p>Strings or Numbers - not strongly typed but should evaluate to a number or string</p>
                                    <p>
                                     <strong>Example Values</strong>									
                                    :
                                    <tt>City</tt> OR <tt>Balance</tt> OR <tt>CallerId</tt> OR <tt>whatever your message needs.</tt>
                                    </p>
                                </div>
                            </div>
                            	
                          	<p>&nbsp;</p>
                          
                            <div class="fieldgroup group-example-request">
                                <h2>Example Request</h2>
                                <div>
                                
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST
                                        <span>Required</span>
                                        </span>
                                        <p>https://ebmdevii.messagebroadcast.com/webservice/ebm/PDC/AddToQueue</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST Data
                                        <span>Required</span>
                                        </span>
                                        <p>inpBatchId=864512&amp;inpContactString=5558121212&amp;inpContactTypeId=1</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Header Data
                                        <span>Required</span>
                                        </span>
                                        <p>type="header" name="Accept" value="application/json"</p>
                                        <p>type="header" name="datetime" value="YYYY-MM-DD HH:MM:SS"</p>
                                        <p>type="header" name="authorization" value="ecrypted(datetime)"</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Custom Data
                                        <span>Optional</span>
                                        </span>
                                        <p>type="form" name="inpCompany" value="WidgeCo"</p>
                                        <p>type="form" name="inpCity" value="Corona"</p>
                                        <p>type="form" name="inpState" value="CA"</p>
                                     </div>
                             
                            	</div>
                  	  		</div>
                            
                            
                    		<p>&nbsp;</p>
            		
                            <div>The EBM REST API uses JSON as its return format, and the standard HTTP methods like GET, PUT, POST and DELETE 
                            <BR />(see API documentation <a>here</a> for which methods are available for each resource)</div>                                                       
           
           
                    	</div>
                        
                    </div>
                            
            </div>
        </div>
                
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>