<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - API's</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">
        <link rel="stylesheet" href="documentation/style.css">
        <link rel="stylesheet" href="documentation/grid.css">  
        <link rel="stylesheet" href="documentation/document.css">
        <link rel="stylesheet" href="documentation/document-public.css">
        <link rel="stylesheet" href="documentation/tomorrow.min.css" />
        <script src="documentation/highlight.min.js"></script>



<cfoutput>
	 
</cfoutput>




<style>
	#content {
	   margin: 0 auto;
	}
#ebmimage1 {
	width:970px;
	height:323px;
	padding:0;
	border:0px solid;
	text-align:center;
	background-image:url(../images/ebm_i_main/FPO-Header.png);
}
</style>
<script type="text/javascript" language="javascript">




	$(document).ready(function() {

		
  	});


</script>

        <!-- Document resource -->
        <script src="documentation/angular.min.js"></script>
        <script src="documentation/angular-route.min.js"></script>
        <script src="documentation/angular-highlightjs.min.js"></script>
        <script src="documentation/documentation-app.js"></script>


</head>
<cfoutput>
    <body>
    <div id="main" align="center">

        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">

    
        <div id="inner-bg" align="left">
            <div id="content" data-ng-app="Documentation">
	             <!--- <div class="container_12" data-ng-init="rootPath='http://<cfoutput>#EBMAPIPath#</cfoutput>ebm.messagebroadcast.com/webservice/ebm';">  --->
                <div class="container_12" data-ng-init="rootPath='https://<cfoutput>#EBMAPIPath#</cfoutput>/webservice/ebm';"> 
                <!--- <div class="container_12" data-ng-init="rootPath='<cfoutput>#rootUrl#</cfoutput>/webservice/ebm';"> --->
                
	                <div class="api-container" data-ng-view>

	                </div>
	            </div>
            </div>
        </div>

	
    <div id="servicemainline"></div>

<!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    
    </div>
    
    </body>
</cfoutput>
</html>
