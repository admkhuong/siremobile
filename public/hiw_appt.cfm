<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Appointment Remiders - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );		
		});		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
				
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
  
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Appointment Reminders</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	Remind patients, clients, customers and others of upcoming appointments with phone calls, emails or text messages.          	                
	            </div>
                
                <img style="position:absolute; top:12px; right:100px; " src="#rootUrl#/#publicPath#/images/m1/appointment-new-icon.png" height="256" width="256" /> 
              
            </div>
        </div>
        
    <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Appointment Reminder Calander</h2>
                         <p>Manage Your Appointments Online</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>
                       
                    </h3>
                    
                    <img src="#rootUrl#/#publicPath#/images/m1/calandershot.png" width="1098" height="907"/>
                     
                </div>     
                         
            </div>
        </div>
        
         <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                      
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>create, schedule and manage </h2>
                         <p>easy reminder tools</p>
                    </header>
                </div>    
                                        
                 <div class="hiw_Info" style="width:960px;  margin-top:0px; text-align:center;" align="center">
                    
                    <h3>
                       
                    </h3>
                
	                <img style="float:none" src="#rootUrl#/#publicPath#/images/m1/createappt.png" width="801" height="433"/>
                </div> 
                         
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Deliver Reminders, Confirm appointments, Decrease No Shows
                
            </div>
        </div>            
               
        <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Multiple Reminders</h2>
                         <p>One Appointment or many</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px; text-align:center;" align="center">
                    
                    <h3>
                       
                    </h3>
                
	                <img style="float:none" src="#rootUrl#/#publicPath#/images/m1/reminderlist.png" width="802" height="483"/>
                </div> 
                
                    
                         
            </div>
        </div>
                
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Data Import and Export Options</h2>
                         <p>Bulk Load, API, Manual Entry</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                </div>    
                                        
              	<img src="#rootUrl#/#publicPath#/images/m1/uploadgroup.png" width="1068" height="403"/>
                   
                             
            </div>
        </div>
                        
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

