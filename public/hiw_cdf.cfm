<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CDF - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>


</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function()
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );

		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );
		});


		if( $img[0].width ){ $img.trigger( 'load' ); }

		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");

		$("#PreLoadIcon").fadeOut();
		$("#PreLoadMask").delay(350).fadeOut("slow");


  	});


	<!--- Image preloader --->
	$.preloadImages = function()
	{
		for (var i = 0; i < arguments.length; i++)
		{
			$("<img />").attr("src", arguments[i]);
		}
	}

</script>

</head>
<cfoutput>
    <body>

    <div id="background_wrap"></div>

    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>

    <div id="main" align="center">

        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">

        <div id="bannersection">
        	<div id="content" style="position:relative;">

                 <div class="inner-main-hd" style="width:400px; margin-top:30px;">Custom Data Fields</div>

                <div style="clear:both;"></div>

                <div class="header-content">
                	Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With #BrandShort#'s Custom Data Fields, you don't have too.
	            </div>

                <img style="position:absolute; top:30px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/3ddatabase.png" />

            </div>
        </div>

   		<div id="inner-bg-m1" style="padding-top:25px;">

            <div id="contentm1">

                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>.Your Data - Your Way</h2>
                         <p>Each Company is Unique in how they handle data</p>
                    </header>
                </div>

                <img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/database-management.jpg" height="250" width="550"/>


                    <div class="hiw_Info" style="width:400px; float:right; margin-top:40px;">
                    	<h3>
                        	Our solutions are specifically designed to capitalize on the unique capabilities of your business, to support your way of doing what you do.
                        </h3>
                    </div>

            </div>
        </div>

         <div id="inner-bg-m1" style="padding-top:25px;">

            <div id="contentm1">

                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Segmentation</h2>
                         <p>Market segment, the smaller subgroups comprising a market</p>
                    </header>
                </div>

                <div class="hiw_Info" style="width:800px; margin-bottom:10px;">
                    	<h3>
                        	<ul>
                                <li>
                                    4 Million Total Loyalty Card Customer Base
                                </li>

                                <li>
                                    1 Million in West Region
                                </li>

                                <li>
                                    200K have not transacted at Store in last 30 days
                                </li>

                                <li>
                                    140K Females 18-35
                                </li>

                                <li>
                                    90K are Signed up for Promotional Notifications via CPP for the SMS channel
                                </li>

                                <li>
                                    Those 90K Customers get incentive coupon with a UUID code that is good on their next visit.
                                </li>
                            </ul>
                        </h3>
                    </div>

                     <img src="#rootUrl#/#publicPath#/images/m1/segsample3.png" height="880" width="800" />

            </div>
        </div>

        <div class="transpacer">
        	<div class="inner-transpacer-hd">

            	Market segmentation is a marketing strategy that involves dividing a broad target market into subsets of consumers who have common needs and priorities

            </div>
        </div>

        <div id="inner-bg-m1" style="padding-top:25px;">

            <div id="contentm1">

                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Personalization</h2>
                         <p>marketing to the Segment of One</p>
                    </header>
                </div>

                  <div class="hiw_Info" style="width:750px; margin-top:10px;">
                    <h3>
                        Personalize certain parts of each message for each individual recipient.
                    </h3>

                	<img style="margin-bottom:10px;" src="#rootUrl#/#publicPath#/images/m1/personalizationcp2.png" height="172" width="750"/>

                </div>



            </div>
        </div>

        <div id="inner-bg-m1" style="padding-top:25px;">

            <div id="contentm1">

                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Customization</h2>
                         <p>personalize the product or service to the customer individually</p>
                    </header>
                </div>

                <div class="hiw_Info" style="width:750px; margin-top:10px;">
                    <h3>
                        Customize parts of the message to relate to each individual recipient's transaction history. The knowledge that a company has about a customer needs to be put into practice and the information held has to be taken into account in order to be able to give the client exactly what he wants
                    </h3>

                	<img style="margin-bottom:10px;" src="#rootUrl#/#publicPath#/images/m1/personalizationcp6.png" height="172" width="750"/>

                </div>



            </div>

        </div>

        <div id="inner-bg-m1" style="padding-bottom:25px;padding-top:25px;">

            <div id="contentm1">

                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Unlimited Custom Data Fields</h2>
                         <p> Your account. Your data. No Limits!</p>
                    </header>
                </div>

                <div class="inner-txt-full" style="margin-bottom:20px;">Beyond just First Name / Last Name. Tools to manage your custom data. Your account. Your data. No Limits!</div>

                <img src="#rootUrl#/#publicPath#/images/m1/cdfeditor.png" height="518" width="854" />

            </div>
        </div>


      	<div class="transpacer"></div>

        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">


        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>
