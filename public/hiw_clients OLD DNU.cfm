<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMC - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<cfoutput>
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
</cfoutput>
        

<style>
		
</style>

<!--- Comparison Chart CSS3 --->
<style>

.table {overflow-x: auto;}
table.pricing {width:auto;}
table.pricing th {padding: 10px 0; color: #999; font: 300 1.154em sans-serif; text-align: center; width:125px;}
table.pricing strong {color: #3f3f3f; font-size: 12px;}
table.pricing sup {position: relative; top: -0.5em; color: #3f3f3f; font-size: 1.2em;}
table.pricing td {color: #3f3f3f; text-align: center; line-height:25px;}
table.pricing td:first-child {color: #999; text-align: left;}
table.pricing td:nth-child(2n+2) {background: #f7f7f7;}
table.pricing tr {margin-bottom: 5px; }
table.pricing tr.action td {padding: 20px 10px; border-bottom-width: 2px;}
table.pricing tr.action td:first-child a {padding-left: 20px; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/more.png") 0 50% no-repeat; color: #3f3f3f;}
table.pricing tr.action td:first-child a:hover {color: #ff8400;}
table.pricing span.yes {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/msg-success.png") 50% 50% no-repeat; text-indent: -50em;}
table.pricing span.no {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/no.png") 50% 50% no-repeat; text-indent: -50em;}


.tooltiptext{
	display: none;
}
.showToolTip{
	cursor:pointer;
	
}

div.hide-info{
	float:left;
	margin: 0 5px 0 0;
	line-height:25px;
}

div.hide-info:hover{
	opacity:.8;
}

.info-block{
	background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
    left: 100px;
    padding: 6px;
    position: relative;
    top: -10px;
}


</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Our Clients</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort# has the privilege of working with some of the most well known companies in the world and we don't take this lightly. Our clients engage us in a number of ways we help them solve business problems, increase visibility and help them thrive in a messaging world where customers and employees have become increasingly empowered. We view our clients as partners and work collaboratively with them to achieve results they can measure.                    
                </div>
                
                <img style="position:absolute; top:0px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/mobilerevolution.png" width="366" height="291" />
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                        <h2>Companies that have used #BrandShort#</h2>
                        <p>#BrandShort# Technologies in action</p>
                    </header>
                </div>    
         
        	</div>
        
        	<img src="#rootUrl#/#publicPath#/images/m1/clientsample.png" alt="" width="688" height="497" />
        
        
        </div>
        
        
        
        <div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omnichannel engagement
                
            </div>
        </div>    
        
        
                        
      
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>