<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Feature Comparison - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<cfoutput>
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
</cfoutput>
        

<style>
		
</style>

<!--- Comparison Chart CSS3 --->
<style>

.table {overflow-x: auto;}
table.pricing {width:auto;}
table.pricing th {padding: 10px 0; color: #999; font: 300 1.154em sans-serif; text-align: center; width:125px;}
table.pricing strong {color: #3f3f3f; font-size: 12px;}
table.pricing sup {position: relative; top: -0.5em; color: #3f3f3f; font-size: 1.2em;}
table.pricing td {color: #3f3f3f; text-align: center; line-height:25px;}
table.pricing td:first-child {color: #999; text-align: left;}
table.pricing td:nth-child(2n+2) {background: #f7f7f7;}
table.pricing tr {margin-bottom: 5px; }
table.pricing tr.action td {padding: 20px 10px; border-bottom-width: 2px;}
table.pricing tr.action td:first-child a {padding-left: 20px; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/more.png") 0 50% no-repeat; color: #3f3f3f;}
table.pricing tr.action td:first-child a:hover {color: #ff8400;}
table.pricing span.yes {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/msg-success.png") 50% 50% no-repeat; text-indent: -50em;}
table.pricing span.no {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/no.png") 50% 50% no-repeat; text-indent: -50em;}


.tooltiptext{
	display: none;
}
.showToolTip{
	cursor:pointer;
	
}

div.hide-info{
	float:left;
	margin: 0 5px 0 0;
	line-height:25px;
}

div.hide-info:hover{
	opacity:.8;
}

.info-block{
	background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
    left: 100px;
    padding: 6px;
    position: relative;
    top: -10px;
}


</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Feature Comparison</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort#  outperforms other messaging products by using a mix of proprietary technologies, open source tools, and experience.
                </div>
                
                <img style="position:absolute; top:50px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/comparisonfruit.png" width="401" height="195" />
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                        <h2>Communications backbone</h2>
                        <p>How #BrandShort# Stacks up Against the competition</p>
                    </header>
                </div>    
          <!---      
                l style="margin: 8px 0 0 30px;">
                            <li>
                                PDC from API   
                            </li>
                            
                            <li>
                                Template Services
                            </li>
                                                            
                            <li>
                                Develpoment Services
                            </li>
                            
                            <li>
                                TDM Vs VoIP
                            </li>
                            
                            <li>
                                CPP Site Builder and Hosting with vanity
                            </li>--->
                            
                            

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;">                
                                   
                    <table class="pricing">
                        <tbody>
                        <tr>
                            <th style="width:450px; text-align:left;"><strong>Feature</strong></th>
                            <th><strong>Soundbite</strong></th>
                            <th><strong>Varolii</strong></th>
                            <th><strong>Twilio</strong></th>
                            <th><strong>My Preferences</strong></th>
                            <th><strong>2 People <BR />in a Basement</strong></th>
                            <th><strong>#BrandShort#</strong></th>
                            
                        </tr>
                                                
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">SMS Simulator</div>
                                <div class="tooltiptext">
                                  Easy to use tool for when you need to test SMS campaign functionality. Full interactive control. For use in development or full production QA. No fees. No need for a test number. Time Machine Option. Less repetitive - step into a campaign at any point. 
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Control Point Triggers</div>
                                <div class="tooltiptext">
                                  Campaign can be configured to trigger additional actions at any step. New messaging. Data Hygene. Cross Channel. API calls. 
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                           
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Campaign Manager</div>
                                <div class="tooltiptext">
                                  Intuitive interface to Create, Start, Stop, Pause, Copy, Monitor - Single, Multi, or Cross Channel Campaigns.  
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>                       
                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Reports and Exports</div>
                                <div class="tooltiptext">
                                  Ability to generate detailed reports and exports.
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>  
                                              
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Configurable  Dashboards</div>
                                <div class="tooltiptext">
                                  Digital dashboards allow managers to monitor the contribution of the various campaigns in their organization. To gauge exactly how well a campaign is performing overall, digital dashboards allow you to capture and report specific data points from each aspect within the campaign, thus providing a "snapshot" of performance.  
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Dynamic Schedules</div>
                                <div class="tooltiptext">
                                	From simple to complex. Schedules can be set for each day of the week.  Blackout widows. Custom Throttleing. Campaigns can change message options based on time of day.
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>  
                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Drip Marketing</div>
                                <div class="tooltiptext">
                                	A Drip marketing campaign is a communication strategy that sends, or "drips," a pre-scripted set of messages to customers or prospects over time. Exchange useful information with your customers as a way to stay on their minds. Increased opportunities for up sell and cross sell. 
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                       
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Basic API</div>
                                <div class="tooltiptext">
                       				Access to functionality via API calls. You develop, we fulfill.
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                                                                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Predefined Campaign Templates via API</div>
                                <div class="tooltiptext">
                                	Build and manage all of the messaging complexity using our simple web based tools. Deliver the messaging within your application with one simple API call. Need to change your messaging? Don't modify or re-compile your app, just update your campaign using #BrandShort# online tools.
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                        
                         <tr>                        
	                        <td>
                                <div class="hide-info showToolTip">API Security via User Name Password</div>
                                <div class="tooltiptext">Support for legacy systems that don't allow easy access or scripting for more advanced security. Requires SSL connections at minimum. </div>		                                                    
                            </td>   
                                                    
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">yes</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">API Security via Pub Key / Private Key</div>
                                <div class="tooltiptext">Authentication in the cloud. Obuscated structures. Changing input tokens over time.</div>		                                                    
                            </td>   
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>        
                                        
                        <tr>
                          	<td>
                                <div class="hide-info showToolTip">API Security via VPN API Option</div>
                                <div class="tooltiptext">Enterprise level security for your most sensitive data.</div>		                                                    
                            </td>   
                            
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                       
                      	<tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Permissions Controls</div>
                                <div class="tooltiptext">
                                	Managed Company Accounts can control which users have access to which functionality. Limit reports, setup, campaign control to select accounts.
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                                                
                      	<tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Filters</div>
                                <div class="tooltiptext">
                                	Filter on an unlimited choice of Custom Data Fields. Market segmentation is a marketing strategy that involves dividing a broad target market into subsets of consumers who have common needs and priorities 
                                </div>		                                                    
                            </td>
                           
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                        
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Compliance</div>
                                <div class="tooltiptext">
                                	Qualified Security Assessor (QSA) reviewed development, structures and flow. PCI 3.O, HIPPA, SSAE 16, GLBA
                                </div>		                                                    
                            </td>
                           
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                                               
                        <tr>                        
                         	<td>
                                <div class="hide-info showToolTip">Customer Preference Management</div>
                                <div class="tooltiptext">
                                	Capture and store customer preference data.
                                </div>		                                                    
                            </td>
                           
                           	<td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>   
                                               
                        <tr>                                                    
                            <td>
                                <div class="hide-info showToolTip">Customer Preference Portals in 15 minutes</div>
                                <div class="tooltiptext">
                                	Capture, Store, Serve - API and online. Online Tools to Help Build Your Own Portal
								</div>		                                                    
                            </td>
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Enterprise Voice TDM</div>
                                <div class="tooltiptext">
                                	Direct Carrier Relationships. Better signal quality. Better success rates. Better volume flow.
								</div>		                                                    
                            </td>                            
                            
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Simple SMS</div>
                                <div class="tooltiptext">
                                	Basic Keyword Response. Delayed Response. Limited Error handling. 
								</div>		                                                    
                            </td>   
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                             
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Advanced SMS</div>
                                <div class="tooltiptext">
                                	Automated Session Tracking. Instant Response. Interactive Campaigns. Double Opt In. Error handling. Drive Mode SMS IC ignore - Automated Safe Driver notifications.
								</div>		                                                    
                            </td>   
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>                                          
                        
                        
                        <tr>                                                      
                            <td>
                                <div class="hide-info showToolTip">Enterprise eMail</div>
                                <div class="tooltiptext">
                                	Template Design. Testing on Various target client tools. Custom DNS/MX records. Whitelisting. 
								</div>		                                                    
                            </td>   
                            
                            <td><span class="yes">yes</span></td>
                            <td><span class="yes">yes</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>                        
                       		<td>
                                <div class="hide-info showToolTip">Time Machine</div>
                                <div class="tooltiptext">QA tools to allow you to step through campaigns over time and to quickly verify multiple possible paths</div>		                                                    
                            </td>
                          
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                         <tr>
                            <td>
                                <div class="hide-info showToolTip">Branching logic with regular expressions</div>
                                <div class="tooltiptext">React to user input in even more dynamic ways.</div>		                                                    
                            </td>
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>                        
                       		<td>
                                <div class="hide-info showToolTip">EAI Legacy Flow Control</div>
                                <div class="tooltiptext">Messaging Campaigns can respond and fulfill through legacy systems.</div>		                                                    
                            </td>
                          
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>                        
                       		<td>
                                <div class="hide-info showToolTip">Unlimited Custom data Fields</div>
                                <div class="tooltiptext">Your company's unique strengths are its competitive advantage. Why should you compromise with vanilla application deployments or other vendor limits? With EBM's Custom Data Fields, you don't have too.</div>		                                                    
                            </td>
                          
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                         
                        <tr>                            
                            <td>
                                <div class="hide-info showToolTip">Messaging Template Services</div>
                                <div class="tooltiptext">Our team will for a low cost, build your templates for you. No more waiting on your own IT priorities, time, and budgets.</div>		                                                    
                            </td>
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Robust User / Company Management  Tools</div>
                                <div class="tooltiptext">Permission and Role based user management that provides access to highly customizable capabilities based on each user.</div>		                                                    
                            </td>                            
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                                                
                        <tr>
                        
                         	<td>
                                <div class="hide-info showToolTip">Custom Agent Tools</div>
                                <div class="tooltiptext">Integrated Custom Agent screens as well as standard screens for common tasks</div>		                                                    
                            </td>      
                                                      
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>                            
                        </tr>
                       
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Online Campaign Definition Forms</div>
                                <div class="tooltiptext">Industry unique drag and drop form based definitions - easier to read, print, and review</div>		                                                    
                            </td>
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="hide-info showToolTip">Advance EAI Tools and Experience</div>
                                <div class="tooltiptext">Do more faster, with less people, and less money.</div>		                                                    
                            </td>
                            
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
            
                        <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">Friends &amp; Messaging System</div>
                                <div class="tooltiptext">Babbles.co - Social interaction tools built on #BrandShort# for sharing Voice, Text, and Pictures.</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                                                
                        <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">e-Messaging Service</div>
                                <div class="tooltiptext">The industries most complex tasks in their simplest form</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                         <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">Concatenated Human Speech</div>
                                <div class="tooltiptext">Online Script Librariy Tools</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                         <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">2-Step / Multi Factor Authentication</div>
                                <div class="tooltiptext">The quickest tools to full production</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                         <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">Data Hygiene Tools</div>
                                <div class="tooltiptext">Custom Data Field Capture tools to enforce data hygiene.</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                         <tr>                           
                       		<td>
                                <div class="hide-info showToolTip">Multi-Channel Contact Tools</div>
                                <div class="tooltiptext">/List/Group Manager for Phone, SMS, eMail, FAX, OTT, Address, and more. Unlimted Contact Types supported in the design.</div>		                                                    
                            </td>                           
                        
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="no">no</span></td>
                            <td><span class="yes">yes</span></td>
                        </tr>
                        
                                                   
                    </tbody></table>
            
            
        		</div>
        
        	</div>
        
        </div>
        
        <div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omni-channel engagement
                
            </div>
        </div>    
        
        
                        
      
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>