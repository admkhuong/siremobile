<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - Contact Centers</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>
	 <script src="#rootUrl#/#publicPath#/js/jquery.powerzoom.js"></script>      
</cfoutput>


<script type="text/javascript" language="javascript">

	$(document).ready(function() {

		$('#PDCSurveyImg img').addpowerzoom({
		  defaultpower:3,
		  magnifiersize: [450, 250],
		  powerrange: [3, 3] <!---//Possible range: 2x to 10x magnification--->
		  });  
		  
		
  	});

</script>

</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
       <!--- *** JLP todo 
		   Add note about VoIP vs TDM and dedicated DSP's
	   ---> 	
                
          <div id="inner-bg">
            
            <div id="content" style="min-height:36px;">
           		<div class="inner-main-hd">Call Center Integration</div>
              	<div class="inner-txt-full" style="margin-bottom:20px;">Live Agent Transfer&c</div>
        	
            	<div id="inner-box-left" align="center">
                        
                      	<ul>
                            <li>Web Services for Additional Information</li>
                            <li>Caller ID Management</li>
                            <li>Whisper Greetings</li>
                            <li>Call for the Expert Queue management</li>                          
                        </ul>
                                 
                </div>      
				
        	</div>
                                              
        </div> 
        
        
        
        <div id="inner-bg-alt">
            <div id="content">
               
               <div id="inner-left-img"> <img src="#rootUrl#/#publicPath#/images/hiw/personalizedii.png" width="241" height="337"/></div>
               
               <div id="inner-box-left" align="center" style="margin-left:60px;">
          
                    <div class="inner-main-hd" >Excel, Surpass, Stand out</div>
                    <div class="inner-txt">
                    	Consumers today are smarter, more savvy recipients and interpreters of information. 'Marketing' per se just doesn't work on them very well. This forces businesses to think about them in a different way -- you need to start with a good product, good service, and good content, and then figure out how to get people to pay attention, rather than the other way around.
                         <br />
                         <BR />
                         Communicate with your customers in ways that will help their lives or their companies. Differentiate your brand by doing something that will help them, inspire them or even will make them laugh. Be unique in the ways you reach out to them, interact with them. Make them remember what it is they love about your company, and why they want to do business with you. The answer will be because you've built a connection with them, and because it isn't just business, it's personal.
                    	
                        <BR />
                        <BR />
                        <BR />
                        <p>Goals to keep in mind while designing message components:</p>
                        <ul>
                            <li>Exceed Expectations</li>
                            <li>Tap into Emotions</li>
                            <li>Be Consistent</li>
                            <li>Be genuine</li>                          
                        </ul>
                    	  
                  </div>
                                   
                 
                                   
                </div>
               
               	
           <!--- Not just TTS but custom concatenated speech libraries to give it a more personalized touch.--->
            </div>
        </div>
        
        
        
        
        <div id="inner-bg">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    <div class="inner-main-full">Message Branching, Business Rules and Interactive Response</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;">While mail merge is still an important tool, #BrandShort# picks up where others leave off. #BrandShort# provides tools and strucures for Branching, Business Rules and Interactive Response as well.</div>
                </div>
                
                 <div class="inner-txt-full" style="margin-bottom:20px; margin-top:20px;">
                        <ul>
                        	<li>Branch to alternate message content based on personalized transactional data details.</li>
                            <li>Apply business rules to modify or block messaging based on recent or upcoming events.</li>
                        	<li>Taylor additional message based on conversational data. Satisfaction response, keywords, identification, etc.</li>                        
                            <li>Content management tools to help manage complex message design and modification.</li>
                        </ul>                       
              	</div>
                
           </div>     
           
           <div id="PDCSurveyImg"> <img src="#rootUrl#/#publicPath#/images/api/hiw_samplesurvey_web.png" height="539" width="900" /></div>
                
        </div>
        
        
        <!--- Add audio file sample with player here --->
         <div id="inner-bg-alt">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    <div class="inner-main-full">Complex Concatenated Human Speech (CCHS)</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;">Not just Text-To-Speech (TTS). #BrandShort# provides tools to use complex concatenated human speech for life-like dynamic voice message quality.</div>
                </div>
                
                 <div class="inner-txt-full" style="margin-bottom:20px; margin-top:20px;">
                                          
              	</div>
                
           </div>   
           
           <div><img src="#rootUrl#/#publicPath#/images/hiw/scriptlibsurveysample.png" width="900" height="560" /></div>  
                
        </div>
            
        
        <div id="inner-bg">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    <div class="inner-main-full">XML driven message customization for Voice, SMS, Email and more.</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;">The possibilities are limitless. Our backend fulfillment devices offer highly customizable capabilities. are message format agnostic. Simple messages to company specific set of rules and strucure that lets your brand excel, surpass and stand out. With our tools and experience, we can help you craft your personalized messages.</div>
                </div>
                
                <div class="inner-txt-full" style="margin-bottom:20px; margin-top:20px;">
                
                    <p>&lt;DM BS='0' DSUID='236' Desc='Complex Survey Sample' LIB='0' MT='1' PT='12'&gt;<br />
                        &lt;ELE ID='0'&gt;0&lt;/ELE&gt;<br />
                        &lt;/DM&gt;<br />
                        &lt;RXSS X='100' Y='60'&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK13='' CK2='(2)' CK3='' CK4='(2,2)' CK5='2' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='1' DSUID='236' LINK='2' QID='1' RQ='0' RXT='2' X='240' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK5='3' DESC='We would like to ask' DI='1' DS='1' DSE='2' DSUID='236' LINK='3' QID='2' RXT='1' X='440' Y='60'&gt;<br />
                        &lt;ELE DESC='2.1a' ID='2'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='2.1b' ID='2'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1)' CK3='' CK4='(1,4)' CK5='4' CK6='3' CK7='8' CK8='5' CK9='30' DI='1' DS='1' DSE='3' DSUID='236' LINK='4' QID='3' RQ='0' RXT='2' X='700' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='5' CP='1' DESC='If at anytime' DI='1' DS='1' DSE='7' DSUID='236' LINK='5' QID='4' RXT='1' X='950' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,6),(9,6),(8,6),(7,6),(6,6),(5,6),(4,6),(3,6),(2,6),(1,6),(*,5)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='8' DSUID='236' LINK='8' QID='5' RQ='1' RXT='6' X='1250' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='10' DESC='In responding' DI='1' DS='1' DSE='9' DSUID='236' LINK='10' QID='6' RXT='1' X='1520' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Please press the ## key...' DI='1' DS='1' DSE='25' DSUID='236' LINK='-1' QID='7' RXT='1' X='1890' Y='60'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Thank you for participate' DI='1' DS='1' DSE='22' DSUID='236' LINK='-1' QID='8' RXT='1' X='1890' Y='190'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Invalid respond' DI='1' DS='1' DSE='23' DSUID='236' LINK='-1' QID='9' RXT='1' X='1890' Y='340'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK5='11' DESC='on [date]' DI='3' DS='25' DSE='2' DSUID='236' LINK='11' QID='10' RXT='1' X='40' Y='290'&gt;<br />
                        &lt;ELE DESC='on' ID='6'&gt;3&lt;/ELE&gt;<br />
                        &lt;ELE DESC='date' DK='21' ID='TTS' RXVID='1'&gt;{%CustomField1_vch%}&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,12),(9,13),(8,14),(7,15),(6,16),(5,17),(4,18),(3,19),(2,20),(1,21)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='10' DSUID='236' LINK='8' QID='11' RQ='2' RXT='6' X='240' Y='290'&gt;<br />
                        &lt;ELE DESC='20.1' ID='10'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,22),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='2' DS='1' DSE='11' DSUID='236' LINK='-1' QID='12' RQ='0' RXT='2' X='420' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1 Conerning the represetitives overall handleing of the call you selected...' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='10' ID='28'&gt;10&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='13' RQ='0' RXT='2' X='560' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='9' ID='28'&gt;9&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='14' RQ='0' RXT='2' X='700' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='8' ID='28'&gt;8&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='15' RQ='0' RXT='2' X='840' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='7' ID='28'&gt;7&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='16' RQ='0' RXT='2' X='980' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='6' ID='28'&gt;6&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='17' RQ='0' RXT='2' X='1120' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='5' ID='28'&gt;5&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='18' RQ='0' RXT='2' X='1250' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='4' ID='28'&gt;4&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='19' RQ='0' RXT='2' X='1390' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='3' ID='28'&gt;3&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='20' RQ='0' RXT='2' X='1530' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='2' ID='28'&gt;2&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,23),(2,11)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='11' DSUID='236' LINK='-1' QID='21' RQ='0' RXT='2' X='1670' Y='290'&gt;<br />
                        &lt;ELE DESC='21.1' ID='11'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='1' ID='28'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='22.1' ID='11'&gt;2&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='24' DESC='We are glad' DI='1' DS='1' DSE='12' DSUID='236' LINK='24' QID='22' RXT='1' X='50' Y='620'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='24' DESC='We are sorry' DI='1' DS='1' DSE='13' DSUID='236' LINK='24' QID='23' RXT='1' X='50' Y='890'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,25),(9,25),(8,25),(7,25),(6,25),(5,25),(4,25),(3,25),(2,25),(1,25)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='15' DSUID='236' LINK='8' QID='24' RQ='3' RXT='6' X='280' Y='750'&gt;<br />
                        &lt;ELE DESC='40.1' ID='15'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,26),(9,26),(8,26),(7,26),(6,26),(5,26),(4,26),(3,26),(2,26),(1,26)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' DI='1' DS='1' DSE='16' DSUID='236' LINK='8' QID='25' RQ='4' RXT='6' X='630' Y='750'&gt;<br />
                        &lt;ELE DESC='50.1' ID='16'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='1' CK1='3' CK10='3' CK11='9' CK12='3' CK2='' CK3='3' CK4='(10,27),(9,27),(8,27),(7,27),(6,27),(5,27),(4,27),(3,27),(2,27),(1,27)' CK5='8' CK6='0' CK7='0' CK8='-1' CK9='7' CP='2' DI='1' DS='1' DSE='17' DSUID='236' LINK='8' QID='26' RQ='5' RXT='6' X='990' Y='750'&gt;<br />
                        &lt;ELE DESC='60.1' ID='17'&gt;1&lt;/ELE&gt;<br />
                        &lt;ELE DESC='Please select a number from one to ten ...after your numeric response.' ID='27'&gt;1&lt;/ELE&gt;<br />
                        &lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='11' CK4='(1,30)' CK5='28' DSUID='236' LINK='28' QID='27' RXT='16' X='1310' Y='750'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='11' CK4='(10,29),(9,29),(6,29),(5,29),(4,29),(3,29),(2,29),(1,29)' CK5='8' DSUID='236' LINK='8' QID='28' RXT='16' X='1600' Y='750'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,32),(2,31)' CK5='-1' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='20' DSUID='236' LINK='-1' QID='29' RQ='0' RXT='2' X='70' Y='1111'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='3' CK10='9' CK11='0' CK12='5' CK2='(1,2)' CK3='' CK4='(1,28),(2,28)' CK5='28' CK6='' CK7='8' CK8='5' CK9='30' CP='0' DI='1' DS='1' DSE='19' DSUID='236' LINK='28' QID='30' RQ='0' RXT='2' X='520' Y='1111'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='3' CK10='' CK11='' CK12='' CK13='' CK2='45' CK3='33' CK4='0' CK5='0' CK6='' CK7='0' CK8='8' CK9='-13' DSUID='236' LINK='8' QID='31' RQ='6' RXT='3' X='1020' Y='1121'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='0' CK5='-1' DESC='Thank you' DI='1' DS='1' DSE='22' DSUID='236' LINK='-1' QID='32' RXT='1' X='1570' Y='1151'&gt;0&lt;/ELE&gt;<br />
                        &lt;ELE BS='0' CK1='3' CK5='-1' DESC='After the tone' DI='1' DS='1' DSE='21' DSUID='236' LINK='-1' QID='33' RXT='1' X='1890' Y='1151'&gt;0&lt;/ELE&gt;<br />
                        &lt;/RXSS&gt;<br />
                        &lt;RULES/&gt;<br />
                        &lt;CCD CID='7777777777' ESI='-12'&gt;0&lt;/CCD&gt;<br />
                        &lt;STAGE h='1700' s='0.8999999999999999' w='3200'&gt;<br />
                        &lt;ELE DESC='Greeting' ID='1' STYLE='width: 164px; height: 28px; top: 0px; left: 0px;' X='230' Y='0'/&gt;<br />
                        &lt;ELE DESC='We would like to ask' ID='2' STYLE='width: 300px; height: 42px; top: 0px; left: 0px;' X='430' Y='0'/&gt;<br />
                        &lt;ELE DESC='If you are the person' ID='3' STYLE='width: 300px; height: 45px; top: 0px; left: 0px;' X='690' Y='0'/&gt;<br />
                        &lt;ELE DESC='If at anytime' ID='4' STYLE='width: 217px; height: 40px; top: 0px; left: 0px;' X='940' Y='0'/&gt;<br />
                        &lt;ELE DESC='If a friend' ID='5' STYLE='width: 300px; height: 48px; top: 0px; left: 0px;' X='1240' Y='0'/&gt;<br />
                        &lt;ELE DESC='In responding' ID='6' STYLE='width: 218px; height: 42px; top: 0px; left: 0px;' X='1510' Y='0'/&gt;<br />
                        &lt;ELE DESC='Please press the ## key...' ID='7' STYLE='width: 257px; height: 36px; top: 0px; left: 0px;' X='1880' Y='20'/&gt;<br />
                        &lt;ELE DESC='Thank you for participate' ID='8' STYLE='width: 256px; height: 36px; top: 0px; left: 0px;' X='1880' Y='150'/&gt;<br />
                        &lt;ELE DESC='You have selected an invalid response' ID='9' STYLE='width: 285px; height: 41px; top: 0px; left: 0px;' X='1880' Y='280'/&gt;<br />
                        &lt;ELE DESC='on [date]' ID='10' STYLE='width: 167px; height: 35px; top: 0px; left: 0px;' X='30' Y='240'/&gt;<br />
                        &lt;ELE DESC='Q1: overall rating' ID='11' STYLE='width: 195px; height: 27px; top: 0px; left: 0px;' X='200' Y='240'/&gt;<br />
                        &lt;ELE DESC='You selected 10 for Q1' ID='12' STYLE='width: 300px; height: 29px; top: 0px; left: 0px;' X='360' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 9' ID='13' STYLE='width: 300px; height: 34px; top: 0px; left: 0px;' X='550' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 8' ID='14' STYLE='width: 300px; height: 29px; top: 0px; left: 0px;' X='690' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 7' ID='15' STYLE='width: 300px; height: 33px; top: 0px; left: 0px;' X='830' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 6' ID='16' STYLE='width: 300px; height: 44px; top: 0px; left: 0px;' X='960' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 5' ID='17' STYLE='width: 300px; height: 47px; top: 0px; left: 0px;' X='1110' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 4' ID='18' STYLE='width: 300px; height: 45px; top: 0px; left: 0px;' X='1240' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 3' ID='19' STYLE='width: 157px; height: 34px; top: 0px; left: 0px;' X='1380' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 2' ID='20' STYLE='width: 213px; height: 43px; top: 0px; left: 0px;' X='1520' Y='240'/&gt;<br />
                        &lt;ELE DESC='Selected 1' ID='21' STYLE='width: 144px; height: 44px; top: 0px; left: 0px;' X='1660' Y='240'/&gt;<br />
                        &lt;ELE DESC='We are glad' ID='22' STYLE='width: 192px; height: 34px; top: 0px; left: 0px;' X='40' Y='570'/&gt;<br />
                        &lt;ELE DESC='We are sorry' ID='23' STYLE='width: 149px; height: 42px; top: 0px; left: 0px;' X='40' Y='850'/&gt;<br />
                        &lt;ELE DESC='Q2: courtesy rating' ID='24' STYLE='width: 212px; height: 34px; top: 0px; left: 0px;' X='230' Y='710'/&gt;<br />
                        &lt;ELE DESC='Q3: knowledge rating' ID='25' STYLE='width: 242px; height: 42px; top: 0px; left: 0px;' X='581.7000122070312' Y='676'/&gt;<br />
                        &lt;ELE DESC='Q4: time resolve rating' ID='26' STYLE='width: 300px; height: 34px; top: 0px; left: 0px;' X='930' Y='710'/&gt;<br />
                        &lt;ELE DESC='If Q1 response is 1, then go to 30 (call back)' ID='27' STYLE='width: 300px; height: 50px; top: 0px; left: 0px;' X='1220' Y='690'/&gt;<br />
                        &lt;ELE DESC='If Q1 response is other than 7 and 8, then go to 29 (leave a comment)' ID='28' STYLE='width: 300px; height: 69px; top: 0px; left: 0px;' X='1530' Y='690'/&gt;<br />
                        &lt;ELE DESC='If you would like to receive a call back' ID='29' STYLE='width: 300px; height: 62px; top: 0px; left: 0px;' X='425.5999755859375' Y='1030'/&gt;<br />
                        &lt;ELE DESC='If you would like to leave a comment' ID='30' STYLE='width: 300px; height: 62px; top: 0px; left: 0px;' X='50' Y='1050'/&gt;<br />
                        &lt;ELE DESC='Record a comment' ID='31' STYLE='width: 231px; height: 41px; top: 0px; left: 0px;' X='980' Y='1080'/&gt;<br />
                        &lt;ELE DESC='Thank you for participating' ID='32' STYLE='width: 267px; height: 43px; top: 0px; left: 0px;' X='1530' Y='1111'/&gt;<br />
                        &lt;ELE DESC='After the tone' ID='33' STYLE='width: 247px; height: 47px; top: 0px; left: 0px;' X='1880' Y='1111'/&gt;<br />
                        &lt;/STAGE&gt;</p>
                </div>
                
                
            </div>
                
        </div>    
              
        <div id="servicemainline"></div> 
        
      
                
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>