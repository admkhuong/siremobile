<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CPP - How It Works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<style>
				
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );				
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
								 
				
		$('#CPPModules').click(function()
		{			
			
			$('#CPPModulesDialogContent').dialog({ 
				autoOpen: true,
				modal: true,
				height: 1985,
				width: 895,
				title: 'CPP Modular Build',	
				<!---// buttons: { "Close": function () { $(this).dialog('destroy'); } },	--->		
				position: { my: "left top", at: "left bottom", of: $(this), collision: 'none' } 
			});
			
		});
		
		$('#CPPStyles').click(function()
		{			
			
			$('#CPPStylesDialogContent').dialog({ 
				autoOpen: true,
				modal: true,
				height: 1400,
				width: 1080,
				title: 'CPP Customizeable Look and Feel.',	
				<!---// buttons: { "Close": function () { $(this).dialog('destroy'); } },--->			
				position: { my: "left top", at: "left bottom", of: $(this), collision: 'none' } 
			});
			
		});

		$('#TryItNow').click(function()
		{			
		
			var page = "<cfoutput>#LocalProtocol#</cfoutput>://contactpreferenceportal.com/ebmcppsample1";

			var $dialog = $('<div></div>')
               .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
               .dialog({
                   autoOpen: true,
                   modal: true,
                   height: 900,
                   width: 1080,
                   title: "Sample Customer Preference Portal"
               });
						
		});
		
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
               <div class="inner-main-hd" style="width:650px; margin-top:30px;">Customer Preference Portals (CPP) and Tools</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">Multi-Channel, Cross-Channel Messaging begins with Customer Preference. Use #BrandShort#'s CPP Tools to help gather Customer Contact Preferences.
                	<!---Capture, Store, Serve - API and online 
                    Consumers, Employees,      --->         	                
	            </div>
                
                <img style="position:absolute; top:30px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/ebmcppsample1qr.png" /> 
              
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                         <h2>Capture, Store, Serve - API and online qqq</h2>
                         <p>Online Tools to Help Build Your Own Portal</p>
                    </header>
                </div>    
                                        
                <div id="CPPModules"><img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/cppdanddsmalllores.png" width="188" height="400" /></div>
                <div id="CPPModulesDialogContent" style="display:none;"><img style="float:left;" src="<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/m1/moduleslores.png" width="863" height="1841" /></div>                
                
                <img style="float:left; margin: 0 10px;" src="#rootUrl#/#publicPath#/images/m1/build400.png" width="40" height="400" />
                
                <div id="CPPStyles" title="Customizeable Look and Feel."><img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/cppnewcostylesmall.png" width="325" height="400" /></div>
                <div id="CPPStylesDialogContent" style="display:none;"><img style="float:left;" src="<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/m1/portaltoolslores.png" width="1055" height="1297" /></div>
                     
                <div class="hiw_Info" style="width:360px; float:right; margin-top:0px;">
                    
	                    <h3>Consumer directed Collection and Updating. Make the portal your own. Brand consistency.</h3>
                    
                    <ul style="margin: 8px 0 0 30px;">
                            
                        <li>Up in running in less than an hour.</li>
                        <li>Generic URLs - CustomerPreferencPortal.com</li>
                        <li>Vanity URLs</li>
                        <li>Access via API, iFrame or Standalone</li>
                        <li>Customize to your company style</li>  
                        <li>Pre-Generated QR codes</li>  
                        <li>Tiny URL compatiable</li>                          
                        <li>allows customers to choose which program and communication channel they want to enable/disable</li>
                        <li>integrate transparently into yoursite.com</li>
                        <li>ability to set multiple mobile phone numbers (by account)</li>
                        <li>ability to set multiple emails (by account)</li>
                        <li>perform double opt ins on text messaging per mobile number</li>
                        <li>tracking of all statuses/opt-ins/pendings/etc</li>
                        <li>audit history of all communications sent via the portal</li>
                        <li>add programs/communications as needed, with communication parameters</li>
                        <li>While integrated well with #BrandShort#, CPP tools are vendor agnostic on outbound IVR, emails and text messaging</li>
                                                                                   
                     </ul>                            
                    
                    <h3 style="padding-top:10px;"><a id="TryItNow" style="text-decoration:underline;">Try one Live</a></h3>
                        
                </div>                                
                             
            </div>
        </div>
                          
		<div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Express Consent</h2>
                         <p>Double Opt In, Consent Tracking, Updatable Preferences yyy</p>
                    </header>
                </div>    
                
                <img style="float:right; margin-right:50px;" src="#rootUrl#/#publicPath#/images/m1/optinsms.png" width="256" height="300" />
                    
                    
                <div class="hiw_Info" style="width:560px; float:left; margin-top:90px; margin-left:50px;">
                    
                    <h3>Confirm the consumer's choices to opt in via the target device and channel.</h3>
                    
                    <ul style="margin: 8px 0 0 30px;">
                            
                        <li>Reply yes to confirm via SMS</li>
                        <li>Press 1 to confirm via Voice</li>
                        <li>Click link to confirm email</li>  
                                                                                         
                     </ul>                            
                        
                </div>
                             
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	CPP Tools Simplify IT, Marketing, and Compliance
                
            </div>
        </div>            
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Mobile Optimized</h2>
                         <p>Capture from Online or Mobile</p>
                    </header>
                </div>    
                                        
                <img style="float:right; margin-right:100px;" src="#rootUrl#/#publicPath#/images/m1/cppmobilecombo.png" width="300" height="400" />
                                              
                <div class="hiw_Info" style="width:360px; float:left; margin-top:90px; margin-left:50px;">
                   
                    <h3>Most Phones and Tablets Compatible</h3>
                        
                </div>             
                             
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Custom Data Fields</h2>
                        <p>Easy method for collecting quantitative information from the customer population</p>
                    </header>
                </div>    
                
 				<img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/optinsms.png" width="256" height="300" />
                    
                    
                <div class="hiw_Info" style="width:560px; float:right;">
                    <h3>Let your consumers provide and update their preference info. You only have to give them the URL to your own branded portal.       Exchange somthing of value - Coupons, Sales, Information, Alerts, Inventory Updates - in exchange for building a 
                    A flier or a table tent with a QR code is a great way to get consumers to opt in for your messages. Coupons, Sales events, inventory changes, there are lots of ways to consumers are willing to interact with your company even if they don't have an "account"</h3>
                    
                    <ul style="margin: 8px 0 0 30px;">
                            
                        <li>Employees</li>
                        <li>Social Groups</li>
                        <li>Coaches / Teams</li>  
                        <li>Outage Notifications</li>
                        <li>Call Center Deflection</li>  
                                                                                         
                     </ul>                            
                    
                    
                    <h3><i>"I use the CPP and EMS tools combined to let the team know when a practice/game has been rescheduled, moved, or added. Much quicker then listening to the parents explain why their kid should be moved to third base to get more playing time."</i></h3>
                    
                        
                </div>                        
                                 
            </div>
                    
        </div>
                              
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:25px;">
        
            <div id="contentm1">    
            
             	<div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Embeded in site</h2>
                        <p>Live Version Embedded in site below</p>
                    </header>
                </div>    
                                   
       	       <iframe src="#LocalProtocol#://contactpreferenceportal.com/ebmcppsample1" scrolling="auto" align="center" height = "920px" width="600px" frameborder="0" id="CPPPRevieFrame"></iframe>
               
           	</div>
                    
        </div>
               
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

