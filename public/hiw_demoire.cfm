<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Demo IRE - How it works</title>

<cfinclude template="../session/cfc/csc/constants.cfm">
<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />

<style>
	
	#OTTDemoContainer{
			padding:0px;
			margin:0px;
			font-family: 'Open Sans', sans-serif;
        }
		
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                 <div class="inner-main-hd" style="width:400px; margin-top:30px;">Demo Interactions</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	SMS, Push, or Web Smart Interactive Response Engine                 
	            </div>
                
               <!--- <img style="position:absolute; top:30px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/mfademoheader.png" /> --->
              
            </div>
        </div>
        
   		<div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                         <h2>DEMO INTERACTIONS</h2>
                         <p>Over the Top</p>
                    </header>
                </div>                
                         
            </div>
            
            <div class="container" style="text-align: left; padding-bottom:25px;">  
	            <cfset BrandDemoKeyword = "DemoEBMOTT" />                              
				<cfset SA = 0 />
                <cfinclude template="../pub/demoire.cfm" />
            </div> 
                
        </div>
                      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

