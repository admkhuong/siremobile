<!DOCTYPE html>
<html lang="en">
<head>

	<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>EBM - Event Based Messaging</title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
       
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
			
			a 
			{
				color: ##E7746F;
			}
			
			

		</style>
        
    </cfoutput>
      
      
      
      <style>	  
	  
	  		#tabletenthome
			{
				
				background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home7assets/images/m7/img_5849a.jpg");
				<!---background: url(../images/cover.jpg);--->
				background-color: #222;
				background-attachment: fixed;
				background-repeat: no-repeat;
				background-position: 50% 50%;
				-webkit-background-size: cover;
				   -moz-background-size: cover;
					 -o-background-size: cover;
						background-size: cover;
				padding: 0;
			}
	  
	  </style>
      
       
     
</head>
<body>

	<!-- Preloader -->

	<div id="preloader">
		<div id="status"></div>
	</div>
  
	<cfif Session.USERID GT 0>
        <cfinclude template="home7assets/inc/signout.cfm">                      
    <cfelse>                        
        <cfinclude template="home7assets/inc/signin.cfm">                                         
    </cfif>
 
	<!--- Home start --->

	<section id="tabletenthome" class="pfblock-image screen-height">
        <div class="home-overlay"></div>
		<div class="intro">
        
        <!---
			<div class="start">
				
                <cfoutput>
					<img src="#LogoImageLink7#" style="border:none;" />
				</cfoutput>
            	    
            </div>
			--->
            			
          <!---  <h1>Event Based Messaging</h1>
			<div class="start">Tools for creating and managing modern responsive consumer communication applications</div>

            <!--- Account log in / out options --->
            <div>                                           
                
				<cfif Session.USERID GT 0>
                    <cfoutput><a id="InSessionAccount" class="InlineLink" href="#rootUrl#/#SessionPath#/account/home">My Account</a></cfoutput>
                    <a href="javascript:;" class="InlineLink" id="signout-popup-with-form" data-mfp-src="#confirm-logout" >Log Out</a>                        
                <cfelse>                        
                    <a id="signin-popup-with-form" href="javascript:;"  data-mfp-src="#EBMSignInSection_0" class="InlineLink">Sign In</a>
                    <a id="signuplink" href="signup" class="InlineLink" >Sign Up</a>                                          
                </cfif>
                           
            </div>--->
            
		</div>

        <a href="#services">
		<div class="scroll-down">
            <span>
                <i class="fa fa-angle-down fa-2x"></i>
            </span>
		</div>
        </a>

	</section>

	<!--- Home end --->


	<cfinclude template="footer.cfm" />

	<!--- Scroll to top --->

	<div class="scroll-up">
		<a href="#home"><i class="fa fa-angle-up"></i></a>
	</div>
    
    <!--- Scroll to top end --->

	<!--- Javascript files --->

	
	<script src="home7assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="home7assets/js/jquery.parallax-1.1.3.js"></script>
	<script src="home7assets/js/imagesloaded.pkgd.js"></script>
	<script src="home7assets/js/jquery.sticky.js"></script>
	<script src="home7assets/js/smoothscroll.js"></script>
	<script src="home7assets/js/wow.min.js"></script>
    <script src="home7assets/js/jquery.easypiechart.js"></script>
    <script src="home7assets/js/waypoints.min.js"></script>
    <script src="home7assets/js/jquery.cbpQTRotator.js"></script>
	<script src="home7assets/js/custom.js"></script>
    
    
    

<script type = "text/javascript" >


$( document ).ready(function() {

	<!--- Fix heights to match max - add well class --->
    var heights = $(".vexpander").map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".vexpander").height(maxHeight);
	
	<!---
	/* ---------------------------------------------- /*
		 * E-mail validation
	/* ---------------------------------------------- */--->

	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};

	
	<!--- CONTACT FORM--->
	$('#contact-form').submit(function(e) {
		
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
			
		var infoName = $('#infoName').val();
		var infoContact = $('#infoContact').val();
		var infoMsg = $('#infoMsg').val();
		var response = $('#contact-form .ajax-response');
		
		if (( infoName== '' || infoContact == '') || (!isValidEmailAddress(infoContact) )) 
		{
			response.fadeIn(500);
			response.html('<i class="fa fa-warning"></i> Please provide your name and a valid conact email address and try again.');
		}
		
		else 
		{	
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data, textStatus, jqXHR) 
				{
					$('#contact-form .ajax-hidden').fadeOut(500);
					response.html("Message Sent. We will contact you asap. Thanks.").fadeIn(500);		
			
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					//if fails      
				}
			});
			
			e.preventDefault(); 
							
			
		}
		
		return false;
	});
			
			
			
});

</script>
    

</body>
</html>