<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Drip Marketing - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				

</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );				
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Drip Marketing Campaigns</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	A Drip marketing campaign is a communication strategy that sends, or "drips," a pre-scripted set of messages to customers or prospects over time. Exchange usefull information with your customers as a way to stay on their minds. Increased opportunities for up sell and cross sell. 
                    
                 </div>
                
                <img style="position:absolute; top:110px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/yourfuture.png" height="74" width="400" />
              
            </div>
        </div>
        
    <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Trickle Campaigns</h2>
                         <p> activley nurture and engage your customers</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:15px;">
                    <h3>
                        <h3>To help MasterCard develop a more engaging SMS campaign for underserved consumers, #BrandShort# has developed the following content to better provide relevant tips and guidance to respondents based on their unique characteristics. The content is designed to better understand who participants are and encouraging action-steps that would best meet their unique situations. Upon completion of the program, MasterCard will also be able to better assess the type of respondents using their program.</h3>
                        <BR />
                        <h3>Using EBM tools, this highly complex 4 week trickle campaign with over 342 control points was designed, built, tested, and launched in less than 2 weeks.</h3>
                    </h3>
                </div>     
                
                <BR />
                
                <img style="" src="#rootUrl#/#publicPath#/images/m1/mcprogram.png" />
                         
            </div>
        </div>
        
         <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Lead Nuturing</h2>
                         <p>marketing campaign with relevant messaging</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    	<h3>New leads are automatically enrolled into a drip marketing campaign with messaging relevant to the call-to-action from which the lead came</h3>
                        <BR />
                        <h3>Drip Marketing methods offer the ability to remain top-of-mind, and even prompt action, without jeopardizing the relationship, whereas persistent follow-up can become a deterrent to closing the sale.</h3>
                        
                    </h3>
                </div>     
                         
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	drip marketing constitutes an automated follow-up method that can augment or replace personal lead follow-up.
                
            </div>
        </div>            
               
        <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Samples</h2>
                         <p>How others are using it</p>
                    </header>
                </div>    
                                        
                   <div class="hiw_Info" style="width:960px; margin-top:50px;">
                    	
                            
                            <ul style="margin: 8px 0 0 30px;">
                            	<li>
                                	What to expect when you are expecting   
                                </li>
                                
                                <li>
                                	Motivational messages - Diet plans, work outs, finacial goals
                                </li>
                                                                
                                <li>
                                	Fun facts
                                </li>
                                
	                            <li>
                                	Qualification Questions
                                </li>
                                
                                <li>
                                	Preference
                                </li>
                               
                            </ul>
                            
                       
                        
                         <br/>
                    	
                    </div>
                         
            </div>
        </div>
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>


    <!---    
        <div id="inner-bg-alt" style="min-height: 380px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:380px;">
               
                                        
                    <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/singlechannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                    	<h2 class="super">SMS - Less intrusive, but more timely than eMail.</h2>
                        <br/>
                    	<h3>Using advance techniques like , a drip marketing campaign can tailor messages specific to a customer.</h3>
                        
                        <h3>While the SMS and eMail channels can provide links to more information and interactions,tThe Voice channel can react to touch tones or even natural launguage speech recognition directly.</h3>
                        
                         <h3>Reasons to still use just the Single Channel.</h3>	
                         <ul style="margin: 8px 0 0 30px;">
                            	
                                <li>
                                	Decreased Time & Expense.
                                </li>
                                
                                <li>
                                	Your Business May Prefer a Specific Channel.  
                                </li>
                                                                                                                               
                                <li>
                                	Less moving parts.
                                </li>
                                                                                             
                            </ul>                            
                            
                    </div>
                                                       
                             
            </div>
        </div>
        --->
     <!---
        
         <div id="inner-bg-alt" style="min-height: 450px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:440px;">
               
                                        
                    <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/multichannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                    	<h2 class="super">Lead Nurturing</h2>
                        <br/>
                    	                        
                        <h3>With minimal investment on the part of sales and marketing, lead nurturing has the potential to activley engage your customers up to and through the conversion stage. </h3>                        
                        
                        <h3>Keyword join, IVR Join, CPP, </h3>
                        
                         <h3>Sales Cycle</h3>	
                         <ul style="margin: 8px 0 0 30px;">
                            	<li>                                	
									Lead expresses interest.  
                                </li>
                                
                                <li>
                                	Escalations - If you dont get a response on one channel, send it to another channel.
                                </li>
                                                                
                                <li>
                                	Create Multiple Touch Points
                                </li>
                               
                                                                                          
                            </ul>
                            
                            
                        
                    </div>
                    
                                     
                             
            </div>
        </div>
     --->   
        
     <!---    <div id="inner-bg-alt" style="min-height: 550px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:550px;">
               
                                        
                    <img style="float:right;  margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:10px;">
                    	<h2 class="super">Sales cycle</h2>
                        <br/>                    	                      
                        
                       
                        <p>&nbsp;</p>
                        <h3>The Case for Calling Low</h3>
                        <p>When you hear the case for &ldquo;call high&rdquo; it intuitively feels obvious. I have met several sales execs who argue the exact opposite strategy. Their view is that without &ldquo;proof points&rdquo; the senior leadership teams are likely to be cynical about the benefits of your product.</p>
                        <p>In the &ldquo;call low&rdquo; camp they advocate</p>
                        <p>1. Find a business unit leader who would be positively impacted by success of your product</p>
                        <p>2. Run a short, quantifiable pilot</p>
                        <p>3. Have that business unit leader champion you to a senior exec with data and proof in hand</p>
                        <p>4. Land bigger deals with more assuredness.</p>
                        <p>The mantra of this school is, &ldquo;<a href="http://dictionary.reverso.net/english-cobuild/put%20one's%20head%20above%20the%20parapet/keep%20one's%20head%20below%20the%20parapet" target="_blank">keep your head below the parapet</a> and avoid getting shot. You can rise up once you have your armaments.&rdquo;</p>
                        <p>While I understand the logic, I personally believe you need to provide enough evidence through case studies to talk with the CEO and if she can&rsquo;t convince herself that it&rsquo;s worth exploring being a buyer then you could recommend you do a pilot internally to prove yourself.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>	
                        
                                              
                            
                        
                    </div>
                    
                                     
                             
            </div>
        </div>--->
                
      