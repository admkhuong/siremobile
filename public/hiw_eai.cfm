<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>How it Works - <cfoutput>#BrandShort#</cfoutput></title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<!---    <link href="home7assets/css/rotating-card.css" rel="stylesheet" />--->
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
        
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
 	<script src="home7assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="home7assets/js/jquery.parallax-1.1.3.js"></script>
	<script src="home7assets/js/imagesloaded.pkgd.js"></script>
	<script src="home7assets/js/jquery.sticky.js"></script>
	<script src="home7assets/js/smoothscroll.js"></script>
	<script src="home7assets/js/wow.min.js"></script>
    <script src="home7assets/js/jquery.easypiechart.js"></script>
    <script src="home7assets/js/waypoints.min.js"></script>
    <script src="home7assets/js/jquery.cbpQTRotator.js"></script>
	<script src="home7assets/js/custom.js"></script>
     
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
									
			.pfblock 
			{
				padding: 0;
			}

		</style>
        
    </cfoutput>
      
      

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>


<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
       
        
<style>

<!--- Override bootstrap --->
.EBMPresentationBody
{
	 min-width:1000px; // suppose you want minimun width of 1000px
   _width: expression( document.body.clientWidth > 1000 ? "1000px" : "auto" ); /* sets max-width for IE */
}

<!---body{
   min-width:1000px; // suppose you want minimun width of 1000px
   _width: expression( document.body.clientWidth > 1000 ? "1000px" : "auto" ); /* sets max-width for IE */
   overflow:scroll;

   
}--->

.row, .container
{
    min-width: 1200px !important;
}




.process-step-box 
{
  <!---background: #4679BD;--->
  
  background: #d0a577; /* Old browsers */
	
	/* IE9 SVG, needs conditional override of 'filter' to 'none' */
	
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNkMGE1NzciIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjYTA3NTQ3IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2MxOWU2NyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNhMDc1NDciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvcmFkaWFsR3JhZGllbnQ+CiAgPHJlY3QgeD0iLTUwIiB5PSItNTAiIHdpZHRoPSIxMDEiIGhlaWdodD0iMTAxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);	
	background: -moz-radial-gradient(center, ellipse cover,  #d0a577 0%, #a07547 100%, #c19e67 100%, #a07547 100%); /* FF3.6+ */	
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#d0a577), color-stop(100%,#a07547), color-stop(100%,#c19e67), color-stop(100%,#a07547)); /* Chrome,Safari4+ */
	background: -webkit-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* Chrome10+,Safari5.1+ */	
	background: -o-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* Opera 12+ */	
	background: -ms-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* IE10+ */	
	background: radial-gradient(ellipse at center,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* W3C */	
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d0a577', endColorstr='#a07547',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */


  text-align: center;
  vertical-align: middle;
  line-height: 40px;
  color: #fff;
  border-radius: 8px;
  border: #000 solid 3px;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.4);
}

.process-step-box-caption 
{
  <!---background: #4679BD;--->
  
  background: #4679BD;
  text-align: center;
  vertical-align: middle;
  line-height: 20px;
  color: #fff;
  border-radius: 8px;
  border: #000 solid 3px;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.4);
}

.hover:hover
{
	cursor:pointer;		
	box-shadow:none;
}

.EnterpriseObj, .ConsumerObj 
{
  background: #4679BD;
  text-align: center;
  vertical-align: middle;
  line-height: 20px;
  color: #fff;
  border-radius: 8px;
  border: #000 solid 3px;
}


.EBMPLatformObject
{
	padding-top:40px;
	left: 0;
	padding: 40px 0 0 0;
	position: relative;
	text-align: center;
	top: 0;
	transform: none;
	width: 100%;
	margin: 20px 0;
	
}


.circle {
  z-index:100;
  background: #4679BD;
  box-shadow: inset -35px -25px 40px rgba(0,0,0,.5);
  border-radius: 50%;
  height: 100px;
  width: 100px;
  opacity: 0.7;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.4);
}


.circleMed {
	z-index:100;		
  background: #4679BD;
  box-shadow: inset -25px -25px 40px rgba(0,0,0,.5);
  border-radius: 50%;
  height: 150px;
  width: 150px;
  opacity: 0.7;
  webkit-transform: translate(-25px, -25px);
   -moz-transform: translate(-25px, -25px);
	-ms-transform: translate(-25px, -25px);
	 -o-transform: translate(-25px, -25px);
		transform: translate(-25px, -25px);	
		box-shadow: 0 4px 4px rgba(0, 0, 0, 0.4);
}

.circle-responsive {
  background: #4679BD;
  box-shadow: inset -25px -25px 40px rgba(0,0,0,.5);
  border-radius: 50%;
  height: 0;
  padding-bottom: 100%;
  width: 100%;
}


.circle-content {
  color: #fff;
  float: left;
  line-height: 1;
  margin-top: -0.5em;
  padding-top: 50%;
  text-align: center;
  width: 100%;
}

#footer
{
	position:absolute;
	bottom:0px;	
	z-index: 800;	
	width: 100%;
	margin:0;
	padding: 2em;
}

#StageNavigation
{
	width: 100%;
	position:absolute;
	bottom:5px;	
	z-index: 1000;
	padding: 2em;
}


#StageNavigation button
{
	display:inline;
	margin-right: 30px;	
}

#StageHIW
{		
	background-repeat: repeat;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 0;
	background-color:#FFFFFF;
	
}

#TheConsumer, #TheEnterprise, #TheEBMPlatform, #ProcessSteps
{
	display:none;
	top:0px;
	left:0px;
}


.MinimizeEnterprise
{
	-ms-transform: scale(0.5,0.5); /* IE 9 */
    -webkit-transform: scale(0.5,0.5);  /* Safari */
	-moz-transform: scale(0.5,0.5); /* FireFox */
	-o-transform: scale(0.5,0.5); /* FireFox */
    transform: scale(0.5,0.5)		
}

.MinimizeConsumer
{
	-ms-transform: scale(0.5,0.5);  /* IE 9 */
    -webkit-transform: scale(0.5,0.5); /* Safari */
	-moz-transform: scale(0.5,0.5); /* FireFox */
	-o-transform: scale(0.5,0.5); /* FireFox */
    transform: scale(0.5,0.5);		
}

.CircleShiftContainer
{
	position: absolute;	
	height: 300px;
	padding-top: 20px;
	
	-webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
	
}

.CircleShift 
{
	position: absolute;
	left:50%;
    margin-left:-75px;			
}

.CircleShiftMaster
{	
	z-index:500;	
}

.CircleShift1 
{
	z-index:101;
	 webkit-transform: translate(0, -100px);
	   -moz-transform: translate(0, -100px);
		-ms-transform: translate(0, -100px);
		 -o-transform: translate(0, -100px);
			transform: translate(0, -100px);			
}

.CircleShift2 
{		
	z-index:102;
	-webkit-transform: translate(70px, -70px);
	   -moz-transform: translate(70px, -70px);
		-ms-transform: translate(70px, -70px);
		 -o-transform: translate(70px, -70px);
			transform: translate(70px, -70px);
}

.CircleShift3 
{
	z-index:103;
	-webkit-transform: translate(90px, -35px);
	   -moz-transform: translate(90px, -35px);
		-ms-transform: translate(90px, -35px);
		 -o-transform: translate(90px, -35px);
			transform: translate(90px, -35px);	
}	

.CircleShift4 
{
	z-index:104;
	-webkit-transform: translate(90px, 35px);
	   -moz-transform: translate(90px, 35px);
		-ms-transform: translate(90px, 35px);
		 -o-transform: translate(90px, 35px);
			transform: translate(90px, 35px);				
}	

.CircleShift5 
{
	z-index:105;
	-webkit-transform: translate(70px, 70px);
	   -moz-transform: translate(70px, 70px);
		-ms-transform: translate(70px, 70px);
		 -o-transform: translate(70px, 70px);
			transform: translate(70px, 70px);		
}	

.CircleShift6 
{
	z-index:106;
	-webkit-transform: translate(0, 100px);
	   -moz-transform: translate(0, 100px);
		-ms-transform: translate(0, 100px);
		 -o-transform: translate(0, 100px);
			transform: translate(0, 100px);		
}	

.CircleShift7 
{
	z-index:107;
	-webkit-transform: translate(-70px, 70px);
	   -moz-transform: translate(-70px, 70px);
		-ms-transform: translate(-70px, 70px);
		 -o-transform: translate(-70px, 70px);
			transform: translate(-70px, 70px);	
}	

.CircleShift8 
{
	z-index:108;
	-webkit-transform: translate(-90px, 35px);
	   -moz-transform: translate(-90px, 35px);
		-ms-transform: translate(-90px, 35px);
		 -o-transform: translate(-90px, 35px);
			transform: translate(-90px, 35px);		
}

.CircleShift9 
{
	z-index:109;
	-webkit-transform: translate(-90px, -35px);
	   -moz-transform: translate(-90px, -35px);
		-ms-transform: translate(-90px, -35px);
		 -o-transform: translate(-90px, -35px);
			transform: translate(-90px, -35px);	
}	

.CircleShift10 
{
	
	z-index:110;
	-webkit-transform: translate(-70px, -70px);
	   -moz-transform: translate(-70px, -70px);
		-ms-transform: translate(-70px, -70px);
		 -o-transform: translate(-70px, -70px);
			transform: translate(-70px, -70px);	
}


div[alt]:hover:after {
  content: attr(alt);
  padding: 4px 8px;
  color: #333;
  position: absolute;
  left: 0;
  top: 100%;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-box-shadow: 0px 0px 4px #222;
  -webkit-box-shadow: 0px 0px 4px #222;
  box-shadow: 0px 0px 4px #222;
  background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));
  background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
}

.EBMPresentationContainer
{
	position: absolute;	

	-webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
	
}

#DataTransferRight, #DataTransferLeft
{
	text-align:center;	
	line-height:40px;
	opacity: 0.1;
}

<!--- Data transfer animations --->
.line {
    display: inline-block;
    width: 15px;
    height: 15px;
    border-radius: 15px;
    background-color: #4679BD;
}

.EBMTransferAnimations
{
	padding-top:60px;	
	display:none;	
}

.DataTransferAnimationContainer .line:nth-last-child(1) {animation: loadingB 1.5s 1s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(2) {animation: loadingB 1.5s .5s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(3) {animation: loadingB 1.5s 0s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(4) {animation: loadingB 1.5s 1s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(5) {animation: loadingB 1.5s .5s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(6) {animation: loadingB 1.5s 0s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(7) {animation: loadingB 1.5s 1s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(8) {animation: loadingB 1.5s .5s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(9) {animation: loadingB 1.5s 0s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(10) {animation: loadingB 1.5s 1s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(11) {animation: loadingB 1.5s .5s infinite;}
.DataTransferAnimationContainer .line:nth-last-child(12) {animation: loadingB 1.5s 0s infinite;}

@keyframes loadingB {
    0 {width: 15px;}
    50% {width: 45%;}
    100% {width: 15px;}
}



<!--- Slide show style --->

.carousel-control.left, .carousel-control.right 
{
	background: none !important;	
}

.carousel-control 
{
	color:#000 !important;
}

.carousel-indicators .active {
    background-color: #333;
    height: 12px;
    margin: 0;
    width: 12px;
}

.carousel-indicators li {
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid #111;
    border-radius: 10px;
    cursor: pointer;
    display: inline-block;
    height: 10px;
    margin: 15px 1px 1px 1px;
    text-indent: -999px;
    width: 10px;
}

.carousel-caption {
    bottom: 30px;  
	opacity: .3; 
}

<!---/* Fade transition for carousel items */
.carousel .item {
    left: 0 !important;
      -webkit-transition: opacity .4s; /*adjust timing here */
         -moz-transition: opacity .4s;
           -o-transition: opacity .4s;
              transition: opacity .4s;
}
.carousel-control {
    background-image: none !important; /* remove background gradients on controls */
}
/* Fade controls with items */function()
.next.left,
.prev.right {
    opacity: 1;
    z-index: 1;
}
.active.left,
.active.right {
    opacity: 0;
    z-index: 2;
}--->

.carousel-content {
    color:black;
   	height: 640px;
    padding-left: 20%;
    padding-right: 20%;	
	padding-top:2em;
}

#EventCarousel .item
{
	min-height: 600px;
}

.EBMPresentationCarousel
{
	margin-top:220px;
	display:none;	
}

#NextSlide, #PrevSlide
{
	font-size: 30px;
    height: 30px;
    margin-top: -15px;
    width: 30px;
	
}

#FinalSlide
{
	font-size: 30px;
    height: 18px;
    margin-top: -16px;
    width: 70px;
	text-align:center;
	
}

#FirstSlide
{
	font-size: 30px;
    height: 18px;
    margin-top: 0px;
    width: 70px;
	text-align:center;
	
}

#NextSlide:hover, #PrevSlide:hover, #FinalSlide:hover, #FirstSlide:hover
{
	cursor:pointer;	
}


.Step1
{
	background-image: URL('home7assets/images/item-1.jpg');
	background-size: 100% auto;
}

.Step2
{
	background-image: URL('home7assets/images/item-3.jpg');
	background-size: 100% auto;
}

.Step3
{
	background-image: URL('home7assets/images/item-4.jpg');
	background-size: 100% auto;
}

.Step4
{
	background-image: URL('home7assets/images/item-2.jpg');
	background-size: 100% auto;
}

</style>

    <script type="text/javascript" language="javascript">
		
		
		<!--- Initialize the step counter at 0--->
		var CurrentStep = 0;
		
		<!---  Dont let the step counter go nuts --->
		var MaxStep = 8;
		
		var ActiveCarousel = $('#EventCarousel');
		
		<!--- Custom jQuery methods to allow centering of absolutely positioned objects --->		
		jQuery.fn.centerStage = function () {
			this.css("position","absolute");
			this.css("top", Math.max(0, (($("#StageHIW").height() - $(this).outerHeight()) / 2) + $("#StageHIW").scrollTop()) + "px");
			this.css("left", Math.max(0, (($("#StageHIW").width() - $(this).outerWidth()) / 2) + $("#StageHIW").scrollLeft()) + "px");
			return this;
		}
				
		$(document).ready(function(){
								
			$('#EventCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#IntelligenceCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#FulfillmentCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#ResultsCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#EnterpriseCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#ConsumerCarousel').carousel({
				pause: true,
				interval: false
			});

			$('#PlatformCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#PlatformCarousel').carousel({
				pause: true,
				interval: false
			});
			
			$('#APICarousel').carousel({
				pause: true,
				interval: false
			});

			$('#AgentToolsCarousel').carousel({
				pause: true,
				interval: false
			});

			$('#EventBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#EventCarousel').carousel(0);
					$('#TheEvent').show();	
					ActiveCarousel = $('#EventCarousel');				
					
				}
				
			});
			
			$('#IntelligenceBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#IntelligenceCarousel').carousel(0);
					$('#TheIntelligence').show();		
					ActiveCarousel = $('#IntelligenceCarousel');			
					
				}
				
			});
			
			$('#FulfillmentBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#FulfillmentCarousel').carousel(0);
					$('#TheFulfillment').show();	
					ActiveCarousel = $('#FulfillmentCarousel');				
					
				}
				
			});
			
			$('#ResultsBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#ResultsCarousel').carousel(0);
					$('#TheResults').show();	
					ActiveCarousel = $('#ResultsCarousel');				
					
				}
				
			});
			
			$('#TheEnterprise').click(function(){
						
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 && parseInt($('#ProcessSteps').position().top) > 0)
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#EnterpriseCarousel').carousel(0);
					$('#TheEnterpriseContainer').show();
					ActiveCarousel = $('#EnterpriseCarousel');					
					
				}
				else
				{
					
					if(CurrentStep == 1)
					{
							
						$('#NextSlide').trigger( "click" );	
						
						<!--- Hide any existing carousels --->	
						$('.EBMPresentationCarousel').hide();
							
						$('#EnterpriseCarousel').carousel(0);
						$('#TheEnterpriseContainer').show();
						ActiveCarousel = $('#EnterpriseCarousel');		
					}
					else
					{
						if(CurrentStep == 2)
						{					
						
							<!--- Hide any existing carousels --->	
							$('.EBMPresentationCarousel').hide();
					
							$('#EnterpriseCarousel').carousel(0);
							$('#TheEnterpriseContainer').show();	
							ActiveCarousel = $('#EnterpriseCarousel');		
						}
					}
					
					
				}
				
				
			});
			
			$('#TheConsumer').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250  && parseInt($('#ProcessSteps').position().top) > 0 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#ConsumerCarousel').carousel(0);
					$('#TheConsumerContainer').show();	
					ActiveCarousel = $('#ConsumerCarousel');				
					
				}
				else
				{
					
					if(CurrentStep == 3)
					{
							
						$('#NextSlide').trigger( "click" );	
						
						<!--- Hide any existing carousels --->	
						$('.EBMPresentationCarousel').hide();
							
						$('#ConsumerCarousel').carousel(0);
						$('#TheConsumerContainer').show();	
						ActiveCarousel = $('#ConsumerCarousel');	
					}
					else
					{
						if(CurrentStep == 4)
						{					
						
							<!--- Hide any existing carousels --->	
							$('.EBMPresentationCarousel').hide();
					
							$('#ConsumerCarousel').carousel(0);
							$('#TheConsumerContainer').show();	
							ActiveCarousel = $('#ConsumerCarousel');		
						}
					}
					
					
				}
				
			});
			
			$('#TheEBMPlatform').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#PlatformCarousel').carousel(0);
					$('#ThePlatform').show();		
					ActiveCarousel = $('#PlatformCarousel');			
					
				}
				
			});		
			
			$('#APIBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#APICarousel').carousel(0);
					$('#TheAPI').show();	
					ActiveCarousel = $('#APICarousel');				
					
				}
				
			});
			
			$('#CPPBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#CPPCarousel').carousel(0);
					$('#TheCPP').show();	
					ActiveCarousel = $('#CPPCarousel');				
					
				}
				
			});			
			
			$('#AgentToolsBox').click(function(){
	
				<!--- Only enable click if position is at top --->
				if( parseInt($('#ProcessSteps').position().top) < 250 )
				{
					<!--- Hide any existing carousels --->	
					$('.EBMPresentationCarousel').hide();
					
					$('#AgentToolsCarousel').carousel(0);
					$('#TheAgentTools').show();	
					ActiveCarousel = $('#AgentToolsCarousel');				
					
				}
				
			});				
			
			<!--- Play out animation to the end --->			
			$('#FinalSlide').click(function(){
				
				PlayToStep(MaxStep);
			});
			
			<!--- Rewind animation to the beginning --->			
			$('#FirstSlide').click(function(){
				
				PlayToStep(0);
			});
			
			
			
			$('#NextSlide').click(function(){
           		
				<!--- Dont let the step counter go nuts --->		
				if(CurrentStep >= MaxStep)
					return false;
					
				<!--- Hide any existing carousels --->	
				$('.EBMPresentationCarousel').hide();
				
				CurrentStep++;
												
				switch(CurrentStep)
				{
					
					case 1:					
						$('#TheEnterprise').centerStage();
						$('#TheEnterprise').show();
						break;
						
					case 2:					
						$('#TheEnterprise').addClass('MinimizeEnterprise');		
						$('#TheEnterprise').css({ left: "100px", top: "0px" });
						break;
					
					case 3:
						$('#TheConsumer').centerStage();
						$('#TheConsumer').show();			
						break;
						
					case 4:
						$('#TheConsumer').addClass('MinimizeConsumer');	
						$('#TheConsumer').css({ left: Math.max(0, (($("#StageHIW").width() - 80)) + $("#StageHIW").scrollLeft()) + "px", top: "0px" });		
						break;	
						
					case 5:					
						$('#TheEBMPlatform').centerStage();
						$('#TheEBMPlatform').show();
						break;
						
					case 6:					
						$('#TheEBMPlatform').css({ top: "0px" });
						$('.EBMTransferAnimations').show();
						break;	
						
					case 7:					
						$('#ProcessSteps').centerStage();
						$('#ProcessSteps').show();
						break;		
					
					case 8:					
						$('#ProcessSteps').css({ top: "160px" });						
						break;		
						
					default:
						break;
						
					
				}
				
			});
				
			$('#PrevSlide').click(function(){
           				

				<!--- Dont let the step counter go nuts --->		
				if(CurrentStep <= 0)
					return false;
				
				<!--- Hide any existing carousels --->	
				$('.EBMPresentationCarousel').hide();
																			
				switch(CurrentStep)
				{
					
					case 1:					
						$('#TheEnterprise').hide();
						break;
						
					case 2:					
						$('#TheEnterprise').removeClass('MinimizeEnterprise');	
						$('#TheEnterprise').centerStage();							
						break;
					
					case 3:
						$('#TheConsumer').hide();			
						break;
						
					case 4:
						$('#TheConsumer').removeClass('MinimizeConsumer');	
						$('#TheConsumer').centerStage();							
						break;	
						
					case 5:					
						$('#TheEBMPlatform').hide();
						break;
						
					case 6:					
						$('.EBMTransferAnimations').hide();
						$('#TheEBMPlatform').centerStage();						
						break;
					
					case 7:					
						$('#ProcessSteps').hide();
						break;
					
					case 8:					
						$('#ProcessSteps').centerStage();						
						break;
								
					default:
						break;
						
					
				}
				
				<!--- Decrement Current Step --->
				CurrentStep--;
				
			});
						
			
			$(document).keydown(function(e) {
				switch(e.which) {
					case 37: <!---// left--->
						
						if(CurrentStep >= MaxStep)
						{
							<!--- Find active carousel and move next --->							
							ActiveCarousel.carousel('prev');							
							
						}
						else
						{
							$('#PrevSlide').trigger( "click" );							
						}
						
						break;
			
					case 38: <!---// up--->
						PlayToStep(0);
						break;
			
					case 39: <!---// right--->
						
						if(CurrentStep >= MaxStep)
						{
							<!--- Find active carousel and move next --->							
							ActiveCarousel.carousel('next');							
							
						}
						else
						{
							$('#NextSlide').trigger( "click" );							
						}
						
						break;
			
					case 40: <!---// down--->
						PlayToStep(MaxStep);
						break;
			
					default: return; // exit this handler for other keys
				}
				e.preventDefault(); // prevent the default action (scroll / move caret)
			});
			
						
			PlayToStep(MaxStep);
				
		});
			
			
			
		function rotateCard(btn){
			var $card = $(btn).closest('.card-container');
			<!---console.log($card);--->
			if($card.hasClass('hover')){
				$card.removeClass('hover');
			} else {
				$card.addClass('hover');
			}
		}
		
		
		function PlayToStep(inpStep)
		{
			
			<!--- Dont play beyond max/min steps --->
			if(inpStep > MaxStep || inpStep < 0)
				return false;
			
			
			if(inpStep < CurrentStep)
			{
				while(inpStep < CurrentStep)
				{
					$('#PrevSlide').trigger( "click" );					
				}
				
			}
			
			
			if(inpStep > CurrentStep)
			{
				while(inpStep > CurrentStep)
				{
					$('#NextSlide').trigger( "click" );					
				}
				
			}
						
		}
				
		
		function MinimizeEnterprise()
		{
			$('#TheEnterprise').addClass('MinimizeEnterprise');
		}
		
		function MaximizeEnterprise()
		{
			$('#TheEnterprise').removeClass('MinimizeEnterprise');
		}
		
		
		
		
		
	
	
	</script>
</head>
            
<cfoutput>
<body class="pfblock-gray">
        
        
   
  	<section class="pfblock pfblock-gray screen-height">
    
    	<div id="StageNavigation">
                    
            <div id="FirstSlide">
	            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
    	        <span class="sr-only">First</span>
            </div>   
            
            <span id="PrevSlide">
	            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    	        <span class="sr-only">Previous</span>
            </span>            
            
            <span id="NextSlide">
	             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                 <span class="sr-only">Next</span>
            </span>    
            
             <div id="FinalSlide">
	            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
    	        <span class="sr-only">Final</span>
            </div>  
             
            
        </div>
    
                
                <div id="StageHIW" class="" <!---class="container"---> >
        
        
                     <div id="TheEnterprise" class="CircleShiftContainer">                             
                                                  
                        <div class="CircleShift CircleShift1 bounceIn">
        
                            <div class="circle hover" alt2="Health You can almost never go wrong with the health industry--who wouldn't want to be fitter, faster, and stronger, after all? At $21.8 billion, this industry ranks high in terms of total revenue.">
                                
                                    <div class="circle-content">Healthcare</div>
                                
                            </div>
        
                        </div>
        
                        <div class="CircleShift CircleShift2 bounceIn">
        
                            <div class="circle hover" alt2="11.87 Billion dollars spent in advertsing revenue on social media last year.">
                                
                                    <div class="circle-content">Social Media</div>
                                                            
                            </div>
        
                        </div>                    
        
                        <div class="CircleShift CircleShift3 bounceIn">
        
                            <div class="circle hover" alt2="Business Products and Services The business products and services industry takes home the bronze medal this year with a combined yearly revenue of $18 billion.">
                                
                                    <div class="circle-content">B 2 B</div>
                                
                            </div>
        
                        </div>    
        
                        <div class="CircleShift CircleShift4 bounceIn">
        
                            <div class="circle hover" alt2="The energy industry grew by 168 percent between 2010 and 2013 and brought in $17.5 billion in combined revenue last year.">
                                
                                    <div class="circle-content">Utilities</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift5 bounceIn">
        
                            <div class="circle hover" alt2="Consumer Products and Services Consumer products and services is the fastest-growing industry on the Inc. 5000, but ranks only eighth among the 10 biggest industries. But the $10.7 billion in revenue that the sector reeled in last year isn't anything to sniff at.">
                                
                                    <div class="circle-content">Consumer Products</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift6 bounceIn">
        
                            <div class="circle hover" alt2="Entertainment is big business in the United States. It is expected, that the sector as a whole will have generated 564 billion U.S. dollars in revenue this year.">
                                
                                    <div class="circle-content">Entertainment</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift7 bounceIn">
        
                            <div class="circle hover" alt2="Financial Services Money management and financial services firms don't just handle money, they're in the money too. All the financial services companies on the Inc. 5000 grew 112 percent, with revenues of $17.2 billion.">
                                
                                    <div class="circle-content">Finance</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift8 bounceIn">
        
                            <div class="circle hover" alt2="Human Resources Human Resources is in charge of many vital functions for growing companies--especially recruiting, hiring, and benefits administration.">
                                
                                    <div class="circle-content">Human Resources</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift9 bounceIn">
        
                            <div class="circle hover" alt2="Logistics and Transportation Think about why Uber is so successful--it removes the annoying logistics from hailing a taxi. It's the same reason drones are causing so much excitement in the delivery space. Companies that reduce friction or remove the entire task in the logistics and transportation industry make a killing and are the reason the sector made a collective $11.1 billion last year.">
                                
                                    <div class="circle-content">Logistics</div>
                                                            
                            </div>
        
                        </div>
                        
                        <div class="CircleShift CircleShift10 bounceIn">
        
                            <div class="circle hover" alt2="Telecommunications Telecommunications is the 10th most profitable industry on the Inc. 5000. In total, all the telecom companies on this year's list brought in $9.4 billion in revenue.">
                                
                                    <div class="circle-content">Telecom</div>
                                                            
                            </div>
        
                        </div>
                                                                 
                        <div class="CircleShift CircleShiftMaster bounceIn">
        
                            <div class="circleMed hover">
                                
                                    <div class="circle-content">Target Market</div>
                                                            
                            </div>
        
                        </div>
        
                </div> <!--- container Enterprise --->
                
                <div id="TheConsumer" class="CircleShiftContainer">
                         
                                              
                    <div class="CircleShift CircleShift1 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">SMS</div>
                            
                        </div>
    
                    </div>
    
      				<div class="CircleShift CircleShift2 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Telephone</div>
                                                        
                        </div>
    
                    </div>                    
    
                    <div class="CircleShift CircleShift3 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Fax</div>
                            
                        </div>
    
                    </div>    
    
                    <div class="CircleShift CircleShift4 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">eMail</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift5 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Social Media</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift6 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Print</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift7 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Agents</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift8 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Chat</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift9 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Banners</div>
                                                        
                        </div>
    
                    </div>
                    
                    <div class="CircleShift CircleShift10 bounceIn">
    
                        <div class="circle hover">
                            
                                <div class="circle-content">Surveys</div>
                                                        
                        </div>
    
                    </div>
                    
                                     
                    <div class="CircleShift CircleShiftMaster bounceIn">
    
                        <div class="circleMed hover">
                            
                                <div class="circle-content">The Consumer</div>
                                                        
                        </div>
    
                    </div>
    
                </div>	<!--- container consumer --->
                
                <div id="TheEBMPlatform" class="EBMPresentationContainer col-md-8">
                                
                    <div class="row">
                    
                    	<div id="DataTransferLeft" class="col-md-4">
                    
                        	<div class="DataTransferAnimationContainer EBMTransferAnimations">
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                                <BR />
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                              <!---  <BR />
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>--->
                            </div>
                        
                        </div>  
                    
                    
                    	<div class="col-md-4">
                    
                            <div class="EBMPLatformObject">
                    
                                    <cfoutput>
                                        <img src="#LogoImageLink7#" style="border:none;" />
                                    </cfoutput>
                                            
                                <h4>Event Based Messaging Platform</h4>
                                
                            </div>
                        
                        </div>
                        
                        <div id="DataTransferRight" class="col-md-4">
                    
                            <div class="DataTransferAnimationContainer EBMTransferAnimations">
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                                <BR />
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                               <!--- <BR />
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>--->
                            </div>
                        
                        </div>
                
                 	</div>
                    
                </div>
               
               
                <div id="ProcessSteps" class="EBMPresentationContainer col-md-8"> 
                 
                    <div class="row">
                
                        <div class="col-md-3 pfblock-header wow fadeInUp animated">
                            <div id="EventBox" class="process-step-box Step1 hover">Lifecycle Events</div>                    
                        </div>
                        
                        <div class="col-md-3 pfblock-header wow fadeInUp animated">
                            <div id="IntelligenceBox" class="process-step-box Step2 hover">Intelligence</div>                    
                        </div>
                        
                        <div class="col-md-3 pfblock-header wow fadeInUp animated">
                            <div id="FulfillmentBox" class="process-step-box Step3 hover">Fulfillment</div>                    
                        </div>
                        
                        <div class="col-md-3 pfblock-header wow fadeInUp animated">
                            <div id="ResultsBox" class="process-step-box Step4 hover">Results</div>                    
                        </div>                
                      
                    </div>

               	</div>
                


				<!--- Include detail carousels in separte files for easier maintenance --->
                
                <!--- Try adding some animations ... --->
               	<cfinclude template="home7assets/inc/car_event.cfm">
               	<cfinclude template="home7assets/inc/car_intelligence.cfm">
               	<cfinclude template="home7assets/inc/car_fulfillment.cfm">
               	<cfinclude template="home7assets/inc/car_results.cfm">
                <cfinclude template="home7assets/inc/car_enterprise.cfm">
                <cfinclude template="home7assets/inc/car_consumer.cfm">
                <cfinclude template="home7assets/inc/car_platform.cfm">
                <cfinclude template="home7assets/inc/car_api.cfm">
                <cfinclude template="home7assets/inc/car_cpp.cfm">
                <cfinclude template="home7assets/inc/car_agenttools.cfm">
                              
                
               
                
    	</div>
        
        
        <div id="footer" class="row">

				<div class="col-sm-12">

										
					<p class="heart">
                                                       
                       <cfoutput>
                               <a class="" href="home"> <img src="#LogoImageLink7#" style="border:none; height:30px; display:inline; margin-right:10px;" /></a>
                       </cfoutput>
                        The Event Based Messaging Platform
                        
                       
                    </p>
                    <p class="copyright" rel1="ReactionX, LLC">
                        All software, systems, and content is &copy; 2000, 2015 <!---| Images: <a href="https://unsplash.com/">Unsplash</a> & <a href="http://zoomwalls.com/">Zoomwalls</a>--->
					</p>

				</div>

			</div><!-- .row -->
            
                      
    </section>
    
    
    </body>
</cfoutput>
</html>


