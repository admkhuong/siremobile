<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EAI - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">            	
                                                    
                <div class="inner-main-hd" style="width:500px; margin-top:30px;">Enterprise Application Integration</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content" style="width:560px;">
                    Enterprise application integration is a business need to make diverse applications in an enterprise, including partner systems,  communicate to each other to achieve a business objective in a seamless reliable fashion irrespective of platform and geographical location of these applications. #BrandShort# EAI comprises differing components to support message acceptance, transformation, translation, routing, message delivery and business process management.              	                
                </div>
           
            	<img style="position:absolute; top:0px; right:10px; " src="#rootUrl#/#publicPath#/images/m1/eaitower2.png" />
              
            </div>
        </div>
              
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>EAI and EMS Campaign Flow</h2>
                         <p>Deep intergations into new and legacy systems</p>
                    </header>
                </div>    
                
               <h3>When designing a messaging application, #BrandShort# helps you know where to put what types of data to share in the message customization / business rules, and likewise where to look for what types of data coming from other applications. </h3>
                                        
               <div id="inner-right-img" style="margin:0px;"> <img src="#rootUrl#/#publicPath#/images/m1/genericcampaignflow.png" width="960" height="720"/></div>
                             
            </div>
        </div>
        
                          
		<div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Data Transfer Options</h2>
                         <p>data transfer mechanisms to suit your need</p>
                    </header>
                </div>    
                                        
                <div id="content" style="min-height:36px; text-align:center;" >
                    <div class="inner-txt-full" style="margin-top:10px; margin-bottom:20px; text-align:center; float:none;"><p>We can re-use and repurpose existing assets, or start new data transfer mechanisms to suit your needs.</p> </div>
                </div>
                
                 <div class="inner-txt-full" style="margin-bottom:20px; margin-top:20px; text-align:center; float:none;">
                        <ul style="text-align:left; list-style-type: none; width:450px; display: inline-block;">                            
                            <li>SFTP</li>                            
                            <li>HTTP/HTTPS - via REST API's</li>
                            <li>HTTP/HTTPS - via form or request variables</li>
                            <li>HTTP/HTTPS - via direct XML</li>
                            <li>HTTP/HTTPS - via SOAP</li>
                            <li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
                            <li>email</li>
                            <li>email with password protected zip files</li>
                            <li>email with PGP encrypted file</li>
                            <li>Direct VPN Tunnel</li>
                            <li>Dial up to secure server</li>
                            <li>DAT/CD/media via courier</li>
                            <li>FAX</li>
                        </ul>   
                        
                       
                                        
                </div>
                    
                             
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omnichannel engagement
                
            </div>
        </div>            
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                                   
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>EAI Campaign Templates</h2>
                        <p>#BrandShort# Builds and Hosts the EAI Campaign Templates</p>
                    </header>
                </div>                    
                                        
                <div id="inner-box-left" align="center" style="margin-left:60px;">          
                    
                    <div class="inner-txt" style="margin-top:25px !important; width:900px;">                    	
                    
                        <p>Messaging templates are hosted in the #BrandShort# environment and are built around libraries of message components and business rules.</p>
                        
                        <BR />
                        <BR />
                                       
                 		<h2>Messages are created from one or more optional basic components.</h2>
 						<ul>                            
                            <li>Text</li>
                            <li>Voice Libraries - Not just TTS but custom concatenated speech libraries to give it a more personalized touch.</li>
                            <li>HTML</li>
                            <li>Images</li>  
                            <li>URL Links</li>                                                                                                           
                        </ul>
                        
                        <BR />
                        <BR />
                        
		                <h2>Templates are built using scripting that is compiled into java byte code for performance. </h2>
                  		<div> <img src="#rootUrl#/#publicPath#/images/m1/sampletemplate.png" width="800" height="417"/></div>
                	  
                  	</div>                
                
                </div>                   
                             
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Business Rules</h2>
                        <p>Rules Based Messaging and Reactive Event Condition Actions</p>
                    </header>
                </div>    
                
 				<div id="inner-left-img"> <img src="#rootUrl#/#publicPath#/images/hiw/personalizedii.png" width="241" height="337"/></div>
               
               	<div id="inner-box-left" align="center" style="margin-left:60px; ">
                           
                    <div class="inner-txt" style="margin-top:0 !important;">                    	
                        <p>Documenting and managing business rules to ensure that they don't conflict is one of our key enterprise class services. When carefully managed, EAI can be used to help an organization to better achieve goals, remove obstacles to market growth, reduce costly mistakes, improve communication, comply with legal requirements, and increase customer loyalty.</p>
                        
                        <BR />
                        <BR />
                        
                        <p>Rules are applied based on triggers either containing the data elements needed to customize message and/or the message template has built in rules to retrieve necessary data. The triggers can be either real time or batch.</p>
                        
                        <BR />
                        <BR />
                                                
                        <p>Inference Rules - IF some-condition THEN allow/block message/content</p>
                         <ul>                            
                            <li>API Results</li>
                            <li>DB Results</li>
                            <li>Messaging History</li>                          
                            <li>Personalize Content for Improved Customer Experience</li>
                        </ul>
                                                                                          	
                        <BR />
                        <BR />
                       
                        <p>Reactive Event Condition Actions:</p>
                        <ul>
                            <li>Automated Notifications</li>
                            <li>Halt/Throttle on Thresholds</li>                                                     
                        </ul>
                    	  
                  	</div>
                                   
                 
                 	<div class="learnmore" ><a href="#rootUrl#/#publicPath#/hiw_rulessample?inpSN=1">Sample 1 - Newsletter &gt;</a></div>
                    <BR />
                    <div class="learnmore" ><a href="#rootUrl#/#publicPath#/hiw_rulessample?inpSN=2">Sample 2 - Account Monitoring &gt;</a></div>
                    <BR />
                    <div class="learnmore" ><a href="#rootUrl#/#publicPath#/hiw_rulessample?inpSN=3">Sample 3 - High Bill Inquiry &gt;</a></div>
                    <BR />
                    <div class="learnmore" ><a href="#rootUrl#/#publicPath#/hiw_rulessample?inpSN=4">Sample 4 - Payment Extension &gt;</a></div>
                 	<BR />
                    <div class="learnmore" ><a href="#rootUrl#/#publicPath#/hiw_rulessample?inpSN=5">Sample 5 - ENBREL Insurance Reverification &gt;</a></div>
                 
                                   
                </div>                         
                                 
            </div>
                    
        </div>
               
        <div id="inner-bg-m1" style="padding-top:25px; padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>SaaS and PaaS</h2>
                         <p>Software As A Service (SaaS) and Platform as a Service (PaaS)</p>
                    </header>
                </div>    
                                        
                  <div class="hiw_Info" style="width:900px; margin-top:25px;">
                    	<br/>
                    	<h3>                        
                        	SaaS is very similar to the old thin-client model of software provisioning, where clients, in this case usually web browsers, provide the point of access to software running on servers. Among the most familiar SaaS applications for business are customer relationship management applications like Salesforce, productivity software suites like Google Apps, and storage solutions brothers like Box and Dropbox. Well, #BrandShort# is your SaaS Tool for messaging for both simple and complex programs.
                        </h3>
                        
                        <br/>
                        
                        <h3>
	                        PaaS functions at a lower level than SaaS, typically providing a platform on which software can be developed and deployed. PaaS providers abstract much of the work of dealing with servers and give clients an environment in which the operating system and server software, as well as the underlying server hardware and network infrastructure are taken care of, leaving users free to focus on the business side of scalability, and the application development of their product or service.
                        	Use #BrandShort#'s APIs for access to the underlying platform.
                        </h3>
                        
                    </div>
                         
            </div>
        </div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Interactive, Reactive, Proactive
                
            </div>
        </div>   
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Interactive, Reactive, Proactive</h2>
                         <p>tools and strucures for Branching, Business Rules and Interactive Response</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>While mail merge is still an important tool, #BrandShort# picks up where others leave off. #BrandShort# provides tools and strucures for Branching, Business Rules and Interactive Response as well.</h3>
                    
                  
                    <ul>
                		<li><b>Mediation</b> (intra-communication) - The #BrandShort# system acts as the go-between or broker between multiple applications. Whenever an interesting event occurs in an application (for instance, new information is created or a new transaction completed) an integration module in the #BrandShort# system is notified. The module then propagates the changes to other relevant applications.</li>
						<li><b>Federation</b> (inter-communication) - In this case, the #BrandShort# system acts as the overarching facade across multiple applications. All event calls from the 'outside world' to any of the applications are front-ended by the #BrandShort# system. The #BrandShort# system is configured to expose only the relevant information and interfaces of the underlying applications to the outside world, and performs all interactions with the underlying applications on behalf of the requester.</li>
                		<li><b>Relevance</b> - Branch to alternate message content based on personalized transactional data details.</li>
                        <li><b>Buisness Rules</b> - Apply business rules to modify or block messaging based on recent or upcoming events.</li>
                        <li><b>Data Integration</b> - Tools to ensures that information in multiple systems is kept consistent. </li>                        
                        <li><b>Content Management</b> - Tools to help manage complex message design and modification.</li>
                    </ul>      
                                     
              	</div>
                             
            </div>
        </div>
              
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

