<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMS - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
			
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">EMS</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	e-Messaging Services - Fast, Simple, Easy.<BR/>
                    #BrandShort# makes even the basic messaging stuff easier.             	                
	            </div>
                
                <img style="position:absolute; top:0px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/emsrabbitweb.png" height="333" width="400" /> 
              
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Quick Content Creation</h2>
                         <p>Online Recording and Editing Tools</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:15px; text-align:center;">
                    <h3>
                    </h3>
                    <img style="float:none;" src="#rootUrl#/#publicPath#/images/m1/emssample3.png" />
                    
                </div>     
              
                             
            </div>
        </div>
                          
		<div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>e-Messaging Services</h2>
                         <p>Three easy steps</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3></h3>
                
                    <ul style="margin: 8px 0 0 30px;">
                        <li>Choose your targets</li>
                        <li>Compose your message</li>
                        <li>Send</li>                        
                    </ul>      
                                     
              	</div>
                             
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	quickly send messages to all your group members
                
            </div>
        </div>            
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Quick Launch Option</h2>
                         <p>When you just need to get the message out and don't have resources or time for EAI</p>
                    </header>
                </div>    
                 
                <div class="hiw_Info" style="width:960px;  margin-top:15px; text-align:center;">
                    <img style="float:none;" src="#rootUrl#/#publicPath#/images/m1/emsformii.png" />
                </div>    
                             
            </div>
        </div>
        
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

