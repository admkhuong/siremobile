<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Interactions - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
			
	
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );				
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Content Management and Business Rules</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	Live and Virtual Interactive Sessions, Auto Respond, Keywords, Agent Assist, IVR, Surveys, Trickle Campaigns, Agent Response                	                
	            </div>
                
                <img style="position:absolute; top:0px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/callcenterheader.png" height="300" width="480" /> 
              
            </div>
        </div>
       
       
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Content Designers</h2>
                         <p>Web based drag and drop dynamic message content and flow controls</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:15px;">
                    <h3>
                        
                        <h3>Our internal fulfillment devices are capable of both pre delivery and during interactive delivery rules application.</h3>
                    </h3>
                </div>     
                
                <BR />
                
                <img style="" src="#rootUrl#/#publicPath#/images/m1/genericivrbuildervsmall.png" width="960px" height="612px" />                
                
               <!--- <BR />
                
                <img style="" src="#rootUrl#/#publicPath#/images/m1/genericivrbuildervsmall.png" width="960px" height="612px" />--->
                             
            </div>
        </div>
        
        
         
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Trickle Campaigns</h2>
                         <p>develop more engaging campaigns</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:15px;">
                    <h3>
                        <h3>To help MasterCard develop a more engaging SMS campaign for underserved consumers, #BrandShort# has developed the following content to better provide relevant tips and guidance to respondents based on their unique characteristics. The content is designed to better understand who participants are and encouraging action-steps that would best meet their unique situations. Upon completion of the program, MasterCard will also be able to better assess the type of respondents using their program.</h3>
                        <BR />
                        <h3>Using EBM tools, this highly complex 4 week trickle campaign with over 342 control points was designed, built, tested, and launched in less than 2 weeks.</h3>
                    </h3>
                </div>     
                
                <BR />
                
                <img style="" src="#rootUrl#/#publicPath#/images/m1/mcprogram.png" />
                             
            </div>
        </div>
                          
		<div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Interactive, Reactive, Proactive</h2>
                         <p>tools and strucures for Branching, Business Rules and Interactive Response</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>While mail merge is still an important tool, #BrandShort# picks up where others leave off. #BrandShort# provides tools and strucures for Branching, Business Rules and Interactive Response as well.</h3>
                
                    <ul>
                        <li>Branch to alternate message content based on personalized transactional data details.</li>
                        <li>Apply business rules to modify or block messaging based on recent or upcoming events.</li>
                        <li>Taylor additional message based on conversational data. Satisfaction response, keywords, identification, etc.</li>                        
                        <li>Content management tools to help manage complex message design and modification.</li>
                    </ul>      
                                     
              	</div>
                             
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omnichannel engagement
                
            </div>
        </div>            
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Net Promoters</h2>
                         <p>a discipline for using customer loyalty metrics to fuel profitable growth</p>
                    </header>
                </div>    
                                        
                   <div class="hiw_Info" style="width:960px;  margin:15px 0;">
                    <h3>
                        <h3>Net Promoter is a powerful management philosophy: both a loyalty metric and a discipline for using customer feedback to fuel profitable growth in your business. The Net Promoter Score is a straightforward metric that helps companies and every employee understand and be accountable for how they engage with customers. It has gained popularity thanks to its simplicity and its linkage to profitable growth. Employees at all levels of the organization understand it, opening the door to customer-centric change and improved business performance. </h3>
                        <BR />
                       	<h3>Use #BrandShort# tools to help capture your Net Promoter metrics.</h3>
                    </h3>
                </div>                   
                             
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Surveys</h2>
                        <p>One method for collecting quantitative information from the customer population</p>
                    </header>
                </div>    
                
 					<div class="hiw_Info" style="width:960px;  margin:30px 0;">
                   
                    <h3>
                        <h3>Survey research is often used to assess your customers thoughts, opinions, and feelings. Survey research can be specific and limited, or it can have more global, widespread goals. Survey research can be used by a variety of different groups. Marketers often use survey research to analyze behavior, while it is also used to meet the more pragmatic needs of the media, such as, in evaluating political candidates, public health officials, professional organizations, and advertising and marketing directors. A survey consists of a predetermined set of questions that is given to a sample. As tool,  one can use surveys to compare the attitudes of different populations as well as look for changes in attitudes over time. A good sample selection is key as it allows one to generalize the findings from the sample to the population.</h3>
                        <BR />
                        <ul>
                        	<li>Cross-Sectional Studies</li>
                            <li>Sampling
                            <li>Questionnaires</li>
                        </ul>
                    </h3>
                </div>                                     
                                 
            </div>
                    
        </div>
               
        <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Contact Center Deflection</h2>
                         <p>Limit contact center expense using simple automation from #BrandShort#.</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <h3>
                        <h3>Broadcasting to your customer base? Customer needs more information in an Ad, Mail, Piece, Flyer, Billboard, or other location? Limit contact center expense using simple automation from #BrandShort#.</h3>
                        <BR />
                        <ul>
                        	<li>Why did we call? Press 1 for more information.</li>
                            <li>Do you wish to be removed from future notifications and alerts? Press 2 to stop receiving thsese messages.</li>
                            <li>Want to talk to a representitive? Press 3 to transfer now. Live Agent Transfer (LAT) &trade;</li>
                        </ul>
                    </h3>
                </div>     
                         
            </div>
        </div>
                
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Contat Center Integration</h2>
                         <p></p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
               
                    <ul>
                       
                       
                        <li>API Post/Get</li>
                        <li>Standard Agent Templates</li>
                        <li>Custom Agent Templates</li>
                        <li>Company User Account Management Tools</li>
                        <li>SMS Chat</li>
                        <li>CID pass in</li>
                        <li>ANI</li> 
                        <li>Automated Response</li>
                        <li>Inbound IVR</li>
                        <li>Keyword Response</li>
                        <li>Canned Response</li>
                        <li>Agent Assisted Response</li>
                       
                    </ul>
               
                  </div>    
                                        
                  <!---  <img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/mfaloginfocused.png" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:right; margin-top:100px;">
                    	<h2 class="super">Something you know and something you have</h2>
                        <br/>
                    	<h3>
                        	Older web sites and applications only have one layer - the password - to protect their users. By adding 2-Step Verification, if a bad guy hacks through your password layer, they'll still need your phone to get into your account.
                        </h3>
                    </div>--->
                             
            </div>
        </div>
                        
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

