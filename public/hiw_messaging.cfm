<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - Messaging</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>
	 <script src="#rootUrl#/#publicPath#/js/jquery.powerzoom.js"></script>      
</cfoutput>




<style>

</style>
<script type="text/javascript" language="javascript">

	$(document).ready(function() {

		
  	});

</script>

</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
        	
                
        <!--- Messaging All Channels --->
        <div id="inner-bg">
            <div id="content">
               
               <div id="inner-right-img"> <img src="#rootUrl#/#publicPath#/images/api/apppuzzle.jpg" /></div>
               
               <div id="inner-box-left" align="center">
          
                    <div class="inner-main-hd">Messaging All Channels </div>
                    <div class="inner-txt">
                    	<p>#BrandShort# systems are designed to provide messaging that is high quality, relevant, personalized, and delivered in context. #BrandShort# helps manage your customer relationships across interactions. These interactions happen at many touch points - or in many channels - from the first visit to a retail store or the website - and dozens of other places as well - both online and offline. Making multichannel messaging work means having a contact and messaging strategy for key member segments and every one of the key touch points they are likely to use. You need an understanding of what makes those individual dialogues successful.</p>
                    </div>
                    
                    
                    <div class="inner-txt">
                    
                    	<p>For multichannel messaging to be manageable, process is critical, as the number of components will be large and will grow exponentially as you experiment with newer channels like mobile and social.</p>
                    	<ul>
                            <li>
                            	<strong>Building a Customer Life Cycle Map for Your Key Segments</strong><BR />
                    			Before creative development begins, start by mapping successful outcomes for each customer segment. What actions do you want prospects or customers to take as they move through their relationship with you?  What channels are they likely to prefer? What will define success for each type of interaction?      	
                            </li>

							<li>
                            	<strong>Creative Development Process</strong><BR />
                    			Establish up front creative, segmentation, response capture and measurement strategies. Multichannel campaign flowcharts that represent the interaction of each medium help everyone understand how the pieces work together.     	
                            </li>
                            
                            <li>
                            	<strong>Manage Response Options</strong><BR />
                    			Consumers may be able to respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.     	
                            </li>
                            
                            <li>
                            	<strong>Customer Experience</strong><BR />
                    			Content is still king, but remember, content on its own can be worthless unless it's high quality, relevant, personalized, and delivered in context.      	
                            </li>
                            
                            <li>
                            	<strong>Events and Triggers</strong><BR />
                    			Data Feeds and mechanisms. Plan for connectivity, integration and impacts on existing IT systems.  
                            </li>
                            
                            <li>
                            	<strong>Testing</strong><BR />
                    			Define up-front the post-campaign analytics at the same time you're doing creative and messaging development. Setup any additional tracking and data capture sites as needed.    	
                            </li>
                            
                            <li>
                            	<strong>Execution</strong><BR />
                    			In conjunction with Testing above, polish your message for the best results. Also, consider how your messaging will move from a one-time pilot to an ongoing communications stream - Capacity planning, timing, data feeds, security, compliance.
                            </li>
                                                                                 
                        </ul>
                    </div>                   
                   
                </div>
               
               	
           <!--- See below for a description of the various usages per channel.--->
            </div>
        </div>
          
        <div id="servicemainline"></div> 
        
        <!--- Voice --->       
        <div id="inner-bg">
                        
            
           <div id="content" style="min-height:36px;">
           		<div class="inner-main-hd">Voice Messaging</div>
              	<div class="inner-txt-full" style="margin-bottom:20px;">
                	<p>Voice messaging is a mass communication method that we pioneered in the 1990's. Voice messaging can quickly and reliably contact targets - whether they be members, subscribers, constituents, employees, or customers. While our technology can do one to many broadcasts, we are the preeminent provider of data driven one to one customized voice messaging using natural language components.</p>
                	<p></p>
                
                </div>
              
           </div>
                         
        </div>
        
        <div id="inner-bg-alt">
            
            <div id="content" style="min-height:36px;">
           		<div class="inner-main-hd">Call Center Integration</div>
              	<div class="inner-txt-full" style="margin-bottom:20px;">Think of a short code as similar to a domain name and a Keyword similar to a page.  When you want the news you might go to the crime (Keyword) page of NEWS.com (Shortcode). If you want special deals on concerts and events from Sport City Tickets you text TICKETS (Keyword) to 55555 (shortcode).</div>
        	
            	<div id="inner-box-left" align="center">
                        
                         <a href="##" style="display:block;">Quick Links</a>
                         <a href="##SMS" style="display:block;">Call Center Integration</a>
                         <a href="#rootUrl#/#publicPath#/hiw_calldetectionlogic.cfm" style="display:block;">Call Answer Detection Logic</a>
                         <a href="#rootUrl#/#publicPath#/hiw_tdmvsvoip.cfm" style="display:block;">TDM vs. VoIP</a>
                         
                    </div>      
				
        	</div>
                                              
        </div> 
            
        <div id="servicemainline"></div>  
       
        </div>
       
            
      
        
              
         
                
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>