<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MFA - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
		
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                 <div class="inner-main-hd" style="width:400px; margin-top:30px;">Multi-Factor Authentication</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	One Time verification code uses information sent to a phone via SMS, Push or Voice for use as part of the login process.                 
	            </div>
                
                <img style="position:absolute; top:30px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/mfademoheader.png" /> 
              
            </div>
        </div>
        
   		<div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Two Factor Authentication</h2>
                         <p>Something you know and something you have</p>
                    </header>
                </div>    
                                        
                <img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/mfaloginfocused.png" />
                    
                <div class="hiw_Info" style="width:460px; float:right; margin-top:100px;">
                    <h3>
                        Older web sites and applications only have one layer - the password - to protect their users. By adding 2-Step Verification, if a bad guy hacks through your password layer, they'll still need your phone to get into your account.
                    </h3>
                </div>
                         
            </div>
        </div>
        
         <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Better Account Protection</h2>
                         <p>Stronger security for your applications</p>
                    </header>
                </div>    
                                        
                <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/userprofilewide.png" />
                    
                <div class="hiw_Info" style="width:460px; float:left; margin-top:0px;">
                    <h3>
                        With the added 2-Step Verification, you'll protect your user's accounts with something they know (their password) and something they have (their phone). 
                    </h3>
                </div>
                         
            </div>
        </div>
       
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	MFA A.K.A Two-factor authentication, requires the use of two independent authentication factors. 
                
            </div>
        </div>            
               
        <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Add to your application</h2>
                         <p>How do I add this functionality to my Application?</p>
                    </header>
                </div>    
                                        
               <img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/how-to.jpg" />
                    
                <div class="hiw_Info" style="width:600px; float:right; margin-top:0px;">
                    
                    <h3>Three easy steps:</h3>
                     
                    <h3>
                        <div style="line-height:38px; margin-bottom:8px; margin-top:8px;"><h2 class="super" style="display:inline; margin-right:10px;"><span class="round">1</span></h2>Get an EBM Account</div>
                        <div style="line-height:38px; margin-bottom:8px;"><h2 class="super" style="display:inline; margin-right:10px;"><span class="round">2</span></h2>Build a Simple Campaign</div>
                        <div style="line-height:38px; margin-bottom:8px;"><h2 class="super" style="display:inline; margin-right:10px;"><span class="round">3</span></h2>Fulfill with just one API call.</div>
                        
                    </h3>
                </div>
                         
            </div>
        </div>
                
        <div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                              
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>#BrandShort# Login</h2>
                         <p>Optionally turn on for your own #BrandShort# account</p>
                    </header>
                </div>                           
               	
            
               <div id="content" style="min-height: 100px">
                    <div class="inner-main-hd" style="width:900px;"> <h2 class="super" style="display:inline; margin-right:10px;"><span class="round" style="line-height:38px; margin-bottom:8px;">1</span></h2> Sign in or Sign up for an #BrandShort# account. </div>
                    
               </div>
               
               <div class="hiw_Info" style="width:450px;margin-top:0px;">
                    <cfset SIID = 1/>	
                    <div id="SignInContainerSample" class="awesome" style=" border: 2px solid ##0085c8; padding:10px; border-radius:8px;"><cfinclude template="signin.cfm"></div>
                    
               </div>             
                             
            </div>
        </div>
              
              
        <div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Create a Campaing</h2>
                         <p>Customize Your MFA Message</p>
                    </header>
                </div>    
                
                <div id="content" style="">
                    <div class="inner-main-hd"><h2 class="super" style="display:inline; margin-right:10px;"><span class="round" style="line-height:38px; margin-bottom:8px;">2</span></h2>Build a Simple Campaign</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;"><h3>Customize Your MFA message. With Pre-Defined Campaigns you can design highly customized messages and business rules that can be accessed with one simple API call. Each campaign you create will have a unique ID you can refer to in API calls.</h3></div>
              
                    <img src="#rootUrl#/#publicPath#/images/m1/mfamisc.png" height="384" width="853" />
                   
               </div>             
                             
            </div>
            
        </div>
        
        <div id="inner-bg-m1" style="padding-top:25px; padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>API Call</h2>
                         <p>Call Campaign via REST API from your app (Web, Mobile, Desktop)</p>
                    </header>
                </div>    
                                        
                 <div class="inner-main-hd"><h2 class="super" style="display:inline; margin-right:10px;"><span class="round" style="line-height:38px; margin-bottom:8px;">3</span></h2> Fulfill with just one API call</div>
            
            		<div class="inner-txt-full">
                                         	
                     	<p><i>Updated on Wed, 2013-01-31 11:30</i></p>
						<p>&nbsp;</p>
                        <p>Allows the authenticating users to post a message to the queue for automated delivery.</p>
                        <p>&nbsp;</p>
                        <p>Returns a JSON structure when successful. Returns a string describing the failure condition when unsuccessful.</p>
                        <p>&nbsp;</p>
                        <p>Actions taken in this method are asynchronous and messages will be delivered based on Pre-Defined Campaign's schedule.</p>

                    	<p>&nbsp;</p>
                        <p>&nbsp;</p>
                    	<div>
                    	    <h2>Resource URL Samples</h2>
                    	    <div>http://#EBMAPIPath#/webservice/ebm/pdc/addtoqueue</div>
                            <div>http://#EBMAPIPath#/webservice/ebm/pdc/addtorealtime</div>
                   	    </div>
                        <p>&nbsp;</p>
                    	
                        <div class="field text field-doc-params">
                            <h2>Parameters</h2>
                            <div>
                                <div id="api-param-inpBatchId" class="parameter">
                                    <span class="param">inpBatchId 
                                        <span>required</span>
                                    </span>
                                    <p>The pre-defined campaign Id you created under your account.</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>864512</tt>
                                    </p>
                                </div>
                                <div id="api-param-inpContactString" class="parameter">
                                    <span class="param">inpContactString
                                    <span>required</span>
                                    </span>
                                    <p>The contact string - can be a Phone number , eMail address, or SMS number </p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>(555)812-1212</tt>
                                    OR
                                    <tt>JoeThompson@eventbasedmessaging.com</tt>
                                    </p>
                                    <p></p>
                                </div>
                                <div id="api-param-inpContactTypeId" class="parameter">
                                    <span class="param">inpContactTypeId
                                    <span>required</span>
                                    </span>
                                    <p>1=Phone number, 2=eMail, 3=SMS</p>
                                    <p>
                                    <strong>Example Values</strong>
                                    :
                                    <tt>1</tt>
                                    </p>
                                </div>
                          
                              	
                                <div id="api-param-unlimited" class="parameter">
                          		    <p>Unlimited custom data parameters are accepted via form data. These parameters can be used to control content, branching logic or other message parameters.</p>
                          			<p>&nbsp;</p>
                                    <span class="param">customFormField
                                    <span>optional</span>
                                    </span>
                                    <p>Strings or Numbers - not strongly typed but should evaluate to a number or string</p>
                                    <p>
                                     <strong>Example Values</strong>									
                                    :
                                    <tt>City</tt> OR <tt>Balance</tt> OR <tt>CallerId</tt> OR <tt>whatever your message needs.</tt>
                                    </p>
                                </div>
                            </div>
                            	
                          	<p>&nbsp;</p>
                          
                            <div class="fieldgroup group-example-request">
                                <h2>Example Request</h2>
                                <div>
                                
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST
                                        <span>Required</span>
                                        </span>
                                        <p>#EBMAPIPath#/webservice/ebm/pdc/addtoqueue</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">POST Data
                                        <span>Required</span>
                                        </span>
                                        <p>inpBatchId=864512&amp;inpContactString=5558121212&amp;inpContactTypeId=1</p>
                                        <p>&nbsp;</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Header Data
                                        <span>Required</span>
                                        </span>
                                        <p>type="header" name="Accept" value="application/json"</p>
                                        <p>type="header" name="datetime" value="YYYY-MM-DD HH:MM:SS"</p>
                                        <p>type="header" name="authorization" value="ecrypted(datetime)"</p>
                                     </div>
                                     
                                     <div id="api-param-unlimited" class="parameter">                          		   
                                        <span class="param">Custom Data
                                        <span>Optional</span>
                                        </span>
                                        <p>type="form" name="inpCompany" value="WidgeCo"</p>
                                        <p>type="form" name="inpCity" value="Corona"</p>
                                        <p>type="form" name="inpState" value="CA"</p>
                                     </div>
                             
                            	</div>
                  	  		</div>
                            
                            
                    		<p>&nbsp;</p>
            		
                            <div>The EBM REST API calls use JSON as their return format, and the standard HTTP methods like GET, PUT, POST and DELETE 
                            <BR /><!---(see API documentation <a>here</a> for which methods are available for each resource)---></div>                                                       
           
           
                    	</div>
                        
                    </div>
             
                             
            </div>
        </div>          
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

