<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Notifcations, Alerts, Surveys - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<style>
			

	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}



</script>
</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Notifications, Alerts, Surveys and more...</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort# systems are designed to provide messaging that is high quality, relevant, personalized, and delivered in context.
	            </div>
                
                <img style="position:absolute; top:10px; right:30px; " src="#rootUrl#/#publicPath#/images/m1/alertslist.png" /> 
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Interactions</h2>
                        <p>#BrandShort# manages interactive messages between you and your consumer.</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:900px; margin-top:30px; margin-bottom:30px;">
                    	                        
                        <h3>Message can be by consumer requested (Inbound Phone Call, SMS Keyword) or triggered by events (Order Updates, Emergency Notifications)</h3>
                    	<br/>
                        <h3>
                        <!---	Need 900,000 Phone calls in the next 2 hours?<br />
                            Need to deliver a Multi-Factor Authentication code in less than 2 seconds?<br />
                            
                            #BrandShort# dispatches Messages directly to the consumer. Message can be by consumer requested (Inbound Phone Call, SMS Keyword) or triggered by events (Order Updates, Emergency Notifications)
               --->             
               
                             <ul>
                                <li>
                                    <strong>Building a Customer Life Cycle Map for Your Key Segments</strong><BR />
                                    Before creative development begins, start by mapping successful outcomes for each customer segment. What actions do you want prospects or customers to take as they move through their relationship with you?  What channels are they likely to prefer? What will define success for each type of interaction?      	
                                </li>
    
                                <li>
                                    <strong>Creative Development Process</strong><BR />
                                    Establish up front creative, segmentation, response capture and measurement strategies. Multichannel campaign flowcharts that represent the interaction of each medium help everyone understand how the pieces work together.     	
                                </li>
                                
                                <li>
                                    <strong>Manage Response Options</strong><BR />
                                    Consumers may be able to respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.     	
                                </li>
                                
                                <li>
                                    <strong>Customer Experience</strong><BR />
                                    Content is still king, but remember, content on its own can be worthless unless it's high quality, relevant, personalized, and delivered in context.      	
                                </li>
                                
                                <li>
                                    <strong>Events and Triggers</strong><BR />
                                    Data Feeds and mechanisms. Plan for connectivity, integration and impacts on existing IT systems.  
                                </li>
                                
                                <li>
                                    <strong>Testing</strong><BR />
                                    Define up-front the post-campaign analytics at the same time you're doing creative and messaging development. Setup any additional tracking and data capture sites as needed.    	
                                </li>
                                
                                <li>
                                    <strong>Execution</strong><BR />
                                    In conjunction with Testing above, #BrandShort# can help polish your message for the best results. Also, consider how your messaging will move from a one-time pilot to an ongoing communications stream - Capacity planning, timing, data feeds, security, compliance.
                                </li>
                                                                                     
                            </ul>
                        
                        </h3>
                    </div>
                                
                                 
            </div>
                    
        </div>
        
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	When designing a messaging application, #BrandShort# helps you know where to put what types of data to share in the message customization / business rules, and likewise where to look for what types of data coming from other applications. 
                
            </div>
        </div>    
        
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Sample Usages</h2>
                         <p>Programs #BrandShort# runs now for Notifications, Alerts, Surveys and more...</p>
                    </header>
                </div>    
                                        
                    <div class="hiw_Info" style="width:900px; margin-top:30px;">
                    	
                    	<h3>
                        	
                            <ul>
                            
                                <li>Account Threshold Alerts</li>                            	
                                <li>Order Confirmation</li>
                                <li>Order Completion</li>
                                <li>Order Delays</li>
                                <li>Go Green Initiatives</li>
                                <li>Repair Ticket Completion</li>
                                <li>Local Svc. Change</li>
                                <li>Service Order Completion</li>
                                <li>Rate Changes</li>
                                <li>Payment Options</li>
                                <li>Non-Usage Alerts</li>
                                <li>Termination Warnings / Notices</li>
                                <li>Account Suspension</li>
                                <li>Corporate Fraud Notification</li>
                                <li>Change Order Notification</li>
                                <li>Event Alerts</li>
                                <li>Event Changes</li>
                                <li>Service Outage</li>
                                <li>Technician in Route</li>
                                <li>Multi Factor Authentication</li>
                                <li>Help</li>
                                <li>Rewards Balance</li>
                                <li>Rewards Signup</li>
                                <li>Offers</li>
                                <li>Reservations - New</li>
                                <li>Reservations - Cancellation</li>
                                <li>Reservations - Change</li>
                                <li>Product Retirement</li>
                                <li>Product Availability</li>
                                <li>Exceed Usage</li>
                                <li>Trouble Resolution</li>
                                <li>Firmware Updates</li>
                                <li>Product Recalls</li>
                                <li>Weather Emergency</li>
                                <li>Emergency Notifications</li>
                                <li>Disaster Recovery</li>
                                <li>Password Changed Alerts</li>
                                <li>New Computer Accessed Your Account</li>
                                <li>Coupons</li>
                                <li>Virtual Assistance</li>
                                <li>First Bill Explanation</li>
                                <li>Bill Pay Processed</li>
                                <li>Billing Errors</li>
                                <li>Credit Card Expired</li>
                                <li>Bill Available Online Now</li>
                                <li>Appointment Reminders</li>
                                <li>Surveys</li>
                                <li>FCC Mandates</li>
                                <li>Blasts</li>
                                <li>Political Messages</li>
                                <li>Program Eligibility</li>
                                <li>Program Enrollment</li>
                                <li>Profile Changes</li>
                                <li>Customer Preferences</li>
                                <li>Running Late</li>
                                <li>Nobody Home</li>
                                <li>Prescription Ready for Pickup</li>
                                <li>Tracking Numbers</li>
                               
                            </ul>
                                             
                        </h3>
                    </div>
                                                       
                             
            </div>
        </div>
        
            
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>