<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>How it Works - <cfoutput>#BrandShort#</cfoutput></title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="home7assets/css/rotating-card.css" rel="stylesheet" />
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
        
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
			
			
			.intro2 {
				left: 0;
				padding: 0 0;
				position: relative;
				text-align: center;
				top: 0;
				transform: none;
				width: 100%;
				margin: 20px 0;
			}
			
			.pfblock 
			{
				padding: 2em;
			}

		</style>
        
    </cfoutput>
      
      

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>


<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
       
        
<style>
	
	
<!---	.circle-text {
    width:50%;
}
.circle-text:after {
    content: "";
    display: block;
    width: 100%;
    height:0;
    padding-bottom: 100%;
    background: #4679BD; 
    -moz-border-radius: 50%; 
    -webkit-border-radius: 50%; 
    border-radius: 50%;
}
.circle-text div {
    float:left;
    width:100%;
    padding-top:50%;
    line-height:1em;
    margin-top:-0.5em;
    text-align:center;
    color:white;
}


.process-step
{
	background: #4679BD;
	text-align: center;
	vertical-align: middle;
	line-height: 90px;
	color: #fff;
	border-radius: 8px;
	border: #000 solid 3px;
}

.request-img-circle
{
	 background: #4679BD;
	
}

--->



.process-step
{
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#d0a577+0,a07547+100,c19e67+100,a07547+100 */

	background: #d0a577; /* Old browsers */
	
	/* IE9 SVG, needs conditional override of 'filter' to 'none' */
	
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNkMGE1NzciIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjYTA3NTQ3IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2MxOWU2NyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNhMDc1NDciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvcmFkaWFsR3JhZGllbnQ+CiAgPHJlY3QgeD0iLTUwIiB5PSItNTAiIHdpZHRoPSIxMDEiIGhlaWdodD0iMTAxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);	
	background: -moz-radial-gradient(center, ellipse cover,  #d0a577 0%, #a07547 100%, #c19e67 100%, #a07547 100%); /* FF3.6+ */	
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#d0a577), color-stop(100%,#a07547), color-stop(100%,#c19e67), color-stop(100%,#a07547)); /* Chrome,Safari4+ */
	background: -webkit-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* Chrome10+,Safari5.1+ */	
	background: -o-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* Opera 12+ */	
	background: -ms-radial-gradient(center, ellipse cover,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* IE10+ */	
	background: radial-gradient(ellipse at center,  #d0a577 0%,#a07547 100%,#c19e67 100%,#a07547 100%); /* W3C */	
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d0a577', endColorstr='#a07547',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */

	text-align: center;
	vertical-align: middle;
	line-height: 90px;
	color: #fff;
	border: none;
}


.circle {
  background: #4679BD;
  box-shadow: inset -25px -25px 40px rgba(0,0,0,.5);
  border-radius: 50%;
  height: 100px;
  width: 100px;
  opacity: 0.7;
}

.circle-responsive {
  background: #4679BD;
  box-shadow: inset -25px -25px 40px rgba(0,0,0,.5);
  border-radius: 50%;
  height: 0;
  padding-bottom: 100%;
  width: 100%;
}


.circle-content {
  color: #fff;
  float: left;
  line-height: 1;
  margin-top: -0.5em;
  padding-top: 50%;
  text-align: center;
  width: 100%;
}


.CircleShiftContainer
{
	position: relative;	
	height: 300px;
	padding-top: 50px;
	
}

.CircleShift 
{
	position: absolute;
	left:50%;
    margin-left:-75px;			
}

.CircleShift1 
{
	
	webkit-transform: translate(0%, -90%);
	   -moz-transform: translate(0%, -90%);
		-ms-transform: translate(0%, -90%);
		 -o-transform: translate(0%, -90%);
			transform: translate(0%, -90%);			
}

.CircleShift2 
{	
	-webkit-transform: translate(0%, 90%);
	   -moz-transform: translate(0%, 90%);
		-ms-transform: translate(0%, 90%);
		 -o-transform: translate(0%, 90%);
			transform: translate(0%, 90%);				
	
}

.CircleShift3 
{
	-webkit-transform: translate(-80%, -25%);
	   -moz-transform: translate(-80%, -25%);
		-ms-transform: translate(-80%, -25%);
		 -o-transform: translate(-80%, -25%);
			transform: translate(-80%, -25%);	
}	

.CircleShift4 
{
	-webkit-transform: translate(80%, -25%);
	   -moz-transform: translate(80%, -25%);
		-ms-transform: translate(80%, -25%);
		 -o-transform: translate(80%, -25%);
			transform: translate(80%, -25%);	
}	

.CircleShift5 
{
	-webkit-transform: translate(-80%, 25%);
	   -moz-transform: translate(-80%, 25%);
		-ms-transform: translate(-80%, 25%);
		 -o-transform: translate(-80%, 25%);
			transform: translate(-80%, 25%);	
}	

.CircleShift6 
{
	-webkit-transform: translate(80%, 25%);
	   -moz-transform: translate(80%, 25%);
		-ms-transform: translate(80%, 25%);
		 -o-transform: translate(80%, 25%);
			transform: translate(80%, 25%);	
}	

.CircleShift7 
{
	-webkit-transform: translate(-60%, 60%);
	   -moz-transform: translate(-60%, 60%);
		-ms-transform: translate(-60%, 60%);
		 -o-transform: translate(-60%, 60%);
			transform: translate(-60%, 60%);	
}	

.CircleShift8 
{
	-webkit-transform: translate(60%, 60%);
	   -moz-transform: translate(60%, 60%);
		-ms-transform: translate(60%, 60%);
		 -o-transform: translate(60%, 60%);
			transform: translate(60%, 60%);	
}

.CircleShift9 
{
	-webkit-transform: translate(-60%, -60%);
	   -moz-transform: translate(-60%, -60%);
		-ms-transform: translate(-60%, -60%);
		 -o-transform: translate(-60%, -60%);
			transform: translate(-60%, -60%);	
}	

.CircleShift10 
{
	-webkit-transform: translate(60%, -60%);
	   -moz-transform: translate(60%, -60%);
		-ms-transform: translate(60%, -60%);
		 -o-transform: translate(60%, -60%);
			transform: translate(60%, -60%);	
}


div[alt]:hover:after {
  content: attr(alt);
  padding: 4px 8px;
  color: #333;
  position: absolute;
  left: 0;
  top: 100%;
 <!--- white-space: nowrap;--->
<!---  z-index: 10000;--->
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-box-shadow: 0px 0px 4px #222;
  -webkit-box-shadow: 0px 0px 4px #222;
  box-shadow: 0px 0px 4px #222;
  background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));
  background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);
  background-image: -o-linear-gradient(top, #eeeeee, #cccccc);
}


</style>

    <script type="text/javascript" language="javascript">
				
		$(document).ready(function(){
			<!---$('#account_type_personal').click();
			if($('#account_type_company').is(':checked')){
				$("#company_name").show();
			}--->
			
					
		});
			
			
		function rotateCard(btn){
			var $card = $(btn).closest('.card-container');
			<!---console.log($card);--->
			if($card.hasClass('hover')){
				$card.removeClass('hover');
			} else {
				$card.addClass('hover');
			}
		}
	
	
	</script>
</head>
            
<cfoutput>
<body class="pfblock-gray">
        
        
    <section id="" class="pfblock pfblock-gray">
                       
        <div class="intro2">
        
            
                
                <cfoutput>
                    <a class="" href="home"><img src="#LogoImageLink7#" style="border:none;" /></a>
                </cfoutput>
                    
           
                        
            <h1>Event Based Messaging</h1>
            <div class="start">Business Plan</div>
            
        </div>
        
    </section>



 
        
    <section id="" class="pfblock">
            
      <div class="row">
        
        
        <div class="col-sm-6 col-sm-offset-3">
		
			<cfoutput>
                 <img class="img-circle" src="#rootUrl#/#publicPath#/home7assets/images/m7/business-plan.png"/>
            </cfoutput>
        
        </div>
        
      </div>
      
    
        <div class="col-sm-6 col-sm-offset-3">
    
            <div class="pfblock-header wow fadeInUp">
                <h2 class="pfblock-title">Business Plan</h2>
                <div class="pfblock-line"></div>
                <div class="pfblock-subtitle">
            
                </div>
            </div>
    
        </div>
                   
    </section>
       
            
  <!--- 	
  
  Buy leads from Salesforce - see Bruce
  
  
  --->
    
    
		<cfinclude template="footer.cfm" />
    
    
    </body>
</cfoutput>
</html>


