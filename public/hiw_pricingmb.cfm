<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pricing - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );		
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Pricing</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	Self Service and Enterprise Standard Price Decks. Enterprise companies with contracts can save even more.          	                
	            </div>
                
                <img style="position:absolute; top:0px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/callcenterheader.png" height="300" width="480" /> 
              
            </div>
        </div>
        
   	<!---	<div id="inner-bg-m1" style="padding-top:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>SMS</h2>
                         <p>Domestic Text Messaging</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <p style="text-transform:uppercase;">
                        
                        <BR />
                       
                        <table border="1" cellpadding="5" cellspacing="0"  >
                        	<tr style="background-color:##CCCCCC; text-transform:uppercase;">
                            	<th style="width:900px; text-align:left; padding:12px;">Domestic Text Messaging</th>
                                <th style="width:200px; text-align:center; padding:12px;">Pricing $US</th>
                            </tr>
                        
                        
                        	<tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    <p style="line-height:19px; margin:8px" >Per Message:</p>
                                    <ul style="margin-left:30px;">
                                        <li>0-500,000</li>
                                        <li>500,001-750,000</li>
                                        <li>750,001-1,000,000</li>
                                        <li>1,000,001-2,000,000</li>
                                        <li>2,000,001-3,000,000</li>
                                        <li>3,000,001-5,000,000</li>
                                        <li>5,000,001+</li>
                                    </ul>
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                  
                                  	<p style="line-height:19px; margin:8px" >&nbsp;</p> 
                                    <p style="line-height:19px; margin:8px" >$0.0149</p>
                                    <p style="line-height:19px; margin:8px" >$0.0139<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0129<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0119<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0109<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0099<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0089<p/>
                                  
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    Carrier Surcharges fee per MT:
                                    <BR />
                                    Surcharge by METRO PCS as of Jan 2012
                                    <BR />
                                    Surcharge by Sprint/Nextel/Boost/Virgin as of Apr 2011
                                    <BR />
                                    Surcharge by T-Mobile as of Oct 2010
                                    <BR />
                                    Surcharge by Cricket as of Feb 2012
                                    <BR />
                                    Surcharge by US Cellularas of Aug 2013
                                    <BR />                                    
                                    (Rates and carriers may change subject to carrier charge increases)                                    
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                	<BR />
                                    $0.0050<BR />
                                    $0.0050<BR />
                                    $0.0025<BR />
                                    $0.0025<BR />
                                    $0.0035<BR />
                                   	<BR />
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    Carrier Surcharges fee per MO:
                                    <BR />
                                    
                                    Surcharge by Sprint/Nextel/Boost as of Apr 2011
                                    <BR />
                                    Surcharge by Virgin as of Feb 2012
                                    <BR />
                                    Surcharge by T-Mobile as of Oct 2010
                                    <BR />
                                    (Rates and carriers may change subject to carrier charge increases)                                    
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                	<BR />
                                    $0.0050<BR />
                                    $0.0050<BR />
                                    $0.0025<BR />
                                    <BR />
                                </td>
                            </tr>                            
                            
                        
                        </table>
                        
                        <BR />
                         
                        <table border="1" cellpadding="5" cellspacing="0"  >
                        	<tr style="background-color:##CCCCCC; text-transform:uppercase; font-weight:200;">
                            	<th style="width:900px; text-align:left; padding:12px;">Short Code Monthly Fees</th>
                                <th style="width:200px; text-align:center; padding:12px;">Pricing $US</th>
                            </tr>
                        
                        
                        	<tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    Option 1 - Standard monthly shortcode fee
                                    with 3 month minimum commitment 
                                    
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                    $750
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    Option 2 - Vanity monthly shortcode fee
                                    with 3 month minimum commitment 
                                    
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                    $1500
                                </td>
                            </tr>
                        
                        </table>
                                                
                        <BR />
                         
                        <table border="1" cellpadding="5" cellspacing="0"  >
                        	<tr style="background-color:##CCCCCC; text-transform:uppercase; font-weight:200;">
                            	<th style="width:900px; text-align:left; padding:12px;">Short Code / Program Setup Fees</th>
                                <th style="width:200px; text-align:center; padding:12px;">Pricing $US</th>
                            </tr>
                        
                        
                        	<tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    Carrier Setup fee per Shortcode Program:
                                    <BR />
                                    Required by T-MOBILE (includes migration from another Aggregator):	
                                    <BR />
                                    All new short codes
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                    $600
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    Carrier Setup fee per Program:
                                    <BR />
                                    Required by AT&amp;T (does NOT includes migration from another Aggregator):
                                    <BR />
                                    All new short codes	
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                    $600
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    Carrier Setup fee per Program:
                                    <BR />
                                    Required by Metro PCS: (includes migration from another Aggregator):
                                    <BR />
                                    All new short codes	
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                    $400
                                </td>
                            </tr>
                            
                             <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    Carrier Setup fee per Program:
                                    <BR />
                                    Required by  Sprint/Nextel/Boost/Virgin: (includes migration from another Aggregator):
                                    <BR />
                                    All new short codes	
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                    $250
                                </td>
                            </tr>
                            
                            <tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    migration from another carrier fee per short code:
                                    <BR />
                                    Required by AT&amp;T (One time Migration Fee from another Aggregator)
                                </td>
                            
                                <td style="text-align:center; padding:12px;">
                                    $1200
                                </td>
                            </tr>                            
                        
                        </table>
                        
                        <BR />
                        
                        <h3>International and Multi-Media SMS Available - Call for Rates</h3>
                    <p>
                    
                </div>     
                         
            </div>
        </div>
        --->
        
  <!---      <div id="inner-bg-m1" style="padding-top:25px;  padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Voice</h2>
                         <p>Domestic Voice Messaging</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <p style="text-transform:uppercase;">
                        
                        <BR />
                       
                        <table border="1" cellpadding="5" cellspacing="0"  >
                        	<tr style="background-color:##CCCCCC; text-transform:uppercase;">
                            	<th style="width:900px; text-align:left; padding:12px;">Domestic Voice Messaging - Outbound</th>
                                <th style="width:200px; text-align:center; padding:12px;">Pricing $US</th>
                            </tr>
                        
                        
                        	<tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    <p style="line-height:19px; margin:8px" >Per Message:</p>
                                    <ul style="margin-left:30px;">
                                        <li>0-500,000</li>
                                        <li>500,001-750,000</li>
                                        <li>750,001-1,000,000</li>
                                        <li>1,000,001-2,000,000</li>
                                        <li>2,000,001-3,000,000</li>
                                        <li>3,000,001-5,000,000</li>
                                        <li>5,000,001+</li>
                                    </ul>
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                  
                                  	<p style="line-height:19px; margin:8px" >&nbsp;</p> 
                                    <p style="line-height:19px; margin:8px" >$0.0295</p>
                                    <p style="line-height:19px; margin:8px" >$0.0290<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0285<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0280<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0275<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0270<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0265<p/>
                                  
                                </td>
                            </tr>
                            
                        </table>
                        
                        <BR />
                        <h3>Note: Voice pricing is per 30 second increment. Basic Message Customization included. Campaign Setup fee of $250 for each campaign less than 50,000 dials</h3>
                        <h3>Inbound and International Voice Available - Call for Rates</h3>
                        
                    <p>
                    
                </div>     
                         
            </div>
        </div>--->
        
        
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omnichannel engagement
                
            </div>
        </div>      
        
        <!--- EMS pricing --->
        
        <!--- Email Templates --->
        
        <!--- Template building per hour pricing --->
        
        <!--- SFTP hosting --->
        
        <!--- Dev per hour - seta pricing with markup --->
        
        <!--- Various CSC fees	--->
        
        
        <!---  http://sendgrid.com/marketing/sendgrid-services?ls=Advertisement&cid=70180000000cl5Z&lsd=adwords&kw=Sendgrid&mt=e&gaid=SendGrid-US-Brand&mc=Paid%20Search&mcd=AdWords&keyword=sendgrid&network=g&matchtype=e&mobile=&content=&search=1&cvosrc=PPC.Google.sendgrid&cvo_cid=SendGrid%20-%20US%20-%20Brand&gclid=Cj0KEQjw4aqgBRCvwLDi_8Tc54YBEiQAs6DLvNTiikGkB53cNEsHEshOWLu-ZvqX93bSwjS0efUwdwMaAv1z8P8HAQ     --->
        
      <!---     <div id="inner-bg-m1" style="padding-top:25px; padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>eMail</h2>
                         <p>eMail Messaging</p>
                    </header>
                </div>    
                                        
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                    
                    <p style="text-transform:uppercase;">
                        
                        <BR />
                       
                        <table border="1" cellpadding="5" cellspacing="0"  >
                        	<tr style="background-color:##CCCCCC; text-transform:uppercase;">
                            	<th style="width:900px; text-align:left; padding:12px;">eMail Messaging</th>
                                <th style="width:200px; text-align:center; padding:12px;">Pricing $US</th>
                            </tr>
                        
                        
                        	<tr style="text-transform:uppercase;">
                                <td style="text-align:left; padding:12px; font-weight:200;">
                                    
                                    <p style="line-height:19px; margin:8px" >Per Message:</p>
                                    <ul style="margin-left:30px;">
                                        <li>0-500,000 (+$250 Setup Fee)</li>
                                        <li>500,001-750,000</li>
                                        <li>750,001-1,000,000</li>
                                        <li>1,000,001-2,000,000</li>
                                        <li>2,000,001-3,000,000</li>
                                        <li>3,000,001-5,000,000</li>
                                        <li>5,000,001+</li>
                                    </ul>
                                </td>
                            
                                <td style="text-align:center; padding:12px;text-transform:capitalize;">
                                  
                                  	<p style="line-height:19px; margin:8px" >&nbsp;</p> 
                                    <p style="line-height:19px; margin:8px" >$0.0100</p>
                                    <p style="line-height:19px; margin:8px" >$0.0089<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0079<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0069<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0059<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0049<p/>
                                    <p style="line-height:19px; margin:8px" >$0.0039<p/>
                                  
                                </td>
                            </tr>
                            
                        </table>
                        
                        <BR />
                        <h3>Note: Basic Message Customization included. Campaign Setup fee of $250 for each campaign less than 500,000 eMails</h3>
                        <h3>Creative, Link Tracking, and advance reporting available. Call for Rates</h3>                       
                    <p>
                    
                </div>     
                         
            </div>
        </div>--->
        
             
               
      
      
      	<div class="transpacer"></div>
        <div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

