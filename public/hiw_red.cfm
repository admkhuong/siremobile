<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reporting and Dashboard - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<style>
				
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );				
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                <div class="inner-main-hd" style="width:600px; margin-top:30px;">Reports, Exports, and Dashboards</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
              	      	Digital dashboards allow managers to monitor the contribution of the various campaigns in their organization. To gauge exactly how well a campaign is performing overall, digital dashboards allow you to capture and report specific data points from each aspect within the campaign, thus providing a "snapshot" of performance.
                </div>
                
                <img style="position:absolute; top:0px; right:0px; " src="#rootUrl#/#publicPath#/images/m1/dashboard-email-combo.png" height="300" width="480" /> 
              
            </div>
        </div>
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Dashboards</h2>
                        <p>Benefits of using digital dashboards include</p>
                    </header>
                </div>    
                
 				<div class="hiw_Info" style="width:960px;  margin:30px 0;">
                   
                   
                     
                        <ul style="width:710px; margin:5px 100px 5px 250px; text-align:left;">               
                            <li>Visual presentation of performance measures</li>
                            <li>Ability to identify and correct negative trends</li>
                            <li>Measure efficiencies/inefficiencies</li>
                            <li>Ability to generate detailed reports showing new trends</li>
                            <li>Ability to make more informed decisions based on collected business intelligence</li>
                            <li>Align strategies and organizational goals</li>
                            <li>Saves time compared to running multiple reports</li>
                            <li>Gain total visibility of all systems instantly</li>
                            <li>Quick identification of data outliers and correlations</li>
                        </ul>
                   
                </div>                                
                                 
            </div>
                    
        </div
           
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
         		Ability to make more informed decisions based on collected business intelligence
            </div>
        </div>            
             
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                 <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Customizable</h2>
                        <p>Standard and Custom Reports available.</p>
                    </header>
                </div>
                
                <BR />
                
                <div class="hiw_Info" style="width:960px;  margin-top:0px; text-align:center;" align="center">
                    
                    <h3>Unlimited Combinations</h3>
                
	                <img style="float:none" src="#rootUrl#/#publicPath#/images/m1/dashboard-half-email.png" width="799" height="397"/>
                </div> 
                             
            </div>
        </div>
    
      
      	<div class="transpacer"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>
