<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Roadmap - <cfoutput>#BrandShort#</cfoutput></title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
        
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
			
			
			.intro2 {
				left: 0;
				padding: 0 0;
				position: relative;
				text-align: center;
				top: 0;
				transform: none;
				width: 100%;
				margin: 20px 0;
			}
			
			.pfblock 
			{
				padding: 2em;
			}

		</style>
        
    </cfoutput>
      
      

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

        
        
<style>
	
.timeline{
	background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home7assets/images/line-grey_3x3.png");
    background-position: center; 
    background-repeat: repeat-y;
    max-width: 960px;
    margin: 0 auto;
    position: relative;
    height: auto;
    padding: 0px;
    clear: both;
    overflow: hidden;
}
.timeline li.year, .timeline li.event{
	list-style-type: none;
	z-index: 999;
}
.timeline li.year{
	clear: both;
	width: 4em;
	margin: 0 auto;
	line-height: 4em;
	background-color: #fff;
	border-radius:50%;
	text-align: center;
	margin-bottom: 25px;
	border: 1px solid #f39c12;
	
	box-shadow: 2px 2px 2px #888888;
	-moz-box-shadow: 2px 2px 2px #888888;
    -webkit-box-shadow: 2px 2px 2px #888888;
}
.timeline li.year:first-child{
	margin-top:0px;
}


.timeline li.event{
	width: 45%;
	background-color: #fff;
	margin-bottom: 20px;
	padding: 20px;
	position: relative;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-color: #e5e6e9 #dfe0e4 #d0d1d5;
    border-image: none;
    border-style: solid;
    border-width: 1px;
}

li.event:nth-child(odd) {
    color: green;
    clear:left;
    float: left;
}
li.event:nth-child(even) {
    color: red;
    clear: right;
    float: right;
    margin-top:100px;
    margin-bottom:50px;
}

.timeline li.event:nth-child(odd)::after {
    background:red;
    content: "";
    height: 2px;
    position: absolute;
    right: -11.2%;
    top: 30px;
    width: 11.2%;
    
}
li.event:nth-child(even)::before{
	background:red;
    content: "";
    height: 2px;
    position: absolute;
    top: 30px;
    width: 11.2%;
    left: -11.2%;
} 

@media (max-width: 640px) {
	.timeline li.event{
		width: 80%;
		background-color: #fff;
		border: 1px solid #ff0000;
		margin: 20px auto;
		padding: 20px;
		position: relative;
	}
	
	li.event:nth-child(odd) {
	    color: green;
	    clear: both;
	    float: none;
	}
	li.event:nth-child(even) {
	    color: red;
	    clear: both;
	    float: none;
	}
	
	.timeline li.event:nth-child(odd)::after, li.event:nth-child(even)::before {
	    width: 0px;
	}
}


@media (max-width: 350px) {
	.timeline li.event{
		width: 98%;
		background-color: #fff;
		border: 1px solid #ff0000;
		margin: 20px auto;
		padding: 20px;
		position: relative;
	}
	
	li.event:nth-child(odd) {
	    color: green;
	    clear: both;
	    float: none;
	}
	li.event:nth-child(even) {
	    color: red;
	    clear: both;
	    float: none;
	}
	
	.timeline li.event:nth-child(odd)::after, li.event:nth-child(even)::before {
	    width: 0px;
	}
}


</style>

    <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					<!---$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}--->
					
							
				});
								
			
			</script>
</head>
            
<cfoutput>
<body class="pfblock-gray">
        
        
        <section id="" class="pfblock pfblock-gray">
                           
            <div class="intro2">
            
                
                    
                    <cfoutput>
                        <a class="" href="home"><img src="#LogoImageLink7#" style="border:none;" /></a>
                    </cfoutput>
                        
               
                            
                <h1>Event Based Messaging</h1>
                <div class="start">Tools for creating and managing modern responsive consumer communication applications</div>
                
            </div>
            
        </section>
    
   <!--- http://www.smarttutorials.net/responsive-facebook-style-timeline-design-with-twitter-bootstrap/ --->
   
   <!---
   
   <!---<span class="month"><i class="fa fa-calendar"></i> &nbsp; March 2015 </span>
			<p>&nbsp;</p>
			<div class="embed-responsive embed-responsive-16by9">
				<iframe frameborder="0" allowfullscreen="allowfullscreen" src="https://www.youtube.com/embed/AGawieZttJ0" class="embed-responsive-item"></iframe>
			</div>
			<p>&nbsp;</p>
			<p>Demo : <a href="http://demo.smarttutorials.net/invoice-script-php/" target="_blank">PHP Invoice System Demo</a></p>
			
			<p>Tutorial : <a href="http://www.smarttutorials.net/invoice-system-using-jquery-php-mysql-bootstrap/" target="_blank">Smart Invoice System</a></p>
			
			<p>This is an awesome tool for your marketing team and they can crack the deal at client’s doorstep. Order PDF Invoice System Script today! and increase sales. It just comes at a fraction of one time price. Grab the deal today.</p>--->
   
   --->
   
   <div class="container">
	<h2 class="title text-center" >Roadmap for Enterprise Class Messaging Programs</h2>
	<ul class="timeline">
		<li class="year">Start</li>
		<li class="event">
			<h3 class="heading">Connectivity</h3>
			
            <ul>
            
            	<li>SFTP</li>                            
                <li>HTTP/HTTPS - via REST API's</li>
                <li>HTTP/HTTPS - via form or request variables</li>
                <li>HTTP/HTTPS - via direct XML</li>
                <li>HTTP/HTTPS - via SOAP</li>
                <li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
                <li>email</li>
                <li>email with password protected zip files</li>
                <li>email with PGP encrypted file</li>
                <li>Direct VPN Tunnel</li>
                <li>Dial up to secure server</li>
                <li>DAT/CD/media via courier</li>
                <li>FAX</li>
                <li>XMPP</li>
                <li>SMPP</li>
                            
            </ul>
            
		</li>
		<li class="event">
			<h3 class="heading">Business Rules - Campaign Definition</h3>
			
            
            
		</li>
        
       <!--- <li class="year">2014</li>--->
		<li class="event">
			<h3 class="heading">Data Structures & Flow</h3>
			
		</li>
		<li class="event">
			<h3 class="heading">Reporting Requirments</h3>
			
		</li>
        
       <!--- <li class="year">2013</li>--->
		<li class="event">
			<h3 class="heading">NOC Monitoring</h3>
		
		</li>
		<li class="event">
			<h3 class="heading">Escalation List</h3>
			
		</li>
        
        <li class="event">
			<h3 class="heading">Approval Process</h3>
			
		</li>
        
        
        <li class="event">
			<h3 class="heading">Legal Review</h3>
			
		</li>
        
        
        <li class="event">
			<h3 class="heading">Provision Channels</h3>
			
            SMS
            Short Codes
            
            Telecomm
            Tollfree
            CID
            
            eMail
            
            From Address
            Reply Address
            SPF - Whitelisting
            Spam Ratings
            
            
		</li>
        
         <li class="event">
			<h3 class="heading">Quality Assurance</h3>
			
		</li>
        
        <li class="event">
			<h3 class="heading">Published Terms and Conditions</h3>
			
		</li>
                    
        <li class="event">
			<h3 class="heading">Continuous Improvment</h3>
			
            <h3 class="heading">Focus Groups</h3>
            <h3 class="heading">Scheduled Program Revies</h3>
            <h3 class="heading">Focus Groups</h3>
            <h3 class="heading">Focus Groups</h3>
		</li>
        
        
        <li class="event">
			<h3 class="heading">Project Plan</h3>
			
            Budget - price, volume discounts, amout, billing contact
            Schedule
            Costs
            Hours
            
            Capacity Planning
            Program Life
            
            
		</li>
        
        
	</ul>
</div>
   
   
    
		<cfinclude template="footer.cfm" />
    
    
    </body>
</cfoutput>
</html>


