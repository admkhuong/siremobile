<cfparam name="inpCS" default="jpijeff@gmail.com" />
<cfparam name="inpCST" default="2" />


<cfparam name="ElectricVehicleFlag" default="0" />
<cfparam name="ForwardForFulfillment" default="" />
<cfparam name="AlreadyBeenSentFlag" default="0" />
<cfparam name="CustomerName" default="John Q. Customer" />
<cfparam name="GraphSource_1" default="" />
<cfparam name="CouponCode" default="12345678" />
   
<cfinclude template="paths.cfm" >

<cfsavecontent variable="outContent">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Account Monitoring</title>
<STYLE type="text/css">
.ReadMsgBody {
	width: 100%;
} /* Forces Hotmail to display emails at full width */
.ExternalClass {
	width: 100%;
} /*Hotmail table centering fix*/
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}  /* Forces Hotmail to display normal line spacing*/
p {
	margin: 1em 0;
} /*Yahoo paragraph fix*/
table td {
	border-collapse: collapse;
}  /*This resolves the Outlook 07, 10, and Gmail td padding issue fix*/
.appleLinks a {
	color: #4b4b4b;
	text-decoration: none;
}
 @media only screen and (max-width: 480px) {
td[class="mobile-hidden"] {
	display: none !important;
}
span[class="hide"] {
	display: none !important;
}
td[class="info-text"] {
	font-size: 18px !important;
}
td[class="title"] {
	font-size: 24px !important;
}
td[class="tertitle"] {
	font-size: 24px !important;
}
td[class="body"] {
	font-size: 20px !important;
}
td[class="account"] {
	font-size: 18px !important;
	padding: 8px 3px !important;
	background-image: none !important;
}
a[class="account"] {
	font-size: 18px !important;
}
td[class="link"] {
	font-size: 20px !important;
	width: 100% !important;
	float: left !important;
}
a[class="link"] {
	font-size: 20px !important;
}
a[class="feature-link"] {
	font-size: 16px !important;
}
td[class="subtitle"] {
	font-size: 20px !important;
}
td[class="subtext"] {
	font-size: 20px !important;
}
td[class="nav"] {
	font-size: 18px !important;
	padding: 8px 0 !important;
	line-height: 30px !important;
}
td[class="share"] {
	text-align: left !important;
	width: 100% !important;
	float: left !important;
	padding-top: 15px !important;
}
td[class="footer"] {
	font-size: 15px !important;
}
a[class="footer"] {
	font-size: 15px !important;
}
span[class="break"] {
	display: block !important;
}
}
</style>
</head>
                                <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body bgcolor="#ffffff" style="margin: 0px; padding:0px; -webkit-text-size-adjust:none;">
	
	
<!-- Table used to center email -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
	
	<div>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="604">
                                <tbody>
                                                <tr>
                                                                <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                                <td colspan="3">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="602">
                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                                <td><img alt="Chase logo" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_1.gif" height="25" hspace="22" vspace="20" width="130"></td>
                                                                                                                                <td width="100%"></td>
                                                                                                                                <td><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_2.gif" height="31" width="138"></td>
                                                                                                                </tr>
                                                                                                </tbody>
                                                                                </table>
                                                                </td>
                                                </tr>
                                                <tr>
                                                                <td colspan="3"><img alt="Welcome to Chase" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_3.gif" height="24" width="604"></td>
                                                </tr>
                                                <tr>
                                                                <td colspan="3">
                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="604">
                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                                <td bgcolor="#0467bc" width="1"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_2.gif" height="1" width="1"></td>
                                                                                                                                <td width="602">
                                                                                                                                                <table border="0" cellpadding="0" cellspacing="20" width="602">
                                                                                                                                                                <tbody>
                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td style="text-align:center">
                                                                                                                                                                                12/23/2013        
                                                                                                                                                                                                </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                                <tr>
                                                                                                                                                                                                <td>
                                                                                                                                                                                                                <p style="margin-top: 1.3em;"><font face="arial" size="2">Dear <cfoutput>#CustomerName#</cfoutput>,</font></p><p style="margin-top: 1.3em;"><font face="arial" size="2"> We are following up on our recent letter to offer you an additional year-for a total of two years-of credit monitoring free of charge.</font></p>
                                                                                                                                                                                                                <p style="margin-top: 1.3em;"><font face="arial" size="2">We are offering you credit monitoring as a precaution because some of your personal information may have been viewed improperly on databases that support our website for your #cardVar#.</font></p>
                                                                                                                                                                                                                <p style="margin-top: 1.3em;"><font face="arial" size="2">Please visit <a href="http://info.chase.publicweblink.com/itac2.cfm?code=SNP8A%2D8CB7%2D%3F345%20">http://info.chase.publicweblink.com/itac2.cfm?code=#code# </a> for important information describing the benefits of ITAC Sentinel?? Plus and how to enroll. To enroll directly, you may also visit the ITAC Sentinel?? Plus website at <a href="www.itacsentinel.com/alert">www.itacsentinel.com/alert</a>. You will need to enter your New Redemption Code: <b><cfoutput>#CouponCode#</cfoutput></b>  to begin the enrollment process.</font></p><p style="margin-top: 1.3em;"><font face="arial" size="2">If you already enrolled, we will extend your coverage for an additional year free of charge. You do not need to take any action.</font></p><p style="margin-top: 1.3em;"><font face="arial" size="2">We ask that you monitor your accounts as well. If you see any purchases that you don't recognize, please call the number on the back of your card. </font></p><p style="margin-top: 1.3em;"><font face="arial" size="2">If you have any questions, please call us at (866)849-5255. </font></p><p style="margin-top: 1.3em;"><font face="arial" size="2">Sincerely,</font><br>
<img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/sign.png" height="50" width="180">
<br><font face="arial" style="font-size:11px">Patricia O. Baker<br>Senior Vice President<br>Chase Executive Office</font></p><br>
 
<p style="margin-top: 1.3em;"><font face="arial" size="2"><b>??Habla espa??ol?</b>  Estamos dando seguimiento a nuestro reciente email para ofrecerle un a??o adicional-por un total de dos a??os-de monitoreo de cr??dito gratuito. Algunos de sus datos personales pueden haberse visto indebidamente en las bases de datos que apoyan nuestro sitio web para su(s) tarjeta(s) #cardVar#. Para recibir asistencia en espa??ol o si tiene alguna pregunta sobre este aviso, por favor comun??quese con Atenci??n al Cliente al 1-866-849-5255.</font></p>
 
<!---<p style="margin-top: 1.3em;"><font face="arial" size="2"><b>&iquest;Habla espa&ntilde;ol?</b>  La seguridad de su informaci&#243;n es muy importante para nosotros y nos esforzamos para manejarla con cuidado y discreci&#243;n. Le escribimos para alertarle que algunos de sus datos personales pueden haber sido vistos de forma indebida en algunas bases de datos de respaldo de nuestro sitio web para las tarjetas NEVADA EBT. Para recibir asistencia en espa&#324;ol o si tiene alguna pregunta sobre este aviso, por favor comun&#237;quese con Atenci&#243;n al Cliente en el 1-866-849-5255.</font></p>--->
 
<!--- <p style="margin-top: 1.3em;"><font face="arial" size="2"><b>&iquest;Habla espa&ntilde;ol?</b> La seguridad de su informaci&#243;n es muy importante para nosotros y nos esforzamos para manejarla con cuidado y discreci&#243;n. Le escribimos para alertarle que algunos de sus datos personales pueden haber sido vistos de forma indebida en algunas bases de datos de respaldo de nuestro sitio web para las tarjetas NEVADA EBT. Para recibir asistencia en espa&ntilde;ol o si tiene alguna pregunta sobre este aviso, por favor comun&igrave;quese con Atenci&#243;n al Cliente al 1-866-849-5255.</font></p> --->
 
<p style="text-align:right"><span style="font-size:9px">LCUCEXT1213<br><br>PRI-12088551</span></p>
 
                                                                                                                                                                                <!--- <p style="text-align: center;"><font face="Arial" size="1">If you wish to opt out of future notification E-mails, click below.<br><a href=abc.com?inpId=111_111>Unsubscribe</a></font></p> --->
                                                                                                                                                               </td>
                                                                                                                                                </tr>
                                                                                                                                </tbody>
                                                                                                                </table>
                                                                                                </td>
                                                                                                <td bgcolor="#0467bc" width="1"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_2.gif" height="1" width="1"></td>
                                                                                                                </tr>
                                                                </tbody>
                                                </table>
                                </td>
                </tr>
                <tr>
                                <td colspan="3" bgcolor="#0467bc"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/cam1_files/image_1_2.gif" height="1" width="1"></td>
                </tr>
        </tbody>
                </table>
    <br>
    <!--- <table align="center" bgcolor="#0467bc" border="0" cellpadding="0" cellspacing="1" width="604">
                <tbody>
                <tr>
                                <td bgcolor="#ffffff">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                                <tr>
                                                                                                <td>
                                                                                                                <table bgcolor="#0467bc" border="0" cellpadding="0" cellspacing="5" width="170">
                                                                                                                                <tbody>
                                                                                                                                                <tr>
                                                                                                                                                                <td><b><font color="#ffffff" face="Arial" size="1">E-mail Security Information</font></b></td>
                                                                                                                                                </tr>
                                                                                                                                </tbody>
                                                                                                                </table>
                                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                                <td>
                                                                                                                <table border="0" cellpadding="0" cellspacing="8">
                                                                                                                                <tbody>
                                                                                                                                                <tr>
                                                                                                                                                                <td><p><b><font face="Arial" size="1">AVOID Scams: Remember, Chase will NEVER contact you via e-mail for confidential financial or personal information about your Chase card (card number, PIN, Social Security Number, etc).&nbsp;&nbsp;If you ever get an e-mail asking for this type of information, do not reply and do not click on any links within the e-mail; delete it immediately.</font></b><br><font face="Arial" size="1"><br>Please do not reply to this automated customer service message.&nbsp;&nbsp;For customer service, visit <a href=http://www.ucard.chase.com/>http://www.ucard.chase.com</a> and click on "Self Service".&nbsp;&nbsp;You can also call the toll-free Customer Service number on the back of your card.</font></p><p><font face="Arial" size="1"><b>Note:</b>&nbsp;&nbsp;If you are concerned about clicking links in this e-mail, the Chase Online services mentioned above can be accessed by typing <a href=http://www.ucard.chase.com/>http://www.ucard.chase.com</a> directly into your browser.<br></font></p><div style="text-align: center;"><font face="Arial" size="1">If you wish to opt out of future notification E-mails, click below.<br><a href=abc.com?inpId=111_111>Unsubscribe</a></font><br></div></td>
                                                                                                                                                </tr>
                                                                                                                                </tbody>
                                                                                                                </table> --->
                                                                                                </td>
                                                                                </tr>
                                                                                                                <tr>
                                                                                                                                <td>
                                                                                                                                               
                                                                                                                                </td>
                                                                                                                </tr>
                                                                </tbody>
                                                </table>
                                </td>
                </tr>
        </tbody>
                </table>
</div>
<div align="center">If you would like to unsubscribe and stop receiving these emails <a href="http://ml01.chase.mbdt01.com/wf/unsubscribe?upn=-2FydMWf8GxE6TL6eILThdsRi8WB5ynFskTZR2rNyLo4i2zbuZNmgmwlUqhGBD-2FptZNNBYoYpmTHPCsxOHfabTyitjQUvzy8ZiH-2FVL6sd9nBL7zFa5s57eV4xGufpaT6vI3lO6iY5eRi3WYhzF-2FS52sB9MctM-2FLcj0de-2FZP7g2-2BgglpVCRzLgcOrPVBp0bAV61-2B2luAyxhxVSo-2B4MOwDpYY0NdyK1OP7dDdrdtmMmBm0s-3D">click here</a>.
<img src="http://ml01.chase.mbdt01.com/wf/open?upn=-2FydMWf8GxE6TL6eILThdsRi8WB5ynFskTZR2rNyLo4i2zbuZNmgmwlUqhGBD-2FptZ0ArvD2R0vRS5D8sFMSXDlsarLhiT07e3RozVWxAQXoI9KZxIIDZEfYXyki32kSqSfal5tpEzdEz5ZLOkFuFN9BKuVN0-2FDQ4Yc2Mgn-2BjSN4ZU0ZASbat0oecUS5Y0o4jRyb8hej-2FLi03i2mPjtCOQW8zX0NHcX-2B1o0QDHHsIve1KddWwY4QxPMoVQQjxTqTg0" alt="" width="1" height="1" border="0" style="height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;"></div>

	</div>
    </td>
  </tr>
</table>


</body></html>
 
 
 

</cfsavecontent>


<cfoutput>

	<cfif TRIM(ForwardForFulfillment) NEQ "">
        <pre name="code" class="html">#outContent#</pre>
    <cfelse>
        #outContent#
    </cfif>

</cfoutput>


<link type="text/css" rel="stylesheet" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/css/syntaxhighlighter.css"></link>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shcore.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushcsharp.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushxml.js"></script>
<script language="javascript">
dp.SyntaxHighlighter.ClipboardSwf = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/clipboard.swf';
dp.SyntaxHighlighter.HighlightAll('code');
</script>




