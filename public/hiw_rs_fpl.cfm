<cfparam name="inpCS" default="jpijeff@gmail.com" />
<cfparam name="inpCST" default="2" />


<cfparam name="ElectricVehicleFlag" default="0" />
<cfparam name="ForwardForFulfillment" default="" />
<cfparam name="AlreadyBeenSentFlag" default="0" />
<cfparam name="CustomerName" default="John Q. Customer" />
<cfparam name="GraphSource_1" default="" />
 
<cfinclude template="paths.cfm" >
  
<cfsavecontent variable="outContent">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FPL</title>
<STYLE type="text/css">
.ReadMsgBody {
	width: 100%;
} /* Forces Hotmail to display emails at full width */
.ExternalClass {
	width: 100%;
} /*Hotmail table centering fix*/
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}  /* Forces Hotmail to display normal line spacing*/
p {
	margin: 1em 0;
} /*Yahoo paragraph fix*/
table td {
	border-collapse: collapse;
}  /*This resolves the Outlook 07, 10, and Gmail td padding issue fix*/
.appleLinks a {
	color: #4b4b4b;
	text-decoration: none;
}
 @media only screen and (max-width: 480px) {
td[class="mobile-hidden"] {
	display: none !important;
}
span[class="hide"] {
	display: none !important;
}
td[class="info-text"] {
	font-size: 18px !important;
}
td[class="title"] {
	font-size: 24px !important;
}
td[class="tertitle"] {
	font-size: 24px !important;
}
td[class="body"] {
	font-size: 20px !important;
}
td[class="account"] {
	font-size: 18px !important;
	padding: 8px 3px !important;
	background-image: none !important;
}
a[class="account"] {
	font-size: 18px !important;
}
td[class="link"] {
	font-size: 20px !important;
	width: 100% !important;
	float: left !important;
}
a[class="link"] {
	font-size: 20px !important;
}
a[class="feature-link"] {
	font-size: 16px !important;
}
td[class="subtitle"] {
	font-size: 20px !important;
}
td[class="subtext"] {
	font-size: 20px !important;
}
td[class="nav"] {
	font-size: 18px !important;
	padding: 8px 0 !important;
	line-height: 30px !important;
}
td[class="share"] {
	text-align: left !important;
	width: 100% !important;
	float: left !important;
	padding-top: 15px !important;
}
td[class="footer"] {
	font-size: 15px !important;
}
a[class="footer"] {
	font-size: 15px !important;
}
span[class="break"] {
	display: block !important;
}
}
</style>
</head>
<!-- ET10.12 -->
<body bgcolor="#ffffff" style="margin: 0px; padding:0px; -webkit-text-size-adjust:none;">

<!-- Table used to center email -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right" valign="top" style="font:10px Verdana, geneva, sans-serif; color:#FFFFFF;">Your energy-saving news from FPL</td>
          </tr>
          <tr>
            <td class="mobile-hidden" align="right" valign="top" style="font:10px Verdana, geneva, sans-serif; color:#797979; padding-bottom:10px;"><a href="http://ebmdevii.messagebroadcast.com/public/hiw_rs_fpl?qs=2096c3f3debfc283310453c5d652d653d1dc22a61f0eae863fa7ced7f2540e5cff00855aeda0a9ea<cfoutput><cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif></cfoutput>"  target="_blank" style="color:#797979;">View this email in your browser</a>.</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding-bottom:15px;"><table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                  <td valign="bottom"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283b62f2953ba28ef7d2618ce5f52955414cbcb02371ffb5b838a64a9ed41f64c82" target="_blank" ><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL_logo_CORRECT80h.jpg" alt="FPL" title="FPL" width="95" height="81" border="0" style="display:block; color:#0096db;  font:25px Verdana; font-weight:bold;" /></a></td>
                  <td align="right" valign="bottom"><table  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="info-text" style="font-family:Verdana; font-size:13px; text-align:right; color:#464646 ;" align="right" ><cfoutput>#CustomerName#</cfoutput><br />
                          Account Number: 12345678 </td>
                      </tr>
                      <tr>
                        <td style="padding-top:8px;" align="right"><table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_L.jpg" 
                                             bgcolor="#74B943" style="background-repeat: no-repeat;" width="5" height="22" class="account"></td>
                              <td background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_Ctile.jpg" 
                                           style="font-family:Verdana; font-size:11px; color:#ffffff;
                                                           font-weight:bold; padding:0px 5px; background-repeat: repeat-x;" align="center" bgcolor="#74B943" class="account"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283ae868f50d87400e27c124eed556eb651393776878e2618e4f6bc0ac5b9331ab9" class="account" target="_blank" style="font-family:Verdana; font-size: 11px; color: #ffffff; text-decoration: none; font-weight: bold;" ><span style="color:#ffffff;">View my account</span></a></td>
                              <td class="account" background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_R.jpg" 
                                             bgcolor="#74B943" style="background-repeat: no-repeat;" width="5" height="22"></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
        </table>
        
        <!-- END Table use to set width of email --> 
      </div></td>
  </tr>
  <tr>
    <td bgcolor="#e9e9e9" style="padding:0px 10px; font-size:0%;"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="2" height="2" alt="" border="0" title="" /></td>
  </tr>
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px; font-size:0%;"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="2" height="2" alt="" border="0" title="" /></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#0096db" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="nav" align="left" style="font-family:Verdana, geneva, sans-serif; font-size:13px; color:#ffffff;"><b>Your&nbsp;October&nbsp;Newsletter</b>&nbsp;&nbsp;&nbsp;&nbsp;<span class="break" style="display:none;"></span><a href="http://click.fplemail.com/?qs=2096c3f3debfc2837b8e93115871e9929ed0ca50f28bec256198767d3b29536121fc5a6bc7594817" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Log&nbsp;in</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc2833b8dc2267422886b99919937743e77d174f99cb62e019fb06f932eb6ebc28289" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Pay&nbsp;bill</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://click.fplemail.com/?qs=2096c3f3debfc283af3019d27ffd0c9847c2e4a82aa1a89961949b8e3226f929db7ba3cbe33d7dc2" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">FPL.com</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  <tr>
    <td bgcolor="#d6effa" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><!-- 

 -->
              
              <table width="560" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="560" valign="top" align="left" bgcolor="#d6effa" style="padding-right:10px; padding-left:10px; padding-bottom:30px;"><table width="540" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="540" align="left" valign="top" bgcolor="#d6effa" style="padding-top:25px;"><table width="540" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left" valign="top" width="298"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td align="left" valign="top" class="title" width="298" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; color:#006dcc; padding-bottom:15px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2839ddaa0f34234676c7876f552b713683888d077c71269384a14df33291727dc00"  target="_blank" style="color:#006dcc; text-decoration:none;">Win a $5,000 home <br />
                                      energy makeover</a></td>
                                  </tr>
                                  <tr>
                                    <td class="body" align="left" valign="top" width="298" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:15px; color:#797979; padding-right:25px; padding-bottom:26px;">Change the current way you use energy and make your bill even lower by taking our free Online Home Energy Survey. Complete the survey by Oct. 31 for a chance to win a $5,000 home energy makeover.</td>
                                  </tr>
                                  <tr>
                                    <td width="290" align="left" valign="top"><table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td width="6" bgcolor="#71c74c" align="right" valign="top" background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" style="background-repeat: no-repeat;"><img alt="" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201306_smb_Biz_Arrow02.jpg" style="display: block;" border="0" title="" width="8" height="42" /></td>
                                          <td bgcolor="#71c74c" align="center" valign="middle" background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL_201304Res_Feature_btnRepeat.jpg" style="background-repeat: repeat-x;"><table border="0" cellpadding="0" cellspacing="0">
                                              <tr> 
                                                <!-- BUTTON -->
                                                <td style="padding:0px 5px 0px 10px;"><a class="feature-link" href="http://click.fplemail.com/?qs=2096c3f3debfc2839ddaa0f34234676c7876f552b713683888d077c71269384a14df33291727dc00"  target="_blank" style="font-family: Verdana; font-size: 14px; font-weight: bold; color: #ffffff; text-decoration: none;"><span style="color: #ffffff;">Enter today</span></a></td>
                                              </tr>
                                            </table></td>
                                          <!-- ARROW -->
                                          <td width="28" bgcolor="#71c74c" align="right" valign="top" background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" style="background-repeat: no-repeat;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2839ddaa0f34234676c7876f552b713683888d077c71269384a14df33291727dc00"  target="_blank" style="text-decoration: none; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;"><img alt="" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201306_smb_Biz_Arrow01.jpg" style="display: block;" border="0" width="32" height="42" /></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                  <tr>
                                    <td align="left" class="share" style="padding-top:10px;"><table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td class="subtext" style="font:11px Verdana, geneva, sans-serif; color:#797979; padding-right:2px;" align="center" valign="bottom" width="48"><strong>Share:</strong></td>
                                          <td width="16" valign="middle" align="left"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283a21d524ce097e7172200f3ef1d518d3385813b09a22d64c3b030804f3a436dcf"  title="Forward this email" target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FTF.jpg" width="16" height="16" alt="Forward to Friend" border="0" title="Forward to Friend" /></a></td>
                                          <td width="16" valign="middle" align="left" style="padding-left:4px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28315bcec1520cfca8b71f7fc3250f953787dff0d35894665c6b44260066074f446"  title="Share on Facebook" target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FB.jpg" width="16" height="16" alt="Facebook" border="0" title="Facebook" /></a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></td>
                              <td width="242" align="left" valign="top" bgcolor="#d6effa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="242" align="left" valign="top" bgcolor="#d6effa" ><a href="http://click.fplemail.com/?qs=2096c3f3debfc2839ddaa0f34234676c7876f552b713683888d077c71269384a14df33291727dc00"  target="_blank"><img style="display:block; font:15px Verdana, geneva, sans-serif; font-weight:bold; color:#797979;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_Hero.jpg" alt="" border="0" height="281" width="242" /></a></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </div></td>
  </tr>
  
  
  
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/20120829_fpl_shadow.jpg" width="540" height="40" border="0" style="display:block;"></td>
          </tr>
  
  
   		  <cfif ElectricVehicleFlag EQ 0 >
  
  
          <tr>
            <td align="left"><table width="540" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="padding-bottom:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="120" valign="top"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_electric_vehicles.jpg" style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" width="120" height="120" alt="" border="0" /></a></td>
                        <td width="20" valign="top"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="20" height="1" border="0" alt="" /></td>
                        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="title" valign="top" align="left" style="font-size:18px; font-family:Verdana, geneva, sans-serif; font-weight:bold; color:#49ab08; padding-bottom:8px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#49ab08; text-decoration:none;"><span style="color:#49ab08;">Get charged up about electric vehicles</span></a></td>
                            </tr>
                            <tr>
                              <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:15px;">Want to learn the latest on electric vehicles? On our new website you can find out what models are on the market today, review vehicle fact sheets and locate charging stations near you.</td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="link" style="font-family:Verdana, geneva, sans-serif; font-size:13px; font-weight:bold; color:#006dcc;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">Check out our new website</span></a> &raquo;</td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          
          </cfif>
          
          <tr>
            <td align="left"><!--

  -->
              
              <table width="540" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="padding-bottom:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="120" valign="top"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28397d8a4af2321bd36dd1374338320c00345a4256b3e70f7babb696fe8aae85925"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_school.jpg" style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" width="120" height="120" alt="" border="0" /></a></td>
                        <td width="20" valign="top"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="20" height="1" border="0" alt="" /></td>
                        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="title" valign="top" align="left" style="font-size:18px; font-family:Verdana, geneva, sans-serif; font-weight:bold; color:#49ab08; padding-bottom:8px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28397d8a4af2321bd36dd1374338320c00345a4256b3e70f7babb696fe8aae85925"  target="_blank" style="color:#49ab08; text-decoration:none;"><span style="color:#49ab08;">Get money for your school</span></a></td>
                            </tr>
                            <tr>
                              <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:15px;">We want to help teachers educate the next generation of leaders. Parents, encourage your teachers to apply for our <a href="http://click.fplemail.com/?qs=2096c3f3debfc28397d8a4af2321bd36dd1374338320c00345a4256b3e70f7babb696fe8aae85925"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">teacher grants</span></a>. All teachers in our service territory are eligible.</td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="link" style="font-family:Verdana, geneva, sans-serif; font-size:13px; font-weight:bold; color:#006dcc;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28397d8a4af2321bd36dd1374338320c00345a4256b3e70f7babb696fe8aae85925"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">Learn about teacher grants</span></a> &raquo;</td>
                                    <td align="right" class="share"><table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td class="subtext" valign="bottom" style="font:11px Verdana, geneva, sans-serif; color:#797979; padding-right:4px;"><strong>Share:</strong></td>
                                          <td width="16" valign="middle" align="left"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283edba98fb46bbec325b1048ec88f68d7451fb84e3c3aeba494b155805db59ef1e"  target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FTF.jpg" width="16" height="16" alt="Forward to Friend" border="0" /></a></td>
                                          <td width="16" valign="middle" align="left" style="padding-left:4px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28339d560a34037e6bdacd53bb527c37b25fa4d20ee93aaa3fc70e279c0885e46ec"  target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FB.jpg" width="16" height="16" alt="Facebook" border="0" /></a></td>
                                        </tr>
                                      </table></td>
                                    <td align="right">&nbsp;</td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td align="left"><!-- 

 -->
              
              <table width="540" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="padding-bottom:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="120" valign="top"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283b7ecca80516d68f20cba3d55b6dbac4266fc1bc171622161571432ee0163334e"  target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/20121008_secondary_ask.jpg" width="120" height="120" alt="" border="0" /></a></td>
                              <td width="20" valign="top"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="20" height="1" border="0" alt="" /></td>
                              <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="subtitle" valign="top" align="left" style="font:12px Verdana, geneva, sans-serif; font-weight:normal; color:#797979; padding-bottom:5px;">You asked. We listened.</td>
                                  </tr>
                                  <tr>
                                    <td class="title" valign="top" align="left" style="font:18px Verdana, geneva, sans-serif; font-weight:bold; color:#49ab08; padding-bottom:8px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283b7ecca80516d68f20cba3d55b6dbac4266fc1bc171622161571432ee0163334e"  target="_blank" style="color:#49ab08; text-decoration:none;"><span style="color:#49ab08;">Ask the Energy Expert: Cooking myths</span></a></td>
                                  </tr>
                                  <tr>
                                    <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:2px;"><i>-Peggy, via <a class="link" href="http://click.fplemail.com/?qs=2096c3f3debfc2839c139ba01d01c1a41b4192fa9848b0555abadbb44a85dd813e79b68409600c39"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">www.FPLblog.com</span></a> </i></td>
                                  </tr>
                                  <tr>
                                    <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:15px; padding-top:7px;">What uses the least amount of energy when cooking: A microwave, electric oven or slow cooker?</td>
                                  </tr>
                                  <tr>
                                    <td style="padding-bottom:0px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td class="link" style="font-family:Verdana, geneva, sans-serif; font-size:13px; font-weight:bold; color:#006dcc;"><a class="link" href="http://click.fplemail.com/?qs=2096c3f3debfc283b7ecca80516d68f20cba3d55b6dbac4266fc1bc171622161571432ee0163334e"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">Learn the answer</span></a> &raquo;</td>
                                          <td align="right" class="share"><table border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td class="subtext" valign="bottom" style="font:11px Verdana, geneva, sans-serif; color:#797979; padding-right:4px;"><strong>Share:</strong></td>
                                                <td width="16" valign="middle" align="left"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28378ffb9b8bf7a1194d57f5a3641e01d6201b6fc72a894c0947c243a2965e3e8f7"  title="Forward this email" target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FTF.jpg" width="16" height="16" alt="Forward to Friend" border="0" /></a></td>
                                                <td width="16" valign="middle" align="left" style="padding-left:4px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2839248dfb6aa457e0ffd63dc5b341a3b75eb6089e1a0e2f706f4722d57cfb995c9"  target="_blank"><img style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201209_SeptNewsletter_FB.jpg" width="16" height="16" alt="Facebook" border="0" /></a></td>
                                              </tr>
                                            </table></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="540">
                <tbody>
                  <tr>
                    <td style="padding-bottom: 40px;"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td bgcolor="#A9D5FC"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283063952587175eb2fefbc1dde32a5c330f8fb67c58e8b5e10d5af4450e4f0687c"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_budget_billing.jpg" alt="BUDGETING IS A BREEZE WITH FPL BUDGET BILLING(R). OUR BUDGET BILLING PROGRAM EVENS OUT YOUR MONTHLY BILLS, MAKING BUDGET BILLING CUSTOMERS AMONG OUR MOST SATISFIED. Say goodbye to bill fluctuations &raquo;" style="display: block; font-style: normal; font-variant: normal; font-size: 12px; line-height: normal; font-family: Verdana, geneva, sans-serif; font-weight: bold; color: #ffffff;" border="0" height="122" width="540" /></a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="540">
                <tbody>
                  <tr>
                    <td style="padding-bottom: 40px;"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td bgcolor="#98CCFE"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283586e384497931a6a0e2368ca4870d7fca2a389ceb5cbed60c860e46def1f2333"  target="_blank"> <img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_power_lines.jpg" alt="SPECIALIZED LINEWORKERS REPAIR ENERGIZED POWER LINES. OUR SPECIALIZED LINEWORKERS CAN REPAIR LIVE POWER LINES CARRYING AS MUCH AS 500,000 VOLTS OF ELECTRICITY, WHICH IS 4,000 GREATER THAN WHAT IS USED TO POWER THE TYPICAL HOME. See how we keep your power reliable &raquo;" style="display: block; font-style: normal; font-variant: normal; font-size: 12px; line-height: normal; font-family: Verdana, geneva, sans-serif; font-weight: bold; color: #ffffff;" border="0" height="122" width="540" /></a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="540">
                <tbody>
                  <tr>
                    <td style="padding-bottom:40px;"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td bgcolor="#E9F2FB"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283517747f14ea043a9dcdca0010062ff21178d1d0dc5228dcfa552b41c043ae25b"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_pirated_energy.jpg" alt="Roughly $6 billion of electricity is pirated each year in the U.S. Learn what it could be costing you &raquo;" style="display:block; font:14px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" border="0" height="133" width="260"></a></td>
                            <td width="20" style="font-size:0%;"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" alt="" title="" border="0" height="2" width="20"></td>
                            <td bgcolor="#E9F2FB"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283166e4bac67163f650e8350f99ba9df1c202fa2c3a27160ebb15a7ef055fd433c"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/201310_res_cleared_vegetation.jpg" alt="Scince 2006, we've cleared vegetation from 100,000 miles of power lines. See improvements near you &raquo;" style="display:block; font:14px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" border="0" height="133" width="260"></a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td style="padding-bottom:20px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2835db2f9c394c49fa3297c7d7b5198f547761a4c8c6a15d525c4ce25149cf217aa" target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/20120829_fpl_social_image.jpg" width="540" height="50" border="0"></a></td>
          </tr>
          <tr>
            <td align="right" style="padding-bottom:10px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2832b0444ce9e61a4c92669e83de83c38bd6bfcf6c910a98cd98ad557499a697e9a" target="_blank" ><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL_CTC_Logo_CORRECT.jpg" width="261" height="50" border="0" style="display:block; color:#0096db; font:14px Verdana, geneva, sans-serif; font-weight:bold;" alt="FPL: Changing the current."></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#0096db" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="nav" align="center" style="font-family:Verdana, geneva, sans-serif; font-size:13px; color:#ffffff;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283668d6bec08dd4e2cda8c30eb00f6ce6ba54d3be9dbe9ea3057cadb013cd300a7" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Join the Power Panel</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc283e4ab4389f4a3957640cb6ed4397f3c3406be0f3953eb0d12dd2edf234f844da5" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">About&nbsp;us</span></a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide">|</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="break" style="display:none;"></span> <a href="http://click.fplemail.com/?qs=2096c3f3debfc283c384ac759aa4ef8e06d4e82b519446fb4c9beb9cbb895d87e0143d3d2175783e" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Contact&nbsp;us</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc283cbb7ec86ba36b4e760b631cbe864d255dd2fdcf1ba90716f99f3356175a70dd4" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Feedback [ + ]</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table cellpadding="0" cellspacing="0" border="0" width="540">
          <tr>
            <td class="footer" align="center" style="padding: 20px 0px 0px 0px; font-family: Verdana; font-size: 10px; color: #797979; border-bottom: none !important;">As a valued customer of Florida Power &amp; Light Company, you have received this email to provide you with information that may interest you. Please add <a href="mailto:FPL_Account_Management@reply.fplemail.com" target="_blank" style="color: #777777; text-decoration: none;">FPL_Account_Management@reply.fplemail.com</a> to your address book.</td>
          </tr>
          <tr>
            <td class="footer" align="center" style="padding: 10px 0px 0px 0px; font-family: Verdana; font-size: 10px; color: #797979;">Florida Power & Light Company, <span class="appleLinks7">700 Universe Blvd., Juno Beach, FL 33408, USA</span></td>
          </tr>
          <tr>
            <td align="center" style="padding: 0px 0px 15px 0px; font-family: Verdana; font-size: 10px; color: #797979;"><a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6ac6aed4693c6aeaa3b01ed2437fbb91edc41dd5d710e90574c025b831f660efa3"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Unsubscribe</span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6a8de9da1499e8ba37ac87cf0b5afc419ae3c40fd5d24ece5897053ce40ea994c2"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Change email frequency</span></a>&nbsp;&nbsp;|&nbsp;&nbsp; <a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6a0321f3d38ab060a7864df75c5454cf4b3cabff139cfdc5bc864be29cc160fb97"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Privacy policy</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
</table>
<!-- END Table used to center email --> 
<img src="http://click.fplemail.com/open.aspx?ffcb10-fe6a15767165067a7514-fdfd13707363047d75117777-fe6e15707764037e7510-fe9816737360017d76-fe27137473600c7d751374-ffce15&d=10033" width="1" height="1">
</body>
</html>

</cfsavecontent>


<cfoutput>

	<cfif TRIM(ForwardForFulfillment) NEQ "">
        <pre name="code" class="html">#outContent#</pre>
    <cfelse>
        #outContent#
    </cfif>

</cfoutput>


<link type="text/css" rel="stylesheet" href="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/css/syntaxhighlighter.css"></link>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shcore.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushcsharp.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushxml.js"></script>
<script language="javascript">
dp.SyntaxHighlighter.ClipboardSwf = '<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/syntaxhighlighter/js/clipboard.swf';
dp.SyntaxHighlighter.HighlightAll('code');
</script>

