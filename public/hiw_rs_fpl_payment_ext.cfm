<cfparam name="inpCS" default="jpijeff@gmail.com" />
<cfparam name="inpCST" default="2" />


<cfparam name="ElectricVehicleFlag" default="0" />
<cfparam name="ForwardForFulfillment" default="" />
<cfparam name="AlreadyBeenSentFlag" default="0" />
<cfparam name="CustomerName" default="John Q. Customer" />
<cfparam name="GraphSource_1" default="" />
<cfparam name="GraphSource_2" default="" />

<cfinclude template="paths.cfm" >
   
<cfsavecontent variable="outContent">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FPL</title>
<STYLE type="text/css">
.ReadMsgBody {
	width: 100%;
} /* Forces Hotmail to display emails at full width */
.ExternalClass {
	width: 100%;
} /*Hotmail table centering fix*/
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
	line-height: 100%;
}  /* Forces Hotmail to display normal line spacing*/
p {
	margin: 1em 0;
} /*Yahoo paragraph fix*/
table td {
	border-collapse: collapse;
}  /*This resolves the Outlook 07, 10, and Gmail td padding issue fix*/
.appleLinks a {
	color: #4b4b4b;
	text-decoration: none;
}
 @media only screen and (max-width: 480px) {
td[class="mobile-hidden"] {
	display: none !important;
}
span[class="hide"] {
	display: none !important;
}
td[class="info-text"] {
	font-size: 18px !important;
}
td[class="title"] {
	font-size: 24px !important;
}
td[class="tertitle"] {
	font-size: 24px !important;
}
td[class="body"] {
	font-size: 20px !important;
}
td[class="account"] {
	font-size: 18px !important;
	padding: 8px 3px !important;
	background-image: none !important;
}
a[class="account"] {
	font-size: 18px !important;
}
td[class="link"] {
	font-size: 20px !important;
	width: 100% !important;
	float: left !important;
}
a[class="link"] {
	font-size: 20px !important;
}
a[class="feature-link"] {
	font-size: 16px !important;
}
td[class="subtitle"] {
	font-size: 20px !important;
}
td[class="subtext"] {
	font-size: 20px !important;
}
td[class="nav"] {
	font-size: 18px !important;
	padding: 8px 0 !important;
	line-height: 30px !important;
}
td[class="share"] {
	text-align: left !important;
	width: 100% !important;
	float: left !important;
	padding-top: 15px !important;
}
td[class="footer"] {
	font-size: 15px !important;
}
a[class="footer"] {
	font-size: 15px !important;
}
span[class="break"] {
	display: block !important;
}
}
</style>
</head>
<!-- ET10.12 -->
<body bgcolor="#ffffff" style="margin: 0px; padding:0px; -webkit-text-size-adjust:none;">

<!-- Table used to center email -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right" valign="top" style="font:10px Verdana, geneva, sans-serif; color:#FFFFFF;"></td>
          </tr>
          <tr>
            <td class="mobile-hidden" align="right" valign="top" style="font:10px Verdana, geneva, sans-serif; color:#797979; padding-bottom:10px;"><a href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/hiw_rs_fpl_payment_ext.cfm?<cfoutput>demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_1=#URLEncodedFormat(GraphSource_1)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_2=#URLEncodedFormat(GraphSource_2)#</cfif></cfoutput>"  target="_blank" style="color:#797979;">View this email in your browser</a>.</td>
          </tr>
          <tr>
            <td align="left" valign="top" style="padding-bottom:15px;"><table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                  <td valign="bottom"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283b62f2953ba28ef7d2618ce5f52955414cbcb02371ffb5b838a64a9ed41f64c82" target="_blank" ><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL_logo_CORRECT80h.jpg" alt="FPL" title="FPL" width="95" height="81" border="0" style="display:block; color:#0096db;  font:25px Verdana; font-weight:bold;" /></a></td>
                  <td align="right" valign="bottom"><table  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="info-text" style="font-family:Verdana; font-size:13px; text-align:right; color:#464646 ;" align="right" ><cfoutput>#CustomerName#</cfoutput><br />
                          Account Number: 12345678 </td>
                      </tr>
                      <tr>
                        <td style="padding-top:8px;" align="right"><table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_L.jpg" 
                                             bgcolor="#74B943" style="background-repeat: no-repeat;" width="5" height="22" class="account"></td>
                              <td background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_Ctile.jpg" 
                                           style="font-family:Verdana; font-size:11px; color:#ffffff;
                                                           font-weight:bold; padding:0px 5px; background-repeat: repeat-x;" align="center" bgcolor="#74B943" class="account"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283ae868f50d87400e27c124eed556eb651393776878e2618e4f6bc0ac5b9331ab9" class="account" target="_blank" style="font-family:Verdana; font-size: 11px; color: #ffffff; text-decoration: none; font-weight: bold;" ><span style="color:#ffffff;">View my account</span></a></td>
                              <td class="account" background="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL-green-button012011_R.jpg" 
                                             bgcolor="#74B943" style="background-repeat: no-repeat;" width="5" height="22"></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
        </table>
        
        <!-- END Table use to set width of email --> 
      </div></td>
  </tr>
  <tr>
    <td bgcolor="#e9e9e9" style="padding:0px 10px; font-size:0%;"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="2" height="2" alt="" border="0" title="" /></td>
  </tr>
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px; font-size:0%;"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="2" height="2" alt="" border="0" title="" /></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#0096db" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="nav" align="left" style="font-family:Verdana, geneva, sans-serif; font-size:13px; color:#ffffff;"><b>Payment Extension Notice</b>&nbsp;&nbsp;&nbsp;&nbsp;<span class="break" style="display:none;"></span><a href="http://click.fplemail.com/?qs=2096c3f3debfc2837b8e93115871e9929ed0ca50f28bec256198767d3b29536121fc5a6bc7594817" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Log&nbsp;in</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc2833b8dc2267422886b99919937743e77d174f99cb62e019fb06f932eb6ebc28289" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Pay&nbsp;bill</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://click.fplemail.com/?qs=2096c3f3debfc283af3019d27ffd0c9847c2e4a82aa1a89961949b8e3226f929db7ba3cbe33d7dc2" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">FPL.com</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  
  <!---   http://navylive.dodlive.mil/2013/05/05/recp-encourages-utility-usage-conservation-in-privatized-housing/  --->
  
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/20120829_fpl_shadow.jpg" width="540" height="40" border="0" style="display:block;"></td>
          </tr>
  
  
   		  <cfif TRIM(GraphSource_2) NEQ "">
          
          <tr>
            <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="540">
                <tbody>
                  <tr>
                    <td style="padding-bottom: 40px;">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td bgcolor="#98CCFE"><a href="#"> <img src="<cfoutput>#GraphSource_2#</cfoutput>" alt="" style="display: block; font-style: normal; font-variant: normal; font-size: 12px; line-height: normal; font-family: Verdana, geneva, sans-serif; font-weight: bold; color: #ffffff;" border="0" height="507" width="540" /></a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          
          </cfif>
  
  
          <tr>
            <td align="left"><table width="540" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="padding-bottom:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="120" valign="top"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/samples/recp.jpg" style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" width="120" height="120" alt="" border="0" /></a></td>
                        <td width="20" valign="top"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="20" height="1" border="0" alt="" /></td>
                        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="title" valign="top" align="left" style="font-size:18px; font-family:Verdana, geneva, sans-serif; font-weight:bold; color:#49ab08; padding-bottom:8px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#49ab08; text-decoration:none;"><span style="color:#49ab08;">Resident Energy Conservation Program</span></a></td>
                            </tr>
                            <tr>
                              <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:15px;">The Resident Energy Conservation Program encourages conservation by transferring responsibility for utility usage to families living in privatized housing. Right now, residents have unlimited usage that can lead to utility usage greater than the allowance included within the Basic Allowance for Housing.</td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="link" style="font-family:Verdana, geneva, sans-serif; font-size:13px; font-weight:bold; color:#006dcc;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">For more information click here</span></a> &raquo;</td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          
         
          <tr>
            <td align="left"><table width="540" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="padding-bottom:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="120" valign="top"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank"><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/samples/light_bulb.jpg" style="display:block; font:10px Verdana, geneva, sans-serif; font-weight:bold; color:#003069;" width="120" height="120" alt="" border="0" /></a></td>
                        <td width="20" valign="top"><img style="display:block;" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/Spacer.gif" width="20" height="1" border="0" alt="" /></td>
                        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="title" valign="top" align="left" style="font-size:18px; font-family:Verdana, geneva, sans-serif; font-weight:bold; color:#49ab08; padding-bottom:8px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#49ab08; text-decoration:none;"><span style="color:#49ab08;">Usage Summary</span></a></td>
                            </tr>
                            <tr>
                              <td class="body" align="left" valign="top" style="font:13px Verdana, geneva, sans-serif; color:#4b4b4b; padding-bottom:15px;">Your usage summary here. How much did you use last year at this time? How much do you use on average? What do other housholds in your neighborhood use?</td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="link" style="font-family:Verdana, geneva, sans-serif; font-size:13px; font-weight:bold; color:#006dcc;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc28393415bd512cd713b93d92d728ff25d54618d0f0b44e0e5ad3a6b04bb09a6de7c"  target="_blank" style="color:#006dcc; text-decoration:underline;"><span style="color:#006dcc;">Check out more ways to save on our website</span></a> &raquo;</td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
                    
          <cfif TRIM(GraphSource_1) NEQ "">
          
          <tr>
            <td align="left"><table border="0" cellpadding="0" cellspacing="0" width="540">
                <tbody>
                  <tr>
                    <td style="padding-bottom: 40px;">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td bgcolor="#98CCFE"><a href="#"> <img src="<cfoutput>#GraphSource_1#</cfoutput>" alt="" style="display: block; font-style: normal; font-variant: normal; font-size: 12px; line-height: normal; font-family: Verdana, geneva, sans-serif; font-weight: bold; color: #ffffff;" border="0" height="122" width="540" /></a></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          
          </cfif>
          
          
          
          <tr>
            <td align="right" style="padding-bottom:10px;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc2832b0444ce9e61a4c92669e83de83c38bd6bfcf6c910a98cd98ad557499a697e9a" target="_blank" ><img src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/demos/fpl1_files/FPL_CTC_Logo_CORRECT.jpg" width="261" height="50" border="0" style="display:block; color:#0096db; font:14px Verdana, geneva, sans-serif; font-weight:bold;" alt="FPL: Changing the current."></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  <tr>
    <td height="30" bgcolor="#0096db" style="padding:0px 10px;"><div align="center">
        <table width="540" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="nav" align="center" style="font-family:Verdana, geneva, sans-serif; font-size:13px; color:#ffffff;"><a href="http://click.fplemail.com/?qs=2096c3f3debfc283668d6bec08dd4e2cda8c30eb00f6ce6ba54d3be9dbe9ea3057cadb013cd300a7" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Join the Power Panel</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc283e4ab4389f4a3957640cb6ed4397f3c3406be0f3953eb0d12dd2edf234f844da5" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">About&nbsp;us</span></a>&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide">|</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="break" style="display:none;"></span> <a href="http://click.fplemail.com/?qs=2096c3f3debfc283c384ac759aa4ef8e06d4e82b519446fb4c9beb9cbb895d87e0143d3d2175783e" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Contact&nbsp;us</span></a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://click.fplemail.com/?qs=2096c3f3debfc283cbb7ec86ba36b4e760b631cbe864d255dd2fdcf1ba90716f99f3356175a70dd4" target="_blank" style="color:#ffffff; text-decoration:none;"><span style="color:#ffffff;">Feedback [ + ]</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
  <tr>
    <td bgcolor="#ffffff" style="padding:0px 10px;"><div align="center">
        <table cellpadding="0" cellspacing="0" border="0" width="540">
          <tr>
            <td class="footer" align="center" style="padding: 20px 0px 0px 0px; font-family: Verdana; font-size: 10px; color: #797979; border-bottom: none !important;">As a valued customer of Florida Power &amp; Light Company, you have received this email to provide you with information that may interest you. Please add <a href="mailto:FPL_Account_Management@reply.fplemail.com" target="_blank" style="color: #777777; text-decoration: none;">FPL_Account_Management@reply.fplemail.com</a> to your address book.</td>
          </tr>
          <tr>
            <td class="footer" align="center" style="padding: 10px 0px 0px 0px; font-family: Verdana; font-size: 10px; color: #797979;">Florida Power & Light Company, <span class="appleLinks7">700 Universe Blvd., Juno Beach, FL 33408, USA</span></td>
          </tr>
          <tr>
            <td align="center" style="padding: 0px 0px 15px 0px; font-family: Verdana; font-size: 10px; color: #797979;"><a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6ac6aed4693c6aeaa3b01ed2437fbb91edc41dd5d710e90574c025b831f660efa3"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Unsubscribe</span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6a8de9da1499e8ba37ac87cf0b5afc419ae3c40fd5d24ece5897053ce40ea994c2"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Change email frequency</span></a>&nbsp;&nbsp;|&nbsp;&nbsp; <a class="footer" href="http://click.fplemail.com/?qs=73c86baa063b5a6a0321f3d38ab060a7864df75c5454cf4b3cabff139cfdc5bc864be29cc160fb97"  style="color: #797979; text-decoration: underline;" target="_blank"><span style="color: #797979;">Privacy policy</span></a></td>
          </tr>
        </table>
      </div></td>
  </tr>
</table>
<!-- END Table used to center email --> 
<img src="http://click.fplemail.com/open.aspx?ffcb10-fe6a15767165067a7514-fdfd13707363047d75117777-fe6e15707764037e7510-fe9816737360017d76-fe27137473600c7d751374-ffce15&d=10033" width="1" height="1">
</body>
</html>

</cfsavecontent>


<cfoutput>

	<cfif TRIM(ForwardForFulfillment) NEQ "">
        <pre name="code" class="html">#outContent#</pre>
    <cfelse>
        #outContent#
    </cfif>

</cfoutput>


<link type="text/css" rel="stylesheet" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/syntaxhighlighter/css/syntaxhighlighter.css"></link>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/syntaxhighlighter/js/shcore.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushcsharp.js"></script>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/syntaxhighlighter/js/shbrushxml.js"></script>
<script language="javascript">
dp.SyntaxHighlighter.ClipboardSwf = '<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/syntaxhighlighter/js/clipboard.swf';
dp.SyntaxHighlighter.HighlightAll('code');
</script>



