<cfsetting showdebugoutput="no"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - Rules Samples</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<cfparam name="inpSN" default="1" />

<cfparam name="inpCS" default="" />
<cfparam name="inpCST" default="2" />


<!--- Use custom test values --->
<cfparam name="inpUseCustom" default="0" />
<cfparam name="ElectricVehicleFlag" default="0" />
<cfparam name="ForwardForFulfillment" default="" />
<cfparam name="AlreadyBeenSentFlag" default="0" />
<cfparam name="CustomerName" default="John Q. Customer" />
<cfparam name="GraphSource_1" default="" />
<cfparam name="GraphSource_2" default="" />
<cfparam name="NotOpenedThreeDays" default="0" />
<cfparam name="CouponCode" default="12345678" />

<cfoutput>
 	<script src="#rootUrl#/#PublicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.powerzoom.js"></script> 
     
     <style>
		 @import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css') all;
		 
	 </style>     
</cfoutput>


<cfset RetVarOutputContactList = {} />
<cfset RetVarOutputContactList.ROWS = arrayNew(1) />

<cftry>

<!--- generate signature string --->
	<cffunction name="generateSignature" returnformat="json" access="remote" output="false">
		<cfargument name="method" required="true" type="string" default="GET" hint="Method" />
                 
		<cfset dateTimeString = GetHTTPTimeString(Now())>
		<!--- Create a canonical string to send --->
		<cfset cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
		<cfset signature = createSignature(cs,variables.secretKey)>
		
		<cfset retval.SIGNATURE = signature>
		<cfset retval.DATE = dateTimeString>
		
		<cfreturn retval>
	</cffunction>
	
	<!--- Encrypt with HMAC SHA1 algorithm--->
	<cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
	   <cfargument name="signKey" type="string" required="true" />
	   <cfargument name="signMessage" type="string" required="true" />
	
	   <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
	   <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
	   <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
	   <cfset var mac = createObject("java","javax.crypto.Mac") />
	
	   <cfset key = key.init(jKey,"HmacSHA1") />
	   <cfset mac = mac.getInstance(key.getAlgorithm()) />
	   <cfset mac.init(key) />
	   <cfset mac.update(jMsg) />
	
	   <cfreturn mac.doFinal() />
	</cffunction>
	
	
	<cffunction name="createSignature" returntype="string" access="public" output="false">
	    <cfargument name="stringIn" type="string" required="true" />
		<cfargument name="scretKey" type="string" required="true" />
		<!--- Replace "\n" with "chr(10) to get a correct digest --->
		<cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
		<!--- Calculate the hash of the information --->
		<cfset var digest = HMAC_SHA1(scretKey,fixedData)>
		<!--- fix the returned data to be a proper signature --->
		<cfset var signature = ToBase64("#digest#")>
		
		<cfreturn signature>
	</cffunction>
    
	<cfset variables.accessKey = 'E0A48A902E98FB45699C' />
    <cfset variables.secretKey = '2e06c1345D1+4df911f5130671+8FfeE90b1e064' />
   
 	<cfset variables.sign = generateSignature("GET") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
 <!---   <cfdump var="#datetime#"> 
 
 	<cfoutput>
    <BR />
    <cfdump var="#signature#"> 
    <BR />
    <cfdump var="#authorization#"> 
    <BR />
    <cfoutput>
        variables.accessKey = #variables.accessKey#
        <BR />
        variables.secretKey = #variables.secretKey#
        <BR />
	</cfoutput>

	--->	
    
    <cfif inpUseCustom EQ 0>
    
		<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
        
            <!--- By default EBM API will return json or XML --->
            <cfhttpparam name="Accept" type="header" value="application/json" />    	
            <cfhttpparam type="header" name="datetime" value="#datetime#" />
            <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="1" />
        
           <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
           <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="ElectricVehicleFlag" />
              
        </cfhttp>
        
       <!--- returnStruct<BR />                        
        <cfdump var="#returnStruct#"> --->
        
        <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
        
        <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        
            <cfset ElectricVehicleFlag = RetVarOutput.CDFV[1]>
        
        </cfif>
        
        
        <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
        
            <!--- By default EBM API will return json or XML --->
            <cfhttpparam name="Accept" type="header" value="application/json" />    	
            <cfhttpparam type="header" name="datetime" value="#datetime#" />
            <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="1" />
        
           <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
           <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="ForwardForFulfillment" />
              
        </cfhttp>
        
       <!--- returnStruct<BR />                        
        <cfdump var="#returnStruct#"> --->
        
        <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
        
        <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        
            <cfset ForwardForFulfillment = RetVarOutput.CDFV[1]>
        
        </cfif>
        
        
        <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
        
            <!--- By default EBM API will return json or XML --->
            <cfhttpparam name="Accept" type="header" value="application/json" />    	
            <cfhttpparam type="header" name="datetime" value="#datetime#" />
            <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="1" />
        
           <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
           <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="AlreadyBeenSentFlag" />
              
        </cfhttp>
        
       <!--- returnStruct<BR />                        
        <cfdump var="#returnStruct#"> --->
        
        <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
        
        <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        
            <cfset AlreadyBeenSentFlag = RetVarOutput.CDFV[1]>
        
        </cfif>
        
        <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
        
            <!--- By default EBM API will return json or XML --->
            <cfhttpparam name="Accept" type="header" value="application/json" />    	
            <cfhttpparam type="header" name="datetime" value="#datetime#" />
            <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="1" />
        
           <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
           <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="GraphSource_1" />
              
        </cfhttp>
        
       <!--- returnStruct<BR />                        
        <cfdump var="#returnStruct#"> --->
        
        <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
        
        <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        
            <cfset GraphSource_1 = RetVarOutput.CDFV[1]>
        
        </cfif>
    
    
     <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
     <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
        
            <!--- By default EBM API will return json or XML --->
            <cfhttpparam name="Accept" type="header" value="application/json" />    	
            <cfhttpparam type="header" name="datetime" value="#datetime#" />
            <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
           
           <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
           <!--- If in debug mode make sure you are using the correct credentials--->
           <cfhttpparam type="formfield" name="DebugAPI" value="1" />
        
           <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
           <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
           <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="GraphSource_2" />
              
        </cfhttp>
        
       <!--- returnStruct<BR />                        
        <cfdump var="#returnStruct#"> --->
        
        <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
        
        <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        
            <cfset GraphSource_2 = RetVarOutput.CDFV[1]>
        
        </cfif>    
    
    </cfif>
    
        
    <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
 	<cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/listgroupmembers" method="GET" result="returnStruct" >
	
	    <!--- By default EBM API will return json or you can request XML --->
	    <cfhttpparam name="Accept" type="header" value="application/json" />    	
	    <cfhttpparam type="header" name="datetime" value="#datetime#" />
        <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
  	   
       <!--- If DebugAPI is greater than 0 and is specidfied then use Dev DB for testing--->
       <!--- If in debug mode make sure you are using the correct credentials--->
       <cfhttpparam type="formfield" name="DebugAPI" value="1" />
    
       <!--- FPL Newsletter List = Group Id 64 --->	
       <cfhttpparam type="formfield" name="INPGROUPID" value="64" />
          
    </cfhttp>
    
   <!--- returnStruct<BR />                        
    <cfdump var="#returnStruct#"> --->
   
   	<!--- Used for Select Box later in this page ---> 
    <cfset RetVarOutputContactList = DeserializeJSON(returnStruct.Filecontent) />
    
<cfcatch type="any">


</cfcatch>


</cftry>




<style>

#inner-box-left ul, .inner-txt-full ul
{
	margin-top: 10px;	
	margin-bottom:10px;
}

#inner-box-left ul li, .inner-txt-full ul li
{
	margin-left: 65px;	
	margin-bottom:10px;
	
}


.ui-tabs .ui-tabs-nav li.ui-tabs-active a, .ui-tabs .ui-tabs-nav li.ui-state-disabled a, .ui-tabs .ui-tabs-nav li.ui-tabs-loading a {
    cursor: text;
}
.ui-tabs .ui-tabs-nav li a, .ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active a {
    cursor: pointer;
}
.ui-tabs .ui-tabs-nav li a {
    float: left;
    padding: 0.5em 1em;
    text-decoration: none;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-widget-header a {
    color: #DDDDDD;
}
.ui-widget-content a {
    color: #DDDDDD;
}

.ui-state-active {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #0085C8, #212121) repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
    border-bottom: 1px solid #0085C8;
    border-image: none;
    border-top: 1px solid #0085C8;
    color: #1C4257;
    outline: medium none;
}


.inner-bg {
    background-color: #F8F8F8;
    float: left;
    margin-top: 6px;
    min-height: 577px;
    padding-bottom: 25px;
    width: 100%;
}


input, option, select {
   
    font-size: 16px;
	margin:5px;	
   
}

</style>


</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
                
         <div id="inner-bg-alt">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    
                    <cfswitch expression="#inpSN#">
                    
                    	<cfcase value="1">
                    		<div class="inner-main-full">Rules Sample - Newsletter</div>                    
                    	</cfcase>
                    
	                    <cfcase value="2">
                    		<div class="inner-main-full">Rules Sample - Account Monitoring</div>                    
                    	</cfcase>
                        
                        <cfcase value="3">
                    		<div class="inner-main-full">Rules Sample - High Bill Inquiry</div>                    
                    	</cfcase>
                    
						<cfcase value="4">
                    		<div class="inner-main-full">Rules Sample - Payment Extension Approved</div>                    
                    	</cfcase>
					
						<cfcase value="5">
                    		<div class="inner-main-full">Rules Sample - ENBREL Insurance Reverification</div>                    
                    	</cfcase>
					
                    	<cfdefaultcase>
                        	<div class="inner-main-full">Rules Sample - Newsletter</div>
                        </cfdefaultcase>
                        
                    </cfswitch>
                    
                    
                    <div class="inner-txt-full" style="margin-bottom:20px;">The possibilities are limitless. Our backend fulfillment devices offer highly customizable capabilities and are message format agnostic. Simple messages to company specific set of rules and strucure that lets your brand excel, surpass and stand out. With our tools and experience, we can help you craft your personalized messages.</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;">Select a contact or enter some data. Select Process Request Button. Scroll down to see results.</div>
                </div>
                
	        </div>
        
        
        	<div id="content" style="min-height:36px;">
        
               <div id="SampleTabs" style="min-height:36px;">
                       
                        <div align="left" class="EBMDialog">
                
                            <form method="post">
                            
                                <input type="hidden" name="inpUseCustom" id="inpUseCustom" value="1" />
                            
                                <div class="inputbox-container">
                                    <label for="CustomerName">Customer Name <span class="small">Personalization Option</span></label>
                                    <input id="CustomerName" name="CustomerName" placeholder="Enter Customer Name Here" size="20" autofocus="autofocus" value="#CustomerName#" style="width:184px;" />
                                </div>
                                
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <div class="inputbox-container">
                                    <!--- Add class class="menulink" to tell menu to ignore clicks here --->
                                    <label for="ElectricVehicleFlag">Electric Vehicle Flag <span class="small">Houshold Info</span></label>
                                    <select id="ElectricVehicleFlag" name="ElectricVehicleFlag" class="menulink">
                                        <option class="menulink" value="1" <cfif ElectricVehicleFlag EQ 1>selected</cfif>>Customer Has An Electric Vehicle</option>
                                        <option class="menulink" value="0" <cfif ElectricVehicleFlag EQ 0>selected</cfif>>Customer does NOT Have an Electric Vehicle</option>
                                        <option class="menulink" value="-1" <cfif ElectricVehicleFlag EQ -1>selected</cfif>>Unknown</option>
                                    </select>  
                                </div>
                                    
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <div class="inputbox-container">
                                    <label for="GraphSource_1">Enter a Graph image source URL here <span class="small">Leave blank to skip</span></label>
                                    <input id="GraphSource_1" name="GraphSource_1" placeholder="Graph Source 1" size="20" autofocus="autofocus" value="#GraphSource_1#" style="width:184px;" />
                                </div>                                    
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <div class="inputbox-container">
                                    <label for="GraphSource_2">Enter a Graph image source URL here <span class="small">Leave blank to skip</span></label>
                                    <input id="GraphSource_2" name="GraphSource_2" placeholder="Graph Source 2" size="20" autofocus="autofocus" value="#GraphSource_2#" style="width:184px;" />
                                </div>                                    
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <div class="inputbox-container">
                                    <label for="CouponCode">Enter a Coupon Code here <span class="small">Leave blank to skip</span></label>
                                    <input id="CouponCode" name="CouponCode" placeholder="Coupon Code" size="20" autofocus="autofocus" value="#CouponCode#" style="width:184px;" />
                                </div>                                    
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <div class="inputbox-container">
                                    <!--- Add class class="menulink" to tell menu to ignore clicks here --->
                                    <label for="AlreadyBeenSentFlag">De-Duplicate <span class="small">Don't sent multiple newsletters if this customer already has been sent this one on another account.</span></label>
                                    <select id="AlreadyBeenSentFlag" name="AlreadyBeenSentFlag" class="menulink">
                                        <option class="menulink" value="1" <cfif TRIM(AlreadyBeenSentFlag) EQ "1">selected</cfif>>Customer has already been sent Newsletter</option>
                                        <option class="menulink" value="0" <cfif TRIM(AlreadyBeenSentFlag) EQ "0">selected</cfif>>Customer has NOT already been sent Newsletter</option>
                                        <option class="menulink" value="-1" <cfif TRIM(AlreadyBeenSentFlag) EQ "-1">selected</cfif>>Unknown</option>
                                    </select>  
                                </div>
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                       
                                <div class="inputbox-container">
                                    <label for="ForwardForFulfillment">Forward Here for Fulfillment <span class="small">Leave blank to deliver Message locally</span></label>
                                    <input id="ForwardForFulfillment" name="ForwardForFulfillment" placeholder="Enter Fulfillment Service" size="20" autofocus="autofocus" value="#ForwardForFulfillment#" style="width:184px;" />
                                </div>
                                                                    
                                <div style="clear:both"></div>
                
                                <BR />
                                      
                                <div class="inputbox-container">
                                    <!--- Add class class="menulink" to tell menu to ignore clicks here --->
                                    <label for="NotOpenedThreeDays">Not Opened - Three Days <span class="small">Send Print on Demand by Mail</span></label>
                                    <select id="NotOpenedThreeDays" name="NotOpenedThreeDays" class="menulink">
                                        <option class="menulink" value="0" <cfif NotOpenedThreeDays EQ 0>selected</cfif>>Has not been 3 days yet</option>
                                        <option class="menulink" value="1" <cfif NotOpenedThreeDays EQ 1>selected</cfif>>Customer does NOT open email - after 3 days</option>                                            
                                    </select>  
                                </div>
                                
                                <div style="clear:both"></div>
                
                                <BR />
                                
                                <input type="submit" class="submit" value="Process Request"/>
                                                     
                            </form>
                            
                        </div>   
                        
                    </div>
                   
	        	</div>
            
            
        </div>              
        
        
         <div id="inner-bg">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    
                  	
					<cfif AlreadyBeenSentFlag NEQ 1 AND (inpCS NEQ "" OR inpUseCustom GT 0)>  
                    
                    
                    	                       
							<cfif TRIM(ForwardForFulfillment) EQ "" >
                            
                            	<cfif NotOpenedThreeDays EQ 1> 
                        			<div class="inner-main-full">The customer will be sent a print on demand copy of the following content by mail.</div>
                        		<cfelse> 
                        			<div class="inner-main-full">The customer will be sent the following email.</div>
                                </cfif>
                                    
                            <cfelse>
                            
                            	<cfif NotOpenedThreeDays EQ 1> 
                        			<div class="inner-main-full">The following print on demand content will be forwarded on<BR />for fulfillment: <span>#HTMLCodeFormat(ForwardForFulfillment)#</span></div>
                        		<cfelse> 
                        			<div class="inner-main-full">The following content will be forwarded on<BR />for fulfillment: <span>#HTMLCodeFormat(ForwardForFulfillment)#</span></div>
                                </cfif>
                               
                            </cfif>
                        
                        
                        
                    <cfelse>
                    
                    	<cfif inpCS NEQ "" OR inpUseCustom GT 0> 
	                    	<div class="inner-main-full">The customer would NOT be sent another email.</div>
                                
                        </cfif>
                        
                    </cfif>
                    
                        <div id="eMailSamples" style="width:615px; height:auto; overflow:auto;" align="left">
                                        
                        </div>
                    
                </div>
                
	        </div>
        
        </div>                
                    
    
      	<div id="servicemainline"></div> 
      
                  
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>


<!--- Do script down here because some of the params passed to url are not calculated until display --->
<script type="text/javascript" language="javascript">


	var Tab1Init = false;
	$(function() {

			 		  
	
	     <!--- $("#eMailSamples").html('<object data="<cfoutput>#rootUrl#/#PublicPath#/hiw_rs_fpl</cfoutput>">');   CustomerName--->
		
		<!--- Block if sent--->
		<cfif AlreadyBeenSentFlag NEQ 1 AND (inpCS NEQ "" OR inpUseCustom GT 0)> 
		
					<cfswitch expression="#inpSN#">
                    
                    	<cfcase value="1">
                    		<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/hiw_rs_fpl?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif>");
							</cfoutput>	                   
                    	</cfcase>
                    
						<cfcase value="2">
                    		<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/hiw_rs_chase?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif><cfif CouponCode NEQ "">&CouponCode=#URLEncodedFormat(CouponCode)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_1=#URLEncodedFormat(GraphSource_1)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_2=#URLEncodedFormat(GraphSource_2)#</cfif>");
							</cfoutput>	                    
                    	</cfcase>
                        
                        <cfcase value="3">
                    		<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/hiw_rs_fpl_payment_ext?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_1=#URLEncodedFormat(GraphSource_1)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_2=#URLEncodedFormat(GraphSource_2)#</cfif>");
							</cfoutput>	                    
                    	</cfcase>
						
	                    <cfcase value="4">
                    		<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/hiw_rs_fpl_high_bill?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_1=#URLEncodedFormat(GraphSource_1)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_2=#URLEncodedFormat(GraphSource_2)#</cfif>");
							</cfoutput>	                    
                    	</cfcase>
                        
                   		<cfcase value="5">
                    		<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/email_samples/amgen/insurancereverification?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_1=#URLEncodedFormat(GraphSource_1)#</cfif><cfif GraphSource_1 NEQ "">&GraphSource_2=#URLEncodedFormat(GraphSource_2)#</cfif>");
							</cfoutput>	                    
                    	</cfcase>
					
                    
                    	<cfdefaultcase>
                        	<cfoutput>
								$("##eMailSamples").load("#rootUrl#/#PublicPath#/hiw_rs_fpl?demo=1<cfif ElectricVehicleFlag NEQ "">&ElectricVehicleFlag=#ElectricVehicleFlag#</cfif><cfif ForwardForFulfillment NEQ "">&ForwardForFulfillment=#URLEncodedFormat(ForwardForFulfillment)#</cfif><cfif AlreadyBeenSentFlag NEQ "">&AlreadyBeenSentFlag=#AlreadyBeenSentFlag#</cfif><cfif CustomerName NEQ "">&CustomerName=#URLEncodedFormat(CustomerName)#</cfif>");
							</cfoutput>	
                        </cfdefaultcase>
                        
                    </cfswitch>
					
					
			
		</cfif>
					
		
  	});

</script>



</html>





<!--- sample for diagram 


	<!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
    <cfhttp url="<cfoutput>#rootUrl#</cfoutput>/webservice/ebm/contacts/cdf" method="GET" result="returnStruct" >
    
		<!--- By default EBM API will return json or you may request XML --->
        <cfhttpparam name="Accept" type="header" value="application/json" />    	
        <cfhttpparam type="header" name="datetime" value="#datetime#" />
        <cfhttpparam type="header" name="authorization" value="#authorization#" /> 
          
        <cfhttpparam type="formfield" name="inpContactString" value="#inpCS#" />
        <cfhttpparam type="formfield" name="inpContactTypeId" value="#inpCST#" />
        <cfhttpparam type="formfield" name="INPCUSTOMDATAFIELD" value="GraphSource_1" />
          
    </cfhttp>
         
    <cfset RetVarOutput = DeserializeJSON(returnStruct.Filecontent) />
    
    <cfif ArrayLen(RetVarOutput.CDFV) GT 0 >
        <cfset GraphSource_1 = RetVarOutput.CDFV[1]>
    </cfif>
	
	--->
        
        
        
        
        
        
        
        
        
        
        
        
