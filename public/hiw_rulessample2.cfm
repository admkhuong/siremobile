<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How it works - Rules Sample 2 - High Bill</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">


<cfparam name="inpCS" default="jpijeff@gmail.com" />
<cfparam name="inpCST" default="2" />

<cfoutput>
	<script src="#rootUrl#/#publicPath#/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
	 <script src="#rootUrl#/#publicPath#/js/jquery.powerzoom.js"></script> 
     
     <style>
		 @import url('#rootUrl#/#publicPath#/css/mb/jquery-ui-1.8.7.custom.css') all;
		 
	 </style>     
</cfoutput>




<style>

#inner-box-left ul, .inner-txt-full ul
{
	margin-top: 10px;	
	margin-bottom:10px;
}

#inner-box-left ul li, .inner-txt-full ul li
{
	margin-left: 65px;	
	margin-bottom:10px;
	
}


.ui-tabs .ui-tabs-nav li.ui-tabs-active a, .ui-tabs .ui-tabs-nav li.ui-state-disabled a, .ui-tabs .ui-tabs-nav li.ui-tabs-loading a {
    cursor: text;
}
.ui-tabs .ui-tabs-nav li a, .ui-tabs-collapsible .ui-tabs-nav li.ui-tabs-active a {
    cursor: pointer;
}
.ui-tabs .ui-tabs-nav li a {
    float: left;
    padding: 0.5em 1em;
    text-decoration: none;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
    color: #DDDDDD;
    text-decoration: none;
}
.ui-widget-header a {
    color: #DDDDDD;
}
.ui-widget-content a {
    color: #DDDDDD;
}

.ui-state-active {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #0085C8, #212121) repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
    border-bottom: 1px solid #0085C8;
    border-image: none;
    border-top: 1px solid #0085C8;
    color: #1C4257;
    outline: medium none;
}


.inner-bg {
    background-color: #F8F8F8;
    float: left;
    margin-top: 6px;
    min-height: 577px;
    padding-bottom: 25px;
    width: 100%;
}

</style>
<script type="text/javascript" language="javascript">


	var Tab1Init = false;
	$(function() {

		$('#PDCSurveyImg img').addpowerzoom({
		  defaultpower:3,
		  magnifiersize: [450, 250],
		  powerrange: [3, 3] <!---//Possible range: 2x to 10x magnification--->
		  });  
		 		  
	
	     <!--- $("#FPLSample").html('<object data="<cfoutput>#rootUrl#/#publicPath#/hiw_rs_fpl.cfm</cfoutput>">');--->
		 $("#FPLSample").load("<cfoutput>#rootUrl#/#publicPath#/hiw_rs_fpl_high_bill<cfif inpCS NEQ "" AND inpCST NEQ "">?inpCS=#inpCS#&inpCST=#inpCST#</cfif></cfoutput>");
    
		$("#SampleTabs").tabs({
			
			<cfif 1 EQ 1>active: 1 , </cfif>
			activate: function( event, ui ) { 
					<!---// $("#inpCST").selectbox();--->
			}
		});
		
		$("#SampleTabs").show();
		
		
		$('#tabs-0 #inpCS').on('change', function() {
		  console.log( this.value ); // or $(this).val()
		});
	
		
  	});

</script>

</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
                
         <div id="inner-bg">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    <div class="inner-main-full">Live Sample - The Newsletter</div>
                    <div class="inner-txt-full" style="margin-bottom:20px;">The possibilities are limitless. Our backend fulfillment devices offer highly customizable capabilities and are message format agnostic. Simple messages to company specific set of rules and strucure that lets your brand excel, surpass and stand out. With our tools and experience, we can help you craft your personalized messages.</div>
                    
                    
                    <div class="inner-txt-full" style="margin-bottom:20px;">
                    	Or enter some other contact from the mbdemo account contact lists. 
                  	</div>
                    
                </div>
                
	        </div>
        
        
        	<div id="content" style="min-height:36px;">
        
               <div id="SampleTabs" class="inner-bg" style="display:none; min-height:36px;">
                        <ul>
                            <li><a href="##tabs-0">Select One</a></li>
                            <li><a href="##tabs-1">Enter Your Own</a></li>
                            
                        </ul>
                        
                        <div id="tabs-0">
                        	
                            <div align="left" class="EBMDialog">
                    
                                <form method="post">
                                
                                    <div class="inputbox-container">
                                        <!--- Add class class="menulink" to tell menu to ignore clicks here --->
                                        <label for="INPDESC">Contact String <span class="small"></span></label>
                                        <select id="inpCS" name="inpCS" class="menulink">
                                            <option class="menulink" value="none" <cfif inpCS EQ "none">selected</cfif>>none</option>
                                            <option class="menulink" value="jpijeff@gmail.com" <cfif inpCS EQ "jpijeff@gmail.com">selected</cfif>>jpijeff@gmail.com</option>
                                            <option class="menulink" value="jpijeff@gmail2.com" <cfif inpCS EQ "jpijeff@gmail2.com">selected</cfif>>jpijeff@gmail2.com</option>
                                            <option class="menulink" value="jpijeff@gmail3.com" <cfif inpCS EQ "jpijeff@gmail3.com">selected</cfif>>jpijeff@gmail3.com</option>
                                        </select>  
                                    </div>
                                        
                                    <div style="clear:both"></div>
                    
                                    <BR />
                                   
                                    <input type="hidden" id="inpCST" name="inpCST" value="2" />
                                                                       
                                    <div style="clear:both"></div>
                    
                                    <BR />
                                    
                                    <input type="submit" class="submit" value="Process Request"/>
                                  <!---  <div class="submit">                                                  
                                        <a href="##" class="button filterButton small" id="inpSubmit" >Process Request</a>
                                    </div>--->
                        
                                </form>
                                
                            </div>    
                    
                        </div>
                        <div id="tabs-1">
                        	
                            <div align="left" class="EBMDialog">
                    
                                <form method="post">
                                
                                    <div class="inputbox-container">
                                        <!--- Add class class="menulink" to tell menu to ignore clicks here --->
                                        <label for="INPDESC">Contact Type <span class="small"></span></label>
                                        <select id="inpCST" name="inpCST" class="menulink">
                                            <option class="menulink" value="1" <cfif inpCST EQ 1>selected</cfif>>Voice</option>
                                            <option class="menulink" value="2" <cfif inpCST EQ 2>selected</cfif>>e-mail</option>
                                            <option class="menulink" value="3" <cfif inpCST EQ 3>selected</cfif>>SMS</option>
                                        </select>  
                                    </div>
                                        
                                    <div style="clear:both"></div>
                    
                                    <BR />
                                    
                                    <div class="inputbox-container">
                                        <label for="INPDESC">Contact String <span class="small"></span></label>
                                        <input id="inpCS" name="inpCS" placeholder="Enter Contact String Here" size="20" autofocus="autofocus" value="#inpCS#" style="width:184px;" />
                                    </div>
                                    
                                    <div style="clear:both"></div>
                    
                                    <BR />
                                    
                                    <input type="submit" class="submit" value="Process Request"/>
                                  <!---  <div class="submit">                                                  
                                        <a href="##" class="button filterButton small" id="inpSubmit" >Process Request</a>
                                    </div>--->
                        
                                </form>
                                
                            </div>    
                            
                        </div>
                        
                    </div>
                    
                    <div id="FPLSample" style="width:600px; height:auto; overflow:auto;" align="left">
                                    
                    </div>
                
	        	</div>
            
            
        </div>              
        
        
         <div id="inner-bg">
        
        	<div id="content">
        
                <div id="content" style="min-height:36px;">
                    
                  	


                    
                    
                    
                    
                </div>
                
	        </div>
        
        </div>                
                    
    
      	<div id="servicemainline"></div> 
      
                  
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>


