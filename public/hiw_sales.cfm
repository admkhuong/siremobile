<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMC - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>
	    
</cfoutput>




<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }

	h3
	{
		margin-bottom:10px;	
		
	}
	
	#content li
	{
		margin:8px;	
	}
	
</style>
<script type="text/javascript" language="javascript">


	$(document).ready(function() {

				
  	});

</script>
</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannerimage">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Sales Tools</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort# systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. 
                    
                 </div>
                
                <img style="position:absolute; top:0px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/channelcubeii.png" height="360" width="480" />
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-alt" style="min-height: 550px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:550px;">
               
                                        
                    <img style="float:right;  margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:10px;">
                    	<h2 class="super">Sales cycle</h2>
                        <br/>                    	                      
                        
                       
                        <p>&nbsp;</p>
                        <h3>The Case for Calling Low</h3>
                        <p>When you hear the case for &ldquo;call high&rdquo; it intuitively feels obvious. I have met several sales execs who argue the exact opposite strategy. Their view is that without &ldquo;proof points&rdquo; the senior leadership teams are likely to be cynical about the benefits of your product.</p>
                        <p>In the &ldquo;call low&rdquo; camp they advocate</p>
                        <p>1. Find a business unit leader who would be positively impacted by success of your product</p>
                        <p>2. Run a short, quantifiable pilot</p>
                        <p>3. Have that business unit leader champion you to a senior exec with data and proof in hand</p>
                        <p>4. Land bigger deals with more assuredness.</p>
                        <p>The mantra of this school is, &ldquo;<a href="http://dictionary.reverso.net/english-cobuild/put%20one's%20head%20above%20the%20parapet/keep%20one's%20head%20below%20the%20parapet" target="_blank">keep your head below the parapet</a> and avoid getting shot. You can rise up once you have your armaments.&rdquo;</p>
                        <p>While I understand the logic, I personally believe you need to provide enough evidence through case studies to talk with the CEO and if she can&rsquo;t convince herself that it&rsquo;s worth exploring being a buyer then you could recommend you do a pilot internally to prove yourself.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>	
                        
                                              
                            
                        
                    </div>
                    
                                     
                             
            </div>
        </div>
        
        
         <div id="inner-bg-alt" style="min-height: 380px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:380px;">
               
                                        
                    <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/singlechannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                    	<h2 class="super">Single-Channel - Point-to-Point Semantics</h2>
                        <br/>
                    	<h3>Being the simplest method, the Single Channel still has multiple point-to-point semantics.</h3>
                        
                        <h3>While the SMS and eMail channels can provide links to more information and interactions,tThe Voice channel can react to touch tones or even natural launguage speech recognition directly.</h3>
                        
                         <h3>Reasons to still use just the Single Channel.</h3>	
                         <ul style="margin: 8px 0 0 30px;">
                            	
                                <li>
                                	Decreased Time & Expense.
                                </li>
                                
                                <li>
                                	Your Business May Prefer a Specific Channel.  
                                </li>
                                                                                                                               
                                <li>
                                	Less moving parts.
                                </li>
                                                                                             
                            </ul>                            
                            
                    </div>
                                                       
                             
            </div>
        </div>
        
         <div id="inner-bg-alt" style="min-height: 450px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:440px;">
               
                                        
                    <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/multichannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                    	<h2 class="super">Multi-Channel - More than one way to communicate</h2>
                        <br/>
                    	                        
                        <h3>Your customers could be anywhere, be where your customers are. With #BrandShort# Customer Preference Portal tools you can let the consumer specify which channel they prefer to recieve which messages on.</h3>                        
                        
                        <h3>Escalations - If you dont get a response on one channel send it to another channel</h3>
                        
                         <h3>Expand your consumers experience.</h3>	
                         <ul style="margin: 8px 0 0 30px;">
                            	<li>                                	
									Tools to Capture and act on your customer's channel preference.   
                                </li>
                                
                                <li>
                                	Escalations - If you dont get a response on one channel, send it to another channel.
                                </li>
                                                                
                                <li>
                                	Create Multiple Touch Points
                                </li>
                               
                                                                                          
                            </ul>
                            
                            
                        
                    </div>
                    
                                     
                             
            </div>
        </div>
        
        
         <div id="inner-bg-alt" style="min-height: 550px">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; height:550px;">
               
                                        
                    <img style="float:right;  margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:10px;">
                    	<h2 class="super">Cross-Channel - Coordinating Between Channels</h2>
                        <br/>                    	                      
                        
                        <h3>Rules Based Messaging and Reactive Event Condition Actions</h3>	
                        
                        <h3>Avoid the silo mentality. Your existing channel campaigns can now be integrated together. #BrandShort# can help you to engage the consumer in more meaningful ways. </h3>
                        
                        <ul style="margin: 8px 0 0 30px;">
 
 		                        <li>Consumers can now respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.</li>
                            	<li>
                                	Opt in to a program using a code from a bill stuffer, direct amil, email, or SMS   
                                </li>
                                
                                <li>
                                	Request more information to be sent on an alternate channel. Terms of use, brochures, policies and more.
                                </li>
                                                                
                                <li>
                                	Direct Mail, SMS, and eMail featuring URLs
                                </li>
                                
                                <li>Block conflicting messages. Don't send bill collection calls at the same time as an emergency storm warning.</li>
                                
                                <li>Seamless approach to messaging across various channels guides potential customers towards engagement.</li>
                                                             
                        </ul>
                            
                            
                        
                    </div>
                    
                                     
                             
            </div>
        </div>
                
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>