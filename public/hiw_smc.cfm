<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
 
<title>SMC - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<style>
				
			
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{			 
			 $( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			 
					
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
    
    
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Single-Channel<BR />Multi-Channel<BR />Cross-Channel</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort# systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. 
                    
                 </div>
                
                <img style="position:absolute; top:0px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/channelcubeii.png" height="360" width="480" />
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>Communications backbone</h2>
                        <p>Supported Messaging Channels</p>
                    </header>
                </div>    
                
                <img style="float:left;" src="#rootUrl#/#publicPath#/images/m1/globalmultichanneliv.png" height="450" width="480" />                    
                    
                <div class="hiw_Info" style="width:460px; float:right; margin-top:50px;">
                    <h3>The EBM directly provides best of breed tools and support for messaging via the following channels: </h3>
                        
                        <ul style="margin: 8px 0 0 30px;">
                            <li>
                                Voice   
                            </li>
                            
                            <li>
                                SMS
                            </li>
                                                            
                            <li>
                                eMail
                            </li>
                            
                            <li>
                                Online/Web
                            </li>
                            
                            <li>
                                Preference
                            </li>
                           
                        </ul>
                    
                     <br/>
                    <h3 style="padding-bottom:20px;">
                        Additionally #BrandShort# platform provides tools and support for messaging via the following third party channels:
                        FAX, Social Media, Print, Radio, TV, and any other channel we can communicate with publicly (Web) or privately (VPN)                                                                                                           
                    </h3>
                </div>
                                
                                 
            </div>
                    
        </div>
        
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omnichannel engagement
                
            </div>
        </div>    
        
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Single-Channel</h2>
                         <p>Point-to-Point Semantics</p>
                    </header>
                </div>    
                                        
                    <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/singlechannel.png" width="480" height="400" />
                    
                    
                    <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                    	
                    	<h3>Being the simplest method, the Single Channel still has multiple point-to-point semantics.</h3>
                        
                        <h3>While the SMS and eMail channels can provide links to more information and interactions, the Voice channel can react to touch tones or even natural launguage speech recognition directly.</h3>
                        
                         <h3>Reasons to still use just the Single Channel.</h3>	
                         <ul style="margin: 8px 0 0 30px;">
                            	
                                <li>
                                	Decreased Time & Expense.
                                </li>
                                
                                <li>
                                	Your Business May Prefer a Specific Channel.  
                                </li>
                                                                                                                               
                                <li>
                                	Less moving parts.
                                </li>
                                                                                             
                            </ul>                            
                            
                    </div>
                                                       
                             
            </div>
        </div>
        
        <div id="inner-bg-m1" style="">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Multi-Channel</h2>
                         <p>More than one way to communicate</p>
                    </header>
                </div>    
                                        
                <img style="float:right;" src="#rootUrl#/#publicPath#/images/m1/multichannel.png" width="480" height="400" />
                
                
                <div class="hiw_Info" style="width:460px; float:left; margin-top:20px;">
                                            
                    <h3>Your customers could be anywhere, be where your customers are. With #BrandShort# Customer Preference Portal tools you can let the consumer specify which channel they prefer to recieve which messages on.</h3>                        
                    
                    <h3>Escalations - If you dont get a response on one channel send it to another channel</h3>
                    
                     <h3>Expand your consumers experience.</h3>	
                     <ul style="margin: 8px 0 0 30px;">
                        <li>                                	
                            Tools to Capture and act on your customer's channel preference.   
                        </li>
                        
                        <li>
                            Escalations - If you dont get a response on one channel, send it to another channel.
                        </li>
                                                        
                        <li>
                            Create Multiple Touch Points
                        </li>                           
                                                                                  
                    </ul>
                    
                </div>        
                         
            </div>
        </div>
                
		<div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Cross-Channel</h2>
                         <p>Coordinating Between Channels</p>
                    </header>
                </div>    
                                        
                <img style="float:right;  margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                
                <div class="hiw_Info" style="width:460px; float:left; margin-top:10px;">
                    
                    <h3>Rules Based Messaging and Reactive Event Condition Actions</h3>	
                    
                    <h3>Avoid the silo mentality. Your existing channel campaigns can now be integrated together. #BrandShort# can help you to engage the consumer in more meaningful ways. </h3>
                    
                    <ul style="margin: 8px 0 0 30px;">

                            <li>Consumers can now respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.</li>
                            <li>
                                Opt in to a program using a code from a bill stuffer, direct mail, email, or SMS   
                            </li>
                            
                            <li>
                                Request more information to be sent on an alternate channel. Terms of use, brochures, policies and more.
                            </li>
                                                            
                            <li>
                                Direct Mail, SMS, and eMail featuring URLs
                            </li>
                            
                            <li>Block conflicting messages. Don't send bill collection calls at the same time as an emergency storm warning.</li>
                            
                            <li>Seamless approach to messaging across various channels guides potential customers towards engagement.</li>
                                                         
                    </ul>
                    
                </div>
                             
            </div>
        </div>
                
      
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>