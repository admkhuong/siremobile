<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Getting Started SMS</title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
        
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
		</style>
        
    </cfoutput>
                   
        
<style>
	
	.intro2 
	{
		left: 0;
		padding: 0 0;
		position: relative;
		text-align: center;
		top: 0;
		transform: none;
		width: 100%;
		margin: 20px 0;
	}
	
	.pfblock 
	{
		padding: 2em;
	}
			
	body 
	{
		background-color: #fff;
	}
			
</style>

    <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					<!---$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}--->
					
							
				});
								
			
			</script>
</head>
            

<body>
        
        
        <section id="" class="pfblock pfblock-gray">
                           
            <div class="intro2">
            
                
                    
                    <cfoutput>
                        <a class="" href="home"><img src="#LogoImageLink7#" style="border:none;" /></a>
                    </cfoutput>
                        
               
                            
                <h1>Event Based Messaging</h1>
                <div class="start">Tools for creating and managing modern responsive consumer communication applications</div>
                
            </div>
            
        </section>
    
   <!--- http://www.smarttutorials.net/responsive-facebook-style-timeline-design-with-twitter-bootstrap/ --->
   
   <!---
   
   <!---<span class="month"><i class="fa fa-calendar"></i> &nbsp; March 2015 </span>
			<p>&nbsp;</p>
			<div class="embed-responsive embed-responsive-16by9">
				<iframe frameborder="0" allowfullscreen="allowfullscreen" src="https://www.youtube.com/embed/AGawieZttJ0" class="embed-responsive-item"></iframe>
			</div>
			<p>&nbsp;</p>
			<p>Demo : <a href="http://demo.smarttutorials.net/invoice-script-php/" target="_blank">PHP Invoice System Demo</a></p>
			
			<p>Tutorial : <a href="http://www.smarttutorials.net/invoice-system-using-jquery-php-mysql-bootstrap/" target="_blank">Smart Invoice System</a></p>
			
			<p>This is an awesome tool for your marketing team and they can crack the deal at client’s doorstep. Order PDF Invoice System Script today! and increase sales. It just comes at a fraction of one time price. Grab the deal today.</p>--->
   
   --->
   
   <!---wrap the page content do not style this--->
<div id="page-content">
  
  
  <section class="pfblock pfblock-gray">
     
      <div class="container" style="text-align:center;" >
      
            <h2 class="title text-center" >Getting Started with SMS</h2>
                            
            <div style="padding-top:15px; text-align:left;">
            
                <h4>Introduction to SMS</h4>    
                  
                <p>
                SMS messages can be typically sent and received within seconds throughout the world. And with more than 1 trillion
                SMS messages sent annually in the US alone, volumes are widely forecast to continue their strong growth rate globally
                for years to come.
                </p>
                
                <p>
                SMS offers businesses a compelling communication channel for effective, two-way interaction with customers and
                prospects. It can extend brand awareness, create customer loyalty and deliver timely content or critical information,
                providing the immediacy and interactivity no other medium can offer. Attracted by the unique characteristics offered
                by SMS, businesses are increasingly utilizing the anytime, anywhere mobile channel as part of a multichannel communication strategy. 
                </p>
            
                <h4>Types of SMS</h4>
            
                <p>
                Person-to-Person (P2P) and Application-to-Person (A2P) are the two categories of SMS messages. While P2P messages
                connect one individual to another, A2P messages are used by companies that want to interact with consumers via an
                SMS application. This document deals solely with A2P messaging.
                </p>
                
                <h4>Standard Rate SMS</h4>
            
                <p>Standard rate mobile messaging services (also known as bulk SMS) allow companies to send quantities of SMS quickly
                and efficiently. Generally, text messaging charges are not applied to the mobile subscriber's wireless bill or deducted
                from a subscriber's messaging plan allowance when they receive messages from an application (known as Mobile
                Terminated, or MT, messages). However, text messaging charges do apply when they send messages from their phone to
                an application (known as Mobile Originated, or MO, messages). An exception to this is found in the US and Canada,
                where many mobile operators charge for both sending and receiving text messages.
                </p>
                
                <h4>Premium SMS (PSMS)</h4>
                <p>Drepecated - Premium SMS was used by businesses that wanted to charge their consumers for content or services using the mobile channel.
                A simple text message was used as a payment method for digital goods that were typically consumed on the handset and
                services such purchasing a bus ticket or paying a parking fee. A fee, connected to the sending of the text message, was
                charged to the consumer's monthly mobile phone bill or deducted from their pre-pay account, providing a simple payment method. The mobile operator required the business to share the revenue generated by this transaction. After too many issues with false charges and chage backs, the most carriers are turning this channel off.
                </p>
                
                
                
                <h4> Free to End User SMS (FTEU)</h4>
                <p>In countries, such as the US, where mobile subscribers pay network operator charges to both send and receive SMS
                messages, FTEU messaging allows businesses to set up programs that are free of operator charges for their consumers.
                The business absorbs the messaging cost on behalf of the consumer - whether sending or receiving SMS messages, the
                consumer pays nothing.
                </p>
                
                <h4>Short Codes, Long Numbers and Keywords</h4>
                <p>Common Short Codes, also known simply as "short codes," are abbreviated mobile phone numbers, usually four to six
                digits, used as a destination address for two way text messaging programs. Short codes are easy to input and remember, providing a fast and convenient way for mobile users to interact with a business application using SMS. Short codes
                are always used for premium SMS services, but can also be used for standard rate SMS services. In some markets,
                including the US and Canada, the use of short codes is mandated for all types of A2P messaging.
                Businesses are using short codes to successfully engage consumers via mobile advertising and marketing promotions.
                For example, an advertisement may instruct mobile subscribers to text to "46322" (which spells '4Free' on a handset
                keypad) to enter a contest to win a free prize. Short codes are widely used for value-added mobile services such as
                traffic, weather, sports, banking, and charity donations.
                </p>
                
                <p>As an alternative to short codes, businesses can use a long number as the destination address in a two way messaging
                program. A long number is a standard mobile telephone number that conforms to the numbering system in the relevant
                country. Using a long number has some advantages for A2P messaging programs. Long numbers are less expensive and
                easier to lease than short codes and can receive A2P messages from outside the country in which the phone is hosted
                (in exactly the same way as individuals can text each other internationally). Also, because they are standard phone
                numbers, they can be voice-enabled.
                </p>
                
                <p>Keywords are often used in conjunction with short codes and long numbers in two-way messaging programs. A keyword
                is a short word which a mobile user places at the beginning of an MO SMS message, which triggers a particular response
                from the application that receives the message. For example, a banking client could use the key word "bal" sent to the
                short code "12345" that will provide a return text message from the bank with their current banking balances.
                Some types of messaging programs may only require a one way communication to the consumer, in which case neither a
                short code nor a long number is necessary. The originator of the message (the "number" it appears to come from) can
                be set to an alphanumeric string, such as a brand name. The consumer cannot reply to this type of message.
                Short codes operate at a national level and are provided and managed by a separate ecosystem of companies in each
                country. These include the Common Short Code Administration (CSCA) in the US (or similar organizations in other
                countries) and participating mobile operators. Processes vary by country, but in general short codes are leased
                centrally and then must be provisioned separately on each operator.
                </p>
                
                
                <h4>Travelling and SMS</h4>
                <p>
                A2P messaging programs often send messages to people who are travelling outside of their home country. These
                messages will most often still be delivered to the intended recipient, even when they are travelling abroad. However,
                in most cases an MO message, or response from the mobile phone, cannot be returned via short code because short
                codes are specific to the country in which they are administered and would not be accurately recognized by the
                international operator. However, long numbers would work because they are standard phone numbers.
                </p>
            
            
            
                <h4>Guidelines and Regulations</h4>
                <p>Guidelines, regulations, and enforcement agencies vary by country and region. In the US for example, the Mobile
                Marketing Association (MMA) publishes a set of Consumer Best Practices, the recognized industry guide to implementing
                short code programs, with a focus on consumer protection and privacy. It is a compilation of accepted industry
                practices, wireless carrier policies, and regulatory guidance that have been agreed upon by representative member
                companies from all parts of the ecosystem. In addition to these guidelines, specific regulations may also be enforced by
                each US network operator. It is best to work with an MTN to ensure compliance when creating messaging programs in
                each geographical location.
                </p>
                
                <h4>Managing SMS Internationally</h4>
                <p>
                In today's global market place, many businesses need to expand communications beyond their home shores. Due to the
                simplicity, reach, and universal adoption of SMS, it has become a uniquely global communication channel. Using an MTN
                with global reach allows many SMS programs to be easily deployed in multiple countries, leveraging systems already
                developed, saving money and decreasing time to market.
                </p>
                <p>For a smooth launch into international markets, businesses need to be aware of local regulatory and operator requirements, and administrative processes such as short code registration and program certification. An experienced global
                MTN, who works with numerous operators and Application Providers, can offer full guidance on local markets.
                </p>
            
            
        
            </div>
        
        </div>
	</section>
    
    <section class="pfblock">   
        <div class="container" style="text-align:left;">
            
               
            <h2 style="text-align:center;">High-Level Short Code Example</h2>
            <p>Think of a short code as similar to a domain name and a Keyword similar to a page.</p>
                   
                    
                <div class="">
                    
                        <h4>Step 1</h4>
                        <p>Mobile users sends the keyword "Tickets" to the short code "55555" from the mobile number (949)555-1212. This is an example of a Mobile Originated SMS message.</p>
                    
                </div>
                
                <BR />
                
                <div class="">
                    
                        <h4>Step 2</h4>
                        <p>An #BrandShort# server recieves the request. The #BrandShort# service parses the request and looks up actions related to the keyword "Tickets" relating to the short code "55555" </p>
                    
                </div>
                
                <BR />
                
                <div class="">
                    
                        <h4>Step 3</h4>
                        <p>An #BrandShort# server responds to (949) 555-1212 with a message with special deals on concert tickets from Sports City Tickets.</p>
                    
                </div>
                
                <BR />
                
                <div class="">
                    
                        <h4>Step 4</h4>
                        <p>(949) 555-1212 receives a message for deals on concert tickets from Sports City Tickets. This is an example of a Mobile Terminated SMS message.</p>
                    
                </div>
            
            
       
        </div>
        <!--- /.container --->
	</section>
    
    
    <section class="pfblock pfblock-gray">
     
        <div class="container" style="text-align:center;" >
        
            <h2 class="title text-center" >Enterprise Application Integration for SMS</h2>
        
            <div style="padding-top:15px; text-align:left;">
            
               <h4>Pre-Defined Campaigns (PDC)</h4>
               <p>All remotely triggered campaigns are assigned a Batch Id, a keyword, and a program description.</p>    
               
               <h4>The Batch ID</h4>
               <p>Every keyword or campaign is assigned a system wide unique Batch ID at the time of creation. This Batch ID is used for defining process flow (see Control Points), system usage, data exports, billing, and reporting.</p>
               <p>All batches are given a user defined "Program Description". This description is used to refer to the program on 	ernal and external docs.</p>
                  
                <h4>The API and Pre-Defined Campaigns (PDC)</h4>
                <p>Every request to the PDC API requires, authentication, the target address, the target type, and the Batch ID to trigger.</p>
                <p>Security - API requests are secured by default via Public/Key Private Key authentication. There is support for legacy systems that require the use of only User Name and Password. There is also support for PCI and HIPPA compliant programs via VPN tunnel.</p>
                <p>Custom Data Fields (CDFs) - Optional CDFs may be used to control content and program flow.</p>       
                <p>The real time API can be called (with proper credentials) via <a href="https://<cfoutput>#EBMAPIPath#</cfoutput>/webservice/ebm/pdc/addtorealtime">https://<cfoutput>#EBMAPIPath#</cfoutput>/webservice/ebm/pdc/addtorealtime</a></p> 
                <h4>HELP</h4>
                <p>All short codes are required by carriers to respond to help messages. There is a standard HELP message programmed for all short codes as part of the carrier approval process.</p>
                <p>Optional Batch level HELP - Based on the last triggered Batch on a specific mobile devicve address (the cell phone number), the #BrandShort# system can respond with customizable HELP messages. If no last Batch is found within in 90 days or no custom STOP is defined, then the response will default to the short code level generic HELP text.</p>   
                
                <h4>STOP</h4>
                <p>All short codes are required by carriers to respond to STOP requests. There is a standard STOP message programmed for all short codes as part of the carrier approval process.</p>
                <p>Optional Batch level STOP - Based on the last triggered Batch on a specific mobile devicve address (the cell phone number), the #BrandShort# system can respond with customizable STOP messages. If no last Batch is found within in 90 days or no custom STOP is defined, then the response will default to the short code level generic STOP text.</p>   
                <p>All STOP requests are treated as Short code wide both carrier and legal compliance. Unless a user specically Opts back in via a Mobile Originated keyword, no more messages should be sent to that device.</p>
                                
                <h4>Control Points</h4>
                <p>All SMS flows are either a single keyword response or a series of responses based on control point flow logic.</p>
                <p>Control points can be a Question, Statement, Interval, OPT IN, Data Capture, API Calls, or Branch logic.</p> 
                
                <h4>Delivery Receipts</h4>
                <p>For an additional fee - Delivery Receipts can be requested at the API, Shortcode, or Keyword level. Short Code Receipt ON settings will override Keyword settings. API Receipt ON requests will only override Short Code or Keyword level settings if they are both off.</p>  
                <p>The PDC API request will respond with an inital SMS MT result. The reult will indicate only if the numner is known bad, not a cell number, or is on the DNC (STOP) for that short code. Delivery Receipts get final tracking from carriers that provide. The top carriers offer Delivery Receipts - 90%+ of all SMS do, but there are still some rural carriers that do not provide delivery reciepts. </p>
            
            	<h4>Concatenated SMS Messages</h4>
                <div>
                    <p>Figuring out maximum character counts for standard SMSmessages is really quite simple. However, the maximum character counts for concatenated SMS messages is a bit more complicated. Throw character encodings into the mix, and everything can become very muddled.</p>
                    <p><span id="more-69449"> </span></p>
                    <h5>Encodings</h5>
                    <p>Languages which use a Latin-based alphabet (such as English, Spanish, French, etc.) usually use phones supporting the GSM character encoding. The GSM character encoding uses 7 bits to represent each character (similar to ASCII). This contrasts with non-Latin-based alphabet languages (such as Chinese, Arabic, Sinhala, Mongolian, etc.) which usually use phones supporting Unicode. The specific character encoding utilized by these phones is usually UTF-16 or UCS-2. Both UTF-16 andUCS-2 use 16 bits to represent each character. For the sake of simplicity, I will refer to the Latin-based alphabet and non-Latin-based alphabet languages in this post as &ldquo;GSM&rdquo; and &ldquo;Unicode&rdquo; languages respectively.</p>
                    <h5>Standard SMS Messages</h5>
                    <p>Standard SMS messages have a maximum payload of 140 bytes (1120 bits).</p>
                    <p>Since GSM phones use a 7-bit character encoding, this allows a maximum of 160 characters per standard SMS message:</p>
                    <pre>1120 bits / (7 bits/character) = 160 characters</pre>
                    <p>For Unicode phones, which use a 16-bit character encoding, this allows a maximum of 70 characters per standard SMS message:</p>
                    <pre>1120 bits / (16 bits/character) = 70 characters</pre>
                    <h5>Concatenated SMS Messages</h5>
                    <p>Things get a little bit more complex with concatenated SMS messages. Concatenated SMS messages allow a phone to send messages longer than 160 GSM characters. The sender creates their message as normal, but without the 140 byte limit. Behind the scenes, the phone detects the message length. If the message is less than or equal to 140 bytes, the phone sends a standard SMS message. However, if the message is greater than 140 bytes characters, the phone automatically divides the longer message into multiple, shorter SMS messages which are then transmitted to the recipient separately.</p>
                    <p>The recipient&rsquo;s phone takes these multiple, shorter SMS messages and recombines them into the original message which was sent. Because the individual segments of the complete message need to be recombined in this way, this is referred to as &lsquo;concatenated SMS&rsquo;. In order to achieve this seamless delivery, additional information is added to each individual concatenated SMS message. This additional information, referred to as the user data header (UDH), provides identification and ordering information. For example, the UDH could relate the three individual concatenated SMS messages to each other, and indicate the order for recombination.</p>
                    <p>The UDH takes up 6 bytes (48 bits) of a normal SMS message payload. This reduces the space for actual message data in concatenated SMSmessages:</p>
                    <pre>1120 bits - 48 bits = 1072 bits</pre>
                    <p>As a result, each individual concatenated SMS message can only contain 1072 bits of message data. This plays an important role in determining how many individual concatenated SMS messages will be sent based on the actual message data length.</p>
                    <p><img src="#rootUrl#/#publicPath#/help/images/sms_2.png" alt="SMS payload diagram"></p>
                    <p>Because GSM phones use a 7-bit character encoding, each individual concatenated SMS message can hold 153 characters:</p>
                    <pre>1072 bits / (7 bits/character) = 153 characters</pre>
                    <p>(<em>Note</em>: 153 characters * 7 bits/character = 1071 bits. However, the extra bit can&rsquo;t be used to represent a full character, so it is added as added as padding so that the actual 7-bit encoding data begins on a septet boundary—the 50th bit.)</p>
                    <p>Unicode phones use a 16-bit character encoding, so each individual concatenated SMS message can hold 67 characters:</p>
                    <pre>1072 bits / (16 bits/character) = 67 characters</pre>
                    <h5>Character Count Thresholds</h5>
                    <p>The character limits for individual concatenated SMS messages results in various thresholds for which additional individual concatenated SMSmessages will be required to support sending a larger overall message:</p>
                    <p><strong>GSM encoding:</strong></p>
                    <ul>
                        <li>1 standard SMS message = up to 160 characters</li>
                        <li>2 concatenated SMS messages = up to 306 characters</li>
                        <li>3 concatenated SMS messages = up to 459 characters</li>
                        <li>4 concatenated SMS messages = up to 612 characters</li>
                        <li>5 concatenated SMS messages = up to 765 characters</li>
                        <li>etc. (153 x number of individual concatenated SMS messages)</li>
                    </ul>
                    <p><strong>UTF-16 encoding:</strong></p>
                    <ul>
                        <li>1 standard SMS message = up to 70 characters</li>
                        <li>2 concatenated SMS messages = up to 134 characters</li>
                        <li>3 concatenated SMS messages = up to 201 characters</li>
                        <li>4 concatenated SMS messages = up to 268 characters</li>
                        <li>5 concatenated SMS messages = up to 335 characters</li>
                        <li>etc. (67 x number of individual concatenated SMS messages)</li>
                    </ul>
                    <h5>Implications</h5>
                    <p>These thresholds are an important consideration for a number of reasons including billing, and the programmatic interfacing with SMS gateways.</p>
                    <p>Generally, telephone companies count individual concatenated SMSmessages separately even though they are being recombined at the phone into a single message. This means a GSM encoded message containing 180 characters could potentially invoke a charge for two SMSmessages, even if the sender/recipient only sees a single message.</p>
                    <p>When interfacing with a telephone company&rsquo;s SMS gateway programmatically, there may be limits on the number of individual concatenated SMS messages which can sent as part of a single message. For example, Clickatell&rsquo;s documentation states that messages sent through their API should not contain more than 5 concatenated SMSsegments. This may require limiting the number of character input in a web application or service which sends SMS messages via an API in such a manner.</p>
                    <p>While it may seem elementary, it is important to point out that SMSmessages are always in one particular encoding; i.e. fully GSM or fullyUTF-16. For example, a period character (&rdquo;.&rdquo;) takes up 7-bits in a GSM SMS message. The same character may exist in a Unicode SMS message, but takes up 16-bits, even it is representing the same character.</p>
                </div>
            
            </div>    
                        
        </div>
        
    </section>        
  
</div>
<!--- /#page-content --->

		<cfinclude template="footer.cfm" />
    
    
    </body>

</html>


