<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Top Hits - EBM - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>

	<script src="#rootUrl#/#publicPath#/js/masonry.pkgd.min.js"></script>
	    
</cfoutput>




<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }

	h3
	{
		margin-bottom:10px;	
		
	}
	
	#content li
	{
		margin:8px;	
	}
	
	<!--- Masonary Styles --->
	* {
	  -webkit-box-sizing: border-box;
		 -moz-box-sizing: border-box;
			  box-sizing: border-box;
	}

	
	.masonry {
	  background: none;
	  max-width: 970px;
	}
	
	.masonry .item {
	  width:  240px;
	  height: 240px;
	  float: left;
	  background: #D26;
	  border: 2px solid #333;
	  border-color: hsla(0, 0%, 0%, 0.5);
	  border-radius: 5px;
	}

	
<!---	.item { width: 100%; }
	.item.w2 { width: 25%; }
	.item.w3 { width: 50%; }
	.item.w4 { width: 75%; }
	--->
	.item.w2 { width:  480px; }
	.item.w3 { width:  720px; }
	.item.w4 { width:  960px; }
	
	.item.h2 { height: 200px; }
	.item.h3 { height: 360px; }
	.item.h4 { height: 460px; }

	
</style>
<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{

	<!---	<!--- Bricks of content --->		
		var $container = $('#contentBricks');
		<!---// initialize--->
		$container.masonry({
		  columnWidth: 200,
		  itemSelector: '.item'
		});--->
				
  	});

</script>
</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannerimage">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Innovative Features</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	#BrandShort# systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. 
                    
                 </div>
                <!---
               ---> <img style="position:absolute; top:0px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/channelcubeii.png" height="360" width="480" />
              
            </div>
        </div>
        
        <div id="inner-bg-alt" style=" overflow:visible; min-height: 405px">
            <div id="contentBricks" class="masonry js-masonry"  data-masonry-options='{ "columnWidth": 60 }' style="min-height: 445px overflow:visible;">
               
    
    
    		  <div class="item">SMS Simulator</div>
              <div class="item w2 h2">Time Machine</div>
              <div class="item h3">Campaign Manager</div>   
              <div class="item h2">Triggers</div>   
              <div class="item w3">Dashboards</div> 
              <div class="item">Drip Campaigns </div> 
              <div class="item">Scheduler</div> 
              <div class="item h2">API</div> 
              <div class="item w2 h3">PDC</div> 
              <div class="item">Sub Accounts - Shared Accounts</div>   
              <div class="item h2">Permissions Control</div>   
              <div class="item">CFDs</div>                            
                                        
                                        
              <div class="item"></div>
              <div class="item w2 h2"></div>
              <div class="item h3"></div>
              <div class="item h2"></div>
              <div class="item w3"></div>
              <div class="item"></div>
              <div class="item"></div>
              <div class="item h2"></div>
              <div class="item w2 h3"></div>
              <div class="item"></div>
              <div class="item h2"></div>
              <div class="item"></div>
              <div class="item w2 h2"></div>
              <div class="item w2"></div>
              <div class="item"></div>
              <div class="item h2"></div>
              <div class="item"></div>
              <div class="item"></div>
              <div class="item h3"></div>
              <div class="item h2"></div>
              <div class="item"></div>
              <div class="item"></div>
              <div class="item h2"></div>
  
  
                                     
                                                   
            </div>
        </div>
        
        
    
    
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>