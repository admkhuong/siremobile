<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Home</title>
     
    <!--- IE 9 hates when header stuff is before doc type definition --->
    <cfinclude template="header.cfm">
    


<cfoutput>

	    
    
          
</cfoutput>

           
    <style>
    
     
				
	.coloredBlock 
	{
		padding: 12px;
		background: rgba(255,0,0,0.6);
		color: # FFF;
		width: 200px;
		left: 20% ;
		top: 5% ;
	}
	
	.infoBlock 
	{
		position: absolute;
		top: 30px;
		right: 30px;
		left: auto;
		max-width: 25%;
		padding-bottom: 0;
		background: #FFF;
		background: rgba(255, 255, 255, 0.85);
		overflow: hidden;
		padding: 20px;
		border-radius: 5px 5px 5px 5px;
	}
	
	.infoBlockII 
	{
		background-color: rgba(0, 0, 0, 0.4);
		border-radius: 6px 6px 6px 6px;
		box-shadow: 0 2px 3px 1px rgba(0, 0, 0, 0.15) inset;
		color: #FFFFFF;
		display: block;
		max-width: 25%;
		position: absolute;		
		padding:20px;
	}

	.infoLeft
	{
		left: 90px;		
	}
	
	.infoRight
	{		
		right: 90px;		
	}
	
	.infoTop
	{
		top: 90px;		
	}
	
	.infoBottom
	{
		bottom: 90px;		
	}
	
	.infoCenter
	{
		top: 150px;
	}
	
	
	
	.infoBlockFull 
	{
		position: absolute;
		top: 30px;
		right: 30px;
		left: auto;
		max-width: 90%;
		padding-bottom: 0;
		background: #FFF;
		background: rgba(255, 255, 255, 0.85);
		overflow: hidden;
		padding: 20px;
		border-radius: 5px 5px 5px 5px;
	}
	
	.infoBlockLeftBlack 
	{
		color: #FFF;
		background: #000;
		background: rgba(0,0,0,0.85);
		left: 30px;
		right: auto;
	}
	
	.infoBlockLeftWhite
	{
		color: #000;
		background: #FFF;
		background: rgba(255,255,255,1);
		left: 30px;
		right: auto;
	}
	
	.infoBlockRightWhite
	{
		color: #000;
		background: #FFF;
		background: rgba(255,255,255,0.85);
		right: 30px;
		left: auto;
	}
	
	#slidecontainermain .orangeFont
	{
		color: #F8853B;		
	}
	
	#slidecontainermain .darkBlueFont
	{
		color: #036;		
	}
	
	#slidecontainermain h1
	{
		font-size: 48px;		
		margin: 0;
		padding-bottom: 3px;	
		text-align:left;	
		text-shadow: 0px 1px #F8853B;	
	}
	
	#slidecontainermain h2
	{
		font-size: 36px;		
		margin: 0;
		padding-bottom: 3px;	
		text-align:left;		
	}
	
	#slidecontainermain h3 
	{
		font-size: 28px;		
		margin: 0;
		padding-bottom: 3px;
		text-align:left;	
		font-weight:700;
		text-shadow: 0px 1px #F8853B;
	}
	
	#slidecontainermain h4 
	{
		font-size: 18px;		
		margin: 0;
		padding-bottom: 3px;
		text-align:left;	
		text-shadow: 0px 1px #F8853B;
		line-height: 18px;
	}
	
	#slidecontainermain p
	{
		font-size: 14px;
		margin: 4px 0 0;
		text-align:left;
		text-shadow: 1px 1px #036;	
	}
	
	.ebmorange
	{		
		color:#F8853B;	
	}
	
	.infoBlock p 
	{
		font-size: 14px;
		margin: 4px 0 0;
	}
	
	.infoBlock a
	{
		color: # FFF;
		text-decoration: underline;
	}
	
	.photosBy
	{
		position: absolute;
		line-height: 24px;
		font-size: 12px;
		background: #FFF;
		color: #000;
		padding: 0px 10px;
		position: absolute;
		left: 12px;
		bottom: 12px;
		top: auto;
		border-radius: 2px;
		z-index: 25;
	}
	
	.photosBy a
	{
		color: # 000;
	}
	
	.fullWidth
	{
		max-width: 1400px;
		margin: 0 auto 24px;
	}
	
	@media screen and(min - width: 960px) and(min - height: 660px)
	{
		.heroSlider.rsOverflow,
		.royalSlider.heroSlider 
		{
			height: 520px!important;
		}
	}
	
	@media screen and(min - width: 960px) and(min - height: 1000px) 
	{
		.heroSlider.rsOverflow,
		.royalSlider.heroSlider
		{
			height: 660px!important;
		}
	}
	@media screen and(min - width: 0px) and(max - width: 800px) 
	{
		.royalSlider.heroSlider,
		.royalSlider.heroSlider.rsOverflow
		{
			height: 300px!important;
		}
		
		.infoBlock
		{
			padding: 10px;
			height: auto;
			max-height: 100% ;
			min-width: 40% ;
			left: 5px;
			top: 5px;
			right: auto;
			font-size: 12px;
		}
		
		.infoBlock h3
		{
			font-size: 14px;
			line-height: 17px;
		}
	}
    
	.tileicon
	{
	   	padding-left: 15px;
    	padding-right: 0;
    	width: 100px;
		border-radius: 5px;
		width: 100px;
		height:100px;
		box-shadow: 0 5px 2px #d1d1d1;
		cursor:pointer;
	}
	
	.tileicon:hover
	{
	    background-color: #f8853b !important;
	}
	
	.tileiconBox 
	{		
		border-radius: 5px;
		display: inline-block;
		padding: 10px;	
		outline:none;
		border:none;
	}
	
	.HideOutline
	{
		outline:none;
		border:none;
	}
	
	grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid:after {
		clear: both;
	}
	.grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid {
		width: 100%;
	}
	
	.grid .col {
		float: left;
		font-size: 14px;
		margin-bottom: 15px;
		table-layout: auto;
		width: 50%;
	}
	.col {
		display: table;
		table-layout: fixed;
		width: 100%;
	}
	
	.HomeTilesInfo h2
	{
		font-size: 18px;
		margin-bottom:8px;
	}

	.HomeTilesInfo p
	{
		font-size: 14px;	
	}
	
	.TileCellHome
	{ 
		display: table-cell;
		padding-left:12px;
		vertical-align: top;
		position:relative;">
	}
	
	.HomeTilesInfo a
	{
		color: #07b4ee;
	    text-decoration: underline;	
	}
	
	.HomeTilesInfo a:hover
	{
		color: #f8853b;	    
	}
	
	
		.clear
		{
			clear:both;	
		}
			
		*[class^="prlx"] {
		  position: absolute;
		  width: 100%;
		  height: 300%;
		  top:0;
		  left:0;
		  z-index: -1;
		}
		.prlx-1{ background: url('<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png') repeat; }
		.prlx-2{ background: url('<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png') repeat; }
		
		.section-one h2, .section-three h2{ 
		<!---	
			color: white;
			position: absolute;
			bottom:90px; left: 200px;
			width:700px;
			padding:10px;
			background-color: #232323;--->
			<!---
						border-top: 1px solid;
			color: #ffffff;
			font-size: 25px;
			margin: auto;
			max-width: 90%;
			padding-top: 20px;
			text-transform: uppercase;--->

			height: 150px;	
    		width: 970px;
	

		}
		
		.section-one, .section-two, .section-three{
			width: 100%; min-height: 200px;
			position: relative;
			overflow: hidden;
		}
		.section-one, .section-three{
		}
		
		.SectionHeadline {
			border-top: 1px solid;
			color: #ffffff;
			font-size: 25px;
			margin: auto;
			max-width: 90%;
			padding-top: 20px;
			text-transform: uppercase;
		}
	

		.blue-mask {
			background: #0085c8;
			height: 100%;
			left: 0;
			opacity: 0.7;
			position: absolute;
			top: 0;
			width: 100%;
		}

	    
    </style>
  
   <script type="text/javascript" language="javascript">
   
	   $(function() {	
	   			
			<!--- Cant load HTTPS from HTTP ?!? --->
			<!--- $('#SignInContainer').load(
				<cfoutput>'http://#CGI.SERVER_NAME#/#publicPath#/signin.cfm',</cfoutput>
				function (response, status, xhr){
				;
			});--->
					 
					 
			$(window).scroll(function(){
				$('*[class^="prlx"]').each(function(r){
					var pos = $(this).offset().top;
					var scrolled = $(window).scrollTop();
					$('*[class^="prlx"]').css('top', -(scrolled * 0.5) + 'px');			
				});
			});
				
			
			var preloads = [
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/targetover.png',
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/target.png',
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/engageover.png',
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/engage.png',
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/deliverover.png',
							'<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/deliver.png'
							];
		
			$(preloads).each(function(){
				$('body')[0].src = this;
			});		
		
			$('.hover-create').hover(
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/targetover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
				 }, 
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/target.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px","cursor":"pointer","cursor":"hand"});
				 }
			);		 	  
	  
			  $(".hover-create").click(function(){
				// $(this).hide();
				var slider = $(".royalSlider").data('royalSlider');
				slider.goTo(11);
				slider.startAutoPlay();
			  });
			  
			
			$('.hover-collect').hover(
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/engageover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
				 }, 
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/engage.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
				 }
			);
			
			$(".hover-collect").click(function(){
				var slider = $(".royalSlider").data('royalSlider');
				slider.goTo(7);
				slider.startAutoPlay();			
			});
				
			$('.hover-analyze').hover(
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/deliverover.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
				 }, 
				 function () {
				   $(this).css({"background-image":"<cfoutput>url(#rootUrl#/#PublicPath#</cfoutput>/images/m1/deliver.png)","background-repeat":"no-repeat","width":"191px", "height":"190px", "float":"left", "padding-top":"30px"});
				 }
			);
			
			$(".hover-analyze").click(function(){
				var slider = $(".royalSlider").data('royalSlider');
				slider.goTo(9);
				slider.startAutoPlay();				
			});
							 		   
		 
		  	<!--- Presentation section - Power Point for the web but works --->
			$(".royalSlider").royalSlider({
				<!---// options go here
				// as an example, enable keyboard arrows nav--->
				keyboardNavEnabled: true,
				navigateByClick: false,
				usePreloader: true,
				autoPlay:
				{
					<!---// autoplay options go gere--->
					enabled: true,
					pauseOnHover: true
				},
				deeplinking: 
				{
					<!---// deep linking options go here--->
					enabled: true,
					prefix: 'slide-'
				},
				loop: true
			});  
			
			<!--- Get rid of F.O.U.C. Flash Of Unstyled Content--->
			$(".royalSlider").show();
			
	});
 
  
   </script>
   </head>
   
   <cfoutput>
    <body>
                             
       <div id="main" align="center">
              
	   <!--- EBM site footer included here --->
   		<cfinclude template="act_ebmsiteheader.cfm">
              
	        <div id="homeimage">
                
                <div id="slidecontainermain">
                    <!---<div id="SignInContainer" class="awesome"><cfinclude template="signin.cfm"></div>--->
                    
                    <div id="slideimage" class="royalSlider rsDefault" style="display:none;">
                
						<!---<!--- Elevator Pitch - who we are / what we do --->                        
                        <div class="rsContent" data-rsDelay="10000">
                         
                         	<!---<div class="rsABlock" data-delay="300" data-move-effect="left" style="position:absolute; top:0px; left:140px; width:700px; height:325px; padding:30px 20px 10px 20px; color:##FFF; background: ##000; background: rgba(0, 0, 0, 0.45); overflow: hidden; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		<p></p>
                            </div>--->
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="300" style="position:absolute; max-width:630px; top:0px; left:0px; margin-top:00px; padding:20px 20px 30px 20px; color:##FFF; overflow: hidden;" >
                                <h6>
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#BrandShort# helps you communicate with your customers in ways that will make a difference in their daily lives. Differentiate your brand by making interactions easier. #BrandShort# will help you to be unique in the relevent ways you reach out to and communicate with your customers. Remind them with the ease and quality of every interaction what it is they love about your company, and why they like to do business with you. They will find that not only have you built a connection with them, it is no longer just business, it's personal.
                                </h6>  
                                <p></p>
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; top:0px; left:100px; padding-top:40px; margin: 0;" >
                           		<!---<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/homeWelcome.png" /> ---> 
                            </div>
                                                   
                        </div>--->
                       
                      
                        
                        <!--- Stage --->
                        <div class="" data-rsDelay="8000">
                        
                        
                        	<!--- Left List of Channels--->
                           	<div class="rsABlock" data-move-effect="left" style="position:absolute; top:0px; left:10px; height:325px; padding:30px 20px 10px 20px; color:##FFF; background: ##000; background: rgba(0, 0, 0, 0.45); overflow: hidden; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		<h4 class="ebmorange">Our Platform (SaaS/PaaS)</h4>  <!---  Messaging SaaS--->
                                <h4>Voice / IVR</h4>
                                <h4>SMS</h4>
                                <h4>eMail</h4>
                                <h4>Preference Tools and more...</h4>
                                <h4>&nbsp;</h4>                                
                                <h4 class="ebmorange">EAI Services</h4>
                                <h4>Data Services</h4>
                                <h4>Social media</h4>
                                <h4>Print</h4> 
                                <h4>Radio</h4> 
                                <h4>Online and more...</h4>
                            </div>                  
                            
                            <div class="rsABlock" data-move-effect="left" style="position:absolute; top:0px; left:280px;" >
                                 <img src="#rootUrl#/#PublicPath#/images/m1/dreamstime_xs_25375759.jpg" />
                            </div>
                           
                            <!--- Target Engage Deliver --->                                                        
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; top:40px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Target</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="800" style="position:absolute; top:115px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Engage</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1000" style="position:absolute; top:190px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Deliver</h1>                                
                            </div>
                            
                                                     
        				</div>
                    
                    	<!--- old stuff  --->
                   <!---     <div class="" data-rsDelay="6000">		               		
                            
                            <!--- border-bottom-width:0; border-top-width:0;border-left-width:5px; border-right-width:5px; border-color: rgba(227, 228, 229, 0.40); border-style:solid; --->
                        	<!--- Left List of Channels--->
                           	<div class="rsABlock" data-move-effect="left" style="position:absolute; top:0px; left:140px; height:325px; padding:30px 20px 10px 20px; color:##FFF; background: ##000; background: rgba(0, 0, 0, 0.45); overflow: hidden; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		<h4 class="ebmorange">Self Services</h4>
                                <h4>IVR</h4>
                                <h4>SMS</h4>
                                <h4>Mail</h4>
                                <h4>&nbsp;</h4>                                
                                <h4 class="ebmorange">EAI Services</h4>
                                <h4>Fax</h4>
                                <h4>Social media</h4>
                                <h4>Events</h4> 
                                <h4>Print</h4> 
                                <h4>Radio</h4> 
                                <h4>TV</h4>                             
                                <h4>Online</h4>
                                
                            </div>                            
                            
                            <!--- Arrow map--->
                            <div class="rsABlock" data-move-effect="none" data-delay="800" style="position:absolute; top:50px; left:300px; overflow: hidden;" >
                           		<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/ted_populated_web.png" />                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="left" data-delay="800" style="position:absolute; top:250px; left:600px; overflow: hidden;" >
                           		<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/engage_web.png" />                                
                            </div>                                                                                                  
                            
                            <!--- Target Engage Deliver --->                                                        
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; top:40px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Target</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="800" style="position:absolute; top:115px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Engage</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1000" style="position:absolute; top:190px; left:700px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Deliver</h1>                                
                            </div>
                                                    
                    	</div>--->
                       
                        
                        <!--- Stage --->
                        <div class="" data-rsDelay="4000">
                        
                            <div class="rsABlock" data-move-effect="left" style="position:absolute; top:0px; left:0px;" >
                                 <img src="#rootUrl#/#PublicPath#/images/api/complexsurvey_970x323_web.png" />
                            </div>
                            
                            <div class="rsABlock" data-move-effect="left" style="position:absolute; top:40px; left:90px; max-width: 25%; padding:20px; color:##FFF; background: ##036; background: rgba(0, 33, 66, 0.85); background-color: rgba(0, 33, 66, 0.85); border-radius: 6px 6px 6px 6px; box-shadow: 0 2px 3px 1px rgba(0, 0, 0, 0.15); overflow: auto; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >                           	
                           		<h4>Drag 'n Drop Tools for Visual Message Design</h4>
                                <p>With Pre-Defined Campaigns you can design highly customized messages and business rules that can be accessed with one simple API call.</p>
                            </div>
                            
        				</div>
                        
                        <!--- Surveys --->                        
                        <div class="" data-rsDelay="4000">
                         
                            <div class="rsABlock" data-move-effect="left" style="position:absolute; top:0px; left:150px;" >
                                 <img src="#rootUrl#/#PublicPath#/images/marketing/survey-templates2.png" width="512" height="358" />
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:230px; top:0px; left:0px; padding-top:40px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Surveys</h3>  
                                <p></p>
                            </div>
                            
                            
                            
                            
                                                   
                       	</div>
                        
                        <!--- Alerts --->                        
                        <div class="" data-rsDelay="4000">
                         
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:230px; top:0px; left:0px; padding-top:40px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Alerts</h3>  
                                <p></p>
                            </div>
                                                   
                       	</div>
                                                                                               
                        <!--- Scheduling --->
                        <div class="" data-rsDelay="4000">
                        
                        	<!--- Need sample of call center hours and other program integrations 
							           like card services midnight switch offs ---> 
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:230px; top:0px; left:0px; padding-top:40px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Service<BR />Oriented<BR />Architecture</h3>  
                                <p>#BrandShort#'s Service Oriented Architecture (SOA) helps seamlessly integrate job scheduling as part of your existing overall IT infrastructure. #BrandShort# scheduling services can play a role in the integration of existing business application's workload with new Web Services based real-time applications.</p>
                            </div>
                                                                                
	                        <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; top:0px; left:250px; padding-top:5px; color:##FFF; overflow: hidden;" >
                           		<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/hiw/schedulesample_web.png" />
                            </div>
                                                   
                       	</div>
                        
                        <!--- APIs --->
                        <div class="" data-rsDelay="4000">
                                                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:650px; top:0px; left:50px; padding:20px; color:##FFF; overflow: hidden;" >                           		
                                <h3>API's to Simplify</h3>
                                <p>Build and manage all of the messaging complexity using our simple web based tools. Deliver the messaging within your application with one simple API call. Need to change your messaging? Don't modify or re-compile your app, just update your campaign.</p>                                    
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:350px; top:150px; left:50px; padding:20px; color:##FFF; overflow: hidden;" >                           		
                                <h4>Simplified integration within all your applications.</h4>
                                <p>Web, Mobile, Desktop, Embeded devices, or even your refirgerator. Deliver secure fast reliable messaging from any of your web connected applications.</p>                                    
                            </div>
                
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:350px; top:150px; left:450px; padding:20px; color:##FFF; overflow: hidden;" >                           		
                                <h4>View customer interactions through reporting and monitoring.</h4>
                                <p>Configurable reports based on messaging interactions. View messaging activity, or check on campaign success rates.</p>
                            </div>
                          
                           <div class="rsABlock" data-move-effect="left" data-delay="800" style="position:absolute; top:25px; left:750px; overflow: hidden;" >
                           		<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/tools.png" />                                
                            </div>    
                
                            
                                                   
                       	</div>
                        
                        <!--- Collect Section --->
                        <div class="rsContent">
		                                               
                            <div class="rsABlock" >
                           		<img class="rsImg" src="#rootUrl#/#PublicPath#/images/m1/engage.png" />
                            </div>
                            
                    	</div>
                                                
                        <!--- Big Data - Medium Data - No Data--->
                        <div class="" data-rsDelay="4000">
		                                               
                            <div class="rsABlock" data-move-effect="left" style="position:absolute; top:20px; left:690px; padding:20px; color:##FFF; background: ##000; background: rgba(0, 0, 0, 0.45); background-color: rgba(0, 0, 0, 0.4); border-radius: 6px 6px 6px 6px; box-shadow: 0 2px 3px 1px rgba(0, 0, 0, 0.15); overflow: auto; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		<h4>SFTP</h4>                                
                                <h4>API Post</h4>
                                <h4>HTTP/HTTPS</h4>
                                <h4>Direct SQL DB Transfer</h4> 
                                <h4>Email</h4> 
                                <h4>Direct VPN Tunnel</h4> 
                                <h4>Dial up to secure server</h4>                             
                                <h4>DAT/CD/media via courier</h4>                                                                
                                <h4>and more...</h4>
                            </div>    
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:65%; top:0px; left:0px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Big Data</h3>  
                                <p>Enterprise data feeds. From encrypted SFTP posts to direct DB access through VPN tunnels. Unlimited custom fields for one to one messaging.</p>                              
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="800" style="position:absolute; max-width:65%; top:90px; left:0px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Medium Data</h3>                                
                                <p>Tools to let you upload and manage your own data lists. #BrandShort# also has self service access to unlimited custom fields for one to one messaging. You dont need to be the countries largest Bank or Telecomm provider to get access to our array of enterprise data tools.</p>
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1200" style="position:absolute; max-width:65%; top:200px; left:0px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">No Data</h3> 
                                <p>Use our Customer Preference Portal tools to collect data direct from your customers. Customers can signup for alerts as well as marketing messages.</p>                               
                            </div> 
                       
                        </div>
						                 
                       
						
						<!--- CPP - Little Data - No Data- --->
                        
                        
                        
                        
                        
                        
						
						
                        
						
						<!--- Business Rules --->
                        
						
                        <!--- Analyse section --->
                        <div class="rsContent">
		                                               
                            <div class="rsABlock" >
                           		<img class="rsImg" src="#rootUrl#/#PublicPath#/images/m1/deliver.png" />
                            </div>
                            
                    	</div>
                        
						<!--- AB Testing --->                        
                        <div class="" data-rsDelay="4000">
                         
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:830px; top:0px; left:0px; padding-top:10px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">AB Testing Tools</h3>  
                                <p>Supports multiple message types and formats.</p>
                            </div>
                            
                            <div class="rsABlock" data-move-effect="none" data-delay="800" style="position:absolute; top:70px; left:20px; overflow: hidden;" >
                           		<img class="HideOutline" src="#rootUrl#/#PublicPath#/images/abdemo_web.png" />                                
                            </div>
                                                       
                                                   
                       	</div>                        
						
						<!--- Reporting --->
                        <div class="" data-rsDelay="4000">
                         
                            <div class="rsABlock" data-move-effect="right" data-delay="400" style="position:absolute; max-width:230px; top:0px; left:0px; padding-top:40px; color:##FFF; overflow: hidden;" >
                           		<h3 class="">Reporting</h3>  
                                <p></p>
                            </div>
                                                   
                       	</div>
                        
                        
                        <!--- Infrastructure - Colos - Security - Compliance - Buy vs Build - no setup fees - pay as you go--->
                        <!--- HAU - Transcriptions - Voice Capture - Surveys --->
                        <!--- Inbound - Outbound --->
                        <!--- Brand consistancy accross channels --->
                        <!--- Scipt Library tools --->
                        
                        <!--- need to SEO this text --->
                        
                        
                        <!--- Create Section --->
                        <div class="rsContent">
		                                               
                            <div class="rsABlock" >
                           		<img class="rsImg" src="#rootUrl#/#PublicPath#/images/m1/target.png" />
                            </div>
                            
                    	</div> 
                                                                        
                                        
                    </div>
                    
            	</div>        
                	
			</div>

               <div id="content"> 
        
        	       <div id="toolmain">
                   		<!---<h2>#BrandShort# provides both self service messaging tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for messaging services to top companies.<BR />Platform as a Service (PaaS) - Software as a Service (SaaS) </h2>
                   --->
                        <div id="toolheader">Tools to target, engage and deliver automated cross-channel communications</div>
                        <div id="toolimagemain"> 
                        <!--<div class="toolimage"><a href="##"><img src="#rootUrl#/#PublicPath#/images/m1/target.png" /></a></div>-->
                        <div class="hover-create"></div>
                        
                        <!---cre??ate
[kree-eyt] Show IPA verb, cre??at??ed, cre??at??ing, adjective
verb (used with object)
1.
to cause to come into being, as something unique that would not naturally evolve or that is not made by ordinary processes.
2.
to evolve from one's own thought or imagination, as a work of art or an invention.
3.
Theater . to perform (a role) for the first time or in the first production of a play.
4.
to make by investing with new rank or by designating; constitute; appoint: to create a peer.
5.
to be the cause or occasion of; give rise to: The announcement created confusion.---> 
                        <!---
<p>Knowing your audience is an integral step in creating good messages. The style and tone of a message sent to inform will certainly have a different approach than a message that you are sending to a friend or family member. Depending on the channel, the steps for creating the actual message may differ, but knowing how to write seasoned messages for any event or channel will always remain key.</p>
--->
                        
                        <div class="diverimage"></div>
                        <div class="hover-collect"></div>
                    
                        <div class="diverimage"></div>
                        <div class="hover-analyze"></div>
                        
                    </div>
                   </div>
            </div>
            
            
            <!---<div id="servicemainline"></div>--->
     <!---       
            <section class="section-one">
            	<div class="blue-mask"></div>
                <div class="prlx-1"></div>
                <h2>#BrandShort# provides both self service messaging tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for messaging services to top companies.<BR />Platform as a Service (PaaS) - Software as a Service (SaaS) </h2>
            </section>--->
        
        	
            
            <div id="inner-bg-min" style="background-color:##f9f9f9; margin-top:0;">
            
                <div id="content1">
                    <div id="servicemain">
                         <!---  <div id="serviceheader">#BrandShort#'s provides both self service tools for the small and medium business, as well as entrprise application integration services to the top companies. 
	                            <h6 class="ServicesText">
                                  <!--- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#BrandShort# helps you communicate with your customers in ways that will make a difference in their daily lives. Differentiate your brand by making interactions easier. #BrandShort# will help you to be unique in the relevent ways you reach out to and communicate with your customers. Remind them with the ease and quality of every interaction what it is they love about your company, and why they like to do business with you. They will find that not only have you built a connection with them, it is no longer just business, it's personal.--->
                                </h6>  
                           </div>--->
                           
                           
                           <div class="HomeTilesInfo" style="float:left; width:435px; text-align:left;">
                           
                                
                                <div class="tileicon t1" style="display: table-cell; background-color: ##0E9EFF; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_smc';" >
                                    <a href="hiw_smc">
                                        <img class="tileiconBox" width="42px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/voice_web2.png" style="margin: 30px 29px 20px 29px">
                                    </a>
                                    
                                    <a href="hiw_smc" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>	
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_smc" style="color: ##0E9EFF; text-decoration:none;">Single-Channel, Multi-Channel, and Cross-Channel</a>
                                    </h2>
                                    <p>
                                        Designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. 
                                        <br>
                                        <a class="more" href="hiw_smc" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                 <div class="tileicon" style="display: table-cell; background-color: ##FC442F; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_nas';">
                                    <a href="hiw_nas">
                                        <img class="tileiconBox" width="30px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/sms_web2.png" style="margin: 30px 36px 30px 34px;">
                                    </a>
                                    
                                    <a href="hiw_nas" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_nas" style="color: ##FC442F; text-decoration:none;">Notifications, Alerts and Surveys</a>
                                    </h2>
                                    <p>
                                        One to one, one to many, message branching, business rules, or even interactive response,  
                                        <br>
                                        <a class="more" href="hiw_nas" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                <div class="tileicon" style="display: table-cell; background-color: ##0085c8; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_lis';">
                                    <a href="hiw_lis">
                                        <img class="tileiconBox" width="31px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/agentfemale.png" style="margin: 30px 37px 30px 32px;">
                                    </a>
                                    
                                    <a href="hiw_lis" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_lis" style="color: ##0085c8; text-decoration:none;">Live and Virtual Interactive Sessions</a>
                                    </h2>
                                    <p>
                                        Auto Respond, Keywords, Agent Assist, IVR, Surveys, Trickle Campaigns, Agent Response.
                                        <br>
                                        <a class="more" href="hiw_lis" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                           
                           
                           </div>
                           
                           
                            <div class="HomeTilesInfo" style="float:right; width:435px; text-align:left;">
                           
                           
                             	<div class="tileicon" style="display: table-cell; background-color: ##006BB2; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_cpp';">
                                    <a href="hiw_cpp">
                                        <img class="tileiconBox" width="30px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/cppicon.png" style="margin: 30px 34px 30px 32px;">
                                    </a>
                                    
                                    <a href="hiw_cpp" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_cpp" style="color: ##006BB2; text-decoration:none;">Customer Preference Portal Tools</a>
                                    </h2>
                                    <p>
                                        Quickly create and manage, account preferences, and privacy settings, or even start new marketing lists for your customers and prospects. 
                                        <br>
                                        <a class="more" href="hiw_cpp" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                 <div class="tileicon" style="display: table-cell; background-color: ##00C0C8; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_red';">
                                    <a href="hiw_red">
                                        <img class="tileiconBox" width="32px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/dashboard3.png" style="margin: 30px 39px 30px 29px;">
                                    </a>
                                    
                                    <a href="hiw_red" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_red" style="color: ##00C0C8; text-decoration:none;">Reports, Exports and Dashboards</a>
                                    </h2>
                                    <p>
                                        Getting an insightful snapshot of important metrics doesn't have to require hours of work in a spreadsheet.
                                        <br>
                                        <a class="more" href="hiw_red" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                 <div class="tileicon" style="display: table-cell; background-color: ##C86F00; position:relative;" onclick="window.location='#rootUrl#/#PublicPath#/hiw_eai';">
                                    <a href="hiw_eai">
                                        <img class="tileiconBox" width="60px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/interactive1.png" style="margin: 30px 20px;">
                                    </a>
                                    
                                    <a href="hiw_eai" style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_eai" style="color: ##C86F00; text-decoration:none;">Enterprise Application Integration</a>
                                    </h2>
                                    <p>
                                    	When carefully managed, EAI can be used to help an organization to better achieve goals, remove obstacles to market growth, reduce costly mistakes.
                                        <br>
                                        <a class="more" href="hiw_eai" style="position:absolute; bottom:0px; left:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                           
                           
                           </div>
                           
                           
                           
                           
                   <!---        
                           <div id="serviceimagemain">
                                <div class="firstcontentbox">
                                       <div class="serviceimagefull">
                                            <div class="serviceimageinner">
                                            <!---Personalized messages with content that is high quality, relevant, personalized, and delivered in context--->
                                            <!---Dedicated Staffing<BR />Design Services<BR />ETL&R on Data<BR />Data Feeds<BR />Custom API Integration<BR />VPN Support<BR />--->
                                            
                                            </div>
                                       </div>
                                       <div class="contentheadertitle"><a href="##">Enterprise Application Integration</a></div>   
                                       <div class="ServicesText">Is your IT team's existing workload preventing you from moving forward on critical projects? Let our professional services team help. <!---Let #BrandShort#'s Team of experts handle everything!---></div>                            
                                </div>
                                <div class="firstcontentbox">
                                        <div class="serviceimageself">
                                            <div class="serviceimageinner"><!---Pre-Defined Campaign Tools<BR />Easy API's<BR />---></div>
                                        </div>    
                                        <div class="contentheadertitle"><a href="##">Self Service</a></div> 
                                        <div class="ServicesText">A powerful set of tools and intuitive interfaces to let you design and launch messaging campaigns</div>                                   
                                </div>
                                <div class="firstcontentbox">
                                       <div class="serviceimagehybrid">
                                            <div class="serviceimageinner"><!---Our staff can help you setup automated programs to get your application up and running fast!---></div>
                                       </div>
                                       <div class="contentheadertitle"><a href="##">Hybrid Model</a></div>                               
                                       <div class="ServicesText">Our staff can help you setup automated programs to get your applications up and running!</div>
                                </div>
                            </div>
                            --->
                            
                       </div>
                </div>
            
            </div>
                           
               <!--- EBM site footer included here --->
               <cfinclude template="act_ebmsitefooter.cfm">
           </div>
    </div>
       </body>
</cfoutput>
</html>
