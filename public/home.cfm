<!DOCTYPE html>
<html lang="en">
<head>

	<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>EBM - Event Based Messaging</title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
       
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
			
			a 
			{
				color: ##E7746F;
			}
		</style>
        
    </cfoutput>
      
       
     
</head>
<body>

	<!-- Preloader -->

	<div id="preloader">
		<div id="status"></div>
	</div>
  
	<cfif Session.USERID GT 0>
        <cfinclude template="home7assets/inc/signout.cfm">                      
    <cfelse>                        
        <cfinclude template="home7assets/inc/signin.cfm">                                         
    </cfif>
 
	<!--- Home start --->

	<section id="home" class="pfblock-image screen-height">
        <div class="home-overlay"></div>
		<div class="intro">
        
        
			<div class="start">
				
                <cfoutput>
					<img src="#LogoImageLink7#" style="border:none;" />
				</cfoutput>
            	    
            </div>
            			
            <h1>Event Based Messaging</h1>
			<div class="start">Tools for creating and managing modern responsive consumer communication applications</div>

            <!--- Account log in / out options --->
            <div>                                           
                
				<cfif Session.USERID GT 0>
                    <cfoutput><a id="InSessionAccount" class="InlineLink" href="#rootUrl#/#SessionPath#/account/home">My Account</a></cfoutput>
                    <a href="javascript:;" class="InlineLink" id="signout-popup-with-form" data-mfp-src="#confirm-logout" >Log Out</a>                        
                <cfelse>                        
                    <a id="signin-popup-with-form" href="javascript:;"  data-mfp-src="#EBMSignInSection_0" class="InlineLink">Sign In</a>
                    <a id="signuplink" href="signup" class="InlineLink" >Sign Up</a>                                          
                </cfif>
                           
            </div>
            
		</div>

        <a href="#services">
		<div class="scroll-down">
            <span>
                <i class="fa fa-angle-down fa-2x"></i>
            </span>
		</div>
        </a>

	</section>

	<!--- Home end --->

	<!--- Navigation start --->

	<header class="header">

		<nav class="navbar navbar-custom" role="navigation">

			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#home"><cfoutput><img src="#LogoImageLinkSmall7#" style="border:none;" /></cfoutput></a>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#home">Home</a></li>
						<li><a href="#services">Services</a></li>
                        <li><a href="#portfolio">Works</a></li>
                        <li><a href="#results">Results</a></li>
						<li><a href="#testimonials">Testimonials</a></li>
						<li><a href="#contact">Contact</a></li>
					</ul>
				</div>

			</div><!-- .container -->

		</nav>

	</header>

	<!--- Navigation end --->

    <!--- Services start --->

	<section id="services" class="pfblock pfblock-gray">
		<div class="container">
			<div class="row">

				<div class="col-sm-6 col-sm-offset-3">

					<div class="pfblock-header wow fadeInUp">
						<h2 class="pfblock-title">What we do</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							<cfoutput>#BrandShort#</cfoutput> systems are engineered to execute high quality compliant interactive messaging that is, relevant, personalized, and delivered in context.
						</div>
					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-sm-3">

					<div class="iconbox wow slideInLeft">
						<div class="iconbox-icon">
							<!---<span class="icon-magic-wand"></span>--->
                            <img src="home7assets/images/m7/icon-cms-small.png" alt="CMS" />
						</div>
						<div class="iconbox-text vexpander">
							<h3 class="iconbox-title">Content Management</h3>
							<div class="iconbox-desc" title="Segmented Messaging with Automated Response and Tracking.">
								S.M.A.R.T &trade;  Technology<BR>SMS, eMail, Voice, and more.
							</div>
						</div>
					</div>

				</div>

				<div class="col-sm-3">

					<div class="iconbox wow slideInLeft">
						<div class="iconbox-icon">
							<!---<span class="icon-puzzle"></span>--->
                            <img src="home7assets/images/m7/icon-delivery.png" alt="Message Delivery" />
						</div>
						<div class="iconbox-text vexpander">
							<h3 class="iconbox-title">Delivery & Fulfillment</h3>
							<div class="iconbox-desc">
								<!---Whitelisting, Carrier Management, Aggregation, LCR, Live Agent Transfer &trade; --->
                                Outbound, Inbound, Over the Top
							</div>
						</div>
					</div>

				</div>

				<div class="col-sm-3">

					<div class="iconbox wow slideInRight">
						<div class="iconbox-icon">
							<!---<span class="icon-badge"></span>--->
                            <img src="home7assets/images/m7/mobile-marketing-3-144.png" alt="Mobile Marketing" />
						</div>
						<div class="iconbox-text vexpander">
							<h3 class="iconbox-title">Mobile Marketing</h3>
							<div class="iconbox-desc">
								Preference Managment, Analytics, Dashboards  
							</div>
						</div>
					</div>

				</div>

				<div class="col-sm-3">

					<div class="iconbox wow slideInRight">
						<div class="iconbox-icon">
							<!---<span class="icon-question"></span>--->
                            <img src="home7assets/images/m7/icon-consult.png" alt="Expertise" />
						</div>
						<div class="iconbox-text vexpander">
							<h3 class="iconbox-title">Operations & Consultation</h3>
							<div class="iconbox-desc">
								Experience, Planning, Implementation
							</div>
						</div>
					</div>

				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</section>

	<!--- Services end --->
        
	<!--- Portfolio start --->
    <style>
		
		<!---.fixaspect
		{
			
  display: block;
  max-width: 100%;
  max-height: 100%;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  
  display: block;
  width: 100%;
  position: relative;
  height: 0;
  padding: 56.25% 0 0 0;
  overflow: hidden;
			
		}--->
	
	</style>

	<section id="portfolio" class="pfblock">
		<div class="container">
			<div class="row">

				<div class="col-sm-6 col-sm-offset-3">

					<div class="pfblock-header wow fadeInUp">
						<h2 class="pfblock-title">How we do it</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							When you use amazing tools, you can create amazing things.
						</div>
					</div>

				</div>

			</div><!-- .row -->
            
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-4 col-md-4">
                    
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-1.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Tools for <span>Compliance</span></h2>
                                <p>Preference management. Opt In, Opt Out, Opt down</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-2.jpg" alt="img01"  class="fixaspect"/>                            
                            <figcaption>
                                <h2>EMS <span>e-Messaging Services</span></h2>
                                <p>Quick command and control</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-3.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Powerful <span>API</span></h2>
                                <p>Simple API for PDC</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-4.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Expertise and <span>Consulting</span></h2>
                                <p>Remove obstacles to market growth, reduce costly mistakes</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-5.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Reporting and <span>Analytics</span></h2>
                                <p>Campaign Results Monitoring and Analysis</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-6.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>EAI<span></span></h2>
                                <p> Enterprise Application Integration helps your organization to better achieve goals. </p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
                    
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-2.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Quality <span>Assurance</span></h2>
                                <p>QA Tools, AB Testing</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-1.jpg" alt="img01" class="fixaspect"/>                            
                            <figcaption>
                                <h2>CMS <span></span></h2>
                                <p>Content Management Systems</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-4">
            
                    <div class="grid wow zoomIn">
                        <figure class="effect-bubba">
                            <img src="home7assets/images/item-3.jpg" alt="img01" class="fixaspect"/>
                            <figcaption>
                                <h2>Agent <span>Portals</span></h2>
                                <p>Standard and Custom Agent Interfaces</p>
                            </figcaption>			
                        </figure>
                    </div>
                    
                </div>
                
            </div>


		</div><!-- .contaier -->

	</section>

	<!--- Portfolio end --->
    
    <!--- results start --->
    
    <section class="pfblock pfblock-gray" id="results">
		
			<div class="container">
			
				<div class="row skills">
					
					<div class="row">

                        <div class="col-sm-6 col-sm-offset-3">

                            <div class="pfblock-header wow fadeInUp">
                                <h2 class="pfblock-title">Results</h2>
                                <div class="pfblock-line"></div>
                                <div class="pfblock-subtitle">
                                    Organizations are undergoing a seismic shift in how they communicate and deliver services to customers. Our products help you to reach them quickly in today's social, mobile, data-driven world with applications and services that are delivered at the speed of now.
                                </div>
                            </div>

                        </div>

                    </div><!-- .row -->
                    
                    
                    <!--- 97% live answer detection accuracy  --   Regular survey resukts 30% completion rate   --->
					
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="100" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <!---<span class="percent">100</span>---><span class="piecenter">10</span>
                        </span>
						<h3 class="text-center">Billion Messages Delivered</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="100" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="piecenter">15</span>
                        </span>
						<h3 class="text-center">Years Experiece</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="100" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="piecenter">104,000</span>
                        </span>
						<h3 class="text-center" title="Buy or Build?">Enterprise Man Hours You can Save</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="100" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="piecenter">10X</span>
                        </span>
						<h3 class="text-center">SMS Survey take Rate vs eMail and Voice</h3>
					</div>
					
				</div><!--End row -->
			
			</div>
		
    </section>
    
    <!--- results end --->

	<!--- CallToAction start --->

	<section class="calltoaction">
		<div class="container">

			<div class="row">

				<div class="col-md-12 col-lg-12">
					<h2 class="wow slideInRight" data-wow-delay=".1s">ARE YOU READY TO START?</h2>
					<div class="calltoaction-decription wow slideInRight" data-wow-delay=".2s">
						Launch your project in minutes.
					</div>
				</div>

				<div class="col-md-12 col-lg-12 calltoaction-btn wow slideInRight" data-wow-delay=".3s">
					<a href="signup" class="btn btn-lg">Sign Up Now</a>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</section>

	<!--- CallToAction end --->

	<!--- Testimonials start --->

	<section id="testimonials" class="pfblock pfblock-gray">

		<div class="container">
            
            <div class="row">

				<div class="col-sm-6 col-sm-offset-3">

					<div class="pfblock-header wow fadeInUp">
						<h2 class="pfblock-title">What our clients say</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							We appreciate your feedback. Strong, positive endorsements help us promote our personal brand and provide us with the professional support we need to succeed in reaching our goals. 
						</div>
					</div>

				</div>

			</div><!-- .row -->

            <div class="row">

			<div id="cbp-qtrotator" class="cbp-qtrotator">
                <div class="cbp-qtcontent">
                    <img src="home7assets/images/client-1.jpg" alt="client-1" />
                    <blockquote>
                      <p>Working with <cfoutput>#BrandShort#</cfoutput> was a pleasure. They understood exactly what I wanted and created an awesome experience for my customers.</p>
                      <footer>Bruce</footer>
                    </blockquote>
                </div>
                <div class="cbp-qtcontent">
                    <img src="home7assets/images/client-2.jpg" alt="client-2" />
                    <blockquote>
                      <p>I'm really happy with the results. Getting 100% satisfaction is difficult but Jeff got it without problems.</p>
                      <footer>Allison</footer>
                    </blockquote>
                </div>

            </div>		

            </div><!-- .row -->

					
		</div><!-- .row -->
	</section>

	<!--- Testimonial end --->


	<!--- Contact start --->

	<section id="contact" class="pfblock">
		<div class="container">
			<div class="row">

				<div class="col-sm-6 col-sm-offset-3">

					<div class="pfblock-header">
						<h2 class="pfblock-title">Get in Touch</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							We are here to answer any questions you might have about our services. Feel free to call or email us.
                                                    
                            <div class="ContactLocations">    
                                <ul>									
                                    <li class="contact-dot"><cfoutput>#BrandingContactAddress#</cfoutput></li>
                                    <li class="contact-call"><cfoutput>#BrandingContactPhone#</cfoutput></li>
                                    <li class="contact-msg"><cfoutput>#BrandingContactEmail#</cfoutput></li>                                    
                                </ul>
            
                            </div>                               
                            
                            Or you can reach out to us using the contact form below and we will get back to you shortly.                             
                             
						</div>
					</div>

				</div>

			</div><!-- .row -->

			<div class="row">



				<div class="col-sm-6 col-sm-offset-3">

					<form id="contact-form" role="form" action="info">
						<div class="ajax-hidden">
							<div class="form-group wow fadeInUp">
								<label class="sr-only" for="infoName">Name</label>
								<input type="text" id="infoName" class="form-control" name="infoName" placeholder="Name">
							</div>

							<div class="form-group wow fadeInUp" data-wow-delay=".1s">
								<label class="sr-only" for="infoContact">Email</label>
								<input type="email" id="infoContact" class="form-control" name="infoContact" placeholder="E-mail">
							</div>

							<div class="form-group wow fadeInUp" data-wow-delay=".2s">
								<textarea class="form-control" id="infoMsg" name="infoMsg" rows="7" placeholder="Message"></textarea>
							</div>

							<button type="submit" class="btn btn-lg btn-block wow fadeInUp" data-wow-delay=".3s">Send Message</button>
						</div>
						<div class="ajax-response"></div>
					</form>

				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</section>

	<!--- Contact end --->

	<cfinclude template="footer.cfm" />

	<!--- Scroll to top --->

	<div class="scroll-up">
		<a href="#home"><i class="fa fa-angle-up"></i></a>
	</div>
    
    <!--- Scroll to top end --->

	<!--- Javascript files --->

	
	<script src="home7assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="home7assets/js/jquery.parallax-1.1.3.js"></script>
	<script src="home7assets/js/imagesloaded.pkgd.js"></script>
	<script src="home7assets/js/jquery.sticky.js"></script>
	<script src="home7assets/js/smoothscroll.js"></script>
	<script src="home7assets/js/wow.min.js"></script>
    <script src="home7assets/js/jquery.easypiechart.js"></script>
    <script src="home7assets/js/waypoints.min.js"></script>
    <script src="home7assets/js/jquery.cbpQTRotator.js"></script>
	<script src="home7assets/js/custom.js"></script>
    
    
    

<script type = "text/javascript" >


$( document ).ready(function() {

	<!--- Fix heights to match max - add well class --->
    var heights = $(".vexpander").map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".vexpander").height(maxHeight);
	
	<!---
	/* ---------------------------------------------- /*
		 * E-mail validation
	/* ---------------------------------------------- */--->

	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};

	
	<!--- CONTACT FORM--->
	$('#contact-form').submit(function(e) {
		
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");
			
		var infoName = $('#infoName').val();
		var infoContact = $('#infoContact').val();
		var infoMsg = $('#infoMsg').val();
		var response = $('#contact-form .ajax-response');
		
		if (( infoName== '' || infoContact == '') || (!isValidEmailAddress(infoContact) )) 
		{
			response.fadeIn(500);
			response.html('<i class="fa fa-warning"></i> Please provide your name and a valid conact email address and try again.');
		}
		
		else 
		{	
			
			$.ajax(
			{
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data, textStatus, jqXHR) 
				{
					$('#contact-form .ajax-hidden').fadeOut(500);
					response.html("Message Sent. We will contact you asap. Thanks.").fadeIn(500);		
			
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					//if fails      
				}
			});
			
			e.preventDefault(); 
							
			
		}
		
		return false;
	});
			
			
			
});

</script>
    

</body>
</html>