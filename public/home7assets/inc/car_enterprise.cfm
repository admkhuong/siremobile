<cfoutput>



<div id="TheEnterpriseContainer" class="EBMPresentationContainer EBMPresentationCarousel col-md-12">                                
                    
                    <div id="EnterpriseCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="##EnterpriseCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="##EnterpriseCarousel" data-slide-to="1"></li>
                        <li data-target="##EnterpriseCarousel" data-slide-to="2"></li>
                        <li data-target="##EnterpriseCarousel" data-slide-to="3"></li>
                      </ol>
                    
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                         
                         	<div class="carousel-content">
                         		
                                <row>
                                    <div class="section-title-content clearfix col-md-12">
                                        <header class="section-title-inner">
                                            <h2>Who uses EBM?</h2>
                                            <p>SMB and the Enterprise</p>
                                        </header>
                                    </div>
                				</row>
                                
                                <row>
                                
                                    <div class="col-md-4">
                                    	<img src="home7assets/images/m7/smb01.jpg" height="316px" width="380px" style="border:none; display:inline; margin-right:10px;" />
                                   
                                    </div>
                                    
                                    <div class="col-md-8">
                                     	<p>#BrandShort# provides both self service Express Messaging Services (EMS) tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for more complex messaging services.</p>
                                    	
                                        <div style="text-align:center;">    
                                        	<img src="home7assets/images/m7/eai02.png" height="256px" width="413px" style="border:none; display:inline;" />
                                    	</div>
                                    </div>
                                                                        
                                </row>
                            </div>
                            
                        </div>
                    
                        <div class="item">
                        
                        	<div class="carousel-content">
                              
                            </div>    
                         
                        </div>
                    
                        <div class="item">
                        	
                            <div class="carousel-content">
                                
	                        </div>
                          
                        </div>
                    
                        <div class="item">
                        	 <div class="carousel-content">
                               
                               
                          
                            </div>
                        
                        </div>
                      </div>
                    
                    
                    	<div class="carousel-caption">
                            <div class="EnterpriseObj">Target Market</div>
                           <!--- <p>CYCLE 1</p>--->
                      	</div>
                          
                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="##EnterpriseCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="##EnterpriseCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    
                </div>



</cfoutput>













