<cfoutput>


<!---

	Events can be triggered by agent forms 
	Application embeded triggers




Call to Rest API
Auto Generated Flat Files
Montiored Data Store
Result 


<ul>
	<li>Bulk one to many</li>
	<li>One to One</li>
</ul>
								


Move sampls to consumers - why would they need messaging - earlier on 

 <ul>
                                            <li>Store Visit</li>
                                            <li>Call Center / Agent Interaction</li>
                                            <li>Web Site Interaction</li>
                                            <li>Mobile App Interaction</li>
                                            <li>Social Activity</li> 
                                            <li>Scheduled Events</li>
                                            <li>Lifecycle State Change</li>
                                            <li>Appointments</li>
                                            <li>Drip Marketing</li>
                                            <li>Many More....</li>
                                        </ul>
										
										
										 <row>
                                    <div class="col-md-12" style="text-align:center; margin-top:35px;">
                                    	<p>EBM systems provide messaging that is high quality, relevant, personalized, and delivered in context.</p>
                                    </div>
                				</row>   
								
								
--->

<div id="TheEvent" class="EBMPresentationContainer EBMPresentationCarousel col-md-12">                                
                    
                    <div id="EventCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="##EventCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="##EventCarousel" data-slide-to="1"></li> 
                        <li data-target="##EventCarousel" data-slide-to="2"></li>                       
                      </ol>
                    
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                       
                       
                        <div class="item active">
                        	
                            <div class="carousel-content">
                            
                            
	                             <row>
                                    <div class="section-title-content clearfix col-md-12">
                                        <header class="section-title-inner">
                                           
                                            <h2>Tools and support for Omnichannel engagement</h2>
                                            <p>Event Based Communications</p> 
                                        </header>
                                    </div>
                				</row>
                                
                                <row>
                                    <div class="col-md-12">
                                    	<p>Communication events are triggered throughout the customer lifecycle</p>
                                    </div>
                				</row>                                
                                
                            	<row>
                                
                                	<div class="col-md-2">
                                	
                                    </div>
                                    
                                    <div class="col-md-2">
                                   
                                   
                                        <ul style="text-align:left; width:450px; display: inline-block;">                            
                                            <li>Real Time</li>
                                            <li>Via Batch Loading</li> 
                                            <li>Business Initiated</li>
                                            <li>#BrandShort# Initiated</li>                          
                                            <li>Consumer Initiated</li>
                                        </ul>
                                   		                                        
                                    </div>
                                    
                                    <div class="col-md-6">
                                     	<div style="text-align:left;">    
                                        	<img src="home7assets/images/m7/interactions31.png"  width="450px" height="375px"style="border:none; display:inline; margin-top:0px;" />
                                    	</div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                	
                                    </div>
                                                                        
                                </row>
                                
                                
                            	
	                        </div>
                          
                        </div>
                        
                                          
                                          
                        <div class="item">
                        	 <div class="carousel-content">
                             
                             
                              <row>
                                    <div class="section-title-content clearfix col-md-12">
                                        <header class="section-title-inner">
                                           
                                            <h2>How can #BrandShort# be integrated into your Application?</h2>
                                            <p>Data Transfer Mechanisms</p> 
                                        </header>
                                    </div>
                				</row>
                                
                                <row>
                                                                           
                                    <div class="col-md-6">
                                                                                                            
                                        <ul style="text-align:left; width:450px; display: inline-block;">                            
                                            <li>HTTP/HTTPS - via REST API's</li>
                                            <li>SFTP</li>                           
                                            <li>HTTP/HTTPS - via form or Event variables</li>
                                            <li>HTTP/HTTPS - via direct XML</li>
                                            <li>HTTP/HTTPS - via SOAP</li>
                                            <li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
                                            <li>email</li>
                                            <li>SMPP</li>
                                            <li>XMPP - Chat</li>
                                            <li>Direct VPN Tunnel</li>
                                            <li>Dial up to secure server</li>
                                            <li>DAT/CD/media via courier</li>
                                            <li>FAX</li>
                                        </ul>
                                
                                
                                        <p><h4>Tip:</h4>Re-use and repurpose existing data transfer assets to save time and reduce IT costs.</p>
                                  
                                    </div>
                                  
                                    <div class="col-md-6">
                                     	<div style="text-align:center;">    
                                        	<img src="home7assets/images/m7/transfer_data.png"  width="430px" height="430px"style="border:none; display:inline;" />
                                    	</div>
                                    </div>
                                
                                </row>
                            </div>
                        
                        </div>
                        
                    
                     <div class="item">
                         <div class="carousel-content">
                           
                           <row>
                                <div class="section-title-content clearfix col-md-12">
                                    <header class="section-title-inner">
                                       
                                        <h2>How customers are using #BrandShort#</h2>
                                        <p>Sample Lifecycle Events</p> 
                                    </header>
                                </div>
                            </row>
                                
                              
                              <row style="font-size:.8em;">   
                                    <div class="col-md-4">
                                    
                                        <ul>
                                            <li>Account Threshold Alerts</li>
                                            <li>Order Confirmation</li>
                                            <li>Order Completion</li>
                                            <li>Order Delays</li>
                                            <li>Go Green Initiatives</li>
                                            <li>Repair Ticket Completion</li>
                                            <li>Local Svc. Change</li>
                                            <li>Service Order Completion</li>
                                            <li>Rate Changes</li>
                                            <li>Payment Options</li>
                                            <li>Non-Usage Alerts</li>
                                            <li>Termination Warnings / Notices</li>
                                            <li>Account Suspension</li>
                                            <li>Corporate Fraud Notification</li>
                                            <li>Change Order Notification</li>
                                            <li>Event Alerts</li>
                                            <li>Event Changes</li>
                                            <li>Service Outage</li>
                                            <li>Technician in Route</li>
                                        </ul>
                                    
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <ul>
                                            <li>Multi Factor Authentication</li>
                                            <li>Help</li>
                                            <li>Rewards Balance</li>
                                            <li>Rewards Signup</li>
                                            <li>Offers</li>
                                            <li>Reservations - New</li>
                                            <li>Reservations - Cancellation</li>
                                            <li>Reservations - Change</li>
                                            <li>Product Retirement</li>
                                            <li>Product Availability</li>
                                            <li>Exceed Usage</li>
                                            <li>Trouble Resolution</li>
                                            <li>Firmware Updates</li>
                                            <li>Product Recalls</li>
                                            <li>Weather Emergency</li>
                                            <li>Emergency Notifications</li>
                                            <li>Disaster Recovery</li>
                                            <li>Password Changed Alerts</li>
                                            <li>New Computer Accessed Your Account</li>
                                            
                                        
                                        </ul>
                                    
                                    </div>
                                    
                                    <div class="col-md-4">
                                    
                                        <ul>
                                            <li>Coupons</li>
                                            <li>Virtual Assistance</li>
                                            <li>First Bill Explanation</li>
                                            <li>Bill Pay Processed</li>
                                            <li>Billing Errors</li>
                                            <li>Credit Card Expired</li>
                                            <li>Bill Available Online Now</li>
                                            <li>Appointment Reminders</li>
                                            <li>Surveys</li>
                                            <li>Government Mandates</li>
                                            <li>Blasts</li>
                                            <li>Political Messages</li>
                                            <li>Program Eligibility</li>
                                            <li>Program Enrollment</li>
                                            <li>Profile Changes</li>
                                            <li>Customer Preferences</li>
                                            <li>Running Late</li>
                                            <li>Nobody Home</li>
                                            <li>Prescription Ready for Pickup</li>
                                            <li>Tracking Numbers</li>                                    
                                        </ul>
                                        
                                    </div>               

                         		</row>
                                                                   
	                     	</div>
                        
                        </div>
                            
                        
                      </div>
                    
                    
                    
                        
                    	<div class="carousel-caption">
                            <div class="process-step-box-caption Step1">Lifecycle Events</div>
                           <!--- <p>CYCLE 1</p>--->
                      	</div>
                          
                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="##EventCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="##EventCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    
                </div>



</cfoutput>













