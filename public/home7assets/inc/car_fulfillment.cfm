<cfoutput>


<!--- 


Notes:

Some Direct Carrier Relationships

AT&T
Level 3
XO
Verizon





--->


<div id="TheFulfillment" class="EBMPresentationContainer EBMPresentationCarousel col-md-12">                                
                    
                    <div id="FulfillmentCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="##FulfillmentCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="##FulfillmentCarousel" data-slide-to="1"></li>
                        <li data-target="##FulfillmentCarousel" data-slide-to="2"></li>
                        <li data-target="##FulfillmentCarousel" data-slide-to="3"></li>
                      </ol>
                    
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        
                        	 <div class="item active">
                                
                               <div class="carousel-content">
                                                       
                                    <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>SMS, Voice, eMail, and more... </h2>
                                                <p>#BrandShort# Fulfillment</p> 
                                            </header>
                                        </div>
                                    </row>
                                    
                                   <row>
                                
                                        <div class="col-md-6">
                                            <div style="text-align:left;">    
                                                <img src="home7assets/images/m7/globalmultichanneliv.png"  width="480px" height="450px"style="border:none; display:inline; margin-top:10px;" />
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                                                              
                                            <p>The EBM directly provides best of breed tools and support for messaging via the following channels: </p>
                        
                                                <ul style="margin: 8px 0 0 30px;">
                                                    <li>
                                                        Voice   
                                                    </li>
                                                    
                                                    <li>
                                                        SMS
                                                    </li>
                                                                                    
                                                    <li>
                                                        eMail
                                                    </li>
                                                    
                                                    <li>
                                                        Online/Web
                                                    </li>
                                                    
                                                    <li>
                                                        Preference
                                                    </li>
                                                   
                                                </ul>
                                            
                                             <br/>
                                            <p style="padding-bottom:20px;">
                                                Additionally #BrandShort# platform provides tools and support for messaging via the following third party channels:
                                                FAX, Social Media, Print, Radio, TV, and any other channel we can communicate with publicly (Web) or privately (VPN)                                                                                                           
                                            </p>
                                                                                    
                                        </div>
                                                                                                                    
                                    </row>
                                    
                                </div>
                              
                        	</div>
                            
                    
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                     <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                 <h2>Single-Channel</h2>
                        						 <p>Point-to-Point Semantics</p>
                                            </header>
                                        </div>
                                    </row>
                                    
                                     <row>
                                    	<div class="col-md-12" style="text-align:left;">
                                        	<p>Being the simplest method, the Single Channel still has multiple point-to-point semantics. While the SMS and eMail channels can provide links to more information and interactions, the Voice channel can react to touch tones or even natural launguage speech recognition directly.</p>
                                    	</div>                                    
                                    </row>
                                     
                                   	<row>
                                                                        
                                        <div class="col-md-6">
                                                                              
                                            <p>Reasons to still use just the Single Channel.</p>	
                                            <ul style="margin: 8px 0 0 30px;">
                                                    
                                                <li>
                                                    Decreased Time &amp; Expense.
                                                </li>
                                                
                                                <li>
                                                    Your Business May Prefer a Specific Channel.  
                                                </li>
                                                                                                                                               
                                                <li>
                                                    Less moving parts.
                                                </li>
                                                                                                             
                                            </ul>   
                                                                                    
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div style="text-align:left;">    
                                                <img src="home7assets/images/m7/singlechannel.png"  width="480px" height="400px"style="border:none; display:inline; margin-top:10px;" />
                                            </div>
                                        </div>
                                        
                                                                                                                    
                                    </row>
                                    
                                    
                                </div>
                              
                            </div>
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                     <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>Multi-Channel</h2>
                         						<p>More than one way to communicate</p>
                                            </header>
                                        </div>
                                    </row>
                                    
                                     <row>
                                    	<div class="col-md-12" style="text-align:left;">
                                        	<p>Your customers could be anywhere, be where your customers are. With #BrandShort# Customer Preference Portal tools you can let the consumer specify which channel they prefer to recieve which messages on.</p>
                                    	</div>                                    
                                    </row>
                                     
                                   	<row>
                                                                        
                                        <div class="col-md-6">
                                                                              
                                            <p>Expand your consumers experience.</p>	
                                             <ul style="margin: 8px 0 0 30px;">
                                                <li>                                	
                                                    Tools to Capture and act on your customer's channel preference.   
                                                </li>
                                                
                                                <li>
                                                    Escalations - If you dont get a response on one channel, send it to another channel.
                                                </li>
                                                                                
                                                <li>
                                                    Create Multiple Touch Points
                                                </li>              
                                                
                                                <li>Escalations - If you dont get a response on one channel send it to another channel</li>             
                                                                                                          
                                            </ul>
                                                                                    
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div style="text-align:left;">    
                                                <img src="home7assets/images/m7/multichannel.png"  width="480px" height="400px"style="border:none; display:inline; margin-top:10px;" />
                                            </div>
                                        </div>
                                        
                                                                                                                    
                                    </row>
                                    
                                    
                                </div>
                              
                            </div>
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                     <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>Cross-Channel</h2>
                         						<p>Coordinating Between Channels</p>
                                            </header>
                                        </div>
                                    </row>
                                    
                                     <row>
                                    	<div class="col-md-12" style="text-align:left;">
                                        	<p>Rules Based Messaging and Reactive Event Condition Actions. Helps avoid the silo mentality. Your existing channel campaigns can now be integrated together. #BrandShort# can help you to engage the consumer in more meaningful ways.</p>
                                    	</div>                                    
                                    </row>
                                     
                                   	<row>
                                                                        
                                        <div class="col-md-6">
                                            	
                                            <ul style="margin: 8px 0 0 30px;">

                                                <li>Consumers can now respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.</li>
                                                <li>
                                                    Opt in to a program using a code from a bill stuffer, direct mail, email, or SMS   
                                                </li>
                                                
                                                <li>
                                                    Request more information to be sent on an alternate channel. Terms of use, brochures, policies and more.
                                                </li>
                                                                                
                                                <li>
                                                    Direct Mail, SMS, and eMail featuring URLs
                                                </li>
                                                
                                                <li>Block conflicting messages. Don't send bill collection calls at the same time as an emergency storm warning.</li>
                                                
                                                <li>Seamless approach to messaging across various channels guides potential customers towards engagement.</li>
                                                                                 
                                            </ul>
                                                                                    
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div style="text-align:left;">    
                                                <img src="home7assets/images/m7/crosschannel.png"  width="480px" height="400px"style="border:none; display:inline; margin-top:10px;" />
                                            </div>
                                        </div>
                                        
                                                                                                                    
                                    </row>
                                    
                                    
                                </div>
                              
                            </div>
                    
                      </div>
                    
                    
                    	<div class="carousel-caption">
                            <div class="process-step-box-caption Step3">Fulfillment</div>
                           <!--- <p>CYCLE 1</p>--->
                      	</div>
                          
                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="##FulfillmentCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="##FulfillmentCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    
                </div>



</cfoutput>













