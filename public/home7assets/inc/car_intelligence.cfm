<cfoutput>

<!---

Start with EMS sample

Then show EAI purpose built

Batch Id

API Security mechnaisms - pub/priv, username password, VPN options 

--->

<div id="TheIntelligence" class="EBMPresentationContainer EBMPresentationCarousel col-md-12">                                
                    
                    <div id="IntelligenceCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="##IntelligenceCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="##IntelligenceCarousel" data-slide-to="1"></li>
                        <li data-target="##IntelligenceCarousel" data-slide-to="2"></li>
                        <li data-target="##IntelligenceCarousel" data-slide-to="3"></li>
                      </ol>
                    
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                            <div class="item active">
                             
                                <div class="carousel-content">
                                
                                
                                    <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>homogeneous analysis of business structure and processes</h2>
                                                <p>Intelligent Rules Engine</p> 
                                            </header>
                                        </div>
                                    </row>
                                    
                                    <row>
                                     
                                        <div class="col-md-12" style="text-align:center;">
                                            <ul style="text-align:left; width:450px; display: inline-block; margin-bottom:3em;">
                                                <li>Complex Event Processing</li>
                                                <li>Rules Applied based on CDFs</li>
                                                <li>Rules Applied based on External Data</li>
                                                <li>Rules Applied based on Customer Preference</li>
                                                <li>Optimization</li>
                                                <li>Classification</li>
                                                <li>Pre-Processing</li>
                                                <li>Dynamically applied based on messaging interactions</li>
                                                <li>Post-Processing</li>
                                            </ul>                                        
                                        </div>
                                      
                                    </row>
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                   			<img src="home7assets/images/m7/rules-engine.jpg" width="450px" height="133px" style="border:none; display:inline;" />
                                        </div>
                                    </row>
                                            
                                        
                                </div>
                                
                            </div>
                        
                            <div class="item">
                            
                                <div class="carousel-content">
                                
                                	<row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>#BrandShort# Hosts the Campaign Templates</h2>
                                                <p>Campaign Templates</p> 
                                            </header>
                                        </div>
                                    </row>
                                    
                                    
                                    <row>
                                    
                                        <p>Messaging templates are hosted in the #BrandShort# environment and are built around libraries of message components and business rules.</p>
                                        <h4>Templates are built Based</h4> 
                                        <ul>
                                            <li>EMS - Self Service</li>
                                            <li>EAI - </li>
                                        </ul>
                                        
                                    </row>    
                                </div>    
                             
                            </div>
                        
                        
                        	<div class="item">
                                
                               <div class="carousel-content">
                                    
                                     <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>Express Messaging System Templates</h2>
                                                <p>#BrandShort# EMS Messaging templates</p> 
                                            </header>
                                        </div>
                                    </row>
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>EMS Templates via online CMS tools online </p>
                                    	</div>
                                    
                                    </row>
                                    
                                </div>
                              
                            </div>
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                                       
                                    <row>
                                        <div class="section-title-content clearfix col-md-12">
                                            <header class="section-title-inner">                                               
                                                <h2>Enterprise Application Integration Templates</h2>
                                                <p>#BrandShort# EAI Messaging templates</p> 
                                            </header>
                                        </div>
                                    </row>
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>EAI Templates are built using scripting that is compiled into java byte code for performance.</p>
                                    	</div>
                                    
                                    </row>
                                        
                                    <row>
                                        <div class="col-md-12">
                                            <div style="text-align:center;">    
                                                <img src="home7assets/images/m7/eaitemplatesample.png"  width="720px" height="375px"style="border:none; display:inline; margin-top:10px;" />
                                            </div>
                                        </div>                                                                            
                                    </row>                                    
                                    
                                </div>
                              
                            </div>
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>Templates can be lauched via Batch Id in Bulk or via API calls </p>
                                    	</div>
                                    
                                    </row>
                                    
                                </div>
                              
                            </div>                            
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>Templates areuniquely idendified by a Batch Id</p>
                                    	</div>
                                    
                                    </row>
                                    
                                </div>
                              
                            </div>
                            
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>CDF</p>
                                    	</div>
                                    
                                    </row>
                                    
                                </div>
                              
                            </div>
                            
                            <!--- CPP in a separate section ?? bottom of page? Link in other Caurosel --->
                            <!--- Api in a separate section ?? bottom of page? Link in other Caurosel --->
                            
                            <div class="item">
                                
                               <div class="carousel-content">
                                    
                                    <row>
                                    	<div class="col-md-12" style="text-align:center;">
                                        	<p>PDC API overview </p>
                                            
                                            <ul>
                                            	<li>3 Levels of security - Pub/Private key, user name and passowerd or VPN</li>
                                                <li>Standard Parameters</li>
                                                <li>CDF Parameters</li>
                                                <li>CURL Sample</li>
                                                <li>PDC - Add To Queue vs Realtime </li> 
                                            </ul>
                                    	</div>                                    
                                    </row>
                                    
                                </div>
                              
                            </div>
                            
                    
                      	</div>
                    
                    
                    	<div class="carousel-caption">
                            <div class="process-step-box-caption Step2">Intelligence</div>
                           <!--- <p>CYCLE 1</p>--->
                      	</div>
                          
                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="##IntelligenceCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="##IntelligenceCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    
                </div>



</cfoutput>













