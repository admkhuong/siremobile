<cfoutput>

<!---


	Use agent forms to trigger messaging based on agent actions - opt down now just out option  

--->

<div id="TheResults" class="EBMPresentationContainer EBMPresentationCarousel col-md-12">                                
                    
                    <div id="ResultsCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                      <ol class="carousel-indicators">
                        <li data-target="##ResultsCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="##ResultsCarousel" data-slide-to="1"></li>
                        <li data-target="##ResultsCarousel" data-slide-to="2"></li>
                        <li data-target="##ResultsCarousel" data-slide-to="3"></li>
                      </ol>
                    
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                         
                         	<div class="carousel-content">
                         		<row>
                                    <div class="col-md-12">
                                        <div style="text-align:center;">    
                                            <img src="home7assets/images/m7/dashboard-half-email.png"  width="799px" height="397px"style="border:none; display:inline; margin-top:10px;" />
                                        </div>
                                    </div>                                                                            
                                </row>   
                            </div>
                            
                        </div>
                    
                        <div class="item">
                        
                        	<div class="carousel-content">
                                <h4>The Results can be triggered:</h4> 
                                <h4>Realtime</h4>
                            </div>    
                         
                        </div>
                    
                        <div class="item">
                        	
                            <div class="carousel-content">
                                <h4>The Results can be triggered:</h4> 
                                <h4>Realtime</h4>
                                <h4>Batch</h4>
	                        </div>
                          
                        </div>
                    
                        <div class="item">
                        	 <div class="carousel-content">
                               
                               	<h4>The Results can be triggered:</h4> 
                                                       		
                                <ul style="text-align:left; list-style-type: none; width:450px; display: inline-block;">                            
                                    <li>SFTP</li>                            
                                    <li>HTTP/HTTPS - via REST API's</li>
                                    <li>HTTP/HTTPS - via form or Results variables</li>
                                    <li>HTTP/HTTPS - via direct XML</li>
                                    <li>HTTP/HTTPS - via SOAP</li>
                                    <li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
                                    <li>email</li>
                                    <li>email with password protected zip files</li>
                                    <li>email with PGP encrypted file</li>
                                    <li>Direct VPN Tunnel</li>
                                    <li>Dial up to secure server</li>
                                    <li>DAT/CD/media via courier</li>
                                    <li>FAX</li>
                                </ul>
                        
                        
                          		<h4>We can re-use and repurpose existing assets, or start new data transfer mechanisms to suit your needs.</h4>
                          
                            </div>
                        
                        </div>
                      </div>
                    
                    
                    	<div class="carousel-caption">
                            <div class="process-step-box-caption Step4">Results</div>
                           <!--- <p>CYCLE 1</p>--->
                      	</div>
                          
                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="##ResultsCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="##ResultsCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                    
                </div>



</cfoutput>













