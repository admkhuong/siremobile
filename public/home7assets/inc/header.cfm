
<!--- Stuff to include in the head section of any public site document --->
<cfinclude template="../../paths.cfm" >

<cfoutput>
    <link rel="shortcut" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />
    <link rel="icon" href="#rootUrl#/#PublicPath#/images/ebm_i_main/fav.png" />
</cfoutput>
	

<style>

	#status 
	{	
		background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home7assets/images/m7/infinite-gif-preloader-grey.gif");
		background-position: center;
		background-repeat: no-repeat;
		height: 200px;
		left: 50%;
		margin: -100px 0 0 -100px;
		position: absolute;
		top: 50%;
		width: 200px;
	}
	
	
	#home
	{
		
		background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/home7assets/images/m7/dreamstime_xl_24928711.jpg");
		<!---background: url(../images/cover.jpg);--->
		background-color: #222;
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-position: 50% 50%;
		-webkit-background-size: cover;
		   -moz-background-size: cover;
			 -o-background-size: cover;
				background-size: cover;
		padding: 0;
	}


	#SignInContainer
	{
		<!---position:relative;
		top:5px;
		right:390px;--->
		background-color: #FAFAFA;
		<!---border: 1px solid rgba(0, 0, 0, 0.3);
		border-radius: 5px 5px 5px 5px;
		box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2), 0 1px 0 0 rgba(250, 250, 250, 0.5) inset;--->

		margin: 0;
		padding: 10px 10px 10px 10px;

		width:450px;
		min-width:450px;
		max-width:450px;
		height:270px;
		min-height:270px;
		max-height:270px;

		display:none;
		text-align:left;
		z-index:10100;
	}
	
	
	#overlay 
	{
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 2000px;
		background-color: #000;
		filter:alpha(opacity=75);
		-moz-opacity:0.75;
		-khtml-opacity: 0.75;
		opacity: 0.75;
		z-index: 1000;
		display:none;
	}
	
	.ui-dialog .ui-dialog-titlebar 
	{
		background-image: url("../../../public/css/images/header.jpg");
		background-repeat: repeat-x;
		border-radius: 5px 5px 0 0;
		cursor: move;
		height: 100px;
	}
	
	body .ui-dialog .ui-dialog-titlebar 
	{
		color: #ffffff;
		font-family:Helvetica, Arial, sans-serif;
		font-size: 15px;
	}
		
	.ui-icon-closethick
	{
		display:none !important;	
		
	}
		
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		background: none;
		border: none;   
	}
	
	.ui-dialog .ui-dialog-titlebar-close 
	{
		height: 26px;
		margin: 0;
		padding: 0;
		position: absolute;
		right: 5px;	
		top: 0;
		width: 27px;
	}
	
	.ui-dialog .ui-dialog-title 
	{
		display: inline-block;
		float: left;
		font-size: 14px;
		margin: 70px 16px 0.2em 25px;
		text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
		text-transform: uppercase;
	}

	.noTitleStuff .ui-dialog-titlebar {display:none}
	.noTitleStuff .ui-dialog {border:none !important;}
	
	.InlineLink, .InlineLink:focus
	{
		display:inline;
		margin-left: 2em;	
		outline: 0  !important;				
	}
		
	a:focus
	{
		color: #e7746f !important;		
	}
	
</style>    



<cfif IsStruct(Session)>
	<cfif !StructKeyExists(Session, "USERID") >
		<cfset Session.USERID = 0 />
	</cfif>
</cfif>


<!--- Validate cookie and remember me option here--->

<cftry>

 	<cfparam name="Session.MFAISON" default="0">
    <cfparam name="Session.MFALENGTH" default="0"> 
    
    <!--- Decrypt out remember me cookie. --->
    <cfset LOCALOUTPUT.RememberMe = Decrypt(
		COOKIE.RememberMe,
		APPLICATION.EncryptionKey_Cookies,
		"cfmx_compat",
		"hex"
		) />
    <!---
	For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
	of list. Extract it from the list.
	--->
    <cfset CurrRememberMe = ListGetAt(
		LOCALOUTPUT.RememberMe,
		3,
		":"
		) />

    <!---
	Check to make sure this value is numeric, otherwise, it was not a valid value.
	--->
    <cfif IsNumeric( CurrRememberMe )>

        <!---
		We have successfully retreived the "remember me" ID from the user's cookie. Now, store
		that ID into the session as that is how we are tracking the logged-in status.
		--->
        <cfset Session.USERID = CurrRememberMe />
        <cfset SESSION.isAdmin = true>
        <cfset SESSION.loggedIn = "1" />
        
        <!--- *** What is this for ?!? --->
        <cfif StructKeyExists(cookie, "VOICEANSWERCOOKIE")>
            <cfset cookie.VOICEANSWERCOOKIE = null>
        </cfif>
    </cfif>

	<cfset Session.PrimaryPhone = ListGetAt(LOCALOUTPUT.RememberMe, 4, ":") />
    <cfset Session.at = ListGetAt(LOCALOUTPUT.RememberMe, 5, ":") />
    <cfset Session.facebook = ListGetAt(LOCALOUTPUT.RememberMe, 6, ":") />
    <cfset Session.fbuserid = ListGetAt(LOCALOUTPUT.RememberMe, 7, ":") />
    <cfset Session.EmailAddress = ListGetAt(LOCALOUTPUT.RememberMe, 8, ":") />
    <cfset Session.UserNAme = ListGetAt(LOCALOUTPUT.RememberMe, 10, ":") />
    <cfset SESSION.CompanyUserId = ListGetAt(LOCALOUTPUT.RememberMe, 11, ":") />
    <cfset Session.permissionManaged = ListGetAt(LOCALOUTPUT.RememberMe, 12, ":") />
    <cfset Session.companyId = ListGetAt(LOCALOUTPUT.RememberMe, 14, ":") />
    <cfset Session.MFAOK = ListGetAt(LOCALOUTPUT.RememberMe, 15, ":") /> 
    <cfset Session.MFAISON = ListGetAt(LOCALOUTPUT.RememberMe, 16, ":") /> 
    <cfset Session.MFALENGTH = ListGetAt(LOCALOUTPUT.RememberMe, 17, ":") /> 
            
    <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
        UPDATE
        	simpleobjects.useraccount
        SET
        	LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'
        WHERE
        	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
        AND
        	(CBPID_int = 1 OR CBPID_int IS NULL)
    </cfquery>


	<!--- Insetad of relocating back to session just let user know they are logged in --->
    <!---<cflocation addtoken="false" url="#rootUrl#/#SessionPath#/account/home">--->
    
    
    
    <!--- Catch any errors. --->
    <cfcatch TYPE="any">
        <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
    </cfcatch>
</cftry>


