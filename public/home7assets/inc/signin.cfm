
<!--- Home 7 Log In --->

    <cfparam name="SIID" default="0"/>
    
    <cfoutput>    
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
        
        <style TYPE="text/css">
            @import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css') all;
        </style>
	</cfoutput>

    
    <style>
	
			
		.signnew{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;
			width: 99%;
			color:#161616;
			margin-left:5px;
			margin-right:5px;
			margin-top:25px;
			clear:both;
		}
		.signnew a{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;   
			width: 100%;
			text-decoration:none;
			color:#161616;
		}
		.signnew a:hover{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;   
			width: 100%;
			color:#27b1e7 !important;
		}

			
		.loginform {	
			font-family: Open Sans,"Helvetica Neue",Helvetica,Arial,sans-serif;
			background: #fafafa none repeat scroll 0 0;
			overflow-x: hidden;
			overflow-y: hidden;
			margin: 40px auto;
			max-width: 450px;
			padding: 20px 30px;
			position: relative;
			text-align: left;	
			border: 2px solid #cecece;
		    border-radius: 5px;
		}
		
		.loginform ul {
			padding: 0;
			margin: 0;
		}
		.loginform li {
			display: block;
			margin-left:10%;
		}
		.loginform input:not([type=submit]) {
			width: 90%;
			font-size:12px !important;
			padding: 5px;
			margin-right: 10px;
			border: 1px solid rgba(0, 0, 0, 0.3);
			border-radius: 3px;
			box-shadow: inset 0px 1px 3px 0px rgba(0, 0, 0, 0.1), 
						0px 1px 0px 0px rgba(250, 250, 250, 0.5) ; 
		}
		
		.loginform input[type=submit] {
			border: 1px solid rgba(0, 0, 0, 0.3);
			background: #64c8ef; /* Old browsers */
			background: -moz-linear-gradient(top,  #64c8ef 0%, #00a2e2 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#64c8ef), color-stop(100%,#00a2e2)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #64c8ef 0%,#00a2e2 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#64c8ef', endColorstr='#00a2e2',GradientType=0 ); /* IE6-9 */
			color: #fff;
			padding: 5px 15px;
			float: right;
			margin-right: 12px;
			margin-top: 15px;
			border-radius: 3px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.3);
		}
		

	
		.loginform label {
			display: block;
			color: #161616;
			margin: 5px 0 0 0;
		}

			
		.LoginHeader
		{
			color: #F8853B;
			font-size: 30px;
			height: 50px;
			margin-bottom: 10px;
			margin-top: 10px;
			text-align: Left;
			width: 100%;		
		}
		
		.LoginHeader span
		{
			color: #007ead;	
		}

			
		.loginbutton {
			display: inline-block;
			zoom: 1; /* zoom and *display = ie7 hack for display:inline-block */
			*display: inline;
			vertical-align: baseline;
			margin: 0 2px;
			outline: none;
			cursor: pointer;
			text-align: center;
			font-weight:bold;
			text-decoration: none;
			font: 14px/100%;
			padding: .5em 2em .55em;
			text-transform:uppercase;
			-webkit-border-radius: 1px; 
			-moz-border-radius: 1px;
			border-radius: 0.5em 0.5em 0.5em 0.5em;
			-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			box-shadow: 0 1px 2px rgba(0,0,0,.2);
		}
		
		
		
		.loginbutton:hover {
			text-decoration: none;
		}
		.loginbutton:active {
			position: relative;
			top: 9px;
		}
		.medium {
			font-size: 12px;
			padding: .4em 1.5em .42em;
		}
		.small {
			font-size: 12px;
			padding: 4px 4px 4px 4px;
		}
		
		.RememberMe
		{
			display:block;
			color: #161616;   
			font-size: 12px;
			margin-left: 10%;
			margin-right: 5px;
			margin-top: 0px;
			text-align: left;
			vertical-align:top;
			height: 40px;
		}
		
		.RememberMe input
		{
			width: 25px !important;
			box-shadow: none !important;
			
		}

		.bluelogin {
			color: #FFFFFF !important;
			border: solid 1px #0076a3;
			background: #0095cd;
			margin-right:10%;
			padding-top: 8px;
		
			background: -webkit-gradient(linear, left top, left bottom, from(#2484c6), to(#2484c6));
			background: -moz-linear-gradient(top,  #2484c6,  #2484c6);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2484c6', endColorstr='#2484c6',GradientType=0 ); /* IE6-9 */
			
			display:inline;
			position:relative;
			top:10px;
			text-shadow: 0 1px 1px rgba(0,0,0,.3);
			-webkit-border-radius: .5em;
			-moz-border-radius: .5em;
			border-radius: .5em;
			-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			box-shadow: 0 1px 2px rgba(0,0,0,.2);
			
			text-transform: uppercase;
			
		}
		.bluelogin:hover {
			background: #007ead;
			background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
			background: -moz-linear-gradient(top,  #0095cc,  #00678e);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0095cc', endColorstr='#00678e',GradientType=0 ); /* IE6-9 */
		}
		.bluelogin:active {
			color: #fff;
			background: -webkit-gradient(linear, left top, left bottom, from(#0078a5), to(#00adee));
			background: -moz-linear-gradient(top,  #0078a5,  #00adee);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0078a5', endColorstr='#00adee',GradientType=0 ); /* IE6-9 */
		
		}
	
	
	</style>		
    
		
	<script type="text/javascript" language="javascript">
			
			

				/* Copyright (C) 2001,2014 Jeffery Lee Peterson - All Rights Reserved
				 * You may NOT in any part use, distribute or modify this code under the
				 * terms of the EBM license
				 *
				 * You should have received a copy of the EBM license with
				 * this file
				  */
		   
				<!--- Start page loaded start scripts --->
				$(document).ready(function() {
					
					$('#signin-popup-with-form').magnificPopup({
						  type: 'inline',
						  preloader: false,
						  focus: '#inpUserID_0',
				
						<!---  // When elemened is focused, some mobile browsers in some cases zoom in
						  // It looks not nice, so we disable it:--->
						  callbacks: {
							beforeOpen: function() {
							  if($(window).width() < 700) {
								this.st.focus = false;
							  } else {
								this.st.focus = '#inpUserID_0';
							  }
							}
						  }
					});
								   
				   	$("#<cfoutput>LoginLink_#SIID#</cfoutput>").click(function(){
  						  <cfoutput>doLogin_#SIID#</cfoutput>();
  					});
					
					$("#<cfoutput>LoginLinkMFA_#SIID#</cfoutput>").click(function(){
  						  <cfoutput>doLoginMFA_#SIID#</cfoutput>();
  					});
  							
				}); <!--- End page loaded start scripts --->
						
				function isValidRequiredFieldSignIn(fieldId){
					if($("#" + fieldId).val() == ''){
						$("#err_" + fieldId).show();
						return false;
					}else{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				
			
				function <cfoutput>doLogin_#SIID#</cfoutput>(){
					var isValid = true;
					if(!isValidRequiredFieldSignIn('<cfoutput>inpUserID_#SIID#</cfoutput>')){
						isValid = false;
					}
					if(!isValidRequiredFieldSignIn('<cfoutput>inpPassword_#SIID#</cfoutput>')){
						isValid = false;
					}
					if(!isValid){
						return false;
					}else{
						var userID = $("#<cfoutput>inpUserID_#SIID#</cfoutput>").val();
						var userPass = $("#<cfoutput>inpPassword_#SIID#</cfoutput>").val();
						// do login action
						try{
							<!---var RememberMeLocal = 0;
							if( $('#EBMSignInSection #inpRememberMe').is(':checked'))
								RememberMeLocal = 1;--->
								
								
							$.ajax({
							   type: "POST",
							   
							   <!--- HTTS for SSL --->
							   url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
							  							  
							   data:{
									inpUserID : userID, 
									inpPassword : encodeURI(userPass), 
									inpRememberMe : 1
							   },
							   dataType: "json", 
							   success: function(d) {
								  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.RXRESULTCODE) != "undefined")
									{					
										CurrRXResultCode = d.RXRESULTCODE;	
										
										<!---console.log(d);
																		
										alert('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home')--->
										
										if(CurrRXResultCode > 0)
										{
											<!---$.cookie("VOICEANSWERCOOKIE", null, { path : '/' })--->
											
											<!--- Check for MFA Requirement --->
											
											if(parseInt(d.MFAREQ) == 1)
											{
												if(d.LAST4.length == 4)
													$("#<cfoutput>LastFour_#SIID#</cfoutput>").html('Phone ******' + d.LAST4);
												
												<!--- Switch to second level of authentication --->	
												$(".LoginContainerMask").toggle();
												$(".MFAContainerMask").toggle();
												
												return false;												
											}
											
											<!--- Navigate tp sesion Home --->
											window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
										}
										else if(parseInt(CurrRXResultCode) == -2 )
										{
											jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );													
										}
										else
										{
											<!--- Unsuccessful Login --->
											<!--- Check if variable is part of JSON result string   d.CCDXMLString  --->								
											if(typeof(d.REASONMSG) != "undefined")
											{	 
												jAlert("Error while trying to login.\n", d.REASONMSG, function(result) { } );													
											}		
											else
											{
												jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );
											}
											
										}
									}
									else
									{<!--- Invalid structure returned --->														
										jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );
									}
									
							   }
						  });
						}
						catch(ex)
						{							
							jAlert("Error while trying to login.\n", "Invalid response from server.", function(result) { } );																		
						}
					}
				}
							
				
				<!--- Auto click Login button when press Enter key --->
				function <cfoutput>submitenter_#SIID#</cfoutput>(myfield,e) {
					var keycode;
					if (window.event) keycode = window.event.keyCode;
					else if (e) keycode = e.which;
					else return true;
									
					<!--- If the parent container is visible react to enter key press as login request  && $('#EBMSignInSection').parent().css("display") != "none"--->
					if (keycode == 13)
					{												
						<cfoutput>doLogin_#SIID#</cfoutput>();
						return false;
					}
					else
						return true;
				}
				
				<!--- Auto click Login button when press Enter key --->
				function <cfoutput>submitentermfa_#SIID#</cfoutput>(myfield,e) {
					var keycode;
					if (window.event) keycode = window.event.keyCode;
					else if (e) keycode = e.which;
					else return true;
									
					<!--- If the parent container is visible react to enter key press as login request  && $('#EBMSignInSection').parent().css("display") != "none"--->
					if (keycode == 13)
					{												
						<cfoutput>doLoginMFA_#SIID#</cfoutput>();
						return false;
					}
					else
						return true;
				}
				
	
				function <cfoutput>doLoginMFA_#SIID#</cfoutput>(){
					var isValid = true;
					if(!isValidRequiredFieldSignIn('<cfoutput>inpMFAID_#SIID#</cfoutput>')){
						isValid = false;
					}
					
					if(!isValid){
						return false;
					}else{
						var MFACode = $("#<cfoutput>inpMFAID_#SIID#</cfoutput>").val();
						
						try{
															
							$.ajax({
							   type: "POST",
							   <!--- HTTS for SSL --->
							   url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateMFA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
							   data:
							   {
									inpMFACode : $("#<cfoutput>inpMFAID_#SIID#</cfoutput>").val()
							   },
							   dataType: "json", 
							   success: function(d) {
								  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
									<!--- Check if variable is part of JSON result string --->								
									if(typeof(d.RXRESULTCODE) != "undefined")
									{					
										CurrRXResultCode = d.RXRESULTCODE;	
										
										<!---console.log(d);
																		
										alert('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home')--->
										
										if(CurrRXResultCode > 0)
										{
											<!---$.cookie("VOICEANSWERCOOKIE", null, { path : '/' })--->
											
											<!--- Check for MFA OK  --->
											
											// jAlert("testing", "MFA OK Result = " + d.MFAOK, function(result) { } );													
											
											if(parseInt(d.MFAOK) > 0 )
												<!--- Navigate tp sesion Home --->
												window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home";
											else
												jAlert("Error while trying to authenticate device.\n", d.REASONMSG, function(result) { } );	
										}
										else if(parseInt(CurrRXResultCode) == -2 )
										{
											jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );													
										}
										else
										{
											<!--- Unsuccessful Login --->
											<!--- Check if variable is part of JSON result string   d.CCDXMLString  --->								
											if(typeof(d.REASONMSG) != "undefined")
											{	 
												jAlert("Error while trying to authenticate device.\n", d.REASONMSG, function(result) { } );													
											}		
											else
											{
												jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );
											}
											
										}
									}
									else
									{<!--- Invalid structure returned --->														
										jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );
									}
									
							   }
						  });
						}
						catch(ex)
						{							
							jAlert("Error while trying to authenticate device.\n", "Invalid response from server.", function(result) { } );																		
						}
					}
				}
				
				
				function requestnewcode()
				{
					<!--- Switch to second level of authentication --->	
					$(".LoginContainerMask").toggle();
					$(".MFAContainerMask").toggle();					
				}
				
			</script>
	
     
     
             
               			
        <div id="EBMSignInSection<cfoutput>_#SIID#</cfoutput>" class="loginform mfp-hide"> 
        	
                        
            <div class="LoginHeader"><span>Log in to</span> <cfoutput>#BrandShort#</cfoutput> <span class="MFAContainerMask" style="display:none;">- Step 2</span></div>
             
            <form name="login" action="index_submit" autocomplete="on">
                <input id="inpSigninMethod" type="hidden" name="method" value="Signin" />
                                
                <div class="LoginContainerMask">
                    <ul>
                        <li>
                            <label for="inpSigninUserID">Email</label>
                            <input type="email" id="<cfoutput>inpUserID_#SIID#</cfoutput>" name="<cfoutput>inpUserID_#SIID#</cfoutput>" placeholder="yourname@email.com" required onKeyPress="return <cfoutput>submitenter_#SIID#</cfoutput>(this,event)" autocomplete="on">
                        </li>
                        
                        <li>
                            <label for="inpSigninPassword">Password</label>
                            <input type="password" placeholder="password" required id="<cfoutput>inpPassword_#SIID#</cfoutput>" name="<cfoutput>inpPassword_#SIID#</cfoutput>" onKeyPress="return <cfoutput>submitenter_#SIID#</cfoutput>(this,event)">
                        </li>
                        
                        <!---
                         <li>
                            <label for="inpSigninPassword">Remember Me</label>
                            <input type="checkbox" required id="inpRememberMe" name="inpRememberMe">
                        </li>
                        --->
                        
                                                                        
                    </ul>   
                    
                    <div><!---Remember Me<input type="checkbox" required id="inpRememberMe" name="inpRememberMe" style="background:transparent;border:0">---> 
                        <a style="float:left; margin-left:10%;" href="forgotpassword" class="RememberMe">Forgot password?</a>
                        <a style="float:right; margin-right:10%;" href="javascript:;" class="loginbutton bluelogin small" id="<cfoutput>LoginLink_#SIID#</cfoutput>">Login</a>                                      
                    </div>  
            
                    <div id="signnew" class="signnew" align="center"><a href="signup">Not a user yet? Create your account here.</a> </div>   
                
                </div>
                
                <div class="MFAContainerMask" style="display:none;">
                
                    <ul>
                        <li style="width:250px; padding-bottom:8px;">
                            <h4 style="font-size:12px;">This account requires a second code to authorize each device. This code was sent to the phone number specified in your user settings.&nbsp;<span id="<cfoutput>LastFour_#SIID#</cfoutput>"></span></h4>
                        </li>
                    
                        <li>
                            <label for="inpMFAID">Authentication Code for This Device</label>
                            <input type="text" id="<cfoutput>inpMFAID_#SIID#</cfoutput>" name="<cfoutput>inpMFAID_#SIID#</cfoutput>" placeholder="Enter the code sent to you here" required onKeyPress="return <cfoutput>submitentermfa_#SIID#</cfoutput>(this,event)" autocomplete="on" style="width:250px;">
                        </li>                            
                     </ul>                
                     
                     <div class="RememberMe" style="padding-top:10px;">
                    <!--- Keep centered if included in middle of a page somewhere - dont apply to home pages--->                        
                        
                        <a href="javascript:;" class="loginbutton bluelogin small" id="<cfoutput>LoginLinkMFA_#SIID#</cfoutput>">Authorize</a>
                        
    				</div>
                    
				    <div id="RequestNewMFACode" class="signnew" align="center"><a href="javascript:;requestnewcode()">Need a new code? Request a new code by logging in again here.</a> </div>                               
                
                </div>
                
                           
                           
                                 
            </form>
                      
           </div>
          
         