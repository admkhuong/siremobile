

<script type = "text/javascript" >

		/* Copyright (C) 2001,2014 Jeffery Lee Peterson - All Rights Reserved
	 * You may NOT in any part use, distribute or modify this code under the
	 * terms of the EBM license
	 *
	 * You should have received a copy of the EBM license with
	 * this file
	  */
	   
	<!--- Start page loaded start scripts --->
	$(document).ready(function() {
		
		 $('#signout-popup-with-form').magnificPopup({
			  type: 'inline',
			  preloader: false,
			  <!---focus: '#inpUserID_0',--->
	
			<!---  // When elemened is focused, some mobile browsers in some cases zoom in
			  // It looks not nice, so we disable it:--->
			  callbacks: {
				beforeOpen: function() {
				  if($(window).width() < 700) {
					this.st.focus = false;
				  } else {
					<!---this.st.focus = '#inpUserID_0';--->
				  }
				}
			  }
			});
	
							
				
		$('#ConfirmLogOffButton').click(function(){				
			
			if($("#DALD").is(':checked'))
				window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=1';
			else
				window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/act_logout?DALD=0';		
				
			$('#signout-popup-with-form').magnificPopup('close');				
			
		});
		
		$('#CancelLogOffButton').click(function(){
			
			$('#signout-popup-with-form').magnificPopup('close');	
			return false;
		});
				
	
	
	}) <!--- End page loaded start scripts --->
</script>
     
     
  <style>
	
			
		.signnew{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;
			width: 99%;
			color:#161616;
			margin-left:5px;
			margin-right:5px;
			margin-top:25px;
			clear:both;
		}
		.signnew a{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;   
			width: 100%;
			text-decoration:none;
			color:#161616;
		}
		.signnew a:hover{
			font-size:12px;
			float:left;
			text-align: left;
			height: 25px;   
			width: 100%;
			color:#27b1e7 !important;
		}

			
		.loginform {	
			font-family: Open Sans,"Helvetica Neue",Helvetica,Arial,sans-serif;
			background: #fafafa none repeat scroll 0 0;
			overflow-x: hidden;
			overflow-y: hidden;
			margin: 40px auto;
			max-width: 450px;
			padding: 20px 30px;
			position: relative;
			text-align: left;	
			border: 2px solid #cecece;
		    border-radius: 5px;
		}
		
		.loginform ul {
			padding: 0;
			margin: 0;
		}
		.loginform li {
			display: block;
			margin-left:10%;
		}
		.loginform input:not([type=submit]) {
			width: 90%;
			font-size:12px !important;
			padding: 5px;
			margin-right: 10px;
			border: 1px solid rgba(0, 0, 0, 0.3);
			border-radius: 3px;
			box-shadow: inset 0px 1px 3px 0px rgba(0, 0, 0, 0.1), 
						0px 1px 0px 0px rgba(250, 250, 250, 0.5) ; 
		}
		
		.loginform input[type=submit] {
			border: 1px solid rgba(0, 0, 0, 0.3);
			background: #64c8ef; /* Old browsers */
			background: -moz-linear-gradient(top,  #64c8ef 0%, #00a2e2 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#64c8ef), color-stop(100%,#00a2e2)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #64c8ef 0%,#00a2e2 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #64c8ef 0%,#00a2e2 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#64c8ef', endColorstr='#00a2e2',GradientType=0 ); /* IE6-9 */
			color: #fff;
			padding: 5px 15px;
			float: right;
			margin-right: 12px;
			margin-top: 15px;
			border-radius: 3px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.3);
		}
		

	
		.loginform label {
			display: block;
			color: #161616;
			margin: 5px 0 0 0;
		}

			
		.LoginHeader
		{
			color: #F8853B;
			font-size: 30px;
			height: 50px;
			margin-bottom: 10px;
			margin-top: 10px;
			text-align: Left;
			width: 100%;		
		}
		
		.LoginHeader span
		{
			color: #007ead;	
		}

			
		.loginbutton {
			display: inline-block;
			zoom: 1; /* zoom and *display = ie7 hack for display:inline-block */
			*display: inline;
			vertical-align: baseline;
			margin: 0 2px;
			outline: none;
			cursor: pointer;
			text-align: center;
			font-weight:bold;
			text-decoration: none;
			font: 14px/100%;
			padding: .5em 2em .55em;
			text-transform:uppercase;
			-webkit-border-radius: 1px; 
			-moz-border-radius: 1px;
			border-radius: 0.5em 0.5em 0.5em 0.5em;
			-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			box-shadow: 0 1px 2px rgba(0,0,0,.2);
		}
		
		
		
		.loginbutton:hover {
			text-decoration: none;
		}
		.loginbutton:active {
			position: relative;
			top: 9px;
		}
		.medium {
			font-size: 12px;
			padding: .4em 1.5em .42em;
		}
		.small {
			font-size: 12px;
			padding: 4px 4px 4px 4px;
		}
		
		.RememberMe
		{
			display:block;
			color: #161616;   
			font-size: 12px;
			margin-left: 10%;
			margin-right: 5px;
			margin-top: 0px;
			text-align: left;
			vertical-align:top;
			height: 40px;
		}
		
		.RememberMe input
		{
			width: 25px !important;
			box-shadow: none !important;
			
		}

		.bluelogin {
			color: #FFFFFF !important;
			border: solid 1px #0076a3;
			background: #0095cd;
			margin-right:10%;
			padding-top: 8px;
		
			background: -webkit-gradient(linear, left top, left bottom, from(#2484c6), to(#2484c6));
			background: -moz-linear-gradient(top,  #2484c6,  #2484c6);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2484c6', endColorstr='#2484c6',GradientType=0 ); /* IE6-9 */
			
			display:inline;
			position:relative;
			top:10px;
			text-shadow: 0 1px 1px rgba(0,0,0,.3);
			-webkit-border-radius: .5em;
			-moz-border-radius: .5em;
			border-radius: .5em;
			-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
			box-shadow: 0 1px 2px rgba(0,0,0,.2);
			
			text-transform: uppercase;
			
		}
		.bluelogin:hover {
			background: #007ead;
			background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
			background: -moz-linear-gradient(top,  #0095cc,  #00678e);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0095cc', endColorstr='#00678e',GradientType=0 ); /* IE6-9 */
		}
		.bluelogin:active {
			color: #fff;
			background: -webkit-gradient(linear, left top, left bottom, from(#0078a5), to(#00adee));
			background: -moz-linear-gradient(top,  #0078a5,  #00adee);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0078a5', endColorstr='#00adee',GradientType=0 ); /* IE6-9 */
		
		}
	
	
	</style>	



<cfoutput>

	<div id="confirm-logout" title="Are you sure you want to log off?" class="loginform mfp-hide">
    
	    <div class="LoginHeader"><span>Log out of</span> <cfoutput>#BrandShort#</cfoutput></div>
      
    	<!--- Only offer if MFA is turned on AND phone has been specified --->
    	<cfif Session.MFAISON GT 0 AND Session.MFALENGTH GT 0>
      		<p style="padding:10px;"></BR> <input type='checkbox' id='DALD' name='DALD' style="width:20px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px;  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);  margin-bottom: 8px; padding: 3px 8px;"> Check the box to deauthorize this device.<BR>May require MFA code at next log in.</p>
      	</cfif>
        
        <div class="" style="text-align:center;">
            <span id="ConfirmLogOffButton" class="loginbutton bluelogin small" style="margin-bottom:15px; color:##fff; display:inline;">Yes, Log Off Now</span>
            <span id="CancelLogOffButton" class="loginbutton bluelogin small" style="margin-left:15px; color:##fff; display:inline;">Cancel</span>
        </div>
    
    </div>

</cfoutput>

