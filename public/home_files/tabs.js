jQuery.noConflict();
// wrap all your code in this	
	jQuery(document).ready(function(jQuery){
		// Main Navigation Mega Menu Functionality
		jQuery('#main ul li a').click(function(){ 
			jQuery('#main ul li').removeClass('active');
			jQuery(this).parent().addClass('active');
			jQuery('#navigation .close-wrap').show();
			var currentTab = jQuery(this).attr('rel');
			
			// if statement for class on li > a of main nav
			if(jQuery(this).hasClass('MegaMenuL')) {
					jQuery('#subnav-container').removeClass('short').addClass('tall').slideDown('fast').animate({'height':'130px',queue:false, duration: 'fast'});
				} 
				else {
					jQuery('#subnav-container').removeClass('tall').addClass('short').slideDown('fast').animate({'height':'130px', queue:false, duration: 'fast'});
				};
	
			// If Statement for opening/closing menus at tab
			if(jQuery(currentTab).hasClass('open')) {
				// Remove Active Class from Main Navigation Tab
				jQuery('#main ul li').removeClass('active');
				// Remove class from any Open Tab
				jQuery('#navigation .tabs').removeClass('open').fadeOut('fast').animate({queue:false, duration: 'fast'});
				// Close the Subnav Menu Container
				jQuery('#subnav-container').slideUp('fast').animate({height:'0px', queue:false, duration: 'fast'});
				// Hide the Close Button
				jQuery('#navigation .close-wrap').hide();
			} 
			else {
				jQuery('#navigation .tabs').removeClass('open').fadeOut('fast').animate({queue:false,duration: 'fast'});
				jQuery(currentTab).addClass('open').fadeToggle('fast');
				
			};
			return false;
		});
		// Close button
		jQuery('#navigation .close').click(function(){
			jQuery(this).parent().hide();
			jQuery('#main ul li').removeClass('active');
			jQuery('#subnav-container').removeClass().addClass('closed').slideUp('fast').animate({'height':'0px', queue:false,duration: 'fast'});
			jQuery('#navigation .tabs').removeClass('open').slideUp('fast').animate({queue:false, duration: 'fast'});
		});
});
