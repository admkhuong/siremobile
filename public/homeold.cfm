<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   
   <title>Home</title>
     
    <!--- IE 9 hates when header stuff is before doc type definition --->
    <cfinclude template="header.cfm">
    


<cfoutput>

	    
    
          
</cfoutput>

           
    <style>
   
				
	.coloredBlock 
	{
		padding: 12px;
		background: rgba(255,0,0,0.6);
		color: # FFF;
		width: 200px;
		left: 20% ;
		top: 5% ;
	}
	
	.infoBlock 
	{
		position: absolute;
		top: 30px;
		right: 30px;
		left: auto;
		max-width: 25%;
		padding-bottom: 0;
		background: #FFF;
		background: rgba(255, 255, 255, 0.85);
		overflow: hidden;
		padding: 20px;
		border-radius: 5px 5px 5px 5px;
	}
	
	.infoBlockII 
	{
		background-color: rgba(0, 0, 0, 0.4);
		border-radius: 6px 6px 6px 6px;
		box-shadow: 0 2px 3px 1px rgba(0, 0, 0, 0.15) inset;
		color: #FFFFFF;
		display: block;
		max-width: 25%;
		position: absolute;		
		padding:20px;
	}

	.infoLeft
	{
		left: 90px;		
	}
	
	.infoRight
	{		
		right: 90px;		
	}
	
	.infoTop
	{
		top: 90px;		
	}
	
	.infoBottom
	{
		bottom: 90px;		
	}
	
	.infoCenter
	{
		top: 150px;
	}
	
	
	
	.infoBlockFull 
	{
		position: absolute;
		top: 30px;
		right: 30px;
		left: auto;
		max-width: 90%;
		padding-bottom: 0;
		background: #FFF;
		background: rgba(255, 255, 255, 0.85);
		overflow: hidden;
		padding: 20px;
		border-radius: 5px 5px 5px 5px;
	}
	
	.infoBlockLeftBlack 
	{
		color: #FFF;
		background: #000;
		background: rgba(0,0,0,0.85);
		left: 30px;
		right: auto;
	}
	
	.infoBlockLeftWhite
	{
		color: #000;
		background: #FFF;
		background: rgba(255,255,255,1);
		left: 30px;
		right: auto;
	}
	
	.infoBlockRightWhite
	{
		color: #000;
		background: #FFF;
		background: rgba(255,255,255,0.85);
		right: 30px;
		left: auto;
	}
	
	#slidecontainermain .orangeFont
	{
		color: #F8853B;		
	}
	
	#slidecontainermain .darkBlueFont
	{
		color: #036;		
	}
	
	#slidecontainermain h1
	{
		font-size: 48px;		
		margin: 0;
		padding-bottom: 3px;	
		text-align:left;	
		text-transform:uppercase;
		text-shadow: 0px 1px #3f3f3f;
	}
	
	#slidecontainermain h2
	{
		font-size: 36px;		
		margin: 0;
		padding-bottom: 3px;	
		text-align:left;		
	}
	
	#slidecontainermain h3 
	{
		font-size: 28px;		
		margin: 0;
		padding-bottom: 3px;
		text-align:left;	
		font-weight:700;
		text-shadow: 0px 2px #3f3f3f;
	}
	
	#slidecontainermain h4 
	{
		font-size: 18px;		
		margin: 0;
		padding-bottom: 3px;
		text-align:left;	
		line-height: 18px;
	}
	
	#slidecontainermain p
	{
		font-size: 14px;
		margin: 4px 0 0;
		text-align:left;
		text-shadow: 1px 1px #036;	
	}
	
	.ebmorange
	{		
		color:#F8853B;	
	}
	
	.infoBlock p 
	{
		font-size: 14px;
		margin: 4px 0 0;
	}
	
	.infoBlock a
	{
		color: # FFF;
		text-decoration: underline;
	}
	
	<!---.photosBy
	{
		position: absolute;
		line-height: 24px;
		font-size: 12px;
		background: #FFF;
		color: #000;
		padding: 0px 10px;
		position: absolute;
		left: 12px;
		bottom: 12px;
		top: auto;
		border-radius: 2px;
		z-index: 25;
	}
	
	.photosBy a
	{
		color: # 000;
	}--->
	
	.fullWidth
	{
		max-width: 1400px;
		margin: 0 auto 24px;
	}
	
	@media screen and(min - width: 960px) and(min - height: 660px)
	{
		.heroSlider.rsOverflow,
		.royalSlider.heroSlider 
		{
			height: 520px!important;
		}
	}
	
	@media screen and(min - width: 960px) and(min - height: 1000px) 
	{
		.heroSlider.rsOverflow,
		.royalSlider.heroSlider
		{
			height: 660px!important;
		}
	}
	@media screen and(min - width: 0px) and(max - width: 800px) 
	{
		.royalSlider.heroSlider,
		.royalSlider.heroSlider.rsOverflow
		{
			height: 300px!important;
		}
		
		.infoBlock
		{
			padding: 10px;
			height: auto;
			max-height: 100% ;
			min-width: 40% ;
			left: 5px;
			top: 5px;
			right: auto;
			font-size: 12px;
		}
		
		.infoBlock h3
		{
			font-size: 14px;
			line-height: 17px;
		}
	}
    
	.tileicon
	{		
		background: rgb(35,138,203); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(35,138,203,1) 0%, rgba(30,87,153,1) 0%, rgba(32,124,202,1) 98%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(35,138,203,1)), color-stop(0%,rgba(30,87,153,1)), color-stop(98%,rgba(32,124,202,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#238acb', endColorstr='#207cca',GradientType=0 ); /* IE6-9 */
		
	   	padding-left: 15px;
    	padding-right: 0;
    	border-radius: 5px;
		width: 124px;
		height:124px;
		box-shadow: 0 5px 2px #d1d1d1;
		cursor:pointer;
	}
	
	.tileicon:hover
	{
	    background-color: #f8853b !important;
	}
	
	.tileiconBox 
	{		
		border-radius: 5px;
		display: inline-block;
		padding: 10px;	
		outline:none;
		border:none;
	}
		
	.shine {
				display: block;
				position: relative;
				background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 100%);
				background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(255,255,255,0)), color-stop(50%,rgba(255,255,255,1)), color-stop(100%,rgba(255,255,255,0)));
				background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				background: -o-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				background: -ms-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#00ffffff',GradientType=1 );
				background: linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				padding: 0px 12px;
				top: 0px;
				left: 0px;
				height: 6px;
				-webkit-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				 -khtml-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				   -moz-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
					 -o-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
						box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				-webkit-transition: all 0.3s ease-in-out;
				 -khtml-transition: all 0.3s ease-in-out;
				   -moz-transition: all 0.3s ease-in-out;
					 -o-transition: all 0.3s ease-in-out;
						transition: all 0.3s ease-in-out;
	}
	.tileicon:hover .shine {left: 24px;}
	.tileicon:active .shine {opacity: 0;}

	
	.HideOutline
	{
		outline:none;
		border:none;
	}
	
	grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid:after {
		clear: both;
	}
	.grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid {
		width: 100%;
	}
	
	.grid .col {
		float: left;
		font-size: 14px;
		margin-bottom: 15px;
		table-layout: auto;
		width: 50%;
	}
	.col {
		display: table;
		table-layout: fixed;
		width: 100%;
	}
	
	.HomeTilesInfo h2
	{
		font-size: 18px;
		margin-bottom:8px;
	}

	.HomeTilesInfo p
	{
		font-size: 14px;	
	}
	
	.TileCellHome
	{ 
		display: table-cell;
		padding-left:12px;
		vertical-align: top;
		position:relative;
		height:114px;
		
	}
	
	.HomeTilesInfo a
	{
		color: #07b4ee;
	    text-decoration: underline;	
	}
	
	.HomeTilesInfo a:hover
	{
		color: #f8853b;	    
	}
	
	
		.clear
		{
			clear:both;	
		}
			
		*[class^="prlx"] {
		  position: absolute;
		  width: 100%;
		  height: 1200%;
		  top:0;
		  left:0;
		  z-index: -1;
		}
		
		<!---.prlx-2{ background: url('<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png') repeat; }--->
		
		.prlx-1, .prlx-2, .prlx-3
		{ 
			background-image: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png");
			background-repeat: repeat-x, repeat;
		}
					
		.section-one h2, .section-two h2, .section-three h2{ 
					
			border-top: 1px solid;
			color: #f9f9f9;
			font-size: 35px;
			margin: auto;
			max-width: 90%;
			padding-top: 20px;
			text-transform: uppercase;
			
	

		}
		
		.section-one, .section-two, .section-three
		{
			width: 100%; 
			min-width:1200px;	
			min-height: 300px;
			position: relative;
			overflow: hidden;
		}
	
		.SectionHeadline 
		{
			border-top: 1px solid;
			color: #ffffff;
			font-size: 25px;
			margin: auto;
			max-width: 90%;
			padding-top: 20px;
			text-transform: uppercase;
		}
	
	
	
	body
	{
		padding:0;
		border:none;
		height:100%;
		width:100%;
		min-width:1200px;	
		background: rgb(51,182,234); 
		background-size: 100% 100%;
		background-attachment: fixed; 	
		background-repeat:no-repeat;
	}
	
	#FixedHeaderSpacer, #FixedHeaderSpacerOpen
	{
		background-color:#22b3ec;		
	}					
	    
		
	<!--- Change any styles for IE 8 crap or other older browsers --->
	<cfif UseLegacyStrucutres > 	
        
		
		#homeimage
		{
			
			background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_25375759.jpg");
			background-repeat: no-repeat;
			background-size: 100% 100%;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_25375759.jpg', sizingMethod='scale');
			-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader( src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_25375759.jpg', sizingMethod='scale')";
		}
				

	</cfif>	
</style>


    </style>
  
   <script type="text/javascript" language="javascript">
   
	   $(function() {	
	   			
			<!--- Cant load HTTPS from HTTP ?!? --->
			<!--- $('#SignInContainer').load(
				<cfoutput>'http://#CGI.SERVER_NAME#/#publicPath#/signin.cfm',</cfoutput>
				function (response, status, xhr){
				;
			});--->
			
			<!--- Top Banner with paralax effect --->
			var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_25375759.jpg" + '">' );
			$img.bind( 'load', function(){
				$( '#homeimage' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_25375759.jpg" + ')' );
			} );
			if( $img[0].width ){ $img.trigger( 'load' ); }
			
			<!--- Body for paralax effect --->
			var $img2 = $( '<img src="' + "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png" + '">' );		
			$img2.bind( 'load', function()
			{
				$( 'body' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png" + ')' );			
				<!---$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/zenbgdark.png" + ')' );	--->		
			});			
			if( $img2[0].width ){ $img2.trigger( 'load' ); }
								
						 
			<!--- Preload images --->
			$.preloadImages("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
										 
					
			<!--- Presentation section - Power Point for the web but works --->
			$(".royalSlider").royalSlider({
				<!---// options go here
				// as an example, enable keyboard arrows nav--->
				keyboardNavEnabled: true,
				navigateByClick: false,
				usePreloader: true,
				controlNavigation: 'none',
				autoPlay:
				{
					<!---// autoplay options go gere--->
					enabled: true,
					pauseOnHover: true
				},
				deeplinking: 
				{
					<!---// deep linking options go here--->
					enabled: true,
					prefix: 'slide-'
				},
				loop: true
			});  
			
			<!--- Get rid of F.O.U.C. Flash Of Unstyled Content--->
			// $(".royalSlider").show();
		
			$("#PreLoadIcon").fadeOut(); 
        	$("#PreLoadMask").delay(350).fadeOut("slow");
		
		 		  	
			
		});
	 
		<!--- Image preloader --->
		$.preloadImages = function() 
		{
			for (var i = 0; i < arguments.length; i++) 
			{
				$("<img />").attr("src", arguments[i]);				
			}
		}



  
   </script>
   </head>
   
   <cfoutput>
    <body>
    
       	<div id="PreLoadMask">
        	<div id="PreLoadIcon">
            	<p>LOADING ...</p>
            </div>
        </div>
                             
       <div id="main" align="center">
              
	    <!--- EBM site footer included here --->
   		<cfinclude template="act_ebmsiteheader.cfm">
              
              
            <div id="homeimage">
               
               <div id="slidecontainermain">
                    <!---<div id="SignInContainer" class="awesome"><cfinclude template="signin.cfm"></div>--->
                    
                    <div id="slideimage" class="royalSlider rsDefault" style="">
                        
                        <!--- Stage --->
                        <div class="" data-rsDelay="8000">
                        
                           <!--- Left List of Channels--->
                           	<div class="rsABlock LegacyHide" data-move-effect="top" data-delay="1200" style="position:absolute; top:15px; left:10px; height:325px; width:800px; padding:30px 20px 10px 20px; color:##FFF; background: ##003; background: rgba(248, 248, 248, 0.45); overflow: hidden; text-align:left;border-radius: 6px 6px 6px 6px; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		
                            </div>  
                        
                        	<!--- Left List of Channels--->
                           	<div class="rsABlock" data-move-effect="left" data-delay="1600" style="position:absolute; top:10px; left:40px; height:350px; width:600px; padding:0px 20px 10px 20px; color:##FFF; background:none; overflow: hidden; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                                                        
                                <div class="inner-main-hd" style="width:400px; margin-top:30px;">The #BrandShort# Platform</div>
                
                                <div style="clear:both;"></div>
                                
                                <div class="header-content">
                                    <!---#BrandShort# systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. --->
                                    <h4>SMS</h4>
                                    <h4>Voice / IVR</h4>
                                    <h4>eMail</h4>
                                    <h4>Preference Tools</h4>
                                    <h4>EAI Services</h4>
                                    <h4>Data Services</h4>
                                    <h4>Social Media</h4>
                                    <h4>Print</h4> 
                                    <h4>Radio</h4> 
                                    <h4>Online</h4> 
                                    <h4>Marketing Tools and more...</h4>                                    
                                </div>
                 
                            </div>      
                                                                                   
                            <!--- Target Engage Deliver --->                                                        
                            <div class="rsABlock" data-move-effect="right" data-delay="600" style="position:absolute; top:40px; left:500px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Target</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1200" style="position:absolute; top:115px; left:500px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Engage</h1>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1600" style="position:absolute; top:190px; left:500px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<h1>Deliver</h1>                                
                            </div>
                                                     
        				</div>
                                        	                       
                	</div>        
                	
				</div>
                
            </div>       
            
            <!---<div id="servicemainline"></div>--->
            
            <div id="inner-bg-min" style="background-color:##f9f9f9; margin-top:0; height:500px;">
            
                <div id="content1">
                    
                    <div id="servicemain">
                                              
                                              
                            <div class="section-title-content clearfix">
                        	    <header class="section-title-inner">
	                                <h2>SMARTER TECHNOLOGY</h2>
    	                            <p>See What Makes Our Solutions Best in Class</p>
                                </header>
                            </div>
     
                           
                           <div class="HomeTilesInfo" style="float:left; width:435px; text-align:left;">
                           
                                
                                <div style="position:relative; float:left;">
                                    <a href="hiw_smc" class="tileicon t1" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="42px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/voice_web2.png" style="margin: 30px 29px 20px 29px">
                                        </span>
                                        
                                        <span style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>	
                                    </a>
                               	</div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_smc" style="color: ##015172; text-decoration:none;">Single-Channel, Multi-Channel, and Cross-Channel</a>
                                    </h2>
                                    <p>
                                        Designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. 
                                        <br>
                                        <a class="more" href="hiw_smc" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                <div style="position:relative; float:left;">
                                    <a href="hiw_nas" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="30px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/sms_web2.png" style="margin: 30px 36px 30px 34px;">
                                        </span>
                                        
                                        <span style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>
                                    </a>
                               	</div>
                               
                               	<div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_nas" style="color: ##015172; text-decoration:none;">Notifications, Alerts and Surveys</a>
                                    </h2>
                                    <p>
                                        One to one, one to many, message branching, business rules, or even interactive response,  
                                        <br>
                                        <a class="more" href="hiw_nas" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                <div style="position:relative; float:left;">
                                    <a href="hiw_lis" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="31px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/agentfemale.png" style="margin: 30px 37px 30px 32px;">
                                        </span>
                                        
                                        <span href="hiw_lis" style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>
                                    </a>
                               	</div>
                                
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_lis" style="color: ##015172; text-decoration:none;">Content Management and Business Rules</a>
                                    </h2>
                                    <p>
                                        Live and Virtual Interactive Sessions, Auto Respond, Keywords, Agent Assist, IVR, Surveys, Trickle Campaigns, Agent Response.                                       
                                    </p>
                                    <BR />
                                      	<a class="more" href="hiw_lis" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                           
                           
                           </div>
                           
                           
                            <div class="HomeTilesInfo" style="float:right; width:435px; text-align:left;">
                           
                           		<div style="position:relative; float:left;">
                                    <a href="hiw_cpp" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="30px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/cppicon.png" style="margin: 30px 34px 30px 32px;">
                                        </span>
                                        
                                        <span style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>
                                    </a>
                                </div>    
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_cpp" style="color: ##015172; text-decoration:none;">Customer Preference Portal Tools</a>
                                    </h2>
                                    <p>
                                        Quickly create and manage, account preferences, and privacy settings, or even start new marketing lists for your customers and prospects. 
                                        <br>
                                        <a class="more" href="hiw_cpp" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                <div style="position:relative; float:left;">
                                    <a href="hiw_red" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="32px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/dashboard3.png" style="margin: 30px 39px 30px 29px;">
                                        </span>
                                        
                                        <span style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>
                                    </a>
                               	</div>
                                
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_red" style="color: ##015172; text-decoration:none;">Reports, Exports and Dashboards</a>
                                    </h2>
                                    <p>
                                        Getting an insightful snapshot of important metrics doesn't have to require hours of work in a spreadsheet.
                                        <br>
                                        <a class="more" href="hiw_red" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                                <div style="position:relative; float:left;">	
                                    <a href="hiw_eai" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                        <span>
                                            <img class="tileiconBox" width="60px" height="40px" alt="icon" src="#rootUrl#/#PublicPath#/images/icons/interactive1.png" style="margin: 30px 20px;">
                                        </span>
                                        
                                        <span style="position:absolute; bottom:5px; right:5px;">
                                            <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                        </span>
                                    </a>
                                </div>
                               
                                <div class="TileCellHome">
                                    <h2>
                                        <a href="hiw_eai" style="color: ##015172; text-decoration:none;">Enterprise Application Integration</a>
                                    </h2>
                                    <p>
                                    	When carefully managed, EAI can be used to help an organization to better achieve goals, remove obstacles to market growth, reduce costly mistakes.
                                        <br>
                                        <a class="more" href="hiw_eai" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                    </p>
                                </div>
                                
                                <div style="clear:both; padding: 8px 0;"></div>
                                
                           
                           
                           </div>
                           
                     
                      
           
                           
                           
                   <!---        
                           <div id="serviceimagemain">
                                <div class="firstcontentbox">
                                       <div class="serviceimagefull">
                                            <div class="serviceimageinner">
                                            <!---Personalized messages with content that is high quality, relevant, personalized, and delivered in context--->
                                            <!---Dedicated Staffing<BR />Design Services<BR />ETL&R on Data<BR />Data Feeds<BR />Custom API Integration<BR />VPN Support<BR />--->
                                            
                                            </div>
                                       </div>
                                       <div class="contentheadertitle"><a href="##">Enterprise Application Integration</a></div>   
                                       <div class="ServicesText">Is your IT team's existing workload preventing you from moving forward on critical projects? Let our professional services team help. <!---Let #BrandShort#'s Team of experts handle everything!---></div>                            
                                </div>
                                <div class="firstcontentbox">
                                        <div class="serviceimageself">
                                            <div class="serviceimageinner"><!---Pre-Defined Campaign Tools<BR />Easy API's<BR />---></div>
                                        </div>    
                                        <div class="contentheadertitle"><a href="##">Self Service</a></div> 
                                        <div class="ServicesText">A powerful set of tools and intuitive interfaces to let you design and launch messaging campaigns</div>                                   
                                </div>
                                <div class="firstcontentbox">
                                       <div class="serviceimagehybrid">
                                            <div class="serviceimageinner"><!---Our staff can help you setup automated programs to get your application up and running fast!---></div>
                                       </div>
                                       <div class="contentheadertitle"><a href="##">Hybrid Model</a></div>                               
                                       <div class="ServicesText">Our staff can help you setup automated programs to get your applications up and running!</div>
                                </div>
                            </div>
                            --->
                
               		</div>
                    
                </div>
            
            </div>
            
            <div class="clear"></div>
            
                      
            <section class="section-one">
            	
               <!--- <div class="prlx-1"><div class="blue-mask"></div></div>--->
                
                <div style="padding:80px; color: ##f9f9f9;"><h2>#BrandShort# provides both e-Messaging Services (EMS) and tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for messaging services.</h2></div>
            </section>
            
         <!---    <div id="inner-bg-min" style="background-color:##f9f9f9; margin-top:0; padding-bottom:0;">
            
                <div id="content1" style="height:auto;">
                    
                    <div id="servicemain">
                                              
                                              
                            <div class="section-title-content clearfix" style="margin-bottom: 20px;">
                        	    <header class="section-title-inner">
	                                <h2>GET SMART TECHNOLOGY</h2>
    	                            <p>Segmented Messaging And Response Tracking</p>
                                </header>
                            </div>
                            
                     </div>
                    
                </div>
            
            </div>       --->
            
             
           <!--- <section class="section-two">
            	
                <div class="prlx-2"><div class="blue-mask"></div></div>
                
                <div style="padding:80px;"><h2>Platform as a Service (PaaS) - Software as a Service (SaaS) </h2></div>
            </section>     
            
            <div class="clear"></div>
            
              <section class="section-three">
            	
                <div class="prlx-3"><div class="blue-mask"></div></div>
                
                <div style="padding:80px;"><h2>Top Companies use #BrandShort#</h2></div>
            </section>     --->
            
                           
               <!--- EBM site footer included here --->
               <cfinclude template="act_ebmsitefooter.cfm">
           </div>
    </div>
       </body>
</cfoutput>
</html>
