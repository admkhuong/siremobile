<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About #BrandShort#</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">



<cfoutput>
	    
</cfoutput>




<style>
#ebmimage1 {
	width:970px;
	height:323px;
	padding:0;
	border:0px solid;
	text-align:center;
	background-image:url(../images/ebm_i_main/FPO-Header.png);
}
</style>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {

  	});

</script>


</head>
<cfoutput>
    <body>
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
       
                        
                            <!--- Last piece of the puzzle --->
        <div id="inner-bg">
            <div id="content">
               
               <div id="inner-right-img"> <img src="#rootUrl#/#PublicPath#/images/api/apppuzzle.jpg" /></div>
               
               <div id="inner-box-left" align="center">
          
                    <div class="inner-main-hd">About</div>
                    <div class="inner-txt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#BrandShort# helps you communicate with your customers in ways that will make a difference in their daily lives. Differentiate your brand by making interactions easier. #BrandShort# will help you to be unique in the relevent ways you reach out to and communicate with your customers. Remind them with the ease and quality of every interaction what it is they love about your company, and why they like to do business with you. They will find that not only have you built a connection with them, it is no longer just business, it's personal.</div>
                    <div class="phonemain">
                        
                         <div class="inner-row">
                                            
                            
                            <div class="inner-span">
                                <div class="inner-headline">
                                    <h2>Simplified integration within all your applications.</h2>
                                    <p>Web, Mobile, Desktop, Embeded devices, or even your refirgerator. Deliver secure fast reliable messaging from any of your web connected applications.</p>
                                </div>
                            </div>
                
			                <div class="inner-span inner-offset1">
                                <div class="inner-headline">
                                <h2>View customer interactions through reporting and monitoring.</h2>
                                <p>Configurable reports based on messaging interactions. View messaging activity, or check on campaign success rates.</p>
                                </div>
                            </div>
                                    
                         </div>
                
                
                         <div class="inner-row">
                                            
                            
                            <div class="inner-span">
                                <div class="inner-headline">
                                    <h2>Engage using rich subscriber centric personalization.</h2>
                                    <p>View and edit profiles across active campaigns and take advantage of limitless, customizable segmentation data.</p>
                                </div>
                            </div>
                
			                <div class="inner-span inner-offset1">
                                <div class="inner-headline">
                                <h2>Rules based filters using S.M.A.R.T.&trade; technology.</h2>
                                <p>Segmented Messaging with Automated Response Tracking. Filter messaging according to demographics, location, preferences, behavior - or any criteria you specify. </p>
                                </div>
                            </div>
                                    
                         </div>
                
                    </div>
               
                </div>
               
               	
           
            </div>
        </div>
          
        <div id="servicemainline"></div>  
 <!---       
        <!--- All of this ... --->       
        <div id="inner-bg">
                        
            
           <div id="content" style="min-height:36px;">
           		<div class="inner-main-hd">Kick off all of this ...</div>
              	<div class="inner-txt-full" style="margin-bottom:20px;">With Pre-Defined Campaigns you can design highly customized messages and business rules that can be accessed with one simple API call.</div>
              
           </div>
            
            <div id="PDCSurveyImg"> <img src="#rootUrl#/#PublicPath#/images/api/hiw_samplesurvey_web.png" height="539" width="900" /></div>
            
           <!---      
                 <div id="inner-content-list">
                     <ul>
                        <li>Complex Scheduling</li>
                        <li>Dynamic Content</li>
                        <li>Documentation</li>
                        <li>List Management</li>
                        <li>Compliance</li>
                        <li>Templates</li>
                        <li>Collaboration</li>
                        <li>Scripting Managment</li>
                        <li>DNC Scrubbing</li>
                        <li>Surveys</li>
                        <li>Reporting</li>
                        <li>... and more</li>
                     </ul>
                 </div>    --->
          
                         
        </div>--->
                        
        <div id="servicemainline"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>