<cfsetting showdebugoutput="no" />
<cfsetting enablecfoutputonly="true" />

<cftry>

    <cfquery name="ISAliveDB" datasource="Bishop" >
        SELECT 1
    </cfquery>

	<cfif ISAliveDB.RecordCount GT 0>
	    <cfoutput>up</cfoutput>  
    <cfelse>
    	<cfoutput>down</cfoutput>    
    </cfif>

<cfcatch type="any">
	<cfoutput>error</cfoutput>
</cfcatch>

</cftry>
