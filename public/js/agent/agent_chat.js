/**
 * Created by neovinhtru on 18/08/2014.
 */

$(document).ready( function() {
//    $('#tab-container').easytabs();
    $('.tooltip').tooltipster({
        theme: 'tooltipster-noir'
    });
    var box_more_u = $(".show_more_number");
    $(".wrap_icon").click(function (e){
        e.preventDefault();
        e.stopPropagation();
        box_more_u.stop().fadeIn(200);
    });

    $(document).bind("click",function (e){
        box_more_u.stop().fadeOut(200);
    });
    //handle for count character
    var max_char = 160;
    $("#count_char").html(max_char + ' / 160');
    $("#area_type").keyup(function (){
        var el = $(this);
        if (el.val().length >= max_char) {
            el.val(el.val().substr(0,max_char));
        }else{
            $("#count_char").text(max_char - el.val().length + ' / 160');
        }
    });
    $('#tab-container ul.etabs li:nth-child(1)').addClass('active');
    $('#tab-container ul.etabs li:nth-child(1) a').addClass('t_active');
    $('#tab-container .tabs_cont .content-tab').each(function(){
        if($(this).index()==0){
            $(this).css('display','block');
        }else{
            $(this).css('display','none');
        }
    });
    // Show content message chat of SMS
    $('#tab-container ul.etabs a').die();
    $('#tab-container ul.etabs a').live("click",function(){
        if(!$(this).hasClass('t_active')){
            $('#tab-container ul.etabs li').removeClass('active');
            $('#tab-container ul.etabs li a').removeClass('t_active');
            $(this).addClass('t_active');
            var this_parent = $(this).parent();
            this_parent.addClass('active');
            $('#tab-container .tabs_cont .content-tab').css('display','none');
            var id_content = $(this).attr('rel');
            $(id_content).css('display','block');
            if(this_parent.find('.icon_new_mess').length >0){
                this_parent.find('.icon_new_mess').remove();
            }
            $("#cancel_response").trigger('click');
        }
    });
    // show more message chat
    $('.show_more_number li').die();
    $('.show_more_number li').live("click",function(){
        $(this).removeClass('newmgs');
        $(this).find('a').removeClass('t_active');
        var clone_more = $(this).clone();
        clone_more = clone_more.addClass('tab');
        var clone_in_tab = $('#tab-container .etabs li:nth-child(4)').clone();
        clone_in_tab = clone_in_tab.removeClass('tab');
        clone_in_tab = clone_in_tab.removeClass('active');
        if(clone_in_tab.find('.icon_new_mess').length >0){
           clone_in_tab = clone_in_tab.addClass('newmgs');
        }
        $('#tab-container .etabs li:nth-child(4)').remove();
        $('#tab-container .etabs').append(clone_more);
        clone_in_tab.insertBefore($(this));
        $(this).remove();
        $('#tab-container .etabs li:nth-child(4) a').trigger('click');
        $("#cancel_response").trigger('click');
    });
});