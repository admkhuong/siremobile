(function() {
  var DEFAULT_BRACKETS = "()[]{}''\"\"";
  var DEFAULT_EXPLODE_ON_ENTER = "[]{}";
  var SPACE_CHAR_REGEX = /\s/;

  codemirror.defineOption("autoCloseBrackets", false, function(cm, val, old) {
    if (old != codemirror.Init && old)
      cm.removeKeyMap("autoCloseBrackets");
    if (!val) return;
    var pairs = DEFAULT_BRACKETS, explode = DEFAULT_EXPLODE_ON_ENTER;
    if (typeof val == "string") pairs = val;
    else if (typeof val == "object") {
      if (val.pairs != null) pairs = val.pairs;
      if (val.explode != null) explode = val.explode;
    }
    var map = buildKeymap(pairs);
    if (explode) map.Enter = buildExplodeHandler(explode);
    cm.addKeyMap(map);
  });

  function charsAround(cm, pos) {
    var str = cm.getRange(codemirror.Pos(pos.line, pos.ch - 1),
                          codemirror.Pos(pos.line, pos.ch + 1));
    return str.length == 2 ? str : null;
  }

  function buildKeymap(pairs) {
    var map = {
      name : "autoCloseBrackets",
      Backspace: function(cm) {
        if (cm.somethingSelected()) return codemirror.Pass;
        var cur = cm.getCursor(), around = charsAround(cm, cur);
        if (around && pairs.indexOf(around) % 2 == 0)
          cm.replaceRange("", codemirror.Pos(cur.line, cur.ch - 1), codemirror.Pos(cur.line, cur.ch + 1));
        else
          return codemirror.Pass;
      }
    };
    var closingBrackets = "";
    for (var i = 0; i < pairs.length; i += 2) (function(left, right) {
      if (left != right) closingBrackets += right;
      function surround(cm) {
        var selection = cm.getSelection();
        cm.replaceSelection(left + selection + right);
      }
      function maybeOverwrite(cm) {
        var cur = cm.getCursor(), ahead = cm.getRange(cur, codemirror.Pos(cur.line, cur.ch + 1));
        if (ahead != right || cm.somethingSelected()) return codemirror.Pass;
        else cm.execCommand("goCharRight");
      }
      map["'" + left + "'"] = function(cm) {
        if (left == "'" && cm.getTokenAt(cm.getCursor()).type == "comment")
          return codemirror.Pass;
        if (cm.somethingSelected()) return surround(cm);
        if (left == right && maybeOverwrite(cm) != codemirror.Pass) return;
        var cur = cm.getCursor(), ahead = codemirror.Pos(cur.line, cur.ch + 1);
        var line = cm.getLine(cur.line), nextChar = line.charAt(cur.ch), curChar = cur.ch > 0 ? line.charAt(cur.ch - 1) : "";
        if (left == right && codemirror.isWordChar(curChar))
          return codemirror.Pass;
        if (line.length == cur.ch || closingBrackets.indexOf(nextChar) >= 0 || SPACE_CHAR_REGEX.test(nextChar))
          cm.replaceSelection(left + right, {head: ahead, anchor: ahead});
        else
          return codemirror.Pass;
      };
      if (left != right) map["'" + right + "'"] = maybeOverwrite;
    })(pairs.charAt(i), pairs.charAt(i + 1));
    return map;
  }

  function buildExplodeHandler(pairs) {
    return function(cm) {
      var cur = cm.getCursor(), around = charsAround(cm, cur);
      if (!around || pairs.indexOf(around) % 2 != 0) return codemirror.Pass;
      cm.operation(function() {
        var newPos = codemirror.Pos(cur.line + 1, 0);
        cm.replaceSelection("\n\n", {anchor: newPos, head: newPos}, "+input");
        cm.indentLine(cur.line + 1, null, true);
        cm.indentLine(cur.line + 2, null, true);
      });
    };
  }
})();
