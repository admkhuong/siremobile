// Depends on coffeelint.js from http://www.coffeelint.org/js/coffeelint.js

// declare global: coffeelint

codemirror.registerHelper("lint", "coffeescript", function(text) {
  var found = [];
  var parseError = function(err) {
    var loc = err.lineNumber;
    found.push({from: codemirror.Pos(loc-1, 0),
                to: codemirror.Pos(loc, 0),
                severity: err.level,
                message: err.message});
  };
  try {
    var res = coffeelint.lint(text);
    for(var i = 0; i < res.length; i++) {
      parseError(res[i]);
    }
  } catch(e) {
    found.push({from: codemirror.Pos(e.location.first_line, 0),
                to: codemirror.Pos(e.location.last_line, e.location.last_column),
                severity: 'error',
                message: e.message});
  }
  return found;
});
codemirror.coffeeValidator = codemirror.lint.coffeescript; // deprecated
