// Depends on jsonlint.js from https://github.com/zaach/jsonlint

// declare global: jsonlint

codemirror.registerHelper("lint", "json", function(text) {
  var found = [];
  jsonlint.parseError = function(str, hash) {
    var loc = hash.loc;
    found.push({from: codemirror.Pos(loc.first_line - 1, loc.first_column),
                to: codemirror.Pos(loc.last_line - 1, loc.last_column),
                message: str});
  };
  try { jsonlint.parse(text); }
  catch(e) {}
  return found;
});
codemirror.jsonValidator = codemirror.lint.json; // deprecated
