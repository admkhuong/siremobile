(function() {
  codemirror.defineMode("markdown_with_stex", function(){
    var inner = codemirror.getMode({}, "stex");
    var outer = codemirror.getMode({}, "markdown");

    var innerOptions = {
      open: '$',
      close: '$',
      mode: inner,
      delimStyle: 'delim',
      innerStyle: 'inner'
    };

    return codemirror.multiplexingMode(outer, innerOptions);
  });

  var mode = codemirror.getMode({}, "markdown_with_stex");

  function MT(name) {
    test.mode(
      name,
      mode,
      Array.prototype.slice.call(arguments, 1),
      'multiplexing');
  }

  MT(
    "stexInsideMarkdown",
    "[strong **Equation:**] [delim $][inner&tag \\pi][delim $]");
})();
