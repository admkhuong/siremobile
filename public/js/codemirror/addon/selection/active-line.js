// Because sometimes you need to style the cursor's line.
//
// Adds an option 'styleActiveLine' which, when enabled, gives the
// active line's wrapping <div> the CSS class "codemirror-activeline",
// and gives its background <div> the class "codemirror-activeline-background".

(function() {
  "use strict";
  var WRAP_CLASS = "codemirror-activeline";
  var BACK_CLASS = "codemirror-activeline-background";

  codemirror.defineOption("styleActiveLine", false, function(cm, val, old) {
    var prev = old && old != codemirror.Init;
    if (val && !prev) {
      updateActiveLine(cm);
      cm.on("cursorActivity", updateActiveLine);
    } else if (!val && prev) {
      cm.off("cursorActivity", updateActiveLine);
      clearActiveLine(cm);
      delete cm.state.activeLine;
    }
  });

  function clearActiveLine(cm) {
    if ("activeLine" in cm.state) {
      cm.removeLineClass(cm.state.activeLine, "wrap", WRAP_CLASS);
      cm.removeLineClass(cm.state.activeLine, "background", BACK_CLASS);
    }
  }

  function updateActiveLine(cm) {
    var line = cm.getLineHandleVisualStart(cm.getCursor().line);
    if (cm.state.activeLine == line) return;
    clearActiveLine(cm);
    cm.addLineClass(line, "wrap", WRAP_CLASS);
    cm.addLineClass(line, "background", BACK_CLASS);
    cm.state.activeLine = line;
  }
})();
