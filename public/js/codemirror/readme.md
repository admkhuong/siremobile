# codemirror
[![Build Status](https://secure.travis-ci.org/marijnh/codemirror.png?branch=master)](http://travis-ci.org/marijnh/codemirror)
[![NPM version](https://badge.fury.io/js/codemirror.png)](http://badge.fury.io/js/codemirror)

codemirror is a JavaScript component that provides a code editor in
the browser. When a mode is available for the language you are coding
in, it will color your code, and optionally help with indentation.

The project page is http://codemirror.net  
The manual is at http://codemirror.net/doc/manual.html  
The contributing guidelines are in [CONTRIBUTING.md](https://github.com/marijnh/codemirror/blob/master/CONTRIBUTING.md)
