	function InitSavedContacts(contactCookieKey, groupId) {
		var savedContacts = $.cookie(contactCookieKey);
		if (savedContacts != undefined && savedContacts != "") {
			var contactList = new Array();
			var elements = savedContacts.split(':');
			if (elements[0] == groupId) {
				var contacts = elements[1].split(',');
				var count = 0;
				$.each(contacts, function (index, item) {
					if (document.getElementById(item) != null) {
						document.getElementById(item).checked = true;
						count++;
					}
				});
				
				if (count == $("input[type=checkbox]").length - 1) {
					$('#chek_box_all').attr('checked', true);
					
				}
				
			}
			
			else {
				savedContacts = '<cfoutput>groupId</cfoutput>:';
			}
		}
		
		$.cookie(contactCookieKey, savedContacts)		
	}

	function SaveCheckedContacts(contactCookieKey, groupId, controlName) {
		var savedContacts = $.cookie(contactCookieKey);
		var contactList = new Array();
		if (savedContacts != undefined && savedContacts != "") {
			var elements = savedContacts.split(':');
			if (elements[0] == groupId) {
				var contacts = elements[1].split(',');
				$.each(contacts, function (index, item) {
					contactList.push(item);
				});
			}
		}
		
		$("input[name=" + controlName + "]").each(function () {
			DoCheck(contactList, this.id, this.checked)
		});
		
		var groupContacts = groupId + ":";
		$.each(contactList, function (index, item) {
			if (index < contactList.length - 1) {
				groupContacts += item + ","
			}
			else {
				groupContacts += item;
			}
		});
		
		$.cookie(contactCookieKey, groupContacts)
	}
	
	function DoCheck(contactList, item, isChecked) {
		var index = GetContact(contactList, item);
		
		if (isChecked) {
			if (index == -1) {
				contactList.push(item)
			}
		}
		else {
			if (index != -1) {
				contactList.splice(index, 1);
			}
		}
	}
	
	function GetContact(contactList, item) {
		for (var i = 0; i < contactList.length; ++i) {
			if (contactList[i] == item) {
				return i;
			}
		}
		
		return -1;
	}
	
	//Check email is valid
    function IsValidEmail(email) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        return reg.test(email);
    }
    
    function GetPage(url) {
    	n = url.lastIndexOf('/');
    	if (n >= 0) {
    		url = url.substring(n + 1);
    	}    	
    	return url;    	
    }
    var ServerProxy = {};

    ServerProxy.DefaultOnError = function (ex) {
        jAlert("System Warning", ex.status + '\n\r' + ex.statusText + '\n\r' + ex.responseText, "System Message");
    };

    ServerProxy.PostToServer = function (dataUrl, callMethod, dataValue, errorMessage, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: dataUrl + "?method=" + callMethod + "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data: dataValue,
            dataType: "json",
            async: false,
            success: function(d) {
            	if (d.ROWCOUNT > 0) 
				{						
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = parseInt(d.DATA.RXRESULTCODE[0]);	
						
						if (typeof (onSuccess) == "function") {
							onSuccess(d);
                        }
						if(CurrRXResultCode < 1)
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert(errorMessage + "\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
						}
					}
				}
				else
				{		
					$.alerts.okButton = '&nbsp;OK&nbsp;';					
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.", function(result) { });
				}
            }, error: onError != undefined ? onError : this.DefaultOnError
        });
    };
    
    ServerProxy.PostToServerStruct = function (dataUrl, callMethod, dataValue, errorMessage, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: dataUrl + "?method=" + callMethod + "&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
            data: dataValue,
            dataType: "json",
            async: false,
            success: function(dStruct ) {
				// Check if variable is part of JSON result string	
								
				if(typeof(dStruct.RXRESULTCODE != "undefined"))
				{				
				
					if(typeof(dStruct.ROWCOUNT != "undefined"))
						if (dStruct.ROWCOUNT > 0) 
							CurrRXResultCode = parseInt(dStruct.DATA.RXRESULTCODE[0]);
						else	
							CurrRXResultCode = parseInt(dStruct.RXRESULTCODE);			
					else
						CurrRXResultCode = parseInt(dStruct.RXRESULTCODE);	
					
					if(CurrRXResultCode > 0)
					{
						if (typeof (onSuccess) == "function") 
						{
							onSuccess(dStruct);
                        }									
						return false;
							
					}
					else
					{
						// $.alerts.okButton = '&nbsp;OK&nbsp;';
						// jAlert(errorMessage + "\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE, "Failure!", function(result) { } );		
						alert(errorMessage + "\n"  + dStruct.MESSAGE + "\n" + dStruct.ERRMESSAGE);					
					}
				}
								
            },
            error: onError != undefined ? onError : this.DefaultOnError
        });
    };
    
    var dialog = 0;
	function ShowVoiceRecordDialog(url, title, height, width) {
		if (title == undefined) {
			title = 'Record Voice';
		}
		
		if (height == undefined) {
			height = 630;
		}
		
		if (width == undefined) {
			width = 500;
		}
		if (dialog != 0) {
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			dialog.remove();
			dialog = 0;
		}
				
		dialog = $('<div></div>');
	
		dialog
			.load(url)
			.dialog({
				modal : true,
				title: title,
				width: width,
				height: height,
				position: 'top',
				beforeClose: function(event, ui) {
					$("#jquery_jplayer_1").jPlayer("stop");
				}			
			});
		dialog.parent().css({position:"fixed", top: "0px"});
		dialog.dialog('open');
	}
	
	var newDialog = 0;
	function OpenDialog(url, title, height, width, id, isResize, isShowMultiDialog, pos) {
		
		pos = typeof pos !== 'undefined' ? pos : 'top';
				
		if (height == undefined) {
			height = 630;
		}
		
		if (width == undefined) {
			width = 500;
		}
		
		if(isShowMultiDialog == undefined){
			if (newDialog != 0) {
				newDialog.remove();
				newDialog = 0;
			}
		}
		
		
		var dialogId = id;
		if(id == undefined){
			dialogId = "";
		}
		
		var resizable = isResize;
		if(isResize == undefined){
			resizable = true;
		}
		newDialog = $('<div id="'+ dialogId +'"></div>');
	
		newDialog
			.load(url)
			.dialog({
				modal : true,
				title: title,
				width: width,
				height: height,
				resizable: resizable,
				position: pos,
				beforeClose: function(event, ui) {
				},
				close: function(event, ui)
				{
					$(this).dialog('destroy').remove();
				}
							
			});
		
		if(pos == 'top')
		  newDialog.parent().css({top: "80px"});  // position:"fixed" - no - this wont allow small browsers to scroll large dialogs 
				
		newDialog.dialog('open');	
	}
	
	function closeDialog() {
		newDialog.dialog('close');
	}
	
	function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function nl2br (str, is_xhtml) 
	{
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
		// return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
		return (str + '').replace(/\n/g,'<br />');
		
	}
	
	function br2nl (str)
	{
		return (str + '').replace(/<br\s*\/?>/mg,"\n");
	};

