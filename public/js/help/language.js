/* HELP contents for MCID tool */
var MCID_HELP = {
	/* For rxt1Template: play */
	rxt1: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input integers only.",
		inpCK5: "Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt2Template */
	rxt2: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Input integers only.",
		inpCK3: "Please input -1 or NR(No Response).",
		inpCK8: " Input integers only.",
		inpCK6: " Input integers only.",
		inpCK7: " Input integers only.",
		inpCK5: "Input existed QID or -1 or positive number.",
		inpCK4: " Please input correctly format: (x,y). X only accepts values from 0 to 9 .Y only accepts QID of MCID object",
		inpCK9: "Please input positive number from 1 to 150.",
		inpCK10: " Input integers only.",
		inpCK11: " Input integers only.",
		inpCK12: " Input integers only.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt3Template: Record a Response */
	rxt3: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Please input positive number.",
		inpCK2: " Input integers only.",
		inpCK3: " Input integers only.",
		inpCK4: " Input integers only.",
		inpCK5: " Input integers only.",
		inpCK6: " Input integers only.",
		inpCK7: " Input integers only.",
		inpCK8:"Input existed QID or -1 or positive number.",
		inpCK9: " Input integers only.",
		inpCK10: " Input integers only.",
		inpCK11: "Please input -1 or NR(No Response).",
		inpCK12:  "Please input positive number.",
		inpCK13: "Maximum 50 characters.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt4Template: Live Agent Transfer (LAT�) */
	rxt4: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Please input more than or equal to 10 digits.",
		inpCK2:  " Input integers only.",
		inpCK3: "Please input positive number.",
		inpCK4:  "Please input more than or equal to 10 digits.",
		inpCK5: " Input integers only.",
		inpCK6: " Input integers only.",
		inpCK7: " Input integers only.",
		inpCK8: "Please input only 1 or 0.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt5Template: Opt Out */
	rxt5: {
		INPDESC: "Input only number, charactor",
		inpCK5: " Input existed QID or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt6Template: IVR Multi Digit String Collection  */
	rxt6: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Input positive number less than or equal 255.",
		inpCK2: "Please input positive number from 1 to 10",
		inpCK3: "Please input positive number from 1 to 29.",
		inpCK4: "This field is read only.",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpCK6: "Please input only 1 or 0.",
		inpCK7: " Input positive number less than or equal 255.",
		inpCK8: " Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt7Template: Opt In */
	rxt7: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Input integers only.",
		inpCK2: " Input integers only.",
		inpCK3: " Input integers only.",
		inpCK4:" Input integers only.",
		inpCK5: " Input integers only.",
		inpCK6: " Input integers only.",
		inpCK7: " Input integers only.",
		inpCK8:" Input existed QID or -1 or positive number.",
		inpCK9: " Input integers only.",
		inpCK10: " Input integers only.",
		inpCK11: " Input integers only.",
		inpCK12: " Input existed QID or -1 or positive number.",
		inpCK13: "Maximum 50 characters.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt9Template: Read Out Digit String */
	rxt9: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Input integers only.",
		inpCK2: " Input positive number from 1 to 10.",
		inpCK3: " Input positive number only.",
		inpCK4: " Input positive number or word ANI .",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpRQ: " Input integers only.",
		inpCP: " Input integers only."
	},
	/* For rxt10Template: SQL Query */
	rxt10: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Maximun up to 2048 character.",
		inpCK2: " Input only 1 or 0.",
		inpCK4: " Read only.",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpRQ: " Input integers only.",
		inpCP: " Input integers only."
	},
	/* For rxt11Template: Play DTMFs */
	rxt11: {
		INPDESC: "Input only number, charactor",
		inpCK1: " Maximum 1024 character.",
		inpCK5:" Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt12Template: Strategic Pause */
	rxt12: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input positive number is less than or equal 10000000 or -1.",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpRQ:  "Input integers only.",
		inpCP:  "Input integers only."
	},
	/* For rxt13Template: eMail */
	rxt13: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input only 1 or 0.",
		inpCK2: "string.",
		inpCK3: "string.",
		inpCK4: "string.",
		inpCK5: "string.",
		inpCK6: "string.",
		inpCK7: "string.",
		inpCK8: "string.",
		inpCK9: "string.",
		inpCK10: "string.",
		inpCK11: "string.",
		inpCK12: "string.",
		inpCK13: "string.",
		inpCK14: "string.",
		inpCK15: " Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt14Template: Store IGData for inbound calls */
	rxt14: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input integer from 1 throgh 10 only.",
		inpCK2: "Maximum up to 50 chacracters.",
		inpCK5:" Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt15Template: Play from absolute path */
	rxt15: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input string has at least two numbers.",
		inpCK2: "Maximum 1024 chacracters.",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt16Template: Switch based on the reponse to a previous MCID */
	rxt16: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input integer (except : 0)",
		inpCK2: "Input integers only.",
		inpCK3: "Input integers only.",
		inpCK4: "Read only.",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt17Template: Switchout MCIDs based on SQL query */
	rxt17: {
		INPDESC: "Input only number, charactor",
		inpCK1: "String with maxlength is 64KB.",
		inpCK2: "Input integers number greater than 1.",
		inpCK5:  " Input existed QID or -1 or positive number.",
		inpRQ:  "Input integers only.",
		inpCP:  "Input integers only."
	},
	/* For rxt18Template: Automatic Speech Recognition */
	rxt18: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input integers only.",
		inpCK2: "Input integers only.",
		inpCK3: "Input integers only.",
		inpCK4: " Please input correctly format: (x,y). X only accepts values from 0 to 9 .Y only accepts QID of MCID object",
		inpCK5: " Input existed QID or -1 or positive number.",
		inpCK8: "Maxlength 64KB",
		inpCK9: "Input integers only.",
		inpCK10: "Input integers only.",
		inpCK11: "Input integers only.",
		inpCK12: "Input only 1 or 0 .",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt19Template: Automatic Speech Recognition */
	rxt19: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input POST or GET only.",
		inpCK2: "Input integers only.",
		inpCK3: "Input positive number only.",
		inpCK4: "Read only.",
		inpCK5:" Input existed QID or -1 or positive number.",
		inpCK6: "Maximum 2048 characters.",
		inpCK7: "Maximum 2048 characters.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt20Template: Automatic Speech Recognition */
	rxt20: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Maximum 2048 characters.",
		inpCK2: "Input existed QID or -1 or positive number.",
		inpCK4: "Read only.",
		inpCK5: "Input existed QID or -1 or positive number.",
		inpCK6: "Input integers only.",
		inpCK7: "Input integers only.",
		inpCK7: "Input integers only.",
		inpCK8: "Input Positive number  greater than 1900.",
		inpCK9: "Input integers only.",
		inpCK10: "Input integer from 1 throgh 12 only.",
		inpCK11: "Input integer from 1 throgh 7 only.",
		inpCK12: "Input integers only.",
		inpCK13:"Input integer from 1 throgh 31 only.",
		inpCK14: "Input 8 or 16 only.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt21Template: Automatic Speech Recognition */
	rxt21: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input integers only.",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt23Template:DM */
	rxt23: {
		INPDESC: "Input only number, charactor",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	/* For rxt23Template:Switch on Call State  */
	rxt24: {
		INPDESC: "Input only number, charactor",
		inpCK1: "Input existed QID or -1",
		inpCK2: "Input existed QID or -1",
		inpCK5: "Input existed QID or -1",
		inpRQ: "Input integers only.",
		inpCP: "Input integers only."
	},
	
};