// Open simple dialogs on top of an editor. Relies on dialog.css.

(function() {
  function dialogDiv(cm, template) {
    var wrap = cm.getWrapperElement();
    var dialog = wrap.insertBefore(document.createElement("div"), wrap.firstChild);
    dialog.className = "codemirror-dialog";
    dialog.innerHTML = '<div>' + template + '</div>';
    return dialog;
  }

  codemirror.defineExtension("openDialog", function(template, callback) {
    var dialog = dialogDiv(this, template);
    var closed = false, me = this;
    function close() {
      if (closed) return;
      closed = true;
      dialog.parentNode.removeChild(dialog);
    }
    var inp = dialog.getElementsByTagName("input")[0], button;
    if (inp) {
      codemirror.connect(inp, "keydown", function(e) {
        if (e.keyCode == 13 || e.keyCode == 27) {
          codemirror.e_stop(e);
          close();
          me.focus();
          if (e.keyCode == 13) callback(inp.value);
        }
      });
      inp.focus();
      codemirror.connect(inp, "blur", close);
    } else if (button = dialog.getElementsByTagName("button")[0]) {
      codemirror.connect(button, "click", function() {
        close();
        me.focus();
      });
      button.focus();
      codemirror.connect(button, "blur", close);
    }
    return close;
  });

  codemirror.defineExtension("openConfirm", function(template, callbacks) {
    var dialog = dialogDiv(this, template);
    var buttons = dialog.getElementsByTagName("button");
    var closed = false, me = this, blurring = 1;
    function close() {
      if (closed) return;
      closed = true;
      dialog.parentNode.removeChild(dialog);
      me.focus();
    }
    buttons[0].focus();
    for (var i = 0; i < buttons.length; ++i) {
      var b = buttons[i];
      (function(callback) {
        codemirror.connect(b, "click", function(e) {
          codemirror.e_preventDefault(e);
          close();
          if (callback) callback(me);
        });
      })(callbacks[i]);
      codemirror.connect(b, "blur", function() {
        --blurring;
        setTimeout(function() { if (blurring <= 0) close(); }, 200);
      });
      codemirror.connect(b, "focus", function() { ++blurring; });
    }
  });
})();
