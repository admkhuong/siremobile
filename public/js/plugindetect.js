var installedPlugins = {};

var plugins = {'flash': {
                    'mimeType': ['application/x-shockwave-flash'],
                    'ActiveX': ['ShockwaveFlash.ShockwaveFlash', 'ShockwaveFlash.ShockwaveFlash.3', 'ShockwaveFlash.ShockwaveFlash.4', 'ShockwaveFlash.ShockwaveFlash.5', 'ShockwaveFlash.ShockwaveFlash.6', 'ShockwaveFlash.ShockwaveFlash.7']
                  },
                  'shockwave-director': {
                    'mimeType': ['application/x-director'],
                    'ActiveX': ['SWCtl.SWCtl', 'SWCt1.SWCt1.7', 'SWCt1.SWCt1.8', 'SWCt1.SWCt1.9', 'ShockwaveFlash.ShockwaveFlash.1']
                  },
                  'quicktime': {
                    'mimeType': ['video/quicktime'],
                    'ActiveX': ['QuickTime.QuickTime', 'QuickTimeCheckObject.QuickTimeCheck.1', 'QuickTime.QuickTime.4']
                  },
                  'windows-media-player': {
                    'mimeType': ['application/x-mplayer2'],
                    'ActiveX': ['WMPlayer.OCX', 'mediaPlayer.mediaPlayer.1']
                  }
                 };
                
 function checkSupport(p){
  var support = false;
  //for standard compliant browsers
  if (navigator.mimeTypes) {
    for (var i = 0; i < p['mimeType'].length; i++) {
      if (navigator.mimeTypes[p['mimeType'][i]] && navigator.mimeTypes[p['mimeType'][i]].enabledPlugin) {
        support = true;
      }
    }
  }
  //for IE
  if (typeof ActiveXObject != 'undefined') {
    for (var i = 0; i < p['ActiveX'].length; i++) {
      try {new ActiveXObject(p['ActiveX'][i]);
            support = true;
           }
      catch (err) {}
      }
  }
 return support;
}




//we loop through all the plugins
var count = 0;
var plugInCheck = new Array(1);

for (plugin in plugins) {
    //if we find one that's installed...
    if (checkSupport(plugins[plugin])) {
        //we add a property that matches that plugin's name to our "installedPlugins" object and set it's value to "true"
        installedPlugins[plugin] = true;
         plugInCheck[count] = true;
        //and add a class to the html element, for styling purposes
        document.documentElement.className += ' ' + plugin;
        //alert(plugin+":"+true);
        count++;
    }
    else {
        installedPlugins[plugin] = false;
        plugInCheck[count] = false;
        //alert(plugin+":"+false);
        count++;
    }
}

var test_audio= document.createElement("audio") //try and create sample audio element
var mediasupport={audio: (test_audio.play)? plugInCheck[count] = true : plugInCheck[count] = false}
 

function focusedTextBox(id,v)
{
	var vNow = $('#'+id).val();
	if(v===vNow)
	{
		$('#'+id).val('');
		$('.notesFilter').css({'color':'black'});
	}
	
}

function bluredTextBox(id,v)
{
	var vNow = $('#'+id).val();
	if(vNow=='')
	{
		$('#'+id).val(v);
		$('.notesFilter').css({'color':'#999'});
	}
	
}

//'search_script','Type here to search...'

