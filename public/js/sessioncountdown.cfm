
<!--- Countdown timer for session login --->
<script type="text/javascript">

// Alert("Yo CD!");

<!--- Default 20 minute timer is  --->
var CurrentTimeLeft = <cfoutput>#TimeOutSeconds#</cfoutput>;

function timecount(inpVal, num1, num2)
{
	strval = ((Math.floor(inpVal/num1))%num2).toString();
  	if (strval.length < 2)
    	strval = "0" + strval
	return strval ;
}

function CountDown()
{
	if (CurrentTimeLeft < 0)
	{
		// document.title = "<cfoutput>#PageTitle#</cfoutput> - Session Expired"; //  document.getElementById("LoginTimer").innerHTML = "Done";
		
		$('.SessionTimer').html('Session Expired');
		
		$('.sessionAccountInfoContainer').html('Logged Out');
		
		
		<!--- Re-Open modal login dialog --->								
		$('#dialogExpired').dialog('open'); 	
		return;
	}
	
	<!---
	Days      timecount(secs,86400,100000)
	Hours     timecount(secs,3600,24)
	Mins      timecount(secs,60,60)
	Secs      timecount(secs,1,60)
	--->
	
	<!--- HH:MM:SS --->
	// document.getElementById("LoginTimer").innerHTML
 	// document.title = "<cfoutput>#PageTitle#</cfoutput> " + timecount(CurrentTimeLeft,3600,24) + ":" + timecount(CurrentTimeLeft,60,60) + ":" + timecount(CurrentTimeLeft,1,60);

	$('#SessionTimer').html("" + timecount(CurrentTimeLeft,3600,24) + ":" + timecount(CurrentTimeLeft,60,60) + ":" + timecount(CurrentTimeLeft,1,60) );

	CurrentTimeLeft--;
	setTimeout("CountDown();", 1000);
}


function ForceLogoutPopup()
{	
	CurrentTimeLeft = -1;
	
	CountDown();
}


function ResetCountDown()
{
	if(CurrentTimeLeft > 100)
		CurrentTimeLeft = <cfoutput>#TimeOutSeconds#</cfoutput>;
}


</script>
