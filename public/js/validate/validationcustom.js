    /**
     *
     * @param {jqObject} the field where the validation applies
     * @param {Array[String]} validation rules for this field
     * @param {int} rule index
     * @param {Map} form options
     * @return an error string if validation failed
     */	
	var QID_ARRAY = [];   
	function checkHELLO(field, rules, i, options){
	    if (field.val() != "HELLO") {
	        // this allows to use i18 for the error msgs
	        return options.allrules.validate2fields.alertText;
	    }
	}
	function checkKey(field, rules, i, options){
		var str = field.val();
		if (str[3] != '-') {
			return options.allrules.checkkey.alertText;
		}
		if (str[7] != '-') {
			return options.allrules.checkkey.alertText;
		}
	}
	function checkPhoneNumber(field, rules, i, options){
		var field = field.val();
		if (field.length > 12) {
			return options.allrules.checkkey.alertText;
		}
		if (field.length != 10) {
			
			if (field[3] != '-') {
				return options.allrules.checkkey.alertText;
			}
			if (field[7] != '-') {
				return options.allrules.checkkey.alertText;
			}
		}
	}
	function checkMap(field, rules, i, options) {
		var field = field.val();
		str = field;
		if (str[0] != '\(') {
			return options.allrules.checkMap.alertText;
		}
		str = field.substring(1,field.length-1);
		var mySplitResult = str.split("),(");
		
		for(i = 0; i < mySplitResult.length; i++){
			var tmp = mySplitResult[i].split(",");
			if ((tmp[0] != "-1") && (tmp[0] != "-6") && (tmp[0] != "-13") && (tmp[0] != "*") && (tmp[0] != "#")) {
				if (isNaN(tmp[0]) || parseInt(tmp[0]) < 0 || parseInt(tmp[0]) > 9){
					return options.allrules.checkMap.alertText;
				}
			}
			if (tmp.length >= 2) {
				if ((isNaN(tmp[1])) || !checkQIDInAnsMap(parseInt(tmp[1]))) {
					return options.allrules.checkMap.alertText;
				}	
			} else {
				return options.allrules.checkMap.alertText;
			}
			
		}
	}
	
	function checkMapASR(field, rules, i, options) {
		var field = field.val();
		str = field;
		if (str[0] != '\(') {
			return options.allrules.checkMap.alertText;
		}
		str = field.substring(1,field.length-1);
		var mySplitResult = str.split("),(");
		
		for(i = 0; i < mySplitResult.length; i++){
			var tmp = mySplitResult[i].split(",");
			if ((tmp[0] != "-1") && (tmp[0] != "-6")&& (tmp[0] != "-13") && (tmp[0] != "*") && (tmp[0] != "#")) {
				if (isNaN(tmp[0]) || parseInt(tmp[0]) < 0 || parseInt(tmp[0]) > 13){
					return options.allrules.checkMapASR.alertText;
				}
			}
			if (tmp.length >= 2) {
				if ((isNaN(tmp[1])) || !checkQIDInAnsMap(parseInt(tmp[1]))) {
					return options.allrules.checkMapASR.alertText;
				}	
			} else {
				return options.allrules.checkMapASR.alertText;
			}
			
		}
	}
	function checkQIDInAnsMap(qid){
    	for (var i=0;i<QID_ARRAY.length;i++) {
       		if (QID_ARRAY[i] == qid) {
       			return true;
       		}
       	}
    	return false;
	}
	function equalsNumber(field, rules, i, options) {
		var field = field.val().replace(/^\s+|\s+$/g, '');
		if (field == '#') field = -13;
		if (field == '*') field = -6;
		if (field.length!=0 && (rules[i+2] != field) && (rules[i+3] != field)) {
			return options.allrules.equalsNumber.alertText;
		}
	}
	function equalsNumber2(field, rules, i, options) {
		var field = field.val().replace(/^\s+|\s+$/g, '');
		if (field.length != 0 && (rules[i+2] != field) && (rules[i+3] != field)) {
			return options.allrules.equalsNumber.alertText2+rules[i+2]+' or '+rules[i+3];
		}
	}
	/* check number QID */
    function equalsNumberQID(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if(field.length == 0){
    		return;
    	}
    	var check = 0;
  		//alert(QID_ARRAY);
       	for (var i=0;i<QID_ARRAY.length;i++) {
       		if (QID_ARRAY[i] == field) {
       			check = 1;
       		}
       	}
       	if (field == -1) {
       		check = 1;
       	}

    	if (check == 0) {
    		return options.allrules.custom.alertTextQID;
    	}
    }

    function formatCDS(field, rules, i, options) {
    	var field = field.val();
    	if ((field.length > 5) || (field.length < 4)) {
    		return options.allrules.custom.alertTextCDS;
    	}
    	if (field.substr(0,3) != 'CDS') {
    		return options.allrules.custom.alertTextCDS;
    	}
    	if (isNaN(field.substring(4,field.length-1))) {
    		return options.allrules.custom.alertTextCDS;
    	}
    }
    
    /**
     * Check valid url 
     * @param field
     * @param rules
     * @param i
     * @param options
     * @returns
     */
    function checkUrl(field, rules, i, options) {
    	var field = field.val();
    	var regexp = /^((https|http|ftp):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.\-\?\%\!&#~%_=]*)*\/?$/;
    	if(!regexp.test(field)){
    		return options.allrules.checkUrl.alertText;
    	}
    }
    /**
     * check positive To From
     * @param field
     * @param rules
     * @param i
     * @param options
     * @return
     */
    function positiveToFrom(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if (isNaN(field) || (parseInt(field) < rules[i+2]) || (parseInt(field) > rules[i+3]) || !isOnlyDigit(field)) {
    		return options.allrules.positive.alertTextFrom + rules[i+2]+' to '+rules[i+3];
    	}
    }
    //input positive number from 0 to 9 or -13 or -6
    function positiveST(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if (field == '#') field = -13;
		if (field == '*') field = -6;
    	if (field != -13 && field != -6 && parseInt(field) < 0 || parseInt(field) > 9  || isNaN(field) || !isOnlyDigit(field)){
        		return options.allrules.positive.alertTextST;
    	}
    }
    //check input all digit only from 1 to 9
    function isOnlyDigit(field){
    	for(i=0;i<field.length;i++)
    	{
    		if(field[i] < "0" || field[i] > "9"){
    			return false;
    		}
    	}
    	return true;
    }
    //input positive number 
    function checkPosInt(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');	
        	if (isNaN(field) || (parseInt(field) < 0) || !isOnlyDigit(field)) {
        		return options.allrules.positive.alertText;
    	}
    }
    //input positive number or ANI word
    function checkANIPosInt(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
        if (field != "ANI" && isNaN(field) || parseInt(field) < 0) {
        		return options.allrules.positive.alertTextANI;
    	}
    }
    function checkInt(field, rules, i, options){
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	var isInteger_re     = /^\s*(\+|-)?\d+\s*$/;
    	if (field.length >0 && !isInteger_re.test(field)) {
    		return options.allrules.integer.alertText;
	}
    }
    //check at least min character expect space
    function checkMinSize(field, rules, i, options){
    	var field = field.val().replace(/^\s+|\s+$/g, '').replace('-', ''); 
    	if(field.length < rules[i+2]){
    		return options.allrules.custom.alertTextMinNum + " " +rules[i+2]+" number ";
    	}
    }
    //checkIntDiff0
    function checkIntDiff(field, rules, i, options){
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	var isInteger_re     = /^\s*(\+|-)?\d+\s*$/;
    	if (parseInt(field) == rules[i+2] || !isInteger_re.test(field)) {
    		return options.allrules.custom.alertTextDiff +" "+ rules[i+2];
	    }
    }
    //check type submit POST or GET
    function checkPostGet(field, rules, i, options){
    	var field = field.val().replace(/^\s+|\s+$/g, '').replace('-', ''); 
    	if(field != "GET" && field != "POST"){
    		return options.allrules.custom.alertTextPostGet;
    	}
    }
    //check repeat key allow only -1 or NR 
    function checkRepeatKey(field, rules, i, options){
    	var field = field.val().replace(/^\s+|\s+$/g, '');
        if (field != "-1"  &&  field.toUpperCase() != "NR" && !(field>0 && field<9) &&  field != "*" && field != "#" && field != "-6" && field != "-13") {
      		return options.allrules.custom.alertTextRepeatKey;
    	}
    }
    function positive(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if(field.length == 0){
    		return;
    	}
    	if ((rules[i+2] == -1) && (field != -1)) {
        	if (isNaN(field) || (parseInt(field) < 0)) {
        		return options.allrules.positive.alertTextOR;
        	}
    	} else {
        	if ((isNaN(field) || (parseInt(field) < 0)) && (field != -1)) {
        		// check rule skip
        		if (rules[i+2] == 1)
        			return options.allrules.positive.alertTextSkip;
        		//check rule greater than 
        		else if (rules[i+2] == 0) 
        			return options.allrules.positive.alertTextGreater+rules[i+2];
        		else 
        			return options.allrules.positive.alertText;
        	}
    	}
    }
    /*
     * check format Phone number
     *    more than 10 number : 0987654321
     *            
     */              
    function checkPhone(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if(!isOnlyDigit(field) || field.length < 10 && field.length > 0){
    		return options.allrules.custom.alertTextPhone;
    	}
    }
    /* check so dien thoai*/
    function customPhone(field, rules, i, options) {
    	var field = field.val().replace(/^\s+|\s+$/g, '');
    	if(field.length == 0){
    		return;
    	}
    	if (field.length != 10) {
    		if (field.length != 17) {
    			return options.allrules.custom.alertTextPhone;
    		}
    	
    		if (isNaN(field.substring(0,9)) || (field[10] != 'P') || isNaN(field[11]) || (field[12] != 'X') || isNaN(field.substring(13))) {
    			return options.allrules.custom.alertTextPhone;
    		}
    	} else if (isNaN(field)) {
    		return options.allrules.custom.alertTextPhone;
    	}
    }
    function customPhone2(field, rules, i, options) {
    	var field = field.val();
    	if ((field.length != 10) || isNaN(field)) {
    		return options.allrules.custom.alertTextPhone2;
    	}
    }
    
    function IsValidDate(day, month, year){
        var dateVal = month + "/" + day + "/" + year;
        var dt = new Date(dateVal);

        if (dt.getDate() != day) {
            return(false);
        }
        else if (dt.getMonth() != month - 1) {
        //this is for the purpose JavaScript starts the month from 0
            return(false);
        }
        else if (dt.getFullYear() != year) {
        	return(false);
        }
            
        return(true);
     }
    
    function GetDate(day, month, year){
        var dateVal = month + "/" + day + "/" + year;
        var dt = new Date(dateVal);

        if (dt.getDate() != day) {
            return null;
        }
        else if (dt.getMonth() != month - 1) {
        //this is for the purpose JavaScript starts the month from 0
        	return null;
        }
        else if (dt.getFullYear() != year) {
        	return null;
        }
            
        return dt;
     }
    
    
    function TrimFirst(s, chr) {
    	while(s.length > 0 && s.charAt(0) == chr) {
    	    s = s.substr(1);
    	}
		return s;
    }
    
    function IsNumber(value){
        if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
            return true;
        } else {
            return false;
        }
    }
    
    function IsDate(txtDate) {
    	var currVal = txtDate;
    	if(currVal == '')
    		return false;
      
    	//Declare Regex  
    	var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
    	var dtArray = currVal.match(rxDatePattern); // is format OK?

    	if (dtArray == null)
    		return false;
     
		//Checks for mm/dd/yyyy format.
		dtMonth = dtArray[1];
		dtDay= dtArray[3];
		dtYear = dtArray[5];
		
		if (dtMonth < 1 || dtMonth > 12)
			return false;
		else if (dtDay < 1 || dtDay> 31)
			return false;
		else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
			return false;
		else if (dtMonth == 2) {
			var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
			if (dtDay> 29 || (dtDay ==29 && !isleap))
				return false;
		}
		return true;
    }

