function ValidateInput(Input)
{
	regex = new RegExp("^[A-Za-z0-9. _*%$@!?+-]*[#]{0,1}[A-Za-z0-9. _*%$@!?+-]+$");
	return Input.match(regex);
}

function ValidateEmail(email)
{
	regex = new RegExp("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$");	//Standard Email RegEx includes characters ._%+-
	//regex = new RegExp("^[A-Za-z0-9][A-Za-z._%+-]*@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$"); //More Strict Email RegEx. 
	//Restricts characters ._%-+ from being in the beginning of the word
		
	return email.match(regex);
}

//Validates a 3 part phone number
function ValidatePhone3v(PhoneArea,PhoneFirst,PhoneEnd)
{
	
	regex = new RegExp("^[1-9][0-9]{2}$");
	regex2 = new RegExp("^[0-9]{4}$");
	return (PhoneArea.match(regex) && PhoneFirst.match(regex) && PhoneEnd.match(regex2));
}

//Validates a 10 digit phone string with or with out spaces
function ValidatePhone10s(Phone)
{
	regex = new RegExp("^[1-9][0-9]{2}[1-9][0-9]{6}$"); //Standard 10 digit string phone number with no separators
	//regex = new RegExp("^[0-9]{3}.[0-9]{3}.[0-9]{4}$"); //10 digit String with any kind of separator; space, *,.,comma,&,#, etc.
	//regex = new RegExp("^[0-9]{3}[-][0-9]{3}[-][0-9]{4}$"); //Modified separator to look for only "-" separators.
	
	return Phone.match(regex);
}

function ValidatePasswordBig(Pass)
{
	regex = new RegExp("^[A-Za-z0-9][A-Za-z0-9._!$?]{4,44}$");

	return Pass.match(regex);
}

function ValidatePassword(Pass)
{
	regex = new RegExp("^[A-Za-z0-9][A-Za-z0-9._!$?]{7,14}$");

	return Pass.match(regex);
}

function textCounter(field,cntfield,maxlimit) {
	if (field.val().length > maxlimit) // if too long...trim it!
		field.val(field.val().substring(0, maxlimit)  );
	// otherwise, update 'characters left' counter
	else
		cntfield.val((maxlimit - field.val().length).toString()) ;
}
