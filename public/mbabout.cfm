
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About Us - How it works</title>

<cfinclude template="paths.cfm" >
<cfinclude template="header.cfm">

<cfoutput>
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
</cfoutput>
        

<style>
		
</style>

<!--- Comparison Chart CSS3 --->
<style>

.table {overflow-x: auto;}
table.pricing {width:auto;}
table.pricing th {padding: 10px 0; color: #999; font: 300 1.154em sans-serif; text-align: center; width:125px;}
table.pricing strong {color: #3f3f3f; font-size: 12px;}
table.pricing sup {position: relative; top: -0.5em; color: #3f3f3f; font-size: 1.2em;}
table.pricing td {color: #3f3f3f; text-align: center; line-height:25px;}
table.pricing td:first-child {color: #999; text-align: left;}
table.pricing td:nth-child(2n+2) {background: #f7f7f7;}
table.pricing tr {margin-bottom: 5px; }
table.pricing tr.action td {padding: 20px 10px; border-bottom-width: 2px;}
table.pricing tr.action td:first-child a {padding-left: 20px; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/more.png") 0 50% no-repeat; color: #3f3f3f;}
table.pricing tr.action td:first-child a:hover {color: #ff8400;}
table.pricing span.yes {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/msg-success.png") 50% 50% no-repeat; text-indent: -50em;}
table.pricing span.no {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/no.png") 50% 50% no-repeat; text-indent: -50em;}


.tooltiptext{
	display: none;
}
.showToolTip{
	cursor:pointer;
	
}

div.hide-info{
	float:left;
	margin: 0 5px 0 0;
	line-height:25px;
}

div.hide-info:hover{
	opacity:.8;
}

.info-block{
	background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
    left: 100px;
    padding: 6px;
    position: relative;
    top: -10px;
}


</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Welcome</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
                	The people and technology that built the #BrandShort# business was born from experience gained building over a hundred thousand marketing and messaging campaigns for Fortune 500 companies. 
                </div>
                
                <img style="position:absolute; top:120px; right:200px; " src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm-trans.png" width="145" height="50" />
              
            </div>
        </div>
        
       
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>About Us</h2>
                        <p>For SMB and the Enterprise</p>
                    </header>
                </div>    
                                 

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;">                
               
					<p>#BrandShort# is located in Newport Beach, CA. We have been in business since the year 1996. #BrandShort# has products and services that target both small to medium sized companies as well as the largest enterprise and government organizations.</p>
                    <BR />
                    <p>Seeing a need, based on experience fulfilling messaging needs within corporate organizations, #BrandShort# was started to serve the business world. Many Established brands (some of the biggest in the world) as well as new startups find our tools and services indispensible to their business goals. Even large telecomm companies that specialize in communications (and provide voice and SMS services already) find it more cost effective to use #BrandShort# for all of their messaging needs vs building out their own dedicated infrastructure and employee base. </p>

        		</div>
        
        	</div>
        
        </div>
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                        <h2>Proven Technology</h2>
                        <p>Fully vetted in real world conditions</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:auto; margin-top:0px;">  
                
                	Fully vetted in real world conditions, #BrandShort# is not just an idea but an accomplished goal. Over a billion customized, interactive, and relevant messages have already been successfully delivered. As of September of 2014, #BrandShort# provides monthly volumes of:

					<ul>
                        <li>30 Million SMS</li>
                        <li>30 Million Voice Calls</li>
                        <li>10 Million emails</li>
                        <li>1 million count mix of Print, Fax, and assorted other legacy channels still served</li>
					</ul>

					<h3>The possibilities are limitless. Our backend fulfillment devices offer highly customizable capabilities and are message format agnostic. Messages can be tailored to a company specific set of rules and structure that lets your brand excel, surpass and stand out. </h3>
                
                </div>
                
                         
       		</div>
        
        </div>   
        
        <div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omni-channel engagement
                
            </div>
        </div>    
              
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                        <h2>Enterprise architecture</h2>
                        <p>complex analysis of business structure and processes</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:auto; margin-top:0px;">  
                	
                    <p>#Brandshort# was built using well-defined practices for conducting enterprise analysis, design, planning, and implementation, using a holistic approach at all times. Using proprietary structures, load balanced web services in a private cloud, distributed database systems, and designs based on real world experience, #BrandShort# systems scale well against several key characteristics.</p>
                
                	<BR />
	                <BR />
                
               		<p><b>Scalable Technology</b> </p>
                    <BR />
                    <p><b>Administrative scalability:</b> The ability for an increasing number of organizations or users to easily share a single distributed system.</p>
                    <BR />
                    <p><b>Functional scalability:</b> The ability to enhance the system by adding new functionality at minimal effort.</p>
                    <BR />
                    <p><b>Geographic scalability:</b> The ability to maintain performance, usefulness, or usability regardless of expansion from concentration in a local area to a more distributed geographic pattern.</p>
                    <BR />
                    <p><b>Load scalability:</b> The ability for a distributed system to easily expand and contract its resource pool to accommodate heavier or lighter loads or number of inputs. Alternatively, the ease with which a system or component can be modified, added, or removed, to accommodate changing load.</p>
                    <BR />
                    <p><b>Generation scalability</b> refers to the ability of a system to scale up by using new generations of components. Thereby, heterogeneous scalability is the ability to use the components from different vendors.[4]</p>

                </div>
                
                         
       		</div>
        
        </div>   
                
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                        <h2>Do more with less</h2>
                        <p>Over 5:1 more work per employee</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:auto; margin-top:0px;">  
                
                	<p>Do 5:1 or more work per employee than similar sized messaging companies. 20 people can do the same scale of business that other companies need 100+ employees to accomplish.</p>

					<p>Go direct. Your other messaging providers may already be using our APIs or other EAI tools.</p>	
                </div>
                
                         
       		</div>
        
        </div>   
        
         <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                        <h2>Secure</h2>
                        <p>Enterprise Class Security</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:auto; margin-top:0px;">  
                
                	<ul>
                        <li>HIPPA Compliant</li>
                        <li>PCI Compliant</li>
                        <li>QSA certified yearly</li>
                        <li>The bigger customers even bring in their own security auditors annually.</li>
					</ul>

                </div>
                         
       		</div>
        
        </div>   
        
                        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                        <h2>Location</h2>
                        <p>4685 MacArthur Ct STE##250 Newport Beach, CA 92660</p>
                    </header>
                </div>    
                
                <div class="hiw_Info" style="width:auto; margin-top:0px;">  
                
                
                </div>
                
                         
       		</div>
        
        </div>                   
          
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>











































