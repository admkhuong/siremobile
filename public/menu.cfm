<cfparam name="LevelMap" default="">

<!--- Cludge fix for IE7 and IE9 compatability mode--->
<cfinclude template="BrowserSniff.cfm">

<ul id="nav-one" class="dropmenu <cfif FindNoCase("MSIE 7.0", browserDetect()) GT 0 >gradient7fix</cfif>"> 
			<li> 
				<a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>home.cfm">Home</a> 
			</li> 
	<!---		<li> 
				<a href="#">Solutions</a> 
				<ul> 
					<li><a href="#">Support</a></li> 
					<li><a href="#">Help</a></li> 
					<li><a href="#">Examples</a></li>
					<li><a href="#">Feeds</a></li>
                    <li><a href="#"><cfoutput>#browserDetect()#</cfoutput></a></li>
                    
				</ul> 
			</li>
	 --->
			<li> 
				<a href="#item3">Platform</a> 
				<ul> 
					<li><a href="#">Tools</a></li> 
					<li><a href="#">Services</a></li> 
					<li><a href="#">Custom projects</a></li> 
				</ul> 
			</li>  
            
           	<li> 
				<a href="#item3">Pricing</a> 
				<ul> 
	                <li><a href="#">Overview</a></li> 
					<li><a href="#">Voice</a></li> 
					<li><a href="#">eMail</a></li> 
					<li><a href="#">SMS</a></li> 
                    <li><a href="#">Direct Mail</a></li> 
                    <li><a href="#">Social media</a></li> 
                    <li><a href="#">FAX</a></li> 
				</ul> 
			</li>  
			<li> 
				<a href="#">Products</a> 
				<div class="products">
					<ul> 
				<!---		<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Customer Preference Portal</h2><p><a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>tools/cpp/about_cpp.cfm">More information about the CPP</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Mod Rewriter</h2><p><a href="#">More information about this product</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Byte Scrambler</h2><p><a href="#">More information about this product</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Image Processor</h2><p><a href="#">More information about this product</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Registry Class</h2><p><a href="#">More information about this product</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Data Validation</h2><p><a href="#">More information about this product</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="Thumb" border="0" /><h2>Ajax Tables</h2><p><a href="#">More information about this product</a></p></li> 
		--->			</ul>
					<div class="small">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</div>
				</div>
			</li>
			<li> 
				<a href="#">Services</a> 
				<div class="tutorials">
					<ul class="left"> 
						<li><a href="#">Javascript</a></li> 
						<li><a href="#">Python</a></li> 
						<li><a href="#">PHP</a></li> 
					</ul>
					<ul class="right"> 
						<li><a href="#">HTML/CSS</a></li> 
						<li><a href="#">ASP.NET</a></li> 
						<li><a href="#">Actionscript</a></li> 
					</ul>
					<div class="small">View <a href="#">all categories</a> or a <a href="#">list of the best tutorials</a>.</div>
				</div>
			</li>
			<!---<li> 
				<a href="#">Tools</a> 
				<ul> 
					<li><a href="#">Programming</a></li> 
					<li><a href="#">Inspiration</a></li> 
					<li><a href="#">My websites</a></li> 
					<li><a href="#">Clients</a></li> 
					<li><a href="#">Cool stuff</a></li> 
					<li><a href="#">Sitebase</a></li> 
					<li><a href="#">Other</a></li> 
				</ul> 
			</li>--->
			<li> 
				
		
		<!---	<a href="#">Login</a> 
				<div class="login">
					<label for="txtuser">Username: </label>
					<input type="text" name="txtuser" id="txtuser" />
					<label for="txtuser">Password: </label>
					<input type="password" name="txtpass" id="txtpass" />
					<button>Login</button>
				</div>
				
		--->
		        <a href="#">Demos</a> 
        		<div class="EBMDemos">
        
			        <div class="small">See us in action: These sites are built using the advanced <cfoutput>#BrandShort#</cfoutput> technology. </div>
	        
		        	<ul> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/BabbleBuddy64x64Web.png" width="40" height="40" alt="BabbleSphere" border="0" /><h2>Featured</h2><p><a href="http://www.babblesphere.com" target="_TechDemos">BabbleSphere</a></p></li> 
						<li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/PASP1.png" width="40" height="40" alt="P.A.S.P." border="0" /><h2>Simplified Cloud Service</h2><p><a href="http://pasp.messagebroadcast.com" target="_TechDemos">Public Application Service Portal</a></p></li> 
                        <li><img src="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/</cfoutput>images/techdemos/cliktocall.png" width="40" height="40" alt="ClikToCall" border="0" /><h2>Web Connect App</h2><p><a href="http://ctc.messagebroadcast.com/index.cfm" target="_TechDemos">User requests call from knowlegeable expert.</a></p></li> 
					</ul>
                </div>
                    
			</li>            
            <li> 
				<a href="#item3">About Us</a> 
				<ul> 
					<li><a href="#">Our Story</a></li> 
					<li><a href="#">Contact Us</a></li> 
				</ul> 
			</li>  
            
            <li style="float:right;" id="SignInTopLink"> 
				<!---<a href="<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/home2.cfm</cfoutput>">Log In</a>---> 		
                <a href="#">Log In</a> 
			</li>  
            
		</ul> 
                
                
<!---
<ul id="nav-one" class="dropmenu mright <cfif FindNoCase("MSIE 7.0", browserDetect()) GT 0 >gradient7fix</cfif>"> 
	
            
            <li class="mright"> 
				<a href="#item3">Log In</a> 				
			</li>  
            
		</ul> --->
