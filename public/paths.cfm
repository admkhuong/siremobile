<cfsetting showdebugoutput="no">

	<cfif cgi.server_port_secure eq '1'>
         <cfset LocalProtocol="https">
     <cfelse>
          <cfset LocalProtocol="http">
     </cfif>

	<cfset LocalPort = "">
<cfset stepUndoRedo = 30 >
<cfset chartReportingType = 3> <!---1 for fusion chart, 2 for highchart,3 for amChart--->

<cfset serverSocket = "#LocalProtocol#://localhost">
<cfset serverSocketPort = "2014">
<cfset serverSocketOn = 1>
<cfset clientTimeEndSession = 5400>

<!--- setup max time record (second) --->
<cfset emsMaxRecordDuration = 180>

<!--- Default branding file--->
<cfset BrandingFile = "branding/ebmbrand.cfm">

<!--- Default URLS --->
<cfset BrandingAbout = "info_about">
<cfset BrandingCareer = "info_career">
<cfset BrandingCompliance = "info_compliance">
<cfset BrandingTerms = "info_terms">
<cfset BrandingPrivacy = "info_privacy">
<cfset BrandingPrivacy = "hiw_pricing">

<cfset BrandingLOLink = "" />

<!--- Default user start date --->
<cfset _UserStartDate = "2016-06-01"/>

<!---<cfset BrandingFile = "branding/mbbrand.cfm">--->

<!--- Paypal REST Integration --->
<cfset PayPalRESTURL = "https://api.sandbox.paypal.com" />
<cfset PayPalClientId = "" />
<cfset PayPalSecretKey = "" />
<cfset EBMAPIPath = "" />


<!--- Email Whitelisting service defaults --->
<cfset DefaultgetUserEmailSubaccountInfo = StructNew() />
<cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "" />
<cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "" />
<cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "" />


<cfif FINDNOCASE( "ebmdev1.seta", "#CGI.SERVER_NAME#") GT 0 >

	<cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

    <cfset AAUDomain = "#LocalProtocol#://hau.seta.com">
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

	<cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="session"> <!--- used to find CFC inside of CFAJAX proxies --->
	<cfset LocalAauDotPath="hau"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.GENERALALERTSDBSOURCE" default="GlobalAlertsDB"/>

    <cfparam name="Session.ManagementCFCPath" default="management.cfc">
    <cfparam name="Session.SessionCFCPath" default="session.cfc">
	<cfparam name="Session.RulesCFCPath" default="session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
	<!---
    <cfparam name="PRIVATEKEY" default="6LeDYOUSAAAAANT0ZpLiCAatO-u18_0c1uppNOjF"/>
	<cfparam name="PUBLICKEY" default="6LeDYOUSAAAAAJ8jswuvjUzCEQoQ8EIoZu0Qon1q"/>
    --->
    <cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/>
    <cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>

	<cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-12"/>

	<!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

    <!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/ebmdevseta'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%ebmdevseta%2F'>
	<cfset APPLICATION.Facebook_AppID = '146749318786991'>
	<cfset APPLICATION.Facebook_AppSecret = 'a15cba3df2f8f1500f6b46245f4df45d'>

   <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />



    <cfset APPLICATION.Google_ClientId = '1045269019562.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "C:\BBP\simplexscripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
	<cfset FFMPEGAudiopath = "C:\CDs\testsox\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\DEV3.TELESPEECH\\devjlp\\EBM_DEV\\Log">

    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {
            application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";
        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

    <!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "/applications/coldfusion9/wwwroot/devjlp/PhoneListUploads">
	<cfset PhoneListDBLocalWritePath = "/applications/coldfusion9/wwwroot/devjlp/PhoneListUploads">

	<cfset CppPdfExportWritePath = "C://CppPdfExport">

	<cfset previewSurveyAudioWritePath = "D://TTS">

	<cfset CppRedirectDomain = '#LocalProtocol#://cppseta.com'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" />
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "" />

<cfelseif FINDNOCASE( "setacinq.com.vn", "#CGI.SERVER_NAME#") GT 0 OR FINDNOCASE( "mb.setatest.com.vn", "#CGI.SERVER_NAME#") GT 0>

	<cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

	<cfset serverSocket = "#LocalProtocol#://hau.setacinq.com.vn">
	<cfset serverSocketPort = "2014">
    <cfset serverSocketOn = 1>
    <cfset clientTimeEndSession = 5400>

	<cfset AAUDomain = "#LocalProtocol#://hau.setacinq.com.vn:8080">
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.GENERALALERTSDBSOURCE" default="GlobalAlertsDB"/>

    <cfparam name="Session.ManagementCFCPath" default="management.cfc">
    <cfparam name="Session.SessionCFCPath" default="session.cfc">
	<cfparam name="Session.RulesCFCPath" default="session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
    <!---
	<cfparam name="PRIVATEKEY" default="6Lc8Ev8SAAAAAFGWCagD8b6lA2y7jUOnpL8kikP2"/>
	<cfparam name="PUBLICKEY" default="6Lc8Ev8SAAAAADNs10gHpMdpLKal2Hayyn2F11Fp"/>
    --->
    <cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/>
    <cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>

	<cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-12"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

    <!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/contactpp'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%contactpp%2F'>
	<cfset APPLICATION.Facebook_AppID = '309014592511192'>
	<cfset APPLICATION.Facebook_AppSecret = '8b6eaf13f5d922efaad3525e47115b99'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />




    <cfset APPLICATION.Google_ClientId = '698138944340.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "C:\BBP\simplexscripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
	<cfset FFMPEGAudiopath = "C:\CDs\testsox\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\DEV3.TELESPEECH\\devjlp\\EBM_DEV\\Log">

    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {
            application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";
        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

	<!--- Path to where main upload DB Server is remotely located--->
	<cfset PhoneListUploadLocalWritePath = "C://PhoneListUploads">
	<cfset PhoneListDBLocalWritePath = "C://PhoneListUploads">
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

	<cfset previewSurveyAudioWritePath = "D:\TTS">
	<cfset CppRedirectDomain = '#LocalProtocol#://cpp.us.setacinq.com.vn'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
	<cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" />
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "" />

<!---<cfelseif FINDNOCASE( "DEV.TELESPEECH.COM", "#CGI.SERVER_NAME#") GT 0>

	<cfset PublicPath = "devjlp/ebmdevii/public">
    <cfset SessionPath = "devjlp/ebmdevii/session">
	<cfset ManagementPath = "devjlp/ebmdevii/management">

    <cfset AAUDomain = "#LocalProtocol#://dev.telespeech.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://www.babblesphere.com/babblesphere">

    <cfset LocalServerDirPath = "devjlp/ebmdevii/">
    <cfset LocalDotPath="devjlp.ebmdevii"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="devjlp.ebmdevii.Session"> <!--- used to find CFC inside of CFAJAX proxies --->
	<cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
	<cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.GENERALALERTSDBSOURCE" default="GlobalAlertsDB"/>

    <cfparam name="Session.ManagementCFCPath" default="DEVJLP.ebmdevii.Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="DEVJLP.ebmdevii.Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="DEVJLP.ebmdevii.Session.Rules.cfc">
    <cfset CFCPATH = "DevJLP.ebmdevii.session.cfc">
	<cfset cfcmonkehTweet = "DEVJLP.ebmdevii.Public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-12"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

    <!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/ebmcppdev'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%ebmcppdev%2F'>
	<cfset APPLICATION.Facebook_AppID = '239247492841662'>
	<cfset APPLICATION.Facebook_AppSecret = '4f8fa297f4e01960028db41912ceb48c'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />

	<cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "D:\BBP\simplexscripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
	<cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\DEV3.TELESPEECH\\devjlp\\ebmdevii\\Log">

    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {
            application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";
        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

	<!--- Path to where main upload DB Server is remotely located--->
	<cfset PhoneListUploadLocalWritePath = "\\10.0.1.114\PhoneListUploads">
	<cfset PhoneListDBLocalWritePath = "\\\\10.0.1.114\\PhoneListUploads">
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "D:\TTS">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://contactpreferenceportal.com'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dot#LocalProtocol#://y">--->

<cfelseif FINDNOCASE( "EBMManagement.mb", "#CGI.SERVER_NAME#") GT 0 OR FINDNOCASE( "EBMMO.MESSAGEBROADCAST.COM", "#CGI.SERVER_NAME#") GT 0 OR FINDNOCASE( "EBMAPI-L.MESSAGEBROADCAST.COM", "#CGI.SERVER_NAME#") GT 0>

    <cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

    <cfset AAUDomain = "#LocalProtocol#://ebm.messagebroadcast.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.GENERALALERTSDBSOURCE" default="GlobalAlertsDB"/>

    <cfparam name="Session.ManagementCFCPath" default="Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="Session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
	<cfparam name="PRIVATEKEY" default=""/>
	<cfparam name="PUBLICKEY" default=""/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "Public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-14"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

	<!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/EBMMESS'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F'>
	<cfset APPLICATION.Facebook_AppID = '413297832034676'>
	<cfset APPLICATION.Facebook_AppSecret = '55b461c58e5f647e5ccc294c99c7fc4c'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />

    <cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "\\10.11.0.80\SimpleXScripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
	<cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\EBMWebsites\\Log">

    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {
            application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";
        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

    <!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "\\10.26.0.40\PhoneListUploads">
	<cfset PhoneListDBLocalWritePath = "\\\\10.26.0.40\\PhoneListUploads">
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "D:\TTS">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://dev.ebmui.com/cpp'>
    <cfset CppRedirectDomainPublic = '#LocalProtocol#://siremobile.com/cpp'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset serverSocket = "#LocalProtocol#://ebms.messagebroadcast.com ">
	<cfset serverSocketPort = "80">
    <cfset serverSocketOn = 0>

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <!--- Paypal REST Integration --->
    <cfset PayPalRESTURL = "https://api.paypal.com" />
    <cfset PayPalClientId = "AQYk3xAFH-CjllG-uKGvsqDyMuWAe95ZBfRwF7KBC1b0TshyoxRE7hRTsBxz" />
    <cfset PayPalSecretKey = "EGyajhCW4B6_8Pr-GtObck92S5POyHAQop_EKlz2q785txuR0sZLe2Vh1u_Q" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" />
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

	<cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "" />



<cfelseif FINDNOCASE( "EBMUI.COM", "#CGI.SERVER_NAME#") GT 0>


	 <cfif cgi.server_port_secure NEQ '1'>

		<!--- Non -LB force SSL --->
   		<cfset local.url = (
            "https://" &
            cgi.server_name &
            cgi.script_name &
            "?" &
            cgi.query_string
            ) />

        <!--- Redirect the use to the target connection. --->
        <cflocation
            url="#local.url#"
            addtoken="false"
            />
     </cfif>


    <cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

    <cfset AAUDomain = "#LocalProtocol#://ebmii.messagebroadcast.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="Session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

    <cfparam name="Session.ManagementCFCPath" default="Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="Session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
    <!---
	<cfparam name="PRIVATEKEY" default="6LekKwATAAAAADUMEvLa6dulNpxdXvjo_Tahd9Yq"/>
	<cfparam name="PUBLICKEY" default="6LekKwATAAAAAEijqjiEMRIZGG9h6O8NooygPzcy"/>
    --->
    <cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/>
    <cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>

    <cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "Public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-14"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

	<!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/EBMMESS'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F'>
	<cfset APPLICATION.Facebook_AppID = '413297832034676'>
	<cfset APPLICATION.Facebook_AppSecret = '55b461c58e5f647e5ccc294c99c7fc4c'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />

    <cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "\\10.11.0.80\SimpleXScripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
    <cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\EBMWebsites\\Log">

    <!--- application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";--->
    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {

        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

 	<!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "#GetTempDirectory()#">
	<cfset PhoneListDBLocalWritePath = "/var/lib/mysql/upload"> <!--- no trailing slash - no blanks - The SFTP command will scp the file here on upload --->
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

    <!--- Linux DB SFTP site for bulk insert files to DB --->
	<cfset DBSFTPHostName = "10.25.0.200">  <!--- Needs to be DNS for failover --->
	<cfset DBSFTPUserName = "ebmsshremote">
    <cfset DBSFTPPassword = "mbW1110!S">
    <!--- To retrieve from console of logged in session ->  ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub--->
    <cfset DBSFTPFingerPrint = "89:a8:2d:11:42:3b:1f:9e:59:f5:7e:27:0b:19:6a:bc">  <!--- Needs to be copied to all failover DBs by Network Admins --->
    <cfset DBSFTPOutDir = "/var/lib/mysql/upload/">  <!--- must end in / if not blank - The SFTP command will scp the file here on upload - The SFTP path is relative to the SFTP login and is usually differnt then the DB Local path used for LOAD FILE --->

	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "C:/Temp_script/tts">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://dev.ebmui.com/cpp'>
    <cfset CppRedirectDomainPublic = '#LocalProtocol#://siremobile.com/cpp'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset serverSocket = "http://localhost">
	<cfset serverSocketPort = "2014">
    <cfset serverSocketOn = 0>

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <cfset PayPalRESTURL = "https://api.paypal.com" />
    <cfset PayPalClientId = "AQYk3xAFH-CjllG-uKGvsqDyMuWAe95ZBfRwF7KBC1b0TshyoxRE7hRTsBxz" />
    <cfset PayPalSecretKey = "EGyajhCW4B6_8Pr-GtObck92S5POyHAQop_EKlz2q785txuR0sZLe2Vh1u_Q" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" /> <!--- <cfset EBMAPIPath = "ebmapi.com" /> --->
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "ebm02" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "6QMx9GqWSLY" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "smtp.sendgrid.net" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "587" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "1" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "0" />

<!--- new dev 20151013 --->
<cfelseif 	FINDNOCASE( "sire.", "#CGI.SERVER_NAME#") GT 0 OR
			FINDNOCASE( ".seta.vn", "#CGI.SERVER_NAME#") GT 0 OR
			FINDNOCASE( "dev.ebmui.com", "#CGI.SERVER_NAME#") GT 0 OR
			FINDNOCASE( "dev.siremobile.com", "#CGI.SERVER_NAME#") GT 0 OR
			FINDNOCASE( ".seta-international.com.vn", "#CGI.SERVER_NAME#") GT 0 OR
            FINDNOCASE( ".seta-international.ie", "#CGI.SERVER_NAME#") GT 0
            >
<!---  AND FINDNOCASE( "management", "#CGI.CF_TEMPLATE#") EQ 0--->
		<!--- FINDNOCASE( "cronjob", "#CGI.SERVER_NAME#") LE 0 AND  --->

	 <cfif FINDNOCASE( "recurring", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "campaign.cfc", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "send_sms_invitation.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "send_email_invitation.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND cgi.server_port_secure NEQ '1' AND FINDNOCASE( "check_connection_timeout.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "BlastToList", "#CGI.REQUEST_URL#") LE 0 AND FINDNOCASE( "act_sire_insertcontactqueue.cfm", "#CGI.REQUEST_URL#") LE 0
        AND FINDNOCASE( "send_email_waitlist.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_insert_csv_contact_queue.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_insert_csv_contact_to_list.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_clean_csv.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "cronjob_delete_csv_contact.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_NOC_checks.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_release_user_data.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_insufficient_credit_notify.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_NOC", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_notify_renew_plan", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_web_alert_now.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_email_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_sms_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_insert_individual_contact_to_list.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_delete_preaccount.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_threshold_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_commission_by_periods.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_notify_event_change_status.cfm", "#CGI.SCRIPT_NAME#") LE 0
    >

		<!--- Non -LB force SSL --->
   		<cfset local.url = (
            "https://" &
            cgi.server_name &
            ReplaceNoCase(cgi.script_name, ".cfm", "", "all") &
            "?" &
            cgi.query_string
            ) />

        <!--- Redirect the use to the target connection. --->
        <cflocation
            url="#local.url#"
            addtoken="false"
            />
     </cfif>

    <cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

    <cfset AAUDomain = "#LocalProtocol#://ebmii.messagebroadcast.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="Session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

    <cfparam name="Session.ManagementCFCPath" default="Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="Session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
	<cfparam name="SITE_VERIFY_URL" default="https://www.google.com/recaptcha/api/siteverify"/>
	<cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/>
	<cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>
    <cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "Public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-14"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

	<!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/EBMMESS'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F'>
	<cfset APPLICATION.Facebook_AppID = '413297832034676'>
	<cfset APPLICATION.Facebook_AppSecret = '55b461c58e5f647e5ccc294c99c7fc4c'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />

    <cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "\\10.11.0.80\SimpleXScripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
    <cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\EBMWebsites\\Log">

    <!--- application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";--->
    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {

        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

 	<!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "#GetTempDirectory()#">
	<cfset PhoneListDBLocalWritePath = "/var/lib/mysql/upload"> <!--- no trailing slash - no blanks - The SFTP command will scp the file here on upload --->
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

    <!--- Linux DB SFTP site for bulk insert files to DB --->
	<cfset DBSFTPHostName = "10.25.0.200">  <!--- Needs to be DNS for failover --->
	<cfset DBSFTPUserName = "ebmsshremote">
    <cfset DBSFTPPassword = "mbW1110!S">
    <!--- To retrieve from console of logged in session ->  ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub--->
    <cfset DBSFTPFingerPrint = "89:a8:2d:11:42:3b:1f:9e:59:f5:7e:27:0b:19:6a:bc">  <!--- Needs to be copied to all failover DBs by Network Admins --->
    <cfset DBSFTPOutDir = "/var/lib/mysql/upload/">  <!--- must end in / if not blank - The SFTP command will scp the file here on upload - The SFTP path is relative to the SFTP login and is usually differnt then the DB Local path used for LOAD FILE --->

	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "C:/Temp_script/tts">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://dev.ebmui.com/cpp'>
    <cfset CppRedirectDomainPublic = '#LocalProtocol#://siremobile.com/cpp'>


	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset serverSocket = "http://localhost">
	<cfset serverSocketPort = "2014">
    <cfset serverSocketOn = 0>

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <cfset PayPalRESTURL = "https://api.paypal.com" />
    <cfset PayPalClientId = "AQYk3xAFH-CjllG-uKGvsqDyMuWAe95ZBfRwF7KBC1b0TshyoxRE7hRTsBxz" />
    <cfset PayPalSecretKey = "EGyajhCW4B6_8Pr-GtObck92S5POyHAQop_EKlz2q785txuR0sZLe2Vh1u_Q" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" /> <!--- <cfset EBMAPIPath = "ebmapi.com" /> --->
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "ebm02" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "6QMx9GqWSLY" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "smtp.sendgrid.net" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "587" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "1" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "0" />

<cfelseif FINDNOCASE( "SIRE.MOBI", "#CGI.SERVER_NAME#") GT 0 OR FINDNOCASE( "SIREMOBILE.COM", "#CGI.SERVER_NAME#") GT 0>

	 <cfif FINDNOCASE( "recurring", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "campaign.cfc", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "send_sms_invitation.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "send_email_invitation.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND cgi.server_port_secure NEQ '1' AND FINDNOCASE( "BlastToList", "#CGI.REQUEST_URL#") LE 0
        AND FINDNOCASE( "check_connection_timeout.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_insertcontactqueue.cfm", "#CGI.REQUEST_URL#") LE 0
        AND FINDNOCASE( "send_email_waitlist.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_insert_csv_contact_queue.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_insert_csv_contact_to_list.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_clean_csv.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "cronjob_delete_csv_contact.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_NOC_checks.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_release_user_data.cfm", "#CGI.SCRIPT_NAME#") LE 0 AND FINDNOCASE( "act_sire_insufficient_credit_notify.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_NOC", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_notify_renew_plan", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_web_alert_now.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_email_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_send_sms_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_insert_individual_contact_to_list.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_delete_preaccount.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_threshold_alert.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_commission_by_periods.cfm", "#CGI.SCRIPT_NAME#") LE 0
        AND FINDNOCASE( "act_sire_notify_event_change_status.cfm", "#CGI.SCRIPT_NAME#") LE 0
     >

        <cfset scriptName = cgi.script_name>

        <cfif cgi.script_name EQ '/index.cfm'>
            <cfset scriptName = '/' />
        </cfif>

		<!--- Non -LB force SSL --->
   	    <cfset local.url = (
            "https://" &
            cgi.server_name &
            ReplaceNoCase(scriptName, ".cfm", "", "all") &
            "?" &
            cgi.query_string
            ) />

        <!--- Redirect the use to the target connection. --->
        <cflocation
            url="#local.url#"
            addtoken="false"
            />
     </cfif>


    <cfset BrandingFile = "branding/sirebrand.cfm">
    <cfset BrandingAbout = "sireabout.cfm">

    <cfset PublicPath = "public">
    <cfset SessionPath = "session">
	<cfset ManagementPath = "management">

    <cfset AAUDomain = "#LocalProtocol#://ebmii.messagebroadcast.com/hau"> <!--- Please change on your server--->
	<cfset AAUPublicPath = "public">

    <cfset BabbleSphereDomain = "#LocalProtocol#://babblesphere.seta.com/babblesphere">

    <cfset LocalServerDirPath = "">
    <cfset LocalDotPath=""> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset LocalSessionDotPath="Session"> <!--- used to find CFC inside of CFAJAX proxies --->
    <cfset CommonDotPath = "session.cfc"> <!--- Different for cfincludes for cfc's in Session, WebService, EBMResponse, etc --->

    <cfset DBSourceEBM = "Bishop"/>
    <cfset DBSourceEBMReportsRealTime = "EBMReportsRealTime"/>
    <cfset DBSourceEBMReportsArchive = "EBMReportsArchive"/>
    <cfset Session.DBSourceEBM = "Bishop"/>
    <cfset DBSourceMain = "Bishop"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>

    <cfparam name="Session.ManagementCFCPath" default="Management.cfc">
    <cfparam name="Session.SessionCFCPath" default="Session.cfc">
	<cfparam name="Session.RulesCFCPath" default="Session.Rules.cfc">

	<cfparam name="CHALLENGE_URL" default="http://www.google.com/recaptcha/api"/>
	<cfparam name="SSL_CHALLENGE_URL" default="https://www.google.com/recaptcha/api"/>
	<cfparam name="VERIFY_URL" default="http://www.google.com/recaptcha/api/verify"/>
	<cfparam name="SITE_VERIFY_URL" default="https://www.google.com/recaptcha/api/siteverify"/>
    <!---
	<cfparam name="PRIVATEKEY" default="6LekKwATAAAAADUMEvLa6dulNpxdXvjo_Tahd9Yq"/>
	<cfparam name="PUBLICKEY" default="6LekKwATAAAAAEijqjiEMRIZGG9h6O8NooygPzcy"/>
    --->
    <cfparam name="PRIVATEKEY" default="6Lf2VhATAAAAAB9-hZi0gesLGf9UMT-KmPxXn_R3"/>
    <cfparam name="PUBLICKEY" default="6Lf2VhATAAAAABpoIA53pWBxHw3A-72kfKCM7oYP"/>

    <cfparam name="AGENT_TYPE" default="11"/>
	<cfparam name="RULE_ENCRYPT_PASS" default="Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString"/>

    <cfset CFCPATH = "session.cfc">
	<cfset cfcmonkehTweet = "Public.monkehTweet.com.coldfumonkeh.monkehTweet" />

	<!--- Used for allowing extractor to pull from the same RXDialer accross multiple web sites - must be unique for each program / web site--->
	<cfset ESIID = "-14"/>

    <!--- Default for Clik For The Expert --->
	<cfset CFTEBatchId = "218">
    <cfset CFTEServiceCode = "CFTE1143!pw">
    <cfset CFTELocation = "9494284899">
    <cfset CFTEInitialCID = "9494283111">

	<!--- Application wide key used to encrypt sensitive information in the client cookies --->
	<cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
    <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />
    <cfset APPLICATION.SaltOne = "lsdijghue32985D6dFG$$2vmni">
    <cfset APPLICATION.SaltTwo = "FDGDSfdgdsf456*7897sdCVbbbcxvb">
    <cfset APPLICATION.sessions = 0 />
    <cfset APPLICATION.PPUidKey = 54321 />

	<cfset APPLICATION.Facebook_URL = 'http://apps.facebook.com/EBMMESS'>
	<cfset APPLICATION.Facebook_URL_PARAM = 'http%3A%2F%2Fapps.facebook.com%EBMMESS%2F'>
	<cfset APPLICATION.Facebook_AppID = '413297832034676'>
	<cfset APPLICATION.Facebook_AppSecret = '55b461c58e5f647e5ccc294c99c7fc4c'>

    <cfset APPLICATION.Twitter_AccessToken = '560895419-9wyrJzYixVqlT9FRqBBqk4dcrlXYYYjPRz44Ep1b' />
	<cfset APPLICATION.Twitter_AccessTokenSecret = 'ubt2e0hTU0Pkhv2P40pQWVyIdS8iR2lro6N1piZY' />
	<cfset APPLICATION.Twitter_Consumerkey = 'fRr4gZTPJ8qNJzSn19YpvQ' />
	<cfset APPLICATION.Twitter_Consumersecret = 'w11SX0lmAcG5NPsS9dmYxY7w13Q5D7WO8t0CMDZemY' />

    <cfset APPLICATION.Google_ClientId = '716059018891-g897kunt3fa8ft46u1cbfd5njjc9hg0j.apps.googleusercontent.com' />

    <!--- Path Structure to files
    rxdsLocalWritePath\U#Session.USERID#\L#inpLibId#\E#inpEleId#\rxds_#Session.USERID#_#inpLibId#_#inpEleId#_#inpDataId#.wav
    --->

    <!--- Use UNC paths for directory exists checks - shared path wont work for some reason --->
    <cfset rxdsLocalWritePath = "\\10.11.0.80\SimpleXScripts">
    <cfset rxdsLegacyReadPath = "\\10.0.1.10\DynaScript">
    <cfset rxdsWebProcessingPath = "C:\Temp_script">
    <cfset RRLocalFilePath = "\\10.11.0.80\SimpleXScripts\ResponseReview">
    <cfset SphinxConfigPath = "C:/cds/transcriber9">

    <!--- Note: you will needd to add to the current RXDialer \\inpDialerIPAddr --->
    <!--- Note: Current RXDialers all should have a shared path DynamicLibraries --->
    <cfset rxdsRemoteRXDialerPath = "RXWaveFiles\DynamicLibraries">

    <!---Path to Audio conversion executable location --->
    <cfset SOXAudiopath = "C:\cds\sox2\sox.exe">
    <cfset FFMPEGAudiopath = "C:\CDs\ffmpeg.exe" />
    <cfset WindowsCMDPath = "C:\windows\system32\cmd.exe">

    <!---<cfset validExtensions = "CD,WAV,MP3,WMA,OGG,AAC,FLAC,MPEG-4,AIFF,AU,VOX,RAW,WavPack,ADPCM,ALAW,ULAW,ALAC">--->
    <cfset validExtensions = "WAV,MP3">
    <cfset fileSizeLimitInMB = 3 >

    <!--- Folder to save log file --->
    <cfset LogFolderPath = "C:\\EBMWebsites\\Log">

    <!--- application.baseUrl = "#application.baseUrl#:#CGI.SERVER_PORT#";--->
    <cfscript>
        application.datasource = "#Session.DBSourceEBM#";
        application.database = "rxds";
        application.User_database = "simpleobjects.useraccount";
        application.baseDir = "C:\Temp_script";

        application.mp3.channels = 1; // 2 || 1
        application.mp3.bitRates = 32; // 128 || 64 || 32
        application.mp3.sampleRates = 44100; // 44100 || 22050 || 11025
        application.mp3.volume = 0; // (db)


        application.soxPath = "#SOXAudiopath#";
        //application.soxPath = "C:\cds\sox2\sox.exe";
        application.soxMaxExeTime = 90;
        //application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels# -v #application.mp3.volume#";
        application.soxDefaultArguments = "-r #application.mp3.sampleRates# -C #application.mp3.bitRates# -c #application.mp3.channels#";
        application.soxDefaultEffects = "gain -n #application.mp3.volume#";

        application.baseUrl = "#LocalProtocol#://#CGI.SERVER_NAME#";
        if (CGI.SERVER_PORT NEQ 80) {

        }
        application.baseUrl = "#application.baseUrl#/#SessionPath#/rxds/";
        application.flashApiBaseUrl = "#application.baseUrl#flash/";
        application.scriptPath = "#rxdsLocalWritePath#";
        //application.scriptPath = "C:\Temp_script";
    </cfscript>

 	<!--- Path to where main upload DB Server is remotely located--->
    <cfset PhoneListUploadLocalWritePath = "#GetTempDirectory()#">
	<cfset PhoneListDBLocalWritePath = "/var/lib/mysql/upload"> <!--- no trailing slash - no blanks - The SFTP command will scp the file here on upload --->
	<cfset CppPdfExportWritePath = "C://CppPdfExport">

    <!--- Linux DB SFTP site for bulk insert files to DB --->
	<cfset DBSFTPHostName = "10.25.0.200">  <!--- Needs to be DNS for failover --->
	<cfset DBSFTPUserName = "ebmsshremote">
    <cfset DBSFTPPassword = "mbW1110!S">
    <!--- To retrieve from console of logged in session ->  ssh-keygen -l -f /etc/ssh/ssh_host_rsa_key.pub--->
    <cfset DBSFTPFingerPrint = "89:a8:2d:11:42:3b:1f:9e:59:f5:7e:27:0b:19:6a:bc">  <!--- Needs to be copied to all failover DBs by Network Admins --->
    <cfset DBSFTPOutDir = "/var/lib/mysql/upload/">  <!--- must end in / if not blank - The SFTP command will scp the file here on upload - The SFTP path is relative to the SFTP login and is usually differnt then the DB Local path used for LOAD FILE --->

	<!--- This path is for saving audio TTS, please change for your server. Make sure that folder has permission to write.------->
	<cfset previewSurveyAudioWritePath = "C:/Temp_script/tts">
	<!--- Please this cpp redirect domain if it's incorrect----->
	<cfset CppRedirectDomain = '#LocalProtocol#://dev.ebmui.com/cpp'>
    <cfset CppRedirectDomainPublic = '#LocalProtocol#://siremobile.com/cpp'>

	<cfset surveyDotNetAssemblyPath = "C:\ColdFusion9\wwwroot\WEB-INF\cfclasses\dotNetProxy">

    <cfset serverSocket = "http://localhost">
	<cfset serverSocketPort = "2014">
    <cfset serverSocketOn = 0>

    <cfset SupportEMailFromNoReply = "noreply@eventbasedmessaging.com" />
    <cfset SupportEMailFrom = "support@siremobile.com" />
    <cfset SupportEMailServer = "smtp.gmail.com" />
    <cfset SupportEMailUserName = "support@siremobile.com" />
    <cfset SupportEMailPassword = "SF927$tyir3" />
    <cfset SupportEMailPort = "465" />
    <cfset SupportEMailUseSSL = "true" />

    <cfset PayPalRESTURL = "https://api.paypal.com" />
    <cfset PayPalClientId = "AQYk3xAFH-CjllG-uKGvsqDyMuWAe95ZBfRwF7KBC1b0TshyoxRE7hRTsBxz" />
    <cfset PayPalSecretKey = "EGyajhCW4B6_8Pr-GtObck92S5POyHAQop_EKlz2q785txuR0sZLe2Vh1u_Q" />

    <cfset EBMAPIPath = "#CGI.SERVER_NAME#" /> <!--- <cfset EBMAPIPath = "ebmapi.com" /> --->
    <cfset EBMUIPath = "#CGI.SERVER_NAME#" />

    <cfset DefaultgetUserEmailSubaccountInfo.UserName_vch = "ebm02" />
	<cfset DefaultgetUserEmailSubaccountInfo.Password_vch = "6QMx9GqWSLY" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemoteAddress_vch = "smtp.sendgrid.net" />
    <cfset DefaultgetUserEmailSubaccountInfo.RemotePort_vch = "587" />
    <cfset DefaultgetUserEmailSubaccountInfo.TLSFlag_ti = "1" />
    <cfset DefaultgetUserEmailSubaccountInfo.SSLFlag_ti = "0" />

</cfif>

<cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME##LocalPort#">

<cfif FINDNOCASE( "setacinq.com.vn", "#CGI.SERVER_NAME#") GT 0>
	<cfif CGI.SERVER_PORT NEQ 80>
		<cfif CGI.SERVER_PORT EQ 444>
			<cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:443">
		<cfelse>
			<cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#CGI.SERVER_PORT#">
	    </cfif>
	<cfelse>
		<cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#">
	</cfif>
	<cfif FINDNOCASE( "ebmdocker.setacinq.com.vn", "#CGI.SERVER_NAME#") GT 0>
		<cfset LocalPort = "81">
		<cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#LocalPort#">
	</cfif>
</cfif>

<cfif FINDNOCASE( "sire.mobi", "#CGI.SERVER_NAME#") GT 0>
    <cfif CGI.SERVER_PORT NEQ 80>
        <cfif CGI.SERVER_PORT EQ 444>
            <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:443">
        <cfelse>
            <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#CGI.SERVER_PORT#">
        </cfif>
    <cfelse>
        <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#">
    </cfif>
    <cfif FINDNOCASE( "ebmdocker.setacinq.com.vn", "#CGI.SERVER_NAME#") GT 0>
        <cfset LocalPort = "81">
        <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#LocalPort#">
    </cfif>
</cfif>

<cfif FINDNOCASE( "siremobile.com", "#CGI.SERVER_NAME#") GT 0>
    <cfif CGI.SERVER_PORT NEQ 80>
        <cfif CGI.SERVER_PORT EQ 444>
            <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:443">
        <cfelse>
            <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#CGI.SERVER_PORT#">
        </cfif>
    <cfelse>
        <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#">
    </cfif>
    <cfif FINDNOCASE( "ebmdocker.setacinq.com.vn", "#CGI.SERVER_NAME#") GT 0>
        <cfset LocalPort = "81">
        <cfset rootUrl="#LocalProtocol#://#CGI.SERVER_NAME#:#LocalPort#">
    </cfif>
</cfif>

<!--- Allow modification of site for different brands --->
<cfinclude template="#BrandingFile#" />


<!--- URL API (only site siremobile.com) --->

<cfif LCase(CGI.SERVER_NAME) EQ 'siremobile.com' OR LCase(CGI.SERVER_NAME) EQ 'www.siremobile.com' >
<!---
<cfif LCase(CGI.SERVER_NAME) EQ 'awsqa.siremobile.com'>
--->
	<cfset rootUrlAPI="https://api.siremobile.com">
<cfelse>
	<cfset rootUrlAPI=rootUrl/>
</cfif>
