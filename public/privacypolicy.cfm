<cfparam name="LevelMap" default="">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><cfoutput>#BrandFull#</cfoutput> - Privacy Policy</title>




<style>
	@import url('css/ebm.css');
	@import url('css/ebmmenu.css');


</style>

<script type="text/javascript" src="../js/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jqueryddmenu.js"></script>



<script type="text/javascript">
	
	
	$(function() {	
	
			$("#nav-one").dropmenu({closeSpeed: 100});	
			
	});

	

</script>



</head>

<!---
Todos


--->


<body>

    <div id="TopMenu">
    	<cfinclude template="menu.cfm">
    </div>
    
    <div id="Spacer1">    
 
    </div>
    
    
    <div id="Section1">
    
    </div>
    
    <div id="LTContent">
    	<div style="float:left;">
    		<img src="images/EBMTriSwoosh105x103_WithShadowWeb.png" />    
        </div>
        
        <div style="float:right; vertical-align:central;">
        	<h1 class="grey inline">Event Based</h1><h1 class="gold inline">Messaging</h1>
        </div>    
    </div>

    
<div id="InteractiveGraphic"></div>
    
    <div id="PrivacyPolicy" class="TextContent">
    
	    <!--content-rht end--><p></p>
        <h1>Privacy policy</h1>
        
        <h3>The <cfoutput>#BrandFull#</cfoutput> commitment to privacy</h3>
        Your privacy is important to us. To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice easy to find, we make it available on our Contact page.
      
      	<BR />
        <BR />
      
        <h3>The information we collect</h3>
        This notice applies to all information collected or submitted on the <cfoutput>#BrandFull#</cfoutput> Contact Page. The type of personal information collected is listed below:<br>
        <ul>
            <li>First Name</li>
            <li>Last Name</li>
            <li>Company</li>
            <li>Title</li>
            <li>Email address</li>
            <li>Phone number</li>
        </ul>
        
        <BR />
        <BR />
        
        <h3>The way we use information:</h3>
        We use the information you submit on the <cfoutput>#BrandFull#</cfoutput> contact page to provide you with more information about our services or to have a sales representative contact you and/or send you correspondence about <cfoutput>#BrandFull#</cfoutput>'s services. We do not share this information with outside parties.<p></p>
        <p>&nbsp;</p>
        <p>We use return email addresses to answer the email we receive. Such addresses are not used for any other purpose and are not shared with outside parties. </p>
        <p>&nbsp;</p>
        <p>Finally, we never use or share the personally identifiable information provided to us online in ways unrelated to the ones described above.</p>
        <p>&nbsp;</p>
        <p>If you would like <cfoutput>#BrandFull#</cfoutput> to remove any personally identifiable information from our database, please contact us at 888-430-1876 or send us an email at info@<cfoutput>#BrandFull#</cfoutput>.com.
        </p>
        
        <BR />
        <BR />
        
        <h3>Our commitment to data security</h3>
        To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.
        
        <BR />
        <BR />
        
        <h3>How to contact us</h3>
        Should you have other questions or concerns about these privacy policies, please call us toll free at 888-430-1876 or send us an email at support@EventBasedMessaging.com. <p></p>
        <p>&nbsp;</p>
        <p>Changes to the <cfoutput>#BrandFull#</cfoutput> Privacy Policy </p>
        <p>&nbsp;</p>
        <p><cfoutput>#BrandFull#</cfoutput> reserves the right to change or modify this policy at any time. Any such changes will be reflected on this page.
        </p>
        
        <BR />
        <BR />

    
    
    </div>


    
    <div id="Section2">
    
</div>
    
    
    <div id="Footer">
	    <cfset ShowCounter = 0>
    	<cfinclude template="#LevelMap#footer.cfm">   	       
    </div>
    


</body>
</html>