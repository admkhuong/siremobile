<cfparam name="posted" default="0"/>
<cfparam name="inpeMailAddress" default=""/>


<cfset RecoverOut = "" />

<cfif posted EQ "1" AND trim(inpeMailAddress) NEQ "">

	<!--- Do recovery options now --->
    
    <!--- Limit attempts per session?--->
    
    <!--- Log sources of the attempts --->
    
    <cfsavecontent variable="RecoverOut" >
    	<cfoutput>
	    	Do recovery options now .... 
        </cfoutput>
	</cfsavecontent>    

</cfif>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
		
        <cfinclude template="paths.cfm" >
        <title><cfoutput>#BrandFull# Password Recovery</cfoutput></title>
		
		<script type="text/javascript">
		    window.history.forward();
		    function noBack() { window.history.forward(); }
		</script>
		<cfoutput>
			<style type="text/css">
				@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');
				@import url('#rootUrl#/#PublicPath#/css/rxform2.css');
				@import url('#rootUrl#/#PublicPath#/css/MB.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
				@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
			</style>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
			<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
		</cfoutput>
	</head>
	<body>
		<div>
			<script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}
				});
				
			</script>
		</div>
		<div id="BSContentHome">
	    	<div align="center">
		    	<div class="content_container">
					<div class="content" id="signup_box">
						<form action="recovery" name="sign_up" method="post" id="sign_up">
                        
                        	<input type="hidden" name="posted" id="posted" value="1" />
                            
							<div class='inp_box'>
								<label class='main_title title'><h2><span>Enter your email Address for this account</span></h2></label>
								<input name="inpeMailAddress" id="inpeMailAddress" type="text" 
										value="someone@somewhere.com">
															
									<BR>							
								
								<div class='inp_box'>
									<button  
										type="submit" 
										class="ui-corner-all"											
									>Recover</button>
								</div>
							</div>
						</form>
					</div>
                    
                    <BR />
                    
                    <div>
                    	<cfoutput>
                        	#RecoverOut#
                        </cfoutput>
                    </div>
				</div>
			</div>
		</div>
	</body>
</html>