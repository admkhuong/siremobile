<cfsetting showdebugoutput="no">

<cfinclude template="../../public/paths.cfm" >
<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
<cfparam name="DBSourceEBM" default="#Session.DBSourceEBM#"/> 


<cfinclude template="../paths.cfm">  
<cfparam name="scrId" default="0_0_0_0" />

<!--- Details - allow force download of content instead of playing in browser--->
<cfparam name="fd" default="0" />

<cfset outFileName = "RecordedResponse_#scrID#.mp3">
<cfset CURRETAG = "">


<cfparam name="Session.USERID" default="">
<cfset ListenUserId = 0>


<cfif Session.USERID GT 0>
	<cfset ListenUserId = Session.USERID>
</cfif>

<!--- Share audio file from BabbleSphere --->
<cfif isDefined("scrId")>
	<cfset ids = ListToArray(scrId, "_") />
	<cfset inpUserId = ids[1] /> 
	<cfset inpBatchId = ids[2] /> 
	<cfset inpCDLId = ids[3] />
    <cfset inpTFLId = ids[4] />
	       
    <cfquery name="GetDateFromTFL" datasource="#DBSourceEBM#">
        SELECT
        	RXCDLStartTime_dt
        FROM     
            simplexrecordedresults.transcriptionfilelog       
        WHERE 
            TFLId_int=#inpTFLId#                        
    </cfquery>
    
    <cfif GetDateFromTFL.RecordCount GT 0>
		<cfset inpDate = DateFormat(GetDateFromTFL.RXCDLStartTime_dt,'yyyy_mm_dd')>    
            
        <cfset MainLibFile = "#RRLocalFilePath#\RecordedResponses\U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#inpTFLId#.mp3" >
     
        <cfset CURRETAG = "#inpUserId#_#inpBatchId#_#inpDate#_#inpCDLId#_#inpTFLId#:0">
	</cfif>
    	
<!--- Other --->
<cfelse>
	<cfset MainLibFile = "#rxdsLocalWritePath#\u999\babble.mp3" />
</cfif>

<cfif isDefined("MainLibFile")>
        
    <!--- Update view counts--->
    <cftry>
    
        <cfquery name="updateViewCount" datasource="#DBSourceEBM#">
            UPDATE 
                simplexrecordedresults.transcriptionfilelog
            SET 
                viewcount_int = viewcount_int+1
            WHERE 
                TFLId_int=#inpTFLId#                        
        </cfquery>
		
    <cfcatch type="any">
    
    </cfcatch>
    
    </cftry>

	<cftry>

		<cfif FileExists(MainLibFile)>
        
			<!--- support for IOS and Android--->
            <cfinclude template="rangeDownloadMP3.cfm">

            <cfset CurrfileInfo = GetFileInfo(MainLibFile)>
               
            <cfheader name="Last-Modified" value="Fri, 06 May 2011 19:08:04 GMT"><!--- Use file meta-data here --->
            <cfheader name="ETag" value="#hash(MainLibFile, 'MD5')#">
            <cfheader name="Content-Location" value="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/share/act_getrecordedresponse.cfm?scrId=#scrId#">
            
            <cfif fd GT 0>
	            <cfheader name="Content-disposition" value="attachment; filename=#outFileName#">
            </cfif>
            
            <cfif structKeyExists(GetHttpRequestData().headers, 'Range')>
                <cfset rangeDownloadMP3(MainLibFile)>
            <cfelse>
				<!---<cfscript>
                    context = getPageContext();
                    context.setFlushOutput(false);
                    response = context.getResponse().getResponse();
                    response.setContentType("audio/mpeg");
                    response.setContentLength(arrayLen(theData));
                    
                    out = response.getOutputStream();
                    out.write(theData);
                    out.flush();  
                    out.close();
					                </cfscript>--->
                
                <cfset CurrfileInfo = GetFileInfo(MainLibFile)>
                <cfheader name="accept-ranges" value="off" />
                <cfheader name="content-length" value="#CurrfileInfo.size#" />            
                <cfheader name="content-range" value="bytes 0-#CurrfileInfo.size - 1#/#CurrfileInfo.size#" />
                <cfheader name="Content-Type" value="audio/mpeg">
                <cfheader name="Expires" value="#LSDateFormat(DateAdd('d', 1, Now()), 'mmm dd,yyyy')# #LSTimeFormat(DateAdd('d', 1, Now()), 'h:mm tt')#">            
                <cfheader name="last-modified" value="#LSDateFormat(DateAdd('d', -1, Now()), 'mmm dd,yyyy')# #LSTimeFormat(DateAdd('d', -1, Now()), 'h:mm tt')#" />            
                <cfheader name="ETag" value="#CURRETAG#" />
                <cfcontent TYPE="audio/mpeg" file="#MainLibFile#" reset="yes" deletefile="no"/>
            
            </cfif>

	    </cfif>
    
    <!---
		Note: You'll need to double up on the double quotes within the CFHEADER tag to properly escape them.

		The HTTP 1.1 specification actually calls for the filename value to be quoted, but many browsers will accept 
		basic filenames without the quotes without an error. Quoting the filename may help the newer (mobile) browsers 
		to better understand what you're asking for here.
	--->    		
   
		<cfcatch>
        	<!---<cfoutput>#cfcatch.Detail# #cfcatch.Message#</cfoutput>--->
		</cfcatch>
	</cftry>
</cfif>