<cfsetting showdebugoutput="no">

<cfinclude template="../../public/paths.cfm" >
<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
<cfparam name="DBSourceEBM" default="#Session.DBSourceEBM#"/> 

<cfinclude template="../paths.cfm">  
<cfparam name="scrId" default="0_0_0_0" />

<!--- Details - allow force download of content instead of playing in browser--->
<cfparam name="fd" default="0" />

<cfset outFileName = "RecordedResponse_#scrID#.mp3">
<cfset CURRETAG = "">


<cfparam name="Session.USERID" default="">
<cfset ListenUserId = 0>


<cfif Session.USERID GT 0>
	<cfset ListenUserId = Session.USERID>
</cfif>

<!--- Share audio file from BabbleSphere --->
<cfif isDefined("scrId")>
	<cfset ids = ListToArray(scrId, "_") />
	<cfset inpUserId = ids[1] /> 
	<cfset inpBatchId = ids[2] /> 
	<cfset inpCDLId = ids[3] />
    <cfset inpTFLId = ids[4] />
	       
    <cfquery name="GetDateFromTFL" datasource="#DBSourceEBM#">
        SELECT
        	RXCDLStartTime_dt
        FROM     
            simplexrecordedresults.transcriptionfilelog       
        WHERE 
            TFLId_int=#inpTFLId#                        
    </cfquery>
    
    <cfif GetDateFromTFL.RecordCount GT 0>
		<cfset inpDate = DateFormat(GetDateFromTFL.RXCDLStartTime_dt,'yyyy_mm_dd')>    
            
        <cfset MainLibFile = "#RRLocalFilePath#\RecordedResponses\U#inpUserId#\#inpBatchId#\#inpDate#\#inpCDLId#_#inpTFLId#.png" >
     
        <cfset CURRETAG = "#inpUserId#_#inpBatchId#_#inpDate#_#inpCDLId#_#inpTFLId#.png">
	</cfif>
    	
<!--- Other --->
<cfelse>
	<cfset MainLibFile = "" />
</cfif>

<cfif isDefined("MainLibFile")>
    
  
	<cftry>

		<cfif FileExists(MainLibFile)>
        			
            <cfimage action="read" source="#MainLibFile#" name="CURRETAG">
            
            <cfcontent type="image/jpg" variable="#imageGetBlob(CURRETAG)#">
     
        </cfif>
        
		<cfcatch>
        	<cfoutput>#cfcatch.Detail# #cfcatch.Message#</cfoutput>
		</cfcatch>
	</cftry>
</cfif>