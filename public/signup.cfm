<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Stuff to include in the head section of any public site document --->
	<cfinclude template="home7assets/inc/header.cfm" >

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Sign Up - <cfoutput>#BrandShort#</cfoutput></title>

	<!-- CSS -->
	<link href="home7assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/simple-line-icons.css" rel="stylesheet" media="screen">
	<link href="home7assets/css/animate.css" rel="stylesheet">
        
	<!-- Custom styles CSS -->
	<link href="home7assets/css/style.css" rel="stylesheet" media="screen">
    
    <script src="home7assets/js/modernizr.custom.js"></script>
    
    <script src="home7assets/js/jquery-1.11.1.min.js"></script>
      
    <script src="home7assets/js/jquery.magnific-popup.min.js"></script>  
    <link href="home7assets/css/magnific-popup.css" rel="stylesheet">
    
    <!--- Legacy stuff - move to assets --->
    <cfoutput>
	<!---    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>--->
	    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.min.js"></script> 
    	<!---<link rel="stylesheet" type="text/css" href="#rootUrl#/#PublicPath#/css/stylelogin.css">--->
                                
		<style>
	        @import url('#rootUrl#/#PublicPath#/js/jquery-ui-1.11.0.custom/jquery-ui.css') all;	
			
			
			.intro2 {
				left: 0;
				padding: 0 0;
				position: relative;
				text-align: center;
				top: 0;
				transform: none;
				width: 100%;
				margin: 20px 0;
			}
			
			.pfblock 
			{
				padding: 2em;
			}

		</style>
        
    </cfoutput>
      
      

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

        
        
<style>
			
	#signup_box .inp_box .error {
    color: red;
    display: none;
    font-size: 12px;
}
	
</style>

    <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					<!---$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}--->
					
							
				});
								
				function isValidRequiredField(fieldId){
					<!---if(fieldId == 'company_name'){
						if(!$('#account_type_company').is(':checked')){
							return true;
						}
					}--->
					if($("#" + fieldId).val() == ''){
						$("#err_" + fieldId).show();
						return false;
					}else{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				function isValidEmail(){
					var isValid = true;
					if($("#emailadd").val() == ''){
						isValid = false;
						$("#err_emailadd").show();
					}else{
						var filter = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if (!filter.test($("#emailadd").val())) {
							isValid = false;
							$("#err_emailadd").show();
						}else{
							$("#err_emailadd").hide();
						}
					}
					return isValid;
				}
				
				<!---
				function isValidAccountType() { 
					if (!$('#account_type_company').is(':checked') && !$('#account_type_personal').is(':checked')) {
				  		$('#err_type').show();
				  		return false;
				  	}
				  	else {
				  		
				  		if($('#account_type_company').is(':checked')){
				  			if(!isValidRequiredField('company_name')){
				  				$("#err_company_name").show();
				  				return false;
				  			}
				  		}else{
				  			$("#err_company_name").hide();
				  		}
				  		
				  		$('#err_type').hide();
					  	return true;
					}
				}
				--->
			<!---	
				function changeType(type){
					if(type == 'company'){
						$("#company_box").show();
					}else{
						$("#company_box").hide();
						$("#err_company_name").hide();
					}
					return;
				}
				--->
				
				<!---// validate pass and confirm pass--->
				function isValidPassword(){
					var isvalid = true;
					$("#validateno1").css("color","#424242");
					$("#validateno2").css("color","#424242");
					$("#validateno3").css("color","#424242");
					$("#validateno4").css("color","#424242");
					$("#validateno5").css("color","#424242");
					$("#err_inpConfirmPassword2").css("display","none");
					
					if ($("#inpPasswordSignup").val().length < 8)
					{
						
						//jAlert("Password Validation Failure", 'Password must be at least 8 characters in length');
						$("#validateno1").css("color","red");
						isvalid = false;
					 					  
					}
					  
					var hasUpperCase = /[A-Z]/.test($("#inpPasswordSignup").val());
					if (!hasUpperCase)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 uppercase letter');
						$("#validateno3").css("color","red");
						isvalid = false;
					 					  
					}
										
					var hasLowerCase = /[a-z]/.test($("#inpPasswordSignup").val());
					if (!hasLowerCase)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 lowercase letter');
						$("#validateno4").css("color","red");
						isvalid = false;
					 					  
					}
					
					var hasNumbers = /\d/.test($("#inpPasswordSignup").val());
					if (!hasNumbers)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 number');
						$("#validateno2").css("color","red");
						isvalid = false;
					 					  
					}
										
					var hasNonalphas = /\W/.test($("#inpPasswordSignup").val());
					if (!hasNonalphas)
					{
						
						//jAlert("Password Validation Failure", 'Password must have at least 1 special character');
						$("#validateno5").css("color","red");
						isvalid = false;
					 					  
					}
					
					
					if(!isValidRequiredField('inpPasswordSignup')){
		  				isvalid = false;
		  			}
		  			if(!isValidRequiredField('inpConfirmPassword')){
		  				isvalid = false;
		  			}
		  			if($("#inpPasswordSignup").val() != $("#inpConfirmPassword").val()){
						$("#err_inpConfirmPassword2").css("display","block");
		  				isvalid = false;
		  			}
					return isvalid;
				}
				<!---// validate form--->
				function isValidSignupForm(){
					var isValidForm = true;
					<!---<!---// account type--->
					if(!isValidAccountType()){
						isValidForm = false;
					}--->
					<!---// name--->
					if(!isValidRequiredField('fname')){
		  				isValidForm = false;
		  			}
					if(!isValidRequiredField('lname')){
		  				isValidForm = false;
		  			}
		  			<!---// email--->
		  			if(!isValidEmail()){
		  				isValidForm = false;
		  			}
		  			<!---// password--->
		  			if(!isValidPassword()){
		  				isValidForm = false;
		  			}
					return isValidForm;
				}
				<!---// Signup--->
				function signUp(){
					
					$('#SignUpButton').hide();			
					$("#loadingSignUp").css('visibility', 'visible');
			
					
					if(!isValidPassword())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					
					if(!isValidSignupForm())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					else
					{
						<!---// input--->
						var accountType = 'personal';
						var companyName = '';
						
						
						<!---	if($('#account_type_company').is(':checked')){
							accountType = 'company';
							companyName = $("#company_name").val();
						}else{
							accountType = 'personal';
						}--->
						
						
						var fname = $("#fname").val();
						var lname = $("#lname").val();
						var userEmail = $("#emailadd").val();
						
						var pass = $("#inpPasswordSignup").val();
						var confirmPass = $("#inpConfirmPassword").val();
						try{
							$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							data:  {
								INPMAINEMAIL: userEmail,
								INPCOMPANYNAME: companyName,
								INPTIMEZONE: 31,
								INPACCOUNTTYPE: accountType,
								INPNAPASSWORD: pass,
								INPFNAME: fname,
								INPLNAME: lname,
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) { $('#SignUpButton').show(); $("#loadingSignUp").css('visibility', 'hidden');},					  
							success:		
								function(d) 
								{
									if(d.RESULT != "SUCCESS"){
										jAlert(d.MESSAGE, 'Error');
										$('#SignUpButton').show();			
										$("#loadingSignUp").css('visibility', 'hidden');
										return false;
									}else{
										<!---// sign up successfull - go to the administrator page--->
										doLoginSignUp();
										<!---//$("#signup_box").html('');
										//var loginLink = '<a href="<cfoutput>#rootUrl#/#PublicPath#/home</cfoutput>">here</a>';
										//$("#signup_box").append('<h2 class="title message">Register successfully! Click ' + loginLink + ' to go login page.</h2>');--->
									}
								} 		
								
							});
						}catch(ex){
							jAlert('Signup Fail', 'Error');
							$('#SignUpButton').show();			
							$("#loadingSignUp").css('visibility', 'hidden');
							return false;
						}
					}
					return true;
				}
				
				function doLoginSignUp(){
					
						var userID = $("#emailadd").val();
						var userPass = $("#inpPasswordSignup").val();
						<!---// do login action--->
						try{
							var RememberMeLocal = 0;
							$.ajax({
						       type: "POST",
						       url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
						       data:{
						       		inpUserID : userID, 
									inpPassword : userPass, 
									inpRememberMe : RememberMeLocal
						       },
						       dataType: "json", 
						       success: function(d) {
						    	  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
																																	
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.RXRESULTCODE) != "undefined")
										{					
											CurrRXResultCode = d.RXRESULTCODE;	
											
											if(CurrRXResultCode > 0)
											{
												<!--- Navigate tp sesion Home --->
												window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/home.cfm";
											}
											else if(parseInt(CurrRXResultCode) == -2 )
											{
												jAlertOK("Error while trying to login.\n" + d.REASONMSG , "Failure!", function(result) { <!---window.location.href="verifyEmail.cfm?email="+$("#frmLogon #inpUserID").val(); return false;---> } );												
											}
											else
											{
												<!--- Unsuccessful Login --->
												<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
												if(typeof(d.DATA.REASONMSG[0]) != "undefined")
												{	 
													jAlertOK("Error while trying to login.\n" + d.REASONMSG , "Failure!");
												}		
												else
												{
													jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
												}
												
											}
										}
										else
										{<!--- Invalid structure returned --->	
											
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
								
										}
									
						       }
						  });
						}catch(ex){
							jAlertOK('Have problem while login to system.', 'Warning');
						}
					}
			</script>
</head>
            
<cfoutput>
<body class="pfblock-gray">
        
        
        <section id="" class="pfblock pfblock-gray">
                           
            <div class="intro2">
            
                
                    
                    <cfoutput>
                        <a class="" href="home"><img src="#LogoImageLink7#" style="border:none;" /></a>
                    </cfoutput>
                        
               
                            
                <h1>Event Based Messaging</h1>
                <div class="start">Tools for creating and managing modern responsive consumer communication applications</div>
                
            </div>
            
        </section>
    
    		      				
     <section id="services" class="pfblock pfblock-gray">
		<div class="container">
			<div class="row">
                     
        		<div id="inner-bg-m1" style="min-height: 450px">
        
					<div class="contentm1" id="signup_box">
						<form action="" name="sign_up" method="post" id="sign_up">
							<!---<div class='inp_box'>
								<label class='main_title title'><h2><span>1. Select Account Type</span></h2></label>
								<input name="account_type" id="account_type_personal" type="radio" 
										value="personal" checked="true" onchange="changeType('personal');">
								<span>Personal</span>
								<input name="account_type" id="account_type_company" type="radio" 
										value="company" onchange="changeType('company');">
								<span>Company</span>
								<label class='error' id='err_type'>You must choose one type of account.</label>
							</div>--->
                            
						<!---	<div class='inp_box hidden' id='company_box'>
								<label class='title'><span>Company Name</span></label><br />
								<input name="company_name" id="company_name" type="text" class="textfield half"><br />
								<label class='error' id='err_company_name'>This field is required.</label>
							</div>
						--->
                                                
							<div class='inp_box'>
								<label class='main_title title'><h2><span>Create <cfoutput>#BrandShort#</cfoutput> Account</span></h2></label>
								<div class="action_box">
									<a href="home.cfm"><span>or click here if you already have an existing account.</span></a>
								</div>
								<div class='inp_box'>
									<div class='half'>
										<label class='title'><span>First Name</span></label><br />
										<input name="fname" id="fname" type="text" 
												class="textfield"><br />
										<label class='error' id='err_fname'>This field is required.</label>
									</div>
									<div class='half padd-left'>
										<label class='title'><span>Last Name</span></label><br />
										<input name="lname" id="lname" type="text" 
												class="textfield">
										<label class='error' id='err_lname'>This field is required.</label>
									</div>
								</div>
								<div class='inp_box'>
								<label class='title'><span>Email Address</span></label><br />
								<input name="emailadd" id="emailadd" type="text" 
										class="textfield full"><br />
								<label class='error' id='err_emailadd'>Invalid Email.</label>
								</div>
								
                                <div style="clear:both"></div>
                                                                                              
                                <div style="">                               								
                                    <div class='inp_box' style="margin-top:15px;">
                                        <label class="inTitle">Password</label><br />
                                        <input type="text" name="inpPasswordSignup" id="inpPasswordSignup" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpPassword">This field is required.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;" >
                                        <label class="inTitle">Confirm Password</label><br />
                                        <input type="text" name="inpConfirmPassword" id="inpConfirmPassword" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpConfirmPassword">Invalid confirm password.</label>
										<label class="error" id="err_inpConfirmPassword2">Two passwords you typed do not match.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;">
                                        <span id="SignUpButton"> <button type="button" class="ui-corner-all" onClick="signUp(); return false;">Sign up</button></span>
                                        <div id="loadingSignUp" style="float:right; display:inline; visibility:hidden; margin-right:5px;">Adding New Account...<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                                    </div>
                               	</div>
                                 
                                <div class='inp_box' style="margin-top:30px;">
                                    <h2>Secure Password Requirements</h2>
                                        <ul style="margin-left:25px;">
                                            <li id="validateno1" class='title'>must be at least 8 characters in length</li>
                                            <li id="validateno2" class='title'>must have at least 1 number</li>
                                            <li id="validateno3" class='title'>must have at least 1 uppercase letter</li>
                                            <li id="validateno4" class='title'>must have at least 1 lower case letter</li>
                                            <li id="validateno5" class='title'>must have at least 1 special character</li>
                                        </ul>
                                    <BR />
                                   	<h4>A secure sample of one has been randomly generated for you.
                                   		<BR />
                                   		Feel free to use or replace with your own.  
                                    </h4>
                                   
                              	</div>
                                
                                
                                
							</div>
						</form>
					</div>
				</div>

			</div><!--End row -->
			
		</div>
		
    </section>
    
		<cfinclude template="footer.cfm" />
    
    
    </body>
</cfoutput>
</html>


