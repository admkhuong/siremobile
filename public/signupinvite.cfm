<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-TYPE" content="text/html; charset=utf-8" />
		<cfinclude template="paths.cfm" >
        <title><cfoutput>#BrandFull#</cfoutput></title>
		
		<script type="text/javascript">
		    window.history.forward();
		    function noBack() { window.history.forward(); }
		</script>
		<cfoutput>
			<style type="text/css">
				@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
				@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');
				@import url('#rootUrl#/#PublicPath#/css/rxform2.css');
				@import url('#rootUrl#/#PublicPath#/css/MB.css');
				@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
				@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
			</style>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
			<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
		</cfoutput>
		
		<script type="text/javascript" language="javascript">
				function isValidRequiredField(fieldId){
					if($("#" + fieldId).val() == ''){
						$("#err_" + fieldId).show();
						return false;
					}else{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				function isValidEmail(){
					var isValid = true;
					if($("#emailadd").val() == ''){
						isValid = false;
						$("#err_emailadd").show();
					}else{
						var filter = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if (!filter.test($("#emailadd").val())) {
							isValid = false;
							$("#err_emailadd").show();
						}else{
							$("#err_emailadd").hide();
						}
					}
					return isValid;
				}
				
				function changeType(type){
					if(type == 'company'){
						$("#account_type_personal").attr('checked', '');
						$("#account_type_company").attr('checked', 'checked');
						$("#company_box").show();
					}else{
						$("#account_type_company").attr('checked', '');
						$("#account_type_personal").attr('checked', 'checked');
						$("#company_box").hide();
					}
					return;
				}
				// validate pass and confirm pass
				function isValidPassword(){
					var isvalid = true;
					if(!isValidRequiredField('inpPassword')){
		  				isvalid = false;
		  			}
		  			if(!isValidRequiredField('inpConfirmPassword')){
		  				isvalid = false;
		  			}
		  			if($("#inpPassword").val() != $("#inpConfirmPassword").val()){
		  				isvalid = false;
		  			}
					return isvalid;
				}
				// validate form
				function isValidSignupForm(){
					var isValidForm = true;

					// name
					if(!isValidRequiredField('fname')){
		  				isValidForm = false;
		  			}
					if(!isValidRequiredField('lname')){
		  				isValidForm = false;
		  			}
		  			if(!isValidRequiredField('invitationCode')){
		  				isValidForm = false;
		  			}
		  			// email
		  			if(!isValidEmail()){
		  				isValidForm = false;
		  			}
		  			// password
		  			if(!isValidPassword()){
		  				isValidForm = false;
		  			}
					return isValidForm;
				}
				// Signup
				function signUp(){
					if(!isValidSignupForm()){
						return false;
					}else{
						// input
						var fname = $("#fname").val();
						var lname = $("#lname").val();
						var userEmail = $("#emailadd").val();
						var timezone = $("#inpTimeZone").val();
						var pass = $("#inpPassword").val();
						var confirmPass = $("#inpConfirmPassword").val();
						
						var invitationCode = $("#invitationCode").val();
						var department = $("#deparment").val();
						var position = $("#position").val();
						var workphone = $("#workphone").val();
						var extension = $("#extension").val();
						var cellphone = $("#cellphone").val();
						var alternatenumber = $("#alternateNumber").val();
						
						try{
							$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RegisterNewAccountToCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							data:  {
								INPMAINEMAIL: userEmail,
								INPTIMEZONE: timezone,
								INPNAPASSWORD: pass,
								INPFNAME: fname,
								INPLNAME: lname,
								INPINVITATIONCODE: invitationCode,
								INPDEPARTMENT: department,
								INPPOSITION: position,
								INPWORKPHONE: workphone,
								INPEXTENSION: extension,
								INPCELLPHONE: cellphone,
								INPALTERNATENUMBER: alternatenumber,
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
							success:		
								function(d) 
								{
									if(d.RESULT != "SUCCESS"){
										jAlert(d.MESSAGE, 'Error');
										return false;
									}else{
										doLogin();
										//jAlert('Singup success. Go to login page.', 'Information');
										//$("#signup_box").html('');
										//var loginLink = '<a href="<cfoutput>#rootUrl#/#PublicPath#/home</cfoutput>">here</a>';
										//$("#signup_box").append('<h2 class="title message">Register successfully! Click ' + loginLink + ' to go login page.</h2>');
									}
								} 		
								
							});
						}catch(ex){
							jAlert('Signup Fail', 'Error');
							return false;
						}
					}
					return true;
				}
				
				function doLogin(){
						var userID = $("#emailadd").val();
						var userPass = $("#inpPassword").val();
						// do login action
						try{
							var RememberMeLocal = 0;
							$.ajax({
						       type: "POST",
						       url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
						       data:{
						       		inpUserID : userID, 
									inpPassword : userPass, 
									inpRememberMe : RememberMeLocal
						       },
						       dataType: "json", 
						       success: function(d) {
						    	  <!--- Alert if failure --->
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{																									
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{					
											CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
											
											if(CurrRXResultCode > 0)
											{
												<!--- Navigate tp sesion Home --->
												window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/administration.cfm";
											}
											else if(parseInt(CurrRXResultCode) == -2 )
											{
												jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] , "Failure!", function(result) { window.location.href="verifyEmail.cfm?email="+$("#frmLogon #inpUserID").val(); return false; } );
												
											}
											else
											{
												<!--- Unsuccessful Login --->
												<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
												if(typeof(d.DATA.REASONMSG[0]) != "undefined")
												{	 
													jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] , "Failure!");
												}		
												else
												{
													jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
												}
												
											}
										}
										else
										{<!--- Invalid structure returned --->	
											// Do something		
											
											jAlertOK("Error while trying to login.\n" + "Invalid response from server." , "Failure!");
								
										}
									}
									else
									{<!--- No result returned --->
										// Do something		
										
										jAlertOK("Error while trying to login.\n" + "No response form remote server." , "Failure!");
									
									}
						       }
						  });
						}catch(ex){
							jAlertOK('Have problem while login to system.', 'Warning');
						}
					}
			</script>
	</head>
	<body onload="noBack();" onpageshow="if (event.persisted) noBack();">
		<div>
			 
		</div>
		<div id="BSContentHome">
	    	<div align="center">
		    	<div class="content_container">
					<div class="content" id="signup_box">
						<cfform action="" name="sign_up" method="post" id="sign_up">
							<div class='inp_box'>
								<label class='main_title title'><h2><span>Create #BrandShort# Account With Invitation Code</span></h2></label>
								<div class='inp_box'>
									<div class='half'>
										<cfif structKeyExists(URL, 'firstName')>
											<cfset firstName = URL.firstName>
										<cfelse>
											<cfset firstName = ''>
										</cfif>
										<label class='title'><span>First Name</span></label><br />
										<cfinput name="fname" id="fname" type="text" 
												 class="textfield" value="#firstName#"><br />
										<label class='error' id='err_fname'>This field is required.</label>
									</div>
									<div class='half padd-left'>
										<cfif structKeyExists(URL, 'lastName')>
											<cfset lastName = URL.lastName>
										<cfelse>
											<cfset lastName = ''>
										</cfif>
										<label class='title'><span>Last Name</span></label>
										<cfinput name="lname" id="lname" type="text" 
												 class="textfield" value="#lastName#">
										<label class='error' id='err_lname'>This field is required.</label>
									</div>
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Department</span></label><br />
								<cfinput name="deparment" id="deparment" type="text" 
										class="textfield full"><br />
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Position</span></label><br />
								<cfinput name="position" id="position" type="text" 
										class="textfield full"><br />
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Email Address</span></label><br />
								<cfif structKeyExists(URL, 'email')>
									<cfset email = URL.email>
								<cfelse>
									<cfset email = ''>
								</cfif>
								<cfinput name="emailadd" id="emailadd" type="text" 
										class="textfield full" value="#email#"><br />
								<label class='error' id='err_emailadd'>Invalid Email.</label>
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Work Phone Number</span></label><br />
								<cfinput name="workphone" id="workphone" type="text" 
										class="textfield half"><br />
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Extension</span></label><br />
								<cfinput name="extension" id="extension" type="text" 
										class="textfield half"><br />
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Cell Phone Number</span></label><br />
								<cfinput name="cellphone" id="cellphone" type="text" 
										class="textfield half"><br />
								</div>
								
								<div class='inp_box'>
								<label class='title'><span>Alternate number</span></label><br />
								<cfinput name="alternateNumber" id="alternateNumber" type="text" 
										class="textfield half"><br />
								</div>
								
								<div class='inp_box'>
									<cfset timezoneClass = CreateObject("java", "java.util.TimeZone")>
									<cfset timezoneIDs = timezoneClass.getAvailableIDs() />
									<label class='title'><span>Time Zone</span></label>
									<cfselect name="inpTimeZone" id="inpTimeZone"  class="select_box">
										<cfloop index="tzId" array="#timezoneIDs#">
											<cfset cTimezone = timezoneClass.getTimeZone(tzId)>
											<cfoutput>
												<option value="#tzId#">#cTimezone.getDisplayName()# (#tzId#)</option>
											</cfoutput>
										</cfloop>
									</cfselect>
									<label class='error' id='err_inpTimeZone'>This field is required.</label>
								</div>
								
								<div class='inp_box'>
									<label class="inTitle">Password</label><br />
									<cfinput type="password" name="inpPassword" id="inpPassword"
											 class="textfield half"><br />
									<label class="error" id="err_inpPassword">This field is required.</label>
								</div>
								
								<div class='inp_box'>
									<label class="inTitle">Confirm Password</label><br />
									<cfinput type="password" name="inpConfirmPassword" id="inpConfirmPassword"
											 class="textfield half"><br />
									<label class="error" id="err_inpConfirmPassword">Invalid confirm password.</label>
								</div>
								
								<div class='inp_box'>
									<cfif structKeyExists(URL, 'code')>
										<cfset invitationCode = URL.code>
									<cfelse>
										<cfset invitationCode = ''>
									</cfif>
									<label class='title'><span>Invitation Code</span></label><br />
									<cfinput name="invitationCode" id="invitationCode" type="text" 
											class="textfield half" value="#invitationCode#" ><br />
									<label class="error" id="err_invitationCode">This field is required.</label>
								</div>
								
								<div class='inp_box'>
									<button  
										type="button" 
										class="ui-corner-all"
										onClick="signUp(); return false;"	
									>Signup</button>
								</div>
							</div>
						</cfform>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>