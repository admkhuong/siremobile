<cfcomponent>

	<cfinclude template="/public/sire/configs/paths.cfm" >
	<cfinclude template="/public/sire/configs/version.cfm" />

	<cfset this.name = "SireMobileApplication" />  <!--- RXMBEBMDemo ->  RXSireApplication --->

	<cfset this.sessionStorage  = "sessionstorage">
	<cfset this.sessionCluster = true>
	<cfset this.mappings["/cfpayment"] = "/public/sire/models/cfc/cfpayment-master/" />

	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first CREATED.">
	     <cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
	     <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />

	     <cfset APPLICATION.sessions = 0>

	    <cfreturn true />
	</cffunction>

	<cffunction name="onRequestStart" returnType="boolean">

		<cfargument type="String" name="targetPage" required=true/>
		<cfparam name="Session.DBSourceEBM" default="bishop">
		<cfparam name="Session.DBSourceREAD" default="bishop_read">


		<!--- 0 Current session user is not an agency GT 0 means this user an Agency --->
	     <cfparam name="Session.AgencyId" default="0">
	     <cfparam name="Session.AgencyManagerId" default="0">
	     <cfparam name="Session.AgencyShortDesc" default="">

	     <!---
	          0 is regular Sire User - GT 0 Client is under specified agancy.
	          Client can only be member of one agency
	     --->
	     <cfparam name="Session.ClientOfAgencyId" default="0">

	     <!---
	          0 is not a impersonation right now
	          is this session an impersonation of a client by an agency - if so store that agency Id here
	     --->
	     <cfparam name="Session.ImpersonateClientAsAgencyId" default="0">

	     <!---
	          0 is not a impersonation right now
	          is this session an impersonation of a client by an agency - if so store the Sire UserId of the Manager for agency Id here
	     --->
	     <cfparam name="Session.ImpersonateClientAsAgencyManagerUserId" default="0">


		<cfset var RetVarLookForRememberMeCookie = '' />
		<cfset var getUser = ''/>
		<cfset var UpdateLastLoggedIn = ''/>
		<cfset var checkLogin = ''/>
		<cfset var getShortCode = ''/>

		<cfset var currRememberMe = structNew() />

		<cfset var checkActive = '' />
		<cfset var queryStringStruct	= '' />

		<cfset APPLICATION.Build_Version ="#Config_BuildVersionString#">

		<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStringStruct">
			<cfinvokeargument name="QueryString" value="#cgi.query_string#">
		</cfinvoke>

		<!--- Marketing Tracking Source Cookie --->
		<cfif structKeyExists(queryStringStruct, "utm_source")>
			<cfif structKeyExists(cookie,'utm_source') AND trim(queryStringStruct.utm_source) NEQ trim(cookie.utm_source)>
				<cfcookie   name = "utm_source" expires = "180" secure = "yes" value = "#queryStringStruct.utm_source#" encodevalue = "yes">
			<cfelseif !structKeyExists(cookie,'utm_source')>
				<cfcookie   name = "utm_source" expires = "180" secure = "yes" value = "#queryStringStruct.utm_source#" encodevalue = "yes">
			<cfelse>

			</cfif>
		</cfif>

	    <cfinvoke component="public.sire.models.cfc.userstools" method="checkLogin" returnvariable="checkLogin"></cfinvoke>

		<cfif _SiteMaintenance EQ 1 >
			<cfif structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/public/sire/pages/coming-soon.cfm')>
				<cflocation url= "/public/sire/pages/coming-soon" addToken = "false"/>
			</cfif>
		<cfelseif _SiteMaintenance EQ 2>
			<cfif structKeyExists(cgi, "script_name") AND (cgi.script_name NEQ '/public/sire/pages/scheduled-maintenance.cfm')>
				<cflocation url= "/public/sire/pages/scheduled-maintenance" addToken = "false"/>
			</cfif>
		</cfif>

		<cfif !checkLogin>

			<cfinvoke component="public.sire.models.cfc.userstools" method="LookForRememberMeCookie" returnvariable="RetVarLookForRememberMeCookie"></cfinvoke>

		</cfif>

    		<cfreturn true>

	</cffunction>

	<cffunction name="onRequestEnd" returnType="void">
	    <cfargument type="String" name="targetPage" required=true/>

	    <cfif !isDefined('variables._Response')>
			<cfset variables._PageContext = GetPageContext()>
			<!--- Coldfusion --->
			<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
			<cfset variables._Response = variables._PageContext.getOut()>
		</cfif>
		<cfset variables._page = variables._Response.getString()>
		<cfif Find('<link', variables._page) GT 0>
			<cfset variables._av = '3.60.3'>
			<cfset variables._page = REReplace(variables._page, '(<(link|script)[^>]+(href|src)=["''][^\?>"'']+)(["''])', "\1?_=#variables._av#\4", "all")>
			<cfset variables._Response.clear()>
			<cfoutput>#variables._page#</cfoutput>
		</cfif>
	</cffunction>

	<cffunction name="onError">
	   <cfargument name="Exception" required=true/>
	   <cfargument name="EventName" type="String" required=true/>

	    <!--- Log all errors. --->
	    <cfset var message = ''>
	    <cfset var request_url = ''>
	    <cfset var messageDetail = ''>
	    <cfset var request_url = ''>
	    <cfset var http_referer = ''>
	    <cfset var http_user_agent = ''>

	    <cfif isStruct(Exception)>
	    	<cfset messageDetail = serializejson(Exception)>
	    </cfif>

	    <cfif structKeyExists(Exception, "message")>
	    	<cfset message = Exception.message>
	    </cfif>

	    <cfif structKeyExists(Exception, "Detail")>
	    	<cfset message &= ' '&Exception.Detail>
	    </cfif>
	    <!---
	    <cfif structKeyExists(Exception, "queryError")>
	    	<cfset message &= ' '&Exception.queryError>
	    </cfif>
	    --->
	    <cfif structKeyExists(cgi, "request_url")>
	    	<cfset request_url = cgi.request_url>
	    </cfif>

	    <cfif structKeyExists(cgi, "http_user_agent")>
	    	<cfset http_user_agent = cgi.http_user_agent>
	    </cfif>

	    <cfif structKeyExists(cgi, "http_referer")>
	    	<cfset http_referer = cgi.http_referer>
	    </cfif>

	    <cftry>
		    <cfinvoke component="public.sire.models.cfc.error_logs" method="LogErrorAction">
		    	 <cfinvokeargument name="INPERRORMESSAGE" value="#message#">
		    	 <cfinvokeargument name="INPREQUESTURL" value="#request_url#">
		    	 <cfinvokeargument name="INPERRORDETAIL" value="#messageDetail#">
		    	 <cfinvokeargument name="INPHttpUSERAGENT" value="#http_user_agent#">
		    	 <cfinvokeargument name="INPHTTPREFERER" value="#http_referer#">
			</cfinvoke>
			<cfcatch>
			</cfcatch>
		</cftry>

		<!--- Display an error message if there is a page context. --->
		<!--- <cflocation url= "/public/sire/pages/error-page" addToken = "false"/> --->

	</cffunction>

</cfcomponent>
