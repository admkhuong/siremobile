var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');
var watch = require('gulp-watch');
var minifyCSS = require('gulp-csso');
var minifyJS = require('gulp-js-minify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var header = require('gulp-header');
var pkg = require('./package.json');

// gulp.task('serve', [], function () {
//     browserSync({
//         notify: false,
//         server: {
//             baseDir: 'sire.lc'
//         }
//     });
//     gulp.watch(['*.cfm'], reload);
//     gulp.watch(['./js/*.js'], reload);
//     gulp.watch(['./css/*.css'], reload);
// });

gulp.task('minify-css', function() {
   gulp.src([
      './css/common.css', 
      // './css/style.css', 
      // './css/learning-and-support.css'
      // './css/bootstrap-home.css', 
      // './css/scroll-down.css', 
      // './css/validationEngine.jquery.css', 
      // './css/live-demo.css',
      // './css/screen.css'
      './css/about-us.css'
      ])
      .pipe(minifyCSS())
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(gulp.dest('./css/'));
});

gulp.task('minify-js', function() {
   gulp.src([
   		// './js/jquery.validationEngine.js',
   		// './js/jquery.validationEngine.en.js',
         // './js/sign_up.js',
         // './js/sign_in.js',
         // './js/forgot_password.js',
         // './js/common.js',
         // './js/jquery.validationEngine.Functions.js',
         // './js/jquery.autogrow-textarea.js',
         // './js/live_demo.js'
         // './js/js.cookie.js'
   	])
      .pipe(minifyJS())
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(gulp.dest('./js/'));
});

// gulp.task('watch-css', function () {
//     gulp.watch('./css/**/*.css', ['minify-css']);
// });

gulp.task('build', ['minify-css']);

gulp.task('buildjs', ['minify-js']);

// gulp.task('default', ['serve', 'watch-css']);