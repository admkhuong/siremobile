function UpdateUserInfo(userInfo)
{
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/public/sire/models/cfc/userstools.cfc?method=UpdateUserInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpAddress:userInfo.address,
                    inpCity:userInfo.city,
                    inpState:userInfo.state,
                    inpZip:userInfo.zip,
                    inpSSNTAX:userInfo.taxid,
                    inpPaypalEmail:userInfo.paypalEmail
                }
            ,beforeSend:function(xhr)
                {
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                var data ={MESSAGE:"Unable to update your infomation at this time. An error occurred."};
                reject(data);
                $('#processingPayment').hide()
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}
function CreateAffiliateCode(affiliatecode){
    return new Promise( function (resolve, reject) {
        $.ajax({
            type:"POST",
            url:'/public/sire/models/cfc/affiliate.cfc?method=SaveAffiliate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
            dataType:'json',
            data:
                {					
                    inpAffiliateCode:affiliatecode						
                }
            ,beforeSend:function(xhr){
                $('#processingPayment').show()
            }
            ,error:function(XMLHttpRequest,textStatus,errorThrown){					
                $('#processingPayment').hide()
                var data ={MESSAGE:"Unable to create your Affiliate Code at this time. An error occurred."};
                reject(data);
            }
            ,success:function(data)
                {
                $('#processingPayment').hide();
                if(data.RXRESULTCODE==1){						
                    resolve(data);
                }
                else
                {
                    reject(data);
                }					
            }
        }); 			
    });
}	

function validate_form()
    {			
    if($("#md-signup_form").validationEngine('validate'))
        {
        var actionType = $("#actionType1").val() || 1; // <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
        var bbSignUpTitle="Sign Up";
        var accountType='personal';
        var fname=$("#fname1").val();
        var lname=$("#lname1").val();
        var userEmail=$("#emailadd1").val();
        var sms_number=$("#sms_number1").val();
        var pass=$("#inpPasswordSignup1").val();
        var confirmPass=$("#inpConfirmPassword1").val();
        var pt=$('#select-plan-1').val();
        var planid=$('#select-plan-1').val();

        var address= $('#address1').val() || "";
        var city= $('#city1').val() || "";
        var state= $('#state1').val() || "";
        var zip= $('#zip1').val() || "";
        var taxid= $('#taxid1').val() || "";
        var paypalEmail= $('#paypalemailadd1').val() || "";			
        
        var affiliatetype=$('#affiliatetype1').val() || "0"; 
        var affiliatecode= $('#affiliatecode1').val() || "";            
        var ami=$('#AMI1').val() || 0; // this is user id off user that we apply affiliate code (Affiliate Marketing Id)      

        try{					
                if(actionType==4){									
                    // have alr signup, only update some compulsory infor and request affiliate code
                    var userInfo = {							
                        fname:fname,
                        lname:lname,
                        userEmail:userEmail,
                        sms_number:sms_number,
                        pass:pass,							
                        address:address,
                        city:city,
                        state:state,
                        zip:zip,
                        taxid:taxid,
                        paypalEmail:paypalEmail							
                    };
                                            
                    UpdateUserInfo(userInfo).then(function(data){
                        if(data.RXRESULTCODE ==1){
                            return CreateAffiliateCode(affiliatecode);
                        }							
                    }).then(function(data){
                        if(data.RXRESULTCODE ==1){
                            bootbox.dialog({
                                message:"Congratulation! Your affiliate code was assigned.",
                                title:"Oops!",
                                buttons:{
                                    success:{
                                        label:"OK",className:"btn btn-success btn-success-custom",callback:function(){
                                            location.href="/session/sire/pages/affiliates-dashboard";
                                        }
                                    }
                                }
                            });
                        }	                        
                    }).catch(function(error){	
                                        
                        bootbox.dialog({
                            message:error.MESSAGE,title:"Oops!",
                            buttons:{
                                success:{
                                    label:"OK",className:"btn btn-success btn-success-custom",callback:function(){
                                        if(error.TYPE=="Exists")
                                        {
                                            location.href="/session/sire/pages/affiliates-dashboard";
                                        }

                                    }
                                }
                            }
                        });
                    });
                }
                else
                {
                    $.ajax({
                        type:"POST",url:'/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccountNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',dataType:'json',data:
                            {
                            INPMAINEMAIL:userEmail,INPTIMEZONE:31,INPACCOUNTTYPE:accountType,INPNAPASSWORD:pass,INPFNAME:fname,INPLNAME:lname,INSMSNUMBER:sms_number,email:userEmail,inpPlanId:planid,
                            inpAddress:address,
                            inpCity:city,
                            inpState:state,
                            inpZip:zip,
                            inpTaxId:taxid,
                            inpAffiliateCode:affiliatecode,
                            inpAffiliateType: affiliatetype,
                            inpAMI:ami,
                            inpPaypalEmail: paypalEmail
                        }
                        ,beforeSend:function(xhr)
                            {
                            $('#processingPayment').show()
                        }
                        ,error:function(XMLHttpRequest,textStatus,errorThrown)
                            {
                            $('#SignUpButton').show();
                            $('#processingPayment').hide()
                        }
                        ,success:function(d)
                            {
                            $('#processingPayment').hide();
                            if(d.RESULT!="SUCCESS")
                                {
                                bootbox.dialog(
                                    {
                                    message:d.MESSAGE,title:bbSignUpTitle,buttons:
                                        {
                                        success:
                                            {
                                            label:"OK",className:"btn btn-success btn-success-custom",callback:function()
                                                {
                                            }
                                        }
                                    }
                                }
                                );
                                return false
                            }
                            else{
                                if(actionType==2){
                                    window.location.href='thankyou?pt=1&redirect=/session/sire/pages/affiliates-dashboard'
                                }
                                else
                                {
                                    window.location.href='thankyou?pt=1'
                                }
                                
                            }
                        }
                    });
                }					
        }
        catch(ex)
            {
            console.log(ex);
            $('#processingPayment').hide();
            bootbox.dialog(
                {
                message:"Sign Up Fail",title:bbSignUpTitle,buttons:
                    {
                    success:
                        {
                        label:"OK",className:"btn btn-success btn-success-custom",callback:function()
                            {
                        }
                    }
                }
            }
            );
            btnSignUp.prop('disabled',false);
            return false
        }
    }
}
$(document).ready(function(){
    $("#sms_number1").mask("(000)000-0000");
	$("#md-signup_form").validationEngine({
		promptPosition:"topLeft",scroll:false,focusFirstField:true
	});

    $(".existing-user-affiliate").on("click",function(){
        if($(this).hasClass("affiliates-loggedIn")){
            //location.href="/signup-affiliates";
            $("#mdSignup").modal("show");
        }
        else
        {                
            bootbox.dialog(
                {
                    message:"Do you already have a Sire account?",
                    title:"Alert",
                    buttons:{
                        success:{
                            label:"YES",
                            className:"btn btn-success ",
                            callback:function(){
                                $("#signin").modal('show'); 
                                var md = new MobileDetect(window.navigator.userAgent);
		
                                if(md.phone() !== null || md.tablet() !== null){
                                    if(!$("body").hasClass('startFreeOpen1')){
                                        $("body").addClass('startFreeOpen1')
                                    }
                                }  
                            }
                        },
                        cancel: {
                            label: "No",
                            className: "btn green-cancel",
                            callback:function(){
                                $("#mdSignup").modal("show");
                                var md = new MobileDetect(window.navigator.userAgent);
		
                                if(md.phone() !== null || md.tablet() !== null){
                                    if(!$("body").hasClass('startFreeOpen1')){
                                        $("body").addClass('startFreeOpen1')
                                    }
                                }
                            }
                        },
                    }
                }
            );        
            
            
        }
    });
    $("#btn-new-signup").on('click',function(event)
		{
            event.preventDefault();
            validate_form()
	    }
    );
    
    $('#mdSignup').on('shown.bs.modal', function (e) {
        $(window).trigger("resize");
    });    

    $('.modal')
    .on('show.bs.modal', function (){
        var md = new MobileDetect(window.navigator.userAgent);
    
        if(md.phone() !== null || md.tablet() !== null){
            if(!$("body").hasClass('startFreeOpen1')){
                $("body").addClass('startFreeOpen1')
            }
        }
    })
    .on('hide.bs.modal', function (){
        var md = new MobileDetect(window.navigator.userAgent);
    
        if(md.phone() !== null || md.tablet() !== null){
            if($("body").hasClass('startFreeOpen1')){
                $("body").removeClass('startFreeOpen1')
            }
        }
    });

});