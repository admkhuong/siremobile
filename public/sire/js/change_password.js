(function($){
	$("#change_password").validationEngine({promptPosition : "topLeft", scroll: false});

	$("#change_password").validationEngine(
	{
		promptPosition:"topLeft",scroll:false,focusFirstField:true
	});
	$("#btnChangepassword").on('click',function(event)
	{
		event.preventDefault();
		validate_form()
	});
	function validate_form(){			
		if($("#change_password").validationEngine('validate'))
		{			 						
			try{	
				var url_change_password = '/public/sire/models/cfc/userstools.cfc?method=ChangePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach';
				var message_change_password = $('#change_password .change-password-message').text('');				
				$.ajax({
					method: 'POST',
					url: url_change_password,
					data: $("#change_password").serialize(),
					beforeSend: function( xhr ) {
						$('#processingPayment').show();
					},	
					success: function(DATA) {						
						$('#processingPayment').hide();
						if (DATA && DATA.RXRESULTCODE === 1) {
							$("#bootstrapAlert .modal-title").text('Request success!');
							$("#bootstrapAlert .alert-message").text('Your password has been reset successfully!');
							$("#bootstrapAlert").modal('show');
							$("#bootstrapAlert").on('hidden.bs.modal', function(){
								location.href = '/';
							});
						} else {							
							message_change_password.text(DATA.MESSAGE);
						}
					}
				});
			}
			catch(ex)
				{				
				$('#processingPayment').hide();				
				return false
			}
		}
	}
	// $('#change_password').submit(function(event){
	// 	event.preventDefault();		
	// 	if ($(this).validationEngine('attach', {focusFirstField : true})) {
	// 		var url_change_password = '/public/sire/models/cfc/userstools.cfc?method=ChangePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocach';
	// 		var message_change_password = $('#change_password .change-password-message').text('');
	// 		$.ajax({
	// 			method: 'POST',
	// 			url: url_change_password,
	// 			data: $(this).serialize(),
	// 			beforeSend: function( xhr ) {
	// 				$('#processingPayment').show();
	// 			},	
	// 			success: function(DATA) {
	// 				$('#processingPayment').hide();
	// 				if (DATA && DATA.RXRESULTCODE === 1) {
	// 					$("#bootstrapAlert .modal-title").text('Request success!');
	// 					$("#bootstrapAlert .alert-message").text('Your password has been reset successfully!');
	// 					$("#bootstrapAlert").modal('show');
	// 					$("#bootstrapAlert").on('hidden.bs.modal', function(){
	// 						location.href = '/';
	// 					});
	// 				} else {
	// 					message_change_password.text(DATA.MESSAGE);
	// 				}
	// 			}
	// 		});
	// 	}
	// });
})(jQuery);