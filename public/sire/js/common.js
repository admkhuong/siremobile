$(document).ready(function() {
	var i, items, count;
	// Bộ đếm
	i = 0;
	
	items = $(".wrap").children('.chatbox');
	
	count = items.length;
	
	items.eq(i).show();
	
	setInterval(function(){
	
		items.eq(i).fadeOut(400);
	
		i++;
	
		if (i == count) {
			i = 0;
		}
	
		items.eq(i).fadeIn(400);
	}, 3000);
	
	
	var i2, items2, count2;
	
	i2 = 0;
	
	items2 = $(".wrap2").children('.chatbox');
	
	count2 = items2.length;
	
	items2.eq(i2).show();
	
	setInterval(function(){
	
		items2.eq(i2).fadeOut(400);
	
		i2++;
	
		if (i2 == count2) {
			i2 = 0;
		}
	
		items2.eq(i2).fadeIn(400);
	}, 3000);
	
	$(".fixed-chat .fixed-btn").click(function(){
		$(".fixed-chat").toggleClass("open");
	});

	$(".fixed-signup .fixed-signup-btn").click(function(){
		$(".fixed-signup").toggleClass("open");
	});


	// trigger quick signup
	$("#trigger-quick-account").on("click", function(){
		$("body").toggleClass("startFreeOpen");
		var md = new MobileDetect(window.navigator.userAgent);
		
		if(md.phone() !== null || md.tablet() !== null){
			if(!$("body").hasClass('startFreeOpen1')){
				$("body").addClass('startFreeOpen1')
			}
		}
	});

	$(".login-wrapper .btn-login").on('click', function(){
		var md = new MobileDetect(window.navigator.userAgent);
		
		if(md.phone() !== null || md.tablet() !== null){
			if(!$("body").hasClass('startFreeOpen1')){
				$("body").addClass('startFreeOpen1')
			}
		}
	});

	// Trigger click video
	$(".item-video-use").on("click", function(){
		var dataId = $(this).attr("data-id");

		$(".item-video-use").removeClass("active")

		$(this).addClass("active");

		$(".items-videos").addClass("hidden");
		$("#" + dataId).removeClass("hidden");
	})
	
});
		
$(function(){if($("#back-top").length>0){
	$(window).scroll(function(){
	if($(this).scrollTop()>100){
		$("#back-top").fadeIn()
	}
	else{$("#back-top").fadeOut()}
	});

	$("#back-top a").click(function(){
		$("body,html").animate({scrollTop:0},800);return false}
	)}

	/*
	var modalVerticalCenterClass = ".modal";
	function centerModals($element) {
	var $modals;
	if ($element.length) {
	    $modals = $element;
	} else {
	    $modals = $(modalVerticalCenterClass + ':visible');
	}
	$modals.each( function(i) {
	    var $clone = $(this).clone().css('display', 'block').appendTo('body');
	    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
	    top = top > 0 ? top : 0;
	    $clone.remove();
	    $(this).find('.modal-content').css("margin-top", top);
	});
	}
	$(modalVerticalCenterClass).on('show.bs.modal', function(e) {
	centerModals($(this));
	});
	$(window).on('resize', centerModals);
	*/

	var fields = $('input,select');

	fields.focus(function(){
		$(this).validationEngine('hide');
		var nextField = $(this).parents('.form-group').first().nextAll(':visible').first().find('input, select');
		setTimeout(function(){
		nextField.validationEngine('hide');
		},1);
	});

	$(window).on('load', function(){
		checkLandingTop();
	});

	$(window).scroll(function () {
        // $offset = $("header.home").outerHeight();
        // if (($(window).scrollTop() >= $offset - 270))
        checkLandingTop();
    });

    function checkLandingTop(){
    	if (($(window).scrollTop() > 0)) {
            $('.landing-top').addClass('navbar-dark');
        }
        else {
            $('.landing-top').removeClass('navbar-dark');
        }
    }

});


$(window).on("load resize", function(event){
	if(event.type == "load"){
		$(".navbar-header .navbar-nav").removeClass('hidden');
	}
	var sttWidth = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
	if(sttWidth < 768){
		$(".navbar-header .navbar-nav").addClass("dropdown-menu");
	}
	else{
		$(".navbar-header .navbar-nav").removeClass("dropdown-menu");
	}

	const window_width = $(window).width();
	$('header .slice').css('border-left-width',window_width);
});



function getParameterByName(name) 
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

if(getParameterByName('tab') == 'building')
{
    $(window).on('load', function(){
		$(".Template-Picker-Filter-Type[ref1='1']").trigger('click');
    })
}

if(getParameterByName('tab') == 'complex')
{
    $(window).on('load', function(){
		$(".Template-Picker-Filter-Type[ref1='5']").trigger('click');
    })
}

