(function($){
	$("#contact_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$(".btn-send-message").click(validate_form);

	var fields = $('#contact_form input, #contact_form textarea');
	
	fields.focus(function(){
		$(this).validationEngine('hide');
		var nextField = $(this).parents('.form-group').first().nextAll('.col-sm-11:visible').first().find('input, textarea');
		setTimeout(function(){
			nextField.validationEngine('hide');
		},1);
	});

	function validate_form(event){
		event.preventDefault();
		var self = $(this);
		//console.log(self.serialize());
		if ($('#contact_form').validationEngine('validate')) {
			var url_contact_us = '/public/sire/models/cfm/contact_us.cfm';
			var infoName = $("#infoName").val();
			var infoContact = $("#infoContact").val();
			var infoSubject = $("#infoSubject").val();
			var infoMsg = $("#infoMsg").val();
			
			$.ajax({
				method: 'POST',
				url: url_contact_us,
				dataType: 'json',
				data:  {
					infoName: infoName,
					infoContact: infoContact,
					infoSubject: infoSubject,
					infoMsg: infoMsg
					},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},	
				success: function(DATA) {
					// console.log(DATA);
					/*if (DATA && DATA.RXRESULTCODE === 1) {
						$('#forgot .success-message').removeClass('hidden');
						$('#forgot form').hide();
						$('#modalForgotEmail').val('');
						message_forgot.text('');
					} else {
						message_forgot.text(DATA.MESSAGE);
					}*/
					$('#processingPayment').hide();
					$("#infoName").val("");
					$("#infoContact").val("")
					$("#infoSubject").val("");
					$("#infoMsg").val("");
					
					
					$("#bootstrapAlert .modal-title").text('THANK YOU!');
					$("#bootstrapAlert .alert-message").html('Your request has been sent to Sire! We will be in touch with you shortly.');
					$("#bootstrapAlert").modal('show');
					
					

				}
			});
			
		}
	}


})(jQuery);