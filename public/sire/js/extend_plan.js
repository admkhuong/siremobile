(function($) {
	var card_info = $('#card_info');
	var card_form = $('#card_form');
	$('.select_current_card').click(function(){
		var val = $(this).val();
		if (val == '2') {
			card_info.show();
		} else {
			card_info.hide();
		}
	});
	
	function update_expiration_date() {
		$('#expiration_date').val($('#expiration_date_month').val() + '/' + $('#expiration_date_year').val());
	}
	
	update_expiration_date();
	
	$('#expiration_date_month, #expiration_date_year').change(function(){
		update_expiration_date();
	});

	card_form.validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});
	
	$('.btn-payment-credits').click(function(){
		switch ($('.select_current_card:checked').val()) {
			case '1':
				extendNow();
				break;
			case '2':
				if (card_form.validationEngine('validate')) {
					extendNow();
				}
				break;
		}
	});
	
	var processingPayment = $("#processingPayment");
	
	var extendNow = function() {
		processingPayment.show();
		$.ajax({
			method: 'POST',
			url: card_form.attr('action'),
			data: card_form.serialize(),
			dataType: 'json',
			complete: function(){
				processingPayment.hide();
			},
			success: function(data) {
				var bootstrapAlert = $('#bootstrapAlert');
				bootstrapAlert.find('.modal-title').text('Payment process');
				bootstrapAlert.find('.alert-message').html(data.MESSAGE);

				if (data.RXRESULTCODE == 1) {
					bootstrapAlert.on('hidden.bs.modal', function(){
						if (typeof data.URL_REDIRECT != 'undefined' && data.URL_REDIRECT != '') {
							location.href = data.URL_REDIRECT;
						} else {
							location.href = '/session/sire/pages/';
						}
					});
				} else {
					
				}
				
				bootstrapAlert.modal('show');
			}
		});
	}
})(jQuery);