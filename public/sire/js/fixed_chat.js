(function($){

	$('#fxdchat-send-msg-btn').click(function(event){
		event.preventDefault();
		if ($('#fixed-frm').validationEngine('validate')) {
			var formInputs = {
				infoName : $('input.fixedchat-frm[name="infoName"]'),
				infoContact : $('input.fixedchat-frm[name="infoContact"]')
			};
			var url_contact_us = '/public/sire/models/cfm/contact_us.cfm';

			$.ajax({
				method: 'POST',
				url: url_contact_us,
				dataType: 'json',
				data:  {
					infoName: formInputs.infoName.val(),
					infoContact: formInputs.infoContact.val(),
					infoSubject: "",
					infoMsg: $('textarea[name="infoMsg"]').val()
					},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},
				success: function(){
					for(prop in formInputs){
						formInputs[prop].val("");
					}
					$('textarea[name="infoMsg"]').val("");
					$('div.fixed-chat div.fixed-btn').trigger('click');

					$("#bootstrapAlert .modal-title").text('Request success!');
					$("#bootstrapAlert .alert-message").html('Thank you,<br>Your request for information has been sent.');
					$("#bootstrapAlert").modal('show');
				},
				complete: function(){
					$('#processingPayment').hide();
				}
			});
		}
		

	});
})(jQuery);