(function($){
	$('#modalSignInForgotLink').click(function(){
		$('#signin').modal('hide');
		setTimeout(function(){
			$('#forgot').modal('show');
		},500);
	});
	
	$('#forgot').on('hidden.bs.modal', function(){
		$('#forgot .success-message').addClass('hidden');
		$('#forgot form').show();
		$('#forgot .forgot-message').text('');
		grecaptcha.reset();
	});
	
		
	$( "#modalForgotEmail" ).focus(function() {
		$('#forgot .forgot-message').text('');
	});
	
	$('#forgot form').submit(function(event){
		event.preventDefault();
		
		if ($('#forgot form').validationEngine('validate',{scroll: false,focusFirstField : true})) {
			var url_forgot_password = '/public/sire/models/users.cfc?method=ResetPassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
			var message_forgot = $('#forgot .forgot-message').text('');
			if ($('#g-recaptcha-response').val() != '') {
				$.ajax({
					method: 'POST',
					url: url_forgot_password,
					data: $(this).serialize(),
					complete: function() {
						grecaptcha.reset();
					},
					beforeSend: function( xhr ) {
						$('#processingPayment').show();
					},	
					success: function(DATA) {
						$('#processingPayment').hide();
						if (DATA && DATA.RXRESULTCODE === 1) {
							$('#forgot .success-message').removeClass('hidden');
							$('#forgot form').hide();
							$('#modalForgotEmail').val('');
							message_forgot.text('');
						} else {
							message_forgot.text(DATA.MESSAGE);
						}
					}
				});
			} else {
				message_forgot.text('Please click captcha');
			}
		}
	});
})(jQuery);