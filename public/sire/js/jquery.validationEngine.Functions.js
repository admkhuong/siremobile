function isValidPassword(field, rules, i, options){
	//$("#validateno1").css("color","#424242");
	//$("#validateno2").css("color","#424242");
	//$("#validateno3").css("color","#424242");
	//$("#validateno4").css("color","#424242");
	//$("#validateno5").css("color","#424242");
	//$("#err_inpConfirmPassword2").css("display","none");

	var password = field.val();
	if (password.length < 8)
	{
		//$("#validateno1").css("color","red");
		return options.allrules.validatePassword.alertTextLength;
	 					  
	}
	
	var hasLetter = /[a-z]/i.test(password);
	if (!hasLetter)
	{
		return options.allrules.validatePassword.alertTextNotFound;
	}

	
/* Reduced this requirement to any case letter2017-08-18 Lee Peterson	  
	var hasUpperCase = /[A-Z]/.test(password);
	if (!hasUpperCase)
	{
		//jAlert("Password Validation Failure", 'Password must have at least 1 uppercase letter');
		//$("#validateno3").css("color","red");
		return options.allrules.validatePassword.alertTextUpperCase;
	}
						
	var hasLowerCase = /[a-z]/.test(password);
	if (!hasLowerCase)
	{
		
		//jAlert("Password Validation Failure", 'Password must have at least 1 lowercase letter');
		//$("#validateno4").css("color","red");
		return options.allrules.validatePassword.alertTextLowerCase;
	}
*/
	
	var hasNumbers = /\d/.test(password);
	if (!hasNumbers)
	{
		
		//jAlert("Password Validation Failure", 'Password must have at least 1 number');
		//$("#validateno2").css("color","red");
		return options.allrules.validatePassword.alertTextNumbers;
	 					  
	}

/* Removed this requirement	2017-08-18 Lee Peterson					
	var hasNonalphas = /\W/.test(password);
	if (!hasNonalphas)
	{
		
		//jAlert("Password Validation Failure", 'Password must have at least 1 special character');
		//$("#validateno5").css("color","red");
		return options.allrules.validatePassword.alertTextNoAlpha;
	}
*/
	
}