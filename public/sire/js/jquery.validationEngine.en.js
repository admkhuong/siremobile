(function ($) {
	$.fn.validationEngineLanguage = function () {
	};
	$.validationEngineLanguage = {
		newLang: function () {
			$.validationEngineLanguage.allRules = {
				"required": { // Add your regex rules here, you can take telephone as an example
					"regex": "none",
					"alertText": "* This field is required",
					"alertTextCheckboxMultiple": "* Please select an option",
					"alertTextCheckboxe": "* This checkbox is required",
					"alertTextDateRange": "* Both date range fields are required"
				},
				"requiredInFunction": {
					"func": function (field, rules, i, options) {
						return (field.val() == "test") ? true : false;
					},
					"alertText": "* Field must equal test"
				},
				"dateRange": {
					"regex": "none",
					"alertText": "* Invalid ",
					"alertText2": "Date Range"
				},
				"dateTimeRange": {
					"regex": "none",
					"alertText": "* Invalid ",
					"alertText2": "Date Time Range"
				},
				"minSize": {
					"regex": "none",
					"alertText": "* Minimum ",
					"alertText2": " characters allowed"
				},
				"maxSize": {
					"regex": "none",
					"alertText": "* Maximum ",
					"alertText2": " characters allowed"
				},
				"groupRequired": {
					"regex": "none",
					"alertText": "* You must fill one of the following fields"
				},
				"min": {
					"regex": "none",
					"alertText": "* Minimum value is "
				},
				"max": {
					"regex": "none",
					"alertText": "* Maximum value is "
				},
				"past": {
					"regex": "none",
					"alertText": "* Date prior to "
				},
				"future": {
					"regex": "none",
					"alertText": "* Date past "
				},
				"maxCheckbox": {
					"regex": "none",
					"alertText": "* Maximum ",
					"alertText2": " options allowed"
				},
				"minCheckbox": {
					"regex": "none",
					"alertText": "* Please select ",
					"alertText2": " options"
				},
				"equals": {
					"regex": "none",
					"alertText": "* Fields do not match"
				},
				"creditCard": {
					"regex": "none",
					"alertText": "* Invalid credit card number"
				},
				//Then use this regex that matches Visa, MasterCard, American Express, Diners Club, Discover, and JCB cards:
				"creditCardFunc": {					
					"regex": "^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$",
					"func": function (field, rules, i, options) {						
						var txt = field.val(), valid = false;
						
						/*if (field.parents("form").first().find("input[name=_card_id]").val() != "") {
							valid = true;
						} else {*/
							var cardNumber = txt.replace(/ +/g, '').replace(/-+/g, '');
							
							var numDigits = cardNumber.length;
							if (numDigits >= 14 && numDigits <= 16 && parseInt(cardNumber) > 0) {
								
								var sum = 0, i = numDigits - 1, pos = 1, digit, luhn = new String();
								do {
									digit = parseInt(cardNumber.charAt(i));
									luhn += (pos++ % 2 == 0) ? digit * 2 : digit;
								} while (--i >= 0)
								
								for (i = 0; i < luhn.length; i++) {
									sum += parseInt(luhn.charAt(i));
								}
								valid = sum % 10 == 0;
							}
							/*}*/
							return (txt == "" || valid);
						},
						"alertText": "* Invalid credit card number"
					},
					"phone": {
					// credit: jquery.h5validate.js / orefalo
					"regex": /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/,
					"alertText": "* Invalid phone number"
				},
				"email": {
					// HTML5 compatible email regex ( http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html# e-mail-state-%28type=email%29 )
					// "regex": /^$|^[a-zA-Z0-9\+.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/,
					// ( http://stackoverflow.com/questions/46155/validate-email-address-in-javascript )
					"regex": /^([\w-\+.]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
					"alertText": "* Invalid email address"
				},
				"email_or_phone": {
					// HTML5 compatible email regex ( http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html# e-mail-state-%28type=email%29 )
					//"regex": /^$|^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/,
					// ( http://stackoverflow.com/questions/46155/validate-email-address-in-javascript )
					"regex": /^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))|(([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$)$/i,
					"alertText": "* Invalid email address or phone"
				},
				
				"integer": {
					"regex": /^[\-\+]?\d+$/,
					"alertText": "* Not a valid integer"
				},
				"number": {
					// Number, including positive, negative, and floating decimal. credit: orefalo
					"regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
					"alertText": "* Invalid floating decimal number"
				},

				"flatRate":{
					"func":function(field){
						var num = parseFloat(field.val());
						var decimalNum = num.toFixed(2);
						if (num/decimalNum != 1) {
							return false;
						}
						return true;
					},
					"alertText": "* Please enter only 2 decimal places"
				},
				"date": {
					// Check if date is valid by leap year
					"func": function (field) {
						var pattern = new RegExp(/^(\d{4})[\/\-\.](0?[1-9]|1[012])[\/\-\.](0?[1-9]|[12][0-9]|3[01])$/);
						var match = pattern.exec(field.val());
						if (match == null)
							return false;
						var year = match[1];
						var month = match[2] * 1;
						var day = match[3] * 1;
						var date = new Date(year, month - 1, day); // because months starts from 0.
						return (date.getFullYear() == year && date.getMonth() == (month - 1) && date.getDate() == day);
					},
					"alertText": "* Invalid date, must be in YYYY-MM-DD format"
				},
				"isDate": {
					// Check if date is valid by leap year
					"func": function (field) {
						var txt = field.val();
						var date = Date.parse(txt);
						return txt=="" || !isNaN(date);
					},
					"alertText": "* Invalid date format"
				},
				"ipv4": {
					"regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
					"alertText": "* Invalid IP address"
				},
				"url": {
					"regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
					"alertText": "* Invalid URL"
				},
				"onlyNumber": {
					"regex": /^[0-9]+$/,
					"alertText": "* Numbers only"
				},
				"onlyNumberNotZero": {
					"regex": /^[1-9][0-9]*$/,
					"alertText": "* Numbers only and must be greater than 0"
				},
				"onlyNumberSp": {
					"regex": /^[0-9\ ]+$/,
					"alertText": "* Numbers and space only"
				},
				"onlyLetterSp": {
					"regex": /^[a-zA-Z\ \']+$/,
					"alertText": "* Letters and space only"
				},
				"onlyLetterNumber": {
					"regex": /^[0-9a-zA-Z]+$/,
					"alertText": "* No special characters allowed"
				},
				"onlyLetterNumberSp": {
					"regex": /^[0-9a-zA-Z\ \']+$/,
					"alertText": "* Must only be alphanumeric and space"
				},
				"validateAddrAndCity": {
					"regex": /^[0-9a-zA-Z\ \'\,\.\;\'\"\-\#\/\_]+$/,
					"alertText": "Only these characters: Alphanumeric, Space and ( ,.'\";/-_#) are allowed."
				},
				"validateCampaign": {
					"regex": /^[0-9a-zA-Z:\-\ \']+$/,
					"alertText": "* Must only be alphanumeric and space"
				},
				"checkCustomURL": {
					"regex": /^[0-9a-zA-Z\-\']+$/,
					"alertText": "* Must only be alphanumeric no special characters or spaces allowed"
				},				
				"noHTML": {
					"func": function (field) {
						var txt = field.val();
						return txt=="" || !/<[^>]*>/.test(txt);
					},
					"alertText": "* No HTML tag allowed"
				},
				// --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
				"ajaxUserCall": {
					"url": "ajaxValidateFieldUser",
					// you may want to pass extra data on the ajax call
					"extraData": "name=eric",
					"alertText": "* This user is already taken",
					"alertTextLoad": "* Validating, please wait"
				},
				"ajaxNameCall": {
					// remote json service location
					"url": "ajaxValidateFieldName",
					// error
					"alertText": "* This name is already taken",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					"alertTextOk": "* This name is available",
					// speaks by itself
					"alertTextLoad": "* Validating, please wait"
				},
				"ajaxKeywordAvailable": {
					// remote json service location
					//"url": "/session/cfc/csc/csc.cfc?method=CheckKeywordAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					"url": "/session/sire/models/cfc/campaign.cfc?method=checkKeywordAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					"extraDataDynamic": ['#shortCode'],
					// error
					"alertText": "* The keyword in use",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					"alertTextOk": "* Available",
					// speaks by itself
					"alertTextLoad": "* Validating, please wait"
				},
				"ajaxKeywordInUse": {
					// remote json service location
					"url": "/session/sire/models/cfc/campaign.cfc?method=checkKeywordInUse&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					"extraDataDynamic": ['#shortCode'],
					// error
					"alertText": "* The keyword in use",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					"alertTextOk": "* Available",
					// speaks by itself
					"alertTextLoad": "* Validating, please wait"
				},
				"validate2fields": {
					"alertText": "* Please input HELLO"
				},
				//tls warning:homegrown not fielded
				"dateFormat": {
					"regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
					"alertText": "* Invalid Date"
				},
				//tls warning:homegrown not fielded
				"dateTimeFormat": {
					"regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
					"alertText": "* Invalid Date or Date Format",
					"alertText2": "Expected Format: ",
					"alertText3": "mm/dd/yyyy hh:mm:ss AM|PM or ",
					"alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
				},
				"ajaxLookupPID": {
					"url": "index.cfm/Inquiry/pid_lookup",
					// you may want to pass extra data on the ajax call
					"extraData": "",//"name=eric",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					//"alertTextOk": "* This Property ID is available",
					"alertText": "* You have not entered a valid Property ID. Please correct it and try again.",
					"alertTextLoad": "* Validating, please wait"
				},
				"usPhoneNumber":{
					"regex": /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/,
					"alertText": "* Invalid Phone Numbers"
				},
				"usPhoneNumberII":{ // With Space "(000) 000-0000"
				"regex": /^(\([0-9]{3}\)|[0-9]{3}-) [0-9]{3}-[0-9]{4}$/,
				"alertText": "* Invalid Phone Numbers"
			},
				"usPhoneNumberIII":{ // With Space "(000) 000-0000"
				"regex": /^[^1]/,
				"alertText": "* First Numbers Cannot be 1"
			},
			"validatePassword": {
				"alertTextLength": "* Must be at least 8 characters",
				"alertTextNotFound": "* Must have at least 1 alpha a-z or A-Z letter",
				"alertTextUpperCase": "* Must have at least 1 uppercase letter",
				"alertTextLowerCase": "* Must have at least 1 lowercase letter",
				"alertTextNumbers": "* Must have at least 1 number",
				"alertTextNoAlpha": "* Must have at least 1 special character",
			},
			"ajaxCPPNameAvailable": {
					// remote json service location
					"url": "/session/sire/models/cfc/mlp.cfc?method=checkCPPNameAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					"extraDataDynamic": ['#ccpxDataId'],
					// error
					"alertText": "*  This name is already in use",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					//"alertTextOk": "* Available",
					// speaks by itself
					"alertTextLoad": "* Validating, please wait"
				},
				"ajaxCPPUrlAvailable": {
					// remote json service location
					"url": "/session/sire/models/cfc/mlp.cfc?method=checkCPPUrlAvailability&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
					"extraDataDynamic": ['#ccpxDataId'],
					// error
					"alertText": "*  This URL is already in use",
					// if you provide an "alertTextOk", it will show as a green prompt when the field validates
					//"alertTextOk": "* Available",
					// speaks by itself
					"alertTextLoad": "* Validating, please wait"
				},
			};
		}
	};
	$.validationEngineLanguage.newLang();
})(jQuery);