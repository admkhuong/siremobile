(function(){
	$('#sign-up-lr').validationEngine('attach', {
		scroll: false
	});

	$("#lrNumber").mask("(000)000-0000");

	$('#btn-submit').click(function(event){
		
		event.preventDefault();
		var sms_number = $('#lrNumber').val(),
			userEmail = $('#lrEmail').val(),
			accountType = $('#lrNumber').val(),
			pass = $('#lrPassword').val(),
			fname = $('#fname').val(),
			lname = $('#lname').val(),
			orgName = $('#org_name').val(),
			rf = $('#rf').val(),
			rftype = $('#rftype').val();

		var bbSignUpTitle = "Sign Up";
		
		if($('#sign-up-lr').validationEngine('validate')){
			try{
				$.ajax({
				type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
				url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  {
					INPMAINEMAIL: userEmail,
					//INPCOMPANYNAME: companyName,
					INPTIMEZONE: 31,
					INPACCOUNTTYPE: accountType,
					INPNAPASSWORD: pass,
					INPFNAME: fname,
					INPLNAME: lname,
					INPORGNAME: orgName,
					INSMSNUMBER: sms_number,
					inpReferralCode: rf,
					inpReferralType: rftype,
				},
				beforeSend: function( xhr ) {
					 //btnSignUp.hide();
					 $('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					$('#SignUpButton').show();
					$('#processingPayment').hide();
				},					  
				success: function(d){
					if(d.RESULT != "SUCCESS"){
						//jAlert(d.MESSAGE, 'Error');
						/*bootbox.alert(d.MESSAGE, function() {});*/
						bootbox.dialog({
						    message: d.MESSAGE,
						    title: bbSignUpTitle,
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {}
						        }
						    }
						});
						return false;
					}else{
						bootbox.dialog({
						    message: d.MESSAGE,
						    title: bbSignUpTitle,
						    buttons: {
						        success: {
						            label: "OK",
						            className: "btn btn-success btn-success-custom",
						            callback: function() {
										window.location.href = 'device-register';
						            }
						        }
						    }
						});
					}
				},
				complete: function(){	
					$('#processingPayment').hide();
				}
				});
			}catch(ex){
				/*bootbox.alert('Signup Fail', function() {});*/
				$('#processingPayment').hide();
				bootbox.dialog({
							    message: "Sign Up Fail",
							    title: bbSignUpTitle,
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-success btn-success-custom",
							            callback: function() {}
							        }
							    }
							});
				btnSignUp.prop('disabled',false);				
				return false;
			}

		}

	});



	/* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#LiveDemoVideoLink").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#LiveDemoVideoModal").on('hide.bs.modal', function(){
        $("#LiveDemoVideoLink").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#LiveDemoVideoModal").on('show.bs.modal', function(){
        $("#LiveDemoVideoLink").attr('src', url);
    });


})(jQuery);