(function($){
	$("#leadscon-request").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$(".leadscon-letsconnect-btn").click(validate_form);

	$("#leadscon-phone").mask("(000)000-0000");

	var fields = $('#leadscon-request input');

	function validate_form(event){
		event.preventDefault();
		var self = $(this);
		//console.log(self.serialize());
		if ($('#leadscon-request').validationEngine('validate')) {
			var url_leadscon_email = '/public/sire/models/cfm/leadscon_email.cfm';
			var infoEmail = $("#leadscon-mail").val();
			var infoName = $("#leadscon-name").val();
			var infoPhonenumber = $("#leadscon-phone").val();
			
			$.ajax({
				method: 'POST',
				url: url_leadscon_email,
				dataType: 'json',
				data:  {
					infoEmail: infoEmail,
					infoName: infoName,
					infoPhonenumber: infoPhonenumber
					},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},	
				success: function(DATA) {
					$('#processingPayment').hide();

					document.getElementById("leadscon-request").reset();
					$('#leadscon-email').modal('hide');
					
					
					// $("#bootstrapAlert .modal-title").text('THANK YOU!');
					// $("#bootstrapAlert .alert-message").html('Your request has been sent to Sire! We will be in touch with you shortly.');
					// $("#bootstrapAlert").modal('show');
					window.location.href = 'thank-you';
					

				}
			});
			
		}
	}

	$(document.body).on('hidden.bs.modal', function () {
	    $('body').css('padding-right','0');
	});

	$('#leadscon-email').on('show.bs.modal', function () {
	    $(this).css('padding-right','0');
	});

	$('#leadscon-email').on('hidden.bs.modal', function () {
	    $('#leadscon-request').validationEngine('hide');
	});


})(jQuery);