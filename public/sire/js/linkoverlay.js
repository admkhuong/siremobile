
function isTouchDevice(){
    return typeof window.ontouchstart !== 'undefined';
}

if (isTouchDevice()==true){
	$('.grid-item').on('click',function(event){
		if($(this).find('.link-overlay').css('visibility')=='hidden'){
			$(this).find('.link-overlay').css('opacity', '1');
			$(this).find('.link-overlay').css('visibility', 'visible');
			$(this).parent().siblings().find('.link-overlay').css('opacity', '0');
			$(this).parent().siblings().find('.link-overlay').css('visibility', 'hidden');
		}
		else{
			$(this).find('.link-overlay').css('opacity', '0');
			$(this).find('.link-overlay').css('visibility', 'hidden');
		}
		
	});
}