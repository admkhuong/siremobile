(function($){

	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if(results  !== null) return results[1];
		return 0;
	};



	var videos = [
		{id: 1, title: "What's Inside Sire?", src: "https://www.youtube.com/embed/qzQJx0FtlBY?rel=0&modestbranding=1&enablejsapi=1", thumb: "thumb-2.png", active: true},
		{id: 2, title: "How to create your first campaign", src: "https://www.youtube.com/embed/hbc6pPq8NH8?rel=0&modestbranding=1&enablejsapi=1", thumb: "thumb-1.png", active: false},
		{id: 3, title: "How To Create Your First Subscriber List", src: "https://www.youtube.com/embed/oNrGjlatHWQ?rel=0&modestbranding=1&enablejsapi=1", thumb: "thumb-2.png", active: false}
	];

	var loadIframe = function(src){
		$('iframe.ifr-video').attr('src', src);
	}

	var loadTitle = function(title){
		$('h2.video-title').html(title);
	}

	var markActiveThumb = function(id){
		$('div.item-video-use').removeClass('active');
		$('div.item-video-use[data-id="'+id+'"]').addClass('active');
	}

	var loadVideo = function(vObject, autoplay){
		if(typeof(autoplay) == "undefined"){
			autoplay = true;
		}
		
		var src = vObject.src;

		if(autoplay === true){
			src +="&autoplay=1";
		}
		loadIframe(src);
		loadTitle(vObject.title);
		markActiveThumb(vObject.id);
	}

	$(document).ready(function(){
		//default load first video
		var videoid = $.urlParam('vplay');
		if(videoid > 0){
			for(var i in videos){
				var video = videos[i];
				if(video.id == videoid){
					loadVideo(video, false);
				}
			} 	
		} else {
			loadVideo(videos[0], false);
		}
		
		//handle when user click to video thumb
		$('div.item-video-use').click(function(event){
			var id = $(this).data('id');
			for(var i in videos){
				var video = videos[i];
				if(video.id == id){
					loadVideo(video);
				}
			}
		});
	});
	
	$('#PhoneMenuToggle').click(function(){
		var menu = $('#PhoneMenuArea');
		if (menu.hasClass('w0')) {
			menu.removeClass('w0');
		} else {
			menu.addClass('w0');
		}
	});
	
	$('#PhoneMenuAreaInner .ottdemokeyword').click(function(){
		$('#PhoneMenuArea').addClass('w0');
		$(this).addClass('active').siblings().removeClass('active');
	});

	// if(window.location.href.indexOf("subcriber-list") > -1){
	// 	$('#use-guide-1').addClass('hidden');
	// 	$('#use-guide-2').removeClass('hidden');
	// 	$('.video-btn-1').removeClass('active');
	// 	$('.video-btn-2').addClass('active');
	// 	$('.video-title-1').hide();
	// 	$('.video-title-2').show();
	// }
	

	// $('body').on('click', '.video-btn-1', function(event) {
	// 	event.preventDefault();
	// 	var video2 = $('.video-2')[0].contentWindow;
	// 	// console.log(video2);

	// 	video2.postMessage('{"event":"command", "func":"stopVideo", "args":""}', '*');
	// 	var video1 = $('.video-1')[0].contentWindow;
	// 	video1.postMessage('{"event":"command", "func":"playVideo", "args":""}', '*');
	// 	$('.video-title-2').hide();
	// 	$('.video-title-1').show();
	// });




	// $('body').on('click', '.video-btn-2', function(event) {
	// 	event.preventDefault();
	// 	var video1 = $('.video-1')[0].contentWindow;
	// 	video1.postMessage('{"event":"command", "func":"stopVideo", "args":""}', '*');
	// 	var video2 = $('.video-2')[0].contentWindow;
	// 	video2.postMessage('{"event":"command", "func":"playVideo", "args":""}', '*');
	// 	$('.video-title-1').hide();
	// 	$('.video-title-2').show();
	// });






})(jQuery);