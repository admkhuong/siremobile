(function($) {
    "use strict";
    $(window).bind('beforeunload', function(){
    	var actionStatus = $("#actionStatus").val();
    	if (actionStatus == 'Processing')
	  		return 'Payment processing. Please don\'t refresh and close.';
	});
    $("#order-plan").change(function(e) {
        var plan = $("#order-plan").val();
        $.ajax({
            type: "GET",
            url: "/public/sire/models/cfc/order_plan.cfc?method=GetOrderPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: 'json',
            data: {
                plan: plan
            },
            beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},	
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            	$('#processingPayment').hide();
            },
            success: function(data) {
            	$('#processingPayment').hide();
            	$('#plan').val(plan);
            	$('#MonthlyAmount').text('$' + data.DATA.AMOUNT);
                $('#NumberKeyword').text(data.DATA.KEYWORDSLIMITNUMBER);
                $('#FirstSMSInclude').text(data.DATA.FIRSTSMSINCLUDED);
                $('#amount').val(data.DATA.AMOUNT);
            }
        });
        
    });
    $('#country').val('US');
	$('#email').val($('#h_email').val());
})(jQuery);

function isValidPassword(field, rules, i, options){
			//$("#validateno1").css("color","#424242");
			//$("#validateno2").css("color","#424242");
			//$("#validateno3").css("color","#424242");
			//$("#validateno4").css("color","#424242");
			//$("#validateno5").css("color","#424242");
			//$("#err_inpConfirmPassword2").css("display","none");
			var password = field.val();
			if (password.length < 8)
			{
				//$("#validateno1").css("color","red");
				return options.allrules.validatePassword.alertTextLength;
			 					  
			}
			  
			var hasLetter = /[a-z]/i.test(password);
			if (!hasLetter)
			{
				return options.allrules.validatePassword.alertTextNotFound;
			}
			
		/* Reduced this requirement to any case letter2017-08-18 Lee Peterson
			var hasUpperCase = /[A-Z]/.test(password);
			if (!hasUpperCase)
			{
				return options.allrules.validatePassword.alertTextUpperCase;
			}
		
			var hasLowerCase = /[a-z]/.test(password);
			if (!hasLowerCase)
			{
				return options.allrules.validatePassword.alertTextLowerCase;
			}
		*/
			
			var hasNumbers = /\d/.test(password);
			if (!hasNumbers)
			{
				return options.allrules.validatePassword.alertTextNumbers;
		
			}
		
		/* Removed this requirement	2017-08-18 Lee Peterson
			var hasNonalphas = /\W/.test(password);
			if (!hasNonalphas)
			{
				return options.allrules.validatePassword.alertTextNoAlpha;
			}
		*/
	}
	
function paymentConfirm() {
	$('#rsSelectYourPlan').text($('#order-plan option:selected').text());
	$('#rsFirstCreditsInclude').text($('#FirstSMSInclude').text());
	$('#rsNumberofPlanKeyword').text($('#NumberKeyword').text());
	$('#rsAmount').text($('#MonthlyAmount').text());
}
