(function($) {
    "use strict";
    
	$('a.commingson,a.price_detail').click(function(event){
		event.preventDefault();
		$("#bootstrapAlert .modal-title").text('Plans & Pricing!');
		$("#bootstrapAlert .alert-message").text('Coming soon...');
		$("#bootstrapAlert").modal('show');
	});
	
	//opt-in-form
    $("#plan-pricing-calc-form")
	    .validationEngine({
	        promptPosition: "topLeft",
	        scroll: false,
	        focusFirstField : false
	    })
	
	$('#Calculate').click(function() {
		validate_form();
	});
	
	$('#Reset').click(function() {
		$('#plan-pricing-calc-form')[0].reset();
		$(".main-wrapper").hide(500);
	});
	
	
	$(".btn-pricing").click(function(){
        var url = $(this).data('url');
        $("#loginRedirect").val(url);
    });

    $('#month-year-switch').on('click', function(){
    	$('.triangle-isosceles.left').toggleClass('changed');
    	if($(this).is(':checked')){
    		$('.yearly-bill').show();
    		$('.monthly-bill').hide();
    		$('#monthly-plan-text').css('color', '#929292');
    		$('#yearly-plan-text').css('color', '#568ca5');
    		$('.triangle-isosceles').css('background', '#568ca5');
    	}
    	else{
    		$('.yearly-bill').hide();
    		$('.monthly-bill').show();
    		$('#monthly-plan-text').css('color', '#568ca5');
    		$('#yearly-plan-text').css('color', '#929292');
    		$('.triangle-isosceles').css('background', '#929292');
    	}
    });

    jQuery(document).ready(function($) {
    	if($('#month-year-switch').is(':checked')){
    		$('.yearly-bill').show();
    		$('.monthly-bill').hide();
    		$('#monthly-plan-text').css('color', '#929292');
    		$('#yearly-plan-text').css('color', '#568ca5');
    		$('.triangle-isosceles').css('background', '#568ca5');
    		$('.triangle-isosceles.left').toggleClass('changed');
    	}
    	else{
    		$('.yearly-bill').hide();
    		$('.monthly-bill').show();
    		$('#monthly-plan-text').css('color', '#568ca5');
    		$('#yearly-plan-text').css('color', '#929292');
    		$('.triangle-isosceles').css('background', '#929292');
    	}
    });


})(jQuery);

function validate_form() {
    if ($('#plan-pricing-calc-form').validationEngine('validate')) {
    	$(".main-wrapper").show(500);
    	planPricing();
    }
	return false;
}

function planPricing () {
	var Q1 = $("#Q1").val();
	var Q2 = $("#Q2").val();
	var Q3 = $("#Q3").val();
	
	var SMSMessages = Q1 * Q2;
	var totalSMS = 0;
	
	if (Q3 == 'beginner') {
		totalSMS = SMSMessages;
	} else if (Q3 == 'intermediate') {
		totalSMS = 2*SMSMessages;
	} else if (Q3 == 'expert') {
		totalSMS = 3*SMSMessages;
	}
	
	if (totalSMS < 5000) {
		//INDIVIDUAL
		$('.sire-pricing').removeClass('active');
		$('#price2').addClass('active');
		plan = 2;
	} else if (totalSMS >= 5000 && totalSMS <= 15000) {
		//SMB
		$('.sire-pricing').removeClass('active');
		$('#price6').addClass('active');
		plan = 6;
	} else if (totalSMS > 15000) {
		//ENTERPRISE
		$('.sire-pricing').removeClass('active');
		$('#price3').addClass('active');
		plan = 3;
	}
	
	
		$.ajax({
            type: "GET",
            url: "/public/sire/models/cfc/order_plan.cfc?method=GetOrderPlan&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
            dataType: 'json',
            data: {
                plan: plan
            },
            beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},	
            error: function(XMLHttpRequest, textStatus, errorThrown) {
            	$('#processingPayment').hide();
            },
            success: function(data) {
            	$('#processingPayment').hide();
                $('#planSelect').text(data.DATA.PLANNAME);
            }
        });
	
}