
var globalShortCode="QA2Nowhere";
if(window.location.hostname.trim().toLowerCase()=="www.siremobile.com")
{
	globalShortCode="39492";
}
function confirmBox(message, title, callback){
    title  = typeof(title) != "undefined" ? title : "Confirmation";
    bootbox.dialog({
        message: '<p>'+ message +'</p>',
		title: '<h4 class="be-modal-title">'+title+'</h4>',
        className: "be-modal",
        buttons: {
            success: {
                label: "Continue",
                className: "btn btn-medium green-gd",
                callback: function(result) {
                   callback();
                }
            },
            cancel: {
                label: "Cancel",
                className: "green-cancel"
            },
        }
    });
}
function alertBox(message, title, callback) {
    title  = typeof(title) != "undefined" ? title : "Alert";
    bootbox.dialog({
        message: '<p>'+ message +'</p>',
        title: '<h4 class="be-modal-title">'+ title +'</h4>',
        className: "be-modal",
        buttons: {
            success: {
                label: "OK",
                className: "green-gd",
                callback: function(){
                    typeof(callback) == "function" ? callback() : "";
                }
            },
        }
    });
}
function CheckKwAvailable(kw){
	var substring_kw=kw;
	if(kw.length > 40) substring_kw= kw.substring(0, 40)+ "...";
	$.ajax({
		type: "POST", /*<!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->*/
		url: '/public/sire/models/cfc/userstools.cfc?method=ValidateKeyword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		async: false,
		data:  
		{ 			
			inpKeyword : kw,
			inpShortCode : globalShortCode		
			
		},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			alertBox("Error. No Response from the remote server. Check your connection and try again.");					
		},					  
		success:				
		function(d) 
		{
						
			if (parseInt(d.RXRESULTCODE) == 1) 
			{									
				var message="Congratulations! Keyword '"+ substring_kw+"' is available. Would you like to see how it works?";
				bootbox.dialog({
					message: '<p>'+ message +'</p>',
					title: '<h4 class="be-modal-title">Congratulations!</h4>',
					className: "be-modal",
					buttons: {
						success: {
							label: "Continue",
							className: "btn btn-medium green-gd",
							callback: function(result) {
								var modalBox=$("#mdEnterMessageTest");
								modalBox.find("#EnterMessageTest-Keyword").text(" '"+substring_kw+"' ");
								modalBox.find("#EnterMessageTest-Shortcode").text(" '"+globalShortCode+"' ");
								
								modalBox.modal('show');
							}
						},
						cancel: {
							label: "Cancel",
							className: "green-cancel"
						},
					}
				});
			}
			else
			{											
				//var message="Sorry keyword '"+kw+"' is taken.</br> Try again";
				var message=d.ERRMESSAGE;
				bootbox.dialog({
					message: '<p>'+ message +'</p>',
					title: '<h4 class="be-modal-title">Oops!</h4>',
					className: "be-modal",
					buttons: {
						success: {
							label: "Continue",
							className: "btn btn-medium green-gd",
							callback: function(result) {
								setTimeout(function(){ 
									$("#EnterKW").select();
								}, 1000); 
							}
						},
						cancel: {
							label: "Cancel",
							className: "green-cancel",
							callback:function(){
								$("#EnterKW").val("");
							}
						},
					}
				});
			}			
		}
	});
}
function CreatePreAccount()
{
	var kw=$("#EnterKW").val();
	$.ajax({
		type: "POST", 
		url: '/public/sire/models/cfc/userstools.cfc?method=CreatePreAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		async: false,
		data:  
		{ 			
			inpKeyword : kw,
			inpShortCode : globalShortCode,		
			inpMessage: $("#EnterMessageTest-Message").val()
		},							  
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			$("#processingPayment").hide();			
			alertBox("Error. No Response from the remote server. Check your connection and try again.");									
		},		
		beforeSend: function () {
			$("#processingPayment").show();
		},
		success: function(d) 
		{			
			$("#processingPayment").hide();			
			if (parseInt(d.RXRESULTCODE) == 1) 
			{					
				$("#mdEnterMessageTest").modal('hide');
				alertBox("Congradulations! You keyword have been setup successfully.</br> Now grab your phone and text the word '"+kw+"' to the phone number '"+globalShortCode+"' to see how your message will appear to customers.","Successful");
			}
			else
			{											
				alertBox(d.ERRMESSAGE);									
			}			
		}
	});	
}
$(document).ready(function(){
	$("#EnterKW").on('keydown',function(e){
		if (e.which == 13) {
			if($(this).val().length > 7){
				CheckKwAvailable($(this).val());
			}
			else
			{
				alertBox("Please enter at least 8 characters","Oops!");
			}
		}
	});
	$("#frmEnterMessageTest").on("submit",function(event){
		event.preventDefault();
		if ($('#frmEnterMessageTest').validationEngine('validate')) {
			CreatePreAccount();
		}
	});
});
