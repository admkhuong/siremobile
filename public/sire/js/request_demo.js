(function($){
	$("#requestdemo_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : false});

	$(".btn-send-message").click(validate_form);

	var fields = $('#requestdemo_form input');

	$("#infoPhone").mask("(000)000-0000");
	
	fields.focus(function(){
		$(this).validationEngine('hide');
		var nextField = $(this).parents('.form-group').first().nextAll('.col-sm-11:visible').first().find('input, textarea');
		setTimeout(function(){
			nextField.validationEngine('hide');
		},1);
	});

	function validate_form(event){
		event.preventDefault();
		var self = $(this);
		//console.log(self.serialize());
		if ($('#requestdemo_form').validationEngine('validate')) {
			var infoName = $("#infoName").val();
			var infoEmail = $("#infoEmail").val();
			var infoCompany = $("#infoCompany").val();
			var infoPhone = $("#infoPhone").val();
			
			$.ajax({
				method: 'POST',
				url: '/public/sire/models/cfm/request_demo.cfm',
				dataType: 'json',
				data:  {
					infoName: infoName,
					infoEmail: infoEmail,
					infoCompany: infoCompany,
					infoPhone: infoPhone
					},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},	
				success: function(DATA) {
					// console.log(DATA);
					/*if (DATA && DATA.RXRESULTCODE === 1) {
						$('#forgot .success-message').removeClass('hidden');
						$('#forgot form').hide();
						$('#modalForgotEmail').val('');
						message_forgot.text('');
					} else {
						message_forgot.text(DATA.MESSAGE);
					}*/
					$('.before-send-request').hide();
					$('.after-send-request').show();
					$('#processingPayment').hide();
					$("#infoName").val("");
					$("#infoEmail").val("")
					$("#infoCompany").val("");
					$("#infoPhone").val("");
					
					

				}
			});
			
		}
	}


})(jQuery);