(function($){
  $("#restaurant-slide-phone").on("slid.bs.carousel", "", checkitem); // on caroussel move

  $(document).ready(function(){               // on document ready
      checkitem();
  });

  function checkitem()                        // check function
  {
    var $this = $('#restaurant-slide-phone');
  	
  	if($('#last-slide-phone').hasClass('active')) {
      $this.children('.left.carousel-control').show();
      $this.children('.right.carousel-control').hide();}
      else if($('.carousel-inner .item:first').hasClass('active')) {
      $this.children('.left.carousel-control').hide();
      $this.children('.right.carousel-control').show();
    } else {
      $this.children('.carousel-control').show();

    } 
  }
})(jQuery);