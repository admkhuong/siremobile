(function($) {
    $('#signin').on('hidden.bs.modal', function() {
        $('#signin form')[0].reset();
        $('#signin .signin-message').text('')
    });
    var url = window.location.href;
    var getemail = url.split("?e=");
    if (getemail.length == 2) {
        $('#signin').modal('show');
        $('#modalSignInEmail').val(getemail[1])
    }
    $('#signin form').submit(function(event) {

        event.preventDefault();
        if ($('#signin form').validationEngine('validate', {
                scroll: false,
                focusFirstField: false
            })) {
            var url_sign_in = '/public/sire/models/cfc/userstools.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
            var message_sign_in = $('#signin .signin-message').text('');
            var loginRedirect = $('#loginRedirect').val();
            var defaultRedirect= "/session/sire/pages/dashboard.cfm?action=checkUserCampaign&firstlogin=1";
            $.ajax({
                method: 'POST',
                url: url_sign_in,
                data: $(this).serialize(),
                beforeSend: function(xhr) {
                    $('#processingPayment').show()
                },
                success: function(DATA) {
                    $('#processingPayment').hide();
                    if (DATA && DATA.RXRESULTCODE === 1) {
                        if (parseInt(DATA.MFAREQ) == 1) {
                            window.location.href = '/device-register'
                        } else {
                            // log IP Address of user login first
                            /* $.getJSON('http://api.ipstack.com/check?access_key=190961977e6e1a24a5d88f2257409732&format=1', function(data) {

                            }); */

                            var url_log_ip = '/public/sire/models/cfc/userstools.cfc?method=LogIPAddress&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true'
                            // Call cfc to save IP Address into database
                            $.ajax({
                                url: url_log_ip,
                                method: 'POST',
                                data: {},
                                beforeSend: function(xhr) {
                                },
                                success: function(DATA) {
                                    if (loginRedirect != '') {
                                        location.href = loginRedirect
                                    } else {
                                        GetAffiliateCode(DATA.USERID).then(function(data){
                                            if(data.RXRESULTCODE==1 && data.AFFILIATECODE !='')
                                            {
                                                location.href = '/session/sire/pages/affiliates-dashboard?firstlogin=1';
                                            }
                                            else
                                            {
                                                location.href = defaultRedirect;
                                            }
                                        }).catch(function(){
                                            location.href = defaultRedirect;
                                        })
                                    }
                                }
                            })
                            // end log IP Address
                        }
                    } else if (DATA && DATA.RXRESULTCODE === -4) {
                        message_sign_in.text(DATA.MESSAGE)
                    } else if (DATA && DATA.RXRESULTCODE === -3) {
                        message_sign_in.text(DATA.REASONMSG)
                    } else {
                        message_sign_in.text('Incorrect email or password.')
                    }
                }
            })
        }
    });
    $('#signout .btn-modal-signout').click(function(event) {
        event.preventDefault();
        if ($('#signout #DALD').is(':checked')) {
            var DALD = 1
        } else var DALD = 0;
        var userId = $('#signout #SESuserId').val();
        var url_sign_out = '/signout?DALD=' + DALD + '&userId=' + userId;
        window.location.href = url_sign_out
    })
    function GetAffiliateCode(userID){
        return new Promise( function (resolve, reject) {
            $.ajax({
                type:"POST",
                url:'/public/sire/models/cfc/affiliate.cfc?method=GetAffiliateCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                dataType:'json',
                data:
                    {
                        inpUserId:userID
                    }
                ,beforeSend:function(xhr){
                    $('#processingPayment').show()
                }
                ,error:function(XMLHttpRequest,textStatus,errorThrown){
                    $('#processingPayment').hide()
                    var data ={MESSAGE:"Unable to get your Affiliate Code at this time. An error occurred."};
                    reject(data);
                }
                ,success:function(data)
                    {
                    $('#processingPayment').hide();
                    if(data.RXRESULTCODE==1){
                        resolve(data);
                    }
                    else
                    {
                        reject(data);
                    }
                }
            });
        });
    }
})(jQuery);
