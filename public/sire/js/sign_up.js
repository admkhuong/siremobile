(function($){
	$("#signup_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});

	$("#btn-sign-up").click(validate_form);

	$("#btn-sign-up1").click(validate_form1);

	$("#sms_number").mask("(000)000-0000");

	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

	var inviteFriends = getUrlParameter('invite');

	function validate_form(){
		if ($('#signup_form').validationEngine('validate', {scroll: false, focusFirstField : true})) {	

			var bbSignUpTitle = "Sign Up";
			var accountType = 'personal';
			//var companyName = '';
			
			var fname = $("#fname").val();
			var lname = $("#lname").val();
			var userEmail = $("#emailadd").val();
			var sms_number = $("#sms_number").val();
			var pass = $("#inpPasswordSignup").val();
			var confirmPass = $("#inpConfirmPassword").val();
			var orgName = $("#org_name").val();
			var btnSignUp = $('#btn-sign-up');
			var plan = $('#plan').val();
			var rf = $('#rf').val(); // Referral Code
			var rftype = $('#rftype').val(); 
			
			var pcode = $('#inpPromotionCode').val();

			btnSignUp.prop('disabled',true);

			try{
				$.ajax({
				type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
				url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  {
					INPMAINEMAIL: userEmail,
					//INPCOMPANYNAME: companyName,
					INPTIMEZONE: 31,
					INPACCOUNTTYPE: accountType,
					INPNAPASSWORD: pass,
					INPFNAME: fname,
					INPLNAME: lname,
					INPORGNAME: orgName,
					INSMSNUMBER: sms_number,
					inpReferralCode: rf,
					inpReferralType: rftype,
					inpPromotionCode: pcode
				},
				beforeSend: function( xhr ) {
   					 //btnSignUp.hide();
   					 $('#processingPayment').show();
  				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					$('#SignUpButton').show();
					$('#processingPayment').hide();
				},					  
				success:		
					function(d) 
					{
						$('#processingPayment').hide();
						btnSignUp.prop('disabled',false);
						if(d.RESULT != "SUCCESS"){
							bootbox.dialog({
							    message: d.MESSAGE,
							    title: bbSignUpTitle,
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-success btn-success-custom",
							            callback: function() {}
							        }
							    }
							});
							return false;
						}else{
							if (plan != '' && rf != '') {
								window.location.href = '/thankyou?plan=' + plan + '&rf=' + rf + '&type=' + rftype;
							} 
							else if(plan != '' && rf == '') {
								window.location.href = '/thankyou?plan=' + plan;	
								} 
							else if(inviteFriends === "true") {
									window.location.href = 'thankyou?invite=true';
								} 
							else {
								window.location.href = 'thankyou';
							}
						}
					} 		
					
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
							    message: "Sign Up Fail",
							    title: bbSignUpTitle,
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-success btn-success-custom",
							            callback: function() {}
							        }
							    }
							});
				btnSignUp.prop('disabled',false);				
				return false;
			}
		}	
	}

	function validate_form1(){
		if ($('#signup_form').validationEngine('validate', {scroll: false, focusFirstField : true})) {	

			var bbSignUpTitle = "Sign Up";
			var accountType = 'personal';
			//var companyName = '';
			
			var fname = $("#fname").val();
			var lname = $("#lname").val();
			var userEmail = $("#emailadd").val();
			var sms_number = $("#sms_number").val();
			var pass = $("#inpPasswordSignup").val();
			var confirmPass = $("#inpConfirmPassword").val();
			var orgName = $("#org_name").val();
			var btnSignUp = $('#btn-sign-up1');
			var plan = $('#plan').val();
			var rf = $('#rf').val(); // Referral Code
			var rftype = $('#rftype').val(); 
			
			var pcode = $('#inpPromotionCode').val();

			btnSignUp.prop('disabled',true);

			try{
				$.ajax({
				type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
				url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  {
					INPMAINEMAIL: userEmail,
					//INPCOMPANYNAME: companyName,
					INPTIMEZONE: 31,
					INPACCOUNTTYPE: accountType,
					INPNAPASSWORD: pass,
					INPFNAME: fname,
					INPLNAME: lname,
					INPORGNAME: orgName,
					INSMSNUMBER: sms_number,
					inpReferralCode: rf,
					inpReferralType: rftype,
					inpPromotionCode: pcode
				},
				beforeSend: function( xhr ) {
   					 //btnSignUp.hide();
   					 $('#processingPayment').show();
  				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					$('#SignUpButton').show();
					$('#processingPayment').hide();
				},					  
				success:		
					function(d) 
					{
						$('#processingPayment').hide();
						btnSignUp.prop('disabled',false);
						if(d.RESULT != "SUCCESS"){
							bootbox.dialog({
							    message: d.MESSAGE,
							    title: bbSignUpTitle,
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-success btn-success-custom",
							            callback: function() {}
							        }
							    }
							});
							return false;
						}else{
							window.location.href = 'thankyou?pt=1';
						}
					} 		
					
				});
			}catch(ex){
				console.log(ex);
				$('#processingPayment').hide();
				bootbox.dialog({
							    message: "Sign Up Fail",
							    title: bbSignUpTitle,
							    buttons: {
							        success: {
							            label: "OK",
							            className: "btn btn-success btn-success-custom",
							            callback: function() {}
							        }
							    }
							});
				btnSignUp.prop('disabled',false);				
				return false;
			}
		}
	}	
})(jQuery);


function isValidPassword(field, rules, i, options){
			//$("#validateno1").css("color","#424242");
			//$("#validateno2").css("color","#424242");
			//$("#validateno3").css("color","#424242");
			//$("#validateno4").css("color","#424242");
			//$("#validateno5").css("color","#424242");
			//$("#err_inpConfirmPassword2").css("display","none");
			var password = field.val();
			if (password.length < 8)
			{
				//$("#validateno1").css("color","red");
				return options.allrules.validatePassword.alertTextLength;
			 					  
			}
			  
			// var hasUpperCase = /[A-Z]/.test(password);
			// if (!hasUpperCase)
			// {
			// 	//jAlert("Password Validation Failure", 'Password must have at least 1 uppercase letter');
			// 	//$("#validateno3").css("color","red");
			// 	return options.allrules.validatePassword.alertTextUpperCase;
			// }
								
			// var hasLowerCase = /[a-z]/.test(password);
			// if (!hasLowerCase)
			// {
				
			// 	//jAlert("Password Validation Failure", 'Password must have at least 1 lowercase letter');
			// 	//$("#validateno4").css("color","red");
			// 	return options.allrules.validatePassword.alertTextLowerCase;
			// }

			var hasLetter = /[a-z]/i.test(password);
			if (!hasLetter)
			{
				return options.allrules.validatePassword.alertTextNotFound;
			}
			
			var hasNumbers = /\d/.test(password);
			if (!hasNumbers)
			{
				
				//jAlert("Password Validation Failure", 'Password must have at least 1 number');
				//$("#validateno2").css("color","red");
				return options.allrules.validatePassword.alertTextNumbers;
			 					  
			}
								
			// var hasNonalphas = /\W/.test(password);
			// if (!hasNonalphas)
			// {
				
			// 	//jAlert("Password Validation Failure", 'Password must have at least 1 special character');
			// 	//$("#validateno5").css("color","red");
			// 	return options.allrules.validatePassword.alertTextNoAlpha;
			// }
	}

