(function() {
	$("#sms_number").mask("(000)000-0000");

	$("#signup_form_1").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});

	$("#btn-new-signup").on('click', function(event){
		event.preventDefault();
		validate_form();
	});

	function validate_form(){
		if ($("#signup_form_1").validationEngine('validate')) {	

			var bbSignUpTitle = "Sign Up";
			var accountType = 'personal';
			
			var fname = $("#fname").val();
			var lname = $("#lname").val();
			var userEmail = $("#emailadd").val();
			var sms_number = $("#sms_number").val();
			var pass = $("#inpPasswordSignup").val();
			var confirmPass = $("#inpConfirmPassword").val();
			var pt = $('#select-plan-2').val();
			var planid = $('#select-plan-2').val();


			try{
				$.ajax({
					type: "POST",
					url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccountNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  {
						INPMAINEMAIL: userEmail,
						INPTIMEZONE: 31,
						INPACCOUNTTYPE: accountType,
						INPNAPASSWORD: pass,
						INPFNAME: fname,
						INPLNAME: lname,
						INSMSNUMBER: sms_number,
						email: userEmail,
						inpPlanId: planid
					},
					beforeSend: function( xhr ) {
						$('#processingPayment').show();
					},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						$('#SignUpButton').show();
						$('#processingPayment').hide();
					},					  
					success:		
					function(d) 
					{
						$('#processingPayment').hide();
						if(d.RESULT != "SUCCESS"){
							bootbox.dialog({
								message: d.MESSAGE,
								title: bbSignUpTitle,
								buttons: {
									success: {
										label: "OK",
										className: "btn btn-success btn-success-custom",
										callback: function() {}
									}
								}
							});
							return false;
						}else{
							window.location.href = 'thankyou?pt=1';
						}
					} 		
					
				});
			}catch(ex){
				console.log(ex);
				$('#processingPayment').hide();
				bootbox.dialog({
					message: "Sign Up Fail",
					title: bbSignUpTitle,
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-success btn-success-custom",
							callback: function() {}
						}
					}
				});
				btnSignUp.prop('disabled',false);				
				return false;
			}
		}
	}
})();