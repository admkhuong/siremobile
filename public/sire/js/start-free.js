// get value parameter from Url
//function getURLParameterNew(name) {
	//return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
//}
//var token = getURLParameterNew('token');	
//var payerid = getURLParameterNew('PayerID');
///var numberSMSToSend = getURLParameterNew('numberSMSToSend');
//var numberKeywordToSend = getURLParameterNew('numberKeywordToSend');

var getUrlParameter = function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),sURLVariables = sPageURL.split('&'),sParameterName,i;
	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};
var inviteFriends = getUrlParameter('invite');
var planOrder = getUrlParameter('plan');

var coupon = getUrlParameter('coupon');
var fromSource = getUrlParameter('fromSource');

$('#select-plan-1 > option').each(function() {
	if($(this).val() == planOrder){
		$(this).prop('selected', true);
	}
});

var startFreeComponent = function () {
	return {
		initHandleSelectPlan: function () {
			var eSelect, eItem;

			eSelect = $('#select-plan-1');
			eItem = $('.content-st .item');

			eSelect.bind('change', function(event) {
				eItem.hide();
				$('#' + $(this).val()).show();
			});
		},
		heightHandleSecurePassword: function () {
			$(window).bind('load resize', function(event) {
				var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
				var hBoxParent = $('#parent-height-follow').outerHeight();

				if (width < 960) {
					$('#implicitly-cal-height').css('height', 'auto');		            
				} else {
					$('#implicitly-cal-height').css('height', hBoxParent);
				}
			});
		},
		init: function () {
			this.initHandleSelectPlan();
			//this.heightHandleSecurePassword();
		}
	}
}();

// jQuery(document).ready(function($) {
	
// });

window.myOpts = $("option.annually-plan").detach();

$('#month-year-switch').on('click', function(){
	// $('#select-plan-1').prop('selectedIndex',0);

	$("#select-plan-1").val($("#no-plan-selected").val()).trigger('change');
	$('.triangle-isosceles.left').toggleClass('changed');
	if($(this).is(':checked')){
		$('#is-yearly-plan').val(1);
		$('.select-plan-2-yearly-amount').toggleClass('hidden');
		$('.select-plan-2-monthly-amount').toggleClass('hidden');
		$("#select-plan-1").append(window.myOpts);
		window.myOpts1 = $("option.monthly-plan").detach()
		$('#monthly-plan-text').css('color', '#929292');
		$('#yearly-plan-text').css('color', '#568ca5');
		$('.triangle-isosceles').css('background', '#568ca5');
		$('#monthly-or-yearly-amount').text('Yearly Amount:');

	}
	else{
		$('#is-yearly-plan').val(0);
		$('.select-plan-2-monthly-amount').toggleClass('hidden');
		$('.select-plan-2-yearly-amount').toggleClass('hidden');
		$("#select-plan-1").append(window.myOpts1);
		window.myOpts2 = $("option.annually-plan").detach();
		$('#monthly-plan-text').css('color', '#568ca5');
		$('#yearly-plan-text').css('color', '#929292');
		$('.triangle-isosceles').css('background', '#929292');
		$('#monthly-or-yearly-amount').text('Monthly Amount:');
		var myOpts = $("option.annually-plan").detach();
	}
});

$(document).ready(function() {
	
	if($('#month-year-switch').is(':checked')){
		$('.yearly-bill').show();
		$('.monthly-bill').hide();
		$('#monthly-plan-text').css('color', '#929292');
		$('#yearly-plan-text').css('color', '#568ca5');
		$('.triangle-isosceles').css('background', '#568ca5');
		$('.triangle-isosceles.left').toggleClass('changed');
	}
	else{
		$('.yearly-bill').hide();
		$('.monthly-bill').show();
		$('#monthly-plan-text').css('color', '#568ca5');
		$('#yearly-plan-text').css('color', '#929292');
		$('.triangle-isosceles').css('background', '#929292');
	}

	startFreeComponent.init();

	$('#' + $('#select-plan-1').val()).show();

	var couponValid = 0;

	$("body").on('click', '#start-btn', function(event) {
		event.preventDefault();

		if (coupon == 'freeKit' && fromSource == 'restaurantmarketing') {
			$('#inpPromotionCode').val('FreeKit');
			$('#inpPromotionCode').prop("disabled", true);
			
			$('#divGroupButton').hide();

			var freeKitEmail = $("#emailadd").val();
			/* Coupon Code using FreeKit */
			getCouponCode('FreeKit',freeKitEmail);
		}

		/* Hide section 1 and show section 2 */
		if ($('#signup_form').validationEngine('validate', {scroll: false, focusFirstField : true})) {
			var planid = $("#select-plan-1").val();
			$(".span-plan-price").hide();
			if($('#is-yearly-plan').val()==1){
				$(".annually-plan #span-plan-price-"+planid).show();
			}
			else{
				$(".monthly-plan #span-plan-price-"+planid).show();
			}
			
			if (planid == 1) {
				$(".section-payment").hide();
				$(".payment-method").hide();
				$(".card-support-type").hide();
				$('#purchase-btn').text('GET STARTED');
			} else {
				$(".section-payment").show();
				$(".card-support-type").show();
				$(".payment-method").show();
				$('#purchase-btn').text('PURCHASE');
			}

			$("#select-plan-2").val(planid);

			try{
				var userEmail = $("#emailadd").val();
				var sms_number = $("#sms_number").val();
				var pass = $("#inpPasswordSignup").val();

				$.ajax({
					type: "POST",
					url: '/public/sire/models/cfc/userstools.cfc?method=ValidateUserStep1&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  {
						inpEmail: userEmail,
						inpPhone: sms_number,
						inpPassword: pass
					},
					beforeSend: function( xhr ) {
   					 // $('#processingPayment').show();
   					},					  
   					error: function(XMLHttpRequest, textStatus, errorThrown) { 
   						$('#SignUpButton').show();
   						$('#processingPayment').hide();
   					},					  
   					success:		
   					function(d) 
   					{
						// $('#processingPayment').hide();
						if(d.RXRESULTCODE == -1){
							bootbox.dialog({
								message: d.MESSAGE,
								title: 'Sign Up',
								buttons: {
									success: {
										label: "OK",
										className: "btn btn-success btn-success-custom",
										callback: function() {}
									}
								}
							});
						}
						else{
							$("#section-1").hide(200);
							$("#section-2").show(200);
						}
					} 		
					
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
					message: "Sign Up Fail",
					title: 'Sign Up',
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-success btn-success-custom",
							callback: function() {}
						}
					}
				});
				btnSignUp.prop('disabled',false);				
				return false;
			}
		}
	});

	$("#inpPromotionCode").keyup(function(event) {
		var code = $(this).val();
		if (code.length == 0) {
			couponValid = 0;
			$("#help-block").hide();
			$(".form-group").removeClass('has-error').removeClass('has-ok');
			$("#apply-btn").show();
			$("#remove-btn").hide();
		}
	});

	$("#select-plan-2").change(function(event) {
		var planid = this.value;
		if (planid > 1) {
			$(".section-payment").show();
			$(".card-support-type").show();

			$(".payment-method").show();
			$('#purchase-btn').text('PURCHASE');
		} else {
			$(".section-payment").hide();
			$(".payment-method").hide();
			$(".card-support-type").hide();
			$('#purchase-btn').text('GET STARTED');
		}
		$(".span-plan-price").hide();
		if($('#is-yearly-plan').val()==1){
			$(".annually-plan #span-plan-price-"+planid).show();
		}
		else{
			$(".monthly-plan #span-plan-price-"+planid).show();
		}
	});

	$("body").on('click', '#back-btn', function(event) {
		event.preventDefault();
		$("#section-2").hide(200);
		$("#section-1").show(200);
	});

	$("body").on('click', '#cancel-btn', function(event) {
		event.preventDefault();
		/* Hide section 2 and show section 1 */
		bootbox.dialog({
			message:'Are you sure you want to cancel the registration?',
			title: 'Cancel Registration',
			buttons: {
				success: {
					label: "OK",
					className: "btn green-gd",
					callback: function() {
						location.href = "/";
					}
				},
				cancel: {
					label: "Cancel",
					className: "btn green-cancel",
					callback: function () {

					}
				}
			}
		});
	});

	$("#agree").change(function(event) {
		/* Display start button when click agree checkbox */
		if (this.checked) {
			$("#start-btn").attr('disabled', false);
		} else {
			$("#start-btn").attr('disabled', true);
		}
	});

	$("#signup_form").validationEngine({promptPosition : "topLeft", scroll: false,focusFirstField : true});

	$("#btn-sign-up").click(validate_form);

	$("#sms_number").mask("(000)000-0000");

	$("body").on('click', '#apply-btn', function(event) {
		event.preventDefault();
		var code = $("#inpPromotionCode").val();
		var email = $("#emailadd").val();
		var isYearly = $('#is-yearly-plan').val();
		if(parseInt(isYearly) != 1){
			isYearly = 0;
		}
		getCouponCode(code,email,isYearly);
		
	});

	$("body").on('click', '#remove-btn', function(event) {
		event.preventDefault();
		$("#inpPromotionCode").val('');
		$("#apply-btn").show();
		$("#remove-btn").hide();
		$("#help-block").hide().html('');
		$("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
	});

	function getCouponCode (code,email,isYearly) {
		if (code != '') {
			$.ajax({
				url: '/public/sire/models/cfc/promotion.cfc?method=getActivePromoCode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: 'POST',
				data: {inpCode: code, inpEmail:email, inpIsYearlyPlan: isYearly},
			})
			.done(function(data) {
				if (parseInt(data.RXRESULTCODE) == 1) {
					$("#help-block").html(data.PROMOTION_DISCOUNT);
					$("#help-block").closest('.form-group').addClass('has-ok').removeClass('has-error');
					$("#apply-btn").hide();
					$("#remove-btn").show();
					couponValid = 1;
				} else {
					$("#help-block").html(data.MESSAGE);
					$("#help-block").closest('.form-group').addClass('has-error').removeClass('has-ok');
					couponValid = -1;
					$("#apply-btn").hide();
					$("#remove-btn").show();
				}
				$("#help-block").show();
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		}
	} 

	$("body").on('click', '#purchase-btn', function(event) {
		event.preventDefault();
		if ($("#inpPromotionCode").val() != "") {
			if (couponValid == 0) {
				bootbox.dialog({
					message:'You must apply your coupon code first!',
					title: 'Coupon Code',
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-success btn-success-custom",
							callback: function() {}
						}
					}
				});
			} else if (couponValid == -1) {
				bootbox.dialog({
					message:'Coupon code is not valid',
					title: 'Coupon Code',
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-success btn-success-custom",
							callback: function() {}
						}
					}
				});
			} else {
				validate_form();
			}
		} else {
			validate_form();
		}
	});

	function validate_form(){
		var planid = $("#select-plan-2").val();
		if (planid == 1 || (planid > 1 && $('#payment_form').validationEngine('validate', {scroll: false, focusFirstField : true}))) {	

			var bbSignUpTitle = "Sign Up";
			var accountType = 'personal';
			
			var fname = $("#fname").val();
			var lname = $("#lname").val();
			var userEmail = $("#emailadd").val();
			var sms_number = $("#sms_number").val();
			var pass = $("#inpPasswordSignup").val();
			var confirmPass = $("#inpConfirmPassword").val();
			var btnSignUp = $('#btn-sign-up');
			var pt = $('#select-plan-2').val();
			
			if (pt == 2 && coupon == 'freeKit' && fromSource == 'restaurantmarketing') {
				pt = 5;
			}else if(pt == 3 && coupon == 'freeKit' && fromSource == 'restaurantmarketing'){
				pt = 7;
			}
			else if(pt == 6 && coupon == 'freeKit' && fromSource == 'restaurantmarketing'){
				pt = 6;
			}
			else if(pt == 6){
				pt = 3;
			}
			else if(pt == 3){
				pt = 4;
			}

			var rf = $('#rf').val(); // Referral Code
			var rftype = $('#rftype').val(); 
			
			var pcode = $('#inpPromotionCode').val();

			btnSignUp.prop('disabled',true);

			var cardNumber = $("#cardNumber").val();
			var cardHolderFirstName = $("#cardHolderFirtsName").val();
			var cardHolderLastName = $("#cardHolderLastName").val();
			
			var securityCode = $("#securityCode").val();
			var expireDate = $("#expireMonth").val() + '/' + $("#expireYear").val();
			var address = $("#address").val();
			var city = $("#city").val();
			var state = $("#state").val();
			var zipcode = $("#zipcode").val();
			var isYearlyPlan = $('#is-yearly-plan').val();
			
			var actionType = $("#actionType").val() || 1; // <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
			var affiliatetype=$('#affiliatetype').val() || "0"; 
            var affiliatecode= $('#affiliatecode').val() || "";            
			var ami=$('#AMI').val() || 0; // this is user id off user that we apply affiliate code (Affiliate Marketing Id)      

			if(parseInt(isYearlyPlan) != 1){
				isYearlyPlan = 0;
			}

			try{
				if($("#payment_method_value").val() == 99){
					$.ajax({
						type: "POST",
						url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccountNewPaypal&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						data:  {
							INPMAINEMAIL: userEmail,
							INPTIMEZONE: 31,
							INPACCOUNTTYPE: accountType,
							INPNAPASSWORD: pass,
							INPFNAME: fname,
							INPLNAME: lname,
							INSMSNUMBER: sms_number,
							inpReferralCode: rf,
							inpReferralType: rftype,
							inpPromotionCode: pcode,
							payment_method: 0,
							number: cardNumber,
							cvv: securityCode,
							expirationDate: expireDate,
							firstName: cardHolderFirstName,
							lastName: cardHolderLastName,
							line1: address,
							city: city,
							state: state,
							zip: zipcode,
							country: 'US',
							primaryPaymentMethodId: 1,
							email: userEmail,
							inpPlanId: planid,
							inpIsYearly: isYearlyPlan,
							inpCancelUrlNewAcc: "http://"+window.location.hostname+"/plans-pricing",
							inpReturnlUrlNewAcc: "http://"+window.location.hostname+"/public/sire/models/cfm/paypal_process?action=buy_plan&planid="+planid+"&isyearlyplan="+isYearlyPlan+"&promotioncode="+pcode+"&email="+userEmail+"&password="+pass+"&referralcode="+rf+"&referraltype="+rftype+"&pt="+pt+"&invitefriends="+inviteFriends
						},
						beforeSend: function( xhr ) {
							$('#processingPayment').show();
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							$('#SignUpButton').show();
							$('#processingPayment').hide();
						},					  
						success:		
						function(d) 
						{
							$('#processingPayment').hide();
							btnSignUp.prop('disabled',false);
							if(d.USERID == 1){
								//paypalCheckoutUrl);
								var url_direct = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='+d.RESULT;
								location.href = url_direct;
							}else{
								bootbox.dialog({
									message: d.MESSAGE,
									title: bbSignUpTitle,
									buttons: {
										success: {
											label: "OK",
											className: "btn btn-success btn-success-custom",
											callback: function() {}
										}
									}
								});
								return false;
							}
						} 
					});
				}
				else 
				{
					$.ajax({
						type: "POST",
						url: '/public/sire/models/cfc/userstools.cfc?method=RegisterNewAccountNew&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						data:  {
							INPMAINEMAIL: userEmail,
							INPTIMEZONE: 31,
							INPACCOUNTTYPE: accountType,
							INPNAPASSWORD: pass,
							INPFNAME: fname,
							INPLNAME: lname,
							INSMSNUMBER: sms_number,
							inpReferralCode: rf,
							inpReferralType: rftype,
							inpPromotionCode: pcode,
							payment_method: 0,
							number: cardNumber,
							cvv: securityCode,
							expirationDate: expireDate,
							firstName: cardHolderFirstName,
							lastName: cardHolderLastName,
							line1: address,
							city: city,
							state: state,
							zip: zipcode,
							country: 'US',
							primaryPaymentMethodId: 1,
							email: userEmail,
							inpPlanId: planid,
							inpIsYearly: isYearlyPlan,
							inpAffiliateCode:affiliatecode,
							inpAffiliateType: affiliatetype,
							inpAMI:ami,
							inpPaymentMethod : $("#payment_method_value").val()
						},
						beforeSend: function( xhr ) {
							$('#processingPayment').show();
						},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							$('#SignUpButton').show();
							$('#processingPayment').hide();
						},					  
						success:		
						function(d) 
						{
							$('#processingPayment').hide();
							btnSignUp.prop('disabled',false);
							if(d.RESULT != "SUCCESS"){
								bootbox.dialog({
									message: d.MESSAGE,
									title: bbSignUpTitle,
									buttons: {
										success: {
											label: "OK",
											className: "btn btn-success btn-success-custom",
											callback: function() {}
										}
									}
								});
								return false;
							}else{
								if (pt != '' && rf != '') {
									window.location.href = '/thankyou?pt=' + pt + '&rf=' + rf + '&type=' + rftype;
								} 
								else if(pt != '' && rf == '') {
									window.location.href = '/thankyou?pt=' + pt;	
								} 
								else if(inviteFriends === "true") {
									window.location.href = 'thankyou?invite=true';
								} 
								else {
									window.location.href = 'thankyou';
								}
							}
						} 		
						
					});
				}
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.dialog({
					message: "Sign Up Fail",
					title: bbSignUpTitle,
					buttons: {
						success: {
							label: "OK",
							className: "btn btn-success btn-success-custom",
							callback: function() {}
						}
					}
				});
				btnSignUp.prop('disabled',false);				
				return false;
			}
		}	
	}

	$('#inpPromotionCode').keypress(function( e ) {
	    if(e.which === 32) 
	        return false;
	});	

	//Paypal
	// $('.select_payment_method').click(function(){
	// 	if($(this).val() == 0){
	// 		$("#payment_method_value").val(0);
	// 		$('.section-payment').hide();
	// 		$(".card-support-type").hide();
	// 	}
	// 	else{
	// 		$("#payment_method_value").val(1);
	// 		$('.section-payment').show();
	// 		$(".card-support-type").show();
	// 	}
	// });
	
	$.ajax({
		method: 'POST',
		url: '/public/sire/models/cfc/userstools.cfc?method=GetPaymentMethodSetting&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',
		data:{},
		dataType: 'json',
		success: function(data) {
			if (data && data.RXRESULTCODE == 1) {
				if(data.RESULT == 2){
					$("#payment_method_value").val(2);
					// $('.section-payment').hide();
					// $(".card-support-type").hide();
					$('.section-payment').show();
					$(".card-support-type").show();
				}else if(data.RESULT == 1){
					$("#payment_method_value").val(1);
					$('.section-payment').show();
					$(".card-support-type").show();
				}else if(data.RESULT == 3){
					$("#payment_method_value").val(3);
					$('.section-payment').show();
					$(".card-support-type").show();
				}else if(data.RESULT == 4){
					$("#payment_method_value").val(4);
					$('.section-payment').show();
					$(".card-support-type").show();
				}

			}
		}
	});
});

function isValidPassword(field, rules, i, options){
			
	var password = field.val();
	if (password.length < 8)
	{
		return options.allrules.validatePassword.alertTextLength;

	}
	
	var hasLetter = /[a-z]/i.test(password);
	if (!hasLetter)
	{
		return options.allrules.validatePassword.alertTextNotFound;
	}
	
/* Reduced this requirement to any case letter2017-08-18 Lee Peterson
	var hasUpperCase = /[A-Z]/.test(password);
	if (!hasUpperCase)
	{
		return options.allrules.validatePassword.alertTextUpperCase;
	}

	var hasLowerCase = /[a-z]/.test(password);
	if (!hasLowerCase)
	{
		return options.allrules.validatePassword.alertTextLowerCase;
	}
*/
	
	var hasNumbers = /\d/.test(password);
	if (!hasNumbers)
	{
		return options.allrules.validatePassword.alertTextNumbers;

	}

/* Removed this requirement	2017-08-18 Lee Peterson
	var hasNonalphas = /\W/.test(password);
	if (!hasNonalphas)
	{
		return options.allrules.validatePassword.alertTextNoAlpha;
	}
*/
	
}