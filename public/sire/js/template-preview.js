

$(document).ready(function(){

	$( document ).on( "click", ".btn-preview-campaign", function(event) {
		var templateid = $(this).data('template-id');
		
		try{
			$.ajax({
				type: "GET",
				url: '/public/sire/models/cfm/template_preview.cfm?templateid='+templateid,   
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					bootbox.alert('Get preview Fail', function() {});
					$('#processingPayment').hide();
				},					  
				success:function(d){
					$('#processingPayment').hide();
					$('#previewCampaignModal .modal-body').html(d);
					$('#previewCampaignModal').modal('show');
				}
			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.alert('Get preview Fail', function() {});
		}
	});
	
	
});