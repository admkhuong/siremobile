(function($){

	var device_not_reg = $('#device_not_reg_wrapper');
	var verification_code = $('#verification_code');
	var invalid_verification_code = $('#invalid_verification_code');
	var device_registered = $('#device_registered');
	var answer_security_question = $('#answer_security_question');

	$("#device_not_reg_form").submit(send_verification_code);
	$("#btn_device_not_reg").click(send_verification_code);
	$("#btn_device_not_reg").on('touchend', function(){
		$('#inpMFACode').focus();
	});

	$("#verification_code_form").submit(check_verification_code);
	$("#btn_verification_code").click(check_verification_code);

	$("#btn_answer_question").click(function(){
		invalid_verification_code.hide();
		answer_security_question.show();
	});

	$("#btn_verify_security_question").click(verify_security_question);
	$("#answer_security_question_form").submit(verify_security_question);

	$("#btn_back_to_dev_not_reg").click(function(){
		$('#verification_code .your-device').addClass('hidden');
		$('#verification_code .your-email').addClass('hidden');
		$('#verification_code_form')[0].reset();
		verification_code.hide();
		invalid_verification_code.hide();
		device_not_reg.show();
	});

	$("#btn_back_to_verification_code").click(function(){
		$('#verification_code_form')[0].reset();
		invalid_verification_code.hide();
		verification_code.show();
		device_not_reg.hide();
	});


	// If user check default -> auto send MFA code & display verification code form
	if(check_default && check_default > 1){
		device_not_reg.hide();

		/* https://setaintl2008.atlassian.net/browse/SIRE-691 */
		/*$("#btn_back_to_dev_not_reg").hide();*/
		$("#btn_back_to_dev_not_reg").text("Resend");
		/* https://setaintl2008.atlassian.net/browse/SIRE-691 */

		$("#btn_verification_code_cancel").show();

		try{
			$.ajax({
			type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
			url: '/public/sire/models/cfc/userstools.cfc?method=SendMFACode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:{},
			beforeSend: function( xhr ) {
				$('#processingPayment').show();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#processingPayment').hide();
				bootbox.alert('Send Code Fail', function() {
					window.location.href = '/';
				});
			},
			success:
				function(d) {
					$('#processingPayment').hide();
					$('#btn_device_not_reg').show();

					if(d.RESULT == 'SUCCESS'){
						switch(check_default) {
							case 1:
								if ($('#optionsRadios1').is(':checked')) {
									$('#verification_code .your-device').removeClass('hidden');
								}
								if ($('#optionsRadios2').is(':checked')) {
									$('#verification_code .your-email').removeClass('hidden');
								}
								break;
							case 2:
								$('#verification_code .your-email').removeClass('hidden');
								break;
							case 3:
								$('#verification_code .your-device').removeClass('hidden');
								break;
						}

						//CALL verification_code
						device_not_reg.hide();

						$('#device_not_reg_form')[0].reset();

						verification_code.show();
						$('#inpMFACode').focus();
					}
					else
					{
						if(d.RESULT == 'Block'){
							$('#InfoModalMFABlock').modal('show');
						}else{
							$('#ErrorSendSMSMFA').removeClass('hidden');
							$('#InfoModalMFAFail').modal('show');
							// bootbox.alert('Send Code Fail', function() {window.location.href = '/';});
						}

					}
				}

			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.alert('Send Code Fail', function() {window.location.href = '/';});
		}
	}


	$("#verification_code_form").validationEngine('attach', {promptPosition : "topLeft", scroll: false});
	$("#answer_security_question_form").validationEngine('attach', {promptPosition : "topLeft", scroll: false});

	function send_verification_code(event){

		event.preventDefault();

		var form_data = $('#device_not_reg_form').serialize();

		try{
			$.ajax({
			type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
			url: '/public/sire/models/cfc/userstools.cfc?method=SendMFACode&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data: form_data,
			async:false,
			beforeSend: function( xhr ) {
				$('#btn_device_not_reg').hide();
				$('#processingPayment').show();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#processingPayment').hide();
				bootbox.alert('Send Code Fail', function() {});
				$('#btn_device_not_reg').show();
			},
			success:
				function(d) {
					$('#processingPayment').hide();
					$('#btn_device_not_reg').show();

					if(d.RESULT == 'SUCCESS'){
						switch(check_default) {
							case 1:
								if ($('#optionsRadios1').is(':checked')) {
									$('#verification_code .your-device').removeClass('hidden');
								}
								if ($('#optionsRadios2').is(':checked')) {
									$('#verification_code .your-email').removeClass('hidden');
								}
								break;
							case 2:
								$('#verification_code .your-email').removeClass('hidden');
								break;
							case 3:
								$('#verification_code .your-device').removeClass('hidden');
								break;
						}

						//CALL verification_code
						device_not_reg.hide();

						$('#device_not_reg_form')[0].reset();

						$("#btn_back_to_dev_not_reg").text("Back");
						verification_code.show();
						$('#inpMFACode').focus();

					}
					else{

						if(d.RESULT == 'Block'){
							$('#InfoModalMFABlock').modal('show');
						}else{
							$('#ErrorSendSMSMFA').removeClass('hidden');
							$('#InfoModalMFAFail').modal('show');
							// bootbox.alert('Send Code Fail', function() {window.location.href = '/';});
						}
					}
				}

			});
		}catch(ex){
			$('#processingPayment').hide();
			bootbox.alert('Send Code Fail', function() {});
			$('#btn_device_not_reg').show();
		}
	}

	function check_verification_code(event){
		event.preventDefault();

		if ($('#verification_code_form').validationEngine('validate')) {
			var form_data = $('#verification_code_form').serialize();
			try{
				$.ajax({
				type: "POST", //Posts data as form data rather than on query string and allows larger data transfers than URL GET does
				url: '/public/sire/models/cfc/userstools.cfc?method=ValidateMFA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				data: form_data,
				beforeSend: function( xhr ) {
					$('.btn-group-verification_code').hide();
					$('#processingPayment').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#processingPayment').hide();
					bootbox.alert('Verify Code Fail', function() {});
					$('.btn-group-verification_code').show();
				},
				success:
					function(d) {
						$('#processingPayment').hide();
						$('.btn-group-verification_code').show();
						if(d.RXRESULTCODE == 1){
							// log IP Address of user login first into database
                                $.ajax({
                                    url: '/public/sire/models/cfc/userstools.cfc?method=LogIPAddress&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
                                    method: 'GET',
                                    beforeSend: function(xhr) {},
                                    success: function(DATA)
                                    {
                                        //goto success page
								verification_code.hide();
								device_registered.show();
                                    }
                               });
						}
						else
                              {
							//goto invalid page
							verification_code.hide();
							invalid_verification_code.show();
						}
						window.scrollTo(0,0);
					}
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.alert('Verify Code Fail', function() {});
				$('.btn-group-verification_code').show();
			}
		}
	}

	function verify_security_question(event){

		event.preventDefault();

		if ($('#answer_security_question_form').validationEngine('validate')) {
			var form_data = {};
			var input_array = $( "#answer_security_question_form .input_answer");
			//code
   			var arr_question_ids = [];

			input_array.each(function(i) {
   				var name = $(this).attr('name');
				var question_name = name.split('_');
				question_name = question_name[1];
				form_data[question_name] = $(this).val();
				arr_question_ids.push(question_name);
			});

			try{
				$.ajax({
				type: "POST",
				url: '/public/sire/models/cfc/userstools.cfc?method=ValidateSecurityQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				dataType: 'json',
				data: {json_answer_data:JSON.stringify(form_data),arr_question_ids:JSON.stringify(arr_question_ids)},
				beforeSend: function( xhr ) {
					$('#processingPayment').show();
					$('.btn-group-answer-security-question').hide();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$('#processingPayment').hide();
					bootbox.alert('Your Answers Are Incorrect', function() {});
					$('.btn-group-answer-security-question').show();
				},
				success:
					function(d) {
						$('#processingPayment').hide();
						$('.btn-group-answer-security-question').show();
						if(d.RESULT == "SUCCESS"){
							//goto success page
							answer_security_question.hide();
							device_registered.show().find('h4 strong').text(
								'Thank you! The answer security question you entered has been verified and the device has been authenticated.'
							);
						}
						else{
							bootbox.alert(d.MESSAGE, function() {});
						}
						window.scrollTo(0,0);
					}
				});
			}catch(ex){
				$('#processingPayment').hide();
				bootbox.alert('Verify Security Question Fail', function() {});
				$('.btn-group-answer-security-question').show();
			}
		}
		return false;
	}

	window.addEventListener('load', function() {
		var textInput = document.querySelector('#inpMFACode');

		FastClick.attach(document.body);

		Array.prototype.forEach.call(document.getElementsByClassName('triggerfocus'), function(testEl) {
			testEl.addEventListener('touchend', function() {
				textInput.focus();
			}, false)
		});
	}, false);

})(jQuery);
