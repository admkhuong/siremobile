(function($){
	/* get paramaeters from url */
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

	/* cancel user waitlist */
	var inputToken = getUrlParameter('token');
	$('#cancel-customer-waitlist').click(function(event) {
		event.preventDefault();
		var url_update_waitlist_status = '/public/sire/models/cfc/waitlistapp.cfc?method=UpdateWaitlistStatusByToken&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true';
		$.ajax({
			url: url_update_waitlist_status,
			type: 'POST',
			dataType: 'JSON',
			data: {inpCustomerToken: inputToken},
			success: function(data){
				if(data.RXRESULTCODE == 1){
					window.location.href = '/waitlist-customer-cancel';
				}
				else{
					window.location.href = '/error-page';
				}
			}
		});
		
	});
})(jQuery);
