<cfcomponent>
	<cfinclude template="/public/sire/configs/paths.cfm">

	<cffunction name="PushNotification" access="remote" output="false" hint="Push notification message to device">
		<cfargument name="PAYLOAD" required="yes" default="" type="string">
		<cfargument name="DEVICETOKEN" required="yes" default="" type="string">
		<cfset var APNSService = StructNew()>
		<cfset var PUSHRESULT = StructNew()>


        <!--- CREATE APNS SERVICE --->
        <cfset var fileCert = _CertPath>
        <cfset var fileCertPass = _CertPass>

        <cfif _APNS_SANDBOX EQ 1>
            <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withSandboxDestination().build()>    
        <cfelse>
            <cfset var APNSService = createObject("java", "com.notnoop.apns.APNS").newService().withCert(fileCert, fileCertPass).withProductionDestination().build()>
        </cfif>

		<cfset PUSHRESULT = APNSService.push(DEVICETOKEN, PAYLOAD)>
	</cffunction>
</cfcomponent>