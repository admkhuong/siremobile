<cfcomponent>
    <cffunction name="QueryStringToStruct" output="false">
        <cfargument name="QueryString" required="yes" type="string">
        <cfset var myStruct = StructNew()>
        <cfset var QueryStringParts = ''/>
        <cfset var i = ''/>
        
        <cfloop list="#QueryString#" delimiters="&" index="i">
            <cfset QueryStringParts = ListToArray(i, "=")>
            <cfif isDefined("QueryStringParts[2]")>
                <cfset structInsert(myStruct, Trim(QueryStringParts[1]),Trim(QueryStringParts[2]))>
            <cfelse>
                <cfset structInsert(myStruct, Trim(QueryStringParts[1]),"")>
            </cfif>
        </cfloop>

        <cfreturn myStruct />
    </cffunction>

    <cffunction name="EpochTimeToLocalDate" hint="converting Epoch/Unix timestamps to date objects">
        <cfargument name="inpEpoch" required="yes" type="string">
        <cfset var d = ''/>
        <cfif isvalid("integer", inpEpoch)>
            <cfset d = DateAdd("s", inpEpoch, DateConvert("utc2Local", "January 1 1970 00:00"))>
        <cfelse>
            <cfset d = DateAdd("s", inpEpoch/1000, DateConvert("utc2Local", "January 1 1970 00:00"))>
        </cfif>
        <cfreturn d />
    </cffunction>
   
</cfcomponent>    