<cfcomponent>
	
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    
    
    <cffunction name="GetUSReachList" access="remote" output="false" hint="Read the current list of carriers that Sire has access to from the database." >
		        
        <cfset var getPlan = '' />
        <cfset var dataout = '' />
           		
		<cfquery name="getPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                OperatorId_int, 
                Country_vch,
                Operator_vch,
                OperatorAlias_vch,
                CarrierGroup_vch
            FROM 
                sms.carrierlist
            WHERE
                Country_vch = 'USA'

        </cfquery>
       
        <cfset dataout =  getPlan>        
       
		<cfreturn dataout />
	</cffunction>
    
    
    
</cfcomponent>