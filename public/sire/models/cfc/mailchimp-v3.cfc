<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/public/sire/configs/paths.cfm">
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfset LOCAL_LOCALE = "English (US)">

     <cffunction name="Subscribed" access="public" output="true" hint="MailChimp API">
     	<cfargument name="inpEmailString" required="yes" default="">
     	<cfargument name="inpFirstName" required="no" default="">
     	<cfargument name="inpLastName" required="no" default="">
         <cfargument name="inpPhone" required="no" default="">
     	<cfargument name="inpLast4CC" required="no" default="">
     	
     	

     	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MAILCHIMPFILECONTENT = "" />
        <cfset var objectFilecontent={}>
        <cfset var subscriberId="">
        <cfset var result={}>
        <cfset var mailChimpURL= "#_MailChimpApiUrl#lists/#_MailChimpNewList#/members/">
	   	<cfset var _Authorization = 'Basic ' & ToBase64('anystring:' & _MailChimpApiKey)> 
        <cfset var dataJson={
            "email_address": "#arguments.inpEmailString#",
            "status": "subscribed",
            "merge_fields": {
                "FNAME": "#arguments.inpFirstName#",
                "LNAME": "#arguments.inpLastName#",
                "LAST4CC":"#arguments.inpLast4CC#",
                "PHONE":"#arguments.inpPhone#"
            }
        }>
        <cftry>

		<cfhttp url="#mailChimpURL#" result="result" method="POST" charset="utf-8" timeout='3'>
			<cfhttpparam type="header" name="Content-type" value="application/json">
			<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
            <cfhttpparam type="body" value="#SerializeJSON(dataJson)#">
		</cfhttp>
				

		<cfif result.status_code EQ 200>							            
            <cfset dataout.RXRESULTCODE = 1>  
            <cfset dataout.MESSAGE = "Update MailChimp Success!"> 
            <cfset dataout.ERRMESSAGE = "">
            <cfset dataout.MAILCHIMPFILECONTENT = result.filecontent>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1>  
            <cfset dataout.MESSAGE = "Update MailChimp Failed!"> 
            <cfset dataout.ERRMESSAGE = ""> 	
            <cfset dataout.MAILCHIMPFILECONTENT = result.filecontent> 	        				     
		</cfif>
        <cfset objectFilecontent=deserializeJSON(dataout.MAILCHIMPFILECONTENT)>
        <cfif structKeyExists(objectFilecontent, "status") AND lcase(objectFilecontent.status) EQ "subscribed">
            <cfset subscriberId=objectFilecontent.id>

        </cfif>
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1>  
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">   
		</cfcatch>
			         
        </cftry>

        <cfreturn dataout>
    </cffunction>    
</cfcomponent>