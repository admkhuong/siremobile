<!---
	Name: mailChimpAPIs

	Argument:
				+ List ID (hard-coded in config)
				+ Email Address (* required)
				+ First Name
				+ Last Name
				+ Account Type (Free, Individual, Pro, SMB)
				+ Sign Up Date
				+ Last Login Date
				+ Last Activity Date (the last time they ran a campaign)
				+ Status (Active or Cancelled)
				+ Date Cancelled
				+ Phone Number

	There are 6 actions:
	1. New register user
		+ Email Address (*)
		+ First Name
		+ Last Name
		+ Account Type: free
		+ Sign Up Date
		+ Status: active

	2. Log In
		+ Email Address (*)
		+ Last Login Date

	3. Update Account Plan (notice: take a look at "auto downgrade")
		+ Email Address (*)
		+ Account Type

	4. Update Account Information
		+ Email Address (*)
		+ First Name
		+ Last Name

	5. Active/Deactive Account
		+ Email Address (*)
		+ Status: active | cancelled
		(
		if Status = "cancelled",
		+ Date Cancelled
		)

	6. Run Campaign
		+ Email Address (*)
		+ Last Activity Date
--->

<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfinclude template="/public/sire/configs/paths.cfm">
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfset LOCAL_LOCALE = "English (US)">

     <cffunction name="mailChimpAPIs" access="public" output="true" hint="MailChimp API">
     	<cfargument name="InpEmailString" required="yes" default="">
     	<cfargument name="InpFirstName" required="no" default="">
     	<cfargument name="InpLastName" required="no" default="">
     	<cfargument name="InpAccountType" required="no" default="">
     	<cfargument name="InpSignupDate" required="no" default="">
     	<cfargument name="InpLastLoginDate" required="no" default="">
     	<cfargument name="InpLastActiveDate" required="no" default="">
     	<cfargument name="InpStatus" required="no" default="">
     	<cfargument name="InpCancelDate" required="no" default="">
		<cfargument name="InpPhoneNumber" required="no" default="">

     	<!--- Fixed Parameters --->
     	<cfset var apiDataMerge = {}/>

     	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.MAILCHIMPFILECONTENT = "" />
	   	<cfset var emailHash = ''/>
	   	<cfset var dataJson = {}/>
	   	<cfset var result = ''/>
	   	<cfset var apiUrl = ''/>
	   	<cfset var _Authorization = ''/>
	   	<cfset var campaignInfo = ''/>
	   	<cfset var newMember = ''/>
	   	<cfset var mail_chimp_list_id = _MailChimpDefaultListId />
	   	<cfset var api_key = _MailChimpApiKey/>
	   	<cfset var apiroot = _MailChimpApiUrl/>

		<cfif arguments.InpEmailString NEQ ''>
			<cfset dataJson['email_address'] = arguments.InpEmailString >
		</cfif>

		<cfif arguments.InpFirstName NEQ ''>
			<cfset apiDataMerge['FNAME'] = arguments.InpFirstName >
		</cfif>

		<cfif arguments.InpLastName NEQ ''>
			<cfset apiDataMerge['LNAME'] = arguments.InpLastName >
		</cfif>

		<cfif arguments.InpAccountType NEQ ''>
			<cfset apiDataMerge['ACCOUNTYPE'] = arguments.InpAccountType >
		</cfif>

		<cfif arguments.InpSignupDate NEQ ''>
			<cfset apiDataMerge['SIGNUPDATE'] = arguments.InpSignupDate >
		</cfif>

		<cfif arguments.InpLastLoginDate NEQ ''>
			<cfset apiDataMerge['LASTLOGIN'] = arguments.InpLastLoginDate >
		</cfif>

		<cfif arguments.InpLastActiveDate NEQ ''>
			<cfset apiDataMerge['LASTACTIV'] = arguments.InpLastActiveDate >
		</cfif>

		<cfif arguments.InpStatus NEQ ''>
			<cfset apiDataMerge['STATUS'] = arguments.InpStatus >
		</cfif>

		<cfif arguments.InpCancelDate NEQ ''>
			<cfset apiDataMerge['CANCELLED'] = arguments.InpCancelDate >
		</cfif>

		<cfif arguments.InpPhoneNumber NEQ ''>
			<cfset apiDataMerge['PHONE'] = arguments.InpPhoneNumber >
		</cfif>

		<cfset _Authorization = 'Basic ' & ToBase64('anystring:' & api_key)>
		<cftry>

		<cfset apiUrl = "#apiroot#lists/#mail_chimp_list_id#"/>

		<cfhttp url="#apiUrl#" result="result" method="GET" charset="utf-8" timeout='3'>
			<cfhttpparam type="header" name="content-type" value="application/json">
			<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
		</cfhttp>

		<cfset campaignInfo = DeserializeJSON(result.filecontent)>

		<cfif result.status_code EQ 200 AND StructKeyExists(campaignInfo, 'id')>

				<cfset emailHash = hash(arguments.InpEmailString,'MD5')/>

				<cfset dataJson['status_if_new'] = 'subscribed'/>
				<cfset dataJson['merge_fields'] = apiDataMerge />

				<cfset var apiUpdateUrl = "#apiroot#lists/#mail_chimp_list_id#/members/#emailHash#"	/>

				<cfhttp url="#apiUpdateUrl#" result="result" method="PUT" charset="utf-8" timeout='3'>
					<cfhttpparam type="header" name="content-type" value="application/json">
					<cfhttpparam type="header" name="Authorization" value="#_Authorization#">
					<cfhttpparam type="body" value="#SerializeJSON(dataJson)#">
				</cfhttp>

				<cfset newMember = DeserializeJSON(result.filecontent)>
				<cfif result.status_code EQ 200>
					<cfset dataout.RXRESULTCODE = 1>
	            	<cfset dataout.MESSAGE = "Update MailChimp Success!">
	            	<cfset dataout.ERRMESSAGE = "">
	            	<cfset dataout.MAILCHIMPFILECONTENT = result.filecontent>
				<cfelse>
					<cfset dataout.RXRESULTCODE = -1>
	            	<cfset dataout.MESSAGE = "Update MailChimp Failed!">
	            	<cfset dataout.ERRMESSAGE = "">
	            	<cfset dataout.MAILCHIMPFILECONTENT = result.filecontent>
				</cfif>

		</cfif>

		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">
		</cfcatch>

        </cftry>

        <cfreturn dataout>
    </cffunction>
</cfcomponent>
