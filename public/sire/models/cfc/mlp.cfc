<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
    <cfset LOCAL_LOCALE = "English (US)">

    <cffunction name="ReadCPPByFilter" access="public" output="true" hint="Read CPP">
    	<cfargument name="inpUUID" type="string" required="yes">
        <cfargument name="inpCPPID" type="number" required="yes">
        <cfargument name="inpURL" type="string" required="yes">
        <cfargument name="inpActive" type="string" required="yes">

    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.DATA = "" />
	   	<cfset dataout.URLCSS = ""/>

	   	<cfset var readCPP = ''>
        <cfset var checkActive = 0>
        <cfset var xmlDataControl = '' />

	   	<cftry>
		   	<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
	            SELECT 
	            	XMLDataString,status_int,cppxUUID_vch,css_custom,css_external_link,css_type
	            FROM	
	                simplelists.cppx_data
	            WHERE  
                    <cfif inpUUID NEQ ''>
                        cppxUUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUUID#">
                    <cfelseif inpCPPID GT 0>
                        ccpxDataId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPPID#">
                    <cfelseif inpURL NEQ ''>
                         cppxURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpURL#">
                    </cfif>
                    <!---
	                <cfif structKeyExists(arguments, "inpActive") AND arguments.inpActive EQ 1>
                        AND    
                        status_int = 1        
                    </cfif>
                    --->
		        LIMIT 1
	               
	        </cfquery>
	        <cfif readCPP.RecordCount GT 0>
	        	<cfif readCPP.css_type EQ 1>
	        		<cfset dataout.URLCSS = "#readCPP.css_custom#"/>
	        	<cfelse>
	        		<cfset dataout.URLCSS = "#readCPP.css_external_link#"/>
	        	</cfif>
	        	
	        	<cfloop query="#readCPP#">
	        		<cfinvoke component="session.sire.models.cfc.mlp" method="ParseCPPXXmlData" returnvariable="xmlDataControl">
	   		 			<cfinvokeargument name="xmlData" value="#XMLDataString#">
	   				</cfinvoke>
	        	 	<cfset dataout.DATA = xmlDataControl>
	        	 	<cfset dataout.CPPID = cppxUUID_vch />
                    <cfset checkActive = status_int >
	        	</cfloop>	

                <!--- CHECK ACTIVE --->
                <cfif arguments.inpActive EQ 1> 
                    <cfif checkActive NEQ 1>
                        <cfset dataout.RXRESULTCODE = 0 />
                        <cfset dataout.MESSAGE = 'This MLP is not active.'>    
                        <cfreturn dataout>  
                    </cfif>
                </cfif>

                <cfif arrayLen(dataout.DATA) GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />    
                <cfelse>
                    <cfset dataout.RXRESULTCODE = 0 />
                    <cfset dataout.MESSAGE = 'This MLP has no objects.'>    
                </cfif>       

	        <cfelse>
	        	<cfset dataout.MESSAGE = 'Can not get this MLP data'>
	        </cfif>	

		    <cfcatch>
		    	<cfset dataout.MESSAGE = 'Can not get this MLP data'>
		    	<cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
		    </cfcatch>    	
	    </cftry>    	
	    <cfreturn dataout>
    </cffunction>

    <cffunction name="saveCPPData" access="remote" output="true" hint="save MLP input data">
    	<cfargument name="inpCPPUUID" required="yes">
		<cfargument name="VoiceNumberObject" default="">
		<cfargument name="SMSNumberObject" default="">
		<cfargument name="EmailObject" default="">
		<cfargument name="SubcriberList" default="">


    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.CPPID = "0" />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.ERRORINDEX = "" />
	   	<cfset dataout.ERRORTYPE = "" />
	   	<cfset dataout.ERRORDATA = "" />

	   	<cfset var AddContactResult = ''>
	   	<cfset var readCPP = ''>

        <cfset var SIndex = '' />
        <cfset var VIndex = '' />
        <cfset var EIndex = '' />
        <cfset var INPCONTACTSTRING = '' />
        <cfset var SMSNumber = '' />
        <cfset var VoiceNumber = '' />
        <cfset var Email = '' />
        <cfset var GroupIDToAdd = '' />
        <cfset var EmailStr = '' />

	   	<cftry>
	   		<!--- VALIDATE AGAIN --->
	    	<cfif isArray(SMSNumberObject) AND arrayLen(SMSNumberObject) GT 0>
	    		<cfset SIndex = 0>											
				<cfloop array="#SMSNumberObject#" index="SMSNumber">

                	<cfif len(SMSNumber) GT 0 >
						<cfif NOT isvalid('telephone', SMSNumber)>
                            <cfset dataout.MESSAGE = "SMS Number is not a valid US telephone number !"> 
                            <cfset dataout.RXRESULTCODE = -1>
                            <cfset dataout.ERRORINDEX = SIndex>
                            <cfset dataout.ERRORTYPE = "SMS">
                            <cfset dataout.ERRORDATA = SMSNumber>
                            <cfreturn dataout />
                        </cfif>
                    </cfif>
					<cfset SIndex++>
				</cfloop>	
	    	</cfif>

	    	<cfif isArray(VoiceNumberObject) AND arrayLen(VoiceNumberObject) GT 0>
				<cfset VIndex = 0>												
				<cfloop array="#VoiceNumberObject#" index="VoiceNumber">																								
					<cfif len(VoiceNumber) GT 0	>
						<cfif NOT isvalid('telephone', VoiceNumber)>
                            <cfset dataout.MESSAGE = "Voice Number is not a valid US telephone number !"> 
                            <cfset dataout.RXRESULTCODE = -1>
                            <cfset dataout.ERRORINDEX = VIndex>
                            <cfset dataout.ERRORTYPE = "VOICE">
                            <cfset dataout.ERRORDATA = VoiceNumber>
                            <cfreturn dataout />
                        </cfif>
                    </cfif>
					<cfset VIndex++>
				</cfloop>
	    	</cfif>

	    	<cfif isArray(EmailObject) AND arrayLen(EmailObject) GT 0>
	    		<cfset EIndex = 0>												
				<cfloop array="#EmailObject#" index="Email">								
					<cfif len(Email) GT 0 >
						<cfif NOT isvalid('email', Email)>
                            <cfset dataout.MESSAGE = "Email Address invalid !"> 
                            <cfset dataout.RXRESULTCODE = -1>
                            <cfset dataout.ROWCOUNT = 1>
                            <cfset dataout.ERRORINDEX = EIndex>
                            <cfset dataout.ERRORTYPE = "EMAIL">
                            <cfset dataout.ERRORDATA = Email>
                            <cfreturn dataout />
                        </cfif>
                    </cfif>                                
					<cfset EIndex++>
				</cfloop>	
	    	</cfif>

	    	<!--- GET CPP DATA --->

	    	<cfquery name="readCPP" datasource="#Session.DBSourceREAD#" >
	            SELECT 
	            	userId_int,ccpxDataId_int
	            FROM	
	                simplelists.cppx_data
	            WHERE  
	                cppxUUID_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCPPUUID#">
		        AND    
		        	status_int >= 0    
		        LIMIT 1
	               
	        </cfquery>
	        <cfif readCPP.RecordCount GT 0>
	        	<cfloop query="#readCPP#">
	        		<cfset var userId_int = userId_int>
                    <cfset var ccpxDataId = ccpxDataId_int>
	        	</cfloop>	
	        <cfelse>
        		<cfset dataout.MESSAGE = "This MLP not found or inactived"> 
        		<cfset dataout.RXRESULTCODE = -1>
        		<cfreturn dataout />
	        </cfif>	

	    	<!--- LOOP TO SAVE SMS--->
            <cfif isArray(SMSNumberObject) AND arrayLen(SMSNumberObject) GT 0>												
				<cfloop array="#SMSNumberObject#" index="smsNumber">
                    <cfset INPCONTACTSTRING = smsNumber>    
                    <!--- Add to each group in list --->
                    <cfloop array="#arguments.SubcriberList#" index="GroupIDToAdd">
                    
                    	<!--- Validate GroupID--->
                    	<cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>
                    
                            <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
                                <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
                                <cfinvokeargument name="INPCONTACTTYPEID" value="3">
                                <cfinvokeargument name="INPUSERID" value="#userId_int#">
                                <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
								<cfinvokeargument name="INP_CPP_UUID" value="#arguments.inpCPPUUID#">
                                <cfinvokeargument name="INP_CPPID" value="#ccpxDataId#">
                            </cfinvoke>
                        </cfif>    
						
					</cfloop>
             	</cfloop>
			</cfif>

            <!--- LOOP TO SAVE EMAIL--->
            <cfif isArray(EmailObject) AND arrayLen(EmailObject) GT 0>                                              
                <cfloop array="#EmailObject#" index="EmailStr">
                    <cfset INPCONTACTSTRING = EmailStr>    
                    <!--- Add to each group in list --->
                    <cfloop array="#arguments.SubcriberList#" index="GroupIDToAdd">
                    
                        <!--- Validate GroupID--->
                        <cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>
                    
                            <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
                                <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
                                <cfinvokeargument name="INPCONTACTTYPEID" value="2">
                                <cfinvokeargument name="INPUSERID" value="#userId_int#">
                                <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
                                <cfinvokeargument name="INP_CPP_UUID" value="#arguments.inpCPPUUID#">
                                <cfinvokeargument name="INP_CPPID" value="#ccpxDataId#">
                            </cfinvoke>
                        </cfif>    
                    </cfloop>
                </cfloop>
            </cfif> 

            <!--- LOOP TO SAVE VOICE NUMBER--->
            <cfif isArray(VoiceNumberObject) AND arrayLen(VoiceNumberObject) GT 0>                                              
                <cfloop array="#VoiceNumberObject#" index="VoiceNumber">
                    <cfset INPCONTACTSTRING = VoiceNumber>    
                    <!--- Add to each group in list --->
                    <cfloop array="#arguments.SubcriberList#" index="GroupIDToAdd">
                    
                        <!--- Validate GroupID--->
                        <cfif ISNUMERIC(GroupIDToAdd) AND GroupIDToAdd GT 0>
                            <cfinvoke method="AddContactStringToList" returnVariable="AddContactResult">
                                <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
                                <cfinvokeargument name="INPCONTACTTYPEID" value="1">
                                <cfinvokeargument name="INPUSERID" value="#userId_int#">
                                <cfinvokeargument name="INPGROUPID" value="#GroupIDToAdd#">
                                <cfinvokeargument name="INP_CPP_UUID" value="#arguments.inpCPPUUID#">
                                <cfinvokeargument name="INP_CPPID" value="#ccpxDataId#">
                            </cfinvoke>
                        </cfif>    
                    </cfloop>
                </cfloop>
            </cfif> 

            <cfset dataout.RXRESULTCODE = 1>  
            <cfset dataout.MESSAGE = "Create Contact Success!"> 
            <cfset dataout.ERRMESSAGE = "">  
	   
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1>  
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">
		</cfcatch>    	
	    </cftry>	

	    <cfreturn dataout>
    </cffunction>

    <cffunction name="AddContactStringToList" access="public" output="true" hint="Add phone number to list if not already on it - Assumes no existing unique top level contact already linked - this is stand alone - Only can be called by another method inside this local CFC">
		<cfargument name="INPCONTACTSTRING" required="yes" default="">
        <cfargument name="INPCONTACTTYPEID" required="yes" default="">
        <cfargument name="INPUSERID" required="yes" default="">
        <cfargument name="INPGROUPID" required="no" default="0">
        <cfargument name="INPUSERSPECIFIEDDATA" required="no" default="">
        <cfargument name="INP_SOURCEKEY" required="no" default="">
        <cfargument name="INP_CPPID" required="no" default="">
        <cfargument name="INP_CPP_UUID" required="no" default="">
        <cfargument name="INP_SOCIALTOKEN" required="no" default="">
        <cfargument name="INPFIRSTNAME" required="no" default="">
        <cfargument name="INPLASTNAME" required="no" default="">
		<cfargument name="LANGUAGEPREFERENCE" required="no" default="">
        <cfargument name="INPCDFDATA" required="no" default="">

        <cfif arguments.INPCDFDATA NEQ "">
            <cfset arguments.INPCDFDATA = deserializeJSON(arguments.INPCDFDATA)>
        <cfelse>
            <cfset arguments.INPCDFDATA = structNew()>
        </cfif>

        <cfset var INPCONTACTSTRING = arguments.INPCONTACTSTRING/>
        
        <cfset var dataout = '0' />
        <cfset var inpDesc = '' />
        <cfset var CurrTZ = '' />
        <cfset var CurrLOC = '' />
        <cfset var CurrCity = '' />
        <cfset var CurrCellFlag = '' />
        <cfset var NextContactListId = '' />
        <cfset var NextContactAddressId = '' />
        <cfset var NextContactId = '' />
        <cfset var Fields = '' />
        <cfset var value = '' />
        <cfset var item = '' />
        <cfset var itemField = '' />
        <cfset var VerifyUnique = '' />
        <cfset var CheckForContactString = '' />
        <cfset var DeleteContactVariable = '' />
        <cfset var AddToContactVariable = '' />
        <cfset var AddToContactList = '' />
        <cfset var AddToContactString = '' />
        <cfset var CheckForGroupLink = '' />
        <cfset var AddToGroup = '' />
        <cfset var GetTZInfo = '' />
        <cfset var shortCode = '' />
        <cfset var optinResult = '' />
        <cfset var AddContactVariable    = '' />
        <cfset var AddContactListResult = '' />
        <cfset var AddContactResult = '' />
        <cfset var AddToGroupResult = '' />
         
                                                          
         <!--- 
        Positive is success
        1 = OK
		2 = Duplicate
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />    
			<cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />  
            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
            <cfset QuerySetCell(dataout, "TYPE", "") />
			<cfset QuerySetCell(dataout, "MESSAGE", "") />                
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />  
            
           
            <cftry>
            
                
            	<!--- Validate user is specified - private methods onlylay - handle gracefully if not --->
            	<cfif INPUSERID GT 0>
            
					<!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers ---> 
                    
                    <!--- Clean up phone string --->        
                                     
                     <!--- Phone number validation--->                 
                    <cfif INPCONTACTTYPEID EQ 1>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
		          	</cfif>
                    
                    <!--- email validation --->
                    <cfif INPCONTACTTYPEID EQ 2>  
						<!--- Add email validation --->                        
		          	</cfif>
                    
                    <!--- SMS validation--->
                    <cfif INPCONTACTTYPEID EQ 3>  
						<!---Find and replace all non numerics except P X * #--->
                        <cfset INPCONTACTSTRING = REReplaceNoCase(INPCONTACTSTRING, "[^\d]", "", "ALL")>
		          	</cfif>
                                        
                    <!--- Get time zone info ---> 
                    <cfset CurrTZ = 0>
                    
                    <!--- Get Locality info --->  
                    <cfset CurrLOC = "">
                    <cfset CurrCity = "">
                    <cfset CurrCellFlag = "0">
                	                             
				  	<!--- Set default to -1 --->         
				   	<cfset NextContactListId = -1>         
				  	
                    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>				
                    	<cfif LEN(INPCONTACTSTRING) LT 10><cfthrow MESSAGE="Not enough numeric digits. Need at least 10 digits to make a call." TYPE="Any" detail="" errorcode="-6"></cfif>                 
                    </cfif>
                                    
					<!--- Add record --->
                    <cftry>
                        
                    
                    
						<!--- Create generic contact first--->
                        <!--- Add Address to contact --->                    
                         <cfif (INPGROUPID EQ "") >
                            <cfthrow MESSAGE="Valid Group ID for this user account required! Please specify a group ID." TYPE="Any" detail="" errorcode="-6">
                         </cfif>
                    
                    
                    	<!--- Only add to group if greater than 0 --->
                       	<cfif TRIM(INP_CPP_UUID) NEQ "" AND INPGROUPID GT 0>  
							<!--- Verify user is group id owner--->                                   
                            <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                                SELECT
                                    COUNT(GroupName_vch) AS TOTALCOUNT 
                                FROM
                                    simplelists.grouplist
                                WHERE                
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">  
                                    AND
                                    GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPGROUPID#">                     
                            </cfquery>  
                            
                            <cfif VerifyUnique.TOTALCOUNT EQ 0>
                                <cfthrow MESSAGE="Group ID does not exists for this user account! Try a different group." TYPE="Any" detail="" errorcode="-6">
                            </cfif> 
                    
                    	</cfif>
                        
                    	<cfset NextContactAddressId = "">
                        <cfset NextContactId = "">
                    
                    	<!--- Check if contact string exists in main list --->
                     	<cfquery name="CheckForContactString" datasource="#Session.DBSourceREAD#">
                         	SELECT 
                                   simplelists.contactlist.contactid_bi,
                                   simplelists.contactstring.contactaddressid_bi
                            FROM 
                               simplelists.contactstring
                               INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
                            WHERE
                               simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                            AND
                               simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#"> 
                            AND
                               simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">
                        	<!---
							<cfif TRIM(INP_CPPID) NEQ "" AND TRIM(INP_CPP_UUID) NEQ "" >
                                AND 
                                    simplelists.contactlist.cppid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPPID)#"> 
                                AND 
                                    simplelists.contactlist.cpp_uuid_vch LIKE  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPP_UUID)#">                                 
                            </cfif>
                            --->
                            
                        </cfquery> 
                        
                        <cfif CheckForContactString.RecordCount GT 0> 
                        
							<cfset NextContactId = "#CheckForContactString.ContactId_bi#">
                            <cfset NextContactAddressId = "#CheckForContactString.ContactAddressId_bi#">
                            <cfquery name="DeleteContactVariable" datasource="#Session.DBSourceEBM#">
                                       DELETE FROM simplelists.contactvariable
                                       WHERE
                                        ContactId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">
                                       AND
                                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">
                            </cfquery>
                            <cfloop collection="#INPCDFDATA#" item="item">
                                <cfset Fields = StructFind(INPCDFDATA,item)/>
                                <cfloop collection="#Fields#" item="itemField">
                                    <cfset value = StructFind(Fields,itemField)/>
                                    <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                           INSERT INTO simplelists.contactvariable
                                                (
                                                    UserId_int,
                                                    ContactId_bi,
                                                    VariableName_vch,
                                                    VariableValue_vch,
                                                    Created_dt,
                                                    CdfId_int
                                                 )
                                           VALUES
                                                (
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itemField#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item#">
                                                )
                                        </cfquery>
                                </cfloop>
                            </cfloop>

                         
                        <cfelse> <!--- ContactList for this user does not exist yet --->
                                
						  	<!--- By default just add new unique contact during this method - add method later on to search for existing contact--->                        
                         	<cfquery name="AddToContactList" datasource="#Session.DBSourceEBM#" result="AddContactListResult">
                                    INSERT INTO simplelists.contactlist
                                        (
                                            UserId_int, 
                                            Created_dt, 
                                            LASTUPDATED_DT, 
                                            CPPID_vch, 
                                            CPP_UUID_vch, 
                                            socialToken_vch, 
                                            FirstName_vch, 
                                            LastName_vch,
                                            LanguagePreference_vch 
                                        )
                                    VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                            NOW(), 
                                            NOW(), 
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPPID)#">, 
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_CPP_UUID)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INP_SOCIALTOKEN)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPFIRSTNAME)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPLASTNAME)#">,
											<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LANGUAGEPREFERENCE)#"> 
                                         )                                        
                            </cfquery>   
                              
                            <cfset NextContactId = "#AddContactListResult.GENERATEDKEY#">  
                                
                                <cfquery name="AddToContactString" datasource="#Session.DBSourceEBM#" result="AddContactResult">
                                   INSERT INTO simplelists.contactstring
                                        (
                                            ContactId_bi,
                                            Created_dt,
                                            LASTUPDATED_DT,
                                            ContactType_int,
                                            ContactString_vch,
                                            TimeZone_int,
                                            CellFlag_int,
                                            UserSpecifiedData_vch,
                                            OptIn_int
                                         )
                                   VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                            NOW(),
                                            NOW(),
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCONTACTTYPEID#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrTZ#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#CurrCellFlag#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPUSERSPECIFIEDDATA#">,
                                            1
                                        )
                                </cfquery>


                                <cfloop collection="#INPCDFDATA#" item="item">
                                    <cfset Fields = StructFind(INPCDFDATA,item)/>
                                    <cfloop collection="#Fields#" item="itemField">
                                        <cfset value = StructFind(Fields,itemField)/>
                                        <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
                                           INSERT INTO simplelists.contactvariable
                                                (
                                                    UserId_int,
                                                    ContactId_bi,
                                                    VariableName_vch,
                                                    VariableValue_vch,
                                                    Created_dt,
                                                    CdfId_int
                                                 )
                                           VALUES
                                                (
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPUSERID#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactId#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#itemField#">,
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#value#">,
                                                    NOW(),
                                                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#item#">
                                                )
                                        </cfquery>
                                    </cfloop>
                                </cfloop>

                              <cfset NextContactAddressId = #AddContactResult.GENERATEDKEY#>
                         
                        </cfif>

                                 
                       <!--- Only add to group if greater than 0 --->
                       <cfif TRIM(INP_CPP_UUID) NEQ "" AND INPGROUPID GT 0>   
                         
                           <cfquery name="CheckForGroupLink" datasource="#Session.DBSourceREAD#">
                               SELECT 
                                   ContactAddressId_bi
                               FROM
                                   simplelists.groupcontactlist
                               WHERE
                                   ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">
                               AND
                                   GroupId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">
                           </cfquery> 
                                                 
                            <cfif CheckForGroupLink.RecordCount EQ 0> 
                           
                             <cfquery name="AddToGroup" datasource="#Session.DBSourceEBM#" result="AddToGroupResult">
                                   INSERT INTO 
                                       simplelists.groupcontactlist
                                       (ContactAddressId_bi, GroupId_bi)
                                   VALUES
                                       (<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPGROUPID#">)                                        
                               </cfquery>    
                           </cfif>
                        
                      </cfif>
                                                            
                       
<!---

Time Zone · The number of hours past Greenwich Mean Time. The time zones are:

   Hours Time Zone
           27 4 Atlantic
           28 5 Eastern
           29 6 Central
           30 7 Mountain
           31 8 Pacific
           32 9 Alaska
           33 10 Hawaii-Aleutian
           34 11 Samoa
           37 14 Guam
         

--->
					    <cfif INPCONTACTTYPEID EQ 1 OR INPCONTACTTYPEID EQ 3>
                                            
							<!--- Get time zones, Localities, Cellular data --->
                            <cfquery name="GetTZInfo" datasource="#Session.DBSourceEBM#">
                                UPDATE MelissaData.FONE AS fx JOIN
                                 MelissaData.CNTY AS cx ON
                                 (fx.FIPS = cx.FIPS) INNER JOIN
                                 simplelists.contactstring AS ded ON (LEFT(ded.ContactString_vch, 6) = CONCAT(fx.NPA,fx.NXX))
                                SET 
                                  ded.TimeZone_int = (CASE cx.T_Z
                                  WHEN 14 THEN 37 
                                  WHEN 10 THEN 33 
                                  WHEN 9 THEN 32 
                                  WHEN 8 THEN 31 
                                  WHEN 7 THEN 30 
                                  WHEN 6 THEN 29 
                                  WHEN 5 THEN 28 
                                  WHEN 4 THEN 27  
                                 END),
                                 ded.State_vch = 
                                 (CASE 
                                  WHEN fx.STATE IS NOT NULL THEN fx.STATE
                                  ELSE '' 
                                  END),
                                  ded.City_vch = 
                                  (CASE 
                                  WHEN fx.CITY IS NOT NULL THEN fx.CITY
                                  ELSE '' 
                                  END),
                                  ded.CellFlag_int =  
                                  (CASE 
                                  WHEN fx.Cell IS NOT NULL THEN fx.Cell
                                  ELSE 0 
                                  END)                                                  
                                 WHERE
                                    cx.T_Z IS NOT NULL   
                                 AND
                                    ded.ContactAddressId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#NextContactAddressId#">                         
                                 AND    
	                                ded.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">
                                    
                                    <!--- Limit to US or Canada?--->
                                    <!---  AND fx.CTRY IN ('U', 'u')  --->
                            </cfquery>                                                 
                                        	                    
                        </cfif>  

                       <!--- GET USER SHORTCODE --->
                        <cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCodeByUserId" returnvariable="shortCode">
                             <cfinvokeargument name="UserId" value="#INPUSERID#">
                        </cfinvoke>

                        <!--- DO OPTIN --->
                        <cfinvoke component="public.sire.models.cfc.optin" method="optInAction" returnvariable="optinResult">
                             <cfinvokeargument name="INPCONTACTSTRING" value="#INPCONTACTSTRING#">
                             <cfinvokeargument name="SHORTCODE" value="#shortCode.SHORTCODE#">
                             <cfinvokeargument name="OPTINSOURCE" value="CPP SubcriberSource">
                             <cfinvokeargument name="USERID" value="#INPUSERID#">
                        </cfinvoke>



                        <!--- LOG CPPX ACTION --->

                        <cfinvoke component="public.sire.models.cfc.mlp" method="CPPXLogAction">
                            <cfinvokeargument name="inpCPPID" value="#INP_CPPID#">
                            <cfinvokeargument name="inpUSERID" value="#INPUSERID#">
                            <cfinvokeargument name="inpMESSGE" value="CPP Subcriber">
                            <cfinvokeargument name="inpSUBJECT" value="CPP Optin">
                            <cfinvokeargument name="inpCONTACTSTRING" value="#INPCONTACTSTRING#">
                        </cfinvoke>
                                             
                        
						<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                        <cfset QueryAddRow(dataout) />
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                        <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                        <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                        <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                        <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />   
                        <cfset QuerySetCell(dataout, "TYPE", "") />
                        <cfset QuerySetCell(dataout, "MESSAGE", "") />                
                        <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                    
                                                       
                    <cfcatch TYPE="any">
                        <!--- Squash possible multiple adds at same time --->	
                        
                        <!--- Does it already exist?--->
                        <cfif FindNoCase("Duplicate entry", #cfcatch.detail#) GT 0>
                        	
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
                        <cfelse>
                        
							<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                            <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                            <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />     
                            <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />
                            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") />  
                            <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
                            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                            <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />      
                        
                        </cfif>
                        
                    </cfcatch>       
                    
                    </cftry>
                   
					     
                <cfelse>
                
                    <cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                    <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                    <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
            		<cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#LEFT(INP_SOURCEKEY,255)#") /> 
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
                    <cfset QuerySetCell(dataout, "MESSAGE", "Invalid UserId sepecified!") />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
                            
                </cfif>          
                                     
            <cfcatch TYPE="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, INPCONTACTTYPEID, INPCONTACTSTRING, INPGROUPID, INP_SOURCEKEY, TYPE, MESSAGE, ERRMESSAGE")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPCONTACTTYPEID", "#INPCONTACTTYPEID#") />   
                <cfset QuerySetCell(dataout, "INPCONTACTSTRING", "#INPCONTACTSTRING#") />
                <cfset QuerySetCell(dataout, "INPGROUPID", "#INPGROUPID#") />     
	            <cfset QuerySetCell(dataout, "INP_SOURCEKEY", "#INP_SOURCEKEY#") /> 
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
         
		</cfoutput>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CPPXLogAction" access="public" output="true" hint="Log action submit from MLP">
        <cfargument name="inpCPPID" type="number" required="yes">
        <cfargument name="inpUSERID" type="number" required="yes">
        <cfargument name="inpMESSGE" type="string" required="yes">
        <cfargument name="inpSUBJECT" type="string" required="yes">
        <cfargument name="inpCONTACTSTRING" type="string" required="yes">
        

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.CPPID = "0" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.DATA = "" />
        <cfset var insertCPPXLog = '' /> 

        <cftry>
        
            <cfquery name="insertCPPXLog" datasource="#Session.DBSourceEBM#" >
                INSERT INTO 
                    simplexresults.cppx_tracking(CPPId_int,UserId_int,Subject_vch,Message_vch,Created_dt,ContactString_vch)
                VALUES(
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCPPID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUSERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpMESSGE#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpSUBJECT#">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCONTACTSTRING#">
                )
                 
            </cfquery>

            <cfif insertCPPXLog.RecordCount GT 0>
                <cfset dataout.RXRESULTCODE = 1 />    
            </cfif> 
           
            <cfcatch>
                <cfset dataout.MESSAGE = 'Insert Errors'>
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
            </cfcatch>      
        </cftry> 
        <cfreturn dataout>
    </cffunction>

    <cffunction name="ReadCPPTemplateByFilter" access="public" output="true" hint="Read MLP">
        <cfargument name="inpTemplateID" type="string" required="yes">

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.CPPID = "0" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.DATA = "" />
        <cfset dataout.URLCSS = ""/>

        <cfset var readCPPTemplate = ''>
        <cfset var checkActive = 0>
        <cfset var xmlDataControl = '' /> 

        <cftry>
            <cfquery name="readCPPTemplate" datasource="#Session.DBSourceREAD#" >
                SELECT 
                   tempalteId_int, XML_txt as XMLDataString
                FROM    
                    simplelists.cppx_template
                WHERE  
                        tempalteId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTemplateID#">
                LIMIT 1
                   
            </cfquery>
            <cfif readCPPTemplate.RecordCount GT 0>
                
                <cfloop query="#readCPPTemplate#">
                    <cfinvoke component="session.sire.models.cfc.mlp" method="ParseCPPXXmlData" returnvariable="xmlDataControl">
                        <cfinvokeargument name="xmlData" value="#XMLDataString#">
                    </cfinvoke>
                    <cfset dataout.DATA = xmlDataControl>
                </cfloop>   
                <cfset dataout.RXRESULTCODE = 1 />
            <cfelse>
                <cfset dataout.MESSAGE = 'Can not get this MLP template data'>
            </cfif> 

            <cfcatch>
                <cfset dataout.MESSAGE = 'Can not get this MLP template data'>
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
            </cfcatch>      
        </cftry>        
        <cfreturn dataout>
    </cffunction>

    <cffunction name="GetMLPList" access="remote" output="true" hint="Get mlp by UserId ">
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <!--- <cfargument name="sEcho"> --->
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_3" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_3" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_4" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_4" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is groupid, 2 is group name, 3 is number of voice, 4 is number of sms, 5 is number of email--->
        <cfargument name="sSortDir_0" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="inpAction" required="false" default="downgrade">

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = '' />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout["ListMLPData"] = ArrayNew(1) />
        <cfset dataout["iTotalRecords"] = 0>
        <cfset dataout["iTotalDisplayRecords"] = 0>
        <cfset var tempItem = {} />

        <cfset var getMLP = '' />
        <cfset var status = 1 >
        <cfset var rsCount = ''/>

        <cfif arguments.inpAction EQ 'upgrade'>
            <cfset status = 2 >
        </cfif>

        <cftry>
            <cfif arguments.iDisplayStart < 0>
                <cfset arguments.iDisplayStart = 0>
            </cfif>

            <cfquery name="getMLP" datasource="#Session.DBSourceREAD#">
            SELECT 
                SQL_CALC_FOUND_ROWS
                c.ccpxDataId_int,
                c.cppxUUID_vch,
                c.cppxName_vch,
                c.cppxURL_vch,
                c.created_dt,
                c.status_int
            FROM
                simplelists.cppx_data c
            WHERE 
                c.status_int = #status#    
            AND 
                c.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#"> 
            ORDER BY 
                c.ccpxDataId_int desc
            LIMIT 
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.iDisplayLength#">
                
            </cfquery>
            
            <cfquery datasource="#Session.DBSourceREAD#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] =  (iTotalRecords GTE 1 ? iTotalRecords : 0)>
                <cfset dataout["iTotalRecords"] = (iTotalRecords GTE 1 ? iTotalRecords : 0)>
            </cfloop>

            <!--- <cfset dataout["iTotalRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) >
            <cfset dataout["iTotalDisplayRecords"] = (getKeyword.RECORDCOUNT GTE 1 ? getKeyword.RECORDCOUNT : 0) > --->

            <cfif getMLP.RECORDCOUNT GT 0>
                <cfloop query="getMLP">
                    <cfset tempItem = 
                        {
                            NAME = '#getMLP.cppxName_vch#',
                            URL = '#getMLP.cppxURL_vch#', 
                            CREATED = '#DateFormat(getMLP.created_dt, 'mm/dd/yyyy ')#',
                            ID = '#getMLP.ccpxDataId_int#'
                        }
                    >
                    <cfset ArrayAppend(dataout["ListMLPData"],tempItem)>
                </cfloop>
                
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = 'Get mlp successfully!' />
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0 />
                <cfset dataout.MESSAGE = 'No mlp found' />
            </cfif>
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

</cfcomponent>	