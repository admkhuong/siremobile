<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfset LOCAL_LOCALE = "English (US)">

     <cffunction name="optInAction" access="public" output="true" hint="optin flow">
     	<cfargument name="INPCONTACTSTRING" required="yes" default="">
     	<cfargument name="SHORTCODE" required="yes" default="">
     	<cfargument name="OPTINSOURCE" required="yes" default="">
     	<cfargument name="USERID" required="no" default="">
     	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset var AddContactVariable = ''>
	   	<cfset var UpdateOldOptOut = ''>
	   	<cfset var AddToContactVariable = ''>

	   	<cftry>
			<!---Update old opt out if any--->
		    <cfquery name="UpdateOldOptOut" datasource="#Session.DBSourceEBM#" >
		        UPDATE
		            simplelists.optinout
		        SET
		            OptIn_dt = now()
		        WHERE
		            ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SHORTCODE#">
		        AND
		            OptIn_dt IS NULL
		        AND
		            
		            ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRING)#">
		    </cfquery>

		    <!--- INSERT INTO simplelists.optinout --->    
		    <cfquery name="AddToContactVariable" datasource="#Session.DBSourceEBM#" result="AddContactVariable">
		        INSERT INTO simplelists.optinout
		            (
		            	<cfif USERID GT 0>
		                UserId_int,
		                </cfif>
		                ContactString_vch,
		                OptOut_dt,
		                OptIn_dt,
		                OptInSource_vch,
		                ShortCode_vch
		                
		            )
		        VALUES
		            (
		            	<cfif USERID GT 0>
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERID#">,
		                </cfif>
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPCONTACTSTRING#">,
		                <CFQUERYPARAM cfsqltype="cf_sql_timestamp" value="" null="Yes">,
		                NOW(),
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OPTINSOURCE#">,
		                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SHORTCODE#">
		            )
		    </cfquery>
		  
		    <cfset dataout.RXRESULTCODE = 1>  
            <cfset dataout.MESSAGE = "OptIn Success!"> 
            <cfset dataout.ERRMESSAGE = "">      
		<cfcatch>
			<cfset dataout.RXRESULTCODE = -1>  
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">   
		</cfcatch>
			         
        </cftry>
        <cfreturn dataout>
    </cffunction>    
</cfcomponent>