<cfcomponent>


	<cfinclude template="/public/sire/configs/paths.cfm">

    <cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
	<cffunction name="GetOrderPlan" access="remote" output="true">
		<cfargument name="plan" TYPE="numeric" required="true"/>
		<cfset var dataout	= {} />
		<cfset var getPlan	= '' />
        <cfset var limitNumber = ''/>
        <cfset var shortUrl = ''/>
    		<cfquery name="getPlan" datasource="#Session.DBSourceREAD#">
                SELECT 
                	PlanId_int, 
                	PlanName_vch,
                	Amount_dec,
                    YearlyAmount_dec,
                	UserAccountNumber_int,
                	KeywordsLimitNumber_int,
                	FirstSMSIncluded_int,
                	PriceMsgAfter_dec,
                	PriceKeywordAfter_dec,
                    MlpsLimitNumber_int,
                    ShortUrlsLimitNumber_int,
                    Status_int
                FROM simplebilling.plans
                WHERE
                    PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.plan#"> 
                AND
                    Status_int in(1,8,4,2);
            </cfquery>
            <cfset dataout =  QueryNew("RXRESULTCODE, PLANID, PLANNAME, AMOUNT,YEARLYAMOUNT, USERACCOUNTNUMBER, KEYWORDSLIMITNUMBER, FIRSTSMSINCLUDED, PRICEMSGAFTER,PRICEKEYWORDAFTER,MLPSLIMITNUMBER,SHORTURLSLIMITNUMBER,MESSAGE,ERRMESSAGE,STATUS")> 
            <cfif getPlan.RecordCount GT 0>

                <!--- MLPSLIMITNUMBER --->
                <cfif getPlan.MlpsLimitNumber_int EQ 0>
                    <cfset limitNumber = "Unlimited"/>
                <cfelse>
                    <cfset limitNumber = getPlan.MlpsLimitNumber_int/>
                </cfif>                

                <!--- SHORTURLSLIMITNUMBER --->
                <cfif getPlan.ShortUrlsLimitNumber_int EQ 0>
                    <cfset shortUrl = "Unlimited"/>
                <cfelse>
                    <cfset shortUrl = getPlan.ShortUrlsLimitNumber_int/>
                </cfif>

                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "PLANID", "#getPlan.PlanId_int#") /> 
                <cfset QuerySetCell(dataout, "PLANNAME", "#getPlan.PlanName_vch#") />  
    			<cfset QuerySetCell(dataout, "AMOUNT", "#getPlan.Amount_dec#") />  
                <cfset QuerySetCell(dataout, "YEARLYAMOUNT", "#getPlan.YearlyAmount_dec#") />
                <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getPlan.UserAccountNumber_int#") />  
                <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getPlan.KeywordsLimitNumber_int#") />  
                <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getPlan.FirstSMSIncluded_int#") />  
                <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#getPlan.PriceMsgAfter_dec#") /> 
                <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#getPlan.PriceKeywordAfter_dec#") /> 
                <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
                <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
                <cfset QuerySetCell(dataout, "STATUS", "#getPlan.Status_int#")/>
                <cfset QuerySetCell(dataout, "MESSAGE", "Ok")/>
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
                
            </cfif>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="GetUserPlan" access="remote" output="true" hint="get current user plan">
        <cfargument name="inpUserId" TYPE="numeric" required="false"/>

        <cfset var userId = '' />
        <cfif structKeyExists(arguments, "inpUserId")>
            <cfset userId = arguments.inpUserId>
        <cfelse>
            <cfset userId = SESSION.USERID>
        </cfif>

        <cfset var totalKeyword = '' />
        <cfset var KeywordUse   = '' />
        <cfset var KeywordAvailable = '' />
        <cfset var dataout  = {} />
        <cfset var getUserPlan  = '' />
        <cfset var getKeywordUse    = '' />
        <cfset var checkUserPlan    = '' />
        <cfset var PriceKeywordAfter    = '' />
        <cfset var PriceMsgAfter    = '' />
         
        <cfset var getOrderPlan= '' />
        <cfset var limitNumber = '' />
        <cfset var shortUrl = '' />

        <cfset var shortUrlUsed   = 0 />
        <cfset var MLPUsed   = 0 />
        <cfset var getShortUrlUsed = ''/>
        <cfset var getMLPUsed = ''/>

        <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                u.UserPlanId_bi, 
                u.Status_int,
                u.PlanId_int,
                u.StartDate_dt,
                u.EndDate_dt,
                p.PlanName_vch,
                p.Amount_dec,
                u.BillingType_ti,
                p.UserAccountNumber_int,
                u.KeywordsLimitNumber_int,
                p.KeywordsLimitNumber_int as PlanKeywordsLimitNumber_int,
                p.FirstSMSIncluded_int,
                u.PriceMsgAfter_dec,
                u.PriceKeywordAfter_dec,
                u.BuyKeywordNumber_int,
                u.BuyKeywordNumberExpired_int,
                u.DowngradeDate_dt,
                u.UserDowngradeDate_dt,
                p.MlpsLimitNumber_int,
                p.ShortUrlsLimitNumber_int
            FROM simplebilling.userplans u INNER JOIN simplebilling.plans p ON u.PlanId_int = p.PlanId_int
            WHERE
                u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
                AND u.Status_int = 1
            ORDER BY UserPlanId_bi DESC
            LIMIT 1
        </cfquery>
        
        <!--- CHECK IF USER PLAN IS EXPIRED --->        
        <cfinvoke component="public.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
            <cfinvokeargument name="userId" value="#userId#">
        </cfinvoke>
        
        <!--- GET FREE PLAN INFO --->
        <cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="getOrderPlan">
            <cfinvokeargument name="plan" value="1">
        </cfinvoke>        

        <cfquery name="getKeywordUse" datasource="#Session.DBSourceREAD#">
            SELECT COUNT(k.KeywordId_int) AS keywordUse 
            FROM sms.keyword k 
                INNER JOIN simpleobjects.batch b ON k.BatchId_bi = b.BatchId_bi 
            WHERE 
                    k.Active_int = 1
                AND k.IsDefault_bit = 0
                AND EMSFlag_int = 0
                AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
        </cfquery>

        <cfquery name="getMLPUsed" datasource="#Session.DBSourceREAD#">
            SELECT 
                count(ccpxDataId_int) as mlpUsed
            FROM 
                simplelists.cppx_data
            WHERE 
                status_int =1
            AND 
                MlpType_ti = 1    
            AND 
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
        </cfquery>

        <cfif getMLPUsed.RecordCount GT 0>
            <cfset mlpUsed = getMLPUsed.mlpUsed>    
        </cfif>

        <cfquery name="getShortUrlUsed" datasource="#Session.DBSourceREAD#">
            SELECT 
                count(PKId_bi) as shortUrlUsed
            FROM 
                simplelists.url_shortner
            WHERE 
                Active_int =1
            AND 
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
        </cfquery>

        <cfif getShortUrlUsed.RecordCount GT 0>
            <cfset shortUrlUsed = getShortUrlUsed.shortUrlUsed>    
        </cfif>
        
        <cfset totalKeyword = 0>
        <cfset KeywordUse = 0>
        <cfset KeywordAvailable = 0>

        <cfif getUserPlan.RecordCount GT 0>
            <cfset totalKeyword = (totalKeyword + getUserPlan.KeywordsLimitNumber_int + getUserPlan.BuyKeywordNumber_int)>
        </cfif>  
                
        <cfif getKeywordUse.RecordCount GT 0>
            <cfset KeywordUse = getKeywordUse.keywordUse>
        </cfif>
        
        <!--- IF USER PLAN HAS EXPRIED within 30 days : keyword avaiable = (free plan keyword + keyword buy after expired) - keyword in used 
            PriceKeywordAfter = Free Plan Price
            PriceMsgAfter = Free Plan Price
        --->    
        <cfif checkUserPlan.RXRESULTCODE GT 0> 
            <cfif checkUserPlan.RXRESULTCODE EQ 1>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER + getUserPlan.BuyKeywordNumberExpired_int - KeywordUse)>        
            </cfif>

            <cfif checkUserPlan.RXRESULTCODE EQ 2>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER - KeywordUse)>    
            </cfif>
            
            <cfset PriceKeywordAfter =  getOrderPlan.PRICEKEYWORDAFTER >
            <cfset PriceMsgAfter =  getOrderPlan.PRICEMSGAFTER >
        <cfelse>
            <cfset KeywordAvailable = (totalKeyword - KeywordUse)>    
            <cfset PriceKeywordAfter =  getUserPlan.PriceKeywordAfter_dec >
            <cfset PriceMsgAfter =  getUserPlan.PriceMsgAfter_dec >
        </cfif>
        
        <cfif KeywordAvailable LT 0>
            <cfset KeywordAvailable = 0 >
        </cfif>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, USERPLANID,STATUS,PLANID,STARTDATE,ENDDATE,PLANNAME,AMOUNT,USERACCOUNTNUMBER,KEYWORDSLIMITNUMBER,FIRSTSMSINCLUDED,PRICEMSGAFTER,PRICEKEYWORDAFTER,KEYWORDPURCHASED, TOTALKEYWORD,KEYWORDUSE,KEYWORDAVAILABLE,KEYWORDBUYAFTEREXPRIED,PLANEXPIRED,PLANKEYWORDSLIMITNUMBER,MLPSLIMITNUMBER,SHORTURLSLIMITNUMBER,FREEPLANKEYWORDSLIMITNUMBER,DOWNGRADEDATE,SHORTURLUSED,MLPUSED,USERDOWNGRADEDATE,MESSAGE,ERRMESSAGE, DATEDIFF,BILLINGTYPE")> 
        <cfif getUserPlan.RecordCount GT 0>

            <!--- MLPSLIMITNUMBER --->
            <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getUserPlan.MlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int/>
            </cfif>   

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "USERPLANID", "#getUserPlan.UserPlanId_bi#") /> 
            <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
            <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />  
            <cfset QuerySetCell(dataout, "STARTDATE", "#getUserPlan.StartDate_dt#") />
            <cfset QuerySetCell(dataout, "ENDDATE", "#getUserPlan.EndDate_dt#") />
            <cfset QuerySetCell(dataout, "PLANNAME", "#getUserPlan.PlanName_vch#") />    
            <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#PriceMsgAfter#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#PriceKeywordAfter#") />
            <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "#getUserPlan.BuyKeywordNumber_int#") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
            <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
            <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
            <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "#getUserPlan.BuyKeywordNumberExpired_int#") />  
            <cfset QuerySetCell(dataout, "PLANEXPIRED", "#checkUserPlan.RXRESULTCODE#") />  
            <cfset QuerySetCell(dataout, "DATEDIFF", "#checkUserPlan.DiffDate#") />  
            <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "#getUserPlan.PlanKeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FREEPLANKEYWORDSLIMITNUMBER", "#getOrderPlan.KEYWORDSLIMITNUMBER#") />  
            <cfset QuerySetCell(dataout, "DOWNGRADEDATE", "#getUserPlan.DowngradeDate_dt#") />  
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
            <cfset QuerySetCell(dataout, "SHORTURLUSED", "#shortUrlUsed#") />
            <cfset QuerySetCell(dataout, "MLPUSED", "#mlpUsed#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEDATE", "#getUserPlan.UserDowngradeDate_dt#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
            <cfset QuerySetCell(dataout, "BILLINGTYPE", "#getUserPlan.BillingType_ti#") />
        <cfelse>
        
           <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
                p.PlanId_int,
                p.PlanName_vch,
                p.Status_int,
                p.Amount_dec,
                p.UserAccountNumber_int,
                p.KeywordsLimitNumber_int,
                p.FirstSMSIncluded_int,
                p.CreditsAddAmount_int,
                p.PriceMsgAfter_dec,
                p.PriceKeywordAfter_dec,
                p.MlpsLimitNumber_int,
                p.ShortUrlsLimitNumber_int
            FROM simplebilling.plans p
            WHERE  PlanId_int = 1 
            
            </cfquery>

            <!--- MLPSLIMITNUMBER --->
            <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getUserPlan.MlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int/>
            </cfif>

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 0) />
            <cfset QuerySetCell(dataout, "USERPLANID", "0") /> 
            <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
            <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />  
            <cfset QuerySetCell(dataout, "STARTDATE", "") />
            <cfset QuerySetCell(dataout, "ENDDATE", "") />
            <cfset QuerySetCell(dataout, "PLANNAME", "#getUserPlan.PlanName_vch#") />    
            <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#getUserPlan.PriceMsgAfter_dec#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#getUserPlan.PriceKeywordAfter_dec#") />
            <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "0") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
            <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
            <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
            <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "0") /> 
            <cfset QuerySetCell(dataout, "PLANEXPIRED", "0") />
            <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "0") /> 
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
             <cfset QuerySetCell(dataout, "SHORTURLUSED", "#shortUrlUsed#") />
            <cfset QuerySetCell(dataout, "MLPUSED", "#mlpUsed#") />
            <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserPlan1" access="remote" output="true" hint="get current user plan">
        <cfargument name="InpUserId" TYPE="numeric" required="false"/>

        <cfset var userId = arguments.InpUserId>

        <cfif structKeyExists(arguments, "InpUserId")>
            <cfset userId = arguments.InpUserId>
        <cfelse>
            <cfset userId = SESSION.USERID>
        </cfif>

        <cfset var totalKeyword = '' />
        <cfset var KeywordUse   = '' />
        <cfset var KeywordAvailable = '' />
        <cfset var dataout  = {} />
        <cfset var getUserPlan  = '' />
        <cfset var getUserBilling  = '' />
        <cfset var getKeywordUse    = '' />
        <cfset var checkUserPlan    = '' />
        <cfset var PriceKeywordAfter    = '' />
        <cfset var PriceMsgAfter    = '' />
        <cfset var getOrderPlan = '' />

        <cfset var limitNumber = '' />
        <cfset var shortUrl = '' />
        
        <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
            SELECT 
            u.UserPlanId_bi, 
            u.Status_int,
            u.PlanId_int,
            u.StartDate_dt,
            u.EndDate_dt,
            p.PlanName_vch,
            p.Amount_dec,
                u.BillingType_ti,
            p.UserAccountNumber_int,
            p.Order_int,
            u.KeywordsLimitNumber_int,
            p.KeywordsLimitNumber_int as PlanKeywordsLimitNumber_int,
            u.KeywordMinCharLimit_int,
            p.KeywordMinCharLimit_int as PlanKeywordMinCharLimit_int,
            p.FirstSMSIncluded_int,
            u.FirstSMSIncluded_int as UserPlanFirstSMSIncluded_int,
            u.PriceMsgAfter_dec,
            u.PriceKeywordAfter_dec,
            u.BuyKeywordNumber_int,
            u.BuyKeywordNumberExpired_int,
            u.DowngradeDate_dt,
            u.MlpsLimitNumber_int,
            u.ShortUrlsLimitNumber_int,
            u.MlpsImageCapacityLimit_bi,
            u.UserDowngradeDate_dt,
            u.UserDowngradePlan_int,
            u.UserDowngradeKeyword_txt,
            u.UserDowngradeMLP_txt,
            u.UserDowngradeShortUrl_txt,
            u.PromotionId_int,
            u.PromotionLastVersionId_int,
            u.PromotionKeywordsLimitNumber_int,
            u.PromotionMlpsLimitNumber_int,
            u.PromotionShortUrlsLimitNumber_int

            FROM simplebilling.userplans u INNER JOIN simplebilling.plans p ON u.PlanId_int = p.PlanId_int
            WHERE
            u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
            AND u.Status_int = 1
            ORDER BY UserPlanId_bi DESC
            LIMIT 1
        </cfquery>

        <cfquery name="getUserBilling" datasource="#Session.DBSourceEBM#">
            SELECT 
            PromotionCreditBalance_int
            FROM simplebilling.billing 
            WHERE
            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#">
            ORDER BY UserId_int DESC
            LIMIT 1
        </cfquery>
        
        <!--- CHECK IF USER PLAN IS EXPIRED --->        
        <cfinvoke component="session.sire.models.cfc.order_plan" method="checkUserPlanExpried" returnvariable="checkUserPlan">
            <cfinvokeargument name="userId" value="#userId#">
            </cfinvoke>
            
            <!--- GET FREE PLAN INFO --->
            <cfinvoke component="session.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="getOrderPlan">
                <cfinvokeargument name="plan" value="1">
                </cfinvoke>        

                <cfquery name="getKeywordUse" datasource="#Session.DBSourceREAD#">
                    SELECT COUNT(k.KeywordId_int) AS keywordUse 
                    FROM sms.keyword k 
                    INNER JOIN simpleobjects.batch b ON k.BatchId_bi = b.BatchId_bi 
                    WHERE 
                    k.Active_int = 1
                    AND k.IsDefault_bit = 0
                    AND EMSFlag_int = 0
                    AND b.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId#"> 
                </cfquery>
                
                <cfset totalKeyword = 0>
                <cfset KeywordUse = 0>
                <cfset KeywordAvailable = 0>

                <cfif getUserPlan.RecordCount GT 0>
                    <cfset totalKeyword = (totalKeyword + getUserPlan.KeywordsLimitNumber_int + getUserPlan.BuyKeywordNumber_int + getUserPlan.PromotionKeywordsLimitNumber_int)>
                </cfif>  
                
                <cfif getKeywordUse.RecordCount GT 0>
                    <cfset KeywordUse = getKeywordUse.keywordUse>
                </cfif>
                
        <!--- IF USER PLAN HAS EXPRIED within 30 days : keyword avaiable = (free plan keyword + keyword buy after expired) - keyword in used 
            PriceKeywordAfter = Free Plan Price
            PriceMsgAfter = Free Plan Price
        --->    
        <cfif checkUserPlan.RXRESULTCODE GT 0> 
            <cfif checkUserPlan.RXRESULTCODE EQ 1>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER + getUserPlan.BuyKeywordNumberExpired_int - KeywordUse)>        
            </cfif>

            <cfif checkUserPlan.RXRESULTCODE EQ 2>
                <cfset KeywordAvailable = (getOrderPlan.KEYWORDSLIMITNUMBER - KeywordUse)>    
            </cfif>
            
            <cfset PriceKeywordAfter =  getOrderPlan.PRICEKEYWORDAFTER >
            <cfset PriceMsgAfter =  getOrderPlan.PRICEMSGAFTER >
        <cfelse>
            <cfset KeywordAvailable = (totalKeyword - KeywordUse)>    
            <cfset PriceKeywordAfter =  getUserPlan.PriceKeywordAfter_dec >
            <cfset PriceMsgAfter =  getUserPlan.PriceMsgAfter_dec >
        </cfif>
        
        <cfif KeywordAvailable LT 0>
            <cfset KeywordAvailable = 0 >
        </cfif>
        
        <cfset dataout =  QueryNew("RXRESULTCODE, USERPLANID,STATUS,PLANID,BILLINGTYPE,PLANORDER, STARTDATE,ENDDATE,PLANNAME,AMOUNT,USERACCOUNTNUMBER,KEYWORDSLIMITNUMBER,FIRSTSMSINCLUDED,PROMOTIONCREDIT,USERPLANFIRSTSMSINCLUDED,PRICEMSGAFTER,PRICEKEYWORDAFTER,KEYWORDPURCHASED, TOTALKEYWORD,KEYWORDUSE,KEYWORDAVAILABLE,KEYWORDBUYAFTEREXPRIED,PLANEXPIRED,PLANKEYWORDSLIMITNUMBER,MLPSLIMITNUMBER,SHORTURLSLIMITNUMBER,IMAGECAPACITYLIMIT,PROMOTIONID,PROMOTIONLASTVERSIONID,PROMOTIONKEYWORDSLIMITNUMBER,PROMOTIONMLPSLIMITNUMBER,PROMOTIONSHORTURLSLIMITNUMBER,FREEPLANKEYWORDSLIMITNUMBER,DOWNGRADEDATE,USERDOWNGRADEPLAN,MESSAGE,ERRMESSAGE")> 
        <cfif getUserPlan.RecordCount GT 0>   

            <!--- MLPSLIMITNUMBER --->
            <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getUserPlan.MlpsLimitNumber_int  + getUserPlan.PromotionMlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int  + getUserPlan.PromotionShortUrlsLimitNumber_int/>
            </cfif>     

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
            <cfset QuerySetCell(dataout, "USERPLANID", "#getUserPlan.UserPlanId_bi#") /> 
            <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
            <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />
            <cfset QuerySetCell(dataout, "BILLINGTYPE", "#getUserPlan.BillingType_ti#") />
            <cfset QuerySetCell(dataout, "PLANORDER", "#getUserPlan.Order_int#") /> 
            <cfset QuerySetCell(dataout, "STARTDATE", "#getUserPlan.StartDate_dt#") />
            <cfset QuerySetCell(dataout, "ENDDATE", "#getUserPlan.EndDate_dt#") />
            <cfset QuerySetCell(dataout, "PLANNAME", "#getUserPlan.PlanName_vch#") />    
            <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PROMOTIONCREDIT", "#getUserBilling.PromotionCreditBalance_int#") />  
            <cfset QuerySetCell(dataout, "USERPLANFIRSTSMSINCLUDED", "#getUserPlan.UserPlanFirstSMSIncluded_int#") />
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#PriceMsgAfter#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#PriceKeywordAfter#") />
            <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "#getUserPlan.BuyKeywordNumber_int#") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
            <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
            <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
            <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "#getUserPlan.BuyKeywordNumberExpired_int#") />  
            <cfset QuerySetCell(dataout, "PLANEXPIRED", "#checkUserPlan.RXRESULTCODE#") />  
            <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "#getUserPlan.PlanKeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FREEPLANKEYWORDSLIMITNUMBER", "#getOrderPlan.KEYWORDSLIMITNUMBER#") />  
            <cfset QuerySetCell(dataout, "DOWNGRADEDATE", "#getUserPlan.DowngradeDate_dt#") />  
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") />
            <cfset QuerySetCell(dataout, "IMAGECAPACITYLIMIT", "#getUserPlan.MlpsImageCapacityLimit_bi#") />
            
            <cfset QuerySetCell(dataout, "PROMOTIONID", "#getUserPlan.PromotionId_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONLASTVERSIONID", "#getUserPlan.PromotionLastVersionId_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONKEYWORDSLIMITNUMBER", "#getUserPlan.PromotionKeywordsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONMLPSLIMITNUMBER", "#getUserPlan.PromotionMlpsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "PROMOTIONSHORTURLSLIMITNUMBER", "#getUserPlan.PromotionShortUrlsLimitNumber_int#") />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEPLAN", "#getUserPlan.UserDowngradePlan_int#") />
            

            <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
        <cfelse>
            
           <cfquery name="getUserPlan" datasource="#Session.DBSourceREAD#">
                SELECT 
                p.PlanId_int,
                p.PlanName_vch,
                p.Status_int,
                p.Amount_dec,
                p.UserAccountNumber_int,
                p.KeywordsLimitNumber_int,
                p.FirstSMSIncluded_int,
                p.CreditsAddAmount_int,
                p.PriceMsgAfter_dec,
                p.PriceKeywordAfter_dec,
                p.Order_int,
                p.MlpsLimitNumber_int,
                p.ShortUrlsLimitNumber_int,
                p.MlpsImageCapacityLimit_bi
                FROM simplebilling.plans p
                WHERE  PlanId_int = 1 
                
            </cfquery>
            
            <!--- MLPSLIMITNUMBER --->
            <cfif getUserPlan.MlpsLimitNumber_int EQ 0>
                <cfset limitNumber = "Unlimited"/>
            <cfelse>
                <cfset limitNumber = getUserPlan.MlpsLimitNumber_int  + getUserPlan.PromotionMlpsLimitNumber_int/>
            </cfif>                

            <!--- SHORTURLSLIMITNUMBER --->
            <cfif getUserPlan.ShortUrlsLimitNumber_int EQ 0>
                <cfset shortUrl = "Unlimited"/>
            <cfelse>
                <cfset shortUrl = getUserPlan.ShortUrlsLimitNumber_int  + getUserPlan.PromotionShortUrlsLimitNumber_int/>
            </cfif>

            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", 0) />
            <cfset QuerySetCell(dataout, "USERPLANID", "0") /> 
            <cfset QuerySetCell(dataout, "STATUS", "#getUserPlan.Status_int#") /> 
            <cfset QuerySetCell(dataout, "PLANID", "#getUserPlan.PlanId_int#") />
            <cfset QuerySetCell(dataout, "PLANORDER", "#getUserPlan.Order_int#") /> 
            <cfset QuerySetCell(dataout, "STARTDATE", "") />
            <cfset QuerySetCell(dataout, "ENDDATE", "") />
            <cfset QuerySetCell(dataout, "PLANNAME", "#getUserPlan.PlanName_vch#") />    
            <cfset QuerySetCell(dataout, "AMOUNT", "#getUserPlan.Amount_dec#") />  
            <cfset QuerySetCell(dataout, "USERACCOUNTNUMBER", "#getUserPlan.UserAccountNumber_int#") />  
            <cfset QuerySetCell(dataout, "KEYWORDSLIMITNUMBER", "#getUserPlan.KeywordsLimitNumber_int#") />  
            <cfset QuerySetCell(dataout, "FIRSTSMSINCLUDED", "#getUserPlan.FirstSMSIncluded_int#") />  
            <cfset QuerySetCell(dataout, "PROMOTIONCREDIT", "#getUserBilling.PromotionCreditBalance_int#") />
            <cfset QuerySetCell(dataout, "USERPLANFIRSTSMSINCLUDED", "0") />  
            <cfset QuerySetCell(dataout, "PRICEMSGAFTER", "#getUserPlan.PriceMsgAfter_dec#") /> 
            <cfset QuerySetCell(dataout, "PRICEKEYWORDAFTER", "#getUserPlan.PriceKeywordAfter_dec#") />
            <cfset QuerySetCell(dataout, "KEYWORDPURCHASED", "0") />
            <cfset QuerySetCell(dataout, "TOTALKEYWORD", "#totalKeyword#") />
            <cfset QuerySetCell(dataout, "KEYWORDUSE", "#keywordUse#") />
            <cfset QuerySetCell(dataout, "KEYWORDAVAILABLE", "#KeywordAvailable#") />  
            <cfset QuerySetCell(dataout, "KEYWORDBUYAFTEREXPRIED", "0") /> 
            <cfset QuerySetCell(dataout, "PLANEXPIRED", "0") />
            <cfset QuerySetCell(dataout, "PLANKEYWORDSLIMITNUMBER", "0") /> 
            <cfset QuerySetCell(dataout, "MLPSLIMITNUMBER", "#limitNumber#") />
            <cfset QuerySetCell(dataout, "SHORTURLSLIMITNUMBER", "#shortUrl#") /> 
            <cfset QuerySetCell(dataout, "IMAGECAPACITYLIMIT", "#getUserPlan.MlpsImageCapacityLimit_bi#") /> 

            <cfset QuerySetCell(dataout, "PROMOTIONID", 0) />
            <cfset QuerySetCell(dataout, "PROMOTIONLASTVERSIONID", 0) />
            <cfset QuerySetCell(dataout, "PROMOTIONKEYWORDSLIMITNUMBER", 0) />
            <cfset QuerySetCell(dataout, "PROMOTIONMLPSLIMITNUMBER", 0) />
            <cfset QuerySetCell(dataout, "PROMOTIONSHORTURLSLIMITNUMBER", 0) />
            <cfset QuerySetCell(dataout, "USERDOWNGRADEPLAN", "0") />

            <cfset QuerySetCell(dataout, "MESSAGE", "OK")/>
            <cfset QuerySetCell(dataout, "ERRMESSAGE", "")/>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <!--- This function do not use in sire code --->
    <!---
    <cffunction name='insertUsersPlan' access="remote" output="true">
        <cfargument name="inpPlanId" type="string" required="yes" default="1">
        <cfargument name="inpUserId" type="string" required="yes" default="0">
        <cfargument name="inpStarDate" type="date" required="yes">
        <cfargument name="inpEndDate" type="date" required="yes">

		<cfset var dataout	= {} />
		<cfset var Amount_dec	= '' />
		<cfset var UserAccountNumber_int	= '' />
		<cfset var KeywordsLimitNumber_int	= '' />
		<cfset var FirstSMSIncluded_int	= '' />
		<cfset var PriceMsgAfter_dec	= '' />
		<cfset var insertUsersPlan	= '' />
		<cfset var planInfo	= '' />
		<cfset var planadd	= '' />
        <cfset dataout = {}>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.USERID = "#arguments.inpUserId#" />
        <cfset dataout.PLANID = "#arguments.inpPlanId#" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfinvoke method="GetOrderPlan" returnvariable="planInfo">
            <cfinvokeargument name="plan" value="#inpPlanId#">
        </cfinvoke>

        <cfif planInfo.RXRESULTCODE GT 0 && inpUserId GT 0>
            <cfset Amount_dec = planInfo.AMOUNT>
            <cfset UserAccountNumber_int = planInfo.USERACCOUNTNUMBER>
            <cfset KeywordsLimitNumber_int = planInfo.KEYWORDSLIMITNUMBER>
            <cfset FirstSMSIncluded_int = planInfo.FIRSTSMSINCLUDED>
            <cfset PriceMsgAfter_dec = planInfo.FIRSTSMSINCLUDED>

			<cftry>
                <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
                    INSERT INTO simplebilling.userplans
                    (
                        Status_int,
                        UserId_int,
                        PlanId_int,
                        Amount_dec,
                        UserAccountNumber_int,
                        KeywordsLimitNumber_int,
                        FirstSMSIncluded_int,
                        PriceMsgAfter_dec,
                        StartDate_dt,
                        EndDate_dt
                    )
                    VALUES 
                    (
                        1,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpPlanId#">,
                        <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#Amount_dec#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#UserAccountNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#KeywordsLimitNumber_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#FirstSMSIncluded_int#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#PriceMsgAfter_dec#">,
                        <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.inpStarDate#">,
                        <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.inpEndDate#">
                    )
                </cfquery>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.MESSAGE = "Add plan success" />
            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "Add Plan Errors." />
                <cfset dataout.ERRMESSAGE = "Add Plan Errors."/>
                <cfreturn dataout /> 
            </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "Plan #inpPlanId# Or User #inpUserId# not found" />
            <cfset dataout.ERRMESSAGE = "Plan #inpPlanId# Or User #inpUserId# not found"/>
            <cfreturn dataout />   
        </cfif>  
        <cfreturn dataout />    

    </cffunction>
    --->
    
    <cffunction name="updatePaymentWorlDay" >
        <cfargument name="planId" type="string" default="0">
        <cfargument name="numberSMS" type="string" default="0">
        <cfargument name="numberKeyword" type="string" default="0">
        <cfargument name="moduleName" type="string" required="no" default="">
        <cfargument name="paymentRespose" type="string" required="yes" default="">
        
        <cfset var data	= '' />
		<cfset var updatePayment	= '' />
        <cftry>
        	<cfset data = deserializeJSON(paymentRespose)>
        	<cfquery name="updatePayment" datasource="#Session.DBSourceEBM#" result="updatePayment">
        		INSERT INTO `simplebilling`.`payment_worldpay` 
        		(	
        			`userId_int`,
        			 `planId`, 
        			 `moduleName`,
        			 `bynumberKeyword`,
        			 `bynumberSMS`,
        			 `transactionType`, 
        			 `customerId`, 
        			 `orderId`, 
        			 `transactionId`, 
        			 `authorizationCode`, 
        			 `authorizedAmount`, 
        			 `paymentTypeCode`, 
        			 `paymentTypeResult`, 
        			 `transactionData_date`, 
        			 `transactionData_amount`, 
        			 `creditCardType`, 
        			 `cardNumber`, 
        			 `avsCode`, 
        			 `cardHolder_FirstName`, 
        			 `cardHolder_LastName`, 
        			 `billAddress_line1`, 
        			 `billAddress_city`, 
        			 `billAddress_state`, 
        			 `billAddress_zip`, 
        			 `billAddress_company`, 
        			 `billAddress_phone`, 
        			 `email`, 
        			 `method`, 
        			 `responseText`,
        			 `created`
        		)VALUES 
                (
                     <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                     <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.planId#">,
                     <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
                     <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberKeyword#">,
                     <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.numberSMS#">,
                     <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionType#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.customerId#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.orderId#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionId#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.authorizationCode#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.authorizedAmount#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.paymentTypeCode#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.paymentTypeResult#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionData.date#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.transactionData.amount#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.creditCardType#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardNumber#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.avsCode#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardHolder_FirstName#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.cardHolder_LastName#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.line1#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.city#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.state#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.zip#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.company#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.billAddress.phone#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.email#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.method#">, 
        			 <cfqueryparam cfsqltype="cf_sql_varchar" value="#data.transaction.responseText#">,
        			 NOW()
                )
        	</cfquery>
        <cfcatch type="any">
        </cfcatch>
        </cftry>
    	
    </cffunction>
    
    <cffunction name="updateLogPaymentWorlPay" >
        <cfargument name="moduleName" type="string" required="no" default="">
        <cfargument name="status_code" type="string" required="no" default="">
        <cfargument name="status_text" type="string" required="no" default="">
        <cfargument name="errordetail" type="string" required="no" default="">
        <cfargument name="filecontent" type="string" required="no" default="">
        <cfargument name="paymentdata" required="no" default="">
        <cfargument name="inpPaymentByUserId" required="no" default="">
                
        <cfset var updateLogPayment	= '' />
        <cfquery name="updateLogPayment" datasource="#Session.DBSourceEBM#" result="updateLogPayment">
        	INSERT INTO `simplebilling`.`log_payment_worldpay` 
        	(
        		`userId_int`,
        		`moduleName`,
        		`status_code`,
        		`status_text`,
        		`errordetail`,
                `filecontent`,
        		`paymentdata`,
        		`created`,
                `PaymentByUserId_int`
                
        	) VALUES 
        	(
        		<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.moduleName#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.status_code#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.status_text#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.errordetail#">,
                <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.filecontent#">,
        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJSON(arguments.paymentdata)#">,
        		NOW(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentByUserId#">,
        	);

        </cfquery>
    </cffunction>

    <cffunction name="checkUserPlanExpried" output="true" hint="check if your plan is expried, expried within 30d or over 30 days">
        <cfargument name="userId" type="numeric" required="yes" default="">
        <!---
        RXRESULTCODE :
        -2 : errors
        -1 : user not exit anymore
        0 : user plan is not exprired
        1 : user plan is expired within 30d
        2 : user plan is expired over 30d to run downgrade
        3 : user plan is already downgrade
        --->

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />
        <cfset dataout.DIFFDATE = 0 />
        <cfset dataout.ENDDATE = "" />

        <cfset var userPlansQuery = ''/>

        <cfif arguments.userId GT 0>
            
            <cftry>

                <cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
                    SELECT UserPlanId_bi,EndDate_dt,DATEDIFF(EndDate_dt,CURDATE()) AS DiffDate, BuyKeywordNumber_int, BuyKeywordNumberExpired_int, KeywordsLimitNumber_int, DowngradeDate_dt 
                    FROM simplebilling.userplans
                    WHERE Status_int = 1
                    <!--- AND DATE(EndDate_dt) <= CURDATE() --->
                    AND UserId_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#arguments.userId#">
                    ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
                </cfquery>

                <cfif userPlansQuery.RecordCount GT 0>

                    <cfset dataout.DIFFDATE = userPlansQuery.DiffDate />
                    <cfset dataout.ENDDATE = userPlansQuery.EndDate_dt />

                    <cfif userPlansQuery.DiffDate GT 0 AND userPlansQuery.BuyKeywordNumberExpired_int EQ 0>
                        <cfset dataout.RXRESULTCODE = 0 />
                    <cfelseif userPlansQuery.DiffDate LT 0 AND userPlansQuery.DiffDate GT -30>                           
                        <cfset dataout.RXRESULTCODE = 1 />
                    <cfelseif userPlansQuery.DiffDate LTE -30 >                               
                        <cfset dataout.RXRESULTCODE = 2 />
                    <cfelseif userPlansQuery.DowngradeDate_dt NEQ '' >
                        <cfset dataout.RXRESULTCODE = 3 />
                    <cfelseif userPlansQuery.DiffDate EQ 0>
                        <cfset dataout.RXRESULTCODE = 1 />
                    </cfif>     
                <cfelse> 
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "UserPlan not exits" />
                </cfif>

            <cfcatch type="any">
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.MESSAGE = "There is an errors when check user plan expired" />
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                <cfreturn dataout>
            </cfcatch>    
            </cftry>

        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "UserId not exits" />
        </cfif>    

        <cfreturn dataout>
    </cffunction>

    <cffunction name="UpdateUserPlanBalance" output="true" hint="Reset user plan balance when insert new plan">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfargument name="inpBalance" type="numeric" required="yes" default="0">
        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var updateBalanceBillings = ''/>
        <cfset var balanceBillingsResult = ''/>

        <cfif arguments.inpUserId GT 0 AND arguments.inpBalance GTE 0>
            <cftry>
                <!--- RESET PLAN BALANCE --->
                <cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
                    UPDATE
                        simplebilling.billing
                    SET
                        Balance_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inpBalance#">
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                </cfquery>
                <cfset dataout.RxResultCode = 1 />
            <cfcatch>
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry>
        <cfelse>
            <cfset dataout.Message = "UserId not exits" />
        </cfif>

        <cfreturn dataout/>

    </cffunction> 

    <cffunction name="ReleaseUserData" output="true" access="public" hint="release user keyword,mlp,shorturl">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfargument name="inpkeywordUsed" type="numeric" required="yes" default="0">
        <cfargument name="inpshortUrlUsed" type="numeric" required="yes" default="0">
        <cfargument name="inpMLPUsed" type="numeric" required="yes" default="0">
        <cfargument name="inpLimitKeyword" type="numeric" required="yes" default="0">
        <cfargument name="inpLimitMLP" type="numeric" required="yes" default="0">
        <cfargument name="inpLimitShortUrl" type="numeric" required="yes" default="0">
        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />
        <cfset var getKeyword = ''/>
        <cfset var numKeyword = 0/>
        <cfset var listKeywordRelease = []/>
        <cfset var releaseKeyword = ''/>

        <cfset var getMLP = ''/>
        <cfset var numMLP = 0/>
        <cfset var listMLPRelease = []/>
        <cfset var releaseMLP = ''/>

        <cfset var getShortUrl = ''/>
        <cfset var numShortUrl = 0/>
        <cfset var listShortUrlRelease = []/>
        <cfset var releaseShortUrl = ''/>

        <cftransaction>
            <cftry>
                <cftransaction action="begin">
                     <!--- release if keyword used > free plan keyword --->
                    <cfif arguments.inpkeywordUsed GT arguments.inpLimitKeyword>
                        <cfquery name="getKeyword" datasource="#Session.DBSourceEBM#">
                            SELECT
                                kw.Keyword_vch,
                                kw.Created_dt,
                                kw.KeywordId_int,
                                kw.BatchId_bi
                            FROM sms.keyword AS kw
                            INNER JOIN simpleobjects.batch AS ba 
                            ON kw.BatchId_bi = ba.BatchId_bi 
                            WHERE kw.Active_int = 1
                            AND ba.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                            <!--- AND kw.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpShortCode#"> --->
                            GROUP BY 
                                kw.Keyword_vch
                            ORDER BY
                                kw.Created_dt
                        </cfquery>
                       
                        <cfif getKeyword.RecordCount GT 0>
                            <cfset numKeyword =1>
                            <cfloop query="#getKeyword#">
                                <cfif numKeyword GT arguments.inpLimitKeyword>
                                    <cfset arrayAppend(listKeywordRelease, KeywordId_int)>     
                                </cfif>
                                <cfset numKeyword ++>
                            </cfloop>

                            <cfif arrayLen(listKeywordRelease) GT 0>
                                <cfquery name="releaseKeyword" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        sms.keyword 
                                    SET
                                       Active_int = 0
                                    WHERE 
                                        KeywordId_int IN (#arrayToList(listKeywordRelease)#) 
                                </cfquery>
                            </cfif>
                        </cfif>
                    </cfif>

                    <!--- release if shortUrl used > free plan shortURL --->
                    <cfif arguments.inpshortUrlUsed GT arguments.inpLimitShortUrl>
                        <cfquery name="getShortUrl" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                PKId_bi 
                            FROM 
                                simplelists.url_shortner
                            WHERE 
                                Active_int = 1
                            AND 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 
                            ORDER BY 
                                PKId_bi
                        </cfquery>
                    
                        <cfif getShortUrl.RecordCount GT 0>
                            <cfset numShortUrl =1>
                            <cfloop query="#getShortUrl#">
                                <cfif numShortUrl GT arguments.inpLimitShortUrl>
                                    <cfset arrayAppend(listShortUrlRelease, PKId_bi)>      
                                </cfif>
                                <cfset numShortUrl ++>
                            </cfloop>
                            <cfif arrayLen(listShortUrlRelease) GT 0>
                                <cfquery name="releaseShortUrl" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.url_shortner 
                                    SET
                                       Active_int = 0
                                    WHERE 
                                        PKId_bi IN (#arrayToList(listShortUrlRelease)#) 
                                </cfquery>
                            </cfif>
                        </cfif>
                    </cfif>

                    <!--- release if MLP used > free plan MLP --->
                    <cfif arguments.inpMLPUsed GT arguments.inpLimitMLP>
                        <cfquery name="getMLP" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                ccpxDataId_int 
                            FROM 
                                simplelists.cppx_data
                            WHERE 
                                status_int = 1
                            AND 
                                MlpType_ti = 1    
                            AND 
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#"> 
                            ORDER BY 
                                ccpxDataId_int
                        </cfquery>
                    
                        <cfif getMLP.RecordCount GT 0>
                            <cfset numMLP =1>
                            <cfloop query="#getMLP#">
                                <cfif numMLP GT arguments.inpLimitMLP>
                                    <cfset arrayAppend(listMLPRelease, ccpxDataId_int)>        
                                </cfif>
                                <cfset numMLP ++>
                            </cfloop>
                            <cfif arrayLen(listMLPRelease) GT 0>
                                <cfquery name="releaseMLP" datasource="#Session.DBSourceEBM#">
                                    UPDATE 
                                        simplelists.cppx_data
                                    SET
                                       status_int = 0
                                    WHERE 
                                        ccpxDataId_int IN (#arrayToList(listMLPRelease)#) 
                                </cfquery>
                            </cfif>
                        </cfif>
                    </cfif>
                <cftransaction action="commit">    
                <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch>
                <cftransaction action="rollback">
                <cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
        </cftransaction>
    <cfreturn dataout/>    
    </cffunction>   

    <cffunction name="DeactiveUserData" output="true" access="public" hint="deactive user keyword,mlp,shorturl">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfargument name="inpDowngradeKeyword" type="array" required="yes" default="0">
        <cfargument name="inpDowngradeShortUrl" type="array" required="yes" default="0">
        <cfargument name="inpDowngradeMLP" type="array" required="yes" default="0">

        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var UpdateKeywordQuery = ''/>
        <cfset var updateShortUrl = ''/>
        <cfset var UpdateMLPQuery = ''/>
       

        <cftransaction>
            <cftry>
                <cftransaction action="begin">
                    <!--- deactive keyword, mlp, shorturl --->
                    <cfif arrayLen(arguments.inpDowngradeKeyword) GT 0>
                        <cfquery name="UpdateKeywordQuery" datasource="#Session.DBSourceEBM#">              
                            UPDATE 
                                sms.keyword 
                            SET
                                Active_int = 2
                            WHERE 
                                KeywordId_int IN (#arrayToList(arguments.inpDowngradeKeyword)#) 
                        </cfquery>      
                    </cfif>

                    <cfif arrayLen(arguments.inpDowngradeShortUrl) GT 0>
                        <cfquery name="updateShortUrl" datasource="#Session.DBSourceEBM#" >
                            UPDATE
                                simplelists.url_shortner
                            SET
                                Active_int = 2
                            WHERE
                                PKId_bi IN (#arrayToList(arguments.inpDowngradeShortUrl)#)
                             AND
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">     
                        </cfquery>    
                    </cfif>

                    <cfif arrayLen(arguments.inpDowngradeMLP) GT 0>
                        <cfquery name="UpdateMLPQuery" datasource="#Session.DBSourceEBM#">              
                           UPDATE 
                                simplelists.cppx_data
                            SET 
                                status_int = 2
                            WHERE  
                                ccpxDataId_int IN (#arrayToList(arguments.inpDowngradeMLP)#)
                             AND
                                UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">       
                        </cfquery>      
                    </cfif>
                    <!--- deactive keyword, mlp, shorturl --->  
                <cftransaction action="commit">    
                <cfset dataout.RXRESULTCODE = 1 />
            <cfcatch>
                <cftransaction action="rollback">
                <cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
        </cftransaction>
    <cfreturn dataout/>    
    </cffunction> 

    <cffunction name="logPaymentWorldPay" hint="log all payment wordpay response when run recurring" access="public" >
        <cfargument name="inplogPaymentWorldpays"required="yes">
        <cfargument name="inpFcm"required="yes">
        <cfargument name="inpNow"required="yes">
        

        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset var logPaymentWorldpays = arguments.inplogPaymentWorldpays />
        <cfset var _comma = '' />
        <cfset var _log = '' />
        <cfset var insertLogPaymentWorldpays = '' />
        <cftry>
            
        
            <cfif !arrayIsEmpty(logPaymentWorldpays[3])>
                <cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#">
                    INSERT INTO `simplebilling`.`recurringlog` (
                        `PaymentDate_dt`,
                        `Status_ti`,
                        `UserPlanId_bi`,
                        `UserId_int`,
                        `PlanId_int`,
                        `FirstName_vch`,
                        `EmailAddress_vch`,
                        `Amount_dec`,
                        `PaymentData_txt`,
                        `PaymentResponse_txt`,
                        `LogFile_vch`,
                        `paymentGateway_ti`,
                        `PaymentByUserId_int`                        
                    ) VALUES
                    <cfset _comma = ''> 
                    <cfloop array="#logPaymentWorldpays[3]#" index="_log">
                        <cfif not IsDefined("_log[12]")>
                            <cfset _log[12]=0>
                        </cfif>
                        #_comma#(
                            <cfqueryparam CFSQLTYPE="cf_sql_timestamp" VALUE="#_log[1]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#!_log[9]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[2]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[3]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[4]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[5]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6]#">,
                            <cfqueryparam cfsqltype="cf_sql_decimal" value="#_log[7]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[8]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[10]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#inpFcm#/#inpNow#.txt">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[11]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[12]#">
                        )
                        <cfset _comma = ','> 
                    </cfloop>
                </cfquery>
            </cfif>
            
            <cfif !arrayIsEmpty(logPaymentWorldpays[2])>
                <cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#">
                    INSERT INTO `simplebilling`.`log_payment_worldpay` (
                        `userId_int`,
                        `moduleName`,
                        `status_code`,
                        `status_text`,
                        `errordetail`,
                        `filecontent`,
                        `paymentdata`,
                        `created`,
                        `PaymentByUserId_int`
                        
                    ) VALUES
                    <cfset _comma = ''> 
                    <cfloop array="#logPaymentWorldpays[2]#" index="_log">
                        <cfif not IsDefined("_log[8]")>
                            <cfset _log[8]=0>
                        </cfif>
                        #_comma#(
                            <cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#_log[1]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[2]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[3]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[4]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[5]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJson(_log[7])#">,
                            NOW(),
                            <cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#_log[8]#">
                        )
                        <cfset _comma = ','> 
                    </cfloop>
                </cfquery>
            </cfif>

            
            <cfif !arrayIsEmpty(logPaymentWorldpays[1])>
                <cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#">
                    INSERT INTO `simplebilling`.`payment_worldpay` (
                        `userId_int`,
                        `planId`, 
                        `moduleName`,
                        `bynumberKeyword`,
                        `bynumberSMS`,
                        `transactionType`, 
                        `customerId`, 
                        `orderId`, 
                        `transactionId`, 
                        `authorizationCode`, 
                        `authorizedAmount`, 
                        `paymentTypeCode`, 
                        `paymentTypeResult`, 
                        `transactionData_date`, 
                        `transactionData_amount`, 
                        `creditCardType`, 
                        `cardNumber`, 
                        `avsCode`, 
                        `cardHolder_FirstName`, 
                        `cardHolder_LastName`, 
                        `billAddress_line1`, 
                        `billAddress_city`, 
                        `billAddress_state`, 
                        `billAddress_zip`, 
                        `billAddress_company`, 
                        `billAddress_phone`, 
                        `email`, 
                        `method`, 
                        `responseText`,
                        `created`,
                        `PaymentByUserId_int`
                    ) VALUES
                    <cfset _comma = ''> 
                    <cfloop array="#logPaymentWorldpays[1]#" index="_log">
                        <cfif not IsDefined("_log[7]")>
                            <cfset _log[7]=0>
                        </cfif>
                        #_comma#(
                            <cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#_log[1]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[2]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[3]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[4]#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[5]#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionType#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.customerId#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.orderId#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionId#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.authorizationCode#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.authorizedAmount#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.paymentTypeCode#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.paymentTypeResult#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionData.date#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionData.amount#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.creditCardType#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardNumber#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.avsCode#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardHolder_FirstName#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardHolder_LastName#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.line1#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.city#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.state#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.zip#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.company#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.phone#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.email#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.method#">, 
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.responseText#">,
                            NOW(),
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[7]#">
                        )
                        <cfset _comma = ','> 
                    </cfloop>
                </cfquery>
            </cfif>

        <cfcatch>
            <cfset dataout.MESSAGE = "#cfcatch.Message#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>    
        </cftry>    

        <cfreturn dataout/>    
    </cffunction>  


    <cffunction name="UpdateUserPlanAddBenefitDate" output="true" hint="update monthly benefit date">
        <cfargument name="inpUserPlanId" type="numeric" required="yes" default="0">
        <cfargument name="inpMonthlyAddBenefitDate" required="yes">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var updateUserPlan = ''/>

        <cfif arguments.inpUserPlanId GT 0 AND arguments.inpMonthlyAddBenefitDate NEQ '' AND arguments.inpUserId GT 0>
            <!--- <cftry> --->
                <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplebilling.userplans
                    SET
                        MonthlyAddBenefitDate_dt = <CFQUERYPARAM cfsqltype="CF_SQL_DATE" value="#arguments.inpMonthlyAddBenefitDate#">
                    WHERE
                        UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserPlanId#">
                    AND 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">        
                </cfquery>
                <cfset dataout.RxResultCode = 1 />
            <!---  
            <cfcatch>
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry> 
            --->
        <cfelse>
            <cfset dataout.Message = "Invalid input data" />
        </cfif>

        <cfreturn dataout/>

    </cffunction> 

    <cffunction name="UpdateNumberOfRecurring" output="true" hint="update number of recurring fail">
        <cfargument name="inpUserPlanId" type="numeric" required="yes" default="0">
        <cfargument name="inpRecurringType" type="numeric" required="yes">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">

        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var updateUserPlan = ''/>

        <cfif arguments.inpUserPlanId GT 0 AND arguments.inpRecurringType GTE 0 AND arguments.inpUserId GT 0>
            <!--- <cftry> --->
                <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplebilling.userplans
                    SET

                    <cfif inpRecurringType EQ 0>
                        NumOfRecurringPlan_int = NumOfRecurringPlan_int + 1
                    <cfelse> <!--- inpRecurringType = 2 --->
                        NumOfRecurringKeyword_int = NumOfRecurringKeyword_int + 1
                    </cfif>
                        
                    WHERE
                        UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserPlanId#">
                    AND 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">            
                </cfquery>
                <cfset dataout.RxResultCode = 1 />

            <!---  
            <cfcatch>
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry> 
            --->
        <cfelse>
            <cfset dataout.Message = "Invalid input data" />
        </cfif>

        <cfreturn dataout/>

    </cffunction>

    <cffunction name="ResetNumberOfRecurring" output="true" hint="reset number of recurring fail">
        <cfargument name="inpUserPlanId" type="numeric" required="yes" default="0">
        <cfargument name="inpRecurringType" type="numeric" required="yes">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">

        <cfset var dataout = {}/>
        <cfset dataout.RxResultCode = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var updateUserPlan = ''/>

        <cfif arguments.inpUserPlanId GT 0 AND arguments.inpRecurringType GTE 0 AND arguments.inpUserId GT 0>
            <cftry>
                <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simplebilling.userplans
                    SET

                    <cfif inpRecurringType EQ 0>
                        NumOfRecurringPlan_int = 0
                    <cfelse> <!--- inpRecurringType = 2 --->
                        NumOfRecurringKeyword_int = 0
                    </cfif>
                        
                    WHERE
                        UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserPlanId#">
                    AND 
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">            
                </cfquery>
                <cfset dataout.RxResultCode = 1 />
             
            <cfcatch>
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry> 
           
        <cfelse>
            <cfset dataout.Message = "Invalid input data" />
        </cfif>

        <cfreturn dataout/>

    </cffunction> 

    <cffunction name="ResetBuyKeyword" output="true" hint="reset number of buy keywords">
        <cfargument name="inpUserPlanId" type="numeric" required="yes" default="0">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfargument name="inpNumberOfBuyKeyword" type="numeric" required="yes" default="0">
        <cfset var dataout = {}/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />

        <cfset var updateUserPlan = ''/>

        <cfif arguments.inpUserPlanId GT 0 AND arguments.inpNumberOfBuyKeyword GTE 0 AND arguments.inpUserId GT 0>
            <cftry>
                <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#">
                        UPDATE
                            simplebilling.userplans
                        SET
                            BuyKeywordNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpNumberOfBuyKeyword#">
                        WHERE
                            UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserPlanId#">
                        AND 
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">    
                    </cfquery>
                <cfset dataout.RXRESULTCODE = 1 />
             
            <cfcatch>
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    
            </cftry> 
           
        <cfelse>
            <cfset dataout.Message = "Invalid input data" />
        </cfif>

        <cfreturn dataout/>

    </cffunction> 

    <cffunction name="ReleaseUserKeyword" output="true" access="public" hint="release user keyword when can not payment monthly">
        <cfargument name="inpUserId" type="numeric" required="yes" default="0">
        <cfargument name="inpkeywordUsed" type="numeric" required="yes" default="0">
        <cfargument name="inpLimitKeyword" type="numeric" required="yes" default="0">
        <cfargument name="inpUserPlanId" type="numeric" required="yes" default="0">

        <cfset var dataout = {}/>
        <cfset dataout.LISTKEYWORD = []/>
        <cfset dataout.TOTALKEYWORD = 0/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.Message = '' />
        <cfset dataout.ErrMessage = '' />
        <cfset var getKeyword = ''/>
        <cfset var numKeyword = 0/>
        <cfset var listKeywordRelease = []/>
        <cfset var listKeywordNameRelease = []/>
        <cfset var releaseKeyword = ''/>
        <cfset var updateUserPlan = {}/>
        <cfset var RXResetBuyKeyword = {}>

        <cftransaction>
            <cftry>
                <cftransaction action="begin">
                 <!--- release if keyword used > limiy keyword --->
                <cfif arguments.inpkeywordUsed GT arguments.inpLimitKeyword>
                    <cfquery name="getKeyword" datasource="#Session.DBSourceEBM#">
                        SELECT
                            kw.Keyword_vch,
                            kw.Created_dt,
                            kw.KeywordId_int,
                            kw.BatchId_bi
                        FROM sms.keyword AS kw
                        INNER JOIN simpleobjects.batch AS ba 
                        ON kw.BatchId_bi = ba.BatchId_bi 
                        WHERE kw.Active_int = 1
                        AND ba.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                        <!--- AND kw.ShortCodeId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpShortCode#"> --->
                        GROUP BY 
                            kw.Keyword_vch
                        ORDER BY
                            kw.Created_dt
                    </cfquery>
                   
                    <cfif getKeyword.RecordCount GT 0>
                        <cfset numKeyword =1>
                        <cfloop query="#getKeyword#">
                            <cfif numKeyword GT arguments.inpLimitKeyword>
                                <cfset arrayAppend(listKeywordRelease, KeywordId_int)>
                                <cfset arrayAppend(listKeywordNameRelease, Keyword_vch) >     
                            </cfif>
                            <cfset numKeyword ++>
                        </cfloop>

                        <cfset dataout.TOTALKEYWORD = arrayLen(listKeywordRelease)/>
                        <cfif arrayLen(listKeywordRelease) GT 0>
                            <cfquery name="releaseKeyword" datasource="#Session.DBSourceEBM#">
                                UPDATE 
                                    sms.keyword 
                                SET
                                   Active_int = 0
                                WHERE 
                                    KeywordId_int IN (#arrayToList(listKeywordRelease)#) 
                            </cfquery>
                        </cfif>
                    </cfif>

                </cfif>

                <cfinvoke method="ResetBuyKeyword" component="public.sire.models.cfc.order_plan" returnvariable="RXResetBuyKeyword">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    <cfinvokeargument name="inpNumberOfBuyKeyword" value="0">
                    <cfinvokeargument name="inpUserPlanId" value="#arguments.inpUserPlanId#">
                </cfinvoke>

                <cfset dataout.LISTKEYWORD = listKeywordNameRelease />

                <cfif RXResetBuyKeyword.RXRESULTCODE EQ 1>
                    <cftransaction action="commit">
                    <cfset dataout.RXRESULTCODE = 1 />
                <cfelse>
                    <cftransaction action="rollback">
                    <cfset dataout.RXRESULTCODE = -2 />
                    <cfset dataout.MESSAGE = "#RXResetBuyKeyword.Message#" />
                    <cfset dataout.ERRMESSAGE = "#RXResetBuyKeyword.ERRMESSAGE#" />    
                </cfif>

            <cfcatch>
                <cftransaction action="rollback">
                <cfset dataout.RXRESULTCODE = -100 />
                <cfset dataout.MESSAGE = "#cfcatch.Message#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
            </cftry>
        </cftransaction>
    <cfreturn dataout/>    
    </cffunction>   


</cfcomponent>

