<cfcomponent entityName="plan" >

	<cfinclude template="/public/sire/configs/paths.cfm">
	<cfparam name="Session.DBSourceREAD" default="bishop_read"/>



	<cffunction name="GetActivedPlans" access="public" hint="GET All Actived plans">
		<cfargument name="inpNotIncludeEnterprisePlan" required="no" default="0">
		<!--- Define local scope dataout variable  --->
		<cfset var dataout = {} />
		<cfset var getPlan = {} />

		<!--- set default return value--->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.DATA = {} />

		<cftry>
			
			<cfquery name="getPlan" datasource="#Session.DBSourceREAD#" >
				SELECT 
					PlanId_int AS id, 
					PlanName_vch AS name, 
					Amount_dec AS price,
					YearlyAmount_dec AS yearlyAmount,
					KeywordsLimitNumber_int, 
					FirstSMSIncluded_int,
					PriceKeywordAfter_dec,
					PriceMsgAfter_dec,
					Order_int as level,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int,
					Status_int					
				FROM 
					simplebilling.plans
				WHERE 
				<cfif arguments.inpNotIncludeEnterprisePlan EQ 1>
					Status_int IN(1)
				<cfelse>
					Status_int IN(1,4)
				</cfif>
				ORDER BY 
					Order_int ASC, 
					Amount_dec ASC 
			</cfquery>			
			<cfset dataout.DATA = getPlan />
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1>  
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">   
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction> 

	<cffunction name="GetActivedAndPrivatePlans" access="public" hint="GET All Actived and Private plans">
		<!--- Define local scope dataout variable  --->
		<cfset var dataout = {} />
		<cfset var getPlan = {} />

		<!--- set default return value--->
		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.TYPE = "" />
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.DATA = {} />

		<cftry>

			<cfquery name="getPlan" datasource="#Session.DBSourceREAD#" >
				SELECT 
					PlanId_int AS id, 
					PlanName_vch AS name,
					Amount_dec AS price,
					YearlyAmount_dec AS yearlyAmount,
					KeywordsLimitNumber_int, 
					FirstSMSIncluded_int,
					PriceKeywordAfter_dec,
					PriceMsgAfter_dec,
					Order_int as level,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int
				FROM 
					simplebilling.plans
				WHERE 
					Status_int = 1
				OR
					Status_int = 8
				ORDER BY 
					Order_int ASC, 
					Amount_dec ASC 
			</cfquery>

			<cfset dataout.DATA = getPlan />
			<cfset dataout.RXRESULTCODE = 1 />

			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1>  
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"> 
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#">   
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

</cfcomponent>