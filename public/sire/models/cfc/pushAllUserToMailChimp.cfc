<cfcomponent>
	<cfsetting RequestTimeout="86400"/>
	<cffunction name="PushAllUserToMailChimp" access="remote" hint="Push all user email address to mailchimp">
		<cfset var dataout = {} />
		<cfset dataout.RXRESULTCODE = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset var getAllSireUser = '' />
		<cfset var getPlanResult = '' />
		<cfset var getStatus = '' />
		<cfset var getPlanName = '' />
		<cfset var getLastActiveDate = '' />
		<cfset var getDeactiveDate = '' />
		<cfset var lastActiveDate = '' />
		<cfset var deactiveDate = '' />
		<cfset var getUserPlan = '' />
		<cfset var getPlanId = '' />
		<cfset var getPlan = '' />
		<cfset var getPlanName = '' />
		<cfset var temp = '' />
		<cfset var RetVarmailChimpAPIs = '' />
		<cfset var dataout['list'] = ArrayNew(1) />

		<cftry>
			<cfquery name="getAllSireUser" datasource="#Session.DBSourceEBM#">
				SELECT
			     uac.UserId_int,
			     uac.EmailAddress_vch,
			     uac.FirstName_vch,
			     uac.LastName_vch,
			     uac.Created_dt AS SignUpDate,
			     uac.LastLogIn_dt,
			     uac.Active_int,
				uac.MFAContactString_vch
			    FROM
			     simpleobjects.useraccount as uac
			    LEFT JOIN
			     simpleobjects.deactivate_logs as sdl
			    ON
			     uac.UserId_int = sdl.UserId_int
			    AND
			     sdl.Status_int = 1
			    WHERE
			     uac.IsTestAccount_ti = 0
				AND
			     uac.EmailAddress_vch != ''
			    AND
			     uac.EmailAddress_vch != "0"
			    AND
			     uac.EmailAddress_vch is not null
			</cfquery>

			<cfif getAllSireUser.RECORDCOUNT GT 0>
				<cfloop query="getAllSireUser">

					<cfquery name="getUserPlan" datasource="#Session.DBSourceEBM#">
						SELECT
							PlanId_int
						FROM
							simplebilling.userplans
						WHERE
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getAllSireUser.UserId_int#">
						AND
							Status_int = 1
						ORDER BY
							UserPlanId_bi
						DESC
						LIMIT 1
					</cfquery>

					<cfif getUserPlan.RECORDCOUNT GT 0>
						<cfset getPlanId = getUserPlan.PlanId_int>
					<cfelse>
						<cfset getPlanId = 0>
					</cfif>

					<cfquery name="getPlan" datasource="#Session.DBSourceEBM#">
						SELECT
							PlanName_vch
						FROM
							simplebilling.plans
						WHERE
							PlanId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlanId#">
						AND
							Status_int = 1
					</cfquery>

					<cfif getPlan.RECORDCOUNT GT 0>
						<cfset getPlanName = getPlan.PlanName_vch />
					<cfelse>
						<cfset getPlanName = 'No plan found' />
					</cfif>

					<cfquery name="getLastActiveDate" datasource="#Session.DBSourceEBM#">
						SELECT
							LastUpdated_dt
						FROM
							simpleobjects.batch
						WHERE
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getAllSireUser.UserId_int#">
						ORDER BY
							BatchId_bi
						DESC
						LIMIT 1
					</cfquery>

					<cfquery name="getDeactiveDate" datasource="#Session.DBSourceEBM#">
						SELECT
							Created_dt
						FROM
							simpleobjects.deactivate_logs
						WHERE
							UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getAllSireUser.UserId_int#">
						AND
							Status_int = 1
						LIMIT 1
					</cfquery>

					<cfif getLastActiveDate.RECORDCOUNT GT 0>
						<cfset lastActiveDate = #DateFormat(getLastActiveDate.LastUpdated_dt, "mm/dd/yy")#/>
					<cfelse>
						<cfset lastActiveDate = '' />
					</cfif>

					<cfif getDeactiveDate.RECORDCOUNT GT 0>
						<cfset deactiveDate = #DateFormat(getDeactiveDate.Created_dt, "mm/dd/yy")#/>
					<cfelse>
						<cfset deactiveDate = '' />
					</cfif>

					<cfif getAllSireUser.Active_int EQ 1>
						<cfset getStatus = 'active'/>
					<cfelse>
						<cfset getStatus = 'cancelled'/>
					</cfif>

					<cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
						<cfinvokeargument name="InpEmailString" value="#LCase(getAllSireUser.EmailAddress_vch)#">
						<cfinvokeargument name="InpFirstName" value="#getAllSireUser.FirstName_vch#">
						<cfinvokeargument name="InpLastName" value="#getAllSireUser.LastName_vch#">
						<cfinvokeargument name="InpAccountType" value="#getPlanName#">
						<cfinvokeargument name="InpSignupDate" value="#dateFormat(getAllSireUser.SignUpDate, 'mm/dd/yy')#">
						<cfinvokeargument name="InpLastLoginDate" value="#dateFormat(getAllSireUser.LastLogIn_dt, 'mm/dd/yy')#">
						<cfinvokeargument name="InpLastActiveDate" value="#lastActiveDate#">
						<cfinvokeargument name="InpStatus" value="#getStatus#">
						<cfinvokeargument name="InpCancelDate" value="#deactiveDate#">
						<cfinvokeargument name="InpPhoneNumber" value="#getAllSireUser.MFAContactString_vch#">
					</cfinvoke>

					<!--- Two lines of code below are for debugging, uncomment them if you need to debug --->
					<cfset temp = RetVarmailChimpAPIs.MAILCHIMPFILECONTENT/>
					<cfset ArrayAppend(dataout['list'] ,temp)>
					<cfdump var="#temp#"/>
					<cfoutput><Br/></cfoutput>
					<cfflush>

				</cfloop>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0/>
				<cfset dataout.MESSAGE = 'Get user list failed' />
				<cfreturn dataout />
			</cfif>
			<cfcatch>
                <cfset dataout.type = "#cfcatch.Type#" />
                <cfset dataout.message = "#cfcatch.Message#" />
                <cfset dataout.errMessage = "#cfcatch.detail#" />
            </cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
</cfcomponent>
