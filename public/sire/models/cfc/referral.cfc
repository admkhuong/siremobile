<cfcomponent>
    <cffunction name="GetUserMonthFree" access="public" hint="get user free month" >
        <cfargument name="inpUserId" required="true" type="numeric">

        <cfset var dataout = {} />
        <cfset var getUserMonthFree = ''/>
        <cfset var getUserMonthFreeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.TotalMonthFeeHave = 0/>
        <cfset dataout.TotalMonthFeeUsed = 0/>
        <cfset dataout.TotalMonthFee = 0/>

        <cftry>
            <cfquery name="getUserMonthFree" datasource="#Session.DBSourceEBM#" result="getUserMonthFreeResult">
                SELECT
                    Total_referral_int, Is_referral_int, Month_free_used_referral_used_int
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
            </cfquery>

            <cfif getUserMonthFree.RecordCount GT 0>
                <cfset dataout.TotalMonthFeeHave = getUserMonthFree.Total_referral_int[1] + getUserMonthFree.Is_referral_int[1] >
                <cfset dataout.TotalMonthFeeUsed = getUserMonthFree.Month_free_used_referral_used_int[1] >
                <cfset dataout.TotalMonthFee = dataout.TotalMonthFeeHave - dataout.TotalMonthFeeUsed >
                <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "0" />
                <cfset dataout.Message = "User does not have referral" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    
        </cftry>

        <cfreturn dataout/>
    </cffunction>

        <!--- NEW REFERRAL PROMOTION CREDITS --->
     <cffunction name="GetUserReferralCredits" access="public" hint="get current rewards credits which not added to billing balance" output="false" >
        <cfargument name="inpUserId" required="true" type="numeric">

        <cfset var dataout = {} />
        <cfset var getUserMonthFree = ''/>
        <cfset var getUserMonthFreeResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />
        <cfset dataout.referralCredits = 0/>

        <cftry>
            <cfquery name="getUserMonthFree" datasource="#Session.DBSourceEBM#" result="getUserMonthFreeResult">
                SELECT
                    Total_promotion_credits_int
                FROM 
                    simpleobjects.sire_user_referral
                WHERE
                    UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
            </cfquery>

            <cfif getUserMonthFree.RecordCount GT 0>
                <cfset dataout.referralCredits = getUserMonthFree.Total_promotion_credits_int[1]/>
                 <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Message = "#cfcatch.Message#" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    

        </cftry>    
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="ResetUserReferral" access="public" hint="reset current rewards credits to 0" output="false">
        <cfargument name="inpUserId" required="true" type="numeric">
        <cfargument name="inpIsDowngrade" required="false" type="numeric" default="0">

        <cfset var dataout = {} />
        <cfset var resetUserMonthFree = ''/>
        <cfset var resetUserMonthFreeResult = ''/>
        <cfset var resetUserPromotionCredits = ''/>
        <cfset var resetUserPromotionCreditsResult = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cftry>

            <cfif arguments.inpIsDowngrade EQ 1>
                <cfquery name="resetUserMonthFree" datasource="#Session.DBSourceEBM#" result="resetUserMonthFreeResult">
                    UPDATE
                        simpleobjects.sire_user_referral
                    SET 
                        Total_promotion_credits_received_last_month_int = 0,
                        Total_promotion_credits_int = 0,
                        Total_referral_int = 0    
                    WHERE
                        UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>    

                <cfquery name="resetUserPromotionCredits" datasource="#Session.DBSourceEBM#" result="resetUserPromotionCreditsResult">
                    UPDATE
                         simplebilling.billing
                    SET 
                        PromotionCreditBalance_int = 0
                    WHERE
                        UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>    

            <cfelse>
                <cfquery name="resetUserMonthFree" datasource="#Session.DBSourceEBM#" result="resetUserMonthFreeResult">
                    UPDATE
                        simpleobjects.sire_user_referral
                    SET 
                        Total_promotion_credits_received_last_month_int = Total_promotion_credits_int,
                        Total_promotion_credits_received_int = Total_promotion_credits_received_int + Total_promotion_credits_int,
                        Total_promotion_credits_int = 0,
                        Total_referral_int = 0    
                    WHERE
                        UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>    
            </cfif>
            

            <cfif resetUserMonthFree.RecordCount GT 0>
                <cfset dataout.RXResultCode = "1" />
            <cfelse>
                <cfset dataout.RXResultCode = "-1" />
                <cfset dataout.Message = "#cfcatch.Message#" />
            </cfif>
        <cfcatch>
            <cfset dataout.Type = "#cfcatch.Type#" />
            <cfset dataout.Message = "#cfcatch.Message#" />
            <cfset dataout.ErrMessage = "#cfcatch.detail#" />
        </cfcatch>    

        </cftry>    
        <cfreturn dataout/>
    </cffunction>

    <cffunction name="AddReferralCredits" access="public" hint="add referral credits to user balance" output="false" >
        <cfargument name="inpUserId" required="true" type="numeric" default="0">
        <cfargument name="inpReferralCredits" required="false" type="numeric" default="0">
        <cfargument name="inpOtherCredits" required="false" type="numeric" default="0">

        <cfset var dataout = {} />
        <cfset var updateUserPromotionCredits = ''/>
        <cfset var updateUserPromotionCreditsResult = ''/>
        <cfset var GetUserReferralCreditsResult = ''/>
        <cfset var referralCredits = ''/>

        <!--- SET DEFAULT VALUES RETURN --->
        <cfset dataout.RXResultCode = "-1" />
        <cfset dataout.Type = ""/>
        <cfset dataout.Message = "" />
        <cfset dataout.ErrMessage = "" />

        <cfif arguments.inpUserId GT 0>
            <cftry>

                <cfif !structKeyExists(arguments, "inpReferralCredits") OR arguments.inpReferralCredits EQ 0 >
                    <cfinvoke method="GetUserReferralCredits" returnvariable="GetUserReferralCreditsResult">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>
                     <cfset referralCredits = GetUserReferralCreditsResult.referralCredits>
                <cfelse>
                      <cfset referralCredits = arguments.inpReferralCredits>
                </cfif> 

                <!--- UPDATE NEW REFERRAL CREDITS = REFERRAL CREDIT --->
                <cfquery name="updateUserPromotionCredits" datasource="#Session.DBSourceEBM#" result="updateUserPromotionCreditsResult">
                    UPDATE
                        simplebilling.billing
                    SET 
                        PromotionCreditBalance_int = PromotionCreditBalance_int + <cfqueryparam cfsqlType="CF_SQL_DECIMAL" value="#referralCredits+arguments.inpOtherCredits#">
                    WHERE
                        UserId_int = <cfqueryparam cfsqlType="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>

                <cfif updateUserPromotionCreditsResult.RecordCount GT 0>

                    <!--- RESET USER CREDITS --->
                    <cfinvoke method="ResetUserReferral">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
                    </cfinvoke>

                    <cfset dataout.RXResultCode = "1" />
                <cfelse>
                    <cfset dataout.RXResultCode = "-1" />
                    <cfset dataout.Message = "#cfcatch.Message#" />
                </cfif>
            <cfcatch>
                <cfset dataout.Type = "#cfcatch.Type#" />
                <cfset dataout.Message = "#cfcatch.Message#" />
                <cfset dataout.ErrMessage = "#cfcatch.detail#" />
            </cfcatch>    

            </cftry>    
        <cfelse>
            <cfset dataout.ErrMessage = "UserId invalid." />
        </cfif>

        <cfreturn dataout/>
    </cffunction>


</cfcomponent>