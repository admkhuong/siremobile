<cfcomponent>
	<cfsetting showdebugoutput="false">

    <cfinclude template="../../../paths.cfm">
	<cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/env_paths.cfm">

	<!--- Used IF locking down by session - User has to be logged in --->

	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.DBSourceREAD" default="bishop_read">
	
	<cfset LOCAL_LOCALE = "English (US)">

	<cffunction name="GetRecurringLog" access="remote" output="true"><!--- returnformat="JSON"--->
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
		<cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
		<cfargument name="sSortDir_0" default="desc"><!---this is the direction of sorting, asc or desc --->
		<cfargument name="customFilter" default=""><!---this is the custom filter data --->
		
		
		<cfset var dataout = {}>
		<cfset dataout.DATA = ArrayNew(1)>
		<cfset var GetEMS = 0>
		
		<cfset var order 	= "">
		<cfset var editHtml	= '' />
		<cfset var deleteHtml	= '' />
		<cfset var OptionLinks	= '' />
		<cfset var tempItem	= '' />
		<cfset var filterItem	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var data	= '' />
		<cfset var GetNumbersCount	= '' />
		<cfset var GetTemplate	= '' />
		
		
		<cfset var AccountID = ''/>
		<cfset var Email = ''/>
		<cfset var Plan = ''/>
		<cfset var PlanID = ''/>
		<cfset var PurchasedKeyword = ''/>
		<cfset var CreditCardNumber = ''/>
		<cfset var DueDate = ''/>
		<cfset var PaymentDate = ''/>
		<cfset var Amount = ''/>
		<cfset var ofPaymentFail = ''/>
		<cfset var Reason = ''/>
		<cfset var Sentemail = ''/>
		<cfset var Status = ''/>
		<cfset var reRunBilling = ''>
        <cfset var Active = '' />
        
        <cfset var PaymentResponse =''/>
        <cfset var paymentResposeFilecontent =''/>

        <cfset var linkSendMail = '' />
        <cfset var getUserPlan = '' />
        <cfset var getUserPlan = '' />
        <cfset var queryStruct = ''/>

        <cfset var xmldoc = ''/>
        <cfset var dataarr = []/>
		<cfset var gwText=''>

		<cftry>
	        			
		 	<cfset dataout.RXRESULTCODE = 1 />
		    <cfset dataout.TYPE = "" />
		    <cfset dataout.MESSAGE = "" />
		    <cfset dataout.ERRMESSAGE = "" />
			
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			
			<!---ListEMSData is key of DataTable control--->
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
		
			<!--- Order by --->
			<cfif iSortCol_0 GTE 0>
				<cfswitch expression="#Trim(iSortCol_0)#">
					<cfcase value="4"> 
					<cfset order = "l.PaymentDate_dt" />
					</cfcase> 
					<cfdefaultcase> 
						<cfset order = "l.PaymentDate_dt"/>   
						<cfset arguments.sSortDir_0 = 'DESC'/>	
					</cfdefaultcase> 	
				</cfswitch>
			<cfelse>
				<cfset order = "l.PaymentDate_dt"/>
				<cfset arguments.sSortDir_0 = 'DESC'/>
			</cfif>
			<!---Get total EMS for paginate --->
			<cfquery name="GetNumbersCount" datasource="#Session.DBSourceREAD#">
				SELECT
					COUNT(l.RecurringLogId_bi) 
				AS 
					TOTALCOUNT
				FROM 
					simplebilling.recurringlog l
				INNER JOIN 
					simplebilling.plans p 
				ON 
					l.PlanId_int = p.PlanId_int
	        	INNER JOIN  
	        		simplebilling.userplans up 
	        	ON 
	        		l.UserPlanId_bi = up.UserPlanId_bi
				WHERE 
					1=1
	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.NAME EQ 'l.Status_ti' AND filterItem.VALUE !=-100>

							<cfelse>
								<cfif filterItem.NAME EQ "Recurringfail">
									<cfif filterItem.OPERATOR EQ "=">
										<cfif filterItem.VALUE EQ "ALL">
											AND	(up.NumOfRecurringKeyword_int > 2 OR up.NumOfRecurringPlan_int > 2)
										<cfelseif filterItem.VALUE eq "RECURRING_PLAN">
											AND	up.NumOfRecurringPlan_int > 2
										<cfelse><!---RECURRING_KEYWORD--->
											AND	up.NumOfRecurringKeyword_int > 2
										</cfif>
									<cfelse>
										<cfif filterItem.VALUE EQ "ALL">
											AND	(up.NumOfRecurringKeyword_int < 3 AND up.NumOfRecurringPlan_int < 3)
										<cfelseif filterItem.VALUE eq "RECURRING_PLAN">
											AND	up.NumOfRecurringPlan_int < 3
										<cfelse><!---RECURRING_KEYWORD--->
											AND	up.NumOfRecurringKeyword_int < 3
										</cfif>
									</cfif>
								<cfelse>
									<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
										<cfif filterItem.VALUE NEQ ''>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										</cfif>
									<cfelse>
										<cfif filterItem.VALUE NEQ ''>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
									</cfif>
								</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>
			</cfquery>
			<!--- <cfreturn serializeJSON(GetNumbersCount)> --->
			<cfif GetNumbersCount.TOTALCOUNT GT 0>
				<cfset dataout["iTotalRecords"] = GetNumbersCount.TOTALCOUNT>
				<cfset dataout["iTotalDisplayRecords"] = GetNumbersCount.TOTALCOUNT>
	        </cfif>
	        
	        <!--- Get ems data --->		
			<cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
				SELECT 
        			l.RecurringLogId_bi,
        			l.PaymentDate_dt,
        			l.Status_ti,
        			p.PlanName_vch,
        			up.BuyKeywordNumber_int,
        			l.SentEmail_ti,
        			l.UserPlanId_bi,
        			l.UserId_int,
        			l.PlanId_int,
        			l.FirstName_vch,
        			l.EmailAddress_vch,
        			l.Amount_dec,
        			l.PaymentData_txt,
        			l.PaymentResponse_txt,
        			l.LogFile_vch,
        			l.paymentGateway_ti
	        	FROM 
        			simplebilling.recurringlog l 
				INNER JOIN 
					simplebilling.plans p 
				ON 
					l.PlanId_int = p.PlanId_int
	        	INNER JOIN  
	        		simplebilling.userplans up 
	        	ON 
	        		l.UserPlanId_bi = up.UserPlanId_bi
	        	WHERE 
	        		1=1

	        	<cfif customFilter NEQ "">
					<cfoutput>
						<cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
							<cfif filterItem.NAME EQ 'l.Status_ti' AND filterItem.VALUE !=-100>

							<cfelse>
								<cfif filterItem.NAME EQ "Recurringfail">
									<cfif filterItem.OPERATOR EQ "=">
										<cfif filterItem.VALUE EQ "ALL">
											AND	(up.NumOfRecurringKeyword_int > 2 OR up.NumOfRecurringPlan_int > 2)
										<cfelseif filterItem.VALUE eq "RECURRING_PLAN">
											AND	up.NumOfRecurringPlan_int > 2
										<cfelse><!---RECURRING_KEYWORD--->
											AND	up.NumOfRecurringKeyword_int > 2
										</cfif>
									<cfelse>
										<cfif filterItem.VALUE EQ "ALL">
											AND	(up.NumOfRecurringKeyword_int < 3 AND up.NumOfRecurringPlan_int < 3)
										<cfelseif filterItem.VALUE eq "RECURRING_PLAN">
											AND	up.NumOfRecurringPlan_int < 3
										<cfelse><!---RECURRING_KEYWORD--->
											AND	up.NumOfRecurringKeyword_int < 3
										</cfif>
									</cfif>
								<cfelse>
									<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
										<cfif filterItem.VALUE NEQ ''>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
										</cfif>
									<cfelse>
										<cfif filterItem.VALUE NEQ ''>
											AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
										</cfif>
									</cfif>
								</cfif>
							</cfif>
						</cfloop>
					</cfoutput>
				</cfif>

				ORDER BY 
					   #order# #arguments.sSortDir_0#
				LIMIT <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayStart#">, <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#iDisplayLength#">
	        </cfquery>
		    <cfloop query="GetEMS">	
				
				<cfset editHtml = "" >
				<cfset editHtml &= '<a data-toggle="modal" href="dsp_update_recurring_log?inpbatchid=#GetEMS.RecurringLogId_bi#" data-target="##RecurringLogModal"><img class="EMSIcon"title="Edit Status
				" height="16px" width="16px" src="/public/images/mb/rename.png"></a>' >	
				
				<cfset linkSendMail = '<img class="EMSIcon" title="Sendmail" height="16px" onclick="return sendMail(#GetEMS.RecurringLogId_bi#);" width="16px" src="/public/css/images/icons16x16/launch_email.png">'>
				    
				<cfset deleteHtml = '<img class="EMSIcon" title="Delete" height="16px" onclick="return deleteEMS(#GetEMS.RecurringLogId_bi#);" width="16px" src="/public/images/icons16x16/delete_16x16.png">'>
				                
				<cfset reRunBilling = ''>
					
				
               <cfset Status = "Closed">
                <cfif GetEMS.Status_ti EQ "0">
                	<cfset Status = "Open">
                <cfelseif GetEMS.Status_ti EQ "-1">
                	<cfset Status = "In Progess">
                <cfelseif GetEMS.Status_ti EQ "-2">
                	<cfset Status = "Tech Review">
                <cfelseif GetEMS.Status_ti EQ "-3">
                	<cfset Status = "QA Review">
                </cfif>
                
                <cfset Sentemail = "No">
                <cfif GetEMS.SentEmail_ti EQ 1>
                	<cfset Sentemail = "Yes">
                </cfif>
                
                <cfset PaymentResponse = ''/>
                <cfset ofPaymentFail=''>
                <cfset Reason = ''>
                <cfset CreditCardNumber = ''>
                <cfset paymentResposeFilecontent = ''>
                <cfset reRunBilling = ''>
				<cfset xmldoc = ''> 
				<cfset dataarr = []/>

            	<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
				<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
		            <cfinvokeargument name="InpUserId" value="#GetEMS.UserId_int#">
		        </cfinvoke>

                <!--- IF STATUS = OPEN _> RERUN BILLING--->
		      <!---   <cfif GetEMS.Status_ti EQ 0>
		        	<cfset reRunBilling = '<a onclick="return ReRunBilling(#GetEMS.RecurringLogId_bi#);"> Re-run Billing</a>' >
		        </cfif> --->

                <cfif isJson(GetEMS.PaymentResponse_txt)>
	                <cfset PaymentResponse = deserializeJSON(GetEMS.PaymentResponse_txt)/>	                
	                <cfif GetEMS.paymentGateway_ti EQ 1>
						<cfset gwText='WorldPay'>
	                	<cfset paymentResposeFilecontent = deserializeJSON(PaymentResponse.filecontent)/>
						<cftry>
							<cfif isdefined(paymentResposeFilecontent.success)>
								<cfif isdefined(paymentResposeFilecontent.result)>
									<cfset ofPaymentFail = paymentResposeFilecontent.result/>
								<cfelse>
									<cfset ofPaymentFail = ''/>
								</cfif>
								<cfif isdefined(paymentResposeFilecontent.message)>
									<cfset Reason = paymentResposeFilecontent.message/>
								<cfelse>
									<cfset Reason = ''/>
								</cfif>
								<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
							</cfif>
							<cfif NOT isNull(paymentResposeFilecontent.transaction.cardNumber)>
								<cfset CreditCardNumber = paymentResposeFilecontent.transaction.cardNumber/>
							</cfif>
							<cfcatch>
								<cfset ofPaymentFail = ''/>
								<cfset Reason = paymentResposeFilecontent.message/>
								<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
							</cfcatch>
						</cftry>	
					<cfelseif GetEMS.paymentGateway_ti EQ 2>	
						<cfset gwText='Paypal'>
						<cfif structKeyExists(PaymentResponse, "RESPMSG")>
							<cfset ofPaymentFail = PaymentResponse.RESPMSG/>
							<cfset Reason = PaymentResponse.RESPMSG/>
						<cfelseif PaymentResponse.FILECONTENT NEQ "" AND structKeyExists( deserializeJSON(PaymentResponse.FILECONTENT), "RESPMSG") AND ! isNull(deserializeJSON(PaymentResponse.FILECONTENT))>
							<cfset ofPaymentFail = deserializeJSON(PaymentResponse.FILECONTENT).RESPMSG/>
							<cfset Reason = deserializeJSON(PaymentResponse.FILECONTENT).RESPMSG/>
						<cfelse>						
							<cfset ofPaymentFail = ''/>
							<cfset Reason = ''/>
						</cfif>
						<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
						<cfset CreditCardNumber = ""/>
					<cfelseif GetEMS.paymentGateway_ti EQ 3>
						<cfset gwText='Mojo'>
						<cfif PaymentResponse.filecontent neq "" AND isXML(PaymentResponse.filecontent)>
							<cfset xmldoc = XMLParse(PaymentResponse.filecontent)>
							<cfset dataarr = xmlsearch(xmldoc,"/UAPIResponse/processorResponse/")>
							<cfif arrayLen(dataarr) GT 0>
								<cfset ofPaymentFail = dataarr[1].XmlText>
								<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
							</cfif>
							
						<cfelse>
							<cfset ofPaymentFail = "Fail">
							<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
						</cfif>
					<cfelseif GetEMS.paymentGateway_ti EQ 4>
						<cfset gwText='TotalApps'>
						<cftry>
							<cfif NOT isNull(PaymentResponse.FILECONTENT)>
								<cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
									<cfinvokeargument name="QueryString" value="#PaymentResponse.FILECONTENT#">
								</cfinvoke>

								<cfset ofPaymentFail = queryStruct.responsetext/>
								<cfset Reason = queryStruct.responsetext/>
							<cfelse>
								<cfset ofPaymentFail = ''/>
								<cfset Reason = ''/>
							</cfif>
							<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
							<cfset CreditCardNumber = ''/>
							<cfcatch>
								<cfset ofPaymentFail = #cfcatch.MESSAGE#/>
								<cfset Reason = ''/>
								<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
							</cfcatch>
						</cftry>
	                </cfif>
	                
                <cfelse>
                	<cfset ofPaymentFail = GetEMS.PaymentData_txt/>
                	<cfset Reason = GetEMS.PaymentResponse_txt/>
                	<cfset Sentemail = '#Sentemail# #linkSendMail#'/>
                </cfif>
                <cfset OptionLinks = '#deleteHtml#' />
                <!--- IS EMS--->    
               <cfset Status = '#Status# #editHtml#'/>
 				
 				<cfset AccountID = GetEMS.UserId_int/>
 				<cfset Email = GetEMS.EmailAddress_vch/>

 				<cfset Plan = GetEMS.PlanName_vch/>
 				<cfset PlanID = GetEMS.PlanId_int/>
 				<cfset PurchasedKeyword = GetEMS.BuyKeywordNumber_int/>
 				
 				<cfset DueDate = GetEMS.PaymentDate_dt/>
 				<cfset PaymentDate =  DateTimeFormat(GetEMS.PaymentDate_dt,'yyyy-mm-dd HH:nn:ss') />
 				
				<cfset Amount= '$' & numberFormat(replace(GetEMS.Amount_dec, "$", "","all"),'.99')>
<!--- 				<cfdump var = "#OptionLinks#" abort ="true"/> --->
				<cfset tempItem = {
					ID = '#GetEMS.RecurringLogId_bi#',
					ACCOUNT_ID = "#AccountID#",
					EMAIL = "#Email#",
					PLAN = "#Plan#",
					PLAN_ID = "#PlanID#",
					PURCHASED_KEYWORD = "#PurchasedKeyword#",
					CREDIT_CARD_NUMBER = "#CreditCardNumber#",
					DUE_DATE = "#DueDate#",
					PAYMENT_DATE = "#PaymentDate#",
					AMOUNT = "#Amount#",
					OF_PAYMENT_FAIL = "#ofPaymentFail#",
					REASON = "#Reason#",
					SENT_EMAIL = "#Sentemail#",
					STATUS = "#Status#",
					RE_RUN_BILLING = "#reRunBilling#",
					OPTION_LINKS = "#OptionLinks#",
					GW= gwText
				}>		
				<cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
		    </cfloop>
        <cfcatch type="Any" >
			<cfset dataout.RXRESULTCODE = -1 />
		    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
		    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
		    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			<cfset dataout["iTotalRecords"] = 0>
			<cfset dataout["iTotalDisplayRecords"] = 0>
			<cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
	</cffunction>
	
	<cffunction name="DeleteRecurringLogItem" access="remote" output="false" hint="DeleteRecurringLogItem">
        <cfargument name="TID" required="no" default="0">

		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />  
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
	
			
			<cftry>
   
                    <cfquery name="DeleteTID" datasource="#Session.DBSourceEBM#">
                        DELETE
						FROM   `simplebilling`.recurringlog
                    
                        WHERE                
							RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TID#">                          
                    </cfquery>  
                    <cfset dataout.RXRESULTCODE = 1/>
                			 	
                <cfset dataout.TID = TID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
				
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
     <cffunction name="UpdateRecurringLog" access="remote" output="false" hint="Save UpdateRecurringLog">
        <cfargument name="INPBATCHID" required="yes" default="0">
        <cfargument name="INPSTATUS" required="yes" default="">
        

        <cfset var UpdateLog = '' />    
        <!--- 
        Positive is success
        1 = OK
        2 = 
        3 = 
        4 = 
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Unique Name
         --->

            
        <cfoutput>
                  
            <!--- Set default to error in case later processing goes bad --->
            <cfset var dataout = {} />        
            <cfset dataout.RXRESULTCODE = -1>
            <cfset dataout.MESSAGE = ''>
            <cfset dataout.ERRMESSAGE = ''>
            <cfset var InsertTemplate = ''>
            <cfset var UpdateTemplate = ''>
       
            <cftry>
            
                                    
                <!--- Validate session still in play - handle gracefully if not --->
                <cfif Session.USERID GT 0>
                   
                   
                    
                    <!--- UPDATE --->
                    <cfquery name="UpdateLog" datasource="#Session.DBSourceEBM#">
                       
                        UPDATE simplebilling.recurringlog 
                        SET 
                        	`Status_ti` = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPSTATUS#">
                        WHERE `RecurringLogId_bi`=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">;

                    </cfquery>  

                      
                
                    <cfset dataout.RXRESULTCODE = 1>
                        
                  <cfelse>
                    <cfset dataout.RXRESULTCODE = -2>
                    <cfset dataout.MESSAGE = 'Session Expired! Refresh page after logging back in.'>
                    <cfset dataout.ERRMESSAGE = 'Session Expired! Refresh page after logging back in.'>
                  </cfif>          
                           
            <cfcatch TYPE="any">
                
                <cfif cfcatch.errorcode EQ "">
                    <cfset cfcatch.errorcode = -1>
                </cfif>

                <cfset dataout.MESSAGE = '#cfcatch.MESSAGE#'>
                <cfset dataout.ERRMESSAGE = '#cfcatch.detail#'>
                            
            </cfcatch>
            
            </cftry>     
        

        </cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	<cffunction name="SendMailRecurringLogItem" access="remote" output="false" hint="SendMailRecurringLogItem">
        <cfargument name="TID" required="no" default="0">
        


		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />

		<cfset var sire_root_url  = '' />
		<cfset var tk = '' />
		<cfset var dataMail = '' />
		<cfset var getLog = '' />

       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
			<cfset var UpdateLog={}>
			
			<cftry>
   					<cfquery name="getLog" datasource="#Session.DBSourceREAD#">
					SELECT 
						l.RecurringLogId_bi,
	        			l.PaymentDate_dt,
	        			l.Status_ti,
	        			l.SentEmail_ti,
	        			l.UserPlanId_bi,
	        			l.UserId_int,
	        			l.PlanId_int,
	        			l.FirstName_vch,
	        			l.EmailAddress_vch,
	        			l.Amount_dec,
	        			l.PaymentData_txt,
	        			l.PaymentResponse_txt,
	        			l.LogFile_vch,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int,
	        			up.UserId_int,
	        			up.StartDate_dt,
	        			up.EndDate_dt,
	        			up.PriceKeywordAfter_dec,
	        			up.BuyKeywordNumber_int
	        			
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE l.RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TID#"> 
					</cfquery>
					
					<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
							<cfset sire_root_url = 'https://siremobile.com'>	
					<cfelse>
						<cfset sire_root_url = rootUrl>	
					</cfif>
                    
                    <cfset tk = ToBase64(DateFormat(getLog.EndDate_dt, 'yyyy-mm-dd-') & getLog.UserId_int)>
                	<cfset dataMail = {
							FirstName_vch			= getLog.FirstName_vch, 
							EmailAddress_vch		= getLog.EmailAddress_vch, 
							PlanId_int				= getLog.PlanId_int,
							PlanName_vch			= getLog.PlanName_vch,
							PaymentDate_dt			= getLog.PaymentDate_dt,
							Amount_dec				= getLog.Amount_dec,
							BuyKeywordNumber_int	= getLog.BuyKeywordNumber_int,
							StartDate_dt			= getLog.StartDate_dt,
							EndDate_dt				= getLog.EndDate_dt,
							Amount_dec				= getLog.Amount_dec,
							BuyKeywordNumber_int	= getLog.BuyKeywordNumber_int,
							PriceKeywordAfter_dec	= getLog.PriceKeywordAfter_dec,
							EXTENDLINK				= sire_root_url & '/public/sire/pages/extend-plan?tk=' & tk,
							ROOTURL					= rootUrl
						}>
						
	                    <cfinvoke component="public.sire.models.cfc.sendmail" method="sendmailtemplate">
				            <cfinvokeargument name="to" value="#dataMail.EmailAddress_vch#">
				            <cfinvokeargument name="type" value="html">
				            <cfinvokeargument name="subject" value="Sire can not do payment monthly fee (Payment date:  #DateFormat(dataMail.PaymentDate_dt, 'mm/dd/yyyy')#)">
				            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_log_recurring_sendmail.cfm">
				            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
			    		</cfinvoke>
					
					<cfquery name="UpdateLog" datasource="#Session.DBSourceEBM#">
                       
                        UPDATE simplebilling.recurringlog 
                        SET 
                        	SentEmail_ti = 1
                        WHERE RecurringLogId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.TID#">;

                    </cfquery>  
                
                
                <cfset dataout.RXRESULTCODE = 1/>
                			 	
                <cfset dataout.TID = TID/>  
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = Message />       
                <cfset dataout.ERRMESSAGE = "" />     
				
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    <cffunction name="ReRunBilling" access="remote" output="false" hint="ReRunBilling">
        <cfargument name="TID" required="no" default="0">

        


		<cfset var dataout = {} />
		<cfset var DeleteTID	= '' />  

		<cfset var closeBatchesData = '' />
       	<cfset var _paymentType = '' />
       	<cfset var _amount = '' />
       	<cfset var paymentData = '' />
       	<cfset var paymentResponseContent = '' />
       	<cfset var _endDate = '' />
       	<cfset var creditsPurchased = '' />
       	<cfset var getLog = '' />
       	<cfset var userPlansQuery = '' />
       	<cfset var plansQuery = '' />
       	<cfset var customersQuery = '' />
       	<cfset var updateUsers = '' />
       	<cfset var updateUserPlan = '' />
       	<cfset var insertUsersPlan = '' />
       	<cfset var getUserPlan = '' />
       	<cfset var paymentResponse = '' />
       	<cfset var closeBatchesRequestResult = '' />
       	<cfset var planadd = '' />

       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TID = TID/>  
            <cfset dataout.TYPE = "" />
			<cfset dataout.MESSAGE = ""/>                
            <cfset dataout.ERRMESSAGE = "" />  
       
            <cfset var isDeleteCampaign = false>
            <cfset var Message = "">
			
			
			<cftry>
   					<cfquery name="getLog" datasource="#Session.DBSourceREAD#">
					SELECT 
	        			l.PlanId_int,
	        			l.UserPlanId_bi,
	        			l.RecurringLogId_bi,
	        			p.PlanName_vch,
	        			up.BuyKeywordNumber_int
	        		FROM `simplebilling`.recurringlog l 
	        				INNER JOIN `simplebilling`.plans p ON l.PlanId_int = p.PlanId_int
	        				INNER JOIN  `simplebilling`.userplans up ON l.UserPlanId_bi = up.UserPlanId_bi
	        		WHERE l.RecurringLogId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TID#"> 
					</cfquery>
                    
                	<cfif getLog.RecordCount GT 0>
	                	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
							SELECT
								UserPlanId_bi, 
								UserId_int,
								PlanId_int,
								Amount_dec,
								UserAccountNumber_int,
								KeywordsLimitNumber_int,
								FirstSMSIncluded_int,
								PriceMsgAfter_dec,
								PriceKeywordAfter_dec,
								BuyKeywordNumber_int,
								EndDate_dt
							FROM 
								simplebilling.userplans
							WHERE 
								Status_int = 1 AND DATE(EndDate_dt) <= CURDATE() AND UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.UserPlanId_bi#"> 
							ORDER BY 
								EndDate_dt DESC, UserPlanId_bi DESC
						</cfquery>
						
							<cfif userPlansQuery.RecordCount GT 0>
								
								<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
									SELECT 
										Amount_dec,
										PriceKeywordAfter_dec 
									FROM 
										simplebilling.plans
									WHERE 
										Status_int = 1
									AND 
										PlanId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.PlanId_int#"> 
								</cfquery>
								
								<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
									SELECT
										PaymentMethodID_vch,
										PaymentType_vch,
										UserId_int 
									FROM 
										simplebilling.authorize_user
									WHERE 
										UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getLog.UserId_int#"> 
								</cfquery>

								<cfset closeBatchesData = {
									developerApplication = {
										developerId: worldpayApplication.developerId,
										version: worldpayApplication.version
									}
								}>
								
								
								<cfset _paymentType = customersQuery.PaymentType_vch>
								<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
								
								<cfset _amount = plansQuery.Amount_dec + (userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec)>

								<cfset paymentData = {  
								   amount = _amount,
								   paymentVaultToken = {  
								      customerId = customersQuery.UserId_int,
								      paymentMethodId = customersQuery.PaymentMethodID_vch,
								      paymentType = _paymentType
								   },
								   developerApplication = {  
								      developerId = worldpayApplication.developerId,
								      version = worldpayApplication.version
								   }
								}>

								<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
								<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
						            <cfinvokeargument name="InpUserId" value="#getLog.UserId_int#">
						        </cfinvoke>

								<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
										<cfhttpparam type="header" name="content-type" value="application/json">
										<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
										<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
								</cfhttp>

								<cfif paymentResponse.status_code EQ 200>
								<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
									<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
									<cfset paymentResponseContent.numberOfCredit = userPlansQuery.FirstSMSIncluded_int/>
									<cfif paymentResponseContent.success>
										<!--- close session--->
								        <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
								        <cfhttpparam type="header" name="content-type" value="application/json">
								        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
								        <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
								        </cfhttp>
								        
								        <cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt)>

                                        <!---
								        <cfif plansQuery.PlanId_int EQ 1>
								        	<cfset creditsPurchased = 0/>
								        <cfelse>
								        	<cfset creditsPurchased =  plansQuery.FirstSMSIncluded_int/>
								        </cfif>
                                        --->
								       
                                        <cfset creditsPurchased =  userPlansQuery.FirstSMSIncluded_int/>

								        <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
							              UPDATE  
							                simplebilling.billing
							              SET   
							                Balance_int = #creditsPurchased#
							              WHERE 
							                userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#getLog.UserId_int#">
							         	 </cfquery>
								        
								        <cfquery name="updateUserPlan" datasource="#Session.DBSourceEBM#" result="updateUserPlan">
								        	UPDATE simplebilling.userplans SET Status_int = 0 WHERE  UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userPlansQuery.UserPlanId_bi#"> 
								        </cfquery>
								        <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
								            INSERT INTO simplebilling.userplans
								              (
								                Status_int,
								                UserId_int,
								                PlanId_int,
								                Amount_dec,
								                UserAccountNumber_int,
								                KeywordsLimitNumber_int,
								                FirstSMSIncluded_int,
								                PriceMsgAfter_dec,
								                PriceKeywordAfter_dec,
								                BuyKeywordNumber_int,
								                StartDate_dt,
								                EndDate_dt
								              )
								              VALUES 
								              (
								                1,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.UserId_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PlanId_int#">,
								                <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#userPlansQuery.Amount_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.UserAccountNumber_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.KeywordsLimitNumber_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.FirstSMSIncluded_int#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PriceMsgAfter_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PriceKeywordAfter_dec#">,
								                <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.BuyKeywordNumber_int#">,
								                <cfqueryparam cfsqltype="CF_SQL_DATE" value="#userPlansQuery.EndDate_dt#">,
								                <cfqueryparam cfsqltype="CF_SQL_DATE" value="#_endDate#">
								              )
								        </cfquery>
								        <!--- Sendmail payment --->
											<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
									            <cfinvokeargument name="to" value="#paymentResponseContent.transaction.email#">
									            <cfinvokeargument name="type" value="html">
									            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
									            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed.cfm">
									            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
									        </cfinvoke>
							        
							        <!--- Add payment --->
							        
							        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
							                <cfinvokeargument name="planId" value="#getLog.PlanId_int#">
							                <cfinvokeargument name="numberKeyword" value="">
							                <cfinvokeargument name="numberSMS" value="">
							                <cfinvokeargument name="moduleName" value="Re-run Billing">
							                <cfinvokeargument name="paymentRespose" value="#paymentResponse.filecontent#">
							            </cfinvoke>
								        <cfset dataout.MESSAGE = "Re-run billing success." /> 
								    
								    <cfelse>
								        <cfset dataout.RXRESULTCODE = -1/>
                			 	
						                <cfset dataout.TID = TID/>  
						                <cfset dataout.TYPE = "" />
						                <cfset dataout.MESSAGE = "Re-run billing not success." />       
						                <cfset dataout.ERRMESSAGE = paymentResponseContent.message />   
									</cfif>
									
								</cfif>
								
								<cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
				                    <cfinvokeargument name="moduleName" value="Re-run Billing">
				                    <cfinvokeargument name="status_code" value="#paymentResponse.status_code#">
				                    <cfinvokeargument name="status_text" value="#paymentResponse.status_text#">
				                    <cfinvokeargument name="errordetail" value="#paymentResponse.errordetail#">
				                    <cfinvokeargument name="filecontent" value="#paymentResponse.filecontent#">
				                    <cfinvokeargument name="paymentdata" value="#paymentData#">
				    			</cfinvoke>
				    			
				    			<cfelse>
				    					<cfset dataout.RXRESULTCODE = -1/>
                			 	
						                <cfset dataout.TID = TID/>  
						                <cfset dataout.TYPE = "" />
						                <cfset dataout.MESSAGE = "Re-run billing not success" />       
						                <cfset dataout.ERRMESSAGE = "" />  
								</cfif>
	
								    
							<cfelse>
								<cfset dataout.MESSAGE = "This account already paid payment" />
							</cfif>
					<cfelse>
							<cfset dataout.ERRMESSAGE = "No exist log" />  
					</cfif>
                	
                
                        
				<cfcatch TYPE="any">
					<cfset dataout =  {}>  
					<cfset dataout.RXRESULTCODE = -1 />
					<cfset dataout.TID = TID />  
					<cfset dataout.TYPE = cfcatch.TYPE />
					<cfset dataout.MESSAGE = cfcatch.MESSAGE />                
					<cfset dataout.ERRMESSAGE = cfcatch.detail />  
					
				</cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
</cfcomponent>	