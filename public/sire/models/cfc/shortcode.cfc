<cfcomponent>
	<cfinclude template="../../../paths.cfm">
	<cfinclude template="/public/sire/configs/paths.cfm">
	<cfinclude template="/public/sire/configs/env_paths.cfm">
	<cfinclude template="/public/sire/configs/userConstants.cfm">
	<cfinclude template="/session/sire/configs/credits.cfm" >

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.USERID" default="0"/>
	<cfparam name="Session.loggedIn" default="0"/>
	<cfparam name="Session.DBSourceEBM" default="Bishop"/>
	<cfparam name="Session.DBSourceREAD" default="Bishop_read"/>
	<cfparam name="Session.userRole" default="User"/>
	<cfparam name="variables.isReferral" default="no"/>

	<cffunction name="GetSystemDefaultShortCode" access="remote" output="false" hint="Get default shortcode of system">
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.SHORTCODE = '' />

		<cfset var getDefaultShortCode = ''/>

		<cftry>
			<cfquery name="getDefaultShortCode" datasource="#session.DBSourceREAD#">
				SELECT
					ShortCode_vch
				FROM
					sms.shortcode
				WHERE
					IsDefault_ti = 1
				LIMIT 1
			</cfquery>

			<cfif getDefaultShortCode.RECORDCOUNT GT 0>
				<cfset dataout.SHORTCODE = getDefaultShortCode.ShortCode_vch/>
			<cfelse>
				<cfquery name="getDefaultShortCode" datasource="#session.DBSourceREAD#">
					SELECT
						ShortCode_vch
					FROM
						sms.shortcode
					WHERE
						ShortCodeId_int = #_DEFAULT_SHORTCODE#
					LIMIT 1
				</cfquery>

				<cfif getDefaultShortCode.RECORDCOUNT GT 0>
					<cfset dataout.SHORTCODE = getDefaultShortCode.ShortCode_vch/>
				<cfelse>
					<cfset dataout.SHORTCODE = "NA"/>
				</cfif>
			</cfif>

			<cfset dataout.RXRESULTCODE = 1/>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetShortCodeById" access="public" output="false" hint="Get shortcode">
		<cfargument name="inpShortCodeId" required="true" hint="Shortcode id"/>
	
		<cfset var dataout = {}/>
		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />
		<cfset dataout.TYPE = "" />
		<cfset dataout.SHORTCODE = '' />
		
		<cfset var getShortCode = '' />

		<cftry>
			<cfquery name="getShortCode" datasource="#session.DBSourceREAD#">
				SELECT
					ShortCode_vch
				FROM
					sms.shortcode
				WHERE
					ShortCodeId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortCodeId#"/>
				LIMIT
					1
			</cfquery>

			<cfif getShortCode.RECORDCOUNT GT 0>
				<cfset dataout.SHORTCODE = getShortCode.ShortCode_vch/>
				<cfset dataout.RXRESULTCODE = 1/>
			</cfif>
	
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
				<cfset dataout.TYPE = cfcatch.TYPE />
			</cfcatch>
		</cftry>
	
		<cfreturn dataout/>
	</cffunction>
</cfcomponent>