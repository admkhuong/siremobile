<cfcomponent hint="SMS Chat Feature" output="false">
	<cfinclude template="/public/sire/configs/paths.cfm"/>

	<cffunction name="callCPAPI" access="remote" returnformat="plain" returntype="string" output="true" hint="Call from cp api">
		<cffile action="write" output="#SerializeJSON(getHttpRequestData().content)#" file="#getDirectoryFromPath(getCurrentTemplatePath())#/zzz.txt" />

		<cfset var requestBody = {} />
		<cfset var requestBodyString = getHttpRequestData().content />
		<cfset var getSessionByContactString = '' />
		<cfset var currHour = '' />
		<cfset var RetVarSendSMS = '' />
		<cfset var shortCode = '' />
		<cfset var SendSingleMT = '' />

		<cfif isJSON(requestBodyString)>
			<cfset requestBody = deserializeJSON(requestBodyString) />
		</cfif>

		<cfdump var="#requestBody#">

		<!---  AND structKeyExists(requestBody, 'OVERTIMESTRING') --->
		<cfif structKeyExists(requestBody, 'CONTACTSTRING') AND structKeyExists(requestBody, 'BATCHID') AND isNumeric(requestBody.BATCHID)>
			<cfquery name="getSessionByContactString" datasource="#session.DBSourceEBM#">
				SELECT
					SessionId_bi,
					SessionState_int,
					CSC_vch,
					ContactString_vch,
					LastCP_int,
					XMLResultString_vch,
					BatchId_bi,
					UserId_int,
					Created_dt,
					LastUpdated_dt
				FROM
					`simplequeue`.`sessionire`
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#requestBody.BATCHID#"> AND
					SessionState_int = 1 AND
					ContactString_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#requestBody.CONTACTSTRING#">
				ORDER BY
					LastUpdated_dt DESC, 
					Created_dt DESC
				LIMIT 
					1
			</cfquery>
			<cfif getSessionByContactString.RecordCount GT 0>
				<!--- 
					After hours notice? What if (MCUD) sends a chat at 3AM? Do we find some way to auto respond no one is around right now but will get back to you when they are? This might be a good place to make API calls as part of flow. Based on API results behave differently. Maybe use a new API call to check if request is during business hours. Slide this into early part of flow?
				 --->
				<cfset currHour = Hour(Now()) />
				<cfif currHour LT 8 AND currHour GT 16>										 
					<!--- remove old method: 
					<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
					--->
                    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
						<cfinvokeargument name="inpBatchId" value="#getSessionByContactString.BatchId_bi#">
                    </cfinvoke>
					<cfparam name="requestBody.OVERTIMESTRING" default="We're sorry, we're currently not available to answer your question. We will reply to you as soon as possible."/>
					<cfinvoke component="session.cfc.csc.csc" method="SendSingleMT" returnvariable="SendSingleMT">
						<cfinvokeargument name="inpContactString" value="#getSessionByContactString.ContactString_vch#"/>
						<cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#"/>
						<cfinvokeargument name="inpIRESessionId" value="#getSessionByContactString.SessionId_bi#"/>
						<cfinvokeargument name="inpBatchId" value="#getSessionByContactString.BatchId_bi#"/>
						<cfinvokeargument name="inpTextToSend" value="#requestBody.OVERTIMESTRING#"/>
					</cfinvoke>
					<!--- <cfinvoke component="session.sire.models.cfc.smschat" method="SendSMS" returnvariable="RetVarSendSMS">
						<cfinvokeargument name="inpSessionId" value="#getSessionByContactString.SessionId_bi#"/>
						<cfinvokeargument name="inpTextToSend" value=""/>
					</cfinvoke> --->
				</cfif>	
			</cfif>	
		</cfif>

		<cfreturn '' />
	</cffunction>

	<cffunction name="CheckSMSChatSession" access="public" hint="Detect chat session message and broadcast">
		<cfargument name="inpIRESessionId" required="true" type="string"/>
		<cfargument name="inpKeyword" required="true" type="string" hint="MO input or keyword"/>
		<cfargument name="inpContactString" required="true" type="string" hint="Contact string"/>
		<cfargument name="inpIREType" required="false" default="2" hint="IRE type, 1-SIRE User, 2-Customer"/>

		<cfset var CheckSMSChatSession = ''/>
		<cfset var channels = [] />
		<cfset var session_channel = '' />
		<cfset var user_channel = '' />
		<cfset var InsertToErrorLog = '' />
		<cfset var reg = '' />

		<cftry>
            <cfquery name="CheckSMSChatSession" datasource="#Session.DBSourceEBM#">
				SELECT
					b.UserId_int
				FROM
					simpleobjects.batch b
				INNER JOIN
					simplequeue.sessionire s
				ON
					b.BatchId_bi = s.BatchId_bi
				WHERE
					b.EMS_Flag_int = 20
				<cfif lCase(arguments.inpKeyword) NEQ "exit">
					AND
						s.SessionState_int = 1
				</cfif>
				AND
					s.SessionId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpIRESessionId#"/>
            </cfquery>

            <cfif CheckSMSChatSession.RECORDCOUNT GT 0>
				<cfinvoke component="public.sire.models.cfc.smschat" method="PushWS" returnvariable="reg">
					<cfinvokeargument name="inpContactString" value="#arguments.inpContactString#"/>
					<cfinvokeargument name="inpSessionId" value="#arguments.inpIRESessionId#"/>
					<cfinvokeargument name="inpUserId" value="#CheckSMSChatSession.UserId_int#"/>
					<cfinvokeargument name="inpType" value="#arguments.inpIREType#"/>
					<cfinvokeargument name="inpTextToSend" value="#arguments.inpKeyword#"/>
				</cfinvoke>
            </cfif>

            <cfcatch type="any">
				<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			        INSERT INTO simplequeue.errorlogs
			        (
			        	ErrorNumber_int,
			            Created_dt,
			            Subject_vch,
			            Message_vch,
			            TroubleShootingTips_vch,
			            CatchType_vch,
			            CatchMessage_vch,
			            CatchDetail_vch,
			            Host_vch,
			            Referer_vch,
			            UserAgent_vch,
			            Path_vch,
			            QueryString_vch
			        )
			        VALUES
			        (
			        	201,
			            NOW(),
			            'CheckSMSChatSession - See Catch Detail for more info',   
			            '',   
			            '',     
			            '', 
			            '',
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#cfcatch.MESSAGE# #cfcatch.DETAIL#">,
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
			            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
			        );
				</cfquery>
            </cfcatch>
		</cftry>
	</cffunction>

	<cffunction name="PushWS" output="false" access="public" hint="Push to web socket">
		<cfargument name="inpTextToSend" required="true" default="" />
		<cfargument name="inpContactString" required="true" default=""/>
		<cfargument name="inpType" required="true" default="1"/>
		<cfargument name="inpSessionId" required="true" default="0"/>
		<cfargument name="inpUserId" required="false" default="#session.UserId#"/>

		<cfset var dataout = {} />
		<cfset var reg = '' />
		<cfset var channels = []/>

		<cfset dataout.RXRESULTCODE = -1/>
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />
		<cfset dataout.TYPE = '' />

		<cftry>
			<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_SESSION_" & "#arguments.inpSessionId#"), 6))/>
			<cfset arrayAppend(channels, left(hash("#EnvPrefix#"&"SIRE_CHAT_USER_" & "#arguments.inpUserId#"), 6))/>
			<cfhttp url="https://ws.siremobile.com:8443/send" result="reg" method="post">
			    <cfhttpparam name="msg" type="formfield" value="#arguments.inpTextToSend#"/>
			    <cfhttpparam name="phone" type="formfield" value="#arguments.inpContactString#"/>
			    <cfhttpparam name="channel" type="formfield" value="#serializeJSON(channels)#"/>
			    <cfhttpparam name="type" type="formfield" value="#arguments.inpType#"/>
			    <cfhttpparam name="userid" type="formfield" value="#arguments.inpUserId#"/>
			    <cfhttpparam name="sessionid" type="formfield" value="#arguments.inpSessionId#"/>
			    <cfhttpparam name="host" type="formfield" value="#CGI.SERVER_NAME#"/>
			</cfhttp>
			<cfif req.status_code EQ "200">
				<cfset dataout.RXRESULTCODE = 1/>
			<cfelse>
				<cfset dataout.ERRMESSAGE = req.errordetail/>
			</cfif>
			<cfcatch type="any">
				<cfset dataout.MESSAGE = cfcatch.MESSAGE />
				<cfset dataout.ERRMESSAGE = cfcatch.DETAIL/>
				<cfset dataout.TYPE = cfcatch.TYPE/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>
</cfcomponent>