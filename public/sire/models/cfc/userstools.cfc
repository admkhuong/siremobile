<cfcomponent>
    <cfsetting showdebugoutput="false">
    <!---
    <cfinclude template="../ScriptsExtend.cfm">

    --->
    <cfinclude template="/public/sire/configs/paths_legacy.cfm">
    <cfinclude template="/public/sire/configs/paths.cfm">
    <cfinclude template="/public/sire/configs/env_paths.cfm">
    <cfinclude template="/public/sire/configs/userConstants.cfm">
    <cfinclude template="/session/sire/configs/credits.cfm" >

    <!--- Used IF locking down by session - User has to be logged in --->
    <cfparam name="Session.USERID" default="0"/>
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/>
    <cfparam name="Session.userRole" default="User"/>
    <cfparam name="variables.isReferral" default="no"/>

    <cfset LOCAL_LOCALE = "English (US)">

    <!--- Careful! This is publicly accessible via javscript and AJAX - Default to low permissions and standard credit billing type --->
    <cffunction name="RegisterNewAccount" access="remote" output="true" hint="Start a new user account">

        <cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
        <cfargument name="INSMSNUMBER" required="yes" default="" type="string">
        <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
        <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
        <cfargument name="INPFNAME" required="yes" default="" type="string">
        <cfargument name="INPLNAME" required="yes" default="" type="string">
        <cfargument name="INPORGNAME" required="no" default="" type="string">
        <cfargument name="INPCBPID" required="no" default="1" type="string">
        <cfargument name="INPSHAREDRESOURCES" required="no" default="0" type="string">
        <cfargument name="INPCOMPANYID" required="no" default="0" type="string">
        <cfargument name="inpReferralCode" required="no" default="" type="string">
        <cfargument name="inpReferralType" required="no" default="" type="string">
        <cfargument name="RegisteredByAgency" required="no" default="0" type="string">

        <cfargument name="inpPromotionCode" required="no" default="" type="string">

        <cfset var dataout = {} />
        <cfset dataout.USERID = '-1'>

        <cfset var sameIpLimit = 25 />
        <cfset var hoursLimit = 2 />
        <cfset var companyUserId = 1 />
        <cfset var NewUserPermissions = ArrayToList(BasicPermission, ",") />
        <cfset var getCurrentUser = StructNew() />
        <cfset var EBMAdminEMSList = "" />
        <cfset var GetEMSAdmins = ""/>
        <cfset var RetVarsetPermission = "" />
        <cfset var VerifyUnique = '' />
        <cfset var checkLimit   = '' />
        <cfset var AddUser  = '' />
        <cfset var AddOrginazation  = '' />
        <cfset var AddBilling   = '' />
        <cfset var addRole  = '' />
        <cfset var safeeMail    = '' />
        <cfset var safePass = '' />
        <cfset var datadump = '' />
        <cfset var checkAddPlan = '' />
        <cfset var RetVarAssignSireSharedCSC    = '' />
        <cfset var userLogin    = '' />
        <cfset var RetVarAddNewCustomFiled1 = '' />
        <cfset var RetVarAddNewCustomFiled2 = '' />
        <cfset var RetVarAddNewCustomFiled3 = '' />
        <cfset var RetVarAddNewCustomFiled4 = '' />
        <cfset var GetNEXTUSERID    = '' />
        <cfset var GetNEXTORGID = '' />
        <cfset var FreePlanCredits = 25/>
        <cfset var dataMail = '' />
        <cfset var PlanCredits = '' />
        <!--- <cfset var inputEmailData ={}/> --->
        <cfset var resultInsertMonthFree = structNew() />
        <cfset var resultUserInfo = structNew() />

        <cfset getCurrentUser.USERROLE = "blank" />
        <cfset var USERCOMPANYID = arguments.INPCOMPANYID>

        <cfset var promocode = {}>
        <cfset promocode.pcok = 0>
        <cfset var newUserPromoCode = {}>
        <cfset var RetVarmailChimpAPIs = '' />
        <cfset var DuplicateSMS_Number = ''/>
        <cfset var WhiteListPhoneNumber = ''/>

        <cfoutput>

            <cftry>
                <!--- Check permissions if an already logged in user adding as admin - otherwise the user permissions is "blank"--->
                <!---
                <cfif session.userId GT 0>
                    <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
                                <cfinvokeargument name="userId" value="#session.userId#">
                    </cfinvoke>

                    <!--- Verify it is a company admin trying to add account to a company - not un-authenticated web sign ups or URL hacks--->
                    <cfif INPCOMPANYID GT 0>

                        <cfif getCurrentUser.companyAccountId GT 0>

                            <cfif getCurrentUser.companyAccountId NEQ INPCOMPANYID >
                                <cfthrow message="Current user can not add company accounts for this compnay ID" detail="Permission Denied" type="any"/>
                            </cfif>

                        <cfelse>

                            <cfif getCurrentUser.USERROLE NEQ "SuperUser">
                                <cfthrow message="Current user (not a company admin) can not add company accounts. (#getCurrentUser.companyAccountId#) (#getCurrentUser.USERROLE#)" detail="Permission Denied" type="any"/>
                            </cfif>
                        </cfif>

                    </cfif>

                </cfif>
                --->

                    <cfset var INPNAPASSWORD = TRIM(arguments.INPNAPASSWORD)>
                    <cfset var INPMAINEMAIL = TRIM(arguments.INPMAINEMAIL)>
                    <cfset var INSMSNUMBER = TRIM(arguments.INSMSNUMBER)>

                    <!--- Validate email address --->
                    <cfinvoke method="VldEmailAddress" returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
                    </cfinvoke>

                    <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Invalid Email.'>
                        <cfreturn dataout>
                    </cfif>

                    <!--- Validate Email Unique --->
                    <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                        SELECT
                            EmailAddress_vch
                        FROM
                            simpleobjects.useraccount
                        WHERE
                            EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
                        AND
                            CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">
                        AND
                            Active_int > 0
                    </cfquery>

                    <cfif VerifyUnique.RecordCount GT 0>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
                        <cfreturn dataout>
                    </cfif>

                    <!--- Valid US Phone Number --->
                    <cfif NOT isvalid('telephone', INSMSNUMBER)>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
                        <cfreturn dataout>
                    </cfif>

                    <!--- Duplicate Phone Number & White Phone Number List --->
                    <cfquery name="DuplicateSMS_Number" datasource="#Session.DBSourceREAD#">
                        SELECT
                            UserId_int
                        FROM
                            simpleobjects.useraccount
                        WHERE
                            MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                    </cfquery>
                    <cfif DuplicateSMS_Number.RECORDCOUNT GT 0>
                         <cfquery name="WhiteListPhoneNumber" datasource="#Session.DBSourceREAD#">
                            SELECT
                                IdPhone
                            FROM
                                simpleobjects.whitelistphonenumber
                            WHERE
                                PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                        </cfquery>
                        <cfif WhiteListPhoneNumber.RECORDCOUNT lt 1>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'The phone number you entered is already in use and cannot be used at this time. Please try another.'>
                            <cfreturn dataout>
                        </cfif>
                    </cfif>
                    <!--- Don't limit SuperUsers and CompanyAdmins everyone else is limited --->
                    <cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">

                        <!--- check is user is registering from same ip address more then limited number of time --->
                        <cfquery name="checkLimit" datasource="#Session.DBSourceREAD#">
                            SELECT
                                count(*) as totalNumber
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                userIp_vch LIKE <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">
                            AND
                                (UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(created_dt))/3600 < #hoursLimit#
                        </cfquery>

                        <cfif checkLimit.totalNumber gte sameIpLimit>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'This IP (#CGI.REMOTE_ADDR#) address has reached the number of accounts limit (#hoursLimit#) in an hour. Call your system administrator if you need more.'>
                            <cfreturn dataout>
                        </cfif>

                    </cfif>

                    <cfif arguments.inpPromotionCode NEQ ''>
	                    <cfinvoke component="session.sire.models.cfc.promotion" method="getActivePromoCode" returnvariable="promocode">
							 <cfinvokeargument name="inpCode" value="#arguments.inpPromotionCode#">
	                    </cfinvoke>
	                    <cfif promocode.RXRESULTCODE EQ 1>
	                    	<cfif promocode.promotion_id GT 0>
								<cfset promocode.pcok = 1>
	                    	<cfelse>
	                            <cfset dataout.RESULT = 'FAIL'>
	                            <cfset dataout.USERID = '-1'>
	                            <cfset dataout.MESSAGE = 'Invalid Promotion Code.'>
	                            <cfreturn dataout>
		                    </cfif>
                    	<cfelse>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = promocode.message>
                            <cfreturn dataout>
	                    </cfif>
                    </cfif>


                        <!--- Validate proper password--->
                        <cfinvoke method="VldInput" returnvariable="safePass">
                            <cfinvokeargument name="Input" value="#INPNAPASSWORD#"/>
                        </cfinvoke>

                        <cfif safePass NEQ true OR INPNAPASSWORD EQ "">
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-'>
                            <cfreturn dataout>
                        </cfif>


                        <cfquery name="PlanCredits" datasource="#Session.DBSourceREAD#">
                            SELECT
                                FirstSMSIncluded_int
                            FROM
                                simplebilling.plans
                            WHERE
                                PlanId_int = 1
                        </cfquery>
                        <cfif PlanCredits.RecordCount GT 0>
                            <cfset FreePlanCredits = PlanCredits.FirstSMSIncluded_int/>
                        </cfif>

                        <!--- Add record --->
                        <cftransaction action="begin">

                        <cftry>
                            <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="GetNEXTUSERID">
                                INSERT INTO
                                    simpleobjects.useraccount
                                    (
                                        Password_vch,
                                        EmailAddress_vch,
                                        Created_dt,
                                        UserIp_vch,
                                        EmailAddressVerified_vch,
                                        CompanyAccountId_int,
                                        CBPId_int,
                                        firstName_vch,
                                        lastName_vch,
                                        timezone_vch,
                                        Active_int,
                                        CompanyUserId_int,
                                        HomePhoneStr_vch,
                                        MFAContactString_vch,
                                        MFAContactType_ti
                                    )
                                VALUES
                                    (
                                        AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">,
                                        NOW(),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                                    )
                            </cfquery>

                        	<cfif promocode.pcok EQ 1>
                        		<cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
		                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
		                            <cfinvokeargument name="inpPromoId" value="#promocode.promotion_id#">
                                    <cfinvokeargument name="inpPromoOriginId" value="#promocode.origin_id#">
		                            <cfinvokeargument name="inpUsedDate" value="#Now()#">
		                            <cfinvokeargument name="inpRecurringTime" value="#promocode.recurring_time#">
		                            <cfinvokeargument name="inpPromotionStatus" value="1">
	                            </cfinvoke>
	                            <cfif newUserPromoCode.RXRESULTCODE NEQ 1>
	                                <cftransaction action="rollback" />
	                                <cfset dataout.RESULT = 'FAIL'>
	                                <cfset dataout.USERID = '-1'>
	                                <cfset dataout.MESSAGE = newUserPromoCode.MESSAGE>
	                                <cfreturn dataout>
	                            </cfif>
                        	</cfif>

                            <cfif GetNEXTUSERID.GENERATED_KEY GT 0 AND INPORGNAME NEQ ''>
                                 <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#" result="GetNEXTORGID">
                                    INSERT INTO simpleobjects.organization
                                        (
                                            OrganizationName_vch,
                                            UserId_int
                                        )
                                    VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPORGNAME#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">
                                        )
                                 </cfquery>
                            </cfif>
                            <!--- remove API credentials
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.credential" method="insertAccessKey" returnvariable="datadump">
                            <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>
                            --->

                            <!--- set basic permissions for new individual user --->
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
                                <cfinvokeargument name="UCID" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="RoleType" value="2">
                                <cfinvokeargument name="Permission" value="#NewUserPermissions#">
                            </cfinvoke>

                            <!--- Setup Billing - Shared accounts will ignore that main value here and use Shared User Id instead but still insert default now --->
                            <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                                INSERT INTO
                                    simplebilling.billing
                                    (
                                        UserId_int,
                                        Balance_int,
                                        UnlimitedBalance_ti,
                                        RateType_int,
                                        Rate1_int,
                                        Rate2_int,
                                        Rate3_int,
                                        Increment1_int,
                                        Increment2_int,
                                        Increment3_int
                                     )
                                 VALUES
                                     (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#FreePlanCredits#"> ,
                                        0,
                                        1,
                                        1,
                                        1,
                                        1,
                                        30,
                                        30,
                                        30
                                        )
                            </cfquery>

                            <!--- add role --->
                            <cfset var roleName = 'User'>

                            <cfquery name="addRole" datasource="#Session.DBSourceEBM#">
                                INSERT INTO
                                    simpleobjects.userroleuseraccountref
                                    (
                                        userAccountId_int,
                                        roleId_int,
                                        modified_dt
                                    )
                                VALUES
                                    (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
                                        (
                                            SELECT
                                                roleId_int
                                            FROM
                                                simpleobjects.userrole
                                            WHERE
                                                roleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#roleName#">
                                        ),
                                        NOW()
                                    )
                            </cfquery>

                            <!--- ADD FREE PLAN --->
                            <cfset var todayDate = Now()>
                            <cfset var startDay = Day(todayDate)>

                            <cfif startDay GT 28>
                                <cfset startDay = 28>
                            </cfif>
                            <cfset var startMonth = Month(todayDate)>
                            <cfset var startYear = Year(todayDate)>

                            <cfset var startDate = CreateDate(startYear, startMonth, startDay)>

                            <cfset var endDate = DateAdd("m",1,startDate)>

                            <cfinvoke method="insertUsersPlan" component="#LocalSessionDotPath#.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                                <cfinvokeargument name="inpPlanId" value="1">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="inpStarDate" value="#startDate#">
                                <cfinvokeargument name="inpEndDate" value="#endDate#">
                            </cfinvoke>

                            <cfif checkAddPlan.RXRESULTCODE EQ -1>
                                <cftransaction action="rollback" />
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = checkAddPlan.MESSAGE>
                                <cfreturn dataout>
                            </cfif>

                            <cfinvoke method="createUserLog" component="#LocalSessionDotPath#.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#session.userid#">
                                <cfinvokeargument name="moduleName" value="Add new Account">
                                <cfinvokeargument name="operator" value="#INPMAINEMAIL#">
                            </cfinvoke>

                            <cftransaction action="commit" />
                            <cfcatch>
                                <cftransaction action="rollback" />
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                                <cfreturn dataout>
                            </cfcatch>
                        </cftry>

                        </cftransaction>

                        <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch
                            FROM
                                simpleobjects.systemalertscontacts
                            WHERE
                                ContactType_int = 2
                            AND
                                EMSNotice_int = 1
                        </cfquery>

                        <cfif GetEMSAdmins.RecordCount GT 0>
                            <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>
                        </cfif>

                        <!--- AUTO ASSIGN SHORTCODE --->
                        <cfinvoke method="AssignSireSharedCSC" component="Session.cfc.csc.csc" returnvariable="RetVarAssignSireSharedCSC">
                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            <cfinvokeargument name="inpKeywordLimit" value="1000">
                        </cfinvoke>

                        <cfif arguments.inpReferralCode NEQ '' && arguments.inpReferralType NEQ ''>
                            <cfset variables.isReferral = "yes" />
                            <!--- Insert Free Month use SIRE to the user send invitation to others who signed up successful --->
                            <cfinvoke method="InsertMonthFree" component="session.sire.models.cfc.referral" returnvariable="resultInsertMonthFree">
                                <cfinvokeargument name="inpReferralCode" value="#arguments.inpReferralCode#">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>

                            <!--- Insert Referral log to simpleobjects.sire_referral_log table --->
                            <cfinvoke method="InsertReferralLog" component="session.sire.models.cfc.referral">
                                <cfinvokeargument name="inpReferralType" value="#arguments.inpReferralType#">
                                <cfinvokeargument name="inpReferralCode" value="#arguments.inpReferralCode#">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>

                            <!--- Call function getuserinfo by id --->
                            <!--- <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
                                <cfinvokeargument name="inpUserId" value="#resultInsertMonthFree.SenderUserId#">
                            </cfinvoke>  --->

                            <!--- Set inputEmailData data to send email to inviter --->
                            <!--- <cfset inputEmailData.fullName = '#resultUserInfo.USERNAME#' & ' ' & '#resultUserInfo.LASTNAME#'>
                            <cfset inputEmailData.firstName = '#resultUserInfo.USERNAME#'>
                            <cfset inputEmailData.emailAddress = '#resultUserInfo.FULLEMAIL#'>
                            <cfset inputEmailData.subject = _ToInviterEmailSubject> --->

                            <!--- Send Notification Email to sender when invited user signed up successful --->
                            <!--- <cfinvoke method="SendMailNotificationToSender" component="session.sire.models.cfc.referral">
                                <cfinvokeargument name="inpData" value="#inputEmailData#">
                            </cfinvoke> --->
                        </cfif>

                        <!--- Event trigger sign in --->
                        <cfinvoke method="trigger" component="session.sire.models.cfc.events">
                            <cfinvokeargument name="event" value="signup"/>
                            <cfinvokeargument name="module" value="userstools.RegisterNewAccount"/>
                            <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#"/>
                        </cfinvoke>

                        <!--- Need change to new event trigger --->
                        <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                            <cfinvokeargument name="InpEmailString" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="InpFirstName" value="#INPFNAME#">
                            <cfinvokeargument name="InpLastName" value="#INPLNAME#">
                            <cfinvokeargument name="InpSignupDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                            <cfinvokeargument name="InpLastLoginDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                            <cfinvokeargument name="InpAccountType" value="Free">
                            <cfinvokeargument name="InpStatus" value="active">
                        </cfinvoke>

                        <!--- jp@ebmui.com; --->
                        <cfset var envServer = CGI />
                        <cfmail to="#EBMAdminEMSList#" subject="EBM User Account Created" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                   <style type="text/css">
                                        h4 {
                                        font-size: 10pt;
                                        font-weight: none;
                                        }

                                    </style>

                                    <body>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                                        <tr>
                                            <td style ="width:40%;"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/logo.jpg"  alt="banner"></td>
                                            <td style ="width:60%;font-size: 10pt; font-weight: nomal; padding: 30px 0px 30px 10px;">Add <u>support@siremobile.com</u> to your contacts.</td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                                        <tr>
                                            <td><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/bot-banner.jpg" width="100%" alt="banner">	  </td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; border-collapse: collapse; width: 25%; align:center;margin:3% 8% 3% 8%;">
                                        <tr style="background-color: ##578DA7;">
                                        <td colspan = "2"><h3 style="text-align: center; color: white;">Sire Account Creation Alert</h3></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>User Name :</b></td>
                                            <td style="padding: 8px;">#INPFNAME# #INPLNAME# </td>
                                        </tr>
                                        <tr style="background-color: ##dedede;">
                                            <td style="padding: 8px;"><b>User Id :</b></td>
                                            <td style="padding: 8px;">#GetNEXTUSERID.GENERATED_KEY# </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>Email :</b></td>
                                            <td style="padding: 8px;">#INPMAINEMAIL#</td>
                                        </tr>
                                        <tr style="background-color: ##dedede;">
                                            <td style="padding: 8px;"><b>Phone :</b></td>
                                            <td style="padding: 8px;">#INSMSNUMBER# </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>Ip Address :</b></td>
                                            <td style="padding: 8px;">#CGI.REMOTE_ADDR#</td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;background-color:##dedede;margin:0px;">
                                        <tr>
                                            <td style ="font-size: 10pt; font-weight: nomal; padding: 20px 0px 0px 20px;">For support requests, please call or email us at:</td>
                                        </tr>
                                        <tr>
                                            <td style ="font-size: 10pt; font-weight: nomal; padding: 0px 0px 20px 20px;"><a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>">https://siremobile.com</a>       |   888.335.0909   |   support@siremobile.com</td>
                                        </tr>
                                    </table>
                                    </div>

                                    </body>
                                </cfoutput>
                                <!---<cfoutput>

                                   <style type="text/css">

                                        body
                                        {
                                            background-image: linear-gradient(to bottom, ##FFFFFF, ##B4B4B4);
                                            background-repeat:no-repeat;
                                        }
                                        .message-block {
                                            font-family: "Verdana";
                                            font-size: 12px;
                                            margin-left: 21px;
                                        }

                                        .m_top_10 {
                                            margin-top: 10px;
                                        }

                                        ##divConfirm{
                                            padding:25px;
                                            width:600px;

                                        }

                                        ##divConfirm .left-input{
                                            border-radius: 4px 0 0 4px;
                                            border-style: solid none solid solid;
                                            border-width: 1px 0 1px 1px;
                                            color: ##666666;
                                            float: left;
                                            height: 28px;
                                            line-height: 25px;
                                            padding-left: 10px;
                                            width: 170px;
                                            background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);
                                        }
                                        ##divConfirm .right-input {
                                           width: 226px;
                                           display:inline;
                                           height: 28px;
                                        }
                                        ##divConfirm input {
                                            width: 210px;
                                            background-color: ##FBFBFB;
                                            color: ##000000 !important;
                                            font-family: "Verdana" !important;
                                            font-size: 12px !important;
                                            height: 30px;
                                            line-height: 30px;

                                            border: 1px solid rgba(0, 0, 0, 0.3);
                                            border-radius: 0 3px 3px 0;
                                            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
                                            margin-bottom: 8px;
                                            padding: 0 8px;


                                        }
                                        ##divConfirm .left-input > span.em-lbl {
                                           width: auto;
                                        }

                                    </style>
                                    <div id="divConfirm" align="left">
                                        <div ><h4>Sire Account Creation Alert!</h4></div>

                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">New User Id</span>
                                            </div>
                                            <div class="right-input">
                                                <input type="text" readonly value="#GetNEXTUSERID.GENERATED_KEY#">
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">New User Name</span>
                                            </div>
                                            <div class="right-input">
                                                <input type="text" readonly value="#INPFNAME# #INPLNAME#">
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">eMail Address</span>
                                            </div>
                                            <div class="right-input">
                                                <input type="text" readonly value="#INPMAINEMAIL#">
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company User Id</span>
                                            </div>
                                            <div class="right-input">
                                                <input type="text" readonly value="#companyUserId#">
                                            </div>
                                        </div>
                                         <div class="clear"></div>
                                        <div class="message-block  m_top_10">
                                            <div class="left-input">
                                                <span class="em-lbl">Company Id</span>
                                            </div>
                                            <div class="right-input">
                                                <input type="text" readonly value="#USERCOMPANYID#">
                                            </div>
                                        </div>

                                        <div class="clear"></div>

                                        <div style="height:200px;"></div>

                                        <div class="clear"></div>

                                        <div style="padding:20px;">
                                           <cfdump var="#CGI#"/>
                                        </div>

                                    </div>
                                </cfoutput>--->
                            </cfmail>
                    <cfset dataMail = {
                        UserName: INPFNAME,
                        LastName: INPLNAME,
                        MobileNumber: INSMSNUMBER,
                        rootUrl:rootUrl
                    }/>

                    <cfif variables.isReferral EQ "yes">
                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                            <cfinvokeargument name="to" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="type" value="html">
                            <cfinvokeargument name="subject" value="Thank you for registering with Sire!">
                            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_referral_registration_completed.cfm">
                            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                            <cfinvokeargument name="to" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="type" value="html">
                            <cfinvokeargument name="subject" value="Welcome to Sire!">
                            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_registration_completed.cfm">
                            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                        </cfinvoke>
                    </cfif>




                    <!--- SET COOKIE --->

                    <cfset dataout.USERID = '#GetNEXTUSERID.GENERATED_KEY#'>
                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.MESSAGE = 'You have registered successfully.'>

                    <!--- CALL TO LOGIN --->
                    <cfif RegisteredByAgency EQ 0>
                         <cfinvoke component="public.sire.models.cfc.userstools" method="validateUsers" returnvariable="userLogin">
                             <cfinvokeargument name="inpUserID" value="#INPMAINEMAIL#"/>
                             <cfinvokeargument name="inpPassword" value="#inpNAPassword#"/>
                             <cfinvokeargument name="inpFromSource" value="SignUp"/>
                         </cfinvoke>
                         <cfif userLogin.RXRESULTCODE EQ 1>
                             <!--- ADD 4 custom field --->
                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled1">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[1]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled2">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[2]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled3">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[3]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled4">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[4]#">
                             </cfinvoke>
                         </cfif>
                    </cfif>

                <cfcatch TYPE="any">
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        </cfoutput>
        <cfreturn dataout />
    </cffunction>

    <!--- Careful! This is publicly accessible via javscript and AJAX - Default to low permissions and standard credit billing type --->
    <cffunction name="RegisterNewAccountNew" access="remote" output="true" hint="Start a new user account">

        <cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
        <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
        <cfargument name="INSMSNUMBER" required="yes" default="" type="string">
        <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
        <cfargument name="INPTIMEZONE" required="yes" default="31" type="string">
        <cfargument name="INPFNAME" required="yes" default="" type="string">
        <cfargument name="INPLNAME" required="yes" default="" type="string">
        <cfargument name="INPORGNAME" required="no" default="" type="string">
        <cfargument name="INPCBPID" required="no" default="1" type="string">
        <cfargument name="INPSHAREDRESOURCES" required="no" default="0" type="string">
        <cfargument name="INPCOMPANYID" required="no" default="0" type="string">
        <cfargument name="inpReferralCode" required="no" default="" type="string">
        <cfargument name="inpReferralType" required="no" default="" type="string">
        <cfargument name="inpPromotionCode" required="no" default="" type="string">
        <cfargument name="inpPlanId" required="yes" default="1">
        <cfargument name="inpIsYearly" required="no" default="0">
        <cfargument name="RegisteredByAgency" required="no" default="0" type="string">

        <cfargument name="inpAddress" required="no" default="" type="string">
        <cfargument name="inpCity" required="no" default="" type="string">
        <cfargument name="inpState" required="no" default="" type="string">
        <cfargument name="inpZip" required="no" default="" type="string">
        <cfargument name="inpTaxId" required="no" default="" type="string">
        <cfargument name="inpAffiliateType" required="no" default="0" type="string" hint=" 1 is request, 2 is apply, 0 is common user sign up">
        <cfargument name="inpAffiliateCode" required="no" default="" type="string">
        <cfargument name="inpAMI" required="no" default="0" type="string">
        <cfargument name="inpPaypalEmail" required="no" default="" type="string">

        <!--- TRE--->
        <cfargument name="inpAllowDuplicatePhone" required="no" default="0">
        <cfargument name="inpAllowDuplicateMail" required="no" default="0">
        <cfargument name="inpUnlimitedCredit" required="no" default="0">
        <cfargument name="inpUserType" required="no" default="1" hint="1 is for comon user, 2 for integrate user">
        <cfargument name="MFAEnable" required="no" default="1" type="string">
        <cfargument name="INPCOMPANYNAME" required="no" default="" type="string">
        <cfargument name="inpIntegrateHigherUserID" required="no" default="0">
        <cfargument name="inpIntegrateUserID" required="no" default="">
        <cfargument name="inpIntegrateUserLevel" required="no" default="0">
        <cfargument name="IntegrateCompanyID" required="no" default="" hint="its foreign key from table simpleobjects.integrate_company. not the same INPCOMPANYID">
        <cfargument name="inpHigherUserID" required="no" default="">


        <cfargument name="payment_method" required="no" default="">
        <cfargument name="line1" required="no" default="">
        <cfargument name="city" required="no" default="">
        <cfargument name="country" required="no" default="">
        <cfargument name="phone" required="no" default="">
        <cfargument name="state" required="no" default="">
        <cfargument name="zip" required="no" default="">
        <cfargument name="email" required="no" default="">
        <cfargument name="cvv" required="no" default="">
        <cfargument name="expirationDate" required="no" default="">
        <cfargument name="firstName" required="no" default="">
        <cfargument name="lastName" required="no" default="">
        <cfargument name="number" required="no" default="">
        <cfargument name="primaryPaymentMethodId" required="no" default="1">

        <cfargument name="inpPaymentMethod" required="no" default="1">

        <cfset var cardinfo = {
            inpNumber   = arguments.number,
            expirationDate  = arguments.expirationDate,
            inpCvv2         = arguments.cvv,
            inpFirstName    = arguments.firstName,
            inpLastName     = arguments.lastName,
            inpLine1        = arguments.line1,
            inpCity         = arguments.city,
            inpCountryCode  = arguments.country,
            inpPostalCode   = arguments.zip,
            inpState        = arguments.state,
            inpEmail           = arguments.email,
            phone           = arguments.phone
        }>

        <cfset var dataout = {} />

        <cfset dataout.USERID = '-1'>

        <cfset var sameIpLimit = 25 />
        <cfset var hoursLimit = 2 />
        <cfset var companyUserId = 1 />
        <cfset var NewUserPermissions = ArrayToList(BasicPermission, ",") />
        <cfset var getCurrentUser = StructNew() />
        <cfset var EBMAdminEMSList = "" />
        <cfset var GetEMSAdmins = ""/>
        <cfset var RetVarsetPermission = "" />
        <cfset var VerifyUnique = '' />
        <cfset var checkLimit   = '' />
        <cfset var AddUser  = '' />
        <cfset var AddOrginazation  = '' />
        <cfset var AddBilling   = '' />
        <cfset var addRole  = '' />
        <cfset var safeeMail    = '' />
        <cfset var safePass = '' />
        <cfset var datadump = '' />
        <cfset var checkAddPlan = '' />
        <cfset var RetVarAssignSireSharedCSC    = '' />
        <cfset var userLogin    = '' />
        <cfset var RetVarAddNewCustomFiled1 = '' />
        <cfset var RetVarAddNewCustomFiled2 = '' />
        <cfset var RetVarAddNewCustomFiled3 = '' />
        <cfset var RetVarAddNewCustomFiled4 = '' />
        <cfset var GetNEXTUSERID    = '' />
        <cfset var GetNEXTORGID = '' />
        <cfset var FreePlanCredits = 25/>
        <cfset var dataMail = '' />
        <cfset var PlanCredits = '' />
        <!--- <cfset var inputEmailData ={}/> --->
        <cfset var resultInsertMonthFree = structNew() />
        <cfset var resultUserInfo = structNew() />

        <cfset getCurrentUser.USERROLE = "blank" />
        <cfset var USERCOMPANYID = arguments.INPCOMPANYID>

        <cfset var promocode = {}>
        <cfset promocode.pcok = 0>
        <cfset var newUserPromoCode = {}>
        <cfset var addPromotionKeyword = 0/>
        <cfset var addPromotionMLP = 0/>
        <cfset var addPromotionShortURL = 0/>
        <cfset var addPromotionCredit = 0/>
        <cfset var totalAMOUNT = 0/>
        <cfset var addPromotionID = 0/>
        <cfset var addPromotionLatestVersion = 0/>
        <cfset var checkPaymentResult = '' />
        <cfset var txtOrderPlan = '' />
        <cfset var paymentData = '' />
        <cfset var closeBatchesData = '' />
        <cfset var paymentResposeContent = '' />
        <cfset var purchaseType = '' />
        <cfset var purchaseAmount = '' />
        <cfset var creditsPurchased = '' />
        <cfset var paymentRespose = '' />
        <cfset var getPromotion = '' />
        <cfset var updateUsersPlan = '' />
        <cfset var getPlan = '' />
        <cfset var updateCustomerResult = '' />
        <cfset var RetVarmailChimpAPIs = '' />
        <cfset var closeBatchesRequestResult = '' />
        <cfset var planUpdate = '' />
        <cfset var updatePromoCode = ''/>
        <cfset var resultUpdateInviter = ''/>
        <cfset var resultUserInfo = ''/>
        <cfset var getInviterPlanResult = ''/>
        <cfset var inputEmailData = {} />
        <cfset var VerifySMS_Number = ''/>
        <cfset var DuplicateSMS_Number = ''/>
        <cfset var WhiteListPhoneNumber = ''/>
        <cfset var rtSaveAffiliate=''/>
        <cfset var AffiliateCodeApply = ''/>
        <cfset var InpAccountTypeText = 'Free'/>
        <cfset var paymentErrMess = ''/>
        <cfset var emailTransactionExceededLimit= {}>
        <cfset var logData = ''/>
        <cfset var emailData = {}/>
        <cfset var rtGetTransactionExceededLimit = {}/>
        <cfset var retvalsendmail = {}/>
        <cfset var RxMakePaymentWithMojo = {}/>
        <cfset var inpCardObject = {} >
        <cfset var rtGetCouponFromAffiliateCode=''>
        <cfset var rtGetPaymentMethodSetting={}>
        <cfset var RxMakePaymentWithCC	= '' />
        <cfset var RetVarmailChimpAPIsV3	= '' />
        <cfset var utm_source	= 'UNKNOWN' />
        <cfset var isAffiliateSource=0>
        <cfset var rtGetAffiliateFromCouponCode	= '' />
            <cftry>
                    <cfif structKeyExists(cookie,'utm_source')>
                        <cfset utm_source= cookie.utm_source>
                    <cfelseif arguments.inpAffiliateCode NEQ "">
                        <cfset utm_source= arguments.inpAffiliateCode>
                        <cfset isAffiliateSource=1>
                    </cfif>

                    <cfinvoke method="GetPaymentMethodSetting" component="public/sire/models/cfc/userstools" returnvariable="rtGetPaymentMethodSetting">

                    </cfinvoke>

                    <cfset arguments.inpPaymentMethod= rtGetPaymentMethodSetting.RESULT>
                    <cfif arguments.inpAMI GT 0>
                        <cfset AffiliateCodeApply = arguments.inpAffiliateCode>
                    </cfif>
                    <cfset var INPNAPASSWORD = TRIM(arguments.INPNAPASSWORD)>
                    <cfset var INPMAINEMAIL = TRIM(arguments.INPMAINEMAIL)>
                    <cfset var INSMSNUMBER = TRIM(arguments.INSMSNUMBER)>

                    <!--- Validate email address --->
                    <cfinvoke method="VldEmailAddress" returnvariable="safeeMail">
                        <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
                    </cfinvoke>
                    <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Invalid Email.'>
                        <cfreturn dataout>
                    </cfif>

                    <!--- Validate Email Unique --->
                    <cfif arguments.inpAllowDuplicateMail EQ "0">
                        <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                            SELECT
                                EmailAddress_vch
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
                            AND
                                CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">
                            AND
                                Active_int > 0
                        </cfquery>
                        <cfif VerifyUnique.RecordCount GT 0>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
                            <cfreturn dataout>
                        </cfif>
                    </cfif>

                    <!--- Valid US Phone Number --->
                    <cfif NOT isvalid('telephone', INSMSNUMBER)>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
                        <cfreturn dataout>
                    </cfif>

                    <cfquery name="VerifySMS_Number" datasource="#Session.DBSourceREAD#">
                        SELECT
                            IdPhone
                        FROM
                            simpleobjects.phone_number_blocked
                        WHERE
                            PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                    </cfquery>
                    <cfif VerifySMS_Number.RecordCount GT 0>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'We were unable to send the verification code to the phone number you entered.<br>Please try the following:<br>*  Make sure the number was entered correctly.<br>*  Ask your mobile phone carrier to enable messages from short codes<br>*  Use a different number with your account<br>Contact Customer Support for further assistance.'>
                        <cfreturn dataout>
                    </cfif>

                    <!--- Duplicate Phone Number & White Phone Number List --->
                    <cfif arguments.inpAllowDuplicatePhone EQ "0">
                        <cfquery name="DuplicateSMS_Number" datasource="#Session.DBSourceREAD#">
                            SELECT
                                UserId_int
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                        </cfquery>
                        <cfif DuplicateSMS_Number.RECORDCOUNT GT 0>
                            <cfquery name="WhiteListPhoneNumber" datasource="#Session.DBSourceREAD#">
                                SELECT
                                    IdPhone
                                FROM
                                    simpleobjects.whitelistphonenumber
                                WHERE
                                    PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                            </cfquery>
                            <cfif WhiteListPhoneNumber.RECORDCOUNT lt 1>
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = 'The phone number you entered is already in use and cannot be used at this time. Please try another.'>
                                <cfreturn dataout>
                            </cfif>
                        </cfif>
                    </cfif>

                    <cfif (arguments.inpAMI EQ 0 OR arguments.inpAMI EQ "") AND arguments.inpAffiliateCode NEQ "" AND arguments.inpAffiliateType EQ 2>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Invalid Affiliate Code. Please Check Again'>
                        <cfreturn dataout>
                    </cfif>
                    <cfif arguments.inpPromotionCode EQ '' AND arguments.inpAMI NEQ 0 AND arguments.inpAMI NEQ "" AND arguments.inpAffiliateCode NEQ "" AND arguments.inpAffiliateType EQ 2>
                        <!--- check linkage coupon code with affiliate code--->
                        <cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
                            <cfinvokeargument name="inpAffiliateCode" value="#arguments.inpAffiliateCode#"/>
                        </cfinvoke>
                        <cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>
                            <cfset arguments.inpPromotionCode =rtGetCouponFromAffiliateCode.COUPONCODE >
                        </cfif>
                    </cfif>

                    <cfif arguments.inpPromotionCode NEQ '' AND arguments.inpAMI EQ 0 AND arguments.inpAffiliateCode EQ "">
                        <!--- check linkage coupon code with affiliate code--->
                        <cfinvoke method="GetAffiliateFromCouponCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetAffiliateFromCouponCode">
                            <cfinvokeargument name="inpCouponCode" value="#arguments.inpPromotionCode#"/>
                        </cfinvoke>
                        <cfif rtGetAffiliateFromCouponCode.RXRESULTCODE EQ 1>
                            <cfset arguments.inpAMI =rtGetAffiliateFromCouponCode.AFFILIATEUSERID >
                            <cfset arguments.inpAffiliateCode =rtGetAffiliateFromCouponCode.AFFILIATECODE >
                            <cfset AffiliateCodeApply= rtGetAffiliateFromCouponCode.AFFILIATECODE >
                        </cfif>
                    </cfif>

                    <!--- Validate proper password--->
                    <cfinvoke method="VldInput" returnvariable="safePass">
                        <cfinvokeargument name="Input" value="#INPNAPASSWORD#"/>
                    </cfinvoke>

                    <cfif safePass NEQ true OR INPNAPASSWORD EQ "">
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-'>
                        <cfreturn dataout>
                    </cfif>

                    <cfquery name="PlanCredits" datasource="#Session.DBSourceREAD#">
                        SELECT
                            FirstSMSIncluded_int
                        FROM
                            simplebilling.plans
                        WHERE
                            PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#">
                    </cfquery>
                    <cfif PlanCredits.RecordCount GT 0>
                        <cfset FreePlanCredits = PlanCredits.FirstSMSIncluded_int/>
                    </cfif>

                    <cfinvoke method="GetOrderPlan" component="session/sire/models/cfc/order_plan" returnvariable="getPlan">
                        <cfinvokeargument name="plan" value="#arguments.inpPlanId#"/>
                    </cfinvoke>
                    <cfset InpAccountTypeText = getPlan.PLANNAME/>
                    <cfif arguments.inpIsYearly EQ 0>
                        <cfset totalAMOUNT = getPlan.AMOUNT/>
                    <cfelseif arguments.inpIsYearly EQ 1>
                        <cfset totalAMOUNT = getPlan.YEARLYAMOUNT*12/>
                    </cfif>
                    <cfif arguments.inpPromotionCode NEQ ''>
                        <cfinvoke component="session.sire.models.cfc.promotion" method="getActivePromoCode" returnvariable="promocode">
                            <cfinvokeargument name="inpCode" value="#arguments.inpPromotionCode#">
                            <cfinvokeargument name="inpIsYearlyPlan" value="#arguments.inpIsYearly#">
                        </cfinvoke>

                        <cfif promocode.RXRESULTCODE EQ 1>
                            <cfif promocode.promotion_id GT 0>
                                <cfset promocode.pcok = 1>

                                 <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
                                    SELECT
                                      pc.promotion_id,
                                      pc.origin_id,
                                      pc.discount_type_group,
                                      pc.discount_plan_price_percent,
                                      pc.discount_plan_price_flat_rate,
                                      pc.promotion_keyword,
                                      pc.promotion_MLP,
                                      pc.promotion_short_URL,
                                      pc.promotion_credit

                                    FROM simplebilling.promotion_codes pc
                                    WHERE
                                        pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#promocode.promotion_id#">
                                        AND pc.promotion_status  = 1
                                    LIMIT 1
                                </cfquery>

                                <cfif getPromotion.RecordCount GT 0>
                                  <cfset addPromotionID = "#getPromotion.origin_id#">
                                  <cfset addPromotionLatestVersion = "#getPromotion.promotion_id#">

                                    <cfif getPromotion.discount_type_group EQ 1>
                                        <cfif arguments.inpIsYearly EQ 0>
                                            <cfset totalAMOUNT = (getPlan.AMOUNT - (getPlan.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
                                        <cfelseif arguments.inpIsYearly EQ 1>
                                            <cfset totalAMOUNT = (getPlan.YEARLYAMOUNT*12 - (getPlan.YEARLYAMOUNT*12 * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
                                        </cfif>
                                        <!--- <cfset totalAMOUNT = (getPlan.AMOUNT - (getPlan.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)> --->
                                    <cfelseif getPromotion.discount_type_group EQ 2>
                                            <cfset addPromotionKeyword = "#getPromotion.promotion_keyword#">
                                            <cfset addPromotionMLP = "#getPromotion.promotion_MLP#">
                                            <cfset addPromotionShortURL = "#getPromotion.promotion_short_URL#">
                                            <cfset addPromotionCredit = "#getPromotion.promotion_credit#">
                                    </cfif>
                                </cfif>

                                <!--- Down Redemption count --->
                                <cfset var now = Now()>
                                <cfquery name="updatePromoCode" datasource="#Session.DBSourceEBM#">
                                    UPDATE simplebilling.promotion_codes
                                    SET redemption_count_int = (redemption_count_int - 1)
                                    WHERE
                                        redemption_count_int > 0
                                        AND max_redemption_int > 0
                                        AND promotion_status = 1
                                        AND date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">
                                        AND IFNULL(expiration_date, date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">)
                                        AND promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#promocode.promotion_id#">
                                </cfquery>
                                <!--- END Down Redemption count --->

                            <cfelse>
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = 'Invalid Promotion Code.'>
                                <cfreturn dataout>
                            </cfif>
                        <cfelse>
                            <cfif arguments.inpAMI NEQ 0 AND arguments.inpAMI NEQ "" AND arguments.inpAffiliateCode NEQ "" AND arguments.inpAffiliateType EQ 2>
                                <!--- for invalid coupon code linkage with affiliate code we allow signup process--->
                                <cfset promocode.pcok=0>
                            <cfelse>
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = promocode.message>
                                <cfreturn dataout>
                            </cfif>
                        </cfif>
                    </cfif>

                        <!--- begin payment process --->
                        <cfset checkPaymentResult = ''>
                       <!---  <cfif totalAMOUNT EQ 0>
                            <cfset checkPaymentResult = 1>
                        </cfif>  --->
                         <cfinvoke method="GetTransactionExceededLimit" component="session.sire.models.cfc.billing" returnvariable="rtGetTransactionExceededLimit">
                            <cfinvokeargument name="inpPaymentMethod" value="#arguments.inpPaymentMethod#">
                        </cfinvoke>
                        <cfif rtGetTransactionExceededLimit.RXRESULTCODE EQ 1 AND rtGetTransactionExceededLimit.TRANSACTIONEXCEEDEDLIMIT GT 0 AND rtGetTransactionExceededLimit.TRANSACTIONEXCEEDEDLIMIT LT totalAMOUNT>
                            <!--- Send Email to Admin --->
                            <cftry>

                                <cfset emailTransactionExceededLimit.USERID = "Signup Fail"/>
                                <cfset emailTransactionExceededLimit.AMOUNT = totalAMOUNT/>
                                <cfset emailTransactionExceededLimit.PHONENUMBER = arguments.INSMSNUMBER />
                                <cfset emailTransactionExceededLimit.USERNAME = arguments.INPFNAME & " " & arguments.INPLNAME/>
                                <cfset emailTransactionExceededLimit.TRANSACTION="Signup Plan">
                                <cfset emailTransactionExceededLimit.GATEWAY="Mojo">
                                <cfif arguments.inpPaymentMethod EQ 1>
                                    <cfset emailTransactionExceededLimit.GATEWAY="WorldPay">
                                <cfelseif arguments.inpPaymentMethod EQ 2>
                                    <cfset emailTransactionExceededLimit.GATEWAY="PayPal">
                                <cfelseif arguments.inpPaymentMethod EQ 3>
                                    <cfset emailTransactionExceededLimit.GATEWAY="Mojo">
                                <cfelseif arguments.inpPaymentMethod EQ 4>
                                    <cfset emailTransactionExceededLimit.GATEWAY="TotalApps">
                                </cfif>

                                <!--- Send email alert #SupportEMailUserName#--->
                                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
                                    <cfinvokeargument name="to" value="#SupportEMailUserName#"/>
                                    <cfinvokeargument name="type" value="html"/>
                                    <cfinvokeargument name="subject" value="Customer Transaction Exceeded Limit By Payment Gateway"/>
                                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_payment_exceeded_limit.cfm"/>
                                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(emailTransactionExceededLimit))#"/>
                                </cfinvoke>
                                <cfcatch>
                                    <cfthrow  type = "any" message = "#cfcatch.MESSAGE#" detail = "#cfcatch.MESSAGE#" >
                                </cfcatch>

                            </cftry>

                            <cfthrow  type = "any" message = "You have transaction exceeded limit, our support will review and get back to you soon." detail = "You have transaction exceeded limit, our support will review and get back to you soon." >

                        </cfif>
                        <!--- Add record --->
                        <cftransaction action="begin">

                        <cftry>
                            <!--- Payment process --->

                            <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="GetNEXTUSERID">
                                INSERT INTO
                                    simpleobjects.useraccount
                                    (
                                        Password_vch,
                                        EmailAddress_vch,
                                        Created_dt,
                                        UserIp_vch,
                                        EmailAddressVerified_vch,
                                        CompanyAccountId_int,
                                        CBPId_int,
                                        firstName_vch,
                                        lastName_vch,
                                        timezone_vch,
                                        Active_int,
                                        CompanyUserId_int,
                                        HomePhoneStr_vch,
                                        MFAContactString_vch,

                                        MFAContactType_ti,
                                        MFAEnabled_ti,
                                        CompanyName_vch,
                                        UserLevel_int,
                                        IntegrateUserID_int,
                                        IntegrateHigherUserID,
                                        UserType_int,
                                        HigherUserID_int,
                                        IntegrateCompanyID_int,


                                        Address1_vch,
                                        City_vch,
                                        State_vch,
                                        PostalCode_vch,
                                        SSN_TaxId_vch,
                                        amc_vch,
                                        ami_int,
                                        amd_dt,
                                        PaypalEmail_vch,
                                        PaymentGateway_ti,
                                        MarketingSource_vch,
                                        IsAffiliateSource_ti

                                    )
                                VALUES
                                    (
                                        AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">,
                                        NOW(),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,

                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.MFAEnable#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.INPCOMPANYNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIntegrateUserLevel#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIntegrateUserID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIntegrateHigherUserID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserType#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpHigherUserID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.IntegrateCompanyID#">,


                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAddress#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpCity#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpState#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpZip#">,
                                        AES_ENCRYPT('#inpTaxId#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#AffiliateCodeApply#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAMI#">,
                                        NOW(),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPaypalEmail#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPaymentMethod#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#utm_source#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#isAffiliateSource#">
                                    )
                            </cfquery>
                            <cfif arguments.inpAffiliateType EQ 1>
                                <!--- create new affilate code for user--->
                                <cfinvoke component="public.sire.models.cfc.affiliate" method="SaveAffiliate" returnvariable="rtSaveAffiliate">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                    <cfinvokeargument name="inpAffiliateCode" value="#arguments.inpAffiliateCode#">
                                    <cfinvokeargument name="inpCreateBy" value="#GetNEXTUSERID.GENERATED_KEY#">

                                </cfinvoke>
                                <cfset Session._rememberPath = "/session/sire/pages/affiliates-dashboard" />
                                <cfif rtSaveAffiliate.RXRESULTCODE EQ -1>
                                    <cftransaction action="rollback" />
                                    <cfset dataout.RESULT = 'FAIL inpAffiliateType'>
                                    <cfset dataout.USERID = '-2'>
                                    <cfset dataout.MESSAGE = rtSaveAffiliate.MESSAGE>
                                    <cfreturn dataout>
                                </cfif>
                            <cfelseif arguments.inpAffiliateType EQ 2>
                                <!--- update user is sign up by affiliate code--->

                            <cfelse>

                            </cfif>

                            <!--- 1.get plan amount --->
                            <cfif promocode.pcok EQ 1>
                                <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                    <cfinvokeargument name="inpPromoId" value="#promocode.promotion_id#">
                                    <cfinvokeargument name="inpPromoOriginId" value="#promocode.origin_id#">
                                    <cfinvokeargument name="inpUsedDate" value="#Now()#">
                                    <cfinvokeargument name="inpRecurringTime" value="#promocode.recurring_time#">
                                    <cfinvokeargument name="inpPromotionStatus" value="1">
                                </cfinvoke>

                                <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                    <cfinvokeargument name="inpUsedFor" value="1">
                                </cfinvoke>

                                <!--- Increase coupon code used time --->
                                <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                                    <cfinvokeargument name="inpCouponId" value="#promocode.origin_id#"/>
                                    <cfinvokeargument name="inpUsedFor" value="1"/>
                                </cfinvoke>
                                <!--- End increase coupon code used time --->

                                <!--- Insert coupon code used log --->
                                <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                                    <cfinvokeargument name="inpCouponId" value="#promocode.promotion_id#"/>
                                    <cfinvokeargument name="inpOriginCouponId" value="#promocode.origin_id#"/>
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#"/>
                                    <cfinvokeargument name="inpUsedFor" value="Sign up and use coupon"/>
                                </cfinvoke>
                                <!--- End insert log --->

                                <cfif newUserPromoCode.RXRESULTCODE NEQ 1>
                                    <cftransaction action="rollback" />
                                    <cfset dataout.RESULT = 'FAIL'>
                                    <cfset dataout.USERID = '-3'>
                                    <cfset dataout.MESSAGE = newUserPromoCode.MESSAGE>
                                    <cfreturn dataout>
                                </cfif>
                            </cfif>

                            <cfif GetNEXTUSERID.GENERATED_KEY GT 0 AND INPORGNAME NEQ ''>
                                 <cfquery name="AddOrginazation" datasource="#Session.DBSourceEBM#" result="GetNEXTORGID">
                                    INSERT INTO simpleobjects.organization
                                        (
                                            OrganizationName_vch,
                                            UserId_int
                                        )
                                    VALUES
                                        (
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPORGNAME#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">
                                        )
                                 </cfquery>
                            </cfif>

                            <cfif totalAMOUNT GT 0 >
                                <cfset checkPaymentResult = 0>
                                <cfset txtOrderPlan =  "Order Plan: "&getPlan.PLANNAME>

                                <cfif arguments.inpPaymentMethod EQ 1>
                                    <cfset paymentData = {
                                            amount = totalAMOUNT,
                                            transactionDuplicateCheckIndicator = 1,
                                            extendedInformation = {
                                                notes = txtOrderPlan,
                                            },
                                            developerApplication = {
                                                developerId: worldpayApplication.developerId,
                                                Version: worldpayApplication.Version
                                          }
                                        }>

                                        <cfset paymentData.card = {
                                                number          = arguments.number,
                                                cvv             = arguments.cvv,
                                                expirationDate  = arguments.expirationDate,
                                                firstName       = arguments.INPFNAME,
                                                lastName        = arguments.INPLNAME,
                                                address         = {
                                                    line1   = arguments.line1,
                                                    city    = arguments.city,
                                                    state   = arguments.state,
                                                    zip     = arguments.zip,
                                                    country = arguments.country,
                                                    phone   = arguments.phone
                                                },
                                                email       = arguments.email

                                            }>
                                    <cfset closeBatchesData = {
                                            developerApplication = {
                                                developerId: worldpayApplication.developerId,
                                                Version: worldpayApplication.Version
                                            }
                                        }>
                                    <cftry>

                                    <cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
                                        <cfhttpparam type="header" name="content-type" value="application/json">
                                        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                                        <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
                                    </cfhttp>

                                    <cfif paymentRespose.status_code EQ 200>
                                        <cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>

                                            <cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
                                            <cfset purchaseType = '#NumberFormat(getPlan.FIRSTSMSINCLUDED)# Credit Purchase for $#NumberFormat( getPlan.AMOUNT)# USD'>
                                            <cfset purchaseAmount = getPlan.AMOUNT>
                                            <cfset creditsPurchased = getPlan.FIRSTSMSINCLUDED>
                                                <cfif paymentResposeContent.success>
                                                    <!--- close session--->
                                                    <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
                                                    <cfhttpparam type="header" name="content-type" value="application/json">
                                                    <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                                                    <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
                                                    </cfhttp>


                                                    <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                                        <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                                        <cfinvokeargument name="planId" value="#arguments.inpPlanId#">
                                                        <cfinvokeargument name="numberKeyword" value="">
                                                        <cfinvokeargument name="numberSMS" value="">
                                                        <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
                                                        <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
                                                    </cfinvoke>

                                                    <cfset checkPaymentResult = 1>

                                                </cfif>
                                        </cfif>

                                    </cfif>

                                    <cftry>
                                        <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                                            <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
                                            <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                                            <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                                            <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
                                            <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                                            <cfinvokeargument name="paymentdata" value="#paymentData#">
                                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                        </cfinvoke>
                                        <cfcatch type="any">
                                        </cfcatch>
                                    </cftry>

                                    <cfcatch type="any">
                                        <cftransaction action="rollback" />
                                        <cftry>
                                            <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                                                <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
                                                <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                                                <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                                                <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail# #arguments.email#">
                                                <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                                                <cfinvokeargument name="paymentdata" value="#paymentData#">
                                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                            </cfinvoke>
                                            <cfcatch type="any">
                                            </cfcatch>
                                        </cftry>

                                        <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                                        <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                                        <cfset dataout.RXRESULTCODE = -5 />
                                        <cfreturn dataout />
                                    </cfcatch>
                                    </cftry>

                                <cfelseif arguments.inpPaymentMethod EQ 2 OR arguments.inpPaymentMethod EQ 3 OR arguments.inpPaymentMethod EQ 4>
                                    <cfinvoke component="session.sire.models.cfc.payment.payment" method="MakePaymentWithCC" returnvariable="RxMakePaymentWithCC">
                                        <cfinvokeargument name="inpOrderAmount" value="#totalAMOUNT#">
                                        <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentMethod#">
                                        <cfinvokeargument name="inpPaymentdata" value="#SerializeJSON(cardinfo)#">
                                        <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                        <cfinvokeargument name="inpModuleName" value="Signup and Buy Plan">
                                    </cfinvoke>

                                    <cfif RxMakePaymentWithCC.RXRESULTCODE EQ 1>
                                        <cfset checkPaymentResult = 1/>

                                        <cfif structKeyExists(RxMakePaymentWithCC, "REPORT")>
                                            <cfset logData =  RxMakePaymentWithCC['REPORT']>
                                        </cfif>

                                        <cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
                                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                            <cfinvokeargument name="planId" value="#arguments.inpPlanId#">
                                            <cfinvokeargument name="numberKeyword" value="">
                                            <cfinvokeargument name="numberSMS" value="">
                                            <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
                                            <cfinvokeargument name="paymentRespose" value="#SerializeJSON(logData)#">
                                            <cfinvokeargument name="inppaymentmethod" value="#arguments.inpPaymentMethod#">
                                        </cfinvoke>
                                        <cfset checkPaymentResult = 1>
                                    <cfelse>
                                       <cfset paymentErrMess =  RxMakePaymentWithCC.MESSAGE/>
                                    </cfif>
                                </cfif>
                            </cfif>

                            <cfif checkPaymentResult EQ 0>
                                <cftransaction action="rollback" />
                                <cfif arguments.inpPaymentMethod EQ 1>
                                    <cftry>
                                        <cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
                                            <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
                                            <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
                                            <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
                                            <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail# #arguments.email#">
                                            <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
                                            <cfinvokeargument name="paymentdata" value="#paymentData#">
                                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                        </cfinvoke>
                                        <cfcatch type="any">
                                        </cfcatch>
                                    </cftry>
                                </cfif>

                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-4'>
                                <cfset dataout.MESSAGE = 'Payment Fail.'&paymentErrMess>
                                <cfreturn dataout>
                            </cfif>
                            <!--- end payment process --->
                            <!--- remove API credentials
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.credential" method="insertAccessKey" returnvariable="datadump">
                            <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>
                            --->
                            <!--- set basic permissions for new individual user --->
                            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
                                <cfinvokeargument name="UCID" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="RoleType" value="2">
                                <cfinvokeargument name="Permission" value="#NewUserPermissions#">
                            </cfinvoke>

                            <!--- Setup Billing - Shared accounts will ignore that main value here and use Shared User Id instead but still insert default now --->
                            <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                                INSERT INTO
                                    simplebilling.billing
                                    (
                                        UserId_int,
                                        Balance_int,
                                        UnlimitedBalance_ti,
                                        RateType_int,
                                        Rate1_int,
                                        Rate2_int,
                                        Rate3_int,
                                        Increment1_int,
                                        Increment2_int,
                                        Increment3_int,
                                        PromotionId_int,
                                        PromotionLastVersionId_int,
                                        PromotionCreditBalance_int
                                     )
                                 VALUES
                                     (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#FreePlanCredits#"> ,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUnlimitedCredit#">,
                                        1,
                                        1,
                                        1,
                                        1,
                                        30,
                                        30,
                                        30,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionID#"> ,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionLatestVersion#"> ,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#addPromotionCredit#">
                                        )
                            </cfquery>
                            <!--- add role --->
                            <cfset var roleName = 'User'>

                            <cfquery name="addRole" datasource="#Session.DBSourceEBM#">
                                INSERT INTO
                                    simpleobjects.userroleuseraccountref
                                    (
                                        userAccountId_int,
                                        roleId_int,
                                        modified_dt
                                    )
                                VALUES
                                    (
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">,
                                        (
                                            SELECT
                                                roleId_int
                                            FROM
                                                simpleobjects.userrole
                                            WHERE
                                                roleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#roleName#">
                                        ),
                                        NOW()
                                    )
                            </cfquery>

                            <!--- ADD FREE PLAN --->
                            <cfset var todayDate = Now()>
                            <cfset var startDay = Day(todayDate)>

                            <cfif startDay GT 28>
                                <cfset startDay = 28>
                            </cfif>
                            <cfset var startMonth = Month(todayDate)>
                            <cfset var startYear = Year(todayDate)>

                            <cfset var startDate = CreateDate(startYear, startMonth, startDay)>

                            <cfif arguments.inpIsYearly EQ 0>
                                <cfset var endDate = DateAdd("m",1,startDate)>
                                <!--- <cfset var monthlyAddBenefitDate = ''> --->
                                <cfset var billingType = 1/>
                                <cfinvoke method="insertUsersPlan" component="#LocalSessionDotPath#.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                                    <cfinvokeargument name="inpPlanId" value="#arguments.inpPlanId#">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                    <cfinvokeargument name="inpStarDate" value="#startDate#">
                                    <cfinvokeargument name="inpEndDate" value="#endDate#">
                                    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#endDate#">
                                    <cfinvokeargument name="inpBillingType" value="#billingType#">
                                </cfinvoke>
                            <cfelseif arguments.inpIsYearly EQ 1>
                                <cfset var endDate = DateAdd("yyyy",1,startDate)>
                                <cfset var monthlyAddBenefitDate = DateAdd("m",1,startDate)>
                                <cfset var billingType = 2/>
                                <cfinvoke method="insertUsersPlan" component="#LocalSessionDotPath#.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                                    <cfinvokeargument name="inpPlanId" value="#arguments.inpPlanId#">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                    <cfinvokeargument name="inpStarDate" value="#startDate#">
                                    <cfinvokeargument name="inpEndDate" value="#endDate#">
                                    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#monthlyAddBenefitDate#">
                                    <cfinvokeargument name="inpBillingType" value="#billingType#">
                                </cfinvoke>
                            </cfif>



                            <cfif checkAddPlan.RXRESULTCODE EQ -1>
                                <cftransaction action="rollback" />
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-5'>
                                <cfset dataout.MESSAGE = checkAddPlan.MESSAGE>
                                <cfreturn dataout>
                            </cfif>

                            <cfquery name="updateUsersPlan" datasource="#Session.DBSourceEBM#" result="planUpdate">
                                UPDATE simplebilling.userplans
                                SET
                                    PromotionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionID#"> ,
                                    PromotionLastVersionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionLatestVersion#"> ,
                                    PromotionKeywordsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionKeyword#">,
                                    PromotionMlpsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionMLP#">,
                                    PromotionShortUrlsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionShortURL#">
                                WHERE
                                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetNEXTUSERID.GENERATED_KEY#">
                                AND Status_int = 1
                            </cfquery>

                              <cfif planUpdate.RECORDCOUNT EQ 0>
                                <cftransaction action="rollback" />
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-11'>
                                <cfset dataout.MESSAGE = 'planUpdate fail'>
                                <cfreturn dataout>
                            </cfif>


                            <cfif arguments.inpPlanId GT 1 and totalAMOUNT GT 0>
                                <!--- <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing" returnvariable="updateCustomerResult">
                                    <cfinvokeargument name="payment_method" value="#arguments.payment_method#">
                                    <cfinvokeargument name="line1" value="#arguments.line1#">
                                    <cfinvokeargument name="city" value="#arguments.city#">
                                    <cfinvokeargument name="country" value="#arguments.country#">
                                    <cfinvokeargument name="phone" value="#arguments.phone#">
                                    <cfinvokeargument name="state" value="#arguments.state#">
                                    <cfinvokeargument name="zip" value="#arguments.zip#">
                                    <cfinvokeargument name="email" value="#arguments.email#">
                                    <cfinvokeargument name="cvv" value="#arguments.cvv#">
                                    <cfinvokeargument name="expirationDate" value="#arguments.expirationDate#">
                                    <cfinvokeargument name="firstName" value="#arguments.firstName#">
                                    <cfinvokeargument name="lastName" value="#arguments.lastName#">
                                    <cfinvokeargument name="number" value="#arguments.number#">
                                    <cfinvokeargument name="primaryPaymentMethodId" value="#arguments.primaryPaymentMethodId#">
                                    <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                </cfinvoke>

                                <cfset dataout.updateCustomerResult = updateCustomerResult />



                                <cfif updateCustomerResult.RXRESULTCODE NEQ 1>
                                    <cftransaction action="rollback" />
                                    <cfset dataout.RESULT = 'FAIL'>
                                    <cfset dataout.USERID = '-6'>
                                    <cfset dataout.MESSAGE = updateCustomerResult.MESSAGE>
                                    <cfreturn dataout>
                                </cfif> --->

                                <Cfset inpCardObject['inpNumber'] = arguments.number/>
                                <Cfset inpCardObject['expirationDate'] = arguments.expirationDate/>
                                <Cfset inpCardObject['inpCvv2'] = arguments.cvv/>
                                <Cfset inpCardObject['inpFirstName'] = arguments.firstName/>
                                <Cfset inpCardObject['inpLastName'] = arguments.lastName/>
                                <Cfset inpCardObject['inpLine1'] = arguments.line1/>
                                <Cfset inpCardObject['inpCity'] = arguments.city/>
                                <Cfset inpCardObject['inpState'] = arguments.state/>
                                <Cfset inpCardObject['inpPostalCode'] = arguments.zip/>
                                <Cfset inpCardObject['inpCountryCode'] = arguments.country/>
                                <Cfset inpCardObject['inpEmail'] = arguments.email/>

                                <Cfset inpCardObject['inpExpireMonth'] = LEFT(arguments.expirationDate,2)/>
                                <Cfset inpCardObject['inpExpreYear'] = RIGHT(arguments.expirationDate,4)/>


                                <cfinvoke method="storeCreditCardInVault" component="session.sire.models.cfc.payment.vault" returnvariable="updateCustomerResult">
                                    <cfinvokeargument name="inpCardObject" value="#SerializeJSON(inpCardObject)#">
                                    <cfinvokeargument name="inpPaymentGateway" value="#arguments.inpPaymentMethod#">
                                    <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                </cfinvoke>

                                <cfif updateCustomerResult.RXRESULTCODE NEQ 1>
                                    <cftransaction action="rollback" />
                                    <cfset dataout.RESULT = 'FAIL'>
                                    <cfset dataout.USERID = '-6'>
                                    <cfset dataout.MESSAGE = updateCustomerResult.MESSAGE>
                                    <cfreturn dataout>
                                </cfif>

                                <!--- if payment success send email --->
                                <cfif totalAMOUNT GT 0>
                                    <cfif arguments.inpPaymentMethod EQ 1>
                                        <cfset paymentRespose.filecontent = deserializeJSON(paymentRespose.filecontent)>
                                        <cfset paymentRespose.filecontent.planname = getPlan.PLANNAME>
                                        <cfset paymentRespose.filecontent = serializeJSON(paymentRespose.filecontent)>
                                        <!--- Sendmail payment --->
                                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                            <cfinvokeargument name="to" value="#email#">
                                            <cfinvokeargument name="type" value="html">
                                            <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                                            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
                                            <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
                                        </cfinvoke>

                                    <cfelseif arguments.inpPaymentMethod EQ 2 OR arguments.inpPaymentMethod EQ 3 OR arguments.inpPaymentMethod EQ 4>
                                        <cfset dataout.inpPaymentMethod =  arguments.inpPaymentMethod />
                                        <cfset emailData = {}/>
                                        <cfset emailData['FullName'] = arguments.INPFNAME&" "&arguments.INPLNAME/>
                                        <cfset emailData['numberKeywords'] = 0/>
                                        <cfset emailData['numberSMS'] = 0/>
                                        <cfset emailData['authorizedAmount'] = totalAMOUNT/>
                                        <cfset emailData['transactionId'] = RxMakePaymentWithCC.REPORT.TRANSACTIONID/>
                                        <cfset emailData['planName'] = getPlan.PLANNAME/>

                                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                            <cfinvokeargument name="to" value="#arguments.email#">
                                            <cfinvokeargument name="type" value="html">
                                            <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
                                            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan_pp.cfm">
                                            <cfinvokeargument name="data" value="#serializeJSON(emailData)#">
                                        </cfinvoke>
                                    </cfif>
                                </cfif>
                            </cfif>

                            <cfinvoke method="createUserLog" component="#LocalSessionDotPath#.cfc.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#">
                                <cfinvokeargument name="moduleName" value="Add new Account">
                                <cfinvokeargument name="operator" value="#INPMAINEMAIL#">
                            </cfinvoke>

                            <cftransaction action="commit" />
                            <cfcatch>
                                <cftransaction action="rollback" />
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-7'>
                                <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                                <cfreturn dataout>
                            </cfcatch>
                        </cftry>

                        </cftransaction>

                        <!--- Notify System Admins who monitor EMS  --->
                        <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
                            SELECT
                                ContactAddress_vch
                            FROM
                                simpleobjects.systemalertscontacts
                            WHERE
                                ContactType_int = 2
                            AND
                                EMSNotice_int = 1
                        </cfquery>

                        <cfif GetEMSAdmins.RecordCount GT 0>
                            <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>
                        </cfif>

                        <!--- AUTO ASSIGN SHORTCODE --->
                        <cfinvoke method="AssignSireSharedCSC" component="Session.cfc.csc.csc" returnvariable="RetVarAssignSireSharedCSC">
                            <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            <cfinvokeargument name="inpKeywordLimit" value="1000">
                        </cfinvoke>

                        <cfif arguments.inpReferralCode NEQ '' && arguments.inpReferralType NEQ ''>
                            <cfset variables.isReferral = "yes" />
                            <!--- Insert Free Month use SIRE to the user send invitation to others who signed up successful --->
                            <cfinvoke method="InsertMonthFree" component="session.sire.models.cfc.referral" returnvariable="resultInsertMonthFree">
                                <cfinvokeargument name="inpReferralCode" value="#arguments.inpReferralCode#">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>

                            <!--- Insert Referral log to simpleobjects.sire_referral_log table --->
                            <cfinvoke method="InsertReferralLog" component="session.sire.models.cfc.referral">
                                <cfinvokeargument name="inpReferralType" value="#arguments.inpReferralType#">
                                <cfinvokeargument name="inpReferralCode" value="#arguments.inpReferralCode#">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>

                            <cfinvoke method="UpdateInviterPromotionCredits" component="session.sire.models.cfc.referral" returnvariable="resultUpdateInviter">
                                <cfinvokeargument name="inpUserId" value="#GetNEXTUSERID.GENERATED_KEY#">
                            </cfinvoke>

                            <cfif resultUpdateInviter.RXRESULTCODE GT 0>
                                <!--- Call function getuserinfo by id --->
                                <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
                                    <cfinvokeargument name="inpUserId" value="#resultUpdateInviter.SenderUserId#">
                                </cfinvoke>

                                <cfinvoke  method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getInviterPlanResult">
                                    <cfinvokeargument name="InpUserId" value="#resultUpdateInviter.SenderUserId#">
                                </cfinvoke>

                                <cfif getInviterPlanResult.PLANID GT 1>

                                    <!--- Set inputEmailData data to send email to inviter --->
                                    <cfset inputEmailData.fullName = '#resultUserInfo.USERNAME#' & ' ' & '#resultUserInfo.LASTNAME#'>
                                    <cfset inputEmailData.firstName = '#resultUserInfo.USERNAME#'>
                                    <cfset inputEmailData.emailAddress = '#resultUserInfo.FULLEMAIL#'>
                                    <cfset inputEmailData.subject = '#_ToInviterEmailSubject#'>

                                    <!--- Send Notification Email to sender when invited user signed up successful --->
                                    <cfinvoke method="SendMailNotificationToSender" component="session.sire.models.cfc.referral">
                                        <cfinvokeargument name="inpData" value="#inputEmailData#">
                                    </cfinvoke>
                                </cfif>
                            </cfif>
                        </cfif>

                        <!--- Event trigger sign in --->
                        <cfinvoke method="trigger" component="session.sire.models.cfc.events">
                            <cfinvokeargument name="event" value="signup"/>
                            <cfinvokeargument name="module" value="userstools.RegisterNewAccountNew"/>
                            <cfinvokeargument name="userId" value="#GetNEXTUSERID.GENERATED_KEY#"/>
                        </cfinvoke>

                        <!--- Need change to new event trigger --->
                        <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                            <cfinvokeargument name="InpEmailString" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="InpFirstName" value="#INPFNAME#">
                            <cfinvokeargument name="InpLastName" value="#INPLNAME#">
                            <cfinvokeargument name="InpSignupDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                            <cfinvokeargument name="InpLastLoginDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                            <cfinvokeargument name="InpAccountType" value="#InpAccountTypeText#">
                            <cfinvokeargument name="InpStatus" value="active">
                            <cfinvokeargument name="InpPhoneNumber" value="#INSMSNUMBER#">
                        </cfinvoke>
                        <cfinvoke method="Subscribed" component="public.sire.models.cfc.mailchimp-v3" returnvariable="RetVarmailChimpAPIsV3">
                            <cfinvokeargument name="inpEmailString" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="inpFirstName" value="#INPFNAME#">
                            <cfinvokeargument name="inpLastName" value="#INPLNAME#">
                            <cfinvokeargument name="inpPhone" value="#INSMSNUMBER#">
                            <cfinvokeargument name="inpLast4CC" value="#arguments.number#">
                        </cfinvoke>

                        <!--- jp@ebmui.com; --->
                        <cfset var envServer = CGI />
                        <cfmail to="#EBMAdminEMSList#" subject="EBM User Account Created" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                   <style type="text/css">
                                        h4 {
                                        font-size: 10pt;
                                        font-weight: none;
                                        }

                                    </style>

                                    <body>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                                        <tr>
                                            <td style ="width:40%;"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/logo.jpg"  alt="banner"></td>
                                            <td style ="width:60%;font-size: 10pt; font-weight: nomal; padding: 30px 0px 30px 10px;">Add <u>support@siremobile.com</u> to your contacts.</td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                                        <tr>
                                            <td><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/bot-banner.jpg" width="100%" alt="banner">	  </td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; border-collapse: collapse; width: 25%; align:center;margin:3% 8% 3% 8%;">
                                        <tr style="background-color: ##578DA7;">
                                        <td colspan = "2"><h3 style="text-align: center; color: white;">Sire Account Creation Alert</h3></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>User Name :</b></td>
                                            <td style="padding: 8px;">#INPFNAME# #INPLNAME# </td>
                                        </tr>
                                        <tr style="background-color: ##dedede;">
                                            <td style="padding: 8px;"><b>User Id :</b></td>
                                            <td style="padding: 8px;">#GetNEXTUSERID.GENERATED_KEY# </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>Email :</b></td>
                                            <td style="padding: 8px;">#INPMAINEMAIL#</td>
                                        </tr>
                                        <tr style="background-color: ##dedede;">
                                            <td style="padding: 8px;"><b>Phone :</b></td>
                                            <td style="padding: 8px;">#INSMSNUMBER# </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 8px;"><b>Ip Address :</b></td>
                                            <td style="padding: 8px;">#CGI.REMOTE_ADDR#</td>
                                        </tr>
                                    </table>
                                    </div>
                                    <div>
                                    <table style="font-family: arial, sans-serif; width: 40%;background-color:##dedede;margin:0px;">
                                        <tr>
                                            <td style ="font-size: 10pt; font-weight: nomal; padding: 20px 0px 0px 20px;">For support requests, please call or email us at:</td>
                                        </tr>
                                        <tr>
                                            <td style ="font-size: 10pt; font-weight: nomal; padding: 0px 0px 20px 20px;"><a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>">https://siremobile.com</a>       |   888.335.0909   |   support@siremobile.com</td>
                                        </tr>
                                    </table>
                                    </div>

                                    </body>
                                </cfoutput>
                            </cfmail>
                    <cfset dataMail = {
                        UserName: INPFNAME,
                        LastName: INPLNAME,
                        MobileNumber: INSMSNUMBER,
                        rootUrl:rootUrl
                    }/>

                    <cfif variables.isReferral EQ "yes">
                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                            <cfinvokeargument name="to" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="type" value="html">
                            <cfinvokeargument name="subject" value="Thank you for registering with Sire!">
                            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_referral_registration_completed.cfm">
                            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                        </cfinvoke>
                    <cfelse>
                        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                            <cfinvokeargument name="to" value="#INPMAINEMAIL#">
                            <cfinvokeargument name="type" value="html">
                            <cfinvokeargument name="subject" value="Welcome to Sire!">
                            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_registration_completed.cfm">
                            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                        </cfinvoke>
                    </cfif>

                    <!--- SET COOKIE --->

                    <cfset dataout.USERID = '#GetNEXTUSERID.GENERATED_KEY#'>
                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.MESSAGE = 'You have registered successfully.'>

                    <cfif RegisteredByAgency EQ 0>
                         <!--- CALL TO LOGIN --->
                         <cfinvoke component="public.sire.models.cfc.userstools" method="validateUsers" returnvariable="userLogin">
                             <cfinvokeargument name="inpUserID" value="#INPMAINEMAIL#"/>
                             <cfinvokeargument name="inpPassword" value="#inpNAPassword#"/>
                             <cfinvokeargument name="inpFromSource" value="SignUp"/>
                         </cfinvoke>
                         <cfif userLogin.RXRESULTCODE EQ 1>
                             <!--- ADD 4 custom field --->
                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled1">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[1]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled2">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[2]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled3">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[3]#">
                             </cfinvoke>

                             <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled4">
                                 <cfinvokeargument name="inpDesc" value="#_UserCustomFields[4]#">
                             </cfinvoke>
                         </cfif>
                    </cfif>

                <cfcatch TYPE="any">
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-8'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>

        <cfreturn dataout />
    </cffunction>
    <!---paypal --->
        <!---
        <cffunction name="RegisterNewAccountNewPaypal" access="remote" output="true" hint="Start a new user account (Paypal)">

            <cfargument name="INPNAPASSWORD" required="yes" default="" type="string">
            <cfargument name="INPMAINEMAIL" required="yes" default="" type="string">
            <cfargument name="INSMSNUMBER" required="yes" default="" type="string">
            <cfargument name="INPACCOUNTTYPE" required="yes" default="personal" type="string">
            <cfargument name="INPTIMEZONE" required="yes" default="" type="string">
            <cfargument name="INPFNAME" required="yes" default="" type="string">
            <cfargument name="INPLNAME" required="yes" default="" type="string">
            <cfargument name="INPORGNAME" required="no" default="" type="string">
            <cfargument name="INPCBPID" required="no" default="1" type="string">
            <cfargument name="INPSHAREDRESOURCES" required="no" default="0" type="string">
            <cfargument name="INPCOMPANYID" required="no" default="0" type="string">
            <cfargument name="inpReferralCode" required="no" default="" type="string">
            <cfargument name="inpReferralType" required="no" default="" type="string">
            <cfargument name="inpPromotionCode" required="no" default="" type="string">
            <cfargument name="inpPlanId" required="yes" default="1">
            <cfargument name="inpIsYearly" required="no" default="0">
            <cfargument name="inpCancelUrlNewAcc" required="yes" default="">
            <cfargument name="inpReturnlUrlNewAcc" required="yes" default="">


            <cfargument name="payment_method" required="no" default="">
            <cfargument name="line1" required="no" default="">
            <cfargument name="city" required="no" default="">
            <cfargument name="country" required="no" default="">
            <cfargument name="phone" required="no" default="">
            <cfargument name="state" required="no" default="">
            <cfargument name="zip" required="no" default="">
            <cfargument name="email" required="no" default="">
            <cfargument name="cvv" required="no" default="">
            <cfargument name="expirationDate" required="no" default="">
            <cfargument name="firstName" required="no" default="">
            <cfargument name="lastName" required="no" default="">
            <cfargument name="number" required="no" default="">
            <cfargument name="primaryPaymentMethodId" required="no" default="1">

            <cfset var dataout = {} />

            <cfset dataout.USERID = '-1'>

            <cfset var sameIpLimit = 25 />
            <cfset var hoursLimit = 2 />
            <cfset var companyUserId = 1 />
            <cfset var NewUserPermissions = ArrayToList(BasicPermission, ",") />
            <cfset var getCurrentUser = StructNew() />
            <cfset var EBMAdminEMSList = "" />
            <cfset var GetEMSAdmins = ""/>
            <cfset var RetVarsetPermission = "" />
            <cfset var VerifyUnique = '' />
            <cfset var checkLimit   = '' />
            <cfset var AddUser  = '' />
            <cfset var AddOrginazation  = '' />
            <cfset var AddBilling   = '' />
            <cfset var addRole  = '' />
            <cfset var safeeMail    = '' />
            <cfset var safePass = '' />
            <cfset var datadump = '' />
            <cfset var checkAddPlan = '' />
            <cfset var RetVarAssignSireSharedCSC    = '' />
            <cfset var userLogin    = '' />
            <cfset var RetVarAddNewCustomFiled1 = '' />
            <cfset var RetVarAddNewCustomFiled2 = '' />
            <cfset var RetVarAddNewCustomFiled3 = '' />
            <cfset var RetVarAddNewCustomFiled4 = '' />
            <cfset var GetNEXTUSERID    = '' />
            <cfset var GetNEXTORGID = '' />
            <cfset var FreePlanCredits = 25/>
            <cfset var dataMail = '' />
            <cfset var PlanCredits = '' />
            <!--- <cfset var inputEmailData ={}/> --->
            <cfset var resultInsertMonthFree = structNew() />
            <cfset var resultUserInfo = structNew() />

            <cfset getCurrentUser.USERROLE = "blank" />
            <cfset var USERCOMPANYID = arguments.INPCOMPANYID>

            <cfset var promocode = {}>
            <cfset promocode.pcok = 0>
            <cfset var newUserPromoCode = {}>
            <cfset var addPromotionKeyword = 0/>
            <cfset var addPromotionMLP = 0/>
            <cfset var addPromotionShortURL = 0/>
            <cfset var addPromotionCredit = 0/>
            <cfset var totalAMOUNT = 0/>
            <cfset var addPromotionID = 0/>
            <cfset var addPromotionLatestVersion = 0/>
            <cfset var checkPaymentResult = '' />
            <cfset var txtOrderPlan = '' />
            <cfset var paymentData = '' />
            <cfset var closeBatchesData = '' />
            <cfset var paymentResposeContent = '' />
            <cfset var purchaseType = '' />
            <cfset var purchaseAmount = '' />
            <cfset var creditsPurchased = '' />
            <cfset var paymentRespose = '' />
            <cfset var getPromotion = '' />
            <cfset var updateUsersPlan = '' />
            <cfset var getPlan = '' />
            <cfset var updateCustomerResult = '' />
            <cfset var RetVarmailChimpAPIs = '' />
            <cfset var closeBatchesRequestResult = '' />
            <cfset var planUpdate = '' />
            <cfset var updatePromoCode = ''/>
            <cfset var resultUpdateInviter = ''/>
            <cfset var resultUserInfo = ''/>
            <cfset var getInviterPlanResult = ''/>
            <cfset var inputEmailData = {} />
            <cfset var VerifySMS_Number = ''/>
            <cfset var DuplicateSMS_Number = ''/>
            <cfset var WhiteListPhoneNumber = ''/>

            <cfset var PaymentDescriptionNewAcc = ''/>
            <cfset var SetPPExpressCheckoutResult	= '' />
                <cftry>

                        <cfset var INPNAPASSWORD = TRIM(arguments.INPNAPASSWORD)>
                        <cfset var INPMAINEMAIL = TRIM(arguments.INPMAINEMAIL)>
                        <cfset var INSMSNUMBER = TRIM(arguments.INSMSNUMBER)>

                        <!--- Validate email address --->
                        <cfinvoke method="VldEmailAddress" returnvariable="safeeMail">
                            <cfinvokeargument name="Input" value="#INPMAINEMAIL#"/>
                        </cfinvoke>
                        <cfif safeeMail NEQ true OR INPMAINEMAIL EQ "">
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'Invalid Email.'>
                            <cfreturn dataout>
                        </cfif>

                        <!--- Validate Email Unique --->
                        <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                            SELECT
                                EmailAddress_vch
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">
                            AND
                                CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">
                            AND
                                Active_int > 0
                        </cfquery>
                        <cfif VerifyUnique.RecordCount GT 0>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
                            <cfreturn dataout>
                        </cfif>

                        <!--- Valid US Phone Number --->
                        <cfif NOT isvalid('telephone', INSMSNUMBER)>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number !'>
                            <cfreturn dataout>
                        </cfif>

                        <cfquery name="VerifySMS_Number" datasource="#Session.DBSourceREAD#">
                            SELECT
                                IdPhone
                            FROM
                                simpleobjects.phone_number_blocked
                            WHERE
                                PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                        </cfquery>
                        <cfif VerifySMS_Number.RecordCount GT 0>
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'We were unable to send the verification code to the phone number you entered.<br>Please try the following:<br>*  Make sure the number was entered correctly.<br>*  Ask your mobile phone carrier to enable messages from short codes<br>*  Use a different number with your account<br>Contact Customer Support for further assistance.'>
                            <cfreturn dataout>
                        </cfif>

                        <!--- Duplicate Phone Number & White Phone Number List --->
                        <cfquery name="DuplicateSMS_Number" datasource="#Session.DBSourceREAD#">
                            SELECT
                                UserId_int
                            FROM
                                simpleobjects.useraccount
                            WHERE
                                MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                        </cfquery>
                        <cfif DuplicateSMS_Number.RECORDCOUNT GT 0>
                            <cfquery name="WhiteListPhoneNumber" datasource="#Session.DBSourceREAD#">
                                SELECT
                                    IdPhone
                                FROM
                                    simpleobjects.whitelistphonenumber
                                WHERE
                                    PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                            </cfquery>
                            <cfif WhiteListPhoneNumber.RECORDCOUNT lt 1>
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = 'The phone number you entered is already in use and cannot be used at this time. Please try another.'>
                                <cfreturn dataout>
                            </cfif>
                        </cfif>
                        <!--- Validate proper password--->
                        <cfinvoke method="VldInput" returnvariable="safePass">
                            <cfinvokeargument name="Input" value="#INPNAPASSWORD#"/>
                        </cfinvoke>

                        <cfif safePass NEQ true OR INPNAPASSWORD EQ "">
                            <cfset dataout.RESULT = 'FAIL'>
                            <cfset dataout.USERID = '-1'>
                            <cfset dataout.MESSAGE = 'Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-'>
                            <cfreturn dataout>
                        </cfif>

                        <cfquery name="PlanCredits" datasource="#Session.DBSourceREAD#">
                            SELECT
                                FirstSMSIncluded_int
                            FROM
                                simplebilling.plans
                            WHERE
                                PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpPlanId#">
                        </cfquery>
                        <cfif PlanCredits.RecordCount GT 0>
                            <cfset FreePlanCredits = PlanCredits.FirstSMSIncluded_int/>
                        </cfif>

                        <cfinvoke method="GetOrderPlan" component="session/sire/models/cfc/order_plan" returnvariable="getPlan">
                            <cfinvokeargument name="plan" value="#arguments.inpPlanId#"/>
                        </cfinvoke>
                        <cfif arguments.inpIsYearly EQ 0>
                            <cfset totalAMOUNT = getPlan.AMOUNT/>
                        <cfelseif arguments.inpIsYearly EQ 1>
                            <cfset totalAMOUNT = getPlan.YEARLYAMOUNT*12/>
                        </cfif>
                        <!--- check and get info PromoCode --->
                        <cfif arguments.inpPromotionCode NEQ ''>
                            <cfinvoke component="session.sire.models.cfc.promotion" method="getActivePromoCode" returnvariable="promocode">
                                <cfinvokeargument name="inpCode" value="#arguments.inpPromotionCode#">
                                <cfinvokeargument name="inpIsYearlyPlan" value="#arguments.inpIsYearly#">
                            </cfinvoke>
                            <cfif promocode.RXRESULTCODE EQ 1>
                                <cfif promocode.promotion_id GT 0>
                                    <cfset promocode.pcok = 1>

                                    <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
                                        SELECT
                                        pc.promotion_id,
                                        pc.origin_id,
                                        pc.discount_type_group,
                                        pc.discount_plan_price_percent,
                                        pc.discount_plan_price_flat_rate,
                                        pc.promotion_keyword,
                                        pc.promotion_MLP,
                                        pc.promotion_short_URL,
                                        pc.promotion_credit

                                        FROM simplebilling.promotion_codes pc
                                        WHERE
                                            pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#promocode.promotion_id#">
                                            AND pc.promotion_status  = 1
                                        LIMIT 1
                                    </cfquery>

                                    <cfif getPromotion.RecordCount GT 0>
                                    <cfset addPromotionID = "#getPromotion.origin_id#">
                                    <cfset addPromotionLatestVersion = "#getPromotion.promotion_id#">

                                        <cfif getPromotion.discount_type_group EQ 1>
                                            <cfif arguments.inpIsYearly EQ 0>
                                                <cfset totalAMOUNT = (getPlan.AMOUNT - (getPlan.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
                                            <cfelseif arguments.inpIsYearly EQ 1>
                                                <cfset totalAMOUNT = (getPlan.YEARLYAMOUNT*12 - (getPlan.YEARLYAMOUNT*12 * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
                                            </cfif>
                                            <!--- <cfset totalAMOUNT = (getPlan.AMOUNT - (getPlan.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)> --->
                                        <cfelseif getPromotion.discount_type_group EQ 2>
                                                <cfset addPromotionKeyword = "#getPromotion.promotion_keyword#">
                                                <cfset addPromotionMLP = "#getPromotion.promotion_MLP#">
                                                <cfset addPromotionShortURL = "#getPromotion.promotion_short_URL#">
                                                <cfset addPromotionCredit = "#getPromotion.promotion_credit#">
                                        </cfif>
                                    </cfif>
                                <!--- Move to UpdateNewAccountPayPal --->
                                <cfelse>
                                    <cfset dataout.RESULT = 'FAIL'>
                                    <cfset dataout.USERID = '-1'>
                                    <cfset dataout.MESSAGE = 'Invalid Promotion Code.'>
                                    <cfreturn dataout>
                                </cfif>
                            <cfelse>
                                <cfset dataout.RESULT = 'FAIL'>
                                <cfset dataout.USERID = '-1'>
                                <cfset dataout.MESSAGE = promocode.message>
                                <cfreturn dataout>
                            </cfif>
                        </cfif>
                        <!--- begin payment process --->
                        <cfset checkPaymentResult = ''>
                        <!--- Add record --->
                            <!--- Payment process --->
                            <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="GetNEXTUSERID">
                                INSERT INTO
                                    simpleobjects.useraccount
                                    (
                                        Password_vch,
                                        EmailAddress_vch,
                                        Created_dt,
                                        UserIp_vch,
                                        EmailAddressVerified_vch,
                                        CompanyAccountId_int,
                                        CBPId_int,
                                        firstName_vch,
                                        lastName_vch,
                                        timezone_vch,
                                        Active_int,
                                        CompanyUserId_int,
                                        HomePhoneStr_vch,
                                        MFAContactString_vch,
                                        MFAContactType_ti
                                    )
                                VALUES
                                    (
                                        AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">,
                                        NOW(),
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="-3">,<!--- Account pending, update =1 if charge successfully --->
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                                    )
                            </cfquery>
                            <cfset PaymentDescriptionNewAcc = "Account Name: " & #INPFNAME# & " " & INPLNAME & "; Plan: " & #getPlan.PLANNAME#/>
                            <!--- Move to UpdateNewAccountNewPaypal ---->
                                <!--- 1 --->
                            <!--- End Move to UpdateNewAccountNewPaypal ---->
                            <cfif totalAMOUNT GT 0 >
                                <cfinvoke component="session.sire.models.cfc.billing" method="SetPPExpressCheckout" returnvariable="SetPPExpressCheckoutResult">
                                    <cfinvokeargument name="inpPaymentDescription" value="#PaymentDescriptionNewAcc#">
                                    <cfinvokeargument name="inpAmount" value="#totalAMOUNT#">
                                    <cfinvokeargument name="inpCancelUrl" value="#arguments.inpCancelUrlNewAcc#">
                                    <cfinvokeargument name="inpReturnlUrl" value="#arguments.inpReturnlUrlNewAcc#&userId=#GetNEXTUSERID.GENERATED_KEY#">
                                </cfinvoke>

                                <!--- Charge --->
                                <!---SetPPExpressCheckout--->

                            </cfif>
                            <!--- Move to UpdateNewAccountNewPaypal --->
                                <!--- 2 --->
                            <!--- End Move to UpdateNewAccountNewPaypal --->
                    <cfcatch TYPE="any">
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                        <cfreturn dataout>
                    </cfcatch>
                </cftry>
            <cfset dataout.RESULT = #SetPPExpressCheckoutResult.RESULT#>
            <cfset dataout.USERID = 1/>
            <cfreturn dataout />
        </cffunction>

        <cffunction name="UpdateNewAccountNewPaypal" access="remote" output="true" hint="Update info account if charge successfully (Paypal)">
            <cfargument name="inpuserId" required="yes"  default="0"/>
            <cfargument name="inpplanId" required="yes"  default="0"/>
            <cfargument name="inppromotioncode" required="no"  default="0"/>
            <cfargument name="inpisyearlyplan" required="no"  default="0"/>
            <cfargument name="inpemail" required="no"  default="0"/>
            <cfargument name="inppassword" required="no"  default="0"/>
            <cfargument name="inpreferralcode" required="no"  default="0"/>
            <cfargument name="inpreferraltype" required="no"  default="0"/>

            <cfset var dataout = {}>
            <cfset dataout.RXRESULTCODE = 0 />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset dataout.ERRCODE = "" />
            <cfset dataout.RESULT = "" />

            <cfset var addPromotionID = "0"/>
            <cfset var addPromotionLatestVersion = "0"/>
            <cfset var addPromotionKeyword = "0"/>
            <cfset var addPromotionMLP = "0"/>
            <cfset var addPromotionShortURL = "0"/>
            <cfset var addPromotionCredit = "0"/>
            <cfset var FreePlanCredits = "0"/>

            <cfset var PlanCredits = ""/>
            <cfset var UserInfoResult = ""/>
            <cfset var promocode = ""/>
            <cfset var newUserPromoCode = ""/>
            <cfset var AddBilling = ""/>
            <cfset var addRole = ""/>
            <cfset var checkAddPlan = ""/>
            <cfset var planUpdate = ""/>
            <cfset var GetEMSAdmins = ""/>
            <cfset var RetVarAssignSireSharedCSC = ""/>
            <cfset var resultInsertMonthFree = ""/>
            <cfset var resultUpdateInviter = ""/>
            <cfset var resultUserInfo = ""/>
            <cfset var RetVarmailChimpAPIs = ""/>

            <cfset var inputEmailData	= '' />
            <cfset var dataMail	= '' />
            <cfset var getPromotion	= '' />
            <cfset var updatePromoCode	= '' />
            <cfset var updateUsersPlan	= '' />
            <cfset var getInviterPlanResult	= '' />
            <cfset var userLogin	= '' />
            <cfset var RetVarAddNewCustomFiled1	= '' />
            <cfset var RetVarAddNewCustomFiled2	= '' />
            <cfset var RetVarAddNewCustomFiled3	= '' />
            <cfset var RetVarAddNewCustomFiled4	= '' />

            <cfset var NewUserPermissions = ArrayToList(BasicPermission, ",") />
            <!--- 1.get plan amount --->
            <cfquery name="PlanCredits" datasource="#Session.DBSourceREAD#">
                SELECT
                    FirstSMSIncluded_int
                FROM
                    simplebilling.plans
                WHERE
                    PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpplanId#">
            </cfquery>
            <cfif PlanCredits.RecordCount GT 0>
                <cfset FreePlanCredits = PlanCredits.FirstSMSIncluded_int/>
            </cfif>

            <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="UserInfoResult">
                <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
            </cfinvoke>
            <!--- check and get info PromoCode --->
            <cfif arguments.inppromotioncode NEQ ''>
                <cfinvoke component="session.sire.models.cfc.promotion" method="getActivePromoCode" returnvariable="promocode">
                    <cfinvokeargument name="inpCode" value="#arguments.inppromotioncode#">
                    <cfinvokeargument name="inpIsYearlyPlan" value="#arguments.inpisyearlyplan#">
                </cfinvoke>
                <cfif promocode.RXRESULTCODE EQ 1>
                    <cfif promocode.promotion_id GT 0>
                        <cfset promocode.pcok = 1>
                        <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
                            SELECT
                                pc.promotion_id,
                                pc.origin_id,
                                pc.discount_type_group,
                                pc.discount_plan_price_percent,
                                pc.discount_plan_price_flat_rate,
                                pc.promotion_keyword,
                                pc.promotion_MLP,
                                pc.promotion_short_URL,
                                pc.promotion_credit
                            FROM simplebilling.promotion_codes pc
                            WHERE
                                pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#promocode.promotion_id#">
                                AND pc.promotion_status  = 1
                            LIMIT 1
                        </cfquery>
                        <cfif getPromotion.RecordCount GT 0>
                            <cfset addPromotionID = "#getPromotion.origin_id#">
                            <cfset addPromotionLatestVersion = "#getPromotion.promotion_id#">
                            <cfif getPromotion.discount_type_group EQ 2>
                                <cfset addPromotionKeyword = "#getPromotion.promotion_keyword#">
                                <cfset addPromotionMLP = "#getPromotion.promotion_MLP#">
                                <cfset addPromotionShortURL = "#getPromotion.promotion_short_URL#">
                                <cfset addPromotionCredit = "#getPromotion.promotion_credit#">
                            </cfif>
                        </cfif>
                    <!--- Move to UpdateNewAccountPayPal --->
                    <cfelse>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.RXRESULTCODE = '-1'>
                        <cfset dataout.MESSAGE = 'Invalid Promotion Code.'>
                        <cfreturn dataout>
                    </cfif>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.RXRESULTCODE = '-1'>
                    <cfset dataout.MESSAGE = promocode.message>
                    <cfreturn dataout>
                </cfif>
                <!--- Down Redemption count --->
                <cfset var now = Now()>
                <cfquery name="updatePromoCode" datasource="#Session.DBSourceEBM#">
                    UPDATE simplebilling.promotion_codes
                    SET redemption_count_int = (redemption_count_int - 1)
                    WHERE
                        redemption_count_int > 0
                        AND max_redemption_int > 0
                        AND promotion_status = 1
                        AND date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">
                        AND IFNULL(expiration_date, date(start_date) <=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#now#">)
                        AND promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#promocode.promotion_id#">
                </cfquery>
                <!--- END Down Redemption count --->
                <cfif promocode.pcok EQ 1>
                    <cfinvoke component="session.sire.models.cfc.promotion" method="insertUserPromoCode" returnvariable="newUserPromoCode">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                        <cfinvokeargument name="inpPromoId" value="#promocode.promotion_id#">
                        <cfinvokeargument name="inpPromoOriginId" value="#promocode.origin_id#">
                        <cfinvokeargument name="inpUsedDate" value="#Now()#">
                        <cfinvokeargument name="inpRecurringTime" value="#promocode.recurring_time#">
                        <cfinvokeargument name="inpPromotionStatus" value="1">
                    </cfinvoke>

                    <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
                        <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                        <cfinvokeargument name="inpUsedFor" value="1">
                    </cfinvoke>

                    <!--- Increase coupon code used time --->
                    <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                        <cfinvokeargument name="inpCouponId" value="#promocode.origin_id#"/>
                        <cfinvokeargument name="inpUsedFor" value="1"/>
                    </cfinvoke>
                    <!--- End increase coupon code used time --->

                    <!--- Insert coupon code used log --->
                    <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                        <cfinvokeargument name="inpCouponId" value="#promocode.promotion_id#"/>
                        <cfinvokeargument name="inpOriginCouponId" value="#promocode.origin_id#"/>
                        <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#"/>
                        <cfinvokeargument name="inpUsedFor" value="Sign up and use coupon"/>
                    </cfinvoke>
                    <!--- End insert log --->

                    <cfif newUserPromoCode.RXRESULTCODE NEQ 1>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.RXRESULTCODE = '-1'>
                        <cfset dataout.MESSAGE = newUserPromoCode.MESSAGE>
                        <cfreturn dataout>
                    </cfif>
                </cfif>
            </cfif>
            <!--- 2 Update Plan, Promo, ... --->
            <cftry>
            <!--- set basic permissions for new individual user --->
            <cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="setPermission">
                <cfinvokeargument name="UCID" value="#arguments.inpuserId#">
                <cfinvokeargument name="RoleType" value="2">
                <cfinvokeargument name="Permission" value="#NewUserPermissions#">
            </cfinvoke>

            <!--- Setup Billing - Shared accounts will ignore that main value here and use Shared User Id instead but still insert default now --->
            <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                    simplebilling.billing
                    (
                        UserId_int,
                        Balance_int,
                        UnlimitedBalance_ti,
                        RateType_int,
                        Rate1_int,
                        Rate2_int,
                        Rate3_int,
                        Increment1_int,
                        Increment2_int,
                        Increment3_int,
                        PromotionId_int,
                        PromotionLastVersionId_int,
                        PromotionCreditBalance_int
                        )
                    VALUES
                        (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpuserId#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#FreePlanCredits#"> ,
                        0,
                        1,
                        1,
                        1,
                        1,
                        30,
                        30,
                        30,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionID#"> ,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionLatestVersion#"> ,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#addPromotionCredit#">
                        )
            </cfquery>

            <!--- add role --->
            <cfset var roleName = 'User'>
            <cfquery name="addRole" datasource="#Session.DBSourceEBM#">
                INSERT INTO
                    simpleobjects.userroleuseraccountref
                    (
                        userAccountId_int,
                        roleId_int,
                        modified_dt
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpuserId#">,
                        (
                            SELECT
                                roleId_int
                            FROM
                                simpleobjects.userrole
                            WHERE
                                roleName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#roleName#">
                        ),
                        NOW()
                    )
            </cfquery>

            <!--- ADD FREE PLAN --->
            <cfset var todayDate = Now()>
            <cfset var startDay = Day(todayDate)>
            <cfif startDay GT 28>
                <cfset startDay = 28>
            </cfif>
            <cfset var startMonth = Month(todayDate)>
            <cfset var startYear = Year(todayDate)>
            <cfset var startDate = CreateDate(startYear, startMonth, startDay)>
            <cfif arguments.inpisyearlyplan EQ 0>
                <cfset var endDate = DateAdd("m",1,startDate)>
                <!--- <cfset var monthlyAddBenefitDate = ''> --->
                <cfset var billingType = 1/>
                <cfinvoke method="insertUsersPlan" component="#LocalSessionDotPath#.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                    <cfinvokeargument name="inpPlanId" value="#arguments.inpplanId#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                    <cfinvokeargument name="inpStarDate" value="#startDate#">
                    <cfinvokeargument name="inpEndDate" value="#endDate#">
                    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#endDate#">
                    <cfinvokeargument name="inpBillingType" value="#billingType#">
                </cfinvoke>
            <cfelseif arguments.inpisyearlyplan EQ 1>
                <cfset var endDate = DateAdd("yyyy",1,startDate)>
                <cfset var monthlyAddBenefitDate = DateAdd("m",1,startDate)>
                <cfset var billingType = 2/>
                <cfinvoke method="insertUsersPlan" component="#LocalSessionDotPath#.sire.models.cfc.order_plan" returnvariable="checkAddPlan">
                    <cfinvokeargument name="inpPlanId" value="#arguments.inpplanId#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                    <cfinvokeargument name="inpStarDate" value="#startDate#">
                    <cfinvokeargument name="inpEndDate" value="#endDate#">
                    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#monthlyAddBenefitDate#">
                    <cfinvokeargument name="inpBillingType" value="#billingType#">
                </cfinvoke>
            </cfif>

            <cfif checkAddPlan.RXRESULTCODE EQ -1>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.RXRESULTCODE = '-1'>
                <cfset dataout.MESSAGE = checkAddPlan.MESSAGE>
                <cfreturn dataout>
            </cfif>

            <cfquery name="updateUsersPlan" datasource="#Session.DBSourceEBM#" result="planUpdate">
                UPDATE simplebilling.userplans
                SET
                    PromotionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionID#"> ,
                    PromotionLastVersionId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionLatestVersion#"> ,
                    PromotionKeywordsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionKeyword#">,
                    PromotionMlpsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionMLP#">,
                    PromotionShortUrlsLimitNumber_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#addPromotionShortURL#">
                WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpuserId#">
                AND Status_int = 1
            </cfquery>

                <cfif planUpdate.RECORDCOUNT EQ 0>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.RXRESULTCODE = '-11'>
                <cfset dataout.MESSAGE = 'planUpdate fail'>
                <cfreturn dataout>
            </cfif>


            <cfinvoke method="createUserLog" component="#LocalSessionDotPath#.cfc.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#arguments.inpuserId#">
                <cfinvokeargument name="moduleName" value="Add new Account">
                <cfinvokeargument name="operator" value="#arguments.inpemail#">
            </cfinvoke>
            <cfcatch>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.RXRESULTCODE = '-1'>
                <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                <cfreturn dataout>
            </cfcatch>
            </cftry>

            <!--- Notify System Admins who monitor EMS  --->
            <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceEBM#">
            SELECT
                ContactAddress_vch
            FROM
                simpleobjects.systemalertscontacts
            WHERE
                ContactType_int = 2
            AND
                EMSNotice_int = 1
            </cfquery>

            <cfif GetEMSAdmins.RecordCount GT 0>
                <cfset var EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>
            </cfif>

            <!--- AUTO ASSIGN SHORTCODE --->
            <cfinvoke method="AssignSireSharedCSC" component="Session.cfc.csc.csc" returnvariable="RetVarAssignSireSharedCSC">
                <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                <cfinvokeargument name="inpKeywordLimit" value="1000">
            </cfinvoke>

            <cfif arguments.inpReferralCode NEQ '' && arguments.inpReferralType NEQ ''>
                <cfset variables.isReferral = "yes" />
                <!--- Insert Free Month use SIRE to the user send invitation to others who signed up successful --->
                <cfinvoke method="InsertMonthFree" component="session.sire.models.cfc.referral" returnvariable="resultInsertMonthFree">
                    <cfinvokeargument name="inpReferralCode" value="#arguments.inpreferralcode#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                </cfinvoke>

                <!--- Insert Referral log to simpleobjects.sire_referral_log table --->
                <cfinvoke method="InsertReferralLog" component="session.sire.models.cfc.referral">
                    <cfinvokeargument name="inpReferralType" value="#arguments.inpreferraltype#">
                    <cfinvokeargument name="inpReferralCode" value="#arguments.inpreferralcode#">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                </cfinvoke>

                <cfinvoke method="UpdateInviterPromotionCredits" component="session.sire.models.cfc.referral" returnvariable="resultUpdateInviter">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpuserId#">
                </cfinvoke>

                <cfif resultUpdateInviter.RXRESULTCODE GT 0>
                    <!--- Call function getuserinfo by id --->
                    <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
                        <cfinvokeargument name="inpUserId" value="#resultUpdateInviter.SenderUserId#">
                    </cfinvoke>

                    <cfinvoke  method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getInviterPlanResult">
                        <cfinvokeargument name="InpUserId" value="#resultUpdateInviter.SenderUserId#">
                    </cfinvoke>

                    <cfif getInviterPlanResult.PLANID GT 1>

                        <!--- Set inputEmailData data to send email to inviter --->
                        <cfset inputEmailData.fullName = '#resultUserInfo.USERNAME#' & ' ' & '#resultUserInfo.LASTNAME#'>
                        <cfset inputEmailData.firstName = '#resultUserInfo.USERNAME#'>
                        <cfset inputEmailData.emailAddress = '#resultUserInfo.FULLEMAIL#'>
                        <cfset inputEmailData.subject = '#_ToInviterEmailSubject#'>

                        <!--- Send Notification Email to sender when invited user signed up successful --->
                        <cfinvoke method="SendMailNotificationToSender" component="session.sire.models.cfc.referral">
                            <cfinvokeargument name="inpData" value="#inputEmailData#">
                        </cfinvoke>
                    </cfif>
                </cfif>
            </cfif>

            <!--- Event trigger sign in --->
            <cfinvoke method="trigger" component="session.sire.models.cfc.events">
                <cfinvokeargument name="event" value="signup"/>
                <cfinvokeargument name="module" value="userstools.RegisterNewAccountNew"/>
                <cfinvokeargument name="userId" value="#arguments.inpuserId#"/>
            </cfinvoke>

            <!--- Need change to new event trigger --->
            <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                <cfinvokeargument name="InpEmailString" value="#UserInfoResult.FULLEMAIL#">
                <cfinvokeargument name="InpFirstName" value="#UserInfoResult.USERNAME#">
                <cfinvokeargument name="InpLastName" value="#UserInfoResult.LASTNAME#">
                <cfinvokeargument name="InpSignupDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                <cfinvokeargument name="InpLastLoginDate" value="#dateFormat(now(), 'mm/dd/yy')#">
                <cfinvokeargument name="InpAccountType" value="Free">
                <cfinvokeargument name="InpStatus" value="active">
            </cfinvoke>

            <!--- jp@ebmui.com; --->
            <cfset var envServer = CGI />
            <cfmail to="#EBMAdminEMSList#" subject="EBM User Account Created" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                <cfoutput>
                    <style type="text/css">
                        h4 {
                        font-size: 10pt;
                        font-weight: none;
                        }

                    </style>

                    <body>
                    <div>
                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                        <tr>
                            <td style ="width:40%;"><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/logo.jpg"  alt="banner"></td>
                            <td style ="width:60%;font-size: 10pt; font-weight: nomal; padding: 30px 0px 30px 10px;">Add <u>support@siremobile.com</u> to your contacts.</td>
                        </tr>
                    </table>
                    </div>
                    <div>
                    <table style="font-family: arial, sans-serif; width: 40%;margin:0px;">
                        <tr>
                            <td><img src="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>/public/sire/images/emailtemplate/bot-banner.jpg" width="100%" alt="banner">	  </td>
                        </tr>
                    </table>
                    </div>
                    <div>
                    <table style="font-family: arial, sans-serif; border-collapse: collapse; width: 25%; align:center;margin:3% 8% 3% 8%;">
                        <tr style="background-color: ##578DA7;">
                        <td colspan = "2"><h3 style="text-align: center; color: white;">Sire Account Creation Alert</h3></td>
                        </tr>
                        <tr>
                            <td style="padding: 8px;"><b>User Name :</b></td>
                            <td style="padding: 8px;">#UserInfoResult.USERNAME# #UserInfoResult.LASTNAME# </td>
                        </tr>
                        <tr style="background-color: ##dedede;">
                            <td style="padding: 8px;"><b>User Id :</b></td>
                            <td style="padding: 8px;">#arguments.inpuserId# </td>
                        </tr>
                        <tr>
                            <td style="padding: 8px;"><b>Email :</b></td>
                            <td style="padding: 8px;">#arguments.inpemail#</td>
                        </tr>
                        <tr style="background-color: ##dedede;">
                            <td style="padding: 8px;"><b>Phone :</b></td>
                            <td style="padding: 8px;">#UserInfoResult.FULLMFAPHONE# </td>
                        </tr>
                        <tr>
                            <td style="padding: 8px;"><b>Ip Address :</b></td>
                            <td style="padding: 8px;">#CGI.REMOTE_ADDR#</td>
                        </tr>
                    </table>
                    </div>
                    <div>
                    <table style="font-family: arial, sans-serif; width: 40%;background-color:##dedede;margin:0px;">
                        <tr>
                            <td style ="font-size: 10pt; font-weight: nomal; padding: 20px 0px 0px 20px;">For support requests, please call or email us at:</td>
                        </tr>
                        <tr>
                            <td style ="font-size: 10pt; font-weight: nomal; padding: 0px 0px 20px 20px;"><a href="https://<cfoutput>#envServer.HTTP_HOST#</cfoutput>">https://siremobile.com</a>       |   888.335.0909   |   support@siremobile.com</td>
                        </tr>
                    </table>
                    </div>

                    </body>
                </cfoutput>
            </cfmail>
            <cfset dataMail = {
                UserName: UserInfoResult.USERNAME,
                LastName: UserInfoResult.LASTNAME,
                MobileNumber: UserInfoResult.FULLMFAPHONE,
                rootUrl:rootUrl
            }/>

            <cfif variables.isReferral EQ "yes">
                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                    <cfinvokeargument name="to" value="#arguments.inpemail#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Thank you for registering with Sire!">
                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_referral_registration_completed.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                </cfinvoke>
            <cfelse>
                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                    <cfinvokeargument name="to" value="#arguments.inpemail#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="Welcome to Sire!">
                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_registration_completed.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                </cfinvoke>
            </cfif>

            <!--- SET COOKIE --->
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.USERID = '#arguments.inpuserId#'>
            <cfset dataout.RESULT = 'SUCCESS'>
            <cfset dataout.MESSAGE = 'You have registered successfully.'>

            <!--- CALL TO LOGIN --->
            <cfinvoke component="public.sire.models.cfc.userstools" method="validateUsers" returnvariable="userLogin">
                <cfinvokeargument name="inpUserID" value="#arguments.inpemail#"/>
                <cfinvokeargument name="inpPassword" value="#arguments.inppassword#"/>
                <cfinvokeargument name="inpFromSource" value="SignUp"/>
            </cfinvoke>
            <cfif userLogin.RXRESULTCODE EQ 1>
                <!--- ADD 4 custom field --->
                <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled1">
                    <cfinvokeargument name="inpDesc" value="#_UserCustomFields[1]#">
                </cfinvoke>

                <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled2">
                    <cfinvokeargument name="inpDesc" value="#_UserCustomFields[2]#">
                </cfinvoke>

                <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled3">
                    <cfinvokeargument name="inpDesc" value="#_UserCustomFields[3]#">
                </cfinvoke>

                <cfinvoke method="addcustomfieldname" component="#LocalSessionDotPath#.cfc.contacts.customdata" returnvariable="RetVarAddNewCustomFiled4">
                    <cfinvokeargument name="inpDesc" value="#_UserCustomFields[4]#">
                </cfinvoke>
            </cfif>
            <cfreturn dataout />
        </cffunction>

        <cffunction name="DeleteNewAccountNewPaypal" access="remote" output="true" hint="Delete info account if charge fail (Paypal)">
            <cfargument name="inpUserId" required="yes" default="" type="number">

            <cfset var dataout	= {} />
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.RESULT = ""/>
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset var GetAccountPaymentFail = ''/>
            <cfset var DeleteNewAccountPayPal	= '' />
            <cfset var UpdateToPlanFree	= '' />
            <cfset var DeleteBillingAccount	= '' />
            <cftry>
                <cfquery name="GetAccountPaymentFail" datasource="#Session.DBSourceEBM#">
                    Select
                        UserId_int
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    AND
                        Active_int = -3
                </cfquery>
                <cfif GetAccountPaymentFail.RECORDCOUNT GT 0>
                    <cfquery name="DeleteNewAccountPayPal" datasource="#Session.DBSourceEBM#">
                        DELETE FROM simpleobjects.useraccount
                        WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAccountPaymentFail.UserId_int#">
                        AND Active_int = -3
                    </cfquery>
                    <cfquery name="UpdateToPlanFree" datasource="#Session.DBSourceEBM#">
                        UPDATE simplebilling.userplans
                        SET
                            PlanId_int = 0,
                            Amount_dec = 0
                        WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAccountPaymentFail.UserId_int#">
                    </cfquery>
                    <cfquery name="DeleteBillingAccount" datasource="#Session.DBSourceEBM#">
                        DELETE FROM simplebilling.billing
                        WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetAccountPaymentFail.UserId_int#">
                    </cfquery>
                </cfif>
                <cfcatch>
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>

        <cffunction name="ActiveNewAccountPaymentSuccess" access="remote" output="true" hint="Active Account payment success (Paypal)">
            <cfargument name="inpUserId" required="yes" default="" type="number">

            <cfset var dataout	= {} />
            <cfset dataout.RXRESULTCODE = 1>
            <cfset dataout.RESULT = ""/>
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />
            <cfset var ActiveNewAccountPayPal	= '' />
            <cftry>
                <cfquery name="ActiveNewAccountPayPal" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        Active_int = 1
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    AND
                        Active_int = -3
                </cfquery>
                <cfcatch>
                    <cfset dataout.RESULT = "FAIL">
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>
            <cfreturn dataout />
        </cffunction>
        --->
    <!--- End paypal --->


<!---     <cffunction name="RegisterNewAccountNew" access="remote" output="true" hint="Start a new user account">
    </cffunction> --->

    <cffunction name="GetUserInfo" access="public" output="true" hint="Get user info by session user id">
        <cfset var arrEmail = '' />
        <cfset var email    = '' />
        <cfset var getUserInfo  = '' />

        <cfset var dataout = structNew()>
        <cfset dataout.EMAIL = ''>
        <cfset dataout.FULLEMAIL = ''>
        <cfset dataout.MFAPHONE = ''>
        <cfset dataout.FULLMFAPHONE = ''>
        <cfset dataout.USERNAME = ''>
        <cfset dataout.FIRSTNAME = ''>
        <cfset dataout.LASTNAME = ''>
        <cfset dataout.SQUESTIONENABLE = "">
        <cfset dataout.ADDRESS = "">
        <cfset dataout.POSTALCODE = "">
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.USERID = '-1'>
        <cfset dataout.MFACONTACTTYPE = '0'>
        <cfset dataout.SecurityQuestionEnabled_ti = '0'>
        <cfset dataout.YEARLYRENEW = ''>
        <cfset dataout.USERLEVEL='0'>

        <cfset dataout.SSNTAXID = ''>
        <cfset dataout.CITY = ''>
        <cfset dataout.STATE = ''>
        <cfset dataout.PaypalEmail =''>
        <cfset dataout.CAPTURECC ='0'>
        <cfset dataout.APPLUAFFILIATECODE =''>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <!--- Validate Email Unique --->
                <cfquery name="getUserInfo" datasource="#Session.DBSourceREAD#">
                    SELECT
                        EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,YearlyRenew_ti,MFAEnabled_ti,AllowNotRealPhone_ti,
                        UserLevel_int,UserType_int,IntegrateHigherUserID,IntegrateUserID_int,HigherUserID_int,
                        AES_DECRYPT(SSN_TaxId_vch, '#APPLICATION.EncryptionKey_UserDB#')  as SSN_TaxId_vch ,
                        SSN_TaxId_vch as abc,
                        City_vch,
                        State_vch,
                        PaypalEmail_vch,
                        WhiteGloveStatus_ti,
                        CaptureCCInfo_ti,
                        amc_vch

                    FROM
                        simpleobjects.useraccount
                    WHERE
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    AND
                        Active_int > 0
                </cfquery>

                <cfif getUserInfo.RecordCount GT 0>
                    <cfset arrEmail = listToArray(getUserInfo.EmailAddress_vch, ",@")>
                    <cfif ARRAYLEN(arrEmail) GT 0>
                         <cfset email = right(arrEmail[1],2)&'@'&arrEmail[2]>
                    <cfelse>
                         <cfset email=""/>
                    </cfif>

                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.USERID = '#session.USERID#'>
                    <cfset dataout.EMAIL = '#email#'>
                    <cfset dataout.FULLEMAIL = '#getUserInfo.EmailAddress_vch#'>
                    <cfset dataout.MFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.FULLMFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.USERNAME = '#getUserInfo.FirstName_vch#'>
                    <cfset dataout.MFACONTACTTYPE = '#getUserInfo.MFAContactType_ti#'>
                    <cfset dataout.SQUESTIONENABLE = "#getUserInfo.SecurityQuestionEnabled_ti#">

                    <cfset dataout.FIRSTNAME = '#getUserInfo.FirstName_vch#'>
                    <cfset dataout.LASTNAME = '#getUserInfo.LastName_vch#'>
                    <cfset dataout.ADDRESS = "#getUserInfo.Address1_vch#">
                    <cfset dataout.POSTALCODE = "#getUserInfo.PostalCode_vch#">
                    <cfset dataout.YEARLYRENEW = "#getUserInfo.YearlyRenew_ti#">
                    <cfset dataout.MFATYPE = "#getUserInfo.MFAEnabled_ti#">
                    <cfset dataout.ALLOWNOTPHONE = "#getUserInfo.AllowNotRealPhone_ti#">
                    <cfset dataout.SSNTAXID = ToString( ToBinary( getUserInfo.SSN_TaxId_vch, "UTF-8" ))>



                    <cfset dataout.CITY = getUserInfo.City_vch>
                    <cfset dataout.STATE = getUserInfo.State_vch>
                    <cfset dataout.PaypalEmail = getUserInfo.PaypalEmail_vch>

                    <cfset dataout.ALLOWNOTPHONE = "#getUserInfo.AllowNotRealPhone_ti#">
                    <cfset dataout.USERLEVEL = "#getUserInfo.UserLevel_int#">
                    <cfset dataout.USERTYPE = "#getUserInfo.UserType_int#">
                    <cfset dataout.INTEGRATEUSERID = "#getUserInfo.IntegrateUserID_int#">
                    <cfset dataout.INTEGRATEHIGHERUSERID = "#getUserInfo.IntegrateHigherUserID#">
                    <cfset dataout.HIGHERUSERID = "#getUserInfo.HigherUserID_int#">
                    <cfset dataout.WhiteGloveStatusID = "#getUserInfo.WhiteGloveStatus_ti#">
                    <cfset dataout.CAPTURECC = "#getUserInfo.CaptureCCInfo_ti#">
                    <cfset dataout.APPLUAFFILIATECODE =getUserInfo.amc_vch>

                    <cfset dataout.MESSAGE = 'SUCCESS'>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = 'User not found'>
                    <cfreturn dataout>
                </cfif>

                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="GetUserInfoAccount" access="public" output="true" hint="Get user info by session user id">
        <cfset var arrEmail = '' />
        <cfset var email    = '' />
        <cfset var getUserInfo  = '' />

        <cfset var dataout = structNew()>
        <cfset dataout.EMAIL = ''>
        <cfset dataout.FULLEMAIL = ''>
        <cfset dataout.MFAPHONE = ''>
        <cfset dataout.FULLMFAPHONE = ''>
        <cfset dataout.USERNAME = ''>
        <cfset dataout.LASTNAME = ''>
        <cfset dataout.SQUESTIONENABLE = "">
        <cfset dataout.ADDRESS = "">
        <cfset dataout.POSTALCODE = "">
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.USERID = '-1'>
        <cfset dataout.MFACONTACTTYPE = '0'>
        <cfset dataout.SecurityQuestionEnabled_ti = '0'>
        <cfset dataout.Address1_vch = "">
        <cfset dataout.City_vch = "">
        <cfset dataout.State = '0'>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <!--- Validate Email Unique --->
                <cfquery name="getUserInfo" datasource="#Session.DBSourceREAD#">
                    SELECT
                        EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,City_vch,State_vch
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    AND
                        Active_int > 0
                </cfquery>

                <cfif getUserInfo.RecordCount GT 0>
                    <cfset arrEmail = listToArray(getUserInfo.EmailAddress_vch, ",@")>
                    <cfset email = right(arrEmail[1],2)&'@'&arrEmail[2]>

                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.USERID = '#session.USERID#'>
                    <cfset dataout.EMAIL = '#email#'>
                    <cfset dataout.FULLEMAIL = '#getUserInfo.EmailAddress_vch#'>
                    <cfset dataout.MFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.FULLMFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.USERNAME = '#getUserInfo.FirstName_vch#'>
                    <cfset dataout.MFACONTACTTYPE = '#getUserInfo.MFAContactType_ti#'>
                    <cfset dataout.SQUESTIONENABLE = "#getUserInfo.SecurityQuestionEnabled_ti#">
                    <cfset dataout.LASTNAME = '#getUserInfo.LastName_vch#'>
                    <cfset dataout.ADDRESS = "#getUserInfo.Address1_vch#">
                    <cfset dataout.POSTALCODE = "#getUserInfo.PostalCode_vch#">
                    <cfset dataout.Address1_vch = "#getUserInfo.Address1_vch#">
                    <cfset dataout.City_vch = "#getUserInfo.City_vch#">
                    <cfset dataout.State = "#getUserInfo.State_vch#">
                    <cfset dataout.MESSAGE = 'SUCCESS'>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = 'User not found'>
                    <cfreturn dataout>
                </cfif>

                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        </cfif>
        <cfreturn dataout />
    </cffunction>




    <cffunction name="GetUserInfoById" access="public" output="true" hint="Get user info by user id">
        <cfargument name="inpUserId" required="no" default="" type="string">
        <cfset var arrEmail = '' />
        <cfset var email    = '' />
        <cfset var getUserInfo  = '' />

        <cfset var dataout = structNew()>
        <cfset dataout.EMAIL = ''>
        <cfset dataout.FULLEMAIL = ''>
        <cfset dataout.MFAPHONE = ''>
        <cfset dataout.FULLMFAPHONE = ''>
        <cfset dataout.USERNAME = ''>
        <cfset dataout.LASTNAME = ''>
        <cfset dataout.SQUESTIONENABLE = "">
        <cfset dataout.ADDRESS = "">
        <cfset dataout.POSTALCODE = "">
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.USERID = '-1'>
        <cfset dataout.MFACONTACTTYPE = '0'>
        <cfset dataout.CAPTURECC = '0'>
        <cfset dataout.SecurityQuestionEnabled_ti = '0'>

        <cfif  arguments.inpUserId GT 0>
            <cftry>
                <!--- Validate Email Unique --->
                <cfquery name="getUserInfo" datasource="#Session.DBSourceREAD#">
                    SELECT
                        EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,CaptureCCInfo_ti
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpUserId#">
                    AND
                        Active_int > 0
                </cfquery>

                <cfif getUserInfo.RecordCount GT 0>
                    <cfset arrEmail = listToArray(getUserInfo.EmailAddress_vch, ",@")>
                    <cfset email = right(arrEmail[1],2)&'@'&arrEmail[2]>

                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.USERID = '#arguments.inpUserId#'>
                    <cfset dataout.EMAIL = '#email#'>
                    <cfset dataout.FULLEMAIL = '#getUserInfo.EmailAddress_vch#'>
                    <cfset dataout.MFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.FULLMFAPHONE = '#getUserInfo.MFAContactString_vch#'>
                    <cfset dataout.USERNAME = '#getUserInfo.FirstName_vch#'>
                    <cfset dataout.MFACONTACTTYPE = '#getUserInfo.MFAContactType_ti#'>
                    <cfset dataout.SQUESTIONENABLE = "#getUserInfo.SecurityQuestionEnabled_ti#">
                    <cfset dataout.LASTNAME = '#getUserInfo.LastName_vch#'>
                    <cfset dataout.ADDRESS = "#getUserInfo.Address1_vch#">
                    <cfset dataout.POSTALCODE = "#getUserInfo.PostalCode_vch#">
                    <cfset dataout.CAPTURECC = "#getUserInfo.CaptureCCInfo_ti#">
                    <cfset dataout.MESSAGE = 'SUCCESS'>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = 'User not found'>
                    <cfreturn dataout>
                </cfif>

                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="sendMFACode" access="remote" output="true" hint="">
        <cfargument name="sendType" required="no" default="" type="string">
        <cfargument name="makeDefault" required="no" default="" type="string">

        <cfset var MFACookieData    = '' />
        <cfset var sendTypeMethod = '' />
        <cfset var CurrMFACode  = '' />
        <cfset var UpdateUserVerifyCodeMethod   = '' />
        <cfset var userInfo = '' />
        <cfset var RetVarSendMFA = '' />
        <cfset var dataMail = '' />
        <cfset var checkipblocked = '' />
        <cfset var updateMFA=''>


        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
<!---
        <cfset dataout.RESULT = 'Block'>
		<cfset dataout.MESSAGE = '' />
        <cfreturn dataout/>
--->
        <cftry>

             <CFQUERY name="checkipblocked" datasource="#Session.DBSourceREAD#">
                SELECT
                        IdIp
                FROM
                        simpleobjects.blockipaddress
                WHERE
                        IpAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">
            </CFQUERY>
            <cfif checkipblocked.RECORDCOUNT GT 0>
                <cfset dataout.RESULT = 'Block'>
                <cfset dataout.MESSAGE = '' />
                <cfreturn dataout/>
            </cfif>
            <cfcatch type="any">
            </cfcatch>
        </cftry>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>

                <cfinvoke method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

                <cfif userInfo.RESULT EQ 'SUCCESS'>

                    <cfif DEVSITE EQ 0>
                        <!--- Decrypt out remember me cookie. --->
                        <cfset  MFACookieData = Decrypt(evaluate("COOKIE.MFAEBM#Session.USERID#"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                    <cfelse>
                        <cfset MFACookieData = evaluate("COOKIE.MFAEBM#Session.USERID#")>
                    </cfif>

                    <cfif !structKeyExists(arguments, "sendType") OR arguments.sendType EQ ''>
                        <cfset sendTypeMethod = userInfo.MFACONTACTTYPE>
                    <cfelse>
                         <cfset sendTypeMethod = arguments.sendType>
                    </cfif>

                    <!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle of list. Extract it from the list. --->
                    <cfset CurrMFACode = ListGetAt(MFACookieData,6,":") />



                    <cfif trim(CurrMFACode) NEQ ''>


                        <!--- SMS Method based on phone number in account --->
                        <cfif sendTypeMethod EQ "3">


                            <!--- For non automated process liberal use of cftry / cfcatch on critical UI sections is a must --->
                            <cftry>

	                            <cfinvoke
	                                method="SendMFA"
	                                returnvariable="RetVarSendMFA">
	                                <cfinvokeargument name="inpMFACode" value="#CurrMFACode#"/>
	                                <cfinvokeargument name="MFAContactString_vch" value="#userInfo.MFAPHONE#"/>
	                                <cfinvokeargument name="MFAContactType_ti" value="3"/>
	                                <cfinvokeargument name="MFAEXT_vch" value=""/>
	                            </cfinvoke>

	                            <cfif RetVarSendMFA.SMSINIT EQ 1>

		                            <cfset dataout.RESULT = 'SUCCESS'>
		                            <cfset dataout.MESSAGE = '#userInfo.MFAPHONE# -  #RetVarSendMFA.RAWJSON# #RetVarSendMFA.SMSINIT# '>

	                            <cfelse>

		                            <cfset dataout.RESULT = 'FAIL'>
									<cfset dataout.MESSAGE = 'Send Code Fail (#RetVarSendMFA.SMSINIT#) #RetVarSendMFA.SMSRESULTMESSAGE#' />

	                            </cfif>

                            <cfcatch type="any">

                            	<cfset dataout.RESULT = 'FAIL'>
								<cfset dataout.MESSAGE = 'Send Code Fail' & cfcatch.message & " " & cfcatch.detail>

                            </cfcatch>

                            </cftry>


                        <!--- Send email version  --->
                        <cfelseif sendTypeMethod EQ "2">
                            <!---<cfmail to="#userInfo.FULLEMAIL#" subject="Your Sire verification code" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                            <cfoutput>
                                    <h2>Welcome to Our Services!</h2>
                                    <p> Hello #userInfo.USERNAME#, </p>
                                    <p> You are receiving this email because you recently signed a new account up or signed in on a new device/browser. </p>
                                    <p> Your Sire verification code is <strong>#CurrMFACode#</strong> .</p>
                                    <p> If you have any problems, please reply to this email to get assistance. </p>
                                    <br/>
                                    <p>Thanks,</p>
                                    <p>Sire Assistance Team!</p>
                                    <p> Website: <a href="https://sire.com"> https://sire.com </a> </p>
                                    <p> Phone: 949.123.4567 </p>
                                    <p> Email: <a href="mailto:support@sire.com"> support@sire.com </a> </p>
                            </cfoutput>
                            </cfmail>--->
                            <cfset dataMail = {
                                username:userInfo.USERNAME,
                                CurrMFACode:CurrMFACode,
                                rootUrl:rootUrl
                            }/>
                            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                                    <cfinvokeargument name="to" value="#userInfo.FULLEMAIL#">
                                    <cfinvokeargument name="type" value="html">
                                    <cfinvokeargument name="subject" value="Your Sire verification code">
                                    <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_new_device_authentication.cfm">
                                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                            </cfinvoke>


                            <cfset dataout.RESULT = 'SUCCESS'>

                        <cfelse>

                            <cfset dataout.RESULT = 'FAIL'>
							<cfset dataout.MESSAGE = 'Send Code Fail Invalid Contact Type Specified (#sendTypeMethod#)' />


                        </cfif>


                        <!--- may not want this feature - let user choose everytime
                        <!--- SAVE DEFAULT SEND VERIFY CODE METHOD --->
                        <cfif arguments.makeDefault EQ "1">
                            <cfquery name="UpdateUserVerifyCodeMethod" datasource="#Session.DBSourceEBM#">
                                UPDATE
                                    simpleobjects.useraccount
                                SET
                                    MFAContactType_ti =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#sendTypeMethod#">
                                WHERE
                                    userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                                AND
                                    Active_int > 0
                            </cfquery>
                        </cfif>
                        --->




                    <cfelse>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.MESSAGE = 'Verify code not found.'>
                    </cfif>
                    <!--- SEND VERIFY CODE --->


                <cfelse>
                     <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = 'User not found'>
                </cfif>

            <cfcatch>
                <cfset dataout.RESULT = 'FAIL'>
                <cfset dataout.MESSAGE = 'Send Code Fail'&cfcatch.message & " " & cfcatch.detail>
            </cfcatch>
            </cftry>
            <cfif dataout.RESULT EQ "SUCCESS">
                <cfquery name="updateMFA" datasource="#Session.DBSourceEBM#">
                    UPDATE  simpleobjects.useraccount
                    SET     MFA_Sent_ti=1
                    WHERE   UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                </cfquery>
           </cfif>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="ValidateMFA" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's MFA code matches.">
        <cfargument name="inpMFACode" TYPE="string"/>

        <cfset var strMFAMe = '' />
        <cfset var dataout = {} />
        <cfset var CurrMFACode = "" />
        <cfset var MFACookieData = "" />
        <cfset var strRememberMe= '' />

        <cfset dataout.inpMFACode = "#arguments.inpMFACode#" />

        <cftry>

        <!--- MULTI FACTOR AUTHETICATION PER COMPUTER--->

            <cfset dataout.MFAOK = 0 />

            <cftry>

                <cfif DEVSITE EQ 0>
                    <!--- Decrypt out remember me cookie. --->
                    <cfset  MFACookieData = Decrypt(evaluate("COOKIE.MFAEBM#Session.USERID#"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                <cfelse>
                    <cfset MFACookieData = evaluate("COOKIE.MFAEBM#Session.USERID#")>
                </cfif>



                <cfset dataout.MFACookieData = "#MFACookieData#" />

                    <!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle
                    of list. Extract it from the list. --->
                <cfset  CurrMFACode = ListGetAt(MFACookieData,6,":") />

                <cfset dataout.CurrMFACode = "#CurrMFACode#" />

                <!--- Check to make sure this value is numeric, otherwise, it was not a valid value.  --->
                <cfif TRIM(CurrMFACode) EQ TRIM(inpMFACode) >

                    <!--- This box has been authorized--->
                    <cfset dataout.MFAOK = 1 />

                    <!--- Set new string for Cookie--->
                    <cfset strMFAMe = (
                            CreateUUID() & ":" &
                            CreateUUID() & ":" &
                            Session.USERID & ":" &
                            1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                            CreateUUID() & ":" &
                            inpMFACode & ":" & <!--- Last code sent for this computer --->
                            CreateUUID()
                    ) />

                    <cfif DEVSITE EQ 0>
                        <!--- Encrypt the value. --->
                        <cfset  strMFAMe = Encrypt(strMFAMe,APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                    </cfif>



                    <!--- Store the cookie such that it will expire after 365 days. --->
                    <cfcookie
                        name="MFAEBM#Session.USERID#"
                        value="#strMFAMe#"
                        expires="365"
                    />

                    <cfif Session.RememberMe EQ 1>
                                    <cfset strRememberMe = (
                                        CreateUUID() & ":" &
                                        CreateUUID() & ":" &
                                        Session.USERID & ":" &
                                        CreateUUID()

                                    ) />

                                    <!--- Encrypt the value. --->
                                    <cfset strRememberMe = Encrypt(
                                    strRememberMe,
                                    APPLICATION.EncryptionKey_Cookies,
                                    "cfmx_compat",
                                    "hex"
                                    ) />


                                    <!--- Store the cookie such that it will expire after 30 days. --->
                                    <cfcookie
                                        name="RememberMe"
                                        value="#strRememberMe#"
                                        expires="30"
                                    />
                    </cfif>

                <cfelse>

                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.REASONMSG = "Code does not match last one sent." />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />

                    <cfreturn dataout />

                </cfif>

            <!--- Catch any errors. --->
            <cfcatch TYPE="any">
                <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
            </cfcatch>
            </cftry>

            <cfif dataout.MFAOK EQ 1>
                <cfset Session.loggedIn = 1>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.REASONMSG = "Device Approved" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />

                <!--- Event trigger sign in --->
                <cfinvoke method="trigger" component="session.sire.models.cfc.events">
                    <cfinvokeargument name="event" value="signin"/>
                    <cfinvokeargument name="module" value="userstools.ValidateMFA"/>
                    <cfinvokeargument name="userId" value="#Session.USERID#"/>
                </cfinvoke>

            <cfelse>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.REASONMSG = "Device Not Approved Yet - Key Error" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />

            </cfif>

        <cfcatch TYPE="any">

            <cfset dataout.RXRESULTCODE = -5 />
            <cfset dataout.REASONMSG = "general CF Error" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>

        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="checkMFACodeExits" access="remote" output="true" hint="Check if MFA code is set in cookie">
        <cfset var MFACookieData    = '' />
        <cfset var CurrMFAOKFlag    = '' />

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.MESSAGE = ''>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfset MFACookieData = evaluate("COOKIE.MFAEBM#Session.USERID#")>
                <cfset CurrMFAOKFlag = ListGetAt(MFACookieData,4,":") />
                <cfif CurrMFAOKFlag EQ 1>
                    <cfset dataout.RESULT = 'SUCCESS'>
                    <cfset dataout.MESSAGE = 'Device registered.'>
                </cfif>
            <cfcatch>
            </cfcatch>
            </cftry>
        </cfif>
        <cfreturn dataout />
    </cffunction>
    <cffunction name="ValidateUserPassword" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's name and password match.">
        <cfargument name="inpUserID" TYPE="string"/>
        <cfargument name="inpPassword" TYPE="string"/>

        <cfset var validated    = '' />
        <cfset var getUser  = '' />
        <cfset var safeName = '' />
        <cfset var safePass = '' />

        <cfset var dataout = {} />
        <!--- Ensure that attempts to authenticate start with new credentials. --->
        <cflogout/>

        <cftry>

            <cfset arguments.inpUserID = TRIM(arguments.inpUserID)>
            <cfset arguments.inpPassword = TRIM(URLDEcode(arguments.inpPassword))>

            <!--- Server Level Input Protection      component="Premierivr.model.Validation.validation" --->
            <cfinvoke method="VldInput" returnvariable="safeName">
                <cfinvokeargument name="Input" value="#inpUserID#"/>
            </cfinvoke>

            <cfinvoke method="VldInput" returnvariable="safePass">
                <cfinvokeargument name="Input" value="#inpPassword#"/>
            </cfinvoke>



            <cfif safeName eq true and safePass eq true>
                <!--- No reference to current session variables - exisits in its on session/application scope --->

                <!----- used when login using username/email and password ---->
                <CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
                    SELECT
                        Password_vch,
                        UserId_int,
                        PrimaryPhoneStr_vch,
                        SOCIALMEDIAID_BI,
                        EmailAddressVerified_vch,
                        EmailAddress_vch,
                        CompanyAccountId_int,
                        Active_int,
                        MFAContactString_vch,
                        MFAContactType_ti,
                        MFAEnabled_ti,
                        MFAEXT_vch,
                        FirstName_vch,
                        LastName_vch,
                        Created_dt
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        (
                            UCASE(UserName_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                            OR
                            UCASE(EmailAddress_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                        )
                        <!---    AND Password_vch = AES_ENCRYPT('#inpPassword#', '#APPLICATION.EncryptionKey_UserDB#')  --->
                        AND Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPassword#">, '#APPLICATION.EncryptionKey_UserDB#')
                        AND (CBPID_int = 1 OR CBPID_int IS NULL)
                        AND Active_int = 1

                </CFQUERY>
                <cfif getUser.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE =1  />
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -4 />
                    <cfset dataout.REASONMSG = "Failed Server Level Validation" />
                    <cfset dataout.USERID = "#inpUserID#" />
                    <cfset dataout.UserName = "" />
                    <cfset dataout.EmailAddress = "" />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "Email password combination invalid" />
                    <cfset dataout.ERRMESSAGE = "" />
                </cfif>
            <cfelse>

                <cfset dataout.RXRESULTCODE = -4 />
                <cfset dataout.REASONMSG = "Failed Server Level Validation" />
                <cfset dataout.USERID = "#inpUserID#" />
                <cfset dataout.UserName = "" />
                <cfset dataout.EmailAddress = "" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "User name password combination invalid" />
                <cfset dataout.ERRMESSAGE = "" />


            </cfif>

        <cfcatch TYPE="any">

            <cfset dataout.RXRESULTCODE = -5 />
            <cfset dataout.REASONMSG = "general CF Error" />
            <cfset dataout.USERID = "" />
            <cfset dataout.UserName = "" />
            <cfset dataout.EmailAddress = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="validateUsers" access="remote" returnformat="JSON" output="true" hint="Validate a specified users's name and password match.">
          <cfargument name="inpUserID" TYPE="string"/>
          <cfargument name="inpPassword" TYPE="string"/>
          <cfargument name="inpFromSource" TYPE="string" default="" required="no"/>
          <cfargument name="inpRememberMe" TYPE="string" default="0" required="no"/>
          <cfargument name="inpAt" TYPE="string" required="no" default="0"/>
          <cfargument name="facebook" TYPE="numeric" required="no" default="0"/>

          <cfset var validated    = '' />
          <cfset var xtrainfo = '' />
          <cfset var MFACode  = '' />
          <cfset var strMFAMe = '' />
          <cfset var getUser  = '' />
          <cfset var getCompanySharedUser = '' />
          <cfset var safeName = '' />
          <cfset var safePass = '' />
          <cfset var getShortCode = '' />
          <cfset var strRememberMe = '' />
          <cfset var checkExpire = '' />

          <cfset var dataout = {} />
          <cfset var MFACookieData = '0' />
          <cfset var CurrMFAOKFlag = '' />
          <cfset var MFAAuthFlag = '' />
          <cfset var RetVarSendMFA = '' />
          <cfset var getUserDeactive = '' />
          <cfset var getUserPlan = '' />
          <cfset var RetVarmailChimpAPIs = '' />
          <cfset var EstablishSessionFromUserInfo = '' />

          <cfset Session.permissionManaged = false>
          <cfset Session.loggedIn = 0>


          <!---
          Positive is success
          1 = OK
          2 =
          3 =
          4 =

          Negative is failure
          -1 = general failure
          -2 = CF Failure
          -3 =
          --->

          <cfset dataout.MFAREQ = 0 />
          <cfset dataout.LAST4 = "" />

          <cftry>

               <cfset arguments.inpUserID = TRIM(arguments.inpUserID)>
               <cfset arguments.inpPassword = TRIM(URLDEcode(arguments.inpPassword))>

               <!--- Server Level Input Protection      component="Premierivr.model.Validation.validation" --->
               <cfinvoke method="VldInput" returnvariable="safeName">
                    <cfinvokeargument name="Input" value="#inpUserID#"/>
               </cfinvoke>

               <cfinvoke method="VldInput" returnvariable="safePass">
                    <cfinvokeargument name="Input" value="#inpPassword#"/>
               </cfinvoke>

            <cfif safeName eq true and safePass eq true>
                <!--- No reference to current session variables - exisits in its on session/application scope --->

                <!--- used when login using username/email and password --->
                <CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
                    SELECT
                        Password_vch,
                        UserId_int,
                        PrimaryPhoneStr_vch,
                        SOCIALMEDIAID_BI,
                        EmailAddressVerified_vch,
                        EmailAddress_vch,
                        CompanyAccountId_int,
                        Active_int,
                        MFAContactString_vch,
                        MFAContactType_ti,
                        MFAEnabled_ti,
                        MFAEXT_vch,
                        FirstName_vch,
                        LastName_vch,
                        Created_dt
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        (
                            UCASE(UserName_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                            OR
                            UCASE(EmailAddress_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                        )
                        <!---    AND Password_vch = AES_ENCRYPT('#inpPassword#', '#APPLICATION.EncryptionKey_UserDB#')  --->
                        AND Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPassword#">, '#APPLICATION.EncryptionKey_UserDB#')
                        AND (CBPID_int = 1 OR CBPID_int IS NULL)
                        AND Active_int = 1

                </CFQUERY>

               <cfif getUser.recordcount gt 0 > <!--- and getUser.EmailAddressVerified_vch eq '1' --->

                    <!--- Establish Session - set values as found --->
                   <cfinvoke component="public.sire.models.cfc.userstools" method="EstablishSessionFromUserInfo" returnvariable="RetVarEstablishSessionFromUserInfo">
                        <cfinvokeargument name="getUser" value="#getUser#">
                   </cfinvoke>

                   <cfset Session.loggedIn = "0" />

                   <cfif inpRememberMe GT 0>
                       <cfset Session.RememberMe = 1>
                   </cfif>

                   <cfset xtrainfo = "NADA">

                   <!--- MULTI FACTOR AUTHETICATION PER COMPUTER--->

                   <!--- Verify Old Cookie for this user is valid --->
                   <cfset MFAAuthFlag = 0>

                   <cftry>

                       <cfif DEVSITE EQ 0>
                           <!--- Decrypt out cookie. --->
                           <cfset  MFACookieData = Decrypt(evaluate("COOKIE.MFAEBM#getUser.UserId_int#"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                       <cfelse>
                           <cfset  MFACookieData = evaluate("COOKIE.MFAEBM#getUser.UserId_int#")>
                       </cfif>


                       <!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle
                           of list. Extract it from the list. --->
                       <cfset  CurrMFAOKFlag = ListGetAt(
                               MFACookieData,
                               4,
                               ":"
                       ) />

                       <!--- Check to make sure this value is numeric, otherwise, it was not a valid value.  --->
                       <cfif CurrMFAOKFlag EQ "1">
                           <!--- This box has been authorized--->
                           <cfset MFAAuthFlag = 1>
                           <cfset dataout.MFAREQ = 0 />
                           <!--- Rememberme --->
                           <cfif inpRememberMe GT 0>
                               <cfset strRememberMe = (
                                   CreateUUID() & ":" &
                                   CreateUUID() & ":" &
                                   Session.USERID & ":" &
                                   CreateUUID()

                               ) />

                               <!--- Encrypt the value. --->
                               <cfset strRememberMe = Encrypt(
                               strRememberMe,
                               APPLICATION.EncryptionKey_Cookies,
                               "cfmx_compat",
                               "hex"
                               ) />


                               <!--- Store the cookie such that it will expire after 30 days. --->
                               <cfcookie
                                   name="RememberMe"
                                   value="#strRememberMe#"
                                   expires="30"
                               />
                           </cfif>
                       </cfif>

                   <!--- Catch any errors. --->
                   <cfcatch TYPE="any">
                       <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->
                   </cfcatch>
                   </cftry>


                   <!--- Look for special Encrypted/Salted Admin Cookie to allow skip MFA - this will allow admins to impersonate a user account without triggering MFA--->

                   <cfif LEN(TRIM(getUser.MFAContactString_vch) ) LT 10 OR getUser.MFAEnabled_ti EQ 0  >
                       <!--- Account is not properly setup for MFA so Skip --->
                       <cfset MFAAuthFlag = 1>
                   </cfif>

                   <!--- Resetup authorization --->
                   <cfif MFAAuthFlag EQ 0>

                       <!--- Generate random 6 digit code --->
                       <cfset MFACode = RandDigitString(6) />

                       <cfset dataout.LAST4 = RIGHT(TRIM(getUser.MFAContactString_vch), 4) />

                       <!--- Create new string for Cookie--->
                       <cfset strMFAMe = (
                               CreateUUID() & ":" &
                               CreateUUID() & ":" &
                               getUser.UserId_int & ":" &
                               0 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                               CreateUUID() & ":" &
                               MFACode & ":" & <!--- Last code sent for this computer --->
                               CreateUUID()
                       ) />

                       <cfif DEVSITE EQ 0>
                           <!--- Encrypt the value.--->
                           <cfset  strMFAMe = Encrypt(strMFAMe,APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                       </cfif>


                       <!--- Store the cookie such that it will expire after 365 days. --->
                       <cfcookie
                           name="MFAEBM#getUser.UserId_int#"
                           value="#strMFAMe#"
                           expires="365"
                       />
                       <cfset dataout.MFAREQ = 1 />

                   <cfelse>

                       <!--- Generate random "BAD" digit code --->
                       <cfset MFACode = "ghtyuesadq12xaw" />

                       <!--- Create new string for Cookie--->
                       <cfset strMFAMe = (
                               CreateUUID() & ":" &
                               CreateUUID() & ":" &
                               getUser.UserId_int & ":" &
                               1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                               CreateUUID() & ":" &
                               MFACode & ":" & <!--- Last code sent for this computer --->
                               CreateUUID()
                       ) />

                       <cfif DEVSITE EQ 0>
                           <!--- Encrypt the value. --->
                           <cfset  strMFAMe = Encrypt( strMFAMe, APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                       </cfif>

                       <!--- Store the cookie such that it will expire after 365 days. --->
                       <cfcookie
                           name="MFAEBM#getUser.UserId_int#"
                           value="#strMFAMe#"
                           expires="365"
                       />

                       <!---
                       <cfset Session.USERNAME = getUser.EmailAddress_vch>
                       <cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch EQ ""? " ": getUser.PrimaryPhoneStr_vch>
                       <cfset Session.EmailAddress = getUser.EmailAddress_vch>
                       <cfset Session.fbuserid = getUser.SOCIALMEDIAID_BI>
                       <cfset Session.at = inpAt>
                       <cfset Session.facebook = facebook>
                       <cfset Session.FULLNAME = getUser.FirstName_vch&' '&getUser.LastName_vch>
                       <cfset Session.FIRSTNAME = getUser.FirstName_vch>
                       --->

                       <cfif LEN(TRIM(getUser.MFAContactString_vch) ) LT 10 OR getUser.MFAEnabled_ti EQ 0  >
                           <cfset Session.MFAOK = 1/>
                       <cfelse>
                           <cfset Session.MFAOK = 0/>
                       </cfif>

                       <!--- If call from signup not set LoggedIn here, only loggedIn after verify code --->
                       <cfif inpFromSource NEQ 'SignUp'>
                           <cfset Session.loggedIn = 1>
                       </cfif>

                       <cfset xtrainfo = "Cookie Set for Session.USERID = #Session.USERID# Session.PrimaryPhone = #Session.PrimaryPhone#">

                       <cfset dataout.REASONMSG = "Login Success! #xtrainfo#" />
                       <!--- <cfset dataout.REASONMSG2 = "WTH - #SerializeJSON(Session)#" /> --->
                       <!--- <cfset dataout.REASONMSG3 = "WTH - #Session.cfid#" /> --->
                       <cfset dataout.USERID = "#Session.USERID#" />
                       <cfset dataout.UserName = "#Session.UserName#" />
                       <cfset dataout.EmailAddress = "#Session.EmailAddress#" />

                   </cfif>

                   <cfif dataout.MFAREQ NEQ 1>





                         <!--- Event trigger sign in --->
                         <cfinvoke method="trigger" component="session.sire.models.cfc.events">
                              <cfinvokeargument name="event" value="signin"/>
                              <cfinvokeargument name="module" value="userstools.validateUsers"/>
                              <cfinvokeargument name="userId" value="#Session.USERID#"/>
                         </cfinvoke>
                   </cfif>

                    <cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
                    </cfinvoke>

                   <!--- Need change to new event trigger --->
                   <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                       <cfinvokeargument name="InpEmailString" value="#getUser.EmailAddress_vch#">
                       <cfinvokeargument name="InpLastLoginDate" value="#dateFormat(NOW(),'mm/dd/yyyy')#">
                       <cfinvokeargument name="InpFirstName" value="#getUser.FirstName_vch#">
                       <cfinvokeargument name="InpLastName" value="#getUser.LastName_vch#">
                       <cfinvokeargument name="InpStatus" value="active">
                       <cfinvokeargument name="InpAccountType" value="#getUserPlan.PLANNAME#">
                       <cfinvokeargument name="InpSignupDate" value="#dateFormat(getUser.Created_dt,'mm/dd/yyyy')#">
                       <cfinvokeargument name="InpPhoneNumber" value="#getUser.MFAContactString_vch#">
                   </cfinvoke>

                   <cfset dataout.RXRESULTCODE = 1 />
                   <cfset dataout.TYPE = "" />
                   <cfset dataout.MESSAGE = "" />
                   <cfset dataout.ERRMESSAGE = "" />

               <cfelse>
                   <!--- check deactive --->
                    <cfquery name="getUserDeactive" datasource="#Session.DBSourceREAD#">
                       SELECT
                           UserId_int
                       FROM
                           simpleobjects.useraccount
                       WHERE
                           (
                               UCASE(UserName_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                               OR
                               UCASE(EmailAddress_vch) = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#UCASE(inpUserID)#">
                           )
                           <!---    AND Password_vch = AES_ENCRYPT('#inpPassword#', '#APPLICATION.EncryptionKey_UserDB#')  --->
                           AND Password_vch = AES_ENCRYPT(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpPassword#">, '#APPLICATION.EncryptionKey_UserDB#')
                           AND (CBPID_int = 1 OR CBPID_int IS NULL)
                           AND Active_int = 0

                   </cfquery>

                   <cfif getUserDeactive.RECORDCOUNT GT 0>

                       <cfset dataout.RXRESULTCODE = -3 />
                       <cfset dataout.REASONMSG = "This account is currently suspended." />
                       <cfset dataout.USERID = "" />
                       <cfset dataout.UserName = "" />
                       <cfset dataout.EmailAddress = "" />
                       <cfset dataout.TYPE = "" />
                       <cfset dataout.MESSAGE = "" />
                       <cfset dataout.ERRMESSAGE = "" />

                       <cfreturn dataout>
                   </cfif>

                   <cfset dataout.RXRESULTCODE = -3 />
                   <cfset dataout.REASONMSG = "User name password combination invalid" />
                   <cfset dataout.USERID = "" />
                   <cfset dataout.UserName = "" />
                   <cfset dataout.EmailAddress = "" />
                   <cfset dataout.TYPE = "" />
                   <cfset dataout.MESSAGE = "" />
                   <cfset dataout.ERRMESSAGE = "" />

               </cfif>

            <cfelse>

                <cfset dataout.RXRESULTCODE = -4 />
                <cfset dataout.REASONMSG = "Failed Server Level Validation" />
                <cfset dataout.USERID = "#inpUserID#" />
                <cfset dataout.UserName = "" />
                <cfset dataout.EmailAddress = "" />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "User name password combination invalid" />
                <cfset dataout.ERRMESSAGE = "" />


            </cfif>

        <cfcatch TYPE="any">

            <cfset dataout.RXRESULTCODE = -5 />
            <cfset dataout.REASONMSG = "general CF Error" />
            <cfset dataout.USERID = "" />
            <cfset dataout.UserName = "" />
            <cfset dataout.EmailAddress = "" />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />

        </cfcatch>

        </cftry>

        <cfreturn dataout />

    </cffunction>

    <!--- Generate random strings of specified length --->
    <cffunction name="RandDigitString" output="no" returntype="string">
        <cfargument name="length" type="numeric" required="yes">

        <!--- Local vars --->
        <cfset var result="">
        <cfset var i=0>

        <!--- Create string --->
        <cfloop index="i" from="1" to="#ARGUMENTS.length#">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(48, 57))>
        </cfloop>

        <!--- Return it --->
        <cfreturn result>
    </cffunction>

    <cffunction name="checkLogin" access="public" output="true" hint="check user login or not">
        <cfif structKeyExists(Session, "USERID") AND structKeyExists(Session, "loggedIn") AND Session.USERID GT 0 AND Session.loggedIn EQ 1>
            <cfreturn true>
        <cfelse>
            <cfreturn false>
        </cfif>
    </cffunction>

    <cffunction name="getUserLoginInfo" access="public" output="true" hint="check user login or not">
        <cfset var returnData   = '' />
        <cfset returnData = {}>
        <cfset returnData.FULLNAME = ''>
        <cfset returnData.USERNAME = ''>

        <cfif structKeyExists(Session, "USERID") AND structKeyExists(Session, "loggedIn") AND Session.USERID GT 0 AND Session.loggedIn EQ 1>
            <cfset returnData.FULLNAME = Session.FULLNAME>
            <cfset returnData.USERNAME = Session.USERNAME>
        </cfif>
        <cfreturn returnData>
    </cffunction>
     <cffunction name="ValidateSecurityQuestion" access="remote" output="true" hint="Get user info by session user id">

        <cfargument name="json_answer_data" type="string" required="yes">
        <cfargument name="arr_question_ids" type="string" required="yes">

        <cfset var answer_data  = '' />
        <cfset var list_question_id = '' />
        <cfset var check_answer = '' />
        <cfset var getUserAnswers   = '' />
        <cfset var listUserAnswer   = '' />

        <cfset answer_data = DeserializeJSON(arguments.json_answer_data)> <!--- struct questionid - answer text --->
        <cfset list_question_id = arrayToList(DeserializeJSON(arguments.arr_question_ids))>

        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.USERID = '-1'>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>

                <cfquery name="getUserAnswers" datasource="#Session.DBSourceREAD#">
                    SELECT
                        QuestionId_int,Answer_txt
                    FROM
                        simpleobjects.secrity_user_answers
                    WHERE
                        userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                    AND
                        QuestionId_int IN (<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#list_question_id#" LIST="yes">)
                </cfquery>

                <cfif getUserAnswers.RecordCount GTE 3>
                    <!---
                    <cfinvoke method="QueryToStruct" returnvariable="listUserAnswer">
                        <cfinvokeargument name="Query" value="#getUserAnswers#"/>
                    </cfinvoke>
                    --->
                    <cfset check_answer = 0>

                    <cfloop query = "#getUserAnswers#">
                        <cfif Hash(answer_data[getUserAnswers.QuestionId_int],"MD5") EQ getUserAnswers.Answer_txt>
                            <cfset check_answer++>
                        <cfelse>
                            <cfset check_answer = 0>
                            <cfbreak>
                        </cfif>
                    </cfloop>

                    <cfif check_answer EQ 3>
                        <cfset Session.loggedIn = 1>
                        <cfset dataout.RESULT = 'SUCCESS'>
                        <cfset dataout.USERID = '#session.USERID#'>
                        <cfset dataout.MESSAGE = 'SUCCESS'>
                    <cfelse>
                        <cfset dataout.RESULT = 'FAIL'>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'Sorry, your answer was incorrect'>
                    </cfif>
                    <cfreturn dataout>
                <cfelse>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = 'User not set security questions.'>
                    <cfreturn dataout>
                </cfif>
                <cfcatch>
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.MESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.USERID = '-1'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="ChangePassword" access="remote" output="false" hint="Change user password">
        <cfargument name="inpCode" type="string" required="yes" />
        <cfargument name="inpPassword" type="string" required="yes" />
        <cfset var GetUserId    = '' />
        <cfset var safePass = '' />

        <cfset var dataout = {} />
        <cftry>

            <cfinvoke method="VldInput" returnvariable="safePass">
                <cfinvokeargument name="Input" value="#arguments.inpPassword#"/>
            </cfinvoke>

            <cfif safePass NEQ true OR arguments.inpPassword EQ "">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-" />
                <cfset dataout.ERRMESSAGE = "Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-" />
                <cfreturn dataout>
            </cfif>

            <!---validate verify code does match with userid--->
            <cfquery name="GetUserId" datasource="#Session.DBSourceREAD#">
                SELECT
                    UserId_int,
                    UserName_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    VerifyPasswordCode_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.inpCode#">
                AND
                    Active_int = 1
                LIMIT 1
            </cfquery>
            <!---if verify code does not match with userid, return--->
            <cfif NOT GetUserId.RecordCount>
                <cfset dataout = {} />
                <cfset dataout.RXRESULTCODE = -2 />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "Your link was invalid! We'll be happy to send you another one when you're ready!" />
                <cfset dataout.ERRMESSAGE = "" />
                <cfreturn dataout>
            <cfelse>
                <!---if verify code does match with userid, update VerifyPasswordCode_vch is null and Password_vch--->
                <cfquery datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        VerifyPasswordCode_vch = NULL,
                        Password_vch = AES_ENCRYPT('#arguments.inpPassword#', '#RULE_ENCRYPT_PASS#')
                    WHERE
                        UserId_int = '#GetUserId.UserId_int#'
                </cfquery>

                <cfset dataout = {} />
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.USERNAME = GetUserId.UserName_vch />
                <cfset dataout.TYPE = "" />
                <cfset dataout.MESSAGE = "" />
                <cfset dataout.ERRMESSAGE = "" />
            </cfif>
            <cfcatch type="any">
                <cfset dataout = {} />
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="getUserShortCode" access="public" output="true">

        <cfset var getShortCode = '' />
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cfset dataout.SHORTCODE = ''>
        <cfset dataout.SHORTCODEID = 0/>

        <cfif structKeyExists(session, "USERID") && session.USERID GT 0>
            <cftry>
                <cfquery datasource="#Session.DBSourceEBM#" name="getShortCode">
                        SELECT
                            sc.ShortCodeId_int,
                            sc.ShortCode_vch,
                            scr.RequesterId_int,
                            scr.IsDefault_ti
                        FROM
                            sms.shortcode sc
                        JOIN
                            sms.shortcoderequest scr
                        ON
                            sc.ShortCodeId_int = scr.ShortCodeId_int
                        WHERE
                            scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                        ORDER BY
                        	scr.ShortCodeRequestId_int ASC
                </cfquery>

                <cfif getShortCode.RECORDCOUNT GT 1>
                    <cfloop query="getShortCode">
                        <cfif getShortCode.IsDefault_ti GT 0>
                            <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
                            <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
                        </cfif>
                    </cfloop>
                    <cfif dataout.SHORTCODE EQ ''>
                        <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
                        <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
                    </cfif>
                    <cfset dataout.RXRESULTCODE = 1/>
                <cfelseif getShortCode.RecordCount GT 0>
                    <cfset dataout.RXRESULTCODE = 1 />
                    <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
                    <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.MESSAGE = "No shortcode found." />
                    <cfset dataout.ERRMESSAGE = "" />
                </cfif>

                <cfcatch type="any">
                    <cfset dataout = {} />
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>

         <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfset dataout.ERRMESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>


        <cfreturn dataout />
    </cffunction>


     <!--- Multi Factor Authentication through production API - Careful - This is live --->

    <!--- generate signature string --->
    <cffunction name="generateSignature" returnformat="json" access="private" output="false">
        <cfargument name="method" required="true" type="string" default="GET" hint="Method" />
        <cfargument name="secretKey" required="true" type="string" default="gsdagadfgadsfgsdfgdsfgsdfg" hint="Secret Key" />

        <cfset var retval = {} />
        <cfset var dateTimeString = GetHTTPTimeString(Now())>

        <!--- Create a canonical string to send --->
        <cfset var cs = "#arguments.method#\n\n\n#dateTimeString#\n\n\n">
        <cfset var signature = createSignature(cs,secretKey)>

        <cfset retval.SIGNATURE = signature>
        <cfset retval.DATE = dateTimeString>

        <cfreturn retval>
    </cffunction>

    <!--- Encrypt with HMAC SHA1 algorithm--->
    <cffunction name="HMAC_SHA1" returntype="binary" access="private" output="false" hint="NSA SHA-1 Algorithm">
       <cfargument name="signKey" type="string" required="true" />
       <cfargument name="signMessage" type="string" required="true" />

       <cfset var jMsg = JavaCast("string",arguments.signMessage).getBytes("iso-8859-1") />
       <cfset var jKey = JavaCast("string",arguments.signKey).getBytes("iso-8859-1") />
       <cfset var key = createObject("java","javax.crypto.spec.SecretKeySpec") />
       <cfset var mac = createObject("java","javax.crypto.Mac") />

       <cfset key = key.init(jKey,"HmacSHA1") />
       <cfset mac = mac.getInstance(key.getAlgorithm()) />
       <cfset mac.init(key) />
       <cfset mac.update(jMsg) />

       <cfreturn mac.doFinal() />
    </cffunction>


    <cffunction name="createSignature" returntype="string" access="private" output="false">
        <cfargument name="stringIn" type="string" required="true" />
        <cfargument name="scretKey" type="string" required="true" />
        <!--- Replace "\n" with "chr(10) to get a correct digest --->
        <cfset var fixedData = replace(arguments.stringIn,"\n","#chr(10)#","all")>
        <!--- Calculate the hash of the information --->
        <cfset var digest = HMAC_SHA1(scretKey,fixedData)>
        <!--- fix the returned data to be a proper signature --->
        <cfset var signature = ToBase64("#digest#")>

        <cfreturn signature>
    </cffunction>

    <cffunction name="SendMFA" access="private" returnformat="JSON" output="true" hint="Send an MFA Code now">
        <cfargument name="inpMFACode" TYPE="string"/>
        <cfargument name="MFAContactString_vch" TYPE="string"/>
        <cfargument name="MFAContactType_ti" TYPE="string"/>
        <cfargument name="MFAEXT_vch" TYPE="string"/>

        <cfset var dataout = {} />
        <cfset var variables = {} />
        <cfset var returnStruct = {} />
        <cfset var PostResultJSON = '' />
        <cfset var RetVarGetOperatorId = '' />
        <cfset var RetVarGetOperatorIdFromService = '' />

        <cfset dataout.RAWJSON = '' />
        <cfset dataout.SMSINIT = '-1' />
        <cfset dataout.SMSRESULTMESSAGE = '' />

        <!--- From the prod@siremobile.com production account - if this changes this will need to be updated --->
        <cfset variables.accessKey = '8A8C2A51BC345F301417' />
        <cfset variables.secretKey = '1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11' />

        <cfset variables.sign = generateSignature("POST", variables.secretKey) /> <!---Verb must match that of request type--->
        <cfset var datetime = variables.sign.DATE>
        <cfset var signature = variables.sign.SIGNATURE>
        <cfset var authorization = variables.accessKey & ":" & signature>
        <cfset var InsertToAPILog = '' />
        <cfset var inpContactString = '' />
        <cfset var userInfo = '' />
        <cfset var CheckBlockedCarrierRs = '' />
        <cfset var inpCarrier = 0 />

        <!---Find and replace all non numerics except P X * #--->
        <cfset inpContactString = REReplaceNoCase(arguments.MFAContactString_vch, "[^\d^\*^P^X^##]", "", "ALL")>

        <!--- Clean up where start character is a 1 --->
        <cfif LEFT(inpContactString, 1) EQ "1">
            <cfset inpContactString = RemoveChars(inpContactString, 1, 1)>
        </cfif>

        <!--- Search list(s) for last carrier info --->
        <cfinvoke component="session.cfc.csc.csc" method="GetOperatorId" returnvariable="RetVarGetOperatorId" >
            <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
        </cfinvoke>

        <cfinvoke method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

        <cfif RetVarGetOperatorId.OPERATORID GT 0>
            <cfset inpCarrier = RetVarGetOperatorId.OPERATORID />
        <cfelse>

            <!--- Next search carrier --->
            <cfinvoke component="session.cfc.csc.csc" method="GetOperatorIdFromService" returnvariable="RetVarGetOperatorIdFromService" >
                <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
            </cfinvoke>

            <cfif RetVarGetOperatorIdFromService.OPERATORID GT 0>
                <cfset inpCarrier = RetVarGetOperatorIdFromService.OPERATORID />

                <!--- Add to generic list for future faster/cheaper search  --->
                <cfinvoke component="session.cfc.csc.csc" method="SaveOperatorId">
                    <cfinvokeargument name="inpContactString" value="#TRIM(inpContactString)#">
                    <cfinvokeargument name="inpOperatorId" value="#TRIM(inpCarrier)#">
                </cfinvoke>

            </cfif>

        </cfif>

        <cfif inpCarrier LTE 0>
            <!--- Create log user authenticate with invalid number --->
            <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                <cfinvokeargument name="moduleName" value="MFA authentication Failure">
                <cfinvokeargument name="operator" value="#Session.USERID#">
            </cfinvoke>

            <cfset dataout.SMSINIT = -2 />
            <cfset dataout.SMSRESULTMESSAGE = "Invalid phone number" />
            <cfreturn dataout/>
        <cfelse>
            <cfinvoke method="CheckBlockedCarrier" returnvariable="CheckBlockedCarrierRs">
                <cfinvokeargument name="inpCarrier" value="#inpCarrier#"/>
            </cfinvoke>

            <cfif CheckBlockedCarrierRs.RXRESULTCODE EQ 1>
                <!--- Create log user authenticate with invalid number --->
                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="MFA authentication Failure">
                    <cfinvokeargument name="operator" value="#Session.USERID#">
                </cfinvoke>
            </cfif>

            <cfif CheckBlockedCarrierRs.RXRESULTCODE EQ 0 OR (CheckBlockedCarrierRs.RXRESULTCODE EQ 1 AND userInfo.ALLOWNOTPHONE GT 0)>
                <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
                <cfhttp url="https://api.siremobile.com/ire/secure/triggerSMS" method="POST" result="returnStruct" >

                   <!--- Required components --->

                   <!--- By default EBM API will return json or XML --->
                   <cfhttpparam name="Accept" type="header" value="application/json" />
                   <cfhttpparam type="header" name="datetime" value="#datetime#" />
                   <cfhttpparam type="header" name="authorization" value="#authorization#" />

                   <!--- Batch Id controls which pre-defined campaign to run --->
                   <cfhttpparam type="formfield" name="inpBatchId" value="160745" />

                   <!--- Contact string--->
                   <cfhttpparam type="formfield" name="inpContactString" value="#MFAContactString_vch#" />

                   <!--- 1=voice 2=email 3=SMS--->
                   <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />


                   <!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
                   <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />

                   <!--- Optional Components --->

                   <!--- Custom data element for PDC Batch Id 1135 --->
                   <cfhttpparam type="formfield" name="inpMFACode" value="#inpMFACode#" />

                </cfhttp>


                <!--- TODO:****
                    Parse Result for STOP Requests?

                    Let user know they have requested a stop - then tell them to opt back in



                    Parse results for successful send... warn user if failure

                --->

                <cfset dataout.RAWJSON = returnStruct.Filecontent />
            </cfif>
        </cfif>

        <cftry>


	        <cfif IsJSON(dataout.RAWJSON)>
                <cfset PostResultJSON = deSerializeJSON(dataout.RAWJSON) />

                <cfset dataout.SMSINIT = PostResultJSON.SMSINIT />
                <cfset dataout.SMSRESULTMESSAGE = PostResultJSON.MESSAGE />
	            <cfif dataout.SMSINIT NEQ 1>
                    <!--- Create log user authenticate with invalid number --->
                    <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                        <cfinvokeargument name="moduleName" value="MFA authentication Failure">
                        <cfinvokeargument name="operator" value="#Session.USERID#">
                    </cfinvoke>
                </cfif>
	        </cfif>


        <cfcatch type="any">


        </cfcatch>
        </cftry>

        <!--- Insert into Log --->
         <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    2000,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendMFA">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendMFA Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={1227} #MFAContactString_vch#, #MFAContactType_ti#, #MFAEXT_vch#, #inpMFACode#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={1227}">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                )
            </cfquery>

           <cfreturn dataout>

    </cffunction>

    <!--- Generate random strings of specified length --->
    <cffunction name="RandString" output="no" returntype="string">
        <cfargument name="length" type="numeric" required="yes">

        <!--- Local vars --->
        <cfset var result="">
        <cfset var i=0>

        <!--- Create string --->
        <cfloop index="i" from="1" to="#ARGUMENTS.length#">
            <!--- Random character in range A-Z --->
            <cfset result=result&Chr(RandRange(65, 90))>
        </cfloop>

        <!--- Return it --->
        <cfreturn result>
    </cffunction>

    <cffunction name="getUserShortCodeByUserId" access="public" output="true">
        <cfargument name="UserId" type="numeric" required="yes">
        <cfset var getShortCode = '' />
        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1>
        <cfset dataout.MESSAGE = ''>
        <cfset dataout.ERRMESSAGE = ''>
        <cfset dataout.SHORTCODE = ''>

        <cfif UserId GT 0>
            <cftry>
                <cfquery datasource="#Session.DBSourceREAD#" name="getShortCode">
                        SELECT
                            sc.ShortCodeId_int,sc.ShortCode_vch,scr.RequesterId_int
                        FROM
                            sms.shortcode sc
                        JOIN
                            sms.shortcoderequest scr
                        ON
                            sc.ShortCodeId_int = scr.ShortCodeId_int
                        WHERE
                            scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.UserId#">
                </cfquery>

                <cfif getShortCode.RecordCount GT 0>
                        <cfset dataout.RXRESULTCODE = 1 />
                        <cfset dataout.SHORTCODE = getShortCode.ShortCode_vch>
                        <cfset dataout.SHORTCODEID = getShortCode.ShortCodeId_int>
                    <cfelse>
                        <cfset dataout.RXRESULTCODE = -1 />
                        <cfset dataout.MESSAGE = "No shortcode found." />
                        <cfset dataout.ERRMESSAGE = "" />
                </cfif>

                <cfcatch type="any">
                    <cfset dataout = {} />
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                    <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
                </cfcatch>
            </cftry>

         <cfelse>
            <cfset dataout.RXRESULTCODE = -1 >
            <cfset dataout.MESSAGE = 'User Id less than zero.'>
            <cfset dataout.ERRMESSAGE = 'User Id less than zero.'>
            <cfreturn dataout>
        </cfif>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="CheckFirstLogin" output="true" hint="Check if it is the first login">
        <cfargument name="InpUserId" type="numeric" required="yes" />

        <cfset var checkLogedIn = '' />

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfif arguments.InpUserId GT 0>
            <cftry>
                <CFQUERY name="checkLogedIn" datasource="#Session.DBSourceREAD#">
                    SELECT
                        FirstLoggedIn_int
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.InpUserId#">
                    LIMIT
                        1
                </CFQUERY>

                <cfif checkLogedIn.RECORDCOUNT GT 0 && checkLogedIn.FirstLoggedIn_int EQ 0>
                    <cfset dataout.RXRESULTCODE = 1>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = 0>
                </cfif>
                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -2 />
                    <cfset dataout.MESSAGE = "Failed to check User First Login" />
                    <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "UserId not exits" />
        </cfif>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="UpdateFirstLogedIn" output="true" hint="Update field FirstLoggedIn_int value when user logged in">
        <cfargument name="InpUserId" type="numeric" required="true">

        <cfset var watchedLiveDemoVideo = '' />

        <cfset var dataout = {}>
        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfif arguments.InpUserId GT 0>
            <cftry>
                <CFQUERY name="watchedLiveDemoVideo" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        FirstLoggedIn_int = 1
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                </CFQUERY>

                <cfif watchedLiveDemoVideo.RECORDCOUNT GT 0>
                    <cfset dataout.RXRESULTCODE = 1>
                <cfelse>
                    <cfset dataout.RXRESULTCODE = 0>
                </cfif>

                <cfcatch type="any">
                    <cfset dataout.RXRESULTCODE = -2 />
                    <cfset dataout.MESSAGE = "Failed to update field!" />
                    <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "UserId not exits" />
        </cfif>
        <cfreturn dataout>
    </cffunction>

    <cffunction name="DeactiveAccount" access="remote" output="false" hint="Deactive an account">
        <cfargument name="inpUserId" required="true" type="numeric">
        <cfif Session.USERID LT 1>
            <cflocation url="/" />
        </cfif>
        <cfset var dataout = {} />
        <cfset var logEntry = {} />
        <cfset var deactiveAccount = '' />

        <cfset var userInfoQuery = '' />
        <cfset var userBillingQuery = '' />
        <cfset var userPlanQuery = '' />
        <cfset var userBatchsQuery = '' />
        <cfset var userShortcodeRequestQuery = '' />
        <cfset var userKeywordQuery = '' />
        <cfset var userContactQueue = '' />
        <cfset var userReferralQueue = '' />
        <cfset var userMLP = '' />
        <cfset var userShortUrl = '' />

        <cfset var retValUpdateUserBilling = '' />
        <cfset var retValUpdateUserPlan = '' />
        <cfset var retValUpdateUserBatchs = '' />
        <cfset var retValUpdateUserShortcodeRequest = '' />
        <cfset var retValUpdateUserKeyword = '' />
        <cfset var retValUpdateUserContactQueue = '' />
        <cfset var retValUpdateUserReferralQueue = '' />
        <cfset var retValUpdateUserMLP = '' />
        <cfset var retValUpdateUserShortUrl = '' />
        <cfset var retValSendSMS = '' />

        <cfset var retValDeactiveAccount = '' />
        <cfset var retValInsertDeactivateLog = '' />

        <cfset var activeCode = GenActiveCode(arguments.inpUserId) />
        <cfset var userWarningMail = {} />

        <cfset var keywordLog = '' />

        <cfset logEntry.User = {} />
        <cfset logEntry.User.UserId = arguments.inpUserId />
        <cfset logEntry.User.EmailAddress = '' />
        <cfset logEntry.User.MFAContactString = '' />
        <cfset logEntry.User.UserName = '' />

        <cfset logEntry.UserBilling = {} />
        <cfset logEntry.UserBilling.Balance = 0 />
        <cfset logEntry.UserBilling.BuyCreditBalance = 0 />
        <cfset logEntry.UserBilling.PromotionCreditBalance = 0 />

        <cfset logEntry.UserPlan = {} />
        <cfset logEntry.UserPlan.UserPlanId = 0 />
        <cfset logEntry.UserPlan.PlanId = 0 />
        <cfset logEntry.UserPlan.EndDate = '' />

        <cfset logEntry.UserBatches = [] />
        <cfset logEntry.UserShortCodeRequestId = 0 />
        <cfset logEntry.UserKeywords = [] />
        <cfset logEntry.UserContactQueue = [] />
        <cfset logEntry.UserReferralQueue = [] />
        <cfset logEntry.UserMLP = [] />
        <cfset logEntry.UserShortUrl = [] />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cftry>

            <!--- ------------------------------------------GET USER LOG DATA------------------------------------------- --->

            <cfinvoke method="GetUserInfoById" returnvariable="userInfoQuery">
                <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#" />
            </cfinvoke>

            <!--- <cfquery name="userInfoQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    EmailAddress_vch,
                    MFAContactString_vch,
                    FirstName_vch,
                    LastName_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery> --->

            <cfif userInfoQuery.USERID LT 1>
                <cfthrow type="Database" message="Datebase error" detail="Failed to get user information">
            </cfif>

            <cfset logEntry.User.EmailAddress = userInfoQuery.FULLEMAIL />
            <cfset logEntry.User.MFAContactString = userInfoQuery.MFAPHONE />
            <cfset logEntry.User.UserName = userInfoQuery.USERNAME & ' ' & userInfoQuery.LASTNAME />

            <cfquery name="userBillingQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    Balance_int,
                    BuyCreditBalance_int,
                    PromotionCreditBalance_int
                FROM
                    simplebilling.billing
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                LIMIT 1
            </cfquery>

            <cfif userBillingQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Failed to get user billing info">
            </cfif>

            <cfset logEntry.UserBilling.Balance = userBillingQuery.Balance_int />
            <cfset logEntry.UserBilling.BuyCreditBalance = userBillingQuery.BuyCreditBalance_int />
            <cfset logEntry.UserBilling.PromotionCreditBalance = userBillingQuery.PromotionCreditBalance_int />

            <cfquery name="userPlanQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    UserPlanId_bi,
                    PlanId_int,
                    EndDate_dt
                FROM
                    simplebilling.userplans
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                ORDER BY
                    UserPlanId_bi DESC
                LIMIT 1
            </cfquery>

            <cfif userPlanQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Fail to get userplan">
            </cfif>

            <cfset logEntry.UserPlan.UserPlanId = userPlanQuery.UserPlanId_bi />
            <cfset logEntry.UserPlan.PlanId = userPlanQuery.PlanId_int />
            <cfset logEntry.UserPlan.EndDate = "#dateFormat(userPlanQuery.EndDate_dt, "yyyy-mm-dd")# #timeFormat(userPlanQuery.EndDate_dt, "HH:mm:ss")#" />

            <cfquery name="userBatchsQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    BatchId_bi
                FROM
                    simpleobjects.batch
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userBatchsQuery.RECORDCOUNT GT 0>
                <cfloop query="userBatchsQuery">
                    <cfset arrayAppend(logEntry.UserBatches, BatchId_bi) />
                </cfloop>
            </cfif>

            <cfquery name="userShortcodeRequestQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    ShortCodeRequestId_int
                FROM
                    sms.shortcoderequest
                WHERE
                    RequesterId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                ORDER BY
                    ShortCodeRequestId_int DESC
                LIMIT 1
            </cfquery>

            <cfif userShortcodeRequestQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Failed to get user shortcode request">
            </cfif>

            <cfset logEntry.UserShortCodeRequestId = userShortcodeRequestQuery.ShortCodeRequestId_int />

            <cfquery name="userKeywordQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    KeywordId_int,
                    Keyword_vch
                FROM
                    sms.keyword
                WHERE
                    ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userKeywordQuery.RECORDCOUNT GT 0>
                <cfloop query="userKeywordQuery">
                    <cfset var temp = {} />
                    <cfset temp.KeywordId = KeywordId_int />
                    <cfset temp.Keyword = Keyword_vch />
                    <cfset arrayAppend(logEntry.UserKeywords, temp) />
                    <cfset keywordLog = keywordLog & '#Keyword_vch#, ' />
                </cfloop>
            </cfif>

            <cfquery name="userContactQueue" datasource="#session.DBSourceREAD#">
                SELECT
                    DTSId_int
                FROM
                    simplequeue.contactqueue
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userContactQueue.RECORDCOUNT GT 0>
                <cfloop query="userContactQueue">
                    <cfset arrayAppend(logEntry.UserContactQueue, DTSId_int) />
                </cfloop>
            </cfif>

            <cfquery name="userReferralQueue" datasource="#session.DBSourceREAD#">
                SELECT
                    Id_int
                FROM
                    simplequeue.sire_referral_msg_queue
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userReferralQueue.RECORDCOUNT GT 0>
                <cfloop query="userReferralQueue">
                    <cfset arrayAppend(logEntry.UserReferralQueue, Id_int) />
                </cfloop>
            </cfif>

            <cfquery name="userMLP" datasource="#session.DBSourceREAD#">
                SELECT
                    cppxURL_vch
                FROM
                    simplelists.cppx_data
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                AND
                    MlpType_ti = 1
            </cfquery>

            <cfif userMLP.RECORDCOUNT GT 0>
                <cfloop query="userMLP">
                    <cfset arrayAppend(logEntry.UserMLP, cppxURL_vch) />
                </cfloop>
            </cfif>

            <cfquery name="userShortUrl" datasource="#session.DBSourceREAD#">
                SELECT
                    ShortURL_vch
                FROM
                    simplelists.url_shortner
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                AND
                    Type_ti = 1
            </cfquery>

            <cfif userShortUrl.RECORDCOUNT GT 0>
                <cfloop query="userShortUrl">
                    <cfset arrayAppend(logEntry.UserShortUrl, ShortURL_vch) />
                </cfloop>
            </cfif>

            <!--- --------------------------------------------UPDATE USER DATA--------------------------------------------- --->
            <cftransaction>
                <cfquery datasource="#session.DBSourceEBM#" result="retValDeactiveAccount">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate useraccount">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserBilling">
                    UPDATE
                        simplebilling.billing
                    SET
                        Balance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">,
                        BuyCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">,
                        PromotionCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Reduce billing balance. Balance = #userBillingQuery.Balance_int#, BuyCreditBalance = #userBillingQuery.BuyCreditBalance_int#, PromotionCreditBalance = #userBillingQuery.PromotionCreditBalance_int# ">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserPlan">
                    UPDATE
                        simplebilling.userplans
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserPlan.UserPlanId#">
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate userplan. UserPlanId = #userPlanQuery.UserPlanId_bi#, DefaultPlanId = #userPlanQuery.PlanId_int#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserBatchs">
                    UPDATE
                        simpleobjects.batch
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user batches. Batches id = #arrayToList(logEntry.UserBatches, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserShortcodeRequest">
                    UPDATE
                        sms.shortcoderequest
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                    AND
                        Status_int= <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user shortcode request. ShortcodeRequestId = #logEntry.UserShortCodeRequestId#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserKeyword">
                    UPDATE
                        sms.keyword
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user keyword. Keywords = #keywordLog#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserContactQueue">
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="7"> -- 7 is stop status
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user contactqueue. ContactQueue = #arrayToList(logEntry.UserContactQueue, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserReferralQueue">
                    UPDATE
                        simplequeue.sire_referral_msg_queue
                    SET
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="4"> -- 4 is stop status
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user referral queue. ReferralQueue = #arrayToList(logEntry.UserReferralQueue, ',')#">
                </cfinvoke>

                <cfquery name="userMLP" datasource="#session.DBSourceEBM#">
                    UPDATE
                        simplelists.cppx_data
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user MLP. MLP Key = #arrayToList(logEntry.UserMLP, ',')#">
                </cfinvoke>

                <cfquery name="userShortUrl" datasource="#session.DBSourceEBM#">
                    UPDATE
                        simplelists.url_shortner
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    AND
                        Type_ti = 1
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #arguments.inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user shortURL. ShortUrl Key = #arrayToList(logEntry.UserShortUrl, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValInsertDeactivateLog">
                    INSERT INTO
                        simpleobjects.deactivate_logs
                        (
                            AdminId_int,
                            UserId_int,
                            LogData_vch,
                            Status_int,
                            ActiveCode_vch
                        )
                    VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJSON(logEntry)#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#activeCode#">
                        )
                </cfquery>

                <cfset userWarningMail = {
                    username:logEntry.User.UserName,
                    ActiveCode:activeCode
                }/>

                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                        <cfinvokeargument name="to" value="#logEntry.User.EmailAddress#">
                        <cfinvokeargument name="type" value="html">
                        <cfinvokeargument name="subject" value="Your account has been suspended">
                        <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_deactivate_account.cfm">
                        <cfinvokeargument name="data" value="#TRIM(serializeJSON(userWarningMail))#">
                </cfinvoke>

                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                        <cfinvokeargument name="to" value="#SupportEMailUserName#">
                        <cfinvokeargument name="type" value="html">
                        <cfinvokeargument name="subject" value="Admin deactivate account">
                        <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_deactivate_account_report.cfm">
                        <cfinvokeargument name="data" value="#TRIM(serializeJSON(logEntry))#">
                </cfinvoke>

                <cfinvoke method="SendDeactivateWarning" returnvariable="retValSendSMS">
                    <cfinvokeargument name="inpActiveCode" value="#activeCode#"/>
                    <cfinvokeargument name="MFAContactString_vch" value="#userInfoQuery.MFAPHONE#"/>
                </cfinvoke>

                <cfset dataout.RXRESULTCODE = 1>
            </cftransaction>

            <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
                <cfinvokeargument name="InpEmailString" value="#logEntry.User.EmailAddress#">
                <cfinvokeargument name="InpStatus" value="cancelled">
                <cfinvokeargument name="InpCancelDate" value="#dateFormat(now(), 'mm/dd/yyyy')#">
            </cfinvoke>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Failed to deactive account!">
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
            </cfcatch>
        </cftry>
        <cfset dataout.log = logEntry />
        <cfreturn dataout />
    </cffunction>

    <cffunction name="ActiveAccount" access="remote" output="false" hint="Active an account">
        <cfargument name="inpUserId" required="true" type="numeric">
        <cfif Session.USERID LT 1>
            <cflocation url="/" />
        </cfif>
        <cfset var dataout = {} />
        <cfset var deactivateLog = '' />
        <cfset var logEntry = '' />
        <cfset var availableAccount = '' />
        <cfset var userInfo = '' />
        <cfset var getPlan = '' />

        <cfset var retValActiveAccount = '' />
        <cfset var retValActiveBatchs = '' />
        <cfset var retValActiveKeywords = '' />
        <cfset var retValActiveShortCodeRequest = '' />
        <cfset var retValActiveUserPlan = '' />
        <cfset var retValActiveBilling = '' />
        <cfset var retValActiveContactQueue = '' />
        <cfset var retValActiveReferralQueue = '' />
        <cfset var retValUpdateDeactivateLog = '' />
        <cfset var retValInsertPlan = '' />
        <cfset var retValUpdateBilling = ''/>

        <cfset var availableKeywords = '' />
        <cfset var availableMLP = '' />
        <cfset var availableShortUrl = '' />

        <cfset var i = '' />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />

        <cftry>

            <cfquery name="userInfo" datasource="#session.DBSourceREAD#">
                SELECT
                    UserId_int
                FROM
                    simpleobjects.useraccount
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
            </cfquery>

            <cfif userInfo.RECORDCOUNT LT 1>
                <cfthrow type="Application" message="Failed to active account" detail="Account is not deactivated">
            </cfif>

            <cfquery name="deactivateLog" datasource="#session.DBSourceREAD#">
                SELECT
                    LogId_int,
                    LogData_vch
                FROM
                    simpleobjects.deactivate_logs
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif deactivateLog.RECORDCOUNT NEQ 1>
                <cfthrow type="Application" message="Application error" detail="This user is deactived from third party. Your action is denined.">
            </cfif>

            <cfset logEntry = deserializeJSON(deactivateLog.LogData_vch) />

            <cfquery name="availableAccount" datasource="#session.DBSourceREAD#">
                SELECT
                    EmailAddress_vch,
                    MFAContactString_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    (
                        EmailAddress_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.User.EmailAddress#">
                        AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    )
                AND
                    UserType_int <> 2
                <!--- OR
                    (
                        MFAContactString_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.User.MFAContactString#">
                        AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    ) --->
            </cfquery>

            <cfif availableAccount.RECORDCOUNT GT 0>
                <cfthrow type="Application" message="Active failed!" detail="Email address or Phone number is not available">
            </cfif>

            <cfif arrayLen(logEntry.UserKeywords) GT 0>
                <cfquery name="availableKeywords" datasource="#session.DBSourceREAD#">
                    SELECT
                        Keyword_vch
                    FROM
                        sms.keyword
                    WHERE
                        Keyword_vch IN (
                                <cfloop from="1" to="#arrayLen(logEntry.UserKeywords)#" index="i">
                                    <cfif i NEQ arrayLen(logEntry.UserKeywords)>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserKeywords[i].Keyword#">,
                                        </cfoutput>
                                    <cfelse>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserKeywords[i].Keyword#">
                                        </cfoutput>
                                    </cfif>
                                </cfloop>
                            )
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    AND
                        ShortCodeRequestId_int != <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                </cfquery>

                <cfif availableKeywords.RECORDCOUNT GT 0>
                    <cfloop query="availableKeywords">
                        <cfloop array="#logEntry.UserKeywords#" index="i">
                            <cfif i.Keyword EQ Keyword_vch>
                                <cfset arrayDelete(logEntry.UserKeywords, i) />
                            </cfif>
                        </cfloop>
                    </cfloop>
                </cfif>
            </cfif>

            <cfif arrayLen(logEntry.UserMLP) GT 0>
                <cfquery name="availableMLP" datasource="#session.DBSourceREAD#">
                    SELECT
                        cppxURL_vch
                    FROM
                        simplelists.cppx_data
                    WHERE
                        cppxURL_vch IN (
                                <cfloop from="1" to="#arrayLen(logEntry.UserMLP)#" index="i">
                                    <cfif i NEQ arrayLen(logEntry.UserMLP)>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserMLP[i]#">,
                                        </cfoutput>
                                    <cfelse>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserMLP[i]#">
                                        </cfoutput>
                                    </cfif>
                                </cfloop>
                            )
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    AND
                        UserId_int != <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>

                <cfif availableMLP.RECORDCOUNT GT 0>
                    <cfloop query="availableMLP">
                        <cfset arrayDelete(logEntry.UserMLP, cppxURL_vch) />
                    </cfloop>
                </cfif>
            </cfif>

            <cfif arrayLen(logEntry.UserShortUrl) GT 0>
                <cfquery name="availableShortUrl" datasource="#session.DBSourceREAD#">
                    SELECT
                        ShortURL_vch
                    FROM
                        simplelists.url_shortner
                    WHERE
                        ShortURL_vch IN (
                                <cfloop from="1" to="#arrayLen(logEntry.UserShortUrl)#" index="i">
                                    <cfif i NEQ arrayLen(logEntry.UserShortUrl)>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserShortUrl[i]#">,
                                        </cfoutput>
                                    <cfelse>
                                        <cfoutput>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserShortUrl[i]#">
                                        </cfoutput>
                                    </cfif>
                                </cfloop>
                            )
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    AND
                        UserId_int != <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Type_ti = 1
                </cfquery>

                <cfif availableShortUrl.RECORDCOUNT GT 0>
                    <cfloop query="availableShortUrl">
                        <cfset arrayDelete(logEntry.UserShortUrl, ShortURL_vch) />
                    </cfloop>
                </cfif>
            </cfif>

            <!--- -------------------------------------------Undo deactive account----------------------------------------------- --->

            <cftransaction>
                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveAccount">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                </cfquery>

                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveBilling">
                    UPDATE
                        simplebilling.billing
                    SET
                        Balance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#logEntry.UserBilling.Balance#">,
                        BuyCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#logEntry.UserBilling.BuyCreditBalance#">,
                        PromotionCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#logEntry.UserBilling.PromotionCreditBalance#">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                </cfquery>

                <cfif dateCompare(now(), logEntry.UserPlan.EndDate) LT 0>
                    <cfquery datasource="#session.DBSourceEBM#" result="retValActiveUserPlan">
                        UPDATE
                            simplebilling.userplans
                        SET
                            Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                        WHERE
                            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                        AND
                            Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                        AND
                            UserPlanId_bi = (SELECT tmp.UserPlanId
                                              FROM (SELECT max(UserPlanId_bi) AS UserPlanId
                                                    FROM simplebilling.userplans
                                                    WHERE UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">) tmp
                                            )
                        LIMIT 1
                    </cfquery>
                <cfelse>
                    <!--- Free plan --->
                    <cfinvoke method="GetOrderPlan" component="session/sire/models/cfc/order_plan" returnvariable="getPlan">
                        <cfinvokeargument name="plan" value="1"/>
                    </cfinvoke>
                    <cfset var todayDate = Now()>
                    <cfset var startDay = Day(todayDate)>

                    <cfif startDay GT 28>
                        <cfset startDay = 28>
                    </cfif>
                    <cfset var startMonth = Month(todayDate)>
                    <cfset var startYear = Year(todayDate)>

                    <cfset var startDate = CreateDate(startYear, startMonth, startDay)>

                    <cfset var endDate = DateAdd("m",1,startDate)>
                    <cfquery datasource="#Session.DBSourceEBM#" result="retValInsertPlan">
                        INSERT INTO simplebilling.userplans
                        (
                            Status_int,
                            UserId_int,
                            PlanId_int,
                            Amount_dec,
                            UserAccountNumber_int,
                            KeywordsLimitNumber_int,
                            FirstSMSIncluded_int,
                            PriceMsgAfter_dec,
                            PriceKeywordAfter_dec,
                            StartDate_dt,
                            EndDate_dt,
                            MlpsLimitNumber_int,
                            ShortUrlsLimitNumber_int
                        )
                        VALUES
                        (
                            1,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.PLANID#">,
                            <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.AMOUNT#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.USERACCOUNTNUMBER#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.KEYWORDSLIMITNUMBER#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.FIRSTSMSINCLUDED#">,
                            <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEMSGAFTER#">,
                            <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEKEYWORDAFTER#">,
                            <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startDate#">,
                            <cfqueryparam cfsqltype="CF_SQL_DATE" value="#endDate#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.MLPSLIMITNUMBER#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.SHORTURLSLIMITNUMBER#">
                        )
                    </cfquery>

                    <!--- RESET BALANCE TO FREE PLAN --->
                    <cfquery datasource="#Session.DBSourceEBM#" result="retValUpdateBilling">
                        UPDATE
                            simplebilling.billing
                        SET
                            Balance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.FIRSTSMSINCLUDED#">
                        WHERE
                            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    </cfquery>

                </cfif>

                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveBatchs">
                    UPDATE
                        simpleobjects.batch
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                </cfquery>

                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveShortCodeRequest">
                    UPDATE
                        sms.shortcoderequest
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    WHERE
                        RequesterId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                </cfquery>

                <cfif arrayLen(logEntry.UserKeywords) GT 0>
                    <cfquery datasource="#session.DBSourceEBM#" result="retValActiveReferralQueue">
                        UPDATE
                            sms.keyword
                        SET
                            Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                        WHERE
                            KeywordId_int IN (
                                    <cfloop from="1" to="#arrayLen(logEntry.UserKeywords)#" index="i">
                                        <cfif i NEQ arrayLen(logEntry.UserKeywords)>
                                            <cfoutput>
                                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserKeywords[i].KeywordId#">,
                                            </cfoutput>
                                        <cfelse>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserKeywords[i].KeywordId#">
                                        </cfif>
                                    </cfloop>
                                )
                        AND
                            ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                        AND
                            Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    </cfquery>
                </cfif>

                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveContactQueue">
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="7">
                </cfquery>

                <cfquery datasource="#session.DBSourceEBM#" result="retValActiveReferralQueue">
                    UPDATE
                        simplequeue.sire_referral_msg_queue
                    SET
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                    AND
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="4">
                </cfquery>

                <cfif arrayLen(logEntry.UserMLP) GT 0>
                    <cfquery datasource="#session.DBSourceEBM#" result="retValActiveReferralQueue">
                        UPDATE
                            simplelists.cppx_data
                        SET
                            Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                        WHERE
                            cppxURL_vch IN (
                                    <cfloop from="1" to="#arrayLen(logEntry.UserMLP)#" index="i">
                                        <cfif i NEQ arrayLen(logEntry.UserMLP)>
                                            <cfoutput>
                                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserMLP[i]#">,
                                            </cfoutput>
                                        <cfelse>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserMLP[i]#">
                                        </cfif>
                                    </cfloop>
                                )
                        AND
                            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                        AND
                            Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    </cfquery>
                </cfif>

                <cfif arrayLen(logEntry.UserShortUrl) GT 0>
                    <cfquery datasource="#session.DBSourceEBM#" result="retValActiveReferralQueue">
                        UPDATE
                            simplelists.url_shortner
                        SET
                            Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                        WHERE
                            ShortURL_vch IN (
                                    <cfloop from="1" to="#arrayLen(logEntry.UserShortUrl)#" index="i">
                                        <cfif i NEQ arrayLen(logEntry.UserShortUrl)>
                                            <cfoutput>
                                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserShortUrl[i]#">,
                                            </cfoutput>
                                        <cfelse>
                                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#logEntry.UserShortUrl[i]#">
                                        </cfif>
                                    </cfloop>
                                )
                        AND
                            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
                        AND
                            Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                        AND
                            Type_ti = 1
                    </cfquery>
                </cfif>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateDeactivateLog">
                    UPDATE
                        simpleobjects.deactivate_logs
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        LogId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#deactivateLog.LogId_int#">
                </cfquery>
            </cftransaction>

            <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
                <cfinvokeargument name="InpEmailString" value="#logEntry.User.EmailAddress#">
                <cfinvokeargument name="InpStatus" value="active">
            </cfinvoke>

            <cfset dataout.RXRESULTCODE = 1 />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = "Failed to active account!">
                <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="GenActiveCode" access="private" output="false" hint="Gennerate an random string for active code">
        <cfargument name="inpUserId" required="false" default="#session.UserId#">
        <cfset var result = ''/>
        <!--- Encrypt --->
        <cfset result = hash(gettickcount()&'_'&arguments.inpUserId)>
        <cfreturn result />
    </cffunction>

    <cffunction name="UserActiveAccount" access="public" output="false" hint="Get active code from user and active account">
        <cfargument name="inpActiveCode" required="true" type="string">

        <cfset var dataout = {} />
        <cfset var logEntry = '' />
        <cfset var activeAccount = '' />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.ACTIVE = -1 />

        <cftry>
            <cfquery name="logEntry" datasource="#session.DBSourceREAD#">
                SELECT
                    UserId_int
                FROM
                    simpleobjects.deactivate_logs
                WHERE
                    ActiveCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpActiveCode#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif logEntry.RECORDCOUNT NEQ 1>
                <cfthrow type="Database" message="Reactive failed" detail="Active code not available">
            </cfif>

            <cfinvoke method="ActiveAccount" component="public.sire.models.cfc.userstools" returnvariable="activeAccount">
                <cfinvokeargument name="inpUserId" value="#logEntry.UserId_int#">
            </cfinvoke>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.ACTIVE = activeAccount.RXRESULTCODE />
            <cfset dataout.MESSAGE = activeAccount.MESSAGE />
            <cfset dataout.ERRMESSAGE = activeAccount.ERRMESSAGE />
            <cfset dataout.TYPE = activeAccount.TYPE />

            <cfcatch type="any">
                <cfset dataout.MESSAGE = 'Reactive failed' />
                <cfset dataout.ERRMESSAGE = cfcatch.MESSAGE & ' ' & cfcatch.DETAIL />
                <cfset dataout.TYPE = cfcatch.TYPE />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="SendDeactivateWarning" access="private" output="false" hint="Send an deactivate warning to user">
        <cfargument name="inpActiveCode" TYPE="string"/>
        <cfargument name="MFAContactString_vch" TYPE="string"/>

        <cfset var dataout = {} />
        <cfset var variables = {} />
        <cfset var returnStruct = {} />
        <cfset var apiUrl = '' />

        <!--- From the prod@siremobile.com production account - if this changes this will need to be updated --->
        <cfset variables.accessKey = '8A8C2A51BC345F301417' />
        <cfset variables.secretKey = '1091c2F+b59dD076Ea52EF531a245f0Cb9E46d11' />

        <cfset variables.sign = generateSignature("POST", variables.secretKey) /> <!---Verb must match that of request type--->
        <cfset var datetime = variables.sign.DATE>
        <cfset var signature = variables.sign.SIGNATURE>
        <cfset var authorization = variables.accessKey & ":" & signature>
        <cfset var InsertToAPILog = '' />

        <cfset var reActiveLinkLong = "https://#CGI.SERVER_NAME#/reactive?ac=#arguments.inpActiveCode#">
        <cfset var reActiveLink = '' />

        <cfinvoke method="GetShortUrl" component="session.sire.models.cfc.waitlistapp" returnvariable="reActiveLink">
            <cfinvokeargument name="inpLongUrl" value="#reActiveLinkLong#" />
        </cfinvoke>

        <cfset reActiveLink = reActiveLink.shortUrl />

        <cfif FINDNOCASE(CGI.SERVER_NAME,'siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'www.siremobile.com') GT 0 OR FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 >
            <cfset apiUrl = "https://api.siremobile.com/ire/secure/triggerSMS" />
        <cfelse>
            <cfset apiUrl = "https://apiqa.siremobile.com/ire/secure/triggerSMS" />
        </cfif>

        <!--- The method="XXX" used in the http request must match the method used to generate key in the generateSignature() function--->
        <cfhttp url="#apiUrl#" method="POST" result="returnStruct" >

           <!--- Required components --->

           <!--- By default EBM API will return json or XML --->
           <cfhttpparam name="Accept" type="header" value="application/json" />
           <cfhttpparam type="header" name="datetime" value="#datetime#" />
           <cfhttpparam type="header" name="authorization" value="#authorization#" />

           <!--- Batch Id controls which pre-defined campaign to run --->
           <cfhttpparam type="formfield" name="inpBatchId" value="#_DeactiveWarningBatch#" />

           <!--- Contact string--->
           <cfhttpparam type="formfield" name="inpContactString" value="#arguments.MFAContactString_vch#" />

           <!--- 1=voice 2=email 3=SMS--->
           <cfhttpparam type="formfield" name="inpContactTypeId" value="3" />

           <!--- ***TODO: Remove this after adding parse for DNC STOP Requests --->
           <cfhttpparam type="formfield" name="inpSkipLocalUserDNCCheck" value="1" />

           <!--- Optional Components --->

           <!--- Custom data element for PDC Batch Id 1135 --->
           <cfhttpparam type="formfield" name="inpReActiveLink" value="#reActiveLink#" />

        </cfhttp>

        <cfset dataout = returnStruct.Filecontent />

        <!--- Insert into Log --->
         <cfquery name="InsertToAPILog" datasource="#Session.DBSourceEBM#">
                INSERT INTO simplexresults.apitracking
                (
                    EventId_int,
                    Created_dt,
                    UserId_int,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    Output_vch,
                    DebugStr_vch,
                    DataSource_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
                VALUES
                (
                    2000,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendDeactivateWarning">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="SendDeactivateWarning Debugging Info">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="See Query String and Result">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#_DeactiveWarningBatch#} #MFAContactString_vch#, #reActiveLink#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="BatchId={#_DeactiveWarningBatch#}">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#Session.DBSourceEBM#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_HOST),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.HTTP_USER_AGENT),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.PATH_TRANSLATED),2024)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_LONGVARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
                )
            </cfquery>

           <cfreturn dataout>
    </cffunction>

    <cffunction name="CheckUserActive" access="public" output="false" hint="Check if user is not deactivated">
        <cfargument name="inpUserId" required="false" default="#session.UserId#"/>

        <cfset var dataout = {} />
        <cfset var userQuery = '' />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset dataout.ISACTIVE = 0 />

        <cftry>
            <cfquery name="userQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    UserId_int
                FROM
                    simpleobjects.useraccount
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserId#"/>
                AND
                    Active_int = 1
            </cfquery>

            <cfif userQuery.RECORDCOUNT GT 0>
                <cfset dataout.ISACTIVE = 1/>
            </cfif>

            <cfset dataout.RXRESULTCODE = 1/>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.message />
                <cfset dataout.ERRMESSAGE = cfcatch.detail />
                <cfset dataout.TYPE = cfcatch.type />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="ValidateUserStep1" access="remote" hint="Validate step 1 when user sign up">
        <cfargument name="inpEmail" required="true"/>
        <cfargument name="inpPhone" required="true"/>
        <cfargument name="inpPassword" required="true"/>
        <cfargument name="INPCBPID" required="no" default="1" type="string">
        <cfargument name="inpAllowDuplicatePhone" required="false" default="0"/>

        <cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.MESSAGE = '' />
        <cfset var VerifyUnique = ''/>
        <cfset var safeeMail = ''/>
        <cfset var safePass = ''/>
        <cfset var UpdateLastLastUserIp = ''/>
        <cfset var VerifySMS_Number = {}/>
        <cfset var DuplicateSMS_Number = {}/>
        <cfset var WhiteListPhoneNumber = {}/>

        <cfset var INSMSNUMBER = TRIM(arguments.inpPhone)>

        <cftry>
            <!--- Validate email address --->
            <cfinvoke method="VldEmailAddress" returnvariable="safeeMail">
                <cfinvokeargument name="Input" value="#arguments.inpEmail#"/>
            </cfinvoke>
            <cfif safeeMail NEQ true OR arguments.inpEmail EQ "">
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'Invalid Email.'>
                <cfreturn dataout>
            </cfif>

            <!--- Valid US Phone Number --->
            <cfif NOT isvalid('telephone', arguments.inpPhone)>
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'Phone Number is not a valid US telephone number!'>
                <cfreturn dataout>
            </cfif>

            <!--- Validate Email Unique --->
            <cfquery name="VerifyUnique" datasource="#Session.DBSourceREAD#">
                SELECT
                    EmailAddress_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpEmail#">
                AND
                    CBPID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.INPCBPID#">
                AND
                    Active_int > 0
            </cfquery>
            <cfif VerifyUnique.RecordCount GT 0>
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'The email address you entered is already in use and cannot be used at this time. Please try another.'>
                <cfreturn dataout>
            </cfif>

            <!--- Validate proper password--->
            <cfinvoke  method="VldInput" returnvariable="safePass">
                <cfinvokeargument name="Input" value="#arguments.inpPassword#"/>
            </cfinvoke>

            <cfif safePass NEQ true OR arguments.inpPassword EQ "">
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'Invalid Password. Accept only : Alphabet, numbers and _ *%$@!?+-'>
                <cfreturn dataout>
            </cfif>

            <cfquery name="VerifySMS_Number" datasource="#Session.DBSourceREAD#">
                SELECT
                    IdPhone
                FROM
                    simpleobjects.phone_number_blocked
                WHERE
                    PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
            </cfquery>
            <cfif VerifySMS_Number.RecordCount GT 0>
                 <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.USERID = '-1'>
                <cfset dataout.MESSAGE = 'We were unable to send the verification code to the phone number you entered.<br>Please try the following:<br>*  Make sure the number was entered correctly.<br>*  Ask your mobile phone carrier to enable messages from short codes<br>*  Use a different number with your account<br>Contact Customer Support for further assistance.'>
                <cfreturn dataout>
            </cfif>

            <!--- Duplicate Phone Number & White Phone Number List --->
            <cfif arguments.inpAllowDuplicatePhone EQ "0">
                <cfquery name="DuplicateSMS_Number" datasource="#Session.DBSourceREAD#">
                    SELECT
                        UserId_int
                    FROM
                        simpleobjects.useraccount
                    WHERE
                        MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                </cfquery>
                <cfif DuplicateSMS_Number.RECORDCOUNT GT 0>
                    <cfquery name="WhiteListPhoneNumber" datasource="#Session.DBSourceREAD#">
                        SELECT
                            IdPhone
                        FROM
                            simpleobjects.whitelistphonenumber
                        WHERE
                            PhoneNumber_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#trim(INSMSNUMBER)#">
                    </cfquery>
                    <cfif WhiteListPhoneNumber.RECORDCOUNT lt 1>
                         <cfset dataout.RXRESULTCODE = -1>
                        <cfset dataout.USERID = '-1'>
                        <cfset dataout.MESSAGE = 'The phone number you entered is already in use and cannot be used at this time. Please try another.'>
                        <cfreturn dataout>
                    </cfif>
                </cfif>
            </cfif>

            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.message />
                <cfset dataout.ERRMESSAGE = cfcatch.detail />
                <cfset dataout.TYPE = cfcatch.type />
            </cfcatch>
        </cftry>

        <cfreturn dataout />
    </cffunction>

    <cffunction name="VldInput" access="public" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$@!?+-]*[##]{0,1}[A-Za-z0-9._ *%$@!?+-]*$">

        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>

        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>

	</cffunction>

    <cffunction name="VldEmailAddress" access="remote" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$">

        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>

        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
	</cffunction>

	<cffunction name="VldEmailAddressList" access="public" returntype="boolean" hint="Validates User Email Address against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="Array" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.][A-Za-z]{2,4}$">

        <cfset var i = '' />

		<cfloop from="1" to="#ArrayLen(Input)#" index="i">
			<cfif len(Input[i]) eq 0>
				<cfreturn false>
			</cfif>

			<cfif REFind(REStr,Input[i]) neq 1>
				<cfreturn false>
			</cfif>
		</cfloop>
		<cfreturn true>
	</cffunction>

    <cffunction name="VldPhone10str" access="public" returntype="boolean" hint="Validates a 10 character string phone number against possible illegal characters and Illegal structure">
		<cfargument name="Input" TYPE="string" required="yes">
        <cfargument name="REStr" TYPE="string" required="no" default="^[1-9][0-9]{2}[1-9][0-9]{6}$">

        <cfif len(Input) eq 0>
        	<cfreturn true>
        </cfif>

        <cfif REFind(REStr,Input) eq 1>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
	</cffunction>

	<cffunction name="VldCompanyName" access="public" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
	<cfargument name="Input" TYPE="string" required="yes">
       <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$@!?+-]*[##]{0,1}[A-Za-z0-9._ *%$@!?+-]*$">

       <cfif len(Input) eq 0>
       	<cfreturn true>
       </cfif>

       <cfif REFind(REStr,Input) eq 1>
       	<cfreturn true>
       <cfelse>
       	<cfreturn false>
       </cfif>

	</cffunction>

  <cffunction name="VldContent" access="public" returntype="boolean" hint="Validates generic User Input against possible illegal characters">
    <cfargument name="Input" TYPE="string" required="yes">
    <cfargument name="REStr" TYPE="string" required="no" default="^[A-Za-z0-9._ *%$@!?+,'&(){}[\]=+^|<>:;""“”’`~\n-]*[##]{0,1}[A-Za-z0-9._ *%$@!?+,'&(){}[\]=+^|<>:;""“”’`~\n-]*$">

    <cfif len(Input) eq 0>
      <cfreturn true>
    </cfif>

    <cfif REFind(REStr,Input) eq 1>
      <cfreturn true>
    <cfelse>
      <cfreturn false>
    </cfif>

  </cffunction>

    <cffunction name="CheckBlockedCarrier" access="remote" hint="Check if a carrier is blocked">
        <cfargument name="inpCarrier" required="true">

        <cfset var dataout = {}/>
        <cfset dataout.MESSAGE = ''/>
        <cfset dataout.RXRESULTCODE = -1/>
        <cfset dataout.ERRMESSAGE = ''/>

        <cfset var checkBlocked = '' />
        <cftry>

            <cfquery name="checkBlocked" datasource="#Session.DBSourceEBM#">
                SELECT
                    PKId_int
                FROM
                    sms.carrierlist
                WHERE
                    OperatorId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCarrier#">
                AND
                    Blocked_ti = 1
            </cfquery>

            <cfif checkBlocked.recordcount GT 0>
                <cfset dataout.RXRESULTCODE = 1/>
                <cfset dataout.MESSAGE = 'Carrier is blocked!'/>
            <cfelse>
                <cfset dataout.RXRESULTCODE = 0/>
                <cfset dataout.MESSAGE = 'Carrier is not blocked!'/>
            </cfif>
            <cfcatch type="any">
                <cfset dataout.MESSAGE = cfcatch.message />
                <cfset dataout.ERRMESSAGE = cfcatch.detail />
                <cfset dataout.TYPE = cfcatch.type />
            </cfcatch>
        </cftry>

        <cfreturn dataout/>
    </cffunction>
    <cffunction name="ValidateKeyword" access="remote" hint="Validate Keyword if Keyword is available for use on specified shortcode.">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpISOLatin" TYPE="any" default="1" hint="Check Keyword for any non- ISO-8859-1 (Latin-1)" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = '' />
		<cfset var inpKeywordquotes = ''/>
		<cfset var checkKeywordquotes = ''/>
		<cfset var RetVarCheckKeywordAvailability = '' />
		<cfset var Pattern = '' />
		<cfset var Matcher = '' />
		<cfset var CSVInvalidChar = '' />
		<cfset var RetVarGetCurrentUserPlanKeywordLimits = '' />
		<cfset var RetVarGetKeywordsCountInUse = '' />
		<cfset var RetVarGetShortAccessData = '' />
		<cfset var RetVarGetBatchOwner = '' />
		<cfset var RetVarGetKeywordId = ''/>
		<cfset var RetVarAddKeyword = ''/>
		<cfset var RetVarUpdateKeyword	= '' />

       	<cfset dataout.RXRESULTCODE = 0 />
       	<cfset dataout.KEYWORDID = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>


        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<cfset inpKeywordStripped = Replace(TRIM(arguments.inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

			<cfset inpKeywordquotes = Replace(TRIM(arguments.inpKeyword), '"', ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "'", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), " ", ",@@@,", "All") />
   			<cfset inpKeywordquotes = Replace(TRIM(inpKeywordquotes), "-", ",@@@,", "All") />

			<cfset checkKeywordquotes = ListFind(inpKeywordquotes, '@@@')>

			<cfif checkKeywordquotes gt 0>
				<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
				<cfset dataout.ERRMESSAGE = "This Keyword contains not allow spaces, dashes, or quotes."/>

				<cfreturn dataout>
			</cfif>

			<!--- Optional ISO-8859-1 (Latin-1) Char validation --->
			<cfif inpISOLatin EQ 1 >
	   			<!--- Validate characters are within - ISO-8859-1 (Latin-1) --->
	   			<cfset Pattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]|[\x00-\x09]|[\x0B-\x0C]|[\x0E-\x0F]|[\x5B-\x5E]|[\x7B-\xA0]|[\xA2]|[\xA6]|[\xA8-\xBE]|[\xC0-\xC3]|[\xC8]|[\xCA-\xD0]|[\xD2-\xD5]|[\xD8-\xDB]|[\xDD-\xDE]|[\xE1-\xE3]|[\xE7]|[\xEA-\xEB]|[\xED-\xF0]|[\xF3-\xF5]|[\xF7]|[\xFA-\xFB]|[\xFD-\xFF]" ) ) />
	   			<cfset Matcher = Pattern.Matcher( JavaCast( "string", inpKeywordStripped ) ) />

	   			<!--- loop through the matches --->
				<cfloop condition="matcher.find()">
					<cfset CSVInvalidChar = ListAppend(CSVInvalidChar, Matcher.group(0)) />
				</cfloop>

				<!--- If any are found return with error message  --->
				<cfif LEN(CSVInvalidChar) GT 0>

					<cfset dataout.RXRESULTCODE = 0 />
					<cfset dataout.MESSAGE = ""/>
			        <cfset dataout.ERRMESSAGE = "This Keyword contains the following comma seperated list of invalid characters (#CSVInvalidChar#)"/>

		   			<cfreturn dataout>

				</cfif>

			</cfif>


   			<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				<cfinvokeargument name="inpBatchId" value="0"/>
			</cfinvoke>

   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Sorry keyword #arguments.inpKeyword# is taken.</br> Try again"/>


	   			<cfreturn dataout>
   			</cfif>



			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = "Keyword is available."/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">

	    	<!--- #SerializeJSON(cfcatch)# for more details if needed during debugging --->
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>
    <cffunction name="CheckKeywordAvailability" access="remote" hint="Check if Keyword is available for use on specified shortcode.">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpBatchId" TYPE="string" hint="The BatchId to read the XMLControlString from" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = '' />


       	<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.EXISTSFLAG = 1 />

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

        	<!--- Get Keyword information --->
            <cfquery name="GetKeywordCount" datasource="#Session.DBSourceEBM#">
	                SELECT
                        COUNT(k.Keyword_vch) AS TOTALCOUNT
                    FROM
                        sms.keyword AS k
                    JOIN
                        sms.shortcoderequest AS scr
                    ON
                        k.ShortCodeRequestId_int = scr.ShortCodeRequestId_int
                    JOIN
                        sms.shortcode AS sc
                    ON
                        sc.ShortCodeId_int = scr.ShortCodeId_int
                    WHERE
                        k.Active_int = 1
                    AND
                    	( k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeyword)#"> OR k.Keyword_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpKeywordStripped)#"> )
                    AND
                        sc.ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpShortCode#">
                    AND
                    	k.BatchId_bi <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpBatchId#">
            </cfquery>

            <cfif GetKeywordCount.TOTALCOUNT GT 0>

              	<!--- Enhancement: Look up user information on who has this Keyword reserved --->

       			<cfset dataout.EXISTSFLAG = 1 />

            <cfelse>

              	<cfset dataout.EXISTSFLAG = 0 />

            </cfif>

			<cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.MESSAGE = ""/>
			<cfset dataout.ERRMESSAGE = ""/>

	    <cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>
    <cffunction name="CreatePreAccount" access="remote" hint="Create pre account">
	    <cfargument name="inpKeyword" TYPE="string" hint="Keyword to lookup if it is in use yet." />
        <cfargument name="inpShortCode" TYPE="string" hint="Shortcode to check if keyword is in use yet." />
        <cfargument name="inpMessage" TYPE="string" hint="Message send to customer when they type kw to shortcode" />

       	<cfset var dataout = {} />
		<cfset var GetKeywordCount = '' />
		<cfset var inpKeywordStripped = '' />
        <cfset var RetVarCheckKeywordAvailability = '' />
        <cfset var AddUser = '' />
        <cfset var GetNEXTUSERID = '' />
        <cfset var AddKeywordQuery = '' />
        <cfset var AddKeywordResult = '' />
        <cfset var getShortCodeId = '' />
        <cfset var AddShortcodeRequest = '' />
        <cfset var AddShortcodeRequestResult = '' />
        <cfset var AddBatch = '' />
        <cfset var AddBatchResult = '' />
        <cfset var XMLBATCH = '' />
        <cfset var AddBilling = '' />
        <cfset var UniSearchPattern = '' />
        <cfset var UniSearchMatcher = '' />
        <cfset var inpT64 = 0 />
        <cfset var inpMessage = arguments.inpMessage />




        <!--- user pre ìnfo--->
        <cfset var inpNAPassword= ''/>
        <cfset var INPMAINEMAIL= ''/>
        <cfset var USERCOMPANYID= 0/>
        <cfset var INPCBPID= 0/>
        <cfset var INPFNAME= ''/>
        <cfset var INPLNAME= ''/>
        <cfset var INPTIMEZONE= ''/>
        <cfset var companyUserId= 0/>
        <cfset var INSMSNUMBER= ''/>
        <cfset var FreePlanCredits= 100/>

        <cfset var PreAccountStep= 1/>

        <!---keyword info --->
        <cfset var inpShortCodeRequestId= 0/>
        <cfset var inpShortCodeId= 0/>
        <cfset var inpDefaultFlag= 0/>
        <!--- --->


       	<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.EXISTSFLAG = 1 />
        <cfset dataout.USERID = '-1'>
        <cfset dataout.KEYWORDID=0>
        <cfset dataout.BATCHID=0>

        <cftry>

		    <!--- Advance Feature - Match keywords with or with quotes and spaces - handle the way users might type or think --->
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeyword), '"', "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "'", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), " ", "", "All") />
   			<cfset inpKeywordStripped = Replace(TRIM(inpKeywordStripped), "-", "", "All") />

        	<!--- Validate keyword not in use --->
   			<cfinvoke method="CheckKeywordAvailability" returnvariable="RetVarCheckKeywordAvailability">
				<cfinvokeargument name="inpKeyword" value="#inpKeywordStripped#"/>
				<cfinvokeargument name="inpShortCode" value="#inpShortCode#"/>
				<cfinvokeargument name="inpBatchId" value="0"/>
			</cfinvoke>

   			<!--- Return error message if it exists --->
   			<cfif RetVarCheckKeywordAvailability.EXISTSFLAG EQ 1>

	   			<cfset dataout.RXRESULTCODE = 0 />
				<cfset dataout.MESSAGE = ""/>
		        <cfset dataout.ERRMESSAGE = "Sorry keyword #arguments.inpKeyword# is taken.</br> Try again"/>
	   			<cfreturn dataout>
   			</cfif>

            <cfquery datasource="#session.DBSourceREAD#" name="getShortCodeId">
            	SELECT
            		ShortCodeId_int
            	FROM
            		sms.shortcode
            	WHERE
            		ShortCode_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpShortCode#"/>
            	LIMIT 1
            </cfquery>
            <cfif getShortCodeId.RECORDCOUNT EQ 0>
                <cfthrow MESSAGE="Invalid Shortcode" TYPE="Any" detail="Invalid Shortcode">
            <cfelse>
                <cfset inpShortCodeId = getShortCodeId.ShortCodeId_int>
            </cfif>


            <cftransaction action="begin">
                <cftry>
                    <!--- create account--->
                    <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="GetNEXTUSERID">
                        INSERT INTO
                            simpleobjects.useraccount
                            (
                                Password_vch,
                                EmailAddress_vch,
                                Created_dt,
                                UserIp_vch,
                                EmailAddressVerified_vch,
                                CompanyAccountId_int,
                                CBPId_int,
                                firstName_vch,
                                lastName_vch,
                                timezone_vch,
                                Active_int,
                                CompanyUserId_int,
                                HomePhoneStr_vch,
                                MFAContactString_vch,
                                MFAContactType_ti,
                                PreAccountStep_ti
                            )
                        VALUES
                            (
                                AES_ENCRYPT('#inpNAPassword#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPMAINEMAIL#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#USERCOMPANYID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPCBPID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPFNAME#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPLNAME#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPTIMEZONE#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#companyUserId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INSMSNUMBER#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#PreAccountStep#">
                            )
                    </cfquery>
                    <cfif GetNEXTUSERID.GENERATED_KEY GT 0>
                            <cfset dataout.USERID = GetNEXTUSERID.GENERATED_KEY>
                    </cfif>
                    <!--- add biling--->
                    <cfquery name="AddBilling" datasource="#Session.DBSourceEBM#">
                        INSERT INTO
                            simplebilling.billing
                            (
                                UserId_int,
                                Balance_int,
                                UnlimitedBalance_ti,
                                RateType_int,
                                Rate1_int,
                                Rate2_int,
                                Rate3_int,
                                Increment1_int,
                                Increment2_int,
                                Increment3_int
                                )
                            VALUES
                                (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataout.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#FreePlanCredits#"> ,
                                0,
                                1,
                                1,
                                1,
                                1,
                                30,
                                30,
                                30
                                )
                    </cfquery>
                    <!--- add short code request--->
                    <cfquery name="AddShortcodeRequest" datasource="#Session.DBSourceEBM#" result="AddShortcodeRequestResult">
                         INSERT INTO sms.shortcoderequest
                            (
                                ShortCodeId_int,
                                RequesterId_int,
                                RequesterType_int,
                                RequestDate_dt,
                                ProcessDate_dt,
                                Status_int,
                                KeywordQuantity_int,
                                VolumeExpected_int,
                                display_int,
                                IsDefault_ti
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataout.USERID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                NOW(),
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1000">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="10000000">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
                            )
                    </cfquery>
                    <cfset inpShortCodeRequestId = AddShortcodeRequestResult.GENERATED_KEY>
                    <!--- add batch--->
                    <!--- Use regex to look for unicode - only look for stuff outside of ASCII 255 do not worry about illegal ISO char here - this is just for encoding in DB--->
                    <cfset UniSearchPattern = CreateObject("java", "java.util.regex.Pattern").Compile(JavaCast( "string", "[^\x00-\xFF]" ) ) />

                    <!--- Create the pattern matcher for our target text. The matcher can optionally loop through all the high ascii values found in the target string. --->
                    <cfset UniSearchMatcher = UniSearchPattern.Matcher(JavaCast( "string", inpMessage ) ) />

                    <!--- Look for character higher than 255 ASCII --->
                    <cfif UniSearchMatcher.Find() >
                        <cfset inpMessage = toBase64(CharsetDecode(inpMessage, "UTF-8"))>
                        <!--- UnicodeCP Save_cp Formatting processing --->
                        <cfset inpT64 = 1 />
                    <cfelse>
                        <!--- UnicodeCP Save_cp Formatting processing --->
                        <cfset inpT64 = 0 />
                    </cfif>
                    <cfsavecontent variable="XMLBATCH">
                        <cfoutput>

                            <RXSSCSC>
                                <Q AF='NOFORMAT' BOFNQ='0' GID='1' ID='1' LDIR='-1' LMAX='0' LMSG='' MDF='0' REQANS='undefined' RQ='1' SWT='1' T64='#inpT64#' TDESC='' TEXT='#inpMessage#' TYPE='TRAILER' errMsgTxt='undefined'></Q>
                            </RXSSCSC>
                        </cfoutput>
                    </cfsavecontent>

                    <cfquery name="AddBatch" datasource="#Session.DBSourceEBM#" result="AddBatchResult">
                         INSERT INTO simpleobjects.batch
                            (
                                UserId_int,
                                Created_dt,
                                Desc_vch,
                                LastUpdated_dt,
                                XMLControlString_vch,
                                Active_int,
                                ContactTypes_vch,
                                ShortCodeId_int
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#dataout.USERID#">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="Pre account campaign">,
                                NOW(),

                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLBATCH#">,

                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeId#">
                            )
                    </cfquery>
                    <cfset dataout.BATCHID=AddBatchResult.GENERATED_KEY>
                    <!--- add kw--->
                    <cfquery name="AddKeywordQuery" datasource="#Session.DBSourceEBM#" result="AddKeywordResult">
                        INSERT INTO sms.keyword
                            (
                                Keyword_vch,
                                ShortCodeRequestId_int,
                                ShortCodeId_int,
                                Response_vch,
                                Created_dt,
                                IsDefault_bit,
                                BatchId_bi,
                                Survey_int,
                                EMSFlag_int
                            )
                        VALUES
                            (
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpKeywordStripped#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeRequestId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpShortCodeId#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                                NOW(),
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDefaultFlag#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dataout.BATCHID#">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">,
                                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">
                            )
                    </cfquery>
                    <cfset dataout.KEYWORDID = "#AddKeywordResult.GENERATED_KEY#" />

                <cftransaction action="commit" />
                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.USERID = '-1'>
                    <cfset dataout.ERRMESSAGE = cfcatch.message & " " & cfcatch.detail>
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
            </cftransaction>

			<cfset dataout.RXRESULTCODE = 1 />

	    <cfcatch TYPE="any">
		    <cfset dataout.RXRESULTCODE = -1 />
    		<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

			<cfreturn dataout>
		</cfcatch>

        </cftry>

		<cfreturn dataout />

	</cffunction>
    <cffunction name="UpdateUserInfo" access="remote" output="true" hint="">
        <cfargument name="inpAddress" type="string" required="yes" default="">
        <cfargument name="inpCity" type="string" required="yes" default="">
        <cfargument name="inpState" type="string" required="yes" default="">
        <cfargument name="inpZip" type="string" required="yes" default="">
        <cfargument name="inpSSNTAX" type="string" required="yes" default="">
        <cfargument name="inpPaypalEmail" type="string" required="yes" default="">



        <cfset var AddUser  = '' />
        <cfset var updateUserSetting	= '' />
        <cfset var dataout = structNew()>
        <cfset dataout.RESULT = 'FAIL'>
        <cfset dataout.RXRESULTCODE = -1 />

        <cfif structKeyExists(session, "loggedIn") && session.loggedIn GT 0>

            <cftransaction action="begin">
            <cftry>
                <cfquery name="AddUser" datasource="#Session.DBSourceEBM#" result="updateUserSetting">
                    UPDATE simpleobjects.useraccount
                    SET

                        Address1_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpAddress#">,
                        City_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCity#">,
                        State_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpState#">,
                        PostalCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpZip#">,
                        SSN_TaxId_vch= AES_ENCRYPT('#arguments.inpSSNTAX#', 'Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString'),
                        PaypalEmail_vch= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpPaypalEmail#">

                    WHERE UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.USERID#">
                </cfquery>
                <cfset dataout.RXRESULTCODE = 1 />
                <cfset dataout.RESULT = 'SUCCESS'>
                <cfset dataout.MESSAGE = 'Your Profile infomation have been saved.'>
                <cfreturn dataout>

                <cftransaction action="commit" />
                <cfcatch>
                    <cftransaction action="rollback" />
                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.RESULT = 'FAIL'>
                    <cfset dataout.MESSAGE = "Unable to update your infomation at this time. An error occurred.">
                    <cfreturn dataout>
                </cfcatch>
            </cftry>
        </cftransaction>
        <cfelse>
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.RESULT = 'FAIL'>
            <cfset dataout.MESSAGE = 'Your session has timed out. Please sign in again.'>
            <cfreturn dataout>
        </cfif>
        <cfreturn dataout />
    </cffunction>

    <cffunction name="DoSIREPayment" access="remote" hint="Do payment">
        <cfargument name="inpPaymentMethod" required="true"/>
        <cfargument name="inpcustomerId" required="false"/>
        <cfargument name="inppaymentMethodId" required="false"/>
        <cfargument name="inppaymentType" required="false"/>
        <cfargument name="inpdeveloperId" required="false"/>
        <cfargument name="inpversion" required="false"/>

        <cfset var dataout = {}/>
        <cfset var paymentData = {}/>
        <cfset var paymentResponse = {}/>
        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <cftry>
            <cfif arguments.inpPaymentMethod EQ 'Paypal'>
                <!--- Do new payment here --->
            <cfelseif arguments.inpPaymentMethod EQ 'Worldpay'>
                <cfset var paymentData = {}>

                <cfset paymentData = {
                   amount = _amount,
                   paymentVaultToken = {
                      customerId = arguments.inpcustomerId,
                      paymentMethodId = arguments.inppaymentMethodId,
                      paymentType = arguments.inppaymentType
                   },
                   developerApplication = {
                      developerId = arguments.inpdeveloperId,
                      version = arguments.inpversion
                   }
                }>

                <cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
                    <cfhttpparam type="header" name="content-type" value="application/json">
                    <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
                    <cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
                </cfhttp>

                <!--- Check payment Response here --->
                <!---  --->
            </cfif>
            <cfcatch TYPE="any">
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

                <cfreturn dataout>
            </cfcatch>
        </cftry>
    </cffunction>


    <cffunction name="IntegrateUserAutoLogin" access="remote" hint="">
        <cfargument name="inpIntegrateUserId" required="yes" TYPE="string" hint="" />
        <cfargument name="inpTokenKey" required="yes" TYPE="string" hint="" />

        <cfset var dataout = {} />

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.MFAREQ = 0 />
        <cfset dataout.WarningAutoLoginFail = 0 />


        <cfset var getUser=""/>
        <cfset var MFAAuthFlag	= '' />
        <cfset var MFACookieData	= '' />
        <cfset var CurrMFAOKFlag	= '' />
        <cfset var MFACode	= '' />
        <cfset var strMFAMe	= '' />
        <cfset var RandDigitString	= '' />
        <cfset var getShortCode	= '' />

        <cftry>
            <!----- used when login using username/email and password ---->
            <CFQUERY name="getUser" datasource="#Session.DBSourceREAD#">
                SELECT
                    Password_vch,
                    UserId_int,
                    PrimaryPhoneStr_vch,
                    SOCIALMEDIAID_BI,
                    EmailAddressVerified_vch,
                    EmailAddress_vch,
                    CompanyAccountId_int,
                    Active_int,
                    MFAContactString_vch,
                    MFAContactType_ti,
                    MFAEnabled_ti,
                    MFAEXT_vch,
                    FirstName_vch,
                    LastName_vch
                FROM
                    simpleobjects.useraccount
                WHERE
                    IntegrateUserID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpIntegrateUserId#">
                AND
                    TokenKeyLogin_vch=<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpTokenKey#">
                AND
                    Active_int=1
                AND
                    TokenKeyLoginExpiryDate_dt > NOW()
            </CFQUERY>


            <cfif getUser.RECORDCOUNT GT 0>
                <cflogin>
                <cfset Session.MFAISON = getUser.MFAEnabled_ti />
                <cfset Session.MFALENGTH = LEN(TRIM(getUser.MFAContactString_vch) ) />
                <cfset Session.USERID = getUser.UserId_int>
                <cfset Session.FULLNAME = getUser.FirstName_vch & ' ' & getUser.LastName_vch>
                <cfset Session.FIRSTNAME = getUser.FirstName_vch>
                <cfset Session.USERNAME = getUser.EmailAddress_vch>
                <cfset Session.COMPANYID = 0>
                <cfset Session.CompanyUserId = 0>
                <cfset Session.isAdmin = 0>
                <cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch>
                <cfset Session.EmailAddress = getUser.EmailAddress_vch>

                <cfset MFAAuthFlag = 0>


                <cftry>
                    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getShortCode"></cfinvoke>
                    <cfset Session.SHORTCODE = getShortCode.SHORTCODE>

                    <cfif DEVSITE EQ 0>
                        <cfset  MFACookieData = Decrypt(evaluate("COOKIE.MFAEBM#getUser.UserId_int#"),APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                    <cfelse>
                        <cfset  MFACookieData = evaluate("COOKIE.MFAEBM#getUser.UserId_int#")>
                    </cfif>
                    <!--- For security purposes, we tried to obfuscate the way the MFA Flag was stored. We wrapped it in the middle
                        of list. Extract it from the list. --->
                    <cfset  CurrMFAOKFlag = ListGetAt(
                            MFACookieData,
                            4,
                            ":"
                    ) />
                    <!--- Check to make sure this value is numeric, otherwise, it was not a valid value.  --->
                    <cfif CurrMFAOKFlag EQ "1">
                        <!--- This box has been authorized--->
                        <cfset MFAAuthFlag = 1>
                    </cfif>
                <cfcatch TYPE="any">

                </cfcatch>
                </cftry>
                <!--- Look for special Encrypted/Salted Admin Cookie to allow skip MFA - this will allow admins to impersonate a user account without triggering MFA--->
                <cfif LEN(TRIM(getUser.MFAContactString_vch) ) LT 10 OR getUser.MFAEnabled_ti EQ 0  >
                    <!--- Account is not properly setup for MFA so Skip --->
                    <cfset MFAAuthFlag = 1>
                </cfif>

                <!--- Resetup authorization --->
                <cfif MFAAuthFlag EQ 0>

                    <!--- Generate random 6 digit code --->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="RandDigitString" returnvariable="RandDigitString">
                        <cfinvokeargument name="length" value="6"/>
                    </cfinvoke>
                    <cfset MFACode = RandDigitString />

                    <cfset dataout.LAST4 = RIGHT(TRIM(getUser.MFAContactString_vch), 4) />

                    <!--- Create new string for Cookie--->
                    <cfset strMFAMe = (
                            CreateUUID() & ":" &
                            CreateUUID() & ":" &
                            getUser.UserId_int & ":" &
                            0 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                            CreateUUID() & ":" &
                            MFACode & ":" & <!--- Last code sent for this computer --->
                            CreateUUID()
                    ) />

                    <cfif DEVSITE EQ 0>
                        <!--- Encrypt the value.--->
                        <cfset  strMFAMe = Encrypt(strMFAMe,APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                    </cfif>
                    <!--- Store the cookie such that it will expire after 30 days. --->
                    <cfcookie
                        name="MFAEBM#getUser.UserId_int#"
                        value="#strMFAMe#"
                        expires="365"
                    />
                    <cfset dataout.MFAREQ = 1 />
                    <!--- <cflocation url = "/device-register" addToken = "no"> --->

                <cfelse>

                    <!--- Generate random "BAD" digit code --->
                    <cfset MFACode = "ghtyuesadq12xaw" />
                    <!--- Create new string for Cookie--->
                    <cfset strMFAMe = (
                            CreateUUID() & ":" &
                            CreateUUID() & ":" &
                            getUser.UserId_int & ":" &
                            1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
                            CreateUUID() & ":" &
                            MFACode & ":" & <!--- Last code sent for this computer --->
                            CreateUUID()
                    ) />

                    <cfif DEVSITE EQ 0>
                        <!--- Encrypt the value. --->
                        <cfset  strMFAMe = Encrypt( strMFAMe, APPLICATION.EncryptionKey_Cookies,"cfmx_compat","hex") />
                    </cfif>

                    <!--- Store the cookie such that it will expire after 30 days. --->
                    <cfcookie
                        name="MFAEBM#getUser.UserId_int#"
                        value="#strMFAMe#"
                        expires="365"
                    />
                    <cfset Session.loggedIn = 1>
                </cfif>
                </cflogin>
            <cfelse>
                <cfset dataout.WarningAutoLoginFail = 1 />
            </cfif>

            <cfset dataout.RXRESULTCODE = 1 />
        <cfcatch TYPE="any">
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>

            <cfreturn dataout>
        </cfcatch>

        </cftry>

        <cfreturn dataout />
    </cffunction>

    <!--- Log IP address when user login to SIRE site --->
    <cffunction name="LogIPAddress" access="remote" output="true" hint="Log IP address when user login to SIRE site">
        <cfargument name="inpIPAddress" required="false" default="#cgi.remote_addr#">
        <cfargument name="inpUserId" required="false" default="#SESSION.USERID#">

        <cfset var moduleName_vch = 'Log IP Address of user login'/>
        <cfset var operator_vch = 'Log IP Address when user login to Sire site.'/>
        <cfset var iPAddress_vch = TRIM(arguments.inpIPAddress)>
        <cfset var userId_int = TRIM(arguments.inpUserId)>

        <cfset var dataout = {}/>

        <cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.RESULT = ''/>
        <cfset dataout.MESSAGE = ''/>

        <cfset var SaveIPAddress = {}/>

        <cftry>
            <cfquery name="SaveIPAddress" datasource="#Session.DBSourceEBM#">
                INSERT INTO simpleobjects.userlogs
                    (
                        UserId_int,
                        ModuleName_vch,
                        Operator_vch,
                        IP_vch,
                        Timestamp_dt
                    )
                VALUES
                    (
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userId_int#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#moduleName_vch#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#operator_vch#">,
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#iPAddress_vch#">,
                        NOW()
                    )
            </cfquery>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.RESULT = 'SUCCESS'>
            <cfset dataout.MESSAGE = "Log IP Address Successfully!">

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.RESULT = 'ERROR'>
                <cfset dataout.MESSAGE = cfcatch.MESSAGE>
            </cfcatch>
        </cftry>
        <cfreturn dataout />
    </cffunction>
    <!--- END Log IP address when user login to SIRE site --->
    <!--- Get top 10 list admin management IP Address by UserId --->
    <cffunction name="TopTenOfIPAddressByUserID" access="remote" output="true"><!--- returnformat="JSON"--->
        <cfargument name="inpUserId" TYPE="numeric" required="true" default="0" />

        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetPMQuery = ''>
        <cfset var userId_int = TRIM(arguments.inpUserId)>
        <cfset var tempItem = {}/>
        <cftry>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>

            <cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    pkw.LogId_int,
                    pkw.UserId_int,
                    pkw.ModuleName_vch,
                    pkw.Operator_vch,
                    pkw.IP_vch,
                    pkw.Timestamp_dt,
                    concat(u.FirstName_vch,' ',u.LastName_vch) as Full_Name,
                    u.EmailAddress_vch

                FROM
                    simpleobjects.userlogs pkw
                INNER JOIN
                    simpleobjects.useraccount u
                    ON pkw.UserId_int = u.UserId_int
                WHERE pkw.ModuleName_vch = 'Log IP Address of user login'
                AND u.Active_int = 1
                AND u.IsTestAccount_ti = 0
                AND u.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userId_int#">
                ORDER BY pkw.Timestamp_dt DESC

                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10">
            </cfquery>

            <cfloop query="GetPMQuery">
                <cfset tempItem = {
                    ID = '#LogId_int#',
                    USERID = '#UserId_int#',
                    MODULENAME = '#ModuleName_vch#',
                    OPERATOR = '#Operator_vch#',
                    IP = '#IP_vch#',
                    NAME = "#Full_Name#",
                    EMAIL = "#EmailAddress_vch#",
                    TIMESTAMP = '#DateTimeFormat(Timestamp_dt,'mm/dd/yyyy HH:nn:ss')#'
                }>
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <!--- End Get list admin management IP Address --->
    <!--- Get list admin management IP Address --->
    <cffunction name="ManagementIPAddressList" access="remote" output="true"><!--- returnformat="JSON"--->
        <cfargument name="UserIdstatus" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->


        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetPMQuery = ''>
        <cfset var sSortDir_0 = 'DESC'/>
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var data = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetTemplate  = '' />
        <cfset var Status = ''/>
        <cfset var rsCount = '0'/>
        <cfset var GetListUserHide = ''/>
        <cftry>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>

            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>

            <!--- Order by --->
            <cfset order = "MAX(pkw.Timestamp_dt)"/>
            <cfset sSortDir_0 = 'DESC'/>


            <cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    pkw.LogId_int,
                    pkw.UserId_int,
                    pkw.ModuleName_vch,
                    pkw.Operator_vch,
                    (SELECT IP_vch FROM simpleobjects.userlogs where Timestamp_dt = MAX(pkw.Timestamp_dt) limit 1) as IP_vch,
                    MAX(pkw.Timestamp_dt) as MaxTimestamp_dt,
                    concat(u.FirstName_vch,' ',u.LastName_vch) as Full_Name,
                    u.EmailAddress_vch

                FROM
                    simpleobjects.userlogs pkw
                INNER JOIN
                    simpleobjects.useraccount u
                    ON pkw.UserId_int = u.UserId_int
                WHERE pkw.ModuleName_vch = 'Log IP Address of user login'
                AND u.Active_int = 1
                AND u.IsTestAccount_ti = 0
                <cfif UserIdstatus EQ 1>
                AND u.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.UserId#">
                </cfif>
                <cfif customFilter NEQ "">
                    <cfoutput>
                        <cfloop array="#deserializeJSON(customFilter)#" index="filterItem">
                            <cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
                            <cfelse>
                                AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
                            </cfif>
                        </cfloop>
                    </cfoutput>
                </cfif>
                GROUP BY pkw.UserId_int

                ORDER BY
                        #order# #sSortDir_0#

                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>
            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetPMQuery">
                <cfset tempItem = {
                    ID = '#LogId_int#',
                    USERID = '#UserId_int#',
                    MODULENAME = '#ModuleName_vch#',
                    OPERATOR = '#Operator_vch#',
                    IP = '#IP_vch#',
                    NAME = "#Full_Name#",
                    EMAIL = "#EmailAddress_vch#",
                    TIMESTAMP = '#DateTimeFormat(MaxTimestamp_dt,'mm/dd/yyyy HH:nn:ss')#'
                }>
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <!--- End Get list admin management IP Address --->

    <cffunction name="TopTenIPAddressList" access="remote" output="true"><!--- returnformat="JSON"--->
        <cfargument name="UserIdstatus" TYPE="numeric" required="no" default="0" />
        <cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
        <cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
        <cfargument name="sColumns" default="" />
        <cfargument name="sEcho">
        <cfargument name="iSortCol_1" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_1" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_2" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_2" default=""><!---this is the direction of sorting, asc or desc --->
        <cfargument name="iSortCol_0" default="-1"><!---this is the index of colums that need to be sorted, 1 is Agent Name, 2 is Time Logged in, 3 is session number--->
        <cfargument name="sSortDir_0" default="DESC"><!---this is the direction of sorting, asc or desc --->
        <cfargument name="customFilter" default=""><!---this is the custom filter data --->


        <cfset var dataout = {}>
        <cfset dataout.DATA = ArrayNew(1)>
        <cfset var GetPMQuery = ''>
        <cfset var sSortDir_0 = 'DESC'/>
        <cfset var order    = "">
        <cfset var tempItem = '' />
        <cfset var filterItem   = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var data = '' />
        <cfset var GetNumbersCount  = '' />
        <cfset var GetTemplate  = '' />
        <cfset var Status = ''/>
        <cfset var rsCount = '0'/>
        <cfset var GetListUserHide = ''/>
        <cftry>

            <cfset dataout.RXRESULTCODE = 1 />
            <cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
            <cfset dataout.ERRMESSAGE = "" />

            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>

            <!---ListEMSData is key of DataTable control--->
            <cfset dataout["ListEMSData"] = ArrayNew(1)>

            <!--- Order by --->
            <cfset order = "pkw.Timestamp_dt"/>
            <cfset sSortDir_0 = 'DESC'/>


            <cfquery name="GetPMQuery" datasource="#Session.DBSourceEBM#">
                SELECT
                    SQL_CALC_FOUND_ROWS
                    pkw.LogId_int,
                    pkw.UserId_int,
                    pkw.ModuleName_vch,
                    pkw.Operator_vch,
                    pkw.IP_vch,
                    pkw.Timestamp_dt,
                    concat(u.FirstName_vch,' ',u.LastName_vch) as Full_Name,
                    u.EmailAddress_vch

                FROM
                    simpleobjects.userlogs pkw
                INNER JOIN
                    simpleobjects.useraccount u
                    ON pkw.UserId_int = u.UserId_int
                WHERE pkw.ModuleName_vch = 'Log IP Address of user login'
                AND u.Active_int = 1
                AND u.IsTestAccount_ti = 0
                AND u.UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#UserIdstatus#">

                ORDER BY
                        #order# #sSortDir_0#

                LIMIT
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
                OFFSET
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#">
            </cfquery>
            <cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
                SELECT FOUND_ROWS() AS iTotalRecords
            </cfquery>
            <cfloop query="rsCount">
                <cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
                <cfset dataout["iTotalRecords"] = iTotalRecords>
            </cfloop>

            <cfloop query="GetPMQuery">
                <cfset tempItem = {
                    ID = '#LogId_int#',
                    USERID = '#UserId_int#',
                    MODULENAME = '#ModuleName_vch#',
                    OPERATOR = '#Operator_vch#',
                    IP = '#IP_vch#',
                    NAME = "#Full_Name#",
                    EMAIL = "#EmailAddress_vch#",
                    TIMESTAMP = '#DateTimeFormat(Timestamp_dt,'mm/dd/yyyy HH:nn:ss')#'
                }>
                <cfset ArrayAppend(dataout["ListEMSData"],tempItem)>
            </cfloop>
        <cfcatch type="Any" >
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            <cfset dataout["iTotalRecords"] = 0>
            <cfset dataout["iTotalDisplayRecords"] = 0>
            <cfset dataout["ListEMSData"] = ArrayNew(1)>
        </cfcatch>
        </cftry>
        <cfreturn dataout>
    </cffunction>
    <!--- End Get list admin management IP Address --->
    <cffunction name="GetPaymentMethodSetting" access="remote" >
		<cfset var dataout	= {} />
		<cfset var rsgetsettdata	= {} />

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.RESULT = ""/>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />


		<cftry>
			<cfquery name="rsgetsettdata" datasource="#Session.DBSourceREAD#">
				Select VariableName_txt, Value_txt
				FROM simpleobjects.sire_setting
				WHERE VariableName_txt = 'PaymentMethodSetting'
			</cfquery>

			<cfif rsgetsettdata.RECORDCOUNT GT 0>
				<cfset dataout.RESULT = rsgetsettdata.Value_txt>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Get Payment Method Successfully." />
			<cfelse>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Get Payment Method Fail." />
			</cfif>
			<cfcatch>
				<cfset dataout.RESULT = "FAIL">
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

		</cftry>
		<cfreturn dataout>
	</cffunction>

     <cffunction name="EstablishSessionFromUserInfo" access="public" output="false" hint="Pass in User Info Query from either Login or Cookie and then establish Session Variables used to control logged in User's options ">
          <cfargument name="getUser" type="query" required="yes" />
          <cfargument name="ImpersonateClientAsAgencyId" type="any" required="no" default="0" hint="Agency that is doing the impersonation"/>
          <cfargument name="ImpersonateClientAsAgencyManagerUserId" type="any" required="no" default="0" hint="Which agency manager is doing this" />

          <!--- Default Return Structure --->
          <cfset var dataout = {} />

          <cfset var getShortCode = "" />
          <cfset var UpdateLastLoggedIn = "" />
          <cfset var UpdateLastLastUserIp = "" />
          <cfset var RetVarAgencyManagerCheck = "" />

          <!--- Set default return values --->
          <cfset dataout.RXRESULTCODE = 0 />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

               <cfif getUser.recordcount gt 0 >

                    <cfset Session.MFAISON = getUser.MFAEnabled_ti />
                    <cfset Session.MFALENGTH = LEN(TRIM(getUser.MFAContactString_vch) ) />
                    <cfset Session.USERID = getUser.UserId_int>
                    <cfset Session.FULLNAME = getUser.FirstName_vch&' '&getUser.LastName_vch>
                    <cfset Session.FIRSTNAME = getUser.FirstName_vch>
                    <cfset Session.USERNAME = getUser.EmailAddress_vch>
                    <cfset Session.COMPANYID = 0>
                    <cfset Session.CompanyUserId = 0>
                    <cfset Session.isAdmin = 0>
                    <cfset Session.PrimaryPhone = getUser.PrimaryPhoneStr_vch EQ ""? " ": getUser.PrimaryPhoneStr_vch>
                    <cfset Session.EmailAddress = getUser.EmailAddress_vch>
                    <cfset Session.fbuserid = getUser.SOCIALMEDIAID_BI>
                    <cfset Session.loggedIn = "1" />

                    <!--- 0 Current session user is not an agency GT 0 means this user an Agency --->
                    <cfset Session.AgencyId = "0">
                    <cfset Session.AgencyManagerId = "0">
                    <cfset Session.AgencyShortDesc = "">

                    <!--- Perform Check of if the current session User is also a Manager for an Agency -- Setup Agency Session Values if found--->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyManagerCheck" returnvariable="RetVarAgencyManagerCheck"></cfinvoke>

                    <!---
                         0 is regular Sire User - GT 0 Client is under specified agancy.
                         Client can only be member of one agency
                    --->
                    <cfset Session.ClientOfAgencyId = "0">

                    <!--- Perform Check of if the current session User is also a client of an Agency -- Setup Agency Session Values if found--->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyClientCheck" returnvariable="RetVarAgencyClientCheck"></cfinvoke>

                    <!---
                         0 is not a impersonation right now
                         is this session an impersonation of a client by an agency - if so store that agency Id here
                    --->
                    <cfset Session.ImpersonateClientAsAgencyId = "#arguments.ImpersonateClientAsAgencyId#">

                    <!---
                         0 is not a impersonation right now
                         is this session an impersonation of a client by an agency - if so store the Sire UserId of the Manager for agency Id here
                    --->
                    <cfset Session.ImpersonateClientAsAgencyManagerUserId = "#arguments.ImpersonateClientAsAgencyManagerUserId#">

                    <cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="getShortCode"></cfinvoke>
                    <cfset Session.SHORTCODE = getShortCode.SHORTCODE>

                    <!--- Store in the DB whenever user has re-logged in even with remember me cookie --->
                    <cfquery name="UpdateLastLoggedIn" datasource="#Session.DBSourceEBM#">
                         UPDATE
                           simpleobjects.useraccount
                         SET
                           LastLogIn_dt = '#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#'
                         WHERE
                           UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUser.UserId_int#">
                         AND (CBPID_int = 1 OR CBPID_int IS NULL)
                    </cfquery>

                    <cfquery name="UpdateLastLastUserIp" datasource="#Session.DBSourceEBM#">
                       UPDATE
                            simpleobjects.useraccount
                       SET
                            LastUserIp_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(TRIM(CGI.REMOTE_ADDR),2024)#">
                       WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUser.UserId_int#">
                       AND (CBPID_int = 1 OR CBPID_int IS NULL)
                   </cfquery>

                    <cfset dataout.RXRESULTCODE = 1>

               </cfif>

          <cfcatch TYPE="any">
                 <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

                 <cfif cfcatch.errorcode EQ "">
                      <cfset cfcatch.errorcode = -1 />
                 </cfif>

                 <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
                 <cfset dataout.TYPE = "#cfcatch.TYPE#">
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                 <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

          </cfcatch>
          </cftry>

          <cfreturn dataout />

     </cffunction>

     <cffunction name="LookForRememberMeCookie" access="public" output="false" hint="Check if cookie exists - if it does log in user based on info stored in cookie">

          <!--- Default Return Structure --->
          <cfset var dataout = {} />

          <cfset var RememberMe = "" />
          <cfset var currRememberMe = "" />
          <cfset var getUser = "" />
          <cfset var getShortCode = "" />
          <cfset var UpdateLastLoggedIn = "" />
          <cfset var EstablishSessionFromUserInfo = "" />

          <!--- Set default return values --->
          <cfset dataout.RXRESULTCODE = 0 />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

                <!--- Decrypt out remember me cookie. --->
                 <cfset RememberMe = Decrypt(
                      COOKIE.RememberMe,
                      APPLICATION.EncryptionKey_Cookies,
                      "cfmx_compat",
                      "hex"
                 ) />

                 <!---
                 For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
                 of list. Extract it from the list.
                 --->
                 <cfset currRememberMe = ListGetAt(
                      RememberMe,
                      3,
                      ":"
                 ) />


               <cfif IsNumeric( currRememberMe )>

                    <cfquery name="getUser" datasource="#Session.DBSourceEBM#">
                         SELECT
                             Password_vch,
                             UserId_int,
                             PrimaryPhoneStr_vch,
                             SOCIALMEDIAID_BI,
                             EmailAddressVerified_vch,
                             EmailAddress_vch,
                             CompanyAccountId_int,
                             Active_int,
                             MFAContactString_vch,
                             MFAContactType_ti,
                             MFAEnabled_ti,
                             MFAEXT_vch,
                             FirstName_vch,
                             LastName_vch
                         FROM
                             simpleobjects.useraccount
                         WHERE
                             UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="#currRememberMe#">
                             AND (CBPID_int = 1 OR CBPID_int IS NULL)
                    </cfquery>

                    <!--- Establish Session - set values as found --->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="EstablishSessionFromUserInfo" returnvariable="RetVarEstablishSessionFromUserInfo">
                         <cfinvokeargument name="getUser" value="#getUser#">
                    </cfinvoke>

                    <cfset dataout.MESSAGE = "#SerializeJSON(RetVarEstablishSessionFromUserInfo)#">
                    <cfset dataout.RXRESULTCODE = 1>

               <cfelse>
                    <cfset dataout.RXRESULTCODE = 0>
               </cfif>

          <cfcatch TYPE="any">
                 <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

                 <cfif cfcatch.errorcode EQ "">
                      <cfset cfcatch.errorcode = -1 />
                 </cfif>

                 <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
                 <cfset dataout.TYPE = "#cfcatch.TYPE#">
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                 <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

          </cfcatch>
          </cftry>

          <cfreturn dataout />

     </cffunction>

     <cffunction name="AgencyActiveCheck" access="public" output="false" hint="Check if current Agency is still active">
          <cfargument name="inpAgencyId" TYPE="numeric" required="no" default="0" />

          <!--- Default Return Structure --->
          <cfset var dataout = {} />
          <cfset var getAgency = "" />

          <!--- Set default return values --->
          <cfset dataout.RXRESULTCODE = 0 />
          <cfset dataout.ACTIVE = "0" />
          <cfset dataout.SHORTDESC = "" />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

               <!--- Look for active manager status --->
               <cfquery name="getAgency" datasource="#Session.DBSourceEBM#">
                    SELECT
                         AgencyId_int,
                         ShortDesc_vch
                    FROM
                         simpleobjects.agency
                    WHERE
                         AgencyId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpAgencyId#">
                    AND
                         Active_int = 1
               </cfquery>

               <!--- setup Agency Session Values --->
               <cfif getAgency.RecordCount GT 0>
                    <cfset dataout.ACTIVE = "1" />
                    <cfset dataout.SHORTDESC = "#getAgency.ShortDesc_vch#" />
               </cfif>

               <cfset dataout.RXRESULTCODE = 1>

          <cfcatch TYPE="any">
                 <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

                 <cfif cfcatch.errorcode EQ "">
                      <cfset cfcatch.errorcode = -1 />
                 </cfif>

                 <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
                 <cfset dataout.ACTIVE = "0" />
                 <cfset dataout.SHORTDESC = "" />
                 <cfset dataout.TYPE = "#cfcatch.TYPE#">
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                 <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

          </cfcatch>
          </cftry>

          <cfreturn dataout />

     </cffunction>

     <cffunction name="AgencyManagerCheck" access="public" output="false" hint="Check if current Session User is a Manager of an Agency">

          <!--- Default Return Structure --->
          <cfset var dataout = {} />
          <cfset var getManager = "" />
          <cfset var RetVarAgencyActiveCheck = '' />

          <!--- Set default return values --->
          <cfset dataout.RXRESULTCODE = 0 />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

               <!--- Set defaults --->
               <!--- 0 Current session user is not an agency GT 0 means this user an Agency --->
               <cfset Session.AgencyId = "0">
               <cfset Session.AgencyManagerId = "0">

               <!--- Look for active manager status --->
               <cfquery name="getManager" datasource="#Session.DBSourceEBM#">
                    SELECT
                        AgencyManagerId_int,
                        AgencyId_int
                    FROM
                        simpleobjects.agencymanagers
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
                    AND
                         Active_int = 1
                    ORDER BY
                         AgencyManagerId_int DESC
                    LIMIT 1
               </cfquery>

               <!--- setup Agency Session Values --->
               <cfif getManager.RecordCount GT 0>

                    <!--- Make sure controlling Agency is still active --->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyActiveCheck" returnvariable="RetVarAgencyActiveCheck">
                         <cfinvokeargument name="inpAgencyId" value="#getManager.AgencyId_int#">
                    </cfinvoke>

                    <cfif RetVarAgencyActiveCheck.ACTIVE GT 0>
                         <cfset Session.AgencyId = "#getManager.AgencyId_int#">
                         <cfset Session.AgencyManagerId = "#getManager.AgencyManagerId_int#">
                         <cfset Session.AgencyShortDesc = "#RetVarAgencyActiveCheck.SHORTDESC#">
                    </cfif>

               </cfif>

               <cfset dataout.RXRESULTCODE = 1>

          <cfcatch TYPE="any">
                 <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

                 <cfif cfcatch.errorcode EQ "">
                      <cfset cfcatch.errorcode = -1 />
                 </cfif>

                 <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
                 <cfset dataout.TYPE = "#cfcatch.TYPE#">
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                 <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

          </cfcatch>
          </cftry>

          <cfreturn dataout />

     </cffunction>

     <cffunction name="AgencyClientCheck" access="public" output="false" hint="Check if current Session User is a Client of an Agency">
          <cfargument name="inpUserID" TYPE="string" required="no" default="#Session.USERID#" hint="This is the User Id to impersonate - verifies current user is Manager and inpUser Id is client of agency"/>

          <!--- Default Return Structure --->
          <cfset var dataout = {} />
          <cfset var getAgency = "" />
          <cfset var RetVarAgencyActiveCheck = '' />

          <!--- Set default return values --->
          <cfset dataout.RXRESULTCODE = 0 />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

               <!--- Set defaults --->
               <!---
                    0 is regular Sire User - GT 0 Client is under specified agancy.
                    Client can only be member of one agency
               --->
               <cfset Session.ClientOfAgencyId = "0">

               <!--- Look for active manager status --->
               <cfquery name="getAgency" datasource="#Session.DBSourceEBM#">
                    SELECT
                        PKId_int,
                        AgencyId_int
                    FROM
                        simpleobjects.agencyclients
                    WHERE
                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpUserID#">
                    AND
                         Active_int = 1
                    ORDER BY
                         PKId_int DESC
                    LIMIT 1
               </cfquery>

               <!--- setup Agency Session Values --->
               <cfif getAgency.RecordCount GT 0>

                    <!--- Make sure controlling Agency is still active --->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyActiveCheck" returnvariable="RetVarAgencyActiveCheck">
                         <cfinvokeargument name="inpAgencyId" value="#getAgency.AgencyId_int#">
                    </cfinvoke>

                    <cfif RetVarAgencyActiveCheck.ACTIVE GT 0>
                         <cfset Session.ClientOfAgencyId = "#getAgency.AgencyId_int#">
                    </cfif>

               </cfif>

               <cfset dataout.RXRESULTCODE = 1>

          <cfcatch TYPE="any">
                 <!--- There was either no remember me cookie, or  the cookie was not valid for decryption. Let the user proceed as NOT LOGGED IN. --->

                 <cfif cfcatch.errorcode EQ "">
                      <cfset cfcatch.errorcode = -1 />
                 </cfif>

                 <cfset dataout.RXRESULTCODE = cfcatch.errorcode>
                 <cfset dataout.TYPE = "#cfcatch.TYPE#">
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
                 <cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#">

          </cfcatch>
          </cftry>

          <cfreturn dataout />

     </cffunction>

     <cffunction name="GetAgencyPermission" access="public" output="false" hint="Lookup whether current logged in account has agency manager permissions">
         <cfargument name="inpSkipRedirect" TYPE="any" required="no" default="0"/>

         <cfset var dataout = {} />
         <cfset var listUser  = '' >
         <cfset var RetVarAgencyManagerCheck = ''>

         <cftry>

               <!--- Perform Check of if the current session User is also a Manager for an Agency -- Setup Agency Session Values if found--->
               <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyManagerCheck" returnvariable="RetVarAgencyManagerCheck"></cfinvoke>

               <!--- Set default return values --->
               <cfset dataout.RXRESULTCODE = "-1" />
               <cfset dataout.ISADMINOK = "0" />
               <cfset dataout.TYPE = "" />
               <cfset dataout.MESSAGE = "" />
               <cfset dataout.ERRMESSAGE = "" />

               <cfif Session.AgencyId GT 0 AND Session.AgencyManagerId GT 0>

                    <cfset dataout.RXRESULTCODE = "1" />
                    <cfset dataout.ISADMINOK = "1" />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />

               <cfelse>

                 <cfif inpSkipRedirect EQ 0>
                     <cflocation url="/session/sire/pages/agency-no-permission" addToken="no" />
                 </cfif>

               </cfif>

               <cfcatch TYPE="any">
                 <cfset dataout.RXRESULTCODE = "-2" />
                 <cfset dataout.ISADMINOK = "1" />
                 <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                 <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                 <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
               </cfcatch>
         </cftry>

         <cfreturn dataout />
     </cffunction>

     <cffunction name="GetAgencyClientList" access="remote" output="false" hint="get Agency client list with filter">
        <cfargument name="inpEmailAddress" TYPE="string" required="yes" default="" hint="This could be either an email address OR a User Id so we can search for either one"/>

        <cfset var dataout = {} />
        <cfset var GetUserList = '' />
        <cfset var rsGetUserList = '' />
        <cfset var displayInfo = '' />

        <cfset dataout.RXRESULTCODE = 1 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "" />
        <cfset dataout.ERRMESSAGE = "" />

        <cfset dataout.UserList = arrayNew() />

        <cftry>
            <cfquery datasource="#Session.DBSourceEBM#" name="GetUserList" result="rsGetUserList">
                    SELECT
                        u.UserId_int,
                        CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
                        u.EmailAddress_vch,
                        u.MFAContactString_vch,
                        o.OrganizationName_vch
                    FROM
                        simpleobjects.useraccount u
                    LEFT JOIN
                        simpleobjects.organization o
                    ON
                        o.UserId_int = u.UserId_int
                    WHERE
                        (
                            u.EmailAddress_vch LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.inpEmailAddress#%">
                            OR
                            u.UserId_int = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.inpEmailAddress#">
                        )
                    AND
                         u.Active_int = 1
                    AND
                         u.UserId_int IN (SELECT UserId_int FROM simpleobjects.agencyclients WHERE AgencyId_int =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session.AgencyId#">)
            </cfquery>

            <cfif GetUserList.RECORDCOUNT GT 0>

                <cfloop query="GetUserList" >
                    <cfset displayInfo = '#UserId_int# - #EmailAddress_vch#' >
                    <!--- <cfif OrganizationName_vch NEQ ''>
                        <cfset displayInfo&=' - #OrganizationName_vch#'>
                    </cfif> --->
                    <cfset var tmpUser = {
                        'ID':'#UserId_int#',
                        'NAME':'#Full_Name#',
                        'EMAIL':'#displayInfo#',
                        'ORG_NAME' : "#OrganizationName_vch#"
                    } />
                    <cfset ArrayAppend(dataout.UserList, tmpUser) />
                </cfloop>
            </cfif>

            <cfcatch>
                <cfset dataout.RXRESULTCODE = -1 />
                <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
                <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
            </cfcatch>
        </cftry>

        <cfreturn dataout>
    </cffunction>

    <cffunction name="ImpersonateAgencyClient" access="remote" output="false" hint="Impersonate an Agency client">
          <cfargument name="inpUserId" TYPE="string" required="yes" default="" hint="This is the User Id to impersonate - verifies current user is Manager and inpUser Id is client of agency"/>

          <cfset var dataout = {} />
          <cfset var GetUserInfo = '' />
          <cfset var RetVarAgencyClientCheck = '' />
          <cfset var RetVarAgencyManagerCheck = '' />
          <cfset var RetVarEstablishSessionFromUserInfo = '' />


          <cfset dataout.RXRESULTCODE = 1 />
          <cfset dataout.TYPE = "" />
          <cfset dataout.MESSAGE = "" />
          <cfset dataout.ERRMESSAGE = "" />

          <cftry>

               <!--- Perform Check of if the current session User is also a Manager for an Agency -- Setup Agency Session Values if found --->
               <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyManagerCheck" returnvariable="RetVarAgencyManagerCheck"></cfinvoke>

               <!--- Return error session user is not an active Manager of an Agency --->
               <cfif Session.AgencyId EQ 0 OR Session.AgencyManagerId EQ 0>

                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "Error" />
                    <cfset dataout.MESSAGE = "Current User is not an Agency Manager" />
                    <cfset dataout.ERRMESSAGE = "" />
                    <cfreturn dataout>

               </cfif>

               <!--- Perform Check of if the current session User is also a CLient of an Agency -- Setup Agency Session Values if found --->
               <cfinvoke component="public.sire.models.cfc.userstools" method="AgencyClientCheck" returnvariable="RetVarAgencyClientCheck">
                    <cfinvokeargument name="inpUserId" value="#arguments.inpUserId#">
               </cfinvoke>

               <!--- Return error target user is not an active Manager of an Agency --->
               <cfif  Session.ClientOfAgencyId  NEQ  Session.AgencyId >

                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "Error" />
                    <cfset dataout.MESSAGE = "Target User is not an active client of Agency  #Session.ClientOfAgencyId# #Session.AgencyId#" />
                    <cfset dataout.ERRMESSAGE = "" />

                    <cfreturn dataout>

               </cfif>

               <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
                 SELECT
                     Password_vch,
                     UserId_int,
                     PrimaryPhoneStr_vch,
                     SOCIALMEDIAID_BI,
                     EmailAddressVerified_vch,
                     EmailAddress_vch,
                     CompanyAccountId_int,
                     Active_int,
                     MFAContactString_vch,
                     MFAContactType_ti,
                     MFAEnabled_ti,
                     MFAEXT_vch,
                     FirstName_vch,
                     LastName_vch
                 FROM
                     simpleobjects.useraccount
                 WHERE
                     UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="#inpUserId#">
                     AND (CBPID_int = 1 OR CBPID_int IS NULL)
               </cfquery>

               <cfif GetUserInfo.RecordCount GT 0 >

                    <!--- Establish Session - set values as found --->
                    <cfinvoke component="public.sire.models.cfc.userstools" method="EstablishSessionFromUserInfo" returnvariable="RetVarEstablishSessionFromUserInfo">
                         <cfinvokeargument name="getUser" value="#GetUserInfo#">
                         <cfinvokeargument name="ImpersonateClientAsAgencyId" value="#Session.AgencyId#">
                         <cfinvokeargument name="ImpersonateClientAsAgencyManagerUserId" value="#Session.AgencyManagerId#">
                    </cfinvoke>

                    <cfset dataout.RXRESULTCODE = "1" />
                    <cfset dataout.TYPE = "" />
                    <cfset dataout.MESSAGE = "" />
                    <cfset dataout.ERRMESSAGE = "" />

               <cfelse>

                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "Error" />
                    <cfset dataout.MESSAGE = "Target User is not found" />
                    <cfset dataout.ERRMESSAGE = "" />

               </cfif>

          <cfcatch>
               <cfset dataout.RXRESULTCODE = -1 />
               <cfset dataout.TYPE = "#cfcatch.TYPE#" />
               <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
               <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
          </cfcatch>
          </cftry>

       <cfreturn dataout>
   </cffunction>

   <cffunction name="UnwindImpersonateAgencyClient" access="remote" output="false" hint="Unwind Impersonate an Agency client and go back to session as manager">

         <cfset var dataout = {} />
         <cfset var GetUserInfo = '' />
         <cfset var GetManager = '' />
         <cfset var RetVarEstablishSessionFromUserInfo = '' />

         <cfset dataout.RXRESULTCODE = 1 />
         <cfset dataout.TYPE = "" />
         <cfset dataout.MESSAGE = "" />
         <cfset dataout.ERRMESSAGE = "" />

         <cftry>


              <!--- Return error target user is not an active Manager of an Agency --->
              <cfif  Session.ImpersonateClientAsAgencyId EQ 0 OR Session.ImpersonateClientAsAgencyManagerUserId EQ 0 >

                    <cfset dataout.RXRESULTCODE = -1 />
                    <cfset dataout.TYPE = "Error" />
                    <cfset dataout.MESSAGE = "Current User is not an active impersonation by an Agency" />
                    <cfset dataout.ERRMESSAGE = "" />

                    <cfreturn dataout>

              </cfif>

              <!--- Look for active manager status --->
              <cfquery name="GetManager" datasource="#Session.DBSourceEBM#">
                   SELECT
                       UserId_int,
                       AgencyId_int
                   FROM
                       simpleobjects.agencymanagers
                   WHERE
                       AgencyManagerId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.ImpersonateClientAsAgencyManagerUserId#">
                   AND
                        Active_int = 1
              </cfquery>

              <cfif GetManager.RecordCount EQ 0>

                   <cfset dataout.RXRESULTCODE = -1 />
                   <cfset dataout.TYPE = "Error" />
                   <cfset dataout.MESSAGE = "Could not find Active Manager  info" />
                   <cfset dataout.ERRMESSAGE = "" />

                   <cfreturn dataout>

              </cfif>

              <cfquery name="GetUserInfo" datasource="#Session.DBSourceEBM#">
               SELECT
                    Password_vch,
                    UserId_int,
                    PrimaryPhoneStr_vch,
                    SOCIALMEDIAID_BI,
                    EmailAddressVerified_vch,
                    EmailAddress_vch,
                    CompanyAccountId_int,
                    Active_int,
                    MFAContactString_vch,
                    MFAContactType_ti,
                    MFAEnabled_ti,
                    MFAEXT_vch,
                    FirstName_vch,
                    LastName_vch
               FROM
                    simpleobjects.useraccount
               WHERE
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_Integer" VALUE="#GetManager.UserId_int#">
                    AND (CBPID_int = 1 OR CBPID_int IS NULL)
              </cfquery>

              <cfif GetUserInfo.RecordCount GT 0 >

                   <!--- Establish Session - set values as found --->
                   <cfinvoke component="public.sire.models.cfc.userstools" method="EstablishSessionFromUserInfo" returnvariable="RetVarEstablishSessionFromUserInfo">
                        <cfinvokeargument name="getUser" value="#GetUserInfo#">
                        <cfinvokeargument name="ImpersonateClientAsAgencyId" value="0">
                        <cfinvokeargument name="ImpersonateClientAsAgencyManagerUserId" value="0">
                   </cfinvoke>

                   <cfset dataout.RXRESULTCODE = "1" />
                   <cfset dataout.TYPE = "" />
                   <cfset dataout.MESSAGE = "" />
                   <cfset dataout.ERRMESSAGE = "" />

              <cfelse>

                   <cfset dataout.RXRESULTCODE = -1 />
                   <cfset dataout.TYPE = "Error" />
                   <cfset dataout.MESSAGE = "Target User is not found" />
                   <cfset dataout.ERRMESSAGE = "" />

              </cfif>

         <cfcatch>
              <cfset dataout.RXRESULTCODE = -1 />
              <cfset dataout.TYPE = "#cfcatch.TYPE#" />
              <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
              <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
         </cfcatch>
         </cftry>

     <cfreturn dataout>
   </cffunction>

</cfcomponent>
