<cfcomponent>
	<cfinclude template="/public/paths.cfm" >
	<cfparam name="Session.USERID" default="0"/> 
    <cfparam name="Session.loggedIn" default="0"/>
    <cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfset LOCAL_LOCALE = "English (US)">
    
    <cffunction name="ValidatePhoneNumber" access="remote" output="true" hint="ValidatePhoneNumber">
    	<cfargument name="PhoneNumber" type="string" required="yes">
    	
    	<cfset var dataout = {}>
		<cfset dataout.RXRESULTCODE = 0 />

		<cfset dataout.MESSAGE = "" />
	   	<cfset dataout.ERRMESSAGE = "" />
	   	<cfset dataout.ERRORINDEX = "" />
	   	<cfset dataout.ERRORTYPE = "" />
	   	<cfset dataout.ERRORDATA = "" />
    	
    	<cfif len(arguments.PhoneNumber) GT 0 >
			<cfif NOT isvalid('telephone', arguments.PhoneNumber)>
                <cfset dataout.MESSAGE = "Phone Number is not a valid US telephone number !"> 
                <cfset dataout.RXRESULTCODE = -1>
                <cfset dataout.ERRORTYPE = "">
                <cfset dataout.ERRORDATA = "">
                <cfreturn dataout />
            </cfif>
        </cfif>
        <cfreturn dataout />
    </cffunction>
</cfcomponent>