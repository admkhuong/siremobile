<cfcomponent>
	<cfinclude template="../../configs/paths.cfm" >
	<cffunction name="GetWaitingCustomer" access="public" output="false" hint="Get Customer of Sire's user waitlist information">
    	<cfargument name="inpCustomerToken" required="true">

    	<cfset var getCustomerInfo = '' />
    	<cfset var getCustomerList = '' />

    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.ESTIMATEDWAIT = ''>
    	<cfset dataout.AHEADINLINE = ''>
    	<cfset dataout.TOTALINLINE = ''>
    	<cfset dataout.POSITION = '' >
    	<cfset dataout.TOKEN = '' />
    	<cfset dataout.CUSTOMERPHONE = '' />
    	<cfset dataout.CUSTOMERNAME = '' />
    	<cfset dataout.CUSTOMEREMAIL = '' />

    	<cftry>
    		<cfquery name="getCustomerInfo" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		CustomerEmail_vch, ServiceId_int, WaitTime_int, Created_dt, Token_vch, CustomerName_vch, CustomerContactString_vch, ListId_bi
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	AND
		    		Active_ti = 1
		    	AND
		    		Status_int = 1
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif getCustomerInfo.Recordcount GT 0>
	    		<cfset var customerWaitPosition = 1/>
	    		<cfquery name="getCustomerList" datasource="#Session.DBSourceREAD#">
			    	SELECT
			    		CustomerEmail_vch, Token_vch, WaitTime_int, Created_dt
			    	FROM
			    		simpleobjects.waitlistapp
			    	WHERE
			    		ListId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getCustomerInfo.ListId_bi#">
			    	AND
			    		Status_int = 1
			    	ORDER BY
			    		Order_bi
			    	ASC
		    	</cfquery>

		    	<cfif getCustomerList.Recordcount EQ 0>
		    		<cfset dataout.POSITION = 1>
		    		<cfset dataout.AHEADINLINE = 0>
		    		<cfset dataout.TOTALINLINE = 1>
		    		<cfset dataout.RXRESULTCODE = 1>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.MESSAGE = 'You are the only one in this service'>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = getCustomerInfo.CustomerContactString_vch />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    	<cfelse>
		    		<cfloop query="getCustomerList">
		    			<cfif arguments.inpCustomerToken NEQ getCustomerList.Token_vch>
		    				<cfset customerWaitPosition = customerWaitPosition + 1>
		    			<cfelse>
		    				<cfbreak>
		    			</cfif> 
		    		</cfloop>
		    		<cfset dataout.POSITION = customerWaitPosition>
		    		<cfset dataout.AHEADINLINE = customerWaitPosition-1>
		    		<cfset dataout.TOTALINLINE = getCustomerList.Recordcount>
		    		<cfset dataout.RXRESULTCODE = 2>
		    		<cfset dataout.ESTIMATEDWAIT = getCustomerInfo.WaitTime_int - (dateDiff("n", getCustomerInfo.Created_dt, NOW()))>
		    		<cfset dataout.MESSAGE = 'Get waitlist information successfully'>
		    		<cfset dataout.TOKEN = getCustomerInfo.Token_vch />
		    		<cfset dataout.CUSTOMERNAME = getCustomerInfo.CustomerName_vch />
		    		<cfset dataout.CUSTOMERPHONE = getCustomerInfo.CustomerContactString_vch />
		    		<cfset dataout.CUSTOMEREMAIL = getCustomerInfo.CustomerEmail_vch />
		    	</cfif>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>
    </cffunction>

    <cffunction name="GetWaitlistStatusByToken" access="public" output="false" hint="Get Waitlist status by token">
    	<cfargument name="inpCustomerToken" required="true">

    	<cfset var getWaitlistCustomer = '' />
    	<cfset var dataout = {} />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />
    	<cfset dataout.STATUS = '' />
    	<cfset dataout.WAITTIME = '' />

		<cftry>
    		<cfquery name="getWaitlistCustomer" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		Status_int,
		    		WaitTime_int
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	AND
		    		Active_ti = 1
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif getWaitlistCustomer.Recordcount GT 0>
	    		<cfset dataout.RXRESULTCODE = 1>
	    		<cfset dataout.STATUS = getWaitlistCustomer.Status_int />
	    		<cfset dataout.WAITTIME = getWaitlistCustomer.WaitTime_int /> 
	    		<cfset dataout.MESSAGE = 'Get customer waitlist status successfully'>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist token'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>    	
   	</cffunction>

   	<cffunction name="UpdateWaitlistStatusByToken" access="remote" output="false" hint="Update Waitlist status by token when user click cancel">
   		<cfargument name="inpCustomerToken" required="true">

   		<cfset var checkWaitlistExist = '' />
   		<cfset var updateWaitlistStatus = '' />
   		<cfset var updateWaitlistStatusResult = '' />
   		<cfset var dataout = {} />
   		<cfset var lastOrderResult = '' />
   		<cfset var updateLowerOrder = '' />
    	<cfset dataout.MESSAGE = '' />
    	<cfset dataout.ERRMESSAGE = '' />

    	<cftry>
    		<cfquery name="checkWaitlistExist" datasource="#Session.DBSourceREAD#">
	    		SELECT
		    		Status_int,
		    		WaitListId_bi,
		    		ListId_bi,
		    		Order_bi
		    	FROM
		    		simpleobjects.waitlistapp
		    	WHERE
		    		Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
		    	LIMIT
		    	 	1
	    	</cfquery>

	    	<cfif checkWaitlistExist.Recordcount GT 0>
	    		<cfquery name="updateWaitlistStatus" datasource="#Session.DBSourceEBM#" result="updateWaitlistStatusResult">
		    		UPDATE
		    			simpleobjects.waitlistapp
		    		SET
		    			Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="4">
		    		WHERE
		    			Token_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpCustomerToken#">
	    		</cfquery>
	    		<cfif updateWaitlistStatusResult.Recordcount GT 0>
					<cfinvoke method="GetLastOrderByUserId" component="public.sire.models.cfc.waitlistapp" returnvariable="lastOrderResult">
						<cfinvokeargument name="inpListId" value="#checkWaitlistExist.ListId_bi#"/>
					</cfinvoke>
					<cfinvoke method="UpdateLowerOrder" component="public.sire.models.cfc.waitlistapp" returnvariable="updateLowerOrder">
						<cfinvokeargument name="inpWaitlistId" value="#checkWaitlistExist.WaitListId_bi#">
						<cfinvokeargument name="inpCurrentCustomerOrder" value="#checkWaitlistExist.Order_bi#">
						<cfinvokeargument name="inpCustomerOrder" value="#lastOrderResult.LASTORDER#">
						<cfinvokeargument name="inpListId" value="#checkWaitlistExist.ListId_bi#"/>
					</cfinvoke>

	    			<cfset dataout.RXRESULTCODE = 1>
	    			<cfset dataout.MESSAGE = "Update waitlist status successfully">
					<cfinvoke method="LogState">
						<cfinvokeargument name="inpWaitlistID" value="#checkWaitlistExist.WaitListId_bi#">
						<cfinvokeargument name="inpWaitlistStatus" value="4">
					</cfinvoke>
	    		<cfelse>
	    			<cfset dataout.RXRESULTCODE = 0>
	    			<cfset dataout.MESSAGE = "Update waitlist status failed">
	    		</cfif>
		    <cfelse>
		    	<cfset dataout.RXRESULTCODE = -1>
		    	<cfset dataout.MESSAGE = 'Invalid waitlist token'>
	    	</cfif>
	    	<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -2 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
    	</cftry>

    	<cfreturn dataout>
   	</cffunction>

   	<cffunction name="LogState" access="private" hint="save state of waitlist">
		<cfargument name="inpWaitlistID" type="numeric" required="yes" default="0">
		<cfargument name="inpWaitlistStatus" type="numeric" required="yes" default="0">
		<cfset var dataout = {}>
		<cfset var rsInsertLog = "">

		<cfset dataout.RXRESULTCODE = 0 />
        <cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "##" />                
        <cfset dataout.ERRMESSAGE= "" />
		<cftry>
			<cfquery result="rsInsertLog" datasource="#Session.DBSourceEBM#">
				INSERT INTO 
					simpleobjects.waitlistapplogs
					(
						WaitlistID_bi, 
						Status_int, 
						CreatedAt_dt
					)
				VALUES
					(
						<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#arguments.inpWaitlistID#">,
						<cfqueryparam cfsqltype="CF_SQL_BIGINT" value="#arguments.inpWaitlistStatus#">,
						NOW()
					)
			</cfquery>
			<cfset dataout.RXRESULTCODE = 1 />
			<cfcatch>
		        <cfset dataout.TYPE = "#cfcatch.TYPE#" />
                <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />                
                <cfset dataout.ERRMESSAGE= "#cfcatch.detail#" />    
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<cffunction name="GetLastOrderByUserId" access="public" hint="Get last customer order by userId">
		<cfargument name="inpListId" required="true" type="numeric"/>
		<cfset var dataout = {} />
		<cfset var dataout.MESSAGE = '' />
		<cfset var dataout.ERRMESSAGE = '' />
		<cfset var dataout.LASTORDER = '' />

		<cfset var getLastOrder = '' />

		<cftry>
			<cfquery name="getLastOrder" datasource="#Session.DBSourceREAD#">
				SELECT
					Order_bi
				FROM
					simpleobjects.waitlistapp
				WHERE
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND
					Active_ti = 1
				AND
					Status_int = 1
				ORDER BY
					Order_bi
				DESC
				LIMIT 1
			</cfquery>
			<cfif getLastOrder.Recordcount GT 0>
				<cfset dataout.LASTORDER = getLastOrder.Order_bi>
				<cfset dataout.MESSAGE = "Get Last Order successfully">
				<cfset dataout.RXRESULTCODE = 1>
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "No record found">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>

	<cffunction name="UpdateLowerOrder" access="public" hint="Update all the lower order than the input order number">
		<cfargument name="inpWaitlistId" required="true">
		<cfargument name="inpCurrentCustomerOrder" required="true">
		<cfargument name="inpCustomerOrder" required="true">
		<cfargument name="inpListId" required="true" type="numeric"/>
 		<cfset var dataout = {} />
 		<cfset var updateHigherOrder = '' />
 		<cfset var updateHigherOrderResult = '' />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.ERRMESSAGE = '' />

		<cftry>
			<cfquery name="updateHigherOrder" datasource="#Session.DBSourceEBM#" result="updateHigherOrderResult">
				UPDATE
					simpleobjects.waitlistapp
				SET
					Order_bi = Order_bi - 1
				WHERE
					ListId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpListId#"/>
				AND 
					Status_int = 1
				AND
					Active_ti = 1
				AND 
					WaitListId_bi != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpWaitlistId#">
				AND
					Order_bi > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCurrentCustomerOrder#">
				AND 
					Order_bi <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpCustomerOrder#">
			</cfquery>
			<cfif updateHigherOrderResult.Recordcount GT 0>
				<cfset dataout.RXRESULTCODE = 1>
				<cfset dataout.MESSAGE = "Update order successfully">
			<cfelse>
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = "Update order failed">
			</cfif>
			<cfcatch type="any">
				<cfset dataout.RXRESULTCODE = -1 />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
	            <cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>

		<cfreturn dataout />
	</cffunction>
</cfcomponent>