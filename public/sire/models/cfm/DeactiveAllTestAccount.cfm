<cfoutput>
Deactive List Account Detail:</br>
------------------------------------------------</br>

<cfset ListAccountDeactive="180
,185
,284
,296
,545
,546
,547
,544
,774
,666
,580
,975
,976
,974
,977
,978
,500
,499
,867
,541
,559
,560
,557
,536
,793
,412
,773
,555
,1648
,1435
,661
,588
,589
,454
,649
,651
,754
,705
,688
,653
,701
,554
,535
,772
,775
,771
,490
,491
,444
,671
,573
,567
,562
,776
,967
,931
,932
,933
,958
,791
,806
,918
,925
,926
,805
,811
,1014
,1099
,763
,1150
,1072
,792
,1043
,417
,512
,515
,516
,517
,518
,519
,471
,470
,751
,424
,704
,707
,708
,675
,582
,745
,685
,749
,896
,897
,899
,750
,467
,769
,778
,929
,881
,882
,930
,756
,757
,673
,684
,674
,979
,752
,1229
,1315
,1317
,1350
,1431
,1464
,1434
,1465
,1467
,1562
,1276
,1599
,1640
,1641
,1642
,1644
,1645
,1646
,1647
,1651
,1652
,1649
,1650
,1659
,1664
,1665
,1671
,1643
,1313
,1314
,1079
,1080
,924
,533
,566
,642
,643
,558
,575
,421
,447
,563
,1137
,683
,1290
,887
,530
,423
,564
,764
,984
,1138
,814
,1044
,689
,577
,576
,449
,549
,581
,587
,583
,584
,460
,462
,464
,465
,468
,590
,593
,595
,596
,597
,598
,599
,602
,456
,466
,605
,611
,648
,650
,556
,448
,1595
,457
,463
,637
,761
,615
,759
,646
,1163
,760
,747
,748
,991
,411
,413
,548
,539
,540
,537
,511
,1076">
<cfset ListDeactiveSuccess="">
<cfset ListDeactiveFail="">
<cfloop list="#ListAccountDeactive#" delimiters="," index="inpUserId">        
        <cfset dataout = {} />
        <cfset logEntry = {} />
        <cfset deactiveAccount = '' />
        
        <cfset userInfoQuery = '' />
        <cfset userBillingQuery = '' />
        <cfset userPlanQuery = '' />
        <cfset userBatchsQuery = '' />
        <cfset userShortcodeRequestQuery = '' />
        <cfset userKeywordQuery = '' />
        <cfset userContactQueue = '' />
        <cfset userReferralQueue = '' />
        <cfset userMLP = '' />
        <cfset userShortUrl = '' />
        
        <cfset retValUpdateUserBilling = '' />
        <cfset retValUpdateUserPlan = '' />
        <cfset retValUpdateUserBatchs = '' />
        <cfset retValUpdateUserShortcodeRequest = '' />
        <cfset retValUpdateUserKeyword = '' />
        <cfset retValUpdateUserContactQueue = '' />
        <cfset retValUpdateUserReferralQueue = '' />
        <cfset retValUpdateUserMLP = '' />
        <cfset retValUpdateUserShortUrl = '' />
        <cfset retValSendSMS = '' />

        <cfset retValDeactiveAccount = '' />
        <cfset retValInsertDeactivateLog = '' />

        <cfset activeCode = hash(gettickcount()&'_'& inpUserId)/>
        <cfset userWarningMail = {} />

        <cfset keywordLog = '' />

        <cfset logEntry.User = {} />
        <cfset logEntry.User.UserId = inpUserId />
        <cfset logEntry.User.EmailAddress = '' />
        <cfset logEntry.User.MFAContactString = '' />
        <cfset logEntry.User.UserName = '' />

        <cfset logEntry.UserBilling = {} />
        <cfset logEntry.UserBilling.Balance = 0 />
        <cfset logEntry.UserBilling.BuyCreditBalance = 0 />
        <cfset logEntry.UserBilling.PromotionCreditBalance = 0 />

        <cfset logEntry.UserPlan = {} />
        <cfset logEntry.UserPlan.UserPlanId = 0 />
        <cfset logEntry.UserPlan.PlanId = 0 />
        <cfset logEntry.UserPlan.EndDate = '' />

        <cfset logEntry.UserBatches = [] />
        <cfset logEntry.UserShortCodeRequestId = 0 />
        <cfset logEntry.UserKeywords = [] />
        <cfset logEntry.UserContactQueue = [] />
        <cfset logEntry.UserReferralQueue = [] />
        <cfset logEntry.UserMLP = [] />
        <cfset logEntry.UserShortUrl = [] />

        <cfset dataout.RXRESULTCODE = -1 />
        <cfset dataout.MESSAGE = '' />
        <cfset dataout.ERRMESSAGE = '' />
        <cfset dataout.TYPE = '' />
        <cfset email="">
        <cftry>

            <!--- ------------------------------------------GET USER LOG DATA------------------------------------------- ---></br>
            
            <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfoQuery">
                <cfinvokeargument name="inpUserId" value="#inpUserId#" />
            </cfinvoke>              
            <cfset email=userInfoQuery.FULLEMAIL>
            <cfif userInfoQuery.USERID LT 1>
                <cfthrow type="Database" message="Datebase error" detail="Account have alr deactivate before that or not exists">
            </cfif>

            <cfset logEntry.User.EmailAddress = userInfoQuery.FULLEMAIL />
            <cfset logEntry.User.MFAContactString = userInfoQuery.MFAPHONE />
            <cfset logEntry.User.UserName = userInfoQuery.USERNAME & ' ' & userInfoQuery.LASTNAME />

            <cfquery name="userBillingQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    Balance_int,
                    BuyCreditBalance_int,
                    PromotionCreditBalance_int
                FROM
                    simplebilling.billing
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                LIMIT 1
            </cfquery>

            <cfif userBillingQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Failed to get user billing info">
            </cfif>

            <cfset logEntry.UserBilling.Balance = userBillingQuery.Balance_int />
            <cfset logEntry.UserBilling.BuyCreditBalance = userBillingQuery.BuyCreditBalance_int />
            <cfset logEntry.UserBilling.PromotionCreditBalance = userBillingQuery.PromotionCreditBalance_int />

            <cfquery name="userPlanQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    UserPlanId_bi,
                    PlanId_int,
                    EndDate_dt
                FROM
                    simplebilling.userplans
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                ORDER BY
                    UserPlanId_bi DESC
                LIMIT 1
            </cfquery>

            <cfif userPlanQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Fail to get userplan">
            </cfif>

            <cfset logEntry.UserPlan.UserPlanId = userPlanQuery.UserPlanId_bi />
            <cfset logEntry.UserPlan.PlanId = userPlanQuery.PlanId_int />
            <cfset logEntry.UserPlan.EndDate = "#dateFormat(userPlanQuery.EndDate_dt, "yyyy-mm-dd")# #timeFormat(userPlanQuery.EndDate_dt, "HH:mm:ss")#" />

            <cfquery name="userBatchsQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    BatchId_bi
                FROM
                    simpleobjects.batch
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userBatchsQuery.RECORDCOUNT GT 0>
                <cfloop query="userBatchsQuery">
                    <cfset arrayAppend(logEntry.UserBatches, BatchId_bi) />
                </cfloop>
            </cfif>

            <cfquery name="userShortcodeRequestQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    ShortCodeRequestId_int
                FROM
                    sms.shortcoderequest
                WHERE
                    RequesterId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                ORDER BY
                    ShortCodeRequestId_int DESC
                LIMIT 1
            </cfquery>

            <cfif userShortcodeRequestQuery.RECORDCOUNT LT 1>
                <cfthrow type="Database" message="Database error" detail="Failed to get user shortcode request">
            </cfif>

            <cfset logEntry.UserShortCodeRequestId = userShortcodeRequestQuery.ShortCodeRequestId_int />

            <cfquery name="userKeywordQuery" datasource="#session.DBSourceREAD#">
                SELECT
                    KeywordId_int,
                    Keyword_vch
                FROM
                    sms.keyword
                WHERE
                    ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userKeywordQuery.RECORDCOUNT GT 0>
                <cfloop query="userKeywordQuery">
                    <cfset temp = {} />
                    <cfset temp.KeywordId = KeywordId_int />
                    <cfset temp.Keyword = Keyword_vch />
                    <cfset arrayAppend(logEntry.UserKeywords, temp) />
                    <cfset keywordLog = keywordLog & '#Keyword_vch#, ' />
                </cfloop>
            </cfif>

            <cfquery name="userContactQueue" datasource="#session.DBSourceREAD#">
                SELECT
                    DTSId_int
                FROM
                    simplequeue.contactqueue
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userContactQueue.RECORDCOUNT GT 0>
                <cfloop query="userContactQueue">
                    <cfset arrayAppend(logEntry.UserContactQueue, DTSId_int) />
                </cfloop>
            </cfif>

            <cfquery name="userReferralQueue" datasource="#session.DBSourceREAD#">
                SELECT
                    Id_int
                FROM
                    simplequeue.sire_referral_msg_queue
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userReferralQueue.RECORDCOUNT GT 0>
                <cfloop query="userReferralQueue">
                    <cfset arrayAppend(logEntry.UserReferralQueue, Id_int) />
                </cfloop>
            </cfif>

            <cfquery name="userMLP" datasource="#session.DBSourceREAD#">
                SELECT
                    cppxURL_vch
                FROM
                    simplelists.cppx_data
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            </cfquery>

            <cfif userMLP.RECORDCOUNT GT 0>
                <cfloop query="userMLP">
                    <cfset arrayAppend(logEntry.UserMLP, cppxURL_vch) />
                </cfloop>
            </cfif>

            <cfquery name="userShortUrl" datasource="#session.DBSourceREAD#">
                SELECT
                    ShortURL_vch
                FROM
                    simplelists.url_shortner
                WHERE
                    UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                AND
                    Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                AND
                    Type_ti = 1
            </cfquery>

            <cfif userShortUrl.RECORDCOUNT GT 0>
                <cfloop query="userShortUrl">
                    <cfset arrayAppend(logEntry.UserShortUrl, ShortURL_vch) />
                </cfloop>
            </cfif>

            <!--- --------------------------------------------UPDATE USER DATA--------------------------------------------- ---></br>
            <cftransaction>
                <cfquery datasource="#session.DBSourceEBM#" result="retValDeactiveAccount">
                    UPDATE
                        simpleobjects.useraccount
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
                        IsTestAccount_ti=1                    
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate useraccount">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserBilling">
                    UPDATE
                        simplebilling.billing
                    SET
                        Balance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">,
                        BuyCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">,
                        PromotionCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Reduce billing balance. Balance = #userBillingQuery.Balance_int#, BuyCreditBalance = #userBillingQuery.BuyCreditBalance_int#, PromotionCreditBalance = #userBillingQuery.PromotionCreditBalance_int# ">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserPlan">
                    UPDATE
                        simplebilling.userplans
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserPlan.UserPlanId#">
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate userplan. UserPlanId = #userPlanQuery.UserPlanId_bi#, DefaultPlanId = #userPlanQuery.PlanId_int#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserBatchs">
                    UPDATE
                        simpleobjects.batch
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user batches. Batches id = #arrayToList(logEntry.UserBatches, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserShortcodeRequest">
                    UPDATE
                        sms.shortcoderequest
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                    AND
                        Status_int= <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user shortcode request. ShortcodeRequestId = #logEntry.UserShortCodeRequestId#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserKeyword">
                    UPDATE
                        sms.keyword
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        ShortCodeRequestId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#logEntry.UserShortCodeRequestId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user keyword. Keywords = #keywordLog#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserContactQueue">
                    UPDATE
                        simplequeue.contactqueue
                    SET
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="7"> -- 7 is stop status
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        DTSStatusType_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user contactqueue. ContactQueue = #arrayToList(logEntry.UserContactQueue, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValUpdateUserReferralQueue">
                    UPDATE
                        simplequeue.sire_referral_msg_queue
                    SET
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="4"> -- 4 is stop status
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user referral queue. ReferralQueue = #arrayToList(logEntry.UserReferralQueue, ',')#">
                </cfinvoke>

                <cfquery name="userMLP" datasource="#session.DBSourceEBM#">
                    UPDATE
                        simplelists.cppx_data
                    SET
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user MLP. MLP Key = #arrayToList(logEntry.UserMLP, ',')#">
                </cfinvoke>

                <cfquery name="userShortUrl" datasource="#session.DBSourceEBM#">
                    UPDATE
                        simplelists.url_shortner
                    SET
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    WHERE
                        UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">
                    AND
                        Active_int = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                    AND
                        Type_ti = 1
                </cfquery>

                <cfinvoke method="createUserLog" component="session.sire.models.cfc.history">
                    <cfinvokeargument name="moduleName" value="Deactivate UserId = #inpUserId#">
                    <cfinvokeargument name="operator" value="Deactivate user shortURL. ShortUrl Key = #arrayToList(logEntry.UserShortUrl, ',')#">
                </cfinvoke>

                <cfquery datasource="#session.DBSourceEBM#" result="retValInsertDeactivateLog">
                    INSERT INTO
                        simpleobjects.deactivate_logs
                        (
                            AdminId_int,
                            UserId_int,
                            LogData_vch,
                            Status_int,
                            ActiveCode_vch
                        )
                    VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_integer" value="476">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUserId#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJSON(logEntry)#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#activeCode#">
                        )
                </cfquery>

                <cfset userWarningMail = {
                    username:logEntry.User.UserName, 
                    ActiveCode:activeCode
                }/>
               
                <cfset dataout.RXRESULTCODE = 1>      
                <cfset dataout.MESSAGE = "Deactive Success">          
                <cfset ListDeactiveSuccess= listappend(ListDeactiveSuccess,inpUserId &" : " & email )>
            </cftransaction>
            
            <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
                <cfinvokeargument name="InpEmailString" value="#email#">
                <cfinvokeargument name="InpStatus" value="cancelled"> 
                <cfinvokeargument name="InpCancelDate" value="#dateFormat(now(), 'mm/dd/yyyy')#"> 
            </cfinvoke>
            

            <cfcatch type="any">
                
                <cfset dataout.MESSAGE = "Failed to deactive account">
                <cfset dataout.ERRMESSAGE = cfcatch.message & " - " & cfcatch.detail />
                <cfset ListDeactiveFail= listappend(ListDeactiveFail,inpUserId &" : " & email & " - " & dataout.ERRMESSAGE)>
            </cfcatch>
        </cftry>
        <cfset dataout.log = logEntry />
        *********** User ID =#inpUserId# <cfif email neq "">(#email#)</cfif>*********</br>
        <cfif dataout.RXRESULTCODE eq 1>
            <cfdump var="#dataout.MESSAGE#"/>
        <cfelse>
            <cfdump var="#dataout.ERRMESSAGE#"/>
        </cfif>
        
    
   
</cfloop>

</br></br></br></br>======================Total Deactive==================</br>
Total : #listlen(ListAccountDeactive)# </br>
Deactive Success : #listlen(ListDeactiveSuccess)# </br>
Deactive Fail : #listlen(ListDeactiveFail)# </br>
===================List Deactive success==================</br>
<cfloop list="#ListDeactiveSuccess#" delimiters="," index="inpUserId">        
    #inpUserId#</br>
</cfloop>
===================List Deactive Fail==================</br>
<cfloop list="#ListDeactiveFail#" delimiters="," index="inpUserId">        
    #inpUserId#</br>
</cfloop>
</cfoutput>