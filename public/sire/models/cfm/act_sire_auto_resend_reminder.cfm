<!--- <cfinclude template="cronjob-firewall.cfm"/> --->
<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="../../../../session/cfc/csc/constants.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
<cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">
<cfinclude template="../../../../session/sire/models/cfc/calendar.cfc">

<cfparam name="DBSourceEBM" default="bishop"/>

<!--- <cfparam name="inpNumberOfDialsToRead" default="100"/> --->
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpLimit" default="100"/>
<cfparam name="inpAct" default="act_sire_auto_resend_reminder"/>
<cfparam name="inpVerboseDebug" default="1"/>

<cfset lockThreadName = inpAct&'_'&inpMod&'_'&DoModValue/>

<!---
    Run multi threaded with inpMod and DoModValue  
    
    
    Sample 10 threads
    
    inpMod = 10
    
    DoModValue = 0-9
    ... so 10 CRON jobs
    
    inpMod = 10 & DoModValue = 0
    inpMod = 10 & DoModValue = 1
    inpMod = 10 & DoModValue = 2
    inpMod = 10 & DoModValue = 3
    inpMod = 10 & DoModValue = 4
    inpMod = 10 & DoModValue = 5
    inpMod = 10 & DoModValue = 6
    inpMod = 10 & DoModValue = 7
    inpMod = 10 & DoModValue = 8
    inpMod = 10 & DoModValue = 9
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset GetEventSentAndNorespone = ""/>

<cfset GetEventStatus = ""/>
<cfset UpdatEventStatus = ""/>
<cfset inpConfirmationFlag = ''/>
<cfset inpConfirmationFlag_name = ""/>
<cfset GetSessionIdEventRespone = ""/>
<cfset valSessionID = 0/>
<cfset phone = ""/>

<cfset StartEvent = ''/>
<cfset EventId = ''/>
<cfset SMSNumber =''/>
<cfset CalendarId = ''/>
<cfset ScheduleFirst = ''/>
<cfset DTSIdFirst = ''/>
<cfset StructDelete(APPLICATION,lockThreadName)/>
<cftry>
    <cflock scope="SERVER" timeout="58" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
            <cfoutput> already run</cfoutput>
            <cfexit/>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>

    <cfquery name="GetEventSentAndNorespone" datasource="#DBSourceEBM#">
        SELECT
            ev.CalendarId_int, 
            ev.Title_vch,
            ev.AllDayFlag_int,
            ev.Start_dt,
            ev.End_dt,
            ev.URL_vch,
            ev.ClassName_vch,
            ev.EditableFlag_int,
            ev.StartEditableFlag_int,
            ev.DurationEditableFlag_int,
            ev.ResourceEditableFlag_int,
            ev.Rendering_vch,
            ev.OverlapFlag_int,
            ev.Constraint_vch,
            ev.Color_vch,
            ev.BackgroundColor_vch,
            ev.BorderColor_vch,
            ev.TextColor_vch,
            ev.eMailAddress_vch,
            ev.SMSNumber_vch,                    	
            ev.Updated_dt, 
            ev.Notes_vch, 
            ev.ConfirmationFlag_int,
            ev.PKId_bi,
            ev.UserID_int,
            ev.ConfirmationOneDTSId_int,
            qu.Scheduled_dt
        FROM
            calendar.events ev
        INNER JOIN
            simplequeue.contactqueue qu ON ev.ConfirmationOneDTSId_int = qu.DTSId_int
        WHERE
            ev.ConfirmationFlag_int = 7
        AND 
            ev.Active_int = 1
        AND 
            qu.DTSStatusType_ti = 5
        LIMIT 
            #inpLimit#    
    </cfquery>
    
    <cfif GetEventSentAndNorespone.RECORDCOUNT GT 0>
        <cfloop query="GetEventSentAndNorespone">
            <cfset EventStart = GetEventSentAndNorespone.Start_dt/>
            <cfset EventId = GetEventSentAndNorespone.PKId_bi/>
            <cfset SMSNumber = GetEventSentAndNorespone.SMSNumber_vch/>
            <cfset CalendarId = GetEventSentAndNorespone.CalendarId_int/>
            <cfset ScheduleFirst = GetEventSentAndNorespone.Scheduled_dt/>
            <cfset DTSIdFirst = GetEventSentAndNorespone.ConfirmationOneDTSId_int/>

            <cfinvoke method="AddEventReminderToQueueAutoResend" returnvariable="RetValAddEventReminderToQueue">
                <cfinvokeargument name="inpCalendarId" value="#CalendarId#">
                <cfinvokeargument name="inpEventId" value="#EventId#">
                <cfinvokeargument name="inpEventStart" value="#EventStart#">	
                <cfinvokeargument name="inpSMSNumber" value="#SMSNumber#">	
                <cfinvokeargument name="inpScheduleFirst" value="#ScheduleFirst#">	
                <cfinvokeargument name="inpDTSId"  value="#DTSIdFirst#">				
            </cfinvoke> 

            <cfif inpVerboseDebug GT 0>
                <cfdump var="#RetValAddEventReminderToQueue#">
            </cfif>

        </cfloop>
    </cfif>
    
    <cfif inpVerboseDebug GT 0>
        <cfdump var="#GetEventSentAndNorespone#">
    </cfif>


    <cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

    <cfcatch TYPE="any">
        <cfset LastErrorDetails = SerializeJSON(cfcatch) />
        <cfif inpVerboseDebug GT 0>
            ------------ Error -----------------<br/><br/>
            <cfdump var="#cfcatch#"/>
        </cfif>
    </cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
    <br/><br/>
    End time - #inpAct# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

    Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
    <BR />
</cfoutput>

<cftry>
    <cfif LastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.errorlogs
            (
                ErrorNumber_int,
                Created_dt,
                Subject_vch,
                Message_vch,
                TroubleShootingTips_vch,
                CatchType_vch,
                CatchMessage_vch,
                CatchDetail_vch,
                Host_vch,
                Referer_vch,
                UserAgent_vch,
                Path_vch,
                QueryString_vch
                )
            VALUES
            (
                101,
                NOW(),
                '#inpAct# - LastErrorDetails is Set - See Catch Detail for more info',   
                '',   
                '',     
                '', 
                '',
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
                );
        </cfquery>  
    </cfif>

    <cfcatch></cfcatch>
</cftry>


<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="58" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>

