<cfinclude template="cronjob-firewall.cfm">

<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm">

<cfparam name="DBSourceEBM" default="bishop">

<cfparam name="inpNumberOfDialsToRead" default = "1000">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_clean_csv">
<cfset arrAddSuccess = []/>
<cfset arrErrorSuccess = []/>
<cfset getLockThread = 0>
<cfset dataout = {}/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>


<!---
   	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  
<cfset LocalPageThreadList = ''/>
<cfset LASTERRORDETAILS = '' />
<cfset setStatus = UPLOADS_READY/>

 <cftry>

 	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>		   
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1>
            <cfset getLockThread = 1>         
        </cfif>
    </cflock>


	<!--- Record(s) post --->
	
    <cfquery name="GetActiveUpload" datasource="#DBSourceEBM#" result="RXGetActiveUpload">            
        SELECT
        	su.PKId_bi,
            su.ProcessType_ti,
            su.Status_ti,
            su.BatchId_bi,
            su.SubcriberList_bi,
            su.UserId_int,
            su.SendToQueue_ti,
            su.AdminUserID_int,
			su.UploadType_ti
    	FROM
        	simplexuploadstage.uploads su
        WHERE
            su.Status_ti = #UPLOADS_RAW#
        ORDER BY
        	su.PKId_bi
        LIMIT 1	
    </cfquery>

   

    <cfif GetActiveUpload.RECORDCOUNT GT 0>
    	<cftry>
    		
    	<cfset tableName = "simplexuploadstage.Contactlist_stage_"&#GetActiveUpload.PKId_bi#>
    	<cfset groupId = GetActiveUpload.SubcriberList_bi>	
    	<cfset userId_int = GetActiveUpload.UserId_int>
    	<cfset batchId = GetActiveUpload.BatchId_bi>
    	<cfif GetActiveUpload.ProcessType_ti EQ PROCESSTYPE_CAMPAIGN_BLAST>
    		<cfset setStatus = ((GetActiveUpload.SendToQueue_ti EQ 1) ? UPLOADS_READY : UPLOADS_WAITING)/>
    	</cfif>



    	<cfquery name="CountAvailableRecord" datasource="#DBSourceEBM#">
    		SELECT
    			COUNT(PKId_bi) as TOTAL
    		FROM
    			#tableName#
    		WHERE 
    			Status_ti = #STAGE_RAW#		
    	</cfquery>

    	<cfif CountAvailableRecord.TOTAL GT 0>

    		<cfinvoke component="session.sire.models.cfc.import-contact" method="CleanData" returnvariable="rxCleanData">
	            <cfinvokeargument name="inpFileRecordID" value="#GetActiveUpload.PKId_bi#">
	            <cfinvokeargument name="inpDoModValue" value="#DoModValue#">
	            <cfinvokeargument name="inpMod" value="#inpMod#">
	            <cfinvokeargument name="inpNumberOfDialsToRead" value="#inpNumberOfDialsToRead#">
			</cfinvoke>

		<cfelse> <!--- THERE IS NO RECORD -> FINISH JOBS --->

			<cfquery name="getDataValid" datasource="#SESSION.DBSourceEBM#">
                SELECT 
                    COUNT(1) AS COUNT
                FROM 
                    #tableName#
                WHERE 
                	Status_ti = #STAGE_READY# or  Status_ti=#STAGE_SUCCESS# or Status_ti =#STAGE_PROCESSING# or  Status_ti= #STAGE_CLEANING#
            </cfquery>
            <cfset dataout["valid"] = getDataValid.COUNT />

            <cfquery name="getDataInValid" datasource="#SESSION.DBSourceEBM#">
                SELECT 
                    COUNT(1) AS COUNT
                FROM 
                    #tableName#
                WHERE 
                Status_ti = #STAGE_ERROR_INVALID#     
            </cfquery>
            <cfset dataout["invalid"] = getDataInValid.COUNT />

            <cfquery name="getDuplicateData" datasource="#SESSION.DBSourceEBM#">
                SELECT 
                    COUNT(1) AS COUNT
                FROM 
                    #tableName#
                WHERE 
                	Status_ti = #STAGE_ERROR_DUPLICATED_DATA#     
            </cfquery>

            <cfset dataout["duplicate"] = getDuplicateData.COUNT />

            <cfif (dataout["duplicate"] GT 0 OR dataout["invalid"] GT 0) AND GetActiveUpload.UploadType_ti EQ 1>
            	<cfset uploadStatus = #UPLOADS_PENDING#>
            <cfelse>
            	<cfset uploadStatus = setStatus>
				<!--- no need send mail here any more. just send mail when import comptelte : file: \public\sire\models\cfm\act_sire_insert_csv_contact_to_list.cfm
            	<cfset inputData={}/>
            	<cfset inputData.UPLOADID=GetActiveUpload.PKId_bi/>
				<cfset inputData.SUBSCRIBERLISTID =groupId>
				<cfset inputData.ProcessType_ti =GetActiveUpload.ProcessType_ti>
            	<cfif GetActiveUpload.ProcessType_ti EQ 1>
            			<cfset inputData.STATUS= "Ready to send contact to subcriber list (#groupId#)"/>		
            		<cfelse>
            			<cfset inputData.STATUS= "ready to send blast (#batchId#) "/>		
            	</cfif>
            	
            	<cfset inputData.DATETIME= datetimeformat(NOW(), "yyyy-mm-dd HH:mm:ss") />

            	<!--- GET ADMIN UPLOAD EMAIL --->
            	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="rxGetUserInfoById">
		            <cfinvokeargument name="inpUserId" value="#GetActiveUpload.AdminUserID_int#">
				</cfinvoke>

	    		<cfinvoke component="session.sire.models.cfc.import-contact" method="notifyToUser" returnvariable="rxNotifyToUser">
		            <cfinvokeargument name="inpTo" value="#rxGetUserInfoById.FULLEMAIL#">
		            <cfinvokeargument name="inpSubject" value="#_UploadCompleteEmailSubject#">
		            <cfinvokeargument name="inpData" value="#inputData#">
				</cfinvoke>
				--->
            </cfif>
			<cfif GetActiveUpload.UploadType_ti EQ 2>
				<!--- new feature: check if user need to pass approval step b4 setup--->
				<cfinvoke component="session.sire.models.cfc.user-import-contact" method="GetUserMaxImportNumber" returnvariable="ReturnMaxImport">				
					<cfinvokeargument name="inpUserId" value="#userId_int#">
				</cfinvoke>    
				<cfif listfind("0,1,2",ReturnMaxImport.UPLOADPERMISSION) GT 0>
					<cfset uploadStatus="#UPLOADS_WAIT_FOR_APPROVAL#">
				</cfif>
			</cfif>

		    <!--- Update information --->
		    <cfquery name="UpdateUploadStatus" datasource="#DBSourceEBM#">
                UPDATE 
                    `simplexuploadstage`.`uploads` su
                SET 
	    	    	su.Status_ti = #uploadStatus#,
                    su.Valid_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["valid"]#"/>,
                    su.CountErrors_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#dataout["invalid"]#"/>
                WHERE   
                    su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetActiveUpload.PKId_bi#"/>
            </cfquery>
		</cfif>

		<cfcatch TYPE="any">
			<cfset LastErrorDetails = SerializeJSON(cfcatch) />
			<cfquery name="UpdateUploadStatus" datasource="#DBSourceEBM#">
			 	UPDATE
					simplexuploadstage.uploads su
				SET
					su.Status_ti = #UPLOADS_ERROR_UNEXPECTED#,
					su.ErrorDetails_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#LastErrorDetails#"/>
				WHERE	
					su.PKId_bi = #GetActiveUpload.PKId_bi#
			</cfquery>
		</cfcatch>
		</cftry>
    </cfif>
    
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
   
<cfcatch TYPE="any">
  	<cfset LastErrorDetails = SerializeJSON(cfcatch) />

</cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
	<BR />

	Errors: #LastErrorDetails#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_clean_csv - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>
