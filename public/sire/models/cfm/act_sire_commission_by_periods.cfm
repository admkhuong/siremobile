<cfinclude template="cronjob-firewall.cfm"/> 
<cfinclude template="/public/sire/configs/paths.cfm">
<cfparam name="DBSourceEBM" default="bishop"/>

<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpAct" default="act_sire_commission_by_periods"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfset lockThreadName = inpAct&'_'&inpMod&'_'&DoModValue/>



<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset LocalPageThreadList = ''/>
<cfset LASTERRORDETAILS = '' />
<cfset count = 1/>

<cffunction name="GetEnterpriseRevenue" access="remote" hint="get enterprise revenue">

	<cfargument name="inpStartDate" type="string" required="true" hint=""/>
	<cfargument name="inpEndDate" type="string" required="true" hint=""/>
	<cfargument name="inpAMI" type="string" required="true" hint=""/>
	<cfset var dataout = {}/>
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout.TYPE = '' />
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset dataout.RESULT = 0 />
	<cfset var dataout["DATAPAYMENT"] = ArrayNew(1)>
	<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
	<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	<cfset var dateDiff = 0 />

	<cfset dateDiff = dateDiff("d", startDate, endDate)/>
	
	<cfset endDate = DateFormat(endDate, 'yyyy-mm-dd')/>
	<cfset startDate = DateFormat(startDate, 'yyyy-mm-dd')/>
	<cfset var getEnterpriseUserBilling = ''/>
	<cfset var listEnterpriseUserBilling = [0]/>


	<cfset var getRevenue = '' />
	<cfset var tmpData=''/>

	<cftry>

		<cfquery name="getEnterpriseUserBilling" datasource="#Session.DBSourceREAD#">
			SELECT  e.UserId_int 
			FROM 	
				simplebilling.enterprise_user_billing_plan e                        
			INNER JOIN 
				simpleobjects.useraccount u
				ON	u.UserId_int= e.UserId_int								
			WHERE
				u.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAMI#"> 
			AND
				u.IsTestAccount_ti=0
		</cfquery>
		
		<cfloop query="getEnterpriseUserBilling">
			<cfset arrayAppend(listEnterpriseUserBilling, UserId_int)/>
		</cfloop>

		<cfquery name="getRevenue" datasource="#Session.DBSourceREAD#">
			SELECT 
				count(si.IREResultsId_bi) as total,
				si.UserId_int				
			FROM
				simplexresults.ireresults as si USE INDEX (IDX_Contact_Created)
			WHERE
				(si.IREType_int = 1 OR si.IREType_int = 2)			
			AND 
				si.UserId_int in (#arrayToList(listEnterpriseUserBilling)#)  
			AND
				si.UserId_int > 0
			AND
				LENGTH(si.ContactString_vch ) < 14
			AND 
			<cfif dateDiff LTE 7>
				si.Created_dt BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> AND 
								DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">, INTERVAL 86399 SECOND)
			<cfelse>
				date(si.Created_dt) >=  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#startDate#"> 
				AND  date(si.Created_dt) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#endDate#">        
			</cfif>
			GROUP BY si.UserId_int
		</cfquery>
		<cfloop query="getRevenue">
			<cfset tmpData={
				UserID=UserId_int,
				Value=total*0.012,
				Notes= '',
				TransactionId='',
				OrderId='',
				Type='enterprise'
			}>
			<cfset ArrayAppend(dataout["DATAPAYMENT"], tmpData) />
		</cfloop>
		<cfset dataout.RXRESULTCODE = 1/>		

		<cfcatch type="any">
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
			<cfset dataout.ERRMESSAGE = "#cfcatch.DETAIL#"/>
			<cfset dataout.TYPE = "#cfcatch.TYPE#"/>			
		</cfcatch>
	</cftry>
	
	<cfreturn dataout />
</cffunction>
<cffunction name="GetPaymentAll" access="remote" hint="Get total revenue wold pay">
	<cfargument name="inpStartDate" type="string" required="true" hint=""/>
	<cfargument name="inpEndDate" type="string" required="true" hint=""/>
	<cfargument name="inpAMI" type="string" required="true" hint=""/>

	<cfargument name="customFilter" default=""/>

	<cfset var inpStartDate = deserializeJSON(arguments.inpStartDate) />
	<cfset var inpEndDate   = deserializeJSON(arguments.inpEndDate) />
	<cfset var startDate = CreateDate(inpStartDate.year, inpStartDate.month, inpStartDate.day) />
	<cfset var endDate   = CreateDate(inpEndDate.year, inpEndDate.month, inpEndDate.day) />
	
	<cfset var dataout = {}/>
	<cfset dataout.MESSAGE = '' />
	<cfset dataout.ERRMESSAGE = '' />
	<cfset dataout.TYPE = '' />
	<cfset dataout.RXRESULTCODE = -1 />
	<cfset var GetUserList= ''>
	<cfset var GetPaymentData= ''>
	<cfset var amount=0>
	
	<cfset var listUser = ArrayNew(1)/>	
	<cfset var dataout["DATAPAYMENT"] = ArrayNew(1)>
	<cfset var tmpData={}>
	<cftry>			
		<cfquery name="GetUserList" datasource="#Session.DBSourceREAD#">
			SELECT  u.UserId_int 
			FROM 					
				simpleobjects.useraccount u											
			WHERE
				u.ami_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpAMI#"> 
			AND
				u.IsTestAccount_ti=0
		</cfquery>
		
		<cfloop query="GetUserList">
			<cfset arrayAppend(listUser, UserId_int)/>
		</cfloop>
		<cfset dataout.LISTUSER = listUser />
		<cfquery name="GetPaymentData" datasource="#Session.DBSourceREAD#">
			SELECT 
				userId_int,
				orderId,
				transactionId,
				transactionData_amount,
				moduleName,
				paymentGateway_ti,
				created
			FROM 
				simplebilling.payment_worldpay
			WHERE
				userId_int IN(<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arraytolist(listUser)#" list="yes">)
			AND
				(
					planId > 1 
					OR
					(byNumberKeyword=0 AND byNumberSMS=0 AND planId=1)
				)
			AND
				(responseText= 'Approved' OR responseText='SUCCESS')
			
			AND
				created BETWEEN <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#"> 
				AND 
                DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#endDate#">, INTERVAL 86399 SECOND)			
		</cfquery>
		<cfloop query="GetPaymentData">
			<cfset amount = transactionData_amount>
			<cfset tmpData={
				UserID=userId_int,
				Value=amount,
				Notes= moduleName,
				TransactionId=transactionId,
				OrderId=orderId,
				Type=paymentGateway_ti,
				TransactionDate= created
			}>
			<cfset ArrayAppend(dataout["DATAPAYMENT"], tmpData) />
		</cfloop>			
		
		<cfset dataout.RXRESULTCODE = 1 />
		

		
		<cfcatch type="any">
			<cfset dataout.MESSAGE = cfcatch.MESSAGE />
			<cfset dataout.ERRMESSAGE = cfcatch.DETAIL />
			<cfset dataout.TYPE = cfcatch.TYPE />
			<cfset dataout.RXRESULTCODE = -1 />
		</cfcatch>
	</cftry>

	<cfreturn dataout />
</cffunction>
<cffunction name="GetCommissionSetting" access="remote" output="false" hint="">	
	<cfargument name="inpUserId" TYPE="string" required="yes" default="" />		
	<cfset var dataout = StructNew()/>
	<cfset dataout.RXRESULTCODE = 0 />		
	<cfset dataout.MESSAGE = ""/>
	<cfset dataout.ERRMESSAGE = ""/>
	<cfset dataout.TYPE = '' />		        
	<cfset dataout["SETTING"] = ArrayNew()>
	<cfset var GetData="">
	<cfset var tmpSetting = {}>
	<cftry>
		<cfquery name="GetData" datasource="#Session.DBSourceREAD#">
			SELECT
				UserID_int,                        
				Condition_int,
				Value_dec,
				ReferredPercent_dec
			FROM
				simpleobjects.commission_setting               			
			WHERE
				UserID_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(arguments.inpUserId)#"/>			 
			ORDER BY 
				Value_dec                      
		</cfquery>
		<cfif GetData.RECORDCOUNT EQ 0>				
			<cfquery name="GetData" datasource="#Session.DBSourceREAD#">
				SELECT
					UserID_int,                        
					Condition_int,
					Value_dec,
					ReferredPercent_dec
				FROM
					simpleobjects.commission_setting               			
				WHERE
					UserID_int=0
				ORDER BY 
					Condition_int DESC, Value_dec  ASC
			</cfquery>
		</cfif>
		<cfif GetData.RECORDCOUNT GT 0>	
			<cfloop query="GetData">
				<cfset tmpSetting = {
					Condition = '#Condition_int#',
					Value = '#Value_dec#',
					Percent = '#ReferredPercent_dec#'
				} />
				
				<cfset ArrayAppend(dataout["SETTING"], tmpSetting) />
			</cfloop>
			
			<cfset dataout.RXRESULTCODE = 1 />
		<cfelse>
			<cfset dataout.MESSAGE = "There is no commission setting" />
		</cfif>
		
		<cfcatch>
			<cfset dataout = {}>
			<cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
			<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
		</cfcatch>
	</cftry>

	<cfreturn dataout>

</cffunction>

<cftry>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
		<cfif structKeyExists(APPLICATION, "#lockThreadName#")>		
			<cfoutput> already run</cfoutput>			
			<cfexit/>
		<cfelse>
			<cfset APPLICATION['#lockThreadName#'] = 1/>
			<cfset getLockThread = 1/>
		</cfif>
	</cflock>
	<cfset Lastmonth= dateadd("M", -1, now())> 
	<!--- <cfset Lastmonth= createDate("2018", "2", "1")>  --->
	
	<cfquery name="getUsersInfo" datasource="#DBSourceEBM#">
		SELECT			
			u.UserId_int,			
			u.EmailAddress_vch,			
			u.FirstName_vch,
			u.LastName_vch			
		FROM
			simpleobjects.useraccount  u
		INNER JOIN
			simpleobjects.affiliate_code_assignment a
			ON	u.UserId_int = a.UserId_int
		WHERE
			u.UserId_int NOT IN (
				SELECT
					UserId_int
				FROM
					simplebilling.affiliate_commission_by_periods
				WHERE
					MONTH(FromDate_dt) = #MONTH(Lastmonth)#
				AND
					YEAR(FromDate_dt) = #YEAR(Lastmonth)#
			)
		AND
			u.IsTestAccount_ti=0
		ORDER BY
			u.UserId_int		
		LIMIT 10
	</cfquery>
	<cfif inpVerboseDebug GT 0>
		<cfdump var="#getUsersInfo#">		
	</cfif>
	
	<cfif getUsersInfo.RECORDCOUNT GT 0>			
		<cfloop query="getUsersInfo">
			<cfset UserID=UserId_int>
			<cfset UserEmail=EmailAddress_vch>

			<cfquery name = "GetAffiliateCodeRequestDate" datasource = "#DBSourceEBM#">			
				SELECT
					CreateDate_dt
				FROM
					simpleobjects.affiliate_code_assignment
				WHERE
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#trim(UserID)#"/>			 
				AND
					Status_ti=1
			</cfquery>
			<cfset AffiliateCodeRequestDate= GetAffiliateCodeRequestDate.CreateDate_dt>
			
			
			<cfset FromDate=CreateDate(YEAR(Lastmonth ), MONTH(Lastmonth),1)>
			<cfset ToDate=CreateDate(YEAR(Lastmonth ), MONTH(Lastmonth),DaysInMonth(FromDate))>

			<cfset inpStartDate= {}>
			<cfset inpStartDate.day= DAY(FromDate)>
			<cfset inpStartDate.month= MONTH(FromDate) >
			<cfset inpStartDate.year= YEAR(FromDate) >

			<cfset inpEndDate= {}>
			<cfset inpEndDate.day= DAY(ToDate)>
			<cfset inpEndDate.month= MONTH(ToDate) >
			<cfset inpEndDate.year= YEAR(ToDate) >
			<cfset ListUserPaid = ArrayNew(1)>


			<cfset TotalPaymentOfReferred=0>
			<cfset ReferredPercent=0>
			<cfset TotalCommission=0>
			
			<cfinvoke method="GetPaymentAll" returnvariable="GetPaymentPlan">
				<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(inpStartDate)#">
				<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(inpEndDate)#">
				<cfinvokeargument  name="inpAMI"  value="#UserID#">
				
			</cfinvoke>
			
			<cfif GetPaymentPlan.RXRESULTCODE EQ 1>								
				<cfloop array="#GetPaymentPlan.DATAPAYMENT#" item="data">
					<cfset TotalPaymentOfReferred+=data.VALUE>		
					<cfif data.USERID GT 0 AND ! ArrayFindNoCase(ListUserPaid,data.USERID)>
						<cfset arrayAppend(ListUserPaid,data.USERID) />
					</cfif>
				</cfloop>		
			</cfif>			
			
			
			<!--- <cfinvoke  method="GetEnterpriseRevenue" returnvariable="RevenueEnterprise">
				<cfinvokeargument  name="inpStartDate"  value="#SerializeJSON(inpStartDate)#">
				<cfinvokeargument  name="inpEndDate"  value="#SerializeJSON(inpEndDate)#">
				<cfinvokeargument  name="inpAMI"  value="#UserID#">
			</cfinvoke>
			<cfif RevenueEnterprise.RXRESULTCODE EQ 1>
				<cfset TotalPaymentOfReferred+=RevenueEnterprise.RESULT>
				<cfloop array="#RevenueEnterprise.DATAPAYMENT#" item="data">
					<cfif data.USERID GT 0 AND ! ArrayFindNoCase(ListUserPaid,data.USERID)>
						<cfset arrayAppend(ListUserPaid,data.USERID) />
					</cfif>
				</cfloop>
			</cfif>		 --->			
			<cfset TotalUserPaid= arrayLen(ListUserPaid)>
			
			

			
			<cfinvoke  method="GetCommissionSetting" returnvariable="CommissionSetting">
				<cfinvokeargument  name="inpUserId"  value="#UserID#">				
			</cfinvoke>			
			<cfif CommissionSetting.RXRESULTCODE EQ 1>
				<cfloop index = "index" array = "#CommissionSetting.SETTING#" >
					<cfif index.VALUE LTE TotalUserPaid AND index.CONDITION EQ 1>
						<cfset ReferredPercent=index.PERCENT>					
					<cfelseif index.VALUE GT TotalUserPaid AND index.CONDITION EQ 0>
						<cfset ReferredPercent=index.PERCENT>					
						 <cfbreak>
					</cfif>					
				</cfloop>
			</cfif>
			
			<cfif inpVerboseDebug GT 0>
				Rate: 	% <cfdump var="#ReferredPercent#"/>
			</cfif>
			<cfset TotalCommission= NumberFormat(TotalPaymentOfReferred*ReferredPercent/100,".99")>
			
			
			<cfset mailto= UserEmail>
			<cfset declineDataContent = {} >
			<cfset declineDataContent.mailtoID =UserID>			
			<cfset declineDataContent.Name =FirstName_vch & " " & LastName_vch>											
			
					
			<cfif mailto NEQ "">						
				<!--- <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
					<cfinvokeargument name="to" value="#mailto#">
					<cfinvokeargument name="type" value="html">
					<cfinvokeargument name="subject" value="[SIRE] Your sub account using credit over threshold">
					<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_threshold_alert.cfm">
					<cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
				</cfinvoke>
				<cfif inpVerboseDebug GT 0>
					<cfoutput>Threshold mail sent to #mailto# <br>	</cfoutput>
				</cfif>  --->
			</cfif>						
					
			<cfquery name = "UpdateCommission" datasource = "#DBSourceEBM#" result="rsUpdateCommission">
				insert into simplebilling.affiliate_commission_by_periods(
					UserID_int,
					FromDate_dt,
					ToDate_dt,
					TotalPaymentOfReferred_dec,
					ReferredPercent_dec,
					TotalCommission_dec,
					PaymentStatus_ti
				)
				values(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#FromDate#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#ToDate#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#TotalPaymentOfReferred#"/>,						
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#ReferredPercent#"/>,						
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#TotalCommission#"/>,						
					0
				)
			</cfquery>
			<!--- <cfif RevenueEnterprise.RXRESULTCODE EQ 1>
				<cfloop array="#RevenueEnterprise.DATAPAYMENT#" item="data">
					<cfif data.USERID GT 0>
						<cfquery name = "UpdateDetail" datasource = "#DBSourceEBM#">
							insert into simplebilling.affiliate_commission_by_periods_detail(
								Commission_int,
								UserID_int,
								Type_vch,
								Value_dec,
								OrderId,
								TransactionId,
								Notes
							)
							values(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rsUpdateCommission.generatedkey#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#data.USERID#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.TYPE#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#data.VALUE#"/>,						
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.ORDERID#">,					
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.TRANSACTIONID#">,					
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.NOTES#">				
								
							)
						</cfquery>	
					</cfif>				

				</cfloop>								
			</cfif> --->
			<cfif GetPaymentPlan.RXRESULTCODE EQ 1>
				<cfloop array="#GetPaymentPlan.DATAPAYMENT#" item="data">
					<cfif data.USERID GT 0>
						<cfquery name = "UpdateDetail" datasource = "#DBSourceEBM#">
							insert into simplebilling.affiliate_commission_by_periods_detail(
								Commission_int,
								UserID_int,
								Type_vch,
								Value_dec,
								OrderId,
								TransactionId,
								Notes,
								TransactionDate_dt
							)
							values(
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#rsUpdateCommission.generatedkey#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#data.USERID#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.TYPE#">,
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" scale="2" VALUE="#data.VALUE#"/>,						
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.ORDERID#">,					
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.TRANSACTIONID#">,					
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#data.NOTES#">,												
								<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#data.TRANSACTIONDATE#">
							)
						</cfquery>	
					</cfif>				

				</cfloop>								
			</cfif>
			<cfif inpVerboseDebug GT 0>
				Commission: <cfdump var="#TotalCommission#">	
			</cfif>
			
		</cfloop>
	</cfif>
	
	


	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
	<cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfif inpVerboseDebug GT 0>
			------------ Error -----------------<br/><br/>
			<cfdump var="#cfcatch#"/>
		</cfif>
	</cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	<br/><br/>
	End time - #inpAct# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
	<BR />
</cfoutput>

<cftry>
	<cfif LastErrorDetails NEQ '' >
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.errorlogs
			(
				ErrorNumber_int,
				Created_dt,
				Subject_vch,
				Message_vch,
				TroubleShootingTips_vch,
				CatchType_vch,
				CatchMessage_vch,
				CatchDetail_vch,
				Host_vch,
				Referer_vch,
				UserAgent_vch,
				Path_vch,
				QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'#inpAct# - LastErrorDetails is Set - See Catch Detail for more info',   
				'act_sire_commission_by_periods: error',   
				'',     
				'', 
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
				);
		</cfquery>	
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive">
		<cfset StructDelete(APPLICATION,lockThreadName)/>
	</cflock>
</cfif>
