<cfinclude template="cronjob-firewall.cfm">

<cfparam name="DBSourceEBM" default="bishop">

<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_delete_preaccount">
<cfparam name="inpProcessThreadNumber" default="5"/>
<cfparam name="inpVerboseDebug" default="0"/>


<cfset arrAddSuccess = []/>
<cfset arrErrorSuccess = []/>

<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>



<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  
<cfset LocalPageThreadList = ''/>

<cfset getLockThread = 0>

 <cftry>
 	
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>	
		   <cfset StructDelete(APPLICATION,lockThreadName)/>	   
            <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1>
            <cfset getLockThread = 1>         
        </cfif>
    </cflock>

	<cfset ExitRecordLoop = 0>

	<!--- Record(s) post ---> 

    <cfquery name="GetPreAccount" datasource="#DBSourceEBM#" >            
        SELECT
        	UserId_int,
			PreAccountStep_ti,
			Created_dt
    	FROM
        	simpleobjects.useraccount
        WHERE
            PreAccountStep_ti=1			
		AND 
			Created_dt <  DATE_ADD(NOW(), INTERVAL -86400 SECOND)			
        ORDER BY
        	UserId_int Desc
        LIMIT 1	
    </cfquery>
	<cfif inpVerboseDebug EQ 1>
		<cfdump var="#GetPreAccount#"	/>
	</cfif>
	<cfloop query="GetPreAccount">
		<cfset UserID= UserId_int>	
		<cfquery name="GetShortCodeRequestID" datasource="#DBSourceEBM#" >            
			SELECT
				ShortCodeRequestId_int
			FROM
				sms.shortcoderequest
			WHERE
				RequesterId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">			
		</cfquery>
		<cfset ShortCodeRequestId= GetShortCodeRequestID.ShortCodeRequestId_int>
		<cftransaction action="begin">   
		<cftry>                        	
			<!--- start delete--->
			<cfquery name="DeleteBatch" datasource="#DBSourceEBM#" >            
				delete from simpleobjects.batch   where UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
			</cfquery>
			<cfquery name="DeleteShortCodeRequest" datasource="#DBSourceEBM#" >            
				delete from sms.shortcoderequest   where ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">
			</cfquery>
			<cfquery name="DeleteKW" datasource="#DBSourceEBM#" >            
				delete from sms.keyword   where ShortCodeRequestId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ShortCodeRequestId#">
			</cfquery>
			<cfquery name="DeleteBilling" datasource="#DBSourceEBM#" >            
				delete from simplebilling.billing   where UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
			</cfquery>
			<cfquery name="DeleteUser" datasource="#DBSourceEBM#" >            
				delete from simpleobjects.useraccount  where UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
			</cfquery>
			<!--- end delete--->
		<cftransaction action="commit" />
		*********************DELETE ACCOUT SUCCESSFULLY**************************</br>
		<cfif inpVerboseDebug EQ 1>
			<cfdump var="#UserID#"	/>
		</cfif>
		<cfcatch>
			<cftransaction action="rollback" />                        
			<cfset LastErrorDetails = SerializeJSON(cfcatch) />
			<cfdump var="#cfcatch#"/>			
		</cfcatch> 
		</cftry>
		</cftransaction>
	</cfloop>
	    
		    	
    <!--- --->
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
   
	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfdump var="#cfcatch#"/>		
	</cfcatch> 
</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_delete_preaccount - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>
<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>    
</cfif>