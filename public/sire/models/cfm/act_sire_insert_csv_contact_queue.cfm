<cfinclude template="cronjob-firewall.cfm">
<cfparam name="DBSourceEBM" default="bishop">

<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_insert_csv_contact_queue">
<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpProcessThreadNumber" default="5"/>

<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>
<cfset TotalProcTimeStart = GetTickCount() />
<!---
	Process queued up messages 

	Sire version assumptions
	
	SMS only - no fulfilment device in the middle
	
	
	
	<!--- inpTypeMask 1=Voice 2=email 3=SMS--->
                    
	
	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
	
	
	
	
	
	
	These are Status Flags to tell what state each entry in the Contact Queue is in. 
	0=Paused
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Now being fulfilled
	4 = Blocked due BR rule (duplicates) on distribution
	5 = Complete by Process
	6 = Blocked due BR rule (bad XML) on distribution
	7 = SMS Stop request killed a queued message
	8 = Fulfilled by Real Time request
	100 - Queue hold due to distribution of audio error(s)
	There will be more status types as the system is built out. 


	
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset LoadQueueResult = {} />
<cfset LoadQueueResult.Success = [] />
<cfset LoadQueueResult.Error = [] />

<cfset ErrorDetails = [] />

<cfset LocalPageThreadList = ''/>
<cfset LastErrorDetails = '' />
<cfset GetActiveUpload = '' />
<cfset RXGetActiveUpload = '' />

<cfset getLockThread = 0>

<cfset queueIds = []/>

 <cftry>
 
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1>
            <cfset getLockThread = 1>         
        </cfif>
    </cflock>

	<cfset ExitRecordLoop = 0>

	<!--- Record(s) post --->

    <cfquery name="GetActiveUpload" datasource="#DBSourceEBM#" result="RXGetActiveUpload">            
        SELECT
        	su.PKId_bi,
            su.ProcessType_ti,
            su.Status_ti,
            su.BatchId_bi,
            su.UserId_int,
            su.ColumnNumber_int,
            su.ColumnNames_vch,
            su.SendToQueue_ti
    	FROM
        	simplexuploadstage.uploads su
        WHERE
            su.Status_ti = #UPLOADS_READY#
        AND
        	su.ProcessType_ti = 2
        AND
        	su.BatchId_bi > 0
        ORDER BY
        	su.PKId_bi
        LIMIT
        	1
    </cfquery>

    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#GetActiveUpload#">
        </cfoutput>
    </cfif>
  
    <cfif RXGetActiveUpload.RECORDCOUNT GT 0>

    	<cfset tableName = "simplexuploadstage.Contactlist_stage_"&#GetActiveUpload.PKId_bi#>
    	<cfset batchId = GetActiveUpload.BatchId_bi>	
    	<cfset UserId_int = GetActiveUpload.UserId_int>
    	<cfset inpUploadStageId = GetActiveUpload.PKId_bi/>
    	<cfset Session.UserId = UserId_int/>
    	<cfset columnNames = listToArray(GetActiveUpload.ColumnNames_vch)/>

    	<cfquery name="CountAvailableRecord" datasource="#DBSourceEBM#">
    		SELECT
    			COUNT(PKId_bi) as TOTAL
    		FROM
    			#tableName#
    		WHERE
    			Status_ti = #STAGE_READY#
    		AND
    			SendToQueue_ti = 1
    	</cfquery>

    	<cfif CountAvailableRecord.TOTAL GT 0>

	        <cfquery name="GetElligibleMessages" datasource="#DBSourceEBM#">            
	            SELECT
	            	blq.PKId_bi,
	                blq.Status_ti,
	                blq.ContactStringCorrect_vch,
	                blq.TimeZone_int,
	                #GetActiveUpload.ColumnNames_vch#
	        	FROM
	            	#tableName# blq
	            WHERE 
	            	blq.PKId_bi MOD #inpMod# = #DoModValue#
	            AND 	
	            	blq.Status_ti = #STAGE_READY#
	            AND
	            	blq.SendToQueue_ti = 1
	            ORDER BY
	            	blq.PKId_bi
	        	LIMIT 
	            	#inpNumberOfDialsToRead#
	        </cfquery>

		    <cfif inpVerboseDebug GT 0>
				<cfoutput>
		            <cfdump var="#GetElligibleMessages#">
		            --------------------------------------------------------------------------------- <br>
		        </cfoutput>
		    </cfif>

		    <cfif GetElligibleMessages.RECORDCOUNT GT 0>

		    	<!--- Update CSV Queue to in process (Status=2) --->
		    	<cfquery name="HoldProcessingRecord" datasource="#session.DBSourceEBM#" result="UpdateResults">
		    		UPDATE
		    			#tableName#
		    		SET
		    			Status_ti = #STAGE_PROCESSING#,
		    			BatchId_bi = #GetActiveUpload.BatchId_bi#
		    		WHERE
		    			PKId_bi IN
		    			(
		    				#valueList(GetElligibleMessages.PKId_bi, ',')#
		    			)
		    	</cfquery>

			<!---<cfloop from="1" to="#inpProcessThreadNumber#" index="MOD_NUMBER">
		    		<cfset ProcessThreadName = "CSVContactQueueInsert_#inpMod#_#DoModValue#_#MOD_NUMBER#"/>
		    		<cfset LocalPageThreadList = listAppend(LocalPageThreadList, "#ProcessThreadName#", ',')/>

		    		<cfthread
		    			name="#ProcessThreadName#"
		    			action="run"
		    			GetElligibleMessages="#GetElligibleMessages#"
		    			batchId="#batchId#"
		    			UserId_int="#UserId_int#"
		    			LoadQueueResult="#LoadQueueResult#"
		    			ErrorDetails="#ErrorDetails#"
		    			inpProcessThreadNumber="#inpProcessThreadNumber#"
		    			MOD_NUMBER="#MOD_NUMBER#">

		    			<cfset var innerId = 1/> --->

		    			<!--- Get user shortcode for block DNC check --->
                        <cfquery datasource="#Session.DBSourceEBM#" name="GetShortCodeData">
                                SELECT
                                    sc.ShortCode_vch
                                FROM 
                                    sms.shortcode sc 
                                JOIN     
                                    sms.shortcoderequest scr
                                ON 
                                    sc.ShortCodeId_int = scr.ShortCodeId_int
                                WHERE        
                                    scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId_int#">
                                ORDER BY
                                    scr.ShortCodeRequestId_int ASC
                                LIMIT 1    
                        </cfquery>

					    <cfif inpVerboseDebug GT 0>
							<cfoutput>
							<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
					            <br>************************************Enter loop: #dateDiff("s", ServerStartTime, ServerEndTime)#<br>
					        </cfoutput>
					    </cfif>

					    <cfloop query="GetElligibleMessages">
					    	<!--- <cfif (innerId++ MOD inpProcessThreadNumber) EQ (MOD_NUMBER-1)> --->

						        <cftry>
						    		<!--- Prepare CDFs --->
				                    <cfloop collection="#GetElligibleMessages#" item="whichField">
				                    	<cfif arrayFind(columnNames, whichField)>
					                    	<cfif inpVerboseDebug GT 0>
							                    <cfoutput>
							                    	#whichField# - #GetElligibleMessages[#whichField#]# <br>
							                    </cfoutput>
					                    	</cfif>
					                       	<cfset FORM[#whichField#] = GetElligibleMessages[#whichField#]/>
				                    	</cfif>
				                    </cfloop>

							         <cfinvoke component="session.sire.models.cfc.import-contact" method="AddToQueue" returnvariable="rx">
							            <cfinvokeargument name="inpBatchId" value="#batchId#">
							            <cfinvokeargument name="inpContactString" value="#GetElligibleMessages.ContactStringCorrect_vch#">
							            <cfinvokeargument name="inpUserId" value="#UserId_int#">
							            <cfinvokeargument name="inpBlockDuplicates" value="1">
							            <cfinvokeargument name="inpShortCode" value="#GetShortCodeData.ShortCode_vch#"/>
							            <cfinvokeargument name="inpTimeZone" value="#GetElligibleMessages.TimeZone_int#"/>
							            <cfinvokeargument name="inpUseInputTimeZone" value="1"/>
							        </cfinvoke>

						            <cfif rx.RXRESULTCODE EQ 1>
						            	<!--- Add to queue success --->
										<cfset arrayAppend(LoadQueueResult.Success, GetElligibleMessages.PKId_bi)/>

										<cfset arrayAppend(queueIds, rx.LASTQUEUEDUPID)/>

										<cfif inpVerboseDebug GT 0>
											<cfoutput>
												LastQueueId = #rx.LASTQUEUEDUPID#<br>
											</cfoutput>
										</cfif>
						            <cfelse>
									
						            	<!--- Add to queue failed, update error to stage table --->
										<cfif rx.RXRESULTCODE NEQ STAGE_ERROR_BLOCKED>
						                	<cfset arrayAppend(LoadQueueResult.Error, GetElligibleMessages.PKId_bi)/>
										</cfif>
						                <!--- <cfset arrayAppend(ErrorDetails, "#GetElligibleMessages.PKId_bi#-#rx.MESSAGE#") /> --->

						                ********** ERROR on AddToQueue **********<BR>	   
									    <cfif inpVerboseDebug GT 0>
											<cfoutput>
									            #rx.MESSAGE#
									        </cfoutput>
									    </cfif>

										<cfquery name="UpdateStageTableError" datasource="#Session.DBSourceEBM#">
								        	UPDATE
								            	#tableName#
								            SET
								            	Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#rx.RXRESULTCODE#"/>,
								                Note_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rx.MESSAGE#. #rx.ERRMESSAGE#"/>
								            WHERE 
								            	PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetElligibleMessages.PKId_bi#"/>
										</cfquery>
						            </cfif>

				                    <cfif inpVerboseDebug GT 0>
				                    	<cfoutput>-----------------------<br></cfoutput>
				                    </cfif>

						            <cfcatch type="any">
						                
						                <!--- Log error? --->
						                ********** ERROR on LOOP **********<BR>	   
									    <cfif inpVerboseDebug GT 0>
											<cfoutput>
									            <cfdump var="#cfcatch#">
									        </cfoutput>
									    </cfif>

						                <cfset arrayAppend(LoadQueueResult.Error, GetElligibleMessages.PKId_bi)/>
						                <!--- <cfset arrayAppend(ErrorDetails, "#GetElligibleMessages.PKId_bi#-#cfcatch.MESSAGE#") /> --->

										<cfquery name="UpdateStageTableError" datasource="#Session.DBSourceEBM#">
								        	UPDATE
								            	#tableName#
								            SET
								            	Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#STAGE_ERROR_UNEXPECTED#"/>,
								                Note_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#cfcatch.MESSAGE#. #cfcatch.DETAIL#"/>
								            WHERE 
								            	PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetElligibleMessages.PKId_bi#"/>
										</cfquery>

						            </cfcatch>

						        </cftry>
					    	<!--- </cfif> --->

					    </cfloop>
				<!---</cfthread>
		    	</cfloop>

		    	<cfthread action="join" name="#LocalPageThreadList#" timeout="12000000"/> --->

			    <cfif inpVerboseDebug GT 0>
					<cfoutput>
						<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
						<cfset LoopTime = dateDiff("s", ServerStartTime, ServerEndTime)/>
			            <br>************************************Loop finished: #LoopTime#<br>
			            <br>************************************Average time: #(LoopTime/inpNumberOfDialsToRead)#<br>
			        </cfoutput>
			    </cfif>

			    <!--- Update success record status - 5 --->
			    <cfif arrayLen(LoadQueueResult.Success) GT 0>
					<cfquery name="UpdateStageTableStatus" datasource="#Session.DBSourceEBM#">
			        	UPDATE
			            	#tableName#
			            SET
			                Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#STAGE_SUCCESS#"/>,
			                Note_vch = "Loaded to queue"
			            WHERE 
			            	PKId_bi IN
			            	(
			            		#arrayToList(LoadQueueResult.Success, ',')#
			            	)
					</cfquery>

					<cfquery name="UpdateUploadStageCount" datasource="#Session.DBSourceEBM#">
						UPDATE
							simplexuploadstage.uploads
						SET
							LoadSuccess_int = LoadSuccess_int + <cfqueryparam cfsqltype="cf_sql_integer" value="#arrayLen(LoadQueueResult.Success)#"/>
						WHERE
							PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUploadStageId#"/>
					</cfquery>
			    </cfif>

			    <cfif arrayLen(LoadQueueResult.Error) GT 0>
					<!--- <cfquery name="UpdateStageTableStatus" datasource="#Session.DBSourceEBM#">
			        	UPDATE
			            	#tableName#
			            SET
			                Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="6"/>
			            WHERE 
			            	PKId_bi IN
			            	(
			            		#arrayToList(LoadQueueResult.Error, ',')#
			            	)
					</cfquery> --->

					<cfquery name="UpdateUploadStageCount" datasource="#Session.DBSourceEBM#">
						UPDATE
							simplexuploadstage.uploads
						SET
							LoadError_int = LoadError_int + <cfqueryparam cfsqltype="cf_sql_integer" value="#arrayLen(LoadQueueResult.Error)#"/>
							<!---,ErrorDetails_vch = CONCAT(ErrorDetails_vch, <cfqueryparam cfsqltype="cf_sql_varchar" value="#SerializeJSON(ErrorDetails)#"/>)--->
						WHERE
							PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUploadStageId#"/>
					</cfquery>
			    </cfif>

			    <!--- Check if uploads had been paused, if so pause all inserted contactqueue --->
			    <cfif arrayLen(queueIds) GT 0>
			    	<cfquery name="checkUploadStatus" datasource="#Session.DBSourceEBM#">
			    		SELECT
			    			PKId_bi
			    		FROM
			    			simplexuploadstage.uploads
			    		WHERE
			    			PKId_bi = "#GetActiveUpload.PKId_bi#"
			    		AND
			    			Status_ti = #UPLOADS_PAUSED#
			    	</cfquery>

			    	<cfif checkUploadStatus.RECORDCOUNT GT 0>
			    		<cfquery name="UpdateContactQueuePaused" datasource="#Session.DBSourceEBM#">
			    			UPDATE
			    				simplequeue.contactqueue
			    			SET
			    				DTSStatusType_ti = #CONTACTQUEUE_PAUSED#
			    			WHERE
			    				DTSId_int IN (#arrayToList(queueIds)#, ',')
			    		</cfquery>
			    	</cfif>
			    </cfif>

			    <cfif inpVerboseDebug GT 0>
			    	<cfoutput>
			    		--------------------------------------------------------------------------------- <br>
			    		Load Error: #arrayLen(LoadQueueResult.ERROR)# - #arrayToList(LoadQueueResult.ERROR)# <br>
			    		Load Success: #arrayLen(LoadQueueResult.SUCCESS)# - #arrayToList(LoadQueueResult.SUCCESS)# <br>
			    		<!--- --------------------------------------------------------------------------------- <br>
			    		Errors:
			    		<cfdump var="#ErrorDetails#"/> --->
					</cfoutput>
			    </cfif>
		    </cfif>
		<cfelse>
			<cfquery name="CheckForRemainContact" datasource="#Session.DBSourceEBM#">
				SELECT
					COUNT(PKId_bi) AS TOTALREMAIN
				FROM
					#tableName#
				WHERE
					SendToQueue_ti = 0
				AND
					Status_ti = #STAGE_READY#
			</cfquery>

			<cfif CheckForRemainContact.TOTALREMAIN GT 0>
				<cfquery name="UpdateUploadStage" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplexuploadstage.uploads
					SET
						Status_ti = #UPLOADS_WAITING#
					WHERE
						PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUploadStageId#"/>
				</cfquery>
			<cfelse>
				<cfquery name="UpdateUploadStage" datasource="#Session.DBSourceEBM#">
					UPDATE
						simplexuploadstage.uploads
					SET
						Status_ti = IF(LoadError_int > ErrorsIgnore_int,#UPLOADS_ERROR_FAILED#,#UPLOADS_SUCCESS#)
					WHERE
						PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpUploadStageId#"/>
				</cfquery>
			</cfif>
		</cfif>
    </cfif>
    
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
   
<cfcatch TYPE="any">
	<cfif isStruct(RXGetActiveUpload)>
		<cfif GetActiveUpload.RECORDCOUNT GT 0>
			<cfquery name="UpdateUploadStage" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplexuploadstage.uploads
				SET
					Status_ti = #UPLOADS_ERROR_FAILED#,
					ErrorDetails_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#cfcatch.message# . #cfcatch.detail#. #cfcatch.queryError#"/>
				WHERE
					PKId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetActiveUpload.PKId_bi#"/>
			</cfquery>
		</cfif>
	</cfif>
	********** ERROR on PAGE **********<BR>	   
    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>
    <cfset LastErrorDetails = cfcatch.detail/>
</cfcatch> 
</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_insert_csv_contact_queue - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>    
</cfif>