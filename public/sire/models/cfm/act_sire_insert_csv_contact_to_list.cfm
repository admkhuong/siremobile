<cfinclude template="cronjob-firewall.cfm">
<cfinclude template="/public/sire/configs/paths.cfm">

<cfparam name="DBSourceEBM" default="bishop">

<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_insert_csv_contact_to_list">
<cfparam name="RXAddContactStringToList" default="">
<cfparam name="inpProcessThreadNumber" default="5"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<cfset arrAddSuccess = []/>
<cfset arrErrorSuccess = []/>

<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<!--- <cfset TotalProcTimeStart = GetTickCount() /> --->

<!---
	Process queued up messages 

	Sire version assumptions
	
	SMS only - no fulfilment device in the middle
	
	
	
	<!--- inpTypeMask 1=Voice 2=email 3=SMS--->
                    
	
	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
	
	
	
	
	
	
	These are Status Flags to tell what state each entry in the Contact Queue is in. 
	0=Paused
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Now being fulfilled
	4 = Blocked due BR rule (duplicates) on distribution
	5 = Complete by Process
	6 = Blocked due BR rule (bad XML) on distribution
	7 = SMS Stop request killed a queued message
	8 = Fulfilled by Real Time request
	100 - Queue hold due to distribution of audio error(s)
	There will be more status types as the system is built out. 


	
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  
<cfset LocalPageThreadList = ''/>

<cfset getLockThread = 0>

 <cftry>
 	
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>			   
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1>
            <cfset getLockThread = 1>         
        </cfif>
    </cflock>

	<cfset ExitRecordLoop = 0>

	<!--- Record(s) post --->

    <cfquery name="GetActiveUpload" datasource="#DBSourceEBM#" result="RXGetActiveUpload">            
        SELECT
        	su.PKId_bi,
            su.ProcessType_ti,
            su.Status_ti,
            su.BatchId_bi,
            su.SubcriberList_bi,
            su.ColumnNumber_int,
            su.ColumnNames_vch,	
			su.SubcriberList_bi,		
            su.UserId_int
    	FROM
        	simplexuploadstage.uploads su
        WHERE
            (
				su.Status_ti = #UPLOADS_READY#
				OR
				(su.Status_ti = #UPLOADS_PENDING# AND UploadType_ti=2)
			)

        AND
        	su.ProcessType_ti = 1
        ORDER BY
        	su.PKId_bi
        LIMIT 1	
    </cfquery>
	<cfdump var="#GetActiveUpload#"/>


    <cfif GetActiveUpload.RECORDCOUNT GT 0>
    	<cfset tableName = "simplexuploadstage.Contactlist_stage_"&#GetActiveUpload.PKId_bi#>
    	<cfset groupId = GetActiveUpload.SubcriberList_bi>	
    	<cfset userId_int = GetActiveUpload.UserId_int>
    	<cfset columnNames = listToArray(GetActiveUpload.ColumnNames_vch)/>

    	<cfquery name="CountAvailableRecord" datasource="#DBSourceEBM#">
    		SELECT
    			COUNT(PKId_bi) as TOTAL
    		FROM
    			#tableName#
    		WHERE
    			Status_ti = #STAGE_READY#
    	</cfquery>
		<cfdump var="#CountAvailableRecord#"/>
    	<cfif CountAvailableRecord.TOTAL GT 0>
	        <cfquery name="GetElligibleMessages" datasource="#DBSourceEBM#">            
	            SELECT
	            	blq.PKId_bi,
	                blq.ContactStringCorrect_vch,
	                blq.Status_ti,
	                #GetActiveUpload.ColumnNames_vch#
	        	FROM
	            	#tableName# blq
	            WHERE 
	            	blq.PKId_bi MOD #inpMod# = #DoModValue#
	            AND 	
	            	blq.Status_ti = #STAGE_READY#
	            ORDER BY
	            	blq.PKId_bi
	        	LIMIT 
	            	#inpNumberOfDialsToRead#
	        </cfquery>

	         <cfif inpVerboseDebug GT 0>
				<cfoutput>
		            <cfdump var="#GetElligibleMessages#">
		            --------------------------------------------------------------------------------- <br>
		            <cfdump var="#columnNames#"/>
		        </cfoutput>
		    </cfif>

	        <cfif GetElligibleMessages.RECORDCOUNT GT 0> <!--- check if have record -> continue process, else update upload staus = 5 (success) --->
	        	<!--- UPDATE All record = 2 - inprogess --->

	        	<cfquery name="updateElligibleMessages" datasource="#DBSourceEBM#">            
		            UPDATE
		            	#tableName# blq
		            SET
		            	blq.Status_ti = #STAGE_PROCESSING#	
		    		WHERE
		    			PKId_bi IN
		    			(
		    				#valueList(GetElligibleMessages.PKId_bi, ',')#
		    			)
		        </cfquery>

		    	<!--- <cfloop from="1" to="#inpProcessThreadNumber#" index="MOD_NUMBER">
		    		<cfset ProcessThreadName = "CSVContactListInsert_#inpMod#_#DoModValue#_#MOD_NUMBER#"/>
		    		<cfset LocalPageThreadList = listAppend(LocalPageThreadList, "#ProcessThreadName#", ',')/>

		    		<cfthread
		    			name="#ProcessThreadName#"
		    			action="run"
		    			GetElligibleMessages="#GetElligibleMessages#"
		    			groupId="#groupId#"
		    			userId_int="#userId_int#"
		    			arrAddSuccess="#arrAddSuccess#"
		    			arrErrorSuccess="#arrErrorSuccess#"
		    			inpProcessThreadNumber="#inpProcessThreadNumber#"
		    			MOD_NUMBER="#MOD_NUMBER#">

		    			<cfset var innerId = 1/> --->

		    			
		    			<!--- remove old method: 
		    			<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="shortCode"></cfinvoke>
						--->
						<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode">
							<cfinvokeargument name="inpBatchId" value="#GetActiveUpload.BatchId_bi#"/>
							<cfinvokeargument name="inpUserIDRequest" value="#GetActiveUpload.UserId_int#"/>							
						</cfinvoke>
						<cfdump var="#shortCode#"/>
				        <cfloop query="GetElligibleMessages">
					    	<!--- <cfif (innerId++ MOD inpProcessThreadNumber) EQ (MOD_NUMBER-1)> --->

					    		<!--- Prepare CDFs --->
			                    <cfloop collection="#GetElligibleMessages#" item="whichField">
			                    	<cfif arrayFind(columnNames, whichField)>
				                    	<cfif inpVerboseDebug GT 0>
						                    <cfoutput>
						                    	#whichField# - #GetElligibleMessages[#whichField#]# <br>
						                    </cfoutput>
				                    	</cfif>
				                       	<cfset FORM[#whichField#] = GetElligibleMessages[#whichField#]/>
			                    	</cfif>
			                    </cfloop>

								<cfinvoke component="session.sire.models.cfc.import-contact" method="AddContactStringToList" returnvariable="RXAddContactStringToList">
						            <cfinvokeargument name="INPCONTACTSTRING" value="#GetElligibleMessages.ContactStringCorrect_vch#" />
						            <cfinvokeargument name="INPCONTACTTYPEID" value="3" />
						            <cfinvokeargument name="INPGROUPID" value="#groupId#" />
						            <cfinvokeargument name="INPUSERID" value="#userId_int#" />
						            <cfinvokeargument name="inpShortCode" value="#shortCode.SHORTCODE#"/>
						            <cfinvokeargument name="INPCDFDATA" value="#FORM#" />
									<cfinvokeargument name="inpUploadTableRecordID" value="#GetActiveUpload.PKId_bi#" />									
						        </cfinvoke>
								
								<cfdump var="#RXAddContactStringToList#">
								
						        <cfif RXAddContactStringToList.RXRESULTCODE EQ 1> <!--- update success --->
						        	<!--- <cfquery name="UpdateContactStatus" datasource="#DBSourceEBM#"> 
						        		UPDATE 
						        			#tableName# blq
						        		SET 
						        			blq.Status_ti = 5
						        		WHERE 
						        			blq.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetElligibleMessages.PKId_bi#"/>
						        	</cfquery> --->
						        	<cfset arrayAppend(arrAddSuccess,GetElligibleMessages.PKId_bi)/>
						        <cfelse>		
						        									
									<cfif RXAddContactStringToList.ERRORCODE EQ "#STAGE_ERROR_BLOCKED#">
										********** ERROR Because DNC Contact **********<BR>	   										
										<cfquery name="UpdateValidRecordWhenBlockPhone" datasource="#Session.DBSourceEBM#">
											UPDATE  
												simplexuploadstage.uploads
											SET
												Valid_int=Valid_int-1,
												Dnc_int=Dnc_int+1
											WHERE
												PKId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetActiveUpload.PKId_bi#"> 
										</cfquery>
										<cfquery name="updateInvalidData" datasource="#Session.DBSourceEBM#" result="RXUpdateInvalidData">
											UPDATE
												#tableName#
											SET
												Status_ti =  #STAGE_ERROR_BLOCKED#,
												Note_vch = "#RXAddContactStringToList.MESSAGE#. #RXAddContactStringToList.ERRMESSAGE#"
											WHERE
												PKId_bi = #GetElligibleMessages.PKId_bi#
										</cfquery>
									<cfelse>
										<!--- update fail --->
										<cfset arrayAppend(arrErrorSuccess,GetElligibleMessages.PKId_bi)/>

										********** ERROR on AddToQueue **********<BR>	   										
										<cfquery name="updateInvalidData" datasource="#Session.DBSourceEBM#" result="RXUpdateInvalidData">
											UPDATE
												#tableName#
											SET
												Status_ti =  #STAGE_ERROR_FAILED#,
												Note_vch = "#RXAddContactStringToList.MESSAGE#. #RXAddContactStringToList.ERRMESSAGE#"
											WHERE
												PKId_bi = #GetElligibleMessages.PKId_bi#
										</cfquery>
									</cfif>
									<cfif inpVerboseDebug GT 0>
										<cfoutput>
											#RXAddContactStringToList.MESSAGE#<br/>
										</cfoutput>
									</cfif>	
									
						        </cfif>
						    <!--- </cfif> --->
				    	</cfloop>

		    		<!--- </cfthread>
		    	</cfloop>
		
		    	<cfthread action="join" name="#LocalPageThreadList#" timeout="12000000"/> --->



		    	<!--- UPDATE STATUS --->
		    	<cfif !arrayIsEmpty(arrAddSuccess)>
	                <cfquery name="updateSuccessData" datasource="#Session.DBSourceEBM#">
	                    UPDATE #tableName#
	                    SET Status_ti =  #STAGE_SUCCESS#
	                    WHERE PKId_bi IN (#ArrayToList(arrAddSuccess)#)
	                </cfquery>

	                <cfquery name="updateSuccessUpload" datasource="#Session.DBSourceEBM#">
	                    UPDATE 
		    				simplexuploadstage.uploads su
	                    SET 
	                    	LoadSuccess_int =  LoadSuccess_int + <cfqueryparam cfsqltype="cf_sql_integer" value="#arrayLen(arrAddSuccess)#"/>
	                    WHERE 
	                    	su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>
	                </cfquery>

	            </cfif>

	            <cfif !arrayIsEmpty(arrErrorSuccess)>


	                 

	                <cfquery name="updateFailUpload" datasource="#Session.DBSourceEBM#" result="RXUpdateFailUpload">
	                    UPDATE 
		    				simplexuploadstage.uploads su
	                    SET 
	                    	LoadError_int =  LoadError_int + <cfqueryparam cfsqltype="cf_sql_integer" value="#arrayLen(arrErrorSuccess)#"/>
	                    WHERE 
	                    	su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>
	                </cfquery>
	            </cfif>
			</cfif>
		<cfelse>
			<!--- check b4 update--->
			<cfquery name="CheckStillHaveReordNeedImport" datasource="#DBSourceEBM#"> 			
				SELECT
					PKId_bi
				FROM
					simplexuploadstage.uploads su
				WHERE 
	    			su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>
				AND
					(
							CountErrors_int > 0
							OR
							Duplicates_int > 0
					)
			</cfquery>

			<cfif CheckStillHaveReordNeedImport.RECORDCOUNT GT 0>
				<cfquery name="UpdateStatusFinal" datasource="#DBSourceEBM#"> 
					UPDATE 
						simplexuploadstage.uploads su
					SET 
						su.Status_ti=#UPLOADS_OUTSTANDING#
					WHERE 
						su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>
				</cfquery>
			<cfelse>
				<cfquery name="UpdateStatusFinal" datasource="#DBSourceEBM#"> 
					UPDATE 
						simplexuploadstage.uploads su
					SET 
						su.Status_ti = IF(su.LoadError_int > su.ErrorsIgnore_int,#UPLOADS_ERROR_FAILED#,#UPLOADS_SUCCESS#) 
					WHERE 
						su.PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>
				</cfquery>
			</cfif>
			<cfquery name="GetFinalStatus" datasource="#DBSourceEBM#">
				SELECT
					Status_ti
				FROM
					simplexuploadstage.uploads su
				WHERE 
					PKId_bi = <cfqueryparam cfsqltype="cf_sql_bigint" value="#GetActiveUpload.PKId_bi#"/>	
			</cfquery>
			<!--- send mail--->
			<cfset inputData={}/>
			<cfset inputData.UPLOADID=GetActiveUpload.PKId_bi/>			
			<cfset inputData.ProcessType_ti =GetActiveUpload.ProcessType_ti>
			<cfset inputData.SUBSCRIBERLISTID =GetActiveUpload.SubcriberList_bi>
			<cfset inputData.STATUS= GetFinalStatus.Status_ti/>					
			
			<cfset inputData.DATETIME= datetimeformat(NOW(), "yyyy-mm-dd HH:mm:ss") />

			<!--- GET ADMIN UPLOAD EMAIL --->
			<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="rxGetUserInfoById">
				<cfinvokeargument name="inpUserId" value="#GetActiveUpload.UserId_int#">
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.import-contact" method="notifyToUser" returnvariable="rxNotifyToUser">
				<cfinvokeargument name="inpTo" value="#rxGetUserInfoById.FULLEMAIL#">
				<cfinvokeargument name="inpSubject" value="#_UploadCompleteEmailSubject#">
				<cfinvokeargument name="inpData" value="#inputData#">
			</cfinvoke>
		</cfif>
    </cfif>
    
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
   
<cfcatch TYPE="any">
  	<cfset LastErrorDetails = SerializeJSON(cfcatch) />
  	<cfdump var="#cfcatch#"/>
  	<cfabort>
</cfcatch> 
</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_insert_csv_contact_queue - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>    
</cfif>