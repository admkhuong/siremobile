<cfinclude template="cronjob-firewall.cfm">

<cfparam name="DBSourceEBM" default="bishop">

<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDialerIPAddr" default="act_sire_insert_individual_contact_to_list">
<cfparam name="RXAddContactStringToList" default="">
<cfparam name="inpProcessThreadNumber" default="5"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<cfset arrAddSuccess = []/>
<cfset arrErrorSuccess = []/>

<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>


<!---
	
	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
	
	
	
	
	
	
	list status
	
	1 = complete
	2 = fail
	3=  block dnc
	
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  
<cfset LocalPageThreadList = ''/>

<cfset getLockThread = 0>

 <cftry>
 	
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>		   
            <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1>
            <cfset getLockThread = 1>         
        </cfif>
    </cflock>

	<cfset ExitRecordLoop = 0>

	<!--- Record(s) post ---> 

    <cfquery name="GetActiveUpload" datasource="#DBSourceEBM#" result="RXGetActiveUpload">            
        SELECT
        	data.PKId_bi,
			data.UserId_int,
			data.SubscriberListId_int,
			data.Status_ti,
			data.ListContactString_vch  
    	FROM
        	simplequeue.import_individual_quee data
        WHERE
            data.Status_ti = #INDIVIDUAL_IMPORT_READY#
        
        ORDER BY
        	data.PKId_bi
        LIMIT 1	
    </cfquery>
	<cfdump var="#GetActiveUpload#"	/>


    <cfif GetActiveUpload.RECORDCOUNT GT 0>
    	
    	<cfset ImportId = GetActiveUpload.PKId_bi>	
		<cfset SubscriberListId = GetActiveUpload.SubscriberListId_int>	
		<cfset UserID = GetActiveUpload.UserId_int>	
		<cfset ListContactString = GetActiveUpload.ListContactString_vch>	
    			
    	<cfif UserID GT 0 AND SubscriberListId GT 0 AND Listlen(ListContactString) GT 0>	        
			<!--- update status to processing--->
			<cfquery name="UpdateStatus" datasource="#DBSourceEBM#"> 
				UPDATE
					simplequeue.import_individual_quee
				SET
					Status_ti=#INDIVIDUAL_IMPORT_PROCESSING#
				WHERE
					PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ImportId#"/>
			</cfquery>
	         <cfif inpVerboseDebug GT 0>
				<cfoutput>		            
		            --------------------------------------------------------------------------------- <br>
		            Import ID :<cfdump var="#ImportId#"/><br>
					User ID :<cfdump var="#UserID#"/><br>
					Subscriber ID :<cfdump var="#SubscriberListId#"/><br>
					List Contact :<cfdump var="#ListContactString#"/><br>
		        </cfoutput>
		    </cfif>
			<!--- start Import--->			
			<cfinvoke method="ImportListContactToSubcriberList" component="session.sire.models.cfc.user-import-contact" returnvariable="ImportResult">
				<cfinvokeargument name="inpListNumber" value="#ListContactString#"/>
				<cfinvokeargument name="inpSubcriberListID" value="#SubscriberListId#"/>							
				<cfinvokeargument name="inpUserId" value="#UserID#"/>							
				<cfinvokeargument name="inpImportId" value="#ImportId#"/>
			</cfinvoke>
			<cfdump var="#ImportResult#"/>
			<!--- update status to processing--->
			<cfquery name="UpdateStatus" datasource="#DBSourceEBM#"> 
				UPDATE
					simplequeue.import_individual_quee
				SET
					Status_ti=#INDIVIDUAL_IMPORT_COMPLETE#
				WHERE
					PKId_bi=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ImportId#"/>
			</cfquery>
		</cfif>
	</cfif>
	    
		    	
    <!--- --->
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />
   
	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfdump var="#cfcatch#"/>		
	</cfcatch> 
</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_insert_individual_contact_to_list - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>
<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>    
</cfif>