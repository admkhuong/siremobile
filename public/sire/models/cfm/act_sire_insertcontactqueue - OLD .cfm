<!--- <cfinclude template="cronjob-firewall-contact-queue.cfm"> --->

<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="CurrDialerArrayIndex" default="0">
<cfparam name="DeviceCount" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDistributionProcessId" default="0">
<cfparam name="inpDialerIPAddr" default="0">
<cfparam name="inpProcessName" default="act_sire_insertcontactqueue">
<cfparam name="inpVerboseDebug" default="0">


<!--- Use the constants for Blast Log and Blas tLog Queue States --->
<!--- When working on radically new code it is eaier to chage the values here than in each file used --->
<cfinclude template="../../../../session/cfc/csc/constants.cfm">


<!---
	Process queued up messages 

	Sire version assumptions
	
	SMS only - no fulfilment device in the middle
	
	
	
	<!--- inpTypeMask 1=Voice 2=email 3=SMS--->
                    
	
	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
	
	
	
	
	
	
	These are Status Flags to tell what state each entry in the Contact Queue is in. 
	0=Paused
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Now being fulfilled
	4 = Blocked due BR rule (duplicates) on distribution
	5 = Complete by Process
	6 = Blocked due BR rule (bad XML) on distribution
	7 = SMS Stop request killed a queued message
	8 = Fulfilled by Real Time request
	100 - Queue hold due to distribution of audio error(s)
	There will be more status types as the system is built out. 


	
--->

<cfset lockThreadName = inpProcessName & '_' & inpDialerIPAddr & '_' & inpMod & '_' & DoModValue/>
<cfset getLockThread = 0>

<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
    <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
       <cfoutput> already run</cfoutput>
       <cfexit>
    <cfelse>
        <cfset APPLICATION['#lockThreadName#'] = 1>
        <cfset getLockThread = 1>         
    </cfif>
</cflock>

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
  

<cfset TQCheckPoint = 0>
<cfset ProcessedRecordCount = 0>
<cfset SuppressedCount = 0>
<cfset DuplicateRecordCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset InvalidRecordCount = 0>
<cfset DistributionCount = 0>
<cfset DistributionCountRegular = 0>
<cfset DistributionCountSecurity = 0>
<cfset MaxInvalidRecords = 1>
<cfset ErrorCount = 0>
<cfset LastErrorDetails = ''>


<cftry>

    <cfquery name="GetBlastLog" datasource="#Session.DBSourceEBM#">
        SELECT
            PKId_int,
            Status_int,
            APIRequestJSON_vch
        FROM
            simpleobjects.batch_blast_log
        WHERE
            Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_PROCESSING#">
        ORDER BY
            PKId_int ASC
        LIMIT
            1
    </cfquery>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
	        
	        BLAST_LOG_QUEUE_READY = #BLAST_LOG_QUEUE_READY# <br/>
            <cfdump var="#GetBlastLog#">
        </cfoutput>
    </cfif>

    <cfif GetBlastLog.RECORDCOUNT GT 0>
        <cfset ExitRecordLoop = 0>

        <!--- Record(s) post --->
        <cfif inpNumberOfDialsToRead GT 0>

			<!--- Check over all counts for Batch Log Id - This count is accross all possible threads --->
            <cfquery name="CountAvailableRecord" datasource="#Session.DBSourceEBM#">
                SELECT
                    COUNT(Id_int) as TOTAL
                FROM
                    simplequeue.batch_load_queue
                WHERE
                    BlastLogId_int = #GetBlastLog.PKId_int#
                AND
                    Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_READY#">  
            </cfquery>

            <cfif CountAvailableRecord.TOTAL GT 0>
                
                <!--- Grab the current threads records to process inpNumberOfDialsToRead should not be too high to keep memory usage stable 100 is a good number--->        
                <cfquery name="GetElligibleMessages" datasource="#Session.DBSourceEBM#">            
                    SELECT
                        blq.Id_int,
                        blq.BatchId_bi,
                        blq.GroupId_int,
                        blq.UserId_int,
                        blq.ContactString_vch,
                        blq.ContactType_int,
                        blq.BlastLogId_int,
                        blq.ShortCode_vch,
                        blq.OperatorId_int
                    FROM
                        simplequeue.batch_load_queue blq
                    WHERE
                        BlastLogId_int = #GetBlastLog.PKId_int#
                    AND
                        blq.Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_READY#">
                    AND 
                        blq.Id_int MOD #inpMod# = #DoModValue#
                    AND
                        blq.ContactType_int = 3
                    ORDER BY
                        blq.Id_int
                    LIMIT 
                        #inpNumberOfDialsToRead#
                </cfquery>

                <cfif inpVerboseDebug GT 0>
                    <cfoutput>                  
                        <cfdump var="#GetElligibleMessages#"> <BR>
                    </cfoutput>
                </cfif>

				<!--- This is to make sure no one else is grabbing these records while we are looping --->
                <cfquery name="HoldProcessingRecord" datasource="#session.DBSourceEBM#" result="UpdateResults">
                    UPDATE
                        simplequeue.batch_load_queue
                    SET
                        Status_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_QUEUE_PROCESSING#"> 
                    WHERE
                        Id_int IN
                        (
                            #valueList(GetElligibleMessages.Id_int, ',')#
                        )
                </cfquery>

                <cfloop query="GetElligibleMessages">

					<!--- Make sure the sesion is set to the current blats's User Id - used in CFCs --->
                    <cfset Session.UserId = GetElligibleMessages.UserId_int />
      
                    <cfif inpVerboseDebug GT 0>
                        <cfoutput>                  
                            inpDialString = #GetElligibleMessages.ContactString_vch# <BR>
                            NOW() = #NOW()# <BR>
                            INPBATCHID = #GetElligibleMessages.BatchId_bi#<BR>
                            GROUPID = #GetElligibleMessages.GroupId_int#<BR />
                        </cfoutput>
                    </cfif>

                    <!--- Process Record Here --->    
                    <cftry>
                      
                       <!--- Create a single request - all the list filtering was done previously  --->
                       <cfinvoke component="session.sire.models.cfc.control-point" method="LoadQueueSMSDynamic" returnvariable="RetValLoadQueueSMSDynamic">
                            <cfinvokeargument name="inpBatchId" value="#GetElligibleMessages.BatchId_bi#">
                            <cfinvokeargument name="inpContactString" value="#GetElligibleMessages.ContactString_vch#">
                            <cfinvokeargument name="inpShortCode" value="#GetElligibleMessages.ShortCode_vch#">
                            <cfinvokeargument name="inpGroupId" value="#GetElligibleMessages.GroupId_int#">
                            <cfinvokeargument name="inpAPIRequestJSON" value="#GetBlastLog.APIRequestJSON_vch#"/>
                            <cfinvokeargument name="inpCarrier" value="#GetElligibleMessages.OperatorId_int#">
                            <cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"> <!--- Blast load queue will already block these so no need to check here. Real time API or anyhting else that skips the Blast Load Queue will want this to be 0 --->
                       </cfinvoke>

                        <cfif inpVerboseDebug GT 0>
                            <cfoutput>
	                            RetValLoadQueueSMSDynamic = 
                                <cfdump var="#RetValLoadQueueSMSDynamic#">
                            </cfoutput>
                        </cfif>

                        <cfif RetValLoadQueueSMSDynamic.RXRESULTCODE GT 0>
	                        
	                        <cfinvoke component="session.sire.models.cfc.control-point" method="UpdateBatchLogQueueStatus" returnvariable="RetValUpdateBatchLogQueueStatus">
	                            <cfinvokeargument name="inpBlastLogId" value="#GetElligibleMessages.BlastLogId_int#">
	                            <cfinvokeargument name="inpBlastLogQueueId" value="#GetElligibleMessages.Id_int#">
	                            <cfinvokeargument name="inpStatus" value="#BLAST_LOG_QUEUE_COMPLETE#">
	                            <cfinvokeargument name="inpNotes" value="">
	                        </cfinvoke>
                       
                        <cfelse>
                        
                        	<cfinvoke component="session.sire.models.cfc.control-point" method="UpdateBatchLogQueueStatus" returnvariable="RetValUpdateBatchLogQueueStatus">
	                            <cfinvokeargument name="inpBlastLogId" value="#GetElligibleMessages.BlastLogId_int#">
	                            <cfinvokeargument name="inpBlastLogQueueId" value="#GetElligibleMessages.Id_int#">
	                            <cfinvokeargument name="inpStatus" value="#BLAST_LOG_QUEUE_ERROR#">
	                            <cfinvokeargument name="inpNotes" value="#RetValLoadQueueSMSDynamic.MESSAGE# #RetValLoadQueueSMSDynamic.ERRMESSAGE#">
	                        </cfinvoke>
                           
                        </cfif>
                        
                        <cfif inpVerboseDebug GT 0>
                            <cfoutput>
	                            RetValUpdateBatchLogQueueStatus = 
                                <cfdump var="#RetValUpdateBatchLogQueueStatus#">
                            </cfoutput>
                        </cfif>
                        
                               
                        <cfcatch type="any">
                            
                            <cfset ErrorCount = ErrorCount + 1>
                            
                            <!--- Log error? --->
                            ********** ERROR on LOOP **********<BR/>    
                            <cfif inpVerboseDebug GT 0>
                                <cfoutput>
                                    <cfdump var="#cfcatch#">
                                </cfoutput>
                            </cfif>             
                            
                            <cfset LastErrorDetails = SerializeJSON(cfcatch) />
                                                                            
                        </cfcatch>
                    
                    </cftry>
                    
                </cfloop>
            <cfelse>
            
            	<!--- No more records to process - update the Blast Log to complete --->
                <cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
                    UPDATE
                        simpleobjects.batch_blast_log
                    SET
                        Status_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#BLAST_LOG_COMPLETE#"> 
                    WHERE
                        PKId_int = #GetBlastLog.PKId_int#
                </cfquery>
                
            </cfif>
        </cfif>
    </cfif>
    
    
	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

    
    
<cfcatch TYPE="any"> <!--- Main Catch --->

	<cfset ErrorCount = ErrorCount + 1>

	********** ERROR on PAGE **********<BR>	   
    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>
    
	<cfset ENA_Message = "ERROR on #GetCurrentTemplatePath()# TQCheckPoint = #TQCheckPoint#">
	<cfset InvalidRecordCount = InvalidRecordCount + 1>
                

</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpProcessName# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO simplequeue.errorlogs
	        (
	        	ErrorNumber_int,
	            Created_dt,
	            Subject_vch,
	            Message_vch,
	            TroubleShootingTips_vch,
	            CatchType_vch,
	            CatchMessage_vch,
	            CatchDetail_vch,
	            Host_vch,
	            Referer_vch,
	            UserAgent_vch,
	            Path_vch,
	            QueryString_vch
	        )
	        VALUES
	        (
	        	101,
	            NOW(),
	            'act_sire_insertcontactqueue - LastErrorDetails is Set - See Catch Detail for more info',   
	            '',   
	            '',     
	            '', 
	            '',
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
	            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
	        );
		</cfquery>	
	</cfif>
	
	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>    
</cfif>