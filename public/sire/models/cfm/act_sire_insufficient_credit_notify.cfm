<cfinclude template="cronjob-firewall.cfm"/>

<cfparam name="DBSourceEBM" default="bishop"/>

<cfparam name="inpNumberOfDialsToRead" default="100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_insufficient_credit_notify"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>


<!---
   	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset LocalPageThreadList = ''/>
<cfset LASTERRORDETAILS = '' />
<cfset count = 1/>
<cfset creditPercentage = 25/>

<cftry>

	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
		<cfif structKeyExists(APPLICATION, "#lockThreadName#")>
			<cfoutput> already run</cfoutput>
			<cfexit/>
		<cfelse>
			<cfset APPLICATION['#lockThreadName#'] = 1/>
			<cfset getLockThread = 1/>
		</cfif>
	</cflock>

	<cfquery name="GetInsufficientCreditSetting" datasource="#DBSourceEBM#">
		SELECT
			Value_txt
		FROM
			simpleobjects.sire_setting
		WHERE
			VariableName_txt = "InsufficientCreditPercentage"
		LIMIT
			1
	</cfquery>

	<cfif GetInsufficientCreditSetting.RECORDCOUNT GT 0>
		<cfset creditPercentage = GetInsufficientCreditSetting.Value_txt/>
	</cfif>

	<!--- Record(s) post --->
	<cfquery name="GetEligibleRecords" datasource="#DBSourceEBM#">
		SELECT
			uc.UserId_int,
			up.UserPlanId_bi,
			uc.FirstName_vch,
			uc.LastName_vch,
			uc.EmailAddress_vch,
			uc.MFAContactString_vch,
			pl.PlanName_vch,
			IF(icl.Notified_ti IS NULL, 0, icl.Notified_ti) AS Notified,
			IF(icl.Updated_dt IS NULL, 1, IF(icl.Updated_dt < ( CURDATE() - INTERVAL 2 DAY ), 1, 0)) AS Allowed,
			(bl.Balance_int + bl.BuyCreditBalance_int + bl.PromotionCreditBalance_int) AS TotalCredit,
			up.FirstSMSIncluded_int,
			ROUND((bl.Balance_int + bl.BuyCreditBalance_int + bl.PromotionCreditBalance_int)/(up.FirstSMSIncluded_int)*100, 2) AS CreditPercentage
		FROM
			simpleobjects.useraccount uc
		JOIN
			simplebilling.billing bl
		ON
			uc.UserId_int = bl.UserId_int
		JOIN
			simplebilling.userplans up
		ON
			uc.UserId_int = up.UserId_int
		JOIN
			simplebilling.plans pl
		ON
			up.PlanId_int = pl.PlanId_int
		LEFT JOIN
			simplequeue.sire_insufficient_credit_logs icl
		ON
			up.UserPlanId_bi = icl.UserPlanId_bi
		WHERE
			uc.Active_int = 1
		<cfif application.Env EQ false>
		AND
			uc.IsTestAccount_ti = 1
		<cfelse>
		AND
			uc.IsTestAccount_ti = 0
		</cfif>
		AND
			up.Status_int = 1
		AND
			uc.UserId_int MOD #inpMod# = #DoModValue#
		HAVING
			CreditPercentage < #creditPercentage#
		AND
			Notified < 3
		AND
			Allowed = 1
		ORDER BY
			uc.UserId_int DESC
		LIMIT
			#inpNumberOfDialsToRead#
	</cfquery>

	<cfif inpVerboseDebug GT 0>
		------------ GetEligibleRecords -----------------<br/><br/>
		<cfdump var="#GetEligibleRecords#">
	</cfif>

	<cfif GetEligibleRecords.RECORDCOUNT GT 0>
		<cfquery datasource="#DBSourceEBM#" result="InsertUpdateLog">
			INSERT INTO 
				simplequeue.sire_insufficient_credit_logs (UserId_int, UserPlanId_bi, UserBalance_vch, PlanCredit_int, Notified_ti) 
			VALUES 

			<cfloop query="GetEligibleRecords">
				<cfoutput>
					(
						#UserId_int#,
						#UserPlanId_bi#,
						#TotalCredit#,
						#FirstSMSIncluded_int#,
						1
						)#(count EQ GetEligibleRecords.RECORDCOUNT ? "" : ",")#
				</cfoutput>
				<cfset count++/>
			</cfloop>

			ON DUPLICATE KEY UPDATE
				UserBalance_vch = CONCAT_WS(",", UserBalance_vch, VALUES(UserBalance_vch)),
				Notified_ti = Notified_ti + 1;
		</cfquery>

		<cfif inpVerboseDebug GT 0>
			<br/><br/>
			------------ Insert/Update Log -----------------<br/><br/>
			<cfdump var="#InsertUpdateLog#">
		</cfif>
		
		<cfloop query="GetEligibleRecords">
			<cfset data = {}/>
			<cfset data.USERNAME = "#FirstName_vch#"/>
			<!--- <cfset data.PLANNAME = "#PlanName_vch#"/>
			<cfset data.USERCREDIT = "#TotalCredit#"/>
			<cfset data.PLANCREDIT = "#FirstSMSIncluded_int#"/> --->
			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
				<cfinvokeargument name="to" value="#EmailAddress_vch#"/>
				<cfinvokeargument name="type" value="html"/>
				<cfinvokeargument name="subject" value="Sire - your credits are running low"/>
				<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_insufficient_credit_notify.cfm"/>
				<cfinvokeargument name="data" value="#trim(serializeJSON(data))#"/>
			</cfinvoke>
		</cfloop>
		<cftry>
			<!--- Send email to support@siremobile.com --->
			<cfset lastErrorDetails = ''/>
			<cfset checkDetails = 'OK'/>
			<cfset checkResult = 1/>
			<cfset NOCName = "Sire User Account Balance Low Warning"/>
			<cfset data = {}/>
			
			<cfset checkResult = 0/>
			<cfset checkDetails = "Sire User Account Balance Low Warning: " & valueList(GetEligibleRecords.UserId_int)/>
			<!--- Send email alert and logs --->
			<cfset data.NOCName = NOCName/>
			<cfset data.Details = checkDetails/>
			<!--- Send email alert --->
			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
				<cfinvokeargument name="to" value="support@siremobile.com"/>
				<cfinvokeargument name="type" value="html"/>
				<cfinvokeargument name="subject" value="#NOCName#"/>
				<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_NOC_checks_report.cfm"/>
				<cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
			</cfinvoke>
			<cfcatch type="any">
			</cfcatch>
		</cftry>
			<!--- End support@siremobile.com --->
	</cfif>

	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
	<cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfif inpVerboseDebug GT 0>
			------------ Error -----------------<br/><br/>
			<cfdump var="#cfcatch#"/>
		</cfif>
	</cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	<br/><br/>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
	<BR />
</cfoutput>

<cftry>
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.errorlogs
			(
				ErrorNumber_int,
				Created_dt,
				Subject_vch,
				Message_vch,
				TroubleShootingTips_vch,
				CatchType_vch,
				CatchMessage_vch,
				CatchDetail_vch,
				Host_vch,
				Referer_vch,
				UserAgent_vch,
				Path_vch,
				QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'#inpDialerIPAddr# - LastErrorDetails is Set - See Catch Detail for more info',   
				'',   
				'',     
				'', 
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
				);
		</cfquery>	
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive">
		<cfset StructDelete(APPLICATION,lockThreadName)/>
	</cflock>
</cfif>
