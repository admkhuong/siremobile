<!--- <cfinclude template="cronjob-firewall.cfm"/> --->
<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="../../../../session/cfc/csc/constants.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_smsdelivery.cfm">
<cfinclude template="../../../../session/cfc/csc/inc_ext_csc.cfm">
<cfinclude template="../../../../session/cfc/csc/inc-billing-sire.cfm">

<cfparam name="DBSourceEBM" default="bishop"/>

<!--- <cfparam name="inpNumberOfDialsToRead" default="100"/> --->
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpLimit" default="100"/>
<cfparam name="inpAct" default="act_sire_notify_event_change_status"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfset lockThreadName = inpAct&'_'&inpMod&'_'&DoModValue/>


<!---
    Run multi threaded with inpMod and DoModValue  
    
    
    Sample 10 threads
    
    inpMod = 10
    
    DoModValue = 0-9
    ... so 10 CRON jobs
    
    inpMod = 10 & DoModValue = 0
    inpMod = 10 & DoModValue = 1
    inpMod = 10 & DoModValue = 2
    inpMod = 10 & DoModValue = 3
    inpMod = 10 & DoModValue = 4
    inpMod = 10 & DoModValue = 5
    inpMod = 10 & DoModValue = 6
    inpMod = 10 & DoModValue = 7
    inpMod = 10 & DoModValue = 8
    inpMod = 10 & DoModValue = 9
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset GetEventToUpdateAndNotify = ""/>
<cfset GetEventStatus = ""/>
<cfset UpdatEventStatus = ""/>
<cfset inpConfirmationFlag = ''/>
<cfset inpConfirmationFlag_name = ""/>
<cfset GetSessionIdEventRespone = ""/>
<cfset valSessionID = 0/>
<cfset listemail = ""/>
<cfset listphone = ""/>
<cfset phone = ""/>

<cftry>

    <cflock scope="SERVER" timeout="58" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
            <cfoutput> already run</cfoutput>
            <cfexit/>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>

    <cfquery name="GetEventToUpdateAndNotify" datasource="#DBSourceEBM#">
        SELECT
            PKId_bi,
            UserId_int,
            Title_vch,
            SMSNumber_vch,
            Start_dt,
            End_dt,
            ConfirmationOneDTSId_int
        FROM
            calendar.events
        WHERE
            ConfirmationFlag_int = 1
        AND 
            Active_int = 1
        LIMIT 
            #inpLimit#    
    </cfquery>
    
    <cfif getEventToUpdateAndNotify.RECORDCOUNT GT 0>
        <cfloop query="#getEventToUpdateAndNotify#">
            <cfquery name="GetEventStatus" datasource="#DBSourceEBM#">
                SELECT
                    DTSStatusType_ti
                FROM
                    simplequeue.contactqueue
                WHERE
                    DTSId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#getEventToUpdateAndNotify.ConfirmationOneDTSId_int#">
                AND 
                    DTSStatusType_ti IN ('5','6','4','100')   
            </cfquery>
            <cfif GetEventStatus.RecordCount GT 0>
                <cfswitch expression="#Trim(GetEventStatus.DTSStatusType_ti)#"> 
                    <cfcase value="5"> <!--- Sent --->
                        <cfset inpConfirmationFlag = 7/>
                        <cfset inpConfirmationFlag_name = "Reminder Sent"/>
                    </cfcase> 
                    <cfdefaultcase> 
                        <cfset inpConfirmationFlag = 5/>
                        <cfset inpConfirmationFlag_name = "Reminder Error"/>
                    </cfdefaultcase> 
                </cfswitch> 

                 <cfquery name="GetSessionIdEventRespone" datasource="#DBSourceEBM#">
                    Select
                        SessionId_bi,UserId_int
                    FROM
                        simplequeue.sessionire
                    WHERE
                        DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#ConfirmationOneDTSId_int#">                            
                </cfquery>
                <cfif GetSessionIdEventRespone.RecordCount GT 0>
                    <cfset valSessionID = GetSessionIdEventRespone.SessionId_bi>
                </cfif>
                <!--- Update status event --->
                <cfquery name="UpdatEventStatus" datasource="#DBSourceEBM#" result="UpdatEventStatusResult">
                    UPDATE
                        calendar.events
                    SET
                        ConfirmationFlag_int =<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpConfirmationFlag#">,
                        Confirmation_dt = NOW(),
                        ConfirmationSessionId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#GetSessionIdEventRespone.SessionId_bi#">,
                        Updated_dt = NOW()
                    WHERE
                        ConfirmationOneDTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#getEventToUpdateAndNotify.ConfirmationOneDTSId_int#">      
                    AND    
                        ConfirmationFlag_int = 1 
                    AND 
                        Active_int = 1
                </cfquery>
                <!--- Sent email OR SMS --->
                <!--- Send contact info to User when change status --->
                <cfif inpConfirmationFlag NEQ 1>
                    <cfquery name="GetContentEmail" datasource="#DBSourceEBM#">
                        SELECT
                            UserId_int,
                            CONCAT(FirstName_vch ,' ', LastName_vch) as USERNAME,
                            EmailAddress_vch,
                            MFAContactString_vch
                        FROM 
                            simpleobjects.useraccount
                        WHERE
                            UserId_int =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventToUpdateAndNotify.UserId_int#">                      
                    </cfquery> 
            
                    <!--- Get config notification --->
                    <cfquery name="GetListNotificationMethod" datasource="#DBSourceEBM#">
                        SELECT
                            NotificationMethod_int,
                            NotificationItem_vch
                        FROM 
                            calendar.configuration
                        WHERE
                            UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventToUpdateAndNotify.UserId_int#">                    
                    </cfquery>
                
                    <cfif GetListNotificationMethod.RecordCount GT 0>
                        <cfif GetListNotificationMethod.NotificationMethod_int EQ 0>  <!--- Email notification --->
                            <cfset listemail = GetListNotificationMethod.NotificationItem_vch>
                            <!---
                            <cfif listemail EQ "">
                                <cfset listemail = GetContentEmail.EmailAddress_vch>
                            </cfif>s
                            --->
                        <cfelse>                                                      <!--- SMS notification --->
                            <cfset listphone = GetListNotificationMethod.NotificationItem_vch>
                            <!---
                            <cfif listphone EQ "">
                                <cfset listphone = REReplaceNoCase(GetContentEmail.MFAContactString_vch, "[^\d^\*^P^X^##]", "", "ALL")>
                            </cfif>
                            --->
                        </cfif>
                    </cfif>

                    <!---  <cfif GetContentEmail.RecordCount GT 0>  --->
                    <cfset USERNAME = #GetContentEmail.USERNAME#/>
                    <cfset EVENTNAME = #getEventToUpdateAndNotify.Title_vch#/>
                    <cfset EVENTSTART = LSDateFormat(#getEventToUpdateAndNotify.Start_dt#, 'mm/dd/yyyy') & ' ' & LSTimeFormat(#getEventToUpdateAndNotify.Start_dt#, 'hh:mm tt')/>
                    <cfset EVENTEND = LSDateFormat(#getEventToUpdateAndNotify.End_dt#, 'mm/dd/yyyy') & ' ' & LSTimeFormat(#getEventToUpdateAndNotify.End_dt#, 'hh:mm tt')/>
                    <cfset CRRSTATUSNAME = #inpConfirmationFlag_name#/>
                    <cfset PHONENUMBER = #getEventToUpdateAndNotify.SMSNumber_vch#/>

                    <cfif GetListNotificationMethod.NotificationMethod_int EQ 0>
                        <cfif listemail NEQ "">

                        
                            <cfset listemail = replace(listemail,"#chr(44)#",";","ALL")>
                            <cfmail to="#listemail#" subject="Notify Calendar Event Change Status" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
                                <cfoutput>
                                    <cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">
                                    <html>
                                        <head>
                                            <meta charset="utf-8">
                                            <title>Notify Calendar Event Change Status</title>
                                        </head>
                                        <!---
                                                ConfirmationFlag States
                                                
                                                0 - No reminder sent
                                                1 - Reminder inQueue
                                                2 - Reminder Accepted
                                                3 - Reminder Declined
                                                4 - Reminder Change Request
                                                5 - Reminder Error - Bad SMS, eMail or other error
                                                6 - Dont send Reminder  
                                                7 - Reminder Sent                   
                                        --->    
                                        <body style="font-family: Arial; font-size: 12px;">
                                            <table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%">
                                                        <a href="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/logo.jpg" alt=""></a>
                                                    </td>
                                                    <td style="text-align: right; color: ##5c5c5c; font-size: 11px;">
                                                        <!--- <p>Trouble seeing this email? <a href="<cfoutput>#content.ROOTURL#</cfoutput>/public/sire/pages/contact-us">Click here</a></p>--->
                                                        <p>Add <a href="mailto:support@siremobile.com">support@siremobile.com</a> to your contacts</p>
                                                    </td>
                                                </tr>
                                                <tbody style="margin: 0; padding: 0;">
                                                    <tr>
                                                        <td colspan="2" style="margin: 0; padding: 0;">
                                                            <div style="text-align: center;">
                                                                <img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 0 30px;">
                                                            <p style="font-size: 16px;">Hi <cfoutput>#USERNAME#</cfoutput>!</p>

                                                            <p style="font-size: 16px;">The event name :<b><cfoutput>#EVENTNAME#</cfoutput>.</b></p>
                                                            <p style="font-size: 16px;">Just changed status to: <b><cfoutput>#CRRSTATUSNAME#</cfoutput>.</b></p>
                                                            <p style="font-size: 16px;">Event start: <cfoutput>#EVENTSTART#</cfoutput>.</p>
                                                            <p style="font-size: 16px;">Event end: <cfoutput>#EVENTEND#</cfoutput>.</p>
                                                            <p style="font-size: 16px;">Phone number: <cfoutput>#PHONENUMBER#</cfoutput>.</p>
                                                            <p style="font-size: 16px;">
                                                                Thanks,<br>
                                                                SIRE Calendar
                                                            </p>
                                                        </td>
                                                    </tr>   
                                                </tbody>
                                                <tfoot>
                                                    <tr style="text-align: center" bgcolor="##274e60">
                                                        <td style="font-family: Arial; font-size: 12px; padding-left: 15px; color: white" colspan="2">
                                                        <p>For support requests, please call or email us at:<br>
                                                        (888) 747-4411 | <a href="mailto:support@siremobile.com" style="text-decoration: none; color: white">support@siremobile.com</a> | Text "<a style="color: white; text-decoration: none;"><strong>support</strong></a>" to <a style="color: white; text-decoration: none;"><strong>39492</strong></a></p>
                                                        </td>
                                                    </tr>

                                                    <tr style="text-align: center" bgcolor="##274e60">
                                                        <td width="100%" style="padding-bottom: 15px" colspan="2">
                                                            <a style="padding-right: 7px" href="https://www.facebook.com/siremobile/"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/facebook.png?v=1.0" alt="fb"/></a>
                                                            <a style="padding-right: 7px" href="https://twitter.com/sire_mobile"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/twitter.png?v=1.0" alt="twt"></a>
                                                            <a style="padding-right: 7px" href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a>
                                                            <a style="padding-right: 7px" href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a>
                                                            <a href="https://plus.google.com/116619152084831668774"><img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/g+.png?v=1.0" alt="g+"></a>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </body>

                                    </html>
                                </cfoutput>
                            </cfmail>
                        </cfif>
                    <cfelse>
                        <cfif listphone NEQ "">
                            <cfquery datasource="#Session.DBSourceEBM#" name="getShortCode">
                                SELECT
                                    sc.ShortCodeId_int,
                                    sc.ShortCode_vch,
                                    scr.RequesterId_int,
                                    scr.IsDefault_ti
                                FROM 
                                    sms.shortcode sc 
                                JOIN     
                                    sms.shortcoderequest scr
                                ON 
                                    sc.ShortCodeId_int = scr.ShortCodeId_int
                                WHERE        
                                    scr.RequesterId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetEventToUpdateAndNotify.UserId_int#">
                                ORDER BY
                                    scr.ShortCodeRequestId_int ASC
                            </cfquery>

                            <cfif getShortCode.RECORDCOUNT GT 1>
                                <cfloop query="getShortCode">
                                    <cfif getShortCode.IsDefault_ti GT 0>
                                        <cfset valShortCode = getShortCode.ShortCode_vch>
                                        <cfset valShortCodeId = getShortCode.ShortCodeId_int>
                                    </cfif>
                                </cfloop>
                                <cfif valShortCode EQ ''>
                                    <cfset valShortCode = getShortCode.ShortCode_vch>
                                    <cfset valShortCodeId = getShortCode.ShortCodeId_int>
                                </cfif>
                            <cfelseif getShortCode.RecordCount GT 0>
                                <cfset valShortCode = getShortCode.ShortCode_vch>
                                <cfset valShortCodeId = getShortCode.ShortCodeId_int>
                            </cfif>

                            <cfloop list="#listphone#" index="phone" delimiters=",">
                                <!--- sent sms --->
                                <cfset Session.UserId = GetSessionIdEventRespone.UserId_int/>
                                <cfset Session.DBSourceEBM = DBSourceEBM/>
                                <cfset phone = ReReplaceNoCase(phone,"[^0-9,]","","ALL")>
                                <cfinvoke method="SendSingleMT" returnvariable="RetVarSendSingleMT">
                                    <cfinvokeargument name="inpContactString" value="#phone#"/>
                                    <cfinvokeargument name="inpShortCode" value="#valShortCode#"/>
                                    <cfinvokeargument name="inpTextToSend" value="Hi, Event name: #EVENTNAME# just change status to #CRRSTATUSNAME# by phone number #PHONENUMBER#. Thanks!."/>
                                </cfinvoke>
                                <cfdump var="#RetVarSendSingleMT#"/>
                            </cfloop>
                        </cfif>
                    </cfif>
                </cfif> <!--- dont sent email when event just inque --->

            </cfif>
        </cfloop>
    </cfif>
    
    <cfif inpVerboseDebug GT 0>
        <cfdump var="#GetEventToUpdateAndNotify#">
    </cfif>


    <cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

    <cfcatch TYPE="any">
        <cfset LastErrorDetails = SerializeJSON(cfcatch) />
        <cfif inpVerboseDebug GT 0>
            ------------ Error -----------------<br/><br/>
            <cfdump var="#cfcatch#"/>
        </cfif>
    </cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
    <br/><br/>
    End time - #inpAct# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

    Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
    <BR />
</cfoutput>

<cftry>
    <cfif LastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.errorlogs
            (
                ErrorNumber_int,
                Created_dt,
                Subject_vch,
                Message_vch,
                TroubleShootingTips_vch,
                CatchType_vch,
                CatchMessage_vch,
                CatchDetail_vch,
                Host_vch,
                Referer_vch,
                UserAgent_vch,
                Path_vch,
                QueryString_vch
                )
            VALUES
            (
                101,
                NOW(),
                '#inpAct# - LastErrorDetails is Set - See Catch Detail for more info',   
                '',   
                '',     
                '', 
                '',
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
                );
        </cfquery>  
    </cfif>

    <cfcatch></cfcatch>
</cftry>


<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="58" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>

