<cfinclude template="cronjob-firewall.cfm"/>
<cfinclude template="/public/sire/configs/paths.cfm">
<cfparam name="DBSourceEBM" default="bishop"/>

<!--- <cfparam name="inpNumberOfDialsToRead" default="100"/> --->
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpAct" default="act_sire_notify_renew_plan"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfset lockThreadName = inpAct&'_'&inpMod&'_'&DoModValue/>


<!---
   	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9
--->

<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset LocalPageThreadList = ''/>
<cfset LASTERRORDETAILS = '' />
<cfset count = 1/>
<cfset creditPercentage = 25/>

<cftry>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
		<cfif structKeyExists(APPLICATION, "#lockThreadName#")>
			<cfoutput> already run</cfoutput>
			<cfexit/>
		<cfelse>
			<cfset APPLICATION['#lockThreadName#'] = 1/>
			<cfset getLockThread = 1/>
		</cfif>
	</cflock>

	<cfquery name="getUsersToNotify" datasource="#DBSourceEBM#">
		SELECT
			up.UserPlanId_bi,
			up.UserId_int,
			ua.EmailAddress_vch,
			up.Status_int,
			up.EndDate_dt
		FROM
			simplebilling.userplans as up
		INNER JOIN
			simpleobjects.useraccount as ua
		ON
			ua.UserId_int = up.UserId_int
		WHERE
			DATE(up.EndDate_dt) = DATE_ADD(DATE(NOW()), INTERVAL 45 DAY)
		AND
			up.BillingType_ti = 2
		AND 
			up.Status_int = 1
		AND 
			up.NotiRenewPlanDate_dt IS NULL
		<cfif application.Env EQ false>
		    AND up.UserId_int IN (476,620,1404,1405,1406,1407,1408,1409,1411,1412,1413,1414,1415,1416,1422,1424,1425,1426,1427,1428,1429,1423,1430,1431)
		</cfif>
		GROUP BY
			up.UserId_int
		ORDER BY
			up.UserPlanId_bi
		DESC
	</cfquery>

	<cfif getUsersToNotify.RECORDCOUNT GT 0>
		<cfloop query="#getUsersToNotify#">
			<cfquery name="getPlanType" datasource="#DBSourceEBM#">
				SELECT
					up.PlanId_int,
					up.BillingType_ti,
					p.PlanName_vch,
					p.Amount_dec,
					p.YearlyAmount_dec
				FROM
					simplebilling.userplans as up
				INNER JOIN
					simplebilling.plans as p
				ON
					up.PlanId_int = p.PlanId_int
				AND
					p.Status_int = 1
				WHERE
					up.UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUsersToNotify.UserPlanId_bi#">
				LIMIT 1
			</cfquery>
			
			<cfset declineDataContent = {} >
			<cfset declineDataContent.USERID = getUsersToNotify.UserId_int >
			<cfset declineDataContent.ENDDATE = dateFormat(getUsersToNotify.EndDate_dt, "mmm dd yyyy") >
			<cfset declineDataContent.PLANTYPE = getPlanType.BillingType_ti EQ 2 ? "#getPlanType.PlanName_vch#" & " ($#getPlanType.YearlyAmount_dec*12#/year)" : "#getPlanType.PlanName_vch#" & " ($#getPlanType.Amount_dec#/month)">

			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
	            <cfinvokeargument name="to" value="#getUsersToNotify.EmailAddress_vch#">
	            <cfinvokeargument name="type" value="html">
	            <cfinvokeargument name="subject" value="[SIRE] Your account plan is going to be renewed in 45 days">
	            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_renew_plan.cfm">
	            <cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
	        </cfinvoke>
			
	        <cfquery name="updateNotiRenewPlanDate" datasource="#DBSourceEBM#" result="updateNotiRenewPlanDateResult">
	        	UPDATE
	        		simplebilling.userplans
	        	SET
	        		NotiRenewPlanDate_dt = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#NOW()#">
	        	WHERE
					UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#getUsersToNotify.UserPlanId_bi#">
	    	</cfquery>
		</cfloop>
	</cfif>
	
	<cfif inpVerboseDebug GT 0>
		<cfdump var="#getUsersToNotify#">
		<!--- <cfdump var="#updateNotiRenewPlanDateResult#"> --->
	</cfif>


	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
	<cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfif inpVerboseDebug GT 0>
			------------ Error -----------------<br/><br/>
			<cfdump var="#cfcatch#"/>
		</cfif>
	</cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	<br/><br/>
	End time - #inpAct# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
	<BR />
</cfoutput>

<cftry>
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.errorlogs
			(
				ErrorNumber_int,
				Created_dt,
				Subject_vch,
				Message_vch,
				TroubleShootingTips_vch,
				CatchType_vch,
				CatchMessage_vch,
				CatchDetail_vch,
				Host_vch,
				Referer_vch,
				UserAgent_vch,
				Path_vch,
				QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'#inpAct# - LastErrorDetails is Set - See Catch Detail for more info',   
				'',   
				'',     
				'', 
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
				);
		</cfquery>	
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive">
		<cfset StructDelete(APPLICATION,lockThreadName)/>
	</cflock>
</cfif>
