<cfparam name="inpVerboseDebug" default="0">

<cfinclude template="/public/sire/configs/paths.cfm">

<cfparam name="DBSourceEBM" default="bishop"/>

<cfset lastErrorDetails = ''/>
<cfset failedRecords = []/>

<cftry>

    <cfquery name="releaseMLP" datasource="#DBSourceEBM#">
        UPDATE
            simplelists.cppx_data
        SET 
            status_int = 0
        WHERE
            status_int = 2
        AND 
            DATEDIFF(CURDATE(),UserDowngradeDate_dt) >= 30
    </cfquery>

    <cfquery name="releaseShortUrl" datasource="#DBSourceEBM#">
        UPDATE
            simplelists.url_shortner c
        SET 
            Active_int = 0
        WHERE
            Active_int = 2
        AND 
            DATEDIFF(CURDATE(),UserDowngradeDate_dt) >= 30
    </cfquery>

    <cfquery name="releaseKeyword" datasource="#DBSourceEBM#">
        UPDATE
            sms.keyword
        SET 
            Active_int = 0
        WHERE
            Active_int = 2
        AND DATEDIFF(CURDATE(),UserDowngradeDate_dt) >= 30
    </cfquery>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on LOOP **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
    <cfif lastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
            INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="102">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="release keyword - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lastErrorDetails#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
        </cfquery>
    </cfif>

    <cfcatch></cfcatch>
</cftry>
