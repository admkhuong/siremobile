<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpNumberOfDialsToRead" default="100"/>
<cfparam name="inpProcessName" default="act_sire_send_email_alert"/>

<cfset ErrorCount = 0 />
<cfset LastErrorDetails = ''/>
<cfset testReceiver = ''>

<cftry>

	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cfquery datasource="#Session.DBSourceEBM#" result="StartThreadQueryResult">
		INSERT INTO
			simpleobjects.jobs_threads
			(
				StartDate_dt,
				ProcessName
			)
		VALUES
			(
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpProcessName#"/>
			)
	</cfquery>
	
	<cfset jobThreadId = StartThreadQueryResult.GENERATEDKEY/>

	<cfquery datasource="#session.DBSourceEBM#" result="emailAlertMessages">
		UPDATE
			simpleobjects.alert2users AS L
		SET
			L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		WHERE
			L.SentTo_int = #ALERT_SEND_TO_EMAIL#
		AND
			L.Status_int = 1
		AND
			(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
		AND
			(L.JobThreadId_bi = 0 OR L.JobThreadId_bi IS NULL)
		LIMIT
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#"/>
	</cfquery>

	<cfif emailAlertMessages.RECORDCOUNT GT 0>
		<cfquery datasource="#session.DBSourceEBM#" name="emailAlertMessages">
			SELECT
				L.PKId_bi,
				L.ReceiverEmailAddress_vch,
				L.Content_vch
			FROM
				simpleobjects.alert2users AS L
			WHERE
				L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		</cfquery>

		<cfset testReceiver = ("siremobile.com" EQ CGI.SERVER_NAME OR "www.siremobile.com" EQ CGI.SERVER_NAME OR "cron.siremobile.com" EQ CGI.SERVER_NAME ? "" : "sirevn1@gmail.com")/>
		<cfloop query="#emailAlertMessages#">
			<cftry>
				<cfif testReceiver EQ "" OR findNoCase("yopmail.com", emailAlertMessages.ReceiverEmailAddress_vch) GT 0>
					<cfinvoke method="SendMailByStringContent" component="public.sire.models.cfc.sendmail">
						<cfinvokeargument name="inpTo" value="#emailAlertMessages.ReceiverEmailAddress_vch#" />
						<cfinvokeargument name="inpSubject" value="Alert Message from Sire Mobile!" />
						<cfinvokeargument name="inpContent" value="#emailAlertMessages.Content_vch#" />
					</cfinvoke>
				<cfelse>
					<cfinvoke method="SendMailByStringContent" component="public.sire.models.cfc.sendmail">
						<cfinvokeargument name="inpTo" value="#testReceiver#" />
						<cfinvokeargument name="inpSubject" value="Alert Message from Sire Mobile!" />
						<cfinvokeargument name="inpContent" value="@#emailAlertMessages.ReceiverEmailAddress_vch#: #emailAlertMessages.Content_vch#" />
					</cfinvoke>
				</cfif>

				<!--- Log bug email in session/sire/logs/bugs/payment/ --->

				<cfcatch TYPE="any"> <!--- Loop Catch --->

					<cfset ErrorCount = ErrorCount + 1/>

					********** ERROR on PAGE **********<BR/>
					<cfif inpVerboseDebug GT 0>
						<cfoutput>
							<cfdump var="#cfcatch#"/>
						</cfoutput>
					</cfif>

					<cfset LastErrorDetails = SerializeJSON(cfcatch) />

					<!--- 	Write to Error log  --->
					<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
						INSERT INTO 
							simplequeue.errorlogs
							(
								ErrorNumber_int,
								Created_dt,
								Subject_vch,
								Message_vch,
								TroubleShootingTips_vch,
								CatchType_vch,
								CatchMessage_vch,
								CatchDetail_vch,
								Host_vch,
								Referer_vch,
								UserAgent_vch,
								Path_vch,
								QueryString_vch
							)
						VALUES
						(
							101,
							NOW(),
							'act_sire_send_email_alert - alert2users.PKId_bi = #emailAlertMessages.PKId_bi# - LastErrorDetails is Set - See Catch Detail for more info',
							'',
							'',
							'',
							'',
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
						)
					</cfquery>

				</cfcatch>
			</cftry>
		</cfloop>
	</cfif>

	<cfcatch TYPE="any"> <!--- Main Catch --->

		<cfset ErrorCount = ErrorCount + 1/>

		********** ERROR on PAGE **********<BR/>
		<cfif inpVerboseDebug GT 0>
			<cfoutput>
				<cfdump var="#cfcatch#"/>
			</cfoutput>
		</cfif>

		<cfset LastErrorDetails = SerializeJSON(cfcatch) />

	</cfcatch>
</cftry>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				simplequeue.errorlogs
				(
					ErrorNumber_int,
					Created_dt,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					CatchType_vch,
					CatchMessage_vch,
					CatchDetail_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'act_sire_send_email_alert - LastErrorDetails is Set - See Catch Detail for more info',
				'',
				'',
				'',
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
			)
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>