<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpNumberOfDialsToRead" default="100"/>
<cfparam name="inpProcessName" default="act_sire_send_sms_alert"/>

<cfset ErrorCount = 0/>
<cfset LastErrorDetails = ''/>
<cfset shortCodes = {}/>
<cfset testReceiver = ''>

<cftry>

	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cfquery datasource="#Session.DBSourceEBM#" result="StartThreadQueryResult">
		INSERT INTO
			simpleobjects.jobs_threads
			(
				StartDate_dt,
				ProcessName
			)
		VALUES
			(
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpProcessName#"/>
			)
	</cfquery>
	
	<cfset jobThreadId = StartThreadQueryResult.GENERATEDKEY/>

	<cfquery datasource="#session.DBSourceEBM#" result="smsAlertMessages">
		UPDATE
			simpleobjects.alert2users AS L
		SET
			L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		WHERE
			L.SentTo_int = #ALERT_SEND_TO_SMS#
		AND
			L.Status_int = 1
		AND
			(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
		AND
			(L.JobThreadId_bi = 0 OR L.JobThreadId_bi IS NULL)
		LIMIT
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#"/>
	</cfquery>

	<cfif smsAlertMessages.RECORDCOUNT GT 0>
		<cfquery datasource="#session.DBSourceEBM#" name="smsAlertMessages">
			SELECT
				L.PKId_bi,
				L.UserId_int,
				L.ReceiverPhoneStr_vch,
				L.Content_vch
			FROM
				simpleobjects.alert2users AS L
			WHERE
				L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		</cfquery>

		<!--- <cfset testReceiver = ("siremobile.com" EQ CGI.SERVER_NAME OR "wwww.siremobile.com" EQ CGI.SERVER_NAME ? "" : "900000001482017")/> --->
		<cfloop query="#smsAlertMessages#">
			<cftry>
				<cfset Session.UserID = smsAlertMessages.UserId_int/>
				<cfset shortCodeKey = "u" & Session.UserID/>

				<cfif !structKeyExists(shortCodes, shortCodeKey)>
					<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="shortCode"></cfinvoke>
					<cfset shortCodes[shortCodeKey] = shortCode.SHORTCODE/>
				</cfif>

				<!--- Send one single MT --->
				<cfinvoke component="session.cfc.csc.csc" method="SendSingleMT" returnvariable="RetVarSendSingleMT">
					<cfinvokeargument name="inpContactString" value="#smsAlertMessages.ReceiverPhoneStr_vch#"/>
					<cfinvokeargument name="inpShortCode" value="#shortCodes[shortCodeKey]#"/>
					<cfinvokeargument name="inpTextToSend" value="#smsAlertMessages.Content_vch#"/>
					<cfinvokeargument name="inpSkipLocalUserDNCCheck" value="1"/>
				</cfinvoke>

				<cfif RetVarSendSingleMT.RXRESULTCODE NEQ 1>
					<cfthrow type="#RetVarSendSingleMT.TYPE#" message="#RetVarSendSingleMT.MESSAGE#" detail="#RetVarSendSingleMT.ERRMESSAGE#" />
				</cfif>

				<cfcatch TYPE="any"> <!--- Loop Catch --->

					<cfset ErrorCount = ErrorCount + 1/>

					********** ERROR on PAGE **********<BR/>
					<cfif inpVerboseDebug GT 0>
						<cfoutput>
							<cfdump var="#cfcatch#"/>
						</cfoutput>
					</cfif>

					<cfset LastErrorDetails = SerializeJSON(cfcatch) />

					<!--- 	Write to Error log  --->
					<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
						INSERT INTO 
							simplequeue.errorlogs
							(
								ErrorNumber_int,
								Created_dt,
								Subject_vch,
								Message_vch,
								TroubleShootingTips_vch,
								CatchType_vch,
								CatchMessage_vch,
								CatchDetail_vch,
								Host_vch,
								Referer_vch,
								UserAgent_vch,
								Path_vch,
								QueryString_vch
							)
						VALUES
						(
							101,
							NOW(),
							'act_sire_send_sms_alert - alert2users.PKId_bi = #smsAlertMessages.PKId_bi# - LastErrorDetails is Set - See Catch Detail for more info',
							'',
							'',
							'',
							'',
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
						)
					</cfquery>

				</cfcatch>
			</cftry>
		</cfloop>
	</cfif>

	<cfcatch TYPE="any"> <!--- Main Catch --->

		<cfset ErrorCount = ErrorCount + 1/>

		********** ERROR on PAGE **********<BR/>
		<cfif inpVerboseDebug GT 0>
			<cfoutput>
				<cfdump var="#cfcatch#"/>
			</cfoutput>
		</cfif>

		<cfset LastErrorDetails = SerializeJSON(cfcatch) />

	</cfcatch>
</cftry>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				simplequeue.errorlogs
				(
					ErrorNumber_int,
					Created_dt,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					CatchType_vch,
					CatchMessage_vch,
					CatchDetail_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'act_sire_send_sms_alert - LastErrorDetails is Set - See Catch Detail for more info',
				'',
				'',
				'',
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
			)
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>