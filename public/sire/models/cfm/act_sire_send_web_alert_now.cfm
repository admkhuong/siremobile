<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpNumberOfDialsToRead" default="100"/>
<cfparam name="inpProcessName" default="act_sire_send_web_alert_now"/>

<cfparam name="inpAppName" default="RXMBEBMDemo"/>

<cfset ErrorCount = 0 />
<cfset LastErrorDetails = ''/>

<cftry>

	<cfinclude template="/session/sire/configs/alert_constants.cfm"/>

	<cfquery datasource="#Session.DBSourceEBM#" result="StartThreadQueryResult">
		INSERT INTO
			simpleobjects.jobs_threads
			(
				StartDate_dt,
				ProcessName
			)
		VALUES
			(
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpProcessName#"/>
			)
	</cfquery>
	
	<cfset jobThreadId = StartThreadQueryResult.GENERATEDKEY/>

	<cfquery datasource="#Session.DBSourceEBM#" result="onlineSession">
		UPDATE
			sessionstorage.cf_session_data
		SET
			alertthread = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		WHERE
			alertthread = 0
		AND
			expires > UNIX_TIMESTAMP()
		AND
			name = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAppName#"/>
		LIMIT
			<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#"/>
	</cfquery>

	<cfif onlineSession.RECORDCOUNT GT 0>
		<cfquery datasource="#Session.DBSourceEBM#" name="onlineSession">
			SELECT
				cfid,
				name,
				data
			FROM
				sessionstorage.cf_session_data
			WHERE
				alertthread = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
		</cfquery>

		<cfif onlineSession.RECORDCOUNT GT 0>
			<cfset userIds = ""/>
			<cfset userDatas = {}/>
			<cfset userCfids = {}/>
			<cfloop query="#onlineSession#">
				<cfif isJSON(onlineSession.data)>
					<cfset sData = deserializeJSON(onlineSession.data)/>
					<cfif structKeyExists(sData, "loggedIn") AND sData.loggedIn GT 0 AND structKeyExists(sData, "USERID") AND sData.USERID GT 0>
						<cfset userIds = listAppend(userIds, sData.USERID)/>
						<cfset userDatas[sData.USERID] = sData/>
						<cfset userCfids[sData.USERID] = onlineSession.cfid/>
					</cfif>
				</cfif>
			</cfloop>

			<cfif listLen(userIds) GT 0>
				<cfquery datasource="#session.DBSourceEBM#" result="webAlertMessages">
					UPDATE
						simpleobjects.alert2users AS L
					SET
						L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
					WHERE
						L.ReceiverId_int IN (#userIds#)
					AND
						L.SentTo_int = #ALERT_SEND_TO_WEB_ALERT#
					AND
						L.Status_int = 1
					AND
						(L.WebAlertHide_int = 0 OR L.WebAlertHide_int IS NULL)
					AND
						(L.Expire_dt > NOW() OR L.Expire_dt IS NULL)
					AND
						L.Trigger_bi & #ALERT_TRIGGER_SEND_NOW# > 0
					AND
						(L.JobThreadId_bi = 0 OR L.JobThreadId_bi IS NULL)
				</cfquery>

				<cfif webAlertMessages.RECORDCOUNT GT 0>
					<cfquery datasource="#session.DBSourceEBM#" name="webAlertMessages">
						SELECT
							L.PKId_bi,
							L.MessageId_bi,
							L.ReceiverId_int,
							L.Content_vch
						FROM
							simpleobjects.alert2users AS L
						WHERE
							L.JobThreadId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
					</cfquery>
					
					<cfif webAlertMessages.RECORDCOUNT GT 0>
						<cfloop query="webAlertMessages">
							<cfif !structKeyExists(userDatas[webAlertMessages.ReceiverId_int], "webAlertMessages")>
								<cfset userDatas[webAlertMessages.ReceiverId_int].webAlertMessages = {}/>
							<cfelseif structKeyExists(userDatas[webAlertMessages.ReceiverId_int].webAlertMessages, webAlertMessages.MessageId_bi)>
								<cfcontinue/>
							</cfif>
							
							<cfset userDatas[webAlertMessages.ReceiverId_int].webAlertMessages[webAlertMessages.MessageId_bi] = [webAlertMessages.PKId_bi, webAlertMessages.Content_vch, webAlertMessages.MessageId_bi]/>
							
							<cfquery datasource="#Session.DBSourceEBM#" result="onlineSession">
								UPDATE
									sessionstorage.cf_session_data
								SET
									data = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#serializeJSON(userDatas[webAlertMessages.ReceiverId_int])#"/>
								WHERE 
									cfid = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#userCfids[webAlertMessages.ReceiverId_int]#"/>
								AND 
									name = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAppName#"/>
							</cfquery>
						</cfloop>
					</cfif>
				</cfif>
			</cfif>

			<cfquery datasource="#Session.DBSourceEBM#" result="onlineSession">
				UPDATE
					sessionstorage.cf_session_data
				SET
					alertthread = -1
				WHERE
					alertthread = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#jobThreadId#"/>
				AND
					expires > UNIX_TIMESTAMP()
				AND
					name = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAppName#"/>
			</cfquery>
		</cfif>
	<cfelse>
		<cfquery datasource="#Session.DBSourceEBM#" result="onlineSession">
			UPDATE
				sessionstorage.cf_session_data
			SET
				alertthread = 0
			WHERE
				alertthread = -1
			AND
				expires > UNIX_TIMESTAMP()
			AND
				name = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpAppName#"/>
		</cfquery>
	</cfif>

	<cfcatch TYPE="any"> <!--- Main Catch --->

		<cfset ErrorCount = ErrorCount + 1/>

		********** ERROR on PAGE **********<BR/>
		<cfif inpVerboseDebug GT 0>
			<cfoutput>
				<cfdump var="#cfcatch#"/>
			</cfoutput>
		</cfif>

		<cfset LastErrorDetails = SerializeJSON(cfcatch) />

	</cfcatch>
</cftry>

<cftry>
	
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				simplequeue.errorlogs
				(
					ErrorNumber_int,
					Created_dt,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					CatchType_vch,
					CatchMessage_vch,
					CatchDetail_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'act_sire_send_web_alert_now - LastErrorDetails is Set - See Catch Detail for more info',
				'',
				'',
				'',
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
			)
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>