<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpProcessName" default="act_sire_summary_sent_and_recieved_each_day"/>
<cfparam name="inpVerboseDebug" default="0"/>

<!--- Use the constants for Blast Log and Blas tLog Queue States --->
<!--- When working on radically new code it is eaier to chage the values here than in each file used --->
<cfinclude template="../../../../session/cfc/csc/constants.cfm"/>

<!---
	
	Run multi threaded with inpMod and DoModValue  
	
	
	Sample 10 threads
	
	inpMod = 10
	
	DoModValue = 0-9
	...	so 10 CRON jobs
	
	inpMod = 10 & DoModValue = 0
	inpMod = 10 & DoModValue = 1
	inpMod = 10 & DoModValue = 2
	inpMod = 10 & DoModValue = 3
	inpMod = 10 & DoModValue = 4
	inpMod = 10 & DoModValue = 5
	inpMod = 10 & DoModValue = 6
	inpMod = 10 & DoModValue = 7
	inpMod = 10 & DoModValue = 8
	inpMod = 10 & DoModValue = 9



	
--->


<cfset lockThreadName = inpProcessName & '_' & inpDialerIPAddr & '_' & inpMod & '_' & DoModValue/>
<cfset getLockThread = 0>

<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
	<cfif structKeyExists(APPLICATION, "#lockThreadName#")>
		<cfoutput> already run</cfoutput>
		<cfexit>
	<cfelse>
		<cfset APPLICATION['#lockThreadName#'] = 1>
		<cfset getLockThread = 1>         
	</cfif>
</cflock>


<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />


<cfset LastErrorDetails = ''/>
<cfset MaxDateSummary="">
<cfset MinDateSummary="">
<cfset stopDate=  CreateDate( "2016", "1","1")>
<cftry>	

	<cfset SummaryDate= DateAdd("d",-1,NOW())>	 <!--- 1 days b4 current day--->
	<cfset SummaryDateDateStart= CreateDate( dateformat(SummaryDate,"yyyy"), dateformat(SummaryDate,"mm"), dateformat(SummaryDate,"dd"))>
	<cfset MaxDateSummary=SummaryDateDateStart>
	<cfset MinDateSummary=SummaryDateDateStart>

	<cfquery name="GetSummaryByDay" datasource="#Session.DBSourceEBM#">	
		SELECT Max(SummaryDate_dt) as MaxDateSummary, Min(SummaryDate_dt) as MinDateSummary
		FROM
			simplexresults.summary_total_send_received_by_date				
	</cfquery>
	<cfif GetSummaryByDay.MaxDateSummary NEQ "">
		<cfset MaxDateSummary=GetSummaryByDay.MaxDateSummary>
		<cfset MinDateSummary=GetSummaryByDay.MinDateSummary>		
	</cfif>			

	<cfquery name="GetLastDateSummaryFromSireSetting" datasource="#Session.DBSourceEBM#">	
		SELECT Value_dt
		FROM
			simpleobjects.sire_setting 
		WHERE
			VariableName_txt='act_sire_summary_sent_and_recieved_each_day'
		AND
			Value_dt is not null
	</cfquery>
	<cfif GetLastDateSummaryFromSireSetting.recordcount GT 0>
		<!---Check from maxdate to today, if have no data then insert  --->
		<cfif DateDiff( "d", MaxDateSummary, SummaryDateDateStart ) GT 0>		
			<cfset SummaryDate= DateAdd("d",1,MaxDateSummary)>	 <!--- 1 days after max day--->
			<cfset SummaryDateDateStart= CreateDate( dateformat(SummaryDate,"yyyy"), dateformat(SummaryDate,"mm"), dateformat(SummaryDate,"dd"))>
			<!--- --->	
			********** Summary for date: **********<BR/>
			<cfif inpVerboseDebug GT 0>
				<cfoutput>
					<cfdump var="#SummaryDateDateStart#">
				</cfoutput>
			</cfif>
			
			<cfquery name="MakeSureHaventInsertDataForSummaryDate" datasource="#Session.DBSourceEBM#">
				SELECT SummaryDate_dt
				FROM
					simplexresults.summary_total_send_received_by_date
				WHERE
					SummaryDate_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">	 

			</cfquery>
			<cfif MakeSureHaventInsertDataForSummaryDate.recordcount EQ 0 AND DateDiff( "d", stopDate, SummaryDateDateStart ) GT 0>
				<cfquery name="QuerySummaryData" datasource="#Session.DBSourceEBM#" result="UpdateResults">	
					INSERT INTO simplexresults.summary_total_send_received_by_date (
						PKid_int,
						UserId_int,
						TotalSent_int,
						TotalReceived_int,
						SummaryDate_dt,
						Created_dt
					)		
					(
						SELECT
							NULL,   
							ire.UserId_int as UserId_int,   
							SUM(IF(ire.IREType_int = 1, 1, 0)) AS TotalSent_int,
							SUM(IF(ire.IREType_int = 2, 1, 0)) AS TotalReceived_int,				
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">,				
							NOW()
						FROM
							simplexresults.ireresults as ire
						LEFT JOIN
							simpleobjects.useraccount as ua
						ON
							ire.UserId_int = ua.UserId_int
						WHERE
							LENGTH(ire.ContactString_vch) < 14
						AND
							ua.IsTestAccount_ti = 0
						AND
							ire.Created_dt
							BETWEEN 						
								#SummaryDateDateStart#
							AND 
								DATE_ADD(#SummaryDateDateStart#, INTERVAL 86399 SECOND)
						GROUP BY
							ire.UserId_int
						HAVING
							(
								TotalSent_int > 0
								OR
								TotalReceived_int > 0
							)
					)
					
				</cfquery>
				<!--- insert 1 fake line data to system understand this date alr process--->
				<cfif UpdateResults.recordcount EQ 0>
					<cfquery name="QuerySummaryData" datasource="#Session.DBSourceEBM#" result="UpdateResults">	
						INSERT INTO	simplexresults.summary_total_send_received_by_date (
							PKid_int,
							UserId_int,
							TotalSent_int,
							TotalReceived_int,
							SummaryDate_dt,
							Created_dt
						)		
						VALUES(
							NULL,
							0,
							0,
							0,							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">,	
							NOW()
						)						
							
					</cfquery>
				</cfif>
				********** Result Insert: **********<BR/>
				<cfif inpVerboseDebug GT 0>
					<cfoutput>
						<cfdump var="#UpdateResults#">
					</cfoutput>
				</cfif>
			</cfif>
		<cfelse><!--- continous run for 1 days b4 min date--->		
			<cfset SummaryDate= DateAdd("d",-1,GetLastDateSummaryFromSireSetting.Value_dt)>	 <!--- 1 days b4 current day--->
			<cfset SummaryDateDateStart= CreateDate( dateformat(SummaryDate,"yyyy"), dateformat(SummaryDate,"mm"), dateformat(SummaryDate,"dd"))>
			<!--- --->	
			********** Summary for date: **********<BR/>
			<cfif inpVerboseDebug GT 0>
				<cfoutput>
					<cfdump var="#SummaryDateDateStart#">
				</cfoutput>
			</cfif>
			
			<cfquery name="MakeSureHaventInsertDataForSummaryDate" datasource="#Session.DBSourceEBM#">
				SELECT SummaryDate_dt
				FROM
					simplexresults.summary_total_send_received_by_date
				WHERE
					SummaryDate_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">	 

			</cfquery>
			<cfif MakeSureHaventInsertDataForSummaryDate.recordcount EQ 0 AND DateDiff( "d", stopDate, SummaryDateDateStart ) GT 0>
				<cfquery name="QuerySummaryData" datasource="#Session.DBSourceEBM#" result="UpdateResults">	
					INSERT INTO simplexresults.summary_total_send_received_by_date (
						PKid_int,
						UserId_int,
						TotalSent_int,
						TotalReceived_int,
						SummaryDate_dt,
						Created_dt
					)		
					(
						SELECT
							NULL,   
							ire.UserId_int as UserId_int,   
							SUM(IF(ire.IREType_int = 1, 1, 0)) AS TotalSent_int,
							SUM(IF(ire.IREType_int = 2, 1, 0)) AS TotalReceived_int,				
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">,				
							NOW()
						FROM
							simplexresults.ireresults as ire
						LEFT JOIN
							simpleobjects.useraccount as ua
						ON
							ire.UserId_int = ua.UserId_int
						WHERE
							LENGTH(ire.ContactString_vch) < 14
						AND
							ua.IsTestAccount_ti = 0
						AND
							ire.Created_dt
							BETWEEN 						
								#SummaryDateDateStart#
							AND 
								DATE_ADD(#SummaryDateDateStart#, INTERVAL 86399 SECOND)
						GROUP BY
							ire.UserId_int
						HAVING
							(
								TotalSent_int > 0
								OR
								TotalReceived_int > 0
							)
					)
					
				</cfquery>
				<!--- insert 1 fake line data to system understand this date alr process--->
				<cfif UpdateResults.recordcount EQ 0>
					<cfquery name="QuerySummaryData" datasource="#Session.DBSourceEBM#" result="UpdateResults">	
						INSERT INTO	simplexresults.summary_total_send_received_by_date (
							PKid_int,
							UserId_int,
							TotalSent_int,
							TotalReceived_int,
							SummaryDate_dt,
							Created_dt
						)		
						VALUES(
							NULL,
							0,
							0,
							0,							
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">,	
							NOW()
						)						
							
					</cfquery>
				</cfif>
				********** Result Insert: **********<BR/>
				<cfif inpVerboseDebug GT 0>
					<cfoutput>
						<cfdump var="#UpdateResults#">
					</cfoutput>
				</cfif>
				
				<cfquery name="UpdateSetting" datasource="#Session.DBSourceEBM#" >	
					UPDATE 
						simpleobjects.sire_setting 
					SET
						Value_dt= <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">	
					WHERE
						VariableName_txt='act_sire_summary_sent_and_recieved_each_day'	
				</cfquery>
			</cfif>
		</cfif>

	<cfelse> <!--- if havent setting, do setting then go out first--->
		<cfquery name="UpdateSetting" datasource="#Session.DBSourceEBM#" >	
			UPDATE 
				simpleobjects.sire_setting 
			SET
				Value_dt= <CFQUERYPARAM CFSQLTYPE="CF_SQL_TIMESTAMP" VALUE="#SummaryDateDateStart#">	
			WHERE
				VariableName_txt='act_sire_summary_sent_and_recieved_each_day'	
		</cfquery>
	</cfif>
	
	
	
	

	<cfcatch TYPE="any"> <!--- Main Catch --->		

		********** ERROR on PAGE **********<BR/>
		<cfif inpVerboseDebug GT 0>
			<cfoutput>
				<cfdump var="#cfcatch#"/>
			</cfoutput>
		</cfif>
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />

	</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	End time - #inpProcessName# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
</cfoutput>

<cftry>
	
	<cfif LastErrorDetails NEQ '' and 1 eq 2>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO 
				simplequeue.errorlogs
				(
					ErrorNumber_int,
					Created_dt,
					Subject_vch,
					Message_vch,
					TroubleShootingTips_vch,
					CatchType_vch,
					CatchMessage_vch,
					CatchDetail_vch,
					Host_vch,
					Referer_vch,
					UserAgent_vch,
					Path_vch,
					QueryString_vch
				)
			VALUES
				(
					101,
					NOW(),
					'act_sire_summary_sent_and_recieved_each_day - LastErrorDetails is Set - See Catch Detail for more info',   
					'',
					'',
					'',
					'',
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">           
				)
		</cfquery>	
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive">
		<cfset StructDelete(APPLICATION,lockThreadName)/>
	</cflock>    
</cfif>