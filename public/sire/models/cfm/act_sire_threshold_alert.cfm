<cfinclude template="cronjob-firewall.cfm"/>
<cfinclude template="/public/sire/configs/paths.cfm">
<cfparam name="DBSourceEBM" default="bishop"/>

<!--- <cfparam name="inpNumberOfDialsToRead" default="100"/> --->
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpAct" default="act_sire_threshold_alert"/>
<cfparam name="inpVerboseDebug" default="0"/>

<cfset lockThreadName = inpAct&'_'&inpMod&'_'&DoModValue/>



<cfset TotalProcTimeStart = GetTickCount() />  
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfset LocalPageThreadList = ''/>
<cfset LASTERRORDETAILS = '' />
<cfset count = 1/>
<cfset creditPercentage = 25/>

<cftry>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
		<cfif structKeyExists(APPLICATION, "#lockThreadName#")>		
			<cfoutput> already run</cfoutput>			
			<cfexit/>
		<cfelse>
			<cfset APPLICATION['#lockThreadName#'] = 1/>
			<cfset getLockThread = 1/>
		</cfif>
	</cflock>
	
	<cfquery name="getUsersInfo" datasource="#DBSourceEBM#">
		SELECT
			ThresholdAlerts_int,
			UserId_int,
			IntegrateUserID_int,
			IntegrateHigherUserID,
			IntegrateCompanyID_int,
			EmailAddress_vch,
			ThresholdAlerts_int,
			FirstName_vch,
			LastName_vch,
			HigherUserID_int
		FROM
			simpleobjects.useraccount 
		WHERE
			ThresholdAlerts_int > 0
		AND
			UserLevel_int > 1				
		AND
			UserId_int NOT IN (
				SELECT
					UserId_int
				FROM
					simplelists.threshold_alert_daily_checked 
				WHERE
					datediff(Created_dt, NOW()) = 0
			)
			
		ORDER BY
			ThresholdAlerts_int		
		LIMIT 1
	</cfquery>
	<cfif inpVerboseDebug GT 0>
		<cfdump var="#getUsersInfo#">		
	</cfif>
	
	<cfif getUsersInfo.RECORDCOUNT GT 0>			
		<cfloop query="getUsersInfo">
			<cfset UserID=UserId_int>
			<cfset Threshold=ThresholdAlerts_int>
			<!--- get user plan--->
			<cfquery name="getUserPlan" datasource="#DBSourceEBM#">
				SELECT 
					u.UserPlanId_bi, 
					u.Status_int,
					u.PlanId_int,
					u.StartDate_dt,
					u.EndDate_dt             
				FROM 
					simplebilling.userplans u            
				WHERE
					u.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
				AND 
					u.Status_int = 1
				ORDER BY 
					UserPlanId_bi DESC
				LIMIT 
					1
			</cfquery>
			<cfset PlanStartDate= getUserPlan.StartDate_dt>
			<cfset PlanEndDate= getUserPlan.EndDate_dt>
			<!---check alert sent --->
			<cfset alert_sent=0>
			<cfquery name="CheckAlertSent" datasource="#DBSourceEBM#">			
				SELECT
					UserId_int
				FROM
					simplelists.threshold_alert_logs
				WHERE
					Created_dt < NOW()
				AND
					UserId_int=<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#" >	
				AND
					Created_dt 	
					BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#PlanStartDate#">
                    AND 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#PlanEndDate#">				
			</cfquery>
			<cfif CheckAlertSent.RECORDCOUNT GT 0>
				<cfset alert_sent=1>
			</cfif>
			<cfif alert_sent EQ 0 AND Dateformat(PlanStartDate,"mm") EQ Dateformat(NOW(),"mm") and Dateformat(PlanStartDate,"yyyy") EQ Dateformat(NOW(),"yyyy")>
				<cfif inpVerboseDebug GT 0>
					<cfoutput>From Date: #Dateformat(PlanStartDate,"dd-mmm-yyyy")#	To   #Dateformat(NOW(),"dd-mmm-yyyy")#<br></cfoutput>
				</cfif>
				<cfset startDate = CreateDate(Dateformat(PlanStartDate,"yyyy"), Dateformat(PlanStartDate,"mm"), Dateformat(PlanStartDate,"dd")) />				
				<!--- get total sent--->
				<cfquery name="getTotalSent" datasource="#DBSourceEBM#">
                    SELECT                        
                        SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
                        SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
                    FROM
                        simplexresults.ireresults as ire USE INDEX (IDX_Contact_Created)
                    LEFT JOIN
                        simpleobjects.useraccount as ua
                    ON
                        ire.UserId_int = ua.UserId_int					
                    WHERE
                        LENGTH(ire.ContactString_vch) < 14
					AND 
						ua.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#" >	
                    AND
                        ua.IsTestAccount_ti = 0                    
                    AND
                        ire.Created_dt
                    BETWEEN 
                        <CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#startDate#">
                    AND 
                        DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#NOW()#">,INTERVAL 1 DAY)
                    GROUP BY
                        ire.UserId_int                    
                </cfquery>  
				<cfif inpVerboseDebug GT 0>
					<cfdump var="#getTotalSent#">		
				</cfif>
				<cfset _totalMesg=0>
				<cfif getTotalSent.RECORDCOUNT GT 0>
					<cfset _totalMesg=LSParseNumber(getTotalSent.Sent) + LSParseNumber(getTotalSent.Received)>
				</cfif>
				
				<cfif Threshold LT _totalMesg> <!---if sent over threshold --->
					<!--- get higher user info to sendmail--->
					<cfquery name="getHigherUserInfo" datasource="#DBSourceEBM#">
						SELECT					
							UserId_int,
							FirstName_vch,
							LastName_vch,
							EmailAddress_vch			
						FROM
							simpleobjects.useraccount 
						WHERE
							UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#HigherUserID_int#">						
					</cfquery>
					<cfif inpVerboseDebug GT 0>
						<cfdump var="#getHigherUserInfo#">		
					</cfif>
					<cfif getHigherUserInfo.RECORDCOUNT GT 0>
						<!--- higher acc info--->
						<cfset mailto= getHigherUserInfo.EmailAddress_vch>
						<cfset declineDataContent = {} >
						<cfset declineDataContent.mailtoID =getHigherUserInfo.UserId_int >
						<!--- sub acc info--->
						<cfset declineDataContent.Name =getUsersInfo.FirstName_vch & " " & getUsersInfo.LastName_vch>			
						<cfset declineDataContent.IntegrateID = IntegrateUserID_int>			
						<cfset declineDataContent.SireID = UserID>			
						<cfset declineDataContent.Email = EmailAddress_vch>			
						<cfset declineDataContent.Threshold = Threshold>			
						<cfset declineDataContent.Used = getTotalSent.Sent>	
								
						<cfif mailto NEQ "">
							<!--- update mail sent log--->
							<cfquery name = "UpdateThresholdAlert" datasource = "#DBSourceEBM#">
								insert into simplelists.threshold_alert_logs(
									UserId_int,
									PlanStartDate_dt,
									PlanEndDate_dt
								)
								values(
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#PlanStartDate#">,
									<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#PlanEndDate#">
								)
							</cfquery>							
							<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								<cfinvokeargument name="to" value="#mailto#">
								<cfinvokeargument name="type" value="html">
								<cfinvokeargument name="subject" value="[SIRE] Your sub account using credit over threshold">
								<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_threshold_alert.cfm">
								<cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
							</cfinvoke>
							<cfif inpVerboseDebug GT 0>
								<cfoutput>Threshold mail sent to #mailto# <br>	</cfoutput>
							</cfif>
						</cfif>						
					</cfif>	
				</cfif>	
			</cfif>
			<!--- add logs have alr checked for daily --->
			<cfquery name = "UpdateThresholdAlert" datasource = "#DBSourceEBM#">
				insert into simplelists.threshold_alert_daily_checked (
					UserId_int
				)
				values(
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserID#">
				)
			</cfquery>
		</cfloop>
	</cfif>
	
	


	<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
	<cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

	<cfcatch TYPE="any">
		<cfset LastErrorDetails = SerializeJSON(cfcatch) />
		<cfif inpVerboseDebug GT 0>
			------------ Error -----------------<br/><br/>
			<cfdump var="#cfcatch#"/>
		</cfif>
	</cfcatch> 

</cftry>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  

<cfoutput>
	<br/><br/>
	End time - #inpAct# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />

	Total time process: #dateDiff("s", ServerStartTime, ServerEndTime)#
	<BR />
</cfoutput>

<cftry>
	<cfif LastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO simplequeue.errorlogs
			(
				ErrorNumber_int,
				Created_dt,
				Subject_vch,
				Message_vch,
				TroubleShootingTips_vch,
				CatchType_vch,
				CatchMessage_vch,
				CatchDetail_vch,
				Host_vch,
				Referer_vch,
				UserAgent_vch,
				Path_vch,
				QueryString_vch
				)
			VALUES
			(
				101,
				NOW(),
				'#inpAct# - LastErrorDetails is Set - See Catch Detail for more info',   
				'act_sire_threshold_alert: error',   
				'',     
				'', 
				'',
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#"/>,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#"/>           
				);
		</cfquery>	
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive">
		<cfset StructDelete(APPLICATION,lockThreadName)/>
	</cflock>
</cfif>
