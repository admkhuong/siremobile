<cfparam name="inpDTSId" default="0">
<cfparam name="inpDTSStatus" default="6">
<cfparam name="inpNote" default="">
<cfparam name="inpDialerIPAddr" default="act_sire_insertcontactqueue">
<cfparam name="inpBlastLogId" default="0">

<!--- Update Queue to finished processing (DTSStatusType_ti=5) --->
<cfset UpdateStatusOK = 0>

<!--- Squash deadlocking issues? --->
<cfloop from="1" to="3" step="1" index="DeadlockIndex">
	<cftry>
	
		<!--- Update Dial Queue Table Status to Queued --->
		<cfset UpdateStatusOK = 1>

		<cfif inpDTSStatus EQ 5>
			<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simplequeue.batch_load_queue
				WHERE
					Id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSId#"/>
			</cfquery>

			<cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.batch_blast_log
				SET
					ActualLoaded_int = ActualLoaded_int + 1
				WHERE
					PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpBlastLogId#"/>
			</cfquery>
		<cfelse>
			<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
				UPDATE 
					simplequeue.batch_load_queue
				SET
					Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSStatus#"/>,
					Note_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#inpNote#"/>
				WHERE
					Id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSId#"/>
			</cfquery>
		</cfif>

		<!--- <cfif inpDTSStatus EQ 6>
			<cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.batch_blast_log
				SET
					Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSStatus#"/>,
					Desc_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="Insert Failed"/>
				WHERE
					PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpBlastLogId#"/>
			</cfquery>
		</cfif> --->
		
		<cfbreak>
		
		<cfcatch type="any">
		
			<cfif findnocase("Deadlock", cfcatch.detail) GT 0 > 
			
				<cfscript> 
					thread = CreateObject("java","java.lang.Thread"); 
					thread.sleep(3000); // CF will now sleep for 5 seconds 
				</cfscript> 
			
			<cfelse>
				
				<!--- other errors - break from loop--->
				<cfbreak>	                        	
			
			</cfif>
		
		</cfcatch>
	
	</cftry>

</cfloop>

<!--- One more try - non-squashed --->
<cfif UpdateStatusOK EQ 0>

	<cfif inpDTSStatus EQ 5>
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			DELETE FROM
				simplequeue.batch_load_queue
			WHERE
				Id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSId#"/>
		</cfquery>

		<cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
			UPDATE
				simpleobjects.batch_blast_log
			SET
				ActualLoaded_int = ActualLoaded_int + 1
			WHERE
				PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpBlastLogId#"/>
		</cfquery>
	<cfelse>
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplequeue.batch_load_queue
			SET
					Status_ti = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSStatus#"/>,
					Note_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#inpNote#"/>
			WHERE
				Id_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSId#"/>
		</cfquery>
	</cfif>

	<!--- <cfif inpDTSStatus EQ 6>
		<cfquery name="UpdateBlastLog" datasource="#Session.DBSourceEBM#">
			UPDATE
				simpleobjects.batch_blast_log
			SET
				Status_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpDTSStatus#"/>,
				Desc_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="Insert Failed"/>
			WHERE
				PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpBlastLogId#"/>
		</cfquery>
	</cfif> --->
</cfif>