<cfparam name="inpTimePeriod" default="30">
<cfparam name="inpVerboseDebug" default="0">

<cfinclude template="/public/sire/configs/paths.cfm">

<cfset lastErrorDetails = ''/>
<cfset failedRecords = []/>

<cftry>
    <cfquery name="GetFailedResults" datasource="#Session.DBSourceEBM#">
        SELECT
        	MasterRXCallDetailId_int,
            DTSID_int,
            BatchId_bi,
            ContactString_vch,
            XMLResultStr_vch,
            Created_dt,
            SMSCSC_vch
    	FROM
        	simplexresults.contactresults
        WHERE
            XMLResultStr_vch LIKE "%Connection Timeout%"
        AND
            (TIME_TO_SEC(TIMEDIFF(NOW(), created_dt))/60) < <cfqueryparam cfsqltype="cf_sql_integer" value="#inpTimePeriod#"/>
        ORDER BY
        	MasterRXCallDetailId_int DESC
    </cfquery>

    <cfif GetFailedResults.RECORDCOUNT GT 0>
        <cfloop query="GetFailedResults">
            <cfset temp = {} />
            <cfset temp.MasterRXCallDetailId = MasterRXCallDetailId_int/>
            <cfset temp.DTSID = DTSID_int/>
            <cfset temp.BatchId = BatchId_bi/>
            <cfset temp.ContactString = ContactString_vch/>
            <cfset temp.XMLResultStr = XMLResultStr_vch/>
            <cfset temp.SMSCSC = SMSCSC_vch/>
            <cfset temp.Created = Created_dt/>

            <cfset arrayAppend(failedRecords, temp)/>
        </cfloop>

        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                <cfinvokeargument name="to" value="#SupportEMailUserName#">
                <cfinvokeargument name="type" value="html">
                <cfinvokeargument name="subject" value="Service connection timeout">
                <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_connection_timeout_report.cfm">
                <cfinvokeargument name="data" value="#TRIM(serializeJSON(failedRecords))#">
        </cfinvoke>

        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                Total records: #GetFailedResults.RECORDCOUNT#
            </cfoutput>
            <BR>********** Failed Records **********<BR>
            <cfdump var="#failedRecords#">
        </cfif>
    </cfif>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on LOOP **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="101">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="check for connection timeout - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lastErrorDetails#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>
