<cfset LocalHost = CreateObject( "java", "java.net.InetAddress" ).getLocalHost() />

<cfset Mac = CreateObject( "java", "java.net.NetworkInterface" ).getByInetAddress( LocalHost ) />
<cfif !isNull(Mac)>
	<cfset Mac = Mac.getHardWareAddress() />
	<cfset MacAddress = '' />
	<cfloop from="1" to="#ArrayLen( Mac )#" index="Pair">
	    <!--- Convert it to Hex, and only use the right two AFTER the conversion--->
	    <cfset NewPair = Right( FormatBaseN( Mac[ Pair ], 16 ), 2 ) />
	
	    <!--- If it's only one letter/string, pad it --->
	    <cfset NewPair = Len( NewPair ) EQ 1 ? '0' & NewPair : NewPair />
	
	    <!--- Append NewPair --->    
	    <cfset MacAddress &= UCase( NewPair ) />
	
	    <!--- Add the dash --->
	    <cfif ArrayLen( Mac ) NEQ Pair>
	        <cfset MacAddress &= '-' />
	    </cfif>
	</cfloop>
	<cfparam name="_showAddress" default="" />
	<cfif _showAddress EQ "bishop">
		<cfdump var="#MacAddress#" />
		<cfabort />
	<cfelse>
		<cfif MacAddress NEQ "06-CF-B0-37-DD-68">
			Access denied<cfabort />
		</cfif>
	</cfif>
</cfif>

<cfif createObject("java", "java.net.InetAddress").getLocalHost().getHostAddress() NEQ CGI.REMOTE_ADDR 
	AND CGI.REMOTE_ADDR NEQ '127.0.0.1'>
Access denied<cfabort></cfif>