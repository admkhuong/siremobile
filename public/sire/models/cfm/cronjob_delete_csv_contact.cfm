<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="cronjob-firewall.cfm">

<cfset filePath = "" />
<cfparam name="DBSourceEBM" default="bishop"/>
<cfset uploadTable = "simplexuploadstage.uploads" />
<cfparam name="inpVerboseDebug" default="0"/>

<cfinclude template="/public/sire/configs/bulk_insert_constants.cfm"/>

<cfquery name="getFileRecords" datasource="#DBSourceEBM#">
	SELECT 
		PKId_bi AS ID,
		UserId_int AS UserID,
		SourceFileName_vch AS FileName,
		Created_dt
	FROM 
		#uploadTable#
	WHERE 
		DATE_ADD(Created_dt, INTERVAL 30 DAY) < NOW()
	AND 
		RemoveStageTable_ti = 0
	AND
		Status_ti IN (#UPLOADS_SUCCESS#, #UPLOADS_ERROR_UNEXPECTED#, #UPLOADS_ERROR_FAILED#, #UPLOADS_DELETED#)
</cfquery>

<cfif inpVerboseDebug GT 0>
	<cfdump var="#getFileRecords#"/>
</cfif>

<cfif getFileRecords.RECORDCOUNT GT 0 >
	<cfloop query="#getFileRecords#">

		<cfset contactStageTable = "simplexuploadstage.contactlist_stage_"&"#ID#">
		<cfset fileName = filePath&"#FileName#">

		<cfif int(ID) GTE 48>
			<cfquery name="nonProcessRecords" datasource="#DBSourceEBM#">
				SELECT
					COUNT(*) AS TOTAL
				FROM
					#contactStageTable#			
				WHERE
					SendToQueue_ti = 0
				
			</cfquery>

			<cfif inpVerboseDebug GT 0>
				<cfoutput>
					Total non process records: #nonProcessRecords.TOTAL#
				</cfoutput>
			</cfif>
		</cfif>

		<cfif int(ID) LT 48 || nonProcessRecords.TOTAL LT 1>
			<cftransaction> 
				<cftry> 
					<!--- Execute drop contact stage table  --->
					<cfquery result="rsDeleteTable" datasource="#DBSourceEBM#">
						DROP TABLE IF EXISTS #contactStageTable#;
					</cfquery>

					<cfif inpVerboseDebug GT 0>
						<cfdump var="#rsDeleteTable#"/>
					</cfif>

					<!--- Mark cropjob done for this record  --->
					<cfquery result="rsUpdateTable" datasource="#DBSourceEBM#">
						UPDATE #uploadTable# SET RemoveStageTable_ti = 1 WHERE PKID_bi = #ID# LIMIT 1
					</cfquery>

					<!--- Commit the query transaction  --->
				   	<cftransaction action="commit" /> 
					<cfcatch type="any"> 
						<cftransaction action="rollback" /> 
					</cfcatch> 
				</cftry> 
			</cftransaction>
		</cfif>
	</cfloop>
</cfif>

