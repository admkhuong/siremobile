<cfparam name="form.tk" default="">

<cfparam name="tk" default="#form.tk#">
<cfset tk = ToString(ToBinary(tk))>
<cfset tk = ReMatch('\d{4}-\d{2}-\d{2}-\d+',tk)>

<cfif arrayLen(tk) GT 0>
	<cfset tk = tk[1]>
	<cfset form.userId = ListLast(tk, '-')>
</cfif>

<cfset dataout = {
	RXRESULTCODE = 1,
	MESSAGE = ''
}>

<cfset checkOwed = 0>


<cfparam name="form.userId" default="">

<cfif isNumeric(form.userId)>

	<cfinvoke component="public.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan">
		<cfinvokeargument name="inpUserId" value="#UserId#">
	</cfinvoke>

	<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
		SELECT UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch 
		FROM simpleobjects.useraccount
		WHERE UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.userId#">
			AND Active_int = 1
		LIMIT 1
	</cfquery>
	<cfif usersQuery.RecordCount GT 0>
		<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
			SELECT * FROM simplebilling.userplans
			WHERE Status_int = 1
				AND UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.userId#">
				AND DATE(StartDate_dt) <= CURDATE()
			ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
			LIMIT 1
		</cfquery>
		<cfif userPlansQuery.RecordCount GT 0>
			<!--- CHECK PAY FOR PLAN (checkOwed = 0) OR FOR OWNED KEYWORD (checkOwed = 1) --->
			
			<cfif userPlansQuery.BuyKeywordNumberExpired_int GT 0> 
				<cfset checkOwed = 1>
			</cfif>

			<cfset expricedTime = DateDiff('s', CreateDate(Year(userPlansQuery.EndDate_dt), Month(userPlansQuery.EndDate_dt), Day(userPlansQuery.EndDate_dt)), Now())>
			<cfif (expricedTime GE 0 AND expricedTime LT 2592000) OR checkOwed EQ 1>
				<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
					SELECT * FROM simplebilling.plans
					WHERE Status_int = 1
						AND PlanId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PlanId_int#">
					LIMIT 1
				</cfquery>
				<cfif plansQuery.RecordCount GT 0>
					<cfset form.PLANNAME = plansQuery.PlanName_vch/>
					<cfif checkOwed EQ 1>
							<cfif RetUserPlan.PLANEXPIRED EQ 1> <!--- IF WITHIN 30 day, pay for buykeyword only --->
								<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec>
							<cfelse>	
								<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumberExpired_int * plansQuery.PriceKeywordAfter_dec>
							</cfif>	
						<cfelse>
							<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec>
					</cfif>	
					
					<!--- zzz --->
					<cfinclude template="/session/sire/configs/credits.cfm">
					
					<cfset newEndDate_dt = DateAdd("m",1,userPlansQuery.EndDate_dt)>
					
					<cfset txtExtendPlan = 'Extend plan "#plansQuery.PlanName_vch#" to ' & DateFormat(newEndDate_dt, 'mm/dd/yyyy')>
					
					<cfparam name="form.currentCard" default="">
					<cfif form.currentCard EQ 1>
						<!---<cfhttp url="#payment_get_customer_payment##form.userId#" method="GET" result="paymentResponse" >
							<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
						</cfhttp>
						<cfif paymentResponse.status_code EQ 200>
							<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
								<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
								<cfif paymentResponseContent.responseCode EQ 1 >
									
									<cfset paymentData = {
										amount = totalAmount,
										transactionDuplicateCheckIndicator = 1,
										extendedInformation = {
										    notes = txtExtendPlan
										},
									    developerApplication = {
									      developerId: worldpayApplication.developerId,
									      version: worldpayApplication.version
									    },
										paymentVaultToken = {  
										   customerId = paymentResponseContent.vaultCustomer.paymentMethods[1].customerId,
										   paymentMethodId = paymentResponseContent.vaultCustomer.paymentMethods[1].paymentId,
										   paymentType = paymentResponseContent.vaultCustomer.paymentMethods[1].method
										}
									}>
									
								<cfelse>
									<!--- error --->
								</cfif>
							<cfelse>
								<!--- error --->
							</cfif>
						<cfelse>
							<!--- error --->
						</cfif>--->
						
						<cfquery name="userAuthen" datasource="#Session.DBSourceREAD#">
			                SELECT PaymentMethodID_vch, PaymentType_vch
			                FROM simplebilling.authorize_user
			                WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#form.userId#">
			                ORDER BY id DESC LIMIT 1
			            </cfquery>
						<cfif userAuthen.RecordCount GT 0>

							<cfset paymentData = {
								amount = totalAmount,
								transactionDuplicateCheckIndicator = 1,
								extendedInformation = {
								    notes = txtExtendPlan
								},
							    developerApplication = {
							      developerId: worldpayApplication.developerId,
							      version: worldpayApplication.version
							    },
								paymentVaultToken = {  
								   customerId = form.userId,
								   paymentMethodId = userAuthen.PaymentMethodID_vch,
								   paymentType = worldpayPaymentTypes[userAuthen.PaymentType_vch]
								}
							}>

						<cfelse>
							<!--- error --->
							<cfset dataout.RXRESULTCODE = 0>
						</cfif>
						
						
					<cfelse>
						
						<cfparam name="form.line1" default="">
						<cfparam name="form.city" default="">
						<cfparam name="form.country" default="">
						<cfparam name="form.cvv" default="">
						<cfparam name="form.email" default="">
						<cfparam name="form.expirationDate" default="">
						<cfparam name="form.firstName" default="">
						<cfparam name="form.lastName" default="">
						<cfparam name="form.number" default="">
						<cfparam name="form.phone" default="">
						<cfparam name="form.state" default="">
						<cfparam name="form.zip" default="">
						<cfparam name="form.save_cc_info" default="">
						
						<cfset paymentData = {
						    amount = totalAmount,
						    transactionDuplicateCheckIndicator = 1,
						    extendedInformation = {
						        notes = txtExtendPlan
						    },
						    developerApplication = {
						      developerId: worldpayApplication.developerId,
						      version: worldpayApplication.version
						    },
						    card = {
								number = form.number,
								cvv = form.cvv,
								expirationDate  = form.expirationDate,
								firstName   = form.firstName,
								lastName    = form.lastName,
								address     = {
									line1   = form.line1,
									city	= form.city,
									state   = form.state,
									zip		= form.zip,
									country = form.country,
									phone	= form.phone
								},
								email = form.email,
								emailReceipt = false
							}
						}>
						
					</cfif>
					
					<cfif dataout.RXRESULTCODE EQ 1>
					
						<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
						  <cfhttpparam type="header" name="content-type" value="application/json">
						  <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
						  <cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
						</cfhttp>
					
						<!---<cfif paymentResponse.status_code EQ 200>--->
							<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
								<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
								<cfif paymentResponseContent.success>
									<!--- IF User do not own keyword after expired --->
									<cfif checkOwed EQ 0 >
											<cfset BuyKeywordNumber = userPlansQuery.BuyKeywordNumber_int + userPlansQuery.BuyKeywordNumberExpired_int>
											<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
												INSERT INTO simplebilling.userplans (
													Status_int,
													UserId_int,
													PlanId_int,
													Amount_dec,
													UserAccountNumber_int,
													Controls_int,
													KeywordsLimitNumber_int,
													FirstSMSIncluded_int,
													UsedSMSIncluded_int,
													CreditsAddAmount_int,
													PriceMsgAfter_dec,
													PriceKeywordAfter_dec,
													BuyKeywordNumber_int,
													StartDate_dt,
													EndDate_dt,
													MlpsLimitNumber_int,
													ShortUrlsLimitNumber_int,
													MlpsImageCapacityLimit_bi
												) VALUES (
													<cfqueryparam cfsqltype="cf_sql_integer" value="1">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#form.userId#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.PlanId_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.Amount_dec#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.UserAccountNumber_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.Controls_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.KeywordsLimitNumber_int#">,

										        	<!--- <cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.KeywordsLimitNumber_int#">, --->
										        	<!---<cfqueryparam cfsqltype="cf_sql_integer" value="#(
										        		plansQuery.Amount_dec GT 0 ?
										        		plansQuery.FirstSMSIncluded_int : userPlansQuery.FirstSMSIncluded_int
										        	)#">,--->
													<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.FirstSMSIncluded_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#(
										        		plansQuery.Amount_dec GT 0 ? 0 :
										        		userPlansQuery.UsedSMSIncluded_int
										        	)#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.CreditsAddAmount_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceMsgAfter_dec#">,
										        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceKeywordAfter_dec#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeywordNumber#">,
										        	<cfqueryparam cfsqltype="cf_sql_date" value="#userPlansQuery.EndDate_dt#">,
										        	<cfqueryparam cfsqltype="cf_sql_date" value="#newEndDate_dt#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.MlpsLimitNumber_int#">,
										        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.ShortUrlsLimitNumber_int#">,
													<cfqueryparam cfsqltype="cf_sql_bigint" value="#userPlansQuery.MlpsImageCapacityLimit_bi#">
												)
											</cfquery>

											<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
												UPDATE simplebilling.userplans 
													SET Status_int = 0
												WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#form.userId#">
													AND Status_int = 1 AND DATE(EndDate_dt) <= CURDATE()
											</cfquery>

											<!--- https://setaintl2008.atlassian.net/browse/SIRE-1469 --->
											<!--- <cfif plansQuery.Amount_dec GT 0> --->
											<!--- RESET PLAN BALANCE --->
											<cfinvoke component="public.sire.models.cfc.order_plan" method="UpdateUserPlanBalance" returnvariable="RetupdateUserPlanBalance">
												<cfinvokeargument name="inpUserId" value="#form.userId#">
												<cfinvokeargument name="inpBalance" value="#userPlansQuery.FirstSMSIncluded_int#">
											</cfinvoke> 
											<!---	
												</cfif>
											--->

											<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
									        <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
									            <cfinvokeargument name="inpUserId" value="#form.userId#">
									        </cfinvoke>
											
									<cfelse> <!--- PAY FOR OWED KEYWORD --->
										<cfset BuyKeywordNumberExpired = 0>

										<cfif RetUserPlan.PLANEXPIRED EQ 1> <!--- within 30 days --->
											<cfset BuyKeywordNumber = userPlansQuery.BuyKeywordNumber_int + userPlansQuery.BuyKeywordNumberExpired_int>
											<cfset newStartDate_dt = userPlansQuery.EndDate_dt>
										<cfelse> <!--- over 30days plan is downgrade but if you own keyword, you can pay for this : buy keyword = buy BuyKeywordNumberExpired_int --->
											<cfset BuyKeywordNumber = userPlansQuery.BuyKeywordNumberExpired_int>
											<cfset newEndDate_dt = userPlansQuery.EndDate_dt>
											<cfset newStartDate_dt = userPlansQuery.StartDate_dt>
										</cfif>	

										<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
											INSERT INTO simplebilling.userplans (
												Status_int,
												UserId_int,
												PlanId_int,
												Amount_dec,
												UserAccountNumber_int,
												Controls_int,
												KeywordsLimitNumber_int,
												FirstSMSIncluded_int,
												UsedSMSIncluded_int,
												CreditsAddAmount_int,
												PriceMsgAfter_dec,
												PriceKeywordAfter_dec,
												BuyKeywordNumber_int,
												BuyKeywordNumberExpired_int,
												StartDate_dt,
												EndDate_dt,
												MlpsLimitNumber_int,
												ShortUrlsLimitNumber_int,
												MlpsImageCapacityLimit_bi
											) VALUES (
												<cfqueryparam cfsqltype="cf_sql_integer" value="1">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#form.userId#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.PlanId_int#">,
									        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.Amount_dec#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.UserAccountNumber_int#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.Controls_int#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.KeywordsLimitNumber_int#">,
									        	<!--- <cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.KeywordsLimitNumber_int#">, --->
									        	<!---
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#(
									        		plansQuery.Amount_dec GT 0 ?
									        		plansQuery.FirstSMSIncluded_int : userPlansQuery.FirstSMSIncluded_int
									        	)#">,
									        	--->
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.FirstSMSIncluded_int#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#(
									        		plansQuery.Amount_dec GT 0 ? 0 :
									        		userPlansQuery.UsedSMSIncluded_int
									        	)#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.CreditsAddAmount_int#">,
									        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceMsgAfter_dec#">,
									        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceKeywordAfter_dec#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeywordNumber#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeywordNumberExpired#">,
									        	<cfqueryparam cfsqltype="cf_sql_date" value="#newStartDate_dt#">,
									        	<cfqueryparam cfsqltype="cf_sql_date" value="#newEndDate_dt#">,
									        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.MlpsLimitNumber_int#">,
										        <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.ShortUrlsLimitNumber_int#">,
										        <cfqueryparam cfsqltype="cf_sql_bigint" value="#userPlansQuery.MlpsImageCapacityLimit_bi#">
											)
										</cfquery>

										<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
											UPDATE simplebilling.userplans 
												SET Status_int = 0
											WHERE UserPlanId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#userPlansQuery.UserPlanId_bi#">
												AND Status_int = 1
										</cfquery>

										<!--- https://setaintl2008.atlassian.net/browse/SIRE-1469 --->
										<!--- <cfif plansQuery.Amount_dec GT 0> --->
										<!--- RESET PLAN BALANCE --->
										<cfinvoke component="public.sire.models.cfc.order_plan" method="UpdateUserPlanBalance" returnvariable="RetupdateUserPlanBalance">
											<cfinvokeargument name="inpUserId" value="#form.userId#">
											<cfinvokeargument name="inpBalance" value="#userPlansQuery.FirstSMSIncluded_int#">
										</cfinvoke> 
										<!---	
											</cfif>
										--->

										<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
								        <cfinvoke method="AddReferralCredits" component="session.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
								            <cfinvokeargument name="inpUserId" value="#form.userId#">
								        </cfinvoke>
						
									</cfif>
								
									
									<cfif form.currentCard NEQ 1 AND form.save_cc_info EQ 1>
										<cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
							                <cfinvokeargument name="payment_method" value="0">
							                <cfinvokeargument name="line1" value="#form.line1#">
							                <cfinvokeargument name="city" value="#form.city#">
							                <cfinvokeargument name="country" value="#form.country#">
							                <cfinvokeargument name="phone" value="#form.phone#">
							                <cfinvokeargument name="state" value="#form.state#">
							                <cfinvokeargument name="zip" value="#form.zip#">
							                <cfinvokeargument name="email" value="#form.email#">
							                <cfinvokeargument name="cvv" value="#form.cvv#">
							                <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
							                <cfinvokeargument name="firstName" value="#form.firstName#">
							                <cfinvokeargument name="lastName" value="#form.lastName#">
							            	<cfinvokeargument name="number" value="#form.number#">
							            	<cfinvokeargument name="primaryPaymentMethodId" value="0">
							            	<cfinvokeargument name="userId" value="#form.userId#">
							            </cfinvoke>
									</cfif>

							        <!--- Add Userlog --->
							        <cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
						                <cfinvokeargument name="userId" value="#form.userId#">
						                <cfinvokeargument name="moduleName" value="Payment Processing">
						                <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Extent plan #plansQuery.PlanName_vch#">
						            </cfinvoke>
						            
									<!--- Sendmail payment --->
									<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							            <cfinvokeargument name="to" value="#paymentResponseContent.transaction.email#">
							            <cfinvokeargument name="type" value="html">
							            <cfinvokeargument name="subject" value="[SIRE][Extent Plan] Payment Info">
							            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
							            <cfinvokeargument name="data" value="#paymentResponse.filecontent#">
							        </cfinvoke>
							        
									<cfset dataout.MESSAGE = "Extend Plan Complete. Thank you!">
									
							        <!--- close session--->
									<cfset closeBatchesData = {
										developerApplication = {
											developerId: worldpayApplication.developerId,
											version: worldpayApplication.version
										}
									}>
							        <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
								        <cfhttpparam type="header" name="content-type" value="application/json">
								        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
								        <cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
							        </cfhttp>
							        
								<cfelse>
									<!--- error --->
									<cfset dataout.RXRESULTCODE = 0>
									<cfset dataout.MESSAGE = paymentResponseContent.message>
								</cfif>
							<cfelse>
								<!--- error --->
								<cfset dataout.RXRESULTCODE = 0>
								<cfset dataout.MESSAGE = paymentResponse.errordetail>
							</cfif>
						<!---<cfelse>
							<!--- error --->
							<cfset dataout.RXRESULTCODE = 0>
						</cfif>--->

						<cfif dataout.RXRESULTCODE EQ 1>
							<cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#" result="logPaymentWorldpaysResult">
								INSERT INTO simplebilling.payment_worldpay (
									userId_int,
									planId, 
									moduleName,
									bynumberKeyword,
									bynumberSMS,
									transactionType, 
									customerId, 
									orderId, 
									transactionId, 
									authorizationCode, 
									authorizedAmount, 
									paymentTypeCode, 
									paymentTypeResult, 
									transactionData_date, 
									transactionData_amount, 
									creditCardType, 
									cardNumber, 
									avsCode, 
									cardHolder_FirstName, 
									cardHolder_LastName, 
									billAddress_line1, 
									billAddress_city, 
									billAddress_state, 
									billAddress_zip, 
									billAddress_company, 
									billAddress_phone, 
									email, 
									method, 
									responseText,
									created
								) VALUES (
				                    <cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#form.userId#">,
				                    <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PlanId_int#">,
				                    <cfqueryparam cfsqltype="cf_sql_varchar" value="Extend">,
				                    <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.BuyKeywordNumber_int#">,
				                    <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
				                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.transactionType#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.customerId#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.orderId#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.transactionId#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.authorizationCode#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.authorizedAmount#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.paymentTypeCode#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.paymentTypeResult#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.transactionData.date#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.transactionData.amount#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.creditCardType#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.cardNumber#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.avsCode#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.cardHolder_FirstName#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.cardHolder_LastName#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.line1#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.city#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.state#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.zip#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.company#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.billAddress.phone#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.email#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.method#">, 
				        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponseContent.transaction.responseText#">,
					        		NOW()
								)
							</cfquery>
						<cfelse>
							<cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#" result="logPaymentWorldpaysResult">
								INSERT INTO simplebilling.log_payment_worldpay (
									userId_int,
									moduleName,
									status_code,
									status_text,
									errordetail,
									filecontent,
									created
								) VALUES (
					        		<cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#form.userId#">,
					        		<cfqueryparam cfsqltype="cf_sql_varchar" value="Extend">,
					        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponse.statuscode#">,
					        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponse.text#">,
					        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponse.errordetail#">,
					        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#paymentResponse.filecontent#">,
					        		NOW()
								)
							</cfquery>
						</cfif>

					</cfif>

				<cfelse>
					<!--- error --->
					<cfset dataout.RXRESULTCODE = 0>
					<cfset dataout.MESSAGE = 'Your plan can not extend. Please contact customer support at support@siremobile.com.'>
				</cfif>
			<cfelseif expricedTime LT 0>
				<!--- error --->
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = 'Your plan has not expired. Please login.'>
			<cfelse>
				<!--- error --->
				<cfset dataout.RXRESULTCODE = 0>
				<cfset dataout.MESSAGE = 'Your plan is expired more than 30 days. Please contact customer support at support@siremobile.com.'>
			</cfif>
		<cfelse>
			<!--- error --->
			<cfset dataout.RXRESULTCODE = 0>
			<cfset dataout.MESSAGE = 'Your plan can not extend. Please contact customer support at support@siremobile.com.'>
		</cfif>
	<cfelse>
		<!--- error --->
		<cfset dataout.RXRESULTCODE = 0>
		<cfset dataout.MESSAGE = 'Your account can not extend. Please contact customer support at support@siremobile.com.'>
	</cfif>
<cfelse>
	<!--- error --->
	<cfset dataout.RXRESULTCODE = 0>
	<cfset dataout.MESSAGE = 'Your account can not extend. Please contact customer support at support@siremobile.com.'>
</cfif>

<cfoutput>#SerializeJSON(dataout)#</cfoutput>