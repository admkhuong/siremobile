<cfset todayDate = Now()>

<cfset startDay = Day(todayDate)>
<cfif startDay GT 28>
    <cfset startDay = 28>
</cfif>

<cfset startMonth = Month(todayDate)>
<cfset startYear = Year(todayDate)>
    
<cfset _startDate = CreateDate(startYear, startMonth, startDay)> 

<cfset _endDate = DateAdd("m",1,_startDate)>	

<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>

<cfset userNewFreePlanStartDate = DateAdd("d", 30, Variables.userPlansQuery.EndDate_dt[threadId])>
<cfset userNewFreePlanEndDate = DateAdd("m", 1, userNewFreePlanStartDate)>

<!--- CALCULATE OWED KEYWORD = KEYWORD USED - (FREEPLAN KEYWORD + Buy Keyword + Buy Keyword within 30 day. If OWED KEYWORD < 0 -> OWED KEYWORD = 0 --->
<cfset owedKeyword = getUserPlan.KEYWORDUSE - getUserPlan.FREEPLANKEYWORDSLIMITNUMBER >

<cfif owedKeyword LT 0>
    <cfset owedKeyword = 0>
<cfelse>
    <cfset _totalkeywordHaveToPaid= owedKeyword>
</cfif>

<cfif getUserPlan.PLANEXPIRED EQ 3 AND getUserPlan.KEYWORDBUYAFTEREXPRIED GT 0> <!--- IF owned keyword -> extend expired date --->
    
    <cfset downgradeDate = getUserPlan.DOWNGRADEDATE > 

    <cfset _newUserPlansInfo['Status_int'] = 1>
    <cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
    <cfset _newUserPlansInfo['PlanId_int'] = Variables.planFree.PlanId_int>
    <cfset _newUserPlansInfo['Amount_dec'] = Variables.planFree.Amount_dec>
    <cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.planFree.UserAccountNumber_int>
    <cfset _newUserPlansInfo['Controls_int'] = Variables.planFree.Controls_int>
    <cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.planFree.KeywordsLimitNumber_int>
    <cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]>
    <cfset _newUserPlansInfo['UsedSMSIncluded_int'] = Variables.userPlansQuery.UsedSMSIncluded_int[threadId]>
    <cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.planFree.CreditsAddAmount_int>
    <cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.planFree.PriceMsgAfter_dec>
    <cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.planFree.PriceKeywordAfter_dec>
    <cfset _newUserPlansInfo['BuyKeywordNumber_int'] = 0>
    <cfset _newUserPlansInfo['StartDate_dt'] = _startDate>
    <cfset _newUserPlansInfo['EndDate_dt'] = _endDate>
    <cfset _newUserPlansInfo['MlpsLimitNumber_int'] = Variables.planFree.MlpsLimitNumber_int>
    <cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.planFree.ShortUrlsLimitNumber_int>
    <cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.planFree.MlpsImageCapacityLimit_bi>
    <cfset _newUserPlansInfo['PromotionId_int'] = 0>
    <cfset _newUserPlansInfo['PromotionLastVersionId_int'] = 0>
    <cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['BillingType_ti'] = 1>
    <cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = _endDate>
    <cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = owedKeyword>
    <cfset _newUserPlansInfo['DowngradeDate_dt'] = downgradeDate>
    <cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = 0>
    <cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _totalkeywordHaveToPaid>
    <!--- <cfset _newUserPlansInfo['PreviousPlan_int'] = Variables.userPlansQuery.PlanId_int[threadId]> --->

    <cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

        <!--- RESET PLAN CREDIT DEFAULT = the last user plan sms --->
    <cfset arrayAppend(Variables.newBalanceBillings, [
        Variables.userPlansQuery.UserId_int[threadId], 
        Variables.userPlansQuery.FirstSMSIncluded_int[threadId]
    ])>							

<cfelse> <!--- IF DOWNGRADE INSERT DowngradeDate_dt --->	
                                                        
    <cfset _newUserPlansInfo['Status_int'] = 1>
    <cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
    <cfset _newUserPlansInfo['PlanId_int'] = Variables.planFree.PlanId_int>
    <cfset _newUserPlansInfo['Amount_dec'] = Variables.planFree.Amount_dec>
    <cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.planFree.UserAccountNumber_int>
    <cfset _newUserPlansInfo['Controls_int'] = Variables.planFree.Controls_int>
    <cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.planFree.KeywordsLimitNumber_int>
    <cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.planFree.FirstSMSIncluded_int>
    <cfset _newUserPlansInfo['UsedSMSIncluded_int'] = 0>
    <cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.planFree.CreditsAddAmount_int>
    <cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.planFree.PriceMsgAfter_dec>
    <cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.planFree.PriceKeywordAfter_dec>
    <cfset _newUserPlansInfo['BuyKeywordNumber_int'] = 0>
    <cfset _newUserPlansInfo['StartDate_dt'] = _startDate>
    <cfset _newUserPlansInfo['EndDate_dt'] = _endDate>
    <cfset _newUserPlansInfo['MlpsLimitNumber_int'] = Variables.planFree.MlpsLimitNumber_int>
    <cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.planFree.ShortUrlsLimitNumber_int>
    <cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.planFree.MlpsImageCapacityLimit_bi>
    <cfset _newUserPlansInfo['PromotionId_int'] = 0>
    <cfset _newUserPlansInfo['PromotionLastVersionId_int'] = 0>
    <cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = 0>
    <cfset _newUserPlansInfo['BillingType_ti'] = 1>
    <cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = _endDate>
    <cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
    <cfset _newUserPlansInfo['DowngradeDate_dt'] = Now()>
    <cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
    <cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
    <cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _totalkeywordHaveToPaid>

    <cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

    <!--- Remove Coupon on this account --->
    <cfinvoke component="public.sire.models.cfc.promotion" method="DeactiveUserCoupon" returnvariable="CouponForUser">
        <cfinvokeargument name="inpUserId" value="#Session.UserId#">
    </cfinvoke>

    <!--- RESET PLAN CREDIT DEFAULT = Free plan credit --->
    <cfset arrayAppend(Variables.newBalanceBillings, [
        Variables.userPlansQuery.UserId_int[threadId], 
        Variables.planFree.FirstSMSIncluded_int
    ])>

    <!--- RELEASE USER DATA --->
    <cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
        <!--- <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#"> --->
        <cfinvokeargument name="inpkeywordUsed" value="0">
        <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
        <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
        <!--- <cfinvokeargument name="inpLimitKeyword" value="#Variables.planFree.KeywordsLimitNumber_int#"> --->
        <cfinvokeargument name="inpLimitKeyword" value="0">
        <cfinvokeargument name="inpLimitShortUrl" value="#Variables.planFree.ShortUrlsLimitNumber_int#">
        <cfinvokeargument name="inpLimitMLP" value="#Variables.planFree.MlpsLimitNumber_int#">
    </cfinvoke>
</cfif>

<cfif getUserPlan.PLANEXPIRED EQ 2> <!--- ONLY SEND EMAIL WHEN DOWNGRADE --->

    <cfset emailsend = "#Variables.usersQuery.EmailAddress_vch[userIndex]#">	
    <!--- SEND EMAIL NOTICE DOWNGRADE PLAN  --->
    <cfset downgradeData = {}>
    <cfset downgradeData['USERID'] = Variables.userPlansQuery.UserId_int[threadId] >
    <cfset downgradeData['PLANSTARTDATE'] = userNewFreePlanStartDate >
    <cfset downgradeData['PLANENDDATE'] = userNewFreePlanEndDate >
    <cfset downgradeData['BUYKEYWORDS'] = owedKeyword >
    <cfset downgradeData['FREEPLANCREDIT'] = Variables.planFree.FirstSMSIncluded_int >
    
    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
        <cfinvokeargument name="to" value="#emailsend#">
        <cfinvokeargument name="type" value="html">
        <cfinvokeargument name="subject" value="Downgrading your plan">
        <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_downgrade_plan.cfm">
        <cfinvokeargument name="data" value="#SerializeJSON(downgradeData)#">
    </cfinvoke>

    <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp">
        <cfinvokeargument name="InpEmailString" value="#emailsend#">
        <cfinvokeargument name="InpAccountType" value="Free">
    </cfinvoke>

</cfif>

<!--- RESET USER REFERRAL CREDITS --->
<cfinvoke method="ResetUserReferral" component="public.sire.models.cfc.referral">
    <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
    <cfinvokeargument name="inpIsDowngrade" value="1">
</cfinvoke>