						<!--- RELEASE USER KEYWORD --->
						<cfset totalAvaiablekeyword = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId] + promotion_keyword>
						<cfset textLog &= NL & 'RELEASE KEYWORD WHEN YEARLY PLAN CAN NOT PURCHASED.'>
						<cfset textLog &= NL & 'Release keyword:' & Variables.userPlansQuery.BuyKeywordNumber_int[threadId]>
						<!--- <cfset textLog &= NL & 'Keyword used:' & keywordUsed>
						<cfset textLog &= NL & 'Keyword limit:' & totalAvaiablekeyword> --->
						
						<cfinvoke method="ReleaseUserKeyword" component="public.sire.models.cfc.order_plan" returnvariable="RXReleaseUserKeyword">
				            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
				            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
				            <cfinvokeargument name="inpLimitKeyword" value="#totalAvaiablekeyword#">
				            <cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
				        </cfinvoke>

				        <cfset todayDate = Now()>

						<cfset startDay = Day(todayDate)>
						<cfif startDay GT 28>
							<cfset startDay = 28>
						</cfif>

						<cfset startMonth = Month(todayDate)>
					    <cfset startYear = Year(todayDate)>
					       
					    <cfset _startDate = CreateDate(startYear, startMonth, startDay)> 

					    <cfset _endDate = DateAdd("m",1,_startDate)>

				        <cfinvoke method="UpdateUserPlanAddBenefitDate" component="public.sire.models.cfc.order_plan">
				 			<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
				            <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#_endDate#">
				            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
				 		</cfinvoke>

				 		<cfinvoke method="ResetNumberOfRecurring" component="public.sire.models.cfc.order_plan">
				 			<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
				 			<cfinvokeargument name="inpRecurringType" value="2">
				            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
				 		</cfinvoke>

				        <cfif RXReleaseUserKeyword.RXRESULTCODE EQ 1>
				        	<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]/>

					        <cfset declineDataContent = {} >

					        <cfset declineDataContent.USERID = Variables.userPlansQuery.UserId_int[threadId]>
					        <cfset declineDataContent.PLANKEYWORD = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId] >
					        <cfset declineDataContent.PROMOKEYWORD = promotion_keyword >
					        <cfset declineDataContent.PURCHASEDKEYWORD = 0 >

					        <cfset declineDataContent.ACTIVEKEYWORD = keywordUsed GT totalAvaiablekeyword ? totalAvaiablekeyword : keywordUsed>

					        <cfset declineDataContent.AVAILABLEKEYWORD = declineDataContent.PLANKEYWORD + declineDataContent.PROMOKEYWORD + declineDataContent.PURCHASEDKEYWORD - declineDataContent.ACTIVEKEYWORD >

					        <cfset declineDataContent.REMOVEDKEYWORDS = arrayToList(RXReleaseUserKeyword.LISTKEYWORD)>
					        
							<cfif IsDate(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
					    		<cfset declineDataContent.SENDDATE = dateFormat(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId], "mmm dd yyyy")>
								<cfset declineDataContent.DISABLEDATE = dateFormat(dateAdd("m", 1, Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId]), "mmm dd yyyy")>
							<cfelse>	
								<cfset declineDataContent.SENDDATE = dateFormat(Variables.userPlansQuery.EndDate_dt[threadId], "mmm dd yyyy")>
								<cfset declineDataContent.DISABLEDATE = dateFormat(dateAdd("m", 1, declineDataContent.SENDDATE), "mmm dd yyyy")>
					    	</cfif>

					        <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
					            <cfinvokeargument name="to" value="#emailsend#">
					            <cfinvokeargument name="type" value="html">
					            <cfinvokeargument name="subject" value="Keyword disabled">
					            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_disable_purchased_keyword.cfm">
					            <cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
					        </cfinvoke>
				        </cfif>