<cfif !arrayIsEmpty(newUserPlans)>
    <!---  Other Discount --->
    <cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
        INSERT INTO simplebilling.userplans (
            Status_int,
            UserId_int,
            PlanId_int,
            Amount_dec,
            UserAccountNumber_int,
            Controls_int,
            KeywordsLimitNumber_int,
            FirstSMSIncluded_int,
            UsedSMSIncluded_int,
            CreditsAddAmount_int,
            PriceMsgAfter_dec,
            PriceKeywordAfter_dec,
            BuyKeywordNumber_int,
            StartDate_dt,
            EndDate_dt,
            MlpsLimitNumber_int,
            ShortUrlsLimitNumber_int,
            MlpsImageCapacityLimit_bi,
            PromotionId_int,
            PromotionLastVersionId_int,
            PromotionKeywordsLimitNumber_int,
            PromotionMlpsLimitNumber_int,
            PromotionShortUrlsLimitNumber_int,
            BillingType_ti,
            MonthlyAddBenefitDate_dt,
            BuyKeywordNumberExpired_int,
            DowngradeDate_dt,
            PreviousPlanId_int,
            UserIdApprovedDowngrade_int,
            TotalKeywordHaveToPaid_int
        ) VALUES
        
        <cfset _comma = ''> 
        <cfloop array="#newUserPlans#" index="_plan">
            #_comma#(
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['Status_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['UserId_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PlanId_int']#">,
                <cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan['Amount_dec']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['UserAccountNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['Controls_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['KeywordsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['FirstSMSIncluded_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['UsedSMSIncluded_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['CreditsAddAmount_int']#">,
                <cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan['PriceMsgAfter_dec']#">,
                <cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan['PriceKeywordAfter_dec']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['BuyKeywordNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_date" value="#_plan['StartDate_dt']#">,
                <cfqueryparam cfsqltype="cf_sql_date" value="#_plan['EndDate_dt']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['MlpsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['ShortUrlsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_bigint" value="#_plan['MlpsImageCapacityLimit_bi']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PromotionId_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PromotionLastVersionId_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PromotionKeywordsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PromotionMlpsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PromotionShortUrlsLimitNumber_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['BillingType_ti']#">,
                <cfqueryparam cfsqltype="cf_sql_date" value="#_plan['MonthlyAddBenefitDate_dt']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['BuyKeywordNumberExpired_int']#">,
                <cfqueryparam cfsqltype="cf_sql_date" value="#_plan['DowngradeDate_dt']#" null="#NOT len(trim(_plan['DowngradeDate_dt']))#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['PreviousPlanId_int']#" null="#NOT len(trim(_plan['PreviousPlanId_int']))#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['UserIdApprovedDowngrade_int']#">,
                <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan['TotalKeywordHaveToPaid']#">
            )
            <cfset _comma = ','> 
        </cfloop>

    </cfquery>

    <!--- userPlansResult.generatedKey --->
    <cfset recLogFile('Main info', 
        userPlansResult.RecordCount & ' User Plans Extended' &
        '#NL#User Plans: ' & userPlansResult.GENERATED_KEY
        )>
</cfif>
        
<cfif !arrayIsEmpty(newBalanceBillings)>
    <cfset userIdList = ''>
    <cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
        UPDATE simplebilling.billing 
            SET Balance_int = (CASE
                <cfloop array="#newBalanceBillings#" index="_balance">
                    <cfset userIdList = listAppend(userIdList,_balance[1])>
                    WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#_balance[1]#"> 
                        THEN <cfqueryparam cfsqltype="cf_sql_decimal" value="#_balance[2]#">
                </cfloop>
                ELSE 0
            END)
        WHERE UserId_int IN (#userIdList#)
    </cfquery>
    <cfset recLogFile('Main info', 
        arrayLen(newBalanceBillings) & ' Balance Billing Updated' &
        '#NL#UserIds: ' & userIdList
        )>
</cfif>

<cfif !arrayIsEmpty(addPromotionCreditBalance)>
    <cfset userIdListPromotion = ''>
    <cfquery name="addPromoCreditBalance" datasource="#Session.DBSourceEBM#" result="addResult">
        UPDATE 
            simplebilling.billing
        SET
            PromotionCreditBalance_int = PromotionCreditBalance_int + (CASE
                <cfloop array="#addPromotionCreditBalance#" index="index">
                <cfset userIdListPromotion = listAppend(userIdListPromotion,index[1])>
                WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[1]#"/>
                    THEN <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#index[2]#"/>
                </cfloop>
            END)
        WHERE 
            UserId_int IN (#userIdListPromotion#)
    </cfquery>
</cfif>

<cfif !arrayIsEmpty(promocodeListToIncreaseUsedTime)>
    <cfloop array="#promocodeListToIncreaseUsedTime#" index="index">
        <!--- Increase coupon code used time --->
        <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
            <cfinvokeargument name="inpCouponId" value="#index[1]#"/>
            <cfinvokeargument name="inpUsedFor" value="1"/>
        </cfinvoke>
        <!--- End increase coupon code used time --->

        <!--- Insert coupon code used log --->
        <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
            <cfinvokeargument name="inpCouponId" value="#index[2]#"/>
            <cfinvokeargument name="inpOriginCouponId" value="#index[1]#"/>
            <cfinvokeargument name="inpUserId" value="#index[3]#"/>
            <cfinvokeargument name="inpUsedFor" value="Recurring"/>
        </cfinvoke>    
        <!--- End insert log --->

        <cfquery name="updateUsedTimePromoCode" datasource="#Session.DBSourceEBM#" result="updateResult">
            UPDATE 
            simplebilling.userpromotions
            SET
            UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
            WHERE 
            UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[3]#">
            AND 
            PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[2]#"> 
            AND 
            UserPromotionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[4]#">
        </cfquery>
    </cfloop>

    <!--- <cfset recLogFile('Main info', 
        arrayLen(promocodeListToIncreaseUsedTime) & ' Promotion Code Used Time' &
        '#NL#Promotion Code Used Time: ' & serializeJSON(promocodeListToIncreaseUsedTime)
        )> --->
</cfif>

<cfif !arrayIsEmpty(oldUserPlans)>
    <cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
        UPDATE simplebilling.userplans 
            SET Status_int = (CASE
                <cfset i = 1>
                <cfloop array="#oldUserPlans#" index="_plan">
                    <cfif isArray(_plan)>
                        WHEN UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#"> 
                            THEN <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">
                        <cfset oldUserPlans[i] = _plan[1]>
                    </cfif>
                    <cfset i++>
                </cfloop>
                WHEN 0 = 1 THEN Status_int ELSE 0
            END) 
        WHERE UserPlanId_bi IN (#arrayToList(oldUserPlans, ',')#)
    </cfquery>
    <cfset recLogFile('Main info', 
        arrayLen(oldUserPlans) & ' User Plans Expired' &
        '#NL#User Plans: ' & arrayToList(oldUserPlans, ',')
        )>
</cfif>

<cfif !arrayIsEmpty(errorUserPlans)>
    <cfset logData.ErrorNumber_bi += arrayLen(errorUserPlans)>
    <cfset recLogFile('Main info', 
        arrayLen(errorUserPlans) & ' User Plans Error' &
        '#NL#User Plans: ' & arrayToList(errorUserPlans, ',')
        )>
</cfif>

<cfif !arrayIsEmpty(userListToSendAlertCCDeclineToAdmin)>
    <cfset datalistacc = {}/>
    <cfset datalistacc.RESULT = 1/>
    <cfset datalistacc['DATALIST'] = userListToSendAlertCCDeclineToAdmin/>

    <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
        <cfinvokeargument name="to" value="#SupportEMailUserName#"/>
        <cfinvokeargument name="type" value="html"/>
        <cfinvokeargument name="subject" value="[SIRE] Customer's credit card declined 3 times"/>
        <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_alert_cc_declined_for_admin.cfm"/>
        <cfinvokeargument name="data" value="#TRIM(serializeJSON(datalistacc))#"/>
    </cfinvoke>
</cfif>

<!--- Log payment worldpay response --->
<cfinvoke method="logPaymentWorldPay" component="public.sire.models.cfc.order_plan">
    <cfinvokeargument name="inplogPaymentWorldpays" value="#logPaymentWorldpays#"/>
    <cfinvokeargument name="inpFcm" value="#_fcm#"/>
    <cfinvokeargument name="inpNow" value="#_now#"/>
</cfinvoke>   