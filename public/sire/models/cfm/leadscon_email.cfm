<cfparam name="infoEmail" default="" />
<cfparam name="infoName" default="" />
<cfparam name="infoPhonenumber" default="" />


<cfinclude template="../../../paths.cfm">
<cfinclude template="../../configs/paths.cfm">

<cfset locationData = StructNew() />
<cfset locationData.Filecontent = "No location Data available at this time" />
<cftry>
 	<cfoutput>
        <cfhttp url="http://freegeoip.net/json/#CGI.REMOTE_ADDR#" method="GET" result="locationData" >
        </cfhttp>
	</cfoutput>
<cfcatch type="any">
</cfcatch>
</cftry>

<!--- Validate fields --->
<cfif TRIM(infoEmail) NEQ "">
     
    <cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
        SELECT
            ContactAddress_vch                              
        FROM 
            simpleobjects.systemalertscontacts
        WHERE
            ContactType_int = 2
        AND
            EMSNotice_int = 1    
    </cfquery>
   
    <cfif GetEMSAdmins.RecordCount GT 0>
        <cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                            
    </cfif>    

    <cfmail to="#EBMAdminEMSList#" subject="[Leadscon] SIRE Information Request" type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
        <cfoutput>
          
            <!--- Inline styles only! - Don't use style tags - gmail wont honor them --->                       
            <body style='background-image: linear-gradient(to bottom, ##FFAE00, ##0085c8);	background-repeat:no-repeat; font-family: "Helvetica";'>
        
                <div id="divConfirm" align="left" style="padding:25px; width:600px;">
                    <div ><h4> [Leadscon] SIRE Information Request</h4></div>
                   
                    <div class="message-block  m_top_10" style='font-family: "helvetica";	font-size: 12px;margin-left: 21px; margin-top: 10px;'>
                        <div class="left-input" style="border-radius: 4px 0 0 4px;	border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666;	float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px;	background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
                            <span class="em-lbl" style="width: auto;">E-mail</span>
                        </div>
                        <div class="right-input" style="width: 226px; display:inline; height: 28px;">		        		
                            <input type="text" readonly value="#infoEmail#" style="width: 210px; background-color: ##FBFBFB; color:##000000;  font-size: 12px;  height: 28px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    <div class="message-block  m_top_10" style='font-family: "helvetica";	font-size: 12px;margin-left: 21px; margin-top: 10px;'>
                        <div class="left-input" style="border-radius: 4px 0 0 4px;	border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666;	float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px;	background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
                            <span class="em-lbl" style="width: auto;">Name</span>
                        </div>
                        <div class="right-input" style="width: 226px; display:inline; height: 28px;">	        		
                            <input type="text" readonly value="#infoName#" style="width: 210px; background-color: ##FBFBFB;color: ##000000;  font-size: 12px;  height: 28px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">
                        </div>				
                    </div>
                    <div class="clear"></div>
                    <div class="message-block  m_top_10" style='font-family: "helvetica";   font-size: 12px;margin-left: 21px; margin-top: 10px;'>
                        <div class="left-input" style="border-radius: 4px 0 0 4px;  border-style: solid none solid solid; border-width: 1px 0 1px 1px; color: ##666666; float: left; height: 28px; line-height: 25px; padding-left: 10px; width: 170px; background-image: linear-gradient(to bottom, ##FFFFFF, ##F4F4F4);">
                            <span class="em-lbl" style="width: auto;">Phone Number</span>
                        </div>
                        <div class="right-input" style="width: 226px; display:inline; height: 28px;">                   
                            <input type="text" readonly value="#infoPhonenumber#" style="width: 210px; background-color: ##FBFBFB;color: ##000000;  font-size: 12px;  height: 28px; line-height: 28px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 0 8px;">
                        </div>              
                    </div>
                    
                    <div class="clear"></div>       

                    <div style="height:200px;"></div>
    
                    <div class="clear"></div> 
    
                    <!---<div style="padding:20px;">		
                       <cfdump var="#CGI#"/>
                    </div>

					<div class="clear"></div> 
    
                    <div style="padding:20px;">		
                       <cfdump var="#locationData.Filecontent.toString()#"/>
                    </div>--->
                </div>
            </body>
        
        </cfoutput>
    </cfmail>
</cfif>

<!--- <cfheader statuscode="202" statustext="Server Error"> --->
{}
