<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_enterprise_user_account_balance_low_warning"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 1/>
<cfset NOCName = "Enterprise User Account Balance Low Warning"/>
<cfset data = {}/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>
<cfset GetUserAccountBanlanceLow = ''>


<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>
    <!--- Enterprise User Account Balance Low Warning --->
    <cfquery name="GetUserAccountBanlanceLow" datasource="#Session.DBSourceEBM#">
        SELECT
			uc.UserId_int,
            CONCAT(uc.UserId_int,' //',uc.FirstName_vch,' ',uc.LastName_vch,' //',uc.MFAContactString_vch,' //',uc.EmailAddress_vch,' //',pl.PlanName_vch,' //',(bl.Balance_int + bl.BuyCreditBalance_int + bl.PromotionCreditBalance_int)) AS UserInfor
		FROM
			simpleobjects.useraccount uc
		JOIN
			simplebilling.billing bl
		ON
			uc.UserId_int = bl.UserId_int
        JOIN
			simplebilling.userplans up
		ON
			uc.UserId_int = up.UserId_int
		JOIN
			simplebilling.plans pl
		ON
			up.PlanId_int = pl.PlanId_int
		JOIN
			simplequeue.enterprise_user_billing_plan ubl
		ON
			uc.UserId_int = ubl.UserId_int
		WHERE
			uc.Active_int = 1
		AND
			uc.IsTestAccount_ti = 0
        AND 
            bl.Balance_int + bl.BuyCreditBalance_int + bl.PromotionCreditBalance_int < ubl.BalanceThreshold_int
        And 
            ubl.BalanceThreshold_int > 0
		ORDER BY
			uc.UserId_int DESC
		LIMIT
			#inpNumberOfDialsToRead#
    </cfquery>
    <cfif GetUserAccountBanlanceLow.RECORDCOUNT gt 0>
        <cfif inpVerboseDebug GT 0>
            ----------- Enterprise User Account Balance Low Warning
            <br/>
            <cfdump var="#GetUserAccountBanlanceLow#" />
            <br/>
        </cfif>

        <cfif GetUserAccountBanlanceLow.RECORDCOUNT GT 0>
            <cfset checkResult = 0/>
            <cfset checkDetails = valueList(GetUserAccountBanlanceLow.UserInfor)/> <!---"Enterprise User Account Balance Low Warning: " & --->
        </cfif>

        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <br/>
                Check result: #checkResult#
                <br/>
            </cfoutput>
        </cfif>

        <cfif inpVerboseDebug GT 0>
            <BR>********** Check Details **********<BR>
            <cfoutput>
                #checkDetails#
            </cfoutput>
        </cfif>
        <!--- Send email alert and logs --->
        <cfinclude template="inc_NOC_SendEmailAccountBalanceLow.cfm"/>
    <cfelse>
    <!--- Data corrected --->
        <cfif inpVerboseDebug GT 0>
           <BR>********** Data correct **********<BR>
        </cfif>
    </cfif>


    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="500" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>