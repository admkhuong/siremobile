<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_check_MBLox"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 0/>
<cfset NOCName = "Check MBLox NOC"/>
<cfset data = {}/>
<cfset listaddition = ''/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>
    <!--- Get NOC infor MBLox --->
    <cfquery datasource="#session.DBSourceREAD#" name="GetNOCMBLox">
        SELECT
            QueryString_vch
        FROM
            simpleobjects.sire_noc_settings
        WHERE
            Name_vch = 'NOC MBLox Status'
        LIMIT
            1
    </cfquery>
    
    <cfset listaddition = deserializeJSON(GetNOCMBLox.QueryString_vch)>
    <cfset TOTALFAIL = val(listaddition['TotalFail'])/>
    <cfset AVGCONNECTTIME = val(listaddition['Avgconnecttime'])/>
    <!--- Check MBLox --->
    <cfquery name="CheckMBLoxStatus" datasource="#Session.DBSourceREAD#">
        SELECT  
                COUNT(*) AS TOTALFAIL 
        FROM 
                simplexresults.contactresults 
        WHERE 
                Created_dt >= DATE_ADD(NOW(), INTERVAL - 1 HOUR) 
                AND Created_dt < NOW() 
                AND SMSMTPostResultCode_vch <> 0
        Having
                 COUNT(*) > <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#TOTALFAIL#'>
    </cfquery>

    <cfquery name="CheckMBLoxStatusSuccess" datasource="#Session.DBSourceREAD#">
        SELECT  
                COUNT(*) AS TOTALSUCCESS 
        FROM 
                simplexresults.contactresults 
        WHERE 
                Created_dt >= DATE_ADD(NOW(), INTERVAL - 1 HOUR) 
                AND Created_dt < NOW() 
                AND SMSMTPostResultCode_vch = 0
    </cfquery>

    <cfquery name="CheckAvgConnectTime" datasource="#Session.DBSourceREAD#">
        SELECT 
            AVG(TotalConnectTime_int) AS AVG_CONNECT_TIME
        FROM 
            simplexresults.contactresults
        WHERE 
            Created_dt >= DATE_ADD(NOW(), INTERVAL - 1 HOUR)
            AND Created_dt < NOW()
        Having 
            AVG(TotalConnectTime_int) > <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#AVGCONNECTTIME#'>
    </cfquery>

    <!--- check MBLox status --->
    <cfif CheckMBLoxStatus.RECORDCOUNT gt 0>
            <cfset checkDetails = "Total Fail: " & CheckMBLoxStatus.TOTALFAIL & "Total Success: " & CheckMBLoxStatusSuccess.TOTALSUCCESS/>
    <cfelse>
             <cfset checkDetails = ""/>
    </cfif>
    <!--- check  avg connect time --->
    <cfif CheckAvgConnectTime.RECORDCOUNT gt 0>
            <cfif checkDetails eq "">
                <cfset checkDetails = "Avg connect time: " & CheckAvgConnectTime.AVG_CONNECT_TIME/>
            <cfelse>
                <cfset checkDetails = checkDetails & ", Avg connect time: " & CheckAvgConnectTime.AVG_CONNECT_TIME/>
            </cfif>
    <cfelse>
            <cfset checkDetails = checkDetails/>
    </cfif>
    <cfif checkDetails eq "">
        <cfset checkResult = 1/>
        <cfset checkDetails = "System work well.">
    </cfif>

    <cfif inpVerboseDebug GT 0>
        ----------- Check MBLox
        <br/>
        <cfdump var="#CheckMBLoxStatus#" />
        <br/>
        <cfdump var="#CheckAvgConnectTime#" />
        <br>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <br/>
            Check result: #checkResult#
            <br/>
        </cfoutput>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <BR>********** Check Details **********<BR>
        <cfoutput>
            #checkDetails#
        </cfoutput>
    </cfif>
       
    
     <!--- Send email alert and logs --->
    <cfinclude template="inc_NOC_SendEmailAndLogs.cfm"/>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="500" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>