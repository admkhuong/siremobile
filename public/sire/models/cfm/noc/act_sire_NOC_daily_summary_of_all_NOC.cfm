<cfinclude template="../cronjob-firewall.cfm"/>
<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_daily_summary_all_NOC"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = ''/>
<cfset checkResult = 0/>
<cfset NOCName = "Daily Summary All NOC"/>
<cfset data = {}/>
<cfset listaddition = ''/>
<cfset GetAllNOCList = ''/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>
    <!--- Daily Summary All NOC --->
    <cfquery datasource="#session.DBSourceREAD#" name="GetAllNOCList">
        SELECT
            Distinct PKId_int,Name_vch
        FROM
            simpleobjects.sire_noc_settings
        WHERE 
            Active_ti = 1
        AND 
            Paused_ti = 0
    </cfquery>

    <!--- Get data for each NOC --->
     <cfloop query = "GetAllNOCList">
            <cfset TotalStatus = 0>
            <cfset TotalStatus = 0>
            <cfquery datasource="#session.DBSourceREAD#" name="GetNOCStatusInfo">
                SELECT 
                        count(*) AS TotalRecord,
                        (SELECT count(*) FROM simplequeue.sire_noc_checks_log Where NOCId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#PKId_int#'> AND Result_int = 0 AND DATE(Created_dt) = DATE(NOW())) As ToTalFail
                FROM 
                        simplequeue.sire_noc_checks_log
                WHERE   
                        NOCId_int = <CFQUERYPARAM CFSQLTYPE='CF_SQL_INTEGER' VALUE='#PKId_int#'>
                AND     
                        DATE(Created_dt) = DATE(NOW())
            </cfquery>
            <cfif GetNOCStatusInfo.RECORDCOUNT GT 0>
                <cfloop query = "GetNOCStatusInfo">
                    <cfset checkDetails = checkDetails & "//" & Name_vch & "/,Total run: " & TotalRecord & ",Total failed: " & ToTalFail/>
                </cfloop>
            <cfelse>
                <cfset checkDetails = checkDetails />
            </cfif>
        <cfdump var = "#GetNOCStatusInfo#">
        <br><br>
    </cfloop>

    <cfif checkDetails eq "">
        <cfset checkResult = 1/>
        <cfset checkDetails = "OK">
    </cfif>

    <cfif inpVerboseDebug GT 0>
        ----------- Daily Summary All NOC
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <br/>
            Check result: #checkResult#
            <br/>
        </cfoutput>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <BR>********** Check Details **********<BR>
        <cfoutput>
            #checkDetails#
        </cfoutput>
    </cfif>
       
    
     <!--- Send email alert and logs --->
    <cfinclude template="inc_NOC_SendEmailDailyAllNOC.cfm"/>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="500" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>