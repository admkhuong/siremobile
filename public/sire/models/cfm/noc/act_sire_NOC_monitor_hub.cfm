<cfinclude template="../cronjob-firewall.cfm"/>

<cfinclude template="/public/sire/configs/paths.cfm">

<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_monitor_hub"/>

<cfset lastErrorDetails = ''/>
<cfset NOCName = "NOC Monitor Hub"/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset index = 1/>
<cfset threadList = ''/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
	<cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
	    <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
	       <cfoutput> already run</cfoutput>
	       <cfexit>
	    <cfelse>
	        <cfset APPLICATION['#lockThreadName#'] = 1/>
	        <cfset getLockThread = 1/>         
	    </cfif>
	</cflock>

	<cfquery name="NOCList" datasource="#session.DBSourceREAD#">
		SELECT
			PKId_int,
			URL_vch,
			Port_int,
			TimeOut_int,
			StartTime_tm,
			IntervalType_vch,
			Interval_tm,
			CASE WHEN IntervalType_vch = "once" AND LaunchedTime_bi = 0
				THEN 1
				ELSE 0
			END AS SentOnce,
			CASE WHEN IntervalType_vch = "weekly" AND 
				IF (DATEDIFF(CURDATE(),DATE(StartDate_dt))%7 =0, 1, 0) > 0 AND 
				IF (TIME_TO_SEC(TIMEDIFF(DATE_FORMAT(NOW(), "%H:%i:00"), DATE_FORMAT(StartTime_tm, "%H:%i:00"))) = 0, 1, 0) > 0
				THEN 1
				ELSE 0
			END AS SentWeekly,
			CASE WHEN IntervalType_vch = "daily" AND
				IF (TIME_TO_SEC(TIMEDIFF(DATE_FORMAT(NOW(), "%H:%i:00"), DATE_FORMAT(StartTime_tm, "%H:%i:00"))) = 0, 1, 0) > 0
				THEN 1
				ELSE 0
			END AS SentDaily,
			CASE WHEN IntervalType_vch = "monthly" AND
				IF (TIME_TO_SEC(TIMEDIFF(DATE_FORMAT(NOW(), "%H:%i:00"), DATE_FORMAT(StartTime_tm, "%H:%i:00"))) = 0, 1, 0) > 0 AND
				IF (DATE_FORMAT(StartDate_dt, "%d") < 29, IF (DATE_FORMAT(NOW(), "%d") = DATE_FORMAT(StartDate_dt, "%d"), 1, 0), IF (DATE_FORMAT(NOW(), "%d") = 28, 1, 0)) > 0
					THEN 1
					ELSE 0
			END AS SentMonthly,

			CASE WHEN IntervalType_vch = "every" AND
				IF ((TIME_TO_SEC((TIMEDIFF(DATE_FORMAT(NOW(), "%H:%i:00"), DATE_FORMAT(StartTime_tm, "%H:%i:00"))))/60)%(TIME_TO_SEC(DATE_FORMAT(Interval_tm, "%H:%i:00"))/60) = 0, 1, 0)
					THEN 1
					ELSE 0
			END AS IntervalCome
		FROM
			simpleobjects.sire_noc_settings
		WHERE
			Active_ti = 1
		AND
			Paused_ti = 0
		AND
			CURDATE() >= DATE(StartDate_dt) <!--- Start date lte Now --->
		AND
			IF (EndDate_dt IS NOT NULL, DATEDIFF(DATE(EndDate_dt),CURDATE()) , 1) >= 0 <!--- End date gte Now --->
		AND
			TIME_TO_SEC(TIME(StartTime_tm)) - TIME_TO_SEC(TIME(NOW())) < 0
		AND
			IF (EndTime_tm IS NOT NULL, TIME_TO_SEC(TIME(EndTime_tm)) - TIME_TO_SEC(TIME(NOW())), 1) > 0
		AND
			PKId_int MOD #inpMod# = #DoModValue#
		HAVING
			SentOnce = '1'
		OR
			SentDaily = '1'
		OR
			SentWeekly = '1'
		OR
			SentMonthly = '1'
		OR
			IntervalCome = '1'
		LIMIT
			#inpNumberOfDialsToRead#
	</cfquery>

	<cfif inpVerboseDebug GT 0>
		<BR>NOC List<BR>
		<cfdump var="#NOCList#"/><BR>
	</cfif>
	<cfif NOCList.RecordCount GT 0>
		<cfloop query="NOCList">
			<cfset NOCURL = "" />
			<cfif findNoCase("?", NOCList.URL_vch) LE 0>
				<cfset NOCURL = NOCList.URL_vch & "?inpNOCId=" & NOCList.PKId_int/>
			<cfelse>
				<cfset NOCURL = NOCList.URL_vch & "&inpNOCId=" & NOCList.PKId_int/>
			</cfif>
			<cfthread action="run" NOCURL="#NOCURL#" NOCPort="#NOCList.Port_int#" NOCTimeout="#NOCList.TimeOut_int#" DBSourceEBM="#session.DBSourceEBM#" name="NOCThreadExecute#index#">
				<cfhttp url="#NOCURL#" port="#NOCPort#" timeout="#NOCTimeout#" method="get" result="NOCResult"></cfhttp>
				
				<!--- For debug --->
				<!--- <cfdump var="#NOCResult#"/> --->

				<cfif !structKeyExists(NOCResult, "status_code") OR NOCResult.status_code NEQ "200">
					<cfquery name="InsertToErrorLog" datasource="#DBSourceEBM#">
						INSERT INTO
							simplequeue.errorlogs
						(
							ErrorNumber_int,
							Created_dt,
							Subject_vch,
							Message_vch,
							TroubleShootingTips_vch,
							CatchType_vch,
							CatchMessage_vch,
							CatchDetail_vch,
							Host_vch,
							Referer_vch,
							UserAgent_vch,
							Path_vch,
							QueryString_vch
						)
						VALUES
						(
							<cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
							NOW(),
							<cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - NOC Monitor -  lastErrorDetails is Set - See Catch Detail for more info">,
							'Call NOC Error',
							'',
							'',
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#NOCURL#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#NOCResult.filecontent# #NOCResult.errordetail#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
						);
					</cfquery>
				</cfif>
			</cfthread>

			<cfset listAppend(threadList,"NOCThreadExecute#index#")>
			<cfset index++/>
		</cfloop>

		<cfif inpVerboseDebug GT 0>
			<cfthread action="join" timeout="86400000" name="#threadList#"></cfthread>
			<BR>Threads<BR>
			<cfdump var="#cfthread#"/>
		</cfif>

		<cfset dataout.RXRESULTCODE = 1/>
		<cfset dataout.MESSAGE = "NOCs executed!"/>
	</cfif>

	<cfcatch type="any">
		<!--- Log error? --->
		********** ERROR on PROCESS **********<BR>
		<cfif inpVerboseDebug GT 0>
			<cfoutput>
				<cfdump var="#cfcatch#">
			</cfoutput>
		</cfif>

		<cfset lastErrorDetails = SerializeJSON(cfcatch) />

	</cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
			INSERT INTO
				simplequeue.errorlogs
			(
				ErrorNumber_int,
				Created_dt,
				Subject_vch,
				Message_vch,
				TroubleShootingTips_vch,
				CatchType_vch,
				CatchMessage_vch,
				CatchDetail_vch,
				Host_vch,
				Referer_vch,
				UserAgent_vch,
				Path_vch,
				QueryString_vch
			)
			VALUES
			(
				<cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
				NOW(),
				<cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - NOC Monitor -  lastErrorDetails is Set - See Catch Detail for more info">,
				'',
				'',
				'',
				'',
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
			);
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>