<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpFrequency" default="1" hint="The frequency to get report is once/inpFrequency"/>
<cfparam name="inpFrequencyUnit" default="DAY"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_payment_failure_report"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 0/>
<cfset NOCName = "Report Payment Failure NOC"/>
<cfset data = {}/>
<cfset data['DATALIST'] = []/>
<cfset listaddition = ''/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>
    
    <!--- Get ems data --->     
    <cfquery name="GetEMS" datasource="#Session.DBSourceREAD#">
        SELECT 
            l.id,
            l.created,
            l.userId_int,
            p.PlanName_vch,
            up.UserPlanId_bi,
            p.PlanId_int,
            u.EmailAddress_vch,
            l.filecontent,
            l.status_text,
            l.paymentdata
        FROM 
            simplebilling.log_payment_worldpay l 
        INNER JOIN 
            simpleobjects.useraccount u
        ON
            u.UserId_int = l.userId_int
        INNER JOIN  
            simplebilling.userplans up 
        ON 
            up.UserId_int = u.UserId_int
        AND 
            up.PlanId_int > 0
        AND
            up.Status_int = 1
        INNER JOIN 
            simplebilling.plans p 
        ON 
            up.PlanId_int = p.PlanId_int
        WHERE 
            l.created >= DATE_ADD(NOW(), INTERVAL - #inpFrequency# #inpFrequencyUnit#)
            AND l.created < NOW()
        ORDER BY 
            l.id 
        DESC
    </cfquery>
    <!--- <cfdump var="#GetEMS#" abort="true"/> --->
    <cfloop query="GetEMS"> 
        <cfif isJson(GetEMS.filecontent)>
            <!--- <cfif isJson(GetEMS.filecontent) > --->
                
            <!--- <cfelse>
                <cfset aamount = 0/>
                <cfset paymentStatus = GetEMS.PaymentData_txt/>
                <cfset Reason = GetEMS.PaymentResponse_txt/>
            </cfif> --->
            <cfset AccountID = GetEMS.UserId_int/>
            <cfset Email = GetEMS.EmailAddress_vch/>

            <cfset Plan = GetEMS.PlanName_vch/>
            <cfset PlanID = GetEMS.PlanId_int/>

            <cfset paymentResposeFilecontent = deserializeJSON(GetEMS.filecontent)/>

            <cfif GetEMS.status_text EQ 'OK' AND paymentResposeFilecontent.result NEQ 'APPROVED'>
                
                
                <cfif !paymentResposeFilecontent.success>
                    <cfset paymentStatus = paymentResposeFilecontent.result/>
                    <cfset Reason = paymentResposeFilecontent.message/>
                </cfif>

                <cfif paymentResposeFilecontent.success EQ 'true'>
                    <cfset aamount = paymentResposeFilecontent.transaction.authorizedAmount/>
                <cfelse>
                    <cfset aamount = 0/>
                </cfif>
                
                <cfif isJson(GetEMS.paymentdata)>
                    <cfset paymentDataSaved = deserializeJSON(GetEMS.paymentdata)>
                    <cfset CreditCardNumber = right(paymentDataSaved.CARD.NUMBER,4)/>
                    <cfset aamount = paymentDataSaved.AMOUNT/>
                <cfelse>
                    <cfset CreditCardNumber = ''/>
                </cfif>
                <!--- </cfif> --->
                
                <cfset DueDate = GetEMS.created/>
                <cfset PaymentDate =  DateTimeFormat(GetEMS.created,'yyyy-mm-dd hh:mm:ss') />
                <cfset Amount = '$' & aamount/>
                <cfset tempItem = {
                    ID = '#GetEMS.id#',
                    ACCOUNT_ID = "#AccountID#",
                    EMAIL = "#Email#",
                    PLAN = "#Plan#",
                    PLAN_ID = "#PlanID#",
                    CREDIT_CARD_NUMBER = "#CreditCardNumber#",
                    DUE_DATE = "#DueDate#",
                    PAYMENT_DATE = "#PaymentDate#",
                    AMOUNT = "#Amount#",
                    PAYMENTSTATUS = "#paymentStatus#",
                    REASON = "#Reason#"
                }>
                <cfset ArrayAppend(data["DATALIST"],tempItem)>
                
                
            <cfelseif GetEMS.status_text NEQ 'OK'>
                <cfset paymentStatus = GetEMS.status_text/>
                <cfset Reason = paymentResposeFilecontent.message/>
                <cfset aamount = 0/>
                <cfif isJson(GetEMS.paymentdata)>
                    <cfset paymentDataSaved = deserializeJSON(GetEMS.paymentdata)>
                    <cfset CreditCardNumber = right(paymentDataSaved.CARD.NUMBER,4)/>
                    <cfset aamount = paymentDataSaved.AMOUNT/>
                <cfelse>
                    <cfset CreditCardNumber = ''/>
                </cfif>
                <!--- </cfif> --->
                
                <cfset DueDate = GetEMS.created/>
                <cfset PaymentDate =  DateTimeFormat(GetEMS.created,'yyyy-mm-dd hh:mm:ss') />
                <cfset Amount = '$' & aamount/>
                <cfset tempItem = {
                    ID = '#GetEMS.id#',
                    ACCOUNT_ID = "#AccountID#",
                    EMAIL = "#Email#",
                    PLAN = "#Plan#",
                    PLAN_ID = "#PlanID#",
                    CREDIT_CARD_NUMBER = "#CreditCardNumber#",
                    DUE_DATE = "#DueDate#",
                    PAYMENT_DATE = "#PaymentDate#",
                    AMOUNT = "#Amount#",
                    PAYMENTSTATUS = "#paymentStatus#",
                    REASON = "#Reason#"
                }>
                <cfset ArrayAppend(data["DATALIST"],tempItem)>
            </cfif>

            <!--- <cfif NOT isNull(paymentResposeFilecontent.transaction.cardNumber)>
                <cfset CreditCardNumber = right(paymentResposeFilecontent.transaction.cardNumber, 4)/>
            <cfelse> --->
            
        <cfelse>

        </cfif>
    </cfloop>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <table>
                <thead>
                    <tr>
                        <th><h3 style="text-align: left;">Account ID</h3></th>
                        <th><h3 style="text-align: left;">Email</h3></th>
                        <th><h3 style="text-align: left;">Plan</h3></th>
                        <th><h3 style="text-align: left;">Card Number</h3></th>
                        <th><h3 style="text-align: left;">Payment Date</h3></th>
                        <th><h3 style="text-align: left;">Amount($)</h3></th>
                        <th><h3 style="text-align: left;">Payment Status</h3></th>
                        <th><h3 style="text-align: left;">Reason</h3></th>
                    </tr>
                </thead>
                <tbody>
                    
                        <cfloop array="#data['DATALIST']#" index="index">
                            <tr>
                                <td>#index.ACCOUNT_ID#</td>
                                <td>#index.EMAIL#</td>
                                <td>#index.PLAN#</td>
                                <td>#index.CREDIT_CARD_NUMBER#</td>
                                <td>#index.PAYMENT_DATE#</td>
                                <td>#index.AMOUNT#</td>
                                <td>#index.PAYMENTSTATUS#</td>
                                <td>#index.REASON#</td>
                            </tr>
                        </cfloop>
                </tbody>
            </table>
        </cfoutput>
    </cfif>
       
    <cfif arrayLen(data["DATALIST"]) GT 0>
     <!--- Send email alert and logs --->
        <cfinclude template="inc_NOC_SendEmailPaymentFailure.cfm"/>
    </cfif>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="500" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>