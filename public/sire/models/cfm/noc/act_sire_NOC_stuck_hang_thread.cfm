<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpDialerIPAddr" default="act_sire_noc_stuck_hang_thread"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 1/>
<cfset NOCName = "Stuck/Hang thread NOC"/>
<cfset data = {}/>
<cfset threadError = 0/>

<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>

    <!--- Get server threads info from javamelody --->
    <cfif FINDNOCASE(CGI.SERVER_NAME,'cron.siremobile.com') GT 0 >
        <cfset apiUrl = "http://cron.siremobile.com/monitoring?part=threads&format=json" />
        <cfset username = "javamelody"/>
        <cfset password = "Yh$ob@HA!&g"/>
    <cfelse>
        <cfset apiUrl = "http://awsqa.siremobile.com/monitoring?part=threads&format=json" />
        <cfset username = "railo"/>
        <cfset password = "ebm.set@.vn"/>
    </cfif>

    <cfhttp
        url="#apiUrl#"
        username="#username#"
        password="#password#"
        result="getRequest">
    </cfhttp>

    <cfif structKeyExists(getRequest, "status_code") && getRequest.status_code EQ 200>
        <cfset requestResponse = deserializeJSON(getRequest.filecontent)/>

        <cfset threads = deserializeJSON(getRequest.filecontent).list />

        <cfif inpVerboseDebug GT 0>
            <BR>********** Thread list **********<BR>            
            <cfoutput>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>State</th>
                            <th>Runtime(s)</th>
                            <th>Deadlocked</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfloop array="#threads#" index="t">
                        <tr>
                            <td>#t.name#</td>
                            <td>#t.state#</td>
                            <td>#t.cpuTimeMillis/(1000)#</td>
                            <td>#t.deadlocked#</td>
                        </tr>
                        </cfloop>
                    </tbody>
                </table>
            </cfoutput>
        </cfif>

        <cfloop array="#threads#" index="thread">
            <cfset requestPath = '' />
            <cfloop array="#thread.stackTrace#" index="stack">
                <cfif FINDNOCASE("cf.call", stack)>
                    <cfset requestPath = stack/>
                </cfif>
            </cfloop>
            <!--- deadlocked thread  --->
            <cfif FINDNOCASE('8009-exec', thread.name) AND thread.deadlocked>
                <cfset threadError = 1/>
                <cfif checkDetails NEQ "">
                    <cfset checkDetails &= "<br/>"/>
                </cfif>
                <cfset checkDetails &= "Deadlocked thread: " & thread.name & " - Runtime: " & thread.cpuTimeMillis & " - Stacktrace: " & requestPath/>
            </cfif>
            <!--- stuck thread --->
            <cfif FINDNOCASE('8009-exec', thread.name) AND thread.cpuTimeMillis/(1000*60) GTE 10>
                <cfset threadError = 1/>
                <cfif checkDetails NEQ "">
                    <cfset checkDetails &= "<br/>"/>
                </cfif>
                <cfset checkDetails &= "Stuck thread: " & thread.name & " - Runtime: " & thread.cpuTimeMillis & " - Stacktrace: " & requestPath/>
            </cfif>
        </cfloop>

        <cfif threadError GT 0>
            <cfset checkResult = 0/>
        </cfif>
    <cfelse>
        <cfset checkResult = 0/>
        <cfset checkDetails = "Failed to get monitoring info!"/>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <br/>
            Check result: #checkResult#
            <br/>
        </cfoutput>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <BR>********** Check Details **********<BR>
        <cfoutput>
            #checkDetails#
        </cfoutput>
    </cfif>

    <!--- Send email alert and logs --->
    <cfinclude template="inc_NOC_SendEmailAndLogs.cfm"/>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
    <cfif lastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
        </cfquery>
    </cfif>

    <cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>