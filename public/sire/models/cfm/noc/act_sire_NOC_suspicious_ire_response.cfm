<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="0"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_suspicious_ire_response"/>

<cfinclude template="/public/sire/configs/paths.cfm">

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 1/>
<cfset NOCName = "Suspicious IRE response NOC"/>
<cfset data = {}/>

<cfset getLockThread = 0>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 

<cftry>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <cfexit>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>

    <!--- Look for suspicious ire response --->
    <cfquery name="CheckSuspiciousSMSReponse" datasource="#Session.DBSourceEBM#">
        SELECT
            ire.IREResultsId_bi,
            ROUND(TIME_TO_SEC(TIMEDIFF(NOW(), ire.Created_dt))/60) AS time_to_now
        FROM
            simplexresults.ireresults ire
        WHERE
            ire.Response_vch LIKE "%IRE - INTERVAL IRE - INTERVAL%"
        AND
            DATE(ire.Created_dt) = CURDATE()
        AND
            ire.IREResultsId_bi MOD #inpMod# = #DoModValue#
        HAVING
            time_to_now >= <cfqueryparam cfsqltype="cf_sql_integer" value="#inpTimePeriod#"/>
        AND
            time_to_now < <cfqueryparam cfsqltype="cf_sql_integer" value="#(2*int(inpTimePeriod))#"/>
        ORDER BY
            ire.IREResultsId_bi DESC
        LIMIT
            #inpNumberOfDialsToRead#
    </cfquery>

    <cfif inpVerboseDebug GT 0>
        ----------- Suspicious sms response
        <br/>
        <cfdump var="#CheckSuspiciousSMSReponse#" />
        <br/>
    </cfif>

    <cfif CheckSuspiciousSMSReponse.RECORDCOUNT GT 0>
        <cfset checkResult = 0/>
        <cfset checkDetails = "Suspicious IRE response ids: " & valueList(CheckSuspiciousSMSReponse.IREResultsId_bi)/>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <br/>
            Check result: #checkResult#
            <br/>
        </cfoutput>
    </cfif>

    <cfif inpVerboseDebug GT 0>
        <BR>********** Check Details **********<BR>
        <cfoutput>
            #checkDetails#
        </cfoutput>
    </cfif>

    <!--- Send email alert and logs --->
    <cfinclude template="inc_NOC_SendEmailAndLogs.cfm"/>

    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
    <cfif lastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
        </cfquery>
    </cfif>

    <cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="5" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>