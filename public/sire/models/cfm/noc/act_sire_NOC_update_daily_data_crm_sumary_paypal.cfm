<cfinclude template="../cronjob-firewall.cfm"/>

<!--- ********* NOC Update Amount Paypal transaction ***********
    1. inpDateUpdate: Date to run NOC (run 1 time only)
    2. inpNumberUpdate: Date range to run this NOC (get DateTime_dt from crm_summary table)
    3. inpStepTime: Time range to run this NOC (hours only)
--->
<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_update_daily_data_crm_summary_paypal"/>
<cfparam name="inpNumberUpdate" default="1"/>
<cfparam name="inpDateUpdate" default=""/>
<cfparam name="inpStepTime" default="3"/>


<cfinclude template="/public/sire/configs/paths.cfm"/>
<cfinclude template="/session/sire/configs/credits.cfm">

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 0/>
<cfset NOCName = "Update Data CRM Summary Paypal NOC"/>
<cfset data = {}/>

<cfset GetAllReportCountCRMNewContent = ""/>
<cfset GetPaypalTransactionList = ""/>
<cfset GetDateQuery = ""/>
<cfset Idsummary = 0/>
<cfset DateTime_dt = 0/>
<cfset Orgtotalpaypal_dbl = 0/>
<cfset totalpaypal_dbl = 0/>
<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>
<cfset transactionlist = ""/>
<cfset startDate_str = ""/>
<cfset endDate_str = ""/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 
<cftry>
    <cflock scope="SERVER" timeout="30" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
           <!---<cfexit>--->
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
    </cflock>
    <!--- get date range to update --->
    <cfquery name="GetDateQuery" datasource="#Session.DBSourceEBM#">
        SELECT 
            Idsummary,
            DateTime_dt
        FROM 
            simplexresults.crm_summary
        WHERE
        1=1
        <cfif inpDateUpdate NEQ "">
            AND DATE(DateTime_dt) = '#inpDateUpdate#'
        </cfif>
        Order by DateTime_dt DESC
        limit #inpNumberUpdate#
    </cfquery>
    <!--- Update Paypal data to new datatable --->
    <!---<cfloop from="#startDate#" to="#endDate#" index="i" step="#CreateTimeSpan(1,0,0,0)#">--->
    <cfloop query="GetDateQuery">
         <cfquery name="GetPaypalTransactionList" datasource="#Session.DBSourceEBM#">    
            SELECT 
                TranList_int,
                TransactionList_vch,
                TransactionDate_dt,
                TransactionAmount_dbl,
                RunDatetime_dt
            FROM
                simplexresults.paypal_transaction_list
            WHERE
                DATE(TransactionDate_dt) = DATE(#GetDateQuery.DateTime_dt#)
        </cfquery>
        <!--- Run NOC for second time (during the day)--->
        <cfif GetPaypalTransactionList.RECORDCOUNT GT 0>
            <!--- get org totaltransaction --->
            <cfset totalpaypal_dbl = LSParseNumber(GetPaypalTransactionList.TransactionAmount_dbl)>
            <!--- get org transaction list --->
            <cfset transactionlist = GetPaypalTransactionList.TransactionList_vch>
            <!--- get time run noc --->
            <cfset startDate = GetPaypalTransactionList.RunDatetime_dt>
            <cfset endDate = DateAdd("h", #inpStepTime#, startDate)/>
            <cfset startDate_str = "#LSDateFormat(startDate, 'yyyy-mm-dd')# #LSTimeFormat(startDate, 'HH:mm:ss')#"/>
            <cfset endDate_str = "#LSDateFormat(endDate, 'yyyy-mm-dd')# #LSTimeFormat(endDate, 'HH:mm:ss')#"/>
            <!--- Get paypal transaction list --->
            <cfhttp url="#_PaypalNVPUrl#" method="post" result="PayPalTransactionSearch">
                <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
                <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
                <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
                <cfhttpparam type="formfield" name="METHOD" value="TransactionSearch" >
                <cfhttpparam type="formfield" name="VERSION" value="94" >
                <cfhttpparam type="formfield" name="STARTDATE" value="#startDate_str#" >
                <cfhttpparam type="formfield" name="ENDDATE" value="#endDate_str#" >
            </cfhttp>
            <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                <cfinvokeargument name="QueryString" value="#URLDecode(PayPalTransactionSearch.FileContent)#">
            </cfinvoke>
            <cfloop collection="#queryStruct#" item="key">
                <cfif Find("L_TRANSACTIONID",key) NEQ 0>
                    <cfif ListFind(transactionlist, queryStruct[key]) LT 1>
                        <cfset transactionlist = ListAppend(transactionlist, queryStruct[key])>
                        <cfset key_AMT = replace(key,"L_TRANSACTIONID","L_AMT","ALL")>
                        <cfset totalpaypal_dbl = totalpaypal_dbl + queryStruct[key_AMT]>
                    </cfif>
                </cfif>
            </cfloop>
            <cfquery name="AddDataCrmSummary" datasource="#Session.DBSourceEBM#">    
                UPDATE 
                    simplexresults.crm_summary
                SET
                    Paypal_dbl = <cfqueryparam cfsqltype="CF_SQL_DOUBLE" value="#totalpaypal_dbl#">
                WHERE
                    Idsummary = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Idsummary#">
                AND 
                    DATE(DateTime_dt) = DATE(#startDate#)
            </cfquery>
            <!--- Update data to transaction list table --->
            <cftry>
                <cfquery name="UpdatePaypalTransactionList" datasource="#Session.DBSourceEBM#">   
                    UPDATE 
                        simplexresults.paypal_transaction_list
                    SET
                        TransactionList_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#transactionlist#">,
                        TransactionAmount_dbl = <cfqueryparam cfsqltype="CF_SQL_DOUBLE" value="#totalpaypal_dbl#">,
                        RunDatetime_dt = <cfqueryparam cfsqltype="cf_sql_varchar" value="#endDate_str#">
                    WHERE
                        TranList_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetPaypalTransactionList.TranList_int#">
                    AND
                        DATE(TransactionDate_dt) = DATE(RunDatetime_dt)
                </cfquery>
                <cfcatch type="any">
                    <cfoutput>
                        #cfcatch.MESSAGE#<br>
                        #cfcatch.DETAIL#<br>
                        #cfcatch.TYPE#
                    </cfoutput>
                </cfcatch>
            </cftry>
        <!--- Run NOC for first time (during the day)--->
        <cfelse>
            <cfset startDate = GetDateQuery.DateTime_dt>
            <cfset endDate = DateAdd("h", #inpStepTime#, startDate)/>
            <cfset startDate_str = "#LSDateFormat(startDate, 'yyyy-mm-dd')# #LSTimeFormat(startDate, 'HH:mm:ss')#"/>
            <cfset endDate_str = "#LSDateFormat(endDate, 'yyyy-mm-dd')# #LSTimeFormat(endDate, 'HH:mm:ss')#"/>
            <!--- Get paypal transaction list --->
                <cfhttp url="#_PaypalNVPUrl#" method="post" result="PayPalTransactionSearch">
                    <cfhttpparam type="formfield" name="USER" value="#_PaypalUserId#" >
                    <cfhttpparam type="formfield" name="PWD" value="#_PaypalPassword#" >
                    <cfhttpparam type="formfield" name="SIGNATURE" value="#_PaypalSignature#" >
                    <cfhttpparam type="formfield" name="METHOD" value="TransactionSearch" >
                    <cfhttpparam type="formfield" name="VERSION" value="94" >
                    <cfhttpparam type="formfield" name="STARTDATE" value="#startDate_str#" >
                    <cfhttpparam type="formfield" name="ENDDATE" value="#endDate_str#" >
                </cfhttp>
                <cfinvoke component="public.sire.models.cfc.common" method="QueryStringToStruct" returnvariable="queryStruct">
                    <cfinvokeargument name="QueryString" value="#URLDecode(PayPalTransactionSearch.FileContent)#">
                </cfinvoke>
                <cfloop collection="#queryStruct#" item="key">
                    <cfif Find("L_TRANSACTIONID",key) NEQ 0>
                        <cfset transactionlist = ListAppend(transactionlist, queryStruct[key])>
                    </cfif>
                    <cfif Find("L_AMT",key) NEQ 0>
                        <cfset totalpaypal_dbl = totalpaypal_dbl + queryStruct[key]>
                        <!---LSParseNumber(totalpaypal_dbl) + LSParseNumber(queryStruct[key])--->
                    </cfif>
                </cfloop>
                <cfquery name="AddDataCrmSummary" datasource="#Session.DBSourceEBM#">    
                    UPDATE 
                        simplexresults.crm_summary
                    SET
                        Paypal_dbl = <cfqueryparam cfsqltype="CF_SQL_DOUBLE" value="#totalpaypal_dbl#">
                    WHERE
                        Idsummary = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Idsummary#">
                </cfquery>
            <!--- Insert data to transaction list table --->
                <cftry>
                    <cfquery name="InsertPaypalTransactionList" datasource="#Session.DBSourceEBM#">    
                        INSERT INTO simplexresults.paypal_transaction_list
                        (        
                            TransactionList_vch,
                            TransactionDate_dt,
                            TransactionAmount_dbl,
                            RunDatetime_dt
                        )
                        VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#transactionlist#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#startDate_str#">,
                            <cfqueryparam cfsqltype="CF_SQL_DOUBLE" value="#totalpaypal_dbl#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#endDate_str#">
                        )
                    </cfquery>
                    <cfcatch type="any">
                        <cfoutput>
                            #cfcatch.MESSAGE#<br>
                            #cfcatch.DETAIL#<br>
                            #cfcatch.TYPE#
                        </cfoutput>
                    </cfcatch>
                </cftry>
        </cfif>
    </cfloop>
    <cfset checkResult = 1/> <!--- not send email --->
    <cfinclude template="inc_NOC_SendEmailAndLogs.cfm"/>
    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="3600" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>
