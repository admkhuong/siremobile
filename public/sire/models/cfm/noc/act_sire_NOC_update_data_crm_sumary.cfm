<cfinclude template="../cronjob-firewall.cfm"/>

<cfparam name="inpVerboseDebug" default="1"/>
<cfparam name="inpTimePeriod" default="15"/>
<cfparam name="inpNOCId" default="0"/>

<cfparam name="inpNumberOfDialsToRead" default = "100"/>
<cfparam name="inpMod" default="1"/>
<cfparam name="DoModValue" default="0"/>
<cfparam name="inpDialerIPAddr" default="act_sire_noc_update_data_crm_summary"/>

<cfinclude template="/public/sire/configs/paths.cfm"/>

<cfset lastErrorDetails = ''/>
<cfset checkDetails = 'OK'/>
<cfset checkResult = 0/>
<cfset NOCName = "Update Data CRM Summary NOC"/>
<cfset data = {}/>

<cfset GetAllReportCountCRMNewContent = ""/>
<cfset Idsummary = 0/>
<cfset DateTime_dt = 0/>
<cfset Uniquesignin_int = 0/>
<cfset KeyWordSupportRequest_int = 0/>
<cfset MessageSent_int = 0/>
<cfset MessageReceived_int = 0/>
<cfset TotalOptin_int = 0/>
<cfset TotalOptout_int = 0/>
<cfset BuyCreadit_int = 0/>
<cfset BuyKeyWord_int = 0/>
<cfset WordPay_int = 0/>


<cfset getLockThread = 0/>
<cfset lockThreadName = inpDialerIPAddr&'_'&inpMod&'_'&DoModValue/>

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" /> 
<cftry>
    <cflock scope="SERVER" timeout="60" TYPE="exclusive" throwontimeout="true">
        <cfif structKeyExists(APPLICATION, "#lockThreadName#")>
           <cfoutput> already run</cfoutput>
        <cfelse>
            <cfset APPLICATION['#lockThreadName#'] = 1/>
            <cfset getLockThread = 1/>
        </cfif>
        
    </cflock>

    <cfset _UserStartDate = CreateDate('2016', '06', '1')/>
    <cfset startDate = CreateDate('2016', '06', '1') />

    <cfquery name="GetDateQuery" datasource="#Session.DBSourceEBM#">
      SELECT 
            DATE_ADD(Max(DateTime_dt), INTERVAL 1 DAY) AS StartDate, 
            DATE_ADD(NOW(), INTERVAL - 1 DAY) AS EndDate
      From 
            simplexresults.crm_summary
    </cfquery>

    <cfif GetDateQuery.StartDate eq "">
         <cfset startDate = CreateDate('2016', '06', '1') />
    <cfelse>
          <cfset startDate = GetDateQuery.StartDate />
    </cfif>
    <cfset endDate = GetDateQuery.EndDate />

    <cfloop from="#startDate#" to="#endDate#" index="i" step="#CreateTimeSpan(1,0,0,0)#"> 
        <cfset DateTime_str = DateFormat(i, 'yyyy-mm-dd')/>

        <cfset DateTime_curr = {
                day: DateFormat(i, 'dd'),
                month: DateFormat(i, 'mm'),
                year : DateFormat(i, 'yyyy')
        }>
        <!--- GET INFOR --->
        <cfset Idsummary = 0/>
        <cfset DateTime_dt = 0/>
        
        <cfset BuyCreadit_int = 0/>
        <cfset BuyKeyWord_int = 0/>
        <cfset KeyWordSupportRequest_int = 0/>
        <cfset WordPay_int = 0/>
        <cfset MessageSent_int = 0/>
        <cfset MessageReceived_int = 0/>
        <cfset TotalOptin_int = 0/>
        <cfset TotalOptout_int = 0/>
        <cfset WordPayRefund_int = 0/>
        <cfset EnterpriseRevenue_dbl = 0/>

        <cfset GetAllReportCountCRMNewContent = ''>
        <cfinvoke method="GetAllReportCountCRMNew" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAllReportCountCRMNew">
            <cfinvokeargument name="inpStartDate" value="#TRIM(serializeJSON(DateTime_curr))#"/>
            <cfinvokeargument name="inpEndDate" value="#TRIM(serializeJSON(DateTime_curr))#"/>
        </cfinvoke>

        <cfset GetAllReportCountCRMNewContent = deserializeJSON(RetVarGetAllReportCountCRMNew)>

        <cfset BuyCreadit_int = GetAllReportCountCRMNewContent['BUYCREDIT']/>
        <cfset BuyKeyWord_int = GetAllReportCountCRMNewContent['BUYKEYWORD']/>
        <cfset KeyWordSupportRequest_int = GetAllReportCountCRMNewContent['KEYWORDSUPPORTREQUEST']/>
        <cfset WordPay_int = GetAllReportCountCRMNewContent['WORDPAY']/>
        <cfset MessageSent_int = GetAllReportCountCRMNewContent['MESSAGESENT']/>
        <cfset MessageReceived_int = GetAllReportCountCRMNewContent['MESSAGERECEIVED']/>
        <cfset TotalOptin_int = GetAllReportCountCRMNewContent['TOTALOPTIN']/>
        <cfset TotalOptout_int = GetAllReportCountCRMNewContent['TOTALOPTOUT']/>
        <cfset WordPayRefund_int = GetAllReportCountCRMNewContent['WORDPAYREFUND']/>
        <cfset EnterpriseRevenue_dbl = GetAllReportCountCRMNewContent['ENTERPRISEREVENUE']/>
        <cfif trim(WordPay_int) EQ "">
            <cfset  WordPay_int  = 0 />
        </cfif>

        <cftry>
            <cfquery name="AddDataCrmSummary" datasource="#Session.DBSourceEBM#">    
                INSERT INTO simplexresults.crm_summary
                (        
                    DateTime_dt,
                    BuyCreadit_int,
                    BuyKeyWord_int,
                    KeyWordSupportRequest_int,
                    WordPay_int,
                    MessageSent_int,
                    MessageReceived_int,
                    TotalOptin_int,
                    TotalOptout_int,
                    WordPayRefund_int,
                    EnterpriseRevenue_dbl
                    )
                VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateTime_str#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#BuyCreadit_int#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeyWord_int#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#KeyWordSupportRequest_int#">,
                    #WordPay_int#,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#MessageSent_int#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#MessageReceived_int#"/>,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#TotalOptin_int#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#TotalOptout_int#"/>,
                    #WordPayRefund_int#,
                    #EnterpriseRevenue_dbl#
                )
            </cfquery>
            <cfcatch type="any">
                <cfoutput>
                     #cfcatch.MESSAGE#<br>
                     #cfcatch.DETAIL#<br>
                     #cfcatch.TYPE#
                </cfoutput>
            </cfcatch>
        </cftry>
    </cfloop>
      ********** OK **********<BR>
    <cfset checkResult = 1/> <!--- not send email --->
    <!---<cfinclude template="inc_NOC_SendEmailAndLogs.cfm"/>--->
    <cfcatch type="any">
        <!--- Log error? --->
        ********** ERROR on PROCESS **********<BR>
        <cfif inpVerboseDebug GT 0>
            <cfoutput>
                <cfdump var="#cfcatch#">
            </cfoutput>
        </cfif>

        <cfset lastErrorDetails = SerializeJSON(cfcatch) />

    </cfcatch>
</cftry>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_integer" value="10111">,
                    NOW(),
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="NOC checks - #NOCName# -  lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#lastErrorDetails#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>

<cfif getLockThread EQ 1>
    <cflock scope="SERVER" timeout="3600" TYPE="exclusive">
        <cfset StructDelete(APPLICATION,lockThreadName)/>
    </cflock>
</cfif>
