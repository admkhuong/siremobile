<cfif checkResult LTE 0>
	<cfset data.NOCName = NOCName/>
	<cfset data.Details = checkDetails/>
	<!--- Send email alert --->
	<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
		<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
		<cfinvokeargument name="type" value="html"/>
		<cfinvokeargument name="subject" value="[SIRE]#NOCName#"/>
		<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_NOC_payment_failure_report.cfm"/>
		<cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
	</cfinvoke>

		<!--- <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
            <cfinvokeargument name="to" value="lee@siremobile.com">
            <cfinvokeargument name="type" value="html"/>
            <cfinvokeargument name="subject" value="#NOCName#"/>
            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_NOC_checks_report.cfm"/>
            <cfinvokeargument name="data" value="#TRIM(serializeJSON(data))#"/>
        </cfinvoke> --->
</cfif>

<cfif inpNOCId EQ 0>
	<cfquery name="GetNOCId" datasource="#Session.DBSourceEBM#">
		SELECT
			PKId_int
		FROM
			simpleobjects.sire_noc_settings
		WHERE
			Name_vch = <cfqueryparam cfsqltype="cf_sql_varchar" value="#NOCName#"/>
		LIMIT 1
	</cfquery>

	<cfif GetNOCId.RECORDCOUNT GT 0>
		<cfset inpNOCId = GetNOCId.PKId_int/>
	</cfif>
</cfif>

<cfif inpNOCId GT 0>
	<cfquery datasource="#session.DBSourceEBM#" result="UpdateNOCLaunch">
		UPDATE
			simpleobjects.sire_noc_settings
		SET
			LaunchedTime_bi = LaunchedTime_bi + 1
		WHERE
			PKId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#inpNOCId#"/>
		LIMIT 1
	</cfquery>
</cfif>

<cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />  
<cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />

<cfif checkResult LTE 0>
	<cfset checkResult = 0/>
</cfif>

<cfif !isDefined("countResult")>
	<cfset countResult = 0/>
</cfif>

<!--- Add log --->
<cfquery name="AddNOCChecksLogEntry" datasource="#Session.DBSourceEBM#">    
	INSERT INTO simplequeue.sire_noc_checks_log
	(        
		RunTimeMS_int,
		Result_int,
		Details_vch,
		StartTime_dt,
		EndTime_dt,
		NOCName_vch,
		NOCId_int,
		CountResult_int
		)
	VALUES
	(
		<cfqueryparam cfsqltype="cf_sql_integer" value="#TotalProcTime#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#checkResult#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#checkDetails#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#ServerStartTime#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#ServerEndTime#">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#NOCName#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#inpNOCId#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#countResult#"/>
	)
</cfquery>