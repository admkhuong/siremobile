<cfparam name="form.payment_method" default="0">
<cfparam name="form.line1" default="">
<cfparam name="form.plan" default="">
<cfparam name="form.city" default="">
<cfparam name="form.country" default="">
<cfparam name="form.cvv" default="">
<cfparam name="form.email" default="">
<cfparam name="form.expirationDate" default="">
<cfparam name="form.firstName" default="">
<cfparam name="form.lastName" default="">
<cfparam name="form.number" default="">
<cfparam name="form.phone" default="">
<cfparam name="form.state" default="">
<cfparam name="form.zip" default="">
<cfparam name="form.rf" default="">
<cfparam name="form.fname" default="">
<cfparam name="form.h_email" default="">
<cfparam name='processPayment' default="0">
<cfparam name='usedPromotionCode' default="0">
<cfparam name="totalAMOUNT" default="0">

<cfinclude template="/public/sire/configs/paths.cfm">

<cfset email = form.email/>
<cfif form.email EQ ''>
	<cfset email = form.h_email/>
</cfif>

<cfif (!structKeyExists(form,'number'))>
	<cfabort>
</cfif>

<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
	SELECT PlanId_int,Order_int FROM simplebilling.plans WHERE Status_int = 1 AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#form.plan#"> 
</cfquery>


<cfif checkExistPlan.RecordCount EQ 0>
	<cfabort>
</cfif>



<cfinvoke method="GetOrderPlan" component="session/sire/models/cfc/order_plan" returnvariable="getPlan">
    <cfinvokeargument name="plan" value="#form.plan#"/>
</cfinvoke>



<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan"></cfinvoke>

<cfset form.PLANNAME = getPlan.PLANNAME>
	
<cfinclude template="/session/sire/configs/credits.cfm" >

 
  <cfset getUserPromotion = ''/>
  <cfset getPromotion = ''/>
  <cfset getUserPromotion = ''/>
  <cfset addPromotionID = GetUserPlan.PROMOTIONID/>
  <cfset addPromotionLatestVersion = GetUserPlan.PROMOTIONLASTVERSIONID/>
  <cfset addPromotionKeyword = GetUserPlan.PROMOTIONKEYWORDSLIMITNUMBER/>
  <cfset addPromotionMLP = GetUserPlan.PROMOTIONMLPSLIMITNUMBER/>
  <cfset addPromotionShortURL = GetUserPlan.PROMOTIONSHORTURLSLIMITNUMBER/>
  <cfset addPromotionCredit = 0/>
	
  <cfset BUYKEYWORDNUMBER = 0 />
<!--- Check User Have Promotion --->
<cfquery name="getUserPromotion" datasource="#Session.DBSourceREAD#">
	SELECT 
		up.PromotionId_int,
		up.PromotionLastVersionId_int,
		up.UsedDate_dt,
		up.RecurringTime_int
	FROM 
		simplebilling.userpromotions up 
	WHERE
	  up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
	AND 
		up.UsedTime_int < up.RecurringTime_int
	AND up.PromotionStatus_int = 1

	LIMIT 1
</cfquery>
 

<cfset totalAMOUNT = getPlan.AMOUNT/>

<cfif getUserPromotion.RecordCount GT 0>
    <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
        SELECT
          pc.promotion_id,
          pc.origin_id,
		  pc.discount_type_group,
		  pc.discount_plan_price_percent,
		  pc.discount_plan_price_flat_rate,
          pc.promotion_keyword,
          pc.promotion_MLP,
          pc.promotion_short_URL,
          pc.promotion_credit

        FROM simplebilling.promotion_codes pc 
        WHERE
            pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPromotion.PromotionId_int#">
            AND pc.promotion_status  = 1
        LIMIT 1
    </cfquery>

    <!--- Have a Promotion --->

    <cfif getUserPromotion.RecordCount GT 0>
    	<cfset usedPromotionCode = 1/>
      <cfset addPromotionID = "#getPromotion.origin_id#">
      <cfset addPromotionLatestVersion = "#getPromotion.promotion_id#">
  	
  		<cfif getPromotion.discount_type_group EQ 1>
	
  			<cfset totalAMOUNT = (getPlan.AMOUNT - (getPlan.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
			
    	<cfelseif getPromotion.discount_type_group EQ 2>
            <cfset addPromotionKeyword = "#getPromotion.promotion_keyword#">
            <cfset addPromotionMLP = "#getPromotion.promotion_MLP#">
            <cfset addPromotionShortURL = "#getPromotion.promotion_short_URL#">
            <cfset addPromotionCredit = "#getPromotion.promotion_credit#">
      </cfif>
    </cfif>
 </cfif>

<cfif totalAMOUNT GT 0>
  <cfset processPayment = 1 >
</cfif>  

<cfset purchaseAmount = totalAMOUNT>
<cfset creditsPurchased = getPlan.FIRSTSMSINCLUDED>

<cfif processPayment EQ 0 >
	<cftransaction>
 	<cftry>
	    <cftransaction action="begin">
	  	<cfset processPayment = 1 >
	  	<!--- UPDATE UPGRADE PLAN PROCESS --->
      	<cfif getUserPromotion.RecordCount GT 0>
          <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#" result="updateUsers">
              UPDATE  
                simplebilling.billing
              SET
                Balance_int = #creditsPurchased#,
                PromotionId_int = #addPromotionID#,
                PromotionLastVersionId_int = #addPromotionLatestVersion#,
                PromotionCreditBalance_int = PromotionCreditBalance_int + #addPromotionCredit#
              WHERE 
                userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
          </cfquery>
        <cfelse>
              <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#">
                  UPDATE  
                    simplebilling.billing
                  SET   
                    Balance_int = #creditsPurchased#
                  WHERE 
                    userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
              </cfquery>
        </cfif>

        <!--- Log Billing --->
        <cfif updateUsers.RecordCount GT 0>
          <cfset getBillingTakEmo = ''>
          <cfquery name="getBillingTakEmo" datasource="#Session.DBSourceEBM#">
              SELECT 
                PromotionCreditBalance_int 
              FROM 
                simplebilling.billing
              WHERE 
                userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
          </cfquery>

          <cfset oldValue = 0>
          <cfset newValue = 0>
          <cfif getBillingTakEmo.RecordCount GT 0>
              <cfset oldValue = "#getBillingTakEmo.PromotionCreditBalance_int#">
              <cfset newValue = "#getBillingTakEmo.PromotionCreditBalance_int + addPromotionCredit#">
          </cfif>
          
          <cfinvoke method="saveLog" returnvariable="rsSaveLog" component="session.sire.models.cfc.billing">
            <cfinvokeargument name="inpUserID" value="#session.UserId#">
            <cfinvokeargument name="inpOldValue" value="#oldValue#">
            <cfinvokeargument name="inpNewValue" value="#newValue#">
            <cfinvokeargument name="inpFieldName" value="Coupon Promotion Credit">
          </cfinvoke>
        </cfif>
        <!---END Log Billing --->
	    
	    
	    <cfset todayDate = Now()>
	    <cfset startDay = Day(todayDate)>
	    
	    <cfif startDay GT 28>
	    	<cfset startDay = 28>
	    </cfif>
	    <cfset startMonth = Month(todayDate)>
	    <cfset startYear = Year(todayDate)>
	    
	    <cfset startDate = CreateDate(startYear, startMonth, startDay)> 
	    
	    <cfset endDate = DateAdd("m",1,startDate)> 

	    <!--- UPDATE ALL OTHER PLAN STATUS = 0 --->
	    <cfquery name="updateStatus" datasource="#Session.DBSourceEBM#" result="updateStatus">
	      	UPDATE simplebilling.userplans
	      	SET 
	      		Status_int = 0
	      	WHERE 
	      		userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
	      	AND 
	      		Status_int = 1
	  	</cfquery>

		<!--- MLPSLIMITNUMBER --->
		<cfif getPlan.MLPSLIMITNUMBER EQ "Unlimited">
			<cfset limitNumber = 0/>
		<cfelse>
			<cfset limitNumber = getPlan.MLPSLIMITNUMBER/>
		</cfif>                

		<!--- SHORTURLSLIMITNUMBER --->
		<cfif getPlan.SHORTURLSLIMITNUMBER EQ "Unlimited">
			<cfset shortUrl = 0/>
		<cfelse>
			<cfset shortUrl = getPlan.SHORTURLSLIMITNUMBER/>
		</cfif>
	    
	   <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
              INSERT INTO simplebilling.userplans
                (
                  Status_int,
                  UserId_int,
                  PlanId_int,
                  Amount_dec,
                  UserAccountNumber_int,
                  KeywordsLimitNumber_int,
                  FirstSMSIncluded_int,
                  PriceMsgAfter_dec,
                  PriceKeywordAfter_dec,
                  BuyKeywordNumber_int,
                  MlpsLimitNumber_int,
                  ShortUrlsLimitNumber_int,
                  MlpsImageCapacityLimit_bi,
                  StartDate_dt,
                  EndDate_dt,
                  PromotionId_int,
                  PromotionLastVersionId_int,
                  PromotionKeywordsLimitNumber_int,
                  PromotionMlpsLimitNumber_int,
                  PromotionShortUrlsLimitNumber_int
                )
                VALUES 
                (
                  1,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.PLANID#">,
                  <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.AMOUNT#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.USERACCOUNTNUMBER#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.KEYWORDSLIMITNUMBER#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.FIRSTSMSINCLUDED#">,
                  <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEMSGAFTER#">,
                  <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEKEYWORDAFTER#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeywordNumber#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#limitNumber#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#shortUrl#">,
                  <cfqueryparam cfsqltype="cf_sql_bigint" value="#getPlan.IMAGECAPACITYLIMIT#">,
                  <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startDate#">,
                  <cfqueryparam cfsqltype="CF_SQL_DATE" value="#endDate#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionID#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionLatestVersion#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionKeyword#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionMLP#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionShortURL#">
                )
            </cfquery>
		<!--- Update Promotion Credits for Inviter and send Congratulations Email --->
		<cfif form.rf NEQ ''>
			<cfinvoke method="UpdateInviterPromotionCredits" component="session.sire.models.cfc.referral" returnvariable="resultUpdateInviter">
				<cfinvokeargument name="inpUserId" value="#SESSION.UserId#">
			</cfinvoke>

			<cfif resultUpdateInviter.RXRESULTCODE GT 0>
	            <!--- Call function getuserinfo by id --->
	            <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
	                <cfinvokeargument name="inpUserId" value="#resultUpdateInviter.SenderUserId#">
	            </cfinvoke>

	            <cfinvoke  method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getInviterPlanResult">
	                <cfinvokeargument name="InpUserId" value="#resultUpdateInviter.SenderUserId#">
	            </cfinvoke>

	            <cfif getInviterPlanResult.PLANID GT 1>                         

	            	<!--- Set inputEmailData data to send email to inviter --->
	                <cfset inputEmailData = {} />
	                <cfset inputEmailData.fullName = '#resultUserInfo.USERNAME#' & ' ' & '#resultUserInfo.LASTNAME#'>
	                <cfset inputEmailData.firstName = '#resultUserInfo.USERNAME#'>
	                <cfset inputEmailData.emailAddress = '#resultUserInfo.FULLEMAIL#'>
	                <cfset inputEmailData.subject = '#_ToInviterEmailSubject#'>

	                <!--- Send Notification Email to sender when invited user signed up successful --->                        
	                <cfinvoke method="SendMailNotificationToSender" component="session.sire.models.cfc.referral">
	                    <cfinvokeargument name="inpData" value="#inputEmailData#">
	                </cfinvoke>
	            </cfif>
			</cfif>
		</cfif>



		<!--- Sendmail payment --->
		<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
	        <cfinvokeargument name="to" value="#email#">
	        <cfinvokeargument name="type" value="html">
	        <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
	        <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
	        <cfinvokeargument name="data" value="">
	    </cfinvoke>
		<!--- Add payment --->
		
	<!--- 	<cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
	        <cfinvokeargument name="planId" value="#form.plan#">
	        <cfinvokeargument name="numberKeyword" value="">
	        <cfinvokeargument name="numberSMS" value="">
	        <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
	        <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
	    </cfinvoke> --->
	    
		<!--- Add Userlog --->
		<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
	        <cfinvokeargument name="userId" value="#session.userid#">
	        <cfinvokeargument name="moduleName" value="Payment Processing">
	        <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Upgrade plan for $#purchaseAmount#">
	    </cfinvoke>

	    <!--- <!--- CREATE ACCOUNT ON SERCUNET --->
	    <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>  
	    <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
	      <cfset primaryPaymentMethodId = 0> <!--- do not print --->
	    <cfelse>  
	      <cfset primaryPaymentMethodId = 1> <!--- do not print --->
	    </cfif>
	    <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
	        <cfinvokeargument name="payment_method" value="#form.payment_method#">
	        <cfinvokeargument name="line1" value="#form.line1#">
	        <cfinvokeargument name="city" value="#form.city#">
	        <cfinvokeargument name="country" value="#form.country#">
	        <cfinvokeargument name="phone" value="#form.phone#">
	        <cfinvokeargument name="state" value="#form.state#">
	        <cfinvokeargument name="zip" value="#form.zip#">
	        <cfinvokeargument name="email" value="#form.email#">
	        <cfinvokeargument name="cvv" value="#form.cvv#">
	        <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
	        <cfinvokeargument name="firstName" value="#form.firstName#">
	        <cfinvokeargument name="lastName" value="#form.lastName#">
	    	<cfinvokeargument name="number" value="#form.number#">
	        <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
	    </cfinvoke> --->

	    <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
	        <cfinvokeargument name="InpEmailString" value="#Session.EmailAddress#">
	        <cfinvokeargument name="InpAccountType" value="#form.PLANNAME#">         
	    </cfinvoke>
	    <!--- adjust usedtime of promotion code --->
        <cfif usedPromotionCode EQ 1>
          <cfinvoke method="UpdateUserPromoCodeUsedTime" component="session.sire.models.cfc.promotion">
              <cfinvokeargument name="inpUserId" value="#SESSION.USERID#">
              <cfinvokeargument name="inpUsedFor" value="1">
          </cfinvoke>
        </cfif>
            

        <cfset dataout.ID = "1">
        <cfset dataout.MSG = "Thank You, Your payment has been processed successfully">
            
		<cftransaction action="commit">
		<cfcatch type="any">
		  <cftransaction action="rollback">
		    <cfset dataout.ID = "-1">
		    <cfset dataout.MSG = "Transaction Failed!">
		    <cfset dataout.ERRMSG =  cfcatch.message & " " & cfcatch.detail>
		</cfcatch>
      </cftry>
    </cftransaction>
    <cfoutput>#serializeJSON(dataout)#</cfoutput>
    <cfexit>

</cfif>  

<!--- <cfset purchaseAmount = totalAMOUNT> --->

<!--- CHECK IF USER DO NOT HAVE TO PAYMENT --->
<cfif processPayment EQ 0 >

</cfif>	

<cfset txtOrderPlan =  "Order Plan: "&getPlan.PLANNAME>

<cfset paymentData = {
		amount = totalAMOUNT,
		transactionDuplicateCheckIndicator = 1,
	    extendedInformation = {
	        notes = txtOrderPlan,
	    },
		developerApplication = {
			developerId: worldpayApplication.developerId,
			Version: worldpayApplication.Version
	  }
	}>

<cfif form.payment_method EQ 1>
	<cfset paymentData.check = {
			routingNumber	= form.number,
			accountNumber	= form.cvv,
			firstName		= form.firstName,
			lastName		= form.lastName,
			address 		= {
				line1 	= form.line1,
				city 	= form.city,
				state 	= form.state,
				zip 	= form.zip,
				country = form.country,
				phone	= form.phone
			},
			email 		= form.email
		}>
<cfelse>
	<cfset paymentData.card = {
			number 			= form.number,
			cvv 			= form.cvv,
			expirationDate 	= form.expirationDate,
			firstName		= form.firstName,
			lastName		= form.lastName,
			address 		= {
				line1 	= form.line1,
				city 	= form.city,
				state 	= form.state,
				zip 	= form.zip,
				country = form.country,
				phone	= form.phone
			},
			email 		= form.email

		}>
</cfif>
<cfset closeBatchesData = {
		developerApplication = {
			developerId: worldpayApplication.developerId,
			Version: worldpayApplication.Version
  		}
	}>
<cftry>

<cfhttp url="#payment_url#" method="post" result="paymentRespose" port="443">
	<cfhttpparam type="header" name="content-type" value="application/json">
	<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
	<cfhttpparam type="body" value='#TRIM( serializeJSON(paymentData) )#'>
</cfhttp>


<cfset dataout = {} />
<cfset dataout.ID = "-1">
<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">


<cfif paymentRespose.status_code EQ 200>
	<cfif trim(paymentRespose.filecontent) NEQ "" AND isJson(paymentRespose.filecontent)>

		<cfset paymentResposeContent = deserializeJSON(paymentRespose.filecontent)>
		<cfset purchaseType = '#NumberFormat(getPlan.FIRSTSMSINCLUDED)# Credit Purchase for $#NumberFormat( getPlan.AMOUNT)# USD'>
		<cfset purchaseAmount = getPlan.AMOUNT>
    	<cfset creditsPurchased = getPlan.FIRSTSMSINCLUDED>
			<cfif paymentResposeContent.success>
				<cfset dataout.ID = "1">
				<cfset dataout.MSG = "Transaction Complete. Thank you!">
				<cfset dataout.url_redirect = "device-register?redirect=#URLEncodedFormat('/session/sire/pages/my-plan')#">
				
				<!--- close session--->
				<cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
				<cfhttpparam type="header" name="content-type" value="application/json">
				<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
				<cfhttpparam type="body" value='#TRIM( serializeJSON(closeBatchesData) )#'>
				</cfhttp>

				 <!--- UPDATE UPGRADE PLAN PROCESS --->
      			<cfif getUserPromotion.RecordCount GT 0>
      		        <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#" result="updateUsers">
      		            UPDATE  
      		              simplebilling.billing
      		            SET
      		              Balance_int = #creditsPurchased#,
      		              PromotionId_int = #addPromotionID#,
      		              PromotionLastVersionId_int = #addPromotionLatestVersion#,
      		              PromotionCreditBalance_int = PromotionCreditBalance_int + #addPromotionCredit#
      		            WHERE 
      		              userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
      		        </cfquery>
      	        <cfelse>
                      <cfquery name="updateUsers" datasource="#Session.DBSourceEBM#" result="updateUsers">
                          UPDATE  
                            simplebilling.billing
                          SET   
                            Balance_int = #creditsPurchased#
                          WHERE 
                            userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                      </cfquery>
      	        </cfif>
		
                <!--- Log Billing --->
                <cfif updateUsers.RecordCount GT 0>
                  <cfset getBillingTakEmo = ''>
                  <cfquery name="getBillingTakEmo" datasource="#Session.DBSourceEBM#">
                      SELECT 
                        PromotionCreditBalance_int 
                      FROM 
                        simplebilling.billing
                      WHERE 
                        userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
                  </cfquery>

                  <cfset oldValue = 0>
                  <cfset newValue = 0>
                  <cfif getBillingTakEmo.RecordCount GT 0>
                      <cfset oldValue = "#getBillingTakEmo.PromotionCreditBalance_int#">
                      <cfset newValue = "#getBillingTakEmo.PromotionCreditBalance_int + addPromotionCredit#">
                  </cfif>
                  
                  <cfinvoke method="saveLog" returnvariable="rsSaveLog" component="session.sire.models.cfc.billing">
                    <cfinvokeargument name="inpUserID" value="#session.UserId#">
                    <cfinvokeargument name="inpOldValue" value="#oldValue#">
                    <cfinvokeargument name="inpNewValue" value="#newValue#">
                    <cfinvokeargument name="inpFieldName" value="Coupon Promotion Credit">
                  </cfinvoke>
                </cfif>
                <!---END Log Billing --->
			    
			    <cfset todayDate = Now()>
			    <cfset startDay = Day(todayDate)>
			    
			    <cfif startDay GT 28>
			    	<cfset startDay = 28>
			    </cfif>
			    <cfset startMonth = Month(todayDate)>
			    <cfset startYear = Year(todayDate)>
			    
			    <cfset startDate = CreateDate(startYear, startMonth, startDay)> 
			    
			    <cfset endDate = DateAdd("m",1,startDate)> 

			    <!--- UPDATE ALL OTHER PLAN STATUS = 0 --->
			    <cfquery name="updateStatus" datasource="#Session.DBSourceEBM#" result="updateStatus">
		          	UPDATE simplebilling.userplans
		          	SET 
		          		Status_int = 0
		          	WHERE 
		          		userid_int = <CFQUERYPARAM CFSQLTYPE="cf_sql_integer" VALUE="#session.UserId#">
		          	AND 
		          		Status_int = 1
	          	</cfquery>

				<!--- MLPSLIMITNUMBER --->
				<cfif getPlan.MLPSLIMITNUMBER EQ "Unlimited">
					<cfset limitNumber = 0/>
				<cfelse>
					<cfset limitNumber = getPlan.MLPSLIMITNUMBER/>
				</cfif>                

				<!--- SHORTURLSLIMITNUMBER --->
				<cfif getPlan.SHORTURLSLIMITNUMBER EQ "Unlimited">
					<cfset shortUrl = 0/>
				<cfelse>
					<cfset shortUrl = getPlan.SHORTURLSLIMITNUMBER/>
				</cfif>
			    
			    <cfquery name="insertUsersPlan" datasource="#Session.DBSourceEBM#" result="planadd">
                  INSERT INTO simplebilling.userplans
                    (
                      Status_int,
                      UserId_int,
                      PlanId_int,
                      Amount_dec,
                      UserAccountNumber_int,
                      KeywordsLimitNumber_int,
                      FirstSMSIncluded_int,
                      PriceMsgAfter_dec,
                      PriceKeywordAfter_dec,
                      BuyKeywordNumber_int,
                      MlpsLimitNumber_int,
                      ShortUrlsLimitNumber_int,
                      MlpsImageCapacityLimit_bi,
                      StartDate_dt,
                      EndDate_dt,
                      PromotionId_int,
                      PromotionLastVersionId_int,
                      PromotionKeywordsLimitNumber_int,
                      PromotionMlpsLimitNumber_int,
                      PromotionShortUrlsLimitNumber_int
                    )
                    VALUES 
                    (
                      1,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#Session.USERID#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.PLANID#">,
                      <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.AMOUNT#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.USERACCOUNTNUMBER#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.KEYWORDSLIMITNUMBER#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#getPlan.FIRSTSMSINCLUDED#">,
                      <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEMSGAFTER#">,
                      <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#getPlan.PRICEKEYWORDAFTER#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#BuyKeywordNumber#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#limitNumber#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#shortUrl#">,
                      <cfqueryparam cfsqltype="cf_sql_bigint" value="#getPlan.IMAGECAPACITYLIMIT#">,
                      <cfqueryparam cfsqltype="CF_SQL_DATE" value="#startDate#">,
                      <cfqueryparam cfsqltype="CF_SQL_DATE" value="#endDate#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionID#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionLatestVersion#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionKeyword#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionMLP#">,
                      <cfqueryparam cfsqltype="cf_sql_integer" value="#addPromotionShortURL#">
                    )
                </cfquery>
				<!--- Update Promotion Credits for Inviter and send Congratulations Email --->
				<cfif form.rf NEQ ''>
					<cfinvoke method="UpdateInviterPromotionCredits" component="session.sire.models.cfc.referral" returnvariable="resultUpdateInviter">
						<cfinvokeargument name="inpUserId" value="#SESSION.UserId#">
					</cfinvoke>

					<cfif resultUpdateInviter.RXRESULTCODE GT 0>
			            <!--- Call function getuserinfo by id --->
	                    <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="resultUserInfo">
	                        <cfinvokeargument name="inpUserId" value="#resultUpdateInviter.SenderUserId#">
	                    </cfinvoke>

	                    <cfinvoke  method="GetUserPlan" component="session.sire.models.cfc.order_plan" returnvariable="getInviterPlanResult">
		                    <cfinvokeargument name="InpUserId" value="#resultUpdateInviter.SenderUserId#">
		                </cfinvoke>

		                <cfif getInviterPlanResult.PLANID GT 1>                         

	                    	<!--- Set inputEmailData data to send email to inviter --->
		                    <cfset inputEmailData = {} />
		                    <cfset inputEmailData.fullName = '#resultUserInfo.USERNAME#' & ' ' & '#resultUserInfo.LASTNAME#'>
		                    <cfset inputEmailData.firstName = '#resultUserInfo.USERNAME#'>
		                    <cfset inputEmailData.emailAddress = '#resultUserInfo.FULLEMAIL#'>
		                    <cfset inputEmailData.subject = '#_ToInviterEmailSubject#'>

		                    <!--- Send Notification Email to sender when invited user signed up successful --->                        
		                    <cfinvoke method="SendMailNotificationToSender" component="session.sire.models.cfc.referral">
		                        <cfinvokeargument name="inpData" value="#inputEmailData#">
		                    </cfinvoke>
		                </cfif>
					</cfif>
				</cfif>



				<!--- Sendmail payment --->
				<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
		            <cfinvokeargument name="to" value="#email#">
		            <cfinvokeargument name="type" value="html">
		            <cfinvokeargument name="subject" value="[SIRE][Order Plan] Payment Info">
		            <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_order_plan.cfm">
		            <cfinvokeargument name="data" value="#paymentRespose.filecontent#">
		        </cfinvoke>
				<!--- Add payment --->
				
				<cfinvoke method="updatePaymentWorlDay" component="session.sire.models.cfc.order_plan">
		            <cfinvokeargument name="planId" value="#form.plan#">
		            <cfinvokeargument name="numberKeyword" value="">
		            <cfinvokeargument name="numberSMS" value="">
		            <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
		            <cfinvokeargument name="paymentRespose" value="#paymentRespose.filecontent#">
		        </cfinvoke>
		        
				<!--- Add Userlog --->
				<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
		            <cfinvokeargument name="userId" value="#session.userid#">
		            <cfinvokeargument name="moduleName" value="Payment Processing">
		            <cfinvokeargument name="operator" value="Securenet Payment Executed OK! Credits Added = #creditsPurchased# for $#purchaseAmount#">
		        </cfinvoke>

		        <!--- CREATE ACCOUNT ON SERCUNET --->
	            <cfinvoke component="session.sire.models.cfc.billing" method="RetrieveCustomer" returnvariable="RetCustomerInfo"></cfinvoke>  
	            <cfif RetCustomerInfo.RXRESULTCODE EQ 1>
	              <cfset primaryPaymentMethodId = 0> <!--- do not print --->
	            <cfelse>  
	              <cfset primaryPaymentMethodId = 1> <!--- do not print --->
	            </cfif>
	            <cfinvoke method="UpdateCustomer" component="session.sire.models.cfc.billing">
	                <cfinvokeargument name="payment_method" value="#form.payment_method#">
	                <cfinvokeargument name="line1" value="#form.line1#">
	                <cfinvokeargument name="city" value="#form.city#">
	                <cfinvokeargument name="country" value="#form.country#">
	                <cfinvokeargument name="phone" value="#form.phone#">
	                <cfinvokeargument name="state" value="#form.state#">
	                <cfinvokeargument name="zip" value="#form.zip#">
	                <cfinvokeargument name="email" value="#form.email#">
	                <cfinvokeargument name="cvv" value="#form.cvv#">
	                <cfinvokeargument name="expirationDate" value="#form.expirationDate#">
	                <cfinvokeargument name="firstName" value="#form.firstName#">
	                <cfinvokeargument name="lastName" value="#form.lastName#">
	            	<cfinvokeargument name="number" value="#form.number#">
	                <cfinvokeargument name="primaryPaymentMethodId" value="#primaryPaymentMethodId#">
	            </cfinvoke>

	            <cfinvoke method="mailChimpAPIs" component="public.sire.models.cfc.mailchimp" returnvariable="RetVarmailChimpAPIs">
                    <cfinvokeargument name="InpEmailString" value="#Session.EmailAddress#">
                    <cfinvokeargument name="InpAccountType" value="#form.PLANNAME#">         
                </cfinvoke>

	        <cfelse>    
				<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed. But, your account was registered with the free plan. Please use it to sign in!">	
			</cfif>

	<cfelse>
		   
		<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed. But, your account was registered with the free plan. Please use it to sign in!">		
	</cfif>
<cfelse>
	<cfset dataout.MSG = "We're Sorry! Unable to complete your transaction at this time. Transaction Failed. But, your account was registered with the free plan. Please use it to sign in!">		
	<!--- Log activity --->
	<cfinvoke method="createUserLog" component="session.cfc.administrator.usersLogs">
        <cfinvokeargument name="userId" value="#session.userid#">
        <cfinvokeargument name="moduleName" value="Payment Processing">
        <cfinvokeargument name="operator" value="Securenet Execute Transaction Failed!">
    </cfinvoke>
</cfif>

<!--- Log payment --->
<cftry>
	<cfinvoke method="updateLogPaymentWorlPay" component="session.sire.models.cfc.order_plan">
        <cfinvokeargument name="moduleName" value="Signup and Buy Plan">
        <cfinvokeargument name="status_code" value="#paymentRespose.status_code#">
        <cfinvokeargument name="status_text" value="#paymentRespose.status_text#">
        <cfinvokeargument name="errordetail" value="#paymentRespose.errordetail#">
        <cfinvokeargument name="filecontent" value="#paymentRespose.filecontent#">
        <cfinvokeargument name="paymentdata" value="#paymentData#">
	</cfinvoke>
<cfcatch type="any">
</cfcatch>
</cftry>


<!--- CHECK referral code to add month free --->
<!--- <cfif form.rf NEQ ''>
	<cfinvoke method="InsertMonthFree" component="session.sire.models.cfc.referral">
        <cfinvokeargument name="inpReferralCode" value="#form.rf#">
	</cfinvoke>
</cfif> --->



<cfcatch type="any">
	<cftry>
    	<!--- Log bug to file /session/sire/logs/bugs/payment  --->
		<cfif !isDefined('variables._Response')>
			<cfset variables._PageContext = GetPageContext()>
			<!--- Coldfusion --->
			<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
			<cfset variables._Response = variables._PageContext.getOut()>
		</cfif>
		<cfset variables._Response.clear()>
		
    	<cfdump var="#cfcatch#">
		<cfset catchContent = variables._Response.getString()>
		<cfset variables._Response.clear()>
    	
    	<cffile action="write" output="#catchContent#"
    	file="../../logs/bugs/payment/securenet_payment_error_#REReplace('#now()#', '\W', '_', 'all')#.txt" 
    	>
         <cfcatch type="any">
        </cfcatch>

    </cftry>
	<cfset dataout.ID="-3">
	<cfset dataout.MSG="We're Sorry! Unable to complete your transaction at this time. Transaction Failed.">
	<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
    <cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
</cfcatch>
</cftry>

<cfoutput>#serializeJSON(dataout)#</cfoutput>