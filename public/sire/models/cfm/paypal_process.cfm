<cfinclude template="/session/sire/configs/credits.cfm">

<cfparam name="url.token" default="">
<cfparam name="url.PayerID" default="">
<cfparam name="url.action" default="">
<cfparam name="url.numberSMSToSend" default="">
<cfparam name="url.numberKeywordToSend" default="">

<cfparam name="url.planid" default="">
<cfparam name="url.userid" default="">
<cfparam name="url.isyearlyplan" default="">
<cfparam name="url.promotioncode" default="">
<cfparam name="url.email" default="">
<cfparam name="url.password" default="">
<cfparam name="url.referralcode" default="">
<cfparam name="url.referraltype" default="">
<cfparam name="url.pt" default="">
<cfparam name="url.invitefriends" default="">

<cfset dataout ={}/>
<cfset dataout.RESULT = 0/>
<cfset dataout.MSG = ''/>

<cfif url.token NEQ '' AND url.PayerID NEQ ''>
	<cfinvoke component="session.sire.models.cfc.billing" method="GetExpressCheckoutDetails" returnvariable="GetExpressCheckoutDetailsResult">
		<cfinvokeargument name="inptoken" value="#url.token#">
	</cfinvoke> 
	<cfif GetExpressCheckoutDetailsResult.RXRESULTCODE EQ 1>
		<cfset getExpressToken = URLDecode(GetExpressCheckoutDetailsResult.TOKEN)>
		<cfset getExpressPayerId = URLDecode(GetExpressCheckoutDetailsResult.PAYERID)>
		<cfset getExpressAmount = URLDecode(GetExpressCheckoutDetailsResult.PAYMENTREQUEST_0_AMT) >

		<cfif action EQ 'buy_plan'>
			<cfinvoke component="session.sire.models.cfc.billing" method="DoExpressCheckoutPayment" returnvariable="DoExpressCheckoutPaymentResult">
				<cfinvokeargument name="inptoken" value="#getExpressToken#">
				<cfinvokeargument name="inpPayerId" value="#getExpressPayerId#">
				<cfinvokeargument name="inpAMT" value="#getExpressAmount#">	
				<cfinvokeargument name="inpUserId" value ="#url.userid#">
				<cfinvokeargument name="inpPlanId" value ="#url.planid#">
				<cfinvokeargument name="inpModuleName" value ="Signup and Buy Plan"/>
			</cfinvoke>
			<cfif DoExpressCheckoutPaymentResult.RXRESULTCODE EQ 1><!--- Payment success --->
				<!--- Active account --->
				<cfinvoke component="public.sire.models.cfc.userstools" method="ActiveNewAccountPaymentSuccess" returnvariable="ActiveNewAccountPaymentSuccessResult">
					<cfinvokeargument name="inpUserId" value="#url.userid#">
				</cfinvoke>
				<!--- Update info --->
				<cfinvoke component="public.sire.models.cfc.userstools" method="UpdateNewAccountNewPaypal" returnvariable="UpdateNewAccountNewPaypalResult">
					<cfinvokeargument name="inpuserId" value="#url.userid#">
					<cfinvokeargument name="inpplanId" value="#url.planid#">
					<cfinvokeargument name="inppromotioncode" value="#url.promotioncode#">
					<cfinvokeargument name="inpisyearlyplan" value="#url.isyearlyplan#">
					<cfinvokeargument name="inpemail" value ="#url.email#">
					<cfinvokeargument name="inppassword" value ="#url.password#">
					<cfinvokeargument name="inpreferralcode" value ="#url.referralcode#">
					<cfinvokeargument name="inpreferraltype" value ="#url.referraltype#">
				</cfinvoke>
				<cfif UpdateNewAccountNewPaypalResult.RXRESULTCODE EQ 1>
					<cfset dataout.RESULT = 1/>	
					<cfset dataout.MSG = 'Payment transaction successfully'/>
				<cfelse><!--- Update fail --->
					<cfinvoke component="public.sire.models.cfc.userstools" method="DeleteNewAccountNewPaypal" returnvariable="DeleteNewAccountNewPaypalResult">
						<cfinvokeargument name="inpUserId" value="#url.userid#">
					</cfinvoke>
					<cfset dataout.RESULT = -3/>	
					<cfset dataout.MSG = #UpdateNewAccountNewPaypalResult.MESSAGE#/>
				</cfif>
			<cfelse><!--- Payment fail --->
				<!--- Remove account --->
				<cfinvoke component="public.sire.models.cfc.userstools" method="DeleteNewAccountNewPaypal" returnvariable="DeleteNewAccountNewPaypalResult">
					<cfinvokeargument name="inpUserId" value="#url.userid#">
				</cfinvoke>
				<cfset dataout.RESULT = -2/>	
				<cfset dataout.MSG = 'Payment failed (ErrCode: #dataout.RESULT#)'/>
			</cfif>
		</cfif>
		
	<cfelse>
		<cfset dataout.RESULT = -1/>
		<cfset dataout.MSG = 'Payment failed (ErrCode:#dataout.RESULT#)'/>
	</cfif>
</cfif>

<cfif pt neq '' AND referralcode neq ''>
 	<cfset redirectUrl = '/thankyou?pt=' & pt & '&rf=' & referralcode & '&type=' & referraltype>
<cfelseif pt neq '' AND referralcode eq ''> 
	<cfset redirectUrl = '/thankyou?pt=' & pt>				
<cfelseif invitefriends eq "true"> 
	<cfset redirectUrl = 'thankyou?invite=true'>					 
<cfelse>
	<cfset redirectUrl = 'thankyou'>
</cfif>
<!---<cfset redirectUrl = "/plans-pricing?action=finish_payment&result="&#dataout.RESULT#&"&msg="&#urlencode(dataout.MSG)#/>--->

<cflocation url="#redirectUrl#" addtoken="false"/>

 