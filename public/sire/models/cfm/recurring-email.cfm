<cfsetting RequestTimeout="86400"/>

<cfinclude template="cronjob-firewall.cfm">

<cfparam name="form._nextPage" default="0"/>
<cfparam name="form._pageOne_now" default=""/>
<cfparam name="form.logFileNameIndex" default="0"/>

<cfset _nextPage = form._nextPage/>
<cfset _pageOne_now = form._pageOne_now/>

<cfparam name="Session.DBSourceEBM" default="bishop"/>
<cfparam name="Session.DBSourceREAD" default="bishop_read"/>

<cfinclude template="/public/sire/configs/paths.cfm">

<cfset TAB = Chr(9) /> 
<cfset NL = Chr(13) & Chr(10) />
<cfset _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
<cfset _fcm = left(_now, 7)>

<cfset logFileName = _now>
<cfset logFileNameIndex = form.logFileNameIndex>
<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring-email/#_fcm#/"/>

<cfloop condition="#_pageOne_now EQ '' AND FileExists('#dictPath##logFileName#.txt')#">
	<cfset logFileNameIndex++>
	<cfset logFileName = '#_now#-#logFileNameIndex#'>
</cfloop>
<cfif logFileNameIndex GT 0>
	<cfset _now &= '-#logFileNameIndex#'>
</cfif>

<cfif _pageOne_now EQ ''>
	<cfset _pageOne_now = now()/>
</cfif>

<cffunction name="recLogFile">
	<cfargument name="logSession" default=""/>
	<cfargument name="logText" default=""/>
	
	<cfparam name="TAB" default="#Chr(9)#"/>
	<cfparam name="NL" default="#Chr(13) & Chr(10)#"/>
	<cfparam name="_now" default="#DateFormat(now(), 'yyyy-mm-dd')#"/>
	<cfparam name="_fcm" default="#left(_now, 7)#"/>
	
	<cfset var logNow = DateFormat(now(), 'yyyy-mm-dd') & ' ' & TimeFormat(now(), 'HH:mm:ss')/>
	<cfset var filePath = "#ExpandPath('/')#session/sire/logs/recurring-email/#_fcm#/#_now#.txt"/>
	<cfset var dictPath = "#ExpandPath('/')#session/sire/logs/recurring-email/#_fcm#/"/>
	
	<cfif !DirectoryExists(dictPath)>
		<cfset DirectoryCreate(dictPath)>
	</cfif>
	
	<cftry>
	
		<cfswitch expression="#lCase(logSession)#">
			<cfcase value="root">
				<!---<cffile action = "delete" file = "#filePath#">--->
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "===== #logText#: #logNow# =====" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
			<cfcase value="main info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##Replace(logText, NL, NL&TAB, 'ALL')##NL##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
			<cfcase value="sub info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##TAB##Replace(logText, NL, NL&TAB&TAB, 'ALL')##NL##TAB##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
		</cfswitch>
		
		<cfcatch>
			<!--- Save file error --->
		</cfcatch>
	
	</cftry>	
</cffunction>

<cftry>

	<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring-email/"/>

	<cfif FileExists(dictPath & 'RecurringEmailProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringEmailProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfif _LimitRecurringProcessNumber GT recurringProcessNumber>
				<cfset recurringProcessNumber++>
				<cfset FileWrite(dictPath & 'RecurringEmailProcessNumber.txt', recurringProcessNumber)>
			<cfelse>
				Access denied<cfabort>
			</cfif>
		<cfelse>
			<cfset FileWrite(dictPath & 'RecurringEmailProcessNumber.txt', "1")>
		</cfif>
	<cfelse>
		<cfset FileWrite(dictPath & 'RecurringEmailProcessNumber.txt', "1")> 
	</cfif>


	<cfset recLogFile('Root', 'Start recurring email')>

	<cfset offset = 100*_nextPage />

	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
		SELECT UserPlanId_bi, StartDate_dt, EndDate_dt, BuyKeywordNumber_int FROM simplebilling.userplans
		WHERE Status_int = 1 AND DATEDIFF(CURDATE(), DATE(EndDate_dt)) < 4
		<cfif application.Env EQ false>
			AND UserId_int IN (850, 851, 852, 853, 476)
		</cfif>
		ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
		LIMIT <cfoutput>#offset#</cfoutput> , 100
	</cfquery>
	
	<cfset userPlanIdList = ValueList(userPlansQuery.UserPlanId_bi)>
	
	<cfset recLogFile('Main info', userPlansQuery.RecordCount & 
		' User Plans#NL#FROM simplebilling.userplans#NL#UserPlanId_bi: #userPlanIdList#')>
	
	<cfif userPlansQuery.RecordCount GT 0>
		
		<cfquery name="recurringLogQuery" result="recurringLogResult" datasource="#Session.DBSourceREAD#">
			SELECT COUNT(RecurringLogId_bi) AS ErrorNumbers_int, UserPlanId_bi, UserId_int, PlanId_int, 
				FirstName_vch, EmailAddress_vch, Amount_dec, PaymentResponse_txt
			FROM simplebilling.recurringlog
			WHERE UserPlanId_bi IN (#userPlanIdList#) AND Status_ti = 0 AND SentEmail_ti =0 AND Amount_dec > 0
			GROUP BY UserPlanId_bi HAVING ErrorNumbers_int >= 4
		</cfquery>
		
		<cfset userPlanIdList = ValueList(recurringLogQuery.UserPlanId_bi)>
		
		<cfset recLogFile('Main info', recurringLogQuery.RecordCount & 
			' User Plans#NL#FROM simplebilling.recurringlog#NL#UserPlanId_bi: #userPlanIdList#')>
	
		<cfif recurringLogQuery.RecordCount GT 0>
		
			<!---<cfset userIdList = ValueList(recurringLogQuery.UserId_int)>
			
			<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
				SELECT UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch 
				FROM simpleobjects.useraccount
				WHERE UserId_int IN (#userIdList#) AND Active_int = 1
				ORDER BY UserId_int DESC
			</cfquery>
			
			<cfset recLogFile('Main info', usersQuery.RecordCount & 
				' User Accounts#NL#FROM simpleobjects.useraccount#NL#UserId_int: #userIdList#')>--->
			
			<cfset planIdList = ValueList(recurringLogQuery.PlanId_int)>

			<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
				SELECT PlanId_int, PlanName_vch, Amount_dec, PriceKeywordAfter_dec FROM simplebilling.plans
				WHERE PlanId_int IN (#planIdList#) AND Status_int = 1
				ORDER BY PlanId_int DESC
			</cfquery>
			
			<cfset planIdList = ValueList(plansQuery.PlanId_int)>
	
			<cfset recLogFile('Main info', plansQuery.RecordCount & 
				' Plans#NL#FROM simplebilling.plans#NL#PlanId_int: #planIdList#')>
			
			<cfset threadIndex = 1>
			<cfset threadList = ''>

			<cfinclude template="/public/paths.cfm">
			<cfinclude template="/public/sire/models/cfc/sendmail.cfc">

			<cfloop query="recurringLogQuery">
				<cfthread action="run" name="t#threadIndex#" threadIndex="#threadIndex#">
					<cfset textLog = ''>
					
					<cfset planIndex = plansQuery.PlanId_int.IndexOf(JavaCast('int', recurringLogQuery.PlanId_int[threadIndex])) + 1>
					<cfset userPlanIndex = userPlansQuery.UserPlanId_bi.IndexOf(JavaCast('int', recurringLogQuery.UserPlanId_bi[threadIndex])) + 1>
					
					<cfif planIndex GT 0>
						
						<cfparam name="cardNumber" default="">

						<cfset _endDate = DateAdd("m", 1, userPlansQuery.EndDate_dt[userPlanIndex])>
						<cfset _amount = plansQuery.Amount_dec[planIndex] + (userPlansQuery.BuyKeywordNumber_int[userPlanIndex] * plansQuery.PriceKeywordAfter_dec[planIndex])>

						<cfif isJson(recurringLogQuery.PaymentResponse_txt[threadIndex])>
							<cfset paymentResponse = DeserializeJSON(recurringLogQuery.PaymentResponse_txt[threadIndex])>
							<cfif paymentResponse.status_code EQ 200>
								<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
									<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
									<cfset cardNumber = paymentResponseContent.transaction.cardNumber>
								</cfif>
							</cfif>
						</cfif>
					
						<cfset textLog = 
							'UserPlanId_bi: ' & recurringLogQuery.UserPlanId_bi[threadIndex] &
							'#NL#User Id: ' & recurringLogQuery.UserId_int[threadIndex] &
							'#NL#Plan Id: ' & recurringLogQuery.PlanId_int[threadIndex] &
							'#NL#Payment amount: $' & _amount
							>
						
						<cfset tk = ToBase64(DateFormat(_endDate, 'yyyy-mm-dd-') & recurringLogQuery.UserId_int[threadIndex])>

						<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
							<cfset sire_root_url = 'https://siremobile.com'>	
						<cfelse>
							<cfset sire_root_url = rootUrl>	
						</cfif>

						<cfset dataMail = {
							ErrorNumbers_int	= recurringLogQuery.ErrorNumbers_int[threadIndex], 
							<!---UserPlanId_bi			= recurringLogQuery.UserPlanId_bi[threadIndex],---> 
							UserId_int				= recurringLogQuery.UserId_int[threadIndex], 
							FirstName_vch			= recurringLogQuery.FirstName_vch[threadIndex], 
							EmailAddress_vch		= recurringLogQuery.EmailAddress_vch[threadIndex], 
							cardNumber				= cardNumber,
							PlanId_int				= recurringLogQuery.PlanId_int[threadIndex],
							PlanName_vch			= plansQuery.PlanName_vch[planIndex],
							StartDate_dt			= userPlansQuery.EndDate_dt[userPlanIndex],
							EndDate_dt				= _endDate,
							Amount_dec				= plansQuery.Amount_dec[planIndex],
							BuyKeywordNumber_int	= userPlansQuery.BuyKeywordNumber_int[userPlanIndex],
							PriceKeywordAfter_dec	= plansQuery.PriceKeywordAfter_dec[planIndex],
							EXTENDLINK				= sire_root_url & '/extend-plan?tk=' & tk,
							ROOTURL					= rootUrl
						}>

		        		<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
				            <cfinvokeargument name="to" value="#dataMail.EmailAddress_vch#">
				            <cfinvokeargument name="type" value="html">
				            <cfinvokeargument name="subject" value="Sire can not make a payment monthly fee">
				            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_fail.cfm">
				            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
			    		</cfinvoke>

			    	<cfelse>
			    	
						<cfset textLog = 
							'UserPlanId_bi: ' & recurringLogQuery.UserPlanId_bi[threadIndex] &
							'#NL#No Plan Id: ' & recurringLogQuery.PlanId_int[threadIndex]
							>
					</cfif>
					
					<cfset recLogFile('Main info', textLog)>
				</cfthread>

				<cfset listAppend(threadList,"t#threadIndex#")>
				<cfset threadIndex++>
			</cfloop>

			<cfthread action="join" timeout="86400000" name="#threadList#"></cfthread>
			
			<cfquery name="recurringLogQuery" result="recurringLogResult" datasource="#Session.DBSourceEBM#">
				UPDATE simplebilling.recurringlog SET SentEmail_ti = 1
				WHERE UserPlanId_bi IN (#userPlanIdList#) AND Status_ti = 0 AND Amount_dec > 0
			</cfquery>
			
		</cfif>
		
	</cfif>

	<cfset recLogFile('Root', 'End recurring email')>

	<cfif FileExists(dictPath & 'RecurringEmailProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringEmailProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfset recurringProcessNumber-->
		<cfelse>
			<cfset recurringProcessNumber = 0>
		</cfif>
		<cfset FileWrite(dictPath & 'RecurringEmailProcessNumber.txt', recurringProcessNumber)>
	</cfif>

	<cfif userPlansQuery.RecordCount EQ 100>
		<cfset _nextPage = _nextPage + 1>
		<cfhttp url="http://#CGI.server_name#/public/sire/models/cfm/recurring-email.cfm" method="post" result="nextPage" port="80" timeout="1">
			<cfhttpparam type="formField" name="_nextPage" value="#_nextPage#">
			<cfhttpparam type="formField" name="_pageOne_now" value="#_pageOne_now#">
			<cfhttpparam type="formField" name="logFileNameIndex" value="#logFileNameIndex#">
			<cfhttpparam type="cookie" name="cftoken" value="#session.CFToken#">
			<cfhttpparam type="cookie" name="cfid" value="#session.cfid#">
		</cfhttp>
	</cfif>

	<cfcatch>
		<cfset recLogFile('Main info', 'Application error' & NL & serializeJSON(cfcatch))>
	</cfcatch>

</cftry>



