<cfsetting RequestTimeout="86400"/>

<cfinclude template="cronjob-firewall.cfm">

<cfparam name="form._nextPage" default="0"/>
<cfparam name="form._pageOne_now" default=""/>
<cfparam name="form.logFileNameIndex" default="0"/>
<cfparam name="form.logDataStartDate_dt" default=""/>
<cfparam name="form.logDataAccountNumber_bi" default="0"/>
<cfparam name="form.logDataErrorNumber_bi" default="0"/>

<cfset _nextPage = form._nextPage/>
<cfset _pageOne_now = form._pageOne_now/>
<cfset logDataStartDate_dt = form.logDataStartDate_dt/>
<cfset logDataAccountNumber_bi = form.logDataAccountNumber_bi/>
<cfset logDataErrorNumber_bi = form.logDataErrorNumber_bi/>

<cfparam name="Session.DBSourceEBM" default="bishop"/>
<cfparam name="Session.DBSourceREAD" default="bishop_read"/>

<cfinclude template="/public/paths.cfm">
<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="/public/sire/models/cfc/sendmail.cfc">
<cfinclude template="/public/sire/models/cfc/order_plan.cfc">
<cfinclude template="/public/sire/models/cfc/referral.cfc">
<cfinclude template="/public/sire/models/cfc/mailchimp.cfc">
<cfinclude template="/session/cfc/csc/constants.cfm">

<cfset TAB = Chr(9) /> 
<cfset NL = Chr(13) & Chr(10) />
<cfset _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
<cfset _fcm = left(_now, 7)>

<cfset logFileName = _now>
<cfset logFileNameIndex = form.logFileNameIndex>
<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring-integrate/#_fcm#/"/>

<cfloop condition="#_pageOne_now EQ '' AND FileExists('#dictPath##logFileName#.txt')#">
	<cfset logFileNameIndex++>
	<cfset logFileName = '#_now#-#logFileNameIndex#'>
</cfloop>
<cfif logFileNameIndex GT 0>
	<cfset _now &= '-#logFileNameIndex#'>
</cfif>

<cfif _pageOne_now EQ ''>
	<cfset _pageOne_now = now()/>
</cfif>

<!--- simplebilling.recurringlogs --->
<cfset logData = {
	RecurringLogId_int	= 0,
	StartDate_dt		= (logDataStartDate_dt EQ '' ? now() : logDataStartDate_dt),
	Status_ti			= 1,
	EndDate_dt			= javaCast( "null", 0 ),
	AccountNumber_bi	= logDataAccountNumber_bi,
	ErrorNumber_bi		= logDataErrorNumber_bi,
	LogFile_vch			= '#_fcm#/#_now#.txt'
}>

<cffunction name="recLogFile" hint="log to file">
	<cfargument name="logSession" default=""/>
	<cfargument name="logText" default=""/>
	
	<cfparam name="TAB" default="#Chr(9)#"/>
	<cfparam name="NL" default="#Chr(13) & Chr(10)#"/>
	<cfparam name="_now" default="#DateFormat(now(), 'yyyy-mm-dd')#"/>
	<cfparam name="_fcm" default="#left(_now, 7)#"/>
	
	<cfset var logNow = DateFormat(now(), 'yyyy-mm-dd') & ' ' & TimeFormat(now(), 'HH:mm:ss')/>
	<cfset var filePath = "#ExpandPath('/')#session/sire/logs/recurring-integrate/#_fcm#/#_now#.txt"/>
	<cfset var dictPath = "#ExpandPath('/')#session/sire/logs/recurring-integrate/#_fcm#/"/>
	
	<cfif !DirectoryExists(dictPath)>
		<cfset DirectoryCreate(dictPath)>
	</cfif>
	
	<cftry>
	
		<cfswitch expression="#lCase(logSession)#">
			<cfcase value="root">
				<!---<cffile action = "delete" file = "#filePath#">--->
				<cffile  action = "append" 
					file = "#filePath#" 
					output = "===== #logText#: #logNow# =====" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
			<cfcase value="main info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##Replace(logText, NL, NL&TAB, 'ALL')##NL##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
			<cfcase value="sub info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##TAB##Replace(logText, NL, NL&TAB&TAB, 'ALL')##NL##TAB##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
		</cfswitch>
		
		<cfcatch>
			<!--- Save file error --->
		</cfcatch>
	
	</cftry>	
</cffunction>


<cftry>	

	<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring-integrate/"/>
	<!--- check to make sure only one recurring process running --->
	<cfif FileExists(dictPath & 'RecurringProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfif _LimitRecurringProcessNumber GT recurringProcessNumber>
				<cfset recurringProcessNumber++>
				<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', recurringProcessNumber)>
			<cfelse>
				Access denied<cfabort>
			</cfif>
		<cfelse>
			<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', "1")>
		</cfif>
	<cfelse>
		<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', "1")> 
	</cfif>
	
	<cfset recLogFile('Root', 'Start recurring')>
	<cfset updatingUserPlanList = ''>
	<cfif FileExists(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "\r\n|\n\r|\n|\r", ",", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "\s+", "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, ",+", ",", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "^,", "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, ",$", "", "all")>
	<cfelse>
		<cfset updatingUserPlanList = ''>
	</cfif>

	<!--- Get all userplan need to run. Except runing on other threead --->
	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
		SELECT 
			UserPlanId_bi,Status_int,UserId_int,PlanId_int,BillingType_ti,Amount_dec,UserAccountNumber_int,Controls_int,KeywordsLimitNumber_int,KeywordMinCharLimit_int,FirstSMSIncluded_int,
			UsedSMSIncluded_int,CreditsAddAmount_int,PriceMsgAfter_dec,PriceKeywordAfter_dec,StartDate_dt,EndDate_dt,MonthlyAddBenefitDate_dt,MlpsLimitNumber_int,ShortUrlsLimitNumber_int,
			MlpsImageCapacityLimit_bi,BuyKeywordNumber_int,BuyKeywordNumberExpired_int,DowngradeDate_dt,PromotionId_int,PromotionKeywordsLimitNumber_int,PromotionMlpsLimitNumber_int, 
			PromotionShortUrlsLimitNumber_int,PromotionLastVersionId_int,UserDowngradeDate_dt,UserDowngradePlan_int,UserDowngradeKeyword_txt,UserDowngradeMLP_txt,UserDowngradeShortUrl_txt,
			NumOfRecurringKeyword_int,NumOfRecurringPlan_int, DATEDIFF(CURDATE(),MonthlyAddBenefitDate_dt) as expireKeywordDateDiff, DATEDIFF(CURDATE(),EndDate_dt) as expirePlanDateDiff 
		FROM 
			simplebilling.userplans
		WHERE 
			Status_int = 1
		AND
			NumOfRecurringPlan_int<4
		<cfif updatingUserPlanList NEQ ''>
			AND UserPlanId_bi NOT IN (#updatingUserPlanList#)
		</cfif>
			AND  DATE(EndDate_dt) <= CURDATE() 
		<cfif application.Env EQ false>
			 
		</cfif>	
		AND
			PlanId_int IN (
				SELECT 	PlanId_int
				FROM	simplebilling.plans
				WHERE	
					Status_int in (4)
			)
		ORDER BY 
			EndDate_dt DESC, UserPlanId_bi DESC
		LIMIT 100
	</cfquery>
	
	
	<cfset userPlanIdList = ValueList(userPlansQuery.UserPlanId_bi)>

	<!---<cfset recLogFile('Main info', userPlansResult.SQL)>--->
	<cfset recLogFile('Main info', userPlansQuery.RecordCount & ' User Plans#NL#FROM simplebilling.userplans#NL#UserPlanId_bi: #userPlanIdList#')>
	
	<cfif userPlansQuery.RecordCount GT 0>

		<cfset logData.AccountNumber_bi	+= userPlansQuery.RecordCount>

		<cffile  
			action = "append" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#userPlanIdList#" 
			addNewLine = "yes" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes"/>

		<cfset userIdList = ValueList(userPlansQuery.UserId_int)>
		
		<!--- get all user info --->
		<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				ua.UserId_int, ua.FirstName_vch, ua.LastName_vch, ua.EmailAddress_vch, ua.HomePhoneStr_vch , ua.YearlyRenew_ti,
				ua.UserLevel_int,
				uhigher.EmailAddress_vch as HigherUserEmail, 
				uhigher.FirstName_vch as HigherUserFirstName,
				uhigher.LastName_vch as HigherUserLastName,
				uhigher.UserId_int as HigherUserId
			FROM 
				simpleobjects.useraccount ua
			LEFT JOIN 
				simpleobjects.useraccount uhigher
				ON	ua.HigherUserID_int= uhigher.UserId_int
			WHERE 
				ua.UserId_int IN (#userIdList#) AND ua.Active_int = 1			
			ORDER BY 
				ua.UserId_int DESC
		</cfquery>
		
		<cfset recLogFile('Main info', usersQuery.RecordCount & 
			' User Accounts#NL#FROM simpleobjects.useraccount#NL#UserId_int: #userIdList#')>
		
		<!--- get all active plan info --->
		<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				PlanId_int, PlanName_vch, Amount_dec, YearlyAmount_dec, UserAccountNumber_int, Controls_int, KeywordsLimitNumber_int, KeywordMinCharLimit_int,
				FirstSMSIncluded_int, CreditsAddAmount_int, PriceMsgAfter_dec, PriceKeywordAfter_dec, ByMonthNumber_int, MlpsLimitNumber_int, ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi, Order_int
			FROM 
				simplebilling.plans
			WHERE 
				Status_int in (4)
			ORDER BY 
				PlanId_int DESC
		</cfquery>
		
		<cfset planIdList = ValueList(plansQuery.PlanId_int)>

		<cfset recLogFile('Main info', plansQuery.RecordCount & 
			' Plans#NL#FROM simplebilling.plans#NL#PlanId_int: #planIdList#')>
		
		<!--- get user authen with sercurenet for auto run payment --->
		<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				a.id, a.PaymentMethodID_vch, a.PaymentType_vch, ua.UserId_int, a.UserId_int as CCBelong,
				a.PaymentGateway_ti
			FROM 
				simplebilling.authorize_user a
			LEFT JOIN 
				simpleobjects.useraccount ua
				ON	a.UserId_int= ua.UserId_int
                OR 	(
					a.UserId_int= ua.HigherUserID_int AND ua.UserLevel_int=3
				)
			WHERE                 
				ua.UserType_int IN(2)
			AND
				ua.UserLevel_int IN (2,3)
			AND
				a.PaymentGateway_ti = (
					Select  PaymentGateway_ti
					FROM 	simpleobjects.useraccount
					WHERE 	(UserId_int = ua.HigherUserID_int AND ua.UserLevel_int=3)
					OR		(UserId_int = ua.UserId_int AND ua.UserLevel_int=2)
				)
			ORDER BY 
				a.id DESC			
		</cfquery>
		
		<cfset recLogFile('Main info', customersQuery.RecordCount & 
			' Customers In Securenet#NL#FROM simplebilling.authorize_user#NL#UserId_int: #userIdList#')>
		
		<cfset userIdsNeedUpdate = []>
	
		<cfinclude template="/session/sire/configs/credits.cfm">
		
		<cfset closeBatchesData = {
			developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.version
			}
		}>
	
		<cfset oldUserPlans = []>
		<cfset newUserPlans = []>
		<cfset addPromotionCreditBalance = []>
		<cfset promocodeListToIncreaseUsedTime = []>
		<cfset promocodeListToIncreaseUsedTimeTemp = []>
		<cfset userListToSendAlertCCDeclineToAdmin = []>
		<cfset errorUserPlans = []>
		<cfset newBalanceBillings = []>
		<cfset logPaymentWorldpays = [[],[],[]]>
		<cfset paymentErrorsPlans = {}>

		<cfset ___c = 0>
		<cfset threadIndex = 1>
		<cfset threadList = ''>
		<cfset emailsend = ''/>		
		<cfset _ccBelong= "">
		<cfset _paymentGateWay = 0/>
		
		<cfloop query="userPlansQuery">
			
			<cfset threadId = threadIndex/>  <!--- need remove when using thread--->
			
			<!--- <cfthread action="run" name="t#threadIndex#" threadId="#threadIndex#"> --->
			
				<cfset textLog = ''>
				
				<cfset planIndex = Variables.plansQuery.PlanId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.PlanId_int[threadId])) + 1>
				<cfset userIndex = Variables.usersQuery.UserId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.UserId_int[threadId])) + 1>
				<cfset customerIndex = Variables.customersQuery.UserId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.UserId_int[threadId])) + 1>				

				<cfset Variables.paymentErrorsPlans[threadId] = 0/>
				<cfset Variables.isDowngrade[threadId] = 0/>
				<cfset Variables.isRemoveCouponBenefit[threadId] = 0/>
				<cfset promocodeListToIncreaseUsedTimeTemp = []/>
				<cfset Variables.paymentErrorsMonthly[threadId] = 1/>
				
				<cfif userIndex GT 0 AND planIndex GT 0 AND arrayFind(userIdsNeedUpdate, Variables.userPlansQuery.UserId_int[threadId]) LE 0>
					<cfset arrayAppend(Variables.userIdsNeedUpdate, Variables.userPlansQuery.UserId_int[threadId])>
					<cfset _paymentGateWay = Variables.customersQuery.PaymentGateway_ti[customerIndex]>
					<cfset _paymentType = Variables.customersQuery.PaymentType_vch[customerIndex]>
					<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
					<cfset _totalkeywordHaveToPaid = 0/>

					<!--- User Downgrade info --->
					<cfset _userDowngradePlan = Variables.userPlansQuery.UserDowngradePlan_int[threadId]/>
					
					<cfset _downgradePlanName = ''/>
					<cfset _downgradePlanCredits = 0/>
					<cfset _downgradePlanKeywords = 0 />

					<!--- Promotion benefit --->
					<cfset _userPromotionKeyword = Variables.userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset _userPromotionMLP = Variables.userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset _userPromotionURL = Variables.userPlansQuery.PromotionShortUrlsLimitNumber_int[threadId]/>

					<cfset percent_dis = 0>
					<cfset flatrate_dis = 0>
					<cfset promotion_credit = 0>
					<cfset promotion_mlp = 0>
					<cfset promotion_keyword = 0>
					<cfset promotion_shorturl = 0>
					<cfset userPromotionId = 0>
					<cfset promotion_id = 0>
					<cfset promotion_origin_id = 0>

					<cfset _ccBelong= Variables.customersQuery.CCBelong[customerIndex]>
					<cfset _userID= Variables.usersQuery.UserId_int[userIndex]>
					<cfset _userBillingType = Variables.userPlansQuery.BillingType_ti[threadId]/>					
					<cfset _numOfRecurringPlan = Variables.userPlansQuery.NumOfRecurringPlan_int[threadId]/> 
					<cfset _expirePlanDateDiff = Variables.userPlansQuery.expirePlanDateDiff[threadId]/> 
					<cfset _priceMesAfter =Variables.userPlansQuery.PriceMsgAfter_dec[threadId]>

					<cfset _planStartDate= Variables.userPlansQuery.StartDate_dt[threadId]>
					<cfset _planEndDate= Variables.userPlansQuery.EndDate_dt[threadId]>

					<cfset _monthAmount = 0 /> <!--- use when try to charge monthly of yearly plan --->

					<cfset _amount = 0 > <!--- total amount have to paid --->
					<!--- get total message sent for calc amount--->
					<cfquery name="TotalSentReceived" datasource="#Session.DBSourceREAD#">
						SELECT                        
							SUM(IF(ire.IREType_int = 1, 1, 0)) AS Sent,
							SUM(IF(ire.IREType_int = 2, 1, 0)) AS Received
						FROM
							simplexresults.ireresults as ire 
									
						WHERE
							LENGTH(ire.ContactString_vch) < 14
						AND 
							ire.UserId_int  = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#_userID#" >	
						              
						AND
							ire.Created_dt
						BETWEEN 
							<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planStartDate#">
						AND 
							
							DATE_ADD(<CFQUERYPARAM CFSQLTYPE="CF_SQL_DATE" VALUE="#_planEndDate#">, INTERVAL 86399 SECOND)
						GROUP BY
							ire.UserId_int     
					</cfquery>
					
					
					<cfset _numberCreditUsing=0>					
					<cfif TotalSentReceived.RecordCount GT 0>											
						<cfset _numberCreditUsing= LSParseNumber(TotalSentReceived.Sent) + LSParseNumber(TotalSentReceived.Received)>					
						<cfset _amount= _numberCreditUsing * _priceMesAfter / 100>	
					<cfelse>
						
					</cfif>		
					<!--- add total kw buy for amount--->
					<cfset _amount= _amount+ Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.userPlansQuery.PriceKeywordAfter_dec[threadId]>
					<cfset _amount =NumberFormat(_amount,'.99')>
								
					<cfset _makePayment = 0 > <!--- check if user need to run payment --->
					<!---_makePayment 0 : do not run payment but still add new plan --->
					<!---_makePayment 1 : run payment --->
					<!---_makePayment 2 : do not run payment but do add new plan --->



					<cfset _recurringType = 0 > 
					<!---_recurringType 0 : renew monthly/yearly --->
					<!---_recurringType 1 : add benefit monthly --->
					<!---_recurringType 2 : payment for buy keyword monthly --->	

					<cfinvoke component="public.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
	                  <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
	                </cfinvoke>
					

					<!--- Check CouponForUser --->
					<cfif CouponForUser.rxresultcode EQ 1>
						<cfloop array="#CouponForUser['DATALIST']#" index="index">
							<cfif index.COUPONDISCOUNTTYPE EQ 1>
								<cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
								<cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								<!--- <cfset promotion_used_time = index.USEDTIME> --->
								<cfset userPromotionId = index.userPromotionId>
							<cfelseif index.COUPONDISCOUNTTYPE EQ 2>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								<!--- <cfset promotion_used_time = index.USEDTIME> --->
								<cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
								<cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
								<cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
								<cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
								<cfset userPromotionId = index.userPromotionId>
							<cfelse>
								<cfset promotion_id = index.PROMOTIONID>
								<cfset promotion_origin_id = index.ORIGINID>
								<cfset userPromotionId = index.userPromotionId>
								<!--- <cfset promotion_used_time = index.USEDTIME> --->
							</cfif>

							<cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, Variables.userPlansQuery.UserId_int[threadId],userPromotionId])/>
						</cfloop>
					</cfif>

					<cfif _numOfRecurringPlan LT 3 AND _amount GT 0>
						<cfset _makePayment = 1>						
					<cfelse>
						<cfset _makePayment = 2>	
					</cfif>


					<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
					<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
			            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
			        </cfinvoke>				
					<cfset keywordUsed = getUserPlan.KEYWORDUSE/>
					<cfset shorturlUsed = getUserPlan.SHORTURLUSED/>
			       	<cfset mlpUsed = getUserPlan.MLPUSED/>
					
					

					<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
					<cfif ( _userPromotionKeyword GT 0 AND promotion_keyword LT _userPromotionKeyword ) 
						  OR ( _userPromotionMLP GT 0 AND promotion_mlp LT _userPromotionMLP )  
						  OR ( _userPromotionURL GT 0 AND promotion_shorturl < _userPromotionURL) >
						  <cfset Variables.isRemoveCouponBenefit[threadId] = 1/>
					</cfif>	  
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						'#NL#User Id: ' & Variables.userPlansQuery.UserId_int[threadId] &
						'#NL#Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] &
						
						'#NL#Payment amount: $' & _amount &
						
						'#NL#make payment: ' & _makePayment &
						'#NL#billing type: ' & _userBillingType &
						'#NL#reccuring type: ' & _recurringType &
						
						'#NL#nums of recurring plan: ' & _numOfRecurringPlan
						>
					
					<cfif getUserPlan.PLANEXPIRED EQ 1> <!--- only check if plan expriry within 30 days--->						
						<cfif _makePayment EQ 1>							
							<cfif customerIndex GT 0 > <!--- If find user have token in securenet ---> 
								<cfset paymentError = 1>
								<cfset paymentData = {}>
								<cfif _paymentGateWay EQ _WORLDPAY_GATEWAY >
									<cfset paymentData = {  
										amount = _amount,
										paymentVaultToken = {  
											customerId = Variables.customersQuery.CCBelong[customerIndex],
											paymentMethodId = Variables.customersQuery.PaymentMethodID_vch[customerIndex],
											paymentType = _paymentType
										},
										developerApplication = {  
											developerId = worldpayApplication.developerId,
											version = worldpayApplication.version
										}
									}>

									<!--- log payment data --->
									<cfset textLog &= NL & TAB & SerializeJSON(paymentData)>
									
									<cfset paymentError = 1>
									<cfset paymentResponse = {
										status_code = '',
										status_text = '',
										errordetail = '',
										filecontent = ''
									}>

									<cftry>
										<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
											<cfhttpparam type="header" name="content-type" value="application/json">
											<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
											<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
										</cfhttp>
										<cfcatch>
											<cfset textLog &= NL & TAB & 'Payment error: ' & payment_url & NL & TAB & SerializeJSON(cfcatch)>
										</cfcatch>
									</cftry>
										
									<!--- log payment data --->
									<cfset textLog &= NL & TAB & SerializeJSON(paymentResponse)>
									
									<cfif paymentResponse.status_code EQ 200>
										<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
											<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
											<cfset paymentResponseContent.numberOfCredit = _numberCreditUsing/>
											<cfset paymentResponseContent.numberOfKeyword = Variables.userPlansQuery.BuyKeywordNumber_int[threadId]/>
											<cfset paymentResponseContent.ROOTURL = rootUrl/>
											<cfset paymentResponseContent.PlanName = Variables.plansQuery.PlanName_vch[planIndex]/>
											<cfset paymentResponseContent.SubAccId = Variables.userPlansQuery.UserId_int[threadId]/>
											<cfset paymentResponseContent.MasterId = Variables.usersQuery.HigherUserId[userIndex]/>
											<cfset paymentResponseContent.MailToMaster = 1/>
											

											<!--- log payment data --->
											<cfset textLog &= NL & TAB & paymentResponse.filecontent>
																																
											<cfif Variables.usersQuery.UserLevel_int[userIndex] EQ 2>
												<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]/>
												<cfset paymentResponseContent.MailToMaster = 0/>
												<cfset subject ="[SIRE][Recurring]Your Account Recurring completed">											
											<cfelse>
												<cfset emailsend = Variables.usersQuery.HigherUserEmail[userIndex]/>
												<cfset paymentResponseContent.MailToMaster = 1/>
												<cfset subject ="[SIRE][Recurring]Your Sub Account Recurring completed">
											</cfif>
											
											<cfif paymentResponseContent.success>																						
												<cfset paymentError = 0>	
																																
											<cfelse> <!---PAYMENT SERCUNET ERROR --->
												<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
												<cfset Variables.paymentErrorsPlans[threadId] = 1/>
											</cfif>
										<cfelse> <!---PAYMENT SERCUNET ERROR --->
											<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
											<cfset Variables.paymentErrorsPlans[threadId] = 1/>
										</cfif>
									<cfelse> <!---PAYMENT SERCUNET ERROR --->
										<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
										<cfset Variables.paymentErrorsPlans[threadId] = 1/>
									</cfif>
									
									<!--- <cfif arrayFind(errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId]) GT 0> --->
									<!--- LOG PAYMENT WORDPAY SUCESS --->
									<cfset arrayAppend(Variables.logPaymentWorldpays[1], [
										Variables.userPlansQuery.UserId_int[threadId],
										Variables.userPlansQuery.PlanId_int[threadId],
										'Recurring',
										Variables.userPlansQuery.BuyKeywordNumber_int[threadId],
										0,
										paymentResponseContent,
										_ccBelong
									])>
									<!--- LOG ALL PAYMENT WORDPAY --->
									<cfset arrayAppend(Variables.logPaymentWorldpays[2], [
										Variables.userPlansQuery.UserId_int[threadId],
										'Recurring',
										paymentResponse.status_code,
										paymentResponse.status_text,
										paymentResponse.errordetail,
										paymentResponse.filecontent,
										paymentData,
										_ccBelong
									])>
								
								<cfelseif _paymentGateWay EQ _PAYPAL_GATEWAY OR _paymentGateWay EQ _MOJO_GATEWAY OR _paymentGateWay EQ _TOTALAPPS_GATEWAY>
									<cfset paymentData = {  
										amount = _amount,
										inpVaultId = Variables.customersQuery.PaymentMethodID_vch[customerIndex]
									}>
									<!--- log payment data --->
									<cfset textLog &= NL & TAB & SerializeJSON(paymentData)>
									
									<cfset paymentError = 1>
									<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>
									<cfset sleep(10000)>
									<cfif _paymentGateWay EQ _PAYPAL_GATEWAY>
										<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
											<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
											<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
											<cfinvokeargument name="inpModuleName" value="Recurring"/>
										</cfinvoke>		
									<cfelseif _paymentGateWay EQ _MOJO_GATEWAY>
										<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
											<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
											<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
											<cfinvokeargument name="inpModuleName" value="Recurring"/>
										</cfinvoke>	
									<cfelseif _paymentGateWay EQ _TOTALAPPS_GATEWAY>
										<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
											<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
											<cfinvokeargument name="inpUserId" value="#_ccBelong#">									
											<cfinvokeargument name="inpModuleName" value="Recurring"/>
										</cfinvoke>	
									</cfif>						
									
																	
									<cfset paymentResponse.filecontent = rtSaleWithTokenzine.FILECONTENT>
									
									<cfif rtSaleWithTokenzine.RXRESULTCODE EQ 1>
										<cfset paymentError = 0>										

										<cfset transactionInfo = {
											cardHolder_FirstName: rtSaleWithTokenzine.REPORT.INPFIRSTNAME,
											cardHolder_LastName: rtSaleWithTokenzine.REPORT.INPLASTNAME,
											authorizedAmount: rtSaleWithTokenzine.REPORT.AMT,
											email:'',
											billAddress:{
												line1: rtSaleWithTokenzine.REPORT.INPLINE1,
												city: rtSaleWithTokenzine.REPORT.INPCITY,
												state: rtSaleWithTokenzine.REPORT.INPSTATE,
												zip: rtSaleWithTokenzine.REPORT.INPPOSTALCODE,
												country: '', 
												phone: rtSaleWithTokenzine.REPORT.PHONE												

											}

										}>
										<cfset paymentResponseContent.transaction=transactionInfo>
										<cfset paymentResponseContent.numberOfCredit = _numberCreditUsing/>
										<cfset paymentResponseContent.numberOfKeyword = Variables.userPlansQuery.BuyKeywordNumber_int[threadId]/>
										<cfset paymentResponseContent.ROOTURL = rootUrl/>
										<cfset paymentResponseContent.PlanName = Variables.plansQuery.PlanName_vch[planIndex]/>
										<cfset paymentResponseContent.SubAccId = Variables.userPlansQuery.UserId_int[threadId]/>
										<cfset paymentResponseContent.MasterId = Variables.usersQuery.HigherUserId[userIndex]/>
										<cfset paymentResponseContent.MailToMaster = 1/>	
										
																															
										<cfif Variables.usersQuery.UserLevel_int[userIndex] EQ 2>
											<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]/>
											<cfset paymentResponseContent.MailToMaster = 0/>
											<cfset subject ="[SIRE][Recurring]Your Account Recurring completed">											
										<cfelse>
											<cfset emailsend = Variables.usersQuery.HigherUserEmail[userIndex]/>
											<cfset paymentResponseContent.MailToMaster = 1/>
											<cfset subject ="[SIRE][Recurring]Your Sub Account Recurring completed">
										</cfif>
										<!--- log payment success only--->
										<cfinvoke component="session.sire.models.cfc.billing" method="UpdatePayment" returnvariable="rtUpdatePayment">
											<cfinvokeargument name="inpPaymentData" value="#SerializeJSON(rtSaleWithTokenzine.REPORT)#"/>
											<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
											<cfinvokeargument name="moduleName" value="Recurring"/>											
											<cfinvokeargument name="inpPlanId" value="#Variables.userPlansQuery.PlanId_int[threadId]#">	
											<cfinvokeargument name="inpPaymentByUserId" value="#_ccBelong#"/>	
											<cfinvokeargument name="inpPaymentGateway" value="#_paymentGateWay#"/>				
										</cfinvoke>	
										
									<cfelse> <!---PAYMENT PAYPAL ERROR --->
										<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
										<cfset Variables.paymentErrorsPlans[threadId] = 1/>										
									</cfif>		
								</cfif>
								
								<cfif paymentError EQ 0>									
									
									<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
										<cfinvokeargument name="to" value="#emailsend#">
										<cfinvokeargument name="type" value="html">
										<cfinvokeargument name="subject" value="#subject#">
										<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_integrate_user_completed.cfm">
										<cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
									</cfinvoke>
									
									<!--- Set plan enddate --->
									<cfif _userBillingType EQ 1>
										<cfset _endDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
									<cfelse>	
										<cfset _endDate = DateAdd("yyyy", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
									</cfif>	
									<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>										
									
									<!--- if renew plan -> need to remove old plan --->
									<cfif _recurringType EQ 0>
										<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
									</cfif>	

																				
									
									<cfif _recurringType EQ 0>
										<cfset arrayAppend(Variables.newUserPlans,[
											1,
											Variables.userPlansQuery.UserId_int[threadId],
											Variables.userPlansQuery.PlanId_int[threadId],
											//Variables.plansQuery.Amount_dec[planIndex],
											(
												_userBillingType EQ 1 ? Variables.plansQuery.Amount_dec[planIndex] : Variables.plansQuery.YearlyAmount_dec[planIndex]*12
											),
											Variables.plansQuery.UserAccountNumber_int[planIndex],
											Variables.plansQuery.Controls_int[planIndex],
											Variables.userPlansQuery.KeywordsLimitNumber_int[threadId],
											Variables.userPlansQuery.FirstSMSIncluded_int[threadId],
											(
												Variables.plansQuery.Amount_dec[planIndex] GT 0 ?
												0 : 
												Variables.userPlansQuery.UsedSMSIncluded_int[threadId]
											),
											Variables.plansQuery.CreditsAddAmount_int[planIndex],
											Variables.userPlansQuery.PriceMsgAfter_dec[threadId],
											Variables.plansQuery.PriceKeywordAfter_dec[planIndex],
											Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] ,
											Variables.userPlansQuery.EndDate_dt[threadId],
											_endDate,
											Variables.userPlansQuery.MlpsLimitNumber_int[threadId],
											Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId],
											Variables.userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
											promotion_origin_id,
											promotion_id,
											promotion_keyword,
											promotion_mlp,
											promotion_shorturl,
											_userBillingType,
											_monthlyDate
										])>

										<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
										<cfif Variables.isRemoveCouponBenefit[threadId] EQ 1 >
											<!--- RELEASE USER DATA --->
											<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
												<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
												<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
												<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
												<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
												<cfinvokeargument name="inpLimitKeyword" value="#Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]+Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
												<cfinvokeargument name="inpLimitShortUrl" value="#Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
												<cfinvokeargument name="inpLimitMLP" value="#Variables.userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
											</cfinvoke>
										</cfif>
									</cfif>

									<!--- RESET CREDIT TO LASTEST USER PLAN --->
									<cfset arrayAppend(Variables.newBalanceBillings, [
										Variables.userPlansQuery.UserId_int[threadId], 
										Variables.userPlansQuery.FirstSMSIncluded_int[threadId], 
									])>
									
									<cfif promotion_credit GT 0>
										<!--- ADD PROMOTION CREDIT BALANCE --->
										<cfset arrayAppend(Variables.addPromotionCreditBalance, [
											Variables.userPlansQuery.UserId_int[threadId],
											promotion_credit
										])>
									</cfif>

									<!--- count promotion code used --->
									<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
										<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
									</cfloop>

									<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
									<cfinvoke method="AddReferralCredits" component="public.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
										<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									</cfinvoke>

									
								</cfif>
								<!--- LOG RECURRING --->
								<cfset arrayAppend(Variables.logPaymentWorldpays[3], [
									now(),
									Variables.userPlansQuery.UserPlanId_bi[threadId],
									Variables.userPlansQuery.UserId_int[threadId],
									Variables.userPlansQuery.PlanId_int[threadId],
									Variables.usersQuery.FirstName_vch[userIndex],
									Variables.usersQuery.EmailAddress_vch[userIndex],
									_amount,
									SerializeJSON(paymentData),
									paymentError,
									SerializeJSON(paymentResponse),
									_paymentGateWay,
									_ccBelong
								])>
							<cfelse>
								<cfset Variables.paymentErrorsPlans[threadId] = 1/>
								<!--- If no customer in Sercurnet --->
								<cfset textLog &= NL & TAB & 'No Customer In Securenet'>
								<!--- <cfset arrayAppend(Variables.oldUserPlans,[Variables.userPlansQuery.UserPlanId_bi[threadId], -2])> --->
								<cfset arrayAppend(Variables.errorUserPlans,Variables.userPlansQuery.UserPlanId_bi[threadId])>

								<!--- LOG RECURRING --->
								<cfset arrayAppend(Variables.logPaymentWorldpays[3], [
									now(),
									Variables.userPlansQuery.UserPlanId_bi[threadId],
									Variables.userPlansQuery.UserId_int[threadId],
									Variables.userPlansQuery.PlanId_int[threadId],
									Variables.usersQuery.FirstName_vch[userIndex],
									Variables.usersQuery.EmailAddress_vch[userIndex],
									_amount,
									"Account Not Found",
									'1',
									"No data in simplebilling.authorize_user",
									_paymentGateWay,
									_ccBelong
								])>

							</cfif>
														

							<!--- IF PAYMENT NOT SUCCESS --->
							<cfif Variables.paymentErrorsPlans[threadId] EQ 1 >
								
								<cfset declineDataContent = {} >								
								<cfset declineDataContent.HIGHERUSERID = Variables.usersQuery.HigherUserId[userIndex] >
								<cfset declineDataContent.USERID = Variables.userPlansQuery.UserId_int[threadId] >
								<cfset declineDataContent.PLANSNAME = Variables.plansQuery.PlanName_vch[planIndex]/>
								<cfset declineDataContent.PLANSTARTDATE = Variables.userPlansQuery.StartDate_dt[threadId] >
								<cfset declineDataContent.PLANENDDATE = Variables.userPlansQuery.EndDate_dt[threadId] >
								<cfset declineDataContent.PLANKEYWORD = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId] >
								<cfset declineDataContent.PROMOKEYWORD = Variables.userPlansQuery.PromotionKeywordsLimitNumber_int[threadId] >
								<cfset declineDataContent.PURCHASEDKEYWORD = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] >
								<cfset declineDataContent.ACTIVEKEYWORD = keywordUsed >
								<cfset declineDataContent.AVAILABLEKEYWORD = declineDataContent.PLANKEYWORD + declineDataContent.PROMOKEYWORD + declineDataContent.PURCHASEDKEYWORD - declineDataContent.ACTIVEKEYWORD>
								<cfset declineDataContent.MailToMaster = 1/>
								
								<cfset declineEmailsend = Variables.usersQuery.HigherUserEmail[userIndex]>
								
								<cfset declinePlanTime = Variables.userPlansQuery.NumOfRecurringPlan_int[threadId]>
								<cfset declineKeywordTime = Variables.userPlansQuery.NumOfRecurringKeyword_int[threadId]>							
								

								<cfif _recurringType EQ 0>																		
									<cfif Variables.paymentErrorsMonthly[threadId] EQ 1>
										<cfif Variables.userPlansQuery.NumOfRecurringPlan_int[threadId] EQ 0> <!--- send for 1st time --->
											<cfif Variables.usersQuery.UserLevel_int[userIndex] EQ 2>
												<cfset declineEmailsend = Variables.usersQuery.EmailAddress_vch[userIndex]>
												<cfset declineDataContent.MailToMaster = 0/>
												<cfset subject ="Do not make a payment for your account">
											<cfelse>
												<cfset declineEmailsend = Variables.usersQuery.HigherUserEmail[userIndex]>
												<cfset declineDataContent.MailToMaster = 1/>
												<cfset subject ="Do not make a payment for your sub account">
											</cfif>
											<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
												<cfinvokeargument name="to" value="#declineEmailsend#">
												<cfinvokeargument name="type" value="html">
												<cfinvokeargument name="subject" value="#subject#">
												<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_integrate_user_decline.cfm">
												<cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
											</cfinvoke>	
										<cfelse> <!--- send for the next  --->
											<cfset dataMail = {
												SubAccId=  Variables.userPlansQuery.UserId_int[threadId], 
												HIGHERUSERID				= Variables.usersQuery.HigherUserId[userIndex] , 												
												PlanName_vch			= Variables.plansQuery.PlanName_vch[planIndex],
												StartDate_dt			= Variables.userPlansQuery.StartDate_dt[threadId],
												EndDate_dt				= Variables.userPlansQuery.EndDate_dt[threadId],
												DownGradeDate_dt		= DateAdd("d", 3, Variables.userPlansQuery.EndDate_dt[threadId]),
												MailToMaster=1
											}>
											<cfif Variables.usersQuery.UserLevel_int[userIndex] EQ 2>
												<cfset declineEmailsend = Variables.usersQuery.EmailAddress_vch[userIndex]>
												<cfset dataMail.MailToMaster=0>
												<cfset subject ="Sire can not make a payment for your account">
											<cfelse>
												<cfset declineEmailsend = Variables.usersQuery.HigherUserEmail[userIndex]>
												<cfset dataMail.MailToMaster=1>
												<cfset subject ="Sire can not make a payment for your sub account">
											</cfif>
											<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
												<cfinvokeargument name="to" value="#declineEmailsend#">
												<cfinvokeargument name="type" value="html">
												<cfinvokeargument name="subject" value="#subject#">
												<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_fail_integrate_user.cfm">
												<cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
											</cfinvoke>
										</cfif>
									</cfif>
										
								</cfif>

								<cfif declinePlanTime EQ 2>
									<cfset declinedAmount = _amount/>
									<cfset tempCustomerInfo = {
										ID = "#Variables.userPlansQuery.UserId_int[threadId]#",
										EMAIL = "#declineEmailsend#",
										NAME = "#Variables.usersQuery.FirstName_vch[userIndex]# #Variables.usersQuery.LastName_vch[userIndex]#",
										PLAN = "#declineDataContent.PLANSNAME#",
										KEYWORD = "0",
										AMOUNT = "#declinedAmount#"
									}/>

									<cfset arrayAppend(userListToSendAlertCCDeclineToAdmin, tempCustomerInfo)/>
								</cfif>

								<!--- Update NumOfRecurring --->
								<cfinvoke method="UpdateNumberOfRecurring" component="public.sire.models.cfc.order_plan">
									<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
									<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
									<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
								</cfinvoke>

							</cfif>
						<cfelse> <!--- --->
							
							<cfif _userBillingType EQ 1>
					        	<cfset _endDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
					        <cfelse>	
					        	<cfset _endDate = DateAdd("yyyy", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
					        </cfif>
					        
							<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>

						    

							<!--- RESET CREDIT TO LASTEST USER PLAN --->							
							<cfif _numOfRecurringPlan EQ 3>
								
								<!--- send mail reset credit to 0--->
								
								<cfset dataMail = {
									MasterId = usersQuery.HigherUserId[userIndex],
									SubAccId=  Variables.userPlansQuery.UserId_int[threadId], 
									USERID				= Variables.userPlansQuery.UserId_int[threadId], 
									PlanName_vch			= Variables.plansQuery.PlanName_vch[planIndex],
									StartDate_dt			= Variables.userPlansQuery.StartDate_dt[threadId],
									EndDate_dt				= Variables.userPlansQuery.EndDate_dt[threadId],
									DownGradeDate_dt		= DateAdd("d", 30, Variables.userPlansQuery.EndDate_dt[threadId]),
									MailToMaster=1
								}>
								<cfif Variables.usersQuery.UserLevel_int[userIndex] EQ 2>
									<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]>	
									<cfset dataMail.MailToMaster=0>																					
									<cfset subject ="Sire reset your account credit to zero">
								<cfelse>
									<cfset emailsend = Variables.usersQuery.HigherUserEmail[userIndex]>		
									<cfset dataMail.MailToMaster=1>																				
									<cfset subject ="Sire reset your sub account credit to zero">
								</cfif>
								<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
									<cfinvokeargument name="to" value="#emailsend#">
									<cfinvokeargument name="type" value="html">
									<cfinvokeargument name="subject" value="#subject#">
									<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_reset_credit_to_zero_integrate_user.cfm">
									<cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
								</cfinvoke>
								<!--- reset credit to 0--->
								<cfset arrayAppend(Variables.newBalanceBillings, [
									Variables.userPlansQuery.UserId_int[threadId], 
									0
								])>
							<cfelse>
								<cfif _recurringType EQ 0>
									<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>						    

									<cfset arrayAppend(Variables.newUserPlans,[
										1,
										Variables.userPlansQuery.UserId_int[threadId],
										Variables.userPlansQuery.PlanId_int[threadId],
										// Variables.plansQuery.Amount_dec[planIndex],
										(
											_userBillingType EQ 1 ? Variables.plansQuery.Amount_dec[planIndex] : Variables.plansQuery.YearlyAmount_dec[planIndex]*12
										),
										Variables.plansQuery.UserAccountNumber_int[planIndex],
										Variables.plansQuery.Controls_int[planIndex],
										Variables.userPlansQuery.KeywordsLimitNumber_int[threadId],
										Variables.userPlansQuery.FirstSMSIncluded_int[threadId],
										Variables.userPlansQuery.UsedSMSIncluded_int[threadId],
										Variables.plansQuery.CreditsAddAmount_int[planIndex],
										Variables.plansQuery.PriceMsgAfter_dec[planIndex],
										Variables.plansQuery.PriceKeywordAfter_dec[planIndex],
										Variables.userPlansQuery.BuyKeywordNumber_int[threadId],
										Variables.userPlansQuery.EndDate_dt[threadId],
										_endDate,
										Variables.userPlansQuery.MlpsLimitNumber_int[threadId],
										Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId],
										Variables.userPlansQuery.MlpsImageCapacityLimit_bi[threadId],
										promotion_origin_id,
										promotion_id,
										promotion_keyword,
										promotion_mlp,
										promotion_shorturl,
										_userBillingType,
										_monthlyDate
									])>

									<cfif promotion_credit GT 0>
										<!--- ADD PROMOTION CREDIT BALANCE --->
										<cfset arrayAppend(Variables.addPromotionCreditBalance, [
										Variables.userPlansQuery.UserId_int[threadId],
										promotion_credit
										])>
									</cfif>

									<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
										<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
									</cfloop>

									<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
										<cfif Variables.isRemoveCouponBenefit[threadId] EQ 1>
										<!--- RELEASE USER DATA --->
										<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
											<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
											<cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
											<cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
											<cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
											<cfinvokeargument name="inpLimitKeyword" value="#Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]+Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
											<cfinvokeargument name="inpLimitShortUrl" value="#Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
											<cfinvokeargument name="inpLimitMLP" value="#Variables.userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
										</cfinvoke>
									</cfif>	
								
								</cfif> 
								<cfset arrayAppend(Variables.newBalanceBillings, [
									Variables.userPlansQuery.UserId_int[threadId], 
									Variables.userPlansQuery.FirstSMSIncluded_int[threadId]
								])>
							</cfif>
							<!--- Update NumOfRecurring --->
							<cfinvoke method="UpdateNumberOfRecurring" component="public.sire.models.cfc.order_plan">
								<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
								<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
								<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
							</cfinvoke>

							<!--- RESET USER REFERRAL CREDITS --->
						    <cfinvoke method="ResetUserReferral" component="public.sire.models.cfc.referral">
						        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
						    </cfinvoke>
						</cfif>
					</cfif>

				<cfelseif userIndex GT 0 AND planIndex GT 0>
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						'#NL#User Id: ' & Variables.userPlansQuery.UserId_int[threadId] &
						'#NL#Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] &
						'#NL#Duplicate plan'
						>
					<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
				<cfelse>
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						(userIndex LE 0 ? '#NL#No User Id: ' & Variables.userPlansQuery.UserId_int[threadId] : '') &
						(planIndex LE 0 ? '#NL#No Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] : '')
						>
					<cfif userIndex LE 0>
						<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
					<cfelse>
						<cfset arrayAppend(Variables.oldUserPlans, [Variables.userPlansQuery.UserPlanId_bi[threadId], -1])>
					</cfif>
					<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
				</cfif>
				

				<cfset recLogFile('Main info', textLog)>

	            <cfset ___c++>
	            <cffile
                    action = "write"
                    file = "#dictPath#UserPlanCount.txt"
                    output = "#___c#"
                    addNewLine = "no"
                    attributes = "normal"
                    charset = "utf-8"
                    fixnewline = "yes"/>
			<!--- </cfthread> --->
		
			<cfset listAppend(threadList,"t#threadIndex#")>
			<cfset threadIndex++>
		</cfloop>
		
		<!--- <cfthread action="join" timeout="86400000" name="#threadList#"></cfthread> --->
		
		<cfif !arrayIsEmpty(newUserPlans)>
			<!---  Other Discount --->
			<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				INSERT INTO simplebilling.userplans (
					Status_int,
					UserId_int,
					PlanId_int,
					Amount_dec,
					UserAccountNumber_int,
					Controls_int,
					KeywordsLimitNumber_int,
					FirstSMSIncluded_int,
					UsedSMSIncluded_int,
					CreditsAddAmount_int,
					PriceMsgAfter_dec,
					PriceKeywordAfter_dec,
					BuyKeywordNumber_int,
					StartDate_dt,
					EndDate_dt,
					MlpsLimitNumber_int,
					ShortUrlsLimitNumber_int,
					MlpsImageCapacityLimit_bi,
					PromotionId_int,
					PromotionLastVersionId_int,
					PromotionKeywordsLimitNumber_int,
					PromotionMlpsLimitNumber_int,
					PromotionShortUrlsLimitNumber_int,
					BillingType_ti,
					MonthlyAddBenefitDate_dt,
					BuyKeywordNumberExpired_int,
					DowngradeDate_dt
				) VALUES
				<cfset _comma = ''> 
				<cfloop array="#newUserPlans#" index="_plan">
					<cfif  ArrayLen(_plan) LT 26>
						<cfset _plan[26] = 0 >
						<cfset _plan[27] = '' >
					<cfelseif ArrayLen(_plan) LT 27>
						<cfset _plan[27] = '' >
					</cfif>
					#_comma#(
						<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[3]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[4]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[5]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[6]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[7]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[8]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[9]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[10]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[11]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[12]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[13]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[14]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[15]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[16]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[17]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[18]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[19]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[20]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[21]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[22]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[23]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[24]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[25]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[26]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[27]#" null="#NOT len(trim(_plan[27]))#">
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>

			<!--- userPlansResult.generatedKey --->
			<cfset recLogFile('Main info', 
				userPlansResult.RecordCount & ' User Plans Extended' &
				'#NL#User Plans: ' & userPlansResult.GENERATED_KEY
				)>
		</cfif>
				
		<cfif !arrayIsEmpty(newBalanceBillings)>
			<cfset userIdList = ''>
			<cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
				UPDATE simplebilling.billing 
					SET Balance_int = (CASE
						<cfloop array="#newBalanceBillings#" index="_balance">
							<cfset userIdList = listAppend(userIdList,_balance[1])>
							WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#_balance[1]#"> 
								THEN <cfqueryparam cfsqltype="cf_sql_decimal" value="#_balance[2]#">
						</cfloop>
						ELSE 0
					END)
				WHERE UserId_int IN (#userIdList#)
			</cfquery>
			<cfset recLogFile('Main info', 
				arrayLen(newBalanceBillings) & ' Balance Billing Updated' &
				'#NL#UserIds: ' & userIdList
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(addPromotionCreditBalance)>
			<cfset userIdListPromotion = ''>
			<cfquery name="addPromoCreditBalance" datasource="#Session.DBSourceEBM#" result="addResult">
				UPDATE 
					simplebilling.billing
				SET
					PromotionCreditBalance_int = PromotionCreditBalance_int + (CASE
						<cfloop array="#addPromotionCreditBalance#" index="index">
						<cfset userIdListPromotion = listAppend(userIdListPromotion,index[1])>
						WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[1]#"/>
							THEN <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#index[2]#"/>
						</cfloop>
					END)
				WHERE 
					UserId_int IN (#userIdListPromotion#)
			</cfquery>
		</cfif>

		<cfif !arrayIsEmpty(promocodeListToIncreaseUsedTime)>
			<cfloop array="#promocodeListToIncreaseUsedTime#" index="index">
				<!--- Increase coupon code used time --->
                <cfinvoke method="IncreaseCouponUsedTime" component="public.sire.models.cfc.promotion">
                    <cfinvokeargument name="inpCouponId" value="#index[1]#"/>
                    <cfinvokeargument name="inpUsedFor" value="1"/>
                </cfinvoke>
                <!--- End increase coupon code used time --->

                <!--- Insert coupon code used log --->
                <cfinvoke method="InsertPromotionCodeUsedLog" component="public.sire.models.cfc.promotion">
                    <cfinvokeargument name="inpCouponId" value="#index[2]#"/>
                    <cfinvokeargument name="inpOriginCouponId" value="#index[1]#"/>
                    <cfinvokeargument name="inpUserId" value="#index[3]#"/>
                    <cfinvokeargument name="inpUsedFor" value="Recurring"/>
                </cfinvoke>    
                <!--- End insert log --->

                <cfquery name="updateUsedTimePromoCode" datasource="#Session.DBSourceEBM#" result="updateResult">
                  UPDATE 
                  	simplebilling.userpromotions
                  SET
                    UsedTimeByRecurring_int = UsedTimeByRecurring_int + 1
                  WHERE 
                  	UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[3]#">
                  AND 
                  	PromotionLastVersionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[2]#"> 
                  AND 
                  	UserPromotionId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#index[4]#">
                </cfquery>
			</cfloop>

			<!--- <cfset recLogFile('Main info', 
				arrayLen(promocodeListToIncreaseUsedTime) & ' Promotion Code Used Time' &
				'#NL#Promotion Code Used Time: ' & serializeJSON(promocodeListToIncreaseUsedTime)
				)> --->
		</cfif>


		
		<cfif !arrayIsEmpty(oldUserPlans)>
			<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				UPDATE simplebilling.userplans 
					SET Status_int = (CASE
						<cfset i = 1>
						<cfloop array="#oldUserPlans#" index="_plan">
							<cfif isArray(_plan)>
								WHEN UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#"> 
									THEN <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">
								<cfset oldUserPlans[i] = _plan[1]>
							</cfif>
							<cfset i++>
						</cfloop>
						WHEN 0 = 1 THEN Status_int ELSE 0
					END) 
				WHERE UserPlanId_bi IN (#arrayToList(oldUserPlans, ',')#)
			</cfquery>
			<cfset recLogFile('Main info', 
				arrayLen(oldUserPlans) & ' User Plans Expired' &
				'#NL#User Plans: ' & arrayToList(oldUserPlans, ',')
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(errorUserPlans)>
			<cfset logData.ErrorNumber_bi += arrayLen(errorUserPlans)>
			<cfset recLogFile('Main info', 
				arrayLen(errorUserPlans) & ' User Plans Error' &
				'#NL#User Plans: ' & arrayToList(errorUserPlans, ',')
				)>
		</cfif>
		<cfif !arrayIsEmpty(userListToSendAlertCCDeclineToAdmin)>
			<cfset datalistacc = {}/>
			<cfset datalistacc.RESULT = 1/>
			<cfset datalistacc['DATALIST'] = userListToSendAlertCCDeclineToAdmin/>

			<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail" returnvariable="retvalsendmail">
				<cfinvokeargument name="to" value="#SupportEMailUserName#"/>
				<cfinvokeargument name="type" value="html"/>
				<cfinvokeargument name="subject" value="[SIRE] Customer's credit card declined 3 times"/>
				<cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_alert_cc_declined_for_admin.cfm"/>
				<cfinvokeargument name="data" value="#TRIM(serializeJSON(datalistacc))#"/>
			</cfinvoke>
		</cfif>
		
        <!--- Log payment worldpay response --->
        <cfinvoke method="logPaymentWorldPay" component="public.sire.models.cfc.order_plan" returnvariable="aaaaa">
            <cfinvokeargument name="inplogPaymentWorldpays" value="#logPaymentWorldpays#"/>
            <cfinvokeargument name="inpFcm" value="#_fcm#"/>
            <cfinvokeargument name="inpNow" value="#_now#"/>
        </cfinvoke>    
		
		<cftry>
		    <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
		        <cfhttpparam type="header" name="content-type" value="application/json">
		        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
		        <cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
		    </cfhttp>
			<cfcatch>
				<cfset recLogFile('Main info', 
					'Payment error: ' & close_batches_url & NL & 
					SerializeJSON(cfcatch)
					)>
			</cfcatch>
	    </cftry>
	
		<!--- remove updatingUserPlanList --->
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = Replace(updatingUserPlanList, userPlanIdList, "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "(\r\n|\n\r|\n|\r)\s*(\r\n|\n\r|\n|\r)", NL, "all")>
		<cfset updatingUserPlanList = Trim(updatingUserPlanList)>
		
		<cffile  
			action = "write" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#updatingUserPlanList#" 
			addNewLine = "no" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes"/>

		<cfif !arrayIsEmpty(errorUserPlans)>
			<cffile  
				action = "append" 
				file = "#dictPath#UserPlanList.txt" 
				output = "#NL#0,#arrayToList(errorUserPlans, ',')#" 
				addNewLine = "yes" 
				attributes = "normal"
				charset = "utf-8"  
				fixnewline = "yes"/>
		</cfif>

	<!---<cfelse>--->
	</cfif>

	<cfset recLogFile('Root', 'End recurring')>

	<cfif FileExists(dictPath & 'RecurringProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfset recurringProcessNumber-->
		<cfelse>
			<cfset recurringProcessNumber = 0>
		</cfif>
		<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', recurringProcessNumber)>
	</cfif>

	<cfif userPlansQuery.RecordCount EQ 100>
		<cfhttp url="http://#CGI.server_name#/public/sire/models/cfm/recurring-multi-thread-integrate-user.cfm" method="post" result="nextPage" port="80" timeout="1">
			<cfhttpparam type="formField" name="_nextPage" value="1">
			<cfhttpparam type="formField" name="_pageOne_now" value="#_pageOne_now#">
			<cfhttpparam type="formField" name="logFileNameIndex" value="#logFileNameIndex#">
			<cfhttpparam type="formField" name="logDataStartDate_dt" value="#logData.StartDate_dt#">
			<cfhttpparam type="formField" name="logDataAccountNumber_bi" value="#logData.AccountNumber_bi#">
			<cfhttpparam type="formField" name="logDataErrorNumber_bi" value="#logData.ErrorNumber_bi#">
			<cfhttpparam type="cookie" name="cftoken" value="#session.CFToken#">
			<cfhttpparam type="cookie" name="cfid" value="#session.cfid#">
		</cfhttp>
	</cfif>

	<cfcatch>
		<cfset recLogFile('Main info', 'Application error' & NL & SerializeJSON(cfcatch))>
	</cfcatch>

</cftry>

<!--- SEND EMAIL WHEN FINISH JOB --->	
<cfif userPlansQuery.RecordCount LT 100 OR (_nextPage GT 0 AND userPlansQuery.RecordCount EQ 0)>
	<!--- remove errorUserPlanList --->
	<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
	<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "0,[\d,]*(\r\n|\n\r|\n|\r)+", "", "all")>
	<cfset updatingUserPlanList = Trim(updatingUserPlanList)>
	
	<cffile  
		action = "write" 
		file = "#dictPath#UserPlanList.txt" 
		output = "#updatingUserPlanList#" 
		addNewLine = "no" 
		attributes = "normal"
		charset = "utf-8"  
		fixnewline = "yes"/>

	<!--- Notify System Admins who monitor --->
	<cfset logData.EndDate_dt = now()>
	<cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
	    SELECT
	        ContactAddress_vch                              
	    FROM 
	    	simpleobjects.systemalertscontacts
	    WHERE
	    	ContactType_int = 2
	    AND
	    	EMSNotice_int = 1    
	</cfquery>
	
	<cfif GetEMSAdmins.RecordCount GT 0>
		<cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                   
	<cfelse>
		<cfset EBMAdminEMSList = "support@siremobile.com">
	</cfif>
	
	<cfset dt = DateFormat(logData.EndDate_dt, 'yyyy-mm-dd') & ' ' & TimeFormat(logData.EndDate_dt, 'HH:mm:ss')>
	<cfabort/>
	<cfmail to="#EBMAdminEMSList#" subject="[Recurring] complete #dt# " type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		<cfoutput>
			Start Date: #DateFormat(logData.StartDate_dt, 'yyyy-mm-dd')# #TimeFormat(logData.StartDate_dt, 'HH:mm:ss')#<br>
			End Date: #dt#<br>
			#logData.AccountNumber_bi# User Plans<br>
			#logData.ErrorNumber_bi# Errors<br>
			Log file: #logData.LogFile_vch#
		</cfoutput>
	</cfmail>

</cfif>