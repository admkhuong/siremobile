<cfsetting RequestTimeout="86400"/>

<cfinclude template="cronjob-firewall.cfm">

<cfparam name="form._nextPage" default="0"/>
<cfparam name="form._pageOne_now" default=""/>
<cfparam name="form.logFileNameIndex" default="0"/>
<cfparam name="form.logDataStartDate_dt" default=""/>
<cfparam name="form.logDataAccountNumber_bi" default="0"/>
<cfparam name="form.logDataErrorNumber_bi" default="0"/>

<cfset _nextPage = form._nextPage/>
<cfset _pageOne_now = form._pageOne_now/>
<cfset logDataStartDate_dt = form.logDataStartDate_dt/>
<cfset logDataAccountNumber_bi = form.logDataAccountNumber_bi/>
<cfset logDataErrorNumber_bi = form.logDataErrorNumber_bi/>

<cfparam name="Session.DBSourceEBM" default="bishop"/>
<cfparam name="Session.DBSourceREAD" default="bishop_read"/>

<cfinclude template="/public/paths.cfm">
<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="/public/sire/models/cfc/sendmail.cfc">
<cfinclude template="/public/sire/models/cfc/order_plan.cfc">
<cfinclude template="/public/sire/models/cfc/referral.cfc">
<cfinclude template="/public/sire/models/cfc/mailchimp.cfc">

<cfset TAB = Chr(9) /> 
<cfset NL = Chr(13) & Chr(10) />
<cfset _now = (_pageOne_now NEQ '' ? DateFormat(_pageOne_now, 'yyyy-mm-dd') : DateFormat(now(), 'yyyy-mm-dd'))>
<cfset _fcm = left(_now, 7)>

<cfset logFileName = _now>
<cfset logFileNameIndex = form.logFileNameIndex>
<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/"/>

<cfloop condition="#_pageOne_now EQ '' AND FileExists('#dictPath##logFileName#.txt')#">
	<cfset logFileNameIndex++>
	<cfset logFileName = '#_now#-#logFileNameIndex#'>
</cfloop>
<cfif logFileNameIndex GT 0>
	<cfset _now &= '-#logFileNameIndex#'>
</cfif>

<cfif _pageOne_now EQ ''>
	<cfset _pageOne_now = now()/>
</cfif>


<!--- simplebilling.recurringlogs --->
<cfset logData = {
	RecurringLogId_int	= 0,
	StartDate_dt		= (logDataStartDate_dt EQ '' ? now() : logDataStartDate_dt),
	Status_ti			= 1,
	EndDate_dt			= javaCast( "null", 0 ),
	AccountNumber_bi	= logDataAccountNumber_bi,
	ErrorNumber_bi		= logDataErrorNumber_bi,
	LogFile_vch			= '#_fcm#/#_now#.txt'
}>

<cffunction name="recLogFile" hint="log to file">
	<cfargument name="logSession" default=""/>
	<cfargument name="logText" default=""/>
	
	<cfparam name="TAB" default="#Chr(9)#"/>
	<cfparam name="NL" default="#Chr(13) & Chr(10)#"/>
	<cfparam name="_now" default="#DateFormat(now(), 'yyyy-mm-dd')#"/>
	<cfparam name="_fcm" default="#left(_now, 7)#"/>
	
	<cfset var logNow = DateFormat(now(), 'yyyy-mm-dd') & ' ' & TimeFormat(now(), 'HH:mm:ss')/>
	<cfset var filePath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/#_now#.txt"/>
	<cfset var dictPath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/"/>
	
	<cfif !DirectoryExists(dictPath)>
		<cfset DirectoryCreate(dictPath)>
	</cfif>
	
	<cftry>		
		<cfswitch expression="#lCase(logSession)#">
			<cfcase value="root">
				<!---<cffile action = "delete" file = "#filePath#">--->
				<cffile  action = "append" 
					file = "#filePath#" 
					output = "===== #logText#: #logNow# =====" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
			<cfcase value="main info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##Replace(logText, NL, NL&TAB, 'ALL')##NL##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
			<cfcase value="sub info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##TAB##Replace(logText, NL, NL&TAB&TAB, 'ALL')##NL##TAB##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes"/>
			</cfcase>
		</cfswitch>
		
		<cfcatch>
			<!--- Save file error --->
		</cfcatch>
	
	</cftry>	
</cffunction>


<cftry>	
	
	<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring/"/>
	<!--- check to make sure only one recurring process running --->
	<cfif FileExists(dictPath & 'RecurringProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfif _LimitRecurringProcessNumber GT recurringProcessNumber>
				<cfset recurringProcessNumber++>
				<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', recurringProcessNumber)>
			<cfelse>
				Access denied<cfabort>
			</cfif>
		<cfelse>
			<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', "1")>
		</cfif>
	<cfelse>
		<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', "1")> 
	</cfif>
	
	<cfset recLogFile('Root', 'Start recurring')>
	<cfset updatingUserPlanList = ''>
	<cfif FileExists(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "\r\n|\n\r|\n|\r", ",", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "\s+", "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, ",+", ",", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "^,", "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, ",$", "", "all")>
	<cfelse>
		<cfset updatingUserPlanList = ''>
	</cfif>

	<!--- Get all userplan need to run. Except runing on other threead --->
	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
		SELECT 
			UserPlanId_bi,Status_int,UserId_int,PlanId_int,BillingType_ti,Amount_dec,UserAccountNumber_int,Controls_int,KeywordsLimitNumber_int,KeywordMinCharLimit_int,FirstSMSIncluded_int,
			UsedSMSIncluded_int,CreditsAddAmount_int,PriceMsgAfter_dec,PriceKeywordAfter_dec,StartDate_dt,EndDate_dt,MonthlyAddBenefitDate_dt,MlpsLimitNumber_int,ShortUrlsLimitNumber_int,
			MlpsImageCapacityLimit_bi,BuyKeywordNumber_int,BuyKeywordNumberExpired_int,DowngradeDate_dt,PromotionId_int,PromotionKeywordsLimitNumber_int,PromotionMlpsLimitNumber_int, 
			PromotionShortUrlsLimitNumber_int,PromotionLastVersionId_int,UserDowngradeDate_dt,UserDowngradePlan_int,UserDowngradeKeyword_txt,UserDowngradeMLP_txt,UserDowngradeShortUrl_txt,
			NumOfRecurringKeyword_int,NumOfRecurringPlan_int,UserIdApprovedDowngrade_int,UserIdDowngrade_int, DATEDIFF(CURDATE(),MonthlyAddBenefitDate_dt) as expireKeywordDateDiff, DATEDIFF(CURDATE(),EndDate_dt) as expirePlanDateDiff, TotalKeywordHaveToPaid_int 
		FROM 
			simplebilling.userplans
		WHERE 
			Status_int = 1
		<cfif updatingUserPlanList NEQ ''>
			AND UserPlanId_bi NOT IN (#updatingUserPlanList#)
		</cfif>
			AND ( DATE(EndDate_dt) <= CURDATE() OR MonthlyAddBenefitDate_dt <= CURDATE() )
		<cfif application.Env EQ false>			
			AND
				UserId_int IN (3002,3017,3018)				
			
		<cfelse>
			
		</cfif>	
		AND
			PlanId_int IN (
				SELECT 	PlanId_int
				FROM	simplebilling.plans
				WHERE	
					Status_int in (1,8)
			)
		-- AND 
		-- 	NumOfRecurringKeyword_int <= 4 AND NumOfRecurringPlan_int <= 4
		ORDER BY 
			EndDate_dt DESC, UserPlanId_bi DESC
		LIMIT 100
	</cfquery>
	
	
	<cfset userPlanIdList = ValueList(userPlansQuery.UserPlanId_bi)>

	<!---<cfset recLogFile('Main info', userPlansResult.SQL)>--->
	<cfset recLogFile('Main info', userPlansQuery.RecordCount & ' User Plans#NL#FROM simplebilling.userplans#NL#UserPlanId_bi: #userPlanIdList#')>
	<cfset EBMAdminEMSList=''>
	<cfif userPlansQuery.RecordCount GT 0>

		<cfset logData.AccountNumber_bi	+= userPlansQuery.RecordCount>

		<cffile  
			action = "append" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#userPlanIdList#" 
			addNewLine = "yes" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes"/>

		<cfset userIdList = ValueList(userPlansQuery.UserId_int)>
		
		<!--- get all user info --->
		<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch , YearlyRenew_ti , PaymentGateway_ti
			FROM 
				simpleobjects.useraccount
			WHERE 
				UserId_int IN (#userIdList#) AND Active_int = 1
			ORDER BY 
				UserId_int DESC
		</cfquery>
		
		<cfset recLogFile('Main info', usersQuery.RecordCount & 
			' User Accounts#NL#FROM simpleobjects.useraccount#NL#UserId_int: #userIdList#')>
		
		<!--- get all active plan info --->
		<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				PlanId_int, PlanName_vch, Amount_dec, YearlyAmount_dec, UserAccountNumber_int, Controls_int, KeywordsLimitNumber_int, KeywordMinCharLimit_int,
				FirstSMSIncluded_int, CreditsAddAmount_int, PriceMsgAfter_dec, PriceKeywordAfter_dec, ByMonthNumber_int, MlpsLimitNumber_int, ShortUrlsLimitNumber_int,
				MlpsImageCapacityLimit_bi, Order_int
			FROM 
				simplebilling.plans
			WHERE 
				Status_int in (1,8)
			ORDER BY 
				PlanId_int DESC
		</cfquery>
		
		<cfset planIdList = ValueList(plansQuery.PlanId_int)>


		<cfquery name="GetEMSAdmins" datasource="#Session.DBSourceREAD#">
		    SELECT
		        ContactAddress_vch                              
		    FROM 
		    	simpleobjects.systemalertscontacts
		    WHERE
		    	ContactType_int = 2
		    AND
		    	EMSNotice_int = 1    
		</cfquery>
		
		<cfif GetEMSAdmins.RecordCount GT 0>
			<cfset EBMAdminEMSList = ValueList(GetEMSAdmins.ContactAddress_vch,";")>                   
		<cfelse>
			<cfset EBMAdminEMSList = "support@siremobile.com">
		</cfif>

		<!--- GET FREE PLAN --->
		<cfset planFree = {}>

		<cfloop query="plansQuery">
			<cfif PlanId_int EQ 1>
				<cfset planFree['Amount_dec'] = Amount_dec >
				<cfset planFree['UserAccountNumber_int'] = UserAccountNumber_int >
				<cfset planFree['KeywordsLimitNumber_int'] = KeywordsLimitNumber_int >
				<cfset planFree['FirstSMSIncluded_int'] = FirstSMSIncluded_int >
				<cfset planFree['CreditsAddAmount_int'] = CreditsAddAmount_int >
				<cfset planFree['PriceMsgAfter_dec'] = PriceMsgAfter_dec >
				<cfset planFree['PriceKeywordAfter_dec'] = PriceKeywordAfter_dec >
				<cfset planFree['PlanId_int'] = PlanId_int >
				<cfset planFree['PlanName_vch'] = PlanName_vch >
				<cfset planFree['Controls_int'] = Controls_int >
				<cfset planFree['MlpsLimitNumber_int'] = MlpsLimitNumber_int >
				<cfset planFree['ShortUrlsLimitNumber_int'] = ShortUrlsLimitNumber_int >
				<cfset planFree['MlpsImageCapacityLimit_bi'] = MlpsImageCapacityLimit_bi >
			</cfif>
		</cfloop>

		<cfset recLogFile('Main info', plansQuery.RecordCount & 
			' Plans#NL#FROM simplebilling.plans#NL#PlanId_int: #planIdList#')>
		
		<!--- get user authen with sercurenet for auto run payment --->
		<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
			SELECT 
				uath.id,uath.PaymentMethodID_vch,uath.PaymentType_vch,uath.UserId_int, uath.PaymentGateway_ti
			FROM 
				simplebilling.authorize_user uath
			WHERE 
				UserId_int IN (#userIdList#)
			AND 
				status_ti = 1	
			AND
				PaymentGateway_ti = (
					Select  PaymentGateway_ti
					FROM 	simpleobjects.useraccount
					WHERE 	UserId_int = uath.UserId_int
				)
			ORDER BY 
				updated_dt desc, id DESC
		</cfquery>
		
		<cfset recLogFile('Main info', customersQuery.RecordCount & 
			' Customers In Securenet#NL#FROM simplebilling.authorize_user#NL#UserId_int: #userIdList#')>
		
		<cfset userIdsNeedUpdate = []>
	
		<cfinclude template="/session/sire/configs/credits.cfm">
		
		<cfset closeBatchesData = {
			developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.version
			}
		}>
	
		<cfset oldUserPlans = []>
		<cfset newUserPlans = []>
		<cfset arrNewUserPlansInfo = []>

		<cfset addPromotionCreditBalance = []>
		<cfset promocodeListToIncreaseUsedTime = []>
		<cfset promocodeListToIncreaseUsedTimeTemp = []>
		<cfset userListToSendAlertCCDeclineToAdmin = []>
		<cfset errorUserPlans = []>
		<cfset newBalanceBillings = []>
		<cfset logPaymentWorldpays = [[],[],[]]>
		<cfset paymentErrorsPlans = {}>

		<cfset ___c = 0>
		<cfset threadIndex = 1>
		<cfset threadList = ''>
		<cfset emailsend = ''/>
		<cfset _paymentGateWay = 0/>
		
		<cfloop query="userPlansQuery">
			
			<cfset threadId = threadIndex/>
			
			<!---<cfthread action="run" name="t#threadIndex#" threadId="#threadIndex#"> --->
			
				<cfset textLog = ''>
				
				<cfset planIndex = Variables.plansQuery.PlanId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.PlanId_int[threadId])) + 1>
				<cfset userIndex = Variables.usersQuery.UserId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.UserId_int[threadId])) + 1>
				<cfset customerIndex = Variables.customersQuery.UserId_int.IndexOf(JavaCast('int', Variables.userPlansQuery.UserId_int[threadId])) + 1>
				

				<cfset Variables.paymentErrorsPlans[threadId] = 0/>
				<cfset Variables.isDowngrade[threadId] = 0/>
				<cfset Variables.isRemoveCouponBenefit[threadId] = 0/>
				<cfset promocodeListToIncreaseUsedTimeTemp = []/>
				<cfset Variables.paymentErrorsMonthly[threadId] = 1/>

				<cfif userIndex GT 0 AND planIndex GT 0 AND arrayFind(userIdsNeedUpdate, Variables.userPlansQuery.UserId_int[threadId]) LE 0>
					
					<cfset arrayAppend(Variables.userIdsNeedUpdate, Variables.userPlansQuery.UserId_int[threadId])>

					<cfset _userFullName =  Variables.usersQuery.FirstName_vch[userIndex]&' '&Variables.usersQuery.LastName_vch[userIndex]>

					<cfset _paymentType = Variables.customersQuery.PaymentType_vch[customerIndex]>
					<cfset _paymentGateWay = Variables.customersQuery.PaymentGateway_ti[customerIndex]>

					<cfset _paymentType = structKeyExists(worldpayPaymentTypes, _paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
					<cfset _totalkeywordHaveToPaid = 0/>

					<!--- User Downgrade info --->
					<cfset _userDowngradePlan = Variables.userPlansQuery.UserDowngradePlan_int[threadId]/>					
					<cfset _userDowngradeKeyword = deserializeJSON(Variables.userPlansQuery.UserDowngradeKeyword_txt[threadId])/>
					<cfset _userDowngradeShortUrl = deserializeJSON(Variables.userPlansQuery.UserDowngradeShortUrl_txt[threadId])/>
					<cfset _userDowngradeMLP = deserializeJSON(Variables.userPlansQuery.UserDowngradeMLP_txt[threadId])/>
					<cfset _downgradePlanName = ''/>
					<cfset _downgradePlanCredits = 0/>
					<cfset _downgradePlanKeywords = 0 />

					<cfset _userBillingType = Variables.userPlansQuery.BillingType_ti[threadId]/>
					<cfset _numOfRecurringKeyword = Variables.userPlansQuery.NumOfRecurringKeyword_int[threadId]/> 
					<cfset _numOfRecurringPlan = Variables.userPlansQuery.NumOfRecurringPlan_int[threadId]/> 
					<cfset _expirePlanDateDiff = Variables.userPlansQuery.expirePlanDateDiff[threadId]/> 
					<cfset _expireKeywordDateDiff = Variables.userPlansQuery.expireKeywordDateDiff[threadId]/> 
					<cfset _yearlyRenew = Variables.usersQuery.YearlyRenew_ti[userIndex]>

					<!--- Promotion benefit --->
					<cfset _userPromotionKeyword = Variables.userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset _userPromotionMLP = Variables.userPlansQuery.PromotionMlpsLimitNumber_int[threadId]/>
					<cfset _userPromotionURL = Variables.userPlansQuery.PromotionShortUrlsLimitNumber_int[threadId]/>

					<cfset _lastTotalKeywordHaveToPaid = Variables.userPlansQuery.TotalKeywordHaveToPaid_int[threadId] />

					<cfset percent_dis = 0>
					<cfset flatrate_dis = 0>
					<cfset promotion_credit = 0>
					<cfset promotion_mlp = 0>
					<cfset promotion_keyword = 0>
					<cfset promotion_shorturl = 0>
					<cfset userPromotionId = 0>
					<cfset promotion_id = 0>
					<cfset promotion_origin_id = 0>
					<cfset list_promotion_have_alr_apply_at_lest_1_time="">

					<cfset _monthAmount = 0 /> <!--- use when try to charge monthly of yearly plan --->

					<cfset _amount = 0 > <!--- total amount have to paid --->

					<cfset _makePayment = 0 > <!--- check if user need to run payment --->
					<!---_makePayment 0 : do not run payment but still add new plan --->
					<!---_makePayment 1 : run payment --->
					<!---_makePayment 2 : do not run payment but do not add new plan --->



					<cfset _recurringType = 0 > 
					<!---_recurringType 0 : renew monthly/yearly --->
					<!---_recurringType 1 : add benefit monthly --->
					<!---_recurringType 2 : payment for buy keyword monthly --->

					<cfset _newUserPlansInfo = {} > 
					<cfset _newUserPlansInfo['Status_int'] = 1>
					<cfset _newUserPlansInfo['UserId_int'] = 0>
					<cfset _newUserPlansInfo['Amount_dec'] = 0>
					<cfset _newUserPlansInfo['UserAccountNumber_int'] = 0>
					<cfset _newUserPlansInfo['Controls_int'] = 0>
					<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = 0>
					<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = 0>
					<cfset _newUserPlansInfo['CreditsAddAmount_int'] = 0>
					<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = 0>
					<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = 0>
					<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = 0>
					<cfset _newUserPlansInfo['StartDate_dt'] = ''>
					<cfset _newUserPlansInfo['EndDate_dt'] = ''>
					<cfset _newUserPlansInfo['MlpsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = 0>
					<cfset _newUserPlansInfo['PromotionId_int'] = 0>
					<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = 0>
					<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = 0>
					<cfset _newUserPlansInfo['BillingType_ti'] = 0>
					<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = 0>
					<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
					<cfset _newUserPlansInfo['DowngradeDate_dt'] = ''>
					<cfset _newUserPlansInfo['PreviousPlanId_int'] = 1>
					<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = 0>
					<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = 0 >
					

					<cfinvoke component="public.sire.models.cfc.promotion" method="CheckAvailableCouponForUser" returnvariable="CouponForUser">
	                  <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
	                </cfinvoke>

					<!--- Check CouponForUser --->
					<cfif CouponForUser.rxresultcode EQ 1>
						<cfloop array="#CouponForUser['DATALIST']#" index="index">	
							<cfif listFind(list_promotion_have_alr_apply_at_lest_1_time, index.ORIGINID) EQ 0>
								<cfif index.COUPONDISCOUNTTYPE EQ 1>
									<cfset percent_dis = percent_dis + index.COUPONDISCOUNTPRICEPERCENT>
									<cfset flatrate_dis = flatrate_dis + index.COUPONDISCOUNTPRICEFLATRATE>
									<cfset promotion_id = index.PROMOTIONID>
									<cfset promotion_origin_id = index.ORIGINID>
									<!--- <cfset promotion_used_time = index.USEDTIME> --->
									<cfset userPromotionId = index.userPromotionId>
								<cfelseif index.COUPONDISCOUNTTYPE EQ 2>
									<cfset promotion_id = index.PROMOTIONID>
									<cfset promotion_origin_id = index.ORIGINID>
									<!--- <cfset promotion_used_time = index.USEDTIME> --->
									<cfset promotion_credit = promotion_credit + index.COUPONCREDIT>
									<cfset promotion_mlp = promotion_mlp + index.COUPONMLP>
									<cfset promotion_keyword = promotion_keyword + index.COUPONKEYWORD>
									<cfset promotion_shorturl = promotion_shorturl + index.COUPONSHORTURL>
									<cfset userPromotionId = index.userPromotionId>
								<cfelse>
									<cfset promotion_id = index.PROMOTIONID>
									<cfset promotion_origin_id = index.ORIGINID>
									<cfset userPromotionId = index.userPromotionId>
									<!--- <cfset promotion_used_time = index.USEDTIME> --->
								</cfif>

								<cfset arrayAppend(promocodeListToIncreaseUsedTimeTemp, [promotion_origin_id, promotion_id, Variables.userPlansQuery.UserId_int[threadId],userPromotionId])/>
								<cfset list_promotion_have_alr_apply_at_lest_1_time=listAppend(list_promotion_have_alr_apply_at_lest_1_time, index.ORIGINID)>
							</cfif>
						</cfloop>
					</cfif>
					

					<!--- calc amount when renew plan --->
					<cfif _userBillingType EQ 2 AND _yearlyRenew EQ 1> <!--- yearly and set renew --->
						<cfset _amount = (Variables.plansQuery.YearlyAmount_dec[planIndex]*12 - flatrate_dis - ((Variables.plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.plansQuery.PriceKeywordAfter_dec[planIndex])>	

						<cfset _monthAmount = (Variables.plansQuery.Amount_dec[planIndex] - flatrate_dis - ((Variables.plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.plansQuery.PriceKeywordAfter_dec[planIndex])>
					<cfelse>
						<cfset _amount = (Variables.plansQuery.Amount_dec[planIndex] - flatrate_dis - ((Variables.plansQuery.Amount_dec[planIndex]*percent_dis)/ 100)) + (Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.plansQuery.PriceKeywordAfter_dec[planIndex])>	
					</cfif> 
					<cfif _amount LT 0>
						<cfset _amount=0>
					</cfif>					
					
					
					<!--- if user buy plan yearly recalc _amount, check to see if it run monthly add benefit --->
					<cfif _userBillingType EQ 2 AND isDate(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
						<!--- if this is recurring monthly EndDate_dt later than MonthlyAddBenefitDate_dt  --->
						<cfif DateCompare(Variables.userPlansQuery.EndDate_dt[threadId],Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId]) EQ 1 AND DateCompare(Variables.userPlansQuery.EndDate_dt[threadId], NOW()) EQ 1  >
							<!--- if user buy keyword -> need to pay for these keywords --->
							<cfif Variables.userPlansQuery.BuyKeywordNumber_int[threadId] GT 0>
								<cfset _amount = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.plansQuery.PriceKeywordAfter_dec[planIndex]>	
								<cfset _recurringType = 2 > 
							<cfelse>
								<!--- add benefit monthly --->
								<cfset _amount = 0 >
								<cfset _recurringType = 1 > 
							</cfif>
						</cfif>
					</cfif>

					<!--- check if yearly plan but do not auto renew (_yearlyRenew = 0) --->
					<cfif _recurringType EQ 0 AND _userBillingType EQ 2 AND _yearlyRenew EQ 0>
						<cfset _userBillingType = 1 />
					</cfif>

					<!--- Try to charge their CC 3 times in three days then 1 more try in 30 day --->
					<cfif _recurringType EQ 2>
						<cfif _numOfRecurringKeyword LT 3>
							<cfset _makePayment = 1>
						<cfelseif _numOfRecurringKeyword EQ 3 AND _expireKeywordDateDiff GTE 29 >	
							<cfset _makePayment = 1>
						<cfelse>
							<cfset _makePayment = 2>
						</cfif>		
					<cfelseif _recurringType EQ 0>
						<cfif _numOfRecurringPlan LT 3>
							<cfset _makePayment = 1>
						<cfelseif _numOfRecurringPlan EQ 3 AND _expirePlanDateDiff GTE 29 >	
							<cfset _makePayment = 1>
						<cfelse>
							<cfset _makePayment = 2>	
						</cfif>
					</cfif>

					<!--- CHECK IF USER PLAN EXPIRED OVER 30 DAYS --->
					<cfinvoke component="public.sire.models.cfc.order_plan" method="GetUserPlan" returnvariable="getUserPlan">
			            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
			        </cfinvoke>

			       	<cfset keywordUsed = getUserPlan.KEYWORDUSE/>
			       	<cfset shorturlUsed = getUserPlan.SHORTURLUSED/>
			       	<cfset mlpUsed = getUserPlan.MLPUSED/>
			       	
					<!--- check if user select downgrade and have to paid --->
					<cfif _userDowngradePlan GT 0>
						<cfset Variables.isDowngrade[threadId] = 1/>
						<!--- get downgrade plan info --->
						<cfquery name="getDowngradePlan" datasource="#Session.DBSourceREAD#">
							SELECT 
								PlanId_int,
								PlanName_vch,
								Amount_dec,
								YearlyAmount_dec,
								UserAccountNumber_int,
								Controls_int, 
								KeywordsLimitNumber_int, 
								KeywordMinCharLimit_int, 
								FirstSMSIncluded_int,
								CreditsAddAmount_int,
								PriceMsgAfter_dec,
								PriceKeywordAfter_dec,
								ByMonthNumber_int,
								MlpsLimitNumber_int,
								ShortUrlsLimitNumber_int,
								MlpsImageCapacityLimit_bi,
								Order_int
							FROM 
								simplebilling.plans
							WHERE 
								Status_int = 1
							AND
								PlanId_int = #_userDowngradePlan#
						</cfquery>
						
						<cfif getDowngradePlan.RecordCount GT 0>

							<cfset _downgradePlanName = getDowngradePlan.PlanName_vch/>
							<cfset _downgradePlanCredits = getDowngradePlan.FirstSMSIncluded_int/>
							<cfset _downgradePlanKeywords = getDowngradePlan.KeywordsLimitNumber_int />
							<!--- get user keyword have to paid --->
							<!--- <cfset _totalkeywordHaveToPaid = keywordUsed - getDowngradePlan.KeywordsLimitNumber_int - Variables.userPlansQuery.BuyKeywordNumber_int[threadId]/> --->
							<cfset _totalkeywordHaveToPaid = keywordUsed - getDowngradePlan.KeywordsLimitNumber_int/>
							
							<!--- if _totalkeywordHaveToPaid greater than 0 then check downgrade keyword --->
							<cfif _totalkeywordHaveToPaid GT 0>
								<cfset currentRemoveKeyword = 0> 
								<!--- check if keyword user select is still active or not --->
								<cfif arrayLen(_userDowngradeKeyword) GT 0>
									<cfquery name="getActiveKeyword" datasource="#Session.DBSourceREAD#">
										SELECT 
											KeywordId_int
										FROM 
											sms.keyword 
										WHERE 
											Active_int = 1
										AND
											KeywordId_int IN (#arrayToList(_userDowngradeKeyword)#) 
									</cfquery>
									<cfif getActiveKeyword.RecordCount GT 0>
										<cfset currentRemoveKeyword = getActiveKeyword.RecordCount/>
									</cfif>
									<cfset _totalkeywordHaveToPaid = _totalkeywordHaveToPaid - currentRemoveKeyword/>
								</cfif>
							<cfelse>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>	

							<cfif _totalkeywordHaveToPaid LT 0>
								<cfset _totalkeywordHaveToPaid = 0/>
							</cfif>
							
							<!--- <cfset _amount = getDowngradePlan.Amount_dec + (Variables.userPlansQuery.BuyKeywordNumber_int[threadId] * Variables.plansQuery.PriceKeywordAfter_dec[planIndex]) + (_totalkeywordHaveToPaid * getDowngradePlan.PriceKeywordAfter_dec) > --->

							<cfset _amount = getDowngradePlan.Amount_dec >
						</cfif>
					</cfif>
					
					
					<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
					<cfif ( _userPromotionKeyword GT 0 AND promotion_keyword LT _userPromotionKeyword ) 
						  OR ( _userPromotionMLP GT 0 AND promotion_mlp LT _userPromotionMLP )  
						  OR ( _userPromotionURL GT 0 AND promotion_shorturl < _userPromotionURL) >
						  <cfset Variables.isRemoveCouponBenefit[threadId] = 1/>
					</cfif>	  
					
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						'#NL#User Id: ' & Variables.userPlansQuery.UserId_int[threadId] &
						'#NL#Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] &
						'#NL#Manual downgrade: ' & Variables.isDowngrade[threadId] &
						'#NL#Payment amount: $' & _amount &
						'#NL#Total keyword have to paid when downgraded: $' & _totalkeywordHaveToPaid &
						'#NL#Total keyword used: ' & keywordUsed &
						'#NL#Total mlp used: ' & mlpUsed &
						'#NL#Total shorturl used: ' & shorturlUsed &
						'#NL#promotion_credit: $' & promotion_credit &
						'#NL#promotion_mlp: $' & promotion_mlp &
						'#NL#promotion_keyword: $' & promotion_keyword &
						'#NL#promotion_shorturl: $' & promotion_shorturl &
						'#NL#percent_dis: $' & percent_dis &
						'#NL#flatrate_dis: $' & flatrate_dis &
						'#NL#make payment: ' & _makePayment &
						'#NL#billing type: ' & _userBillingType &
						'#NL#reccuring type: ' & _recurringType &
						'#NL#nums of recurring keyword: ' & _numOfRecurringKeyword &
						'#NL#nums of recurring plan: ' & _numOfRecurringPlan &
						'#NL#Yearly review: ' & _yearlyRenew
						>

					<!--- AUTO DOWNGRADE PLAN --->
			        <cfif getUserPlan.PLANEXPIRED EQ 2 OR ( getUserPlan.PLANEXPIRED EQ 3 AND getUserPlan.KEYWORDBUYAFTEREXPRIED GT 0 ) > 
			        	<cfset textLog &= NL & 'Auto downgrade:' & getUserPlan.PLANEXPIRED>

			        	<!--- IF USER PLAN HAS EXPIRED OVER 30 DAYS OR USER NOT PAY OWED KEYWORD --->
			     
						<!--- Insert Free Plan But keep the keyword buy and do not add FirstSMSIncluded --->	
						<!--- CHECK IF STARTDATE = 29,30,31 -> START DATE = 28 --->		
						<cfinclude template="/public/sire/models/cfm/inc_recurring/inc_recurring_case_1.cfm">															

					<cfelseif _numOfRecurringKeyword GTE 4 AND _expireKeywordDateDiff GTE 29 > 
						<!--- RELEASE KEYWORD WHEN YEARLY PLAN CAN NOT PURCHASED 
							 1 Relese keyword
							 2 update monthly date & _numOfRecurringKeyword
							 3 Send email
						--->
						<cfinclude template="/public/sire/models/cfm/inc_recurring/inc_recurring_case_2.cfm">
				        
					<cfelseif _amount GT 0 AND _makePayment EQ 1> <!--- IF USER HAVE TO PAID $ --->						
						<cfif customerIndex GT 0 > <!--- If find user have token in securenet ---> 							
							<!--- worldpay --->
							<cfif _paymentGateWay EQ 1 >
								<cfset paymentError = 1>
								<!--- <cfset paymentData = {  
								   amount = _amount,
								   paymentVaultToken = {  
								      customerId = Variables.customersQuery.UserId_int[customerIndex],
								      paymentMethodId = Variables.customersQuery.PaymentMethodID_vch[customerIndex],
								      paymentType = _paymentType
								   },
								   developerApplication = {  
								      developerId = worldpayApplication.developerId,
								      version = worldpayApplication.version
								   }
								}>

								<!--- log payment data --->
								<cfset textLog &= NL & TAB & SerializeJSON(paymentData)>
								
								<cfset paymentError = 1>
								<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>

								<cftry>
									<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
										<cfhttpparam type="header" name="content-type" value="application/json">
										<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
										<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
									</cfhttp>
									<cfcatch>
										<cfset textLog &= NL & TAB & 'Payment error: ' & payment_url & NL & TAB & SerializeJSON(cfcatch)>
									</cfcatch>
								</cftry>
								<!--- log payment data --->
								<cfset textLog &= NL & TAB & SerializeJSON(paymentResponse)>

								<cfif paymentResponse.status_code EQ 200>
									<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
										<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
										<cfif paymentResponseContent.success>
											<cfset paymentError = 0>
											<!--- LOG PAYMENT WORDPAY SUCESS --->
									        <cfset arrayAppend(Variables.logPaymentWorldpays[1], [
									        	Variables.userPlansQuery.UserId_int[threadId],
									        	Variables.userPlansQuery.PlanId_int[threadId],
									        	'Recurring',
									        	Variables.userPlansQuery.BuyKeywordNumber_int[threadId],
									        	0,
									        	paymentResponseContent
								        	])>	
										<cfelse> <!---PAYMENT SERCUNET ERROR --->
											<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
											<cfset Variables.paymentErrorsPlans[threadId] = 1/>
										</cfif>
									<cfelse> <!---PAYMENT SERCUNET ERROR --->
										<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
										<cfset Variables.paymentErrorsPlans[threadId] = 1/>
									</cfif>
								<cfelse> <!---PAYMENT SERCUNET ERROR --->
									<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
									<cfset Variables.paymentErrorsPlans[threadId] = 1/>
								</cfif>

								<!--- LOG ALL PAYMENT WORDPAY --->
						        <cfset arrayAppend(Variables.logPaymentWorldpays[2], [
						        	Variables.userPlansQuery.UserId_int[threadId],
						        	'Recurring',
						        	paymentResponse.status_code,
						        	paymentResponse.status_text,
						        	paymentResponse.errordetail,
						        	paymentResponse.filecontent,
						        	paymentData
					        	])>
 								--->					        	

							<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>																															
					        	<cfset paymentData = {  
								   amount = _amount,
								   inpVaultId = Variables.customersQuery.PaymentMethodID_vch[customerIndex]
								}>
								<!--- log payment data --->
								<cfset textLog &= NL & TAB & SerializeJSON(paymentData)>
								
								<cfset paymentError = 1>
								<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>
								<cfset sleep(10000)>
								<cfif _paymentGateWay EQ 2>
									<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>			
								<cfelseif _paymentGateWay EQ 3>
									<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>	
								<cfelseif _paymentGateWay EQ 4>
									<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
										<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
										<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="inpModuleName" value="Recurring"/>
									</cfinvoke>	
								</cfif>					
								
								<cfset paymentResponse.filecontent = rtSaleWithTokenzine.FILECONTENT>
								<cfif rtSaleWithTokenzine.RXRESULTCODE EQ 1>
									<cfset paymentError = 0>
									<cfset paymentResponseContent ={}/>																
									<cfset paymentResponseContent.FullName = _userFullName/>
									<!--- log payment success only--->
									<cfinvoke component="session.sire.models.cfc.billing" method="UpdatePayment" returnvariable="rtUpdatePayment">
										<cfinvokeargument name="inpPaymentData" value="#SerializeJSON(rtSaleWithTokenzine.REPORT)#"/>
										<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
										<cfinvokeargument name="moduleName" value="Recurring"/>											
										<cfinvokeargument name="inpPlanId" value="#Variables.userPlansQuery.PlanId_int[threadId]#">	
										<cfinvokeargument name="inpPaymentGateway" value="#_paymentGateWay#"/>
									</cfinvoke>									
								<cfelse> <!---PAYMENT PAYPAL ERROR --->
									<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
									<cfset Variables.paymentErrorsPlans[threadId] = 1/>										
								</cfif>								
							</cfif>
							
							<cfif paymentError EQ 0>
								<cfset paymentResponseContent.numberOfCredit = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]/>
								<cfset paymentResponseContent.numberOfKeyword = Variables.userPlansQuery.BuyKeywordNumber_int[threadId]/>
								<cfset paymentResponseContent.ROOTURL = rootUrl/>
								<cfset paymentResponseContent.PlanName = Variables.plansQuery.PlanName_vch[planIndex]/>
								<cfset paymentResponseContent.authorizedAmount = _amount/>

								<cfif Variables.isDowngrade[threadId] EQ 1>
									<cfset paymentResponseContent.PlanName = _downgradePlanName/>
									<cfset paymentResponseContent.numberOfCredit = _downgradePlanCredits/>
									<cfset paymentResponseContent.numberOfKeyword = _downgradePlanKeywords/>
								</cfif>	

								<!--- log payment data --->
								<cfset textLog &= NL & TAB & paymentResponse.filecontent>
								
								<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]/>

									<!--- SEND MAIL COMPLETED TO USER --->
									<cfif _paymentGateWay EQ 1>
									<!--- 	<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#emailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
							        	</cfinvoke>	 --->							       
									<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#emailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed_mojo.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
							        	</cfinvoke>
									</cfif>
							        
							        <!--- Set plan enddate --->
							        <cfif _userBillingType EQ 1>
							        	<cfset _endDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
							        <cfelse>	
							        	<cfset _endDate = DateAdd("yyyy", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
							        </cfif>

							        <cfif _recurringType EQ 2 AND IsDate(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
							        	<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
							        <cfelse>		
							        	<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
							        </cfif>
			    					
			    					<!--- if renew plan -> need to remove old plan --->
							        <cfif _recurringType EQ 0>
							        	<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
							        </cfif>	

							        <!--- check if is downgrade then add new plan = downgradeplan --->
							        <!--- buy keyword = keyword buy + buy after expired + keyword need to paid when downgrade --->
							        <cfif Variables.isDowngrade[threadId] EQ 1>


										<cfset _lastTotalKeywordHaveToPaid= _totalkeywordHaveToPaid>
							       		<cfset _newUserPlansInfo['Status_int'] = 1>
										<cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
										<cfset _newUserPlansInfo['PlanId_int'] = getDowngradePlan.PlanId_int>
										<cfset _newUserPlansInfo['Amount_dec'] = getDowngradePlan.Amount_dec>
										<cfset _newUserPlansInfo['UserAccountNumber_int'] = getDowngradePlan.UserAccountNumber_int>
										<cfset _newUserPlansInfo['Controls_int'] = getDowngradePlan.Controls_int>
										<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = getDowngradePlan.KeywordsLimitNumber_int>
										<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = getDowngradePlan.FirstSMSIncluded_int>
										<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = Variables.userPlansQuery.UsedSMSIncluded_int[threadId]>
										<cfset _newUserPlansInfo['CreditsAddAmount_int'] = getDowngradePlan.CreditsAddAmount_int>
										<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = getDowngradePlan.PriceMsgAfter_dec>
										<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = getDowngradePlan.PriceKeywordAfter_dec>
										<!--- <cfset _newUserPlansInfo['BuyKeywordNumber_int'] = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid> --->
										<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = 0>
										<cfset _newUserPlansInfo['StartDate_dt'] = Variables.userPlansQuery.EndDate_dt[threadId]>
										<cfset _newUserPlansInfo['EndDate_dt'] = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
										<cfset _newUserPlansInfo['MlpsLimitNumber_int'] = getDowngradePlan.MlpsLimitNumber_int>
										<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = getDowngradePlan.ShortUrlsLimitNumber_int>
										<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = getDowngradePlan.MlpsImageCapacityLimit_bi>
										<cfset _newUserPlansInfo['PromotionId_int'] = 0>
										<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = 0>
										<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = 0>
										<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = 0>
										<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = 0>
										<cfset _newUserPlansInfo['BillingType_ti'] = 1>
										<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
										<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
										<cfset _newUserPlansInfo['DowngradeDate_dt'] = Now()>
										<cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
										<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
										<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _lastTotalKeywordHaveToPaid>

										<cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

							       		 <!---DEACTIVE USER DATA IF USER'VE ALREADY SELECT KW,MLP,URL --->
					                    <cfinvoke method="DeactiveUserData" component="public.sire.models.cfc.order_plan">
					                        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
					                        <cfinvokeargument name="inpDowngradeKeyword" value="#_userDowngradeKeyword#">
					                        <cfinvokeargument name="inpDowngradeShortUrl" value="#_userDowngradeShortUrl#">
					                        <cfinvokeargument name="inpDowngradeMLP" value="#_userDowngradeMLP#">
					                    </cfinvoke>

							       		<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
										<!--- <cfif Variables.isRemoveCouponBenefit[threadId] EQ 1 > --->
											<!--- RELEASE USER DATA --->
									
											<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
									            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									            <!--- <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#"> --->
									            <cfinvokeargument name="inpkeywordUsed" value="0">
									            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
									            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
									            <!--- <cfinvokeargument name="inpLimitKeyword" value="#getDowngradePlan.KeywordsLimitNumber_int + Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid#"> --->
									            <cfinvokeargument name="inpLimitKeyword" value="0">
									            <cfinvokeargument name="inpLimitShortUrl" value="#getDowngradePlan.ShortUrlsLimitNumber_int#">
									            <cfinvokeargument name="inpLimitMLP" value="#getDowngradePlan.MlpsLimitNumber_int#">
									        </cfinvoke>
									        
										<!--- </cfif>	 --->

										<cfif _totalkeywordHaveToPaid GT 0>
								        	<cfset content = {} >
								        	<cfset content['toPlanName'] = getDowngradePlan['PlanName_vch']>
								        	<cfset content['_totalkeywordHaveToPaid'] = _totalkeywordHaveToPaid >
								        	<cfset content['FromPlanName'] = Variables.plansQuery.PlanName_vch[planIndex] >
								        	<cfset content['USERID'] = Variables.userPlansQuery.UserId_int[threadId] >

								        	<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
									            <cfinvokeargument name="to" value="#EBMAdminEMSList#">
									            <cfinvokeargument name="type" value="html">
									            <cfinvokeargument name="subject" value="User downgraded plan with un-paid keyword">
									            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_admin_downgrade.cfm">
									            <cfinvokeargument name="data" value="#SerializeJSON(content)#">
						        			</cfinvoke>					
								        </cfif>

										<!--- RESET CREDIT TO downgrade plan credit --->
							        	<cfset arrayAppend(Variables.newBalanceBillings, [
								        	Variables.userPlansQuery.UserId_int[threadId], 
								        	_downgradePlanCredits, 
						        		])>

						        		<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>

							        <cfelse> <!--- insert new plan or reset credits --->
							        	<cfif _recurringType EQ 0>							        		

							       			<cfset _newUserPlansInfo['Status_int'] = 1>
											<cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
											<cfset _newUserPlansInfo['PlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
											<cfset _newUserPlansInfo['Amount_dec'] = _userBillingType EQ 1 ? Variables.plansQuery.Amount_dec[planIndex] : Variables.plansQuery.YearlyAmount_dec[planIndex]*12>
											<cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.plansQuery.UserAccountNumber_int[planIndex]>
											<cfset _newUserPlansInfo['Controls_int'] = Variables.plansQuery.Controls_int[planIndex]>
											<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]>
											<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]>
											<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = Variables.userPlansQuery.UsedSMSIncluded_int[threadId]>
											<cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.userPlansQuery.CreditsAddAmount_int[threadId]>
											<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.userPlansQuery.PriceMsgAfter_dec[threadId]>
											<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.plansQuery.PriceKeywordAfter_dec[planIndex]>
											<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId]>
											<cfset _newUserPlansInfo['StartDate_dt'] = Variables.userPlansQuery.EndDate_dt[threadId]>
											<cfset _newUserPlansInfo['EndDate_dt'] = _endDate>
											<cfset _newUserPlansInfo['MlpsLimitNumber_int'] = Variables.userPlansQuery.MlpsLimitNumber_int[threadId]>
											<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId]>
											<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.userPlansQuery.MlpsImageCapacityLimit_bi[threadId]>
											<cfset _newUserPlansInfo['PromotionId_int'] = promotion_origin_id>
											<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = promotion_id>
											<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = promotion_keyword>
											<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = promotion_mlp>
											<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = promotion_shorturl>
											<cfset _newUserPlansInfo['BillingType_ti'] = _userBillingType>
											<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = _monthlyDate>
											<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
											<cfset _newUserPlansInfo['DowngradeDate_dt'] = ''>
											<cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
											<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
											<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _lastTotalKeywordHaveToPaid>

											<cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

								       		<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
											<cfif Variables.isRemoveCouponBenefit[threadId] EQ 1 >
												<!--- RELEASE USER DATA --->
												<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
										            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
										            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
										            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
										            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
										            <cfinvokeargument name="inpLimitKeyword" value="#Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]+Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
										            <cfinvokeargument name="inpLimitShortUrl" value="#Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
										            <cfinvokeargument name="inpLimitMLP" value="#Variables.userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
										        </cfinvoke>
											</cfif>

										<cfelseif _recurringType EQ 2> <!--- update date to add benefit --->
									 		<cfinvoke method="UpdateUserPlanAddBenefitDate" component="public.sire.models.cfc.order_plan">
									 			<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
									            <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#_monthlyDate#">
									            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									 		</cfinvoke>

									 		<cfinvoke method="ResetNumberOfRecurring" component="public.sire.models.cfc.order_plan">
									 			<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
									            <cfinvokeargument name="inpRecurringType" value="#_recurringType#">
									            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									 		</cfinvoke>
							        	</cfif>

							        	<!--- RESET CREDIT TO LASTEST USER PLAN --->
							        	<cfset arrayAppend(Variables.newBalanceBillings, [
								        	Variables.userPlansQuery.UserId_int[threadId], 
								        	Variables.userPlansQuery.FirstSMSIncluded_int[threadId], 
						        		])>

							        </cfif>	

							        <cfif _recurringType EQ 0>

								        <cfif promotion_credit GT 0>
										    <!--- ADD PROMOTION CREDIT BALANCE --->
									        <cfset arrayAppend(Variables.addPromotionCreditBalance, [
									        	Variables.userPlansQuery.UserId_int[threadId],
									        	promotion_credit
							        		])>
							        	</cfif>

							        	<!--- count promotion code used --->
										<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
        									<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
       									</cfloop>

       									<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
										<cfinvoke method="AddReferralCredits" component="public.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
										    <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
										</cfinvoke>
									</cfif>	

							</cfif>
							
							<!--- <cfif arrayFind(errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId]) GT 0> --->

					        <!---	
							</cfif>
							--->
							<!--- LOG RECURRING --->
					        <cfset arrayAppend(Variables.logPaymentWorldpays[3], [
					        	now(),
					        	Variables.userPlansQuery.UserPlanId_bi[threadId],
					        	Variables.userPlansQuery.UserId_int[threadId],
					        	Variables.userPlansQuery.PlanId_int[threadId],
					        	Variables.usersQuery.FirstName_vch[userIndex],
					        	Variables.usersQuery.EmailAddress_vch[userIndex],
					        	_amount,
					        	SerializeJSON(paymentData),
					        	paymentError,
					        	SerializeJSON(paymentResponse),
					        	_paymentGateWay,
								Variables.userPlansQuery.UserId_int[threadId]
				        	])>
						<cfelse>
							<cfset Variables.paymentErrorsPlans[threadId] = 1/>
							<!--- If no customer in Sercurnet --->
							<cfset textLog &= NL & TAB & 'No Customer In Securenet'>
							<!--- <cfset arrayAppend(Variables.oldUserPlans,[Variables.userPlansQuery.UserPlanId_bi[threadId], -2])> --->
							<cfset arrayAppend(Variables.errorUserPlans,Variables.userPlansQuery.UserPlanId_bi[threadId])>

							<!--- LOG RECURRING --->
					        <cfset arrayAppend(Variables.logPaymentWorldpays[3], [
					        	now(),
					        	Variables.userPlansQuery.UserPlanId_bi[threadId],
					        	Variables.userPlansQuery.UserId_int[threadId],
					        	Variables.userPlansQuery.PlanId_int[threadId],
					        	Variables.usersQuery.FirstName_vch[userIndex],
					        	Variables.usersQuery.EmailAddress_vch[userIndex],
					        	_amount,
					        	"Account Not Found",
					        	'1',
					        	"No data in simplebilling.authorize_user",
					        	_paymentGateWay,
								Variables.userPlansQuery.UserId_int[threadId]
					    	])>

						</cfif>

						<!--- IF PAYMENT NOT SUCCESS --->
						<cfif Variables.paymentErrorsPlans[threadId] EQ 1 >
							
							<cfset declineDataContent = {} >
							<cfset declineDataContent.USERID = Variables.userPlansQuery.UserId_int[threadId] >
							<cfset declineDataContent.PLANSNAME = Variables.plansQuery.PlanName_vch[planIndex]/>
							<cfset declineDataContent.PLANSTARTDATE = Variables.userPlansQuery.StartDate_dt[threadId] >
							<cfset declineDataContent.PLANENDDATE = Variables.userPlansQuery.EndDate_dt[threadId] >
							<cfset declineDataContent.PLANKEYWORD = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId] >
							<cfset declineDataContent.PROMOKEYWORD = Variables.userPlansQuery.PromotionKeywordsLimitNumber_int[threadId] >
							<cfset declineDataContent.PURCHASEDKEYWORD = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] >
							<cfset declineDataContent.ACTIVEKEYWORD = keywordUsed >
							<cfset declineDataContent.AVAILABLEKEYWORD = declineDataContent.PLANKEYWORD + declineDataContent.PROMOKEYWORD + declineDataContent.PURCHASEDKEYWORD - declineDataContent.ACTIVEKEYWORD>
							<cfset declineEmailsend = Variables.usersQuery.EmailAddress_vch[userIndex]>
							<cfset declinePlanTime = Variables.userPlansQuery.NumOfRecurringPlan_int[threadId]>
							<cfset declineKeywordTime = Variables.userPlansQuery.NumOfRecurringKeyword_int[threadId]>
							

							<cfif _recurringType EQ 0>
									
								<!--- if yearly renew fail -> try to charge monthly --->
								<cfif _userBillingType EQ 2 AND customerIndex GT 0 AND Variables.isDowngrade[threadId] EQ 0 >
									<!--- BEGIN CHARGE MONTHLY --->
									<cfif _paymentGateWay EQ 1>
										
										<!---<cfset paymentData = {  
										   amount = _monthAmount,
										   paymentVaultToken = {  
										      customerId = Variables.customersQuery.UserId_int[customerIndex],
										      paymentMethodId = Variables.customersQuery.PaymentMethodID_vch[customerIndex],
										      paymentType = _paymentType
										   },
										   developerApplication = {  
										      developerId = worldpayApplication.developerId,
										      version = worldpayApplication.version
										   }
										}>
										
										<!--- log payment data --->
										<cfset textLog &= NL & 'try to charge monthly :' & TAB & SerializeJSON(paymentData)>
										<cfset paymentResponse = { status_code = '',status_text = '', errordetail = '',filecontent = '' }>

										<cftry>
											<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
												<cfhttpparam type="header" name="content-type" value="application/json">
												<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
												<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
											</cfhttp>
											<cfcatch>
												<cfset textLog &= NL & TAB & 'Payment monthly error: ' & payment_url & NL & TAB & SerializeJSON(cfcatch)>
											</cfcatch>
										</cftry>

										log payment data
										<cfset textLog &= NL & TAB & SerializeJSON(paymentResponse)>

										<cfif paymentResponse.status_code EQ 200>
											<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
												<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
												<cfif paymentResponseContent.success>
													<cfset Variables.paymentErrorsMonthly[threadId] = 0>
													<!--- LOG PAYMENT WORDPAY SUCESS --->
											        <cfset arrayAppend(Variables.logPaymentWorldpays[1], [
											        	Variables.userPlansQuery.UserId_int[threadId],
											        	Variables.userPlansQuery.PlanId_int[threadId],
											        	'Recurring',
											        	Variables.userPlansQuery.BuyKeywordNumber_int[threadId],
											        	0,
											        	paymentResponseContent
										        	])>	
												</cfif>
											</cfif>
										</cfif>

										<!--- LOG ALL PAYMENT WORDPAY --->
								        <cfset arrayAppend(Variables.logPaymentWorldpays[2], [
								        	Variables.userPlansQuery.UserId_int[threadId],
								        	'Recurring',
								        	paymentResponse.status_code,
								        	paymentResponse.status_text,
								        	paymentResponse.errordetail,
								        	paymentResponse.filecontent,
								        	paymentData
							        	])> --->
							        
									<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4 >
										<cfset paymentData = {  
										amount = _amount,
										inpVaultId = Variables.customersQuery.PaymentMethodID_vch[customerIndex]
										}>
										<!--- log payment data --->
										<cfset textLog &= NL & TAB & SerializeJSON(paymentData)>																				
										<cfset paymentResponse = { status_code = '', status_text = '', errordetail = '', filecontent = ''}>
										<cfset sleep(10000)>
										<cfif _paymentGateWay EQ 2>
											<cfinvoke component="session.sire.models.cfc.vault-paypal" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
												<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
												<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
												<cfinvokeargument name="inpModuleName" value="Recurring"/>
											</cfinvoke>
										<cfelseif _paymentGateWay EQ 3>
											<cfinvoke component="session.sire.models.cfc.vault-mojo" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
												<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
												<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
												<cfinvokeargument name="inpModuleName" value="Recurring"/>
											</cfinvoke>
										<cfelseif _paymentGateWay EQ 4>
											<cfinvoke component="session.sire.models.cfc.vault-totalapps" method="SaleWithTokenzine" returnvariable="rtSaleWithTokenzine">
												<cfinvokeargument name="inpOrderAmount" value="#_amount#"/>
												<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
												<cfinvokeargument name="inpModuleName" value="Recurring"/>																								
											</cfinvoke>
										</cfif>
										
										<cfset paymentResponse.filecontent = rtSaleWithTokenzine.FILECONTENT>
										<cfif rtSaleWithTokenzine.RXRESULTCODE EQ 1>
											<cfset Variables.paymentErrorsMonthly[threadId] = 0>
											<cfset paymentResponseContent.FullName = _userFullName/>
											<cfset paymentResponseContent.authorizedAmount = _monthAmount/>
											<!--- log payment success only--->
											<cfinvoke component="session.sire.models.cfc.vault-mojo" method="UpdatePayment" returnvariable="rtUpdatePayment">
												<cfinvokeargument name="inpPaymentData" value="#SerializeJSON(rtSaleWithTokenzine.REPORT)#"/>
												<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">									
												<cfinvokeargument name="moduleName" value="Recurring"/>											
												<cfinvokeargument name="inpPlanId" value="#Variables.userPlansQuery.PlanId_int[threadId]#">	
												<cfinvokeargument name="inpPaymentGateway" value="#inpPaymentGateway#"/>	
											</cfinvoke>	
										<cfelse> <!---PAYMENT ERROR --->
											<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
											<cfset Variables.paymentErrorsPlans[threadId] = 1/>	
										</cfif>	
									</cfif>

									<cfset paymentResponseContent.numberOfCredit = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]/>
									<cfset paymentResponseContent.numberOfKeyword = Variables.userPlansQuery.BuyKeywordNumber_int[threadId]/>
									<cfset paymentResponseContent.ROOTURL = rootUrl/>
									<cfset paymentResponseContent.PlanName = Variables.plansQuery.PlanName_vch[planIndex]/>

									<!--- log payment data --->
									<cfset textLog &= NL & TAB & 'Payment monthly post data: ' & paymentResponse.filecontent>
								
									<cfset emailsend = Variables.usersQuery.EmailAddress_vch[userIndex]/>

									<cfif _paymentGateWay EQ 1>
									<!--- 	<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#emailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
						        		</cfinvoke>	 --->
						        	
									<cfelseif _paymentGateWay EQ 2 OR _paymentGateWay EQ 3 OR _paymentGateWay EQ 4>
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#emailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="[SIRE][Recurring] Recurring completed">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_completed_mojo.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(paymentResponseContent)#">
							        	</cfinvoke>	
									</cfif>

						        	<cfset _userBillingType = 1>
							        <cfset _endDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
			    					<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])/>

			    					<!--- need to remove old plan --->
							        <cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>

							        <!--- insert new --->							        

					       			<cfset _newUserPlansInfo['Status_int'] = 1>
									<cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
									<cfset _newUserPlansInfo['PlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
									<cfset _newUserPlansInfo['Amount_dec'] = _userBillingType EQ 1 ? Variables.plansQuery.Amount_dec[planIndex] : Variables.plansQuery.YearlyAmount_dec[planIndex]*12>
									<cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.plansQuery.UserAccountNumber_int[planIndex]>
									<cfset _newUserPlansInfo['Controls_int'] = Variables.plansQuery.Controls_int[planIndex]>
									<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]>
									<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = Variables.userPlansQuery.UsedSMSIncluded_int[threadId]>
									<cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.plansQuery.CreditsAddAmount_int[planIndex]>
									<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.userPlansQuery.PriceMsgAfter_dec[threadId]>
									<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.plansQuery.PriceKeywordAfter_dec[planIndex]>
									<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId]>
									<cfset _newUserPlansInfo['StartDate_dt'] = Variables.userPlansQuery.EndDate_dt[threadId]>
									<cfset _newUserPlansInfo['EndDate_dt'] = _endDate>
									<cfset _newUserPlansInfo['MlpsLimitNumber_int'] = Variables.userPlansQuery.MlpsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.userPlansQuery.MlpsImageCapacityLimit_bi[threadId]>
									<cfset _newUserPlansInfo['PromotionId_int'] = promotion_origin_id>
									<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = promotion_id>
									<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = promotion_keyword>
									<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = promotion_mlp>
									<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = promotion_shorturl>
									<cfset _newUserPlansInfo['BillingType_ti'] = _userBillingType>
									<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = _monthlyDate>
									<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
									<cfset _newUserPlansInfo['DowngradeDate_dt'] = ''>
									<cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
									<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
									<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _lastTotalKeywordHaveToPaid>

									<cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

						       		<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
									<cfif Variables.isRemoveCouponBenefit[threadId] EQ 1 >

										 <!---DEACTIVE USER DATA IF USER'VE ALREADY SELECT KW,MLP,URL --->
					                    <cfinvoke method="DeactiveUserData" component="public.sire.models.cfc.order_plan">
					                        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
					                        <cfinvokeargument name="inpDowngradeKeyword" value="#_userDowngradeKeyword#">
					                        <cfinvokeargument name="inpDowngradeShortUrl" value="#_userDowngradeShortUrl#">
					                        <cfinvokeargument name="inpDowngradeMLP" value="#_userDowngradeMLP#">
					                    </cfinvoke>
										<!--- RELEASE USER DATA --->
										<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
								            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
								            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
								            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
								            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
								            <cfinvokeargument name="inpLimitKeyword" value="#Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]+Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
								            <cfinvokeargument name="inpLimitShortUrl" value="#Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
								            <cfinvokeargument name="inpLimitMLP" value="#Variables.userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
								        </cfinvoke>
									</cfif>

									<!--- RESET CREDIT TO LASTEST USER PLAN --->
					        		<cfset arrayAppend(Variables.newBalanceBillings, [
							        	Variables.userPlansQuery.UserId_int[threadId], 
							        	Variables.userPlansQuery.FirstSMSIncluded_int[threadId], 
				        			])>

									<cfif promotion_credit GT 0>
									    <!--- ADD PROMOTION CREDIT BALANCE --->
								        <cfset arrayAppend(Variables.addPromotionCreditBalance, [
								        	Variables.userPlansQuery.UserId_int[threadId],
								        	promotion_credit
						        		])>
					        		</cfif>

						        	<!--- count promotion code used --->
									<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
    									<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
   									</cfloop>

   									<!--- ADD CURRENT REFERRAL CREDITS TO BALANCE --->
									<cfinvoke method="AddReferralCredits" component="public.sire.models.cfc.referral" returnvariable="RetAddReferralCredits">
									    <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									</cfinvoke>

									<!--- END CHARGE MONTHLY --->

								</cfif>

								<cfif Variables.paymentErrorsMonthly[threadId] EQ 1>
									<cfif Variables.userPlansQuery.NumOfRecurringPlan_int[threadId] EQ 0> <!--- send for 1st time --->
										<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#declineEmailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="Do not make a payment">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_decline.cfm">
								            <cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
						        		</cfinvoke>	 
						        	<cfelse> <!--- send for the next  --->
						        		<cfset dataMail = {
											USERID				= Variables.userPlansQuery.UserId_int[threadId], 
											PlanName_vch			= Variables.plansQuery.PlanName_vch[planIndex],
											StartDate_dt			= Variables.userPlansQuery.StartDate_dt[threadId],
											EndDate_dt				= Variables.userPlansQuery.EndDate_dt[threadId],
											DownGradeDate_dt		= DateAdd("d", 30, Variables.userPlansQuery.EndDate_dt[threadId]),
										}>
						        		<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
								            <cfinvokeargument name="to" value="#declineEmailsend#">
								            <cfinvokeargument name="type" value="html">
								            <cfinvokeargument name="subject" value="Sire can not make a payment monthly fee">
								            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_fail.cfm">
								            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
							    		</cfinvoke>
									</cfif>
								</cfif>
									
						    <cfelseif _recurringType EQ 2> <!--- can not make payment for keyword --->

						    	<cfif IsDate(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
						    		<cfset declineDataContent.SENDDATE = dateFormat(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId], "mmm dd yyyy")>
									<cfset declineDataContent.DISABLEDATE = dateFormat(dateAdd("m", 1, Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId]), "mmm dd yyyy")>
								<cfelse>	
									<cfset declineDataContent.SENDDATE = dateFormat(now(), "mmm dd yyyy")>
									<cfset declineDataContent.DISABLEDATE = dateFormat(dateAdd("m", 1, declineDataContent.SENDDATE), "mmm dd yyyy")>
						    	</cfif>

								<!--- Recurring Keyword Fail --->
								<cfif Variables.userPlansQuery.NumOfRecurringKeyword_int[threadId] EQ 0>
									<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							            <cfinvokeargument name="to" value="#declineEmailsend#">
							            <cfinvokeargument name="type" value="html">
							            <cfinvokeargument name="subject" value="Do not make a payment">
							            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_decline.cfm">
							            <cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
					        		</cfinvoke>	
					        		
					        		<!--- RESET CREDIT TO LASTEST USER PLAN --->
					        		<cfset arrayAppend(Variables.newBalanceBillings, [
							        	Variables.userPlansQuery.UserId_int[threadId], 
							        	Variables.userPlansQuery.FirstSMSIncluded_int[threadId], 
				        			])>
					        	<cfelse>
									<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							            <cfinvokeargument name="to" value="#declineEmailsend#">
							            <cfinvokeargument name="type" value="html">
							            <cfinvokeargument name="subject" value="Recurring Keywords Failed">
							            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_recurring_keyword_fail.cfm">
							            <cfinvokeargument name="data" value="#SerializeJSON(declineDataContent)#">
					        		</cfinvoke>						        							        		
								</cfif>
							</cfif>

							<cfif declinePlanTime EQ 2 OR declineKeywordTime EQ 2>
								<cfset declinedAmount = _amount/>
								<cfset tempCustomerInfo = {
									ID = "#Variables.userPlansQuery.UserId_int[threadId]#",
									EMAIL = "#declineEmailsend#",
									NAME = "#Variables.usersQuery.FirstName_vch[userIndex]# #Variables.usersQuery.LastName_vch[userIndex]#",
									PLAN = "#declineDataContent.PLANSNAME#",
									KEYWORD = "#declineDataContent.PURCHASEDKEYWORD#",
									AMOUNT = "#declinedAmount#"
								}/>

								<cfset arrayAppend(userListToSendAlertCCDeclineToAdmin, tempCustomerInfo)/>
							</cfif>

					        <!--- Update NumOfRecurring --->
					        <cfinvoke method="UpdateNumberOfRecurring" component="public.sire.models.cfc.order_plan">
		                        <cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
								<cfinvokeargument name="inpRecurringType" value="#_recurringType#">
								<cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
		                    </cfinvoke>

						</cfif>

					<cfelse> <!--- IF USER DO NOT HAVE TO PAID - INSERT FREE PLAN OR USE COUPON (100%) --->

						<cfif _makePayment EQ 0 OR _makePayment EQ 1 >

						    <cfif _userBillingType EQ 1>
					        	<cfset _endDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
					        <cfelse>	
					        	<cfset _endDate = DateAdd("yyyy", 1, Variables.userPlansQuery.EndDate_dt[threadId])>	
					        </cfif>
					        
							<cfif (_recurringType EQ 2 OR _recurringType EQ 1) AND IsDate(Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
					        	<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.MonthlyAddBenefitDate_dt[threadId])>
					        <cfelse>		
					        	<cfset _monthlyDate = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
					        </cfif>

						    <cfif _recurringType EQ 0>
						    	<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
						    </cfif>	

						    <!--- downgrade to free --->
					        <cfif Variables.isDowngrade[threadId] EQ 1> 
					        	

						        <cfset _newUserPlansInfo['Status_int'] = 1>
								<cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
								<cfset _newUserPlansInfo['PlanId_int'] = Variables.planFree.PlanId_int>
								<cfset _newUserPlansInfo['Amount_dec'] = Variables.planFree.Amount_dec>
								<cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.planFree.UserAccountNumber_int>
								<cfset _newUserPlansInfo['Controls_int'] = Variables.planFree.Controls_int>
								<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.planFree.KeywordsLimitNumber_int>
								<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.planFree.FirstSMSIncluded_int>
								<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = 0>
								<cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.planFree.CreditsAddAmount_int>
								<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.planFree.PriceMsgAfter_dec>
								<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.planFree.PriceKeywordAfter_dec>
								<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = 0>
								<cfset _newUserPlansInfo['StartDate_dt'] = Variables.userPlansQuery.EndDate_dt[threadId]>
								<cfset _newUserPlansInfo['EndDate_dt'] = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
								<cfset _newUserPlansInfo['MlpsLimitNumber_int'] = Variables.planFree.MlpsLimitNumber_int>
								<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.planFree.ShortUrlsLimitNumber_int>
								<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.planFree.MlpsImageCapacityLimit_bi>
								<cfset _newUserPlansInfo['PromotionId_int'] = 0>
								<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = 0>
								<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = 0>
								<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = 0>
								<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = 0>
								<cfset _newUserPlansInfo['BillingType_ti'] = 1>
								<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] = DateAdd("m", 1, Variables.userPlansQuery.EndDate_dt[threadId])>
								<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
								<cfset _newUserPlansInfo['DowngradeDate_dt'] = now()>
								<cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
								<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
								<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _totalkeywordHaveToPaid>
								<cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>


						        <!--- RESET CREDIT TO LASTEST USER PLAN --->
					        	<cfset arrayAppend(Variables.newBalanceBillings, [
						        	Variables.userPlansQuery.UserId_int[threadId], 
						        	_downgradePlanCredits, 
				        		])>

						        <!---DEACTIVE USER DATA IF USER'VE ALREADY SELECT KW,MLP,URL --->
			                    <cfinvoke method="DeactiveUserData" component="public.sire.models.cfc.order_plan">
			                        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
			                        <cfinvokeargument name="inpDowngradeKeyword" value="#_userDowngradeKeyword#">
			                        <cfinvokeargument name="inpDowngradeShortUrl" value="#_userDowngradeShortUrl#">
			                        <cfinvokeargument name="inpDowngradeMLP" value="#_userDowngradeMLP#">
			                    </cfinvoke>
			                    <!--- <cfif Variables.isRemoveCouponBenefit[threadId] EQ 1> --->
		                    	<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
						            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
						            <!--- <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#"> --->
						            <cfinvokeargument name="inpkeywordUsed" value="0">
						            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
						            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
						            <!--- <cfinvokeargument name="inpLimitKeyword" value="#getDowngradePlan.KeywordsLimitNumber_int + Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + _totalkeywordHaveToPaid#"> --->
						            <cfinvokeargument name="inpLimitKeyword" value="0">
						            <cfinvokeargument name="inpLimitShortUrl" value="#getDowngradePlan.ShortUrlsLimitNumber_int#">
						            <cfinvokeargument name="inpLimitMLP" value="#getDowngradePlan.MlpsLimitNumber_int#">
						        </cfinvoke>
						        <!--- </cfif> --->

						        <!--- send email to admin --->
						        <cfif _totalkeywordHaveToPaid GT 0>
						        	<cfset content = {} >
						        	<cfset content['toPlanName'] = planFree['PlanName_vch']>
						        	<cfset content['_totalkeywordHaveToPaid'] = _totalkeywordHaveToPaid >
						        	<cfset content['FromPlanName'] = Variables.plansQuery.PlanName_vch[planIndex] >
						        	<cfset content['USERID'] = Variables.userPlansQuery.UserId_int[threadId] >

						        	<cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
							            <cfinvokeargument name="to" value="#EBMAdminEMSList#">
							            <cfinvokeargument name="type" value="html">
							            <cfinvokeargument name="subject" value="User downgraded plan with un-paid keyword">
							            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_notify_admin_downgrade.cfm">
							            <cfinvokeargument name="data" value="#SerializeJSON(content)#">
				        			</cfinvoke>					
						        </cfif>


					        <cfelse> <!--- if is renew -> insert new plan, if add benefit -> only reset credits to current plan credits --->

					        	<cfif _recurringType EQ 0 > <!--- renew plan --->									

							    	<cfset _newUserPlansInfo['Status_int'] = 1>
									<cfset _newUserPlansInfo['UserId_int'] = Variables.userPlansQuery.UserId_int[threadId]>
									<cfset _newUserPlansInfo['PlanId_int'] = Variables.userPlansQuery.PlanId_int>
									<cfset _newUserPlansInfo['Amount_dec'] = _userBillingType EQ 1 ? Variables.plansQuery.Amount_dec[planIndex] : Variables.plansQuery.YearlyAmount_dec[planIndex]*12>
									<cfset _newUserPlansInfo['UserAccountNumber_int'] = Variables.plansQuery.UserAccountNumber_int[planIndex]>
									<cfset _newUserPlansInfo['Controls_int'] = Variables.plansQuery.Controls_int[planIndex]>
									<cfset _newUserPlansInfo['KeywordsLimitNumber_int'] = Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['FirstSMSIncluded_int'] = Variables.userPlansQuery.FirstSMSIncluded_int[threadId]>
									<cfset _newUserPlansInfo['UsedSMSIncluded_int'] = Variables.userPlansQuery.UsedSMSIncluded_int[threadId]>
									<cfset _newUserPlansInfo['CreditsAddAmount_int'] = Variables.plansQuery.CreditsAddAmount_int[planIndex]>
									<cfset _newUserPlansInfo['PriceMsgAfter_dec'] = Variables.userPlansQuery.PriceMsgAfter_dec>
									<cfset _newUserPlansInfo['PriceKeywordAfter_dec'] = Variables.userPlansQuery.PriceKeywordAfter_dec>
									<cfset _newUserPlansInfo['BuyKeywordNumber_int'] = Variables.userPlansQuery.BuyKeywordNumber_int[threadId]>
									<cfset _newUserPlansInfo['StartDate_dt'] = Variables.userPlansQuery.EndDate_dt[threadId]>
									<cfset _newUserPlansInfo['EndDate_dt'] = _endDate>
									<cfset _newUserPlansInfo['MlpsLimitNumber_int'] =  Variables.userPlansQuery.MlpsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['ShortUrlsLimitNumber_int'] = Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId]>
									<cfset _newUserPlansInfo['MlpsImageCapacityLimit_bi'] = Variables.userPlansQuery.MlpsImageCapacityLimit_bi[threadId]>
									<cfset _newUserPlansInfo['PromotionId_int'] = promotion_origin_id>
									<cfset _newUserPlansInfo['PromotionLastVersionId_int'] = promotion_id>
									<cfset _newUserPlansInfo['PromotionKeywordsLimitNumber_int'] = promotion_keyword>
									<cfset _newUserPlansInfo['PromotionMlpsLimitNumber_int'] = promotion_mlp>
									<cfset _newUserPlansInfo['PromotionShortUrlsLimitNumber_int'] = promotion_shorturl>
									<cfset _newUserPlansInfo['BillingType_ti'] = _userBillingType>
									<cfset _newUserPlansInfo['MonthlyAddBenefitDate_dt'] =_monthlyDate>
									<cfset _newUserPlansInfo['BuyKeywordNumberExpired_int'] = 0>
									<cfset _newUserPlansInfo['DowngradeDate_dt'] = ''>
									<cfset _newUserPlansInfo['PreviousPlanId_int'] = Variables.userPlansQuery.PlanId_int[threadId]>
									<cfset _newUserPlansInfo['UserIdApprovedDowngrade_int'] = Variables.userPlansQuery.UserIdDowngrade_int[threadId]>
									<cfset _newUserPlansInfo['TotalKeywordHaveToPaid'] = _lastTotalKeywordHaveToPaid>
									<cfset arrayAppend(Variables.newUserPlans,_newUserPlansInfo)/>

							    	<cfif promotion_credit GT 0>
								    	<!--- ADD PROMOTION CREDIT BALANCE --->
								    	<cfset arrayAppend(Variables.addPromotionCreditBalance, [
							        	Variables.userPlansQuery.UserId_int[threadId],
							        	promotion_credit
						        		])>
				        			</cfif>

				        			<cfloop array="#promocodeListToIncreaseUsedTimeTemp#" index="index">
										<cfset arrayAppend(promocodeListToIncreaseUsedTime, [index[1], index[2], index[3],index[4]]) />
									</cfloop>

									<!--- check if user promotion not reccuring anymore then release user kw,mlp,url  --->
									 <cfif Variables.isRemoveCouponBenefit[threadId] EQ 1>
										<!--- RELEASE USER DATA --->
										<cfinvoke method="ReleaseUserData" component="public.sire.models.cfc.order_plan">
								            <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
								            <cfinvokeargument name="inpkeywordUsed" value="#keywordUsed#">
								            <cfinvokeargument name="inpshortUrlUsed" value="#shorturlUsed#">
								            <cfinvokeargument name="inpMLPUsed" value="#mlpUsed#">
								           <cfinvokeargument name="inpLimitKeyword" value="#Variables.userPlansQuery.KeywordsLimitNumber_int[threadId]+Variables.userPlansQuery.BuyKeywordNumber_int[threadId] + Variables.userPlansQuery.BuyKeywordNumberExpired_int[threadId] + promotion_keyword#">
											<cfinvokeargument name="inpLimitShortUrl" value="#Variables.userPlansQuery.ShortUrlsLimitNumber_int[threadId] + promotion_mlp#">
										    <cfinvokeargument name="inpLimitMLP" value="#Variables.userPlansQuery.MlpsLimitNumber_int[threadId] + promotion_shorturl#">
								        </cfinvoke>
									</cfif>	
								<cfelseif _recurringType EQ 1>	
									<cfinvoke method="UpdateUserPlanAddBenefitDate" component="public.sire.models.cfc.order_plan">
										<cfinvokeargument name="inpUserPlanId" value="#Variables.userPlansQuery.UserPlanId_bi[threadId]#">
									    <cfinvokeargument name="inpMonthlyAddBenefitDate" value="#_monthlyDate#">
									    <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
									</cfinvoke>	

									<cfset textLog &= NL & TAB & 'Update add benefit date planId:#Variables.userPlansQuery.UserPlanId_bi[threadId]# date:#_monthlyDate#'>
					        	</cfif> 

								<!--- RESET CREDIT TO LASTEST USER PLAN --->
					        	<cfset arrayAppend(Variables.newBalanceBillings, [
						        	Variables.userPlansQuery.UserId_int[threadId], 
						        	Variables.userPlansQuery.FirstSMSIncluded_int[threadId], 
				        		])>

					        </cfif>	

							<!--- RESET USER REFERRAL CREDITS --->
						    <cfinvoke method="ResetUserReferral" component="public.sire.models.cfc.referral">
						        <cfinvokeargument name="inpUserId" value="#Variables.userPlansQuery.UserId_int[threadId]#">
						    </cfinvoke>
					    </cfif>
					</cfif>

				<cfelseif userIndex GT 0 AND planIndex GT 0>
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						'#NL#User Id: ' & Variables.userPlansQuery.UserId_int[threadId] &
						'#NL#Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] &
						'#NL#Duplicate plan'
						>
					<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
				<cfelse>
					<cfset textLog = 
						'UserPlanId_bi: ' & Variables.userPlansQuery.UserPlanId_bi[threadId] &
						(userIndex LE 0 ? '#NL#No User Id: ' & Variables.userPlansQuery.UserId_int[threadId] : '') &
						(planIndex LE 0 ? '#NL#No Plan Id: ' & Variables.userPlansQuery.PlanId_int[threadId] : '')
						>
					<cfif userIndex LE 0>
						<cfset arrayAppend(Variables.oldUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
					<cfelse>
						<cfset arrayAppend(Variables.oldUserPlans, [Variables.userPlansQuery.UserPlanId_bi[threadId], -1])>
					</cfif>
					<cfset arrayAppend(Variables.errorUserPlans, Variables.userPlansQuery.UserPlanId_bi[threadId])>
				</cfif>

				<cfset recLogFile('Main info', textLog)>

	            <cfset ___c++>
	            <cffile
                    action = "write"
                    file = "#dictPath#UserPlanCount.txt"
                    output = "#___c#"
                    addNewLine = "no"
                    attributes = "normal"
                    charset = "utf-8"
                    fixnewline = "yes"/>
			<!--- </cfthread> --->
		
			<cfset listAppend(threadList,"t#threadIndex#")>
			<cfset threadIndex++>
		</cfloop>
		
		<!--- <cfthread action="join" timeout="86400000" name="#threadList#"></cfthread> --->
		
		<cfinclude template="/public/sire/models/cfm/inc_recurring/inc_recurring_final_logs.cfm">															
		 

		<cftry>
		    <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
		        <cfhttpparam type="header" name="content-type" value="application/json">
		        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
		        <cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
		    </cfhttp>
			<cfcatch>
				<cfset recLogFile('Main info', 
					'Payment error: ' & close_batches_url & NL & 
					SerializeJSON(cfcatch)
					)>
			</cfcatch>
	    </cftry>
	
		<!--- remove updatingUserPlanList --->
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = Replace(updatingUserPlanList, userPlanIdList, "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "(\r\n|\n\r|\n|\r)\s*(\r\n|\n\r|\n|\r)", NL, "all")>
		<cfset updatingUserPlanList = Trim(updatingUserPlanList)>
		
		<cffile  
			action = "write" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#updatingUserPlanList#" 
			addNewLine = "no" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes"/>

		<cfif !arrayIsEmpty(errorUserPlans)>
			<cffile  
				action = "append" 
				file = "#dictPath#UserPlanList.txt" 
				output = "#NL#0,#arrayToList(errorUserPlans, ',')#" 
				addNewLine = "yes" 
				attributes = "normal"
				charset = "utf-8"  
				fixnewline = "yes"/>
		</cfif>

	<!---<cfelse>--->
	</cfif>

	<cfset recLogFile('Root', 'End recurring')>

	<cfif FileExists(dictPath & 'RecurringProcessNumber.txt')>
		<cfset recurringProcessNumber = FileRead(dictPath & 'RecurringProcessNumber.txt')>
		<cfif isNumeric(recurringProcessNumber)>
			<cfset recurringProcessNumber-->
		<cfelse>
			<cfset recurringProcessNumber = 0>
		</cfif>
		<cfset FileWrite(dictPath & 'RecurringProcessNumber.txt', recurringProcessNumber)>
	</cfif>

	<cfif userPlansQuery.RecordCount EQ 100>
		<cfhttp url="http://#CGI.server_name#/public/sire/models/cfm/recurring-multi-thread.cfm" method="post" result="nextPage" port="80" timeout="1">
			<cfhttpparam type="formField" name="_nextPage" value="1">
			<cfhttpparam type="formField" name="_pageOne_now" value="#_pageOne_now#">
			<cfhttpparam type="formField" name="logFileNameIndex" value="#logFileNameIndex#">
			<cfhttpparam type="formField" name="logDataStartDate_dt" value="#logData.StartDate_dt#">
			<cfhttpparam type="formField" name="logDataAccountNumber_bi" value="#logData.AccountNumber_bi#">
			<cfhttpparam type="formField" name="logDataErrorNumber_bi" value="#logData.ErrorNumber_bi#">
			<cfhttpparam type="cookie" name="cftoken" value="#session.CFToken#">
			<cfhttpparam type="cookie" name="cfid" value="#session.cfid#">
		</cfhttp>
	</cfif>

	<cfcatch>
		<cfset recLogFile('Main info', 'Application error' & NL & SerializeJSON(cfcatch))>
	</cfcatch>

</cftry>


<!--- SEND EMAIL WHEN FINISH JOB --->	
<cfif  (userPlansQuery.RecordCount GT 0 AND userPlansQuery.RecordCount LT 100) OR (_nextPage GT 0 AND userPlansQuery.RecordCount EQ 0)>
	<!--- remove errorUserPlanList --->
	<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
	<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "0,[\d,]*(\r\n|\n\r|\n|\r)+", "", "all")>
	<cfset updatingUserPlanList = Trim(updatingUserPlanList)>
	
	<cffile  
		action = "write" 
		file = "#dictPath#UserPlanList.txt" 
		output = "#updatingUserPlanList#" 
		addNewLine = "no" 
		attributes = "normal"
		charset = "utf-8"  
		fixnewline = "yes"/>

	<!--- Notify System Admins who monitor --->
	<cfset logData.EndDate_dt = now()>
	
	<cfset dt = DateFormat(logData.EndDate_dt, 'yyyy-mm-dd') & ' ' & TimeFormat(logData.EndDate_dt, 'HH:mm:ss')>
	
	<cfmail to="#EBMAdminEMSList#" subject="[Recurring] complete #dt# " type="html" from="#SupportEMailFrom#" username="#SupportEMailUserName#" password="#SupportEMailPassword#" server="#SupportEMailServer#" port="#SupportEMailPort#" UseSSL="#SupportEMailUseSSL#">
		<cfoutput>
			Start Date: #DateFormat(logData.StartDate_dt, 'yyyy-mm-dd')# #TimeFormat(logData.StartDate_dt, 'HH:mm:ss')#<br>
			End Date: #dt#<br>
			#logData.AccountNumber_bi# User Plans<br>
			#logData.ErrorNumber_bi# Errors<br>
			Log file: #logData.LogFile_vch#
		</cfoutput>
	</cfmail>

</cfif>