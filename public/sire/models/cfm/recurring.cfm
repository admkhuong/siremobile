<cfparam name="Session.DBSourceEBM" default="bishop"/>
<cfparam name="Session.DBSourceREAD" default="bishop_read"/>

<cfset TAB = Chr(9) /> 
<cfset NL = Chr(13) & Chr(10) />
<cfset _now = DateFormat(now(), 'yyyy-mm-dd')>
<cfset _fcm = left(_now, 7)>

<cfset logFileName = _now>
<cfset logFileNameIndex = 0>
<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/"/>
<cfloop condition="#FileExists('#dictPath##logFileName#.txt')#">
	<cfset logFileNameIndex++>
	<cfset logFileName = '#_now#-#logFileNameIndex#'>
</cfloop>
<cfif logFileNameIndex GT 0>
	<cfset _now &= '-#logFileNameIndex#'>
</cfif>

<cffunction name="recLogFile">
	<cfargument name="logSession" default=""/>
	<cfargument name="logText" default=""/>
	
	<cfparam name="TAB" default="#Chr(9)#"/>
	<cfparam name="NL" default="#Chr(13) & Chr(10)#"/>
	<cfparam name="_now" default="#DateFormat(now(), 'yyyy-mm-dd')#"/>
	<cfparam name="_fcm" default="#left(_now, 7)#"/>
	
	<cfset var logNow = DateFormat(now(), 'yyyy-mm-dd') & ' ' & TimeFormat(now(), 'HH:mm:ss')/>
	<cfset var filePath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/#_now#.txt"/>
	<cfset var dictPath = "#ExpandPath('/')#session/sire/logs/recurring/#_fcm#/"/>
	
	<cfif !DirectoryExists(dictPath)>
		<cfset DirectoryCreate(dictPath)>
	</cfif>
	
	<cftry>
	
		<cfswitch expression="#lCase(logSession)#">
			<cfcase value="root">
				<!---<cffile action = "delete" file = "#filePath#">--->
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "===== #logText#: #logNow# =====" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
			<cfcase value="main info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##Replace(logText, NL, NL&TAB, 'ALL')##NL##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
			<cfcase value="sub info">
				<cffile  
					action = "append" 
					file = "#filePath#" 
					output = "#TAB##TAB##Replace(logText, NL, NL&TAB&TAB, 'ALL')##NL##TAB##TAB#==============================" 
					addNewLine = "yes" 
					attributes = "normal"
					charset = "utf-8"  
					fixnewline = "yes" 
					mode = "777">
			</cfcase>
		</cfswitch>
		
		<cfcatch>
			<!--- Save file error --->
		</cfcatch>
	
	</cftry>	
</cffunction>

<cfset recLogFile('Root', 'Start recurring')>

<cftry>

	<cfset dictPath = "#ExpandPath('/')#session/sire/logs/recurring/"/>
	
	<cfif FileExists(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "\r\n|\n\r|\n|\r", ",", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "^[\s,]+|[\s,]{2}|[\s,]+$", "", "all")>
	<cfelse>
		<cfset updatingUserPlanList = ''>
	</cfif>

	<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
		SELECT * FROM simplebilling.userplans
		WHERE Status_int = 1
		<cfif updatingUserPlanList NEQ ''>
			AND UserPlanId_bi NOT IN (#updatingUserPlanList#)
		</cfif>
			AND DATE(EndDate_dt) <= CURDATE()
		ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
	</cfquery>
	
	<cfset userPlanIdList = ValueList(userPlansQuery.UserPlanId_bi)>
	
	<!---<cfset recLogFile('Main info', userPlansResult.SQL)>--->
	<cfset recLogFile('Main info', userPlansQuery.RecordCount & ' User Plans#NL#UserPlanId_bi: #userPlanIdList#')>
	
	<cfif userPlansQuery.RecordCount GT 0>
		
		<cffile  
			action = "append" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#userPlanIdList#" 
			addNewLine = "yes" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes" 
			mode = "777">

		<cfinclude template="/session/sire/configs/credits.cfm">
	
		<cfset userIdList = ValueList(userPlansQuery.UserId_int)>
		
		<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
			SELECT UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch 
			FROM simpleobjects.useraccount
			WHERE UserId_int IN (#userIdList#) AND Active_int = 1
			ORDER BY UserId_int DESC
		</cfquery>
		
		<cfset recLogFile('Main info', usersQuery.RecordCount & 
			' User Accounts#NL#FROM simpleobjects.useraccount#NL#UserId_int: #userIdList#')>
		
		<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
			SELECT * FROM simplebilling.plans
			WHERE Status_int = 1
			ORDER BY PlanId_int DESC
		</cfquery>
		
		<cfset planIdList = ValueList(plansQuery.PlanId_int)>

		<cfset recLogFile('Main info', plansQuery.RecordCount & 
			' Plans#NL#FROM simplebilling.plans#NL#PlanId_int: #planIdList#')>
		
		<cfquery name="customersQuery" datasource="#Session.DBSourceREAD#">
			SELECT * FROM simplebilling.authorize_user
			WHERE UserId_int IN (#userIdList#)
			ORDER BY id DESC
		</cfquery>
		
		<cfset recLogFile('Main info', customersQuery.RecordCount & 
			' Customers In Securenet#NL#FROM simplebilling.authorize_user#NL#UserId_int: #userIdList#')>
		
		<cfset userIdsNeedUpdate = []>
	
		<cfset closeBatchesData = {
			developerApplication = {
				developerId: worldpayApplication.developerId,
				version: worldpayApplication.version
			}
		}>
	
		<cfset oldUserPlans = []>
		<cfset newUserPlans = []>
		<cfset errorUserPlans = []>
		<cfset newBalanceBillings = []>
		<cfset logPaymentWorldpays = [[],[],[]]>

		<cfset ___c = 0>

		<cfloop query="userPlansQuery">
			<cfset planIndex = plansQuery.PlanId_int.IndexOf(JavaCast('int', PlanId_int)) + 1>
			<cfset userIndex = usersQuery.UserId_int.IndexOf(JavaCast('int', UserId_int)) + 1>
			<cfset customerIndex = customersQuery.UserId_int.IndexOf(JavaCast('int', UserId_int)) + 1>
	
			<cfif userIndex GT 0 AND planIndex GT 0 AND arrayFind(userIdsNeedUpdate, UserId_int) LE 0>
				
				<cfset arrayAppend(userIdsNeedUpdate, UserId_int)>
	
				<cfset _paymentType = customersQuery.PaymentType_vch[customerIndex]>
				<cfset _paymentType = structKeyExists(worldpayPaymentTypes,_paymentType) ? worldpayPaymentTypes[_paymentType] : worldpayPaymentTypes.UNKNOWN>
				<cfset _amount = plansQuery.Amount_dec[planIndex] + (BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec[planIndex])>
				
				<cfset recLogFile('Main info', 
					'UserPlanId_bi: #UserPlanId_bi#'&
					'#NL#User Id: ' & UserId_int &
					'#NL#Plan Id: ' & PlanId_int &
					'#NL#Payment amount: $#_amount#'
					)>
	
				<cfif _amount GT 0>
					<cfif customerIndex GT 0>
						<cfset paymentData = {  
						   amount = _amount,
						   paymentVaultToken = {  
						      customerId = customersQuery.UserId_int[customerIndex],
						      paymentMethodId = customersQuery.PaymentMethodID_vch[customerIndex],
						      paymentType = _paymentType
						   },
						   developerApplication = {  
						      developerId = worldpayApplication.developerId,
						      version = worldpayApplication.version
						   }
						}>
						
						<!--- log payment data --->
						<cfset recLogFile('Sub info', SerializeJSON(paymentData))>

						<cfset paymentError = 1>
						<cfset paymentResponse = {
							status_code = '',
				        	status_text = '',
				        	errordetail = '',
				        	filecontent = ''
						}>
							
						<cftry>
							<cfhttp url="#payment_url#" method="post" result="paymentResponse" port="443">
								<cfhttpparam type="header" name="content-type" value="application/json">
								<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
								<cfhttpparam type="body" value='#TRIM( SerializeJSON(paymentData) )#'>
							</cfhttp>
							<cfcatch>
								<cfset recLogFile('Sub info', 
									'Payment error: ' & payment_url & NL & 
									SerializeJSON(cfcatch)
									)>
							</cfcatch>
						</cftry>
						
						<!--- log payment data --->
						<cfset recLogFile('Sub info', SerializeJSON(paymentResponse))>
						
						<cfif paymentResponse.status_code EQ 200>
							<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
								<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
								
								<!--- log payment data --->
								<cfset recLogFile('Sub info', paymentResponse.filecontent)>
								
								<cfif paymentResponseContent.success>
							        <!--- close session: need check time --->
							        <!---<cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
								        <cfhttpparam type="header" name="content-type" value="application/json">
								        <cfhttpparam type="header" name="Authorization" value="#payment_header_Authorization#">
								        <cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
							        </cfhttp>--->
							        
							        <cfset paymentError = 0>
							        <cfset _endDate = DateAdd("m",1,EndDate_dt)>
							        <cfset arrayAppend(oldUserPlans,UserPlanId_bi)>
							        <cfset arrayAppend(newUserPlans,[
							        	1,
							        	UserId_int,
							        	PlanId_int,
							        	plansQuery.Amount_dec[planIndex],
							        	plansQuery.UserAccountNumber_int[planIndex],
							        	plansQuery.Controls_int[planIndex],
							        	plansQuery.KeywordsLimitNumber_int[planIndex],
							        	(
							        		plansQuery.Amount_dec[planIndex] GT 0 ?
							        		plansQuery.FirstSMSIncluded_int[planIndex] :
							        		FirstSMSIncluded_int
							        	),
							        	(
							        		plansQuery.Amount_dec[planIndex] GT 0 ?
							        		0 : UsedSMSIncluded_int
							        	),
							        	plansQuery.CreditsAddAmount_int[planIndex],
							        	plansQuery.PriceMsgAfter_dec[planIndex],
							        	plansQuery.PriceKeywordAfter_dec[planIndex],
							        	BuyKeywordNumber_int,
							        	EndDate_dt,
							        	_endDate
							        ])>
							        <cfif plansQuery.Amount_dec[planIndex] GT 0>
								        <cfset arrayAppend(newBalanceBillings, [
								        	UserId_int, 
								        	plansQuery.FirstSMSIncluded_int[planIndex]
							        	])>
							        </cfif>
							        <cfset arrayAppend(logPaymentWorldpays[1], [
							        	UserId_int,
							        	PlanId_int,
							        	'Recurring',
							        	BuyKeywordNumber_int,
							        	0,
							        	paymentResponseContent
						        	])>
								<cfelse>
									<cfset arrayAppend(errorUserPlans,UserPlanId_bi)>
								</cfif>
							<cfelse>
								<cfset arrayAppend(errorUserPlans,UserPlanId_bi)>
							</cfif>
						<cfelse>
							<cfset arrayAppend(errorUserPlans,UserPlanId_bi)>
						</cfif>
						
						<cfif arrayFind(errorUserPlans,UserPlanId_bi) GT 0>
					        <cfset arrayAppend(logPaymentWorldpays[2], [
					        	UserId_int,
					        	'Recurring',
					        	paymentResponse.status_code,
					        	paymentResponse.status_text,
					        	paymentResponse.errordetail,
					        	paymentResponse.filecontent,
					        	paymentData
				        	])>
						</cfif>
						
				        <cfset arrayAppend(logPaymentWorldpays[3], [
				        	now(),
				        	UserPlanId_bi,
				        	UserId_int,
				        	PlanId_int,
				        	usersQuery.FirstName_vch[userIndex],
				        	usersQuery.EmailAddress_vch[userIndex],
				        	_amount,
				        	paymentData,
				        	paymentError,
				        	paymentResponse
			        	])>
						
					<cfelse>
						<cfset recLogFile('Sub info', 'No Customer In Securenet')>
						<cfset arrayAppend(oldUserPlans,[UserPlanId_bi, -2])>
						<cfset arrayAppend(errorUserPlans,UserPlanId_bi)>
					</cfif>
				<cfelse>
			        <cfset _endDate = DateAdd("m",1,EndDate_dt)>
			        <cfset arrayAppend(oldUserPlans,UserPlanId_bi)>
			        <cfset arrayAppend(newUserPlans,[
			        	1,
			        	UserId_int,
			        	PlanId_int,
			        	plansQuery.Amount_dec[planIndex],
			        	plansQuery.UserAccountNumber_int[planIndex],
			        	plansQuery.Controls_int[planIndex],
			        	plansQuery.KeywordsLimitNumber_int[planIndex],
			        	(
			        		plansQuery.Amount_dec[planIndex] GT 0 ?
			        		plansQuery.FirstSMSIncluded_int[planIndex] :
			        		FirstSMSIncluded_int
			        	),
			        	UsedSMSIncluded_int,
			        	plansQuery.CreditsAddAmount_int[planIndex],
			        	plansQuery.PriceMsgAfter_dec[planIndex],
			        	plansQuery.PriceKeywordAfter_dec[planIndex],
			        	BuyKeywordNumber_int,
			        	EndDate_dt,
			        	_endDate
			        ])>
				</cfif>
			<cfelseif userIndex GT 0 AND planIndex GT 0>
				<cfset recLogFile('Main info', 
					'UserPlanId_bi: #UserPlanId_bi#' &
					'#NL#User Id: ' & UserId_int &
					'#NL#Plan Id: ' & PlanId_int &
					'#NL#Duplicate plan'
					)>
				<cfset arrayAppend(oldUserPlans,UserPlanId_bi)>
			<cfelse>
				<cfset recLogFile('Main info', 
					'UserPlanId_bi: #UserPlanId_bi#' &
					(userIndex LE 0 ? '#NL#No User Id: #UserId_int#' : '') &
					(planIndex LE 0 ? '#NL#No Plan Id: ' & PlanId_int : '')
					)>
				<cfif userIndex LE 0>
					<cfset arrayAppend(oldUserPlans,UserPlanId_bi)>
				<cfelse>
					<cfset arrayAppend(oldUserPlans,[UserPlanId_bi, -1])>
				</cfif>
				<cfset arrayAppend(errorUserPlans,UserPlanId_bi)>
			</cfif>

            <cfset ___c++>
            <cffile
                action = "write"
                file = "#dictPath#UserPlanCount.txt"
                output = "#___c#"
                addNewLine = "no"
                attributes = "normal"
                charset = "utf-8"
                fixnewline = "yes"
                mode = "777">
		</cfloop>
		
		<cfif !arrayIsEmpty(newUserPlans)>
			<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				INSERT INTO simplebilling.userplans (
					Status_int,
					UserId_int,
					PlanId_int,
					Amount_dec,
					UserAccountNumber_int,
					Controls_int,
					KeywordsLimitNumber_int,
					FirstSMSIncluded_int,
					UsedSMSIncluded_int,
					CreditsAddAmount_int,
					PriceMsgAfter_dec,
					PriceKeywordAfter_dec,
					BuyKeywordNumber_int,
					StartDate_dt,
					EndDate_dt
				) VALUES
				<cfset _comma = ''> 
				<cfloop array="#newUserPlans#" index="_plan">
					#_comma#(
						<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[3]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[4]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[5]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[6]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[7]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[8]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[9]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[10]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[11]#">,
			        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#_plan[12]#">,
			        	<cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[13]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[14]#">,
			        	<cfqueryparam cfsqltype="cf_sql_date" value="#_plan[15]#">
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>
			<!---<cfdump var="#insertUserPlans#">--->
			<!---<cfdump var="#userPlansResult#">--->
			<!--- userPlansResult.generatedKey --->
			<cfset recLogFile('Main info', 
				userPlansResult.RecordCount & ' User Plans Extended' &
				'#NL#User Plans: ' & userPlansResult.GENERATED_KEY
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(newBalanceBillings)>
			<cfset userIdList = ''>
			<cfquery name="updateBalanceBillings" datasource="#Session.DBSourceEBM#" result="balanceBillingsResult">
				UPDATE simplebilling.billing 
					SET Balance_int = (CASE
						<cfloop array="#newBalanceBillings#" index="_balance">
							<cfset userIdList = listAppend(userIdList,_balance[1])>
							WHEN UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#_balance[1]#"> 
								THEN <cfqueryparam cfsqltype="cf_sql_decimal" value="#_balance[2]#">
						</cfloop>
						ELSE Balance_int
					END)
				WHERE UserId_int IN (#userIdList#)
			</cfquery>
			<!---<cfdump var="#updateBalanceBillings#">--->
			<!---<cfdump var="#balanceBillingsResult#">--->
			<cfset recLogFile('Main info', 
				arrayLen(newBalanceBillings) & ' Balance Billing Updated' &
				'#NL#UserIds: ' & userIdList
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(oldUserPlans)>
			<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
				UPDATE simplebilling.userplans 
					SET Status_int = (CASE
						<cfset i = 1>
						<cfloop array="#oldUserPlans#" index="_plan">
							<cfif isArray(_plan)>
								WHEN UserPlanId_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[1]#"> 
									THEN <cfqueryparam cfsqltype="cf_sql_integer" value="#_plan[2]#">
								<cfset oldUserPlans[i] = _plan[1]>
							</cfif>
							<cfset i++>
						</cfloop>
						WHEN 0 = 1 THEN Status_int ELSE 0
					END) 
				WHERE UserPlanId_bi IN (#arrayToList(oldUserPlans, ',')#)
			</cfquery>
			<!---<cfdump var="#expiredUserPlans#">--->
			<!---<cfdump var="#userPlansResult#">--->
			<cfset recLogFile('Main info', 
				arrayLen(oldUserPlans) & ' User Plans Expired' &
				'#NL#User Plans: ' & arrayToList(oldUserPlans, ',')
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(errorUserPlans)>
			<cfset recLogFile('Main info', 
				arrayLen(errorUserPlans) & ' User Plans Error' &
				'#NL#User Plans: ' & arrayToList(errorUserPlans, ',')
				)>
		</cfif>
		
		<cfif !arrayIsEmpty(logPaymentWorldpays[3])>
			<cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#" result="logPaymentWorldpaysResult">
				INSERT INTO `simplebilling`.`recurringlog` (
					`PaymentDate_dt`,
					`Status_ti`,
					`UserPlanId_bi`,
					`UserId_int`,
					`PlanId_int`,
					`FirstName_vch`,
					`EmailAddress_vch`,
					`Amount_dec`,
					`PaymentData_txt`,
					`PaymentResponse_txt`,
					`LogFile_vch`
				) VALUES
				<cfset _comma = ''> 
				<cfloop array="#logPaymentWorldpays[3]#" index="_log">
					#_comma#(
		        		<cfqueryparam CFSQLTYPE="cf_sql_timestamp" VALUE="#_log[1]#">,
		        		<cfqueryparam cfsqltype="cf_sql_integer" value="#!_log[9]#">,
		        		<cfqueryparam cfsqltype="cf_sql_integer" value="#_log[2]#">,
		        		<cfqueryparam cfsqltype="cf_sql_integer" value="#_log[3]#">,
		        		<cfqueryparam cfsqltype="cf_sql_integer" value="#_log[4]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[5]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6]#">,
		        		<cfqueryparam cfsqltype="cf_sql_decimal" value="#_log[7]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#SerializeJSON(_log[8])#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#SerializeJSON(_log[10])#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_fcm#/#_now#.txt">
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>
		</cfif>
		
		<cfif !arrayIsEmpty(logPaymentWorldpays[2])>
			<cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#" result="logPaymentWorldpaysResult">
				INSERT INTO `simplebilling`.`log_payment_worldpay` (
					`userId_int`,
					`moduleName`,
					`status_code`,
					`status_text`,
					`errordetail`,
					`filecontent`,
					`paymentdata`,
					`created`
				) VALUES
				<cfset _comma = ''> 
				<cfloop array="#logPaymentWorldpays[2]#" index="_log">
					#_comma#(
		        		<cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#_log[1]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[2]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[3]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[4]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[5]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6]#">,
		        		<cfqueryparam cfsqltype="cf_sql_varchar" value="#serializeJSON(_log[7])#">,
		        		NOW()
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>
		</cfif>
		
		<cfif !arrayIsEmpty(logPaymentWorldpays[1])>
			<cfquery name="insertLogPaymentWorldpays" datasource="#Session.DBSourceEBM#" result="logPaymentWorldpaysResult">
				INSERT INTO `simplebilling`.`payment_worldpay` (
					`userId_int`,
					`planId`, 
					`moduleName`,
					`bynumberKeyword`,
					`bynumberSMS`,
					`transactionType`, 
					`customerId`, 
					`orderId`, 
					`transactionId`, 
					`authorizationCode`, 
					`authorizedAmount`, 
					`paymentTypeCode`, 
					`paymentTypeResult`, 
					`transactionData_date`, 
					`transactionData_amount`, 
					`creditCardType`, 
					`cardNumber`, 
					`avsCode`, 
					`cardHolder_FirstName`, 
					`cardHolder_LastName`, 
					`billAddress_line1`, 
					`billAddress_city`, 
					`billAddress_state`, 
					`billAddress_zip`, 
					`billAddress_company`, 
					`billAddress_phone`, 
					`email`, 
					`method`, 
					`responseText`,
					`created`
				) VALUES
				<cfset _comma = ''> 
				<cfloop array="#logPaymentWorldpays[1]#" index="_log">
					#_comma#(
	                    <cfqueryparam CFSQLTYPE="cf_sql_integer" VALUE="#_log[1]#">,
	                    <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[2]#">,
	                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[3]#">,
	                    <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[4]#">,
	                    <cfqueryparam cfsqltype="cf_sql_integer" value="#_log[5]#">,
	                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionType#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.customerId#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.orderId#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionId#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.authorizationCode#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.authorizedAmount#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.paymentTypeCode#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.paymentTypeResult#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionData.date#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.transactionData.amount#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.creditCardType#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardNumber#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.avsCode#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardHolder_FirstName#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.cardHolder_LastName#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.line1#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.city#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.state#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.zip#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.company#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.billAddress.phone#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.email#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.method#">, 
	        			<cfqueryparam cfsqltype="cf_sql_varchar" value="#_log[6].transaction.responseText#">,
		        		NOW()
					)
					<cfset _comma = ','> 
				</cfloop>
			</cfquery>
		</cfif>
		
		<cftry>
		    <cfhttp url="#close_batches_url#" method="post" result="closeBatchesRequestResult" port="443">
		        <cfhttpparam type="header" name="content-type" value="application/json">
		        <cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
		        <cfhttpparam type="body" value='#TRIM( SerializeJSON(closeBatchesData) )#'>
		    </cfhttp>
			<cfcatch>
				<cfset recLogFile('Main info', 
					'Payment error: ' & variables.close_batches_url & NL & 
					SerializeJSON(cfcatch)
					)>
			</cfcatch>
	    </cftry>
	
		<!--- remove updatingUserPlanList --->
		<cfset updatingUserPlanList = FileRead(dictPath & 'UserPlanList.txt')>
		<cfset updatingUserPlanList = Replace(updatingUserPlanList, userPlanIdList, "", "all")>
		<cfset updatingUserPlanList = REReplace(updatingUserPlanList, "^(\r\n|\n\r|\n|\r)+|(\r\n|\n\r|\n|\r)+$", "", "all")>
		<cfset updatingUserPlanList = Trim(updatingUserPlanList)>
		
		<cffile  
			action = "write" 
			file = "#dictPath#UserPlanList.txt" 
			output = "#updatingUserPlanList#" 
			addNewLine = "no" 
			attributes = "normal"
			charset = "utf-8"  
			fixnewline = "yes" 
			mode = "777">
		
		
	<!---<cfelse>--->
	</cfif>

	<cfcatch>
		<cfset recLogFile('Main info', 
			'Application error' & NL & 
			SerializeJSON(cfcatch)
			)>
	</cfcatch>

</cftry>

<cfset recLogFile('Root', 'End recurring')>