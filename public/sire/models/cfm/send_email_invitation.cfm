<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpDialerIPAddr" default="send_sms_invitation">
<cfparam name="inpVerboseDebug" default="0">
<cfparam name="inpDisableSend" default="0">

<!---
	Process queued up referral

	SMS and Email

	<!--- inpTypeMask 1=SMS 2=Email --->

	These are Status Flags to tell what state each entry in the Referral Queue is in. 
	0 = Failed
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Complete by Process
	There will be more status types as the system is built out. 
--->

<cfset totalProcTimeStart = GetTickCount() />
<cfset serverStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />

<cfset processedRecordCount = 0>
<cfset duplicateRecordCount = 0>
<cfset errorCount = 0>
<cfset lastErrorDetails = ''>

<cftry>

	<cfset exitRecordLoop = 0>

	<!--- Record(s) post --->
	<cfif inpNumberOfDialsToRead GT 0>

        <cfquery name="GetElligibleContacts" datasource="#Session.DBSourceEBM#">
                SELECT
                	rsq.Id_int,
                    rsq.UUID_vch,
                    rsq.Type_ti,
                    rsq.UserId_int,
                    rsq.ContactString_vch,
                    rsq.Status_ti,
                    rsq.Content_txt
            	FROM
                	simplequeue.sire_referral_msg_queue rsq
                WHERE
                    rsq.Status_ti = 1
                AND
                	rsq.Type_ti = 2
                ORDER BY
                	Id_int
            	LIMIT
                	#inpNumberOfDialsToRead#
        </cfquery>

        <!--- Reset Defaults --->
        <cfset inpDialString ="">
        <cfset inpUserId = 0>



        <cfloop query="GetElligibleContacts">

        	<!--- Reset Defaults --->
            <cfset currResult = 0>

        	<!--- Check for bad records TZ, Etc--->
        
            <cfif inpVerboseDebug GT 0>
            	<cfoutput>                  
                    inpDialString = #GetElligibleContacts.ContactString_vch# <BR />
                    userID = #GetElligibleContacts.UserId_int#<BR />
                    NOW() = #NOW()# <BR />
                    inpType = #GetElligibleContacts.Type_ti#<BR />
                    Content = #HTMLCodeFormat(GetElligibleContacts.Content_txt)# <BR />
                </cfoutput>
            </cfif>

            <!--- Update Queue to in process (Status_ti=2) --->
            <cfset inpId = GetElligibleContacts.Id_int />
            <cfset inpStatus = 2 />            
            <cfinclude template="update_email_invitation_queue.cfm" />

            <!--- Process Record Here --->
            <cftry>
                <cfif inpVerboseDebug GT 0>
					<cfoutput>
                        GetElligibleContacts.Content = <code>#GetElligibleContacts.Content_txt#</code> <BR />
                    </cfoutput>
                </cfif>

                <cfif inpDisableSend EQ 0>

                 <!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />

                    <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="userInfoById">
                        <cfinvokeargument name="inpUserId" value="#GetElligibleContacts.UserId_int#">
                    </cfinvoke>

                    <cfif userInfoById.USERNAME EQ '' AND userInfoById.LASTNAME EQ ''>
                        <cfset userInfoById.USERNAME = "A Friend">
                    </cfif>

                    <cfif inpVerboseDebug GT 0>
                        <cfoutput>
                            <cfdump var="#userInfoById#">
                        </cfoutput>
                    </cfif>

		    <cfinvoke method="SendMailByStringContent" component="public.sire.models.cfc.sendmail" returnvariable="retValSendMail">
                        <cfinvokeargument name="inpTo" value="#GetElligibleContacts.ContactString_vch#" />
                        <cfinvokeargument name="inpSubject" value="#userInfoById.USERNAME# #userInfoById.LASTNAME# has invited you to try Sire Mobile!" />
                        <cfinvokeargument name="inpContent" value="#GetElligibleContacts.Content_txt#" />
                    </cfinvoke>

                </cfif>

                <!--- Update Queue to finished processing (Status_ti=3) --->
				<cfset inpId = GetElligibleContacts.Id_int />
                <cfset inpStatus = 3 />
                <cfinclude template="update_email_invitation_queue.cfm" />

                <cfcatch type="any">

                    <cfset errorCount = errorCount + 1>

                    <!--- Log error? --->
                    ********** ERROR on LOOP **********<BR>
				    <cfif inpVerboseDebug GT 0>
						<cfoutput>
				            <cfdump var="#cfcatch#">
				        </cfoutput>
				    </cfif>


                    <!--- Update Queue to finished processing (StatusType_ti=6) --->
                    <cfset inpId = GetElligibleContacts.Id_int />
                	<cfset inpStatus = 0 />
                    <cfinclude template="update_email_invitation_queue.cfm" />


                    <cfset lastErrorDetails = SerializeJSON(cfcatch) />

                </cfcatch>

            </cftry>

			<!--- Increment is duplicate record is found. --->
			<cfif currResult eq -2>
				<cfset duplicateRecordCount = duplicateRecordCount + 1>
			</cfif>

            <!--- Increment processed --->
			<cfset processedRecordCount = processedRecordCount + 1>

        </cfloop>

    </cfif>


	<cfset serverEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
    <cfset totalProcTime = (GetTickCount() - totalProcTimeStart) />


    <!--- Report Results to Table --->
    <cfquery name="AddProductionLogEntry" datasource="#Session.DBSourceEBM#">
        INSERT INTO
            simplequeue.sire_contact_queue_proc_log
            (
                LogType_int,
                DistributionProcessId_int,
                Check_dt,
                RunTimeMS_int,
                Start_dt,
                End_dt,
                CountProcessed_int,
                CountInvalid_int,
                CountDistributed_int,
                CountSuppressed_int,
                CountDuplicate_int,
                CountErrors_int,
                inpMod_int,
                DoModValue_int,
                inpNumberOfDialsToRead_int
            )
		VALUES
            (
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="40">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                NOW(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#totalProcTime#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#serverStartTime#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#serverEndTime#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#processedRecordCount#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#errorCount#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#">
            )
    </cfquery>


<cfcatch TYPE="any"> <!--- Main Catch --->
	<cfset errorCount = errorCount + 1>
	********** ERROR on PAGE **********<BR>
    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>
</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
</cfoutput>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="101">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="send_sms_invitation - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lastErrorDetails#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>
