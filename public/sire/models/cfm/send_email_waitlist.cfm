<cfparam name="inpNumberOfDialsToRead" default = "100">
<cfparam name="inpDialerIPAddr" default="send_email_waitlist">
<cfparam name="inpVerboseDebug" default="0">
<cfparam name="inpDisableSend" default="0">

<!---
	Process queued up referral

	SMS and Email

	<!--- inpTypeMask 1=SMS 2=Email --->

	These are Status Flags to tell what state each entry in the Referral Queue is in. 
	0 = Failed
	1 = Queued
	2 = Queued to go to fulfillment - this means processing has started
	3 = Complete by Process
	There will be more status types as the system is built out. 
--->

<cfset totalProcTimeStart = GetTickCount() />
<cfset serverStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />

<cfset processedRecordCount = 0>
<cfset duplicateRecordCount = 0>
<cfset errorCount = 0>
<cfset lastErrorDetails = ''>

<cftry>

	<cfset exitRecordLoop = 0>

	<!--- Record(s) post --->
	<cfif inpNumberOfDialsToRead GT 0>

        <cfquery name="GetEmailToSend" datasource="#Session.DBSourceEBM#">
            SELECT
            	Id_int,
                UserId_int,
                EmailAddress_vch,
                EmailSubject_vch,
                EmailMessage_txt,
                Status_ti
        	FROM
            	simplequeue.sire_waitlist_email_queue
            WHERE
                Status_ti = 1
            ORDER BY
            	Id_int
        	LIMIT
            	#inpNumberOfDialsToRead#
        </cfquery>

        <!--- Reset Defaults --->
        <cfset inpDialString ="">
        <cfset inpUserId = 0>



        <cfloop query="GetEmailToSend">

        	<!--- Reset Defaults --->
            <cfset currResult = 0>

        	<!--- Check for bad records TZ, Etc--->
        
            <cfif inpVerboseDebug GT 0>
            	<cfoutput>                  
                    inpEmail = #GetEmailToSend.EmailAddress_vch# <BR />
                    userID = #GetEmailToSend.UserId_int#<BR />
                    NOW() = #NOW()# <BR />
                    Subject = #GetEmailToSend.EmailSubject_vch# </BR>
                    Message = #HTMLCodeFormat(GetEmailToSend.EmailMessage_txt)# <BR />
                </cfoutput>
            </cfif>

            <!--- Update Queue to in process (Status_ti=2) --->
            <cfset inpId = GetEmailToSend.Id_int />
            <cfset inpStatus = 2 />            
            <cfinclude template="update_email_waitlist_queue.cfm" />

            <!--- Process Record Here --->
            <cftry>
                <cfif inpVerboseDebug GT 0>
					<cfoutput>
                        GetEmailToSend.Content = <code>#GetEmailToSend.EmailMessage_txt#</code> <BR />
                    </cfoutput>
                </cfif>

                <cfif inpDisableSend EQ 0>

                 <!--- Track each requests processing tiume to watch for errors on remote end --->
                    <cfset intHTTPStartTime = GetTickCount() />

                    <cfinvoke method="GetUserInfoById" component="public.sire.models.cfc.userstools" returnvariable="userInfoById">
                        <cfinvokeargument name="inpUserId" value="#GetEmailToSend.UserId_int#">
                    </cfinvoke>

                    <cfif userInfoById.USERNAME EQ '' AND userInfoById.LASTNAME EQ ''>
                        <cfset userInfoById.USERNAME = "A Friend">
                    </cfif>

                    <cfif inpVerboseDebug GT 0>
                        <cfoutput>
                            <cfdump var="#userInfoById#">
                        </cfoutput>
                    </cfif>

                <!--- <cfinvoke method="SendMailByStringContent" component="public.sire.models.cfc.sendmail" returnvariable="retValSendMail">
                    <cfinvokeargument name="inpTo" value="#GetEmailToSend.EmailAddress_vch#" />
                    <cfinvokeargument name="inpSubject" value="#userInfoById.USERNAME# #userInfoById.LASTNAME# has invited you to try Sire Mobile!" />
                    <cfinvokeargument name="inpContent" value="#GetEmailToSend.Content_txt#" />
                </cfinvoke> --->

                <cfset dataMail = {
                    MESSAGE: GetEmailToSend.EmailMessage_txt
                }/>
                <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
                    <cfinvokeargument name="to" value="#GetEmailToSend.EmailAddress_vch#">
                    <cfinvokeargument name="type" value="html">
                    <cfinvokeargument name="subject" value="#GetEmailToSend.EmailSubject_vch#">
                    <cfinvokeargument name="template" value="/session/sire/views/emailtemplates/mail_template_confirmation_alert_waitlist.cfm">
                    <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
                </cfinvoke>

                </cfif>

                <!--- Update Queue to finished processing (Status_ti=3) --->
				<cfset inpId = GetEmailToSend.Id_int />
                <cfset inpStatus = 3 />
                <cfinclude template="update_email_waitlist_queue.cfm" />

                <cfcatch type="any">

                    <cfset errorCount = errorCount + 1>

                    <!--- Log error? --->
                    ********** ERROR on LOOP **********<BR>
				    <cfif inpVerboseDebug GT 0>
						<cfoutput>
				            <cfdump var="#cfcatch#">
				        </cfoutput>
				    </cfif>


                    <!--- Update Queue to finished processing (StatusType_ti=6) --->
                    <cfset inpId = GetEmailToSend.Id_int />
                	<cfset inpStatus = 0 />
                    <cfinclude template="update_email_waitlist_queue.cfm" />


                    <cfset lastErrorDetails = SerializeJSON(cfcatch) />

                </cfcatch>

            </cftry>

			<!--- Increment is duplicate record is found. --->
			<cfif currResult eq -2>
				<cfset duplicateRecordCount = duplicateRecordCount + 1>
			</cfif>

            <!--- Increment processed --->
			<cfset processedRecordCount = processedRecordCount + 1>

        </cfloop>

    </cfif>


	<cfset serverEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
    <cfset totalProcTime = (GetTickCount() - totalProcTimeStart) />



<cfcatch TYPE="any"> <!--- Main Catch --->
	<cfset errorCount = errorCount + 1>
	********** ERROR on PAGE **********<BR>
    <cfif inpVerboseDebug GT 0>
		<cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>
</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

<cfoutput>
	End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
</cfoutput>

<cftry>
	<cfif lastErrorDetails NEQ ''>
		<!--- 	Write to Error log  --->
		<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
	        INSERT INTO
                simplequeue.errorlogs
                (
                    ErrorNumber_int,
                    Created_dt,
                    Subject_vch,
                    Message_vch,
                    TroubleShootingTips_vch,
                    CatchType_vch,
                    CatchMessage_vch,
                    CatchDetail_vch,
                    Host_vch,
                    Referer_vch,
                    UserAgent_vch,
                    Path_vch,
                    QueryString_vch
                )
            VALUES
                (
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="101">,
                    NOW(),
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="send_email_waitlist - lastErrorDetails is Set - See Catch Detail for more info">,
                    '',
                    '',
                    '',
                    '',
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#lastErrorDetails#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                    <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
                );
		</cfquery>
	</cfif>

	<cfcatch></cfcatch>
</cftry>
