<cfparam name="inpNumberOfDialsToRead" default = "12">
<cfparam name="CurrDialerArrayIndex" default="0">
<cfparam name="DeviceCount" default="0">
<cfparam name="inpMod" default="1">
<cfparam name="DoModValue" default="0">
<cfparam name="inpDistributionProcessId" default="0">
<cfparam name="inpDialerIPAddr" default="send_sms_invitation">
<cfparam name="inpVerboseDebug" default="0">
<cfparam name="inpDisableSend" default="0">
<cfparam name="INPGROUPID" default="0">
<cfparam name="INPDISTRIBUTIONPROCESSIDLOCAL" default="0">
<cfparam name="inpRegisteredDeliverySMS" default="0">

<cfinclude template="/public/sire/configs/paths.cfm">
<cfinclude template="/session/cfc/csc/constants.cfm">

<!---
    Process queued up messages

    Sire version assumptions

    SMS only - no fulfilment device in the middle



    <!--- inpTypeMask 1=Voice 2=email 3=SMS--->


    Run multi threaded with inpMod and DoModValue


    Sample 10 threads

    inpMod = 10

    DoModValue = 0-9
    ... so 10 CRON jobs

    inpMod = 10 & DoModValue = 0
    inpMod = 10 & DoModValue = 1
    inpMod = 10 & DoModValue = 2
    inpMod = 10 & DoModValue = 3
    inpMod = 10 & DoModValue = 4
    inpMod = 10 & DoModValue = 5
    inpMod = 10 & DoModValue = 6
    inpMod = 10 & DoModValue = 7
    inpMod = 10 & DoModValue = 8
    inpMod = 10 & DoModValue = 9






    These are Status Flags to tell what state each entry in the Contact Queue is in.
    0=Paused
    1 = Queued
    2 = Queued to go to fulfillment - this means processing has started
    3 = Now being fulfilled
    4 = Blocked due BR rule (duplicates) on distribution
    5 = Complete by Process
    6 = Blocked due BR rule (bad XML) on distribution
    7 = SMS Stop request killed a queued message
    8 = Fulfilled by Real Time request
    100 - Queue hold due to distribution of audio error(s)
    There will be more status types as the system is built out.



--->

<cfset TotalProcTimeStart = GetTickCount() />
<cfset ServerStartTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />


<cfset TQCheckPoint = 0>
<cfset ProcessedRecordCount = 0>
<cfset SuppressedCount = 0>
<cfset DuplicateRecordCount = 0>
<cfset InvalidTZRecordCount = 0>
<cfset InvalidRecordCount = 0>
<cfset DistributionCount = 0>
<cfset DistributionCountRegular = 0>
<cfset DistributionCountSecurity = 0>
<cfset MaxInvalidRecords = 1>
<cfset ErrorCount = 0>
<cfset LastErrorDetails = ''>
<cfset DeliveryServiceComponentPath = "#Session.SessionCFCPath#.csc.csc" />
<cftry>

    <cfset ExitRecordLoop = 0>

    <!--- Record(s) post --->
    <cfif inpNumberOfDialsToRead GT 0>

        <cfquery name="GetElligibleMessages" datasource="#Session.DBSourceEBM#">
                SELECT
                    dts.DTSId_int,
                    dts.BatchId_bi,
                    DTS_UUID_vch,
                    dts.TypeMask_ti,
                    dts.TimeZone_ti,
                    dts.CurrentRedialCount_ti,
                    dts.UserId_int,
                    dts.PushLibrary_int,
                    dts.PushElement_int,
                    dts.PushScript_int,
                    dts.PushSkip_int,
                    dts.CampaignTypeId_int,
                    dts.ContactString_vch,
                    dts.XMLControlString_vch,
                    dts.Scheduled_dt
                FROM
                    simplequeue.contactqueue dts JOIN
                    simpleobjects.scheduleoptions so ON (so.BatchId_bi = dts.BatchId_bi)
                    <!---FORCE INDEX (IDXComboDistribution)--->
                WHERE
                    dts.DTSStatusType_ti = 1
                AND
                    DTSId_int MOD #inpMod# = #DoModValue#
                AND
                    so.enabled_ti = 1
                AND
                    so.STARTHOUR_TI <= (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                AND
                    so.ENDHOUR_TI > (EXTRACT(HOUR FROM NOW()) - dts.TimeZone_ti + 31)
                AND
                    so.#DateFormat(NOW(),"dddd")#_ti = 1
                AND
                    Scheduled_dt < NOW()
                AND
                    NOW() BETWEEN so.Start_dt and so.Stop_dt
                AND
                    DistributionProcessId_int = #inpDistributionProcessId#
                AND
                    ContactType_int = 10 -- SMS Invite type
                ORDER BY
                    DTSId_int ASC
                LIMIT
                    #inpNumberOfDialsToRead#
        </cfquery>

        <!--- Reset Defaults --->
        <cfset INPBATCHID = "0">
        <cfset inpDialString ="">
        <cfset inpDialString2 ="">
        <cfset TimeZone_ti = "">
        <cfset RedialCount_int = 0>
        <cfset XMLControlString_vch = "">
        <cfset FileSeqNumber_int = "0">
        <cfset UserSpecifiedData_vch = "">
        <cfset inpUserId = 0>
        <cfset inpCampaignTypeId = "30">
        <cfset inpLibId = -1>
        <cfset inpEleId = -1>
        <cfset inpScriptId = -1>
        <cfset inpPushSkip = 0>
        <cfset inpDQDTSId = -1>
        <cfset inpCurrUUID = "">
        <cfset ESIID = "-14"/>


        <cfloop query="GetElligibleMessages">

            <!--- Reset Defaults --->
            <cfset CurrResult = 0>
            <cfset inpInvalidNumber = 0>
            <cfset EstimatedCost = 0>
            <cfset ServiceInputFlag = 1>
            <cfset inpPostToQueueForWebServiceDeviceFulfillment = 0>
            <cfset NEXTBATCHID = GetElligibleMessages.BatchId_bi>
            <cfset INPBATCHID = GetElligibleMessages.BatchId_bi>
            <cfset inpSkipLocalUserDNCCheck = 1>
            <cfset BlockGroupMembersAlreadyInQueue = 0>
            <cfset inpSkipLocalUserDNCCheck = 0>
            <cfset QueuedScheduledDate = "#GetElligibleMessages.Scheduled_dt#">
            <cfset COUNTQUEUEDUPSMS = 0>
            <cfset inpIRETypeAdd = 10 />
            <cfset inpContactTypeId = 10 />
            <cfset DebugStr = "Start" />


            <!--- Check for business rules, max 5 invite per day per user, no same phone number invite within 30 days --->

            <cfquery name="checkUserInviteQuota" datasource="#Session.DBSourceEBM#">
                SELECT
                    ContactString_vch
                FROM
                    simplequeue.contactqueue
                WHERE
                    DTSStatusType_ti IN ('1', '2', '5')
                AND
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetElligibleMessages.UserId_int#">
                AND
                    DATE(Scheduled_dt) = CURDATE()
                AND
                    ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="10"> -- SMS Invite type
                ORDER BY
                    DTSId_int
            </cfquery>

            <cfquery name="checkNumber30DaysLimit" datasource="#Session.DBSourceEBM#">
                SELECT
                    ContactString_vch
                FROM
                    simplequeue.contactqueue
                WHERE
                    DTSStatusType_ti IN ('1', '2', '5')
                AND
                    ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetElligibleMessages.ContactString_vch#">
                AND
                    DATEDIFF(CURDATE(), DATE(Scheduled_dt)) <= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#_SameNumberInviteDuration#">
                AND
                    ContactType_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="10"> -- SMS Invite type
                AND
                    DTSId_int != <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetElligibleMessages.DTSId_int#">
                ORDER BY
                    DTSId_int
            </cfquery>

            <cfif checkUserInviteQuota.RECORDCOUNT LT _MaxSMSInvite AND checkNumber30DaysLimit.RECORDCOUNT LT 1>

                <!--- Check for bad records TZ, Etc--->
                <cfif inpVerboseDebug GT 0>
                    <cfoutput>
                        inpDialString = #GetElligibleMessages.ContactString_vch# <BR>
                        TimeZone_ti = #GetElligibleMessages.TimeZone_ti# <BR>
                        NOW() = #NOW()# <BR>
                        INPBATCHID = #GetElligibleMessages.BatchId_bi#<BR>
                        inpTypeMask_ti = #GetElligibleMessages.TypeMask_ti#<BR />
                        XMLControlString_vch RAWDB = #HTMLCodeFormat(GetElligibleMessages.XMLControlString_vch)# <BR />
                    </cfoutput>
                </cfif>

                <!--- Ignore Time Zone for eMail and SMS --->

                <cfif GetElligibleMessages.TimeZone_ti LTE 0>
                    <!--- Default --->
                    <cfset GetElligibleMessages.TimeZone_ti = 31>
                </cfif>

                <cfset XMLControlString_vch = deserializeJSON(XMLControlString_vch) />

                <cfif inpVerboseDebug GT 0>
                    <cfoutput>
                       XMLControlString_vch After = #HTMLCodeFormat(GetElligibleMessages.XMLControlString_vch)# <BR />
                    </cfoutput>
                </cfif>


                <!--- Update Queue to in process (DTSStatusType_ti=2) --->
                <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                <cfset inpDTSStatus = 2 />
                <cfinclude template="update_sms_invitation_queue.cfm" />

                <!--- Process Record Here --->
                <cftry>

                    <cfset #FORM.inpFriendName# = XMLControlString_vch.inpFriendName />
                    <cfset #FORM.inpSignupURL# = XMLControlString_vch.inpSignupURL />

                    <cfset ServiceRequestdataout =  QueryNew("  UserId_int,
                                                                ContactTypeId_int,
                                                                ContactString_vch,
                                                                TimeZone_int,
                                                                OperatorId_int
                                                                 ", "VarChar,VarChar,VarChar,VarChar,VarChar")>

                    <cfset QueryAddRow(ServiceRequestdataout) />


                    <!--- Add form data  --->
                    <cfloop collection="#FORM#" item="whichField">

                       <!--- Only add fields not already on list of standard fields --->
                       <cfif NOT structKeyExists(ServiceRequestdataout, '#whichField#')  >

                            <!---<cfoutput>#whichField# = #FORM[whichField]#</cfoutput><br>--->

                            <cfset VariableNamesArray = ArrayNew(1)>
                            <cfset ArraySet(VariableNamesArray, 1, 1, "#FORM[whichField]#")>
                            <cfset QueryAddColumn(ServiceRequestdataout, whichField, "VarChar", VariableNamesArray)>

                        </cfif>

                    </cfloop>

                    <cfset QuerySetCell(ServiceRequestdataout, "UserId_int", "#GetElligibleMessages.UserId_int#") />
                    <cfset QuerySetCell(ServiceRequestdataout, "ContactString_vch", "#GetElligibleMessages.ContactString_vch#") />
                    <cfset QuerySetCell(ServiceRequestdataout, "ContactTypeId_int", "10") />
                    <cfset QuerySetCell(ServiceRequestdataout, "TimeZone_int", "#GetElligibleMessages.TimeZone_ti#") />
                    <cfset QuerySetCell(ServiceRequestdataout, "OperatorId_int", "0") />

                    <cfquery name="GetRawDataCount" dbTYPE="query">
                        SELECT
                            COUNT(*) AS TOTALCOUNT
                        FROM
                            ServiceRequestdataout
                    </cfquery>

                    <cfif GetRawDataCount.TOTALCOUNT EQ "">

                       <cfthrow MESSAGE="Distribution Error" TYPE="Any" detail="(#GetRawDataCount.TOTALCOUNT#) (#CurrUserId#) No data elligable to send - check your contact strings." errorcode="-5">

                    </cfif>

                    <cfinclude template="/session/cfc/Includes/LoadQueueSMS_Dynamic.cfm">

                    <cfif UserLocalDNCBlocked GT 0>
                        <!--- Update Queue to finished processing (DTSStatusType_ti=5) --->
                        <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                        <cfset inpDTSStatus = 6 />
                        <cfinclude template="update_sms_invitation_queue.cfm" />
                    </cfif>


                    <!--- Update Queue to finished processing (DTSStatusType_ti=5) --->
                    <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                    <cfset inpDTSStatus = 5 />
                    <cfinclude template="update_sms_invitation_queue.cfm" />

                    <cfcatch type="any">

                        <cfset ErrorCount = ErrorCount + 1>

                        <!--- Log error? --->
                        ********** ERROR on LOOP **********<BR>
                        <cfif inpVerboseDebug GT 0>
                            <cfoutput>
                                <cfdump var="#cfcatch#">
                            </cfoutput>
                        </cfif>


                        <!--- Update Queue to finished processing (DTSStatusType_ti=6) --->
                        <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                        <cfset inpDTSStatus = 6 />
                        <cfinclude template="update_sms_invitation_queue.cfm" />


                        <cfset LastErrorDetails = SerializeJSON(cfcatch) />

                    </cfcatch>

                </cftry>

                <!--- Increment if TimeZone is bad. --->
                <cfif TimeZone_ti EQ "">
                    <cfset InvalidTZRecordCount = InvalidTZRecordCount + 1>
                </cfif>

                <!--- Increment is duplicate record is found. --->
                <cfif CurrResult eq -2>
                    <cfset DuplicateRecordCount = DuplicateRecordCount + 1>
                </cfif>

                <!--- Increment distribution count. --->
                <cfset DistributionCount = DistributionCount + 1>

                <!--- Increment processed --->
                <cfset ProcessedRecordCount = ProcessedRecordCount + 1>

                <cfscript>
                    sleep(5000);
                </cfscript>
            <cfelse>
                <!--- Update Queue to failed (DTSStatusType_ti=6) --->
                <cfset inpDTSId = GetElligibleMessages.DTSId_int />
                <cfset inpDTSStatus = 6 />
                <cfinclude template="update_sms_invitation_queue.cfm" />

                <cfif inpVerboseDebug GT 0>
                    <cfif checkUserInviteQuota.RECORDCOUNT GTE _MaxSMSInvite>
                        <cfoutput>
                            ContactString_vch = #GetElligibleMessages.ContactString_vch#<BR>
                            Invite quota exceeded<BR>
                        </cfoutput>
                    </cfif>
                    <cfif checkNumber30DaysLimit.RECORDCOUNT GT 0>
                        <cfoutput>
                            ContactString_vch = #GetElligibleMessages.ContactString_vch#<BR>
                            Contact invited within 30 days<BR>
                        </cfoutput>
                    </cfif>
                </cfif>
            </cfif>
        </cfloop>

    </cfif>


    <cfset ServerEndTime = "#LSDateFormat(NOW(), 'yyyy-mm-dd')# #LSTimeFormat(NOW(), 'HH:mm:ss')#" />
    <cfset TotalProcTime = (GetTickCount() - TotalProcTimeStart) />


    <!--- Report Results to Table --->
    <cfquery name="AddProductionLogEntry" datasource="#Session.DBSourceEBM#">
        INSERT INTO simplequeue.sire_contact_queue_proc_log
        (
            LogType_int,
            DistributionProcessId_int,
            Check_dt,
            RunTimeMS_int,
            Start_dt,
            End_dt,
            CountDistributed_int,
            CountInvalid_int,
            CountProcessed_int,
            CountSuppressed_int,
            CountDuplicate_int,
            CountErrors_int,
            inpMod_int,
            DoModValue_int,
            inpNumberOfDialsToRead_int
        )
        VALUES
        (
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="40">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDistributionProcessId#">,
            NOW(),
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#TotalProcTime#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerStartTime#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#ServerEndTime#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ProcessedRecordCount#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="0">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#ErrorCount#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpMod#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#DoModValue#">,
            <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpNumberOfDialsToRead#">

        )
    </cfquery>


<cfcatch TYPE="any"> <!--- Main Catch --->

    <cfset ErrorCount = ErrorCount + 1>

    ********** ERROR on PAGE **********<BR>
    <cfif inpVerboseDebug GT 0>
        <cfoutput>
            <cfdump var="#cfcatch#">
        </cfoutput>
    </cfif>

    <cfset ENA_Message = "ERROR on #GetCurrentTemplatePath()# TQCheckPoint = #TQCheckPoint#">
    <cfset InvalidRecordCount = InvalidRecordCount + 1>
    <!--- <cfinclude template="..\NOC\act_EscalationNotificationActions.cfm"> --->


</cfcatch> <!--- End Main Catch --->
</cftry> <!--- End Main try ---> 

<cfoutput>
    End time - #inpDialerIPAddr# - #LSDateFormat(Now(), 'yyyy-mm-dd')# #LSTimeFormat(Now(), 'HH:mm:ss')# <BR />
</cfoutput>

<cftry>

    <cfif LastErrorDetails NEQ ''>
        <!---   Write to Error log  --->
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.errorlogs
            (
                ErrorNumber_int,
                Created_dt,
                Subject_vch,
                Message_vch,
                TroubleShootingTips_vch,
                CatchType_vch,
                CatchMessage_vch,
                CatchDetail_vch,
                Host_vch,
                Referer_vch,
                UserAgent_vch,
                Path_vch,
                QueryString_vch
            )
            VALUES
            (
                101,
                NOW(),
                'send_sms_invitation - LastErrorDetails is Set - See Catch Detail for more info',
                '',
                '',
                '',
                '',
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LastErrorDetails#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_HOST, 2048)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_REFERER, 2048)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.HTTP_USER_AGENT, 2048)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.PATH_TRANSLATED, 2048)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#LEFT(CGI.QUERY_STRING, 2048)#">
            );
        </cfquery>
    </cfif>

    <cfcatch></cfcatch>
</cftry>