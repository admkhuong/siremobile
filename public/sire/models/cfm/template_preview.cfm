<!--- This templateid is used to look up data for a preview bootstrap modal page --->
<cfparam name="templateid" default="">

<!--- If bad templateid is passed in just redirect them to the campaign manage page --->
<cfif !isNumeric(templateid)>
	<cfabort/>
</cfif>

<div class="row">
	
	<!--- Read list of CP from template --->
	<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPs" returnvariable="RetVarReadCPs">
		<cfinvokeargument name="inpBatchId" value="#templateid#">
		<cfinvokeargument name="inpTemplateFlag" value="1">
	</cfinvoke>

	<!--- Loop over each Template CP --->
	<cfloop array="#RetVarReadCPs.CPOBJ#" index="CPOBJ">	    			
	    
	    <!--- Read CP data for each CP in Template --->
	 	<cfinvoke component="session.sire.models.cfc.control-point" method="ReadCPDataById" returnvariable="RetVarReadCPDataById">
			<cfinvokeargument name="inpBatchId" value="#templateid#">
			<cfinvokeargument name="inpTemplateFlag" value="1">
			<cfinvokeargument name="inpQID" value="#CPOBJ.RQ#">
		</cfinvoke>
	
		<!--- Render CP data for each CP in Template --->
		<cfinvoke component="session.sire.models.cfc.control-point" method="RenderCP" returnvariable="RetVarRenderCP">
			<cfinvokeargument name="controlPoint" value="#RetVarReadCPDataById.CPOBJ#">
			<cfinvokeargument name="inpBatchId" value="#templateid#">
			<cfinvokeargument name="inpTemplateFlag" value="1">	
			<cfinvokeargument name="inpPreview" value="1">						
		</cfinvoke>
		
		<cfoutput>						
			<!--- This is the HTML for a CP object - HTML is maintained in RenderCP function --->	
			#RetVarRenderCP.HTML#
		</cfoutput>
    	
	</cfloop>
	
</div>