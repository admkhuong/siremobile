<cfparam name="inpDTSId" default="0">
<cfparam name="inpDTSStatus" default="0">

<!--- Update Queue to finished processing (StatusType_ti=5) --->
<cfset updateStatusOK = 0>

<!--- Squash deadlocking issues? --->
<cfloop from="1" to="3" step="1" index="DeadlockIndex">
	<cftry>
	
		<!--- Update Dial Queue Table Status to Queued --->
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplequeue.contactqueue
			SET
				DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSStatus#">,
				LastUpdated_dt = NOW() 
			WHERE
				DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#">
		</cfquery>
		
		<cfset updateStatusOK = 1>
		
		<cfbreak>
		
		<cfcatch type="any">
		
			<cfif findnocase("Deadlock", cfcatch.detail) GT 0 > 
			
				<cfscript> 
					thread = CreateObject("java","java.lang.Thread"); 
					thread.sleep(3000); // CF will now sleep for 5 seconds 
				</cfscript> 
			
			<cfelse>
				
				<!--- other errors - break from loop--->
				<cfbreak>	                        	
			
			</cfif>
		
		</cfcatch>
	
	</cftry>

</cfloop>

<!--- One more try - non-squashed --->
<cfif updateStatusOK EQ 0>

		<!--- Update Dial Queue Table Status to Queued --->
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplequeue.contactqueue
			SET
				DTSStatusType_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSStatus#">,
				LastUpdated_dt = NOW() 
			WHERE
				DTSId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpDTSId#">
		</cfquery>

</cfif>