<cfcomponent>
	<cffunction name="addCss">
		<cfargument name="path">
			<cfargument name="pluginSection" type="boolean" required="false" default="false">
				<cfscript>
					if(!arguments.pluginSection){
					if (!isDefined("Request._css") || !isArray(Request._css)) {
					Request._css = [];
				}
				arrayAppend(Request._css,arguments.path);
			} else {
			if (!isDefined("Request._css_plugin") || !isArray(Request._css_plugin)) {
			Request._css_plugin = [];
		}
		arrayAppend(Request._css_plugin,arguments.path);
	}
	
	return this;
</cfscript>
</cffunction>
<cffunction name="addJs">
	<cfargument name="path">
		<cfargument name="pluginSection" type="boolean" required="false" default="false">
			<cfscript>
				if(!arguments.pluginSection){
				if (!isDefined("Request._js") || !isArray(Request._js)) {
						Request._js = [];
				}
				arrayAppend(Request._js,arguments.path);
		} else {
		if (!isDefined("Request._js_plugin") || !isArray(Request._js_plugin)) {
		Request._js_plugin = [];
	}
	arrayAppend(Request._js_plugin,arguments.path);
}
return this;
</cfscript>
</cffunction>
</cfcomponent>