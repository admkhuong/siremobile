<cfparam name="inpDTSId" default="0">
<cfparam name="inpDTSStatus" default="6">
<cfparam name="inpDialerIPAddr" default="sire_push_notificationqueue">

<!--- Update Queue to finished processing (DTSStatusType_ti=5) --->
<cfset UpdateStatusOK = 0>

<!--- Squash deadlocking issues? --->
<cfloop from="1" to="3" step="1" index="DeadlockIndex">
	<cftry>
	
		<!--- Update Dial Queue Table Status to Queued --->
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplequeue.contactqueue
			SET
				DTSStatusType_ti = #inpDTSStatus#,
				Queued_DialerIP_vch = '#inpDialerIPAddr#',
				Queue_dt = NOW() 
			WHERE
				DTSId_int = #inpDTSId#
		</cfquery>
		
		<cfset UpdateStatusOK = 1>
		
		<cfbreak>
		<cfcatch type="any">
		
			<cfif findnocase("Deadlock", cfcatch.detail) GT 0 > 
			
				<cfscript> 
					thread = CreateObject("java","java.lang.Thread"); 
					thread.sleep(3000); // CF will now sleep for 5 seconds 
				</cfscript> 
			
			<cfelse>
				
				<!--- other errors - break from loop--->
				<cfbreak>	                        	
			
			</cfif>
		
		</cfcatch>
	
	</cftry>

</cfloop>

<!--- One more try - non-squashed --->
<cfif UpdateStatusOK EQ 0>

		<!--- Update Dial Queue Table Status to Queued --->
		<cfquery name="UpdateSimpleXCallQueueData" datasource="#Session.DBSourceEBM#">
			UPDATE 
				simplequeue.contactqueue
			SET
				DTSStatusType_ti = #inpDTSStatus#,
				Queued_DialerIP_vch = '#inpDialerIPAddr#',
				Queue_dt = NOW() 
			WHERE
				DTSId_int = #inpDTSId#
		</cfquery>
</cfif>