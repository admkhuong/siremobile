<cfcomponent>
	<cfinclude template="../../paths.cfm" >
	<cfinclude template="../configs/paths.cfm">
	
	<cffunction name="ResetPassword" access="remote" output="true">
		<cfargument name="inpUserName" TYPE="string" required="yes" />
		<cfargument name="g-recaptcha-response" TYPE="string" required="yes" />
		
		<cfset var dataout = {} />
        <cfset dataout.RXRESULTCODE = -1 />
		<cfset dataout.TYPE = "" />
        <cfset dataout.MESSAGE = "Unhandeled Exception Error - Support has been notified." />
		<cfset dataout.ERRMESSAGE = "Unhandeled Exception Error - Support has been notified." />

		<cfset var RetValRECaptchaData = check_reCAPTHCHA(arguments['g-recaptcha-response']) />
            
		<cfif RetValRECaptchaData NEQ true>
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -2 />
			<cfset dataout.TYPE = "Any" />
            <cfset dataout.MESSAGE = "reCAPTCHA data does not match." />
			<cfset dataout.ERRMESSAGE = "#RetValRECaptchaData#" />
			<cfreturn dataout>
        </cfif>
		
		<!---<cfinvoke component="public.cfc.users" method="DoResetPassword" returnvariable="dataout">
			<cfinvokeargument name="inpUserName" value="#arguments.inpUserName#">
			<cfinvokeargument name="PublicPath" value="#PublicPath#">
			<cfinvokeargument name="BrandShort" value="#BrandShort#">
		</cfinvoke>--->
		
		<cfset dataout = DoResetPassword(arguments.inpUserName)>
		
		<cfreturn dataout>
	</cffunction>

	<cffunction name="check_reCAPTHCHA" returntype="boolean" hint="I check the captcha">
		<cfargument name="g-recaptcha-response" type="string" hint="Pass me the value of FORM.g-recaptcha-response" />
		
		<cftry>
			<cfhttp url="#SITE_VERIFY_URL#" method="post" timeout="5" throwonerror="true">
				<cfhttpparam type="formfield" name="secret" value="#PRIVATEKEY#">
				<cfhttpparam type="formfield" name="response" value="#arguments['g-recaptcha-response']#">
				<cfhttpparam type="formfield" name="remoteip" value="#cgi.REMOTE_ADDR#">
			</cfhttp>
		<cfcatch>
			<cfthrow type="RECAPTCHA_NO_SERVICE"
				message="recaptcha: unable to contact recaptcha verification service on url '#SITE_VERIFY_URL#'">
		</cfcatch>
		</cftry>
	
		<cfset VARIABLES.aResponse = DeserializeJSON(cfhttp.fileContent)>
		
		<cfreturn VARIABLES.aResponse.success />
	</cffunction>
	
	<cffunction name="DoResetPassword" access="remote" output="false">
		<cfargument name="inpUserName" TYPE="string" required="yes" />
		<cfset var dataout = {} />
		<cfset var verifyCode = '' />
		<cfset var GetUserId = '' />
		<cfset var dataMail = {}/>
		<cfset var checkExpire = '' />

		<!--- Clean up to avoid SQL injection attack --->
		<cfset var inpUserName = Trim(inpUserName) />		
		<cftry>		
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Unhandeled Exception Error - Support has been notified." />
			<cfset dataout.ERRMESSAGE = "Unhandeled Exception Error - Support has been notified." />
            
            <!---get user account in useraccount table with username or email param---> 
            <cfquery name="GetUserId" datasource="#Session.DBSourceREAD#">
				SELECT 
                	UserId_int,
                    EmailAddress_vch,
                    UserName_vch,
                    FirstName_vch
				FROM 
                	simpleobjects.useraccount
				WHERE
                	USERNAME_VCH = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpUserName#">
                OR 
                	EmailAddress_vch = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#inpUserName#">
                AND 
                    Active_int = 1
				LIMIT 1 
			</cfquery>
            <!---if not result, return--->
			<cfif GetUserId.RecordCount EQ 0>
				<cfset dataout = {} />
	            <cfset dataout.RXRESULTCODE = -2 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "Your email/username is not valid." />
				<cfset dataout.ERRMESSAGE = "Your email/username is not valid." />
				<cfreturn dataout>
			<cfelse>
			<!---if has user account match with username or email param,
			gen new VerifyPasswordCode, update to useraccount table and send mail to user--->    	
			<cfset verifyCode = Replace(CreateUUID(), "-", "", "All") />                            
            <cfquery datasource="#Session.DBSourceEBM#">
                UPDATE 
                    simpleobjects.useraccount
                SET 
                    VerifyPasswordCode_vch = '#verifyCode#', 
                    validPasswordLink_ti = 0
                WHERE 
                    UserId_int = '#GetUserId.UserId_int#'
            </cfquery>
            
            <!--- Detail - Feature using mail account from BabbleSphere --->
            <!--- Mail user their welcome aboard email --->
            
             <!--- check account expire (over 30day) --->
             <cfquery name="checkExpire" datasource="#Session.DBSourceREAD#">
				SELECT DATEDIFF(NOW(), EndDate_dt) AS exprireDate FROM simplebilling.userplans
				WHERE UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#GetUserId.UserId_int#">
				AND Status_int = 1 
				HAVING exprireDate > 30
			</cfquery>
            
            <cfif checkExpire.recordcount EQ 0>
            <cfset dataMail = {
				FirstName:GetUserId.FirstName_vch, 
				verifyCode:verifyCode,
				rootUrl:rootUrl,
				publicPath:publicPath,
				BrandShort: BrandShort
			}/>
			
            <cfinvoke method="sendmailtemplate" component="public.sire.models.cfc.sendmail">
		            <cfinvokeargument name="to" value="#TRIM(GetUserId.EmailAddress_vch)#">
		            <cfinvokeargument name="type" value="html">
		            <cfinvokeargument name="subject" value="Email request reset password for #BrandShort# Account">
		            <cfinvokeargument name="template" value="/public/sire/views/emailtemplates/mail_template_forgot_password.cfm">
		            <cfinvokeargument name="data" value="#TRIM(serializeJSON(dataMail))#">
		    </cfinvoke>
		    </cfif>
            
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = 1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "" />
			<cfset dataout.ERRMESSAGE = "" />			
        </cfif>

        <cfcatch type="any">
        	<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "#cfcatch.TYPE#" />
            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
			<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
        </cfcatch>

		</cftry>
		<cfreturn dataout />
	</cffunction>
	
	<cffunction name="get" access="remote" hint="Get useraccount with optional query param">

		<cfargument name="inpFieldName" type="string" required="true" default="UserId_int">
		<cfargument name="inpOperator" type="string" required="true" default="LIKE">
		<cfargument name="inpValue" type="string" default="">

		<cfset var dataout = {} />
		<cfset var filterable = ["USERID_INT", "HOMEPHONESTR_VCH", "EMAILADDRESS_VCH"] />
		<cfset var operators = ["LIKE", "="] />
		<cfset var sqlFilter = "" />
		<cfset var userQuery = {} />
		<cfset var userList = [] />


		<cftry>
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
            <cfset dataout.USERLIST = [] />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Not found!" />
			<cfset dataout.ERRMESSAGE = "Not found!" />

			<cfif arguments.inpValue neq "" >
				<cfif ArrayContains(filterable, UCase(arguments.inpFieldName)) AND ArrayContains(operators, UCase(arguments.inpOperator))>
					
					<cfquery name="userQuery" datasource="#Session.DBSourceREAD#">
						SELECT 
							UserId_int, 
							EmailAddress_vch,
							CONCAT(FirstName_vch," ", LastName_vch) as FullName,
							HomePhoneStr_vch
						FROM 
							simpleobjects.useraccount
						WHERE
							<cfif UCase(arguments.inpOperator) eq 'LIKE' >
								#arguments.inpFieldName# #arguments.inpOperator# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="%#arguments.inpValue#%">
							<cfelse>
								#arguments.inpFieldName# #arguments.inpOperator# <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.inpValue#">
							</cfif>
						LIMIT 
							100
					</cfquery>
					
					<cfloop query="#userQuery#">
						<cfset var item = {userId=UserId_int, userPhone=HomePhoneStr_vch, userName=Fullname, userEmail=EmailAddress_vch} />
						<cfset ArrayAppend(userList, item) >
					</cfloop>
					<cfset dataout.USERLIST = userList />
					<cfset dataout.RXRESULTCODE = 1 />
				</cfif>
			</cfif>
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction>

	<!--- <cffunction name="UpdateUserAccountLimitation" access="public" hint="Update User's limitation info">
		<cfargument name="inpUserId" required="yes" type="number">
		<cfargument name="inpCredit" required="yes" type="number" default="0">
		<!--- <cfargument name="inpPromotion" required="yes" type="number" default="0"> --->
		<cfargument name="inpKeyword" required="yes" type="number" default="0">
		<cfargument name="inpMlp" required="yes" type="number" default="0">
		<cfargument name="inpShortUrl" required="yes" type="number" default="0">
		<cfargument name="inpMlpImageCapacity" required="yes" type="number" default="0">





		<cfset var dataout = {} />
		<cfset var rsUpdateUser = {} />
		<cfset var rsUpdateUserBilling = {} />


		<cftry>
			<cfset dataout = {} />
            <cfset dataout.RXRESULTCODE = -1 />
			<cfset dataout.TYPE = "" />
            <cfset dataout.MESSAGE = "Nothing to be updated!" />
			<cfset dataout.ERRMESSAGE = "Nothing to be updated!" />

			<cfquery result="rsUpdateUser" datasource="#Session.DBSourceEBM#" >
				UPDATE 
					simplebilling.userplans
				SET
					FirstSMSIncluded_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpCredit#">,
					KeywordsLimitNumber_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpKeyword#">,
					MlpsLimitNumber_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpMlp#">,
					ShortUrlsLimitNumber_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpShortUrl#">,
					MlpsImageCapacityLimit_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpMlpImageCapacity#">
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
					AND 
					Status_int = 1
				LIMIT
					1 
			</cfquery>

			<!--- <cfquery result="rsUpdateUserBilling" datasource="#Session.DBSourceEBM#" >
				UPDATE 
					simplebilling.billing
				SET
					PromotionCreditBalance_int = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.inpPromotion#">
				WHERE
					UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.inpUserId#">
				LIMIT
					1 
			</cfquery> --->

			<!--- <cfif rsUpdateUser.RECORDCOUNT GT 0 AND rsUpdateUserBilling.RECORDCOUNT GT 0> --->
			<cfif rsUpdateUser.RECORDCOUNT GT 0>
				<cfset dataout.RXRESULTCODE = 1 />
				<cfset dataout.TYPE = "" />
	            <cfset dataout.MESSAGE = "Update Success!" />
				<cfset dataout.ERRMESSAGE = "Update Success!" />
			</cfif>
			
			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout />
	</cffunction> --->

	<cffunction name="GetUserList" access="remote" output="true" hint="get user list with filter">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />
		<cfargument name="sColumns" default="" />
		<cfargument name="sEcho">
		<cfargument name="iSortCol_0" default="-1">
		<cfargument name="sSortDir_0" default="ASC">
		<cfargument name="iSortCol_1" default="-1">
		<cfargument name="sSortDir_1" default="">
		<cfargument name="iSortCol_2" default="-1">
		<cfargument name="sSortDir_2" default="">
		<cfargument name="iSortCol_3" default="-1">
		<cfargument name="sSortDir_3" default="">
		<cfargument name="iSortCol_4" default="-1">
		<cfargument name="sSortDir_4" default="">
		<cfargument name="customFilter" default="">
		<cfset var dataout = {} />
		<cfset var filterItem = '' />
		<cfset var rsCount = '' />
		<cfset var rsGetUserList = '' />
		<cfset var filterData = DeserializeJSON(arguments.customFilter) />
		<cfset var order = ''/>
		<cfset var getPlanRs = ''/>
		<cfset var plans = ''/>

		<!--- <cfdump var="#filterData#" abort="true"/> --->
		<cfset var GetUserList = "" />
			
	 	<cfset dataout.RXRESULTCODE = 1 />
	    <cfset dataout.TYPE = "" />
	    <cfset dataout.MESSAGE = "" />
	    <cfset dataout.ERRMESSAGE = "" />
		
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>


		<cfif Session.USERID LT 1>
			<cflocation url="/" />
		</cfif>

		
		<!--- Build Order --->

		<cfif iSortCol_0 GTE 0>
			<cfswitch expression="#Trim(iSortCol_0)#">
				<cfcase value="1"> 
			       <cfset order = "Full_Name"/>
			    </cfcase> 
			    <cfcase value="2"> 
			       <cfset order = "MFAContactString_vch" />
			    </cfcase> 
			    <cfcase value="4"> 
			       <cfset order = "MlpsImageCapacityLimit_bi"/>
			    </cfcase> 
			    <cfcase value="5"> 
			       <cfset order = "TotalImageSize"/>
			    </cfcase> 
			    <cfdefaultcase> 
			 		<cfset order = "UserId_int"/>   	
			    </cfdefaultcase> 	
			</cfswitch>
		<cfelse>
			 <cfset order = "UserId_int"/>
		</cfif>

		<!---ListEMSData is key of DataTable control--->
		<cfset dataout["UserList"] = ArrayNew()>
		<!--- <cfdump var="#filterData#" abort="true" /> --->
		<cfquery datasource="#Session.DBSourceEBM#" name="GetUserList" result="rsGetUserList">
				SELECT SQL_CALC_FOUND_ROWS
					tmpTable.UserId_int,
					Full_Name,
					FirstName_vch,
					LastName_vch,
					EmailAddress_vch,
					MFAContactString_vch,
					MlpsImageCapacityLimit_bi,
					IF(SUM(ImageSize_bi) > 0, SUM(ImageSize_bi), 0) AS TotalImageSize,
					Active_int,
					IsTestAccount_ti,
					UserPlanStatus,
					UserType_int,
					UserLevel_int,
					ShortCodeId_int,
					ShortCode_vch,
					Affiliate_code_vch,
					Affiliate_ID
				FROM (SELECT 
						u.UserId_int,
						CONCAT(u.FirstName_vch ," ", u.LastName_vch) AS Full_Name,
						u.FirstName_vch,
						u.LastName_vch,
						u.EmailAddress_vch,
						u.MFAContactString_vch,
						IF(up.MlpsImageCapacityLimit_bi > 0 , up.MlpsImageCapacityLimit_bi, 0) AS MlpsImageCapacityLimit_bi,
						u.Active_int,
						u.IsTestAccount_ti,
						p.Status_int as UserPlanStatus,
						u.UserType_int,
						u.UserLevel_int,
						sr.ShortCodeId_int,
                    	s.ShortCode_vch,
						aca.Affiliate_code_vch,
						aca.Id_int as Affiliate_ID
					FROM 
						simpleobjects.useraccount u
						INNER JOIN simplebilling.userplans up
						ON u.UserId_int = up.UserId_int 

						LEFT JOIN simplebilling.plans p
						ON up.PlanId_int=p.PlanId_int

						INNER JOIN sms.shortcoderequest sr
						ON sr.RequesterId_int = up.UserId_int

						INNER JOIN sms.shortcode s
						ON s.ShortCodeId_int = sr.ShortCodeId_int

						LEFT JOIN simpleobjects.affiliate_code_assignment aca
						ON u.UserId_int = aca.UserId_int
						AND aca.Status_ti=1

					WHERE up.UserPlanId_bi IN ( SELECT MAX(UserPlanId_bi) FROM simplebilling.userplans GROUP BY userplans.UserId_int  )
					ORDER BY 
						up.UserPlanId_bi DESC
				    ) AS tmpTable
					LEFT JOIN simpleobjects.userimages 
					ON tmpTable.UserId_int = userimages.UserId_int 
				WHERE
					1
				<cfif customFilter NEQ "">

					<cfloop array="#filterData#" index="filterItem">			
						<cfif filterItem.NAME EQ ' u.Status_int ' AND  filterItem.VALUE LT 0>							
						<!--- DO nothing --->
						<cfelseif Trim(filterItem.NAME) EQ 'UserType_int' AND  filterItem.VALUE GT 1>						
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#filterItem.VALUE#'>
							AND		UserLevel_int=1			
						<cfelseif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelseif filterItem.NAME EQ ' MFAContactString_vch '>
							<cfif filterItem.OPERATOR EQ '<>' OR filterItem.OPERATOR EQ '='>						
                                	AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") 
                                		#filterItem.OPERATOR#  
                                		<CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#'>)
                            <cfelse>   
                                                             
	                            	AND (REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#REReplaceNoCase(filterItem.VALUE, "[^\d^*^P^X^##]", "", "ALL")#%'>
									OR REPLACE(REPLACE(REPLACE(MFAContactString_vch, "(", ""),  ")", ""), "-", "") #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>)                              
                            </cfif>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>

				GROUP BY 
					tmpTable.UserId_int
				ORDER BY 
						#order# #sSortDir_0#
				LIMIT 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
					
		</cfquery>
		
		<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
			SELECT FOUND_ROWS() AS iTotalRecords
		</cfquery>
		<cfloop query="rsCount">
			<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
			<cfset dataout["iTotalRecords"] = iTotalRecords>
		</cfloop>
		<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans"></cfinvoke>
		<cfloop query="GetUserList" >
			<cfinvoke method="GetUserPlan" component="public.sire.models.cfc.order_plan" returnvariable="getPlanRs">
				<cfinvokeargument name="inpUserId" value="#UserId_int#"/>
			</cfinvoke>

			<cfset var tmpUser = {
				ID = '#UserId_int#',
				NAME = '#Full_Name#',
				EMAIL = '#EmailAddress_vch#',
				PHONE = '#MFAContactString_vch#',
				MLP_IMAGE_CAP = '#MlpsImageCapacityLimit_bi#',
				TOTAL_IMAGE_SIZE = '#TotalImageSize#',
				STATUS = '#Active_int#',
				ISTESTACCOUNT = '#IsTestAccount_ti#',
				PLANID = '#getPlanRs.PLANID#',
				USERDOWNGRADEDATE = getPlanRs.RXRESULTCODE EQ 1 ? '#getPlanRs.USERDOWNGRADEDATE#':'',
				ENDDATE = '#DateFormat(getPlanRs.ENDDATE, 'mm/dd/yyyy')#',
				BILLINGTYPE = '#getPlanRs.BILLINGTYPE#',
				PRICEMSGAFTER = '#getPlanRs.PRICEMSGAFTER#',
				USECUSTOMPLAN = '#!ListContains(ValueList(plans.DATA.id), getPlanRs.PLANID)#',
				USERPLANSTATUS ='#GetUserList.UserPlanStatus#',
				SHORTCODEID = '#ShortCodeId_int#',
				SHORTCODEVCH = '#ShortCode_vch#',
				AFFILIATE_ID_ASSIGN= '#Affiliate_ID#',
				AFFILIATE_CODE_ASSIGN= '#Affiliate_code_vch#'

			} />
			
			<cfset ArrayAppend(dataout["UserList"], tmpUser) />
		</cfloop>

   		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetAccountUpdateLogs" access="remote" hint="">
		<cfargument name="inpOwnerID" type="numeric" required="true">
		<cfargument name="inpCustomFilter" type="string" required="true">
		<cfargument name="iDisplayStart" TYPE="numeric" required="no" default="1" />
		<cfargument name="iDisplayLength" TYPE="numeric" required="no" default="1" />


		<cfset var dataout	= {} />
		<cfset var filterData = DeserializeJSON(arguments.inpCustomFilter) />
		<cfset var rsGetData = {} />
		<cfset var rsCount = {} />
		<cfset var displayOldValue = '' />
		<cfset var displayNewValue = '' />
		<cfset var filterItem = '' />

		<cfset dataout["dataList"]	= [] />
		<cfset dataout["iTotalRecords"] = 0>
		<cfset dataout["iTotalDisplayRecords"] = 0>

		<cfset dataout.RXRESULTCODE = -1>
		<cfset dataout.DATA = ''>
		<cfset dataout.MESSAGE = "" />
		<cfset dataout.ERRMESSAGE = "" />

		<cftry>
			<cfquery name="rsGetData" datasource="#Session.DBSourceEBM#">
				SELECT SQL_CALC_FOUND_ROWS
					BUL.ID_bi, 
					BUL.UserId_int,
					BUL.FieldName_vch,
					BUL.OldValue_dec,
					BUL.NewValue_dec,
					BUL.Created_dt,
					UA.FirstName_vch,
					UA.LastName_vch
				FROM simplebilling.account_update_logs BUL
					INNER JOIN simpleobjects.useraccount UA ON BUL.UserId_int = UA.UserId_int
				WHERE 
					BUL.OwnerID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpOwnerID#">
				<cfif inpCustomFilter NEQ "">
					<cfloop array="#filterData#" index="filterItem">
						<cfif filterItem.OPERATOR NEQ 'LIKE' AND filterItem.OPERATOR NEQ 'NOT LIKE'>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='#filterItem.TYPE#' VALUE='#filterItem.VALUE#'>
						<cfelse>
							AND #PreserveSingleQuotes(filterItem.NAME)# #filterItem.OPERATOR#  <CFQUERYPARAM CFSQLTYPE='CF_SQL_VARCHAR' VALUE='%#filterItem.VALUE#%'>
						</cfif>
					</cfloop>
				</cfif>
				ORDER BY Created_dt DESC
				LIMIT
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayLength#">
				OFFSET 
					<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.iDisplayStart#"> 
			</cfquery>

			<cfquery datasource="#Session.DBSourceEBM#"  name="rsCount">
				SELECT FOUND_ROWS() AS iTotalRecords
			</cfquery>
			<cfloop query="rsCount">
				<cfset dataout["iTotalDisplayRecords"] = iTotalRecords>
				<cfset dataout["iTotalRecords"] = iTotalRecords>
			</cfloop>

			<cfloop query="rsGetData" >
				<cfset displayOldValue = rsGetData.OldValue_dec/>
				<cfset displayNewValue = rsGetData.NewValue_dec/>

				<cfif (rsGetData.FieldName_vch EQ "MLPs" OR rsGetData.FieldName_vch EQ "Short URLs") AND rsGetData.OldValue_dec EQ 0>
					<cfset displayOldValue = "Unlimited"/>
				</cfif>

				<cfif (rsGetData.FieldName_vch EQ "MLPs" OR rsGetData.FieldName_vch EQ "Short URLs") AND rsGetData.NewValue_dec EQ 0>
					<cfset displayNewValue = "Unlimited"/>
				</cfif>

				<cfif (rsGetData.FieldName_vch EQ "Cost per Credit")>
					<cfset displayOldValue = "$" & rsGetData.OldValue_dec/100>
					<cfset displayNewValue = "$" & rsGetData.NewValue_dec/100>
				</cfif>

				<cfset var item = {
					ID = '#ID_bi#',
					FIELD = '#FieldName_vch#',
					USERID = '#UserId_int#',
					UFIRSTNAME = '#FirstName_vch#',
					ULASTNAME = '#LastName_vch#',
					OLDVALUE = '#displayOldValue#',
					NEWVALUE = '#displayNewValue#',
					CREATED = '#DateFormat(Created_dt, "mm/dd/yyyy")#'
				} />
				<cfset dataout["dataList"].append(item) />
			</cfloop>


			<cfset dataout.RXRESULTCODE = 1>
			<cfset dataout.MESSAGE = "" />

			<cfcatch>
				<cfset dataout = {}>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>
		</cftry>
		<cfreturn dataout>
	</cffunction>

	<cffunction name="GetDetailsForMonthlyPlanLimit" access="remote" hint="">
		<cfargument name="inpUserId" required="true">

		<cfset var dataout = {} />
		<cfset dataout.MESSAGE = '' />
		<cfset dataout.RXRESULTCODE = 0 />

		<cfset var userPlanName = '' />
		<cfset var monthlyAmount = '' />
		<cfset var activeSince = '' />
		<cfset var RetUserPlan = '' />

		<cftry>
			<cfinvoke component="session.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan">
				<cfinvokeargument name="InpUserId" value="#arguments.inpUserId#">
			</cfinvoke>

			<cfset userPlanName = 'Free'>
	        <cfif RetUserPlan.RXRESULTCODE EQ 1>
	            <cfset userPlanName = RetUserPlan.PLANNAME>
	        </cfif>

	        <cfif RetUserPlan.PLANEXPIRED GT 0> 
	            <cfset monthlyAmount = RetUserPlan.AMOUNT + (RetUserPlan.KEYWORDPURCHASED + RetUserPlan.KEYWORDBUYAFTEREXPRIED)*RetUserPlan.PRICEKEYWORDAFTER >
            <cfelse>    
                <cfset monthlyAmount = RetUserPlan.AMOUNT + RetUserPlan.KEYWORDPURCHASED*RetUserPlan.PRICEKEYWORDAFTER >
            </cfif>

	        <cfset activeSince = "#dateFormat(RetUserPlan.STARTDATE, 'm/d/yyyy')#">

	        <cfset dataout.PLANENAME = userPlanName/>
	        <cfset dataout.MONTHLYAMOUNT = monthlyAmount/>
	        <cfset dataout.ACTIVESINCE = activeSince/>

	        <cfset dataout.RXRESULTCODE = 1/>
	        <cfset dataout.MESSAGE = "Get plan details successfully!"/>

			<cfcatch>
				<cfset dataout.RXRESULTCODE = -1 />
				<cfset dataout.TYPE = "#cfcatch.TYPE#" />
	            <cfset dataout.MESSAGE = "#cfcatch.MESSAGE#" />
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#" />
			</cfcatch>

			<cfreturn dataout />
		</cftry>
	</cffunction>

	<cffunction name="UpdateAccountProfile" access="remote" output="false" hint="Update account profile on Account Management">
		<cfargument name="inpuserID" TYPE="string" hint="User Id" />
		<cfargument name="inplastname" TYPE="string" hint="Last Name" />
		<cfargument name="inpfirstname" TYPE="string" hint="First Name" />
		<cfargument name="inpphone" TYPE="string" hint="Phone" />
		<cfargument name="inpemail" TYPE="string" hint="Email" />
		<cfargument name="inpmfatype" TYPE="numeric" hint="MFA Type" />
		<cfargument name="inpSecurityCredential" TYPE="numeric" default="0"/>
		<cfargument name="inpallowggmfa" TYPE="numeric" hint="Allow google MFA or not" />
		<cfargument name="inptamountlimited" TYPE="numeric" hint="Limited Purchase Amount" />
		<cfargument name="inpuserXMLCSEditable" TYPE="numeric" hint="Allow Edit XMLControlString" />
		<cfargument name="inpDefaultPaymentGateway" TYPE="string" default="3" hint="Default Payment Gateway " />
		<cfargument name="inpUserCaptureCCInfo" TYPE="numeric" hint="Allow Capture user cc info" default="0" />

		<cfset var dataout = {}>
		<cfset var UpdateAccountProfileQuery = '' />
		<cfset var updateAllUserAuthen=''>
		<cfset var updateUserAuthen=''>
		<cfset var RetVarGetAdminPermission	= '' />
		
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>

        <!--- <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif> --->

		<cfif NOT isvalid('telephone', inpphone)>
			<cfset dataout.ERRMESSAGE = 'SMS Number is not a valid US telephone number !'> 
			<cfset dataout.MESSAGE = 'SMS Number is not a valid US telephone number !'>
			<cfreturn dataout>
		</cfif>
		<cftransaction>
		<cftry>
			<cftransaction action="begin">
			<!--- Update keyword in db --->
            <cfquery name="UpdateAccountProfileQuery" datasource="#Session.DBSourceEBM#">              
                UPDATE 
					simpleobjects.useraccount 
				SET
					FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpfirstname#">, 
                    LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inplastname#">,
					MFAContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpphone#">,
					MFAEnabled_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpmfatype#">,
					EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpemail#">,
					SecurityCredential_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpSecurityCredential#">,
					AllowNotRealPhone_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpallowggmfa#">,
					LimitedPurchaseAmount_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#arguments.inptamountlimited#">,
					AllowEditXMLControlString_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpuserXMLCSEditable#">,
					AllowEditXMLControlString_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpuserXMLCSEditable#">,
					PaymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpDefaultPaymentGateway#">,
					CaptureCCInfo_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpUserCaptureCCInfo#">
                WHERE 
                    UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpuserID#">            
            </cfquery>  
			<cfquery name="updateAllUserAuthen" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplebilling.authorize_user
				SET 
					status_ti = 0 
				WHERE
					status_ti = 1   
				AND
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpuserID#">
			</cfquery>

				<cfquery name="updateUserAuthen" datasource="#Session.DBSourceEBM#">
				UPDATE
					simplebilling.authorize_user
				SET 
					status_ti = 1 
				WHERE
					paymentGateway_ti = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.inpDefaultPaymentGateway#">
				AND
					UserId_int= <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpuserID#">
				AND 
					status_ti = 0   
			</cfquery>  
			<cftransaction action="commit">
			<cfset dataout.RXRESULTCODE = 1 />
									
			<cfcatch TYPE="any">
				<cftransaction action="rollback">
				<cfset dataout.RXRESULTCODE = -1 /> 
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>   
		
        </cftry>
		</cftransaction> 
        
		<cfreturn dataout />	
			
	</cffunction> 

	<cffunction name="Checkcompulsoryemail" access="remote" output="false" hint="Check duplicate the user email">
		<cfargument name="inpemail" TYPE="string" hint="Email" />
		<cfargument name="inpiduser" TYPE="string" hint="UserID" />
				
		<cfset var dataout = {}>
		<cfset var CheckcompulsoryemailQuery = '' />
		
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.KEYWORDID = "0" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
		
		<cftry>
			<!--- Update keyword in db --->
            <cfquery name="CheckcompulsoryemailQuery" datasource="#Session.DBSourceEBM#">              
                SELECT	
					UserId_int
				FROM
					simpleobjects.useraccount 
                WHERE 
                    EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpemail#">     
				AND	
					UserId_int <> <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpiduser#">       
				AND
					UserType_int <> 2
            </cfquery>  
            
			<cfif CheckcompulsoryemailQuery.RecordCount gt 0>
				<cfset dataout.RXRESULTCODE = 1 />
			<cfelse>
				<cfset dataout.RXRESULTCODE = -1 />
			</cfif>
									
			<cfcatch TYPE="any">
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfreturn dataout>
			</cfcatch>   
		
        </cftry>
		<cfreturn dataout />	
			
	</cffunction>

	<cffunction name="GetUserInfor" access="remote" output="false" hint="get user infor">
		<cfargument name="inpiduser" TYPE="string" hint="UserID" default="0" >
				
		<cfset var dataout = {}>
		<cfset var GetUserInforName = '' />
		
		<!--- Set defaults --->
        <cfset dataout.RXRESULTCODE = 0 /> 		
		<cfset dataout.FIRSTNAME = "" />
		<cfset dataout.LASTNAME = "" />
		<cfset dataout.PHONENUMBER = "" />
		<cfset dataout.EMAIL = "" />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
		<cfset dataout.SECURITYCREDENTIAL = 0 />
		<cfset dataout.AMOUNTLIMITED = 0 />
		<cfset dataout.ALLEDITXMLCS ='0'/>
		<cfset dataout.FULLNAME = '' />
		<cfset dataout.PAYMENTGATEWAY = '' />
		
		<cfset dataout.FULLNAME = '' />
		<cfset dataout.CAPTURECC = '' />
		<cfset var RetVarGetAdminPermission	= '' />

		<!--- <cfinvoke component="session.sire.models.cfc.admin"  method="GetAdminPermission" returnvariable="RetVarGetAdminPermission">
            <cfinvokeargument name="inpSkipRedirect" value="1"/>
        </cfinvoke>
        <cfif RetVarGetAdminPermission.ISADMINOK NEQ "1">
            <cfthrow message="You do not have permission!" />
        </cfif> --->

		<cfif arguments.inpiduser GT 0>
			<cftry>
	            <cfquery name="GetUserInforName" datasource="#Session.DBSourceEBM#">              
					SELECT
						EmailAddress_vch,MFAContactString_vch,FirstName_vch,LastName_vch,MFAContactType_ti,MFAEXT_vch,SecurityQuestionEnabled_ti,Address1_vch,PostalCode_vch,MFAEnabled_ti,SecurityCredential_ti,AllowNotRealPhone_ti,
					LimitedPurchaseAmount_int, AllowEditXMLControlString_ti,PaymentGateway_ti,CaptureCCInfo_ti
					FROM
						simpleobjects.useraccount
					WHERE                
						userId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#arguments.inpiduser#">
					AND
						Active_int > 0       
	            </cfquery>  
	            
				<cfif GetUserInforName.RecordCount gt 0>
					<cfset dataout.RXRESULTCODE = 1 />
					<cfset dataout.FIRSTNAME = "#GetUserInforName.FirstName_vch#" />
					<cfset dataout.LASTNAME = "#GetUserInforName.LastName_vch#" />
					<cfset dataout.PHONENUMBER = "#GetUserInforName.MFAContactString_vch#"/>
					<cfset dataout.EMAIL = "#GetUserInforName.EmailAddress_vch#" />
					<cfset dataout.MFATYPE = "#GetUserInforName.MFAEnabled_ti#" />
					<cfset dataout.SECURITYCREDENTIAL = "#GetUserInforName.SecurityCredential_ti#" />
					<cfset dataout.ALLOWNOTPHONE = "#GetUserInforName.AllowNotRealPhone_ti#" />
					<cfset dataout.AMOUNTLIMITED = "#GetUserInforName.LimitedPurchaseAmount_int#" />
					<cfset dataout.ALLEDITXMLCS = "#GetUserInforName.AllowEditXMLControlString_ti#" />
				<cfset dataout.FULLNAME =  dataout.FIRSTNAME&" "&dataout.LASTNAME />				
				<cfset dataout.PAYMENTGATEWAY = "#GetUserInforName.PaymentGateway_ti#" />
				<cfset dataout.CAPTURECC = "#GetUserInforName.CaptureCCInfo_ti#" />

				<cfelse>
					<cfset dataout.RXRESULTCODE = -1 />
				</cfif>
										
				<cfcatch TYPE="any">
						<cfset dataout.RXRESULTCODE = -1 /> 
						<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
						<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
					<cfreturn dataout>
				</cfcatch>
	        </cftry>
        </cfif>
		<cfreturn dataout />	
	</cffunction>

	<cffunction name="Checkrechargedisplay" access="remote" hint="Re-charge button display.">
		<cfargument name="UserId" required="yes" default="">

		<cfset var dataout = {} />
		<cfset dataout.RESULT = 0>
        <cfset dataout.MESSAGE = ''>
		<cfset dataout.PLANID = 1>
		<cfset dataout.RECURRINGKEYWORD = 0>
		<cfset dataout.RECURRINGPLAN = 0>
		<cfset dataout.BUYKEYWORDNUMBER = 0>
		<cfset dataout.ERRMESSAGE = ''/>

		<cfset var UserPlansInforByUserID = ''>		
		<cftry>
			<cfquery name="UserPlansInforByUserID" datasource="#Session.DBSourceEBM#">
				SELECT 
					PlanId_int,
					NumOfRecurringKeyword_int,
					NumOfRecurringPlan_int,
					BuyKeywordNumber_int
				FROM 
					simplebilling.userplans
				WHERE 
					Status_int = 1
				AND 
					(DATE(EndDate_dt) <= CURDATE() OR MonthlyAddBenefitDate_dt <= CURDATE())
				AND 
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#arguments.UserId#">				
			</cfquery>

			<cfif UserPlansInforByUserID.RECORDCOUNT GT 0>
				<cfset dataout.PLANID = #UserPlansInforByUserID.PlanId_int#>
				<cfset dataout.RECURRINGKEYWORD = #UserPlansInforByUserID.NumOfRecurringKeyword_int#>
				<cfset dataout.RECURRINGPLAN = #UserPlansInforByUserID.NumOfRecurringPlan_int#>
				<cfset dataout.BUYKEYWORDNUMBER = #UserPlansInforByUserID.BuyKeywordNumber_int#>
				<cfset dataout.RESULT = 1>
			</cfif>
			<cfcatch TYPE="any">
				<cfset dataout.RESULT = -1>
				<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#">
				<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
				<cfset dataout.PLANID = 1>
				<cfset dataout.RECURRINGKEYWORD = 0>
				<cfset dataout.RECURRINGPLAN = 0> 
				<cfset dataout.BUYKEYWORDNUMBER = 0>
				<cfreturn dataout>
			</cfcatch>
		</cftry>
		<cfreturn dataout/>
	</cffunction>

	<cffunction name="GetAccountReportLikeDashboard" access="remote" hint="Get a report that lets an admin quickly see the same info that the user would normally see on the dashboard">
		<cfargument name="inpUserId" required="true"/>

		<cfset var dataout = {}>
		<cfset var getSentMessageResutl = ''>
		<cfset var getSubsList = ''>
		<cfset var getOptCountSum = ''>
		<cfset var getCampaignList = ''>
		<cfset var getMLPShortURLCount = ''>
		<cfset var getKeywordList = ''>

		<cfset dataout.RXRESULTCODE = 0 />
		<cfset dataout.MESSAGE = ""/>
        <cfset dataout.ERRMESSAGE = ""/>
        <cfset dataout.MESSAGESENT = 0/>
        <cfset dataout.OPTIN = 0/>
        <cfset dataout.OPTOUT = 0/>

		<cftry>
			<cfinvoke component="session.sire.models.cfc.reports.dashboard" method="GetSentMessageForDashboard" returnvariable="getSentMessageResutl">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.reports.opt" method="ConsoleSummaryOptCounts" returnvariable="getOptCountSum">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>

			<cfinvoke component="session.sire.models.cfc.reports.dashboard" method="GetMLPShortURL" returnvariable="getMLPShortURLCount">
				<cfinvokeargument name="inpUserId" value="#arguments.inpUserId#"/>
			</cfinvoke>

			<cfset dataout.RXRESULTCODE = 1/>
			<cfset dataout.MESSAGESENT = getSentMessageResutl.TOTALMESSAGE/>
			<cfset dataout.OPTIN = getOptCountSum.OPTINTOTALCOUNT/>
			<cfset dataout.OPTOUT = getOptCountSum.OPTOUTTOTALCOUNT/>
			<cfset dataout.URLCLICK = getMLPShortURLCount.TOTALCLICKSHORTURL/>
			<cfset dataout.MLPVIEW = getMLPShortURLCount.MLPVIEWS/>
			<cfcatch TYPE="any">
					<cfset dataout.RXRESULTCODE = -1 /> 
					<cfset dataout.MESSAGE = "#cfcatch.MESSAGE#"/>
					<cfset dataout.ERRMESSAGE = "#cfcatch.detail#"/>
			</cfcatch>
		</cftry>

		<cfreturn dataout/>
	</cffunction>


</cfcomponent>