	<header class="home header-about-us clearfix">
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-xs-3">
						<div class="landing-logo">
							<a href="/">
								<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                        					<img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
							</a>
						</div>
					</div>	

					<div class="col-sm-9 col-xs-9">
						<div class="login-wrapper">
							<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
							<!--<button type="button" class="btn btn-success btn-login btn-success-custom" data-toggle="modal" data-target="#signout">SIGN OUT</button>-->
							<cfinclude template="../views/commons/menu.cfm">
							<cfelse>
							<cfinclude template="../views/commons/menu_public.cfm">
							</cfif>
						</div>
					</div>	
				</div>
			</div>		
		</section>
			
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						                        
                        <h1 class="marketing_title_big hidden-xs">About Us</h1>
                        <h3 class="marketing_title visible-xs">About Us</h3>
                        <h4 class="hidden-xs"><i><span class="hidden-xs">The next generation of </span>interactive mobile marketing and engagement</i></h4>
                        
					</div>	
				</div>	
                
                <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div>
                
                <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div>
                
			</div>	
		</section>
	</header>

	<section class="main-wrapper" id="feature-main">
		<div class="feature-white">
			<div class="container">
				<div class="row">					
					<div class="col-md-12">
						<div class="feature-title" id="feature1">Delivering Customer Engagement at the Speed of Now</div>
						<div class="feature-content">
                        	<p>Sire was created with an understanding that in the competitive marketplace, small to medium sized businesses need a cost effective resource to deliver better content and service. Sire provides a powerful toolset that is easy to use. You don’t need to be a marketing expert to see positive results. </p>

                            <p>The people and technology behind Sire have built over 100,000 marketing and messaging campaigns for Fortune 500 companies. Now that knowledge and expertise is ready to be shared with your businesses, helping you to leverage the power of Mobile to reach and interact with customers.  Our experienced team of experts are ready to provide the technical support and infrastructure services that create the very best in Interactive SMS campaigns.</p>
                            
                            <p>In today's social, mobile, and data-driven world Sire has the applications and services you need delivered at the speed of now.</p>
                        
                        </div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container" style="background-color: #274e60; color: #fff; font-family: 'Source Sans Pro', sans-serif;">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2" style="color:#fff;">The Management Team</div>
				</div>                      
			</div>
		</div>

		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2">Founder / CTO</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Jeffery "Lee" Peterson</b>                            
                            </p>
                            
                            <p>Lee is a technology executive with extensive expertise in the development of data driven applications for business, retail, utilities, healthcare and more. He is personally involved in the creation of new technology and its application to different company products and services. Lee uses personal technical talent to direct and motivate the technical staff. </p>
                            
                            <p>For the past 15+ years, Lee has been an entrepreneur building software and systems into the market leading multi-channel Event Based Messaging (EBM) product. His software and systems provide marketing and communications services to many of the top Fortune 500 companies.</p>                                            
                            
                            <p>Programing, Integrations, EAI, Engineering and R&D</p> 
                                               
                            <p>
	                            <div style="" >
                           			<div class="header-content" style="width:560px; color:##FFFFFF;">                                
	                               		<a href="http://www.linkedin.com/in/jefferyleepeterson" style="color:##CCC; margin-right:20px;">      
	          								<img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_profile_greytxt_80x15.png" width="80" height="15" border="0" alt="View Jeff Peterson's profile on LinkedIn"><span style="margin-left:20px;">Connect with me!</span>
	        							</a>                    
	    							</div>
	                           	</div>      
                           	</p>            
                        </div>
					</div>
					<div class="col-md-4">
						
                        <img class="feature-img" alt="" src="/public/sire/images/learning/leecto.jpg">
                        
					</div>
				</div>
                      
			</div>
		</div>
		
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2">Co-Founder / CEO</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Bruce Watanabe</b>                            
                            </p>
                            
                            <p>Bruce Watanabe is a serial entrepreneur that has co-founded a number of technology and internet ventures including, SIRE (Messaging), SETA International (Global Systems Integrator & Solution Provider) and Mass Genie (Internet Marketplace).</p>
							
							<p>With over 15+ years of leadership experience ranging from start-ups to Fortune 10 companies, Bruce is proficient in Business Development, Sales, Channel Development and Business Strategy.</p>
							
							<p>Prior to joining SIRE as CEO, Bruce was a Channel Executive at IBM responsible for over 800+ channel partners worldwide across the IBM Commerce and IBM Analytics Divisions.  Bruce has also held positions at IBM including Business Unit Executive where he was responsible for managing IBM labs across China and India.</p>
							
							<p>Bruce has received a bachelors degree in Economics from the University of California, Irvine and an Executive MBA (EMBA) degree from Pepperdine University.</p>
                                                                                       
                        </div>
					</div>
					<div class="col-md-4">
						
                        <img class="feature-img" alt="" src="/public/sire/images/learning/brucewatanabe.jpg">
                        
					</div>
				</div>
                      
			</div>
		</div>

		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2">Co-Founder / CFO</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Mark Mitamura</b>                            
                            </p>
                            
                            <p>As the Chief Financial Officer (CFO) for SIRE, Mark is responsible for managing all financial risks and management for the company.  Mark brings over 15+ years of experience specializing in M&A, Capital Investments, Asset Management and Capital Markets. His focus has been on creating unique client experiences that build brand awareness for both corporate and individuals.  He has earned his FINRA Series 7 and Series 66 licensing (held with LPL Financial. He Holds the designations of Accredited Investment Fiduciary (AIF), Chartered Mutual Fund Counselor (CMFC), Chartered Retirement Plan Specialist (CRPS) and Chartered Retirement Planning Counselor (CRPC).</p>
                                                                                       
                        </div>
					</div>
					<div class="col-md-4">
						
                        <img class="feature-img" alt="" src="/public/sire/images/learning/markmitamura.jpg">
                        
					</div>
				</div>
                      
			</div>
		</div>

		<div class="container" style="background-color: #274e60; color: #fff; font-family: 'Source Sans Pro', sans-serif;">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2" style="color:#fff;">Board of Advisors</div>
				</div>                      
			</div>
		</div>
				
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2"></div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Jeffrey Pearl</b>                            
                            </p>
                            
                            <p>A successful track record of building and managing sales organizations (Sales Leaders, Client Account Managers, Sales Engineers, etc....) , with a focus on classic sales activity and funnel management programs. Twenty eight years of experience in the areas of local, long distance, CPE, data, private line, IP, VOIP (Voice over Internet Protocol), cloud and hosting services. Delivering Communications Services to Business Customers and Wholesale Service Providers. A history of top performance, high productivity, achievement and a leader in the industry.</p>

							<p>Specialties: Building and selling companies, negotiating contracts, executive leadership and management, Sales training and motivating employees. Creating a fun and positive environment.</p>
							
							<p>The most important thing to me are the people on the team, who I work with everyday! We have to have products and services that make a difference, this is what adds to natural drive and enthusiasm.</p>
                                                                                       
                        </div>
					</div>
					<div class="col-md-4">
						
                        <img class="feature-img" alt="" src="/public/sire/images/learning/jeffreypearl.jpg">
                        
					</div>
				</div>
                      
			</div>
		</div>

<!---

		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature2"></div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Nelson F. Granados</b>                            
                            </p>
                            
                            <p>Associate Professor of Information Systems Academic Director,Institute of Media, Entertainment, and Culture Graziadio School of Business and Management Pepperdine University</p>
                            <p>My research in the last 13 years has led to both academic publications and consulting engagements in the areas of digital strategy and multichannel pricing. I seek to always develop projects that are sponsored by organizations, so far mostly in the travel, media, and entertainment sectors. The products of these collaborations are either academic journal publications or consulting engagements, or a combination of both. </p>
                                                                                       
                        </div>
					</div>
					<div class="col-md-4">
						
                                                
					</div>
				</div>
                      
			</div>
		</div>
--->

		<!---<div class="feature-gray">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature2">Title</div>
						<div class="feature-content">Aliquam et erat sollicitudin, gravida urna et, laoreet urna. Nulla vel imperdiet eros. Maecenas augue purus, porta sit amet velit vel, tincidunt ullamcorper massa. Phasellus a posuere ex. Nunc turpis nisl, volutpat id sagittis convallis, ornare in ex. Curabitur ac tortor id magna pellentesque pulvinar. Cras ipsum dolor, rutrum in felis ut, viverra bibendum diam. </div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="../images/feature-demo2.png">
					</div>
				</div>
			</div>
		</div>--->
		
	</section>
	

<cfparam name="variables._title" default="About Us - Sire">
<cfparam name="variables.body_class" default="body-about-us">
<cfinclude template="../views/layouts/plan_pricing.cfm">

<!---
<cfinclude template="../views/layouts/plan_pricing.cfm">
--->