<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/about-us.min.css">
</cfinvoke>
	<header class="sire-banner2 about-bannernew header-banners">
		
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
			
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						<!--- <h1 class="banner-title-big hidden-xs">ABOUT US</h1>

						<h3 class="banner-title-small visible-xs">ABOUT US</h3> --->

						<h1 class="sire-title-updated">about us</h1>

						<p class="banner-sub-title "><i>The next generation of interactive mobile marketing and engagement</i></p>

						<cfif !(structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1) >
							<a href="/signup" class="get-started mt-20">get started</a>
						</cfif>

						<!--- <p class="banner-sub-title hidden-xs"><i>The next generation of </span>interactive mobile marketing and engagement</i></p> --->
						                        
                        <!--- <h1 class="marketing_title_big hidden-xs">ABOUT US</h1>
                        <h2 class="marketing_title visible-xs">About Us</h1>
                        <h4 class="hidden-xs"><i><span class="hidden-xs">The next generation of </span>interactive mobile marketing and engagement</i></h4> --->
                        
					</div>	
				</div>	
                
                <!--- <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div> --->
                
                <!--- <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div> --->
                
			</div>	
		</section>
	</header>

	<section class="aboutus-block">
		<div class="container-fluid">
			<div class="inner-aboutus-block">
				<div class="row col-sm-12">
					<h4 class="title">Delivering customer engagement at the speed of now</h4>
				</div>
			</div>
		</div>

		<div class="container new-container1">
			<div class="inner-aboutus-block">
				<div class="row">
					<div class="col-sm-12">
						
						<p class="desc text-left">
							Sire was created with an understanding that in the competitive marketplace, small to medium sized businesses need a cost effective resource to deliver better content and service. Sire provides a powerful toolset that is easy to use. You don’t need to be a marketing expert to see positive results.
						</p>
						<p class="desc text-left">
							The people and technology behind Sire have built over 100,000 marketing and messaging campaigns for Fortune 500 companies. Now that knowledge and expertise is ready to be shared with your businesses, helping you to leverage the power of Mobile to reach and interact with customers. Our experienced team of experts are ready to provide the technical support and infrastructure services that create the very best in Interactive SMS campaigns.
						</p>
						<p class="desc text-left">
							Sire is smarter than before with natural language processing that gives you a greater advantage to understand and derive meaning from your customers’ responses. Sire has established partnerships with all major carriers in US and Canada - AT&T, Verizon, Sprint, T-mobile and many others. In today's social, mobile, and data-driven world Sire has the applications and services you need delivered at the speed of now.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--- <section class="main-wrapper" id="feature-main">
		<div class="feature-white">
			<div class="container">
				<div class="row">					
					<div class="col-md-12">
						<div class="feature-title" id="feature1"><h2>Delivering Customer Engagement at the Speed of Now</h2></div>
						<div class="feature-content">
                        	<p>Sire was created with an understanding that in the competitive marketplace, small to medium sized businesses need a cost effective resource to deliver better content and service. Sire provides a powerful toolset that is easy to use. You don’t need to be a marketing expert to see positive results.</p>
                            <p>The people and technology behind Sire have built over 100,000 marketing and messaging campaigns for Fortune 500 companies. Now that knowledge and expertise is ready to be shared with your businesses, helping you to leverage the power of Mobile to reach and interact with customers. Our experienced team of experts are ready to provide the technical support and infrastructure services that create the very best in Interactive SMS campaigns.</p>
                            <p>Sire is smarter than before with natural language processing that gives you a greater advantage to understand and derive meaning from your customers’ responses. Sire has established partnerships with all major carriers in US and Canada - AT&T, Verizon, Sprint, T-mobile and many others. In today's social, mobile, and data-driven world Sire has the applications and services you need delivered at the speed of now.</p>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="feature-gray">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature2">Title</div>
						<div class="feature-content">Aliquam et erat sollicitudin, gravida urna et, laoreet urna. Nulla vel imperdiet eros. Maecenas augue purus, porta sit amet velit vel, tincidunt ullamcorper massa. Phasellus a posuere ex. Nunc turpis nisl, volutpat id sagittis convallis, ornare in ex. Curabitur ac tortor id magna pellentesque pulvinar. Cras ipsum dolor, rutrum in felis ut, viverra bibendum diam. </div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="/public/sire/images/feature-demo2.png">
					</div>
				</div>
			</div>
		</div>
		
	</section> --->
	

<cfparam name="variables._title" default="About Us - Sire">
<cfparam name="variables.body_class" default="body-about-us">
<cfinclude template="../views/layouts/plan_pricing.cfm">

<!---
<cfinclude template="../views/layouts/plan_pricing.cfm">
--->