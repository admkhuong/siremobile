<!--- <cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1>
    <cfinclude template="../views/commons/fixed-signup.cfm">
</cfif> --->

<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<div class="static-wrapper">
      <div class="container">
         
      <section class="static-page-container">
            
            <!--- <h1>Terms and Conditions</h1> --->
            <div class="AlignCenter static-page-title">SIRE Affiliate Agreement</div>
            <!--- <div class="last-update AlignCenter">Last updated: January 12, 2018</div> --->
            <div class="term-content">
            <p>THIS AFFILIATE AGREEMENT (“Agreement”) is entered between SIRE INVESTMENTS, LLC, a California Based LLC Corporation (“Company”) and (“Affiliate”). Company and Affiliate may sometimes be referred to individually as the “Party” and collectively as the “Parties.”</p>
            <p><strong>RECITALS </strong></p>                                   

            <p>A. Company is engaged in the business of providing SMS Marketing (SMS) Services (“Services”).</p>
            <p>B. Affiliate desires to become an authorized Sales and Marketing Affiliate for Company, and Company wishes to engage Affiliate to promote and resell of Company Services.</p>
            <p>C. This Agreement pertains only to Affiliate’s promotion and resell of Company Services.</p>                 
            <p>NOW, THEREFORE, in consideration of the foregoing Recitals, which are incorporated into the operative provisions of the Agreement by this reference, and for other good and valuable consideration, the receipt and adequacy of which are hereby acknowledged, the parties hereto agree as follow:</p>
            <p><strong>Article 1. Definitions  </strong></p>                                   
            <ul>
                <li>1.01. &nbsp;&nbsp;SMS Marketing Services Software- Affiliate will receive commission based on the commission plan outlined in Exhibit A. All commissions will be paid at the end of each month and consequent billing will occur every 30 days until the customer wishes to cancel services.</li>
            </ul>

            <p><strong>Article 2. Appointment </strong></p>  
            <ul>
                <li>
                    2.01. &nbsp;&nbsp;The Parties agree that Affiliate shall have the non-exclusive right to solicit sales of Company’s Services to potential customers. A customer (“Customer”) shall be defines as any person or entity that purchases the Company’s Services and/or purchases a license for the Company’s Service. All sales of the Services or solicitations for sale of the Services shall be in accordance with Company forms, procedures and policies, which may be amended or modified by Company, at its sole discretion, from time to time, upon written notice to Affiliate. Any such amendment or modification shall take effect upon receipt of written notice by the Affiliate.
                </li>
                <li>
                    2.02. &nbsp;&nbsp;Company reserves the right to market and sell Services through its own employees or other Affiliates, and appoint other Affiliates, both within and outside any territory in which Affiliate operates.
                </li>
                <li>
                    2.03. &nbsp;&nbsp;The Relationship between the Parties created by this Agreement shall not be construed as one of agency, partnership, joint venture, franchise or employer/employee. The Parties acknowledge the Affiliate shall be and remain an independent Affiliate and shall not have the authority to represent or bind Company except with the express prior written authorization of Company.  Affiliate’s employees and independent Affiliates shall be employed and/or contracted at Affiliate’s own risk, expense, and supervision, and Affiliate’s employees and Independent Affiliates shall not have any claim against Company for salaries, commissions, items of cost, or other form of compensation or reimbursement, and Affiliate represents, warrants, and covenants that Affiliate’s employees and independent Affiliates shall be subordinate to Affiliate and subject to each and all of the terms, provisions, and conditions applying to Affiliate hereunder.
                </li>
                <li>
                    2.04. &nbsp;&nbsp;Company reserves the right to change, add to, or discontinue its Services at any time, in Company’s sole discretion. Company shall provide notice to Affiliate if it changes, adds to, or discontinues any or all of its Services.
                </li>
            </ul>

            <p><strong>Article 3. Pricing  </strong></p>  
            <ul>
                <li>
                    3.01. &nbsp;&nbsp;Pricing will be based on the pricing list outlined in Exhibit A. Customers provided that Affiliate shall not, under any circumstances, charge a Customer any price for the Services that would obligate the Company for an amount that exceeds the Setup Retail Price (“SRP”) established by the Company for the services as listed in Exhibit A attached hereto. The Price List shall constitute Confidential information as defined in Section 6.01. Company, at its sole discretion, may amend, modify or revise the Price List by written notice to Affiliate, consistent with pricing terms and conditions at the time pricing was submitted to the Affiliate by the Company. Any amendment, modification or revision to the Price List shall take effect upon receipt of written notice to Affiliate.
                </li>
            </ul>

             <p><strong>Article 4. Remuneration  </strong></p>  
            <ul>
                <li>
                    4.01. &nbsp;&nbsp;For all Services rendered by Affiliate pursuant to this agreement and during the Term of this Agreement, Affiliate shall be compensated solely through commissions which shall accrue as follows:
                    <ul>
                        <li type="i">                        
                            Sales Commissions: 
                            <ul>
                                <li type="a">
                                    SMS Monthly Subscription Packages – For the sale of an SMS Subscription Package Originated by Affiliate, and whereby research and sales is being handled by the Affiliate, the Affiliate shall receive 15% of the total monthly gross billing for each customer. 
                                </li>
                            </ul>
                        </li>
                        <li type="i">
                             Payment of Sales & Commissions:
                             <ul>
                                <li type="a">
                                    SMS Marketing Package – Payment and Reconciliation of the sale of an SMS Subscription Package originated by the Affiliate shall be completed by the 30th of each month. Payment of the Monthly Residual Fee shall continue to be paid as long as customer continues to order the SMS Marketing Package, based upon receipt of payment in full from the customer.  Accounts wherein a customer initiates a chargeback or credit will be reconciled against future fees earned such that the amount of fees overpaid to Affiliate shall be deducted from fees owed to Affiliate for other sales.
                                </li>
                             </ul>
                        </li>
                        <li type="i">
                             General Terms 
                             <p>
                                Company’s obligations to pay the Commissions shall continue for six (6) months commencing upon Affiliate’s termination of new sales for more than sixty (60) days and for so long as Affiliate’s Customers remain subscribers of the Services Provided that: 
                             </p>
                             <ul>
                                <li type="a">
                                     Affiliate is not terminated for material breach of this Agreement, and
                                </li>
                                <li type="a">
                                       Affiliate’s Customer billings in a given month equal or exceed one hundred ($100) dollars.
                                </li>
                             </ul>
                             <p>
                                Should any transaction be originated by both Affiliate and any other person or entity that is contractually entitled to a commission for such transaction, Affiliate shall receive only its pro-rata share of such transaction.  No commissions shall accrue during any such time periods that Affiliate is in material breach of this Agreement, providing that written notice of such breach has been given and received by Affiliate, nor shall any commissions accrue for any transaction in which Affiliate Violated Company policies or made misrepresentations in inducing such transaction, providing that written notice has been given and received regarding such violation. 
                             </p>
                        </li>
                    </ul>
                </li>
                <li>
                    4.02. &nbsp;&nbsp;Affiliate shall be responsible for all of its own expenses incurred in connection with the performance of this agreement.
                </li>
            </ul>

            <p><strong>Article 5. Duties of Affiliate </strong></p>  
            <ul>
                <li>
                    5.01. &nbsp;&nbsp;Affiliate agrees to comply with any laws, regulations, ordinances or other legal requirements applicable to Affiliate in the course of performing its services pursuant to this Agreement.
                </li>
                <li>
                    5.02. &nbsp;&nbsp;Affiliate warrants that its services will be performed in a professional manner, and that none of its services or any part of this Agreement will be inconsistent with any obligation Affiliate may have to others.
                </li>
                <li>
                    5.03. &nbsp;&nbsp;Affiliate shall use reasonable sales and marketing efforts to promote the sales of the Services. Affiliate shall provide comprehensive training commensurate with training received from the Company, for all of its sales representatives who sell and market the Services. Affiliate represents and warrants that it is familiar with the Internet and the Services and that it is presently qualified to promote the sale and provide accurate sales support for such services.

                </li>
                <li>
                    5.04. &nbsp;&nbsp;Affiliate shall follow such procedures and use such contracts and forms as Company requests, and as Company may request from time to time in its sole discretion.
 
                </li>
            </ul>

            <p><strong>Article 6 Confidentiality  </strong></p>  
            <ul>
                <li>
                    6.01. &nbsp;&nbsp;In the performance of this Agreement, Affiliate may be exposed or otherwise have access to information and data that are confidential or proprietary to Company.  The Parties agrees that al such information and data, including, without limitation, any information or data pertaining to customers, clients, vendors, prospective venders, prospective customers or clients, prospective customer lists, pricing list, promotional material, method of operation, methods of selling, pricing, contracts, policies and procedure, concepts, techniques, technology, software, drawing research, development, designs, and any other confidential information concerning or in anyway related to any products  or services of Company whether or not reduced to writing and whether or not produced by Affiliate or received form a third party in the course of providing the services are confidential material disclosed to (collectively “Confidential Material”). Affiliate shall keep and maintain all confidential material disclosed to Affiliate in connection with the performance of the Agreement or learned or discovered by Affiliate during the performance of the Agreement and for a period of three years thereafter, in complete confidence and will not disclose to any other person or entity or permit any of its employees, agents, or representative to disclose, directly or indirectly, through any medium such Confidential information to any other person or entity or use it for its own benefit or for the benefit of others without the prior written consent of company.  

                </li>
            </ul>

            <p><strong>Article 7. Customer Ownership and Competition  </strong></p>  
            <ul>
                <li>
                    7.01. &nbsp;&nbsp;Customer shall be defined as the party that entered into a contract directly with Company. Affiliate may be both the “Affiliate” and the “Customer” in the same transaction. 
                </li>
                <li>
                    7.02. &nbsp;&nbsp;Company shall retain full ownership of all Customers, including without limitations, all information, specifications and special requirements relating to or derived for such Customers. During the term of this agreement and for a period of two years thereafter, Affiliate shall not, directly or indirectly, solicit employees or independent Affiliates (including resellers, sub-resellers and/or their employees and sales agents) of company, or solicit customer for any competitor of company. All documents and information relating to or derived from customer shall be deemed confidential information and the property of company.  Notwithstanding the foregoing, nothing in this agreement shall be interpreted to prohibit Affiliate from selling to customer’s products and services which do not compete with the SMS software services.

                </li>
                <li>
                    7.03. &nbsp;&nbsp;Excluding any relationships that Company had a prior business relationship with, Company agrees that during the term of this agreement and for a period of three years after this relationship has ended, Company shall not directly or indirectly circumvent the Affiliate with customers, employees, independent Affiliates, strategic alliances or others that are made known to the Company by the Affiliate in connection with doing business with the Affiliate and/ or in connection with the Affiliate doing business with the Company, without the knowledge and written consent of the Affiliate. 
 
                </li>
                <li>
                    7.04. &nbsp;&nbsp;Any customer brought on service by Affiliate will remain representative of record and will be sold or given to any other entity during the term of this agreement.
                </li>
                <li>
                    7.05. &nbsp;&nbsp;Affiliate shall not market to potential customers any completing SMS products and services provided by any entity or person other than company, unless presented to company for first right of refusal, and shall not diver or attempt to direct any SMS related business or customer (including, but not limited to , a customer solicited or originated by Affiliates under this agreement) to any person or entity other than company.  Affiliate shall not induce or actively attempt to influence any person to terminate, delay, or reduce in size or scope any contractual or business relationship with Company.
 
                </li>
            </ul>


            <p><strong>Article 8. Duties of Company  </strong></p>  
            <ul>
                <li>
                    8.01. &nbsp;&nbsp;Company shall use reasonable efforts to provide Affiliate with such technical information, technical tools, marketing data, and marketing tools necessary for Affiliate to market the services in accordance with this agreement.
                </li>
                <li>
                    8.02. &nbsp;&nbsp;Company shall use reasonable efforts to provide training to Affiliate regarding the services to enable Affiliate to promote and facilitate sales of services in accordance with this agreement.
 
                </li>
                <li>
                    8.03. &nbsp;&nbsp;Company shall share any information regarding any laws, regulations, ordinances, or other requirements applicable to Affiliate in the course of performing its services pursuant to this Agreement.
 
                </li>
            </ul>


            <p><strong>Article 9. Intellectual Property; Sales and Marketing Materials </strong></p>  
            <ul>
                <li>
                    9.01. &nbsp;&nbsp;Company grants Affiliate a non-exclusive, non-transferable and royalty free right and license to use company’s trademarks, service marks, trade dress or other designations (“Marks”), solely in connection with the promotion of company and the services to prospective customers and in accordance with company’s instructions an policies as communicated to Affiliate in writing from time to time. Affiliate’s use of the Marks, and all good will generated thereby, shall inure to the benefit of company.  Except as otherwise provided in the section 9.01, Affiliate shall acquire no right, title or interest in such Marks. All rights to use the marks shall cease upon expiration or termination of this agreement.
 
                </li>
                <li>
                    9.02. &nbsp;&nbsp;In promoting company and the services, Affiliate may use only advertising or marketing materials prepared by company or approved in writing by company.  Affiliate may use such advertising materials only upon the terms and conditions stated by company from time to time.  Affiliate may not modify or delete any advertising materials which it is authorized to use without the prior written consent of company.
 
                </li>
                <li>
                    9.03. &nbsp;&nbsp;Affiliate shall not knowingly market the services to a current customer of company.  In the event a current customer of company is solicited to buy the services by Affiliate, company, in its sole discretion, shall determine the services and the rates that will apply.
 
                </li>
                <li>
                    9.04. &nbsp;&nbsp;Affiliate has the right to private label Company services under its own brand providing that the promotion of such services are in compliance with the provisions set forth in this Agreement and do not obligate the Company in any way without its express written approval.
 
                </li>
            </ul>

            <p><strong>Article 10. Limitation of Liability </strong></p>  
            <ul>
                <li>
                    10.01. &nbsp;&nbsp;Limitation of Liability  COMPAY SHALL NOT BE LIABLE TO AFFILIATE OR ANY THIRD PARTY FOR ANY LOSSED OR EXPENSES OR ANY SPECIAL, CONSEQUENTIAL , INCIDENTAL, INDIRECT PUNITIVE DAMAGES WHETHER OR NOT FORESEEABLE RESULTING FROM, (i) THE DELIVERY, INSTALLATION, MAINTENACE, OPERATION, USE OR INABILITY TO USE THE SERVICES PR AMU EQUIPMENT USED IN CONNECTION WITH THE SERVICES; (ii) ANY ACT OR OMISSION OF AFFILIATE OR CUSTOMER OR THEIR AGENTS, OR ANY OTHER ENTITY FURNISHING EQUIPMENT, PRODUCTS OR SERVICES TO A AFFILIATE CUSTOMER; (iii) DELAY OF DELIVERY OR IMPLEMENTATION OF SERVICES, AND (iv) THE LOSS OF STORED, TRANSMITTED OR RECORDED DATA, OR LOSS OF GOOD WILL OR PROFITS, EVEN COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  COMPANY’S LIABILITY FOR A CLAIM OF ANY KIND ARISING OUT OF OR RELATING TO THIS AGREEMENT SHALL BE LIMITED SOLELY TO MONETARY DAMAGES AND SHALL NOT EXCEED FIVE THOUSAND DOLLARS ($5,000). THE PARTIES AGREE TO WORK IN GOOD FAITH TO IMPEMENT THE PURPOSES OF THIS AGREEMENT, BUT RECOGNIZE THAT THE SERVICES COULD NOT BE MADE AVAILABLE UNDER THESE TERMS OR OTHER SIMILAR TERMS WITHOUT SUBSTANTIAL INCREASE IN COST IF COMPANY WERE TO ASSUME A GREATER DEGREE OF LIABILITY TO CONTRATOR OR ANY THIRD PARTY.                    
                    
                </li>                
            </ul>

            <p><strong>Article 11.  Disclaimer of Warranties</strong></p>  
            <ul>
                <li>
                    11.01. &nbsp;&nbsp;UNLESS COMPANY NOTIFIES AFFILIATE OTHERWISE, COMPANY DISCLAIMS ALL WARRANTIES WITH REGARD TO THE SERVICES OR PRODUCTS RENDERED UNDER THIS AGREEMENT INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR CUSTOMER. COMPANY DOES NOT WARRANT THAT THE SERVICES WILL BE ENTIRELY FREE OF PROBLEMS OR INTERRUPTIONS.  AFFILIATE SHALL EXTEND NO WARRANTIES OR GUARANTEES WITHOUT THE APPROVAL OF COMPANY, ORALLY OR IN WRITING, IN THE NAME OF THE COMPANY OR WHICH WOULD BENEFIT COMPANY WITH RESPECT TO THE PERFORMANCE, DESCOMPANY, QUALITY, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE OF THE SERVICES OR PRODUCTS.  Affiliate and customer are responsible for assessing their own computer and transmission network needs, and the results to be obtained therefore. Company makes no warranty as to the results to be obtained from use of the services.  Use of any information or data obtained through the services is at customers sole risk.  The parties specifically deny the company shall bear any responsibility for the accuracy or quality of information or data obtained through the services.
                </li>
                <li>
                    11.02. &nbsp;&nbsp;Affiliate shall defend, indemnify and hold company and company’s subsidiaries, partners afflicts successors, licensees and assigns, and their respective directors, officers employees and agents, from and against and all claims, costs, damages, losses or expenses, including reasonable attorneys’ fees, incurred by company as result of or in connection with (i) any breach of this agreement by Affiliate or (ii) any negligent action or omission Affiliate or any Affiliate’s employees or agents.  Company’s liability to Affiliate is restricted by section 4 & 8 of this agreement.

                </li>
                <li>
                    11.03. &nbsp;&nbsp;Company shall defend, indemnify and hold Contactor and Affiliate’s subsidiaries, partners afflicts successors, and their respective directors, officers employees and agents, from and against and all claims, costs, damages, losses or expenses, including reasonable attorneys’ fees, incurred by Affiliate as result of or in connection with (i) any breach of this agreement by Company or (ii) any negligent action or omission by Company or any of Company’s employees or agents.
 
                </li>
                <li>
                    11.04. &nbsp;&nbsp;Affiliate shall assume full responsibility for any violation to the rights of third parties in the selling of the Company’s services to its customers. Affiliate understands and agrees that should there arise any dispute with, or challenge by, a third party concerning or arising from the use of the services sold by Affiliate, Affiliate shall not hold Company responsible for any of its liabilities, damages, losses and expenses (including, without limitation, reasonable attorney fees) arising out of any such dispute or challenge pursuant to section 11.02
 
                </li>
            </ul>
           

           <p><strong>Article 12. Termination of Agreement </strong></p>  
            <ul>
                <li>
                    12.01. &nbsp;&nbsp;Notwithstanding any other provision of this agreement, either party may terminate this agreement any time by giving fourteen (14) days written notice to the other party or may terminate this agreement immediately with written notice in the event of a Material Breach by the other Party.  Unless otherwise terminated as provided in this agreement, this agreement shall continue in force for a period of two years. This Agreement shall automatically renew on the same terms and conditions for successive one-year periods unless terminated in writing by certified mail. 
 
                </li>
                <li>
                    12.02. &nbsp;&nbsp;In the following circumstances, the company may terminate this agreement immediately by providing written notice to Affiliate.
                    <ul>
                        <li type="a">
                            Breach of any covenant, term or condition of this agreement by Affiliate, or
                        </li>
                        <li type="a">
                            Engagement by Affiliate in any of the following activities during the term of this          agreement malicious business practices, fraud, unlawful business behavior of any kind, selling or displaying pornographic materials, selling the services to any customer for use in connection with pornographic materials or any illegal products or services, allowing service failures caused by Affiliate’s negligence that result in the substantial loss of market share, defaming or deprecating company or company services to customers or any other third-parties, making any misrepresentations regarding company or the services, defaming or deprecating other company’s independent Affiliates or other Companys’ independent Affiliate services to customers or any other third-parties, competition with other independent Affiliates that company deems to be unfair or malicious, or
                        </li>
                        <li type="a">
                            An assignment by Affiliate for the benefit of creditors or Affiliate becomes bankrupt or insolvent, or takes benefit of, or becomes subject to any legislation in force relating to bankruptcy or insolvency, it being understood that the appointment of a receiver or trustee of the property and assets of Affiliate is conclusive evidence, of insolvency, or
                        </li>
                        <li type="a">
                            Company is unable to provide the services by reason of any lay, rule, regulation, or order of any municipal, state or federal authority, including, but not limited to any regulatory authority having jurisdiction , or 
                        </li>
                        <li type="a">
                            Any violation by Affiliate of any company policies, or
                        </li>
                        <li type="a">
                            Bankruptcy of company, or
                        </li>
                        <li type="a">
                            Sale of Company’s business, or
                        </li>
                        <li type="a">
                            Assignment of this agreement by Affiliate without company’s written consent.

                        </li>
                    </ul>
                </li>
            </ul>

             <p><strong>Article 13. Additional Provisions  </strong></p>  
            <ul>
                <li>
                    13.01. &nbsp;&nbsp;Force Majeure	 Company shall not be liable for, and is excused from , any failure to perform or for delay in the performance of its obligations under this agreement due to causes beyond its control, including without limitation, interruptions of power or telecommunications services, failure of company’s suppliers or subAffiliates, acts of nature, governmental actions, fire, flood, natural disaster, or labor disputes.
 
                </li>
                <li>
                    13.02. &nbsp;&nbsp;Arbitration	Any claim, dispute or controversy arising in connection with this agreement shall be submitted to arbitration in accordance with the commercial arbitration rules of the American Arbitration Association (“AAA”, in Orange County, California). The AAA decision shall be binding on the parties and judgment on the awarded rendered by the arbitrator may be entered in any court having jurisdiction thereof. Notwithstanding the foregoing, company shall have the right to pursue injunctive relief in any court of competent jurisdiction in connection with any dispute arising in connection with this agreement
 
                </li>
                <li>
                    13.03. &nbsp;&nbsp;Attorneys’ Fees.  If any legal action, including an action for declaratory relief, is brought to enforce or interpret the provisions of this agreement, the prevailing party will be entitled to reasonable attorney’s fees, which may be set by the court in the same action or in a separate action brought for the purpose in addition to any other relief to which the party may be entitled.
 
                </li>
                <li>
                    13.04. &nbsp;&nbsp;Assignment  This Agreement may not be assigned by Affiliate, in whole or in part, without the express written consent of company, which consent shall not be unreasonably withheld, but which consent shall remain in the sole discretion of the company.  Company may assign this agreement without the consent of Affiliate (i) to any entity controlled by or under common control with company, (iii) pursuant to any sale or transfer of substantially all of the assets of company or (iii) pursuant to merger or other reorganization of company.
 
                </li>
                <li>
                    13.05. &nbsp;&nbsp;Waiver	  No failure of either party to pursue any remedy resulting from a breach of this agreement shall be construed as a waiver of that breach or as a waiver of any subsequent or other breach unless such waiver is in writing and signed by the non-breaching party.

                </li>
                <li>
                    13.06. &nbsp;&nbsp;Entire Agreement.  This agreement constitutes the complete and entire agreement between the parties, and there are no other oral agreements, understandings, representations or warranties, express or implied, between the parties.  No modification or amendment of this agreement shall be binding upon the parties unless executed in writing by the parties hereto.
                </li>
                <li>
                    13.07. &nbsp;&nbsp;Interpretation  The parties hereby acknowledge and agree that they have had the opportunity to have this agreement reviewed by their own independent counsel and therefore agree that this agreement shall be deemed to have been jointly drafted and shall not be construed in favor or against either party.
                </li>
                <li>
                    13.08. &nbsp;&nbsp;Severability  The provisions of this agreement shall be severable; in the event that any provision is held to be invalid, void or otherwise unenforceable, by a court or arbitration panel of competent jurisdiction, the remaining provisions shall remain enforceable to the fullest extent permitted by law.
                </li>
                <li>
                    13.09. &nbsp;&nbsp;Governing Law and Venue  The validity, interpretation, construction and performance of this agreement shall be governed  by the laws of the state of California without regard to its conflicts-of-law principles.  The parties expressly agree that any action at law or in equity arising out of or related to this agreement or relating to the subject matter thereof, shall be filed only in the state or federal courts located in Orange County California and each party hereby expressly submits themselves to the exclusive, personal jurisdiction of such courts for the purposes of litigating any such action.
 
                </li>
                <li>
                    13.10. &nbsp;&nbsp;Injunctive Relief  The Parties recognize and acknowledge that any breach or threatened breach of section 6.01, 7.01 and 7.02 of this agreement by Affiliate may cause company irreparable harm for which monetary Affiliate from such breach or threatened breach. Nothing in this agreement shall be construed as preventing company from pursuing any remedy at law or in equity for any breach or threatened breach of this Agreement.
 
                </li>
                <li>
                    13.11. &nbsp;&nbsp;Modification  THE PARTIES AGREE THAT COMPANY, AT ANY TIME, IN ITS SOLE DISCRETION, MAY AMEND THE TERMS AND CONDITIONS OF THIS AGREEMENT. ANY SUCH AMENDMENT WILL BE BINDING AND EFFECTIVE ON THE PARTIES FOURTEEN (14) DAYS FOLLOWING NOTIFICATION TO AFFILIATE BY E-MAIL OR UNITED STATES MAIL.  IN THE EVENT THAT AFFILIATE DOES NOT WISH TO BE BOUND BY ANY SUCH AMENDMENT, AFFILIATE MAY TERMINATE THIS AGREEMENT AT ANY TIME BY FOLLOWING COMPANY’S RECEIPT OF SUCH NOTICE AND DURING SUCH TIME, AFFILIATE SHALL NOT BE BOUND BY SUCH AMENDMENT. NOTWITHSTANDING THE FOREGOING, IF FOLLOWING RECEIPT OF AFFILIATE’S TERMINATION NOTICE UNDER THIS SECTION 13.11 BUT PRIOR TO THE TERMINATION DATE OF SUCH NOTICE, COMPANY PROVIDES WRITTEN NOTICE TO AFFILIATE THAT COMPANY IS CANCELING THE NOTICED AMENDMENT, THIS AGREEMENT SHALL CONTINUE TO REMAIN IN EFFECTS AND AFFILIATE'S TERMINATION NOTICE SHALL BE NULL AND VOID.
                </li>
                <li>
                    13.12. &nbsp;&nbsp;Counterparts.	This agreement may be executed in counterparts, each of which when so executed and delivered shall be deemed an original, but all such counterparts together shall constitute but one and the same instrument.
                </li>
                
            </ul>
           

            <p><strong>Exhibit A  </strong></p> 
            <p>
                <u>Sire Pricing Packages </u></br> 
                https://www.siremobile.com/plans-pricing </br></br>
               
            </p>
            
            </div>
      </section>
      </div>
</div>
<style>
    table .Exhibit {
        border-collapse: collapse;
    }

    .Exhibit  th, .Exhibit td {
        border: 1px solid black;
        padding: 2px 10px;
    }
</style>      

<cfparam name="variables._title" default="Terms Of Use - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">