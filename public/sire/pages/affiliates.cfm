<cfparam  name="actionType" default="2"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="1">
<cfparam  name="ami" default="">
<cfparam  name="rc" default="n">
<cfset Session._rememberPath = "affiliates?rc=y" />
<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")		
		.addCss("/public/sire/css/affiliate.css")				
        .addJs("/public/sire/js/affiliate.js")

		.addCss("/session/sire/assets/global/plugins/uikit/css/uikit3.css", true)        
        .addJs("/session/sire/assets/global/plugins/Chartist/plugins/tooltips/chartist-plugin-tooltip.js", true)

        .addJs("/session/sire/assets/global/plugins/uikit/js/uikit.min.js", true);
</cfscript>

<cfif  structKeyExists(cookie,'affiliatecode')> 
	<cfcookie   name = "affiliatecode" expires = "now" secure = "yes" value = "" encodevalue = "yes">
</cfif>

<cfparam name="variables._title" default="Affiliate Referral Program">
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/live-demo.min.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.min.js">
</cfinvoke>

<cfset randomCode=Chr(RandRange(65, 90)) & Chr(RandRange(65, 90)) & Chr(RandRange(65, 90))>  
<cfset UserInfomation= {
    FirstName = "",
    LastName = "",
    SMSNumber  = "",   
    EmailAddress = "",
    Address = "",
    City = "",
    State = "",
    Zip = "",
    SSN_TaxID = "", 
    AffiliateCode=randomCode,
    PaypalEmailAddress=""
}>

<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>	
	<cfset actionType= 4>	
	<cfset UserInfomation= {
		FirstName = userInfo.FIRSTNAME,
		LastName = userInfo.LASTNAME,
		SMSNumber  = userInfo.MFAPHONE,   
		EmailAddress = userInfo.FULLEMAIL,
		
		Address = userInfo.ADDRESS,
		City = userInfo.CITY,
		State = userInfo.STATE,
		Zip = userInfo.POSTALCODE,
		SSN_TaxID = userInfo.SSNTAXID, 
		AffiliateCode= randomCode,
		PaypalEmailAddress=userInfo.PaypalEmail
	}>	
	<cfinvoke method="GetAffiliateCode" component="public.sire.models.cfc.affiliate" returnvariable="GetAffiliateCode"></cfinvoke>
	<cfif GetAffiliateCode.RXRESULTCODE EQ 1 AND GetAffiliateCode.AFFILIATECODE NEQ "">  
		<cfset UserInfomation.AffiliateCode= GetAffiliateCode.AFFILIATECODE >	
		<cflocation  url="/session/sire/pages/affiliates-dashboard"  addtoken="false">
	</cfif>
</cfif>

<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<header class="sire-banner2 affiliate-banner header-banners">
   
   <section class="landing-top"> 
		<div class="container-fluid">
			<div class="row">
				<div class="public-logo">
					<div class="landing-logo clearfix">
						<a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
							<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
							<img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
						</a>
					</div>
				</div>
				
				<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
					<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
					<div class="header-nav pull-right" style="margin-right: 30px">
						<cfinclude template="../views/commons/menu.cfm">    
					</div>  
				<cfelse>
					<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
					<div class="header-nav">
						<cfinclude template="../views/commons/menu_public.cfm">
					</div>  
				</cfif>
						
				<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
					&nbsp;	
				<cfelse>
					<cfinclude template="../views/commons/public_signin.cfm">  
				</cfif>
			</div>
		</div>		
	</section>
   
   
   <section class="landing-jumbo how-it-works-lj">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">

                    <h1 class="sire-title-updated">Affiliate Referral Program</h1>

                    <p class="banner-sub-title ">
                        <i>Partner with us. Get paid by us.</i>
                    </p>					
                    <div class=" col-sm-12 col-xs-12 text-center existing-user-affiliate <cfoutput>#structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1? 'affiliates-loggedIn': ''#</cfoutput>">
						<a href="#" class="get-started mt-30">Sign Me Up</a>
					</div>						
                        
                </div>	
            </div>	 
        </div>
    </section>
        
</header>


<section class=" feature-white">
	<div class=" feature-item-info">
		<div class="container text-center">
			<div class="row feature-items ">	
				<div class="inner-home-block-3">
					<h4>Refer Your Customers and Get Paid!</h4>
				</div>
				<div class="col-sm-12">					
					<p class="hasBold">
						Sire’s text marketing platform has helped thousands of companies increase sales. </br>
						Now it’s your turn to get paid. Receive a monthly commission on every referral as long as that</br> 
						account remains an active paid user.  Earning cash has never been easier. Help others earn </br>
						more revenue by tapping into the power of SMS marketing and increase your income too.
					</p>
				</div>
			</div>			
		</div>
	</div>
</section>
<section class="home-block-3 feature-gray">
	<div class=" feature-item-info">
		<div class="container">
			<div class="inner-home-block-3">
				<div class="row text-left">					
					<div class="col-sm-4">
						<div class="wrap-feature-item">
							<div class="text-center hold-img">
								<img class="feature-img" alt="" src="/public/sire/images/affiliate/1.png">
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="wrap-feature-item feature-item-info">
							<div class="inner-info">
								<h4>How It Works </h4>
								<div class="newfeature-title">Share your Referer Code</div>
								<p class="hasBold">						
									Each affiliate is given a unique referral code.</br>
									Every account that signs up for Sire with your code will</br>
									be tracked in your account.
								</p>
							</div>
						
						</div>						
					</div>
				</div>
				<div class="row text-left">	
					<div class="col-sm-4 col-sm-push-8">
						<div class="wrap-feature-item">
							<div class="text-center hold-img">
								<img class="feature-img" alt="" src="/public/sire/images/affiliate/2.png">
							</div>
						</div>
					</div>	
					<div class="col-sm-8 col-sm-pull-4">						
						<div class="wrap-feature-item feature-item-info">
							<div class="inner-info">								
								<div class="newfeature-title">Affiliate Dashboard Interface</div>
								<p class="hasBold">						
									The easy-to-use Affiliate Dashboard lets you view all your referrals,</br>
									monitor your account progress, view monthly reports and</br>
									track your commission.
								</p>
							</div>						
						</div>	
					</div>				
									
				</div>
				<div class="row text-left">					
					<div class="col-sm-4">
						<div class="wrap-feature-item">
							<div class="text-center hold-img">
								<img class="feature-img" alt="" src="/public/sire/images/affiliate/3.png">
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="wrap-feature-item feature-item-info">
							<div class="inner-info">								
								<div class="newfeature-title">Get Paid</div>
								<p class="hasBold">						
									Your monthly commission check will be automatically</br>
									calculated and sent to you via Paypal.  Commissions are paid</br>
									on the 15th of every month.
								</p>
								<div class="existing-user-affiliate <cfoutput>#structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1? 'affiliates-loggedIn': ''#</cfoutput>">
									<a href="#" class="get-started mt-30">get started</a>
								</div>
							</div>						
						</div>													
					</div>
				</div>				
			</div>
		</div>
	</div>
</section>
<section class="home-block-3 feature-white">
	<div class=" feature-item-info">
		<div class="container text-center">
			<div class="row feature-items ">	
				<div class="inner-home-block-3">
					<h4>Commission</h4>
				</div>
				<div class="col-sm-12 ">
					<p class="hasBold">
						Enjoy a simple and straightforward income structure.</br>
						The volume driven tiers reward you for bringing more clients onboard.</br>
						The more people you refer, the higher the odds some will convert, increasing your income every month.

					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="item">
						<ul class="nav">
							<li class="c-title">TOTAL ACTIVE ACCOUNTS</li>
							<li class="c-total-user">1-25</li>
							<li class="c-rate">15%</li>
							<li class="c-rate-text">Commission Rate</li>							
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="item">
						<ul class="nav nav-list">
							<li class="c-title">TOTAL ACTIVE ACCOUNTS</li>
							<li class="c-total-user">26-100</li>
							<li class="c-rate">20%</li>
							<li class="c-rate-text">Commission Rate</li>							
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="item">
						<ul class="nav nav-list">
							<li class="c-title">TOTAL ACTIVE ACCOUNTS</li>
							<li class="c-total-user">101+ &nbsp;&nbsp;</li>
							<li class="c-rate">25%</li>
							<li class="c-rate-text">Commission Rate</li>							
						</ul>
					</div>
				</div>				
			</div>	
			<div class="row">
				<div class=" col-sm-12 col-xs-12 existing-user-affiliate <cfoutput>#structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1? 'affiliates-loggedIn': ''#</cfoutput>">
					<a href="#" class="get-started mt-30">get paid</a>
				</div>		
			</div>	
		</div>
	</div>
</section>
<section class="home-block-3 feature-gray">
	<div class=" feature-item-info">
		<div class="container">
			<div class="inner-home-block-3">
				<div class="row">	
					
					<h4>You’re not alone </h4>					
					<div class="col-sm-12">
						<p class="hasBold">
							All of our affiliates have access to our Customer Experience Specialists.</br> 
							Our goal is to make sure you’re equipped with the knowledge and support to</br>
							accelerate the growth of your accounts. It truly is a symbiotic relationship where</br>
							we win if we can help you win. We’ll be here for you every step of the way as you </br>
							share the features and benefits of text message marketing with your network of</br>
							contacts. You bring the relationships and we’ll bring the expertise.  
						</p>																		
					</div>
				</div>	
				<div class="row text-left">					
					<strong >
					<div class="col-sm-12 col-md-2">	
					</div>
					<div class="col-sm-8 col-md-4">						
						<ul class="new-list list-mb ">	
							<li>In-depth Product Training</li>	
							<li>Technical and customer Support</li>	
							<li>Bonus Keywords</br>for your demo account</li>	
							<li>Admin Affiliate Dashboard</li>																												
						</ul>												
					</div>
					<div class="col-sm-8 col-md-4">
						<ul class="new-list list-mb">	
							<li>Custom Referral Codes</li>
							<li>Unique Tracking Link</li>
							<li>180 day cookie tracking</li>
							<li>Free Marketing Landing Pages</li>
							<li>All Promo Codes are eligible</li>																				
							
						</ul>	
					</div>
					<div class="col-sm-12 col-md-2">	
					</div>
					</strong>
				</div>
			</div>
		</div>
	</div>
</section>

<cfoutput>
<div class="modal fade" id="mdSignup" role="dialog" tabindex="-1" >
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">                
				<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title gray-text"><strong >Affiliate Sign-up</strong></h4>				
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-xs-12">                             
                            <form name="md-signup_form" id="md-signup_form" autocomplete="off">
                                                    
                                <input type="hidden" name="actionType" id="actionType1" value="#actionType#">
                                <input type="hidden" name="select-plan-2" id="select-plan-1" value="1">
                                <input type="hidden" name="affiliatetype" id="affiliatetype1" value="#affiliatetype#">
                                <input type="hidden" name="AMI" id="AMI1" value="0">
                                            
                                <div class="uk-grid uk-grid-small" uk-grid>
                                    <div class="uk-width-1-2 ">
                                        <div class="form-group">
                                            <label for="">First name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="fname1" placeholder="" maxlength="50" value="#UserInfomation.FirstName#" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname1" placeholder="" maxlength="50" value="#UserInfomation.LastName #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>
									<cfif actionType NEQ 4>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">SMS Number</label>
                                            <input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number1" placeholder="( ) - " value="#UserInfomation.SMSNumber  #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">E-mail Address</label>
                                            <input type="text" class="form-control validate[required,custom[email]]" id="emailadd1" placeholder="" value="#UserInfomation.EmailAddress #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Password</label>
                                            <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup1" placeholder="">
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Confirm Password</label>
                                            <input type="password" class="form-control validate[required,equals[inpPasswordSignup1]]" id="inpConfirmPassword1" placeholder="">
                                        </div>
                                    </div>
                                    </cfif>
                                    
                                    
                                    <div class="uk-width-1-1">
                                        <div class="form-group">                                        
                                            <label for="">Address</label>
                                            <input type="text" class="form-control validate[required]" id="address1" placeholder="" value="#UserInfomation.Address #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control validate[required]" id="city1" placeholder="" value="#UserInfomation.City #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" class="form-control validate[required]" id="state1" placeholder="" value="#UserInfomation.State #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Zip</label>
                                            <input type="text" class="form-control validate[required]" id="zip1" placeholder="" value="#UserInfomation.Zip #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">SSN/Tax ID</label>
                                            <input type="text" class="form-control validate[required]" id="taxid1" placeholder="" value="#UserInfomation.SSN_TaxID #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Paypal E-mail Address</label>&nbsp;<span>(Commission payments will be sent to this account)</span>
                                            <input type="text" class="form-control validate[required,custom[email]]" id="paypalemailadd1" placeholder="" value="#UserInfomation.PaypalEmailAddress #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Affiliate Code</label>
                                            <input type="text" class="form-control validate[required]" id="affiliatecode1" value="#ami NEQ '' ? ami: UserInfomation.AffiliateCode #" #ami NEQ '' ? 'readonly': ''# placeholder="">
                                        </div>
                                    </div>                
                                    <div class="uk-width-1-1 last-box-newsignup">
                                        <div class="">
                                            <label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
                                                <input name="agree1" class="validate[required]" id="agree1" type="checkbox"> 
                                                <span>I’ve read and agree to the <a href="/affiliate-term-of-use" target="_blank">Terms and Conditions of Use.</a></span>
                                            </label>
                                        </div>                                    
                                    </div>

                                    <div class="uk-width-1-1">
                                        <button class="btn newbtn green-gd mb-50" id="btn-new-signup">get started!</button>
                                    </div>
                                </div>
                            </form>   
                        </div>   
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
</cfoutput>



<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.autogrow-textarea.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/live_demo.js">
</cfinvoke>
	
<cfparam name="variables.body_class" default="body-live-demo">


<cfinclude template="../views/layouts/home.cfm">
<cfif rc EQ "y" AND structKeyExists(Session,'UserID') AND Session.UserID GT 0>
	<script>
		$("#mdSignup").modal("show");
		var md = new MobileDetect(window.navigator.userAgent);
		
		if(md.phone() !== null || md.tablet() !== null){
			if(!$("body").hasClass('startFreeOpen1')){
				$("body").addClass('startFreeOpen1')
			}
		}
	</script>
</cfif>


