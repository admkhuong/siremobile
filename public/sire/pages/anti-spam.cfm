<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1>
    <cfinclude template="../views/commons/fixed-signup.cfm">
</cfif>

 <div class="container">
         
        <section class="static-page-container">
        
            <div class="static-page-title AlignCenter">Anti-Spam Policy</div>
            <div class="last-update AlignCenter">Last updated: November 25, 2015</div>
            <div class="antispam">
 			<p><strong><em>SIRE</em> will NOT knowingly do business with any company or person that sends unsolicited SMS messages.</strong></p>
             
            <p><b>Our Definition of Spam<br /></b></p>
            <p>We consider any unsolicited, unexpected, or unwanted text message as Spam. We do NOT allow use of 3rd party lists, whether consent has been gathered or not. We believe that any type of communication sent to a subscriber about an unrelated subject, that the subscriber did not request, to be Spam.</p>
            <p>We do not condone unsolicited messages, notifications, alerts or any message that you may send to someone who did not provide thier address or mobile number.</p>
            
            <p><b>Our Opt-In Policy<br /></b></p>            
            <p>The <i>SIRE</i> text messaging system allows only verified opt-in subscriptions. It ensures that all subscribers can opt-out quickly, easily, and permanently from unwanted SMS or other text communications. This is done either from our website tools or by sending a text message from your phone to any of the the short codes used by our system with the word 'STOP, REMOVE, OPT OUT or CANCEL'.</p>
                        
            <p><b>Our Anti-Spam Policy Enforcement<br /></b></p>
            <p>Any evidence of users not adhering to this anti-spam policy will cause immediate disconnection of their spam messages sending, suspension of the service and might also result in freezing of all of their SMS accounts to ensure an immediate end to the violation until the details of the case are cleared up.</p>
                        
            <p><b>SPAM or Abuse Reporting<br /></b></p>
            <p>Please let us know about any abuse or unwanted/unsolicited messages you are recieving. In your email, please include your mobile number, the date and time you received it and the contents of the message. Please send your details to <a href="mailto:abuse@siremobile.com">abuse@siremobile.com</a></p>

            </div>
	</section>

</div>

<cfparam name="variables._title" default="Anti Spam - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">

