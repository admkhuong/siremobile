<cfparam name="url.rf" default="">
<cfparam name="url.type" default="">

<cfparam name="url.coupon" default="">
<cfparam name="url.fromSource" default="">

<cfset variables._title = 'Sign Up - Sire'>
<cfset variables.body_class = 'sign_up_page'>
<cfset hideFixedSignupForm = '1'>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/start-free.js">
	</cfinvoke>
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
		</cfinvoke>
		<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
			<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
			</cfinvoke>

			<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans">
			</cfinvoke>

			<cfif plans.RXRESULTCODE NEQ 1>
				<cflocation url="/" addtoken="false"/>
			</cfif>

			<section class="landing-top">
				<div class="container-fluid">
					<div class="row">
						<div class="public-logo">
							<div class="landing-logo clearfix">
								<a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
									<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
									<img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
								</a>
							</div>
						</div>
						
						

						<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
							<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
							<div class="header-nav pull-right" style="margin-right: 30px">
								<cfinclude template="../views/commons/menu.cfm">    
							</div>  
						<cfelse>
							<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
							<div class="header-nav">
								<cfinclude template="../views/commons/menu_public.cfm">
							</div>  
						</cfif>
						
						<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
							&nbsp;	
						<cfelse>
							<cfinclude template="../views/commons/public_signin.cfm">  
						</cfif>
					</div>
				</div>		
			</section>

			<!-- For referral -->
			<input type="hidden" id="rf" name="rf" value="<cfoutput>#url.rf#</cfoutput>">
			<input type="hidden" id="rftype" name="rftype" value="<cfoutput>#url.type#</cfoutput>">
			<input type="hidden" value="" name="plan" id="plan">
			<input type="hidden" value="" name="" id="is-yearly-plan">

			

			<div class="bg-start-free">
				<div class="uk-container uk-container-center">
					<div class="uk-grid" uk-grid>
						<div class="uk-width-1-1">
							<div class="inner-start-free">
								<h4 class="start-title">LET'S GET STARTED!</h4>
								<!--- <p class="start-italic"><i>No credit card required</i></p> --->
								<p class="start-italic">&nbsp;</p>

								<div class="box-start-free" id="section-1">
									<form name="signup_form" id="signup_form" autocomplete="off">
										<div class="uk-grid uk-grid-medium" uk-grid>
											<div id="parent-height-follow" class="uk-width-1-2@m">
												<div class="uk-grid uk-grid-small" uk-grid>
													<div class="uk-width-1-2">
														<div class="form-group">
															<label for="">First Name</label>
															<input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50">
														</div>
													</div>

													<div class="uk-width-1-2">
														<div class="form-group">
															<label for="">Last Name</label>
															<input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50">
														</div>
													</div>
													
													<div class="uk-width-1-1">
														<div class="form-group">
															<label for="sms_number">SMS Number</label>
															<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - ">
														</div>
													</div>
													
													<div class="uk-width-1-1">
														<div class="form-group">
															<label for="">E-mail Address</label>
															<input type="text" class="form-control validate[required,custom[email]]" id="emailadd" placeholder="">
														</div>
													</div>
													
													<div class="uk-width-1-1">
														<div class="form-group">
															<label for="">Password</label>
															<input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" placeholder="">
														</div>
													</div>

													<div class="uk-width-1-1">
														<div class="form-group has-ok">
															<label for="">Confirm Password</label>
															<input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" placeholder="">
														</div>
													</div>
												</div>
											</div>

											<div class="uk-width-1-2@m">
												<div id="implicitly-cal-height" class="uk-height-1-1 uk-position-relative">
													<div class="uk-grid uk-grid-small" uk-grid>
														<div class="uk-width-1-1">
															<div class="form-group monthly-yearly-signup">
																<label for="">Plan Billing Type</label>
																<div class="text-center month-year-plan-switch">
																	<div class="inner">
																		<span class="month-year-plan-text" id="monthly-plan-text">MONTHLY</span>
																		<label class="switch">
																			<input id="month-year-switch" type="checkbox">
																			<span class="slider round"></span>
																		</label>
																		<span class="month-year-plan-text" id="yearly-plan-text">YEARLY</span>
																		<span class="triangle-isosceles left">Save up to 20%</span>
																	</div>
																</div>
															</div>
															<div class="form-group">
																<label for="">Plans</label>
																<select class="form-control validate[required]" id="select-plan-1">
																	<option id="no-plan-selected" value="" disabled selected>Select a plan</option>
																	<cfoutput>
																		<cfloop query="#plans.DATA#">
																			<cfif url.coupon eq "freeKit" && url.fromSource eq "restaurantmarketing">
																				<cfif plans.DATA.name neq "Free"><option class="monthly-plan" value="#id#"><b>#name#</b> - $#price#/month</option></cfif>
																			<cfelse>
																				<option class="monthly-plan" value="#id#"><b>#name#</b> - $#price#/month</option>
																			</cfif>
																			<cfif url.coupon eq "freeKit" && url.fromSource eq "restaurantmarketing">
																				<cfif plans.DATA.name neq "Free"><option class="annually-plan" style="display: none" value="#id#"><b>#name#</b> - $#yearlyAmount*12#/year</option></cfif>
																			<cfelse>
																				<option class="annually-plan" style="display: none" value="#id#"><b>#name#</b> - $#yearlyAmount*12#/year</option>
																			</cfif>		    											
																		</cfloop>
																	</cfoutput>
																</select>																
															</div>

															<div class="content-st">
																<cfoutput>
																	<cfloop query="#plans.DATA#">
																		<div class="item" id="#id#">
																			<div class="uk-grid uk-grid-small" uk-grid>
																				<div class="uk-width-1-2@s">
																					<ul>
																						<li>First #FirstSMSIncluded_int# SMS Included</li>
																						<li>#KeywordsLimitNumber_int# User Reserved <a data-toggle="modal" title="What is a Keyword?" href="##" data-target="##InfoModalKeyword">Keywords</a></li>
																						<li>#PriceMsgAfter_dec# Cents per Msg after</li>
																					</ul>
																				</div>

																				<div class="uk-width-1-2@s">
																					<ul>
																						<li>#(MlpsLimitNumber_int EQ 0 ? "Unlimited" : MlpsLimitNumber_int)# <a data-toggle="modal" title="What is a MLP?"  href="##" data-target="##InfoModalMLP">MLPs</a></li>
																						<li>#(ShortUrlsLimitNumber_int EQ 0 ? "Unlimited" : ShortUrlsLimitNumber_int)# Short URLs</li>
																					</ul>
																				</div>
																			</div>
																		</div>
																	</cfloop>
																</cfoutput>
															</div>
														</div>
													</div>

													<div class="secure-password">
														<h4>Minimum Secure Password Requirements</h4>
														<ul>
															<li>must be at least 8 characters</li>
															<li>must have at least 1 number</li>
															<li>must have at least 1 letter</li>
														</ul>
													</div>
												</div>
											</div>

											<div class="uk-width-1-1">
												<div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
													<label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
														<input class="" id="agree" type="checkbox"> 
														<span>I’ve read and agree to the <a href="/term-of-use">Terms and Conditions of Use.</a></span>
													</label>
												</div>
											</div>

											<div class="uk-width-1-1">
												<button class="btn newbtn green-gd" id="start-btn" disabled>get started!</button>
											</div>
										</div>
									</form>
								</div>

								<div class="box-start-free" style="display:none" id="section-2">
									<form name="payment_form" id="payment_form" autocomplete="off">
										<div class="uk-grid uk-grid-medium" uk-grid>
											<div class="uk-width-1-1">
												<h4 class="payment-title">Secure Payment</h4>

												<div class="uk-grid" uk-grid>
													<div class="uk-width-1-2@m">
														<div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
															<div class="uk-width-auto">
																<div class="form-group">
																	<span>Select Your Plan:</span>
																</div>
															</div>
															<div class="uk-width-1-3@m">
																<select name="select-plan-2" id="select-plan-2" class="form-control">
																	<cfoutput>
																		<cfloop query="#plans.DATA#">
																			<cfif url.coupon eq "freeKit" && url.fromSource eq "restaurantmarketing">
																				<cfif plans.DATA.name neq "Free"><option value="#id#">#name#</option></cfif>
																			<cfelse>
																				<option value="#id#">#name#</option>
																			</cfif>	
																		</cfloop>
																	</cfoutput>
																</select>

																<!--- <select name="select-plan-2" id="select-plan-2" class="form-control annually-plan" style="display: none">
																	<cfoutput>
																		<cfloop query="#plans.DATA#">
																			<cfif url.coupon eq "freeKit" && url.fromSource eq "restaurantmarketing">
																				<cfif plans.DATA.name neq "Free"><option value="#id#">#name#</option></cfif>
																			<cfelse>
																				<option value="#id#">#name#</option>
																			</cfif>	
																		</cfloop>
																	</cfoutput>
																</select> --->
															</div>
														</div>

														<div class="uk-grid uk-grid-small uk-flex-middle">
															<div class="uk-width-auto">
																<div class="form-group">
																	<span>Monthly Amount:</span>
																</div>
															</div>
															<div class="uk-width-expand monthly-plan">
																<cfoutput>
																	<cfloop query="#plans.DATA#">
																		<span id="span-plan-price-#id#" class="span-plan-price">$#price#</span>
																	</cfloop>
																</cfoutput>
															</div>

															<div class="uk-width-expand annually-plan" style="display: none">
																<cfoutput>
																	<cfloop query="#plans.DATA#">
																		<span id="span-plan-price-#id#" class="span-plan-price">$#yearlyAmount*12#</span>
																	</cfloop>
																</cfoutput>
															</div>
														</div>
													</div>

													<div class="uk-width-1-2@m">
														<div class="form-group">
															<div class="uk-grid uk-grid-small " uk-grid>
																<div class="uk-width-auto">
																	<div class="form-group" style="margin-top: 7px;">
																		<span>Coupon Code: </span>
																	</div>
																</div>

																<div class="uk-width-1-3@m">
																	<input type="text" id="inpPromotionCode" class="form-control">
																	<span class="help-block help-block-coupon-signup-input" style="display: none;font-weight: bold;" id="help-block"></span>
																</div>

																<div class="uk-width-expand" id="divGroupButton">
																	<button class="btn green-gd" id="apply-btn">apply</button>
																	<button class="btn green-cancel" id="remove-btn" style="display: none">remove</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="uk-width-1-1 uk-grid-margin uk-first-column">
												<h4 class="payment-title section-payment">Payment Information</h4>

												<div class="uk-grid uk-grid-medium holder-payment" >
													<div class="uk-width-1-2@m section-payment">
														<div class="uk-grid uk-grid-small" >
															<div class="uk-width-1-2@s">
																<div class="form-group">
																	<label for="cardHolderFirtsName"><b>Cardholder Name </b> <span>*</span></label>
																	<input type="text" id="cardHolderFirtsName" placeholder="First name" class="form-control validate[required,custom[onlyLetterNumberSp]]">
																</div>
															</div>

															<div class="uk-width-1-2@s">
																<div class="form-group">
																	<label for="cardHolderLastName">&nbsp;</label>
																	<input type="text" id="cardHolderLastName" placeholder="Last name" class="form-control validate[required,custom[onlyLetterNumberSp]]">
																</div>
															</div>

															<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
																<div class="form-group">
																	<label for=""><b>Card Number </b> <span>*</span></label>
																	<input type="text" id="cardNumber" class="form-control validate[required,custom[creditCardFunc]]">
																</div>
															</div>

															<div class="uk-width-1-3@s uk-grid-margin">
																<div class="form-group">
																	<label for=""><b>Security Code </b> <span>*</span></label>
																	<input type="text" id="securityCode" class="form-control validate[required,custom[onlyNumber]]" maxlength="4">
																</div>
															</div>
															
															<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
																<div class="form-group">
																	<label for="expireMonth"><b>Expiration Date </b> <span>*</span></label>
																	<select name="expireMonth" id="expireMonth" class="form-control validate[required]">
																		<cfoutput>
																			<cfloop from="1" to="12" index="month">
																				<option value="#(month LT 10 ? "0"&month : month)#">#(month LT 10 ? "0"&month : month)#</option>
																			</cfloop>
																		</cfoutput>
																	</select>
																</div>
															</div>

															<div class="uk-width-1-3@s uk-grid-margin">
																<div class="form-group">
																	<label for="expireYear">&nbsp;</label>
																	<select name="expireYear" id="expireYear" class="form-control validate[required]">
																		<cfoutput>
																			<cfset currYear = year(now())/>
																			<cfloop from="0" to="9" index="year">
																				<option value="#(currYear+year)#">#(currYear+year)#</option>
																			</cfloop>
																		</cfoutput>
																	</select>
																</div>
															</div>
														</div>											
													</div>

													<div class="uk-width-1-2@m section-payment">
														<div class="uk-grid uk-grid-small" >
															<div class="uk-width-1-1">
																<div class="form-group">
																	<label for="address"><b>Address </b> <span>*</span></label>
																	<input type="text" id="address" class="form-control validate[required,custom[noHTML]]">
																</div>
															</div>

															<div class="uk-width-1-1 uk-grid-margin uk-first-column">
																<div class="form-group">
																	<label for="city"><b>City </b> <span>*</span></label>
																	<input type="text" id="city" class="form-control validate[required,custom[noHTML]]">
																</div>
															</div>
															
															<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
																<div class="form-group">
																	<label for="state"><b>State </b> <span>*</span></label>
																	<input type="text" id="state" class="form-control validate[required,custom[noHTML]]">
																</div>
															</div>

															<div class="uk-width-1-3@s uk-grid-margin">
																<div class="form-group">
																	<label for="zipcode"><b>Zip Code</b> <span>*</span></label>
																	<input type="text" id="zipcode" class="form-control validate[required,custom[onlyNumber]]" maxlength="5">
																</div>
															</div>
														</div>
													</div>

													<div class="uk-width-1-1 uk-grid-margin uk-first-column">
														<button class="btn newbtn green-gd" id="purchase-btn">purchase</button>
														<button class="btn newbtn green-cancel" id="back-btn">back</button>
														<button class="btn newbtn green-cancel" id="cancel-btn">cancel</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div id="InfoModalKeyword" class="modal fade" role="dialog">
				<div class="modal-dialog new-modal">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">What is a Keyword?</h4>
						</div>
						<div class="modal-body">
							<p class="text-center">
								Imagine if Joe’s Coffee Shop offers this promotion: Text “FreeCoffee” to 39492 and receive a free coffee on your next visit.  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.  
							</p>
						</div>
					</div>

				</div>
			</div>
			<!-- Modal -->
			<div id="InfoModalMLP" class="modal fade" role="dialog">
				<div class="modal-dialog new-modal">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">What is a MLP?</h4>
						</div>
						<div class="modal-body">
							<p class="text-center">
								Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once customers clicks on that link they will be sent to your own Marketing Landing Page (MLP) displaying the coupon.  It’s a standalone web page promoting your offer and the best part is it’s included for free!
							</p>
						</div>
					</div>

				</div>
			</div>

			<cfparam name="variables._title" default="Start 1">

			<cfinclude template="../views/layouts/home.cfm">