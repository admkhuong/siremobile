<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/waitlist-html.css">
</cfinvoke>

<cfoutput>
<main class="">
    <div class="brand-banner">
    	<img src="/public/sire/images/logo-v14.png">
    	<p>4533 MacArthur Blvd, Suite 1090</p>
		<p>Newport Beach, CA 92660</p>
		<p>(888) 747-4411</p>
    </div>
    <div class="thumb">
    	<div class="item rd">
			<div class="cell">
				<p class=""><span>Your visit was canceled or already over</span></p>
			</div>
    	</div>
    </div>
</main>
</cfoutput>

<cfparam name="variables._title" default="Customer Waitlist View - Sire">

<cfinclude template="../views/layouts/waitlist-customer-layout.cfm">