<cfparam name="url.token" default="0">

<cfinvoke component="session.sire.models.cfc.waitlistapp" method="GetCustomerWaitlistInfoByToken" returnvariable="waitlistInfo">
	<cfinvokeargument name="inpCustomerToken" value="#url.token#">
</cfinvoke>

<cfif waitlistInfo.RXRESULTCODE EQ -1 OR waitlistInfo.RXRESULTCODE EQ -2>
	<cflocation url="/error-page" addToken="no" />
<cfelseif waitlistInfo.ESTIMATEDWAIT LT 0>
	<cflocation url="/waitlist-customer-cancel" addToken="no" />
</cfif>

<cfdump var="#waitlistInfo#">
<div class="row">
	<label>Your position is: <cfoutput>#waitlistInfo.POSITION#</cfoutput></label>
	<label>Total: <cfoutput>#waitlistInfo.TOTALINLINE#</cfoutput></label>
	<label>Ahead progress: <cfoutput>#waitlistInfo.AHEADINLINE#</cfoutput></label>
	<label>Estimated waiting time:<cfoutput>#waitlistInfo.ESTIMATEDWAIT#</cfoutput></label>
</div>