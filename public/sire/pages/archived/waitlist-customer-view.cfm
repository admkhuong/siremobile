<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/waitlist-html.css">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/wait-list-app.js">
</cfinvoke>

<cfparam name="url.token" default="0">

<cfinvoke component="public.sire.models.cfc.waitlistapp" method="GetWaitingCustomer" returnvariable="waitlistInfo">
    <cfinvokeargument name="inpCustomerToken" value="#url.token#">
</cfinvoke>

<cfinvoke component="public.sire.models.cfc.waitlistapp" method="GetWaitlistStatusByToken" returnvariable="waitlistStatus">
    <cfinvokeargument name="inpCustomerToken" value="#url.token#">
</cfinvoke>

<cfif waitlistStatus.RXRESULTCODE EQ -1 OR waitlistStatus.RXRESULTCODE EQ -2>
    <cflocation url="/error-page" addToken="no" />
<cfelseif waitlistStatus.STATUS GT 1>
    <cflocation url="/waitlist-customer-cancel" addToken="no" />
<cfelseif waitlistStatus.WAITTIME EQ 0>
    <cfoutput>
        <main class="">
            <div class="brand-banner">
                <img src="/public/sire/images/logo_top@3x.png">
                <p>4533 MacArthur Blvd, Suite 1090</p>
                <p>Newport Beach, CA 92660</p>
                <p>(888) 747-4411</p>
            </div>
            <div class="thumb">
                <div class="item st">
                    <div class="cell">
                        <p>You are #waitlistInfo.POSITION# of #waitlistInfo.TOTALINLINE# in line</p>
                    </div>
                </div>
                <div class="item nd">
                    <div class="cell">
                        <p class="em">#waitlistInfo.AHEADINLINE# Ahead <span>Progress</span></p>
                    </div>
                </div>
            </div>
            <button href="" class="btn-back-custom" id="cancel-customer-waitlist">Cancel visit</button>
        </main>
    </cfoutput>
<cfelseif waitlistInfo.ESTIMATEDWAIT LT 0>
    <cflocation url="/waitlist-customer-cancel" addToken="no" />
<cfelse>
    <cfset WaitTime_int = waitlistInfo.ESTIMATEDWAIT />
    <cfif WaitTime_int GT 60>
        <cfset waitTime = WaitTime_int\60 & ' hour' />
        <cfif WaitTime_int%60 GT 0>
            <cfset waitTime = waitTime & ' ' & WaitTime_int%60 & ' min'/>
        </cfif>
    <cfelseif WaitTime_int EQ 60>
        <cfset waitTime = '1 hour' />
    <cfelseif WaitTime_int GT 0>
        <cfset waitTime = '#WaitTime_int#' & ' min' />
    <cfelse>
        <cfset waitTime = '' />
    </cfif>
    <cfoutput>
        <main class="">
            <div class="brand-banner">
    	<img src="/public/sire/images/logo-v14.png">
                <p>4533 MacArthur Blvd, Suite 1090</p>
                <p>Newport Beach, CA 92660</p>
                <p>(888) 747-4411</p>
            </div>
            <div class="thumb">
                <div class="item st">
                    <div class="cell">
                        <p>You are #waitlistInfo.POSITION# of #waitlistInfo.TOTALINLINE# in line</p>
                    </div>
                </div>
                <div class="item nd">
                    <div class="cell">
                        <p class="em">#waitlistInfo.AHEADINLINE# Ahead <span>Progress</span></p>
                    </div>
                </div>
                <div class="item rd">
                    <div class="cell">
                        <p class="em">#waitTime# Minutes <span>Estimated Wait</span></p>
                    </div>
                </div>
            </div>
            <button href="" class="btn-back-custom" id="cancel-customer-waitlist">Cancel visit</button>
        </main>
    </cfoutput>
</cfif>



<cfparam name="variables._title" default="Customer Waitlist View - Sire">

<cfinclude template="../views/layouts/waitlist-customer-layout.cfm">