<h1>Sire Investments, LLC California Privacy Rights</h1>
<h4>Your Privacy Rights — Notice to California Residents</h4>
<p>If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by Sire Investments, LLC or its subsidiaries to a third party for the third party's direct marketing purposes. To make such a request, please send an email to <a href="mailto:privacy@siremobile.com?subject=In%20Regards%20To%20California%20Privacy%20Rights" data-om-config="no_area_name,privacy@siremobile">privacy@siremobile.com</a> or write us:</p>
<address>
 Sire Investments, LLC
<br>
123 anywhere
</address>
<address>
somewhere, CA 92660
<br>
Attention: Privacy Manager
</address>


