<cfif StructKeyExists(url,"inpCode")>
	<cfset hideFixedSignupForm = '1'>
	<cfset inpCode = url['inpCode'] />
	<cfinvoke component="public.cfc.users" method="CheckLinkInValid" returnvariable="CheckPemission">
		<cfinvokeargument name="inpCode" value="#inpCode#">
	</cfinvoke>

	<cfif CheckPemission.RXRESULTCODE eq 1>

	<header class="home_signup clearfix">
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-4">
						<div class="landing-logo">
							<a href="/">
								<img class="logo-top" src="/public/sire/images/logo-v14.png">
							</a>
						</div>
					</div>
					<div class="col-sm-4">
						<nav class="nav-top">

						</nav>
					</div>
				</div>
			</div>
		</section>

		<section class="main-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-1 col-lg-2"></div>
					<div class="col-sm-10 signup_title">
						<p class="device_reg_title"><span><strong>Change your password</strong></span></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-10 col-lg-8 col-sm-offset-1 col-lg-offset-2 wraper_public_form">
						<div class="row">
							<cfoutput>
								<form name="changepassword" id="change_password" class="col-sm-6 col-xs-11 signup_form" autocomplete="off">
								  <input type="hidden" name="inpCode" value="#inpCode#">
								  <div class="form-group">
								    <label for="ChangePasswordPassword">Password</label>
								    <input type="password" required="required" class="form-control validate[required,funcCall[isValidPassword]]" maxlength="50" id="ChangePasswordPassword" name="inpPassword">
								  </div>
								  <div class="form-group">
								    <label for="ChangePasswordConfirm">Confirm</label>
								    <input type="password" required="required" class="form-control validate[required,equals[ChangePasswordPassword]]" maxlength="50" id="ChangePasswordConfirm">
								  </div>
								  <div class="change-password-message"></div>
								  <div class="form-group public-form-btn-func">
								  	<button class="btn btn-success btn-success-custom btn-change-password" id="btnChangepassword">Change password</button>
								  </div>
								</form>
								<div class="col-sm-5 col-xs-11 vcenter">
									<h4><strong>Secure Password Requirements</strong></h4>
									<ul>
										<li>must be at least 8 characters</li>
                                                  <li>must contain at least 1 alphabetical character</li>
										<li>must contain at least 1 number</li>
										<!--- <li>must have at least 1 lower case letter</li> --->
										<!--- <li>must have at least 1 special character</li> --->
									</ul>
									<br/>
									<!---<p>A secure sample of one has been randomly generated for you. Feel free to use or replace with your own.</p>--->
								</div>
	                        </cfoutput>
						</div>
					</div>
				</div>
			</div>
		</section>

	</header>

	<!--- <cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
	</cfinvoke> --->
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.min.js">
	</cfinvoke>
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/change_password.js">
	</cfinvoke>

	<cfparam name="variables._title" default="Change your password - Sire">
	<cfinclude template="../views/layouts/home.cfm">

	</cfif>

</cfif>
