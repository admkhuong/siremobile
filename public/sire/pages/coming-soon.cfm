<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Website coming soon</title>
    <style type="text/css">
    	body {
		    margin: 0;
		}
        #header {
        	background: #274e60;
        }
        h1 { text-align: center; }
        img.logo {
        	padding: 20px 0px 20px 20px;
        }
    </style>
</head>
<body>
	<div id="header">
		<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
        <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
	</div>
	
    <h1>Coming soon</h1>
</body>
</html>