<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/contact-us.min.css">
</cfinvoke>
<meta name="format-detection" content="telephone=no">

<header class="sire-banner2 contact-bannernew header-banners">
   
   <section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                        <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
   
   
    <section class="landing-jumbo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                	<!--- <h1 class="banner-title-big hidden-xs">Get in touch</h1>

                    <h3 class="banner-title-small visible-xs">Get in touch</h3> --->
                    
                    <h1 class="sire-title-updated">Get in touch</h1>

				
					<cfif !(structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1) >
						<a href="/signup" class="get-started mt-30">get started</a>
		   			</cfif>
                    
                   
                    <!--- <p class="banner-sub-title hidden-xs"><i>What Can We Do To Help?</i></p> --->
                                          
                    <!--- <h1 class="marketing_title_big hidden-xs">Get in touch</h1>
                    <h2 class="marketing_title visible-xs">Get in touch</h2>
                                            
                    <h4 class="hidden-xs"><i>What Can We Do To Help?</i></h4> --->
                
                </div>	
            </div>	
            
            <!--- <div class="row hidden-xs hidden-sm">
                <div class="scroll-down-spacer_3"></div>
            </div>
            
            <div class="row visible-xs visible-sm">
                <div class="scroll-down-spacer_1"></div>
            </div> --->
            
            <!--- <div class="row">
                <a href="#feature-main">
                <div class="scroll-down">
                    <span>
                        <i class="fa fa-angle-down fa-2x"></i>
                    </span>
                </div>
                </a>
            </div> --->
        
        </div>
    </section>

    <!--- <section class="landing-jumbo landing-jumbo2">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-sm-12 text-center">
    				<cfif !(structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1) >
						<a href="/signup" class="get-started mt-20">get started</a>
		   			</cfif>
    			</div>
    		</div>
    	</div>
    </section> --->
        
</header>

<section class="home-block-3 block-for-contact">
    <div class="container new-container1 contact-us-container">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12">
                    <h4>
                    	Whether you have questions about our service, you want to brag about how well your campaign is doing, or you’re just bored and need a friend, we’re here for you
                    </h4>
                    <p>Feel free to text, fill out the form below or give us a ring.  We look forward to hearing from you!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-block">
	<div class="container new-container1">
		<div class="inner-contact-block">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						<div class="col-lg-10 col-lg-offset-1">
							<form name="contact-us" class="row contact-form" id="contact_form">
								<div class="form-group col-sm-4">
									<!--<input type="text" class="form-control" name="infoName" maxlength="120" placeholder="Name" required>-->
									<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoName" name="infoName" maxlength="120" placeholder="Name" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-8">
									<!--<input type="text" class="form-control" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" >-->
									<input type="text" class="form-control validate[required,custom[email_or_phone]]" id="infoContact" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-12">
									<!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
									<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoSubject" name="infoSubject" maxlength="250" placeholder="Subject" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-12">
									<!--<textarea class="form-control" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" ></textarea>-->
									<textarea class="form-control validate[required,custom[noHTML]]" id="infoMsg" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" data-prompt-position="topLeft:100"></textarea>
								</div>
								<div class="col-sm-12 contact-us-message"></div>
								<div class="form-group col-sm-12 text-center submit-contact">
									<button type="submit" class="btn btn-success btn-success-custom btn-send-message">Send Message</button>
								</div>
							</form>
						</div>
					</div>
							

					
				</div>

				<div class="col-sm-12">
					<div class="row contact-inf">
						<div class="col-sm-12">
							<ul class="nav nav-list">
								<li>4533 MacArthur Blvd #1090 Newport Beach, CA 92660</li>
								<li>For billing or technical questions, Text “support” to <cfoutput>#shortcode#</cfoutput></li>
								<li>For sales and pricing questions, please call (888) 747-4411 between 9am and 5pm, PST</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<!--- <section class="main-wrapper" id="feature-main">


		<div class="container text-center">
               
                   <h2 class="heading-home">We are here to answer any questions that you may have about our service. Feel free to text, email, call or simply fill out the form below and we will get back to you shortly.</h2>
        
        </div> --->
        
        
<!--- <div class="container">
	
		
	<div class="row contact-func">
		<div class="col-sm-8">
			<form name="contact-us" class="row contact-form" id="contact_form">
				<div class="form-group col-sm-4">
					<!--<input type="text" class="form-control" name="infoName" maxlength="120" placeholder="Name" required>-->
					<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoName" name="infoName" maxlength="120" placeholder="Name" data-prompt-position="topLeft:100"/>
				</div>
				<div class="form-group col-sm-7">
					<!--<input type="text" class="form-control" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" >-->
					<input type="text" class="form-control validate[required,custom[email_or_phone]]" id="infoContact" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" data-prompt-position="topLeft:100"/>
				</div>
				<div class="form-group col-sm-11">
					<!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
					<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoSubject" name="infoSubject" maxlength="250" placeholder="Subject" data-prompt-position="topLeft:100"/>
				</div>
				<div class="form-group col-sm-11">
					<!--<textarea class="form-control" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" ></textarea>-->
					<textarea class="form-control validate[required,custom[noHTML]]" id="infoMsg" name="infoMsg" placeholder="Message" rows="6" maxlength="3000" data-prompt-position="topLeft:100"></textarea>
				</div>
				<div class="col-sm-11 contact-us-message"></div>
				<div class="form-group col-sm-11">
					<button type="submit" class="btn btn-success btn-success-custom btn-send-message">Send Message</button>
				</div>
			</form>
		</div>
		<div class="col-sm-4">
			<p>
				4533 MacArthur Blvd, Suite 1090<br>
				Newport Beach, CA 92660<br>
				<br>
				For billing or technical questions, Text "<strong>support</strong>" to <cfoutput>#shortcode#</cfoutput><br>
				<br>
				<!---Mon - Fri 9AM - 6PM Pacific--->
				For sales and pricing questions, please call (888) 747-4411 between 9am and 5pm PST.
			</p>
		</div>
	</div> --->
    
    
</div>

</section>
	
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>	
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/contact_us.js">
</cfinvoke>
	
<cfparam name="variables._title" default="Contact Us">
<cfparam name="variables.body_class" default="">

<cfparam name="variables.meta_description" default="Contact us for a free demo to learn about how text message marketing can help your business grow.  Our SMS marketing experts will help you reach your goals.">
<cfinclude template="../views/layouts/contact_us.cfm">
<script type="text/javascript">
    $('.li-contact').addClass('active');
</script>
