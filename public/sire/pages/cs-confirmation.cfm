

<!---


Store as encrypted cookie on your target device browser
Can you store cookie in IOS app?


Practice has been changed until 

Team,

I want to schedule an additional practice this Thurs at 7:00 at the Orange Terrace Sports Complex. 
Can you make it?




Natural Human Language Responses

Yes

Yes I can go

yup I'll be there


no

nope



Great - See you there!



Bummer - maybe next time.



y,yes,si,ya,yup,yea,yeah,sure,ok,okay


^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay


REGEXP(?i)in|yes


REGEXP(?i)^y$|yes|si|ya|yup|yea|yeah|sure|ok|okay|all right|very well|of course|by all means|sure|certainly|absolutely|indeed|right|affirmative|agreed|roger|aye|uh-huh|okey|aye|positive|^1$|confirm|will|in|join|true



QA2Nowhere
CSConfirmationYESNO


	all right, very well, of course, by all means, sure, certainly, absolutely, indeed, right, affirmative, in the affirmative, agreed, roger; aye aye; informalyeah, yep, yup, ya, uh-huh, okay, OK, okey-dokey, okey-doke; archaicyea, aye
	
	
	Informal
	rodger dodger
		
	Binary
	1
	
	
	
	
Confirmation Message - Natural Human Language	


Message - Confirm - Natural Human Language



--->

<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white">
        <div class="container">
            <div class="row">


                <div class="col-md-6">
                    <div class="feature-content">
                  
                  	<h4>When your message needs confirmation</h4>  
                    
                    <h4>Confirmation message blasts can:</h4>
                    <ul>
                        <li>Let people know about important events</li>
                        <li>Drive traffic</li>
                        <li>Help you better serve your customers</li>
                        <li>Provide valuable feedback</li>
                        
                    </ul>
                    
                    <h4>Sire Advantages</h4>
                    
                    <ul>
                        <li>Natural language processing (NLP)</li>
                        <li>Advanced Branch Logic </li>
                        <li>Template Ideas for your business</li>
                        <li>Not just fire and forget - advanced Session flows</li>
                        <li>Automated template customization based on your Organization's profile settings</li>
                        <li>No App required! Works great on phone and tablets browsers too.</li>
                        <li>Still simple to use</li>
                    </ul>
                    
                    
                    <h4>What makes a good confirmation message?</h4>      
                        
                    <p>By customizing your confirmation message, you provide your subscribers with a message they recognize and understand, two factors that increase the likelihood that they will take that one step to respond to the message.

					We provide the option to customize the initial question of the confirmation message, as well as the confirmation response and closing.

					This knowledge base article is meant to cover what should be written in the message.</p>
                                                   
                       	<h4>Who Is the Message Coming From?</h4>

						<p>You want to make sure that it is clear who the confirmation message is from so that the new subscriber knows that the sender can be trusted. Whether the message is from your company, your website, or you personally, the important thing will be that your subscribers will recognize the sender.</p>

						<h4>Why Was the Message Sent?</h4>

                        <p>This may seem redundant, but you want to make it very clear why you are sending this confirmation message could be as simple as someone just signed up for your SMS list. It's very important here to make the connection between their initial subscription and the confirmation message.</p>
                        
                        <h4>What Needs to Be Done With the Message?</h4>
                        
                        <p>Finally, you need to make it clear that something needs to be done with the message - that confirmation needs to be sent as a reply. This ensures that the message isn't simply ignored or set aside for later and forgotten.</p>

                    </div>
                </div>
                
                
                <div class="col-md-6">
                	                                  
                    <div class="feature-title" id="feature1">Message Confirm Template</div>
                        
                    <div class="feature-content">
                        
                       <div class="row">
                        <div class="form-group clearfix control-point" id="cp1">
                        <div class="col-sm-12">
                        <div class="control-point-border clearfix">
                        <div class="col-sm-12">
                        <label class="control-point-label"># 1. Question that needs to be confirmed. Accepts Natural Human Language yes and no answers. </label>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr0">
                        <div class="col-sm-12 clearfix">
                        <div class="control-point-body">
                        <div class="control-point-text Preview-Bubble Preview-Me ">Team Sharks: I want to schedule an additional practice this Thurs at 7:00 PM at the Orange Terrace Sports Complex. <br>Can you make it? Reply yes or no.</div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        <div class="form-group clearfix control-point" id="cp3">
                        <div class="col-sm-12">
                        <div class="control-point-border clearfix">
                        <div class="col-sm-12">
                        <label class="control-point-label"># 2. Response SMS if yes answer. </label>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr0">
                        <div class="col-sm-12 clearfix">
                        <div class="control-point-body">
                        <div class="control-point-text Preview-Bubble Preview-Me ">Great! See you there.</div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        <div class="form-group clearfix control-point" id="cp4">
                        <div class="col-sm-12">
                        <div class="control-point-border clearfix">
                        <div class="col-sm-12">
                        <label class="control-point-label"># 3. Response SMS if negative answer. </label>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr0">
                        <div class="col-sm-12 clearfix">
                        <div class="control-point-body">
                        <div class="control-point-text Preview-Bubble Preview-Me ">Bummer - see you next time then.</div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                            
                    </div>
				</div>                        
				

            </div>
        </div>
    </div>

	     
    <div class="feature-gray">
        <div class="container">
            <div class="row">


			<div class="col-md-6">                    
                   		                     
                 	<div class="definitionBox white-bg">
                       <h1>yes</h1>
                       <h2>/yes/</h2>
                       
                       <h3><i>exclamation</i></h3>
                       		
                            <div>
                            	<b>1. used to give an affirmative response. </b><br/>
								"“Do you understand?” “Yes.”"<br/>
                                <i>synonyms:</i>		all right, very well, of course, by all means, sure, certainly, absolutely, indeed, right, affirmative, in the affirmative, agreed, roger; aye aye; informalyeah, yep, yup, ya, uh-huh, okay, OK, okey-dokey, okey-doke; archaicyea, aye
                                <ul>                                
	                                <li>
                                    	an instance of this.
                                    	<p>"yes, I'll come to your party"</p>
                                    </li>                                                                    
                                </ul>                           
                            
                            </div>
                       
                 
                 	</div>
                    
                                       
                 
                 </div>	         
                 
                <div class="col-md-6">
                    <div class="feature-title" id="feature1">Natural language processing (NLP)</div>
                    <div class="feature-content">
                                              
                       	<p>Natural language processing (NLP) is a field of computer science, artificial intelligence, and computational linguistics concerned with the interactions between computers and human (natural) languages. As such, NLP is related to the area of human–computer interaction. Many challenges in NLP involve natural language understanding, that is, enabling computers to derive meaning from human or natural language input, and others involve natural language generation.</p>

                    </div>
                    
                     <div class="feature-content">
	                    <h3>&nbsp;</h3>
                    	<h4>How Sire makes this task easier for you</h4>
                        <p>Natural human language responses accepted</p>
                    
                    	<b>Partial List of Acceptable Responses</b>
                        <p>Millions of ways a target user can respond and be recognized</p>
                    	<h4>Common</h4>
                        <p><i>"yes"</i></p>
                        <p><i>"no"</i></p>
                        <p><i>"I will be there"</i></p>
                          
                        <h4>Informal</h4>
                        <p><i>"yes, I'll come to your party"</i></p>
                        <p><i>"Roger Dodger!"</i></p>
                      	<p><i>"Okey Dokey"</i></p>
                        
                        <h4>Computer Geeks</h4>	
                    	<p><i>"1"</i></p>
                        <p><i>"0"</i></p>
                        <p><i>"true"</i></p>
                        <p><i>"false"</i></p>
                    </div>
                    
                </div>
                
                
				   

            </div>
        </div>
    </div>



        
</div>

<cfparam name="variables._title" default="Message Confirmation">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

