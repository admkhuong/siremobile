<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>
<!---



--->
<section class="main-wrapper feature-main" id="feature-main">
<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white">
        <div class="container">
            <div class="row">


                <div class="col-md-6">
                    <div class="feature-content">
                        <p>Did you know?</p>
                        <p>Completed Survey take rate by mail is < 1%</p>
                        <p>Completed Survey take rate by email/web is 3-6%</p>
                        <p><b>Completed Survey take rate by SMS is 22-24%</b></p>
                    </div>
                    <hr>
                    <div class="feature-content">        
                        <h4>A two minute SMS customer satisfaction survey can:</h4>
                        <ul>
                            <li>Let you Gain Actionable Insights</li>
                            <li>Close the loop with your Customers</li>
                            <li>Ask intelligent questions</li>
                            <li>Gather valuable feedback</li>
                            <li>Help you Spot Trends</li>
                            <li>Free information</li>
                            <li>Improve Problem Areas</li>
                            <li>Help Customers Feel Appreciated</li>
                            <li>Retain Your Business</li>
                            
                        </ul>
                        
                        <h4>Sire Advantages</h4>
                        
                        <ul>
                            <li>Weighted question answers</li>
                            <li>Advanced Branch Logic </li>
                            <li>Template Ideas for your business</li>
                            <li>Not just fire and forget - advanced Session flows</li>
                            <li>Still simple to use</li>
                            
                        </ul>
                        
                    </div>
                                        
                    
                    <div class="feature-content">                    
                    
                    	<p>What are your customers are saying about you? (Customer Promoter Score), acts as a leading indicator of growth. If your organization’s CPS is higher than those of your competitors, you will likely outperform the market, and managing your organization to improve CPS will also improve your business performance. Whether you are aiming for faster growth or increased profits, you can use CPS as the foundation of a measurement framework that is tightly tied to the customer journey.</p>
                    
                        <h4>The CPS Calculation</h4>
                        
                       <!--- <img class="feature-img-no-bs" src="../images/learning/cps-know-1.png" /> --->    
                        <img class="feature-img-no-bs" src="/public/sire/images/learning/nps.jpg" />                     
                        
                        <p>Calculate your Customer Promoter Scores using the answer to a single question, using a 0-10 scale: How likely is it that you would recommend [brand] to a friend or colleague? This is called the Customer Promoter Score question or the recommend question.</p>
    
                        <p>Respondents are grouped as follows:</p>
                        
                        <p><b><i>Promoters</i></b>(score 9-10) are loyal enthusiasts who will keep buying and refer others, fueling growth</p>
                        
                        <p><b><i>Passives</i></b>(score 7-8) are satisfied but unenthusiastic customers who are vulnerable to competitive offerings.</p>
                        
                        <p><b><i>Detractors</i></b>(score 0-6) are unhappy customers who can damage your brand and impede growth through negative word-of-mouth.</p>
                        
                       	<p>Subtracting the percentage of Detractors from the percentage of Promoters yields the Customer Promoter Score, which can range from a low of -100 (if every customer is a Detractor) to a high of 100 (if every customer is a Promoter).</p>
                       
                      

                    </div>
                    
                    <h3 data-animation="animated bounceInLeft">
						Sample Program
					</h3>	
                    
                    <div style="border:#999 solid 1px; margin-bottom:100px;">                                         
                        
                        <div style="text-align:left; padding: 1.2em;">
    	                    <img class="" src="/public/sire/images/client_logo.png" style="width:150px;">
                            <h3 style="margin-top:.2em; padding-top:0;">Helping You Prepare for the Road Ahead</h3>                            
                        </div>
                        
                        
                        <div style="background-color:#648ba4; padding:1em; color:#FFF;">
                        	<p>Tells us how we are doing!</p>
	                        <p style="margin-bottom:0;">On your mobile phone<br/>Text the keyword: <b>Survey</b><BR/> To: <b><cfoutput>#shortcode#</cfoutput></b></p>
                        </div>
                    
                    </div>      
                    
                </div>
                
                
                <div class="col-md-6">
                	
                    <div class="feature-title" id="feature1">Customer Satisfaction Survey<br/>Social Media Version Template</div>
                                                            
                    <div class="feature-content">
                                        
                                         
						<div class="row">
                            <div id="cp1" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 1. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me"><code class="mceNonEditable">inpCustomerName</code>, thanks for your recent social media contact with <code class="mceNonEditable">inpBrandName</code>. We'll text you shortly for some feedback about your experience.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp2" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 2. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">How likely are you to recommend <code class="mceNonEditable">inpBrandName</code> services to a friend or family member - on a scale from 10 (definitely) to 1 (definitely not)?</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp4" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 3. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Great! How satisfied are you with the overall social media experience by <code class="mceNonEditable">inpRepName</code> from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp6" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 4. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Thanks. How satisfied are you with the overall social media experience by <code class="mceNonEditable">inpRepName</code> from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp8" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 5. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Sorry to hear. How satisfied are you with the overall social media experience by <code class="mceNonEditable">inpRepName</code> from 10 (very satisfied) to 1 (very dissatisfied)?</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp11" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 6. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Great! How many times did you have to contact <code class="mceNonEditable">inpBrandName</code> to resolve your request?</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            <span class="control-point-option-text">1. One</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">2. Two</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">3. Three</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">4. Four or more</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">5. Not Resolved</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp13" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 7. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Thanks. How many times did you have to contact <code class="mceNonEditable">inpBrandName</code> to resolve your request?</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            <span class="control-point-option-text">1. One</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">2. Two</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">3. Three</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">4. Four or more</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">5. Not Resolved</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp15" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 8. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Sorry to hear. How many times did you have to contact <code class="mceNonEditable">inpBrandName</code> to resolve your request?</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            <span class="control-point-option-text">1. One</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">2. Two</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">3. Three</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">4. Four or more</span>
                            </div>
                            <div class="control-point-option">
                            <span class="control-point-option-text">5. Not Resolved</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp17" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 9. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Excellent! Please provide any additional feedback to help us fully understand your experience.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp19" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 10. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Thank you. Please provide any additional feedback to help us fully understand your experience.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp21" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 11. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Sorry you were not satisfied. Please provide any additional feedback to help us fully understand your experience.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp22" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 12. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Thanks again for your time. Your comments help us improve, such as offering flexible 24/7 service. Get it with the my <code class="mceNonEditable">inpBrandName</code> app - <code class="mceNonEditable">inpBrandWebLinkShort</code></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>    
                        
                        
                        
                       
                    </div>
				</div>                        
				

            </div>
        </div>
    </div>
           
</div>


<cfparam name="variables._title" default="Satisfaction Surveys">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

