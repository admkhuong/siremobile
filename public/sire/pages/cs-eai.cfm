<section class="main-wrapper feature-main cs-eai" id="feature-main">
		
	 	<div class="feature-white no-padding-bottom no-padding-top">
            <div class="container">
            	<div class="request-live-demo-btn">
                    <img class="hidden-xs img-right" src="/public/sire/images/learning/eaitower50.png" />
                    <img src="/public/sire/images/tutorial-phone.png" class="hidden-xs img-left">
                    <a href="contact-us" class="btn btn-success-custom">Request live demo</a>
                </div>
            </div>
        
			<div class="container padding-top-1em">
				<div class="row">
					<div class="col-sm-9 col-md-9">
						<div class="feature-title">Enterprise Application Integration</div>
						<div class="feature-content">
                        	 
                            <p> Enterprise application integration is a business need to make diverse applications in an enterprise, including partner systems,  communicate to each other to achieve a business objective in a seamless reliable fashion irrespective of platform and geographical location of these applications. Sire EAI comprises differing components to support message acceptance, transformation, translation, routing, message delivery and business process management. Sire provides both self service tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for more complex messaging services.</p>
                            
                        </div>
					</div>
             	</div>   
			</div>
		</div>        
        
     	<div class="feature-white">
			<div class="container">
				<div class="row">
					                    
                    <div class="col-md-6 col-md-offset-3">
						
                        <img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/eai-flow.png">
                        
					 </div>
                                     
				</div>
			</div>
		</div>
        
        <div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="feature-title">How can Sire be integrated into your Application?</div>
						<div class="feature-content">
                        	<h4>Request - Automated Data Transfer Mechanisms</h4>
                             <ul class="new-list">                            
                                <li>HTTP/HTTPS - via REST API's</li>
                                <li>SFTP</li>                           
                                <li>HTTP/HTTPS - via form or Event variables</li>
                                <li>HTTP/HTTPS - via direct XML</li>
                                <li>HTTP/HTTPS - via SOAP</li>
                                <li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
                                <li>email</li>
                                <li>SMPP</li>
                                <li>XMPP - Chat</li>
                                <li>Direct VPN Tunnel</li>
                                <li>Dial up to secure server</li>
                                <li>DAT/CD/media via courier</li>
                                <li>FAX</li>
                            </ul>
                            <p><h4>Tip:</h4><i>Sire personnel can help you re-use and repurpose existing data transfer assets to save time and reduce IT costs.</i></p>
                         </div>
                         
                        <h4>Event Based Communications</h4>
						<div class="feature-content">
                        	 
	                        <ul class="new-list">                            
                                <li>Real Time</li>
                                <li>Via Batch Loading</li> 
                                <li>Business Initiated</li>
                                <li>Sire Automation Initiation</li>                          
                                <li>Consumer Initiated Inbound Requests</li>
                                <li>Business Initiated Outbound Requests</li>
                            </ul>
                            
                            <p>Communication events are triggered throughout the customer lifecycle</p>
                            
                        </div>

					</div>
                     <div class="col-md-6">   
                    	<img style="width:100%" class="" alt="feature-img-no-bs" src="/public/sire/images/learning/transfer-data.png">                           
					</div>   
				</div>
			</div>
		</div>
                
        
        <div class="feature-white">
			<div class="container">
				<div class="row">
                                  
                                  
                    <div class="col-md-6">   
                    	<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/rules-engine.jpg">                           
					</div>   
                           
					<div class="col-md-6">
						<div class="feature-title">Intelligent Rules Engine</div>
						<div class="feature-content">
                        	<h4>Templates - Based on homogeneous analysis of business structure and processes</h4>
                            <ul class="new-list">
                                <li>Complex Event Processing</li>
                                <li>Rules Applied based on CDFs</li>
                                <li>Rules Applied based on External Data</li>
                                <li>Rules Applied based on Customer Preference</li>
                                <li>Optimization</li>
                                <li>Classification</li>
                                <li>Pre-Processing</li>
                                <li>Dynamically applied based on messaging interactions</li>
                                <li>Post-Processing</li>
                                <li>Block conflicting messages. Don't send bill collection messages at the same time as an emergency storm warning.</li>
                            </ul>       
                      
                          
                         
                         </div>
                                  
					</div>
                  					    
				</div>                
			</div>
		</div>
                       
        <div class="feature-white no-padding-bottom no-padding-top">
        
			<div class="container padding-top-1em">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="feature-title">Best of Breed Tools and Support for Messaging</div>
						<div class="feature-content">
                        	<h4>Fulfillment - Your customers could be anywhere, be where your customers are.</h4>
	                        <ul class="new-list">                            
                                <li>Real Time</li>
                                <li>Tools to Capture and act on your customer's channel preference.</li> 
                                <li>Escalations</li>
                                <li>Seamless approach to messaging guides potential customers towards engagement.</li>
                            </ul>
                                                                                    
                        </div>
					</div>									
             	
	             	<div class="col-md-6">   
	                   	<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/platform-engine.png">                           
					</div>   
             	
             	</div>				   
			</div>
		</div>      
       
       
        <div class="feature-white no-padding-bottom no-padding-top">
        
			<div class="container padding-top-1em">
				<div class="row">
					
					<div class="col-md-6">   
                    	<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/marketing-mix.jpg">                           
					</div>   
					
					<div class="col-sm-6 col-md-6">
						<div class="feature-title">Reporting and Analytics</div>
						<div class="feature-content">
                        	<h4>Results - Your customers could be anywhere, be where your customers are.</h4>
	                        <ul class="new-list">                            
                                <li>Consumers can now respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.</li>
                                <li>Request more information to be sent on an alternate channel. Terms of use, brochures, policies and more.</li> 
                                <li>Escalations</li>
                                <li>Sire Automation Initiation</li>                          
                                <li>Consumer Initiated</li>
                            </ul>
                            
                            <p>Communication events are triggered throughout the customer lifecycle</p>
                            
                        </div>
					</div>
             	</div>   
			</div>
		</div>      
		 
</section>



<cfparam name="variables._title" default="Enterprise Application Intergration">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

