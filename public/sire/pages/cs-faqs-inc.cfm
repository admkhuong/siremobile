<cfsetting showdebugoutput="no"/>

<!---


Store as encrypted cookie on your target device browser
Can you store cookie in IOS app?


--->


<style>

.main-wrapper{
    padding-bottom: 0;
}

.hidden-content{
	display:none;
	margin-left: 9%;
}

a {
    color: #337ab7;
    text-decoration: none;
}

a:focus {
    color: #74c37f !important;
}

.expand-content-link
{
	
	display:block;
    font-style: italic;
    color: #5c5c5c;    
    text-decoration: none !important;
    cursor: pointer;
	
}
@media (min-width: 1200px){
    .expand-content-link{
        padding-left: 4rem;
    }
}
#ShowAll
{
	Margin-right: 2em;
	
}
#sms-marketing-basic{
    font-size: 2.4rem;
    font-style: italic;
    font-weight: 700;
}
@media (min-width: 320px){
    #sms-marketing-basic{
        font-size: 2rem;
        font-style: italic;
    }
}

@media (min-width: 480px){
}

@media (min-width: 768px){
    #sms-marketing-basic{
        font-size: 2.2rem;
        font-style: italic;
    }
}
@media (min-width: 1200px){
    #sms-marketing-basic{
        font-size: 2.4rem;
        font-style: italic;
    }
}

#plans-pricing, #sms-marketing, #geek-talk{
    margin-top: 5rem;
    font-style: italic;
    font-size: 2.4rem;
    font-weight: 700;
}

@media (min-width: 320px){
    #plans-pricing, #sms-marketing, #geek-talk{
        margin-top: 2rem;
        font-style: italic;
        font-size: 2rem;
    }
}

@media (min-width: 480px){
}

@media (min-width: 768px){
    #plans-pricing, #sms-marketing, #geek-talk{
        margin-top: 3rem;
        font-style: italic;
        font-size: 2.2rem;
    }
}
@media (min-width: 1200px){
    #plans-pricing, #sms-marketing, #geek-talk{
        margin-top: 5rem;
        font-style: italic;
        font-size: 2.4rem;
    }
}

</style>
        	        
            
				
                
					                   
                    <div class="faqs-content">
                
                        
                		<h4 id="sms-marketing-basic">SMS Marketing Basics</h4> 
                                                
                        <a class="expand-content-link">What is SMS?</a>
                        <div class="hidden-content">
                        
                        	<p data-animation="animated bounceInLeft">
                                Our tech geeks will till you SMS stands for Short Message Service.  Everyone else simply refers to it as a "text message".
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a Keyword?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Imagine if Joe’s Coffee Shop offers this promotion: <i>Text “FreeCoffee” to <cfoutput>#shortcode#</cfoutput> and receive a free coffee on your next visit.</i>  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a Short Code?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                               In the example above, <cfoutput>#shortcode#</cfoutput> is the Short Code.  Short Codes are special telephone numbers, significantly shorter than full telephone numbers, used to address SMS messages. Short codes are designed to be easier to read and remember than normal telephone numbers.
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What phones work with SMS?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Most phones work with SMS. It’s rare that a mobile device in this day and age doesn’t contain SMS capabilities.  Now, my grandmother’s rotary phone?  That a whole other story. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How many characters are in a text message?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                160. This includes numbers, spaces, symbols, and punctuation.
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Why 160 characters?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Telecom regulators chose 160 because it is the sum of the first eleven prime numbers.  (2 + 3 + 5 + 7 + 11 + 13 + 17 + 19 + 23 + 29 + 31).  I totally made that up.  We have no idea why the rules say 160 is the limit but that’s a cool fact, right?
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Can I send messages longer than 160 characters?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Yes but it will be split into 160 character segments.  For example, if you send a message with 1,600 characters, it will be split and delivered in 10 segments of 160 characters.  Each segment will be charged one text message credit.  Sire also allows you to receive inbound messages.  The price of an inbound message will also be charged per each 160 character block.</p>
                        </div>

                        <a class="expand-content-link">How long does it take to deliver a text message?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Text messages are usually delivered within a few seconds to a few minutes. During peak hours, there could be a short delay. If a customer decides to unplug while on vacation or lives off the grid, a carrier will attempt to deliver your message for up to 72 hours.</p>
                        </div>
                        
                        <a class="expand-content-link">What is SMS Marketing?</a>
                        <div class="hidden-content">
                        
                        	<p data-animation="animated bounceInLeft">
                                  SMS marketing is the most efficient way to connect with customers who have opted into your list.  Studies have shown acquiring a new customer costs 6-10x more than retaining an existing customer.  Sire’s user friendly platform was designed to help businesses, non-profit groups and social organizations build and stay connected with their customer base.
                            </p>
                        
                        </div>
                                    
                        
                        <h4 id="plans-pricing">Plans and Pricing</h4>
                                                                
                        <a class="expand-content-link">Do I need a contract to get started?</a>
                        <div class="hidden-content">
                        
                        	<p data-animation="animated bounceInLeft">
                               	We don’t tie you down with long term contracts.  Just sign up online for free and follow the terms and conditions.  Upgrade or downgrade whenever you like and cancel anytime. 
                            </p>
                        
                        </div>                        
                        
                        <a class="expand-content-link">How many contacts can I have?</a>
                        <div class="hidden-content">
                        
                        	<p data-animation="animated bounceInLeft">
                               	We believe you can never have too many friends.  The same goes for customers.  With Sire, you’re allowed unlimited Contacts & Groups so go ahead and build as many lists as you want.
                            </p>
                        
                        </div>
                        
                        <a class="expand-content-link">What are Pay as You Go Credits?</a>
                        <div class="hidden-content">
                        
                        	<p data-animation="animated bounceInLeft">
                               	Let’s say you’re having an annual clearance sale or you have a seasonal promotion that requires more message credits than usual.  Instead of upgrading your account you can just purchase extra “Pay as You Go” credits.  These "Pay as You Go" credits will never expire as long as you maintain your account. Your monthly plan credits are always used up first so the “Pay as You Go” continue to rollover until you use them all. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How do I downgrade my plan?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Business needs ebb and flow. We get it. You're always welcome to downgrade and upgrade (or even cancel. Gasp!) as needed. Just email us at <a href="mailto:support@siremobile.com" target="_top">hello@siremobile.com</a> or call us at 1-888-747-4411 and we'll get it done.
                            </p>
                        
                        </div>


                        <h4 id="sms-marketing">SMS Marketing Service</h4>    

                        <a class="expand-content-link">How fast can I get my campaign up & running?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                It will take less than 5 minutes to setup and launch your first campaign.  
                            </p>
                        
                        </div>

                        <a class="expand-content-link">I’m not technically savvy.  Will I be able to do this on my own?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Yes. Sire was developed for non-tech savvy users with a very simple user interface and easy to use features.  
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How do I launch my first campaign?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                You’ll be able to build a campaign in 3 easy steps:</p>
                                <ol>
                                    <li>Choose a Keyword</li>
                                    <li>Select a Template</li>
                                    <li>Customize your message</li>
                                </ol>
                            <p>That’s it!  Click send and you’re done!</p>
                        </div> 

                        <a class="expand-content-link">What is my short code?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Your short code is “<cfoutput>#shortcode#</cfoutput>”. If you want a dedicated short code, please contact us @ <a href="mailto:support@siremobile.com" target="_top"> support@siremobile.com</a>
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a subscriber list?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                A subscriber list is a list of people who have opted into your campaign by texting your keyword to “<cfoutput>#shortcode#</cfoutput>”. Sire automatically captures all of your subscribers phone numbers and saves them in your list. This is to help keep non-compliant spammers at bay. Because, let’s be real, everyone hates spam. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Do I need to create a new keyword for every campaign?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Yes. Campaigns are built around your Keywords so you’ll need a unique keyword for each campaign. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Who can use SMS Marketing?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Anyone who needs to increase engagement.  Uses you’ve probably experience yourself are the restaurant down the street announcing weekly happy hour specials, banks sending you balance alerts, retailers texting you receipts, and churches texting event reminders.  The uses for Text marketing are endless. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How do I report a problem or close an account?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                To report a problem or delete an account, please email us @ <a href="mailto:support@siremobile.com" target="_top">support@siremobile.com</a> with your request and we will reply within 24 hours. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a Marketing Landing Page (MLP)?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                A marketing landing page is a web page that you can use to display more information about your company, service, announcement or promotion. Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once a customer clicks on that link he will be sent to your own MLP displaying the coupon.  The best part is it’s included for free! 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How do I create a Marketing Landing Page?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                We’re known for making things easy on our customers.  We offer MLP templates, under Advanced Tools, that you can just plug and play.  If you want to show off a bit, you can create a landing page from scratch using our MLP builder. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a short URL?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                A text marketing message has a 160 character limit.  If your normal URL is long, it will eat into how many characters you can send in your message.  Short URL’s literally just that.  You can display a shorter URL which, when clicked, still points to your website. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How do I create short url?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                There’s a short URL builder available for free in the Advanced Tools section. Enter your target URL and a dynamic short URL will be generated for you or you can customize your own short URL. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is the QA tool (time machine)?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Your Jr. High Algebra teacher always told you to double check your work. Sire’s QA tool was developed in honor of that teacher.  It allows you to test your campaign before launching it to your customers. Just enter your keyword in the QA tool and click send. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What kind of information can I get from Sire’s reporting & analytics?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                You can get information such as your subscriber growth, active subscribers, opt out rate as well as the growth activity of your subscribers to name a few and you can download reports as well. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How many templates do you have?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                We’ve build over 60 templates ranging from promotions, surveys, table reservation updates, service completion alerts, order delivery updates and more.  Most businesses only use a few but we’ve got you covered if you need more. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Can I customize my own template?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                If the DIYer within you is itching to get out, you’re more than welcome to build your own custom template.  Just select the Build Your Own option in the Templates section. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Can I use Sire on my phone/tablet?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Yes, you can use Sire on any device - PC, tablet or mobile device without any downloads. Just go to your browser, type in siremobile.com, and start using Sire directly from your device. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What are credits?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Each credit = one text message. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How can I increase my credits?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                There are two ways that you can add credits to your account.  Upgrade your account type with no hidden cost or setup fees, or invite a friend to join Sire and you will get one month free after they sign up. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is a SMS Blast?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                A SMS blast allows you to send a message to your entire subscriber list without using a template or keyword. 
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How is my privacy protected?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Your privacy is our utmost priority. Sire’s secured system ensures that all of your information including account information, subscriber list, etc. is protected all times.  
                            </p>
                        
                        </div>

                        <a class="expand-content-link">Can Sire be integrated into my POS system?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Yes, Sire can be integrated to any POS system, please contact us @  <a href="mailto:support@siremobile.com" target="_top"> support@siremobile.com</a> for more information.
                            </p>
                        
                        </div>

                        <a class="expand-content-link">How long does my account remain active?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Accounts with no login or usage for more than 18 months will be deactivated.
                            </p>
                        
                        </div>


                        <h4 id="geek-talk">Geek Talk</h4>
                                                                
                        <a class="expand-content-link">What is an MO?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                MO stands for Mobile Originated and is the process where an end user sends a text message from their phone device to another device address. You can send an MO to another Phone Number OR when marketing to US based mobile phones you can send an MO to a short code. 
                            </p>
                        
                        </div>                        
                        
                        <a class="expand-content-link">What is an MT?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                MT stands for Mobile Terminated when an application transmits a message to a mobile phone. The message can be sent from another phone or by a software application. When messaging from your business to US based mobile phones you should always send an MT from a short code.
                            </p>
                        
                        </div>
                        
                        <a class="expand-content-link">What is natural language processing and how is it used in Sire?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                It’s a field in computer science and artificial intelligence that is related to human-computer interaction. For example: there are different ways of saying “yes” - Some may say “yeah” or “yea” or “yes”. Sire was developed with this technology and will understand all the different natural language expression received by your customers.
                            </p>
                        
                        </div>

                        <a class="expand-content-link">What is Sire?</a>
                        <div class="hidden-content">
                        
                            <p data-animation="animated bounceInLeft">
                                Sire is a Smarter Interactive Response Engine. Whether you're sending transactional or marketing SMS, the Sire solution is built upon solid SMS infrastructure, providing you with the industry’s best reliability, scalability, and SMS deliverability. Easily integrate with Sire’s cloud-based SMS infrastructure, eliminating the need to build, scale and maintain these systems in-house, allowing you to focus on growing your business.
                            </p>
                        
                        </div>
                            
                    </div>
                    
               
            

