<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white static-page-container">
        <div class="container">
	        
	        <div class="row">
            	<div class="col-md-8 col-md-offset-2">  
					<div class="AlignCenter static-page-title" id="feature1">Frequently Asked Questions</div>
                </div>    	
        	</div>
			<cfinclude template="../pages/cs-faqs-inc.cfm">


			<div class="row" style="margin-top: 2em;">
		            	
            	<div class="col-md-8 col-md-offset-2 show-hide-faqs">  
	            	<div class="learning">								
						<div class="feature-content">
	                    	<a id="ShowAll" href="javascript:void(0)">Show All</a> <a id="HideAll" href="javascript:void(0)">Hide All</a>
	                    </div>
	                </div>  
            	</div>  	
        	</div>    	
    	</div>                   
	</div>


   
<cfparam name="variables._title" default="FAQs">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">


<script type="text/javascript">

	$(function() {
	
	
		$(".expand-content-link").click(function() {
			$(this).next(".hidden-content").toggle();
			return false;   
		});
		
		
		
		$("#ShowAll").click(function() {
			$(".hidden-content").show();
			return false;   
		});
		
		$("#HideAll").click(function() {
			$(".hidden-content").hide();
			return false;   
		});
		
		
		
		
		
	});




</script>

<style type="text/css">
	
	.feature-white{
		padding-bottom: 0rem !important;
	}
</style>


