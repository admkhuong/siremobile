

<!---

Replace this page with the preview functionality 

https://litmus.com/blog/best-practices-for-optimizing-order-confirmation-emails

Best Practices for Optimizing Order Confirmation Emails


     <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="feature-title" id="feature1">Event Based Communications</div>
                    <div class="feature-content">
                        
                        <p>Communication events are triggered throughout the customer lifecycle</p>
                        
                        
                        <ul>
                        	<li></li>
                            
                        </ul>
                        
                            
                    </div>
                </div>

              	<div class="col-md-6">                    
                                    
                       <img style="float:left;" src="../images/learning/interactions31.png" />                                                                  
                    
                </div>

            </div>
        </div>
    </div>
	
--->


<section class="main-wrapper feature-main" id="feature-main">

	<div class="feature-white">
        <div class="container">
            <div class="row">

				<div class="col-md-8">
                    <div class="feature-title" id="feature1">Transactional SMS and the Customer Lifecycle</div>
                    <div class="feature-content">
                        
                        <p>Marketers and designers typically focus the majority of their efforts on optimizing their newsletters and promotional messages. The primary goals of these communications are usually new sales, leads, and conversions. However, there is a missed opportunity in engaging and retaining users with order confirmation SMS messages.</p>
                        
                        
                        <p>It typically costs between six and seven times more to acquire a new customer than to retain existing customers. In addition, repeat customers tend to purchase as much as 67% more than new customers.</p>
                        
                        
                        <p>Many businesses are leaving money on the table by neglecting current customers in favor of creating new businesses. Consider shifting some of your focus to moving customers from the first purchase to repeat purchases and brand loyalty.</p>    
                        
                        <p>While it’s still crucial to send and optimize promotional messages aimed at gaining new customers, take a look at what happens after a purchase is made. What do your after order messages look like? Confirmation? Tracking Numbers? What type of information do they include?</p>
                    </div>
                </div>

              	<div class="col-md-4">                    
                	<img class="feature-img-no-bs" src="/public/sire/images/learning/interactions31.png" />                                                                  
                </div>

            </div>
        </div>
    </div>

	
    
    <div class="container">
        <div class="row">
                                              
            <div class="col-md-12">
                <div class="feature-title" id="feature1">Aquire</div>
                <div class="feature-content">
                    
                    <p>                         
                        So, one of your customers has bitten the bullet and made a purchase (maybe even motivated by your fabulous SMS marketing). While it may be celebration time, your work isn't quite done yet. When properly executed, order and shipping confirmation messages can seal the deal with your customers, making them even more pleased with their recent purchases and perhaps even securing their lifelong loyalty. Take advantage of this great post-purchase opportunity to strengthen their relationship with your brand and encourage repeat shopping by following the tried-and-true techniques below.                           
                    </p>
                  
                </div>
            </div>
            
        </div>
    </div>
  
    <div class="container">
        <div class="row">
                                              
            <div class="col-md-12">
                <div class="feature-title" id="feature1">Delight</div>
                <div class="feature-content">
                      
                    <p>When it comes to transactional messaging, clarity is king. Make sure that your message stands out as specifically concerning their order. Consider keeping the order number handy and up-front for subscribers who scan their messages while on the phone to your customer service reps. Emphasize the order number and customer service info.</p>    
                    
                    <p>Keep your copy short, to-the-point and grateful. When a shopper has just made a purchase, a note of thanks is in order, but keep it brief; subscribers mostly just want to see their info.</p>
                </div>
            </div>
            
        </div>
    </div>

    <div class="container">
        <div class="row">
                                              
            <div class="col-md-12">
                <div class="feature-title" id="feature1">Upsell</div>
                <div class="feature-content">
                    
                    <p>                         
                         Encourage subscribers to sign up for your promotional messages. While there's a good chance that your online shoppers have already joined your marketing list, your order and shipping confirmation messages are great places to make sure. Having just placed an order, recipients likely want the scoop on your future offerings. Sample - Text the keyword OFFERS to 39492 for future offers and discounts.
                    </p>
                  
                </div>
            </div>
            
        </div>
    </div>

    <div class="container">
        <div class="row">
                                              
            <div class="col-md-12">
                <div class="feature-title" id="feature1">Retain</div>
                <div class="feature-content">
                    
                    <p>                         
                        Consider other ways to make the transaction special. Some brands court shoppers by sending them extra thank-you messages in addition to the standard order or shipping notification. Offer to help purchasers know how to make the most of their new device. Once you've hit the important points with your order and shipping notifications, it might be worth considering ways to make shoppers feel extra-special. While your promotional campaigns should always look beautiful, don't pass up the opportunity to dazzle subscribers with your shipping and order confirmation messages, too. After all, we hope that customers will be receiving many of these... over and over again.
                    </p>
                  
                </div>
            </div>
            
        </div>
    </div>

    <div class="container">
        <div class="row">
                                              
            <div class="col-md-12">
                <div class="feature-title" id="feature1">Get that Recommendation</div>
                <div class="feature-content">
                    
                    <p>Good customer relationship management can turn happy customers into evangelists for your product or service, spreading the word and bringing in new business.</p>
                    <p>                         
                        Customer satisfaction is at the core of human experience, reflecting our liking of a company’s business activities. High levels of customer satisfaction (with pleasurable experiences) are strong predictors of customer retention, customer loyalty, and product repurchase.
                    </p>
                    
                    <p><i>Effective businesses</i> focus on creating and reinforcing pleasurable experiences so that they might retain existing customers and add new customers. <i>Properly constructed</i> customer satisfaction surveys provide the insights that are the foundation to creating and reinforcing pleasurable customer experiences.</p>
                    
                    <p>Sire has several templates to help you get started with customer satisfaction surveys.</p>
                  
                </div>
            </div>
            
        </div>
    </div>
            
    <div class="feature-white">
        <div class="container">
            <div class="row">
            
            </div>
           
        </div>
    </div>
                            
        
</div>

<cfparam name="variables._title" default="Customer Lifecycle">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

