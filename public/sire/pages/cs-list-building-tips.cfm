<!---


--->

<cfoutput>

<cfset TipCount = 0 />


	<section class="main-wrapper feature-main" id="feature-main">
		
	 	<div class="feature-white no-padding-bottom no-padding-top">
        
        	<img class="hidden-xs" style="float:right;" src="/public/sire/images/learning/woman-sharing-news-50.jpg" />
            <img class="visible-xs" style="float:right;" src="/public/sire/images/learning/woman-sharing-news-25.jpg" />   
        
			<div class="container padding-top-1em">
				<div class="row">
					<div class="col-sm-8 col-md-8">
						<div class="feature-title">Amazing Marketing Tools, Tips & Tricks for building your SMS marketing lists</div>
						<div class="feature-content">
                        	 
                            <p>Here are some of the things that we like to use as marketers -- because they help us do our jobs better, quicker, stronger, faster.  We hope you can take away a few gems to implement in your efforts -- and if you have ideas that work well for you share some of them with your fellow Sire users via our social links </p>
                            
                        </div>
					</div>
             	</div>   
			</div>
		</div>
	
    
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Tell Everyone You Know </div>
						<div class="feature-content">
                        	
                            <p>	Leverage your existing contacts to build your SMS list. Tell your friends about your opt-in gift and share a link to your squeeze page with your social media audiences. Email your existing contacts and let them know about the new exclusive SMS perks you are offering. 
                     	    </p>
                            
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Optimize Your Website</div>
						<div class="feature-content">
                        
                        <b>Become a Lead Capturing Machine</b>
                        	<p>Publish Your Subscribe Option (<i>Call to Action</i>) on Every Page of Your Website. Make sure your call to acion is placed in a prominent spot on your site - above the fold is best.
                                Sire will help you to capture, store and manage SMS subscribers.  Your users can either send a keyword to join your list(s) or they can sign up via a form and that will trigger a confirmation via an API call. The more people that see your call to action, the more will use it.
                     	    </p>
                            
                            <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.</p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
    	
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b>  Give your List a Cool Name</div>
						<div class="feature-content">
                        	
                            <b>Example: “Awesome News and Updates”</b>
                            
                            <p>                                                    
								Try as hard as you can to make every message you send as awesome as possible.
                            </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Offer an Awesome Free Gift to Subscribers</div>
						<div class="feature-content">
                        	
                            <p>                         
                               Offer an Awesome Free Gift to Subscribers. Coupons, One Time discounts, Buy one Get One. It is worth your time to create two or more lead magnets and find out which work best for you.  Different offers attract different people.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Create a Compelling Sign up Option</div>
						<div class="feature-content">
                        	
                            <p>                         
                               Be sure to include a compelling graphic of the free gift or other perks you are offering.
                            </p>
                                
						</div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Give People Multiple Reasons to Sign Up</div>
						<div class="feature-content">
                        	<p>
                            	It's hard to fit all of the reasons why someone should sign up to your SMS list into a sidebar form. One way that you can better convince people to sign up is by creating a 10 Reasons why "You Should Join my SMS List" page and linking to it under this form. Naturally the page featuring these 10 reasons to join your list should have a sign-up call to action as well.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Use Squeeze Landing Pages</div>
						<div class="feature-content">
                        	<p>                         
                                An effective landing page (squeeze page) has a much better rate of success for capturing new leads than any other form placement. The idea behind an effective squeeze page is to maximize conversions with a single call to action.
                     	    </p>     
                            
                            <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts to drive traffice to your squeeze page.</p>                   
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Leverage the Power of Social Media</div>
						<div class="feature-content">
                            <p>
                            	Using social media on an ongoing basis has helped many customers add thousands of people to their SMS lists, and build some amazing relationships in the process. Facebook and Twitter are obvious examples but don't stop there.  Try Pinterest and others too.                          
                            </p>
                            
                            <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.</p>
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Article Marketing</div>
						<div class="feature-content">
                            <p>                                                         
								Article marketing is a way to spread your content across the web and get plenty of links back to your website in the process. The links and traffic you get from article marketing can help build your list and boost your Search Engine Optimization (SEO).
                     	    </p>
                        
                        </div>
					</div>                    
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Generate Leads with Pay Per Click</div>
						<div class="feature-content">
                            <p>                                                         
								Although pay per click is not free it becomes free as soon as you know your numbers. Let’s say that every lead that signs up earns you an average of $1 over the next year.  This means that if you can use PPC to get a lead for less than $1 you will be getting that lead for free, and earning profit on the margin between your cost and income.
                     	    </p>
                            
                            <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.</p>
                        
                        </div>
					</div>         
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> YouTube Video Optimization</div>
						<div class="feature-content">                        	
                            <b>Use annotations with a call to action</b>
                            <p>Annotations are those little text boxes that you have probably seen on top of YouTube videos before. Annotations take only a few moments to publish but if you include them they can send a steady amount of traffic to your site over time, especially if your video starts to gain serious traction!</p>
							<b>youtube-annotation-link</b>
                            <p>Studies show that reading while watching increases retention by as much as 38%.  If you use a verbal call to action for engagement combined with a written call to action via an annotation this should amp up your response.</p>
                            <b>Include a hyperlink in your video description.</b>
                            <p>Include a link to one of your your squeeze pages in the first line of your video description.
                            Another tactic to get more people on your list with your videos is to watermark your videos with a URL of your squeeze page.
                            You can insert an an intro and outro slide with a call to action for people to visit your squeeze page and join your list.
                  	    	</p>         
                            
                            <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.</p>               
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Alternate Video Distribution</div>
						<div class="feature-content">
                        	                            
                            <p>                                                     
                                Although YouTube is by far the biggest video website on the net, and even nets more traffic than Google.com some days, you can still pick up a significant amount of traffic from other video sites and this can lead to many new opt-ins over time.
                                One way to distribute your videos is using the video syndication service <a target="_blank" href="http://www.oneload.com/">oneload.com</a>
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Offline Marketing (Business Cards)</div>
						<div class="feature-content">
                        	
                            <p>
								Promotional business cards are a great way to build your business and your brand. Be different: don’t just create a boring ordinary card like everyone else. Use curiosity and a call to action. 
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Display your Keywords and Sign Up Link</div>
						<div class="feature-content">
                        	
                            <ul>
                            	<li>Stickers</li>
                                <li>Table Tents</li>
                                <li>Banners</li>
                                <li>Windows</li>
                                <li>Checkout</li>
                                <li>Menus and more...</li>
                            </ul>
                           
                            <p>  
								Display a kewwords and links to your list everywhere you possibly can.
								Although simple, this strategy is incredibly powerful if you put it into action you can start to spread your name far and wide and influence the masses.
                     	    </p>
                        
                        
                        	<br>
                            
                            <div style="border:##999 solid 1px; margin-bottom:0px;">                                         
                        
                                <div style="text-align:left; padding: 1.2em;">
                                    <img class="" src="/public/sire/images/client_logo.png" style="width:150px;">
                                    <h3 style="margin-top:.2em; padding-top:0;">Helping You Prepare for the Road Ahead</h3>                            
                                </div>
                                
                                
                                <div style="background-color:##648ba4; padding:1em; color:##FFF">
                                    <p>Take our new investor survey to get started.</p>
                                    <p style="margin-bottom:0;">On your mobile phone<br/>Text the keyword: <b>Profile</b><BR/> To: <b>39492</b></p>
                                </div>
                            
                            </div>  
                    
                        </div>
					</div>
                </div>
			</div>
		</div>
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Guest Blogging</div>
						<div class="feature-content">
                        	                            
                            <p>  
                            Guest blogging is a great way to tap into larger audiences than your own and build your personal brand and your email list simultaneously. When you publish a guest post for another blog they give you a signature line at the end where you can link back to your website. The optimal way to use this link is not to link to your main page, but to an optimized squeeze page within your website, so that you capture the maximum amount of subscribers.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Install a Sign up Form into Your Facebook Page</div>
						<div class="feature-content">                        	
                          
                            <p>
                            An awesome free Facebook iframe app host called a <a target="_blank" href="https://apps.facebook.com/iframehost-star/">Woobox</a> to add sign up forms to your Facebook page. This form can brings you opt-ins over time, depending on how active you are on your Facebook page.
                     	    </p>
                        
	                        <p>Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.</p>
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b>  Leave Insightful Comments</div>
						<div class="feature-content">
                        	
                            <p> Go above and beyond with the comments you leave on blogs. Include your blog name within your name field, in addition to your name.
								For example: <b>Your Name from SmartBlogX.com</b> instead of just <b>Your Name</b>.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b>  Encourage Word of Mouth Marketing</div>
						<div class="feature-content">
                        
                        	<p> 
								In your welcome SMS that automatically deploys when someone signs up to your list encourage your new subscribers to tell their friends.
								Once new subscribers reply confirm to join your list you can send them thank you message that encourages them to spread the word about your offerings.
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Build Multiple Lists</div>
						<div class="feature-content">
                        	
                            <p>             
								For examples you can have a general list, a Twitter focused list, and a Facebook list. Have multiple opt-in offers. Tell each list about the other lists and gain people who are subscribed to more than one of your lists.
								This way you can focus the messages you send and create deeper relationships with the leads on your lists.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Keep Sign up Form Fields to a Minimum</div>
						<div class="feature-content">
                        	
                            <p>               
                                The fewer fields you have in your sign up form the more people will sign up.
                                Some marketers try to get more than just a name and an SMS phone number from their subscribers so they can personalize messages, but you can start by just asking for just an mobile phone number because that way you can grow your list faster.
                                The more fields you try to capture up front, the more your conversion rate will seriously suffer. Future messages can be setup to ask for more personalization 
							</p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Use the 80/20 Rule for Your Content vs Sales</div>
						<div class="feature-content">
                        	
                            <p>
								It’s okay to include the occasional pitch or product recommendation into your messages but stay focused on providing awesome content first.
								A good rule to use is to be at most 20% pitch and 80% content, and the more quality content you give your subscribers, the better.
                            </p>
                            
                            <p>
                            	If your messages are a constant stream of sales pitches then you will have a hard time building your list because so many people will be leaving it.
								Conversely, if your messages are action packed full of awesome your subscribers will tell their friends and your list will organically spread by word of mouth.
                            </p>    
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="feature-title"><b>#TipCount#.</b> Give Exclusive Content to Subscribers</div>
						<div class="feature-content">
                        	
                            <p>      
                                Make your subscribers feel special by giving them special subscriber only content.
                                Provide useful alerts and reminders or provide enhanced access and privileges and are just two ways to create the rapport with your subscribers that will get them to tell their friends about you, and keep them from unsubscribing.
                            </p>
                                
						</div>
					</div>   
                    
                    <div class="col-md-4">
						
                        <img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/vip-50.jpg">
                        
					 </div>
                                     
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Social Sharing Plugins</div>
						<div class="feature-content">
                        	<p> 
								Install Facebook like buttons, Twitter tweet buttons, and any other buttons that are relevant to your opt-in offer directly on your squeeze page.
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Create Epic Message Content</div>
						<div class="feature-content">
                        	
                            <p>                         
                                Epic content is the only kind of message content you should focus on creating. Use Sire templates for ideas on where to start.
                            </p>
                            
                            <p>
                            	Deliver awesome every time you hit the send button. Make Your Content So Awesome People Share it with others
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Run a Contest</div>
						<div class="feature-content">
                        	<p> 
								Give away a product or service for free and make it for SMS list subscribers only so new people feel compelled to subscribe.
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Integrate Testimonials into Your Calls to Action</div>
						<div class="feature-content">
                        	
                            <ul>
                            	<li>Keyword Display Ads</li>
                                <li>Sign Up Forms</li>
                                
                            </ul>
                        
                            <p> 
								This tactic will re-assure the people who are hesitant.  If someone else endorses you then your messsages must be worthwhile.
								The more influential the person who gives the testimonial is, the better.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Craigslist</div>
						<div class="feature-content">
                        	<p> 
								Craigslist is one of the top 50 websites in the world, and consequently has millions of advocates who spend plenty of time on the site.
								Place ads on Craigslist and there is a good chance you will get some response from it.  Track the clicks you get with Google Analytics or bit.ly short links to measure the effectiveness of your efforts.
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b>  The Thesaurus</div>
						<div class="feature-content">
                        	
                            <p>                         
                               Ridiculous? Maybe. But holy cow do content writers use this a lot. If you aren't leveraging it, get on it! Use this so your message vocabulary doesn't get stale (let's not call everything , "great," am I right?) and consumers want to keep subscribed toyour lists. Bookmark a Thesaurus on your toolbar for easier brainstorming, and more variety in your vocabulary! 
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Email Signature</div>
						<div class="feature-content">
                        	<p>                         
                               Include a link to your squeeze page or a Keyword call to action in your email signature.
                     	    </p>
                        
                        
                        	<div>
                            
                            	Hello, <br/>
                                
                            	<ul>
                                	<li></li>
                                    <li></li>
                                    <li></li>                                
                                </ul>
								<img src="http://siremobile.com/public/sire/images/sirelogos/sire-logo100x32.png" /><br/>
                                Joe User <br>
                                welcome@siremobile.com <br/>
                                Text the Keyword <b>Welcome</b> to 39492
                            
                            </div>
                        </div>
					</div>
                </div>
			</div>
		</div>
        
        
    	<cfset TipCount = TipCount + 1 />
		<div class="feature-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Post Your SMS Offers on Your Blog</div>
						<div class="feature-content">
                        	
                            <p> 
							If you post your SMS Offers on your blog this enables you to pick up extra traffic from people who find your blog in Google.
							Make sure to add as many relevant “tags” to your posts to help your content rank.
                            </p>
                                
						</div>
					</div>                    
				</div>
			</div>
		</div>
        
        <cfset TipCount = TipCount + 1 />
		<div class="feature-white wg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="feature-title"><b>#TipCount#.</b> Leverage Linkedin</div>
						<div class="feature-content">
                        	<p>  
								Include a link to your list from your profile.
								Mention that you are a publisher of exclusive SMS in your description and reference your list as many times as possible in your profile.
                     	    </p>
                        
                        </div>
					</div>
                </div>
			</div>
		</div>
        

	</section>


</cfoutput>



<cfparam name="variables._title" default="Case Studies">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

