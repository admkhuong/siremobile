<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<section class="main-wrapper feature-main" id="feature-main">
	<div class="feature-white no-padding-bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="feature-title" id="feature1">Multi-Factor Authentication</div>
                    <div class="feature-content">
                        <p>One Time verification code uses information sent to a phone via SMS, Push or Voice for use as part of the login process.</p>
                    </div>
                </div>

              	<div class="col-md-6 image">                    
                   <img style="float:left;" src="/public/sire/images/learning/mfademoheader.png" />
                   <h2><cfoutput>#shortcode#</cfoutput></h2>
                </div>
            </div>
        </div>
    </div>
	<div class="feature-gray">
		<div class="container">
			<div class="row">
                 <div class="col-md-6">                    
                   <img style="float:left;" src="/public/sire/images/learning/mfaloginfocused.png" />                                                                  
				</div>
				<div class="col-md-6">
					<div class="feature-title" id="feature1">Two Factor Authentication</div>
					<div class="feature-content">
                        <p>                         
                            <b>Something you know and something you have</b>                            
                        </p>
                        <p>Older web sites and applications only have one layer - the password - to protect their users. By adding 2-Step Verification, if a bad guy hacks through your password layer, they'll still need your phone to get into your account.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Case Studies">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

<style type="text/css">
    .image { 
        position: relative;
    }
    <cfif len(shortcode) EQ 5>
        h2 { 
            position: absolute; 
            top: 76px;
            left: 140px;
            font-size: 20px;
            font-weight: bold;
        }
    <cfelse>
        h2 { 
            position: absolute; 
            top: 80px;
            left: 134px;
            font-size: 15px;
            font-weight: bold;
        }
    </cfif>
</style>