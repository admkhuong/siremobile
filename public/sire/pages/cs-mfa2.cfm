

<!---


Store as encrypted cookie on your target device browser
Can you store cookie in IOS app?


--->


<style>
	
	.bsImage
	{		
		width:100%; 
		height: auto;
	}
	
	.main-wrapper {
    	padding-bottom: 0;
	}
	

	.TutorialText {
	  text-align: left; 
	}
	
	.TutorialText ul
	{
		list-style: none;
		
	}

.post-outer
{
	
	margin-top:2em;
	
}


.NextStep
{
	width: 100%; height: .5em; border-bottom: 1px solid #ccc; text-align: center;
}

.NextStep span
{
	
	font-size: 1em; background-color: #FFFFFF; padding: 0 10px; color: #ccc;
	
}

.entry-content:before, .entry-content:after {
    content: '';
    display: table;
}
*, *:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

	.SireArticle {
	    background-color: white;
	    border-top: 8px solid #f45145;
	    margin: 0 0 81px;
	    padding: 27px 27px 22px;
	    position: relative;
        border-radius: 1em;
	}
	
	article, aside, details, figcaption, figure, footer, header, main, nav, section {
	    display: block;
	}


	.entry-content, .entry-summary {
	    margin-bottom: 17px;
	    margin-top: 17px;
	}



.nbtentry-meta, .nbtentry-meta a, .pingback .edit-link {
    color: #9ba2af;
    font-size: 0.83333em;
    line-height: 1.8em;
}

h6, button, input[type="button"], input[type="reset"], input[type="submit"], .site-footer, .main-navigation, .secondary-navigation, a.nbtmore-link, .nbtentry-meta, .nbtentry-meta a, .pingback .edit-link, .trackback .edit-link, #cancel-comment-reply-link, .reply, .comment-metadata, .comment-metadata a, .comment-form-author, .comment-form-email, .comment-form-url, .comment-form-comment, #infinite-footer .blog-info a, #infinite-footer .blog-credits, #infinite-handle span {
    letter-spacing: 0px;
    text-transform: uppercase;
}


a {
    color: #f45145;
    text-decoration: none;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
}

.SireArticle:before {
    content: "\f0c1";
    z-index: 2;
    font-family: FontAwesome;
}
.entry-format, .SireArticle:before {
    font-size: 2.61111em;
    line-height: 1.14894em;
    background: #f45145;
    border-radius: 50%;
    color: white;
    display: block;
    line-height: 80px;
    margin-left: -40px;
    position: absolute;
    top: -44px;
    left: 50%;
    text-align: center;
    width: 80px;
    height: 80px;
    -webkit-font-smoothing: antialiased;
    text-decoration: none;
    vertical-align: text-bottom;
    
}


.entry-header h2
{
	
	 color: #f45145;
    text-decoration: none;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
	
}
	
	


</style>	

<section class="main-wrapper feature-main" id="feature-main">

	<div class="feature-gray no-padding-bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                        				
                        				
                    <div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									Multi-Factor Authentication
								</h2>
								<h4>One Time verification code uses information sent to a phone via SMS for use as part of the login process.</h4>
							</header>
							
							<div class="post-body entry-content">
														
								
																
								<img class="" style="display: inline; width:35%; height: auto; border-radius: 1em;" src="/public/sire/images/learning/entercode.jpg" /> 
									
								<img class="" style="display: inline; width:10%; height: auto; margin-left: 5%; margin-right: 5%;" src="/public/sire/images/learning/plusimg90x90.png" /> 
									
								<img style="display: inline; width:35%; height: auto;" src="/public/sire/images/learning/mfademoheader.png" /> 
															
									
								<div style="clear: both;"></div>
	
							</div>
							
							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>

					<!--- Make the links jump to a nicer place by putting here --->
					<div id="SYKA"> &nbsp; </div>

					<div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									Two-Factor Authentication
								</h2>
								
								<h3>Something you know AND something you have</h3>
							</header>
							
							<div class="post-body entry-content">
											
								<div class="col-md-12 TutorialText">   
								
									<h4>Older web sites and applications only have one layer - the password - to protect their users. By adding 2-Step Verification, if a bad guy hacks through your password layer, they'll still need your phone to get into your account.</h4>
									
								</div>	
								
								<div class="col-md-4" style="text-align: left;">   
															
									<img class="bsImage" style="" src="/public/sire/images/learning/mfaloginfocused.png" /> 
								
								</div>
									
								<div style="clear: both;"></div>
	
							</div>
							
							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>

    				<!--- Make the links jump to a nicer place by putting here --->
					<div id="AuthSample1"> &nbsp; </div>

				    <div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									MFA Scenario - Authorize Device Sample
								</h2>
							</header>
							
							<div class="NextStep"><span>First Step</span></div>
							
							<div class="post-body entry-content">
																
								<div class="col-md-12 TutorialText">   
								
									<h3>User signs in to your application.</h3>
									
								</div>	
								
								<div class="col-md-8" style="text-align: right;">   
									
									<!---  width:240px; height: auto; --->						
									<img class="bsImage" style="margin-right: 0em; border: solid 1px #898787; width:100%; height: auto;" src="/public/sire/images/learning/signinsample.png" /> 
								
								</div>
																
								<div style="clear: both;"></div>
	
							</div>
							
							<div class="NextStep"><span>Next Step</span></div>
																					
							<div class="post-body entry-content">
																
								
								<div class="col-md-12 TutorialText">   
								
									<h3>Your Application verifies if it is already authorized.</h3>
									
									<ul>
										<li>Check Cookie (Simplest Method)</li>
										<li>Check Session</li>
										<li>Check Database</li>
									</ul>										
									
								</div>	
								
								<div class="col-md-4" style="text-align: left;">   
															
									<img class="bsImage" style="" src="/public/sire/images/learning/browsercookie.jpg" /> 
								
								</div>
								
								<div style="clear: both;"></div>
	
							</div>
							
							<div class="NextStep"><span>Next Step</span></div>
														
							<div class="post-body entry-content">
								
								<div class="col-md-12 TutorialText">   
								
									<h3>Request the Sire platform to Generate and Send Code</h3>
									<p>Returned Code is encrypted and then stored in application memory</p>
									<p>(be sure to add salts and hash)</p>
									<ul>
										<li>Cookie Storage</li>
										<li>Memory Storage</li>
										<li>Session Storage</li>
										<li>Database Storage</li>
									</ul>										
									
								</div>	
								
								<div class="col-md-8" style="text-align: right;">   
															
									<img class="bsImage" style="" src="/public/sire/images/learning/https-api.png" /> 
								
								</div>
								
								<div style="clear: both;"></div>
	
							</div>
							
							<div class="NextStep"><span>Next Step</span></div>
							
							<div class="post-body entry-content">
																
								
								<div class="col-md-12 TutorialText">   
								
									<h3>Prompt for User for Code</h3>
									
									<ul>
										<li>Check Cookie (Simplest Method)</li>
										<li>Check Session</li>
										<li>Check Database</li>
									</ul>										
									
								</div>	
								
								<div class="col-md-6" style="text-align: left;">   
															
									<img class="bsImage" style="" src="/public/sire/images/learning/entercode.jpg" /> 
								
								</div>
								
								
								
								<div style="clear: both;"></div>
	
							</div>

							<div class="NextStep"><span>Last Step</span></div>

							<div class="post-body entry-content">
																
								
								<div class="col-md-12 TutorialText">   
								
									<h3>Verify Code</h3>
									
									<ul>
										<li>Check Cookie (Simplest Method)</li>
										<li>Check Session</li>
										<li>Check Database</li>
									</ul>										
									
								</div>	
								
								<div class="col-md-6" style="text-align: left;">   
															
									<img class="bsImage" style="" src="/public/sire/images/learning/verifiedcode.jpg" /> 
								
								</div>
								
								
								
								<div style="clear: both;"></div>
	
							</div>
							

							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>            
                   
                   
					<!--- Make the links jump to a nicer place by putting here --->
					<div id="Signup"> &nbsp; </div>
                    
                    <div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									Your app must collect the alternate channel information.
								</h2>
								<h4>In this case the Mobile Phone number for the SMS</h4>
							</header>
							
							<div class="post-body entry-content">
														
								<p>Always honor opt outs</p>
								
								<p>Limit attempts</p>
								
								<!--- Shared short code dangers --->
								
								<p>Offer/Respect opt out - in one real world customer example a large banking client uses MFA to authenticate logins on new devices. When they first started doing this, there was no consideration for expired device addresses. When a customer turned in their device, and another person picked up that same device address, the new person was getting unsolicited messages, but could not turn them off.</p>								
																	
								<div style="clear: both;"></div>
	
							</div>
							
							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>

					
					
                                        
                    <!--- http://writingcenter.fas.harvard.edu/pages/ending-essay-conclusions  --->
                    <!--- Make the links jump to a nicer place by putting here --->
					<div id="Conclusion"> &nbsp; </div>
                    
                    <div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									You can now add MFA to your application.
								</h2>
								<h4>One Time verification code uses information sent to a phone via SMS for use as part of the login process.</h4>
							</header>
							
							<div class="post-body entry-content">
								
												
								<p>Multifactor authentication (MFA) is a security system that requires more than one method of authentication from independent categories of credentials to verify the user's identity for a login or other transaction.</p>																
								
								<p>When you enable 2-Step Verification, you add an extra layer of security to your account. You sign in with something you know (your password) and something you have (a code sent to your phone).</p>															
									
								<div style="clear: both;"></div>
	
							</div>
							
							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>

					<!--- http://writingcenter.fas.harvard.edu/pages/ending-essay-conclusions  --->
                    <!--- Make the links jump to a nicer place by putting here --->
					<div id="DogFood"> &nbsp; </div>
                    
                    <div class="post-outer">
						
						<article class="post SireArticle">
							
							<header class="entry-header">
								<h2 class="post-title entry-title">
									We eat our own brand of dog food.
								</h2>
								
							</header>
							
							<div class="post-body entry-content">
								
												
								<p>In an effort to reduce spam and abuse of our systems, Sire requires all user accounts to authenticate each device by using MFA over a second channel.</p>															
								
								<img style="float:right; display: inline; width:35%; height: auto;" src="/public/sire/images/learning/dogfooding.jpg" /> 
									
								<div style="clear: both;"></div>
	
							</div>
							
							<footer class="nbtentry-meta">
							
							
							</footer>
							
						</article>
						
						<div style="clear: both;"></div>
					</div>
                    
                </div>

              	<div class="col-md-3">                    
                                
                    <div class="widget HTML" data-version="1" id="HTML1">
						<h2 class="title">MFA Guide</h2>
						<div class="widget-content">
							
							<ul>
								<li><a href="">What is MFA?</a></li>
								<li><a href="#SYKA">Something you know AND something you have</a></li>
								<li><a href="#AuthSample1">Authorize Device Sample</a></li>
								<li><a href="#DogFood">Sire uses its own product</a></li>
								<li><a href="#Conclusion">Conclusion</a></li>
							</ul>
						</div>
						<div class="clear"></div>
						
						<div class="clear"></div>
					</div>                                                     
                    
                </div>




            </div>
        </div>
    </div>

	
    
</section>    	        
        
        

<cfparam name="variables._title" default="MFA">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

