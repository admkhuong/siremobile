
<style>
	
	.article-content:before {
	  content: ' ';
	  display: table;
	  width: 10em;    
	}
	
	<!--- https://zellwk.com/blog/responsive-typography/ --->
		<!--- https://css-tricks.com/viewport-sized-typography/ --->
		.article-content h1, #main-stage-content h1 {
			font-size: 5.7vw;
		}
		
		.article-content h2, #main-stage-content h2 {
			font-size: 4.0vw;
		}
		
		.article-content h3, #main-stage-content h3 {
			font-size: 2.8vw;
		}
		
		.article-content h4, #main-stage-content h4 {
			font-size: 2.0vw;
		}
		
		.article-content h5, #main-stage-content h5 {
			font-size: 1.6vw;
		}
		
		.article-content p, #main-stage-content p {
			font-size: 2.5vw;
		}
	
		.article-content pre {
			font-size: 0.8vw;
		}
		
		@media all and (min-width: 800px) {
		  .article-content h1, #main-stage-content h1 {
				font-size: 5.7vw;
			}
			
			.article-content h2, #main-stage-content h2 {
				font-size: 4.0vw;
			}
			
			.article-content h3, #main-stage-content h3 {
				font-size: 2.8vw;
			}
			
			.article-content h4, #main-stage-content h4 {
				font-size: 2.0vw;
			}
			
			.article-content h5, #main-stage-content h5 {
				font-size: 1.6vw;
			}
			
			.article-content p, #main-stage-content p {
				font-size: 2.5vw;
			}
			
			.article-content pre {
				font-size: 0.8vw;
			}

		}
		
		@media all and (min-width: 1200px) {
		  .article-content h1, #main-stage-content h1 {
				font-size: 2.65vw;
			}
			
			.article-content h2, #main-stage-content h2 {
				font-size: 2.0vw;
			}
			
			.article-content h3, #main-stage-content h3 {
				font-size: 1.4vw;
			}
			
			.article-content h4, #main-stage-content h4 {
				font-size: 1.0vw;
			}
			
			.article-content h5, #main-stage-content h5 {
				font-size: 0.8vw;
			}
			
			.article-content p, #main-stage-content p {
				font-size: 1.2vw;
			}
			
			.article-content pre {
				font-size: 0.8vw;
			}
		}


</style>	


<section class="main-wrapper feature-main cs-eai" id="feature-main">
		
	<div class="feature-white no-padding-bottom no-padding-top">
		<div class="container">
			<div class="row">
				
				<div class="col-md-10 col-md-offset-1" style="margin-bottom: 2em;">
					
					<div class="feature-content article-content" >

						<h2 >Social Sharing - Control what users see when they come across your MLP links</h2>

					</div>
										
				</div>
										 
				<div class="col-md-10 col-md-offset-1" style="margin-bottom: 2em;">
																				
					<img class="feature-img-no-bs" alt="" src="/public/sire/images/home/homeupdate/promotions.jpg" style=" max-height: 135px; width: auto; float: left; margin-right: 1em;">
										
					<div class="feature-content article-content" >
		            		            	                
		                <div class="post-text" itemprop="text">
							<p>Facebook uses whats called the <a href="http://ogp.me/" rel="noreferrer">Open Graph Protocol</a> to decide what things to display when you share a link.  The OGP looks at your page and try's to decide what content to show.  We can lend a hand and actually <strong>tell</strong> Facebook what to take from our page.</p>
							
							<p><strong>The way we do that is with <code>og:meta</code> tags.</strong></p>
							
							<p>The tags look something like this - </p>
<!--- Preserve whitespace  --->							
<pre><code>&lt;meta property="og:title" content="Affordable Mobile Marketing & SMS Marketing" //&gt; 
&lt;meta property="og:type" content="website" //&gt; 
&lt;meta property="og:image" content="	https://www.siremobile.com/public/sire/images/home/homeupdate/promotions.jpg" //&gt; 
&lt;meta property="og:url" content="https://www.siremobile.com/" //&gt; 
&lt;meta property="og:description" content="You're better than your competition. You deserve a text marketing interface that is better than the rest. Artificial Intelligence is a concept that large enterprises have had the luxury of being able to afford. At Sire, we don't think the big boys should have all the fun. We built our ICI so that SM..." //&gt; 
</code></pre>
							
							<p>You'll need to place these or similar meta tags in the Coder->Custom Global Header of your MLP document.  Don't forget to substitute the values for your own!</p>
							
							<p>For more info you can read all about how Facebook uses these meta tags in their documentation.  Here is one of the tutorials from there -  <a href="https://developers.facebook.com/docs/opengraph/tutorial/" rel="noreferrer">https://developers.facebook.com/docs/opengraph/tutorial/</a></p>
							
							<hr>
							
							<p>Facebook gives us a great little tool to help us when dealing with these meta tags - you can use the <a href="https://developers.facebook.com/tools/debug" rel="noreferrer">Facebook Sharing Debugger</a> to see how Facebook see's your URL and it'll even tell you if there are problems with it.</p>
							
							<p>One thing to note here is that every time you make a change to the meta tags, you'll need to tell the <a href="https://developers.facebook.com/tools/debug" rel="noreferrer">Facebook Sharing Debugger</a> to <button value="1" class="" type="submit">Scrape Again</button> so that Facebook will clear all the data that is cached on their servers about your URL.</p>
						</div>

		                
		            </div>
				</div>		
				
				<div class="col-md-10 col-md-offset-1" style="margin-bottom: 2em;">
																				
					<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/mlp-link-preview-sample1.png" style="">
				</div>				
				
				
				
				
						
		                         
			</div>
		</div>
	
	</div>
</section>



<cfparam name="variables._title" default="What happens when I run out of credits?">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

