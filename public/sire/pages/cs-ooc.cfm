
<style>
	
	.article-content:before {
	  content: ' ';
	  display: table;
	  width: 10em;    
	}
	
	<!--- https://zellwk.com/blog/responsive-typography/ --->
		<!--- https://css-tricks.com/viewport-sized-typography/ --->
		.article-content h1, #main-stage-content h1 {
			font-size: 5.7vw;
		}
		
		.article-content h2, #main-stage-content h2 {
			font-size: 4.0vw;
		}
		
		.article-content h3, #main-stage-content h3 {
			font-size: 2.8vw;
		}
		
		.article-content h4, #main-stage-content h4 {
			font-size: 2.0vw;
		}
		
		.article-content h5, #main-stage-content h5 {
			font-size: 1.6vw;
		}
		
		.article-content p, #main-stage-content p {
			font-size: 2.5vw;
		}
	
		@media all and (min-width: 800px) {
		  .article-content h1, #main-stage-content h1 {
				font-size: 5.7vw;
			}
			
			.article-content h2, #main-stage-content h2 {
				font-size: 4.0vw;
			}
			
			.article-content h3, #main-stage-content h3 {
				font-size: 2.8vw;
			}
			
			.article-content h4, #main-stage-content h4 {
				font-size: 2.0vw;
			}
			
			.article-content h5, #main-stage-content h5 {
				font-size: 1.6vw;
			}
			
			.article-content p, #main-stage-content p {
				font-size: 2.5vw;
			}
		}
		
		@media all and (min-width: 1200px) {
		  .article-content h1, #main-stage-content h1 {
				font-size: 2.65vw;
			}
			
			.article-content h2, #main-stage-content h2 {
				font-size: 2.0vw;
			}
			
			.article-content h3, #main-stage-content h3 {
				font-size: 1.4vw;
			}
			
			.article-content h4, #main-stage-content h4 {
				font-size: 1.0vw;
			}
			
			.article-content h5, #main-stage-content h5 {
				font-size: 0.8vw;
			}
			
			.article-content p, #main-stage-content p {
				font-size: 1.2vw;
			}
		}


</style>	


<section class="main-wrapper feature-main cs-eai" id="feature-main">
		
	<div class="feature-white no-padding-bottom no-padding-top">
		<div class="container">
			<div class="row">
									 
				<div class="col-md-8 col-md-offset-2">
					
					<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/out-of-credits-broke.png" style="max-height: 33vh; width: auto; float: left;">
					
					
					<div class="feature-content article-content" >
		            
		            	<h2>What happens when I run out of credits?</h2>
		            	 
		            	<p>As long as you keep within your monthly plan limits you’re not likely to run out of credit, but if you do, here’s what will happen. </p> 
		            	                        	
		            	<p>No more messages can be sent to your customers. Any of your target audience who responds to your <i>Keywords</i>, will not get a response.</p>                        	
		            	<p>Because <i>Keywords</i> are not responsive, you will not be able to gather more subscribers to your various lists.</p>	                        	 
		                <p>Worth repeating... Any of your target audience who send to your <i>Keywords</i>, will not get a response.</p>
		                
		                <h2>What can I do?</h2>
		                <p>Check your meter/balance regularly.</p>
		                <p>You can always buy anytime credits (these credits will not expire) to buffer you from just going over your plan credits.</p>
		                <p>You can upgrade your plan at anytime.</p>
		                <p>Make sure your email address is working and up to date, and we will send a courtesy notice when your balance is getting low.</p>
		                
		                <h2>As a courtesy...</h2>
		                <p>We will still honor HELP and STOP requests for 30 days.</p>
		                <p>We will still keep your <i>Keywords</i> reserved as long as your account remains active.</p>
		                
		            </div>
				</div>				
		                         
			</div>
		</div>
	
	</div>
</section>



<cfparam name="variables._title" default="What happens when I run out of credits?">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

