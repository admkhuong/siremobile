<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<!---



--->
<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white">
        <div class="container">
            <div class="row">


                <div class="col-md-6">
                    
                    <div class="feature-content">        
                        <h4>A two minute SMS survey can:</h4>
                        <ul>
                            <li>Attract new leads.</li>
                            <li>Help you better serve your customers</li>
                            <li>Ask intelligent questions</li>
                            <li>Provide valuable feedback</li>
                            
                        </ul>
                        
                        <h4>Sire Advantages</h4>
                        
                        <ul>
                            <li>Weighted question answers</li>
                            <li>Advanced Branch Logic </li>
                            <li>Template Ideas for your business</li>
                            <li>Not just fire and forget - advanced Session flows</li>
                            <li>Still simple to use</li>
                            
                        </ul>
                        
                    </div>
                                        
                    
                    <div class="feature-content">                    
                    
                    	<p>Important considerations when choosing your investment portfolio</p>
                    
                        <p><b>Your Time Horizon</b></p>
                        
                        <p>When will you begin withdrawing your money from your account, and at what rate? If that date is many years away, you may be comfortable with a portfolio that carries a greater potential for appreciation and higher level of risk. There’s more time to weather the inevitable ups and downs of the market. </p>
    
    
                        <p><b>Your Risk Tolerance</b></p>
                        <p>How do you feel about risk? Some investments fluctuate more dramatically in value than others but may have the potential for higher returns. It’s important that you select investments that fit within your level of tolerance for this risk. </p>

                    </div>
                    
                    <h3 data-animation="animated bounceInLeft">
						Sample Program
					</h3>	
                    
                    <div style="border:#999 solid 1px; margin-bottom:100px;">                                         
                        
                        <div style="text-align:left; padding: 1.2em;">
    	                    <img class="" src="/public/sire/images/client_logo.png" style="width:150px;">
                            <h3 style="margin-top:.2em; padding-top:0;">Helping You Prepare for the Road Ahead</h3>                            
                        </div>
                        
                        
                        <div style="background-color:#648ba4; padding:1em; color:#FFF;">
                        	<p>Take our new investor survey to get started.</p>
	                        <p style="margin-bottom:0;">On your mobile phone<br/>Text the keyword: <b>Profile</b><BR/> To: <b><cfoutput>#shortcode#</cfoutput></b></p>
                        </div>
                    
                    </div>      
                    
                </div>
                
                
                <div class="col-md-6">
                	
                    <div class="feature-title" id="feature1">Investor Profile Template</div>
                                                            
                    <div class="feature-content">
                                        
                                         
                        <div class="row">
                            <div id="cp1" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 1. Welcome message for Investor Profile Questionnaire.<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Creating an asset allocation plan. This questionnaire will help you decide whether one of these portfolios is right for you.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp2" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 2. Question 1 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 3<br>C - 7<br>D - 10<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">I plan to begin withdrawing money from my<br>investments in:</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">Less than 3 years</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">3-5 years</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">6-10 years</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">11 years or more</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp3" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 3. Question 2 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 1<br>C - 4<br>D -8<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Once I begin withdrawing funds from my investments,<br>I plan to spend all of the funds in:</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">Less than 2 years</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">2-5 years</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">6-10 years</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">11 years or more</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp6" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 4. A weighted score combination of Questions 1 and 2 of less than 3 indicates a very short investment time horizon. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">You have indicated a very short investment time horizon. For such a short time horizon, a relatively low-risk portfolio of 40% short-term (average maturity of five years or less) bonds or bond funds and 60% cash is suggested, as stock investments may be significantly more volatile in the short term.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp7" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 5. Question 3 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 2<br>C - 4<br>D - 6<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">I would describe my knowledge of investments as</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">None</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">Limited</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">Good</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">Extensive</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp8" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 6. Question 4 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 4<br>C - 8 </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">When I invest my money, I am:</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">Most concerned about my investment losing value</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">Equally concerned about my investment losing or gaining value</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">Most concerned about my investment gaining value</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp9" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 7. Question 5 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 3<br>C - 6<br>D - 8<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Select the investments you currently own or have owned in the past with the highest letter. Example: You now own stock funds. In the past, you've<br>purchased international securities. Your highest letter<br>would be 'D'.</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">Money market funds or cash investments</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">Bonds and/or bond funds</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">Stocks and/or stock funds</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">International securities and/or international funds</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp10" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 8. Question 6 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 2<br>C - 5<br>D - 8<br> </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Consider this scenario:<br>Imagine that in the past three months, the overall stock market lost 25% of its value. An individual stock investment you own also lost 25% of its value. What would you do?</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">Sell all of my shares</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">Sell some of my shares</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">Do nothing</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">Buy more shares</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp11" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 9. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">We've outlined the most likely best- and worst-case annual returns of five hypothetical investment plans. The figures are hypothetical and do not represent the performance of any particular investment. </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp12" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 10. Question 7 of the survey.<br>Point Scoring: Answer Weight<br>A - 0<br>B - 3<br>C - 8<br>D - 6<br>E - 10 </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">Which range of possible outcomes is most acceptable to you?</div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">7.2% Avg 16.3% Best-Case -5.6% Worst-Case</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">9.0% Avg 25.0% Best-Case -12.1% Worst-Case</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">10.4% Avg 33.6% Best-Case -18.2% Worst-Case</span>
                            </div>
                            <div class="control-point-option">
                            D) <span class="control-point-option-text">11.7% Avg 42.8% Best-Case -24.0% Worst-Case</span>
                            </div>
                            <div class="control-point-option">
                            E) <span class="control-point-option-text">12.5% Avg 50.0% Best-Case -28.2% Worst-Case</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp16" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 11. Final Message based on result of calculation of Section 1 (Questions 1 and 2) and Section 2 (Questions 3-7) weighted answers. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Your Investor Profile: Conservative<br>For investors who seek current income and stability and are less concerned about growth.<br><br>Now that we have determined your Investor Profile, we can use this information to help you create an asset allocation plan.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp17" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 12. Final Message based on result of calculation of Section 1 (Questions 1 and 2) and Section 2 (Questions 3-7) weighted answers. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Your Investor Profile: Moderately Conservative<br>For investors who seek current income and stability, with modest potential for increase in the value of their investments.<br><br>Now that we have determined your Investor Profile, we can use this information to help you create an asset allocation plan.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp18" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 13. Final Message based on result of calculation of Section 1 (Questions 1 and 2) and Section 2 (Questions 3-7) weighted answers. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Your Investor Profile: Moderate<br>For long-term investors who don't need current income and want some growth potential. Likely to entail some fluctuations in value, but presents less volatility than the overall equity market.<br><br>Now that we have determined your Investor Profile, we can use this information to help you create an asset allocation plan.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp19" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 14. Final Message based on result of calculation of Section 1 (Questions 1 and 2) and Section 2 (Questions 3-7) weighted answers. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Your Investor Profile: Moderately Aggressive<br>For long-term investors who want good growth potential and don't need current income. Entails a fair amount of volatility, but not as much as a portfolio invested exclusively in equities.<br><br>Now that we have determined your Investor Profile, we can use this information to help you create an asset allocation plan.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div id="cp20" class="form-group clearfix control-point">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 15. Final Message based on result of calculation of Section 1 (Questions 1 and 2) and Section 2 (Questions 3-7) weighted answers. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Your Investor Profile: Aggressive<br>For long-term investors who want high growth potential and don't need current income. May entail substantial year-to-year volatility in value in exchange for potentially high long-term returns. <br><br>Now that we have determined your Investor Profile, we can use this information to help you create an asset allocation plan.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                        
                        
                        
                       
                    </div>
				</div>                        
				

            </div>
        </div>
    </div>
           
</div>

<cfparam name="variables._title" default="Investor Profile Template">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

