

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/template-preview.js">
</cfinvoke>


<section class="main-wrapper feature-main" id="feature-main">

    <div class="feature-white">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-12 TopHeaderLearning">                    
                   		                     
                      	<h2 class="FeatureHeaderColor">Sire SMS Samples</h2>
						<h4>                      	                            
                            Whether you're sending transactional or marketing SMS, the Sire solution is built upon solid SMS infrastructure, providing you with the industry’s best reliability, scalability, and SMS deliverability. Easily integrate with Sire’s cloud-based SMS infrastructure, eliminating the need to build, scale and maintain these systems in-house, allowing you to focus on growing your business.
                        </h4>                                   
                        
					</div>
                                    
				</div>
			</div>
		</div>
	    
         <div class="feature-white">
			<div class="container">
	         	<div class="row">
				
                     <div class="col-md-12">                    
                   		                     
                      	<div class="feature-title AlignCenter" id="feature1">Currently Featured Templates</div>
						<div class="feature-content">
                        
                        	<div class="row">
                            
                            	<div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-2xoptin">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Double Opt In</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                            
                        		<div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-confirmation">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Confirmation</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>                                
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-profile">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Investor Profile</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-mfa">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Multi Factor Auth</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-whosin">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Meet Up Group</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>                                
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-css">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Satisfaction Survey</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                               
                        	</div>
                                                            
						</div>                                   
                        
					</div>
                                       
                               
				</div>
                
                
			</div>
		</div>
        
         
        <div class="feature-white">
			<div class="container">
	                     
				<div class="row">
				
                     <div class="col-md-12">                    
                   		                     
                      <div class="feature-title" id="feature1">What other customers are using Sire for: <h4>Lots of ideas that you can use and customize.</h4></div>
						<div class="feature-content">
                        	
                             <div class="col-sm-4 col-md-4">
                                        
                        <ul>
                            <li class="btn-preview-campaign" data-template-id="24"><a href="javascript:void(0)">Account Threshold Alerts</a></li>
                            
                            <!--- Link to real templates via preview page for most of these --->
                            <li class="btn-preview-campaign" data-template-id="40"><a href="javascript:void(0)">Notice of Completion</li></a>
                            <li class="btn-preview-campaign" data-template-id="41"><a href="javascript:void(0)">Order Delays</li></a>
                            <li class="btn-preview-campaign" data-template-id="42"><a href="javascript:void(0)">Order Confirmation</li></a>
                            <li class="btn-preview-campaign" data-template-id="39"><a href="javascript:void(0)">Go Green Initiatives</li></a>
                            <li class="btn-preview-campaign" data-template-id="43"><a href="javascript:void(0)">Repair Ticket Completion</li></a>
                            <li class="btn-preview-campaign" data-template-id="44"><a href="javascript:void(0)">Local Service Change</li></a>
                            <li class="btn-preview-campaign" data-template-id="45"><a href="javascript:void(0)">Service Order Completion</li></a>  
                            <li class="btn-preview-campaign" data-template-id="46"><a href="javascript:void(0)">Rate Changes</li></a>             
                            <li class="btn-preview-campaign" data-template-id="47"><a href="javascript:void(0)">Payment Options</li></a>                            
                            <li class="btn-preview-campaign" data-template-id="48"><a href="javascript:void(0)">Data Usage Alert</li></a>       
                            <li class="btn-preview-campaign" data-template-id="55"><a href="javascript:void(0)">Termination Warnings / Notices</li></a>     
                            <li class="btn-preview-campaign" data-template-id="54"><a href="javascript:void(0)">Account Suspension</li></a>     
                            <li class="btn-preview-campaign" data-template-id="51"><a href="javascript:void(0)">Potential Fraud Notification</li></a>   
                            <li class="btn-preview-campaign" data-template-id="49"><a href="javascript:void(0)">New Device Alert</li></a>    
                            <li class="btn-preview-campaign" data-template-id="50"><a href="javascript:void(0)">Change Order Notification</li></a>    
                            <li class="btn-preview-campaign" data-template-id="53"><a href="javascript:void(0)">Special Event Notification</li></a>  
                            <li class="btn-preview-campaign" data-template-id="44"><a href="javascript:void(0)">Event Changes</li></a>
                            <li class="btn-preview-campaign" data-template-id="38"><a href="javascript:void(0)">Service Outage</li></a>
                            <li class="btn-preview-campaign" data-template-id="52"><a href="javascript:void(0)">Technician in Route</li></a>                            
                        </ul>
                    
                    </div>
                    
                    <div class="col-sm-4 col-md-4">
                        <ul>
                            <li><a href="cs-mfa">Multi Factor Authentication</li></a>
                            <li class="btn-preview-campaign" data-template-id="60"><a href="javascript:void(0)">Support</li></a>
                            <li class="btn-preview-campaign" data-template-id="68"><a href="javascript:void(0)">Rewards Balance</li></a>     
                            <li class="btn-preview-campaign" data-template-id="69"><a href="javascript:void(0)">Rewards Signup</li></a>   
                            <li class="btn-preview-campaign" data-template-id="70"><a href="javascript:void(0)">Offers</li></a>   
                            <li class="btn-preview-campaign" data-template-id="71"><a href="javascript:void(0)">Reservations - New</li></a> 
                            <li class="btn-preview-campaign" data-template-id="72"><a href="javascript:void(0)">Reservations - Cancellation</li></a> 
                            <li class="btn-preview-campaign" data-template-id="73"><a href="javascript:void(0)">Reservations - Change</li></a> 
                            <li class="btn-preview-campaign" data-template-id="77"><a href="javascript:void(0)">Product Retirement</li></a>     
                            <li class="btn-preview-campaign" data-template-id="78"><a href="javascript:void(0)">Product Availability</li></a> 
                            <li class="btn-preview-campaign" data-template-id="79"><a href="javascript:void(0)">Exceed Usage</li></a>    
                            <li class="btn-preview-campaign" data-template-id="80"><a href="javascript:void(0)">Trouble Resolution</li></a> 
                            <li class="btn-preview-campaign" data-template-id="81"><a href="javascript:void(0)">Firmware Updates</li></a> 
                            <li class="btn-preview-campaign" data-template-id="82"><a href="javascript:void(0)">Product Recalls</li></a>     
                            <li class="btn-preview-campaign" data-template-id="38"><a href="javascript:void(0)">Weather Emergency</li></a>
                            <li class="btn-preview-campaign" data-template-id="83"><a href="javascript:void(0)">Emergency Notifications</li></a>
                            <li class="btn-preview-campaign" data-template-id="84"><a href="javascript:void(0)">Disaster Recovery</li></a>
                            <li class="btn-preview-campaign" data-template-id="86"><a href="javascript:void(0)">Password Changed</li></a>
                            <li class="btn-preview-campaign" data-template-id="85"><a href="javascript:void(0)">New Device Accesed Your Account</li></a>
                        </ul>
                    
                    </div>
                    
                    <div class="col-sm-4 col-md-4">
                    
                        <ul>                          
                            <li class="btn-preview-campaign" data-template-id="74"><a href="javascript:void(0)">Coupons</li></a>     
                            <li class="btn-preview-campaign" data-template-id="87"><a href="javascript:void(0)">Virtual Assistance</li></a>     
                            <li class="btn-preview-campaign" data-template-id="88"><a href="javascript:void(0)">First Bill Explanation</li></a>     
                            <li class="btn-preview-campaign" data-template-id="89"><a href="javascript:void(0)">Bill Pay Processed</li></a> 
                            <li class="btn-preview-campaign" data-template-id="90"><a href="javascript:void(0)">Billing Errors</li></a>
                            <li class="btn-preview-campaign" data-template-id="91"><a href="javascript:void(0)">Credit Card Expired</li></a> 
                            <li class="btn-preview-campaign" data-template-id="92"><a href="javascript:void(0)">Statement Available Online Now</li></a>   
                            <li class="btn-preview-campaign" data-template-id="93"><a href="javascript:void(0)">Appointment Reminders</li></a>     
                            <li class="btn-preview-campaign" data-template-id="37"><a href="javascript:void(0)">Surveys</li></a>
                            <li class="btn-preview-campaign" data-template-id="47"><a href="javascript:void(0)">Government Mandates</li></a>    
                            <li class="btn-preview-campaign" data-template-id="95"><a href="javascript:void(0)">Blasts</li></a>     
                            <li class="btn-preview-campaign" data-template-id="98"><a href="javascript:void(0)">Political Messages</li></a>      
                            <li class="btn-preview-campaign" data-template-id="99"><a href="javascript:void(0)">Program Eligibility</li></a>
                            <li class="btn-preview-campaign" data-template-id="100"><a href="javascript:void(0)">Program Enrollment</li></a>  
                            <li class="btn-preview-campaign" data-template-id="101"><a href="javascript:void(0)">Profile Changes</li></a> 
                            <li class="btn-preview-campaign" data-template-id="102"><a href="javascript:void(0)">Customer Preferences</li></a>  
                            <li class="btn-preview-campaign" data-template-id="103"><a href="javascript:void(0)">Running Late</li></a>   
                            <li class="btn-preview-campaign" data-template-id="104"><a href="javascript:void(0)">Nobody Home</li></a>      
                            <li class="btn-preview-campaign" data-template-id="105"><a href="javascript:void(0)">Prescription Ready for Pickup</li></a>      
                            <li class="btn-preview-campaign" data-template-id="106"><a href="javascript:void(0)">Tracking Numbers</li></a> 
                                                
                        </ul>
                        
                    </div>             
                                                        
						</div>                                   
                        
					</div>
                    
				</div>
			</div>
		</div>
        
    	   
        
        
    
  
         
        
</div>




<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<cfparam name="variables._title" default="Samples Templates Solutions - SMS">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

