
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<style type="text/css">
.main-wrapper{
	padding-bottom: 0;
}
</style>

<cfinclude template="../pages/cs-transactional-api-inc.cfm">

<cfinclude template="../views/layouts/learning-and-support-layout.cfm">


<script type="application/javascript">

(function($){
	$("#http-api-form").validationEngine({promptPosition : "topRight", scroll: false});

	$("#btn_build_URL").click(function() {

		var validatePhone = 0;
		$.ajax({
			async: false,
			type: "POST",
			url: '/public/sire/models/cfc/validate.cfc?method=ValidatePhoneNumber&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data: {PhoneNumber:$('#inpContactString').val()},
			beforeSend: function( xhr ) {
					
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				
			},					  
			success: function(data)	{
				if(data.RXRESULTCODE == -1) {
					 validatePhone = 1;
				}
			}
				
		});

		if (validatePhone == 1) {
		    bootbox.dialog({
			    message: 'Phone Number is not a valid US telephone number',
			    title: "Signup",
			    buttons: {
			        success: {
			            label: "OK",
			            className: "btn btn-success btn-success-custom",
			            callback: function() {}
			        }
			    }
			});
			return false;
		}
		
		var LocalHTTPSAPIURL = 'https://api.siremobile.com/ire/secure/triggerSMS?inpContactString=' + encodeURIComponent($("#inpContactString").val()) + '&inpBatchId=' + encodeURIComponent($("#inpBatchId").val()) + '&inpUserName=' + encodeURIComponent($("#inpUserName").val()) + '&inpPassword=' + encodeURIComponent($("#inpPassword").val()) ;
		
		$("#HTTPSAPIURL").html(LocalHTTPSAPIURL);
		
			
		var LocalHTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
		$("#HTTPSAPIJSON").html(LocalHTTPSAPIJSON);
			
	});	
	
	$("#btn_build_JSON").click(function() {		
		
		var HTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
		$("#HTTPSAPIJSON").html(HTTPSAPIJSON);
			
	});	

	$("#inpContactString").mask("(000) 000-0000");
    
	var HTTPSAPIURL = 'https://api.siremobile.com/ire/secure/triggerSMS?inpContactString=' + encodeURIComponent($("#inpContactString").val()) + '&inpBatchId=' + encodeURIComponent($("#inpBatchId").val()) + '&inpUserName=' + encodeURIComponent($("#inpUserName").val()) + '&inpPassword=' + encodeURIComponent($("#inpPassword").val()) ;
	$("#HTTPSAPIURL").html(HTTPSAPIURL);
	
	var HTTPSAPIJSON = '{\n'
       +'"inpContactString" : "'+ encodeURIComponent($("#inpContactString").val()) + '",\n'
       +'"inpBatchId"  : "' + encodeURIComponent($("#inpBatchId").val()) + '",\n'
       +'"inpUserName" : "' + encodeURIComponent($("#inpUserName").val()) + '",\n'
	   +'"inpPassword" : "' + encodeURIComponent($("#inpPassword").val())  + '"\n'
       +'}';
		
	$("#HTTPSAPIJSON").html(HTTPSAPIJSON);
		
		
})(jQuery);


</script>
