<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>
<!---



--->
<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white">
        <div class="container">
            <div class="row">


                <div class="col-md-6">
                    
                    <div class="feature-content">        
                        <h4>With Meetup Groups</h4>
                        <ul>
                            <li>Attract new leads.</li>
                            <li>Help you better serve your customers</li>
                            <li>Ask intelligent questions</li>
                            <li>Provide valuable feedback</li>
                            
                        </ul>
                        
                        <h4>Sire Advantages</h4>
                        
                        <ul>
                            <li>Weighted question answers</li>
                            <li>Advanced Branch Logic </li>
                            <li>Template Ideas for your business</li>
                            <li>Not just fire and forget - advanced Session flows</li>
                            
                        </ul>
                        
                    </div>
                                      
                    <h3 data-animation="animated bounceInLeft">
						Sample Program
					</h3>	
                    
                    <div style="border:#999 solid 1px; margin-bottom:100px;">                                         
                        
                        <div style="text-align:left; padding: 1.2em;">
    	                    <img class="" src="/public/sire/images/learning/24hourlogo.png" style="width:150px;">
                            <h3 style="margin-top:.2em; padding-top:0;">1:00 PM Basketball Meet up Group<br/>Lakeshore Towers</h3>                            
                        </div>
                        
                        
                        <div style="background-color:#648ba4; padding:1em; color:#FFF;">
                        	<p>Daily Message Group</p>
	                        <p style="margin-bottom:0;">On your mobile phone<br/>Text the keyword: <b>24LST</b><BR/> To: <b><cfoutput>#shortcode#</cfoutput></b></p>
                        </div>
                    
                    </div>      
                    
                </div>
                
                
                <div class="col-md-6">
                	
                    <div class="feature-title" id="feature1">Meet up Groups</div>
                                                            
                    <div class="feature-content">
                                        
                        <div class="row">
                           
                            <div class="form-group clearfix control-point" id="cp1">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 1. First message to the end user. This question is to verify their request to join the list. Accepts Natural Human Language for confirmation. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">24 Hour LST BBall Who's in for 1:00? Reply yes to JOIN this group. TXT STOP to end RPLY HELP for help. msg&amp;data rates may apply. Msgs Daily.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp3">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 2. Warning message if they do not successfully join </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">You have NOT confirmed your membership in the Who's in Group. To try again send the text 24LST to this short code to try again.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp4">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 3. Add phone number to a subscriber list. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-optin">
                            <div class="control-point-text Preview-Bubble Preview-Me"></div>
                            <br><label>Subscriber list:</label> 
                            <span class="cp-oig-text">
                            N/A
                            </span>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp5">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 4. Confirmation message that you have joined this meet up group. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">You are now part of the group. We will send weekday Msgs at 9:00 AM to see who is in each day. TXT STOP to end RPLY HELP for help.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp9">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 5. Daily question - goes out at 9:00AM M-F </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="Preview-Bubble Preview-Me">
                            <div class="control-point-text">24LS Daily 1:00 PM Basketball Group Message.<br>Reply STOP to leave this group<br>Are you in or out today? Reply before noon for todays game. </div>
                            <div class="control-point-options">
                            <div class="control-point-option">
                            A) <span class="control-point-option-text">IN</span>
                            </div>
                            <div class="control-point-option">
                            B) <span class="control-point-option-text">OUT</span>
                            </div>
                            <div class="control-point-option">
                            C) <span class="control-point-option-text">MAYBE</span>
                            </div>
                            </div>
                            </div> 
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp12">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 6. Response if they answer IN. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Great! See you there. We will send a group message around 12:00 Noon to let you know how many people are IN by that point.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp15">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 7. Counts on who is in will be sent at Noon to those that replied IN or MAYBE. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">See you there.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp18">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 8. Response if they answer MAYBE. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">OK, Hope to see you there. We will send a group message around 12:00 Noon to let you know how many people are IN by that point.</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group clearfix control-point" id="cp23">
                            <div class="col-sm-12">
                            <div class="control-point-border clearfix">
                            <div class="col-sm-12">
                            <label class="control-point-label"># 9. Response if they answer OUT. </label>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr0">
                            <div class="col-sm-12 clearfix">
                            <div class="control-point-body">
                            <div class="control-point-text Preview-Bubble Preview-Me">Ok see you next time</div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                                                    
                        
                       
                    </div>
				</div>                        
				

            </div>
        </div>
    </div>
           
</div>


<cfparam name="variables._title" default="Case Studies">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

