	<header class="home bg-gray clearfix">
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-xs-5">
						<div class="landing-logo">
							<a href="/">
								<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                        		<img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
							</a>
						</div>
					</div>	

					
							<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
								<div class="col-sm-9 col-xs-5">
									<div class="login-wrapper">
										<cfinclude template="../views/commons/menu.cfm">
									</div>	
								</div>	
							<cfelse>
								<div class="col-sm-9 col-xs-7">
									<div class="login-wrapper">
										<cfinclude template="../views/commons/menu_public.cfm">
									</div>
								</div>	
							</cfif>
				</div>
			</div>		
		</section>
			
		<section class="landing-jumbo hidden-xs">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p class="landing_title">Easy to use features </p>
						<p class="sms_marketing">Tools and Support for Mobile Engagement</p>
					</div>	
				</div>	
			</div>	
		</section>
	</header>
	<div class="container-fluid text-center">
                        
    
         <div class="container">
         
           <div class="row contact-intro">
                <p class="col-lg-8 col-sm-12">
                   <h2 class="heading-home">SMS/Text is the most widely-used and frequently used app on a smartphone with 97% of Americans using it at least once a day. Sire makes it easy to exchange useful information with your customers to ensure the ultimate in customer service and experience.  Provide mobile messaging that is high quality, relevant, personalized and delivered in context.</h2>
                </p>
            </div>                    
         </div>   
          
        
        <div class="container">
	
    
    		<div class="row">
				<div class="col-lg-3 col-md-6 col-xs-6">
					<div class="feature-box">
						<a href="#feature1">
						<div class="feature-box-title">Template Picker</div>
						<div class="feature-box-img feature1"></div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-xs-6">
					<div class="feature-box">
						<a href="#feature2">
						<div class="feature-box-title">Contact List Mgmt</div>
						<div class="feature-box-img feature2"></div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-xs-6">
					<div class="feature-box">
						<a href="#feature3">
						<div class="feature-box-title">Campaign Mgmt Details</div>
						<div class="feature-box-img feature3"></div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-xs-6">
					<div class="feature-box">
						<a href="#feature4">
						<div class="feature-box-title">Reporting</div>
						<div class="feature-box-img feature4"></div>
						</a>
					</div>
				</div>
			</div>
		</div>
   </div>
	<section class="main-wrapper feature-main">
		<div class="feature-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-6"><img class="feature-img" alt="" src="/public/sire/images/feature-demo1.png"></div>
					<div class="col-md-6">
						<div class="feature-title" id="feature1">Template Picker</div>
						<div class="feature-content">
                        	<p>Not all text messages are created equal. When designing a messaging application, Sire helps users know where to put different types of data to share in the message customization and business rules. It also shows how to use many types of data coming from other applications.</p>
                        	<p>Achieve the impressive response and conversion rates mobile marketing is known for with Sire’s highly personalized template picker for simple to complex tasks, and everything in between:
                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            
                            	<ul>
                                	<li>Self Service and Templates available with no setup fees</li>
                                    <li>Guided entry into the SMS and Mobile App world</li>
                                    <li>Shared Short Codes</li>
                                   
                                </ul>
                            
                            </div>                            
                         
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            	<ul>
                                	<li>Dedicated Short Codes</li>
                                    <li>Reporting and Analytics</li>
                                    <li>List Building Tools</li>
                                    <li>Rest API – access</li>
                                </ul>
                            
                            </div>
                                
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature2">Contact List Mgmt</div>
						<div class="feature-content">
                        	<p>SIRE’s powerful control panel manages list with thousands of mobile phone numbers in just a few clicks.  Develop a data-rich subscriber database to build successful campaigns.</p>
                        	
                            <p>
                            	
                                <ul>
                                	<li>
                                    	<p><b>Organize Lists</b></p>
                                        <p>View information the way your business needs, based on preferences for maximum results in bulk text distribution</p>                                    
                                    </li>
                                    
                                    <li>
                                    	<p><b>List Building Tools</b></p>
                                        <p>Build your contact list to its full potential with no limits to how many contacts you can store.</p>                                    
                                    </li>
                                                                                                            
                                    <li>
                                    	<p><b>Filter, Edit, Remove, or Add Contacts</b></p>
                                        <p>Update individual contacts, add large or small amounts of data, remove bulk information to keep information fresh.</p>                                    
                                    </li>
                                                                      
                                </ul>
                            
                            </p>
                        
                        </div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="/public/sire/images/feature-demo2.png">
					</div>
				</div>
			</div>
		</div>
		<div class="feature-gray">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div>
							<img class="feature-img" alt="" src="/public/sire/images/feature-demo3.png">
						</div>
					</div>
					<div class="col-md-6">
						<div class="feature-title" id="feature3">Campaign Details</div>
						<div class="feature-content">
                        	<p>Discover reliable options to best manage your SMS campaigns.  Easy to use dashboard, dozens of features, and delivery speeds of up to 200 per second make Sire one of the fastest Interactive SMS providers.</p>
                            
                            
                            <p>                            
                             	<ul>
                                	<li>
                                    	<p><b>Drip Marketing Campaigns</b></p>
                                        <p>Strategically "drips" a pre-scripted set of messages to customers or prospects over time as a way to up-sell and cross-promote.</p>                                    
                                    </li>
                                    
                                    <li>
                                    	<p><b>Multi-Factor Authentication</b></p>
                                        <p>One Time verification code uses information sent to a phone via SMS, Push or Voice for use as part of the login process.</p>                                    
                                    </li>
                                                                                                            
                                    <li>
                                    	<p><b>Power of Customer Experience Management</b></p>
                                        <p>Tailor your messaging strategies to focus on the needs of individual customers.</p>                                    
                                    </li>
                                    
                                    <li>
                                    	<p><b>Custom Data Fields</b></p>
                                        <p>Our solutions are specifically designed to capitalize on the unique capabilities of your business, to support your way of doing what you do.</p>                                    
                                    </li>
                                    
                                    <li>
                                    	<p><b>Express Messaging Services</b></p>
                                        <p>Fast, Simple, Easy. Sire makes even the basic messaging stuff easier.</p>                                    
                                    </li>
                                                                      
                                </ul>                            
                            </p>
                        
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature4">Reporting</div>
						<div class="feature-content"><p>SIRE’s reporting tools allow users to obtain information and insights on message deliverability and engagement.  Advanced reporting tools provide up-to-date information on account activity at all times. Users receive information in real-time allowing you to gauge how well things are going.</p></div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="/public/sire/images/feature-demo4.png">
					</div>
				</div>
			</div>
		</div>
	</section>
	
<cfparam name="variables._title" default="Easy To Use">
<cfparam name="variables.body_class" default="body-featured">
<cfinclude template="../views/layouts/plan_pricing.cfm">

<!---
<cfinclude template="../views/layouts/plan_pricing.cfm">
--->