

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/enterprise.min.css">
</cfinvoke>




<header class="home header-why-sire header-banners si-enterprise-header">
	<section class="landing-top">
        <div class="container-fluid">
            <div class="row">
                <div class="public-logo">
                    <div class="landing-logo clearfix">
                        <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                            <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                            <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                        </a>
                    </div>
                </div>
                
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    <!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    <div class="header-nav">
                        <cfinclude template="../views/commons/menu.cfm">    
                    </div>  
                <cfelse>
                    <!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    <div class="header-nav">
                        <cfinclude template="../views/commons/menu_public.cfm">
                    </div>  
                </cfif>
                        
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                    &nbsp;  
                <cfelse>
                    <cfinclude template="../views/commons/public_signin.cfm">  
                </cfif>
            </div>
        </div>      
    </section>
	<section class="landing-jumbo">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="/contact-us" class="btn btn-success-custom hidden-xs request-demo">REQUEST LIVE DEMO</a>
				</div>	
			</div>
			<div class="jumbo-image hidden-xs hidden-sm">
				<img src="/public/sire/images/enterprise-iphone.png" alt="enterprise sire">
			</div>
		</div>
	</section>
</header>        
 
<section class="main-wrapper si-enterprise" id="feature-main">
	<div class="si-enterprise-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6 col-xs-12">
					<div class="img-thumb">
						<img src="/public/sire/images/enterprise_img_01.png" alt="enterprise sire">
					</div>
				</div>
				<div class="col-md-5 col-sm-6 col-xs-12">
					<div class="texture">
						<h2>Enterprise Application Integration</h2>
						<p>Enterprise application integration is a business need to make diverse applications in an enterprise, including partner systems, communicate to each other to achieve a business objective in a seamless reliable fashion irrespective of platform and geographical location of these applications. Sire EAI comprises differing components to support message acceptance, transformation, translation, routing, message delivery and business process management. Sire provides both self service tools for the Small and Medium Business (SMB), as well as Enterprise Application Integration (EAI) for more complex messaging services.</p>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!--- /intro --->
	<div class="si-enterprise-how">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-7">
					<div class="texture">
						<h2>How can Sire be integrated into your Application?</h2>
						<p class="em">Data Transfer Mechanisms</p>
						<ul>
							<li>HTTP/HTTPS - via REST API's</li>
							<li>SFTP</li>
							<li>HTTP/HTTPS - via form or Event variables</li>
							<li>HTTP/HTTPS - via direct XML</li>
							<li>HTTP/HTTPS - via SOAP</li>
							<li>Direct SQL - mySQL, MS SQL Server, Oracle</li>
							<li>Email</li>
							<li>SMPP</li>
							<li>XMPP - Chat</li>
							<li>Direct VPN Tunnel</li>
							<li>Dial up to secure server</li>
							<li>DAT/CD/media via courier</li>
							<li>FAX</li>
						</ul>
						<p class="em">Tip:</p>
						<p>Sire personel can help you re-use and repurpose existing data transfer assets to save time and reduce IT costs.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5">
					<div class="img-thumb">
						<img src="/public/sire/images/enterprise_img_02.png" alt="enterprise sire">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--- /how --->
	<div class="si-enterprise-engine">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="img-thumb">
						<img src="/public/sire/images/enterprise_img_03.png" alt="enterprise sire">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="texture">
						<h2>Intelligent Rules Engine</h2>
						<p class="em">Via homogeneous analysis of business structure and processes</p>
						<ul>
							<li>Complex Event Processing</li>
							<li>Rules Applied based on CDFs</li>
							<li>Rules Applied based on External Data</li>
							<li>Rules Applied based on Customer Preference</li>
							<li>Optimization</li>
							<li>Classification</li>
							<li>Pre-Processing</li>
							<li>Dynamically applied based on messaging interactions</li>
							<li>Post-Processing</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--- /engine --->
	<div class="si-enterprise-tools">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-7">
					<div class="texture">
						<h2>Best of Breed Tools and Support for Messaging</h2>
						<p class="em">Fulfillment - Your customers could be anywhere, be where your customers are.</p>
						<ul>
							<li>Real Time</li>
							<li>Tools to Caputure and act on your customer’s channel preference.</li>
							<li>Escalations</li>
							<li>Seamless approache to messaging guides potential customers towards engagement.</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5">
					<div class="img-thumb">
						<img src="/public/sire/images/enterprise_img_04.png" alt="enterprise sire">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--- /tools --->
	<div class="si-enterprise-analytics">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="img-thumb">
						<img src="/public/sire/images/enterprise_img_05.png" alt="enterprise sire">
					</div>
					
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="texture">
						<h2>Reporting and Analytics</h2>
						<p class="em">Results - Your customers could be anywhere, be where your customers are.</p>
						<ul>
							<li>Consumers can now respond in multiple ways to any given campaign, and you'll want to make sure those responses can be captured and measured appropriately.</li>
							<li>Request more information to be sent on an alternate channel. Terms of use, brochures, policies and more.</li>
							<li>Escalations</li>
							<li>Sire Automation Initiation</li>
							<li>Consumer Initiated</li>
						</ul>
						<p>Communication events are triggered throughout the customer lifecycle</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	
<cfparam name="variables._title" default="Enterprise - Sire">
<cfinclude template="../views/layouts/plan_pricing.cfm">

