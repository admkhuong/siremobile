<div class="container">
    <section class="container-body">
        <h1>We apologize - An Error Has Occurred</h1>
        <p> If this is a persistent error please try one of the following:</p>
        <p> Contact customer support at <a href="mailto:support@siremobile.com">support@siremobile.com</a>  </p>
        <p> We apologize for any inconvenience this may cause you. </p>
    </section>
</div>

<cfparam name="variables._title" default="Error Page - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">