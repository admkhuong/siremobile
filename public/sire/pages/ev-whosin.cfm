<!--- Load Setting Defaults --->
<cfset DBSourceRead = "Bishop" />
<cfparam name="bid" default="0">
<cfparam name="inpDate" default="#dateformat(Now(),'yyyy-mm-dd')#">

<!--- User Id used to lookup list data  --->
<cfparam name="UIDFL" default="497">


		<!--- Pure CSS Date Display ---> 
		<style>
				
			time.icon
			{
			  font-size: 1em; /* change icon size */
			  display: block;
			  position: relative;
			  width: 7em;
			  height: 7em;
			  background-color: #fff;
			  margin: 1em 1em;
			  border-radius: 0.6em;
			  box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
			  overflow: hidden;
			  -webkit-backface-visibility: hidden;
			  -webkit-transform: rotate(0deg) skewY(0deg);
			  -webkit-transform-origin: 50% 10%;
			  transform-origin: 50% 10%;
			}
			
			time.icon *
			{
			  display: block;
			  width: 100%;
			  font-size: 1em;
			  font-weight: bold;
			  font-style: normal;
			  text-align: center;
			}
			
			time.icon strong
			{
			  position: absolute;
			  top: 0;
			  padding: 0.4em 0;
			  color: #fff;
			  background-color: #f00;
			  border-bottom: 1px dashed #ffffff;
			  box-shadow: 0 2px 0 #f00;
			}
			
			time.icon em
			{
			  position: absolute;
			  bottom: 0.3em;
			  color: #f00;
			}
			
			time.icon span
			{
			  width: 100%;
			  font-size: 2.8em;
			  letter-spacing: -0.05em;
			  padding-top: 0.8em;
			  color: #2f2f2f;
			}
			
			time.icon:hover, time.icon:focus
			{
			  -webkit-animation: swing 0.6s ease-out;
			  animation: swing 0.6s ease-out;
			}
			
			@-webkit-keyframes swing {
			  0%   { -webkit-transform: rotate(0deg)  skewY(0deg); }
			  20%  { -webkit-transform: rotate(12deg) skewY(4deg); }
			  60%  { -webkit-transform: rotate(-9deg) skewY(-3deg); }
			  80%  { -webkit-transform: rotate(6deg)  skewY(-2deg); }
			  100% { -webkit-transform: rotate(0deg)  skewY(0deg); }
			}
			
			@keyframes swing {
			  0%   { transform: rotate(0deg)  skewY(0deg); }
			  20%  { transform: rotate(12deg) skewY(4deg); }
			  60%  { transform: rotate(-9deg) skewY(-3deg); }
			  80%  { transform: rotate(6deg)  skewY(-2deg); }
			  100% { transform: rotate(0deg)  skewY(0deg); }
			}
			
		</style>


	</head>


<!---


Need Tiny URL System for Sire 
.co?
sire.co?
ire.co?

default tinyurl landing page just parses for UID

Hide phone numbers

Capture Names
	Via keyword?
	Via Form?



--->
	
		<cfoutput>
			<section class="main-wrapper feature-main" id="feature-main">
					
			 	<div class="feature-white no-padding-bottom no-padding-top">
		    
					<div class="container padding-top-1em">
						<div class="row">
							 <div class="col-md-8 col-md-offset-2">
								<div class="feature-title">Who is in today?</div>
								<h5 style="margin:0;">24 Hr Fitness Lakeshore Towers 1:00 Basketball Group</h5>	
								<h5 style="margin:0;">Text <b>Lakeshore</b> to <b>39492</b> to Join List</h5>					
								
								<div class="feature-content">
			                    	
			                    	<cfset StatusIn = "0"/> 
							        <cfset StatusMaybe = "0"/> 
							        <cfset StatusOut = "0"/> 
									<cfset StatusUnknown = "0"/> 
			
			
			                    	 <!--- Get list of people in today --->                          
						            <cfquery name="GetEventStatuses" datasource="#DBSourceRead#" >   
						                SELECT 
						                	ContactString_vch,
						               		Status_int              	
						                FROM
						                	simplequeue.eventapp
						                WHERE
						                	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_BIGINT" VALUE="#bid#">
						                AND
						                	Event_date = '#LSDateFormat(inpDate, 'yyyy-mm-dd')#'			                			                        
						            </cfquery>
						            
						            
						            <cfloop query="GetEventStatuses">


										<cfswitch expression="#GetEventStatuses.Status_int#">
							       		
							       			<cfcase value="1">								       			
								       			<cfset StatusIn = StatusIn + 1/>  
							       			</cfcase>
							       			
							       			<cfcase value="2">								       		
								       			<cfset StatusOut = StatusOut + 1/> 
							       			</cfcase>
							       										       			
							       			<cfcase value="3">								       		
								       			<cfset StatusMaybe = StatusMaybe + 1/> 
							       			</cfcase>
							       									       			
							       			<cfdefaultcase>								       		
								       			<cfset StatusUnknown = StatusUnknown + 1/> 
							       			</cfdefaultcase>
							       			
							       		</cfswitch>			

						            </cfloop>
						            
						            <div class="row" style="margin:1em; ">
								       		
								      <div class="col-md-12" style="border-radius: 2em; padding:.5em; background-color:rgba(39, 78, 96, 0.99);" align="center">
									       
									   		<table> 
										   		<tr>
											   		<td rowspan="5">												
														<!-- the icon as a time element -->
												      	<time datetime="#inpDate#" class="icon">
														  <em>#DayofWeekAsString(DayOfWeek(inpDate))#</em>
														  <strong>#MonthAsString(Month(inpDate))#</strong>
														  <span>#DatePart("d", inpDate)#</span>
														</time>   		
											   		</td>											   		
											   		<td>&nbsp;</td>
										   		</tr>
										   	
										   		<tr>
											   		<td>&nbsp;</td>
											   		<td style="text-align: left; color: ##ffffff; white-space: nowrap;">  
											           (#StatusIn#) IN today<br/>
											           (#StatusMaybe#) MAYBE today<br/>
											           (#StatusOut#) OUT today</td>
											   		</td>   
										   		</tr>
										   	
										   											   		
									   		</table>
									  		
									        								   
								      </div>
								       		
						           </div>		
						            
						            <div class="row" style="margin-top:2em; margin-bottom: 2em;">
							            
							            <div class="col-xs-12 col-sm-12 col-md-12">
								            To set your own display name:<br/>
								            Text the keyword <b>24Name</b><br/>
								            To short code <b>39492</b>
							            </div>
						                
						            </div>
						            
						            
									<cfloop query="GetEventStatuses">
							          							            
							       		<div class="row" style="border-bottom: rgba(0, 0, 0, 0.2) solid 1px;">
								       		
								       		<cfset CurrDisplayName = "Name Not Set" />
								       		
								       		<!--- Looup user data name --->
								       		
								       		<!--- Check if contact string exists in main list of EBM User who is using this keyword - assumes contact string is an SMS number ContactType_int=3--->
						                    <cfquery name="CheckForContactString" datasource="#DBSourceRead#">
						                        SELECT 
						                            simplelists.contactlist.contactid_bi,
						                            simplelists.contactstring.contactaddressid_bi
						                        FROM 
						                            simplelists.contactstring
						                            INNER JOIN simplelists.contactlist on simplelists.contactlist.contactid_bi = simplelists.contactstring.contactid_bi
						                        WHERE
						                            simplelists.contactstring.contactstring_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetEventStatuses.ContactString_vch#">
						                        AND
						                            simplelists.contactlist.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UIDFL#"> 
						                        AND
						                        	(
						                            		simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="3">
						                            	OR
						                            		simplelists.contactstring.contacttype_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="1">
						                            )
						                        ORDER BY 
						                             simplelists.contactlist.contactid_bi ASC                                	    
						                    </cfquery> 
                    
						                     <!--- IF there is a contact defined then get it's data--->
						                    <cfif CheckForContactString.RecordCount GT 0>
						                        
						                        <!---<cfset DebugStr = DebugStr & " #CheckForContactString.ContactId_bi# #inpRequesterId_int#">--->
						                         
						                        <!--- Get custom user defined data for this current contact id--->
						                        <cfquery name="GetCustomFieldsData" datasource="#DBSourceRead#">
						                            SELECT 
						                                VariableName_vch,
						                                VariableValue_vch
						                            FROM 
						                                simplelists.contactvariable
						                            WHERE
						                                simplelists.contactvariable.contactid_bi = <cfqueryparam cfsqltype="cf_sql_integer" value="#CheckForContactString.ContactId_bi#">
						                            AND	
						                            	VariableName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="24Name">
						                        </cfquery> 
						                        
						                        <cfif GetCustomFieldsData.RecordCount GT 0 AND LEN(TRIM(GetCustomFieldsData.VariableValue_vch)) GT 0>							                        
							                        <cfset CurrDisplayName = "#GetCustomFieldsData.VariableValue_vch#" /> 
						                        </cfif>    						                        					                                                
						                                                                 
						                    </cfif>  <!--- CheckForContactString.RecordCount GT 0 --->    
				
								       		
								       		<div class="col-xs-5 col-sm-5 col-md-5">#CurrDisplayName#</div>
									       		
								       		<div class="col-xs-5 col-sm-5 col-md-5">(***) ***-#RIGHT(GetEventStatuses.ContactString_vch,4)#</div>	
								       		
									   		<div class="col-xs-2 col-sm-2 col-md-2">	
									       		<cfswitch expression="#GetEventStatuses.Status_int#">
									       		
									       			<cfcase value="1">
										       			In										       			
									       			</cfcase>
									       			
									       			<cfcase value="2">
										       			Out 
									       			</cfcase>
									       										       			
									       			<cfcase value="3">
										       			Maybe 
									       			</cfcase>
									       									       			
									       			<cfdefaultcase>
										       			Unknown 
									       			</cfdefaultcase>
									       			
									       		</cfswitch>									    
											</div>											   		
										</div>
						            </cfloop>
						           								
								</div>
							</div>
			         	</div>   
					</div>
				</div>        
			        
			</section>
		
		</cfoutput>	
		

<cfparam name="variables._title" default="Who's In Today">
<cfinclude template="../views/layouts/plain-layout.cfm">

