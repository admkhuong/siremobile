<cfparam name="url.tk" default="">
<cfparam name="tk" default="#url.tk#">
<cfset tk = ToString(ToBinary(tk))>
<cfset tk = ReMatch('\d{4}-\d{2}-\d{2}-\d+',tk)>

<cfif arrayLen(tk) GT 0>
	<cfset tk = tk[1]>
	<cfset UserId = ListLast(tk, '-')>
	<cfset DueDate = ListDeleteAt(tk, ListLen(tk, '-'), '-')>
	<!---<cfdump var="#DueDate#">
	<cfdump var="#UserId#">--->

	<cfset maskedNumber = ''>
	<cfset expirationDate = ''>
	<cfset firstName = '' > 
	<cfset lastName = '' > 

	<cfinvoke component="public.sire.models.cfc.order_plan"  method="GetUserPlan" returnvariable="RetUserPlan">
		<cfinvokeargument name="inpUserId" value="#UserId#">
	</cfinvoke> 

	<cfquery name="usersQuery" datasource="#Session.DBSourceREAD#">
		SELECT UserId_int, FirstName_vch, LastName_vch, EmailAddress_vch, HomePhoneStr_vch 
		FROM simpleobjects.useraccount
		WHERE UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#UserId#">
			AND Active_int = 1
		LIMIT 1
	</cfquery>
	
	<cfif usersQuery.RecordCount GT 0>
		<cfquery name="userPlansQuery" datasource="#Session.DBSourceREAD#">
			SELECT * FROM simplebilling.userplans
			WHERE Status_int = 1
				AND UserId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#UserId#">
				AND DATE(StartDate_dt) <= CURDATE()
			ORDER BY EndDate_dt DESC, UserPlanId_bi DESC
			LIMIT 1
		</cfquery>

		<cfset newEndDate_dt = DateAdd("m",1,userPlansQuery.EndDate_dt)>

		<cfif RetUserPlan.PLANEXPIRED GTE 2>
			<cfset newEndDate_dt = userPlansQuery.EndDate_dt>			
		</cfif>


		<!--- CHECK PAY FOR PLAN (checkOwed = 0) OR FOR OWNED KEYWORD (checkOwed = 1) --->
		<cfset checkOwed = 0>
		<cfif userPlansQuery.BuyKeywordNumberExpired_int GT 0> 
			<cfset checkOwed = 1>
		</cfif>

		<cfif userPlansQuery.RecordCount GT 0>
			<cfset expricedTime = DateDiff('s', CreateDate(Year(userPlansQuery.EndDate_dt), Month(userPlansQuery.EndDate_dt), Day(userPlansQuery.EndDate_dt)), Now())>
			<cfif (expricedTime GE 0 AND expricedTime LT 2592000) OR checkOwed EQ 1>
				<cfquery name="plansQuery" datasource="#Session.DBSourceREAD#">
					SELECT * FROM simplebilling.plans
					WHERE Status_int = 1
						AND PlanId_int = <cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.PlanId_int#">
					LIMIT 1
				</cfquery>
				<cfif plansQuery.RecordCount GT 0>
					<cfif checkOwed EQ 1> <!--- If user pay for owe keyword amount = BuyKeywordNumberExpired_int --->

							<cfif RetUserPlan.PLANEXPIRED EQ 1> <!--- IF WITHIN 30 day, pay for buykeyword only --->
								<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec>
								<cfset keywordsNumber = userPlansQuery.BuyKeywordNumber_int>
								<cfset keywordsFee = keywordsNumber * plansQuery.PriceKeywordAfter_dec>	
							<cfelse>	
								<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumberExpired_int * plansQuery.PriceKeywordAfter_dec>
								<cfset keywordsNumber = userPlansQuery.BuyKeywordNumberExpired_int>
								<cfset keywordsFee = keywordsNumber * plansQuery.PriceKeywordAfter_dec>
							</cfif>
							
					<cfelse>
							<cfset totalAmount = plansQuery.Amount_dec + userPlansQuery.BuyKeywordNumber_int * plansQuery.PriceKeywordAfter_dec>
							<cfset keywordsNumber = userPlansQuery.BuyKeywordNumber_int>
							<cfset keywordsFee = keywordsNumber * plansQuery.PriceKeywordAfter_dec>
					</cfif>
					
					<cfif totalAmount GT 0>
						<cfinclude template="/session/sire/configs/credits.cfm">
						<cfhttp url="#payment_get_customer_payment##UserId#" method="GET" result="paymentResponse" >
							<cfhttpparam type="header" name="Authorization" value="Basic #payment_header_Authorization#">
						</cfhttp>

						<!---<cfif paymentResponse.status_code EQ 200>--->
							<cfif trim(paymentResponse.filecontent) NEQ "" AND isJson(paymentResponse.filecontent)>
								<cfset paymentResponseContent = DeserializeJSON(paymentResponse.filecontent)>
								<cfif paymentResponseContent.success AND paymentResponseContent.responseCode EQ 1>
									<!--- CHECK IF CUSTOMER NOT FOUND IN SERCUNET --->
									<cfif isNumeric(paymentResponseContent.vaultCustomer.customerId)>

										<cfset maskedNumber = paymentResponseContent.vaultCustomer.paymentMethods[1].card.maskedNumber>
										<cfset expirationDate = paymentResponseContent.vaultCustomer.paymentMethods[1].card.expirationDate>
										<cfif paymentResponseContent.vaultCustomer.paymentMethods[1].card.firstName NEQ ''>
											<cfset firstName = paymentResponseContent.vaultCustomer.paymentMethods[1].card.firstName>
										<cfelse>
											<cfset firstName = paymentResponseContent.vaultCustomer.firstName>
										</cfif>
										<cfif paymentResponseContent.vaultCustomer.paymentMethods[1].card.lastName NEQ ''>
											<cfset lastName = paymentResponseContent.vaultCustomer.paymentMethods[1].card.lastName>
										<cfelse>
											<cfset lastName = paymentResponseContent.vaultCustomer.lastName>
										</cfif>
										<cfset pageCase = 1>
									<cfelse>
										<cfset pageCase = 98>	
									</cfif>
									
								<cfelse>
									<cfset pageCase = 99>	
								</cfif>

							<cfelse>
								<cfset pageCase = 99>
							</cfif>
						<!---<cfelse>
							<cfset pageCase = -6>
						</cfif>--->

					<cfelse>
						<!--- auto add free plan to user --->
						<cfset newEndDate_dt = DateAdd("m",1,userPlansQuery.EndDate_dt)>
						<cfquery name="insertUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
							INSERT INTO simplebilling.userplans (
								Status_int,
								UserId_int,
								PlanId_int,
								Amount_dec,
								UserAccountNumber_int,
								Controls_int,
								KeywordsLimitNumber_int,
								FirstSMSIncluded_int,
								UsedSMSIncluded_int,
								CreditsAddAmount_int,
								PriceMsgAfter_dec,
								PriceKeywordAfter_dec,
								BuyKeywordNumber_int,
								StartDate_dt,
								EndDate_dt
							) VALUES (
								<cfqueryparam cfsqltype="cf_sql_integer" value="1">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#UserId#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.PlanId_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.Amount_dec#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.UserAccountNumber_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.Controls_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.KeywordsLimitNumber_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.FirstSMSIncluded_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.UsedSMSIncluded_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#plansQuery.CreditsAddAmount_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceMsgAfter_dec#">,
					        	<cfqueryparam cfsqltype="cf_sql_decimal" value="#plansQuery.PriceKeywordAfter_dec#">,
					        	<cfqueryparam cfsqltype="cf_sql_integer" value="#userPlansQuery.BuyKeywordNumber_int#">,
					        	<cfqueryparam cfsqltype="cf_sql_date" value="#userPlansQuery.EndDate_dt#">,
					        	<cfqueryparam cfsqltype="cf_sql_date" value="#newEndDate_dt#">
							)
						</cfquery>

						<cfquery name="expiredUserPlans" datasource="#Session.DBSourceEBM#" result="userPlansResult">
							UPDATE simplebilling.userplans 
								SET Status_int = 0
							WHERE UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#UserId#">
								AND Status_int = 1 AND DATE(EndDate_dt) <= CURDATE()
						</cfquery>
						
						<!--- RESET PLAN BALANCE --->
						<cfinvoke component="public.sire.models.cfc.order_plan" method="UpdateUserPlanBalance" returnvariable="RetupdateUserPlanBalance">
							<cfinvokeargument name="inpUserId" value="#UserId#">
							<cfinvokeargument name="inpBalance" value="#userPlansQuery.FirstSMSIncluded_int#">
						</cfinvoke> 
						
						<cfset pageCase = 3>
						
						<cflocation url='/session/sire/pages/' addtoken="false">
						<cfabort>
					</cfif>
				<cfelse>
					<cfset pageCase = -5>
				</cfif>
			<cfelseif expricedTime LT 0>
				<cfset pageCase = 2>
			<cfelse>
				<cfset pageCase = -4>
			</cfif>
		<cfelse>
			<cfset pageCase = -3>
		</cfif>
	<cfelse>
		<cfset pageCase = -2>
	</cfif>
<cfelse>
	<cfset pageCase = -1>
</cfif>

<cfoutput>
<div id="extend-plan" class="container">
         
	<section class="container-body">

    	<h1>Extend plan</h1>
		<!---<cfdump var="#ToBase64('2016-04-10-485')#">
		<cfdump var="#pageCase#">--->
    	
    	<cfswitch expression="#pageCase#">
    		<cfdefaultcase>
    			<div class="row">
            		<div class="col-md-12">
            			Your account is expired. Please contact customer support at <a href="mailto:support@siremobile.com">support@siremobile.com</a>.
            		</div>
        		</div>
    		</cfdefaultcase>
    		<cfcase value="2">
    			<div class="row">
            		<div class="col-md-12">
            			Your plan has not expired. Please login.
            		</div>
        		</div>
    		</cfcase>
    		<cfcase value="1;99;98" delimiters=";">
    			<!---<cfdump var="#paymentResponse#"><cfabort>--->
    			<div class="row">
            		<h4 class="col-md-12">
            			<div class="alert alert-info">
	            			<cfif checkOwed EQ 0>
	            				Your account has expired, please make a payment to continue.
	            			<cfelse>
	            				Please pay for your owed keywords.
	            			</cfif>
            			</div>
            		</h4>
        		</div>
    			<div class="row">
            		<div class="col-md-6">
		                <div class="row heading">
		                	<h3 class="col-sm-12 heading-title">My Plan<hr></h3>
		                </div>
		                <div class="row">
		                    <div class="col-xs-11 col-sm-11 secure-payment-plan">
		                    	<div class="pull-left">Plan Type:</div>
		                    	<div class="pull-right active">#plansQuery.PlanName_vch#</div>
		                    </div>
			                <div class="col-xs-11 col-sm-11 secure-payment-plan">
			                	<div class="pull-left">Monthly Plan Price:</div>
			                	<div class="pull-right active">$ #NumberFormat(plansQuery.Amount_dec,',')#</div>
			                </div>
			                <div class="col-xs-11 col-sm-11 secure-payment-plan">
			                	<div class="pull-left">Number of Buy Keyword:</div>
			                	<div class="pull-right active">#keywordsNumber#</div>
			                </div>
			                <div class="col-xs-11 col-sm-11 secure-payment-plan">
			                	<div class="pull-left">Monthly Keyword Fee:</div>
			                	<div class="pull-right active">$ #NumberFormat(keywordsFee,',')#</div>
			                </div>
			                <div class="col-xs-11 col-sm-11 secure-payment-plan">
			                	<div class="pull-left">Monthly Amount:</div>
			                	<div class="pull-right active">$ #NumberFormat(totalAmount,',')#</div>
			                </div>
			                <div class="col-xs-11 col-sm-11 secure-payment-plan">
			                	<div class="pull-left">Extend to Date:</div>
			                	<div class="pull-right active">#DateFormat(newEndDate_dt, 'mm/dd/yyyy')#</div>
			                </div>
		                    <!---<div class="col-xs-11 col-sm-11 secure-payment-plan">
		                    	<div class="pull-left">Plan Details Include:</div>
		                    </div>
		                   <div class="col-xs-12 col-sm-5 col-sm-offset-4 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
		                        <div class="sire-pricing">
		                            <div class="price-title">
		                                <h4>#plansQuery.PlanName_vch#</h4>
		                            </div>
		                            <div class="price-content">
		                                <p>#plansQuery.UserAccountNumber_int# User Account</p>
		                                <p>#plansQuery.KeywordsLimitNumber_int# Keywords</p>
		                                <p>First #NumberFormat(plansQuery.FirstSMSIncluded_int,',')# SMS Included</p>
		                                <p>#NumberFormat(plansQuery.PriceKeywordAfter_dec,'._')# Cents per Msg after</p>
		                            </div>
		                        </div>
		                    </div>--->
	                	</div>
            		</div>
            		<div class="col-md-6">
						<form id="card_form" method="post" action="/public/sire/models/cfm/extend-plan.cfm">
						
							<div class="row heading">
		            			<h3 class="col-sm-12 heading-title">Card Information<hr></h3>
		            			<!---<div class="col-sm-11 small-info" style="color:red">
	                    			For consistency, your billing date will be the same day every month.  If you start your plan on the 29th, 30th or 31st then your monthly billing date will fall on the 28th.
	                    		</div>
	                    		<div class="col-sm-11 small-info" style="color: ##74c37f">
	                    			Please review your purchase details, then select a payment method to continue.
	                    		</div>--->
	                    		<div style="<cfif pageCase NEQ 99 AND pageCase NEQ 98> display: block <cfelse> display: none </cfif>">
	                    			<div class="col-sm-11 secure-payment-plan">
										Card Number: #maskedNumber#<br>
			            				Expiration Date: #expirationDate#<br>
										Cardholder Name: #firstName# #lastName#<br>
			            			</div>

			            			<cfif pageCase EQ 99 or pageCase EQ 98>
			            				<div class="col-sm-11 text-danger ">
			            					Your current credit card is invalid right now, please use other one to make a payment!
			            				</div>	
		            				</cfif>
		            				
									<div class="col-sm-11">
										<br>
			            				<div class="radio-inline">
										 <label>
										    <input type="radio" name="currentCard" class="select_current_card" id="optionsRadios1" value="1" <cfif pageCase NEQ 99 AND pageCase NEQ 98> checked <cfelse> disabled </cfif> >
										    	Current Card
										  </label>
										</div>
										<div class="radio-inline">
										  	<label>
										    	<input type="radio" name="currentCard" class="select_current_card" id="optionsRadios2" value="2" <cfif pageCase EQ 99 OR pageCase EQ 98> checked </cfif> >
										    	New Card
										  	</label>
										</div>			
									</div>
	                    		</div>
	                    		<!--- <cfif pageCase NEQ 99 AND pageCase NEQ 98> --->
			            			
								<!--- </cfif> --->
							</div>
						
						<div id="card_info" style="<cfif pageCase NEQ 99 AND pageCase NEQ 98> display:none; </cfif>">
							<hr style="<cfif pageCase EQ 99 OR pageCase EQ 98> display: none </cfif>">
							<input type="hidden" name="tk" value="#url.tk#">
	        				<div class="row heading">
	        					<div class="col-sm-11 form-horizontal">
									<label class="payment_method_card payment_method_card_0" for="payment_method_0">
			        					&nbsp;
			        				</label>
			        				<br><br>
		        				</div>
	        				</div>
	        				<div class="row">
			                	<div class="col-sm-11 form-horizontal">
								  <div class="form-group">
								    <label for="card_number" class="col-sm-4 control-label">Card Number:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="security_code" class="col-sm-4 control-label">Security Code:<span class="text-danger">*</span>
								    	<a href="##" data-toggle="modal" data-target="##scModal"><img src="/session/sire/images/help-small.png"/></a>
								    </label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
								    </div>
								  </div>
								  <div class="form-group" id="ExpirationDate">
								    <label for="" class="col-sm-4 control-label">Expiration Date:<span class="text-danger">*</span></label>
							        <input type="hidden" id="expiration_date" name="expirationDate" value="">
								    <div class="col-sm-4">
								      <select class="form-control validate[required]" id="expiration_date_month">
								      	<option value="01">January</option>
								      	<option value="02">February</option>
								      	<option value="03">March</option>
								      	<option value="04">April</option>
								      	<option value="05">May</option>
								      	<option value="06">June</option>
								      	<option value="07">July</option>
								      	<option value="08">August</option>
								      	<option value="09">September</option>
								      	<option value="10">October</option>
								      	<option value="11">November</option>
								      	<option value="12">December</option>
								      </select>
								    </div>
								    <div class="col-sm-4">
								    	<select class="form-control validate[required]" id="expiration_date_year">
								    		<cfloop from="#year(now())#" to="#year(now())+50#" index="i">
								    			<option value="#i#">#i#</option>
								    		</cfloop>
								    	</select>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="first_name" class="col-sm-4 control-label" id="card_name_label">Cardholder Name:<span class="text-danger">*</span></label>
								    <div class="col-sm-4">
								      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name" value="">
								    </div>
								    <div class="col-sm-4">
								      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name" value="">
								    </div>
								  </div>
								</div>
			            	</div>
	        				<div class="row heading">
	        					<h3 class="col-sm-12 heading-title">Cardholder Details<hr></h3>
	        				</div>
							<div class="row">
			                	<div class="col-sm-11 form-horizontal">
								  <div class="form-group">
								    <label for="line1" class="col-sm-4 control-label">Address:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="60" id="line1" name="line1">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="city" class="col-sm-4 control-label">City:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="40" id="city" name="city">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="state" class="col-sm-4 control-label">State:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="40" id="state" name="state">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="zip" class="col-sm-4 control-label">Zip Code:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="5" id="zip" name="zip" data-errormessage-custom-error="* Invalid zip code">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="country" class="col-sm-4 control-label">Country:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
										<select class="form-control validate[required]" id="country" name="country">
											<option value="AF">Afghanistan</option>
											<option value="AX">Åland Islands</option>
											<option value="AL">Albania</option>
											<option value="DZ">Algeria</option>
											<option value="AS">American Samoa</option>
											<option value="AD">Andorra</option>
											<option value="AO">Angola</option>
											<option value="AI">Anguilla</option>
											<option value="AQ">Antarctica</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AR">Argentina</option>
											<option value="AM">Armenia</option>
											<option value="AW">Aruba</option>
											<option value="AU">Australia</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BS">Bahamas</option>
											<option value="BH">Bahrain</option>
											<option value="BD">Bangladesh</option>
											<option value="BB">Barbados</option>
											<option value="BY">Belarus</option>
											<option value="BE">Belgium</option>
											<option value="BZ">Belize</option>
											<option value="BJ">Benin</option>
											<option value="BM">Bermuda</option>
											<option value="BT">Bhutan</option>
											<option value="BO">Bolivia, Plurinational State of</option>
											<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BW">Botswana</option>
											<option value="BV">Bouvet Island</option>
											<option value="BR">Brazil</option>
											<option value="IO">British Indian Ocean Territory</option>
											<option value="BN">Brunei Darussalam</option>
											<option value="BG">Bulgaria</option>
											<option value="BF">Burkina Faso</option>
											<option value="BI">Burundi</option>
											<option value="KH">Cambodia</option>
											<option value="CM">Cameroon</option>
											<option value="CA">Canada</option>
											<option value="CV">Cape Verde</option>
											<option value="KY">Cayman Islands</option>
											<option value="CF">Central African Republic</option>
											<option value="TD">Chad</option>
											<option value="CL">Chile</option>
											<option value="CN">China</option>
											<option value="CX">Christmas Island</option>
											<option value="CC">Cocos (Keeling) Islands</option>
											<option value="CO">Colombia</option>
											<option value="KM">Comoros</option>
											<option value="CG">Congo</option>
											<option value="CD">Congo, the Democratic Republic of the</option>
											<option value="CK">Cook Islands</option>
											<option value="CR">Costa Rica</option>
											<option value="CI">Côte d'Ivoire</option>
											<option value="HR">Croatia</option>
											<option value="CU">Cuba</option>
											<option value="CW">Curaçao</option>
											<option value="CY">Cyprus</option>
											<option value="CZ">Czech Republic</option>
											<option value="DK">Denmark</option>
											<option value="DJ">Djibouti</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EG">Egypt</option>
											<option value="SV">El Salvador</option>
											<option value="GQ">Equatorial Guinea</option>
											<option value="ER">Eritrea</option>
											<option value="EE">Estonia</option>
											<option value="ET">Ethiopia</option>
											<option value="FK">Falkland Islands (Malvinas)</option>
											<option value="FO">Faroe Islands</option>
											<option value="FJ">Fiji</option>
											<option value="FI">Finland</option>
											<option value="FR">France</option>
											<option value="GF">French Guiana</option>
											<option value="PF">French Polynesia</option>
											<option value="TF">French Southern Territories</option>
											<option value="GA">Gabon</option>
											<option value="GM">Gambia</option>
											<option value="GE">Georgia</option>
											<option value="DE">Germany</option>
											<option value="GH">Ghana</option>
											<option value="GI">Gibraltar</option>
											<option value="GR">Greece</option>
											<option value="GL">Greenland</option>
											<option value="GD">Grenada</option>
											<option value="GP">Guadeloupe</option>
											<option value="GU">Guam</option>
											<option value="GT">Guatemala</option>
											<option value="GG">Guernsey</option>
											<option value="GN">Guinea</option>
											<option value="GW">Guinea-Bissau</option>
											<option value="GY">Guyana</option>
											<option value="HT">Haiti</option>
											<option value="HM">Heard Island and McDonald Islands</option>
											<option value="VA">Holy See (Vatican City State)</option>
											<option value="HN">Honduras</option>
											<option value="HK">Hong Kong</option>
											<option value="HU">Hungary</option>
											<option value="IS">Iceland</option>
											<option value="IN">India</option>
											<option value="ID">Indonesia</option>
											<option value="IR">Iran, Islamic Republic of</option>
											<option value="IQ">Iraq</option>
											<option value="IE">Ireland</option>
											<option value="IM">Isle of Man</option>
											<option value="IL">Israel</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JP">Japan</option>
											<option value="JE">Jersey</option>
											<option value="JO">Jordan</option>
											<option value="KZ">Kazakhstan</option>
											<option value="KE">Kenya</option>
											<option value="KI">Kiribati</option>
											<option value="KP">Korea, Democratic People's Republic of</option>
											<option value="KR">Korea, Republic of</option>
											<option value="KW">Kuwait</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="LA">Lao People's Democratic Republic</option>
											<option value="LV">Latvia</option>
											<option value="LB">Lebanon</option>
											<option value="LS">Lesotho</option>
											<option value="LR">Liberia</option>
											<option value="LY">Libya</option>
											<option value="LI">Liechtenstein</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="MO">Macao</option>
											<option value="MK">Macedonia, the former Yugoslav Republic of</option>
											<option value="MG">Madagascar</option>
											<option value="MW">Malawi</option>
											<option value="MY">Malaysia</option>
											<option value="MV">Maldives</option>
											<option value="ML">Mali</option>
											<option value="MT">Malta</option>
											<option value="MH">Marshall Islands</option>
											<option value="MQ">Martinique</option>
											<option value="MR">Mauritania</option>
											<option value="MU">Mauritius</option>
											<option value="YT">Mayotte</option>
											<option value="MX">Mexico</option>
											<option value="FM">Micronesia, Federated States of</option>
											<option value="MD">Moldova, Republic of</option>
											<option value="MC">Monaco</option>
											<option value="MN">Mongolia</option>
											<option value="ME">Montenegro</option>
											<option value="MS">Montserrat</option>
											<option value="MA">Morocco</option>
											<option value="MZ">Mozambique</option>
											<option value="MM">Myanmar</option>
											<option value="NA">Namibia</option>
											<option value="NR">Nauru</option>
											<option value="NP">Nepal</option>
											<option value="NL">Netherlands</option>
											<option value="NC">New Caledonia</option>
											<option value="NZ">New Zealand</option>
											<option value="NI">Nicaragua</option>
											<option value="NE">Niger</option>
											<option value="NG">Nigeria</option>
											<option value="NU">Niue</option>
											<option value="NF">Norfolk Island</option>
											<option value="MP">Northern Mariana Islands</option>
											<option value="NO">Norway</option>
											<option value="OM">Oman</option>
											<option value="PK">Pakistan</option>
											<option value="PW">Palau</option>
											<option value="PS">Palestinian Territory, Occupied</option>
											<option value="PA">Panama</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PY">Paraguay</option>
											<option value="PE">Peru</option>
											<option value="PH">Philippines</option>
											<option value="PN">Pitcairn</option>
											<option value="PL">Poland</option>
											<option value="PT">Portugal</option>
											<option value="PR">Puerto Rico</option>
											<option value="QA">Qatar</option>
											<option value="RE">Réunion</option>
											<option value="RO">Romania</option>
											<option value="RU">Russian Federation</option>
											<option value="RW">Rwanda</option>
											<option value="BL">Saint Barthélemy</option>
											<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
											<option value="KN">Saint Kitts and Nevis</option>
											<option value="LC">Saint Lucia</option>
											<option value="MF">Saint Martin (French part)</option>
											<option value="PM">Saint Pierre and Miquelon</option>
											<option value="VC">Saint Vincent and the Grenadines</option>
											<option value="WS">Samoa</option>
											<option value="SM">San Marino</option>
											<option value="ST">Sao Tome and Principe</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SN">Senegal</option>
											<option value="RS">Serbia</option>
											<option value="SC">Seychelles</option>
											<option value="SL">Sierra Leone</option>
											<option value="SG">Singapore</option>
											<option value="SX">Sint Maarten (Dutch part)</option>
											<option value="SK">Slovakia</option>
											<option value="SI">Slovenia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SO">Somalia</option>
											<option value="ZA">South Africa</option>
											<option value="GS">South Georgia and the South Sandwich Islands</option>
											<option value="SS">South Sudan</option>
											<option value="ES">Spain</option>
											<option value="LK">Sri Lanka</option>
											<option value="SD">Sudan</option>
											<option value="SR">Suriname</option>
											<option value="SJ">Svalbard and Jan Mayen</option>
											<option value="SZ">Swaziland</option>
											<option value="SE">Sweden</option>
											<option value="CH">Switzerland</option>
											<option value="SY">Syrian Arab Republic</option>
											<option value="TW">Taiwan, Province of China</option>
											<option value="TJ">Tajikistan</option>
											<option value="TZ">Tanzania, United Republic of</option>
											<option value="TH">Thailand</option>
											<option value="TL">Timor-Leste</option>
											<option value="TG">Togo</option>
											<option value="TK">Tokelau</option>
											<option value="TO">Tonga</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TN">Tunisia</option>
											<option value="TR">Turkey</option>
											<option value="TM">Turkmenistan</option>
											<option value="TC">Turks and Caicos Islands</option>
											<option value="TV">Tuvalu</option>
											<option value="UG">Uganda</option>
											<option value="UA">Ukraine</option>
											<option value="AE">United Arab Emirates</option>
											<option value="GB">United Kingdom</option>
											<option value="US" selected="selected">United States</option>
											<option value="UM">United States Minor Outlying Islands</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VU">Vanuatu</option>
											<option value="VE">Venezuela, Bolivarian Republic of</option>
											<option value="VN">Viet Nam</option>
											<option value="VG">Virgin Islands, British</option>
											<option value="VI">Virgin Islands, U.S.</option>
											<option value="WF">Wallis and Futuna</option>
											<option value="EH">Western Sahara</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="phone" class="col-sm-4 control-label">Telephone:</label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[custom[phone]]" maxlength="30" id="phone" name="phone">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="email" class="col-sm-4 control-label">Email:<span class="text-danger">*</span></label>
								    <div class="col-sm-8">
								      <input type="text" class="form-control validate[required,custom[email]]" maxlength="128" id="email" name="email">
								    </div>
								  </div>
								  <!---
								  <div class="form-group">
									<div class="col-sm-12 checkbox">
									    <label>
									      <input type="checkbox" name='save_cc_info' value='1'>Save Credit Card information for recurring and furture payments.
									    </label>
									</div>
								  </div>
								  --->
							    </div>
			                </div>
						</div>
						</form>
		                <div class="row table-footer">
		            		<div class="col-md-12">
				            	<hr>
		                    	<div class="pull-left"><img src="/session/sire/images/powered-worldpay.png"></div>
		                    	<div class="pull-right worldpay-help">For help with your payment visit: <a target="_blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></div>
		            		</div>
			        	</div>
	            		<br>
		            	<div class="row">
							<div class="col-md-12" style="text-align:right">
								<button type="button" class="btn btn-success btn-success-custom btn-payment-credits">Extend Now</button>
								<a type="button" class="btn btn-pricing btn-medium" href="/" onclick="window.history.back(); return false;" style="margin-left:5%"> &nbsp;  &nbsp; Cancel &nbsp; &nbsp; </a>
							</div>
		            	</div>
					</div>
        		</div>

				<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
					<cfinvokeargument name="path" value="/public/sire/js/extend_plan.js">
				</cfinvoke>
    		</cfcase>
    	</cfswitch>
    	
        <br/>
        <br/>
    
	</section>

</div>
</cfoutput>

<!-- Modal -->
<div class="modal fade" id="scModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Security Code</h4>
      </div>
      <div class="modal-body text-center">
			<img src="/session/sire/images/securecode.gif"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">       		
        	</p>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<button class="btn btn-pricing btn-medium pull-right" data-dismiss="modal" aria-label="Close" style="height: 38px; min-width: auto; text-transform: none"> &nbsp;  &nbsp; OK &nbsp; &nbsp; </button>
        	</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="processingPayment">
    <img src="/public/sire/images/loading.gif" class="ajax-loader"/>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/style.css">
</cfinvoke>
<cfparam name="variables._title" default="Extend plan - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">

