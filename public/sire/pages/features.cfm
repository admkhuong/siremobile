<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>
    <header class="sire-banner2 featured-banner header-banners">
        
		<section class="landing-top">
            <div class="container-fluid">
                <div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
                </div>
            </div>      
        </section>
			
		<section class="landing-jumbo landing-jumbo-feature">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
                        <!--- <h1 class="banner-title-big hidden-xs">the easiest, most efficient way to</h1>
                        <h1 class="banner-title-big hidden-xs">communicate to your customers</h1>

                        <h3 class="banner-title-small visible-xs">the easiest, most efficient way to</h3>
                        <h3 class="banner-title-small visible-xs">communicate to your customers</h3> --->
                        <h1 class="sire-title-updated">THE EASIEST, MOST EFFICIENT WAY TO<br class="hidden-xs"> COMMUNICATE WITH YOUR CUSTOMERS</h1>
                        <h1 class="sire-title-updated hide">TO COMMUNICATE WITH YOUR CUSTOMERS</h1>

                        <p class="banner-sub-title"><i>Launch your free campaign in 5 minutes</i></p>

                        <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                        <cfelse>
                            <a href="signup" class="get-started mt-20">get started</a>
                        </cfif>
                        

						<!---<p class="landing_title_med">SMS and Text Marketing: Relevant, Personalized, and Delivered in Context. </p>--->
                        <!---<p class="sms_marketing">Drive your own mobile customer engagements today!</p>--->
                        
                        <!--- <h1 class="marketing_title_big hidden-xs">
                            THE MOST EASIEST AND EFFECTIVE WAY
                        </h1>

                        <h1 class="marketing_title_big hidden-xs">
                            TO COMMUNICATE WITH YOUR CUSTOMERS
                        </h1>

                        <h4 class="marketing_title visible-xs">
                            THE MOST EASIEST AND EFFECTIVE WAY
                        </h4>

                        <h4 class="marketing_title visible-xs">
                            TO COMMUNICATE WITH YOUR CUSTOMERS
                        </h4>
						                        
                        <h4 class="hidden-xs"><i>Launch your first campaign within 5 minutes!</i></h4> --->
					</div>	
				</div>	
                
                <!--- <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div> --->
                
                <!--- <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div> --->
			</div>	
		</section>
	</header>
	
    <section class="home-block-3">
        <div class="container-fluid">
            <div class="inner-home-block-3 inner-features-block-3">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Text Marketing shouldn’t be complicated.<br>Growing your business should be easy and fun.</h4>
                        <p>
                            Sire was built by small business owners like you. Our easy-to-use templates will have <br class="hidden-xs"> you launching your first campaign in minutes!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>        
     

	<section class="main-wrapper feature-main" id="feature-main">
		<div class="feature-gray">
			<div class="container">
				<div class="row feature-items">
                    <div class="col-sm-4">                    
                        <div class="wrap-feature-item">
                             <!---feature-demo1.png templates-img.png --->
                             <div class="text-center hold-img">
                                <img class="feature-img" alt="" src="/public/sire/images/features/1.png">
                             </div>
                            <div class="abs-bottom-left hidden">
                                <div class="image-wrapper">
                                  <span class="image-overlay overlay-grey">
                                    <span class="content">Guided entry into SMS and Mobile App Marketing</span>
                                  </span>
                                  <img class="" alt="" src="/public/sire/images/ipadtraining.jpg">
                                </div>
                            </div>                        
                        </div>
					</div>
                    
					<div class="col-sm-8">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                
                                <div class="newfeature-title">Templates</div>
                                <div class="newfeature-content">
                                    
                                    <p class="hasBold">                         
                                        Our template make it easy for you to launch your campaign
                                    </p>
                                    
                                    <ul class="new-list list-mb">
                                        <li>Simple, ready-to-use customizable templates</li>
                                        <li>Free mobile landing page creator</li>
                                        <li>Just select a template, choose a keyword, and personalize your message!</li>
                                    </ul>
                                        
                                </div>
                            </div>
                        </div>    						
					</div>
				</div>
			</div>
		</div>

        
		<div class="feature-white">
			<div class="container">
			 	<div class="row feature-items">  
                    <div class="col-sm-4 col-sm-push-8">
                        <div class="wrap-feature-item">
                            <div class="text-center hold-img">
                                <div class="image">
                                    <img class="feature-img" alt="" src="/public/sire/images/features/2.png">
                                    <h2><cfoutput>
                                        <cfif len(shortcode) EQ 5>
                                            #left(shortcode,3)#-#right(shortcode,2)#
                                        <cfelse>
                                            #shortcode#
                                        </cfif>
                                    </cfoutput></h2>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="col-sm-8 col-sm-pull-4">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                <span class="anchor" id="feature1"></span>
                                <div class="newfeature-title">Intelligent Conversational Interface (ICI)</div>
                                <div class="newfeature-content">
                                    
                                    <p class="hasBold">                         
                                        Artificial Intelligence harnessed in a SMB platform
                                    </p>
                                    
                                    <ul class="new-list list-mb">
                                        <li>Save valuable time with AI automation.</li>
                                        <li>ICI creates a more pleasurable customer experience.</li>
                                        <li>Intelligent SMS creates cleaner survey data sets.</li>
                                    </ul>                            
                                </div>
                            </div>
                        </div>    						
					</div>				
				</div>                
			</div>
		</div>

        
		<div class="feature-gray">
			<div class="container">
			 	<div class="row feature-items">
					<div class="col-sm-4">
						<div class="wrap-feature-item">
                            <div class="text-center hold-img">
							     <img class="feature-img" alt="" src="/public/sire/images/features/3.png">
                            </div>
						</div>
					</div>
					<div class="col-sm-8">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                <span class="anchor" id="feature2"></span>
                                <div class="newfeature-title">Campaign Management</div>
                                <div class="newfeature-content">
                                    <p class="hasBold">Easily track and manage all of your campaigns on a single dashboard</p>

                                    <ul class="new-list list-mb">
                                        <li>A user friendly campaign management dashboard</li>
                                        <li>Simple keyword organization</li>
                                        <li>Generate reports with a single click of a button</li>
                                    </ul>    
                                    
                                     
                               <!---      
                               
                               At Sire, we provide mobile marketing tools and solutions that are designed for you to spend less time coding and more time finding creative ways to serve your customers.
                               
                               
                                    <p>While we certainly can do Keywords and Blasts with unprecedented ease, Sire also has a whole range of additional Interactive Campaign templates at your fingertips.</p>
                                    <p>                            
                                        <ul>
                                            <li>
                                                <p><b>Drip Marketing Campaigns</b></p>
                                                <p>Strategically "drips" a pre-scripted set of messages to customers or prospects over time as a way to up-sell and cross-promote.</p>                                    
                                            </li>
                                            
                                            <li>
                                                <p><b>Multi-Factor Authentication</b></p>
                                                <p>One Time verification code uses information sent to a phone via SMS, Push or Voice for use as part of the login process.</p>                                    
                                            </li>
                                                                                                                    
                                            <li>
                                                <p><b>Power of Customer Experience Management</b></p>
                                                <p>Tailor your messaging strategies to focus on the needs of individual customers.</p>                                    
                                            </li>
                                            
                                            <li>
                                                <p><b>Custom Data Fields</b></p>
                                                <p>Our solutions are specifically designed to capitalize on the unique capabilities of your business, to support your way of doing what you do.</p>                                    
                                            </li>
                                            
                                            <li>
                                                <p><b>Express Messaging Services</b></p>
                                                <p>Fast, Simple, Easy. Sire makes even the basic messaging stuff easier.</p>                                    
                                            </li>
                                                                              
                                        </ul>                            
                                    </p>--->
                                
                                </div>
                            </div>
                        </div>    						
					</div>
				</div>
			</div>
		</div>

        
		<div class="feature-white">
			<div class="container">
			 	<div class="row feature-items">
                    <div class="col-sm-4 col-sm-push-8">
                        <div class="wrap-feature-item">
                            <div class="text-center hold-img">
                                <img class="feature-img" alt="" src="/public/sire/images/features/4.png">
                            </div>
                            
                            <div class="abs-bottom-left hidden">
                                <div class="image-wrapper">
                                  <span class="image-overlay overlay-grey">
                                    <span class="content">Track your success in real time.</span>
                                  </span>
                                  <img class="" alt="" src="/public/sire/images/tracksuccess.jpg">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8 col-sm-pull-4">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                <span class="anchor" id="feature3"></span>
                                <div class="newfeature-title" >Subscribers</div>
                                <div class="newfeature-content">
                                <p class="hasBold">Easy contact management</p>
                                
                                <ul class="new-list list-mb">
                                    <li>Time saving contact management dashboard</li>
                                    <li>Automated subscriber list management</li>
                                    <li>Segments users, subscribers, and nonsubscribers</li>
                                </ul>
                                
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>

       
        <div class="feature-gray last-feature-gray">
            <div class="container">
                <div class="row feature-items">
                
                    <div class="col-sm-4">    
                        <div class="wrap-feature-item">
                            <div class="text-center hold-img">
                                <img class="feature-img" alt="" src="/public/sire/images/features/5.png">
                            </div>
                        </div>                                            
                    </div>
                    
                    <div class="col-sm-8">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                <span class="anchor" id="feature4"></span>
                                <div class="newfeature-title" >Reporting</div>
                                <div class="newfeature-content">
                                    
                                    <p class="hasBold">                         
                                        Track the success of your campaigns
                                    </p>

                                    <ul class="new-list list-mb">
                                        <li>Watch your growth in real time</li>
                                        <li>Monitor message deliverability, subscriber grown and engagement</li>
                                        <li>Interaction history and account activity in one place</li>
                                    </ul>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="feature-white">
            <div class="container">
                <div class="row feature-items">
                    <div class="col-sm-4 col-sm-push-8">
                        <div class="wrap-feature-item">
                            <div class="text-center hold-img">
                                <img class="feature-img" alt="" src="/public/sire/images/features/6.png">
                            </div>
                            
                            <div class="abs-bottom-left hidden">
                                <div class="image-wrapper">
                                  <span class="image-overlay overlay-grey">
                                    <span class="content">Track your success in real time.</span>
                                  </span>
                                  <img class="" alt="" src="/public/sire/images/tracksuccess.jpg">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8 col-sm-pull-4">
                        <div class="wrap-feature-item feature-item-info">
                            <div class="inner-info">
                                <span class="anchor" id="feature3"></span>
                                <div class="newfeature-title" >Advanced Tools</div>
                                <div class="newfeature-content">
                                <p class="hasBold">Geek out with Sire’s Advanced Tools</p>
                                
                                <ul class="new-list list-mb">
                                    <li>Create reports with Advanced Level Reporting and Analytics</li>
                                    <li>Shorten URL links with Sire’s URL administrator</li>
                                    <li>Test your personal campaign using Sire’s QA Tool</li>
                                </ul>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>

    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
    <cfelse>
        <section class="home-block-3 feature-gray">
            <div class="container">
                <div class="inner-home-block-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Get Your Free Account Today</h4>
                            <p>
                                <i>
                                    <span class="block-on-mobile">No Credit Card Required</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">Free Upgrade or Downgrade</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">No Hidden Fees</span>
                                </i>
                            </p>
                            <a href="/signup" class="get-started">start now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </cfif>
   
	
<cfparam name="variables._title" default="SMS Marketing Features">
<cfparam name="variables.meta_description" default="Text marketing shouldn’t be complicated.  Launch your first SMS campaign in 5 minutes using our simple templates, Artificial Intelligence integration, detailed reporting.">


<cfinclude template="../views/layouts/plan_pricing.cfm">
<script type="text/javascript">
    $('.li-features').addClass('active');
</script>

<style type="text/css">
    .image { 
        position: relative;
    }

    .image > h2 { 
        position: absolute; 
        top: 118px; 
        left: 0px;
        right: 10%;
        margin-left: auto;
        margin-right: auto;
        font-size: 11px;
        color: rgb(80,80,80);
        font-weight: bold;
    }

    @media screen and (max-width: 1199px) {
        .image > h2 { 
            position: absolute; 
            top: 105px; 
            left: 0px;
            right: 0%;
            margin-left: auto;
            margin-right: auto;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 991px) {
        .image > h2 { 
            position: absolute; 
            top: 74px; 
            left: 0px;
            right: 0%;
            margin-left: auto;
            margin-right: auto;
            font-size: 8px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 767px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 0px;
            right: 0%;
            margin-left: auto;
            margin-right: auto;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

/*    

    @media screen and (max-width: 991px) {
        .image > h2 { 
            position: absolute; 
            top: 74px; 
            left: 99px;
            font-size: 8px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 767px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 47.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 650px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 47%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 640px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 46.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }


    @media screen and (max-width: 450px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 45.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-width: 362px) {
        .image > h2 { 
            position: absolute; 
            top: 36.5%; 
            left: 44.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media only screen and (max-device-width: 736px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 47.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media only screen and (max-device-width: 667px) {
        .image > h2 { 
            position: absolute; 
            top: 37%; 
            left: 47%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-device-width: 568px) {
        .image > h2 { 
            position: absolute; 
            top: 21vw; 
            left: 47%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-device-width: 414px) {
        .image > h2 { 
            position: absolute; 
            top: 28.5vw; 
            left: 45.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-device-width: 412px) {
        .image > h2 { 
            position: absolute; 
            top: 29vw; 
            left: 45.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-device-width: 375px) {
        .image > h2 { 
            position: absolute; 
            top: 31.5vw; 
            left: 45%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media screen and (max-device-width: 360px) {
        .image > h2 { 
            position: absolute; 
            top: 33vw; 
            left: 45%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }

    @media only screen and (max-device-width: 320px) {
        .image > h2 { 
            position: absolute; 
            top: 32vw; 
            left: 44.5%;
            font-size: 11px;
            color: rgb(80,80,80);
            font-weight: bold;
        }
    }*/
</style>
