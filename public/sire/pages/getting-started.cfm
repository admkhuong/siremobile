
<link href="/session/sire/css/style.css" rel="stylesheet"> 
<style>
	
	body
	{
		background-color:#FFFFFF;
		color:#333;	
	}

</style>

	<header class="home header-featured clearfix">
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-xs-5">
						<div class="landing-logo">
							<a href="/">
								<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
							</a>
						</div>
					</div>	

					
							<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
								<div class="col-sm-9 col-xs-5">
									<div class="login-wrapper">
										<cfinclude template="../views/commons/menu.cfm">
									</div>	
								</div>	
							<cfelse>
								<div class="col-sm-9 col-xs-7">
									<div class="login-wrapper">
										<cfinclude template="../views/commons/menu_public.cfm">
									</div>
								</div>	
							</cfif>
				</div>
			</div>		
		</section>
			
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						<!---<p class="landing_title_med">SMS and Text Marketing: Relevant, Personalized, and Delivered in Context. </p>--->
                        <!---<p class="sms_marketing">Drive your own mobile customer engagements today!</p>--->
                        
                        <h1 class="marketing_title_big">Getting Started</h1>
                        						                        
                        <h4 class="hidden-xs"><i>Drive your business with mobile <span class="hidden-xs">customer</span> engagements</i></h4>
					</div>	
				</div>	
                
                <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div>
                
                <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div>
        
        
        
			</div>	
		</section>
	</header>
	
        
     

	<section class="main-wrapper feature-main" id="feature-main">
    	
        <div class="container-fluid text-center">
               
            <div class="row">
                <p class="col-lg-8 col-sm-8 col-xs-8">
                   <h2 class="heading-home">SMS/Text is still the most frequently used app on a smartphone with 97% of owners using it at least once a week or more.</h3><h5><a href="http://www.pewinternet.org/2015/04/01/us-smartphone-use-in-2015/" target="_blank">Pew Research Center (2015)</a></h2>
                </p>
            </div>                    
        
        </div>
        
		<div class="feature-gray">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-6">
						<div class="feature-title" id="feature1">Create a Campaign</div>
						<div class="feature-content">
                        	                       
                            <p>
                               Businesses operating in highly competitive markets may find themselves initiating frequent marketing <b>campaigns</b> and devoting significant resources to generating brand awareness and sales. 
                               Marketing campaigns can be designed with different ends in mind, including building a brand image, introducing a new product, increasing sales of a product already on the market, or even reducing the impact of negative news.
                            </p>		
                                          
						</div>
					</div>
                                        
                    
					<div class="col-md-6">
						<div class="feature-content" style="text-align:center;">
                        	                            
                            <p><b>Easy to create</b></p>
                            <p class="btn-line-height">After logging in click on <br/><button class="btn btn-larger btn-success-custom" id="btn-popup-campaign" style="margin-top:.4em !important;">Create campaign</button></p>
                                
						</div>
					</div>
                    
				</div>
			</div>
		</div>
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature2">Select a Template</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Templates</b> are pre-defined SMS campaigns to give you a jump start on many different tasks.                         
                            </p>
                            
                            <p>Sire assists you with easy to customize design templates that create the campaigns that will delight your customers.</p>		
                        	
                            <p>Yes, monetary incentives typically do better at driving higher amounts of users into your program but it’s not mandatory. Not everybody want’s to offer discounts and many are successful without doing so. It’s up to you.</p>
                          
                          	<br/>
                            
                            <p>
                            	
                                <ul>
                                	<li>
                                    	<p>Provide Useful Alerts and Reminders</p>                                                                          
                                    </li>
                                    
                                    <li>
                                    	<p>Provide Enhanced Access and Privileges</p>                                                                          
                                    </li>   
                                    
                                    <li>
                                    	<p>Capture Feedback and Engage your Customers</p>                                                                          
                                    </li>
                                                                	                                    
                                     <li>
                                        <p>Track and Capture Opt In, Double Opt In, Opt Out, Opt Down</p>                                                                          
                                    </li>      
                                                                                                     
                                </ul>
                            
                            </p>
                        
                        </div>
					</div>
					
                    <div class="col-md-6">
						<div class="feature-content" style="text-align:center;">
                        	                            
                            <p><b>Easy to choose</b></p>
                            <p class="btn-line-height">For this sample Select the "Build SMS List" <b>Template</b></p>
                                
                                
                            <div style="text-align:center;">                                        
                                <div class="TPButton" style="">
                
                                    <img class="" src="/public/sire/images/template-1-image.png" style="">
                
                                    <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Build SMS List</h3>
                
                                </div>
                            </div>
                        
						</div>
					</div>
                    
                    
                    
				</div>
                
			</div>
		</div>
		<div class="feature-gray">
			<div class="container">
			 	<div class="row">
				
                 <div class="col-md-6">
						<div class="feature-title" id="feature1">Branding and Style a Campaign</div>
						<div class="feature-content">
                        	                       
                            <p>
                               Add your Brand Name. Encourage them to Join.
                            </p>		
                                          
						</div>
					</div>
                                        
                    
					<div class="col-md-6">
						<div class="feature-content" style="text-align:center;">
                        	                            
    	                     <p><b>Easy to Standout</b></p>
	                         <p></p>
                             
                             
                             <div class="content-body">
				<div class="form-group clearfix">
					<div class="col-sm-3 col-md-3 col-lg-2">
            			<label for="campaign_name">Campaign Name:*</label>
        			</div>
        			<div class="col-sm-9 col-md-9 col-lg-10">
        				<input value="Build SMS List" class="form-control validate[required]" id="campaign_name" type="text" maxlength="255">
    				</div>
    			</div>
				<div class="form-group clearfix">
			
					<div class="col-sm-2 col-md-2 col-lg-2">
            			<label for="EMS_Flag_0">
            				<input type="radio" id="EMS_Flag_0" name="EMS_Flag_int" value="0" checked="checked">
            				Keyword
        				</label>
        			</div>
        			<div class="col-sm-7 col-md-7 col-lg-8 cbic">
        				<div class="keyword-group">
        					<input value="39492" id="shortCode" type="hidden" name="shortCode">
        					<input value="941" id="keywordId" type="hidden" name="keywordId">
    						<input value="JOIN" id="KeywordVch" type="hidden" name="Keyword_vch">
        					<input value="JOIN" class="form-control" id="txtKeyword" type="text" name="txtKeyword" maxlength="160" disabled="">
    						
    					</div>
    				</div>
    				<div class="col-sm-12 cbic">
    					
						<hr class="hrt0 hrb10">
					</div>
										
    			</div>
    			
    			<div class="row mb15">
    			<div class="col-md-12">
    			
    				<div class="form-group clearfix control-point" id="cp1">
    					<div class="col-sm-12">
    						<div class="control-point-border clearfix">
    							<div class="col-sm-12">
    								<label class="control-point-label"># 1. First message to the end user. This question is to verify their request to join the list.  </label>
    								<button type="button" class="btn btn-link pull-right control-point-btn-edit" data-control-point-id="1" data-control-point-type="SHORTANSWER" data-control-point-af="NOFORMAT" data-control-point-oig="0" data-control-point-scdf="" data-control-point-one-selection-no-branch="0">
    									<img src="/session/sire/images/icon/icon_edit_small.png">
									</button>
    							</div>
    							<div class="clearfix"></div>
	    						<hr class="hr0">
    							<div class="col-sm-12 clearfix">
    								<div class="control-point-body">
    									
					    				
					    						<div class="control-point-text">GS Alert: You've chosen to receive SMART Project msgs.Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&amp;Data rates may apply</div>
					    					
					    				<h5 class="text-right control-point-char">
					    					
					    						6 characters available
					    					
				    					</h5>
			    					</div>
			    				</div>
    						</div>
    					</div>
    				</div>
    			
    				<div class="form-group clearfix control-point" id="cp3">
    					<div class="col-sm-12">
    						<div class="control-point-border clearfix">
    							<div class="col-sm-12">
    								<label class="control-point-label"># 2. This is the invalid response message. Adjust or leave blank to skip it. This template will then repeat the first question up to 3 times.  </label>
    								<button type="button" class="btn btn-link pull-right control-point-btn-edit" data-control-point-id="3" data-control-point-type="STATEMENT" data-control-point-af="NOFORMAT" data-control-point-oig="0" data-control-point-scdf="" data-control-point-one-selection-no-branch="0">
    									<img src="/session/sire/images/icon/icon_edit_small.png">
									</button>
    							</div>
    							<div class="clearfix"></div>
	    						<hr class="hr0">
    							<div class="col-sm-12 clearfix">
    								<div class="control-point-body">
    									
					    				
					    						<div class="control-point-text">GS Alert: You have sent an invalid response. Please call us at 999-999-9999 for assistance. </div>
					    					
					    				<h5 class="text-right control-point-char">
					    					
					    						68 characters available
					    					
				    					</h5>
			    					</div>
			    				</div>
    						</div>
    					</div>
    				</div>
    			
    				<div class="form-group clearfix control-point" id="cp5">
    					<div class="col-sm-12">
    						<div class="control-point-border clearfix">
    							<div class="col-sm-12">
    								<label class="control-point-label"># 3. Confirmation of the user's acceptance. You will need to choose which list to store the registered users in to.  </label>
    								<button type="button" class="btn btn-link pull-right control-point-btn-edit" data-control-point-id="5" data-control-point-type="OPTIN" data-control-point-af="NOFORMAT" data-control-point-oig="0" data-control-point-scdf="" data-control-point-one-selection-no-branch="0">
    									<img src="/session/sire/images/icon/icon_edit_small.png">
									</button>
    							</div>
    							<div class="clearfix"></div>
	    						<hr class="hr0">
    							<div class="col-sm-12 clearfix">
    								<div class="control-point-body">
    									
					    				
					    						<div class="control-point-optin">
					    							<div class="control-point-text">GS Alert: Registration for SMART Project text alerts is complete. Reply HELP for help or STOP to cancel.</div>
					    							<br><label>Subscriber list:</label> 
					    							<span class="cp-oig-text">
					    								N/A
				    								</span>
				    							</div>
				    						
					    				<h5 class="text-right control-point-char">
					    					
					    						56 characters available
					    					
				    					</h5>
			    					</div>
			    				</div>
    						</div>
    					</div>
    				</div>
    			
    			</div>
    			<div class="col-md-6 text-center clearfix">
					
		    				
    			
				</div>	
	    	</div>
            
            
                             
                                
						</div>
					</div>
                    
                
                	
                </div>
			</div>
		</div>
		<div class="feature-white">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature4">Reporting</div>
						<div class="feature-content">
                        <p><b>See more people come through the door</b></p>
                        
                        <p>SIRE’s reporting tools allow users to obtain information and insights on message deliverability and engagement levels.  Advanced reporting tools provide up-to-date information on account activity at all times. Users receive information in real-time allowing you to gauge how well things are going.</p>
                        <p>Customers love getting your special offers, event invites, and product updates. See who’s engaged with real-time reports.</p>
                        
                        </div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="/public/sire/images/feature-demo4.png">
                        
                        <div class="abs-bottom-left">
                            <div class="image-wrapper">
                              <span class="image-overlay overlay-grey">
                                <span class="content">Track your success in real time.</span>
                              </span>
                              <img class="" alt="" src="/public/sire/images/tracksuccess.jpg">
                            </div>
                        </div>
                        
					</div>
				</div>
			</div>
		</div>
	</section>
   
	
<cfparam name="variables._title" default="Features - Sire">
<cfinclude template="../views/layouts/plan_pricing.cfm">
