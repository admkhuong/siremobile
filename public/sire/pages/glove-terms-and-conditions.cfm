<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1>
    <cfinclude template="../views/commons/fixed-signup.cfm">
</cfif>

<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<div class="static-wrapper">
      <div class="container">
         
      <section class="static-page-container">
            
            <!--- <h1>Terms and Conditions</h1> --->
            <div class="AlignCenter static-page-title">Terms and Conditions</div>
            <div class="last-update AlignCenter">Last updated: January 8, 2016</div>
            <div class="term-content">
            <p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the siremobile.com website and the Sire App mobile application (together, or individually, the "Service") operated by Sire Investments, LLC (“Sire Investments, LLC” "us", "we", or "our").</p>
            <p><strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</strong></p>
                     
            <p><strong>From Your Mobile Device</strong></p>
      <!---       <p>Text <strong>JOIN</strong> to <cfoutput>#shortcode#</cfoutput> to receive standard rate SMS text on/for Sire Interactive Campaigns. Msg freq up to 5 msg/mo. <p> --->            

            <p>Text <strong>REG</strong> to <cfoutput>39492</cfoutput> to receive SMS alerts on their mobile device for the purposes of providing notices and or alerts regarding service use, promotional and related account information Msg freq up to 5 survey msg/mo.</p>
                      
            <p>Text <strong>REGTX</strong> to <cfoutput>39492</cfoutput> to receive SMS account alerts on their mobile device for the purposes of providing notices and or alerts regarding service use, promotional and related account information Msg frequency varies.</p>
                      
                        <p>Text <strong>REG3</strong> to <cfoutput>39492</cfoutput> to receive SMS promo and info alerts on their mobile device for the purposes of providing notices and or alerts regarding service use, promotional and related account information Msg frequency varies.</p>
                      
                        <p>Text <strong>24LS</strong> to <cfoutput>39492</cfoutput> to receive standard rate meet up group alerts. Daily Messages.</p>

            <p>Text <strong>STOP</strong> to <cfoutput>39492</cfoutput> to opt out.</p>
            
            <p>Text <strong>HELP</strong> to <cfoutput>39492</cfoutput> for support or call 1-888-335-0909, or email support@siremobile.com. Msg&amp;Data Rates May Apply.</p>
            
            
            <p><strong>URL for Customer Info:</strong>    siremobile.com</p>
                  <p><strong>Customer Care E-Mail:</strong>     support@siremobile.com</p>
                  <p><strong>Toll Free Customer Care Number:</strong> 888-335-0909</p>



            <p>Mobile carriers are not responsible for lost or undelivered messaging. Participating Carriers include: Ntelos, Cellcom, Cellsouth, Carolina West, AT&T, MetroPCS, T-Mobile, U.S. Cellular, Sprint, Google Voice, Boost, Virgin Mobile and Verizon Wireless. </p>

            <p>Terms &amp; Conditions located at <a href="/term-of-use">https://siremobile.com/public/sire/pages/term-of-use</a></p>
            <p>Privacy Policy can be viewed at <a href="/privacy-policy">https://siremobile.com/public/sire/pages/privacy-policy</a>.</p>
                        
                        <p>Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms.  These Terms apply to all visitors, users and others who wish to access or use the Service.</p>
                        
                        <p>The SIRE "Services" are provided subject to these Website and Product <a href="/term-of-use">Terms and Conditions of Use</a>, as they may be amended by us, and any guidelines, rules or operating policies that we may post on this website, including, without limitation, our <a href="/anti-spam">Anti-Spam Policy</a> and our <a href="/privacy-policy">Privacy Policy</a>, which are specifically incorporated herein by reference (collectively, the "Agreement"). </p>

            <p><strong><span>Communications</span></strong></p>

            <p>By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.</p>

            <p>Sire is not responsible for lost or undelivered messaging. Many factors can influence whether your message will be successfully delivered or received. These factors can include but are not limited to: Poor Signal, Network congestion, Carrier Outage, Low Battery, Device out of service range, Device turned off, Plan blockages and other factors out of Sires control and for which Sire expressly disclaims any liability for.</p>

            <p><strong><span>Purchases</span></strong></p>

            <p>If you wish to purchase any product or service made available through the Service (&quot;Purchase&quot;), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information.</p>

            <p>You represent and warrant that: (i) you have the legal right to
            use any credit card(s) or other payment method(s) in connection with any
            Purchase; and that (ii) the information you supply to us is true, correct and
            complete.</p>

            <p>The service may employ the use of third party services for the
            purpose of facilitating payment and the completion of Purchases. By submitting
            your information, you grant us the right to provide the information to these
            third parties subject to our Privacy Policy.</p>

            <p>We reserve the right to refuse or cancel your order at any time
            for reasons including but not limited to: product or service availability,
            errors in the description or price of the product or service, error in your
            order or other reasons.</p>

            <p>We reserve the right to refuse or cancel your order if fraud or
            an unauthorized or illegal transaction is suspected.</p>

            <p>The White Glove service does not guarantee execution of the specific program or campaign being offered. Sire will facilitate the creation and management of the programs or campaigns but will not guarantee the execution or delivery of the service.</p>

            <p><strong><span>Availability, Errors and Inaccuracies</span></strong></p>

            <p>We are constantly updating product and service offerings on the Service. We may experience delays in updating information on the Service and in our advertising on other web sites. The information found on the Service may contain errors or inaccuracies and may not be complete or current. Products or services may be mispriced, described inaccurately, or unavailable on the Service and we cannot guarantee the accuracy or completeness of any information found on the Service.</p>

            <p>We therefore reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.</p>

            <p><strong><span>Contests, Sweepstakes and Promotions</span></strong></p>

            <p>Any contests, sweepstakes or other promotions (collectively,
            &quot;Promotions&quot;) made available through the Service may be governed by
            rules that are separate from these Terms &amp; Conditions. If you participate
            in any Promotions, please review the applicable rules as well as our Privacy
            Policy. If the rules for a Promotion conflict with these Terms and Conditions, the
            Promotion rules will apply.</p>

            <p><strong><span>Subscriptions</span></strong></p>

            <p>Some parts of the Service are billed on a subscription basis
            (&quot;Subscription(s)&quot;). You will be billed in advance on a recurring and
            periodic basis (&quot;Billing Cycle&quot;). Billing cycles are set either on a
            monthly or annual basis, depending on the type of subscription plan you select
            when purchasing a Subscription.</p>

            <p>At the end of each Billing Cycle, your Subscription will automatically renew for the same time period as the original Subscription under the exact same terms and conditions unless you cancel it or Sire Mobile cancels it. You may cancel your Subscription renewal either through your online account management page or by contacting Sire Mobile customer support team.</p>

            <p>A valid payment method, including credit card or PayPal, is required to process the payment for your Subscription. You shall provide Sire Mobile with accurate and complete billing information including full name, address, state, zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize Sire Mobile to charge all Subscription fees incurred through your account to any such payment instruments.</p>

            <p>Should automatic billing fail to occur for any reason, Sire Mobile will issue an electronic invoice indicating that you must proceed manually, within a certain deadline date, with the full payment corresponding to the billing period as indicated on the invoice.</p>

            <p><strong><span>Free Trial</span></strong></p>

            <p>Sire Mobile may, at its sole discretion, offer a Subscription with a free trial for a limited period of time (&quot;Free Trial&quot;).</p>

            <p>You may be required to enter your billing information in order to sign up for the Free Trial .</p>

            <p>If you do enter your billing information when signing up for the Free Trial, you will not be charged by Sire Mobile until the Free Trial has expired. On the last day of the Free Trial period, unless you cancelled your Subscription, you will be automatically charged the applicable Subscription fees for the type of Subscription you have selected.</p>

            <p>At any time and without notice, Sire Mobile reserves the right to (i) modify the terms and conditions of the Free Trial offer, or (ii) cancel such Free Trial offer.</p>

            <p><strong><span>Fee Changes</span></strong></p>

            <p>Sire Mobile, in its sole discretion and at any time, may modify the Subscription fees for the Subscriptions. Any Subscription fee change will become effective at the end of the then-current Billing Cycle.</p>

            <p>Sire Mobile will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective.</p>

            <p>Your continued use of the Service after the Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount.</p>

            <p><strong><span>Refunds</span></strong></p>

            <p>Subscription fees are non-refundable.</p>

            <p><strong><span>Content</span></strong></p>

            <p>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material
            (&quot;Content&quot;). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness.</p>

            <p>By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright.</p>

            <p>You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service. You agree that this license includes the right for us to make your Content available to other users of the Service, who may also use your Content subject to these Terms.</p>

            <p>Sire Mobile has the right but not the obligation to monitor and edit all Content provided by users.</p>

            <p>In addition, Content found on or through this Service are the property of Sire Mobile or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us.</p>

            <p><strong><span>Accounts</span></strong></p>

            <p>When you create an account with us, you represent and warrant that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.</p>

            <p>You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>

            <p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.</p>

            <p>We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in our sole discretion.</p>

            <p><strong><span>Copyright Policy</span></strong></p>

            <p>We respect the intellectual property rights of others. It is our
            policy to respond to any claim that Content posted on the Service infringes on
            the copyright or other intellectual property rights (&quot;Infringement&quot;)
            of any person or entity.</p>

            <p>If you are a copyright owner, or authorized on behalf of one,
            and you believe that the copyrighted work has been copied in a way that
            constitutes copyright infringement, please submit your claim via email to support@siremobile.com,
            with the subject line: &quot;Copyright Infringement&quot; and include in your
            claim a detailed description of the alleged Infringement as detailed below,
            under&nbsp;<strong>&quot;DMCA Notice and Procedure for Copyright Infringement
            Claims&quot;</strong></p>

            <p>You may be held accountable for damages (including costs and
            attorneys' fees) for misrepresentation or bad-faith claims on the infringement
            of any Content found on and/or through the Service on your copyright.</p>

            <p><strong><span>DMCA Notice and Procedure for Copyright Infringement Claims</span></strong></p>

            <p>You may submit a notification pursuant to the Digital Millennium
            Copyright Act (DMCA) by providing our Copyright Agent with the following
            information in writing (see 17 U.S.C 512(c)(3) for further detail):</p>

            <ul type=disc>
             <li><span>an electronic or
                 physical signature of the person authorized to act on behalf of the owner
                 of the copyright's interest;</span></li>
             <li><span>a description of
                 the copyrighted work that you claim has been infringed, including the URL
                 (i.e., web page address) of the location where the copyrighted work exists
                 or a copy of the copyrighted work;</span></li>
             <li><span>identification
                 of the URL or other specific location on the Service where the material
                 that you claim is infringing is located;</span></li>
             <li><span>your address,
                 telephone number, and email address;</span></li>
             <li><span>a statement by
                 you that you have a good faith belief that the disputed use is not
                 authorized by the copyright owner, its agent, or the law;</span></li>
             <li><span>a statement by
                 you, made under penalty of perjury, that the above information in your
                 notice is accurate and that you are the copyright owner or authorized to
                 act on the copyright owner's behalf.</span></li>
            </ul>

            <p>You can contact our Copyright Agent via email at
            support@siremobile.com</p>

            <p><strong><span>Intellectual Property</span></strong></p>

            <p>The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of Sire Investments, LLC and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Sire Investments, LLC.</p>

            <p><strong><span>Links To Other Web Sites</span></strong></p>

            <p>Our Service may contain links to third party web sites or services that are not owned or controlled by Sire Mobile.Our Service may contain links to third party web sites or services that are not owned or controlled by Sire Mobile.</p>

            <p>Sire Mobile has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.</p>

            <p>You acknowledge and agree that Sire Mobile shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.</p>

            <p>We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.</p>

            <p><strong><span>Termination</span></strong></p>

            <p>We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.</p>

            <p>If you wish to terminate your account, you may simply discontinue using the Service.</p>

            <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

            <p><strong><span>Indemnification</span></strong></p>

            <p>You agree to defend, indemnify and hold harmless Sire Mobile and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service; d) any claims made against Sire Mobile for your violations of the Telephone Consumer Protection Act. </p>

            <p><strong><span>Limitation Of Liability</span></strong></p>

            <p>In no event shall Sire Mobile, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>

            <p><strong><span>Disclaimer</span></strong></p>

            <p>Your use of the Service is at your sole risk. The Service is
            provided on an &quot;AS IS&quot; and &quot;AS AVAILABLE&quot; basis. The
            Service is provided without warranties of any kind, whether express or implied,
            including, but not limited to, implied warranties of merchantability, fitness
            for a particular purpose, non-infringement or course of performance.</p>

            <p>Sire Mobile its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.</p>

            <p><strong><span>Exclusions</span></strong></p>

            <p>Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.</p>

            <p><strong><span>Governing Law</span></strong></p>

            <p>These Terms shall be governed and construed in accordance with
            the laws of California, United States, without regard to its conflict of law
            provisions.</p>

            <p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.</p>

            <p><strong><span>Changes</span></strong></p>

            <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>

            <p>By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.</p>


            <p><strong><span>Contact Us</span></strong></p>

            <p>If you have any questions about these Terms, please contact us
            here:&nbsp;</span><a
            href="contact-us">Contact
            Us</a></p>
            </div>
      </section>
      </div>
</div>
      

<cfparam name="variables._title" default="Terms and Conditions - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">