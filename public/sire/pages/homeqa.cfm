<cfinvoke component="public.sire.models.cfc.userstools" method="checkLogin" returnvariable="checkLogin"></cfinvoke>
<cfif checkLogin>
	<cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/linkoverlay.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/preaccount.js">
</cfinvoke>


<cfset variables.body_class = 'home_page'>	
	
	<header class="sire-banner home-banner header-banners">
		<!--- <div class="slice"></div> --->

		<div class="home_bg_inner clearfix">
		
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
					

                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 text-center caption-text" style="">

						<!--- <h1 class="banner-title-big hidden-xs">Reach your customers</h1>
						<h1 class="banner-title-big hidden-xs">with less than a penny per text!</h1>

						<h3 class="banner-title-small visible-xs">Reach your customers</h3>
						<h3 class="banner-title-small visible-xs">with less than a penny per text!</h3> --->

						<h1 class="sire-title-updated">Reach your customers <br class="hidden-xs"> with less than a penny per text</h1>
                        <h1 class="sire-title-updated hide">with less than a penny per text</h1>

						<p class="banner-sub-title "><i>No Fees | No Commitment | No risk</i></p>

						<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >

						<cfelse>
							<a href="signup" class="get-started mt-20">get started</a>
						</cfif>
							
						
                        <!--- <h1 class="marketing_title_big tit-heading-banner hidden-xs">Reach your customers</h1>
                        <h1 class="marketing_title_big tit-heading-banner hidden-xs">with less than a penny per text!</h1>

                        <h3 class="marketing_title temp-mak-title visible-xs">Reach your customers</h3>
                        <h3 class="marketing_title temp-mak-title visible-xs">with less than a penny per text!</h3>
                                                
						<p class="sms_marketing hidden-xs">No fees | No commitment | No risk </p> --->
						<!--- <cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
							<p>
								<a href="signup" class="btn btn-info btn-lg btn-info-custom start_trial">
									START YOUR FREE TRIAL TODAY
								</a>
							</p>
						</cfif> --->
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-xs-1">
					</div>
					<div class="col-md-4 col-xs-10 text-center caption-text margin-top-set">
						<input type="text" class="form-control validate[required,custom[noHTML]]" id="EnterKW" name="EnterKW" maxlength="160" placeholder="Type in your keyword to see if it is available">
						<span id="KeywordStatus"></span>
					</div>
					<div class="col-md-4 col-xs-1">
					</div>
				</div>
			</div>	
		</section>
		</div>
        
		<!--- <div class="wrap visible-lg">
			<div class="chatbox" id="chatbox1">
				<p class="content"><!--- Your table is ready! --->Our installer, James, is on his way. To see his profile visit: http://mlp-x.com/jc
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">Your one time verification code is 678435
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">Energy Alerts: Scattered outages in your area caused by winter storms have been restored. Thank you for your patience and for being a valued Energy customer.
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">You've chosen to join the GENIUS group. Reply yes to confirm.
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">How likely are you to recommend Our services to your friends and family - On a scale of 10 (definitely) to 1 (definitely not)?
					<span class="arrow"></span>
				</p>
			</div>
            
            <div class="chatbox">
				<p class="content" style="text-align:left;">Lakeshore Daily 1:00 PM Basketball Group Message.<br/>Are you in or out today? Reply before noon for todays game.<br/>A) In<br/>B) Out<br/>C) Maybe
					<span class="arrow"></span>
				</p>
			</div>

			<div class="chatbox">
				<p class="content" style="text-align:left;">I plan to begin withdrawing $$$ from my investments in:<br/>A) Less than 3 years<br/>B) 3-5 years<br/>C) 6-10 years<br/>D) 11 years or more
					<span class="arrow"></span>
				</p>
			</div>
		</div> --->
	</header>
	<div class="main-wrapper">

		<section class="home-block-1">
			<div class="uk-container uk-container-center sae-homepage-container">
				<div class="inner-home-block-1">
					<div class="uk-grid">
						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/simple-image.png" alt="">	
								</div>
								<div class="info">
									<h4>simple</h4>
									<p>
										In 3 simple steps, you will be up and running with your first  campaign.
									</p>	
								</div>
							</div>	
						</div>

						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/affordable-image.png" alt="">	
								</div>
								<div class="info">
									<h4>affordable</h4>
									<p>
										Communicate with your customers for less than a penny per message.
									</p>	
								</div>
							</div>
						</div>

						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/effective-image.png" alt="">	
								</div>
								<div class="info">
									<h4>effective</h4>
									<p>
										97% of customers read text messages within the first 5 minutes.
									</p>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--- <div class="container">
				<div class="inner-home-block-1">
					<div class="row">
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/simple-image.png" alt="">	
										</div>
										<div class="info">
											<h4>simple</h4>
											<p>
												In 3 simple steps, you will be up and running with your first  campaign.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/affordable-image.png" alt="">	
										</div>
										<div class="info">
											<h4>affordable</h4>
											<p>
												Communicate with your customers for less than a penny per message.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/effective-image.png" alt="">	
										</div>
										<div class="info">
											<h4>effective</h4>
											<p>
												97% of customers read text messages within the first 5 minutes.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
					</div>
				</div>
			</div> --->
		</section>

		<section class="home-block-2">
			<div class="inner-home-block-2">
				<div class="uk-grid uk-grid-collapse">
					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/promotions.jpg" alt="">
								</div>
								<div class="block-overlay"><span>promotions</span></div>
								<a class="link-overlay">
									<span>
										Keep customers coming back and increase brand loyalty by promoting sales, coupons, and special promotions.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/alerts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>alerts</span></div>
								<a class="link-overlay">
									<span>
										When urgency is high, quickly alert your audience list with important information about your business or products.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/reminder.jpg" alt="">
								</div>
								<div class="block-overlay"><span>reminders</span></div>
								<a class="link-overlay">
									<span>
										Make sure patients don’t miss appointments, ensure timely arrivals to events and increase billing receivables with text reminders.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/surveys.jpg" alt="">
								</div>
								<div class="block-overlay"><span>surveys</span></div>
								<a class="link-overlay">
									<span>
										Improve your customer experience and quickly gain feedback about your company with text surveys.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/receipts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>receipts</span></div>
								<a class="link-overlay">
									<span>
										Save some trees!  SMS is a more environmentally friendly and efficient way to issue receipts.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/Confirmation.jpg" alt="">
								</div>
								<div class="block-overlay"><span>Confirmation</span></div>
								<a class="link-overlay">
									<span>
										Make sure your audience gets the message by sending text confirmations for transactions, appointments, or event RSVPs.
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>

				<!--- <div class="row row-collapse">
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/promotions.jpg" alt="">
								</div>
								<div class="block-overlay"><span>promotions</span></div>
								<a class="link-overlay">
									<span>
										Keep customers coming back and increase brand loyalty by promoting sales, coupons, and special promotions.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/alerts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>alerts</span></div>
								<a class="link-overlay">
									<span>
										When urgency is high, quickly alert your audience list with important information about your business or products.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/reminder.jpg" alt="">
								</div>
								<div class="block-overlay"><span>reminders</span></div>
								<a class="link-overlay">
									<span>
										Make sure patients don’t miss appointments, ensure timely arrivals to events and increase billing receivables with text reminders.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/surveys.jpg" alt="">
								</div>
								<div class="block-overlay"><span>surveys</span></div>
								<a class="link-overlay">
									<span>
										Improve your customer experience and quickly gain feedback about your company with text surveys.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/receipts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>receipts</span></div>
								<a class="link-overlay">
									<span>
										Save some trees!  SMS is a more environmentally friendly and efficient way to issue receipts.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/Confirmation.jpg" alt="">
								</div>
								<div class="block-overlay"><span>Confirmation</span></div>
								<a class="link-overlay">
									<span>
										Make sure your audience gets the message by sending text confirmations for transactions, appointments, or event RSVPs.
									</span>
								</a>
							</div>
						</div>
					</div>
				</div> --->
			</div>
		</section>

		<section class="home-block-3 feature-gray">
			<div class="container sae-homepage-container">
				<div class="inner-home-block-3">
					<div class="row">
						<div class="col-sm-12">
							<h4>We're not just a SMS interface. <br> We are an Intelligent Conversational Interface (ICI).</h4>
							<p>
								<i>
									You're better than your competition. You deserve a text marketing interface that is better than the rest.  Artificial Intelligence is a concept that large enterprises have had the luxury of being able to afford. At Sire, we don't think the big boys should have all the fun. We built our ICI so that SMBs can affordably leverage the same AI marketing power as large corporations to gain new customers and increase sales.
								</i>
							</p>
							
							<a href="how-it-works" class="get-started">learn more</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="home-block-2">
			<div class="inner-home-block-2">
				<div class="uk-grid uk-grid-collapse">
					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="features">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/easy-to-use-features.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">easy to use<br>features</span></div>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="plans-pricing">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/plans-and-pricing.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">plans and<br>pricing</span></div>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="why-sire">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/why-use-sire.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">why use<br>sire?</span></div>
								</a>
							</div>
						</div>
					</div>
				</div>

				<!--- <div class="row row-collapse">
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="features">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/easy-to-use-features.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">easy to use<br>features</span></div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="plans-pricing">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/plans-and-pricing.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">plans and<br>pricing</span></div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="why-sire">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/why-use-sire.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">why use<br>sire?</span></div>
								</a>
							</div>
						</div>
					</div>
				</div> --->
			</div>
		</section>

		<section class="home-block-5 feature-gray">
			<div class="container how-sire-work-container">
				<div class="inner-home-block-5">
					<div class="row">
						<div class="col-sm-12">
							<div class="header-block">
								<h4>How Sire Works</h4>
								<p><i>No Fees | No Commitment | No risk</i></p>
							</div>
						</div>
					</div>

					<div class="row body-block">
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-1.png" alt="">
								</div>
								<div class="content">
									<h4>Choose Your Keyword</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-2.png" alt="">
								</div>
								<div class="content">
									<h4>Select a Template</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-3.png" alt="">
								</div>
								<div class="content">
									<h4>Personalize Your Campaign</h4>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 text-center">
							<a href="how-it-works" class="get-started">learn more</a>
						</div>
					</div>
				</div>
			</div>
		</section>						

		<section class="home-block-3">
			<div class="container-fluid">
				<div class="inner-home-block-3">
					<div class="row">
						<div class="col-sm-12">
							<h4>See How Sire Can Help Grow Your Business</h4>
							<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
								
							<cfelse>
								<p>Take advantage of the benefits and sign up for your free account today!</p>
								<a href="signup" class="get-started">get started</a>
							</cfif>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="home-block-4">
			<div class="container">
				<div class="inner-home-block-4">
					<!-- 16:9 aspect ratio -->
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/N0UrR_G_tFc?rel=0&modestbranding=1&enablejsapi=1&autoplay=0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section>



		

		<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >

		<cfelse>
			<section class="home-block-3 feature-gray">
				<div class="container">
					<div class="inner-home-block-3">
						<div class="row">
							<div class="col-sm-12">
								<h4>Get Your Free Account Today</h4>
								<p>
									<i>
										<span class="block-on-mobile">No Credit Card Required</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">Free Upgrade or Downgrade</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">No Hidden Fees</span>
									</i>
								</p>
								
								<a href="signup" class="get-started">start now</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</cfif>
		


		<!--- <section class="features-home">
			<div class="features-home-inner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="how-it-work">
								<h3>How Sire Works</h3>
								<h4> No fees  |  No commitment  |  No risk</h4>
							</div>
						</div>
					</div>

					<div class="wrap-value-prop how-sire-work">
						<div class="row">
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-1">
									<div class="img"></div>
									<h3 class="title">Select from over<br>50+ templates</h3>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-2">
									<div class="img"></div>
									<h3 class="title">Personalize Your<br>Campaign</h3>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-3">
									<div class="img"></div>
									<h3 class="title">Click Send<br><i>It’s that Easy!</i></h3>
								</figure>
							</div>
						</div>
					</div>                    
				</div>

				<div class="no-credit-tit text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h4><b>No credit card required for a free account  |  Free upgrade or downgrade  |  No hidden fees</b></h4>
							</div>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="row">                    
	                	<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="cs-templates" title="Sample Templates">
									<h2>TEMPLATE LIBRARY</h2>
									<span>
										<img src="/public/sire/images/sample_templates.png"/>
									</span>
								</a>	
							</div>
						</div>
						
						<div class="col-xs-6 col-sm-3 col-lg-3">
							<div class="feature-home">
								<a href="/features" title="Easy to Use Features">
									<h2> EASY TO USE FEATURES </h2>
									<span>
										<img src="/public/sire/images/easy_to_use.png"/>
									</span>
								</a>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="/plans-pricing" title="Plans & Pricing" >
									<h2> PLANS & PRICING </h2>
									<span>
										<img src="/public/sire/images/plan_pricing.png"/>
									</span>
								</a>	
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="how-to-use" title="How to Use">
									<h2>HOW TO USE</h2>
									<span>
										<img src="/public/sire/images/live_demo.png"/>
									</span>
								</a>	
							</div>
						</div>                   	
					</div>
				</div>			
			</div>
		</section> --->
	</div>
	
	<!--- modal enter message--->
	<div tabindex="-1" class="modal be-modal fade in" id="mdEnterMessageTest" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="frmEnterMessageTest">					
					<div class="modal-body">
						<h4 class="be-modal-title">Enter message to your customer</h4>
						<div class="form-gd">
							<div class="form-group">								
								<textarea rows="4" class="form-control validate[required]" id="EnterMessageTest-Message" maxlength="255"></textarea>
								<span class="help-block">Enter the message a customer will receive if they type <b><span id="EnterMessageTest-Keyword">keyword</span></b> to <b><span id="EnterMessageTest-Shortcode">39492</span></b>. </span>
							</div>
						</div>
						<div class="text-right">
							<input type="submit" class="btn green-gd" id="btn-save-message" type="button" value="Save"/>
							<button data-bb-handler="cancel" type="button" class="btn green-cancel" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--- SHOULD NOT REMOVE, WE USE THIS TO CHECK CURRENT BUILD VERSION --->
	<cfoutput>
		<cfif structKeyExists(APPLICATION, "Build_Version")>
			<!-- #APPLICATION.Build_Version# -->	
		</cfif>
	</cfoutput>
	   
<cfparam name="variables._title" default="Affordable Mobile Marketing & SMS Marketing">
<cfparam name="variables.meta_description" default="The affordable, simple text message marketing service that lets you reach your customers for less than a penny per text. Free trial with no contracts.">

<style>
	.margin-top-set {
		margin-top:40px !important;
	}	
</style>


<cfinclude template="../views/layouts/home.cfm">
