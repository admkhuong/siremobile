
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/live-demo.min.css">
</cfinvoke>

<cfinclude template="/public/sire/views/layouts/shortcode.cfm"/>

<header class="sire-banner2 howitwwork-banner header-banners">
   
   <section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
   
   
   <section class="landing-jumbo how-it-works-lj">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <!--- <h1 class="banner-title-big hidden-xs">How to Use Sire</h1> --->

                    <!--- <h3 class="banner-title-small visible-xs">How to Use Sire</h3> --->

                    <!--- <h1 class="banner-title-big hidden-xs">we've made text marketing easy</h1> --->
                    <!--- <h1 class="banner-title-big hidden-xs">with less than a penny per text!</h1> --->

                    <!--- <h3 class="banner-title-small visible-xs">we've made text marketing easy</h3> --->
                    <!--- <h3 class="banner-title-small visible-xs">with less than a penny per text!</h3> --->

                    <h1 class="sire-title-updated">we’ve made text marketing easy</h1>

                    <p class="banner-sub-title ">
                        <i>Create and launch your own personalized campaign within a few minutes.<br> It’s that simple...and it's free.</i>
                    </p>

                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                    <cfelse>
                        <a href="signup" class="get-started mt-20">get started</a>
                    </cfif>

                    <!--- <p class="banner-sub-title hidden-xs"><i>Via Over the Top API</i></p> --->
                             
                    <!--- <h1 class="marketing_title_big hidden-xs">Demo Interactions</h1>
                    <h2 class="marketing_title visible-xs">Demo Interactions</h2>
                                            
                    <h4 class="hidden-xs"><i>Via Over the Top API</i></h4> --->
                        
                </div>	
            </div>	
            
            <!--- <div class="row hidden-xs hidden-sm">
                <div class="scroll-down-spacer_3"></div>
            </div>
            
            <div class="row visible-xs visible-sm">
                <div class="scroll-down-spacer_1"></div>
            </div> --->
            
            <!--- <div class="row">
                <a href="#feature-main">
                <div class="scroll-down">
                    <span>
                        <i class="fa fa-angle-down fa-2x"></i>
                    </span>
                </div>
                </a>
            </div> --->
                
        </div>
    </section>
        
</header>

<section class="home-block-3 home-block-how-sire-work feature-gray">
    <div class="container how-sire-work-container">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12">
                    <h4>We believe small businesses should have cool toys too.</h4>
                    <p>
                        <i>
                            Good marketing campaigns resonate with natural human behavior. This is where AI is a game changer. We took the best elements of AI and packaged them into an affordable, SMB-friendly text marketing interface. Here are a couple simple examples of how you benefit from ICI. 
                        </i>
                    </p>
                </div>
            </div>

            <div class="row phone-exam">
                <div class="col-md-6">
                    <div class="box-phone text-center boxphone1">
                        <p><i>Our competitors without AI integration</i></p>
                        <p class="stayhia" >
                            Joe’s Coffee: Text “FreeCoffee” to <cfoutput>#shortcode#</cfoutput> to<br>
                            receive a free coffee on your next visit!
                        </p>
                        <img class="img-responsive extend-img" src="/public/sire/images/leadscon/phone_1.png" alt="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box-phone text-center boxphone2">
                        <p><b>Sire's Intelligent Communication Interface (ICI)</b></p>
                        <p class="stayhia" >
                            Pete’s Plumbing: Based on our last visit, would you<br>
                            recommend our service to friends & family?
                        </p>
                        <img class="img-responsive extend-img" src="/public/sire/images/leadscon/phone_2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-block-gal">
    <div class="container-fluid">
        <div class="inner-how-block-gal">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="title">Just a few examples of how you can use SMS marketing</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="inner-how-block-gal">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <div class="image2">
                                    <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-1.png" alt="">
                                    <h2><cfoutput>
                                        <cfif len(shortcode) EQ 5>
                                            #left(shortcode,3)#-#right(shortcode,2)#
                                        <cfelse>
                                            #shortcode#
                                        </cfif>
                                    </cfoutput></h2>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h4>Reminders, alerts, announcements</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square image1">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-2_backup.png" alt="">
                                <h2><cfoutput>#left(shortcode,5)#</cfoutput></h2>
                            </div>
                        </div>
                        <div class="content">
                            <h4>Build your customer list</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square image3">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-3_v2.0.png" alt="">
                                <h2><cfoutput>
                                    <cfif len(shortcode) EQ 5>
                                        #left(shortcode,3)#-#right(shortcode,2)#
                                    <cfelse>
                                        #shortcode#
                                    </cfif>
                                </cfoutput></h2>
                            </div>
                        </div>
                        <div class="content">
                            <h4>Customer service, feedback, surveys</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <div class="image4">
                                    <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-4_backup.png" alt="">
                                    <h2><cfoutput>#left(shortcode,5)#</cfoutput></h2>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h4>Text-to-win ads</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square image2">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-5.png" alt="">
                                <h2><cfoutput>
                                    <cfif len(shortcode) EQ 5>
                                        #left(shortcode,3)#-#right(shortcode,2)#
                                    <cfelse>
                                        #shortcode#
                                    </cfif>
                                </cfoutput></h2>
                            </div>
                        </div>
                        <div class="content">
                            <h4>Coupons, promotions, specials</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-6_2.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Information, Education, Instruction</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-block-3 feature-gray">
    <div class="container">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Built for the Enterprise Class. Tailored for SMBs.</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-container uk-container-center container-for-enterprise">
        <div class="inner-home-block-1">
            <div class="uk-grid" uk-grid>
                <div class="uk-width-1-3@m enterprise-icon-block">
                    <div class="enterprise-item uk-flex uk-flex-middle uk-flex-center clearfix">
                        <div class="img">
                            <img src="/public/sire/images/affordable-image2.png" alt="">
                        </div>
                        <div class="info">
                            <h4>
                                Priced for Startups & Solopreneurs
                            </h4>
                        </div>
                    </div>  
                </div>

                <div class="uk-width-1-3@m enterprise-icon-block">
                    <div class="enterprise-item uk-flex uk-flex-middle uk-flex-center clearfix">
                        <div class="img">
                            <img src="/public/sire/images/simple-image.png" alt=""> 
                        </div>
                        <div class="info">
                            <h4>
                                Easy to use for SMBs & Marketers
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="uk-width-1-3@m enterprise-icon-block">
                    <div class="enterprise-item uk-flex uk-flex-middle uk-flex-center clearfix">
                        <div class="img">
                            <img src="/public/sire/images/effective-image.png" alt="">  
                        </div>
                        <div class="info">
                            <h4>
                                Created for Agencies & Enterprise
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
<cfelse>
    <section class="home-block-3 how-block-3">
        <div class="inner-home-block-3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Sign up for a free account</h4>
                        <p><i>No credit card | No fees | No commitment</i></p>
                        
                        <a href="signup" class="get-started">get started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</cfif>

<section class="home-block-5 home-block-5-extend mt-remove">
    <div class="container how-sire-work-container">
        <div class="inner-home-block-5">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-block">
                        <h4>It’s easy as 1-2-3 and send</h4>
                    </div>
                </div>
            </div>

            <div class="row body-block">
                <div class="col-sm-4">
                    <div class="how-sire-item">
                        <div class="img">
                            <img class="img-responsive" src="/public/sire/images/home/bubble-1.png" alt="">
                        </div>
                        <div class="content">
                            <h4>Choose Your Keyword</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="how-sire-item">
                        <div class="img">
                            <img class="img-responsive" src="/public/sire/images/home/bubble-2.png" alt="">
                        </div>
                        <div class="content">
                            <h4>Select a Template</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="how-sire-item">
                        <div class="img">
                            <img class="img-responsive" src="/public/sire/images/home/bubble-3.png" alt="">
                        </div>
                        <div class="content">
                            <h4>Personalize Your Campaign</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>







<section class="home-block-3 grow-business">
    <div class="container-fluid">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Growing your business shouldn’t be complicated</h4>
                    <p>Watch how we made it both simple and fun with these three videos</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-inside-sire inside-sire">
    <div class="container">
        <div class="inner-show-inside">
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9" id="use-guide-3">
                <iframe class="embed-responsive-item ifr-video" src="" allowfullscreen></iframe>
            </div>

            <div class="row row-collapse has-border-video">
                <div class="col-xs-6 col-sm-4">
                    <div class="item-video-use active video-btn-3" data-id="1">
                        <div class="imges">
                            <img class="img-responsive" src="/public/sire/images/how-it-work/thumb-1.jpg" alt="">

                            <div class="overlay">
                                <img class="img-responsive" src="/public/sire/images/how-it-work/Playing-Now.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item-video-use video-btn-1" data-id="2">
                        <div class="imges">
                            <img class="img-responsive" src="/public/sire/images/how-it-work/thumb-2.jpg" alt="">

                            <div class="overlay">
                                <img class="img-responsive" src="/public/sire/images/how-it-work/Playing-Now.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item-video-use video-btn-2" data-id="3">
                        <div class="imges">
                            <img class="img-responsive" src="/public/sire/images/how-it-work/thumb-3.jpg" alt="">

                            <div class="overlay">
                                <img class="img-responsive" src="/public/sire/images/how-it-work/Playing-Now.png" alt="">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>        
</section>


<!--- <section class="main-wrapper">
    <div class="container video" style="text:center">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="bold-blue-text">
                    <h2 class="video-title">What's Inside Sire?</h2>
                </div>
            </div>
        </div>
                
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="embed-responsive embed-responsive-16by9" id="use-guide-3">            
                    <iframe class="embed-responsive-item ifr-video" src="" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <ul class="nav nav-pills list-thumb-video">
                    <li>
                        <div class="item-video-use active video-btn-3" data-id="1">
                            <div class="imges">
                                <img class="img-responsive" src="/public/sire/images/thumb-3.png" alt="">

                                <div class="overlay">
                                    <img class="img-responsive" src="/public/sire/images/Playing-Now.png" alt="">
                                </div>
                            </div>
                            <div class="content">
                                <h4>What's Inside Sire?</h4>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-video-use video-btn-1" data-id="2">
                            <div class="imges">
                                <img class="img-responsive" src="/public/sire/images/thumb-1.png" alt="">

                                <div class="overlay">
                                    <img class="img-responsive" src="/public/sire/images/Playing-Now.png" alt="">
                                </div>
                            </div>
                            <div class="content">
                                <h4>How to create your first campaign</h4>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-video-use video-btn-2" data-id="3">
                            <div class="imges">
                                <img class="img-responsive" src="/public/sire/images/thumb-2.png" alt="">

                                <div class="overlay">
                                    <img class="img-responsive" src="/public/sire/images/Playing-Now.png" alt="">
                                </div>
                            </div>
                            <div class="content">
                                <h4>How to create your first subscriber list</h4>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section> --->


<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
<cfelse>
    <section class="home-block-3 how-last">
        <div class="container">
            <div class="inner-home-block-3">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Get Your Free Account Today</h4>
                        <p>No Credit Card Required | Free Upgrade or Downgrade | No Hidden Fees</p>
                        <a href="signup" class="get-started">start now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</cfif>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.autogrow-textarea.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/live_demo.js">
</cfinvoke>
	
<cfparam name="variables._title" default="How SMS Marketing Works">
<cfparam name="variables.body_class" default="body-live-demo">

<cfparam name="variables.meta_description" default="Build and launch your first SMS marketing campaign in minutes. Great for promotions, coupons, specials, surveys, reminders, alerts, announcements and live chat.">


<cfinclude template="../views/layouts/home.cfm">
<script type="text/javascript">
    $('.li-live-demo').addClass('active');
</script>
