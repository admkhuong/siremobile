<cfinvoke component="public.sire.models.cfc.userstools" method="checkLogin" returnvariable="checkLogin"></cfinvoke>
<cfif checkLogin>
	<cflocation url="/session/sire/pages/dashboard" addtoken="false"/>
</cfif>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/linkoverlay.js">
</cfinvoke>

<cfset variables.body_class = 'home_page'>	


<!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: https://www.jssor.com -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jssor-slider/26.1.5/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Opacity:2}
            ];

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,sX:-0.2,sY:-0.2},{b:0,d:1600,y:-461,e:{y:31}}],
              [{b:0,d:1600,y:-460,e:{y:31}}],
              [{b:0,d:1600,y:-460,e:{y:31}}],
              [{b:0,d:1600,y:-473,e:{y:31}}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:400,kX:20},{b:620,d:400,kX:-20}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:20,d:180,y:-50},{b:240,d:200,sY:-0.8},{b:440,d:200,sY:0.8},{b:640,d:200,sY:-0.8},{b:840,d:200,sY:0.8},{b:1040,d:160,y:50}],
              [{b:20,d:180,y:-50},{b:240,d:200,sY:-0.8},{b:440,d:200,sY:0.8},{b:640,d:200,sY:-0.8},{b:840,d:200,sY:0.8},{b:1040,d:160,y:50}],
              [{b:240,d:400,kX:20},{b:640,d:400,kX:-20}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:220,d:380,sY:-0.8},{b:600,d:600,sY:0.2},{b:1200,d:200,sY:-0.2},{b:1400,d:200,sY:0.8}],
              [{b:220,d:400,kX:40},{b:620,d:400,kX:-40}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:400,kX:20},{b:620,d:400,kX:-20}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8},{b:1020,d:600,x:67,y:-22},{b:2800,d:600,x:-67,y:22}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8},{b:1020,d:600,x:33,y:-35,sX:-0.1,sY:-0.1},{b:2800,d:600,x:-33,y:35,sX:0.1,sY:0.1}],
              [{b:1020,d:600,x:58,y:-8,sX:-0.4,kY:10},{b:2800,d:600,x:-58,y:8,sX:0.4,kY:-10}],
              [{b:-1,d:1,sX:-0.2,sY:-0.2}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:200,sY:-0.8},{b:420,d:200,sY:0.8},{b:620,d:200,sY:-0.8},{b:820,d:200,sY:0.8}],
              [{b:220,d:400,kX:20},{b:620,d:400,kX:-20}]
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 500,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin double-tail-spin css */

        .jssorl-004-double-tail-spin img {
            animation-name: jssorl-004-double-tail-spin;
            animation-duration: 1.2s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-004-double-tail-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }

    </style>
    
    
	
	<header class="sire-banner home-banner header-banners">
		<!--- <div class="slice"></div> --->

		<div class="home_bg_inner clearfix">
		
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/smile/logo-smile-sm.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/smile/logo-smile-xs.png">
                            </a>
                        </div>
                    </div>
					
					

                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 text-center caption-text" style="xpadding-left: 0px">

						<!--- <h1 class="banner-title-big hidden-xs">Reach your customers</h1>
						<h1 class="banner-title-big hidden-xs">with less than a penny per text!</h1>

						<h3 class="banner-title-small visible-xs">Reach your customers</h3>
						<h3 class="banner-title-small visible-xs">with less than a penny per text!</h3> --->

<!--- 						<h1 class="sire-title-updated">Reach your customers <br class="hidden-xs"> with less than a penny per text</h1> ---> 
<!---                         <h1 class="sire-title-updated hide">with less than a penny per text</h1> --->

						<h2 class="">Text Marketing 4 UR Business</h2>
						<h3 class=" "><i>Try if for Free - No Credit Card Required</i></h3>

						<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >

						<cfelse>
							<a href="signup" class="get-started mt-20">get started</a>
						</cfif>
							
						
                        <!--- <h1 class="marketing_title_big tit-heading-banner hidden-xs">Reach your customers</h1>
                        <h1 class="marketing_title_big tit-heading-banner hidden-xs">with less than a penny per text!</h1>

                        <h3 class="marketing_title temp-mak-title visible-xs">Reach your customers</h3>
                        <h3 class="marketing_title temp-mak-title visible-xs">with less than a penny per text!</h3>
                                                
						<p class="sms_marketing hidden-xs">No fees | No commitment | No risk </p> --->
						<!--- <cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
							<p>
								<a href="signup" class="btn btn-info btn-lg btn-info-custom start_trial">
									START YOUR FREE TRIAL TODAY
								</a>
							</p>
						</cfif> --->
																		
						<div id="jssor_1" style="position:relative; margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
					        <!-- Loading Screen -->
					        <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;text-align:center;background-color:rgba(0,0,0,0.7);">
					            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="/public/sire/images/smile/double-tail-spin.svg" />
					        </div>
					        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="0" style="position:absolute;top:290px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="1" style="position:absolute;top:210px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="2" style="position:absolute;top:210px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="3" style="position:absolute;top:648px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="4" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="5" style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="6" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="7" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="8" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="9" style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="10" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="11" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="12" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="13" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="14" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="15" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="16" style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="17" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="18" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="19" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="20" style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="21" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="22" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            <div>
					                <div style="position:absolute;top:0px;left:0px;width:300px;height:150px;z-index:0;">
					                    <img data-u="caption" data-t="23" style="position:absolute;top:-171px;left:200px;width:595px;height:842px;z-index:0;" src="/public/sire/images/smile/ball-yellow-1064402.svg" />
					                    <img data-u="caption" data-t="24" style="position:absolute;top:-250px;left:112px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="25" style="position:absolute;top:-250px;left:254px;width:612px;height:792px;z-index:0;" src="/public/sire/images/smile/eyeball-black-2.svg" />
					                    <img data-u="caption" data-t="26" style="position:absolute;top:175px;left:340px;width:300px;height:150px;z-index:0;" src="/public/sire/images/smile/smile-mouth.svg" />
					                </div>
					            </div>
					            
					        </div>
					    </div>
					    <script type="text/javascript">jssor_1_slider_init();</script>
					    <!-- #endregion Jssor Slider End -->
					    
    
					</div>
				</div>
			</div>	
		</section>
		</div>
        
		<!--- <div class="wrap visible-lg">
			<div class="chatbox" id="chatbox1">
				<p class="content"><!--- Your table is ready! --->Our installer, James, is on his way. To see his profile visit: http://mlp-x.com/jc
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">Your one time verification code is 678435
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">Energy Alerts: Scattered outages in your area caused by winter storms have been restored. Thank you for your patience and for being a valued Energy customer.
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">You've chosen to join the GENIUS group. Reply yes to confirm.
					<span class="arrow"></span>
				</p>
			</div>
			<div class="chatbox">
				<p class="content">How likely are you to recommend Our services to your friends and family - On a scale of 10 (definitely) to 1 (definitely not)?
					<span class="arrow"></span>
				</p>
			</div>
            
            <div class="chatbox">
				<p class="content" style="text-align:left;">Lakeshore Daily 1:00 PM Basketball Group Message.<br/>Are you in or out today? Reply before noon for todays game.<br/>A) In<br/>B) Out<br/>C) Maybe
					<span class="arrow"></span>
				</p>
			</div>

			<div class="chatbox">
				<p class="content" style="text-align:left;">I plan to begin withdrawing $$$ from my investments in:<br/>A) Less than 3 years<br/>B) 3-5 years<br/>C) 6-10 years<br/>D) 11 years or more
					<span class="arrow"></span>
				</p>
			</div>
		</div> --->
	</header>
	<div class="main-wrapper">

		<section class="home-block-1">
			<div class="uk-container uk-container-center sae-homepage-container">
				<div class="inner-home-block-1">
					<div class="uk-grid">
						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/simple-image.png" alt="">	
								</div>
								<div class="info">
									<h4>simple</h4>
									<p>
										In 3 simple steps, you will be up and running with your first  campaign.
									</p>	
								</div>
							</div>	
						</div>

						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/affordable-image.png" alt="">	
								</div>
								<div class="info">
									<h4>affordable</h4>
									<p>
										Communicate with your customers for less than a penny per message.
									</p>	
								</div>
							</div>
						</div>

						<div class="uk-width-1-3@m">
							<div class="home-block-1-item clearfix">
								<div class="img">
									<img src="/public/sire/images/effective-image.png" alt="">	
								</div>
								<div class="info">
									<h4>effective</h4>
									<p>
										97% of customers read text messages within the first 5 minutes.
									</p>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--- <div class="container">
				<div class="inner-home-block-1">
					<div class="row">
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/simple-image.png" alt="">	
										</div>
										<div class="info">
											<h4>simple</h4>
											<p>
												In 3 simple steps, you will be up and running with your first  campaign.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/affordable-image.png" alt="">	
										</div>
										<div class="info">
											<h4>affordable</h4>
											<p>
												Communicate with your customers for less than a penny per message.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
						<div class="col-sm-4">
							<figure class="row">
								<div class="col-xs-12">
									<div class="home-block-1-item clearfix">
										<div class="img">
											<img src="/public/sire/images/effective-image.png" alt="">	
										</div>
										<div class="info">
											<h4>effective</h4>
											<p>
												97% of customers read text messages within the first 5 minutes.
											</p>	
										</div>
									</div>
								</div>
							</figure>
						</div>
					</div>
				</div>
			</div> --->
		</section>

		<section class="home-block-2">
			<div class="inner-home-block-2">
				<div class="uk-grid uk-grid-collapse">
					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/promotions.jpg" alt="">
								</div>
								<div class="block-overlay"><span>promotions</span></div>
								<a class="link-overlay">
									<span>
										Keep customers coming back and increase brand loyalty by promoting sales, coupons, and special promotions.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/alerts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>alerts</span></div>
								<a class="link-overlay">
									<span>
										When urgency is high, quickly alert your audience list with important information about your business or products.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/reminder.jpg" alt="">
								</div>
								<div class="block-overlay"><span>reminders</span></div>
								<a class="link-overlay">
									<span>
										Make sure patients don’t miss appointments, ensure timely arrivals to events and increase billing receivables with text reminders.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/surveys.jpg" alt="">
								</div>
								<div class="block-overlay"><span>surveys</span></div>
								<a class="link-overlay">
									<span>
										Improve your customer experience and quickly gain feedback about your company with text surveys.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/receipts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>receipts</span></div>
								<a class="link-overlay">
									<span>
										Save some trees!  SMS is a more environmentally friendly and efficient way to issue receipts.
									</span>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/Confirmation.jpg" alt="">
								</div>
								<div class="block-overlay"><span>Confirmation</span></div>
								<a class="link-overlay">
									<span>
										Make sure your audience gets the message by sending text confirmations for transactions, appointments, or event RSVPs.
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>

				<!--- <div class="row row-collapse">
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/promotions.jpg" alt="">
								</div>
								<div class="block-overlay"><span>promotions</span></div>
								<a class="link-overlay">
									<span>
										Keep customers coming back and increase brand loyalty by promoting sales, coupons, and special promotions.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/alerts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>alerts</span></div>
								<a class="link-overlay">
									<span>
										When urgency is high, quickly alert your audience list with important information about your business or products.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/reminder.jpg" alt="">
								</div>
								<div class="block-overlay"><span>reminders</span></div>
								<a class="link-overlay">
									<span>
										Make sure patients don’t miss appointments, ensure timely arrivals to events and increase billing receivables with text reminders.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/surveys.jpg" alt="">
								</div>
								<div class="block-overlay"><span>surveys</span></div>
								<a class="link-overlay">
									<span>
										Improve your customer experience and quickly gain feedback about your company with text surveys.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/receipts.jpg" alt="">
								</div>
								<div class="block-overlay"><span>receipts</span></div>
								<a class="link-overlay">
									<span>
										Save some trees!  SMS is a more environmentally friendly and efficient way to issue receipts.
									</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<div class="inner-retangle">
									<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/Confirmation.jpg" alt="">
								</div>
								<div class="block-overlay"><span>Confirmation</span></div>
								<a class="link-overlay">
									<span>
										Make sure your audience gets the message by sending text confirmations for transactions, appointments, or event RSVPs.
									</span>
								</a>
							</div>
						</div>
					</div>
				</div> --->
			</div>
		</section>

		<section class="home-block-3 feature-gray">
			<div class="container sae-homepage-container">
				<div class="inner-home-block-3">
					<div class="row">
						<div class="col-sm-12">
							<h4>We're not just a SMS interface. <br> We are an Intelligent Conversational Interface (ICI).</h4>
							<p>
								<i>
									You're better than your competition. You deserve a text marketing interface that is better than the rest.  Artificial Intelligence is a concept that large enterprises have had the luxury of being able to afford. At Sire, we don't think the big boys should have all the fun. We built our ICI so that SMBs can affordably leverage the same AI marketing power as large corporations to gain new customers and increase sales.
								</i>
							</p>
							
							<a href="how-it-works" class="get-started">learn more</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="home-block-2">
			<div class="inner-home-block-2">
				<div class="uk-grid uk-grid-collapse">
					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="features">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/easy-to-use-features.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">easy to use<br>features</span></div>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="plans-pricing">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/plans-and-pricing.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">plans and<br>pricing</span></div>
								</a>
							</div>
						</div>
					</div>

					<div class="uk-width-1-3@m">
						<div class="grid-item">
							<div class="retangle">
								<a href="why-sire">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/why-use-sire.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">why use<br>sire?</span></div>
								</a>
							</div>
						</div>
					</div>
				</div>

				<!--- <div class="row row-collapse">
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="features">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/easy-to-use-features.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">easy to use<br>features</span></div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="plans-pricing">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/plans-and-pricing.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">plans and<br>pricing</span></div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="grid-item">
							<div class="retangle">
								<a href="why-sire">
									<div class="inner-retangle">
										<img class="img-responsive extend-img" src="/public/sire/images/home/homeupdate/why-use-sire.jpg" alt="">
									</div>
									<div class="block-overlay"><span class="block-under">why use<br>sire?</span></div>
								</a>
							</div>
						</div>
					</div>
				</div> --->
			</div>
		</section>

		<section class="home-block-5 feature-gray">
			<div class="container how-sire-work-container">
				<div class="inner-home-block-5">
					<div class="row">
						<div class="col-sm-12">
							<div class="header-block">
								<h4>How Sire Works</h4>
								<p><i>No Fees | No Commitment | No risk</i></p>
							</div>
						</div>
					</div>

					<div class="row body-block">
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-1.png" alt="">
								</div>
								<div class="content">
									<h4>Choose Your Keyword</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-2.png" alt="">
								</div>
								<div class="content">
									<h4>Select a Template</h4>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="how-sire-item">
								<div class="img">
									<img class="img-responsive" src="/public/sire/images/home/bubble-3.png" alt="">
								</div>
								<div class="content">
									<h4>Personalize Your Campaign</h4>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 text-center">
							<a href="how-it-works" class="get-started">learn more</a>
						</div>
					</div>
				</div>
			</div>
		</section>						

		<section class="home-block-3">
			<div class="container-fluid">
				<div class="inner-home-block-3">
					<div class="row">
						<div class="col-sm-12">
							<h4>See How Sire Can Help Grow Your Business</h4>
							<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
								
							<cfelse>
								<p>Take advantage of the benefits and sign up for your free account today!</p>
								<a href="signup" class="get-started">get started</a>
							</cfif>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="home-block-4">
			<div class="container">
				<div class="inner-home-block-4">
					<!-- 16:9 aspect ratio -->
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/N0UrR_G_tFc?rel=0&modestbranding=1&enablejsapi=1&autoplay=0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</section>



		

		<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >

		<cfelse>
			<section class="home-block-3 feature-gray">
				<div class="container">
					<div class="inner-home-block-3">
						<div class="row">
							<div class="col-sm-12">
								<h4>Get Your Free Account Today</h4>
								<p>
									<i>
										<span class="block-on-mobile">No Credit Card Required</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">Free Upgrade or Downgrade</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">No Hidden Fees</span>
									</i>
								</p>
								
								<a href="signup" class="get-started">start now</a>
							</div>
						</div>
					</div>
				</div>
			</section>
		</cfif>
		


		<!--- <section class="features-home">
			<div class="features-home-inner">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="how-it-work">
								<h3>How Sire Works</h3>
								<h4> No fees  |  No commitment  |  No risk</h4>
							</div>
						</div>
					</div>

					<div class="wrap-value-prop how-sire-work">
						<div class="row">
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-1">
									<div class="img"></div>
									<h3 class="title">Select from over<br>50+ templates</h3>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-2">
									<div class="img"></div>
									<h3 class="title">Personalize Your<br>Campaign</h3>
								</figure>
							</div>
							<div class="col-sm-4">
								<figure class="text-center value-prop-item" id="figs-3">
									<div class="img"></div>
									<h3 class="title">Click Send<br><i>It’s that Easy!</i></h3>
								</figure>
							</div>
						</div>
					</div>                    
				</div>

				<div class="no-credit-tit text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h4><b>No credit card required for a free account  |  Free upgrade or downgrade  |  No hidden fees</b></h4>
							</div>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="row">                    
	                	<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="cs-templates" title="Sample Templates">
									<h2>TEMPLATE LIBRARY</h2>
									<span>
										<img src="/public/sire/images/sample_templates.png"/>
									</span>
								</a>	
							</div>
						</div>
						
						<div class="col-xs-6 col-sm-3 col-lg-3">
							<div class="feature-home">
								<a href="/features" title="Easy to Use Features">
									<h2> EASY TO USE FEATURES </h2>
									<span>
										<img src="/public/sire/images/easy_to_use.png"/>
									</span>
								</a>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="/plans-pricing" title="Plans & Pricing" >
									<h2> PLANS & PRICING </h2>
									<span>
										<img src="/public/sire/images/plan_pricing.png"/>
									</span>
								</a>	
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-lg-3">	
							<div class="feature-home">
								<a href="how-to-use" title="How to Use">
									<h2>HOW TO USE</h2>
									<span>
										<img src="/public/sire/images/live_demo.png"/>
									</span>
								</a>	
							</div>
						</div>                   	
					</div>
				</div>			
			</div>
		</section> --->
	</div>
	
	

	<!--- SHOULD NOT REMOVE, WE USE THIS TO CHECK CURRENT BUILD VERSION --->
	<cfoutput>
		<cfif structKeyExists(APPLICATION, "Build_Version")>
			<!-- #APPLICATION.Build_Version# -->	
		</cfif>
	</cfoutput>
	   
<cfparam name="variables._title" default="Affordable Mobile Marketing & SMS Marketing">
<cfparam name="variables.meta_description" default="The affordable, simple text message marketing service that lets you reach your customers for less than a penny per text. Free trial with no contracts.">




<cfinclude template="../views/layouts/home.cfm">
