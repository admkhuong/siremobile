

<!---


Store as encrypted cookie on your target device browser
Can you store cookie in IOS app?


--->


<section class="main-wrapper feature-main" id="feature-main">


	<div class="feature-white no-padding-bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="feature-title" id="feature1">Multi-Factor Authentication</div>
                    <div class="feature-content">
                        
                        <p>One Time verification code uses information sent to a phone via SMS, Push or Voice for use as part of the login process.</p>
                            
                    </div>
                </div>

              	<div class="col-md-6">                    
                                    
                       <img style="float:left;" src="/public/sire//learning/mfademoheader.png" />                                                                  
                    
                </div>

            </div>
        </div>
    </div>

	
    
    	<div class="feature-gray">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-6">                    
                   		                
		                   <img style="float:left;" src="/public/sire/images/learning/mfaloginfocused.png" />                                                                  
                        
					</div>
                    
					<div class="col-md-6">
						<div class="feature-title" id="feature1">Two Factor Authentication</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Something you know and something you have</b>                            
                            </p>
                            
                            <p>Older web sites and applications only have one layer - the password - to protect their users. By adding 2-Step Verification, if a bad guy hacks through your password layer, they'll still need your phone to get into your account.</p>
                                                       
                        
                                
						</div>
					</div>
                    
				</div>
			</div>
		</div>
        
        	<div class="feature-white">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-6">                    
                   		                     
                       <div class="definitionBox white-bg">
                       <h1>con·ver·sa·tion</h1>
                       <h2>/ˌkänvərˈsāSH(ə)n/</h2>
                       
                       <h3><i>noun</i></h3>
                       		
                            <div>
                            	the informal exchange of ideas by words.<br/>
								"the two people were deep in conversation"<br/>
                                <i>synonyms:</i>	discussion, talk, chat, gossip, tête-à-tête, heart-to-heart, exchange, dialogue;
                                <ul>                                
	                                <li>
                                    	an instance of this.
                                    	<p>"she used her phone to have a conversation using SMS"</p>
                                    </li>                                                                    
                                </ul>                           
                            
                            </div>
                       
                       </div>	                                              
                        
					</div>
                    
					<div class="col-md-6">
						<div class="feature-title" id="feature1">Lists</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Market to your lists</b>                            
                            </p>
                            
                            <p>Sire assists you with easy to customize design templates that create the campaigns that will delight your customers.</p>
                                                       
                        	<p>Achieve the impressive response and conversion rates mobile marketing is known for with Sire’s highly personalized template picker for simple to complex tasks, and everything in between:</p>
                           
                            <p>                         
                                <b>Our team will help you get started, faster.</b>                            
                            </p>
                                
                            <p>Expert-customized templates available. We can build customized templates to suit your business needs. Then, just add your message, and hit send.</p>
                            
                            <p>Or schedule a personal product walk-through.</p>
                                
						</div>
					</div>
                    
				</div>
			</div>
		</div>
        
        
</div>

<cfparam name="variables._title" default="Case Studies">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

