<cfset variables.body_class = 'home_page'>
<cfset hideFixedSignupForm = '1'>
	
<cfif structKeyExists(Session,'loggedIn')>
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
	</cfinvoke>

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
	</cfinvoke>

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/sign_up.js">
	</cfinvoke>
</cfif>

	
	<header class="home home_bg header-banners">
		<!--- <div class="slice"></div> --->

		<div class="home_bg_inner clearfix">
		
		<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
					

                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center caption-text" style="">

						<h1 class="banner-title-big special-banner-title hidden-xs">Higher table turnover & sales with our simple & affordable text marketing solution. Try it for free!</h1>
						<!--- <h1 class="banner-title-big hidden-xs">with Sire’s simple and affordable text messaging solution!</h1>
						<h1 class="banner-title-big hidden-xs">Sign up for a free account with Sire</h1> --->

						<!--- <h3 class="banner-title-small visible-xs">Increase table turnover, higher foot traffic and sales volumes with Sire’s simple and affordable text messaging solution! Sign up for a free account with Sire</h3> --->
						<!--- <h3 class="banner-title-small visible-xs">with Sire’s simple and affordable text messaging solution!</h3>
						<h3 class="banner-title-small visible-xs">Sign up for a free account with Sire</h3> --->

						<p class="banner-sub-title hidden-xs"><i>No credit card | No hidden fees | No commitment</i></p>
					</div>
				</div>
			</div>	
		</section>
		</div>
	</header>
	<div class="main-wrapper">
		<section class="heading-page">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1 class="heading-page-title">"Once I started to use Sire, within 3 months, we were able to increase our sales volume by over 25%! It’s so much better than any of the tools or services that I had used in the past!"</h1>
						<h4 class="heading-page-subtitle">- James Kozono, Co-founder, Roll It Sushi and Teriyaki -</h4>
					</div>
				</div>
			</div>
		</section>	

		<section class="landing-block-1">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h4 class="Challenge-title">Top 3 Challenges for Restaurants</h4>
						<div class="row">
							<div class="col-sm-4">
								<div class="challenge-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-wc.png" alt="">
									</div>
									<div class="content">
										<h4>Reduced Repeat Customers</h4>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="challenge-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-people.png" alt="">
									</div>
									<div class="content">
										<h4>Lower foot traffic</h4>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="challenge-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-spoon.png" alt="">
									</div>
									<div class="content">
										<h4>Reduced table turnover</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 grey-bg">
						<div class="block-signup-landing">
							<div class="block-signup-landing">
								<h4 class="title">Sign up for a free account!</h4>
								<form name="sign-up-lr" id="sign-up-lr" method="post">
									<div class="form-group">
										<label for="lrNumber">SMS Number *</label>
										<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="lrNumber" name="lrNumber" placeholder="">
									</div>
									<div class="form-group">
										<label for="lrEmail">E-mail Address *</label>
										<input type="email" class="form-control validate[required,custom[email]]" id="lrEmail" name="lrEmail" placeholder="">
									</div>
									<div class="form-group">
										<label for="lrPassword">Password *</label>
										<input type="password" class="form-control validate[required,funcCall[isValidPassword]]" name="lrPassword" id="lrPassword" placeholder="">
									</div>
									<div class="form-group">
										<label for="cfPassword">Confirm Password *</label>
										<input type="password" class="form-control validate[required,equals[lrPassword]]" name="cfPassword" id="cfPassword" placeholder="">
									</div>
									<div class="checkbox form-group">
										<label>
											<input type="checkbox" class="validate[required]" name="agree" id="agree"> I’ve read and agree to the <a href="/term-of-use">Terms of Use</a> and <a href="/anti-spam">SMS Terms of Service.</a>
										</label>
									</div>
									<div class="text-right form-group">
										<button id="btn-submit" type="button" class="btn btn-success btn-login btn-success-custom">Get Started</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	

		<section class="landing-block-2">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h4 class="title">Here’s How We Solve It</h4>
						<div class="row">
							<div class="col-sm-4">
								<div class="solve-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-file.png" alt="">
									</div>
									<div class="content">
										<p>
											Ready to use “<b>Reservation</b>” templates: Customers no longer wait long lines on the phone or at your restaurant. With a simple text message, customers are happy to come back to your restaurant more often!
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="solve-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-chart.png" alt="">
									</div>
									<div class="content">
										<p>
											Higher traffic means higher profits. With “<b>Coupon</b>” templates and Sire’s Marketing Landing Pages, you can now share about specials, events and happy hours with an effective and affordable solution.
										</p>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="solve-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-buble.png" alt="">
									</div>
									<div class="content">
										<p>
											Hear your customer’s feedback, opinions and comments using Sire’s “<b>Survey</b>” templates. Improve customer satisfaction and increase table turnover with our advanced reporting and analytics!
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	

		<section class="landing-block-3 grey-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="wrap-item-ld3">
							<div class="inner">
								<h1>Watch the <a href="#LiveDemoVideoModal" style="cursor: pointer;" data-type="videp-popup" data-video="promotion" data-toggle="modal">video</a> to see how Sire can help you grow your business</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="wrap-item-ld3">
							<div class="inner hold-img">
								<a target="_blank" href="https://s3-us-west-1.amazonaws.com/siremobile/docs/SIRE-rollitsushiteriyaki-case-study_v1.pdf">
									<img class="img-responsive" src="/public/sire/images/landing-roll-it-case-study.png" alt="">	
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="landing-block-4">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h4 class="title">Sire’s recommended templates for you!</h4>
						<div class="row">
							<div class="col-sm-3">
								<div class="template-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-reservation-new.png" alt="">
									</div>
									<div class="content">
										<h4>Reservation</h4>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="template-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-coupons.png" alt="">
									</div>
									<div class="content">
										<h4>Coupons</h4>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="template-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-offers.png" alt="">
									</div>
									<div class="content">
										<h4>Offers</h4>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="template-item">
									<div class="image">
										<img class="img-responsive" src="/public/sire/images/landing-rewards-sign-up.png" alt="">
									</div>
									<div class="content">
										<h4>Rewards Signup</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div id="LiveDemoVideoModal" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                <h4 class="modal-title">Sire Mobile</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="embed-responsive embed-responsive-16by9">
							<iframe id="LiveDemoVideoLink" class="embed-responsive-item" src="https://www.youtube.com/embed/LmJfUelQ7fE?rel=0&modestbranding=1&enablejsapi=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
		            	</div>
		            </div>
		            <div class="modal-footer">
				        <button type="button" class="btn btn-success-custom" data-dismiss="modal">Close</button>
				      </div>
		        </div>
		    </div>
		</div>	
	</div>
	
	

	<!--- SHOULD NOT REMOVE, WE USE THIS TO CHECK CURRENT BUILD VERSION --->
	<cfoutput>
		<cfif structKeyExists(APPLICATION, "Build_Version")>
			<!-- #APPLICATION.Build_Version# -->	
		</cfif>
	</cfoutput>

	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/landing-restaurant.js">
	</cfinvoke>

	<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
		<cfinvokeargument name="path" value="/public/sire/css/landing-restaurant.css">
	</cfinvoke>
	   
<cfparam name="variables._title" default="SMS Marketing - Sire">


<cfinclude template="../views/layouts/home.cfm">
