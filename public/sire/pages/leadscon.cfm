<cfset hideFixedSignupForm = 1 />
<cfset isLeadsconPage = 1 />

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/leadsconmodal.css">
</cfinvoke>


<header class="sire-banner home-banner header-banners">
    <div class="home_bg_inner clearfix">
        <div class="container content-banner-leadscon">
            <a href="/" class="leadscon-logo hidden-xs">
                <img src="/public/sire/images/leadscon/logo-leadscon2.png">
            </a>
            <a href="/" class="leadscon-logo visible-xs">
                <img src="/public/sire/images/logo2.png">
            </a>

            <p class="special-margin">
                The Smarter SMS Marketing Platform:
            </p>
            <h4>DID you know...</h4>
            <p>
                <i>Artificial Intelligence was used in your SMS conversation at LeadsCon to get you to this page? 
                <br>
                Scroll down to learn how it works.</i>
            </p>
            <p>
                <i>Want to learn more about how to use AI+SMS in your business?</i>
            </p>
            <a href="##" class="get-started" data-toggle="modal" data-target="#leadscon-email">Yes, Let's Talk</a>
        </div>
    </div>
</header>

<section class="leadscon-block2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="inner-leadscon-block2">
                    <h4 class="title text-center">AI is costly to develop and difficult to fully understand.<br>We made it easy and affordable.</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="inner-leadscon-block2">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            
                            <p class="short-desc">
                                Sire is more than a SMS marketing interface. It is an Intelligent Conversational Interface (ICI) which integrates AI so you can provide better Customer Service, gather Cleaner Data, reach More Customers and Increase Revenue.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div class="inner-leadscon-block2">
                    <div class="row">
                        <div class="col-sm-12 ai-ici-image text-center">
                            <img class="img-responsive extend-img" src="/public/sire/images/leadscon/ai-ici.png" alt="">
                        </div>

                        <div class="col-sm-12 text-center">
                            <a href="##" class="get-started" data-toggle="modal" data-target="#leadscon-email">Request More Info</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-wrapper feature-main leadscon-feature" id="feature-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4 class="title-common-page">Here are a few features of our <br> AI + SMS marketing flow that got you here.</h4>
            </div>
        </div>
    </div>

    <div class="feature-white">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row feature-items">
                        <div class="col-sm-4">                    
                            <div class="wrap-feature-item">
                                 <!---feature-demo1.png templates-img.png --->
                                 <div class="text-center hold-img">
                                    <img class="feature-img" alt="" src="/public/sire/images/leadscon/img_1.png">
                                 </div>                     
                            </div>
                        </div>
                        
                        <div class="col-sm-8">
                            <div class="wrap-feature-item feature-item-info">
                                <div class="inner-info">                        
                                    <div class="newfeature-title">Natural Language Processing (NLP)</div>

                                    <div class="newfeature-content">
                                        
                                        <p class="hasBold">                         
                                            When you texted the keyword to the shortcode at Leadscon, you were asked if you wanted to meet with our team. Most SMS platforms would require a Yes or No. With Sire, you could have answered with common language responses such as Sure, Nope, Yup, Nah, or even Okey Dokey and our integrated AI would've understood your reply.                    
                                        </p>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="feature-white">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row feature-items">  
                        <div class="col-sm-4 col-sm-push-8">
                            <div class="wrap-feature-item">
                                <div class="text-center hold-img">
                                    <img class="feature-img" alt="" src="/public/sire/images/leadscon/img_2.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8 col-sm-pull-4">
                            <div class="wrap-feature-item feature-item-info">
                                <div class="inner-info">
                                    <span class="anchor" id="feature1"></span>
                                    <div class="newfeature-title">Rules Engine</div>
                                    <div class="newfeature-content">
                                        
                                        <p class="hasBold">                         
                                            You responded that you didn't want to meet in-person at the show. (Ouch! That hurt.) Yet here we are. Our rules engine actually knew exactly where to guide you so we'd meet again. It's not serendipity that we're back together again. It's AI. We can help you connect with customers in the same way.
                                        </p>                           
                                    </div>
                                </div>
                            </div>                          
                        </div>              
                    </div>
                </div>
            </div>          
        </div>
    </div>

    
    <div class="feature-white">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row feature-items">
                        <div class="col-sm-4">
                            <div class="wrap-feature-item">
                                <div class="text-center hold-img">
                                     <img class="feature-img" alt="" src="/public/sire/images/leadscon/img_3.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="wrap-feature-item feature-item-info">
                                <div class="inner-info">
                                    <span class="anchor" id="feature2"></span>
                                    <div class="newfeature-title">Intelligent Conversational Interface (ICI)</div>
                                    <div class="newfeature-content">
                                        <p class="hasBold">
                                            Want to know something cool about our system that you probably didn't catch? You could've texted the keyword in a variety of ways and our AI platform would have known exactly what you meant. "marketing", Marketing and 'Marketing" all would have worked.
                                        </p>                            
                                    </div>
                                </div>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="feature-white">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row feature-items">
                        <div class="col-sm-4 col-sm-push-8">
                            <div class="wrap-feature-item">
                                <div class="text-center hold-img">
                                    <img class="feature-img" alt="" src="/public/sire/images/leadscon/img_4.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8 col-sm-pull-4">
                            <div class="wrap-feature-item feature-item-info">
                                <div class="inner-info">
                                    <span class="anchor" id="feature3"></span>
                                    <div class="newfeature-title" >Automated Enterprise Application Integration (EAI)</div>

                                    <div class="newfeature-content">
                                        <p class="hasBold">
                                            With our EAI, we were able to push 3rd party email and text notifications 
                                            to our team in real-time so they knew who to meet and where to meet them. We didn't miss a single meeting. Learn how you can leverage this feature 
                                            in your company.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
    <div class="feature-white last-feature-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row feature-items">            
                        <div class="col-sm-4">    
                            <div class="wrap-feature-item">
                                <div class="text-center hold-img">
                                    <img class="feature-img" alt="" src="/public/sire/images/leadscon/img_5.png">
                                </div>
                            </div>                                            
                        </div>
                        
                        <div class="col-sm-8">
                            <div class="wrap-feature-item feature-item-info">
                                <div class="inner-info">
                                    <span class="anchor" id="feature4"></span>
                                    <div class="newfeature-title" >List Building & Storage</div>

                                    <div class="newfeature-content">    
                                        <p class="hasBold">                         
                                            Data is valuable. Our AI integration helps you gain more data compared to the basic SMS competitors out there. We were able to build quite a list of leads at the show. All helped along by our ICI platform. Don't worry... we hate SPAM as much as you so we won't bombard you. Promise!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="leadscon-block4">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="inner-leadscon-block4 leadscon-besmart">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h4 class="title">Be Smarter than your competition</h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-phone text-center">
                                <h4 title="Our competitors without AI integration"><i>Our competitors without AI integration</i></h4>
                                <img class="img-responsive extend-img" src="/public/sire/images/leadscon/phone_1.png" alt="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="box-phone text-center">
                                <h4 title="Sire's Intelligent Communication Interface (ICI)"><i>Sire's Intelligent Communication Interface (ICI)</i></h4>
                                <img class="img-responsive extend-img" src="/public/sire/images/leadscon/phone_2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-block-1 leadscon-class">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h4 class="title-common-page">Built for the Enterprise Class. Tailored for SMBs.</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="inner-home-block-1">
                    <div class="row">
                        <div class="col-sm-4">
                            <figure class="row row-small home-block-1-item">
                                <div class="col-xs-3 col-md-3">
                                    <div class="img">
                                        <img class="img-responsive" src="/public/sire/images/affordable-image.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-9 col-md-9">
                                    <div class="info">
                                        <h4>Priced for Starups & Solopreneurs</h4>
                                    </div>
                                </div>
                            </figure>
                        </div>

                        <div class="col-sm-4">
                            <figure class="row row-small row-small home-block-1-item">
                                <div class="col-xs-3 col-md-3">
                                    <div class="img">
                                        <img class="img-responsive" src="/public/sire/images/simple-image.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-9 col-md-9">
                                    <div class="info">
                                        <h4>Easy to use for SMBs & Marketers</h4>
                                    </div>
                                </div>
                            </figure>
                        </div>
                        
                        <div class="col-sm-4">
                            <figure class="row row-small home-block-1-item">
                                <div class="col-xs-3 col-md-3">
                                    <div class="img">
                                        <img class="img-responsive" src="/public/sire/images/effective-image.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-9 col-md-9">
                                    <div class="info">
                                        <h4>Created for Agencies & Enterprise</h4>
                                    </div>
                                </div>
                            </figure>
                        </div>

                        <div class="col-sm-12 text-center">
                            <a href="#" class="get-started" data-toggle="modal" data-target="#leadscon-email">Request More Info</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</section>

<section class="how-block-gal leadscon-gal">
    <div class="container-fluid">
        <div class="inner-how-block-gal">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="title">Just a few examples of how you can use SMS marketing.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="inner-how-block-gal">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-1-1.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Reminders, alerts, announcements</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-2.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Build your customer list</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-3-1.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Customer service, feedback, surveys</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-4.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Text-to-win ads</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-5-1.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Coupons, promotions, specials</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="gal-item">
                        <div class="square">
                            <div class="inner-square">
                                <img class="img-responsive extend-img" src="/public/sire/images/how-it-work/gal-6.png" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <h4>Information, Education, Instruction</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="##" class="get-started" data-toggle="modal" data-target="#leadscon-email">Let's get started!</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="leadscon-block4 leadscon-more-future bg-white">
    <div class="container">
        <div class="inner-leadscon-block4">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h4 class="title">More Features & Benefits</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="nav-list-three none-center">
                                <li><i class="fa fa-circle"></i> Quick start templates</li>
                                <li><i class="fa fa-circle"></i> Customer Service AI automation</li>
                                <li><i class="fa fa-circle"></i> Customer Loyalty</li>
                                <li><i class="fa fa-circle"></i> Compliance and Privacy</li>
                                <li><i class="fa fa-circle"></i> Message Personalization </li>
                                <li><i class="fa fa-circle"></i> Automate repetitive inquiries </li>
                            </ul>
                        </div>

                        <div class="col-sm-6">
                            <ul class="nav-list-three none-center space-list">
                                <li><i class="fa fa-circle"></i> Over the Top Applications</li>
                                <li><i class="fa fa-circle"></i> Marketing, promotions</li>
                                <li><i class="fa fa-circle"></i> Content distribution </li>
                                <li><i class="fa fa-circle"></i> Transactional SMS</li>
                                <li><i class="fa fa-circle"></i> Surveys</li>
                                <li><i class="fa fa-circle"></i> Billings transactions & Reciepts</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center leadscon-last-text"><span class="text-center">Sire Mobile 2017 | <a href="http://www.Siremobile.com" target="_blank" class="leadscon-sire-link">www.SireMobile.com</a></span></div>
    </div>
</section>

<!--- Email Modal --->
<div class="modal fade" id="leadscon-email" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content leadscon-modal-content">
      <div class="modal-body leadscon-modal-body">
        <!--- <button type="button" class="close leadscon-modal-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> --->
        <a class="leadscon-modal-close" tabindex="0" data-dismiss="modal" aria-label="Close"><svg viewBox="0 0 10.6 11"><polygon points="6.3,5.4 10.4,1.4 9.2,0.3 5.3,4.2 1.3,0.3 0.2,1.4 4.3,5.4 0.2,9.5 1.3,10.7 5.3,6.8 9.2,10.7 10.4,9.5 "></polygon></svg></a>
        <div class="modal-body-inner row leadscon-modal-body-inner">
            <form name="leadscon-request" id="leadscon-request" class="col-sm-12" autocomplete="off">

                <div class="col-sm-4 col-xs-12">
                    <div class="leadscon-sirelogo">
                    </div>

                    <div class="leadscon-detail">
                        Send us your contact info and a Enterprise Campaign Consultant will contact you asap.
                    </div>
                    
                    <div class="leadscon-detail">
                        Don't worry, our experts aren't those annoying, high-pressure sales guys.
                    </div>
                </div>

                <div class="col-sm-8 col-xs-12">
                    <div class="form-group leadscon-letsconnect">
                       <strong>Let's connect and discuss how AI + SMS can help grow your business. We'd love to help.</strong>
                    </div>
                    <br>
                    <div class="form-group leadscon-form-input">
                        <input type="text" class="form-control validate[required,custom[email]]" data-prompt-position="topLeft:100" id="leadscon-mail" placeholder="E-mail">
                    </div>

                    <div class="text-left leadscon-validate-email hidden">
                        <label class="text-left"><i class="fa fa-exclamation-circle"></i><span>Please enter your E-mail</span></label>
                    </div>

                    <div class="form-group leadscon-form-input">
                        <input type="text" class="form-control" data-prompt-position="topLeft:100" id="leadscon-name" placeholder="Name">
                    </div>

                    <div class="form-group leadscon-form-input leadscon-phonenumber">
                        <input type="text" class="form-control validate[custom[usPhoneNumber]]" data-prompt-position="topLeft:100" id="leadscon-phone" placeholder="Phone Number">
                    </div>
                    <div class="form-group leadscon-form-button">
                        <a class="request-info green text-center leadscon-letsconnect-btn">Let's Connect!</a>
                    </div>
                    <div class="form-group text-center leadscon-pp">
                        <span class="leadscon-policy">Privacy Policy: We hate spam and promise to keep your email address safe</span>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/leadscon.js">
</cfinvoke>

<cfparam name="variables._title" default="Affordable Mobile Marketing & SMS Marketing">
<cfinclude template="../views/layouts/home.cfm">