

<!---
<div class="container">

         <section class="container-body">

            
            <h1>USA - Carrier Reach List</h1>
                        
            <p>In the United States, Sire has connectivity with the optimized gateways each operator reserves solely for application-to-person (A2P) message traffic. This should not be confused with services offering access to interoperator gateways used for peer-to-peer messaging—the sending of A2P traffic on those connections is strictly against market regulations in the United States.</p>
                               
               
	</section>

</div>
--->



<div class="container main-container">

	<!---<h2>Learning and Support</h2>--->

	<ul class="nav nav-pills">
        
        <li class="dropdown pull-right">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Learning and Support<b class="caret"></b></a>
            <ul class="dropdown-menu">
	            <li><a href="#" data-target="#carousel-quickstart" data-slide-to="0">Welcome</a></li>
                <li><a href="#" onclick="goToSlideName('GuidedTour')" href="javascript:void(0);">Guided Tour</a></li>
                <li><a href="#" onclick="goToSlideName('SampleProgram')" href="javascript:void(0);">Sample Program</a></li>
                <li><a href="#" onclick="goToSlideName('Terminology')" href="javascript:void(0);">Terminology</a></li>
                
               <!--- 
			    <li><a href="#" data-target="#carousel-quickstart" data-slide-to="0">Campaigns</a></li>
                <li><a href="#" data-target="#carousel-quickstart" data-slide-to="0">Lists</a></li>
                <li><a href="#" data-target="#carousel-quickstart" data-slide-to="0">Templates</a></li>
                <li><a href="#" data-target="#carousel-quickstart" data-slide-to="0">API</a></li>
				--->
            </ul>
        </li>
    </ul>
    
    
    
	<div id="carousel-quickstart" class="carousel slide">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-quickstart" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-quickstart" data-slide-to="1"></li>
			<li data-target="#carousel-quickstart" data-slide-to="2"></li>
            <li data-target="#carousel-quickstart" data-slide-to="3"></li>
            <li data-target="#carousel-quickstart" data-slide-to="4"></li>
            <li data-target="#carousel-quickstart" data-slide-to="5"></li>
            <li data-target="#carousel-quickstart" data-slide-to="6"></li>
            <li data-target="#carousel-quickstart" data-slide-to="7"></li>
            <li data-target="#carousel-quickstart" data-slide-to="8"></li>
            <li data-target="#carousel-quickstart" data-slide-to="9"></li>
            <li data-target="#carousel-quickstart" data-slide-to="10"></li>
            <li data-target="#carousel-quickstart" data-slide-to="11"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
		
        
          
            <!-- First slide -->
			<div class="item active">				                                   
				<div class="carousel-caption carousel-caption-bottom">
				                
	                <h3 data-animation="animated bounceInLeft" style="text-align:right;">
						<p>Sire SMS Marketing</p>
                        <p><b>Learning and Support</b></p>
					</h3>	
                    
                    <p class="" data-animation="animated bounceInDown">
                        <img class="" src="/public/sire/images/tutorial-phone-220.png" style="float:right;">
                    </p>
                                   
				</div>
			</div> <!-- /.item -->
            
            <!--- Guided Tour --->
			<div class="item" id='GuidedTour'>
				                                   
				<div class="carousel-caption">				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>Guided Tour</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
	                    <img class="" src="/public/sire/images/tour.gif" style="">						
					</h3>				
                    
                    <h3>Build your first SMS Campaign on Sire</h3>
				</div>
                
			</div> 
            
           	<div class="item">
				                                   
				<div class="carousel-caption">				
                	<!---
						Learn more detail... 
					
						Each campaign will have a system wide unique Id
						Can be called by Id from API
						KEYWORDs will trigger
						Blasts can send
											
					--->
                
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>Create Campaign</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						Everything in Sire revolves around Campaigns. 
					</h3>				
                    
                    <h3 data-animation="animated zoomInUp" class="btn-line-height">After logging in click on <br/><button class="btn btn-larger btn-success-custom" id="btn-popup-campaign" style="margin-top:.4em !important;">Create campaign</button></h3>
				</div>
                
			</div> 
                        
            <div class="item">
            
            <!---
				
				A little more about 2x opt in 
			
				You can easily customize templates around your business 
				
				http://www.entrepreneur.com/article/236542
				
			--->
				                                   
                <div class="carousel-caption" style="text-align:center;">				
                        <h5 class="icon-container" data-animation="animated bounceInDown">
                            <b>Templates</b>
                        </h5>
                    
                        <h3 data-animation="animated bounceInLeft">
                            Next choose a template. <b>Templates</b> are pre-defined SMS campaigns to give you a jump start on many different tasks. 
                        </h3>				
                                
                        <div style="text-align:center; width:100%;">                                        
                            <div class="TPButton" style="">
            
                                <img class="" src="/public/sire/images/template-1-image.png" style="">
            
                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Build SMS List</h3>
            
                            </div>
                        </div>
                        
                        <h3 data-animation="animated zoomInUp" style="padding-top:0; margin-top:.2em;">Pick the "Build SMS List" <b>Template</b></h3>
                </div>
           </div>
                
           
           <div class="item">            
            <!---
				
				pick a keyword
				adjust content 
				
			--->
				                                   
                <div class="carousel-caption" style="text-align:center;">				
                        <h5 class="icon-container" data-animation="animated bounceInDown">
                            <b>Customize</b>
                        </h5>
                    
                        <h3 data-animation="animated bounceInLeft">
                            Add your own custom flair.
                        </h3>		
                        
                        
                         <div class="row">
                        	<div class="form-group clearfix control-point" id="cp1">
                        		<div class="col-sm-12">
                        			<div class="control-point-border clearfix">
                       
                       					<div class="col-sm-12">
                                       	 	<label class="control-point-label SamplePreviewText"># 1. First message to the end user. This question is to verify their request to join the list. </label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr class="hr0">
                                        <div class="col-sm-12 clearfix">
                                            <div class="control-point-body">
                                                 <div class="control-point-text SamplePreviewText Preview-Bubble Preview-You">GS Alert: You've chosen to receive SMART Project msgs. Reply YES to confirm, HELP for help or STOP to cancel. Up to 5 msg/mo. Msg&Data rates may apply</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                            	</div>        
                        	</div>
                        </div>
                       
                                             
                       
                    
                    		
                       
                </div>
            </div>    
          
           	<div class="item">
            
            <!---
				Where to Publish
					Packaging
					Register
					Table
					Counter
					Email Tag Lines
					Signature Line
					Web Site
					Menu
					Front Door
					Display Ads
					http://www.entrepreneur.com/article/236542
					
					
					
					A good list of potential customers and prospects has been a terrific marketing weapon and is essential in the new world of inbound, permission-based marketing. 
					
								
			--->
                        
            	<div class="carousel-caption" style="text-align:center;">				
                    <h3 class="" data-animation="animated bounceInDown">
                        Choose Blast OR Give your campaign a Keyword
                    </h3>
                                        
                    <h3 data-animation="animated bounceInLeft">
	                    <img class="" src="/public/sire/images/learning/keyword-or-blast.png" style="">						
					</h3>		
                
                	<h3 data-animation="animated zoomInUp">
						If you want your customers to reachout to you, give them a <b>Keyword</b>. Otherwise you can blast your survey to any of your SMS marketing lists.
					</h3>				
                            
                   
				</div>
			
            </div>
                          
           	<div class="item">
            
            <!---
				Where to Publish
					Packaging
					Register
					Table
					Counter
					Email Tag Lines
					Signature Line
					Web Site
					Menu
					Front Door
					Display Ads
					http://www.entrepreneur.com/article/236542
					
					
					
					A good list of potential customers and prospects has been a terrific marketing weapon and is essential in the new world of inbound, permission-based marketing. 
					
								
			--->
                        
            	<div class="carousel-caption" style="text-align:center;">				
                    <h5 class="icon-container" data-animation="animated bounceInDown">
                        <b>Publish</b>
                    </h5>
                
                	<h3 data-animation="animated bounceInLeft">
						Now that you have a Keyword - Get the (Key)word out there.
					</h3>				
                            
                   
				</div>
			
            </div>
            
            
              <!--- Sample Program --->
			<div class="item" id="SampleProgram">
				                                   
				<div class="carousel-caption">				
                    
                
                	<h3 data-animation="animated bounceInLeft">
						Sample Program
					</h3>				
                    
                    
                    <div style="border:#999 solid 1px; margin-bottom:100px;">                                         
                        
                        <div style="text-align:left; padding: 1.2em;">
    	                    <img class="" src="/public/sire/images/client_logo.png" style="width:150px;">
                            <h3 style="margin-top:.2em; padding-top:0;">Helping You Prepare for the Road Ahead</h3>                            
                        </div>
                        
                        
                        <div style="background-color:#648ba4; padding:1em;">
                        	<p>Take our new investor survey to get started.</p>
	                        <p style="margin-bottom:0;">On your mobile phone<br/>Text the keyword: <b>Profile</b><BR/> To: <b>39492</b></p>
                        </div>
                    
                    </div>              
                    
				</div>
			</div> <!-- /.item -->
           
            <!--- Terminology --->
			<div class="item" id="Terminology">				                                   
				<div class="carousel-caption">
				
                    <h2 class="" data-animation="animated bounceInDown">
                        <b>Terminology</b>
                    </h2>
                
                	<h3 data-animation="animated bounceInLeft">
                    
                    	<!--- http://media.photobucket.com/user/tele5/media/smiley/yawn.gif.html?filters[term]=yawn%20smiley&filters[primary]=images&filters[secondary]=videos&sort=1&o=12 --->
						<img class="" src="/public/sire/images/yawn.gif" style="float:right;">
                        Just a couple of quick minutes on some of the more common terms you will run accross in SMS Marketing. Then we will have you on your way. Promise.
                        
					</h3>				
                    
                    <!---<button class="btn btn-primary btn-lg" data-animation="animated zoomInUp" data-slide-to="5" data-target="#carousel-quickstart">Skip to Next Section</button>--->
				</div>
			</div> <!-- /.item -->
            
             
                        
            <!-- slide -->
			<div class="item">				                                   
				<div class="carousel-caption">
				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <span class="glyphicon glyphicon-phone"></span>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>SMS</b> stands for <b>Short Message Service</b> and is also commonly referred to as a "text message". With <b>SMS</b>, you can send a message of up to 160 characters to another device. Longer messages will automatically be split up into several parts.
					</h3>				
                    
                                        
                    <h3 data-animation="animated zoomInUp">SMS/Text is still the most frequently used app on a smartphone with 97% of owners using it at least once a week or more.</h3>
                    
                    
				</div>
			</div> <!-- /.item -->
            
            
                                    
                                  
          <!---<!--  -->
			<div class="item">
				                                   
				<div class="carousel-caption">
				
                
                 	<h3 data-animation="animated bounceInDown">Lesson 1 - Terminology</h3>
                 
                 	<ul>
                    	<li>SMS</li>
                        <li>MO</li>
                        <li>MT</li>
                        <li>Short Code</li>
                        <li>Long Code</li>
                        <li>Keyword</li>
                        <li>Opt In</li>
                        <li>Double Opt In</li>
                        <li>Opt Out</li>
                        <li>List</li>
                        <li>Blast</li>
                        <li>Interactive Campaign</li>
                    
                    </ul>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>MT</b> stands for <b>Mobile Terminated</b> and is the process where an end user sends a text message from their phone device to another device address. You can send an <b>MO</b> to another Phone Number OR when marketing to US based mobile phones you can send an MO to a short code.
					</h3>				
                    
                    <h3 data-animation="animated zoomInUp">Think of MT as a message sent <b>to</b> a phone.</h3>
				</div>
			</div> <!-- /.item -->--->
			              
            <!--  -->
			<div class="item">				                                   
				<div class="carousel-caption">
				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>MO</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>MO</b> stands for <b>Mobile Originated</b> and is the process where an end user sends a text message from their phone device to another device address. You can send an <b>MO</b> to another Phone Number OR when marketing to US based mobile phones you can send an MO to a short code.
					</h3>	
                
	                <h3 data-animation="animated zoomInUp">Think of MO as a message sent <b>from</b> a phone.</h3>
                    			
				</div>
			</div> <!-- /.item -->
                        
            <!--  -->
			<div class="item">
				                                   
				<div class="carousel-caption">				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>MT</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>MT</b> stands for <b>Mobile Terminated</b> when an application transmits a message to a mobile phone. The message can be sent from another phone or by a software application.  When messaging from your business to US based mobile phones you should always send an <b>MT</b> from a short code.
					</h3>				
                    
                    <h3 data-animation="animated zoomInUp">Think of MT as a message sent <b>to</b> a phone.</h3>
				</div>
			</div> <!-- /.item -->
            
            
            <!--  -->
			<div class="item">
				                                   
				<div class="carousel-caption">				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>Short code</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>Short codes</b> are special telephone numbers, significantly shorter than full telephone numbers, that can be used to address SMS messages. Short codes are designed to be easier to read and remember than normal telephone numbers.
					</h3>				
                    
                    <h3 data-animation="animated zoomInUp">Think of a short code as an address to/from your business for SMS</h3>
				</div>
			</div> <!-- /.item -->
            
            <!--  -->
			<div class="item">
				                                   
				<div class="carousel-caption">				
                    <h3 class="icon-container" data-animation="animated bounceInDown">
                        <b>Keywords</b>
                    </h3>
                
                	<h3 data-animation="animated bounceInLeft">
						<b>Keywords</b> are special mobile originated (MO) messages sent to a Short Code. Keywords are used to trigger new Sire SMS sessions. These sessions can include simple response messages, list building via opt in confirmation, surveys, meet-ups, drip marketing, and additional two-way data exchange and capture. 
					</h3>				
                    
                    <h3 data-animation="animated zoomInUp">Think of a keyword as a button that you push from SMS</h3>
				</div>
			</div> <!-- /.item -->
                                
                        
           
    <!---        
	
	
	
	To protect consumers from SPAM , the wireless carriers set up a group called the CTIA to enforce SMS marketing practices that are in the best interests of the consumer. To do this, the CTIA carries out audits on SMS programs based on the rules found in their CTIA Short Code Compliance Handbook. If a text messaging campaign is found to be in violation of any of the guidelines in the CTIA Short Code Compliance Handbook, the text messaging campaign can be deactivated by the wireless carriers.
	
	<div style="float:left; background-color:#648ba4; width:100px; height:170px; margin-right:1em;"></div>
	
	<!-- next slide -->
			<div class="item deepskyblue">
				
				<div class="carousel-caption">
					<h3 data-animation="animated bounceInLeft">
						This is the caption for slide 1
					</h3>
					<h3 data-animation="animated bounceInRight">
						This is the caption for slide 1
					</h3>
					<button class="btn btn-primary btn-lg" data-animation="animated zoomInUp">Button</button>
				</div>
			</div> <!-- /.item -->
            
			<!-- Second slide -->
			<div class="item skyblue">
				<div class="carousel-caption">
					<h3 class="icon-container" data-animation="animated bounceInDown">
						<span class="glyphicon glyphicon-heart"></span>
					</h3>
					<h3 data-animation="animated bounceInUp">
						This is the caption for slide 2
					</h3>
					<button class="btn btn-primary btn-lg" data-animation="animated zoomInRight">Button</button>
				</div>
			</div><!-- /.item -->
			
			<!-- Third slide -->
			<div class="item darkerskyblue">
				<div class="carousel-caption">
					<h3 class="icon-container" data-animation="animated zoomInLeft">
						<span class="glyphicon glyphicon-glass"></span>
					</h3>
					<h3 data-animation="animated flipInX">
						This is the caption for slide 3
					</h3>
					<button class="btn btn-primary btn-lg" data-animation="animated lightSpeedIn">Button</button>
				</div>
			</div><!-- /.item -->
			--->
		
		</div><!-- /.carousel-inner -->

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-quickstart" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-quickstart" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div><!-- /.carousel -->

</div><!-- /.container -->


<cfparam name="variables._title" default="Learning And Support - Sire">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

