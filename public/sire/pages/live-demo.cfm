<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/live-demo.css">
</cfinvoke>

<header class="home header-demo header-banners">
   
   <section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                        <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>
   
   
   <section class="landing-jumbo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="banner-title-big hidden-xs">How to Use Sire</h1>

                    <h3 class="banner-title-small visible-xs">How to Use Sire</h3>

                    <!--- <p class="banner-sub-title hidden-xs"><i>Via Over the Top API</i></p> --->
                             
                    <!--- <h1 class="marketing_title_big hidden-xs">Demo Interactions</h1>
                    <h2 class="marketing_title visible-xs">Demo Interactions</h2>
                                            
                    <h4 class="hidden-xs"><i>Via Over the Top API</i></h4> --->
                        
                </div>	
            </div>	
            
            <div class="row hidden-xs hidden-sm">
                <div class="scroll-down-spacer_3"></div>
            </div>
            
            <div class="row visible-xs visible-sm">
                <div class="scroll-down-spacer_1"></div>
            </div>
            
            <div class="row">
                <a href="#feature-main">
                <div class="scroll-down">
                    <span>
                        <i class="fa fa-angle-down fa-2x"></i>
                    </span>
                </div>
                </a>
            </div>
                
        </div>
    </section>
        
</header>


<section class="main-wrapper">


    <!--- <div class="container text-center" id="feature-main">
           
        <div class="row">
            <div class="container-fluid">
               <h2 class="heading-home">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis deserunt quis ullam alias rem. Aliquid dolorum ipsum incidunt cupiditate deleniti illum quia sequi, labore pariatur, veritatis, quis molestias ab debitis!</h2>
            </div>
        </div>                    
    
    </div> --->

    <div class="container video" style="text:center">
        <div class="embed-responsive embed-responsive-16by9">
            
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/W8vijCInaNE" allowfullscreen></iframe>
            
            <!--- <img src="/public/sire/images/video.png" style="width:100%"> --->
            <!--- <iframe width="560" height="315" src="https://www.youtube.com/embed/W8vijCInaNE" frameborder="0" allowfullscreen></iframe> --->
        </div>
    </div>

    <!--
    <div class="container-fluid">
               
        <div class="row live-demo-func">
            <div class="col-sm-12 text-center clearfix">
                <div id="QAScreen">
                    <img class="ScreenBackground" src="/public/sire/images/iphone6base.png">
                    <div id="PhoneScreenArea">
                        <div class="bubble-tooltip">
                            <div class="bubble bubble-live-demo">Click this menu bar to choose from a list of sample keywords.</div>
                        </div>
                        <div style="clear:both"></div>
                        <div id="SMSToAddress"><input id="inpSMSToAddress" name="inpSMSToAddress" placeholder="To:" size="20" readonly value="DemoSire"><input id="SMSClear" name="SMSClear" placeholder="" size="" readonly value="X"></div>
                        <div id="SMSHistoryScreenArea"></div>
                        <div id="SMSTextInputAreaContainer">
                            <textarea id="SMSTextInputArea" name="SMSTextInputArea" maxlength="160"></textarea>
                        </div>
                        <a class="button filterButton small SMSSendButton btn btn-primary btn-primary-custom" id="SMSSend">Send</a>
                    </div>
                    <img class="ScreenBackgroundGlare" src="/public/sire/images/iphonebaseglareonly.png">
                    <div id="PhoneMenuToggle" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                    <div id="PhoneMenuArea" class="w0">
                        <div id="PhoneMenuHelp">Select from keywords listed bellow</div>
    
                        <div id="PhoneMenuAreaInner">
                        <button class="btn btn-link DemoLink ottdemokeyword" id="DemoKeyword" rel1="Welcome">Welcome</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="SurveyCSS1" rel1="Survey">Survey Sample</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="RedReg" rel1="Branch">Natural Human Language</button>
        
                        <button class="btn btn-link DemoLink ottdemokeyword" id="money" rel1="Money">Digital Companion</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="MFAKW" rel1="MFA">Multi Factor Auth</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="DoubleOpt" rel1="2Opt">Double Opt In</button>
        
                        <button class="btn btn-link DemoLink ottdemokeyword" id="DataCapture" rel1="Capture">Data Capture</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="RemainKeyword" rel1="Remain">Deductible Remaining</button>
                        <button class="btn btn-link DemoLink ottdemokeyword" id="EnergyKeyword" rel1="Energy">Outage Alerts</button>
                        </div>
                    </div>
                </div>
                <div class="tooltip left tooltip-success live-demo-iphone-input-tip" role="tooltip">
                  <div class="tooltip-arrow"></div>
                  <div class="tooltip-inner">
                    Tip: You can input keywords and answers in the box to the right and then hit the send button.
                  </div>
                </div>
                <div class="tooltip right tooltip-success live-demo-iphone-menu-tip" role="tooltip">
                  <div class="tooltip-arrow"></div>
                  <div class="tooltip-inner">
                    Tip: Use the menu button to try sample keywords.
                  </div>
                </div>
            </div>
            <div class="col-sm-5 col-md-6 hidden">
                <p class="module-title">Try it!</p>
                <p class="module-intro">
                    These buttons will trigger sample interactions.<br> 
                    You may also type the keywords directly.
                </p>
                
                <div class="form-group btn-group-live-demo">
                    
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="DemoKeyword" rel1="Welcome">Welcome</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="SurveyCSS1" rel1="Survey">Survey</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="RedReg" rel1="Branch">Branch RegEx</button>
    
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="money" rel1="Money">Digital<br>Companion</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="MFAKW" rel1="MFA">Multi Factor<br>Auth</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="DoubleOpt" rel1="2Opt">Double<br>Opt In</button>
    
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="DataCapture" rel1="Capture">Data<br>Capture</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="RemainKeyword" rel1="Remain">Deductible<br>Remaining</button>
                    <button class="btn btn-primary btn-back-custom DemoLink ottdemokeyword" id="EnergyKeyword" rel1="Energy">Outage<br>Alerts</button>
                    
                </div>
                    
                    
                    <div class="form-right-portal hidden">
                    
                        <div id="TimeMachineOptions" style="padding-top:20px; padding-bottom:0px; clear:both">
                            <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="TimeMachineOptionsLink">Time Machine</span></label>
                        </div>
                    
                        <div id="TimeMachineContainer" style="display:none;">
                    
                            <div id="IntervalOptions" class="inputbox-container" style="width:400px;">
                                <label>The next response has been queued up for delivery after:</label>
                                <div id="INTERVALSCHEDULED" class="QASmall">NA - MM:DD:YYYY</div>
                    
                                <a class="bluebuttonAuto small2" id="TIMEMACHINECHECK">
                                    <div class="hide-info showToolTip">Check Queue</div>
                                    <div class="tooltiptext">
                                        <em>Check Queue</em>
                                        Check when next response is scheduled to run - if there is one. Use Time Machine button separately to force any queued responses to run now.
                                    </div>
                                </a>
                    
                                <a class="bluebuttonAuto small2" id="TIMEMACHINE">
                                    <div class="hide-info showToolTip">Run Time Machine</div>
                                    <div class="tooltiptext">
                                        <em>Run Time Machine</em>
                                        Force any queued responses to run now.
                                    </div>
                                </a>
                            </div>
                    
                        </div>
                    
                        <div id="CPOptions" style="padding-top:20px; padding-bottom:0px; clear:both;">
                            <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="CPOptionsLink">Starting Control Point</span></label>
                        </div>
                    
                        <div id="StartCPContainer" style="display:none;">
                    
                            <div class="inputbox-container">
                                <label for="inpCP">Starting Control Point</label>
                                <input id="inpCP" name="inpCP" placeholder="Enter CP" size="10" value=""/>
                    
                                <label for="inpCPKeyword">Program Start Keyword</label>
                                <input id="inpCPKeyword" name="inpCPKeyword" placeholder="Enter Keyword" size="20" value=""/>
                    
                                <a class="bluebuttonAuto small2" id="STARTCP">
                                    <div class="hide-info showToolTip">Start IC at Control Point</div>
                                    <div class="tooltiptext">
                                        <em>Start IC at Control Point</em>
                                        This will start a new Interactive Campaign beginning at the specified Control Point. All running campaigns on this device address will be stopped.
                                    </div>
                                </a>
                    
                            </div>
                        </div>
                    
                        <div id="JSONOptions" style="padding-top:20px; padding-bottom:0px; clear:both;">
                            <label class="qe-label-a">Advanced - <span style="text-decoration:underline; cursor:pointer;" id="JSONOptionsLink">JSON CDFs</span></label>
                        </div>
                    
                        <div id="JSONOptionsContainer" style="display:none;">
                            <div class="inputbox-container">
                                <label for="inpJSON">JSON list of Custom Data Fields</label>
                                <textarea id="inpJSON" name="inpJSON" placeholder="Enter Valid JSON" value="" style="padding:5px; width:100%; height:445px;"></textarea>
                            </div>
                        </div>
                    
                    </div>
                    
    
            </div>
        </div>
    </div>
    -->


</section>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.autogrow-textarea.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/live_demo.js">
</cfinvoke>
	
<cfparam name="variables._title" default="Live Demo - Sire">
<cfparam name="variables.body_class" default="body-live-demo">
<cfinclude template="../views/layouts/home.cfm">


<cfinclude template="../../../session/cfc/csc/constants.cfm">
<cfinclude template="../../paths.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>
<cfset inpContactString = "Demo_#getSecurePassword#_#LSDateFormat(now(), 'yyyy-mm-dd')##LSTimeFormat(now(), 'HH:mm:ss')#"/>

<cfoutput>
<input type="hidden" id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" value="#inpContactString#"/>
</cfoutput>

<script type="text/javascript" language="javascript">
    $('.li-live-demo').addClass('active');
	<!--- Track time traveled so far so display gives user an semi-accurate feedback of which message when. --->
	var inpTimeTraveledSoFarInMinutes = 0;
	var AllowBlanks = 0;
	
	$(function() {	
		
		<cfset inpFormData = StructNew() />
		<cfset inpFormData.inpFirstName = "Lee" />
		<cfset inpFormData.inpLastName = "Peterson" />
		<cfset inpFormData.inpRepName = "Mary" />
		<cfset inpFormData.inpOneTimePassword = "85490527" />
		<cfset inpFormData.inpBatchIdAppt = "7451284" />
		<cfset inpFormData.inpContactStringAppt = "9493945710" />
		<cfset inpFormData.inpContactTypeIdAppt = "1" />
		<cfset inpFormData.inpStart = "2016-10-06 13:00" />
		<cfset inpFormData.inpConfirm = "1" />
		<cfset inpFormData.inpCurrEMail = "someone@somewhere.com" />
		<cfset inpFormData.inpAsOfDate = "#LSDateFormat(now(), 'yyyy-mm-dd')#" />
		<cfset inpFormData.inpDeductibleInMet = "$350" />
		<cfset inpFormData.inpDeductibleIn = "$500" />
		<cfset inpFormData.inpDeductibleOutMet = "$1350" />
		<cfset inpFormData.inpDeductibleOut = "$1500" />
		
		
		
		
		<cfoutput> 
      		var inpFormDataJSON = '#SerializeJSON(inpFormData)#';
		</cfoutput> 
		 
		$('#inpJSON').html(inpFormDataJSON);
		
		<!--- Turns off caching of AJAX requests --->
		$.ajaxSetup({ cache: false });
		
		$('#SMSTextInputArea').autogrow();
						
		<!--- Resize history screen based on size of text input --->
		$('#SMSTextInputArea').resize(function() {
						
		  	$("#SMSHistoryScreenArea").height($('#PhoneScreenArea').height() - $('#SMSTextInputAreaContainer').height() - 31 - $('#SMSToAddress').height());
			$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
			
		});

	
		$('#STARTCP').click(function(){
			SetStartCP();
		});
		
		
		
		$("#CPOptionsLink").click(function(){
			
			$('#StartCPContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
  
		});	
		
		$("#JSONOptionsLink").click(function(){
			
			$('#JSONOptionsContainer').toggle('slow', function() {
				<!--- Animation complete. --->
				$('#MenuTipSlideMiddle').toggle();
				
			  });
  
		});		
										
		
		$("#TimeMachineOptionsLink").click(function(){
			
			$('#TimeMachineContainer').toggle('slow', function() {
				<!--- Animation complete. --->
			  });
  
		});		
		

		<!--- Action to perform on SMS send--->
		$('#SMSSend').click(function(){
			var inpContactString = $('#inpContactString').val(); 
			if(inpContactString.length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			var SMSTextInputArea = $('#SMSTextInputArea').val();
			
			if(AllowBlanks == 0)
				if(SMSTextInputArea.length == 0 )
				{
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jConfirm("Text to send has not been entered.\n"  + "Text to send can not normally be blank. Hit OK if you want to allow future blanks to be sent in this session." + "\n", "Warning!", function(result) { AllowBlanks = 1 } );										
					return false;						
				}
			
			var inpSMSToAddress = $('#inpSMSToAddress').val();		
			if(inpSMSToAddress.length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}			

			<!--- Post text that is sent from me class --->
			if(SMSTextInputArea == "")
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + '&nbsp;' + '</div>');
			else
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + escapeHtml(SMSTextInputArea) + '</div>');
					
			$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
						
					
			GetResponseSimple(inpSMSToAddress, SMSTextInputArea, inpContactString, 0 );
			
		
			
			
		});	
		
		$('#TIMEMACHINE').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}		
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			
			TimeMachineProcess($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
			
			
		});	
		
		$('#TIMEMACHINECHECK').click(function(){			
				
			if($('#inpContactString').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Device Address: Address has not been entered.\n"  + "Device Address: Address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
				
				
			if($('#inpSMSToAddress').val().length == 0 )
			{
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("SMS TO: address has not been entered.\n"  + "SMS TO: address can not be blank." + "\n", "Warning!", function(result) { } );										
				return false;						
			}	
			
			CheckNextResponse($('#inpSMSToAddress').val(), '', $('#inpContactString').val());
			
			
		});	
		
		$('.ottdemokeyword').click(function(){			
			
			var inpContactString = $('#inpContactString').val();
			var inpSMSToAddress = $('#inpSMSToAddress').val();					
						
			<!--- Post text that is sent from me class --->
			if($(this).attr("rel1") == "")
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + '&nbsp;' + '</div>');
			else
				$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + $(this).attr("rel1") + '</div>');
					
			$("#SMSHistoryScreenArea").stop(true, true).animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
						
					
			GetResponseSimple(inpSMSToAddress, $(this).attr("rel1"), inpContactString, 0 );
			
			
			
		});	
				
		
		$("#SMSTextInputArea").keyup(function (e) {
			if (e.keyCode == 13) {
				$('#SMSSend').click();
			}
		});


		<!---
		$('#SMSClear').click(function(){	
			
			$('#SMSHistoryScreenArea').html("");
			$('#SMSTextInputArea').resize();
		});--->


		<!---CheckNextResponse($('#inpSMSToAddress').val(), '', $('#inpContactString').val());--->
		
		$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + 'Welcome' + '</div>');
		GetResponseSimple($('#inpSMSToAddress').val(), 'Welcome', $('#inpContactString').val(), 0 );
				
		
		<!--- skip force resize on page load --->
		<!---$('#SMSTextInputArea').resize();--->
				
	<!---	
		$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble me">' + 'Welcome' + '</div>');
		$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + 'Welcome to the <font style="color:#0085c8; font-family: &quot;Open Sans&quot;, sans-serif; font-weight:700;">S</font><font style="color:#ff9900; font-family: &quot;Open Sans&quot;, sans-serif; font-weight:700;">ire</font> interactive response engine demo. Try some of the sample interactions...' + '</div>');
	--->	
	
		<!---$('.EBMDialog').show();--->
		
		$('#MenuTipSlide').click(function(){
			$('#MenuTipSlide').hide();
		});
		
		
	<!---	setTimeout(function() {
			$('#MenuTipSlide').fadeOut('fast');
		},5000);
		--->
					
	});
	
	function MidStr(str, start, len)
        /***
                IN: str - the string we are LEFTing
                    start - our string's starting position (0 based!!)
                    len - how many characters from start we want to get

                RETVAL: The substring from start to start+len
        ***/
        {
                // Make sure start and len are within proper bounds
                if (start < 0 || len < 0) return "";

                var iEnd, iLen = String(str).length;
                if (start + len > iLen)
                        iEnd = iLen;
                else
                        iEnd = start + len;

                return String(str).substring(start,iEnd);
        }
	
	<!---Browser is caching identical ajax request	force unique id for keyword on statement re-push --->
	var inpflag = 0;
	var MaxStatements = 20;
	var TIMEZONEOFFSETPST = 0;
	
	function GetResponseSimple(inpShortCode, inpKeyword, inpContactString, inpOverRideInterval) {
			
							
		$.ajax({
		<!---url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   --->
		url: '<cfoutput>#rootUrlAPI#/webservice</cfoutput>/ebm/pdc/GetNextResponse',
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					username : 'demo.sire',
					password : 'A0DF875079D6267E05FC',
					inpKeyword : inpKeyword,
					inpShortCode : inpShortCode,
					inpContactString : inpContactString,
					inpNewLine : "<BR>",
					inpOverRideInterval : inpOverRideInterval,
					inpFormDataJSON : $('#inpJSON').val(),
					inpQATool : 1					
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if Warning --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0)
						{
							
							if(d.RESPONSE.length > 0)
							{	
							
								var ConcatenateOffFlag = 0;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.RESPONSE;		
									
									<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');--->
									<!--- Break message on space if within last 60 characters --->
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
										<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');	--->								
									}
									
									<!--- Move back to last space--->
									AmountToGrab = AmountToGrab - 1;
									
									<!---console.log('AmountToGrab=' + AmountToGrab);--->
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									<!---console.log(BuffStrOut);--->
									
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + BuffStrOut + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										<!--- Break message on space if within last 60 characters --->
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										}
										
										<!--- Move back to last space--->
										AmountToGrab = AmountToGrab -1;
													
										<!---console.log('AmountToGrab=' + AmountToGrab);--->
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										<!---console.log(BuffStrOut);--->
										CurrPos = CurrPos + AmountToGrab;
										
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + BuffStrOut + '</div>');									
									}
								
								}
								else
								{
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you" rel="' + d.RESPONSELENGTH + '">' + d.RESPONSE + '</div>');																		
								}
								
								$('#SMSTextInputArea').resize();																	
								<!--- Scroll to bottom on new data --->							
								$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
							}
							
							<!--- Get next Response--->
							if(d.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF')
							{
								EBMText = 'EBM - ' +  d.RESPONSETYPE;
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								if(inpflag < MaxStatements)
									GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
								
								<!---var t=setTimeout(function(){
										GetResponseSimple(inpShortCode, inpKeyword, inpContactString, 0);
									},1500)--->
																		
							}
							else if(d.RESPONSETYPE == 'AGENT')
							{
								d.RESPONSETYPE = '';
								
								inpflag++;
								
								<!--- 
									Now what? How do we make this page list for AAU message to the current device address?
									How to ignore messages aau message if device address is changed?
								--->
															
							}
							else if(d.RESPONSETYPE == 'INTERVAL')
							{																
								QueueNextResponse(inpShortCode, 'EBM - INTERVAL', inpContactString, d.INTERVALSCHEDULED, d.INTERVALEXPIREDNEXTQID)
																
								<!--- Update optional interval processing and display --->
								$('#INTERVALSCHEDULED').html(d.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.TIMEZONEOFFSETPST);
								$('#IntervalOptions').show();
							}
							else
							{
								<!--- Non repeat question type --->
								MaxStatements = inpflag + 20; 
								
							}							
						}
						else
						{
							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
						   var isTestKeyword = d.ISTESTKEYWORD;
								
								if(isTestKeyword == 1)
								{
									$("#isTestKeyword").val("0");
									$("#isEmitChatContent").val("0");
									$("#Keyword").val(inpKeyword);
								}
								else if(isTestKeyword == -1)
								{
									$("#isTestKeyword").val("1");
								}	
								else
								{
									$("#isTestKeyword").val("");
									$("#isEmitChatContent").val("");
								}
								
								var isEmitChatContent = $("#isEmitChatContent").val();
								isTestKeyword = $("#isTestKeyword").val();
								if(isEmitChatContent == 0 && isTestKeyword == 1 && inpKeyword != "")
								{
									$("#isEmitChatContent").val("1");
									$('#SMSTextInputArea').resize();
								}	 		
					$('#SMSTextInputArea').val('');							
			} 		
			
		});
		return false;
	}
	
	function TimeMachineProcess(inpShortCode, inpKeyword, inpContactString) {
	
		MaxStatements = inpflag + 20; 
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=TimeMachineGetNextResponseQA&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  	{ 					
					inpContactString : inpContactString,
					inpShortCode : inpShortCode,					
					inpNewLine : "<BR>",
					inpTimeTraveledSoFarInMinutes : inpTimeTraveledSoFarInMinutes
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if Warning --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
											
						if(CurrRXResultCode > 0 && typeof(d.GETRESPONSE.RESPONSE) != "undefined" )
						{
							if(d.GETRESPONSE.RESPONSE.length > 0 )
							{	
								var ConcatenateOffFlag = 0;
							
								inpTimeTraveledSoFarInMinutes = d.INPTIMETRAVELEDSOFARINMINUTES;
							
								if(ConcatenateOffFlag == 1)	
								{						
									var CurrPos = 0;
									var AmountToGrab = 160;								
									var BuffStr = d.GETRESPONSE.RESPONSE;		
									
									<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');--->
									<!--- Break message on space if within last 60 characters --->
									while(BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(AmountToGrab-2,AmountToGrab-1) != "") 
									{
										AmountToGrab = AmountToGrab - 1;
										<!---console.log('{' + BuffStr.substring(AmountToGrab-2,AmountToGrab-1) + '}');	--->								
									}
									
									<!--- Move back to last space--->
									AmountToGrab = AmountToGrab - 1;
									
									<!---console.log('AmountToGrab=' + AmountToGrab);--->
									var BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
																		
									<!---console.log(BuffStrOut);--->
									
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');								
									$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + BuffStrOut + '</div>');								
									
									CurrPos = CurrPos + AmountToGrab;
									
									while( BuffStr.length > CurrPos)
									{					
									
										AmountToGrab = 160;
										
										<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										<!--- Break message on space if within last 60 characters --->
										while(BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != " " && AmountToGrab > 100 && BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) != "")
										{
											AmountToGrab = AmountToGrab - 1;
											<!---console.log('{' + BuffStr.substring(CurrPos+AmountToGrab-2,CurrPos+AmountToGrab-1) + '}');--->
										}
										
										<!--- Move back to last space--->
										AmountToGrab = AmountToGrab -1;
													
										<!---console.log('AmountToGrab=' + AmountToGrab);--->
										BuffStrOut = MidStr(BuffStr, CurrPos, AmountToGrab);
										<!---console.log(BuffStrOut);--->
										CurrPos = CurrPos + AmountToGrab;
										
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you">' + BuffStrOut + '</div>');									
									}
								
								}
								else
								{
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="you" style="margin-bottom:0; color:#999; font-size:10px;">' + d.INTERVALSCHEDULED + '</div>');
										$('#SMSHistoryScreenArea').append('<div style="clear:both"></div><div class="bubble you" rel="' + d.GETRESPONSE.RESPONSELENGTH + '">' + d.GETRESPONSE.RESPONSE + '</div>');
								}
							
								<!--- Scroll to bottom on new data --->		
								$('#SMSTextInputArea').resize();					
								$("#SMSHistoryScreenArea").animate({ scrollTop: $('#SMSHistoryScreenArea')[0].scrollHeight}, 1000);
						
							}
							
							
							CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
							
							if(typeof(d.GETRESPONSE.RESPONSETYPE) != "undefined")
							{		
								<!--- Get next Response--->
								if(d.GETRESPONSE.RESPONSETYPE == 'STATEMENT' || d.RESPONSETYPE == 'OPTIN' || d.RESPONSETYPE == 'API' || d.RESPONSETYPE == 'CDF')
								{
									
									EBMText = 'EBM - ' +  d.GETRESPONSE.RESPONSETYPE;
									d.GETRESPONSE.RESPONSETYPE = '';
									
																		
									inpflag++;
									
									if(inpflag < MaxStatements)
										GetResponseSimple(inpShortCode, EBMText, inpContactString, 1); 
									
																		
									<!---var t=setTimeout(function(){
											GetResponseSimple(inpShortCode, inpKeyword, inpContactString, 1);
										},1500)--->
																			
								}
								else if(d.RESPONSETYPE == 'AGENT')
								{
									d.RESPONSETYPE = '';
									
									inpflag++;
									
									<!--- 
										Now what? How do we make this page list for AAU message to the current device address?
										How to ignore messages aau message if device address is changed?
									--->
																			
								}
								else if(d.GETRESPONSE.RESPONSETYPE == 'INTERVAL')
								{									
									QueueNextResponse(inpShortCode, 'EBM - INTERVAL', inpContactString, d.GETRESPONSE.INTERVALSCHEDULED, d.GETRESPONSE.INTERVALEXPIREDNEXTQID)
									
									<!--- Update optional interval processing and display --->
									<!---$('#INTERVALSCHEDULED').html(d.GETRESPONSE.INTERVALSCHEDULED + '<BR/>Device Address Time Zone offset from PST is ' + d.GETRESPONSE.TIMEZONEOFFSETPST);
									$('#IntervalOptions').show();--->
									
									<!--- force resize on empty --->
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();
								}
								else
								{
									<!--- force resize on empty --->
									$('#SMSTextInputArea').val('');
									$('#SMSTextInputArea').resize();									
									
								}
							}
							
						}
						else
						{
							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
											
			} 		
			
		});
		return false;
	}
	
	
	function QueueNextResponse(inpShortCode, inpKeyword, inpContactString, inpScheduled, inpTimeOutNextQID)
	{		
		var data = 
		{ 
			username : 'demo.sire',
			password : 'A0DF875079D6267E05FC',
			inpContactString : inpContactString,
			inpShortCode : inpShortCode,
			inpQueueState : '<cfoutput>#SMSQCODE_QA_TOOL_READYTOPROCESS#</cfoutput>',
			inpScheduled : inpScheduled,
			inpTimeOutNextQID : inpTimeOutNextQID
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'QueueNextResponseSMS', data, "Error: Next Interval response has not been set!", function(d ) {
			CheckNextResponse(inpShortCode, inpKeyword, inpContactString);
		});	
	
	}
	
	function CheckNextResponse(inpShortCode, inpKeyword, inpContactString)
	{		
		
		<!---$.ajax({
		<!---url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc?method=GetSMSResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   --->
		url: '<cfoutput>#rootUrlAPI#/webservice</cfoutput>/ebm/pdc/TimeMachineCheckNextResponse',
		type: 'POST',
		dataType: 'json',
		data:  	{ 
					username : 'demo.sire',
					password : 'A0DF875079D6267E05FC',
					inpContactString : inpContactString,
					inpShortCode : inpShortCode
				},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if Warning --->
																							
				<!--- Get row 1 of results if exisits--->
									
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.RXRESULTCODE) != "undefined")
					{							
						CurrRXResultCode = d.RXRESULTCODE;	
						
						if(CurrRXResultCode > 0 && typeof(d.NEXTEVENT) != "undefined" )
						{
							$('#INTERVALSCHEDULED').html(d.NEXTEVENT);	
							$('#IntervalOptions').show();						
						}
						else
						{
							$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');	
						}
					}
					else
					{
						$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');
					}
						   				
			} 		
			
		});
		return false;--->
	
	
	
		var data = 
		{ 
			username : 'demo.sire',
			password : 'A0DF875079D6267E05FC',
			inpContactString : inpContactString,
			inpShortCode : inpShortCode
		};
		
		
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'TimeMachineCheckNextResponseQA', data, "Error: Next Interval response can not be found!", function(d ) {		
				
			if(typeof(d.RXRESULTCODE) != "undefined")
			{							
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && typeof(d.NEXTEVENT) != "undefined" )
				{
					$('#INTERVALSCHEDULED').html(d.NEXTEVENT);	
					$('#IntervalOptions').show();						
				}
				else
				{
					$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');					
				}
			}
			else
			{
				$('#INTERVALSCHEDULED').html('NA - MM:DD:YYYY');				
			}
							
		});	
	
	}
	
	
	function SetStartCP()
	{		
		var data = 
		{ 
			inpContactString : $('#inpContactString').val(),
			inpShortCode : $('#inpSMSToAddress').val(),
			inpKeyword : $('#inpCPKeyword').val(),
			inpCP : $('#inpCP').val()
			
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'SetSurveyStateToCP', data, "Error: Starting Control Point has not been set!", function(d ) {
			
											
			if(typeof(d.RXRESULTCODE) != "undefined")
			{		
				CurrRXResultCode = d.RXRESULTCODE;	
									
				if(CurrRXResultCode > 0 && d.PROGRAMSETCPOK > 0 )
				{
					inpTimeTraveledSoFarInMinutes = 0;
					
					MaxStatements = inpflag + 20; 
			
					inpflag++;
			
					if(inpflag < MaxStatements)
						GetResponseSimple($('#inpSMSToAddress').val(), inpflag, $('#inpContactString').val(), 1); 					
				}
			}
		});	
	
	}
	
	function escapeHtml(text) {
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };
	
	  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	}	
	
</script>