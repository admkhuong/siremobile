<cfparam name="url.plan" default="0">
<cfparam name="url.rf" default="">
<cfparam name="url.type" default="">
<cfset variables.menuToUse = 4 />

<cfscript>
	createObject("component", "public.sire.models.helpers.layout")
        .addJs("/session/sire/js/buy_credits_payment.js")
        .addCss("/session/sire/assets/layouts/layout4/css/custom.css")
        .addCss("/session/sire/assets/layouts/layout4/css/custom1.css")
        .addCss("/session/sire/assets/layouts/layout4/css/custom2.css")
        .addCss("/session/sire/css/style.css")
        .addCss("/session/sire/css/screen.css")
		.addJs("/public/sire/js/order_plan.js");
</cfscript>


<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
	SELECT PlanId_int,Order_int FROM simplebilling.plans WHERE Status_int = 1 AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.plan#"> 
</cfquery>
<!--- <cfif checkExistPlan.RecordCount EQ 0>
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>

<cfif Session.keyExists('loggedIn') && Session.loggedIn EQ 1 >
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>

<cfif !Session.keyExists('loggedIn') OR Session.USERID EQ 0 OR Session.USERID EQ ''>
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>
 --->
<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
</cfinvoke>

<!--- <cfif getUserPlan.PLANORDER GTE checkExistPlan.Order_int >
	<cflocation url="/plans-pricing" addtoken="false">
</cfif> --->

<cfquery name="getPlans" datasource="#Session.DBSourceREAD#">
	SELECT * FROM simplebilling.plans 
	WHERE 
		Status_int = 1 AND  Order_int > <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPlan.PLANORDER#"> 
	ORDER BY Order_int  
</cfquery>

<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price">
	<cfinvokeargument name="plan" value="#url.plan#">
</cfinvoke>

<!--- CHECK USER PROMOTION --->
<cfquery name="getUserPromotion" datasource="#Session.DBSourceREAD#">
  SELECT 
    up.PromotionId_int,
    up.PromotionLastVersionId_int,
    up.UsedDate_dt,
    up.RecurringTime_int
  FROM simplebilling.userpromotions up 
  WHERE
      up.UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#SESSION.USERID#">
      AND up.UsedTime_int <= up.RecurringTime_int
      AND up.PromotionStatus_int = 1
  LIMIT 1
</cfquery>


<cfset totalAMOUNT = price.AMOUNT>

<!--- Check User Have Promotion --->
<cfif getUserPromotion.RecordCount GT 0>

    <cfquery name="getPromotion" datasource="#Session.DBSourceREAD#">
        SELECT
          pc.promotion_id,
          pc.origin_id,
          pc.discount_type_group,
          pc.discount_plan_price_percent,
          pc.discount_plan_price_flat_rate,
          pc.promotion_keyword,
          pc.promotion_MLP,
          pc.promotion_short_URL,
          pc.promotion_credit

        FROM simplebilling.promotion_codes pc 
        WHERE
            pc.promotion_id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#getUserPromotion.PromotionId_int#">
            AND pc.promotion_status  = 1
        LIMIT 1
    </cfquery>

    <!--- Have a Promotion --->
    <cfif getUserPromotion.RecordCount GT 0>
        <cfif getPromotion.discount_type_group EQ 1>
            <cfset totalAMOUNT = (price.AMOUNT - (price.AMOUNT * getPromotion.discount_plan_price_percent / 100) - getPromotion.discount_plan_price_flat_rate)>
        </cfif>

    </cfif>
</cfif>

<cfif totalAMOUNT < 0 >
    <cfset totalAMOUNT = 0>
</cfif>


<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>

<style type="text/css">
div.secure-payment {
	font-size: 16px;
	color: #5c5c5c;
	line-height: 2.5em;
}

.control-label {
	font-size: 16px;
	color: #5c5c5c;
}

div.small-info {
    color: #74c37f;
    font-size: 14px;
    font-style: italic;
}

.secure-payment .pull-right {
    color: #568ca5;
    font-weight: bold;
}
.btn-primary-custom{
    color: #FFF;
    background-color: #49849f;
    border-color: #49849f;
    text-transform: uppercase;
    font-weight: 700;
}
</style>

<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="RetUserPlan">
</cfinvoke>


<!--- GET MAX KEYWORD CAN EXPIRED --->
<cfset UnsubscribeNumberMax = 0>

<cfif RetUserPlan.PLANEXPIRED EQ 1>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
<cfelseif RetUserPlan.PLANEXPIRED EQ 2> <!--- IF USER PLAN HAS DOWNGRADE IT CAN NOT UNSCRIBE KEYWORD --->
    <!---
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDBUYAFTEREXPRIED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDBUYAFTEREXPRIED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>    
    --->
    <cfset UnsubscribeNumberMax = 0>
<cfelse>
    <cfif RetUserPlan.KEYWORDAVAILABLE GT RetUserPlan.KEYWORDPURCHASED>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDPURCHASED>
    <cfelse>
        <cfset UnsubscribeNumberMax = RetUserPlan.KEYWORDAVAILABLE>
    </cfif>
</cfif>

<cfset planMonthlyAmount = RetUserPlan.AMOUNT />
<!--- <div class="portlet light bordered"> --->
    <main class="container-fluid page">
        <section class="row bg-white">

        <div class="content-header">
            <div class="row">
                <div class="col-sm-6 content-title">
                    <p class="title">Welcome to Sire!</p>
                </div>
                <div class="col-sm-6 content-menu">
                    <a class="active"><span class="icon-oder-plan"></span><span>Order Plan</span></a>
                </div>
            </div>
        </div>
        <hr class="hrt0">

    	<!---<cfinclude template="../configs/credits.cfm">--->
    	<cfoutput>
        <form id="buy_credits" class="container-fluid content-body" action="/public/sire/models/cfm/order-plan-signup">
	    	<input type="hidden" id="plan" name="plan" value="#url.plan#">
            <input type="hidden" id="rf" name="rf" value="#URL.rf#">
	    	<input type="hidden" id="actionStatus" value="">
	    	<input type="hidden" id="amount" name="amount" value="#totalAMOUNT#">
	    	<input type="hidden" value="#userInfo.FULLEMAIL#" id="h_email" name="h_email">
    		<div class="row">
            <div class="col-md-6">
            	<div id ="step1">
                <h4 class="form-heading"><strong>Secure Payment</strong></h4>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 secure-payment">
                    	<div class="pull-left">Currency:</div>
                    	<div class="pull-right">$ US Dollars</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                    	<div class="pull-left">Select Your Plan:</div>
                    	<div class="pull-right number-credits">
                    		<select class="form-control" id="order-plan">
                   				<cfloop query="getPlans">
		                          	<option value="#getPlans.PlanId_int#" <cfif url.plan EQUAL getPlans.PlanId_int>selected</cfif>>#getPlans.PlanName_vch#</option>
		                         </cfloop> 
                    		</select>
                    	</div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                    	<div class="pull-left">Monthly Amount:</div>
                    	<div class="pull-right amount" id="MonthlyAmount">$<cfoutput>#price.AMOUNT#</cfoutput></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                    	<div class="pull-left">Number of Included Keyword:</div>
                    	<div class="pull-right amount" id="NumberKeyword"><cfoutput>#price.KEYWORDSLIMITNUMBER#</cfoutput></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 secure-payment">
                    	<div class="pull-left">Number of Included Credits:</div>
                    	<div class="pull-right amount" id="FirstSMSInclude"><cfoutput>#NumberFormat(price.FIRSTSMSINCLUDED,',')#</cfoutput></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 small-info" style="color:red">
                    	Your start date of plan is reset on 28th if you buy the plan on 29th, 30th or 31st.
                    </div>
<!---                     <div class="col-xs-12 col-sm-12 small-info">
                    	Please review your purchase details, then select a payment method to continue
                    </div> --->
                    
                </div>
                <hr>
	        	<div class="row">
        			<div class="col-sm-12">
        				<input type="radio"  name="payment_method" class="check_payment_method hidden" id="payment_method_0" value="0" checked="checked"/>
	        			<label for="payment_method_0" class="payment_method_card payment_method_card_0">
	        				&nbsp;
	        			</label>
        			</div>
        			<div class="col-sm-12">
    					<hr style="margin-top: 10px;"> 
        			</div>
	        	</div>
                <!---<div class="row heading">
                	<div class="col-sm-12 heading-title">Card Information<hr></div>
                </div>--->
                <div class="row">
                	<div class="col-sm-12 form-horizontal">
					  <div class="form-group">
					    <label for="card_number" class="col-sm-4 control-label">Card Number:<span class="text-danger">*</span></label>
					    <div class="col-sm-8">
					      <input type="text" class="form-control validate[required,custom[creditCardFunc]]" maxlength="32" id="card_number" name="number">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="security_code" class="col-sm-4 control-label">Security Code:<span class="text-danger">*</span>
					    	<a href="##" data-toggle="modal" data-target="##scModal"><img src="/session/sire/images/help-small.png"/></a>
					    </label>
					    <div class="col-sm-8">
					      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="4" id="security_code" name="cvv" data-errormessage-custom-error="* Invalid security code">
					    </div>
					  </div>
					  <div class="form-group" id="ExpirationDate">
					    <label for="" class="col-sm-4 control-label">Expiration Date:<span class="text-danger">*</span></label>
				        <input type="hidden" id="expiration_date" name="expirationDate">
					    <div class="col-sm-4">
					      <select class="form-control validate[required]" id="expiration_date_month">
					      	<option value="01">January</option>
					      	<option value="02">February</option>
					      	<option value="03">March</option>
					      	<option value="04">April</option>
					      	<option value="05">May</option>
					      	<option value="06">June</option>
					      	<option value="07">July</option>
					      	<option value="08">August</option>
					      	<option value="09">September</option>
					      	<option value="10">October</option>
					      	<option value="11">November</option>
					      	<option value="12">December</option>
					      </select>
					    </div>
					    <div class="col-sm-4">
					    	<select class="form-control validate[required]" id="expiration_date_year">
					    		<cfloop from="#year(now())#" to="#year(now())+50#" index="i">
					    			<option value="#i#">#i#</option>
					    		</cfloop>
					    	</select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="first_name" class="col-sm-4 control-label" id="card_name_label">Cardholder Name:<span class="text-danger">*</span></label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="first_name" name="firstName" placeholder="First Name">
					    </div>
					    <div class="col-sm-4">
					      <input type="text" class="form-control validate[required,custom[onlyLetterNumberSp]]" maxlength="50" id="last_name" name="lastName" placeholder="Last Name">
					    </div>
					  </div>
					</div>
            	</div>
            	<div class="col-sm-12 form-horizontal form-footer text-center">
					<a type="button" class="btn btn-medium green-cancel pull-right"  href="javascript:history.go(-1)">Cancel</a>
					<a type="button" id="btnStep1" class="btn btn-medium green-gd pull-right margin-right-5">Next</a>
				 </div>
        	</div>
        	</div>
        	</div>
			<cfinclude template="../views/commons/payment/step2.cfm">
			<cfinclude template="../views/commons/payment/step3_order_plan.cfm">
    	</form>

    	<div class="row">
    		<div class="col-md-6">
    			<div class="row">
            		<div class="col-sm-12">
                    	<div class="pull-left worldpay-logo"><img src="/session/sire/images/powered-worldpay.png"></div>
                    	<div class="pull-right worldpay-help">For help with your payment visit: <a target="_blank" href="http://www.worldpay.com/uk/support">WorldPay Help</a></div>
            		</div>
			     </div>         		
			</div>            	
		</div>
    	</cfoutput>
    </section>
    </main>
<!--- </div> --->

<!-- Modal -->
<div class="modal fade" id="scModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Security Code</h4>
      </div>
      <div class="modal-body text-center">
			<img src="/session/sire/images/securecode.gif"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn green-gd" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<cfparam name="variables._title" default="Order Plan - Sire">

<cfinclude template="/public/sire/views/layouts/main_front_core_gray.cfm" >
<!--- <cfinclude template="/session/sire/views/layouts/order_plan_layout.cfm" > --->