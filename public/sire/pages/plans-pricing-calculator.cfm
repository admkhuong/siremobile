<cfparam name="URL.rf" default=""/>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/plans_pricing.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/plan-pricing.css">
</cfinvoke>

	<header class="home plans_pricing_calc header-banners">
		<!--- <div class="slice"></div> --->
		<section class="landing-top">
	        <div class="container-fluid">
	            <div class="row">
					<div class="public-logo">
	                    <div class="landing-logo clearfix">
                            <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
	                        </a>
	                    </div>
	                </div>
	                
	                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
	                        <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
	                <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
	                        <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
	                </cfif>
	                        
	                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
	                    &nbsp;  
	                <cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
	            </div>
	        </div>      
	    </section>
			

		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h1 class="banner-title-big hidden-xs">Savings calculator</h1>

						<h3 class="banner-title-small visible-xs">Savings calculator</h3>

						<p class="banner-sub-title hidden-xs"><i>Let us help you find out which plan works best for your business</i></p>

						<!--- <h1 class="marketing_title_big hidden-xs">Savings calculator</h1>
						<h4 class="marketing_title visible-xs">Savings calculator</h4>
						<h4 class="hidden-xs"><i>Let us help you find out which plan works best for your business</i></h4> --->
					</div>	
				</div>	
                
                <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div>
                
                <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div>
                
			</div>	
		</section>

	</header>


    
    
	<div class="container-fluid home-intro text-center">
    
   		
               
       
        
        <div class="container">

            <div class="row">
                <p class="col-lg-8 col-sm-8 col-xs-8">
                   <h2 class="heading-home">Answer a few questions to find out which plan fits your needs</h2>
                </p>
            </div>                    
        
	         <div class="row price-plan-select">
	            <div class="col-sm-12 col-xs-12 col-lg-offset-3 col-lg-6">
	            	<form id="plan-pricing-calc-form" nam="plan-pricing-calc-form" action="" method="POST">
	               <div class="row form-group">
	                  <label class="col-sm-6 col-xs-12">How many customers do you have?</label>
	                  <div class="col-sm-6 col-xs-12">
	                     <input class="form-control validate[required,custom[onlyNumber]]" data-prompt-position="topLeft:150" id="Q1">
	                  </div>
	               </div>
	               <div class="row form-group">
	                  <label class="col-sm-6 col-xs-12">How frequently would you like to reach out to the customer?</label>
	                  <div class="col-sm-6 col-xs-12">
	                     <select class="form-control" id="Q2">
						    <option value="5">1‐5x/month</option>
						    <option value="10">6‐10x/month</option>
						    <option value="20">11+x/month</option>
						  </select>
	                  </div>
	               </div>
	               <div class="row form-group">
	                  <label class="col-sm-6 col-xs-12">What kind of marketer are you?</label>
	                  <div class="col-sm-6 col-xs-12">
	                     <select class="form-control" id="Q3">
						    <option value="beginner">Beginner</option>
						    <option value="intermediate">Intermediate</option>
						    <option value="expert">Expert</option>
						  </select>
	                  </div>
	               </div>
	               </form>
	               <div class="row form-group">
	                  <label class="col-sm-6 col-xs-12"></label>
	                  <div class="col-sm-6 col-xs-12" style="text-align:right">
	                     <button class="btn btn-success btn-success-custom calculate" id="Calculate">Calculate</button>
	                     <button class="btn btn-primary btn-back-custom1 calculate_reset" type="reset" id="Reset">Reset</button>
	                  </div>
	               </div>
	            </div>
	         </div>
	  </div>       
        
   </div>
	<section class="main-wrapper" id="feature-main" style="display: none;">
		<hr>
		<p class="plan-works-best">Our <span class="active" id="planSelect">SMB</span> plan works best for you!</p>
		<!--- <cfinclude template="../views/commons/plan_pricing.cfm" > --->
	</section>
	

<cfparam name="variables._title" default="Plans & Pricing Calculator - Sire">
<cfparam name="variables.body_class" default="plans_pricing_calc">
<cfinclude template="../views/layouts/plan_pricing.cfm">