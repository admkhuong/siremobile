<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/plans_pricing.js">
</cfinvoke>
<cfinvoke component="public.sire.models.cfc.order_plan" method="GetOrderPlan" returnvariable="price1">
	<cfinvokeargument name="plan" value="1">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/plan-pricing.css">
</cfinvoke>


<cfparam name="URL.rf" default=""/>
<cfif URL['rf'] neq "">
	<cfset Request.meta_data = [
		[{property = "og:title", content = "SIRE REFERRAL PROGRAM - FRIEND INVITATION"}, 
		{property = "og:type", content = "website"}, 
		{property = "og:url", content = cgi.request_url}, 
		{property = "og:description", content = "Want to invite your friend and receive more credits? Receive 1,000 credits after your friend signs up for an account on Sire."}, 
		{property = "og:image", content = "https://s3-us-west-1.amazonaws.com/siremobile/sire-refer-a-friend-facebook-post_v6.jpg?v=3.0" },
		{property = "og:image:width", content = "1200" },
		{property = "og:image:height", content = "627" }],

		[{property = "twitter:card", content = "summary_large_image"}, 
		{property = "twitter:site", content = "@sire_mobile"}, 
		{property = "twitter:url", content = cgi.request_url}, 
		{property = "twitter:title", content = "SIRE REFERRAL PROGRAM - FRIEND INVITATION"}, 
		{property = "twitter:description", content = "Want to invite your friend and receive more credits? Receive 1,000 credits after your friend signs up for an account on Sire."}, 
		{property = "twitter:image:src", content = "https://s3-us-west-1.amazonaws.com/siremobile/sire-refer-a-friend-facebook-post_v6.jpg?v=3.0" },
		]
	] />
</cfif>


<style>

<!---
header.home {
	background-color: #274e60;
    min-height: 50px !important;
}
--->
	
</style>
	
	
	<header class="sire-banner2 plans-pricing-banner header-banners">
		<!--- <div class="slice"></div> --->
		<section class="landing-top">
	        <div class="container-fluid">
	            <div class="row">
					<div class="public-logo">
	                    <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
	                        <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
	                        </a>
	                    </div>
	                </div>
	                
	                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
	                        <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
	                <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
	                        <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
	                </cfif>
	                        
	                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
	                    &nbsp;  
	                <cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">
                    </cfif>
	            </div>
	        </div>      
	    </section>
			

		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
						<!--- <h1 class="banner-title-big hidden-xs">text marketing made easy,</h1>
                        <h1 class="banner-title-big hidden-xs">affordable and effective</h1>

                        <h3 class="banner-title-small visible-xs">text marketing made easy,</h3>
                        <h3 class="banner-title-small visible-xs">affordable and effective</h3> --->

                        <h1 class="sire-title-updated">TEXT MARKETING MADE EASY</h1>
                        <h1 class="sire-title-updated">AND AFFORDABLE</h1>

						
						<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                        
                        <cfelse>
                            <a href="signup" class="get-started mt-30">get started</a>
                        </cfif>

						<!---<p class="landing_title">Plans & Pricing</p>
						<p class="sms_marketing">Choose your plan</p>--->
                        
                        <!--- <h1 class="marketing_title_big hidden-xs">Plans & Pricing</h1>
                        <h2 class="marketing_title visible-xs">Plans & Pricing</h2>
						                        
                        <h4 class="hidden-xs" ><i>Choose your plan</i></h4> --->
                        
						<!--- Removed
							
						<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
							<p class="sms_marketing">Choose your plan or</p>
							<p>
								<a href="signup" class="btn btn-info btn-lg btn-info-custom start_trial2">
									START YOUR FREE TRIAL TODAY
								</a>
							</p>
							<h4 class="hidden-xs hidden-sm"><i><cfoutput>#NumberFormat(price1.FIRSTSMSINCLUDED,',')#</cfoutput> SMS and <cfoutput>#price1.KEYWORDSLIMITNUMBER#</cfoutput> Keywords<i></h4>
						<cfelse>
							<p class="sms_marketing">Choose your plan</p>
						</cfif>
						
						--->
						
					</div>	
				</div>	
                
                <!--- <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div> --->
                
                <!--- <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div> --->
                
			</div>	
		</section>

		<!--- <section class="landing-jumbo landing-jumbo2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
                        <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                        
                        <cfelse>
                            <a href="signup" class="get-started mt-20">get started</a>
                        </cfif>					
					</div>
				</div>
			</div>
		</section> --->

	</header>

	<!--- <div class="container-fluid home-intro plan-tit text-center">
        <h2 class="heading-home">Break free with fast, simple tools and services to help your business with mobile messaging.</h2>	        
        <p class="price-question">Not sure which plan is right for you? <a href="/public/sire/pages/plans-pricing-calculator<cfif URL.rf NEQ ''><cfoutput>?rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>">Click here</a></p>
        
    </div> --->

	<section class="pricing-block-1">
		<div class="container new-container1">
			<div class="inner-pricing-block-1">
				<div class="row">
					<div class="col-sm-12 text-center">
						<p class="short-desc">
							It costs 7-10x more to acquire new customers than to retain existing customers. You’ve worked hard to earn every one of your customers. It’s time to keep them coming back for more
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="main-wrapper" id="feature-main">
		<cfinclude template="../views/commons/plan_pricing.cfm" >
	</section>
	

<cfparam name="variables._title" default="SMS Marketing Plans & Pricing">
<cfparam name="variables.body_class" default="">
<cfparam name="variables.meta_description" default="Sire Mobile’s text marketing platform has enterprise class features with but is priced with SMBs in mind.  Affordable yet powerful text marketing service.">

<cfinclude template="../views/layouts/plan_pricing.cfm">




<!---
<cfinclude template="../views/layouts/plan_pricing.cfm">
--->
<script type="text/javascript">
    $('.li-plan-price').addClass('active');
</script>
