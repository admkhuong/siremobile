<cfif structKeyExists(URL, 'ac')>
	<cfinvoke method="UserActiveAccount" component="public.sire.models.cfc.userstools" returnvariable="retValUserActive">
		<cfinvokeargument name="inpActiveCode" value="#trim(URL.ac)#">
	</cfinvoke>

	<cfif retValUserActive.RXRESULTCODE GT 0>
		<cfif retValUserActive.ACTIVE GT 0>
			<cfoutput>
				<p>Account activated!</p>
			</cfoutput>
		<cfelse>
			<cfoutput>
				<p>Reactive failed!</p>
			</cfoutput>
		</cfif>
		<p>You will be redirect to homepage in <span id="timer">5</span> secs. Or click <a href="/">here</a> to leave now.</p>
		<cfoutput>
			<script type="text/javascript">
				var count=5;

				var counter=setInterval(timer, 1000);

				function timer()
				{
					count=count-1;
					if (count < 0) {
						clearInterval(counter);
						window.location.href = '/';
						return;
					}
					document.getElementById('timer').innerHTML = count;
				}
			</script>
		</cfoutput>
	<cfelse>
		<cflocation url="/" addtoken="false">
	</cfif>
<cfelse>
	<cflocation url="/" addtoken="false">
</cfif>