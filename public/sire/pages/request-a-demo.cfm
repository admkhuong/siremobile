
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
    <cfinvokeargument name="path" value="/public/sire/css/contact-us.css">
</cfinvoke>
<meta name="format-detection" content="telephone=no">

<header class="sire-banner2 request-demobanner header-banners">
   
   <section class="landing-top">
		<div class="container-fluid">
			<div class="row">
				<div class="public-logo">
                    <div class="landing-logo clearfix">
                    <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                            <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                            <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                        </a>
                    </div>
                </div>
				
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                	<div class="header-nav pull-right" style="margin-right: 30px">
                        <cfinclude template="../views/commons/menu.cfm">    
                    </div>  
                <cfelse>
                	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                	<div class="header-nav">
                        <cfinclude template="../views/commons/menu_public.cfm">
                    </div>  
            	</cfif>
                        
				<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
					&nbsp;	
				<cfelse>
	                <cfinclude template="../views/commons/public_signin.cfm">  
                </cfif>
			</div>
		</div>		
	</section>
   
   
    <section class="landing-jumbo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    
                    <h1 class="sire-title-updated">Request a demo</h1>
                </div>	
            </div>
        
        </div>
    </section>
        
</header>

<section class="home-block-3 block-for-requestdemo">
    <div class="container new-container1 contact-us-container request-demo-container">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12 before-send-request">
                    <h4>
                    	Schedule a personal demo with Sire today.
                    </h4>
                    <p>Fill out the info below and a member of our team will get back to you to schedule your demo.<br>Don't worry, we love eating Spam but hate receiving it in our inbox. Your info will only be used to contact you regarding the demo.</p>
                </div>

                <div class="col-sm-12 after-send-request" style="display: none">
                	<h4>
                    	Thank you for scheduling a demo. We'll be in touch soon!
                    </h4>
                    <p>It may take a business day for us to reply to our request. If you have immediate questions, shoot them over to <a href="mailto:hello@siremobile.com" style="color: #578ca5">hello@siremobile.com</a> and we'll get right on it!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="request-demo-block before-send-request">
	<div class="container new-container1">
		<div class="inner-contact-block">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						<div class="col-lg-10 col-lg-offset-1">
							<form name="request-demo" class="row request-demo" id="requestdemo_form">
								<div class="form-group col-sm-12 col-lg-10 col-lg-offset-1">
									<!--<input type="text" class="form-control" name="infoName" maxlength="120" placeholder="Name" required>-->
									<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoName" name="infoName" maxlength="120" placeholder="Name*" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-12 col-lg-10 col-lg-offset-1">
									<!--<input type="text" class="form-control" name="infoContact" maxlength="250" placeholder="Contact Address (Email or Phone)" >-->
									<input type="text" class="form-control validate[required,custom[email]]" id="infoEmail" name="infoEmail" maxlength="250" placeholder="Work Email*" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-12 col-lg-10 col-lg-offset-1">
									<!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
									<input type="text" class="form-control validate[required,custom[noHTML]]" id="infoCompany" name="infoCompany" maxlength="250" placeholder="Company*" data-prompt-position="topLeft:100"/>
								</div>
								<div class="form-group col-sm-12 col-lg-10 col-lg-offset-1">
									<!--<input type="text" class="form-control" name="infoSubject" maxlength="250" placeholder="Subject" >-->
									<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="infoPhone" name="infoPhone" maxlength="250" placeholder="Phone*" data-prompt-position="topLeft:100"/>
								</div>
								<div class="col-sm-12 contact-us-message"></div>
								<div class="form-group col-sm-12 text-center submit-requestdemo">
									<button type="submit" class="btn btn-send-message">SCHEDULE A DEMO</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="request-demo-block after-send-request" style="display: none">
	<div class="container new-container1">
		<div class="col-sm-4 col-sm-offset-4 thanks-request-demo-img">
			<img class="img-responsive" src="/public/sire/images/request-a-demo/request-demo-ic.png" alt="" style="">
		</div>
		<div class="col-sm-12 text-center thanks-request-demo-text">
			<p>Thank you for scheduling a demo!<br>We'll be in touch soon.</p>
		</div>
	</div>
</section>
	
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>	
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/request_demo.js">
</cfinvoke>
	
<cfparam name="variables._title" default="Request A Demo">
<cfparam name="variables.body_class" default="">
<cfinclude template="../views/layouts/contact_us.cfm">