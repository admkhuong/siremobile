<header class="sire-banner2 restaurant-banner header-banners">    
    <section class="landing-top">
        <div class="container-fluid">
            <div class="row">
                <div class="public-logo">
                    <div class="landing-logo clearfix">
                        <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                            <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                            <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                        </a>
                    </div>
                </div>
                
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
                    <div class="header-nav pull-right" style="margin-right: 30px">
                        <cfinclude template="../views/commons/menu.cfm">    
                    </div>  
                <cfelse>
                    <div class="header-nav">
                        <cfinclude template="../views/commons/menu_public.cfm">
                    </div>  
                </cfif>
                
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                    &nbsp;	
                <cfelse>
                    <cfinclude template="../views/commons/public_signin.cfm">  
                </cfif>
            </div>
        </div>      
    </section>
    
    <section class="landing-jumbo landing-jumbo-feature">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="sire-title-updated">THE RESTAURANT MARKETING PLATFORM<br class="hidden-xs"> THAT PAYS FOR ITSELF IN 1 CAMPAIGN.</h1>
                    <h1 class="sire-title-updated hide">TO COMMUNICATE WITH YOUR CUSTOMERS</h1>

                    <p class="banner-sub-title"><i>Increase walk-in customers. Thwart negative online reviews.</i></p>

                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                    <cfelse>
                        <a href="signup" class="get-started mt-20">get started</a>
                    </cfif>
                </div>	
            </div>
        </div>	
    </section>
</header>

<!-- <section class="blue-block-1">
    <div class="container-fluid">
        <div class="inner-blue-block-1">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Limited Time Special!  Free Custom SMS Marketing Kit.</h4>
                    <p>Use code: FreeKit at checkout to receive 12 custom designed tabletop displays and 2 window stickers when you sign up for any paid account.</p>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section class="free-promotion feature-gray">
    <div class="container">
        <div class="inner-block">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="newtitle">Free Promotion Kit - limited time offer!</h4>
                    <p class="newsubtitle">
                        <i>To help you get your campaigns cooking (pun intended), Sire Mobile will send you 12 tabletop displays and 2 stickers when you sign up for any paid membership. You provide your logo, colors and promotion.  We'll print and ship it for free.  Use code FreeKit at checkout.</i>
                    </p>
                </div>
            </div>

            <div class="uk-grid uk-grid-small" uk-grid>
                <div class="uk-width-1-2@s uk-text-center">
                    <img src="/public/sire/images/restaurants/stickers.png" alt="" class="uk-responsive-width">
                </div>

                <div class="uk-width-1-2@s uk-text-center">
                    <img src="/public/sire/images/restaurants/table-top.png" alt="" class="uk-responsive-width">
                </div>
            </div>
            <cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1>
                <div class="row">
                    <div class="col-sm-12 uk-margin-medium-top">
                        <div class="uk-text-center">
                            <h4 class="redeemtitle">Redeem your promo kit now!</h4>
                            <a href="signup-wcp?coupon=freeKit&fromSource=restaurantmarketing" class="get-started">let's do this!</a>
                        </div>
                    </div>
                </div>
            </cfif>
        </div>
    </div>
</section>

<section class="you-get-block">
    <div class="container">
        <h4 class="title">What You Get</h4>
        
        <div class="you-get-item">
            <div class="uk-grid uk-flex-middle">
                <div class="uk-width-auto@s uk-text-center">
                    <img class="item-img" src="/public/sire/images/restaurants/you-get-1.png" alt="">
                </div>
                <div class="uk-width-expand@s">
                    <div class="content">
                        <h4>Prevent bad Yelp reviews</h4>
                        <p>
                            <i>Nobody’s perfect and mistakes will happen in your restaurant.  If a customer has an unsatisfactory experience, they feel the need to vent.  They don’t specifically need turn to Yelp.  They just need to feel like someone is listening.  Our Customer Feedback campaigns provides a buffer for customers to communicate with you to reduce the likelihood of going to a public forum like Yelp.</i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="you-get-item">
            <div class="uk-grid uk-flex-middle uk-flex-row-reverse">
                <div class="uk-width-auto@s uk-text-center">
                    <img class="item-img" src="/public/sire/images/restaurants/you-get-2.png" alt="">
                </div>
                <div class="uk-width-expand@s">
                    <div class="content">
                        <h4>More customers during slow times</h4>
                        <p>
                            <i>One of our clients once shared, “the lunch and dinner rush isn’t our problem.  It’s when I have 8 servers standing around doing nothing between 2-5:30 pm.  That’s the problem.”  Between fluctuating food costs and high overhead, you need an edge to earn more profits. Create flash deals and happy hour promotions to draw customers in the door during the slow hours of the day.</i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="you-get-item">
            <div class="uk-grid uk-flex-middle">
                <div class="uk-width-auto@s uk-text-center">
                    <img class="item-img" src="/public/sire/images/restaurants/you-get-3.png" alt="">
                </div>
                <div class="uk-width-expand@s">
                    <div class="content">
                        <h4>Increase table turnover</h4>
                        <p>
                            <i>Aside from food cost, Table or Seat turnover ratio is often considered one of the most important metrics for a restuarants success.  This ratio is simply the number of times a table is filled in your restaurant during a particular meal period or time. This ratio has a direct correlation to your revenue.  SMS marketing is a cost effective way for you to achieve a higher table turnover rate for each meal service.</i>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="you-get-item">
            <div class="uk-grid uk-flex-middle uk-flex-row-reverse">
                <div class="uk-width-auto@s uk-text-center">
                    <img class="item-img" src="/public/sire/images/restaurants/you-get-4.png" alt="">
                </div>
                <div class="uk-width-expand@s">
                    <div class="content">
                        <h4>Build loyalty</h4>
                        <p>
                            <i>No matter how good your food and service, the truth is your customers have options.  With so many competing dining establishments in your local neighborhood, it’s easy for you to get lost in the shuffle.  Loyalty programs are a time tested way to keep customers coming back.  SMS loyalty campaigns do the same without the hefty cost and headache of dealing with POS software integration, printing costs and physical loyalty cards.</i>
                        </p>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</section>

<section class="home-block-2">
    <div class="inner-home-block-2">
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-3@m">
                <div class="grid-item">
                    <div class="retangle">
                        <div href="features">
                            <div class="inner-retangle">
                                <img class="img-responsive extend-img" src="/public/sire/images/restaurants/bl3-1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-width-1-3@m">
                <div class="grid-item">
                    <div class="retangle">
                        <div href="plans-pricing">
                            <div class="inner-retangle">
                                <img class="img-responsive extend-img" src="/public/sire/images/restaurants/bl3-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-width-1-3@m">
                <div class="grid-item">
                    <div class="retangle">
                        <div href="why-sire">
                            <div class="inner-retangle">
                                <img class="img-responsive extend-img" src="/public/sire/images/restaurants/bl3-3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-block-3 feature-gray res-question-block res-question-block-1">
    <div class="container-fluid">
        <div class="inner-home-block-3">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Have questions or want to learn more?</h4>
                    <p>
                        Schedule a free demo with one of our marketing experts today!
                    </p>
                    <a href="https://sire-mobile.appointlet.com/b/sire" target="_blank" class="get-started">request free demo</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-block-2 oki-doki-block">
    <div class="inner-home-block-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="newtitle oki-doki-title">Featured Restaurant - Oki Doki</h4>
                </div>
            </div>
        </div>
        
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-3@m">
                <div class="grid-item doki-item">
                    <div class="retangle">
                        <div class="inner-retangle">
                            <img class="img-responsive extend-img" src="/public/sire/images/restaurants/backgr-1.jpg" alt="">
                        </div>
                        <div class="block-overlay"><span>background</span></div>
                        <a class="link-overlay">
                            <span>
                                Oki Doki is a Japanese restaurant specializing in high quality sushi and hand pulled udon noodles made fresh, in-house daily.  The owner is a traditionally trained and highly experienced sushi chef who has made mastering the art of fine sushi his life’s passion.
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="uk-width-1-3@m">
                <div class="grid-item doki-item">
                    <div class="retangle">
                        <div class="inner-retangle">
                            <img class="img-responsive extend-img" src="/public/sire/images/restaurants/challenge-2.jpg" alt="">
                        </div>
                        <div class="block-overlay"><span>challenge</span></div>
                        <a class="link-overlay">
                            <span>
                                He’s mastered his menu and is an expert in Japanese cuisine but, like most restaurant owners, rarely can afford the time or know-how for marketing.
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="uk-width-1-3@m">
                <div class="grid-item doki-item">
                    <div class="retangle">
                        <div class="inner-retangle">
                            <img class="img-responsive extend-img" src="/public/sire/images/restaurants/result-3.jpg" alt="">
                        </div>
                        <div class="block-overlay"><span>results</span></div>
                        <a class="link-overlay">
                            <span>
                                He started using Sire in January and, by May, was experiencing a 40% increase in business.  “The thing I love about Sire is that it’s so easy to use that a sushi chef like me had no problem figuring it out. The price was so low that it pays itself off it the first SMS blast of the month.”
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <p class="busy-shushi">
                        <i>
                            If a busy sushi chef can do this, so can you. Sire is designed to be easy enough for anyone to learn and affordable enough that even the smallest restaurants can use it.
                        </i>
                    </p>
                </div>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uBxXbIPQljg?rel=0&modestbranding=1&enablejsapi=1&autoplay=0" allowfullscreen></iframe>
            </div>
        </div>

        
    </div>
</section> 

<section class="res-sample-block">
    <div class="container-fluid">
        <div class="inner-block-sample">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="title">Examples of how you can use SMS Marketing</h4>
                </div>
            </div>

            <!-- <div class="uk-grid" uk-grid uk-height-match="target: .des">
                <div class="uk-width-1-3@m">
                    <div class="item uk-text-center">
                        <h4 class="des"><i>Prevent Bad Reviews with Live Chat</i></h4>
                        <div class="img">
                            <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-1.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m">
                    <div class="item uk-text-center">
                        <h4 class="des"><i>Improve Customer Service with Automated Surveys</i></h4>
                        <div class="img">
                            <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m">
                    <div class="item uk-text-center">
                        <h4 class="des"><i>Increase Customer Loyalty with Weekly Promotions</i></h4>
                        <div class="img">
                            <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-3.png" alt="">
                        </div>
                    </div>
                </div>
            </div> -->

            <div id="restaurant-slide-phone" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <!-- <div class="item active">
                        <img src="la.jpg" alt="Los Angeles" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="chicago.jpg" alt="Chicago" style="width:100%;">
                    </div>

                    <div class="item">
                        <img src="ny.jpg" alt="New york" style="width:100%;">
                    </div> -->

                    <!-- <div class="uk-grid" uk-grid uk-height-match="target: .des"> -->
                    <div class="uk-width-3-3@m item active">
                        <div class="item uk-text-center">
                            <h4 class="des"><i>Prevent Bad Reviews with Live Chat</i></h4>
                            <div class="img">
                                <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-1.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-3-3@m item">
                        <div class="item uk-text-center">
                            <h4 class="des"><i>Improve Customer Service with Automated Surveys</i></h4>
                            <div class="img">
                                <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-2.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-3-3@m item" id="last-slide-phone">
                        <div class="item uk-text-center">
                            <h4 class="des"><i>Increase Customer Loyalty with Weekly Promotions</i></h4>
                            <div class="img">
                                <img class="uk-responsive-width" src="/public/sire/images/restaurants/example-3.png" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- </div> -->
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#restaurant-slide-phone" data-slide="prev" style="background-image: initial;">
                    <span class="fa fa-angle-left" style="color: black;"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#restaurant-slide-phone" data-slide="next" style="background-image: initial;">
                    <span class="fa fa-angle-right"  style="color: black;"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>


        </div>
    </div>
</section>
<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1>
    <section class="home-block-3 feature-gray res-question-block">
        <div class="container-fluid">
            <div class="inner-home-block-3">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Start Your Free Trial Today</h4>
                        <p>
                            No Credit Card Required | Free Upgrade or Downgrade | No Hidden Fees
                        </p>
                        <a href="/signup" class="get-started">start now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</cfif>


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/restaurant.js">
    </cfinvoke>  

    <cfparam name="variables._title" default="Restaurant Marketing made affordable and easy.">


    <cfparam name="variables.meta_description" default="Text messaging is the best way for restaurants to get more customers in the door, increase table turnover and reduce negative yelp reviews. Free trial.">
    <cfinclude template="../views/layouts/home.cfm">

