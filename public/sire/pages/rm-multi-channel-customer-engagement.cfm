

<!---

Replace this page with the preview functionality 

https://litmus.com/blog/best-practices-for-optimizing-order-confirmation-emails

Best Practices for Optimizing Order Confirmation Emails


     <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="feature-title" id="feature1">Event Based Communications</div>
                    <div class="feature-content">
                        
                        <p>Communication events are triggered throughout the customer lifecycle</p>
                        
                        
                        <ul>
                        	<li></li>
                            
                        </ul>
                        
                            
                    </div>
                </div>

              	<div class="col-md-6">                    
                                    
                       <img style="float:left;" src="../images/learning/interactions31.png" />                                                                  
                    
                </div>

            </div>
        </div>
    </div>
	
--->

<section class="main-wrapper feature-main" id="feature-main">
		
	 	<div class="feature-white no-padding-bottom no-padding-top">
        
        	<img class="hidden-xs hidden-sm hidden-md" style="float:right;" src="/public/sire/images/learning/eaitower50.png" />
            <img class="visible-xs visible-sm visible-md" style="float:right;" src="/public/sire/images/learning/eaitower25.png" />   
        
			<div class="container padding-top-1em">
				<div class="row">
					<div class="col-sm-9 col-md-9">
						<div class="feature-title">Roadmap to Multi-Channel Customer Engagement</div>
						<div class="feature-content">
                        	 
                            <p>Sire systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context.</p>
                            
                        </div>
					</div>
             	</div>   
			</div>
		</div>        
        
     	<div class="feature-white">
			<div class="container">
				<div class="row">
					                    
                    <div class="col-md-6 col-md-offset-3">
						
                        <img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/eai-flow.png">
                        
					 </div>
                                     
				</div>
			</div>
		</div>
        
        <div class="feature-white">
			<div class="container">
				<div class="row">
                                         
					<div class="col-md-6">
						<div class="feature-title">Sections</div>
						<div class="feature-content">
                        	
                             <ul style="text-align:left; width:450px; display: inline-block;">                            
                                
                                <!--- Hypothetical walk through campaign zero - use walmart as example 
								
									Sire Roadmap to gaols
									
									
									Look at previous RFP questions and answers 
								
								
									Hourly Rate - vs number of hours per task 
									
									Each section - what can sire do for you?
									
									
									
									CONTACT CENTER DEFLECTION
									LIMIT CONTACT CENTER EXPENSE USING SIMPLE AUTOMATION FROM EBM.


								--->
                                
                                
                                <!--- 
									SAAS, PAAS, In-House 
									VPN - MSLS - 
									Data Strucutres 
									CDFs
									inport CPP
									Update CPP
									ETL
									EAI
									
									
									Contact Center Integration
									API Post/Get
									Standard Agent Templates
									Custom Agent Templates
									Company User Account Management Tools
									SMS Chat
									CID pass in
									ANI
									Automated Response
									Inbound IVR
									Keyword Response
									Canned Response
									Agent Assisted Response

								---->
                                <li>Service Connectivity</li>  
                                
                                <!--- PCI SAS70 HIPPA  --->
                                <li>Security</li>   
                                
                                <!---
									Approval Process - digital signature
									Business Rules
									SLA's
									Retry Logic
									Content - CMS all channels
									
								--->                        
                                <li>Defineing a new Campaign</li>
                                
                                <!---
								
								
								--->
                                <li>Customer Preference </li>
                                
                                <!---
									ROI
									Visibility
									Problems
									Found Knowledge
									
									Exports
									Dashboards
									MAnagement Tools
									Agent Tools
								--->
                                <li>Reporting</li>
                                
                                <!---
									Alerts
									Sample issue where problem not noticed for several weeks
									Responsible parties
									Escalation Lists
								--->
                                <li>NOC Monitoring</li>
                                
                                
                                <!--- 
								Toll Fre numbers
								Short Codes
								emaIl
								Terms and conditions
								CPP
								--->
                                <li>Provisioning</li>
                                
                                <!---
									Interanl Teams
									Management
									Front line employees
									Customers
								--->                                
                                <li>Focus Groups</li>
                                
                                <!--- Tie results to actions--->
                                <li>Close the Loop </li>
                                
                                <!--- Sceduled Improvement Reviews
								New Technologies															
								Work with fit bit / Apps  to add local alerts and 
								
								Reminder Snooze
								#WMT
								**Walmart
								--->
                                <li>Constant Improvment</li>
                                
                                                                
                                <li>Budget - Costs - Hours - Schedule</li>
                              
                            </ul>
                    
                    
                            <p><h4>Tip:</h4>Sire personel can help you re-use and repurpose existing data transfer assets to save time and reduce IT costs.</p>
                         
                         </div>
                                  
					</div>
                    
                    
                     <div class="col-md-6">   
                    	<img class="" alt="feature-img-no-bs" src="/public/sire/images/learning/transfer-data.png">                           
					</div>   
                    
					    
				</div>
			</div>
		</div>
                
        
       
        
</section>



<cfparam name="variables._title" default="Enterprise Application Intergration">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

