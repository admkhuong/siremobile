<cfcomponent>
	<cfset this.name = "RXMBEBMDemo" />  <!--- RXMBEBMDemo ->  RXSireApplication --->

	<cfset this.sessionStorage  = "sessionstorage">
	<cfset this.sessionCluster = true>
	
	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first CREATED.">
	     <cfset APPLICATION.EncryptionKey_Cookies = "GHer459836RwqMnB529L" />
	     <cfset APPLICATION.EncryptionKey_UserDB = "Encryptlksdjgh7486yumnvpwe09wdsafmcssdafString" />

	     <cfset APPLICATION.sessions = 0>

	    <cfreturn true />
	</cffunction>
	

	<cffunction name="onError"> 
	   <cfargument name="Exception" required=true/> 
	   <cfargument name="EventName" type="String" required=true/> 

	    <!--- Log all errors. ---> 
	    <cfset var message = ''>
	    <cfset var request_url = ''>
	    <cfset var messageDetail = ''>
	    <cfset var request_url = ''>
	    <cfset var http_referer = ''>
	    <cfset var http_user_agent = ''>

	    <cfif isStruct(Exception)>
	    	<cfset messageDetail = serializejson(Exception)>
	    </cfif>

	    <cfif structKeyExists(Exception, "message")>
	    	<cfset message = Exception.message>
	    </cfif>

	    <cfif structKeyExists(Exception, "Detail")>
	    	<cfset message &= ' '&Exception.Detail>
	    </cfif>
	    <!---
	    <cfif structKeyExists(Exception, "queryError")>
	    	<cfset message &= ' '&Exception.queryError>
	    </cfif>
	    --->
	    <cfif structKeyExists(cgi, "request_url")>
	    	<cfset request_url = cgi.request_url>
	    </cfif>

	    <cfif structKeyExists(cgi, "http_user_agent")>
	    	<cfset http_user_agent = cgi.http_user_agent>
	    </cfif>
	    
	    <cfif structKeyExists(cgi, "http_referer")>
	    	<cfset http_referer = cgi.http_referer>
	    </cfif>

	    <!--- Display an error message if there is a page context. ---> 
		<cflocation url= "/error-5xx.html" addToken = "false"/>
	    
	</cffunction>

</cfcomponent>