
<!--- Load Setting Defaults - Assume No Sessions here --->
<cfset DBSourceManagementRead = "Bishop" />

<!--- id comes from the .htaccess redrect - anything after the slash  --->
<cfparam name="id" default="0">


<!--- Did we get a valid id and not the default 0? --->
<cfif id NEQ "0">


	<!--- Read target URL from DB - do strIght look up here as this will be a simple site with redirect -- Assume No Sessions here  --->	
    <cfquery name="LookupFullURL" datasource="#DBSourceManagementRead#">
        SELECT 
            TargetURL_vch,
            UniqueTargetId_int,
        	RedirectCode_int,
        	TargetURL_vch,
            DynamicData_vch
        FROM
            simplelists.url_shortner
        WHERE
            ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#id#">  
        AND
        	Active_int = 1	              
    </cfquery>       
            
    <cfif LookupFullURL.RECORDCOUNT GT 0>
	    
	    	
	    <cfset NewTarget = TRIM(LookupFullURL.TargetURL_vch) />
	    
	    <!--- Repare poorly defined links to default to non-ssl http  --->
	    <cfif COMPARENOCASE(LEFT(NewTarget,4), "HTTP") NEQ 0>
		    
		    <cfset NewTarget = "http://" &  NewTarget />
		    
	    </cfif>    	    
	    
	    <!--- Append params if specified  --->
	    <cfif LEN(TRIM(LookupFullURL.DynamicData_vch)) GT 0>	
	
			<cfif LEFT(TRIM(LookupFullURL.DynamicData_vch), 1) EQ "?">
				
				<cfset NewTarget = NewTarget & LookupFullURL.DynamicData_vch />
				
			<cfelse>
				
				<cfset NewTarget = NewTarget & "?" & LookupFullURL.DynamicData_vch />
				
			</cfif>	
	
	    </cfif>
	    
	    <!--- Update click counter --->
	    <cftry>
	    
	    
	    <!--- Update entry into DB --->
            <cfquery name="UpdateShortURLData" datasource="#DBSourceManagementRead#">
               	UPDATE  
               		simplelists.url_shortner
			   	SET 	
	               ClickCount_int = ClickCount_int + 1,
				   LastClicked_dt = NOW()
               	WHERE
                    ShortURL_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#id#">                 
                AND
                	Active_int = 1	              
            </cfquery>      
	    
	    <cfcatch type="any">
	    
	    </cfcatch>
        </cftry> 
	    	    
		<cflocation addToken="no" statusCode="#LookupFullURL.RedirectCode_int#" url="#NewTarget#" />  
	    	    
    </cfif>        
        
</cfif>	

<!--- wont get here if re-direct is found but if it is not found....give the user a semi-useful error message --->
<link rel="icon" href="oops.png">
<title>Enterprise Application Integration URL Shortening service for Mobile marketing</title>

<body style="">


	<div style="margin: 2em; padding: 2em;">		
		
		<img src="oops.png" style="float: left; margin-right: 3em; " /> 
		
		<h1>WHOOPS!</h1>
		<h3>The page you requested was not found. We have a couple of guesses as to why. </h3>
		
		<ul>
			
			<li>If you typed the URL directly, please make sure the spelling is correct.</li>
			<li>If you clicked on a link to get here, the link is turned off or is outdated.</li>
			
		</ul>
	
		<p>Enterprise Application Integration URL Shortening service for Mobile marketing</p>					
	</div>

	

</body>




