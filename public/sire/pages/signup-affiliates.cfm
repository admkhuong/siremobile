<cfset Session._rememberPath = "/session/sire/pages/affiliates-dashboard" />
<cfparam  name="actionType" default="4"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="1">
<cfparam  name="ami" default="">
<cfif ami NEQ "">
    <cfset affiliatetype=2>
</cfif>
<cfscript>
	CreateObject("component","public.sire.models.helpers.layout")		
		.addCss("/public/sire/css/affiliate.css")
        .addJs("/public/sire/js/affiliates-program-signup.js");                
</cfscript>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/start-free-new.min.js">
</cfinvoke>
<cfset AffiliateInfomation = {
    AffiliateCode = "",
	AffiliateUserEmail= "" ,
	AffiliateUserID= 0	
}>

<cfset UserInfomation= {
    FirstName = "",
    LastName = "",
    SMSNumber  = "",   
    EmailAddress = "",
    Address = "",
    City = "",
    State = "",
    Zip = "",
    SSN_TaxID = "", 
    AffiliateCode= "",
    PaypalEmailAddress=""
}>
<cfif ami NEQ "">
    <cfinvoke method="GetMasterInfo" component="public.sire.models.cfc.affiliate" returnvariable="GetMasterInfo">
        <cfinvokeargument name="inpAffiliateCode" value="#ami#">
    </cfinvoke>
    <cfif GetMasterInfo.RXRESULTCODE EQ 1>       
        <cfset actionType= 3> 
        <cfset AffiliateInfomation = {
            AffiliateCode = #ami#,
            AffiliateUserEmail= #GetMasterInfo.AFFILIATEINFO.EmailAddress_vch#,
            AffiliateUserID= #GetMasterInfo.AFFILIATEINFO.UserId_int#	
        }>
        <cfcookie   name = "AFFILIATECODE" expires = "180" secure = "yes" value = "#ami#" encodevalue = "yes">
    </cfif>
    <cfset StructClear(session)>   

    <cfset Session.USERID = "">
    <cfset Session.loggedIn = 0>    
<cfelseif  structKeyExists(cookie,'affiliatecode')> 
    <cfset ami=cookie.affiliatecode>
    <cfinvoke method="GetMasterInfo" component="public.sire.models.cfc.affiliate" returnvariable="GetMasterInfo">
        <cfinvokeargument name="inpAffiliateCode" value="#ami#">
    </cfinvoke>
    <cfif GetMasterInfo.RXRESULTCODE EQ 1>       
        <cfset actionType= 3> 
        <cfset AffiliateInfomation = {
            AffiliateCode = #ami#,
            AffiliateUserEmail= #GetMasterInfo.AFFILIATEINFO.EmailAddress_vch#,
            AffiliateUserID= #GetMasterInfo.AFFILIATEINFO.UserId_int#	
        }>               
    </cfif>
    <cfset StructClear(session)>   

    <cfset Session.USERID = "">
    <cfset Session.loggedIn = 0>   
<cfelseif structKeyExists(Session,'UserID') AND Session.UserID GT 0>
    <cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfo" returnvariable="userInfo"></cfinvoke>
    <cfset actionType= 4>
    <cfset UserInfomation= {
        FirstName = userInfo.FIRSTNAME,
        LastName = userInfo.LASTNAME,
        SMSNumber  = userInfo.MFAPHONE,   
        EmailAddress = userInfo.FULLEMAIL,
        
        Address = userInfo.ADDRESS,
        City = userInfo.CITY,
        State = userInfo.STATE,
        Zip = userInfo.POSTALCODE,
        SSN_TaxID = userInfo.SSNTAXID, 
        AffiliateCode= "",
        PaypalEmailAddress=userInfo.PaypalEmail
    }>

<cfelseif !structKeyExists(Session,'UserID') OR Session.UserID EQ 0 OR Session.UserID EQ "">  
    <cflocation  url="/affiliates" addToken ="no">
</cfif>

<cfoutput>
<div class="container">         
    <div class="col-sm-12 text-center">
        <p class="short-desc">
            Affiliate Program Sign-up
        </p>
    </div>
	<section class="signup-block">        
        <div class="inner-signup-block">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <form name="signup_form_1" id="signup_form_1" autocomplete="off">
                                
                                <input type="hidden" name="" id="actionType" value="#actionType#">
                                <input type="hidden" name="" id="select-plan-2" value="1">
                                <input type="hidden" name="" id="affiliatetype" value="#affiliatetype#">
                                <input type="hidden" name="" id="AMI" value="#AffiliateInfomation.AffiliateUserID#">
                                
                                <cfif ami NEQ "" AND AffiliateInfomation.AffiliateUserID GT 0>
                                    <div class="uk-width-1-1">
                                        <div class="sub-acc-infomation">											
                                            
                                            <hr style="border:0;border-bottom:1px solid ##74c37f">	   
                                            <p style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">Affiliate Information</p>                                         
                                            Affiliate Code: <span style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">#AffiliateInfomation.AffiliateCode#</span></br>
                                            Affiliate User Email: <b>#AffiliateInfomation.AffiliateUserEmail#</b></br>                                            
                                            <hr style="border:0;border-bottom:1px solid ##74c37f">											
                                        </div>
                                    </div>
                                </cfif>
                                <div class="uk-grid uk-grid-small" uk-grid>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">First name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50" value="#UserInfomation.FirstName#" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Last name</label>
                                            <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50" value="#UserInfomation.LastName #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>
                                    <cfif actionType NEQ 4>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">SMS Number</label>
                                            <input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - " value="#UserInfomation.SMSNumber  #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">E-mail Address</label>
                                            <input type="text" class="form-control validate[required,custom[email]]" id="emailadd" placeholder="" value="#UserInfomation.EmailAddress #" #actionType EQ 4 ? 'readonly': ''#>
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Password</label>
                                            <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" placeholder="">
                                        </div>
                                    </div>

                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Confirm Password</label>
                                            <input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" placeholder="">
                                        </div>
                                    </div>
                                    </cfif>
                                    <cfif listfind("2,4",actionType) GT 0>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">                                        
                                            <label for="">Address</label>
                                            <input type="text" class="form-control validate[required]" id="address" placeholder="" value="#UserInfomation.Address #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control validate[required]" id="city" placeholder="" value="#UserInfomation.City #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" class="form-control validate[required]" id="state" placeholder="" value="#UserInfomation.State #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Zip</label>
                                            <input type="text" class="form-control validate[required]" id="zip" placeholder="" value="#UserInfomation.Zip #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">SSN/Tax ID</label>
                                            <input type="text" class="form-control validate[required]" id="taxid" placeholder="" value="#UserInfomation.SSN_TaxID #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1">
                                        <div class="form-group">
                                            <label for="">Paypal E-mail Address</label>&nbsp;<span >(Commission payments will be sent to this account)</span>
                                            <input type="text" class="form-control validate[required,custom[email]]" id="paypalemailadd" placeholder="" value="#UserInfomation.PaypalEmailAddress #">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <div class="form-group">
                                            <label for="">Affiliate Code</label>
                                            <input type="text" class="form-control validate[required]" id="affiliatecode" value="#ami#" #ami NEQ '' ? 'readonly': ''# placeholder="">
                                        </div>
                                    </div>
                                    <cfelseif ami NEQ "">
                                        <input type="hidden" class="hidden" id="affiliatecode" value="#ami#" >
                                    </cfif>
                                    <div class="uk-width-1-1 last-box-newsignup">
                                        <div class="checkbox">
                                            <label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
                                                <input name="agree" class="validate[required]" id="agree" type="checkbox"> 
                                                <span>I’ve read and agree to the <a href="/affiliate-term-of-use" target="_blank">Terms and Conditions of Use.</a></span>
                                            </label>
                                        <div class="checkbox">
                                    </div>

                                    <div class="uk-width-1-1">
                                        <button class="btn newbtn green-gd" id="btn-new-signup">get started!</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>

</div>
</cfoutput>

<cfparam name="variables._title" default="Affiliate Program Sign-up">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">

