<cfparam name="url.plan" default="0">
<cfparam name="url.rf" default="">
<cfparam name="url.type" default="">
<cfset hideFixedSignupForm = "1">
<cfquery name="checkExistPlan" datasource="#Session.DBSourceREAD#">
	SELECT PlanId_int,Order_int FROM simplebilling.plans WHERE Status_int = 1 AND PlanId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#url.plan#"> 
</cfquery>
<cfif checkExistPlan.RecordCount EQ 0>
	<cflocation url="/plans-pricing" addtoken="false">
</cfif>

<cfset variables._title = 'Sign Up'>
<cfset variables.body_class = 'sign_up_page'>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/sign_up.js">
</cfinvoke>

<header class="home_signup clearfix">
	<div class="offset-nav"></div>
	<section class="landing-top">
			<div class="container-fluid">
				<div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                        <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
				</div>
			</div>		
		</section>


<section class="main-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-lg-7 col-sm-offset-1 col-lg-offset-2 signup_title">
				<p class="row landing_title">Sign Up And Order Plan</p>
				<!---<p class="row no_credit">No credit card required</p>--->
			</div>	
		</div>	
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-lg-7 col-sm-offset-1 col-lg-offset-2 wraper_signup_form">
				<div class="row">
					<div class="col-sm-6 col-xs-11 signup_form">
							<form method="post" action="##" name="signup_form" id="signup_form" autocomplete="off">
							  <cfoutput><input type="hidden" id="plan" name="plan" value="#url.plan#"></cfoutput>
							  <cfoutput><input type="hidden" id="rf" name="rf" value="#url.rf#"></cfoutput>
							  <cfoutput><input type="hidden" id="rftype" name="rftype" value="#url.type#"></cfoutput>
							  <div class="form-group">
							  	<div class="row">
							  		<div class="col-sm-6">
									    <label for="fname">First Name</label>
									    <input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" name="fname" maxlength="50" placeholder="" data-prompt-position="topLeft:100"/>
								    </div>	
								    <div class="col-sm-6">
									    <label for="lname">Last Name</label>
									    <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" name="lname" maxlength="50" placeholder="" data-prompt-position="topLeft:100"/>
									</div>    
							    </div>
							  </div>

							  <div class="form-group">
							    <label for="sms_number">SMS Number</label>
							    <input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - " data-prompt-position="topLeft:100"/>
							  </div>

							   <div class="form-group">
							    <label for="emailadd">E-mail Address</label>
							    <input type="email" class="form-control validate[required,custom[email]]" id="emailadd" name="emailadd" placeholder="" maxlength="250" data-prompt-position="topLeft:100">
							  </div>
							  <!---	
							   <div class="form-group">
							    <label for="org_name">Organization Name</label>
							    <input type="text" class="form-control validate[custom[noHTML]]" id="org_name" name="org_name" placeholder="" maxlength="250" data-prompt-position="topLeft:100"/>
							  </div>
							  --->

							   <div class="form-group">
							    <label for="inpPasswordSignup">Password</label>
							    <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" name="inpPasswordSignup" placeholder="" maxlength="50" data-prompt-position="topLeft:100"/>
							  </div>

							   <div class="form-group">
							    <label for="inpConfirmPassword">Confirm Password</label>
							    <input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" name="inpConfirmPassword" placeholder="" maxlength="50" data-prompt-position="topLeft:100"/>
							  </div>


							  <div class="form-group">
							    <label for="inpPromotionCode">Promotion Code</label>
							    <input type="text" class="form-control" id="inpPromotionCode" name="inpPromotionCode" placeholder="" maxlength="16" data-prompt-position="topLeft:100"/>
							  </div>

							  <div class="checkbox">
							    <label>
							      <input type="checkbox" name="agree" id="agree" class="validate[required]" /> I've read and agree to the <a href="/term-of-use" target="_blank">Terms and Conditions of Use</a>.
							    </label>
							  </div>

							  <button type="button" class="btn btn-success btn-login btn-success-custom" id="btn-sign-up">ORDER NOW</button>

							</form>
					</div>
					<div class="col-sm-5 col-xs-11 vcenter signup_form_info">
						<h4><strong>Secure Password Requirements</strong></h4>
						<ul>
							<li>must be at least 8 characters</li>
							<li>must have at least 1 number</li>
							<li>must have at least 1 uppercase letter</li>
							<li>must have at least 1 lower case letter</li>
							<li>must have at least 1 special character</li>
						</ul>
						<br/>
						<!--- <p>A secure sample of one has been randomly generated for you. Feel free to use or replace with your own.</p>	--->
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	
</section>
</header>


<cfinclude template="../views/layouts/home.cfm">