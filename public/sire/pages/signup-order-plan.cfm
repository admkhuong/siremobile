<cfparam name="url.rf" default="">
<cfparam name="url.type" default="">
<cfparam name="url.yearly" default="">
<cfparam  name="actionType" default="1"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="0">
<cfparam  name="ami" default="">
<cfparam  name="couponCodeDefault" default="">

<cfset variables._title = 'Sign Up - Sire'>
<cfset variables.body_class = 'sign_up_page'>
<cfset hideFixedSignupForm = '1'>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/start-free.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>
<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/session/sire/assets/layouts/layout4/css/custom2.css">
</cfinvoke>

<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans">
	<cfinvokeargument name="inpNotIncludeEnterprisePlan" value="1">
</cfinvoke>

<!--- Get Payment method setting --->		
<cfinvoke component="session.sire.models.cfc.billing" method="GetPaymentMethodSetting" returnvariable="RetGetPaymentMethodSetting"></cfinvoke>

<cfif plans.RXRESULTCODE NEQ 1>
	<cflocation url="/" addtoken="false"/>
</cfif>
<cfset AffiliateInfomation = {
    AffiliateCode = "",
	AffiliateUserEmail= "" ,
	AffiliateUserID= 0	
}>

<cfif  structKeyExists(cookie,'affiliatecode')> 
    <cfset ami=cookie.affiliatecode>
    <cfinvoke method="GetMasterInfo" component="public.sire.models.cfc.affiliate" returnvariable="GetMasterInfo">
        <cfinvokeargument name="inpAffiliateCode" value="#ami#">
    </cfinvoke>
    <cfif GetMasterInfo.RXRESULTCODE EQ 1>       
        <cfset actionType= 3> 
		<cfset affiliatetype= 2>
        <cfset AffiliateInfomation = {
            AffiliateCode = #ami#,
            AffiliateUserEmail= #GetMasterInfo.AFFILIATEINFO.EmailAddress_vch#,
            AffiliateUserID= #GetMasterInfo.AFFILIATEINFO.UserId_int#	
        }>               
    </cfif>
    <cfset StructClear(session)>   

    <cfset Session.USERID = "">
    <cfset Session.loggedIn = 0>   
    
</cfif>
<cfif ami NEQ "">
	 <!--- check linkage coupon code with affiliate code--->
	<cfinvoke method="GetCouponFromAffiliateCode" component="public/sire/models/cfc/affiliate" returnvariable="rtGetCouponFromAffiliateCode">
		<cfinvokeargument name="inpAffiliateCode" value="#ami#"/>
	</cfinvoke>
	<cfif rtGetCouponFromAffiliateCode.RXRESULTCODE EQ 1>                           
		<cfset couponCodeDefault=rtGetCouponFromAffiliateCode.COUPONCODE >
	</cfif>
</cfif>
<section class="landing-top">
	<div class="container-fluid">
		<div class="row">
			<div class="public-logo">
                <div class="landing-logo clearfix">
                    <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                        <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                        <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                    </a>
                </div>
            </div>
			
			

            <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
            	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
            	<div class="header-nav pull-right" style="margin-right: 30px">
                    <cfinclude template="../views/commons/menu.cfm">    
                </div>  
            <cfelse>
            	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
            	<div class="header-nav">
                    <cfinclude template="../views/commons/menu_public.cfm">
                </div>  
        	</cfif>
                    
			<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
				&nbsp;	
			<cfelse>
                <cfinclude template="../views/commons/public_signin.cfm">  
            </cfif>
		</div>
	</div>		
</section>

<!-- For referral -->
<input type="hidden" id="rf" name="rf" value="<cfoutput>#url.rf#</cfoutput>">
<input type="hidden" id="rftype" name="rftype" value="<cfoutput>#url.type#</cfoutput>">
<input type="hidden" id="is-yearly-plan" name="is-yearly-plan" value="<cfoutput>#url.yearly#</cfoutput>">
<input type="hidden" value="" name="plan" id="plan">

<div class="bg-start-free">
	<div class="uk-container uk-container-center">
		<div class="uk-grid" uk-grid>
			<div class="uk-width-1-1">
				<div class="inner-start-free">
					<h4 class="start-title">LET'S GET STARTED!</h4>
					<!--- <p class="start-italic"><i>No credit card required</i></p> --->
					<p class="start-italic">&nbsp;</p>

					<div class="box-start-free" id="section-1">
						<form name="signup_form" id="signup_form" autocomplete="off">							
							<cfif ami NEQ "" AND AffiliateInfomation.AffiliateUserID GT 0>
								<cfoutput>
								<input type="hidden" name="" id="actionType" value="#actionType#">								
								<input type="hidden" name="" id="affiliatetype" value="#affiliatetype#">
								<input type="hidden" name="" id="AMI" value="#AffiliateInfomation.AffiliateUserID#">
								<input type="hidden" class="hidden" id="affiliatecode" value="#ami#" >
								<div class="uk-width-1-1">
									<div class="sub-acc-infomation">											
										
										<hr style="border:0;border-bottom:1px solid ##74c37f">	   
										<p style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">Affiliate Information</p>                                         
										Affiliate Code: <span style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">#AffiliateInfomation.AffiliateCode#</span></br>
										Affiliate User Email: <b>#AffiliateInfomation.AffiliateUserEmail#</b></br>                                            
										<hr style="border:0;border-bottom:1px solid ##74c37f">											
									</div>
								</div>
								</cfoutput>
							</cfif>
							<div class="uk-grid uk-grid-medium" uk-grid>
								<div id="parent-height-follow" class="uk-width-1-2@m">
									<div class="uk-grid uk-grid-small" uk-grid>
										<div class="uk-width-1-2">
											<div class="form-group">
												<label for="">First Name</label>
	    										<input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50">
											</div>
										</div>

										<div class="uk-width-1-2">
											<div class="form-group">
												<label for="">Last Name</label>
	    										<input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50">
											</div>
										</div>
										
										<div class="uk-width-1-1">
											<div class="form-group">
												<label for="sms_number">SMS Number</label>
	    										<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - ">
											</div>
										</div>
										
										<div class="uk-width-1-1">
											<div class="form-group">
												<label for="">E-mail Address</label>
	    										<input type="text" class="form-control validate[required,custom[email]]" id="emailadd" placeholder="">
											</div>
										</div>
										
										<div class="uk-width-1-1">
											<div class="form-group">
												<label for="">Password</label>
	    										<input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" placeholder="">
											</div>
										</div>

										<div class="uk-width-1-1">
											<div class="form-group has-ok">
												<label for="">Confirm Password</label>
	    										<input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" placeholder="">
											</div>
										</div>
									</div>
								</div>

								<div class="uk-width-1-2@m">
									<div id="implicitly-cal-height" class="uk-height-1-1 uk-position-relative">
										<div class="uk-grid uk-grid-small" uk-grid>
											<div class="uk-width-1-1">
												<div class="form-group">
													<cfif url.yearly NEQ 1>
														<label for="">Plans</label>
			    										<select class="form-control validate[required]" id="select-plan-1">
			    											<option value="" disabled selected>Select a plan</option>
			    											<cfoutput>
			    												<cfloop query="#plans.DATA#">
			    													<option value="#id#"><b>#name#</b> - $#price#/month</option>
			    												</cfloop>
			    											</cfoutput>
			    											<!--- <option value="1"><b>Free</b> - $0/month</option>
			    											<option value="2"><b>Individual</b> - $24/month</option>
			    											<option value="6"><b>Pro</b> - $99/month</option>
			    											<option value="3"><b>SMB</b> - $249/month</option> --->
			    										</select>
			    									<cfelse>
			    										<label for="">Plans</label>
			    										<select class="form-control validate[required]" id="select-plan-1">
			    											<option value="" disabled selected>Select a plan</option>
			    											<cfoutput>
			    												<cfloop query="#plans.DATA#">
			    													<option value="#id#"><b>#name#</b> - $#yearlyAmount*12#/year</option>
			    												</cfloop>
			    											</cfoutput>
			    											<!--- <option value="1"><b>Free</b> - $0/month</option>
			    											<option value="2"><b>Individual</b> - $24/month</option>
			    											<option value="6"><b>Pro</b> - $99/month</option>
			    											<option value="3"><b>SMB</b> - $249/month</option> --->
			    										</select>
			    									</cfif>
												</div>

												<div class="content-st">
													<cfoutput>
														<cfloop query="#plans.DATA#">
															<div class="item" id="#id#">
																<div class="uk-grid uk-grid-small" uk-grid>
																	<div class="uk-width-1-2@s">
																		<ul>
																			<li>First #FirstSMSIncluded_int# SMS Included</li>
																			<li>#KeywordsLimitNumber_int# User Reserved <a data-toggle="modal" title="What is a Keyword?" href="##" data-target="##InfoModalKeyword">Keywords</a></li>
																			<li>#PriceMsgAfter_dec# Cents per Msg after</li>
																		</ul>
																	</div>

																	<div class="uk-width-1-2@s">
																		<ul>
																			<li>#(MlpsLimitNumber_int EQ 0 ? "Unlimited" : MlpsLimitNumber_int)# <a data-toggle="modal" title="What is a MLP?"  href="##" data-target="##InfoModalMLP">MLPs</a></li>
																			<li>#(ShortUrlsLimitNumber_int EQ 0 ? "Unlimited" : ShortUrlsLimitNumber_int)# Short URLs</li>
																		</ul>
																	</div>
																</div>
															</div>
														</cfloop>
													</cfoutput>
													<!--- <div class="item" id="1">
														<div class="uk-grid uk-grid-small" uk-grid>
															<div class="uk-width-1-2@s">
																<ul>
																	<li>First 25 SMS Included</li>
																	<li>3 User Reserved <a href="##">Keywords</a></li>
																	<li>2.7 Cents per Msg after</li>
																</ul>
															</div>

															<div class="uk-width-1-2@s">
																<ul>
																	<li>2 <a href="##">MLPs</a></li>
																	<li>2 Short URLs</a></li>
																</ul>
															</div>
														</div>
													</div>
													<div class="item" id="2">
														<div class="uk-grid uk-grid-small" uk-grid>
															<div class="uk-width-1-2@s">
																<ul>
																	<li>First 1,000 SMS Included</li>
																	<li>5 User Reserved <a href="##">Keywords</a></li>
																	<li>1.9 Cents per Msg after</li>
																</ul>
															</div>

															<div class="uk-width-1-2@s">
																<ul>
																	<li>5 <a href="##">MLPs</a></li>
																	<li>5 Short URLs</a></li>
																</ul>
															</div>
														</div>
													</div>
													<div class="item" id="6">
														<div class="uk-grid uk-grid-small" uk-grid>
															<div class="uk-width-1-2@s">
																<ul>
																	<li>First 4,000 SMS Included</li>
																	<li>10 User Reserved <a href="##">Keywords</a></li>
																	<li>1.7 Cents per Msg after</li>
																</ul>
															</div>

															<div class="uk-width-1-2@s">
																<ul>
																	<li>Unlimited <a href="##">MLPs</a></li>
																	<li>Unlimited Short URLs</a></li>
																</ul>
															</div>
														</div>
													</div>
													<div class="item" id="3">
														<div class="uk-grid uk-grid-small" uk-grid>
															<div class="uk-width-1-2@s">
																<ul>
																	<li>First 10,000 SMS Included</li>
																	<li>25 User Reserved <a href="##">Keywords</a></li>
																	<li>1.5 Cents per Msg after</li>
																</ul>
															</div>

															<div class="uk-width-1-2@s">
																<ul>
																	<li>Unlimited <a href="##">MLPs</a></li>
																	<li>Unlimited Short URLs</a></li>
																</ul>
															</div>
														</div>
													</div> --->
												</div>
											</div>
										</div>

										<div class="secure-password">
											<h4>Secure Password Requirements</h4>
											<ul>
												<li>must be at least 8 characters</li>
												<li>must have at least 1 number</li>
												<li>must have at least 1 letter</li>
															
											</ul>
										</div>
									</div>
								</div>

								<div class="uk-width-1-1">
									<div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
							            <label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
							            	<input class="" id="agree" type="checkbox"> 
							            	<span>I’ve read and agree to the <a href="/term-of-use">Terms and Conditions of Use.</a></span>
							            </label>
							        </div>
								</div>

								<div class="uk-width-1-1">
									<button class="btn newbtn green-gd" id="start-btn" disabled>get started!</button>
								</div>
							</div>
						</form>
					</div>

					<div class="box-start-free" style="display:none" id="section-2">
						<form name="payment_form" id="payment_form" autocomplete="off">
						 <input type="hidden" name="payment_method_value" id="payment_method_value" value="">
							<div class="uk-grid uk-grid-medium" uk-grid>
								<div class="uk-width-1-1">
									<h4 class="payment-title">Secure Payment</h4>

									<div class="uk-grid" uk-grid>
										<div class="uk-width-1-2@m">
											<div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
												<div class="uk-width-auto">
													<div class="form-group">
														<span>Select Your Plan:</span>
													</div>
												</div>
												<div class="uk-width-1-3@m">
													<cfif url.yearly NEQ 1>
			    										<select class="form-control validate[required]" id="select-plan-2">
			    											<option value="" disabled selected>Select a plan</option>
			    											<cfoutput>
			    												<cfloop query="#plans.DATA#">
			    													<option value="#id#"><b>#name#</b> - $#price#/month</option>
			    												</cfloop>
			    											</cfoutput>
			    											<!--- <option value="1"><b>Free</b> - $0/month</option>
			    											<option value="2"><b>Individual</b> - $24/month</option>
			    											<option value="6"><b>Pro</b> - $99/month</option>
			    											<option value="3"><b>SMB</b> - $249/month</option> --->
			    										</select>
			    									<cfelse>
			    										<select class="form-control validate[required]" id="select-plan-2">
			    											<option value="" disabled selected>Select a plan</option>
			    											<cfoutput>
			    												<cfloop query="#plans.DATA#">
			    													<option value="#id#"><b>#name#</b> - $#yearlyAmount*12#/year</option>
			    												</cfloop>
			    											</cfoutput>
			    											<!--- <option value="1"><b>Free</b> - $0/month</option>
			    											<option value="2"><b>Individual</b> - $24/month</option>
			    											<option value="6"><b>Pro</b> - $99/month</option>
			    											<option value="3"><b>SMB</b> - $249/month</option> --->
			    										</select>
			    									</cfif>
												</div>
											</div>

											<div class="uk-grid uk-grid-small uk-flex-middle">
												<div class="uk-width-auto">
													<div class="form-group">
														<span>Monthly Amount:</span>
													</div>
												</div>
												<div class="uk-width-expand">
													<cfif url.yearly NEQ 1>
														<cfoutput>
															<cfloop query="#plans.DATA#">
																<span id="span-plan-price-#id#" class="span-plan-price">$#price#</span>
															</cfloop>
														</cfoutput>
													<cfelse>
														<cfoutput>
															<cfloop query="#plans.DATA#">
																<span id="span-plan-price-#id#" class="span-plan-price">$#yearlyAmount*12#</span>
															</cfloop>
														</cfoutput>
													</cfif>
												</div>
											</div>
										</div>

										<div class="uk-width-1-2@m">
											<div class="form-group">
												<div class="uk-grid uk-grid-small " uk-grid>
													<div class="uk-width-auto">
														<div class="form-group" style="margin-top: 7px;">
															<span>Coupon Code: </span>
														</div>
													</div>

													<div class="uk-width-1-3@m">
														<input type="text" id="inpPromotionCode" class="form-control" value="<cfoutput>#couponCodeDefault#</cfoutput>">
														<span class="help-block help-block-coupon-signup-input" style="display: none;font-weight: bold;" id="help-block"></span>
													</div>

													<div class="uk-width-expand">
														<button class="btn  green-gd" id="apply-btn">apply</button>
														<button class="btn  green-cancel" id="remove-btn" style="display: none">remove</button>
													</div>
												</div>
												<!--- <div class="row">
													<div class="col-md-6 col-md-offset-2">
														
													</div>
												</div> --->
											</div>
										</div>
									</div>
								</div>

								<div class="form-group uk-first-column uk-width-1-1 payment-method hidden">
									<h4 class="payment-title">Payment Method</h4>
									<cfif RetGetPaymentMethodSetting.RESULT EQ 1>
										<div class="radio-inline" id="rad-Crecard">
											<label>
												<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="1" checked>
												<b>Credit/Debit Card</b>
											</label>
										</div>
									<cfelseif RetGetPaymentMethodSetting.RESULT EQ 2>
										<div class="radio-inline" id="rad-Paycard">
											<label>
												<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod2" value="2" checked>
												<img src="/public/sire/images/paypal_icon.png" class="paypal_icon_large">
											</label>
										</div>
									<cfelseif RetGetPaymentMethodSetting.RESULT EQ 3>
										<div class="radio-inline" id="rad-Crecard">
											<label>
												<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="3" checked>
												<b>Credit/Debit Card Mojo</b>
											</label>
										</div>
									<cfelse>
										<div class="radio-inline" id="rad-Crecard">
											<label>
												<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod1" value="1" checked>
												<b>Credit/Debit Card</b>
											</label>
										</div>
										<div class="radio-inline" id="rad-Paycard" style="padding-left: 22px !important;">
											<label>
												<input type="radio" name="select_payment_method" class="select_payment_method" id="optionsRadiosMethod2" value="0">
												<img src="/public/sire/images/paypal_icon.png" class="paypal_icon_large">
											</label>
										</div>
									</cfif>
								</div>
								<div class="form-group uk-width-1-1 uk-grid-margin">
									<div class="card-support-type">
										<img class="img-responsive" src="session/sire/assets/layouts/layout4/img/supported-payment-types.png" alt="">
									</div>
								</div>

								<div class="uk-width-1-1 uk-grid-margin uk-first-column">
									<h4 class="payment-title section-payment">Payment Information</h4>

									<div class="uk-grid uk-grid-medium holder-payment" >
										<div class="uk-width-1-2@m section-payment">
											<div class="uk-grid uk-grid-small" >
												<div class="uk-width-1-2@s">
													<div class="form-group">
														<label for="cardHolderFirtsName"><b>Cardholder Name </b> <span>*</span></label>
														<input type="text" id="cardHolderFirtsName" placeholder="First name" class="form-control validate[required,custom[onlyLetterNumberSp]]">
													</div>
												</div>

												<div class="uk-width-1-2@s">
													<div class="form-group">
														<label for="cardHolderLastName">&nbsp;</label>
														<input type="text" id="cardHolderLastName" placeholder="Last name" class="form-control validate[required,custom[onlyLetterNumberSp]]">
													</div>
												</div>

												<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
													<div class="form-group">
														<label for=""><b>Card Number </b> <span>*</span></label>
														<input type="text" id="cardNumber" class="form-control validate[required,custom[creditCardFunc]]">
													</div>
												</div>

												<div class="uk-width-1-3@s uk-grid-margin">
													<div class="form-group">
														<label for=""><b>Security Code </b> <span>*</span></label>
														<input type="text" id="securityCode" class="form-control validate[required,custom[onlyNumber] , minSize[3]]" maxlength="4" data-errormessage-range-underflow="* Security code should be 3 or 4 characters" >
													</div>
												</div>
													
												<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
													<div class="form-group">
														<label for="expireMonth"><b>Expiration Date </b> <span>*</span></label>
														<select name="expireMonth" id="expireMonth" class="form-control validate[required]">
															<cfoutput>
																<cfloop from="1" to="12" index="month">
																	<option value="#(month LT 10 ? "0"&month : month)#">#(month LT 10 ? "0"&month : month)#</option>
																</cfloop>
															</cfoutput>
														</select>
													</div>
												</div>

												<div class="uk-width-1-3@s uk-grid-margin">
													<div class="form-group">
														<label for="expireYear">&nbsp;</label>
														<select name="expireYear" id="expireYear" class="form-control validate[required]">
															<cfoutput>
																<cfset currYear = year(now())/>
																<cfloop from="0" to="9" index="year">
																	<option value="#(currYear+year)#">#(currYear+year)#</option>
																</cfloop>
															</cfoutput>
														</select>
													</div>
												</div>
											</div>											
										</div>

										<div class="uk-width-1-2@m section-payment">
											<div class="uk-grid uk-grid-small" >
												<div class="uk-width-1-1">
													<div class="form-group">
														<label for="address"><b>Address </b> <span>*</span></label>
														<input type="text" id="address" class="form-control validate[required,custom[noHTML],custom[validateAddrAndCity] ]">
													</div>
												</div>

												<div class="uk-width-1-1 uk-grid-margin uk-first-column">
													<div class="form-group">
														<label for="city"><b>City </b> <span>*</span></label>
														<input type="text" id="city" class="form-control validate[required,custom[noHTML],custom[validateAddrAndCity] ]">
													</div>
												</div>
													
												<div class="uk-width-2-3@s uk-grid-margin uk-first-column">
													<div class="form-group">
														<label for="state"><b>State </b> <span>*</span></label>
														<input type="text" id="state" class="form-control validate[required,custom[noHTML],custom[onlyLetterNumberSp]]" maxlength="2">
													</div>
												</div>

												<div class="uk-width-1-3@s uk-grid-margin">
													<div class="form-group">
														<label for="zipcode"><b>Zip Code</b> <span>*</span></label>
														<input type="text" id="zipcode" class="form-control validate[required,custom[onlyNumber]]" maxlength="5">
													</div>
												</div>
											</div>
										</div>

										<div class="uk-width-1-1 uk-grid-margin uk-first-column">
											<button class="btn newbtn green-gd" id="purchase-btn">purchase</button>
											<button class="btn newbtn green-cancel" id="back-btn">back</button>
											<button class="btn newbtn green-cancel" id="cancel-btn">cancel</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="InfoModalKeyword" class="modal fade" role="dialog">
	<div class="modal-dialog new-modal">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">What is a Keyword?</h4>
			</div>
			<div class="modal-body">
				<p class="text-center">
					Imagine if Joe’s Coffee Shop offers this promotion: Text “FreeCoffee” to 39492 and receive a free coffee on your next visit.  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.  
				</p>
			</div>
		</div>

	</div>
</div>
<!-- Modal -->
<div id="InfoModalMLP" class="modal fade" role="dialog">
	<div class="modal-dialog new-modal">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">What is a MLP?</h4>
			</div>
			<div class="modal-body">
				<p class="text-center">
					Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once customers clicks on that link they will be sent to your own Marketing Landing Page (MLP) displaying the coupon.  It’s a standalone web page promoting your offer and the best part is it’s included for free!
				</p>
			</div>
		</div>

	</div>
</div>

<cfparam name="variables._title" default="Start 1">

<cfinclude template="../views/layouts/home.cfm">