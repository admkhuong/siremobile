<cfparam  name="actionType" default="1"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="0">
<cfparam  name="ami" default="">


<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/start-free-new.min.js">
</cfinvoke>

<cfset variables._title = 'Sign Up - Sire'>

<cfset hideFixedSignupForm = '1'>
<cfset AffiliateInfomation = {
    AffiliateCode = "",
	AffiliateUserEmail= "" ,
	AffiliateUserID= 0	
}>

<script>    
    <cfif structKeyExists(url,'rf')>
        rfCode = <cfoutput>'#url.rf#'</cfoutput>
        <cfelse>
        rfCode = ''
    </cfif>   
     <cfif structKeyExists(url,'type')>
        rfType = <cfoutput>#url.type#</cfoutput>
        <cfelse>
        rfType = ''
    </cfif>    
</script>

<cfif  structKeyExists(cookie,'affiliatecode')> 
    <cfset ami=cookie.affiliatecode>
    <cfinvoke method="GetMasterInfo" component="public.sire.models.cfc.affiliate" returnvariable="GetMasterInfo">
        <cfinvokeargument name="inpAffiliateCode" value="#ami#">
    </cfinvoke>
    <cfif GetMasterInfo.RXRESULTCODE EQ 1>       
        <cfset actionType= 3> 
		<cfset affiliatetype= 2>
        <cfset AffiliateInfomation = {
            AffiliateCode = #ami#,
            AffiliateUserEmail= #GetMasterInfo.AFFILIATEINFO.EmailAddress_vch#,
            AffiliateUserID= #GetMasterInfo.AFFILIATEINFO.UserId_int#	
        }>               
    </cfif>
    <cfset StructClear(session)>   

    <cfset Session.USERID = "">
    <cfset Session.loggedIn = 0>   
    
</cfif>
<header class="sire-banner2 signup-new-banner header-banners">
	<section class="landing-top">
		<div class="container-fluid">
			<div class="row">
				<div class="public-logo">
					<div class="landing-logo clearfix">
						<a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
							<img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
							<img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
						</a>
					</div>
				</div>
				
				<div class="signup-without-navbar-btn">
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
						<cfinclude template="../views/commons/public_signin.cfm">  
					</cfif>
				</div>
				
			</div>
		</div>		
	</section>

	<section class="landing-jumbo landing-jumbo-feature landing-jumbo-newsignup">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 text-center">
	               <h1 class="sire-title-updated">LET'S CREATE YOUR FREE ACCOUNT</h1>
	               <p class="banner-sub-title"><i>100 Free Credits | No Commitment | No Credit Card Required</i></p>
				</div>
			</div>
		</div>	
	</section>
</header>

<section class="signup-block">
	<div class="container">
		<div class="inner-signup-block">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="row">
						<div class="col-lg-10 col-lg-offset-1">
							<form name="signup_form_1" id="signup_form_1" autocomplete="off">	
								<cfoutput>							
								<input type="hidden" name="" id="actionType" value="#actionType#">
                                <input type="hidden" name="" id="select-plan-2" value="1">
                                <input type="hidden" name="" id="affiliatetype" value="#affiliatetype#">
                                <input type="hidden" name="" id="AMI" value="#AffiliateInfomation.AffiliateUserID#">
								<input type="hidden" class="hidden" id="affiliatecode" value="#ami#" >								
								</cfoutput>
								<div class="uk-grid uk-grid-small" uk-grid>
									<cfif ami NEQ "" AND AffiliateInfomation.AffiliateUserID GT 0>
										<cfoutput>
										<div class="uk-width-1-1">
											<div class="sub-acc-infomation">											
												
												<hr style="border:0;border-bottom:1px solid ##74c37f">	   
												<p style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">Affiliate Information</p>                                         
												Affiliate Code: <span style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">#AffiliateInfomation.AffiliateCode#</span></br>
												Affiliate User Email: <b>#AffiliateInfomation.AffiliateUserEmail#</b></br>                                            
												<hr style="border:0;border-bottom:1px solid ##74c37f">											
											</div>
										</div>
										</cfoutput>
									</cfif>
									<div class="uk-width-1-2">
										<div class="form-group">
											<label for="">First name</label>
											<input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50">
										</div>
									</div>

									<div class="uk-width-1-2">
										<div class="form-group">
											<label for="">Last name</label>
											<input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50">
										</div>
									</div>

									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">SMS Number</label>
											<input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - ">
										</div>
									</div>

									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">E-mail Address</label>
											<input type="text" class="form-control validate[required,custom[email]]" id="emailadd" placeholder="">
										</div>
									</div>

									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">Password</label>
											<input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" placeholder="">
										</div>
									</div>

									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">Confirm Password</label>
											<input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" placeholder="">
										</div>
									</div>

									<div class="uk-width-1-1 last-box-newsignup">
										<div class="checkbox">
											<label for="agree" class="uk-flex uk-flex-middle conditioner uk-position-relative">
												<input name="agree" class="validate[required]" id="agree" type="checkbox"> 
												<span>I’ve read and agree to the <a href="/term-of-use">Terms and Conditions of Use.</a></span>
											</label>
										<div class="checkbox">
									</div>

									<div class="uk-width-1-1">
										<button class="btn newbtn green-gd" id="btn-new-signup">get started!</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-block-5 feature-gray">
	<div class="container how-sire-work-container">
		<div class="inner-home-block-5">

			<div class="row body-block">
				<div class="col-sm-4">
					<div class="how-sire-item">
						<div class="img">
							<img class="img-responsive" src="/public/sire/images/home/bubble-1.png" alt="">
						</div>
						<div class="content">
							<h4>Choose Your Keyword</h4>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="how-sire-item">
						<div class="img">
							<img class="img-responsive" src="/public/sire/images/home/bubble-2.png" alt="">
						</div>
						<div class="content">
							<h4>Select a Template</h4>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="how-sire-item">
						<div class="img">
							<img class="img-responsive" src="/public/sire/images/home/bubble-3.png" alt="">
						</div>
						<div class="content">
							<h4>Personalize Your Campaign</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home-block-3 dif-padding">
	<div class="container">
		<div class="inner-home-block-3">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="title">Testimonials</h4>					
				</div>

				<div class="col-sm-12">
					<div class="tes-item">
						<p><i>"Sire allows me to quickly shoot a text to my list of clients letting them know there’s a last minute appointment available.  I used to do this directly on my personal phone but was always uncomfortable with giving everyone my personal cell number."</i></p>
						<p><b>- Kim, HS Salon</b></p>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="tes-item">
						<p><i>"We Started doing text promotions this year and our business increased 40%."</i></p>
						<p><b>- Taka, owner of Oki Doki Restaurant</b></p>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="tes-item">
						<p><i>"Sire gave us a powerful tool to communicate with our customers. Communicating with Sire is always fast and efficient, allowing us to update current promotions, events and new models."</i></p>
						<p><b>- Stella, La Verne Power Equipment</b></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style type="text/css">
	@media (max-width: 480px){
		#signup_form_1 .formError {
		    left: inherit !important;
		}
	}

	#btn-new-signup{
		margin-top: 20px;
	}

	@media (min-width: 768px) and (max-width: 1440px){
		#btn-new-signup{
			margin-top: 8px;
		}
	}
	
</style>

<cfinclude template="../views/layouts/home.cfm">