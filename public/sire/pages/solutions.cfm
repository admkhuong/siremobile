

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/template-preview.js">
</cfinvoke>
<style type="text/css">
    div.TopHeaderLearning h4 {
        line-height: 25px;
    }
</style>
<div class="container solutions-container">

    <section class="container-body solutions-container-body">
        <h2 class="feature-title">USA - Carrier Reach List</h2>
                    
        <p class="feature-header-content usa-carrier-reach-list">In the United States, Sire has connectivity with the optimized gateways each operator reserves solely for application-to-person (A2P) message traffic. This should not be confused with services offering access to interoperator gateways used for peer-to-peer messaging—the sending of A2P traffic on those connections is strictly against market regulations in the United States.</p> 
	</section>

</div>


<section class="main-wrapper feature-main" id="feature-main" style="padding-bottom: 0">

    <div class="feature-white">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-12 TopHeaderLearning sire-sms-solution-header">                    
                   		                     
                      	<h2 class="FeatureHeaderColor">Sire SMS Solution</h2>
						<h4>                      	                            
                            Whether you're sending transactional or marketing SMS, the Sire solution is built upon solid SMS infrastructure, providing you with the industry’s best reliability, scalability, and SMS deliverability. Easily integrate with Sire’s cloud-based SMS infrastructure, eliminating the need to build, scale and maintain these systems in-house, allowing you to focus on growing your business. Sire SMS Solution provides tools that help you maintain compliance with CTIA and TCPA (Telephone Consumer Protection Act of 1991) most recent guidelines.
                        </h4>                                   
                        
					</div>
                                    
				</div>
			</div>
		</div>
	
	<!---
	
		Blasts
		
			Send alerts and information to a list of peopl
				Filter
				Segment
				Single Message or full interactive sessions
				
				
		
		List Building
		
				
		Triggered Messages
			Form or Automated API
	
	
		Consumer intiated Communications
			MO
			Surveys
			Data Aquisition
			
		
		EAI and custom integrations
		
		
		SIRE Power Features
		
	Cheaper Better Faster 
	Triggered CDF Campaigns
	
	
		
	
	--->
    
    <div class="feature-gray">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-12">                    
                   		                     
                      <div class="feature-title" id="feature1">One Platform, One Plan</div>
						<div class="feature-content">
                        	                                                       
                            <p>Our industry-leading features are available through both our transactional and marketing solutions—just purchase one plan and start sending.</p>
                                                        
						</div>                                   
                        
					</div>
                    
					<div class="col-md-12">
						 
                     	<img class="feature-img-no-bs" alt="" src="/public/sire/images/learning/platform.png">
                          
					</div>
                    
				</div>
			</div>
		</div>
        
        <div class="feature-white">
			<div class="container">
	         	<div class="row">
				
                     <div class="col-md-12">                    
                   		                     
                      	<div class="feature-title AlignCenter" id="feature1">Featured Templates:</div>
						<div class="feature-content">
                        
                            <div class="row">
                                                        
                            	<div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-2xoptin">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Double Opt In</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                
                        		<div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-confirmation">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Confirmation</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>                                
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-profile">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Investor Profile</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-mfa">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Multi Factor Auth</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-whosin">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Who is in</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                                                
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                
                                    <a href="cs-css">
                                        <div style="text-align:center; width:100%; margin-top:1em;">                                        
                                            <div class="TPButton" style="">
                            
                                                <img class="" src="/public/sire/images/template-1-image.png" style="">
                            
                                                <h3 style="margin-top:.2em; padding-bottom:0; margin-bottom:0;">Satisfaction Survey</h3>
                            
                                            </div>
                                        </div>
                                    </a>  
                                
                                </div>
                                               
                        	</div>
                                                            
						</div>                
                                                        
						                               
                        
					</div>
                                       
                               
				</div>
                
                
			</div>
		</div>
        
    	<div class="feature-gray">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-6">                    
                   		                     
                       <div class="definitionBox white-bg">
                       <h1>con·ver·sa·tion</h1>
                       <h2>/ˌkänvərˈsāSH(ə)n/</h2>
                       
                       <h3><i>noun</i></h3>
                       		
                            <div>
                            	the informal exchange of ideas by words.<br/>
								"the two people were deep in conversation"<br/>
                                <i>synonyms:</i>	discussion, talk, chat, gossip, tête-à-tête, heart-to-heart, exchange, dialogue;
                                <ul>                                
	                                <li>
                                    	an instance of this.
                                    	<p>"she used her phone to have a conversation about her order using SMS"</p>
                                    </li>                                                                    
                                </ul>                           
                            
                            </div>
                       
                       </div>	                                              
                        
					</div>
                    
					<div class="col-md-6">
						<div class="feature-title" id="feature1">Start a conversation with your customers</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Session Based Interactive SMS</b>                            
                            </p>
                            
                            <p>Sire assists you with easy to customize design templates that create the campaigns that will delight your customers.</p>
                                                       
                        	<p>Achieve the impressive response and conversion rates mobile marketing is known for with Sire’s highly personalized template picker for simple to complex tasks, and everything in between:</p>
                           
                            <p>                         
                                <b>Our team will help you get started, faster.</b>                            
                            </p>
                                
                            <p>Expert-customized templates available. We can build customized templates to suit your business needs. Then, just add your message, and hit send.</p>
                            
                            <p>Or schedule a personal product walk-through.</p>
                                
						</div>
					</div>
                    
				</div>
			</div>
		</div>
        
       	<div class="feature-white">
			<div class="container">
				<div class="row">
		
        
             		
                    <div class="col-md-4">   
                    	<img class="" alt="" src="/public/sire/images/learning/scalability-cloud.png">                           
					</div>          
                  		                     
                                         
					<div class="col-md-8">
						<div class="feature-title" id="feature1">Incredibly Scalable Infrastructure</div>
						<div class="feature-content">
                        	                            
                            <p>Whether you’re sending 100 SMS one time or millions of SMS per day, Sire’s SMS delivery infrastructure can scale to meet your demands. Our delivery infrastructure can easily scale up or down with your business ensuring that your transactional and marketing SMS are delivered. Sire also saves you time and money by eliminating the need for you to build and maintain your own infrastructure.</p>
                                                            
						</div>
					</div>
                    
                    
                    
                    
                    
				</div>
			</div>
		</div>
        
        
        <div class="feature-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="feature-title" id="feature1">List Building and Contact Management</div>
						<div class="feature-content">
                        	
                            <p>                         
                                Sire will help you to capture, store and manage SMS subscribers.  Your users can either send a keyword to join your list(s) or they can sign up via a form and that will trigger a confirmation via an API call. Make sure your call to action is placed in a prominent spot on your site - above the fold is best.  The more people that see your call to action, the more will use it.
                     	    </p>
                        
                        	<p> <a href="cs-list-building-tips">31 Tips and Tricks for Building Your Own SMS Marketing List</a></p>
                        </div>
					</div>
                    
                    
                     <div class="col-md-4">   
                    	<img class="" alt="" src="/public/sire/images/learning/list-building-strategies-7.png">                           
					</div>    
                    
                </div>
			</div>
		</div>
        
        
        <div class="feature-white">
        <div class="container">
            <div class="row">

              	<div class="col-md-4">                    
                       <img class="feature-img-no-bs" src="/public/sire/images/learning/interactions31.png" />                                                                  
                </div>

 				<div class="col-md-8">
                    <div class="feature-title" id="feature1">Transactional SMS and the Customer Lifecycle</div>
                    <div class="feature-content">
                        
                         
                        <p>Marketers and designers typically focus the majority of their efforts on optimizing their newsletters and promotional messages. The primary goals of these communications are usually new sales, leads, and conversions. However, there is a missed opportunity in engaging and retaining users with order confirmation SMS messages.</p>
                        
                        
                        <p>It typically costs between six and seven times more to acquire a new customer than to retain existing customers. In addition, repeat customers tend to purchase as much as 67% more than new customers.</p>
                        
                        
                        <p>Many businesses are leaving money on the table by neglecting current customers in favor of creating new businesses. Consider shifting some of your focus to moving customers from the first purchase to repeat purchases and brand loyalty.</p>    
                        
                        <p>While it’s still crucial to send and optimize promotional messages aimed at gaining new customers, take a look at what happens after a purchase is made. What do your after order messages look like? Confirmation? Tracking Numbers? What type of information do they include?</p>
                        
                        <p> <a href="cs-lifecycle">Learn more about the Customer Lifecycle</a></p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
     <!---   
         <div class="feature-white">
			<div class="container">
				<div class="row">
				
                     <div class="col-md-6">                    
                   		                     
                       	<img class="feature-img" alt="" src="../images/feature-demo1.png">
                                              
                        <div class="abs-bottom-left">
                            <div class="image-wrapper">
                              <span class="image-overlay overlay-grey">
                                <span class="content">Guided entry into SMS and Mobile App Marketing</span>
                              </span>
                              <img class="" alt="" src="../images/ipadtraining.jpg">
                            </div>
                        </div>                        
                        
					</div>
                    
					<div class="col-md-6">
						<div class="feature-title" id="feature1">Professional SMS starts with Sire templates.</div>
						<div class="feature-content">
                        	
                            <p>                         
                                <b>Not all message campaigns are created equal.</b>                            
                            </p>
                            
                            <p>Sire assists you with easy to customize design templates that create the campaigns that will delight your customers.</p>
                                                       
                        	<p>Achieve the impressive response and conversion rates mobile marketing is known for with Sire’s highly personalized template picker for simple to complex tasks, and everything in between:</p>
                           
                            <p>                         
                                <b>Our team will help you get started, faster.</b>                            
                            </p>
                                
                            <p>Expert-customized templates available. We can build customized templates to suit your business needs. Then, just add your message, and hit send.</p>
                            
                            <p>Or schedule a personal product walk-through.</p>
                                
						</div>
					</div>
				</div>
			</div>
		</div>
        
        
        <div class="feature-gray">
			<div class="container">
			 	<div class="row">
					<div class="col-md-6">
						<div class="feature-title" id="feature4">Reporting</div>
						<div class="feature-content">
                        <p><b>See more people come through the door</b></p>
                        
                        <p>SIRE’s reporting tools allow users to obtain information and insights on message deliverability and engagement levels.  Advanced reporting tools provide up-to-date information on account activity at all times. Users receive information in real-time allowing you to gauge how well things are going.</p>
                        <p>Customers will love getting your special offers, event invites, and product updates. See who’s engaged with real-time reports.</p>
                        
                        </div>
					</div>
					<div class="col-md-6">
						<img class="feature-img" alt="" src="../images/feature-demo4.png">
                        
                        <div class="abs-bottom-left">
                            <div class="image-wrapper">
                              <span class="image-overlay overlay-grey">
                                <span class="content">Track your success in real time.</span>
                              </span>
                              <img class="" alt="" src="../images/tracksuccess.jpg">
                            </div>
                        </div>
                        
					</div>
				</div>
			</div>
		</div>
        
         --->
         
        
</div>




<!--- MODAL POPUP --->
<div class="bootbox modal fade" id="previewCampaignModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Preview Campaign</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <!--- <button type="button" class="btn btn-primary btn-success-custom" id="btn-rename-campaign">Save</button> --->
        <button type="button" class="btn btn-default btn-back-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<cfparam name="variables._title" default="Solutions">
<cfinclude template="../views/layouts/learning-and-support-layout.cfm">

