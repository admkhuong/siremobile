<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/start-free.js">
</cfinvoke>

<section class="landing-top">
	<div class="container-fluid">
		<div class="row">
			<div class="public-logo">
                <div class="landing-logo clearfix">
                    <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                        <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                        <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                    </a>
                </div>
            </div>
			
			

            <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
            	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
            	<div class="header-nav pull-right" style="margin-right: 30px">
                    <cfinclude template="../views/commons/menu.cfm">    
                </div>  
            <cfelse>
            	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
            	<div class="header-nav">
                    <cfinclude template="../views/commons/menu_public.cfm">
                </div>  
        	</cfif>
                    
			<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
				&nbsp;	
			<cfelse>
                <cfinclude template="../views/commons/public_signin.cfm">  
            </cfif>
		</div>
	</div>		
</section>

<div class="bg-start-free">
	<div class="uk-container uk-container-center">
		<div class="uk-grid" uk-grid>
			<div class="uk-width-1-1">
				<div class="inner-start-free">
					<h4 class="start-title">START FOR FREE TODAY</h4>
					<p class="start-italic"><i>No credit card required</i></p>

					<div class="box-start-free">
						<div class="uk-grid uk-grid-medium" uk-grid>
							<div class="uk-width-1-2@m">
								<div class="uk-grid uk-grid-small" uk-grid>
									<div class="uk-width-1-2">
										<div class="form-group">
											<label for="">First Name</label>
    										<input type="text" class="form-control" id="" placeholder="">
										</div>
									</div>

									<div class="uk-width-1-2">
										<div class="form-group">
											<label for="">Last Name</label>
    										<input type="text" class="form-control" id="" placeholder="">
										</div>
									</div>
									
									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">SMS Number</label>
    										<input type="text" class="form-control" id="" placeholder="">
										</div>
									</div>
									
									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">E-mail Address</label>
    										<input type="text" class="form-control" id="" placeholder="">
										</div>
									</div>
									
									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">Password</label>
    										<input type="password" class="form-control" id="" placeholder="">
										</div>
									</div>

									<div class="uk-width-1-1">
										<div class="form-group">
											<label for="">Confirm Password</label>
    										<input type="password" class="form-control" id="" placeholder="">
										</div>
									</div>									
								</div>
							</div>

							<div class="uk-width-1-2@m">
								<div class="uk-height-1-1 uk-position-relative">
									<div class="uk-grid uk-grid-small" uk-grid>
										<div class="uk-width-1-1">
											<div class="form-group">
												<label for="">Plans</label>
	    										<select class="form-control" id="select-plan">
	    											<option value="">Select a plan</option>
	    											<option value="st1"><b>Free</b> - $0/month</option>
	    											<option value="st2"><b>Individual</b> - $24/month</option>
	    											<option value="st3"><b>Pro</b> - $99/month</option>
	    											<option value="st4"><b>SMB</b> - $249/month</option>
	    										</select>
											</div>

											<div class="content-st">
												<div class="item" id="st1">
													<div class="uk-grid uk-grid-small" uk-grid>
														<div class="uk-width-1-2@s">
															<ul>
																<li>First 25 SMS Included</li>
																<li>3 User Reserved <a href="##">Keywords</a></li>
																<li>2.7 Cents per Msg after</li>
															</ul>
														</div>

														<div class="uk-width-1-2@s">
															<ul>
																<li>2 <a href="##">MLPs</a></li>
																<li>2 Short URLs</a></li>
															</ul>
														</div>
													</div>
												</div>
												<div class="item" id="st2">
													<div class="uk-grid uk-grid-small" uk-grid>
														<div class="uk-width-1-2@s">
															<ul>
																<li>First 1,000 SMS Included</li>
																<li>5 User Reserved <a href="##">Keywords</a></li>
																<li>1.9 Cents per Msg after</li>
															</ul>
														</div>

														<div class="uk-width-1-2@s">
															<ul>
																<li>5 <a href="##">MLPs</a></li>
																<li>5 Short URLs</a></li>
															</ul>
														</div>
													</div>
												</div>
												<div class="item" id="st3">
													<div class="uk-grid uk-grid-small" uk-grid>
														<div class="uk-width-1-2@s">
															<ul>
																<li>First 4,000 SMS Included</li>
																<li>10 User Reserved <a href="##">Keywords</a></li>
																<li>1.7 Cents per Msg after</li>
															</ul>
														</div>

														<div class="uk-width-1-2@s">
															<ul>
																<li>Unlimited <a href="##">MLPs</a></li>
																<li>Unlimited Short URLs</a></li>
															</ul>
														</div>
													</div>
												</div>
												<div class="item" id="st4">
													<div class="uk-grid uk-grid-small" uk-grid>
														<div class="uk-width-1-2@s">
															<ul>
																<li>First 10,000 SMS Included</li>
																<li>25 User Reserved <a href="##">Keywords</a></li>
																<li>1.5 Cents per Msg after</li>
															</ul>
														</div>

														<div class="uk-width-1-2@s">
															<ul>
																<li>Unlimited <a href="##">MLPs</a></li>
																<li>Unlimited Short URLs</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="secure-password">
										<h4>Secure Password Requirements</h4>
										<ul>
											<li>must be at least 8 characters</li>
											<li>must have at least 1 number</li>
											<li>must have at least 1 uppercase letter</li>
											<li>must have at least 1 lower case letter</li>
											<li>must have at least 1 special character</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="uk-width-1-1">
								<div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
						            <label for="condition" class="uk-flex uk-flex-middle conditioner uk-position-relative">
						            	<input class="" id="condition" type="checkbox"> 
						            	<span>I’ve read and agree to the <a href="##">Terms and Conditions of Use.</a></span>
						            </label>
						        </div>
							</div>

							<div class="uk-width-1-1">
								<button class="btn newbtn green-gd">get started!</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Start 1">

<cfinclude template="../views/layouts/home.cfm">