<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
    <cfinvokeargument name="path" value="/public/sire/js/start-free.js">
</cfinvoke>

<section class="landing-top">
	<div class="container-fluid">
		<div class="row">
			<div class="public-logo">
                <div class="landing-logo clearfix">
                    <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                        <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                        <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                    </a>
                </div>
            </div>
			
			

            <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
            	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
            	<div class="header-nav pull-right" style="margin-right: 30px">
                    <cfinclude template="../views/commons/menu.cfm">    
                </div>  
            <cfelse>
            	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
            	<div class="header-nav">
                    <cfinclude template="../views/commons/menu_public.cfm">
                </div>  
        	</cfif>
                    
			<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
				&nbsp;	
			<cfelse>
                <cfinclude template="../views/commons/public_signin.cfm">  
            </cfif>
		</div>
	</div>		
</section>

<div class="bg-start-free">
	<div class="uk-container uk-container-center">
		<div class="uk-grid" uk-grid>
			<div class="uk-width-1-1">
				<div class="inner-start-free">
					<h4 class="start-title">START FOR FREE TODAY</h4>
					<p class="start-italic"><i>No credit card required</i></p>

					<div class="box-start-free">
						<div class="uk-grid uk-grid-medium" uk-grid>
							<div class="uk-width-1-1">
								<h4 class="payment-title">Secure Payment</h4>

								<div class="uk-grid" uk-grid>
									<div class="uk-width-1-2@m">
										<div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
											<div class="uk-width-auto">
												<div class="form-group">
													<span>Select Your Plan:</span>
												</div>
											</div>
											<div class="uk-width-1-3@m">
												<select name="" id="" class="form-control">
													<option value="">Free</option>
													<option value="">Individual</option>
													<option value="">Pro</option>
													<option value="">SMB</option>
												</select>
											</div>
										</div>

										<div class="uk-grid uk-grid-small uk-flex-middle">
											<div class="uk-width-auto">
												<div class="form-group">
													<span>Monthly Amount:</span>
												</div>
											</div>
											<div class="uk-width-expand">
												<span>$99</span>
											</div>
										</div>
									</div>

									<div class="uk-width-1-2@m">
										<div class="uk-grid uk-grid-small uk-flex-middle" uk-grid>
											<div class="uk-width-auto">
												<div class="form-group">
													<span>Coupon Code: </span>
												</div>
											</div>

											<div class="uk-width-1-3@m">
												<input type="text" class="form-control">
											</div>

											<div class="uk-width-expand">
												<button class="btn  green-gd">apply</button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="uk-width-1-1">
								<h4 class="payment-title">Payment Information</h4>

								<div class="uk-grid uk-grid-medium holder-payment" uk-grid>
									<div class="uk-width-1-2@m">
										<div class="uk-grid uk-grid-small" uk-grid>
											<div class="uk-width-1-1">
												<div class="form-group">
													<label for=""><b>Cardholder Name </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>
												
											<div class="uk-width-2-3@s">
												<div class="form-group">
													<label for=""><b>Card Number </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>

											<div class="uk-width-1-3@s">
												<div class="form-group">
													<label for=""><b>Security Code </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>
												
											<div class="uk-width-2-3@s">
												<div class="form-group">
													<label for=""><b>Expiration Date </b> <span>*</span></label>
													<select name="" id="" class="form-control">
														<option value="">Month</option>
														<option value="">Individual</option>
														<option value="">Pro</option>
														<option value="">SMB</option>
													</select>
												</div>
											</div>

											<div class="uk-width-1-3@s">
												<div class="form-group">
													<label for="">&nbsp;</label>
													<select name="" id="" class="form-control">
														<option value="">Year</option>
														<option value="">Individual</option>
														<option value="">Pro</option>
														<option value="">SMB</option>
													</select>
												</div>
											</div>
										</div>											
									</div>

									<div class="uk-width-1-2@m">
										<div class="uk-grid uk-grid-small" uk-grid>
											<div class="uk-width-1-1">
												<div class="form-group">
													<label for=""><b>Address </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>

											<div class="uk-width-1-1">
												<div class="form-group">
													<label for=""><b>City </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>
												
											<div class="uk-width-2-3@s">
												<div class="form-group">
													<label for=""><b>State </b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>

											<div class="uk-width-1-3@s">
												<div class="form-group">
													<label for=""><b>Zip Code</b> <span>*</span></label>
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
									</div>

									<div class="uk-width-1-1">
										<button class="btn newbtn green-gd">purchase</button>
										<button class="btn newbtn green-cancel">cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<cfparam name="variables._title" default="Start 2">

<cfinclude template="../views/layouts/home.cfm">