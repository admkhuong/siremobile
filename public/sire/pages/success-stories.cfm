	<header class="sire-banner2 success-bannernew header-banners">
        
		<section class="landing-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
                    
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                        <!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                        <div class="header-nav">
                            <cfinclude template="../views/commons/menu.cfm">    
                        </div>  
                    <cfelse>
                        <!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                        <div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
                        </div>  
                    </cfif>
                            
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                        &nbsp;  
                    <cfelse>
                        <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
                </div>
            </div>      
        </section>
			
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
                        <!--- <h1 class="banner-title-big hidden-xs">SUCCESS STORIES</h1>

                        <h3 class="banner-title-small visible-xs">SUCCESS STORIES</h3> --->

                        <h1 class="sire-title-updated">success stories</h1>

                        <p class="banner-sub-title "><i>Leverage SMS marketing for your business</i></p>

                        <cfif !(structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1) >
                            <a href="/signup" class="get-started mt-20">get started</a>
                        </cfif>


						<!---<p class="landing_title_med">SMS and Text Marketing: Relevant, Personalized, and Delivered in Context. </p>--->
                        <!---<p class="sms_marketing">Drive your own mobile customer engagements today!</p>--->
                        <!--- <h1 class="marketing_title_big hidden-xs">SUCCESS STORIES</h1>

                        <h4 class="marketing_title visible-xs">SUCCESS STORIES</h4> --->
					</div>	
				</div>	
                
                <!--- <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div> --->
                
                <!--- <div class="row">
                    <a href="#stories-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div> --->     
			</div>	
		</section>
	</header>
	
     <section class="success-block">
        <div class="container">
            <div class="inner-success-block">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="title">Case study: Roll It Sushi & Teriyaki</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="stories-logo">
                            <img class="img-responsive" src="/public/sire/images/stories-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="stories-content">
                            <p>
                                Having experienced lower sales volume at two of his locations, James Kozono, co-founder of Roll It Sushi and Teriyaki was searching for a better way to increase sales and find a marketing solution that was more reliable and effective. Sire had helped Roll It achieve a 25% increase in sales volume in just over three months!  <a href="https://s3-us-west-1.amazonaws.com/siremobile/docs/SIRE-rollitsushiteriyaki-case-study_v1.pdf">Read more</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>   
     
	<!--- <section class="main-wrapper feature-main" id="stories-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="stories-logo">
                        <img class="img-responsive" src="/public/sire/images/stories-logo.png" alt="">
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="stories-content">
                        <!--- <h4>Case Study: Roll It Sushi & Teriyaki</h4> --->
                        <p>
                            Having experienced lower sales volume at two of his locations, James Kozono, co-founder of Roll It Sushi and Teriyaki was searching for a better way to increase sales and find a marketing solution that was more reliable and effective.
                        </p>
                        <p>
                            <a href="https://s3-us-west-1.amazonaws.com/siremobile/docs/SIRE-rollitsushiteriyaki-case-study_v1.pdf" target="_blank">Read more</a> on how Sire had helped Roll It achieve a 25% increase in sales volume in just over three months!
                        </p>
                    </div>
                </div>
            </div>

            <div class="linein-stories">
                <h4><a href="https://s3-us-west-1.amazonaws.com/siremobile/docs/SIRE-restaurant-data-sheet_v8.pdf" target="_blank">Learn</a> how to leverage SMS marketing for your Restaurant</h4>
            </div>
        </div>
	</section> --->
   
	
<cfparam name="variables._title" default="Success Stories - Sire">
<cfinclude template="../views/layouts/plan_pricing.cfm">