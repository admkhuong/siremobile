<cfset hideFixedSignupForm = 1 />
<cfset isLeadsconPage = 1 />

<header class="sire-banner2 thankyou-banner mb-50">
        <div class="home_bg_inner">
            <div class="container content-thankyou-banner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="thankyou-logo hidden-xs">
                            <a href="/">
                                <img class="img-responsive" src="/public/sire/images/leadscon/logo-leadscon2.png">
                            </a>
                        </div>
                        <div class="thankyou-logo visible-xs">
                            <a href="/">
                                <img class="img-responsive" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>

                    <div class="col-md-8 col-md-offset-2">
                        <div class="box-thankyou">
                            <h4>Thank You. You're all set!</h4>

                            <p>
                                We recieved your info and our Senior Campaign Strategist will reach out to you shortly.
                            </p>

                            <p>
                                If you can't wait and you're just dying to talk to us sooner, shoot us an email at support@siremobile.com. We'll get back to you asap!
                            </p>

                            <a href="leadscon" class="get-started">Return to Previous Page</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
</header>

<section class="home-block-4 mb-20">
    <div class="container">
        <div class="inner-home-block-4">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/N0UrR_G_tFc?rel=0&modestbranding=1&enablejsapi=1&autoplay=0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center thankyou-last-text"><span class="text-center">Sire Mobile 2017 | <a href="http://www.Siremobile.com" target="_blank" class="thankyou-sire-link">www.SireMobile.com</a></span></div>
    </div>
</section>

<cfparam name="variables._title" default="Affordable Mobile Marketing & SMS Marketing">
<cfinclude template="../views/layouts/home.cfm">