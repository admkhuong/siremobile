<cfset hideFixedSignupForm = 1 />
<cfset ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">


<header class="sire-banner2 thankyou-bannernew header-banners">

    <section class="landing-top">
        <div class="container-fluid">
            <div class="row">
                <div class="public-logo">
                    <div class="landing-logo clearfix">
                    <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                            <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                            <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                        </a>
                    </div>
                </div>
                
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    <!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    <div class="header-nav pull-right" style="margin-right: 30px">
                        <cfinclude template="../views/commons/menu.cfm">    
                    </div>  
                <cfelse>
                    <!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    <div class="header-nav">
                        <cfinclude template="../views/commons/menu_public.cfm">
                    </div>  
                </cfif>
                        
                <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                    &nbsp;  
                <cfelse>
                    <cfinclude template="../views/commons/public_signin.cfm">  
                </cfif>
            </div>
        </div>      
    </section>
   
   
    <section class="landing-jumbo landing-thankyou">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center thankyou-detail">
                    <!--- <h1 class="banner-title-big hidden-xs">Get in touch</h1>

                    <h3 class="banner-title-small visible-xs">Get in touch</h3> --->
                    
                    <h1 class="sire-title-updated">Welcome to the family!</h1>

                    <p class="banner-sub-title thankyou-desc">
                        <i>We just sent you an email with the next steps to launching your first campaign.<br>You marketing savants will probably dive in without much guidance. Go for it! <br> If you're a rookie and need some help, we're here for you. <br>Shoot us a note to <a href="mailto:help@siremobile.com" target="_top">help@siremobile.com</a> and we'll make you a pro in no time.</i>
                    </p>

                
                    <cfif !(structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1) >
                        <a class="get-started mt-30 ok-padding ok-thankyou">OK</a>
                    </cfif>
                
                </div>  
            </div>  
        
        </div>
    </section>
        
</header>

<section class="thankyou-block-1">
    <div class="container-fluid">
        <div class="inner-thankyou-block-1">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Thank you for joining Sire!</h4>
                </div>
            </div>
        </div>
    </div>
</section>
   
	
<cfparam name="variables._title" default="Thank You">
<cfinclude template="../views/layouts/plan_pricing.cfm">
<script> 
    <cfoutput>var userid="#SESSION.USERID#";</cfoutput>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga'); 
    ga('create', 'UA-82724101-1', 'auto',{
                    "userId": userid
            }); 
    ga('set', 'dimension1', userid); 
    ga(function(tracker) { 
    tracker.set('dimension2', tracker.get('clientId'));
    });
     ga('send', 'pageview');
</script> 

<script type="application/javascript">
    
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var inviteFriends = getUrlParameter('invite');
    var plan = getUrlParameter('plan');
    var rf = getUrlParameter('rf');
    var rftype= getUrlParameter('type');    
    var redirect=    getUrlParameter('redirect');    
    

    $(".ok-padding").click(function(event) {
         
        if(typeof(plan) !== 'undefined' && typeof(rf) === 'undefined') {
            window.location.href = '<cfoutput>#ROOTURL_HTTP#</cfoutput>/order-plan?plan=' + plan;   
            }

        else if (typeof(plan) !== 'undefined' && typeof(rf) !== 'undefined') {
            window.location.href = '<cfoutput>#ROOTURL_HTTP#</cfoutput>/order-plan?plan=' + plan + '&rf=' + rf + '&type=' + rftype;
        }
        else if(inviteFriends === "true") {
                window.location.href = '<cfoutput>#ROOTURL_HTTP#</cfoutput>/device-register?invite=true';
            } 
        else {
            if(redirect != undefined)
            {
                window.location.href = '<cfoutput>#ROOTURL_HTTP#</cfoutput>/device-register?redirect='+redirect;
            }
            else
            {
                window.location.href = '<cfoutput>#ROOTURL_HTTP#</cfoutput>/device-register';
            }                        
        }
    });
</script>
