

<cfinvoke component="public.sire.models.cfc.info" method="GetUSReachList" returnvariable="RetVarGetUSReachList">	
</cfinvoke>

 <div class="container">

         <section class="container-body">

            
            <h1>USA - Carrier Reach List</h1>
                        
            <p>In the United States, Sire has connectivity with the optimized gateways each operator reserves solely for application-to-person (A2P) message traffic. This should not be confused with services offering access to interoperator gateways used for peer-to-peer messaging—the sending of A2P traffic on those connections is strictly against market regulations in the United States.</p>
                               
                
                <cfoutput>
                    
                    <div class="table-responsive">          
                          <table class="table">
                            <thead>
                              <tr>                                
                                <th>Country</th>
                                <th>Carrier</th>
                                <th>Alias</th>
                                <th>Group</th>                                
                              </tr>
                            </thead>
                            <tbody>
                            
                    <cfloop query="RetVarGetUSReachList">   
                    
                     	
                              <tr>
                                <td>#RetVarGetUSReachList.Country_vch#</td>
                                <td>#RetVarGetUSReachList.Operator_vch#</td>
                                <td>#RetVarGetUSReachList.OperatorAlias_vch#</td>
                                <td>#RetVarGetUSReachList.CarrierGroup_vch#</td>                                
                              </tr>
                                                  
                          
                    </cfloop>         


							</tbody>
                          </table>
                          </div>
                          

				</cfoutput>

   
    		<br/>
            <br/>

	</section>

</div>


<cfinclude template="../views/layouts/main_front_core_gray.cfm">