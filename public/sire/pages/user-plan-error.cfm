<cfset isUserPlanErrorPage = 1 />

<div id="extend-plan" class="container">
	<section class="container-body">
		<div class="row">
    		<div class="col-md-12">
    			Your account is expired. Please contact customer support at <a href="mailto:support@siremobile.com">support@siremobile.com</a>.
    		</div>
        </div>
    </section>
</div>        

<div id="processingPayment">
    <img src="/public/sire/images/loading.gif" class="ajax-loader"/>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addCss">
	<cfinvokeargument name="path" value="/public/sire/css/style.css">
</cfinvoke>
<cfparam name="variables._title" default="Extend plan - Sire">
<cfinclude template="../views/layouts/main_front_core_gray.cfm">
<!--- <cfinclude template="../../../session/sire/views/layouts/master.cfm"> --->

