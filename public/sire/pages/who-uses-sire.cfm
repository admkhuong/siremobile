	<header class="home header-who-sire header-banners">
        
		<section class="landing-top">
            <div class="container-fluid">
                <div class="row">
					<div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
					
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                    	<!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                    	<div class="header-nav pull-right" style="margin-right: 30px">
                            <cfinclude template="../views/commons/menu.cfm">    
	                    </div>  
                    <cfelse>
                    	<!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                    	<div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
	                    </div>  
                	</cfif>
                            
					<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
						&nbsp;	
					<cfelse>
		                <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
                </div>
            </div>      
        </section>
			
		<section class="landing-jumbo">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
                        <h1 class="banner-title-big hidden-xs">Who uses SIRE</h1>

                        <h3 class="banner-title-small visible-xs">Who uses SIRE</h3>

						<!---<p class="landing_title_med">SMS and Text Marketing: Relevant, Personalized, and Delivered in Context. </p>--->
                        <!---<p class="sms_marketing">Drive your own mobile customer engagements today!</p>--->
                        
                        <!--- <h1 class="marketing_title_big">Who uses SIRE</h1> --->
					</div>	
				</div>	
                
                <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div>
                
                <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div>
			</div>	
		</section>
	</header>
	
        
     

	<section class="main-wrapper feature-main" id="feature-main">
    	
        <div class="container text-center">
            <div class="row who-row-1">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="img sprite-2"></div>
                    <div class="text">Small<br>Businesses</div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="img sprite-3"></div>
                    <div class="text">Non-Profit<br>Organizations</div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="img sprite-1"></div>
                    <div class="text">Group<br>Organizers</div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="img sprite-4"></div>
                    <div class="text">And many<br>more!</div>
                </div>
            </div>
        </div>

        <div class="who-row-2">
            <h2 class="heading-home text-center">
                With Sire, we make it very easy using the most commonly used communication method, text messages. Sire’s easy to use application allows you to create and launch your own personalized campaign within a few minutes. It’s that Simple!
            </h2>
        </div>

        <div class="clearfix"></div>

        <!--- <div class="container">
            <div class="wrap-sire-mbls">
                <div class="who-row-3 row text-center">
                    <h2>With Sire Mobile, You can:</h2>
                    <div class="col-md-4 col-sm-6">
                        <div class="img sprite-1"></div>
                        <div class="text">Choose Template</div>
                        <!--- <div class="text">Build your group <br> using <a href="/cs-templates?tab=building">List building </a></div> --->
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="img sprite-2"></div>
                        <div class="text">Build Campaign</div>
                        <!--- <div class="text">Send <a href="/cs-templates?tab=complex">complex <br> interactive campaigns</a> </div> --->
                    </div>
                    <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3">
                        <div class="img sprite-3"></div>
                        <div class="text">Click Send</div>
                        <!--- <div class="text">And a lot more! See our <br> <a href="/cs-templates">Template Library</a> and <a href="/public/sire/pages/features">Easy To Use Features</a> to learn more</div> --->
                    </div>
                </div>
            </div>
        </div> --->

        <div class="container">
            <div class="wrap-who-step">
                <div class="text-center heading-who">
                    <h4>It’s easy as 1-2-3 with Sire:</h4>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="who-step">
                            <div class="img-step">
                                <img src="/public/sire/images/who-step-1.png" alt="">
                            </div>
                            <div class="tit-step">
                                <h4>Choose from over<br>50+ templates</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="who-step">
                            <div class="img-step">
                                <img src="/public/sire/images/who-step-2.png" alt="">
                            </div>
                            <div class="tit-step">
                                <h4>Personalize your<br>campaign</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="who-step">
                            <div class="img-step no-step">
                                <img src="/public/sire/images/who-step-3.png" alt="">
                            </div>
                            <div class="tit-step">
                                <h4>Click Send</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        
        <div class="bg-free-account">
            <h3>Sign up for a free account!</h3>
            <h4>No credit card  |  No fees  |  No Commitment</h4>
        </div>
	</section>
   
	
<cfparam name="variables._title" default="Who Uses Sire - Sire">
<cfinclude template="../views/layouts/plan_pricing.cfm">
<script type="text/javascript">
    $('.li-who').addClass('active');
</script>
