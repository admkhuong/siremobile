	<header class="sire-banner2 whynew-banner header-banners">
        
		<section class="landing-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="public-logo">
                        <div class="landing-logo clearfix">
                            <a href="/">
                                <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                                <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
                            </a>
                        </div>
                    </div>
                    
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
                        <!--- <div class="col-sm-10 col-sm-push-0 col-xs-1 col-xs-push-6 col-lg-11 header-nav"> --->
                        <div class="header-nav">
                            <cfinclude template="../views/commons/menu.cfm">    
                        </div>  
                    <cfelse>
                        <!--- <div class="col-sm-8 col-sm-push-0 col-xs-2 col-xs-push-6 col-lg-9 header-nav"> --->
                        <div class="header-nav">
                            <cfinclude template="../views/commons/menu_public.cfm">
                        </div>  
                    </cfif>
                            
                    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
                        &nbsp;  
                    <cfelse>
                        <cfinclude template="../views/commons/public_signin.cfm">  
                    </cfif>
                </div>
            </div>      
        </section>
			
		<section class="landing-jumbo why-sire-lj">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 text-center">
                        <!--- <h1 class="banner-title-big hidden-xs">SMS Marketing is the most affordable</h1>
                        <h1 class="banner-title-big hidden-xs">way to grow your business</h1>

                        <h3 class="banner-title-small visible-xs">SMS Marketing is the most affordable</h3>
                        <h3 class="banner-title-small visible-xs">way to grow your business</h3> --->

                        <h1 class="sire-title-updated">SMS Marketing is the most affordable</h1>
                        <h1 class="sire-title-updated">way to grow your business</h1>

                        <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
                        <cfelse>
                            <a href="signup" class="get-started mt-30">get started</a>
                        </cfif>
                        

						<!---<p class="landing_title_med">SMS and Text Marketing: Relevant, Personalized, and Delivered in Context. </p>--->
                        <!---<p class="sms_marketing">Drive your own mobile customer engagements today!</p>--->
                        
                        <!--- <h1 class="marketing_title_big">Why sire?</h1> --->
					</div>	
				</div>	
                
                <!--- <div class="row hidden-xs hidden-sm">
                	<div class="scroll-down-spacer_3"></div>
                </div>
                
                <div class="row visible-xs visible-sm">
                	<div class="scroll-down-spacer_1"></div>
                </div> --->
                
                <!--- <div class="row">
                    <a href="#feature-main">
                    <div class="scroll-down">
                        <span>
                            <i class="fa fa-angle-down fa-2x"></i>
                        </span>
                    </div>
                    </a>
                </div> --->     
			</div>	
		</section>
	</header>

    <section class="leadscon-block2">
        <div class="container why-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="inner-leadscon-block2 inner-why-block-0">
                        <h4 class="title text-center">AI is confusing but using it should be simple.</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="inner-leadscon-block2">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                
                                <p class="short-desc">
                                    <i>
                                        The ability to understand natural human language responses means more sales, better customer service and a cooler sexier marketing platform for your business.  Check out all elements of AI we incorporate into our Intelligent Conversational Interface (ICI). 
                                    </i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="inner-leadscon-block2">
                        <div class="row">
                            <div class="col-sm-12 ai-ici-image text-center">
                                <img class="img-responsive extend-img" src="/public/sire/images/leadscon/ai-ici.png" alt="">
                            </div>

                            <div class="col-sm-12 text-center">
                                <a href="signup" class="get-started">get started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
    <section class="why-block-1">
        <div class="container-fluid">
            <div class="inner-why-block-1">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>If these stats don’t convince you, I don’t know what will</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-2">
        <div class="container">
            <div class="inner-why-block-2">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>It takes an<br>average consumer</li>
                                        <li>90</li>
                                        <li>seconds</li>
                                        <li>to respond to</li>
                                        <li>a <span class="mess">text mesage</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>It takes an<br>average consumer</li>
                                        <li>90</li>
                                        <li>minutes</li>
                                        <li>to respond to</li>
                                        <li>an <span class="email">email</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-3">
        <div class="container-fluid">
            <div class="inner-why-block-3">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>90% of texts are read within 3 minutes</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-4">
        <div class="container">
            <div class="inner-why-block-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>98%</li>
                                        <li>of <span class="texts">texts</span> are read</li>
                                        <li>45%</li>
                                        <li>response rate for <span class="texts">texts</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>28%</li>
                                        <li>of <span class="emails">emails</span> are read</li>
                                        <li>6%</li>
                                        <li>response rate for <span class="emails">emails</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-5">
        <div class="container-fluid">
            <div class="inner-why-block-5">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>text is the <span>#1</span> used feature on a smartphone</h4>
                        <h4><i>millennials exchange an average of <span>67 texts per day</span></i></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-4">
        <div class="container">
            <div class="inner-why-block-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>36%</li>
                                        <li>CTR for <span class="texts">texts</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="item">
                                    <ul class="nav nav-list">
                                        <li>6%</li>
                                        <li>CTR for <span class="emails">emails</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-block-5">
        <div class="container-fluid">
            <div class="inner-why-block-5">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>text coupons are redeemed <span>10x</span> more than traditional coupons</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
    <cfelse>
        <section class="home-block-3 how-last">
            <div class="container">
                <div class="inner-home-block-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Convinced yet?</h4>
                            <p>Pay $0 and start your free account right away</p>
                            <a href="signup" class="get-started">get started</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </cfif>
    

    <section class="why-block-6">
        <div class="container new-container">
            <div class="inner-why-block-6">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="title">Why customers choose Sire</h4>
                    </div>
                </div>

                <div class="row block-why-item">
                    <div class="col-xs-12">
                        <div class="row">
                             <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-1.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Fast & simple</h4>
                                            <p>
                                                Launch your own (marketing) campaign
                                                in less than 5 minutes after signing up
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-2.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Ready to use templates</h4>
                                            <p>
                                                More than 50+ ready to use templates for
                                                group organizers and businesses
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-3.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Customization</h4>
                                            <p>
                                                Easily create your own personalized
                                                templates for any unique situation
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-4.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Affordable</h4>
                                            <p>
                                                The most affordable plans to fit your
                                                budget in flexible ways
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-5.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Support</h4>
                                            <p>
                                                24/7 text support
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row why-item">
                                    <div class="col-xs-3">
                                        <div class="img">
                                            <img class="img-responsive" src="/public/sire/images/why-sire/why-ic-6.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="content">
                                            <h4>Smart technology</h4>
                                            <p>
                                                “Smarter Interactive Response Engine” with 
                                                with natural language processing and AI
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
    <cfelse>
        <section class="home-block-3 how-last">
            <div class="container">
                <div class="inner-home-block-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Get Your Free Account Today</h4>
                            <p>
                                <i>
                                    <span class="block-on-mobile">No Credit Card Required</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">Free Upgrade or Downgrade</span> <span class="hidden-mobile">|</span> <span class="block-on-mobile">No Hidden Fees</span>
                                </i>
                            </p>
                            <a href="signup" class="get-started">start now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </cfif>
	<!--- <section class="main-wrapper feature-main" id="feature-main">
    	
        <div class="container text-center">
            <h2 class="heading-home">
                Sire is an easy-to-use, affordable, and powerful text messaging tool that will improve your communications and campaigns.
            </h2>
            <h2 class="heading-home">Here’s why customers choose text messaging over everything else:</h2>
        </div>

        <div class="container">
            <div class="wrap-why-item sub">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>Guaranteed response</b> - 98% of text messages are read within 3 mins
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>Coupons will be redeemed</b> - 94% mobile redemption rate i.e. your customers will redeem your coupons if you send it to them
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>Reach more customers</b> - 80% of consumers use text messaging and is the most commonly used feature on a mobile device
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>Increased customer loyalty</b> - Opt out rate for a text messaging campaign is less than 3% meaning that your customers will be loyal if you send them a text message
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>No special downloads required</b> - text messaging is preinstalled on every mobile device in the globe
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-why-item">
                            <li>
                                <b>Text messaging is very fast and easy to use</b> - It takes an average consumer 90 seconds to respond to a text message
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                
        </div>

        <div class="container text-center">
            <h2 class="heading-home">Here is why customers choose Sire:</h2>
        </div>
        
        <div class="container">
            <div class="wrap-why-item">
                <div class="row">
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-1"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Fast & simple </span>- Launch your own (marketing) campaign in less than 5 minutes after signing up
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-3"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Ready to use templates </span>– More than 50+ ready to use templates for group organizers & businesses
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
                <div class="row">
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-2"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Customization </span>– Easily create your own personalized templates
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-4"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Affordable</span> – The most affordable plans to fit your budget
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-6"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Support</span> – 24/7 text support
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 why-sire-figure">
                        <div class="row row-medium">
                            <div class="col-xs-2 sprite">
                                <div class="sprite-5"></div>
                            </div>
                            <div class="col-xs-10">
                                <div class="wrap-ctwhy">
                                    <span class="highlight">Smart technology</span> – With natural language processing and artificial intelligence, Sire stands true for its name (Smarter Interactive Response Engine)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
            
        <div class="clearfix"></div>
	</section> --->
   
	
<cfparam name="variables._title" default="SMS Marketing Benefits">
<cfparam name="variables.meta_description" default="Text is the most used app on a smartphone. Text coupons are redeemed 10x more than traditional coupons. Marketing texts are read faster and at a higher rate than email.">
<cfinclude template="../views/layouts/plan_pricing.cfm">
<script type="text/javascript">
    $('.li-why').addClass('active');
</script>
<style type="text/css">
    @media (max-width: 480px){
        #footer {
            margin-top: 0px !important;
        }
    }
</style>
