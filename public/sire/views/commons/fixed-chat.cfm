<cfif application.liveChatEnabled eq false>
	<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
		<cfinvokeargument name="path" value="/public/sire/js/fixed_chat.js">
	</cfinvoke>
	<div class="fixed-chat">
		<div class="text-center fixed-btn">Contact Sire Team </div>
		<div class="fixed-chat-form">
			<div class="container-fluid">
				<form role="form" id="fixed-frm" method="POST" action="/contact-us">
					<div class="form-group">
						<label for="name">Name *</label>
						<input  required type="text" name="infoName" class="form-control fixedchat-frm validate[required,custom[noHTML]]" data-prompt-position="bottomLeft">
					</div>
				  	<div class="form-group">
					    <label for="email">Email or Phone*</label>
					    <input  required type="text" name="infoContact" class="form-control fixedchat-frm validate[required,custom[email_or_phone]]" data-prompt-position="bottomLeft">
				  	</div>
				  	<div class="form-group">
				    	<label for="phone">Message</label>
				    	<textarea name="infoMsg" class="form-control"></textarea> 
				  	</div>
				  	<div class="form-group">
				    	<button id="fxdchat-send-msg-btn"  type="submit" class=" pull-right btn btn-success-custom">Submit</button>
				  	</div>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
</cfif>