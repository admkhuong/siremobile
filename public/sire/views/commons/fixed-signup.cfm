<cfparam  name="actionType" default="1"> <!--- 1: sign-up , 2: sign-up-request-affiliate, 3: sign-up-apply-affiliate, 4: request-affiliate --->
<cfparam  name="affiliatetype" default="0">
<cfparam  name="ami_code" default="">
<cfset AffiliateInfomation = {
    AffiliateCode = "",
	AffiliateUserEmail= "" ,
	AffiliateUserID= 0	
}>

<cfif  structKeyExists(cookie,'affiliatecode')> 
    <cfset ami_code=cookie.affiliatecode>
    <cfinvoke method="GetMasterInfo" component="public.sire.models.cfc.affiliate" returnvariable="GetMasterInfo">
        <cfinvokeargument name="inpAffiliateCode" value="#ami_code#">
    </cfinvoke>
    <cfif GetMasterInfo.RXRESULTCODE EQ 1>       
        <cfset actionType= 3> 
		<cfset affiliatetype= 2>
        <cfset AffiliateInfomation = {
            AffiliateCode = #ami_code#,
            AffiliateUserEmail= #GetMasterInfo.AFFILIATEINFO.EmailAddress_vch#,
            AffiliateUserID= #GetMasterInfo.AFFILIATEINFO.UserId_int#	
        }>               
    </cfif>
    
</cfif>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.validationEngine.Functions.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/jquery.mask.min.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/sign_up.min.js">
</cfinvoke>

<!--- <div class="fixed-signup" style="font-family: Source Sans Pro">
	<div class="text-center fixed-signup-btn">Start Free Account</div>
	<div class="fixed-signup-form">
		<div class="container-fluid">
			<form method="post" action="##" name="signup_form" id="signup_form" class="signup_form_contact" autocomplete="off">
				<input type="hidden" value="" name="plan" id="plan">
				<div class="form-group">
					<label for="sms_number">SMS Number*</label>
				    <input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - " data-prompt-position="topLeft:100"/>
				</div>
			  	<div class="form-group">
				    <label for="emailadd">E-mail Address*</label>
				    <input type="email" class="form-control validate[required,custom[email]]" id="emailadd" name="emailadd" placeholder="" maxlength="250" data-prompt-position="topLeft:100">
			  	</div>
			  	<div class="form-group">
			    	<label for="inpPasswordSignup">Password*</label>
				    <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" name="inpPasswordSignup" placeholder="qduCkT*5" maxlength="50" data-prompt-position="topLeft:100"/> 
			  	</div>
			  	<div class="form-group">
				    <label for="inpConfirmPassword">Confirm Password*</label>
				    <input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" name="inpConfirmPassword" placeholder="qduCkT*5" maxlength="50" data-prompt-position="topLeft:100"/>
			    </div>
			  	<div class="checkbox">
				    <label>
			        <input type="checkbox" name="agree" id="agree" class="validate[required]" style="margin-top:10px" /> I've read and agree to the <a href="/public/sire/pages/term-of-use" target="_blank">Terms of Use</a> and <a href="/public/sire/pages/anti-spam" target="_blank"> SMS Terms of Service</a>.
				    </label>
				</div>
				<button type="button" class="btn btn-success btn-login btn-success-custom" id="btn-sign-up">GET STARTED</button>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
</div> --->


<div class="quick-start-account">
	<div class="inner">
		<div class="quick-btn" id="trigger-quick-account">
			<span>Start Free Account</span>
		</div>
		<div class="form-signup">
			<form method="post" action="##" name="signup_form" id="signup_form" class="" autocomplete="off">
				<input type="hidden" value="" name="plan" id="plan">
				<cfoutput>							
					<input type="hidden" name="" id="actionType" value="#actionType#">					
					<input type="hidden" name="" id="affiliatetype" value="#affiliatetype#">
					<input type="hidden" name="" id="AMI" value="#AffiliateInfomation.AffiliateUserID#">
					<input type="hidden" class="hidden" id="affiliatecode" value="#ami_code#" >
				</cfoutput>
				<cfif ami_code NEQ "" AND AffiliateInfomation.AffiliateUserID GT 0>
					<cfoutput>
					<div class="form-group">
						<div class="sub-acc-infomation">											
							
							<hr style="border:0;border-bottom:1px solid ##74c37f">	   
							<p style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">Affiliate Information</p>                                         
							Affiliate Code: <span style="font-size:18px;color:##74c37f;text-align:center;font-weight:bold">#AffiliateInfomation.AffiliateCode#</span></br>
							Affiliate User Email: <b>#AffiliateInfomation.AffiliateUserEmail#</b></br>                                            
							<hr style="border:0;border-bottom:1px solid ##74c37f">											
						</div>
					</div>
					</cfoutput>
				</cfif>
				<div class="form-group">
					<label for="sms_number">First Name*</label>
				    <input type="text" class="form-control validate[required,custom[noHTML]]" id="fname" placeholder="" maxlength="50" data-prompt-position="topLeft:100"/>
				</div>
				<div class="form-group">
					<label for="sms_number">Last Name*</label>
				    <input type="text" class="form-control validate[required,custom[noHTML]]" id="lname" placeholder="" maxlength="50" data-prompt-position="topLeft:100"/>
				</div>
				<div class="form-group">
					<label for="sms_number">SMS Number*</label>
				    <input type="text" class="form-control validate[required,custom[usPhoneNumber]]" id="sms_number" placeholder="( ) - " data-prompt-position="topLeft:100"/>
				</div>
			  	<div class="form-group">
				    <label for="emailadd">E-mail Address*</label>
				    <input type="email" class="form-control validate[required,custom[email]]" id="emailadd" name="emailadd" placeholder="" maxlength="250" data-prompt-position="topLeft:100">
			  	</div>
			  	<div class="form-group">
			    	<label for="inpPasswordSignup">Password*</label>
				    <input type="password" class="form-control validate[required,funcCall[isValidPassword]]" id="inpPasswordSignup" name="inpPasswordSignup" maxlength="50" data-prompt-position="topLeft:100"/> 
			  	</div>
			  	<div class="form-group">
				    <label for="inpConfirmPassword">Confirm Password*</label>
				    <input type="password" class="form-control validate[required,equals[inpPasswordSignup]]" id="inpConfirmPassword" name="inpConfirmPassword" maxlength="50" data-prompt-position="topLeft:100"/>
			    </div>
			  	<div class="checkbox">
				    <label>
			        <input type="checkbox" name="agree" id="agree" class="validate[required]"/> I've read and agree to the <a href="/public/sire/pages/term-of-use" target="_blank" class="term-of-use-link">Terms of Use</a> and <a href="/public/sire/pages/anti-spam" target="_blank" class="term-of-use-link"> SMS Terms of Service</a>.
				    </label>
				</div>
				
				<div class="clearfix">
					<button type="button" class="btn btn-success btn-login btn-success-custom" id="btn-sign-up1">GET STARTED</button>
				</div>
			</form>
		</div>
	</div>
</div>