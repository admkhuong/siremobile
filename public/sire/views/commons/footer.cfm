<cfif !isDefined('PublicPath')>
	<cfinclude template="../../../paths.cfm">
</cfif>

<cfparam name="Session._rememberPath" default="" />


<footer id="footer">
	<div class="container-fluid">
		<section class="contact-info">
			<!---888.335.0909  No contact in fo here at this time - go to contact us--->
		</section>
		<section class="social-link">
			<ul>
			  <li> <a href="https://www.facebook.com/siremobile/" target="_blank" title="Facebook"><img src="/public/sire/images/facebook.png?v=1.0" alt="Facebook"/></a></li>
			  <li> <a href="https://twitter.com/sire_mobile" target="_blank" title="Twitter"><img src="/public/sire/images/twitter.png?v=1.0" alt="Twitter"/></a></li>
			  <!--- <li> <a href="https://www.pinterest.com/siremobile/" target="_blank" title="Pinterest"><img src="../images/pinterest.png" alt="Twitter"/></a></li> --->
			  <!--- <li> <a href="https://www.instagram.com/siremobile/" target="_blank" title="Instagram"><img src="../images/instagram.png" alt="Twitter"/></a></li> --->

			  <li> <a href="https://www.youtube.com/channel/UCYqFX_Uy0MIY5i7gjgr_LXg" target="_blank" title="Youtube"><img src="/public/sire/images/youtube.png?v=1.0" alt="Youtube"/></a></li>
			  <li> <a href="https://www.linkedin.com/company/12952559" target="_blank" title="Linkedin"><img src="/public/sire/images/linkedin.png?v=1.0" alt="LinkedIn"/></a></li>
			  <li> <a href="https://plus.google.com/116619152084831668774" target="_blank" title="Google+"><img src="/public/sire/images/google_plus.png?v=1.0" alt="Twitter"/></a></li>
			</ul>
		</section>
		<nav class="nav-bot">
			<ul>
				<li>
					<a href="/about-us">About Us</a>
				</li>
				<li>
					<a href="/term-of-use">Terms of Use</a>
				</li>
				<li>
					<a href="/privacy-policy">Privacy Policy</a>
				</li>
				<li>
					<a href="/anti-spam">Anti-Spam Policy</a>
				</li>
                <br/>
                <br/>
				<li>
					<a href="/">&copy; SIRE, Inc. 2015</a>
				</li>
			</ul>
		</nav>
	 </div>
</footer>

<!--- Sign in popup --->
<div class="modal fade" id="signin" tabindex="-1" role="dialog" aria-labelledby="signInLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="modal-title col-sm-10">Sign In</h4>
        	<form name="signin" class="col-sm-10" autocomplete="off">
        	<input type="hidden" name="inpRememberMe" value=1>
        	 	<cfoutput>
        	       <input type="hidden" id="loginRedirect" value="#SESSION._rememberPath#">
                </cfoutput> 
			  <div class="form-group">
			    <label for="modalSignInEmail">E-mail Address</label>
			    <input type="text" class="form-control validate[required,custom[email]]" data-prompt-position="topLeft:100" id="modalSignInEmail" name="inpUserID">
			  </div>
			  <div class="form-group">
			    <label for="modalSignInPassword">Password</label>
			    <input type="password" class="form-control validate[required]" data-prompt-position="topLeft:100" id="modalSignInPassword" name="inpPassword" maxlength="50">
			  </div>
			  <div class="form-group">
			  	<label for="inpSigninPassword">Remember Me</label>
                <input type="checkbox" required="" id="inpRememberMe" name="inpRememberMe" value="1">
			  </div>
			  <div class="signin-message"></div>
			  <div class="form-group row modal-btn-func">
			    <div class="col-sm-5">
				    <button type="submit" class="btn btn-success btn-success-custom btn-modal-signin-submit">Login</button>
			    </div>
			    <div class="col-sm-7 modal-signin-forgot-link">
			    	<a id="modalSignInForgotLink" href="javascript:void(0)">Forgot Your Password?</a>
			    </div>
			  </div>
			  <div class="modal-btn-func modal-signin-signup">
			  	Don’t have a sign in? Create your free account <a href="signup">here</a>.
			  </div>
			</form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--- Sign out popup --->
<div class="modal fade" id="signout" tabindex="-1" role="dialog" aria-labelledby="signOutLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<form name="signout" class="row signout-form" autocomplete="off">
        		<input type="hidden" name="SESuserId" id="SESuserId" value="<cfoutput>#SESSION.USERID#</cfoutput>">
		    	<h4 class="col-sm-10 modal-title">Sign Out of SIRE</h4>
		    	<div class="col-sm-12">
					<div class="checkbox">
					    <label>
					      <input type="checkbox" id='DALD' name='DALD'> Deauthorize this device? May require MFA code at next log in.
					    </label>
					 </div>
				</div>	 
			    <div class="col-sm-12">
				    <button type="submit" class="btn btn-success btn-success-custom text-uppercase btn-modal-signout">Yes, Sign Out now</button>
				    <button type="submit" class="btn btn-primary btn-back-custom btn-modal-signout-cancel text-uppercase" data-dismiss="modal">Cancel</button>
			    </div>
		    </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--- Forgot Password popup --->
<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="modal-title col-sm-10">Forgot Password</h4>
        	<p class="col-sm-12 success-message hidden">
        		Check your email for a link with a special code needed to set your new password.
        	</p>
        	<form name="forgot" class="col-sm-10" autocomplete="off">
        	  <div class="form-group">
			    <label for="modalForgotEmail">Enter your email or username below to reset your password:</label>
			    <input type="text" class="form-control validate[required,custom[email]]" data-prompt-position="topLeft:100" id="modalForgotEmail" name="inpUserName">
			  </div>
        	  <div class="form-group">
        	  	<script src='https://www.google.com/recaptcha/api.js'></script>
        	  	<cfoutput>
    	  		<div class="g-recaptcha" data-sitekey="#PUBLICKEY#"></div>
        	  	</cfoutput>
			  </div>
			  <div class="forgot-message"></div>
			  <div class="form-group row modal-btn-func">
			    <div class="col-sm-5">
				    <button type="submit" class="btn btn-success btn-success-custom btn-modal-forgot-submit">Submit</button>
			    </div>
			  </div>
        	</form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--- Alert popup --->
<div class="modal fade" id="bootstrapAlert" tabindex="-1" role="dialog" aria-labelledby="alertLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-body-inner row">
        	<h4 class="col-sm-10 modal-title"></h4>
        	<p class="col-sm-12 alert-message">       		
        	</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="processingPayment">
    <img src="/public/sire/images/loading.gif" class="ajax-loader"/>
</div>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/sign_in.js">
</cfinvoke>

<cfinvoke component="public.sire.models.helpers.layout" method="addJs">
	<cfinvokeargument name="path" value="/public/sire/js/forgot_password.js">
</cfinvoke>