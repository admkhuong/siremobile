<header class="clearfix">
   <div class="container-fluid bg-gray">
      <div class="row">
         <div class="col-xs-4">
            <div class="landing-logo">
		       <h5 style="color: #74c37f; padding-top: 0; margin-top: 0;">Powered By:</h5>    
               <a href="/">
                  <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                  <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
               </a>
            </div>
         </div>
         <div class="col-xs-10">
            
         </div>
      </div>
   </div>
</header>