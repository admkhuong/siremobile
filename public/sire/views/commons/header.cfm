<header id="header">
    <nav class="navbar navbar-default navbar-site">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="#">
              <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
              <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Home</a></li>
              <li><a href="../navbar-static-top/">About</a></li>
              <li><a href="../navbar-fixed-top/">Contact</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reporting <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Reporting1</a></li>
                  <li><a href="#">Reporting2</a></li>
                  <li><a href="#">Reporting3</a></li>
                </ul>
              </li>
              <li><a href="#">My account</a></li>
            </ul>
          </div>
        </div>
      </nav>
</header>