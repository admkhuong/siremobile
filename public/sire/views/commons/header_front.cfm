<header class="clearfix">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-4 col-xs-4">
            <div class="landing-logo">
               <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                  <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                  <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
               </a>
            </div>
         </div>
         <div class="col-sm-4 col-sm-offset-4 col-xs-8">
            <div class="login-wrapper">
               <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
               <!--<button type="button" class="btn btn-success btn-login btn-success-custom" data-toggle="modal" data-target="#signout">SIGN OUT</button>-->
               <cfinclude template="menu.cfm">
               <cfelse>
               <cfinclude template="menu_public.cfm">
               </cfif>
            </div>
         </div>
      </div>
   </div>
</header>