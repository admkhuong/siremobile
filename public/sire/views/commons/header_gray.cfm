<header class="clearfix">
   <div class="offset-nav"></div>
   <div class="container-fluid bg-gray">
      <div class="row">
         <div class="public-logo">
            <div class="landing-logo clearfix">
               <a href="<cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > <cfoutput>/session/sire/pages/dashboard</cfoutput> <cfelse> <cfoutput>/</cfoutput> </cfif>">
                  <img class="logo-top hidden-xs" src="/public/sire/images/logo-v14.png">
                  <img class="logo-top visible-xs" src="/public/sire/images/logo2.png">
               </a>
            </div>
         </div>
      
         <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >  
            <div class="header-nav pull-right" style="margin-right: 30px;">
               <cfinclude template="menu.cfm">    
            </div>  
            <cfelse>
            <div class="header-nav">
               <cfinclude template="menu_public.cfm">
            </div>  
         </cfif>
                   
         <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 > 
            &nbsp;   
         <cfelse>
            <cfinclude template="../commons/public_signin.cfm">  
         </cfif>
      </div>
   </div>
</header>