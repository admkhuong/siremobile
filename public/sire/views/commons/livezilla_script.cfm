<cfparam name="applicationSetting" default="Off">
<cfinvoke component="session.sire.models.cfc.setting" method="getByName" returnvariable="applicationSetting">
	<cfinvokeargument name="inpName" value="liveChatOnOff">
	</cfinvoke> 

	<cfif structKeyExists(applicationSetting, "liveChatOnOff") and applicationSetting.liveChatOnOff EQ 'On'>
		<!--- livezilla.net code --->
		<!--- <script type="text/javascript" id="070c06149f6a8c5853eb0e07c126e11e" src="https://livezilla.siremobile.com/script.php?id=070c06149f6a8c5853eb0e07c126e11e"></script> --->
		<!--- http://www.livezilla.net --->
		<!-- Start of siremobile Zendesk Widget script -->
		<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="siremobile.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
		/*]]>*/</script>
<!-- End of siremobile Zendesk Widget script -->

	</cfif> 
