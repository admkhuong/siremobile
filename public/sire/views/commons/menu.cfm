<!---Verify whether to show dev features in production see - session.sire.models.cfc.admin method GetDevPermission to add your account user id for dev and testing --->
<cfparam name="isUserPlanErrorPage" default="" />
<cfinvoke method="GetDevPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetDevPermission" />
<cfinvoke method="GetAdminPermission" component="session.sire.models.cfc.admin" returnvariable="RetVarGetAdminPermission"><cfinvokeargument name="inpSkipRedirect" value="1"></cfinvoke>

<style>

.dropdown-header {
    color: #777 !important;  
}

</style>

<ul class="nav navbar-nav navbar-right large_screen new-public-menu <cfif isUserPlanErrorPage NEQ 1>hidden</cfif>">
  <li>
      <a href="/session/sire/pages/dashboard">Dashboard</a>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Campaigns<span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="/session/sire/pages/campaign-template-choice">Create Campaign</a></li>
      <li><a href="/session/sire/pages/campaign-manage">Manage Campaigns</a></li>
      <li><a href="/session/sire/pages/subscriber">Subscribers</a></li>
    </ul>
  </li>
          
		<li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools<span class="caret"></span></a>
       <ul class="dropdown-menu">
         <!--- Requires session.sire.models.cfc.admin method GetDevPermission to be loaded on this page first - remove this check when ready for full production release --->
	      <cfif RetVarGetDevPermission.ISDEVOK EQ  1>	        
       	</cfif>
			<li class="dropdown-header">Advanced Features</li>
			<li class=""><a href="/session/sire/pages/mlp-list" style="padding-left:2em;">Marketing Landing Pages</a></li>
			<li class=""><a href="/session/sire/pages/short-url-manage" style="padding-left:2em;">Manage Short URLs</a></li>
			<li class=""><a href="/session/sire/pages/send-one" style="padding-left:2em;">Single Campaign Trigger</a></li>
			<li><a href="/session/sire/pages/qa-tool" style="padding-left:2em;">QA Tool (With Time Machine)</a></li>
			<li><a href="/session/sire/pages/reports-dashboard" style="padding-left:2em;">Account Level Reporting and Analytics</a></li>
      
      <cfif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0>
        <li class="divider"></li>
        <li class="dropdown-header">Waitlist</li>
        <li><a href="/session/sire/pages/wait-list-manage" style="padding-left:2em;">Waitlist Manage</a></li>
        <li><a href="/session/sire/pages/wait-list-app" style="padding-left:2em;">Waitlist App</a></li>
      </cfif>  

			<li class="divider"></li>

			<li class="dropdown-header">Advanced Campaigns</li>
			<li><a href="/session/sire/pages/campaign-edit?templateid=116&adv=1" style="padding-left:2em;">Build new custom campaign</a></li>
	    <!--- Requires session.sire.models.cfc.admin method GetAdminPermission to be loaded on this page first --->
        <cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
	    	<li class="divider"></li>
			<li class="dropdown-header">Administrator Only Tools</li>
        <li class=""><a href="/session/sire/pages/admin-home" style="padding-left:2em;">Admin</a></li>
        </cfif>	          
       </ul>
    </li>            
    <!--- <li class="hidden-xs hidden-sm space"><a>|</a></li> --->
    <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support<span class="caret"></span></a>
       <ul class="dropdown-menu">
       	  	<li><a href="/session/sire/pages/welcome">Welcome Overview</a></li>
            <li class="divider"></li>
            <li class="dropdown-header">Video Tutorial</li>
              <li><a href="/how-it-works?vplay=1" style="padding-left:2em;">Overview</a></li>
              <li><a href="/how-it-works?vplay=2" style="padding-left:2em;">Create Campaign</a></li>
              <li><a href="/how-it-works?vplay=3" style="padding-left:2em;">Subscriber List</a></li>
       	  	<li class="divider"></li>
       	  	<li class="dropdown-header">Guided Tours</li>
       		<li><a href="/session/sire/pages/my-account?tour=gettingstarted&tourrs=true" style="padding-left:2em;">System Guide</a></li>
           	<li class="divider"></li>
            <li class="dropdown-header">Tutorials</li>
        	<li><a href="/session/sire/pages/support-transactional-api.cfm" style="padding-left:2em;">Transactional API</a></li>
			<li><a href="/public/sire/pages/cs-faqs.cfm" style="padding-left:2em;">FAQs</a></li>                     
       </ul>
    </li>
    <!--- <li class="hidden-xs hidden-sm space"><a>|</a></li> --->
    <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
       <ul class="dropdown-menu">
       	  <li><a href="/session/sire/pages/profile">My Profile</a></li>
       	  <!--- <li><a href="/session/sire/pages/change-password">Change Password</a></li> --->
       	  <li><a href="/session/sire/pages/my-plan">My Plan</a></li>
       	  <li><a href="/session/sire/pages/security-credentials">Security</a></li>
       	  <li><a href="#" data-toggle="modal" data-target="#signout">Sign out</a></li>
       </ul>
    </li>
</ul>
<ul class="nav navbar-nav navbar-right small_screen hidden" style="<cfif isUserPlanErrorPage NEQ 1>display:none !important</cfif>">
  <a href="#" class="navbar-toggle dropdown-toggle dropdown-toggle-custom new-toggle-button" data-toggle="dropdown" data-submenu="" aria-expanded="false">
     <span class="sr-only">Toggle navigation</span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
  </a>
  <ul class="dropdown-menu small-max-widthmenu">
	<li>
      <a href="/session/sire/pages/dashboard">Dashboard</a>
    </li>
   	<li class="dropdown-submenu"><a tabindex="0">Campaigns</a>
        <ul class="dropdown-menu">
          	<li><a href="/session/sire/pages/campaign-template-picker">Create Campaign</a></li>
		  	<li><a href="/session/sire/pages/campaign-manage">Manage Campaigns</a></li>
		  	<li><a href="/session/sire/pages/subscribers">Subscribers</a></li>
        </ul>
    </li>
	             	
    <li class="dropdown-submenu">
             <a tabindex="0">Tools</a>
             <ul class="dropdown-menu">
               <!--- Requires session.sire.models.cfc.admin method GetDevPermission to be loaded on this page first - remove this check when ready for full production release --->
				<cfif RetVarGetDevPermission.ISDEVOK EQ  1>	        
	            	
	           	</cfif>
				<li class="dropdown-header">Advanced Features</li>
				<li class=""><a href="/session/sire/pages/mlp-list" style="padding-left:2em;">Marketing Landing Pages</a></li>
				<li class=""><a href="/session/sire/pages/short-url-manage" style="padding-left:2em;">Manage Short URLs</a></li>
				<li class=""><a href="/session/sire/pages/send-one" style="padding-left:2em;">Single Campaign Trigger</a></li>
				<li><a href="/session/sire/pages/qa-tool" style="padding-left:2em;">QA Tool (With Time Machine)</a></li>
				<li><a href="/session/sire/pages/reports-dashboard" style="padding-left:2em;">Account Level Reporting and Analytics</a></li>
        
        <cfif FINDNOCASE(CGI.SERVER_NAME,'awsqa.siremobile.com') GT 0>
          <li class="divider"></li>
          <li class="dropdown-header">Waitlist</li>
          <li><a href="/session/sire/pages/wait-list-manage" style="padding-left:2em;">Waitlist Manage</a></li>
          <li><a href="/session/sire/pages/wait-list-app" style="padding-left:2em;">Waitlist App</a></li>
        </cfif>  
            
		<li class="divider"></li>
				<li class="dropdown-header">Advanced Campaigns</li>
				<li><a href="/session/sire/pages/campaign-edit?templateid=116&adv=1" style="padding-left:2em;">Build new custom campaign</a></li>
			
        <!--- Requires session.sire.models.cfc.admin method GetAdminPermission to be loaded on this page first --->
        <cfif RetVarGetAdminPermission.ISADMINOK EQ  1>
	      <li class="divider"></li>
          <li class="dropdown-header">Administrator Only Tools</li>
        <li class=""><a href="/session/sire/pages/admin-home" style="padding-left:2em;">Admin</a></li>
        </cfif>
		            
             </ul>
      </li>            
         	
		<li class="dropdown-submenu">
             <a tabindex="0">Help</a>
             <ul class="dropdown-menu">
             	  	 <li><a href="/session/sire/pages/welcome">Welcome Overview</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Video Tutorial</li>
                    <li><a href="/how-it-works?vplay=1" style="padding-left:2em;">Overview</a></li>
                    <li><a href="/how-it-works?vplay=2" style="padding-left:2em;">Create Campaign</a></li>
                    <li><a href="/how-it-works?vplay=3" style="padding-left:2em;">Subscriber List</a></li>
             	  	<li class="divider"></li>
             	  	<li class="dropdown-header">Guided Tours</li>
             		<li><a href="/session/sire/pages/my-account?tour=gettingstarted&tourrs=true" style="padding-left:2em;">System Guide</a></li>
                 	<li class="divider"></li>
                  <li class="dropdown-header">Tutorials</li>
              	<li><a href="/session/sire/pages/support-transactional-api.cfm" style="padding-left:2em;">Transactional API</a></li>                  
             </ul>
    </li>
      <li class="dropdown-submenu">
	    <a tabindex="0">My account</a>
	    <ul class="dropdown-menu">
	   	  <li><a href="/session/sire/pages/my-account">My Profile</a></li>
	   	  <li><a href="/session/sire/pages/change-password">Change Password</a></li>
	   	  <li><a href="/session/sire/pages/my-plan">My Plan</a></li>
	      <li><a href="/session/sire/pages/security-credentials">Security credentials</a></li>
	   	  <li><a href="#" data-toggle="modal" data-target="#signout">Sign out</a></li>
	    </ul>
  	</li>
 </ul>

</ul>