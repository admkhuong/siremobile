<nav class="navbar">	
	<div class="container-fluid">
		<div class="navbar-header has-toggle-dropdown dropdown">
			<button class="dropdown-toggle navbar-toggle" data-toggle="dropdown">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<ul class="nav navbar-nav hidden">
				<li class="li-features"><a tabindex="-1" href="/features">Features</a></li>
				<li class="li-live-demo"><a tabindex="-1" href="/how-it-works">How It Works</a></li>
				<!--- <li class="li-who"><a tabindex="-1" href="/who-uses-sire">Who</a></li> --->
				<li class="li-why"><a tabindex="-1" href="/why-sire">Why</a></li>
				<li class="li-plan-price"><a tabindex="-1" href="/plans-pricing">Pricing</a></li>
				<!--- <li class="li-live-demo"><a tabindex="-1" href="/how-to-use">How to use</a></li> --->
				<li class="li-contact"><a tabindex="-1" href="/contact-us">Contact</a></li>
			   <!--- <hr/>
				<li><a tabindex="-1" href="/public/sire/pages/learning-and-support">Learning and Support</a></li>--->
				<!---<li class="divider"></li>--->
			</ul>
		</div>
		
	</div>
</nav>


