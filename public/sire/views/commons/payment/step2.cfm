<cfoutput>
<div id="step2" style="display:none">
    	<div class="row">
    	
    	<div class="col-md-6">
            <div class="row heading">
                <div class="col-sm-12 heading-title">Cardholder Details<hr></div>
            </div>
            <div class="row">
            	<div class="col-sm-12 form-horizontal">
				  <div class="form-group">
				    <label for="line1" class="col-sm-4 control-label">Address:<span class="text-danger">*</span></label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="60" id="line1" name="line1">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="city" class="col-sm-4 control-label">City:<span class="text-danger">*</span></label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="40" id="city" name="city">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="state" class="col-sm-4 control-label">State:<span class="text-danger">*</span></label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[required,custom[noHTML]]" maxlength="40" id="state" name="state">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="zip" class="col-sm-4 control-label">Zip Code:<span class="text-danger">*</span></label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[required,custom[onlyNumber]]" maxlength="5" id="zip" name="zip" data-errormessage-custom-error="* Invalid zip code">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="country" class="col-sm-4 control-label">Country:<span class="text-danger">*</span></label>
				    <div class="col-sm-8">
						<select class="form-control validate[required]" id="country" name="country">
							<cfinclude template="/session/sire/views/commons/payment/country.cfm" >
						</select>
				    </div>
				  </div>
				  <!---
				  <div class="form-group">
				    <label for="phone" class="col-sm-4 control-label">Telephone:</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[custom[phone]]" maxlength="30" id="phone" name="phone">
				    </div>
				  </div>--->
				  <input type="hidden" class="form-control validate[custom[phone]]" maxlength="30" id="phone" name="phone">
				  <div class="form-group">
				    <label for="email" class="col-sm-4 control-label">Email:</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control validate[custom[email]]" maxlength="128" id="email" name="email">
				    </div>
				  </div>
			    </div>
            </div>
            <div class="row">
            <div class="col-sm-12 form-horizontal form-footer text-center">
	        		<a type="button" class="btn btn-medium btn-back-custom pull-right"  href="javascript:history.go(-1)">Cancel</a>
	        		<a type="button" id="btnStep2" class="btn btn-medium btn-success-custom  pull-right margin-right-5">Next</a>
	            	<a type="button" id="btnStep2_back" class="btn btn-medium btn-primary-custom pull-right margin-right-5">Back</a>
	    	</div>
		</div>
		</div>
        	</div>
        </div>
       
</cfoutput>