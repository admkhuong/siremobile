<cfoutput >
<div id="step3" style="display:none">
	<div class="row">
		<div class="col-md-6">
			<div class="row heading">
        		<div class="col-sm-12 heading-title">Payment Information<hr></div>
    		</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Currency:</div>
				<div class="col-xs-8"><div class="pull-right active">$ US Dollars</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Select Your Plan:</div>
				<div class="col-xs-8"><div id="rsSelectYourPlan" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Monthly Amount:</div>
				<div class="col-xs-8"><div id="rsAmount" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">Number of Plan Keyword:</div>
				<div class="col-xs-8"><div id="rsNumberofPlanKeyword" class="pull-right active">0</div></div>
			</div>
			<div class="row secure-payment-plan">
				<div class="col-xs-4">First Credits Include:</div>
				<div class="col-xs-8"><div id="rsFirstCreditsInclude" class="pull-right active">0</div></div>
			</div>
    		
    		<cfinclude template="/session/sire/views/commons/payment/billing_info.cfm" >
			
			<div class="row">
			    <div class="col-sm-12 small-info form-footer" style="color:red;padding-bottom:10px;">
			        By signing up you are signing up for a monthly subscription
			    </div>
				<div class="col-sm-12 text-center">
				   	<a type="button" class="btn btn-medium btn-back-custom pull-right"  href="/plans-pricing">Cancel</a>
			    	<button type="submit" class="btn btn-medium btn-success-custom btn-payment-credits pull-right margin-right-5">Sign Up Now</button>
			        <a type="button" id="btnStep3_back" class="btn btn-medium btn-primary-custom pull-right margin-right-5">Back</a>
			    </div>
			</div>
		</div>
		
		
	</div>
	
</div>
</cfoutput>