
<cfinvoke component="public.sire.models.cfc.plan" method="GetActivedPlans" returnvariable="plans">
	<cfinvokeargument name="inpNotIncludeEnterprisePlan" value="1">
</cfinvoke>
<cfif Session.loggedIn EQ 1>
	<cfinvoke method="GetUserPlan" component="session/sire/models/cfc/order_plan" returnvariable="getUserPlan">
	</cfinvoke>
</cfif>

<!--- Reduce plan price if users buy yearly plan --->
<cfset _SAVEPERCENTBYYEAR = 20/>

<!--- 
<p>Application or API Control</p>
<p>Reports, Exports and Dashboards</p>
<p>Common Shortcode Assigned</p>
 --->

<div class="">

	<div id ="price-page" class="features-home-inner container">
		<section>
			<div class="text-center month-year-plan-switch">
				<div class="inner">
					<span class="month-year-plan-text" id="monthly-plan-text">MONTHLY</span>
					<label class="switch">
						<input id="month-year-switch" type="checkbox">
						<span class="slider round"></span>
					</label>
					<span class="month-year-plan-text" id="yearly-plan-text">YEARLY</span>
					<span class="triangle-isosceles left">Save up to 20%</span>
				</div>
			</div>
		</section>
	
		<section class="row">

			<cfloop query="#plans.data#">				
				<div class="col-lg-3 col-sm-6" style="margin-top: 2em;">
	                <div class="sire-pricing" id="price<cfoutput>#id#</cfoutput>">
	                    <div class="price-title">
	                        <cfif price eq 0>
		                        <cfoutput><h2 class="other free-acc-other">#name#</h2></cfoutput>
		                    	<h4 class="no-credit-required">(No Credit Card Required)</h4>
	                    	<cfelse>
	                    		 <cfoutput><h2 class="other">#name#</h2></cfoutput>
	                    	</cfif>
	                    </div>
	                    <div class="price-header">
	                        <h2 class="monthly-bill"><cfoutput>$#price#<span class="mo">/month</span></cfoutput></h2>
	                        <cfif price neq 0>
		                        <br class="monthly-bill">
		                        <br class="monthly-bill">
		                        <h4 class="billing-monly-annually monthly-bill">(Billed monthly)</h4>
		                    <cfelse>
		                    	<br class="monthly-bill">
		                    	<br class="monthly-bill">
		                    	<h4 class="billing-monly-annually monthly-bill">&nbsp</h4>
		                    </cfif>
	
	                        <h2 class="yearly-bill"><cfoutput>$#yearlyAmount#<span class="mo">/month</span></cfoutput></h2>
		                    <cfif price neq 0>
		                        <br class="yearly-bill">
		                        <br class="yearly-bill">
		                        <h4 class="billing-monly-annually yearly-bill">(Billed annually)</h4>
		                    <cfelse>
		                    	<br class="yearly-bill">
		                    	<br class="yearly-bill">
		                    	<h4 class="billing-monly-annually yearly-bill">&nbsp</h4>
		                    </cfif>
	                    </div>
	                    <div class="price-content">
	                       <!--- <p><cfoutput>#price1.USERACCOUNTNUMBER#</cfoutput> User Account</p>--->
	                        <p>First <cfoutput>#NumberFormat(firstSMSIncluded_int,',')#</cfoutput> SMS Included</p>
	                        <p><cfoutput>#keywordsLimitNumber_int#</cfoutput> User Reserved <a data-toggle="modal" title="What is a Keyword?" href="#" data-target="#InfoModalKeyword">Keywords</a></p>
	                        <p><cfoutput>#NumberFormat(PriceMsgAfter_dec,'._')#</cfoutput> Cents per Msg after</p> 
	                        <p><cfoutput>#(MlpsLimitNumber_int EQ 0 ? "Unlimited" : NumberFormat(MlpsLimitNumber_int,','))#</cfoutput> <a data-toggle="modal" title="What is a MLP?"  href="#" data-target="#InfoModalMLP"> MLPs</a></p>
	                        <p><cfoutput>#(ShortUrlsLimitNumber_int EQ 0 ? "Unlimited" : NumberFormat(ShortUrlsLimitNumber_int,','))#</cfoutput> Short URLs</p>
	                    </div>
	                    <div class="pricing-btn monthly-bill">
	                    	<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
	                    		<cfif price eq 0>
	                    			<!--- <p><a class="btn btn-pricing" href="/signup-order-plan?plan=1<cfif URL.rf NEQ ''><cfoutput>&rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>" class="btn btn-pricing">Sign Up</a></p> --->
	                    			<p><a class="btn btn-pricing" href="signup<cfif URL.rf NEQ ''><cfoutput>/&rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>" class="btn btn-pricing">Sign Up</a></p>
		                    	<cfelse>
		                    		<p><a href="/signup-order-plan?plan=<cfoutput>#id#</cfoutput><cfif URL.rf NEQ ''><cfoutput>&rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>" class="btn btn-pricing">Order Now</a><p>
	                    		</cfif>
	                        	
	                        <cfelse>
		                        <p><a 	<cfif getUserPlan.PLANORDER GTE level OR getUserPlan.PLANEXPIRED GT 0 >disabled 
		                        	  	<cfelse> 
			                        	  	<cfif price eq 0>
				                        	  	href="/signup-order-plan?plan=1"
				                        	<cfelse>
				                        	  	<!--- href="/session/sire/pages/order-plan?plan=<cfoutput>#id#</cfoutput>" --->
				                        	  	href="/session/sire/pages/my-plan?active=upgrade"
			                        	  	</cfif>
		                        		</cfif> 
		                        		class="btn btn-pricing">
		                        		<cfif price eq 0>
			                        	  	Sign Up 
			                        	<cfelse>
			                        	  	Upgrade Plan
		                        	  	</cfif>
		                        	</a>
		                        <p>
	                        </cfif>
	                    </div>

	                    <div class="pricing-btn yearly-bill">
	                    	<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
	                    		<cfif price eq 0>
	                    			<!--- <p><a class="btn btn-pricing" href="/signup-order-plan?plan=1<cfif URL.rf NEQ ''><cfoutput>&rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>&yearly=1" class="btn btn-pricing">Sign Up</a></p> --->
	                    			<p><a class="btn btn-pricing" href="signup" class="btn btn-pricing">Sign Up</a></p>
		                    	<cfelse>
		                    		<p><a href="/signup-order-plan?plan=<cfoutput>#id#</cfoutput><cfif URL.rf NEQ ''><cfoutput>&rf=#URL.rf#&type=#URL.type#</cfoutput></cfif>&yearly=1" class="btn btn-pricing">Order Now</a><p>
	                    		</cfif>
	                        	
	                        <cfelse>
		                        <p><a 	<cfif getUserPlan.PLANORDER GTE level OR getUserPlan.PLANEXPIRED GT 0 >disabled 
		                        	  	<cfelse> 
			                        	  	<cfif price eq 0>
				                        	  	href="/signup-order-plan?plan=1&yearly=1"
				                        	<cfelse>
				                        	  	<!--- href="/session/sire/pages/order-plan?plan=<cfoutput>#id#</cfoutput>" --->
				                        	  	href="/session/sire/pages/my-plan?active=upgrade"
			                        	  	</cfif>
		                        		</cfif> 
		                        		class="btn btn-pricing">
		                        		<cfif price eq 0>
			                        	  	Sign Up 
			                        	<cfelse>
			                        	  	Upgrade Plan
		                        	  	</cfif>
		                        	</a>
		                        <p>
	                        </cfif>
	                    </div>

	                </div>
	                <div class="space"></div>
	            </div>				
			</cfloop>
       </section><!--price row-->

       

       <!--- <div class="block-get-these-feautures">
           	<div class="row">
	            <div class="col-md-12">
	                <div class="price-content">
	                    <div class="row">
							<div class="col-sm-5 col-md-4 text-center"> 
								<img class="logo_03 img-responsive" src="/public/sire/images/logo_03.png" alt="">
							</div>
							<div class="col-sm-7 col-md-8"> 
								<h3 class="price_features">Get these features with any Plan</h3>
								<div class="row">
									<div class="col-sm-6">
										<ul class="pricing-list">
											<li>Inbound and Outbound <a data-toggle="modal" href="#" data-target="#InfoModalSMS">Text Messaging</a></li>
											<li>Reporting</li>
											<li>Subscriber Management</li>
											<li>Unlimited Subscribers</li>
											<li>Templates</li>
										</ul>
									</div>
									<div class="col-sm-6">
								        <!--- <h4><a data-toggle="modal" href="#" data-target="#InfoModalAPI">API</a> Access</h4> --->
								        <ul class="pricing-list">
											<li>Assigned Common <a data-toggle="modal" href="#" data-target="#InfoModalShortCode">Short Code</a></li>
											<li>API Access</li>
											<li>Guides and Tutorials</li>
											<li>No Long Term Commitment</li>
										</ul>
									</div>
								</div>
							</div>
	                	</div>
	                </div>
	        	</div>
			</div>
       </div>			

        <cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 >
            <a href="/public/sire/pages/signup?invite=true">
            	<div class="col-md-12 plan-pricing-invite">
					<div class="col-md-4 col-md-push-8 col-sm-5 col-sm-push-7">
						<img src="/public/sire/images/plan_pricing_icon.png" alt="" class="img-responsive center-block">
					</div>
					<div class="col-md-8 col-md-pull-4 col-sm-7 col-sm-pull-5">
						<h3>Sign up and Invite your friend to Sire! </h3>
						<p>You and your friends receive incredible benefits!</p>
					</div>
				</div>
            </a>
            	<cfelse>
            		<a href="/session/sire/pages/invite-friend">
                    	<div class="col-md-12 plan-pricing-invite">
							<div class="col-md-4 col-md-push-8 col-sm-5 col-sm-push-7">
								<img src="/public/sire/images/plan_pricing_icon.png" alt="" class="img-responsive center-block">
							</div>
							<div class="col-md-8 col-md-pull-4 col-sm-7 col-sm-pull-5">
								<h3>Sign up and Invite your friend to Sire! </h3>
								<p>You and your friends receive incredible benefits!</p>
							</div>
						</div>
					</a>
		</cfif> --->

	</div>
	
	
	<!--- Need to close the main-wrapper section first so this will span the full broswer --->
	</section>
	

	<!--- <section id="sample-templates" class="clearfix">
		<div id="sthead">
			<div id="stsliptop"></div>
		</div>
		<div id="stmid">
				<a id="sttext" href="/cs-eai">
				<div id="sttmain">Need More?</div>
				<div id="stthead">Enterprise Application Integration</div>
				<div id="sttmain">Call Us for Enterprise and Wholesale Pricing</div>
				<div id="sttmain">Click Here To Learn More</div>
			</a>
		</div>
		<div id="stfoot">
			<div id="stslipbot"></div>
		</div>
	</section> --->

	<section class="pricing-block-2">
		<div class="container">
			<div class="inner-pricing-block-2">
				<div class="row">
					<div class="col-sm-12 text-center">
						<!--- <p>Not sure which plan is right for you? <a href="plans-pricing-calculator">Click here</a></p> --->
						<p>If you need more horsepower to grow your business, <a href="contact-us" class="pricing-contact-us">Contact us</a> and we’ll be happy to create a custom plan for you</p>
					</div>
				</div>
			</div>
		</div>       	
   </section>

	<section class="pricing-block-3 block-for-plan">
        <div class="container new-container1">
            <div class="inner-pricing-block-3">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="title">All plans include the following features</h4>
                    </div>
                </div>

                <div class="row block-pricing-item">
                    <div class="col-sm-6">
                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-1.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Inbound and Outbound Text Messaging</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-3.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Reporting</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-5.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Subscriber Management</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-7.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Unlimited Subscribers</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-2.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Templates</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-4.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Assigned Common Short Code</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-6.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>API Access</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-8.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>Guides and Tutorials</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row pricing-item com-flex com-flex-middle">
                            <div class="col-xs-3">
                                <div class="img">
                                    <img class="img-responsive" src="/public/sire/images/pricing/price-ic-9.png" alt="">
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="content">
                                    <h4>No Long Term Commitment</h4>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </section>


    <cfif structKeyExists(Session,'loggedIn') AND Session.loggedIn EQ 1 >
	<cfelse>
		<section class="home-block-3">
	        <div class="container">
	            <div class="inner-home-block-3">
	                <div class="row">
	                    <div class="col-sm-12">
	                        <h4>Get Your Free Account Today</h4>
	                        <p>No Credit Card Required | Free Upgrade or Downgrade | No Hidden Fees</p>
	                        <a href="/signup" class="get-started">start now</a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</cfif>


	<!-- Modal -->
	<div id="InfoModalKeyword" class="modal fade" role="dialog">
	  <div class="modal-dialog new-modal">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">What is a Keyword?</h4>
	      </div>
	      <div class="modal-body">
	      	<p class="text-center">
	      		Imagine if Joe’s Coffee Shop offers this promotion: Text “FreeCoffee” to 39492 and receive a free coffee on your next visit.  “FreeCoffee” is the Keyword.  It’s the unique word or code you create so your customers can respond to your campaign.  
	      	</p>
	      </div>
	      <!--- <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div> --->
	    </div>
	
	  </div>
	</div>


	<!-- Modal -->
	<div id="InfoModalShortCode" class="modal fade" role="dialog">
	  <div class="modal-dialog new-modal">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">What is a Short Code?</h4>
	      </div>
	      <div class="modal-body">
	      	<p><b>Short Codes</b> are special telephone numbers, significantly shorter than full telephone numbers, that can be used to address SMS messages. Short codes are designed to be easier to read and remember than normal telephone numbers.</p>
	      </div>
	      <!--- <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div> --->
	    </div>
	
	  </div>
	</div>


	<!-- Modal -->
	<div id="InfoModalSMS" class="modal fade" role="dialog">
	  <div class="modal-dialog new-modal">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">What is a Text Message?</h4>
	      </div>
	      <div class="modal-body">
	      	<p><b>SMS</b> stands for <b>Short Message Service</b> and is also commonly referred to as a <b>"text message"</b>. With <b>SMS</b>, you can send a message of up to 160 characters to another device. Longer messages will automatically be split up into several parts.</p>
	      </div>
	      <!--- <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div> --->
	    </div>
	
	  </div>
	</div>
	
	<!-- Modal -->
	<div id="InfoModalAPI" class="modal fade" role="dialog">
	  <div class="modal-dialog new-modal">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">What is an API</h4>
	      </div>
	      <div class="modal-body">
	      	<p>Application Program Interface <b>(API)</b> is a set of routines, protocols, and tools for building software applications. An <b>API</b> specifies how software components should interact. To trigger a SMS session via REST <b>API</b> you need to send information via an HTTPS request to the URL address https://api.siremobile.com/ire/secure/triggerSMS. At a minimum of you need four pieces of information to send along with this request.</p>
	      </div>
	      <!--- <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div> --->
	    </div>
	
	  </div>
	</div>

	<!-- Modal -->
	<div id="InfoModalMLP" class="modal fade" role="dialog">
	  <div class="modal-dialog new-modal">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">What is a MLP?</h4>
	      </div>
	      <div class="modal-body">
	      	<p class="text-center">
	      		Let’s say Pete’s Gym offered a promotion: Buy 5 group fitness classes and get 3 free! Click here to redeem coupon.  Once customers clicks on that link they will be sent to your own Marketing Landing Page (MLP) displaying the coupon.  It’s a standalone web page promoting your offer and the best part is it’s included for free!
	      	</p>
	      </div>
	      <!--- <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div> --->
	    </div>
	
	  </div>
	</div>
					
					