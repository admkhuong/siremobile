<cfparam name="applicationSetting" default="Off">
<cfinvoke component="session.sire.models.cfc.setting" method="getByName" returnvariable="applicationSetting">
	<cfinvokeargument name="inpName" value="liveChatOnOff">
</cfinvoke> 
<cfif structKeyExists(applicationSetting, "liveChatOnOff") and applicationSetting.liveChatOnOff eq 'On'>
	<!--Start of Tawk.to Script-->
	<cfif application.Env eq true>
		<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/57a2049cae9764090930a107/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
		</script>
	<cfelse>
		<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/57a06f400d4930e271d6f982/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
		</script>
	</cfif>
	<!--End of Tawk.to Script-->
</cfif>
