<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data) />
<cfset recordlist = ListToArray(content.Details,',')>

<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 70%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<cfoutput>
<body>
    <thead>
        <tr>
            <th><h3 style="text-align: center;">#content.NOCName#</h3></th>
        </tr>
        <tr>
            <th><h5>Enterprise User Account Balance Low Warning</h5></th>
        </tr>
    </thead>
	<table>
		<tbody>
			<tr>
                <td>ID User</td>
                <td>Name</td>
                <td>Phone Number</td>
                <td>Email</td>
                <td>Account Type</td>
				<td>Current Balance</td>
			</tr>
            <cfloop array="#recordlist#" item = "recordAccount">
                <tr>
                   <td>#replace(recordAccount,'//','</td><td>','ALL')#</td>
                </tr>
            </cfloop>
           
		</tbody>
	</table>
</body>
</html>
</cfoutput>