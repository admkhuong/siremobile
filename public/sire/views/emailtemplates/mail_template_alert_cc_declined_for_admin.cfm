<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data) />

<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<cfoutput>
<body>
	<table>
		<thead>
			<tr>
				<th><h3 style="text-align: left;">Account ID</h3></th>
				<th><h3 style="text-align: left;">Name</h3></th>
				<th><h3 style="text-align: left;">Email</h3></th>
				<th><h3 style="text-align: left;">Plan</h3></th>
				<th><h3 style="text-align: left;">Purchased Keyword(s)</h3></th>
				<th><h3 style="text-align: left;">Amount</h3></th>
			</tr>
		</thead>
		<tbody>
			
				<cfloop array="#content['DATALIST']#" index="index">
					<tr>
						<td>#index.ID#</td>
						<td>#index.NAME#</td>
						<td>#index.EMAIL#</td>
						<td>#index.PLAN#</td>
						<td>#index.KEYWORD#</td>
						<td>$#index.AMOUNT#</td>
					</tr>
				</cfloop>
		</tbody>
	</table>
</body>
</html>
</cfoutput>