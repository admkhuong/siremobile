<cfset var content	= {} />
<cfset failedRecords = deserializeJSON(arguments.data) />
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<body>
	<table cellpadding="6" cellspacing="6" style="">
		<thead>
			<tr>
				<th colspan="6"><h3 style="text-align: center;">Service connection timeout</h3></th>
			</tr>
			<tr>
				<th>MasterRXCallDetailId</th><th>DTSID</th><th>BatchId</th><th>ContactString</th><th>SMSCSC</th><th>Created</th>
			</tr>
		</thead>
		<cfoutput>
		<tbody>
			<cfloop array="#failedRecords#" index="record">
				<tr>
					<td>#record.MasterRXCallDetailId#</td>
					<td>#record.DTSID#</td>
					<td>#record.BatchId#</td>
					<td>#record.ContactString#</td>
					<td>#record.SMSCSC#</td>
					<td>#dateFormat(record.Created, 'yyyy-mm-dd')# #timeFormat(record.Created, 'HH:mm:ss')#</td>
				</tr>
			</cfloop>
		</tbody>
		</cfoutput>
	</table>
</body>
</html>