<cfset var content	= {} />
<cfset logEntry = deserializeJSON(arguments.data) />
<cfset keywordLog = '' />
<cfloop array="#logEntry.UserKeywords#" index="i">
	<cfset keywordLog = keywordLog & '#i.Keyword#, '/>
</cfloop>
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			background-color: #f1f1c1;
			width: 100%;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}

		strong.red {
			color: red;
		}
	</style>
</head>
<body>
	<table cellpadding="3" cellspacing="3" style="">
		<thead>
			<tr>
				<th colspan="3"><h3 style="text-align: center;">Admin Deactivate Account</h3></th>
			</tr>
			<tr>
				<th>Modules</th><th>Operations</th><th>Setting</th>
			</tr>
		</thead>
		<cfoutput>
		<tbody>
			<tr>
				<td>Deactivate account</td>
				<td>#logEntry.User.UserName# - #logEntry.User.EmailAddress# - #logEntry.User.UserId#</td>
				<td>Active = 0</td>
			</tr>
			<tr>
				<td>Reduce billing balance</td>
				<td>Balance: #logEntry.UserBilling.Balance# => 0<br/>BuyCreditBalance: #logEntry.UserBilling.BuyCreditBalance# => 0<br/>PromotionCreditBalance: #logEntry.UserBilling.PromotionCreditBalance# => 0</td>
				<td></td>
			</tr>
			<tr>
				<td>Deactivate user plan</td>
				<td>UserPlanId = #logEntry.UserPlan.UserPlanId#</td>
				<td>Status = 0</td>
			</tr>
			<tr>
				<td>Deactivate user campaign</td>
				<td><cfif arrayLen(logEntry.UserBatches) LT 1>User has no campaign<cfelse>#arrayToList(logEntry.UserBatches, ', ')#</cfif></td>
				<td>Active = 0</td>
			</tr>
			<tr>
				<td>Deactivate user keywords</td>
				<td><cfif arrayLen(logEntry.UserKeywords) LT 1>User has no keyword<cfelse>Keywords: #keywordLog#</cfif></td>
				<td>Active = 0</td>
			</tr>
			<tr>
				<td>Deactivate user blast queue</td>
				<td><cfif arrayLen(logEntry.UserContactQueue) LT 1>User has no blast queue<cfelse>Blast queue id: #arrayToList(logEntry.UserContactQueue)#</cfif></td>
				<td>DTSStatusType = 7 (stop)</td>
			</tr>
			<tr>
				<td>Deactivate user referral queue</td>
				<td><cfif arrayLen(logEntry.UserReferralQueue) LT 1>User has no referral queue<cfelse>Referral queue id: #arrayToList(logEntry.UserReferralQueue)#</cfif></td>
				<td>Status = 4 (stop)</td>
			</tr>
			<tr>
				<td>Deactivate user MLP</td>
				<td><cfif arrayLen(logEntry.UserMLP) LT 1>User has no MLP<cfelse>User MLP key: #arrayToList(logEntry.UserMLP)#</cfif></td>
				<td>Status = 0</td>
			</tr>
			<tr>
				<td>Deactivate user ShortURL</td>
				<td><cfif arrayLen(logEntry.UserShortUrl) LT 1>User has no shorturl<cfelse>User shorturl key: #arrayToList(logEntry.UserShortUrl)#</cfif></td>
				<td>Active = 0</td>
			</tr>
			<tr>
				<td colspan="3" style="text-align: center">Operated by <strong class="red">#Session.USERNAME#</strong> at <strong>#dateformat(now(), "yyyy-mm-dd")# #TimeFormat(now(), "HH:mm:ss")#</strong></td>
			</tr>
		</tbody>
		</cfoutput>
	</table>
</body>
</html>