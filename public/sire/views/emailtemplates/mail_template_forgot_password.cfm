<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>

<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Forgot Your Password?</title>
	</head>

	<body style="font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;font-family: Arial;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  					<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/forgot-pass.jpg'); height: 250px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/forgot-pass.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">
		  				<h1 style="text-align: center; font-size: 36px; color: #73c37e;">Forgot Your Password?</h1>


						<p style="font-size: 16px; color: #5c5c5c;">Hi <cfoutput>#content.FIRSTNAME#</cfoutput>!</p>

						<p style="font-size: 16px; color: #5c5c5c;">This email is in response to a request to change your password.</p>

						<p style="font-size: 16px; color: #5c5c5c;">Please confirm this is a valid request by clicking this link (or copy the entire link and then paste into a browser navigation bar) : </p>



						<table width="100%" style="font-family: Arial;">
                      		<tr>
                                <td width="30%"></td>
								<td width="40%" align="center" style="padding: 10px 20px; background: #74c37f;"><a href="<cfoutput>#content.ROOTURL#/change-password?inpCode=#content.VERIFYCODE#</cfoutput>" style="display: inline-table; text-decoration: none;font-weight: bold; color: white; font-size: 14px;">RESET MY PASSWORD</a></td>
                                <td width="30%"></td>
                      		</tr>
                      	</table>

						<p style="font-size: 16px; color: #5c5c5c;">
							Thanks,<br>
							SIRE Assistance Team
						</p>			
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>