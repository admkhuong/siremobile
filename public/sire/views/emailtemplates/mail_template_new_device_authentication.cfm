<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>

<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>New Device Authentication</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">
						<p style="font-size: 16px; color: #5c5c5c;">Hello <cfoutput>#content.USERNAME#</cfoutput>!</p>

						<p style="font-size: 16px; color: #5c5c5c;">You are receiving this email because you recently signed a new account up or signed in on a new device/browser.</p>

						<p style="font-size: 16px; color: #5c5c5c;">Your SIRE verification code is:<br>
						<span style="color: #578da6; font-weight: bold; font-size: 24px;"><cfoutput>#content.CurrMFACode#</cfoutput></span>
						</p>

						<p style="font-size: 16px; color: #5c5c5c;">If you have any problems, please reply to this email to get assistance.</p>

						<p style="font-size: 16px; color: #5c5c5c;">
							Thanks,<br>
							SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>