<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>

<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Customer Transaction Limit Alert</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">
						<p style="font-size: 16px; color: #5c5c5c;">Hello SIRE Support Team!</p>

						<p style="font-size: 16px; color: #5c5c5c;">The below account has exceeded the daily transaction limit, please review and notify the customer as soon as you can.</p>

						<p style="font-size: 16px; color: #5c5c5c;">UserID: <cfoutput>#content.USERID#</cfoutput><br>UserName: <cfoutput>#content.USERNAME#</cfoutput><br>PhoneNumber: <cfoutput>#content.PHONENUMBER#</cfoutput><br>Profile Name: <cfoutput>#content.COMPANYNAME#</cfoutput><br>Amount: <cfoutput>#content.AMOUNT#</cfoutput></p>

						<p style="font-size: 16px; color: #5c5c5c;">
							Thanks,<br>
							SIRE Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>