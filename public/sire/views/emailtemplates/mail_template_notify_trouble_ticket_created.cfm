<cfset var content	= '' />
<cfset content = deserializeJSON(arguments.data)/>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#"/>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Notify Trouble Ticket Created</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
        <table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
             <cfinclude template="header.cfm">
            <tbody style="margin: 0; padding: 0;">
                <tr>
                    <td colspan="2" style="margin: 0; padding: 0;">
                        <div style="text-align: center;">
                            <img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0 30px;">
                        <p style="font-size: 16px;">Hi <cfoutput>#content.USERNAME#</cfoutput>!</p>
                        <cfif content.TROUBLETICKETID NEQ 0>
                            <p style="font-size: 16px;">A new Trouble ticket have created for you:</p>
                            <p style="font-size: 16px;">The ticket ID: <b><cfoutput>#content.TROUBLETICKETID#</cfoutput>.</b></p>
                        <cfelse>
                            <p style="font-size: 16px;">A customer need your help!</p>
                            <p style="font-size: 16px;">Please, create Trouble ticket and contact to customer as soon as possible.</p>
                        </cfif>
                        <p style="font-size: 14px;">Trouble content: <cfoutput>#content.MESSAGETEXT#</cfoutput>.</p>
                        <p style="font-size: 14px;">Phone number: <cfoutput>#content.CONTACTSTRING#</cfoutput>.</p>
                        <p style="font-size: 16px;">
                            Thanks,<br>
                            SIRE Support
                        </p>
                    </td>
                </tr>	
            </tbody>
             <cfinclude template="footer.cfm">
        </table>
    </body>

</html>