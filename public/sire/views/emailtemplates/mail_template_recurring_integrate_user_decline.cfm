<cfset var content	= {} />
<cfset content = DeserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!--- SET DOMAIN FOR CRON --->
<cfif LCase(CGI.SERVER_NAME) EQ 'cron.siremobile.com'>
	<cfset content.ROOTURL_HTTP = 'https://siremobile.com'>	
</cfif>
<cfif content.MailToMaster EQ 1>
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">	
		<cfinvokeargument name="inpUserId" value="#content.HIGHERUSERID#"> 
	</cfinvoke>
<cfelse>
	<cfinvoke component="public.sire.models.cfc.userstools" method="GetUserInfoById" returnvariable="userInfo">	
		<cfinvokeargument name="inpUserId" value="#content.USERID#"> 
	</cfinvoke>
</cfif>

<cfset userName = userInfo.USERNAME>

<cfinvoke component="session.sire.models.cfc.billing"  method="GetBalance"  returnvariable="RetValBillingData">
	<cfinvokeargument name="inpUserId" value="#content.USERID#">
</cfinvoke>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recurring Completed</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td colspan="2" style="margin: 0; padding: 0;">
		  				<!---
		  				<div style="background: url('<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/authen-img.jpg'); height: 318px;"></div>
		  				--->
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/monthly-billing-email.jpg" />
		  				</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="padding: 0 30px;">

						<p style="font-size: 16px;color: #5c5c5c;">Dear <cfoutput>#userName#</cfoutput>,</p>
						
						<br>

						<p style="font-size: 16px;text-align:left;">We're sorry, but your credit card on file was declined.  Can you please log onto <a href="https://www.siremobile.com">www.siremobile.com<a/> and update your payment information to avoid any interruption to your account and service.</p>
						<hr style="border: 0;border-bottom: 1px solid #74c37f;">

						<p style="font-size: 18px;color: #74c37f; text-align:center; font-weight:bold">
							<cfif content.MailToMaster EQ 0>
								Information of your account
							<cfelse>
								Information of your sub account
							</cfif>													
						</p>
						<hr style="border: 0;border-bottom: 1px solid #74c37f;">

						<table style="width:100%">
							<tr>
								<td style="width:10%"></td>
								<td style="width:30%">
									<p style="font-size: 16px;color: #5c5c5c;">
										
										<cfif content.MailToMaster EQ 0>
											Your account ID:
										<cfelse>
											Your sub account ID:
										</cfif>	
									</p> 
									<p style="font-size: 16px;color: #5c5c5c;">Plan:</p> 
									<p style="font-size: 16px;color: #5c5c5c;">Start Date: </p>
									<p style="font-size: 16px;color: #5c5c5c;">End Date:</p>
									<!---
									<p style="font-size: 16px;color: #5c5c5c;">Buy Keywords:</p>
									<p style="font-size: 16px;color: #5c5c5c;">Credit Available:</p>
									--->
								</td>
								<td style="width:50%">
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.USERID#</cfoutput> </p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.PLANSNAME#</cfoutput> </p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#DateFormat(content.PLANSTARTDATE,'mm/dd/yyyy')#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#DateFormat(content.PLANENDDATE,'mm/dd/yyyy')#</cfoutput></p>
									<!---
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#content.BUYKEYWORDS#</cfoutput></p>
									<p style="font-size: 16px;color: #5c5c5c;"><cfoutput>#NumberFormat(RetValBillingData.BUYCREDITBALANCE,',')#</cfoutput></p>
									--->

								</td>
								<td style="width:10%"></td>
							</tr>
							
						</table>
						<p style="font-size: 16px; color: #5c5c5c;">Please contact us to <a href="mailto:support@siremobile.com">support@siremobile.com</a> if you have any question.</p>
						
						<p style="font-size: 16px;color: #5c5c5c;">Thanks,<br>
						SIRE Assistance Team
						</p>
		  			</td>
		  		</tr>	
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>