<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head>

	<body style="font-family: Arial; font-size: 12px;">
		<table style="width: 600px; margin: 0 auto;" cellpadding="0" cellspacing="0">
			<cfinclude template="header.cfm">
		  	<tbody style="margin: 0; padding: 0;">
		  		<tr>
		  			<td width="50%" style="margin: 0; padding: 0;">
		  				<div style="text-align: center;">
		  					<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/welcome.jpg" alt="">
		  				</div>
		  				<br>
		  				<p style="font-size: 16px;">Dear <cfoutput>#content.USERNAME#</cfoutput>,</p>
						<p style="font-size: 16px;">Thank you for registering with Sire! Did you know that you can receive 1,000 credits by inviting your friend to Sire? Visit our <a href="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/plans-pricing">website</a> for more information.</p>
		  			</td>
		  			<td style="text-align: right; margin: 0; padding: 0;">
		  				<img src="<cfoutput>#content.ROOTURL_HTTP#</cfoutput>/public/sire/images/emailtemplate/welcome-img.jpg" alt="">
		  			</td>
		  		</tr>
		  	</tbody>
		  	<cfinclude template="footer.cfm">
		</table>
	</body>

</html>