<cfset var content	= {} />
<cfset content = deserializeJSON(arguments.data)>
<cfset content.ROOTURL_HTTP = "https://#CGI.SERVER_NAME#">

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>mail_template_registration_affiliate</title>
	</head>

	<body style="font-family: Arial; font-size: 16px">
		<table style="width: 570px; margin: 0 auto; color:#5c5c5c;" cellpadding="0" cellspacing="0" align="center">

				<tr>
					<td>
						<table width="100%">
							<tr>
								<td width="100%" style="margin: 0; padding-left: 2%; font-size: 16px">
									<br><br>
									
									Hi <cfoutput>#content.USERNAME#</cfoutput>,<br>

									<p>Thanks for joining our growing network of affiliates who are helping businesses connect with more customers.  We're glad you joined the Sire family.   </p>
									

									<p>
										We're here to help you succeed.  If you need a quick demo of our templates or have any questions about our service, give us a holler at hello@siremobile.com.  We'll also reach out to keep you updated on any new tools or features we add to our platform.  These new features usually stem from customer feedback so don't be shy about sending us the scoop on what your clients are saying.																				
									</p>

									<p>
										
										Welcome aboard and happy selling!
									</p>
									<p>
										<br><br>
										Sincerely,<br><br>

										Bruce Watanabe<br>
										CEO
									</p>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
	</body>

</html>

