<cfif !isDefined('variables._Response')>
	<cfset variables._PageContext = GetPageContext()>
	<!--- Coldfusion --->
	<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
	<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>#variables._title#</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="#replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;')#">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0,user-scalable=no">

	<link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900,700,600,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
	<link href="/public/sire/css/bootstrap.css" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-theme.css" rel="stylesheet">
	<link href="/public/sire/css/validationEngine.jquery.css" rel="stylesheet">
	<link href="/public/sire/css/style.css" rel="stylesheet">
	<link href="/public/sire/css/screen.css" rel="stylesheet">

	<cfif (isDefined("Request._css") && isArray(Request._css))>
		<cfloop array="#Request._css#" index="cssPath">
    		<link href="#cssPath#" rel="stylesheet">
		</cfloop>
		<cfset Request._css=[]>
	</cfif>
	
</head>
<body class="main-bg">
	<!-- HEADER SECTION
	=============================================================-->
	<cfinclude template="../commons/header.cfm">

	<!-- BODY SECTION
	=============================================================-->
	<section class="main">
		<div class="container-fluid">
			#variables._page#
		</div>
	</section>
	<!-- FOOTER SECTION
	=============================================================-->
	<cfinclude template="../commons/footer.cfm">
	<script src="/public/sire/js/jquery-2.1.4.js"></script>
	<script src="/public/sire/js/bootstrap.js"></script>
	<!---
	<script src="/public/sire/js/retina.js"></script>
	--->

	<script src="/public/sire/js/jquery.validationEngine.en.js"></script>
	<script src="/public/sire/js/jquery.validationEngine.js"></script>
	<script src="/public/sire/js/bootbox.min.js"></script>
	<!---<script src="/public/sire/js/angular.js"></script>
	<script src="/public/sire/js/ui-bootstrap-0.14.3.js"></script>--->

	<cfif (isDefined("Request._js") && isArray(Request._js))>
		<cfloop array="#Request._js#" index="jsPath">
			<script src="#jsPath#"></script>
		</cfloop>
		<cfset Request._js=[]>
	</cfif>
	<!--- Integrate Live chat --->
	<cfinclude template="../commons/tawkto_script.cfm">
</body>
</html>
</cfoutput>