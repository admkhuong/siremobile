<cfif !isDefined('variables._Response')>
	<cfset variables._PageContext = GetPageContext()>
	<!--- Coldfusion --->
	<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
	<cfset variables._Response = variables._PageContext.getOut()>
</cfif>

<cfinclude template="/public/sire/configs/version.cfm" />

<cfset Build_Version_numb = "?_=#Config_BuildVersionNumber#">

<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="variables.body_class" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfparam name="Request.meta_data" default=[]>
<cfparam name="hideFixedSignupForm" default="">
<cfparam name="isLeadsconPage" default="">


<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>#variables._title#</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="#replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;')#">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=0">
	<cfif (isDefined("Request.meta_data") AND isArray(Request.meta_data))>
		<cfloop array="#Request.meta_data#" index="types">
			<cfloop array="#types#" index="tag" >
				<meta property="#tag.property#" content="#tag.content#">
			</cfloop>
		</cfloop>
	</cfif>

	<link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-home.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-theme.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/css/validationEngine.jquery.min.css#Build_Version_numb#" rel="stylesheet">
     <link href="/public/sire/css/scroll-down.min.css#Build_Version_numb#" rel="stylesheet">
     <link href="/public/sire/css/style.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/css/common.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/css/font-awesome.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-submenu.min.css#Build_Version_numb#" rel="stylesheet">
	<link href="/public/sire/assets/plugins/uikit/uikit3.min.css#Build_Version_numb#" rel="stylesheet">
	<cfif (isDefined("Request._css") && isArray(Request._css))>
		<cfloop array="#Request._css#" index="cssPath">
    		<link href="#cssPath#" rel="stylesheet">
		</cfloop>
		<cfset Request._css=[]>
	</cfif>
    <!--- http://realfavicongenerator.net/favicon?file_id=p1a4qpcvhn1jltrkukhpvas1f206#.VlNZzHunXvA  --->
    <link rel="apple-touch-icon" sizes="57x57" href="/public/sire/images/sireicons/apple-touch-icon-57x57.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="60x60" href="/public/sire/images/sireicons/apple-touch-icon-60x60.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/sire/images/sireicons/apple-touch-icon-72x72.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/sire/images/sireicons/apple-touch-icon-76x76.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/sire/images/sireicons/apple-touch-icon-114x114.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/sire/images/sireicons/apple-touch-icon-120x120.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/sire/images/sireicons/apple-touch-icon-144x144.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/sire/images/sireicons/apple-touch-icon-152x152.png#Build_Version_numb#">
    <link rel="apple-touch-icon" sizes="180x180" href="/public/sire/images/sireicons/apple-touch-icon-180x180.png#Build_Version_numb#">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-32x32.png#Build_Version_numb#" sizes="32x32">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/android-chrome-192x192.png#Build_Version_numb#" sizes="192x192">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-96x96.png#Build_Version_numb#" sizes="96x96">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-16x16.png#Build_Version_numb#" sizes="16x16">
    <link rel="manifest" href="/public/sire/images/sireicons/manifest.json#Build_Version_numb#">
    <link rel="mask-icon" href="/public/sire/images/sireicons/safari-pinned-tab.svg#Build_Version_numb#" color="##5bbad5">
    <meta name="msapplication-TileColor" content="##da532c">
    <meta name="msapplication-TileImage" content="/public/sire/images/sireicons/mstile-144x144.png">
    <meta name="theme-color" content="##ffffff">


    <cfinclude template="/public/sire/views/commons/google_task_script_head.cfm">

</head>
<body class="#variables.body_class#">
	<cfinclude template="/public/sire/views/commons/google_task_script_body.cfm">

	<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 AND #hideFixedSignupForm# EQ "">
		<cfinclude template="../commons/fixed-signup.cfm">
	</cfif>

	#variables._page#
	<cfif isLeadsconPage NEQ 1>
		<cfinclude template="../commons/footer_front.cfm">
	</cfif>

	<!--- <script src="/public/sire/js/jquery-2.1.4.min.js#Build_Version_numb#"></script> --->
	<script src="/public/sire/js/jquery-3.3.1.min.js#Build_Version_numb#"></script>
	<!---
	<script src="/public/sire/js/retina.js"></script>
	--->
	<script src="/public/sire/js/bootstrap.min.js#Build_Version_numb#"></script>

	<script src="/public/sire/js/jquery.validationEngine.en.min.js#Build_Version_numb#"></script>
	<!--- <script src="/public/sire/js/jquery.validationEngine.min.js#Build_Version_numb#"></script> --->
	<script src="/public/sire/js/jquery.validationEngine.js#Build_Version_numb#"></script>
	<script src="/public/sire/js/bootbox.min.js#Build_Version_numb#"></script>
	<script src="/public/sire/assets/plugins/mobile-detect/mobile-detect.js#Build_Version_numb#"></script>
	<script src="/public/sire/js/common.js#Build_Version_numb#"></script>
	<script src="/public/sire/js/bootstrap-hover-dropdown.min.js#Build_Version_numb#"></script>
	<script src="/public/sire/js/bootstrap-submenu.min.js#Build_Version_numb#"></script>
	<script src="/public/sire/assets/plugins/uikit/uikit.min.js#Build_Version_numb#"></script>

	<!---<script src="/public/sire/js/angular.js"></script>
	<script src="/public/sire/js/ui-bootstrap-0.14.3.js"></script>--->

	<cfif (isDefined("Request._js") && isArray(Request._js))>
		<cfloop array="#Request._js#" index="jsPath">
			<script src="#jsPath##Build_Version_numb#"></script>
		</cfloop>
		<cfset Request._js=[]>
	</cfif>
	<script >
		  $('[data-submenu]').submenupicker();
	</script>
	<!--- Integrate Live chat --->
	<cfinclude template="../commons/livezilla_script.cfm">

	<script>
	    <cfoutput>var userid="#SESSION.USERID#";</cfoutput>
	    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//ssl.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-82724101-1', 'auto',{
						"userId": userid
				});
		ga('set', 'dimension1', userid);
		ga(function(tracker) {
        tracker.set('dimension2', tracker.get('clientId'));
        });
         ga('send', 'pageview');

	</script>
</body>
</html>
</cfoutput>
