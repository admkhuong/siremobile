<cfif !isDefined('variables._Response')>
	<cfset variables._PageContext = GetPageContext()>
	<!--- Coldfusion --->
	<!---<cfset variables._Response = variables._PageContext.getCFOutput()>--->
	<cfset variables._Response = variables._PageContext.getOut()>
</cfif>
<cfset variables._page = variables._Response.getString()>
<cfset variables._Response.clear()>
<cfparam name="variables._page" default="">
<cfparam name="variables._title" default="">
<cfparam name="variables.meta_description" default="">
<cfparam name="variables.body_class" default="">
<cfparam name="Request._css" default="[]">
<cfparam name="Request._js" default="[]">
<cfparam name="variables._pageClass" default="">
<cfparam name="hideFixedSignupForm" default="">

<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>#variables._title#</title>
	<meta name="robots" content="index, follow">
	<meta name="description" content="#replaceList(variables.meta_description, '<,>,"', '&lt;,&gt;,&quot;')#">
	<meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=10.0,initial-scale=1.0,user-scalable=no">
	<cfif (isDefined("Request.meta_data") AND isArray(Request.meta_data))>
		<cfloop array="#Request.meta_data#" index="types">
			<cfloop array="#types#" index="tag" >
				<meta property="#tag.property#" content="#tag.content#">
			</cfloop>
		</cfloop>
	</cfif>
	<link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900,700,600,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
	<link href="/public/sire/css/bootstrap-home.min.css" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="/public/sire/css/validationEngine.jquery.min.css" rel="stylesheet">
	<link href="/public/sire/css/style.min.css" rel="stylesheet">
    <link href="/public/sire/css/common.min.css" rel="stylesheet">
    <link href="/public/sire/css/scroll-down.min.css" rel="stylesheet">
    <link href="/public/sire/css/marketing-overlay.min.css" rel="stylesheet">
	<link href="/public/sire/css/font-awesome.min.css" rel="stylesheet">
	<link href="/public/sire/css/bootstrap-submenu.min.css" rel="stylesheet">
	<link href="/public/sire/assets/plugins/uikit/uikit3.min.css" rel="stylesheet">
	<cfif (isDefined("Request._css") && isArray(Request._css))>
		<cfloop array="#Request._css#" index="cssPath">
    		<link href="#cssPath#" rel="stylesheet">
		</cfloop>
		<cfset Request._css=[]>
	</cfif>

    <link href="/public/sire/css/learning-and-support.min.css" rel="stylesheet">



    <!--- http://realfavicongenerator.net/favicon?file_id=p1a4qpcvhn1jltrkukhpvas1f206#.VlNZzHunXvA  --->
    <link rel="apple-touch-icon" sizes="57x57" href="/public/sire/images/sireicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/public/sire/images/sireicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/public/sire/images/sireicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/public/sire/images/sireicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/public/sire/images/sireicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/public/sire/images/sireicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/public/sire/images/sireicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/public/sire/images/sireicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/public/sire/images/sireicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/public/sire/images/sireicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/public/sire/images/sireicons/manifest.json">
    <link rel="mask-icon" href="/public/sire/images/sireicons/safari-pinned-tab.svg" color="##5bbad5">
    <meta name="msapplication-TileColor" content="##da532c">
    <meta name="msapplication-TileImage" content="/public/sire/images/sireicons/mstile-144x144.png">
    <meta name="theme-color" content="##ffffff">

    <cfinclude template="/public/sire/views/commons/google_task_script_head.cfm">
</head>
<body class="home_page">
	<cfinclude template="/public/sire/views/commons/google_task_script_body.cfm">

	<cfif !structKeyExists(Session,'loggedIn') OR Session.loggedIn NEQ 1 AND #hideFixedSignupForm# EQ "">
		<cfinclude template="../commons/fixed-signup.cfm">
	</cfif>
	#variables._page#
	<cfinclude template="../commons/footer_front.cfm">

	<!--- <script src="/public/sire/js/jquery-2.1.4.min.js"></script> --->
	<script src="/public/sire/js/jquery-3.3.1.min.js"></script>
	<script src="/public/sire/js/bootstrap.min.js"></script>
	<!---
	<script src="/public/sire/js/retina.min.js"></script>
	--->

	<script src="/public/sire/js/jquery.validationEngine.en.min.js"></script>
	<!--- <script src="/public/sire/js/jquery.validationEngine.min.js"></script> --->
	<script src="/public/sire/js/jquery.validationEngine.js"></script> 

	<script src="/public/sire/js/bootbox.min.js"></script>
	<script src="/public/sire/js/common.js"></script>
	<script src="/public/sire/js/bootstrap-submenu.min.js"></script>
	<script src="/public/sire/assets/plugins/uikit/uikit.min.js"></script>
	<!---<script src="/public/sire/js/angular.js"></script>
	<script src="/public/sire/js/ui-bootstrap-0.14.3.js"></script>--->

	<cfif (isDefined("Request._js") && isArray(Request._js))>
		<cfloop array="#Request._js#" index="jsPath">
			<script src="#jsPath#"></script>
		</cfloop>
		<cfset Request._js=[]>
	</cfif>
	<script >
		  $('[data-submenu]').submenupicker();
	</script>
	<cfinclude template="../commons/livezilla_script.cfm">
</body>
</html>
</cfoutput>