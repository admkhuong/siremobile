<cfset shortcode = ''/>
<cfif session.UserId GT 0>
    
    <!--- remove old method: 
	<cfinvoke component="public.sire.models.cfc.userstools" method="getUserShortCode" returnvariable="RETVARSHORTCODE"/>			
	--->
	<cfinvoke method="GetAssignedShortCode" component="session.sire.models.cfc.control-point" returnvariable="RETVARSHORTCODE"></cfinvoke>
    <cfif RETVARSHORTCODE.RXRESULTCODE GT 0>
        <cfset shortcode = RETVARSHORTCODE.SHORTCODE/>
    </cfif>
<cfelse>
    <cfinvoke method="GetSystemDefaultShortCode" component="public.sire.models.cfc.shortcode" returnvariable="RETVARSHORTCODE"/>
    <cfif RETVARSHORTCODE.RXRESULTCODE GT 0>
        <cfset shortcode = RETVARSHORTCODE.SHORTCODE/>
    </cfif>
</cfif>