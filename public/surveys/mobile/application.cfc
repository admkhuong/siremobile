

<cfcomponent output="true" hint="Handle the application." >
	<cfset THIS.SessionManagement = true />
	<cfinclude template="../../paths.cfm" />
	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">
	 <!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			TYPE="string"
			required="true"
			/>
	        <cfif UCASE(ListLast(GetTemplatePath(), "/")) DOES NOT CONTAIN "CFDIV" AND UCASE(RIGHT(cgi.script_name, 3)) NEQ "CFC" AND UCASE(ListLast(GetTemplatePath(), "/")) DOES NOT CONTAIN ".CFC" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "FILETREECONNECTOR.CFM" AND UCASE(ListLast(GetTemplatePath(), "\")) DOES NOT CONTAIN "ACT_" AND UCASE(GetTemplatePath()) DOES NOT CONTAIN "DSP_" AND  UCASE(GetTemplatePath()) DOES NOT CONTAIN "rxds">
	            <!--- Start regulaer HTML here --->
	            <cfinclude template="display/dsp_header.cfm" />
	            
				<!--- Wrap the body --->
			  	<cfinclude template="display/dsp_body.cfm" /> 
	                      
	            <!--- Include the requested page. --->
	            <cfinclude template="#ARGUMENTS.TargetPage#" />
			            
				<!--- Do footer stuff here --->
				<cfinclude template="display/dsp_footer.cfm" />
	       <cfelseif  UCASE(GetTemplatePath()) CONTAINS "DSP_"> 
	        	<!--- Just Include the requested page. --->
	           	<cfinclude template="#ARGUMENTS.TargetPage#" />
	        <cfelse>        
				<!--- Just Include the requested page. --->
	            <cfinclude template="#ARGUMENTS.TargetPage#" />  
	        </cfif>
	    <!--- Return out. --->
		<cfreturn />
	</cffunction>
	 
	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true"	hint="Fires after the page processing is complete.">
		
	</cffunction>
	
	<cffunction	name="OnSessionEnd"	access="public"	returntype="void" output="false" hint="Fires when the session is terminated.">
		<!--- Define arguments. --->
		<cfargument
			name="SessionScope"
			type="struct"
			required="true"
			 />
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"
			 />
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	 
	<cffunction name="OnApplicationEnd"	access="public"	returntype="void" output="false" hint="Fires when the application is terminated.">
	<!--- Define arguments. --->
	<cfargument
		name="ApplicationScope"
		TYPE="struct"
		required="false"
		default="#StructNew()#"
		/>
	 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
</cfcomponent> 