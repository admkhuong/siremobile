<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<cfparam name="BATCHID" default="-1">
<cfinclude template="../../../paths.cfm" >
<!----
<cfinvoke 
     component="#Session.SessionCFCPath#.SurveyTools"
     method="getSurveyTemplate"
     returnvariable="SurveyTemplate">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
		<cfinvokeargument name="public" value="1"/>
</cfinvoke>

<cfset Session.TemplateContent = SurveyTemplate.TEMPLATE />
--->
<cfinvoke 
     component="#Session.SessionCFCPath#.marketing.marketingSurveyTools"
     method="getConfigPage"
     returnvariable="configPage">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
</cfinvoke>
<!---<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=640px; initial-scale=.5; maximum-scale=.5;" />--->
<cfsavecontent variable="surveyContent">
<cfoutput>
	<style type="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey/finish.css');
	</style>
	<!---<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>--->
	<!---<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>--->
</cfoutput>
<div class="surveyTitle">
	<div class="surveyBorder">Thank you for taking 
		<cfif StructKeyExists(configPage, "SURVEYTITLE")>
			<cfoutput>#configPage.SURVEYTITLE#</cfoutput>
		</cfif>
	</div>
	<div class="surveyContent">
		<cfif configPage.THANKPAGE NEQ ""> 
			<cfoutput>#configPage.THANKPAGE#</cfoutput>
		<!--- <cfelse>
		<h3>Thank you for taking the time to complete survey <cfoutput>#configPage.SURVEYTITLE#</cfoutput>!</h3>
		Your input will help us serve you better during your future visits to messageBroadcast.com. 
		See you soon. . . --->
		<cfelse>
			Thank you for taking the time to complete this survey!
		</cfif>
	</div>
</div>
</cfsavecontent>
<cfsavecontent variable="surveyHeader">
	<cfoutput>
	<div style="margin: 0 auto;" class="finish_header">
	<cfif configPage.LOGO NEQ ''>
		<img src="#rootUrl#/#sessionPath#/marketing/survey/act_previewImage.cfm?serverfileName=#configPage.LOGO#&userId=#configPage.USERID#" height='65'/>
	<cfelse>
		<img src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm.png" height="54px">
	</cfif>
	<cfif StructKeyExists(configPage, "SURVEYTITLE")>
		<span id='survey_title'>#configPage.SURVEYTITLE#</span>
	</cfif>
	</div>
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="surveyFooter">
	<div class="surveyBorder">
		<cfif configPage.FOOTERTEXT NEQ ''>
			<cfoutput>#configPage.FOOTERTEXT#</cfoutput>
		</cfif>
	</div>
</cfsavecontent>

<cfset displaySurvey = 'block'>

<style>
	#surveyContent{
		display:<cfoutput>#displaySurvey#</cfoutput>;
	}
</style>

<cfoutput>
<html>
	<head>
		<!--- fixbug JSON.stringify in ie8 --->
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				#configPage.SURVEYTITLE#
			</cfif>
		</title>
	</head>
	<body>
		<div id="surveyContent">
			<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
			<table width="100%" style="margin:auto;">
					<tr>
						<td>#surveyHeader#</td>
					</tr>
					<tr>
						<td>#surveyContent#</td>
					</tr>
					<tr>
						<td>#surveyFooter#</td>
					</tr>
				</table>
		</div>
	</body>
</html>
</cfoutput>