<cfparam name="BATCHID" default="-1">
<cfparam name="UUID" default="-1">
<cfparam name="PREVIEW" default="0">

<cfinclude template="../../../paths.cfm" >

<cfoutput>
	
</cfoutput>
<!--- check config enable file start ---->
<cfinvoke 
     component="#Session.SessionCFCPath#.marketing.marketingSurveyTools"
     method="getConfigPage"
     returnvariable="configPage">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
</cfinvoke>

<cfif configPage.TYPE GT 0>
	<cfif (NOT StructKeyExists(configPage, "WELCOMEPAGE")) OR configPage.WELCOMEPAGE EQ ''>
	<script>
	$(function () {
		<!---
		<cfif PREVIEW eq 1>
			window.location = "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/surveys/previewsurvey.cfm?BATCHID=<cfoutput>#BATCHID#</cfoutput>&UUID=<cfoutput>#UUID#</cfoutput>"
		<cfelse>
			window.location = "<cfoutput>#rootUrl#/#publicPath#/surveys/survey.cfm?BATCHID=#BATCHID#&UUID=#UUID#</cfoutput>"
		</cfif>
		--->
	});
	</script>
	</cfif>
</cfif>
<!---
<cfinvoke 
     component="#Session.SessionCFCPath#.SurveyTools"
     method="getSurveyTemplate"
     returnvariable="SurveyTemplate">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
		<cfinvokeargument name="public" value="1"/>
</cfinvoke>
<cfset Session.TemplateContent = SurveyTemplate.TEMPLATE />
--->
<script>
var UUID = '<cfoutput>#UUID#</cfoutput>';
var BATCHID = <cfoutput>#BATCHID#</cfoutput>;
$(function () {
	<cfif StructKeyExists(configPage, "SURVEYTITLE")>
		document.title = '<cfoutput>#configPage.SURVEYTITLE#</cfoutput>'		
	</cfif>

	$("#startsurvey").click(function() {
		$.ajax({
			type: "POST",
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/marketing/marketingSurveyTools.cfc?method=SaveAnswerSurvey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				BATCHID : BATCHID,
				UUID: UUID,
				inpXML: 'NA'			
				},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									<cfif PREVIEW eq 1>
										window.location = "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/surveys/mobile/previewsurvey/?BATCHID=<cfoutput>#BATCHID#</cfoutput>&UUID=<cfoutput>#UUID#</cfoutput>"
									<cfelse>
										window.location = "<cfoutput>#rootUrl#/#publicPath#/surveys/mobile/survey/?BATCHID=#BATCHID#&UUID=#UUID#</cfoutput>"
									</cfif>
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Survey has NOT been save.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}			
				} 		
		});
		
	});
	
});
</script>
<!--- CSS Section--->

<cfsavecontent variable="surveyContent">
	<div class="surveyTitle">
		<div class="surveyBorder">Welcome to
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				<cfoutput>#configPage.SURVEYTITLE#</cfoutput>
			</cfif>
		</div>
		<div class="surveyContent">
		<cfif StructKeyExists(configPage, "WELCOMEPAGE") AND configPage.WELCOMEPAGE NEQ ""> 
			<cfoutput>#configPage.WELCOMEPAGE#</cfoutput>
		<cfelse>
			Welcome! Please press start survey to begin!
		</cfif>
		</div>
		<div class="surveyStartButon" align="center">
			<button id="startsurvey" >Start survey</button>
		</div>
		
	</div>
</cfsavecontent>
<cfsavecontent variable="surveyHeader">
	<cfoutput>
	<div style="width:100%; margin: 0 auto;">
	<cfif StructKeyExists(configPage, "LOGO") AND configPage.LOGO NEQ ''>
		<img src="#rootUrl#/#sessionPath#/marketing/survey/act_previewImage.cfm?serverfileName=#configPage.LOGO#&userId=#configPage.USERID#" height='65'/>
	<cfelse>
		<img src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm.png" height="54px">
	</cfif>
	<cfif StructKeyExists(configPage, "SURVEYTITLE")>
		<span id='survey_title'>#configPage.SURVEYTITLE#</span>
	</cfif>
	</div>
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="surveyFooter">
	<div id="surveyFooter" class="surveyBorder">
	<cfif StructKeyExists(configPage, "FOOTERTEXT") AND configPage.FOOTERTEXT NEQ ''>
		<cfoutput>#configPage.FOOTERTEXT#</cfoutput>
	</cfif>
	</div>
</cfsavecontent>

<cfset displaySurvey = 'block'>
<cfoutput>
	<div id="surveyContent">		
		<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
		<table width="100%" style="margin:auto;">
			<tr>
				<td>#surveyHeader#</td>
			</tr>
			<tr>
				<td>#surveyContent#</td>
			</tr>
			<tr>
				<td>#surveyFooter#</td>
			</tr>
		</table>
	</div>

</cfoutput>