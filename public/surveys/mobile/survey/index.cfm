<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<cfparam name="UUID" default="-1">
<cfparam name="BATCHID" default="-1">

<cfinclude template="../../../paths.cfm" >

<cfinvoke 
     component="#Session.SessionCFCPath#.marketing.marketingSurveyTools"
     method="getConfigPage"
     returnvariable="configPage">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
</cfinvoke>

<cfinclude template="jsSurvey.cfm" >

<!---
<cfinvoke 
     component="#Session.SessionCFCPath#.SurveyTools"
     method="getSurveyTemplate"
     returnvariable="SurveyTemplate">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
		<cfinvokeargument name="public" value="1"/>
</cfinvoke>

<cfset TemplateContent =""/>
--->

<style>
	.displayNone{
		display:none;
	}
	.displayBlock{
		display:block;
	}
	label.error {
    color: red;
    float: left;
    font-size: 100%;
    margin-left: 0.5em;
    text-align: left;
}
</style>
<cfsavecontent variable="cssContent">
	<cfoutput>
	<!--- <style type="text/css">
		<!--- @import url('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/base/jquery-ui.css'); --->
		@import url('#rootUrl#/#PublicPath#/css/jquery-ui.css');
		@import url('#rootUrl#/#PublicPath#/css/jWizard.base.css');
		<!--- @import url('#rootUrl#/#PublicPath#/css/rxform2.css'); --->
		@import url('#rootUrl#/#PublicPath#/css/MB.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
		@import url('#rootUrl#/#PublicPath#/js/validate/not_custom/validationEngine.jquery.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
	</style> --->
	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="jsContent">
	<cfoutput>
    <!--- <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
    
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/mobile/jquery.jWizard.RXMod.js"></script>
	<script src="#rootUrl#/#PublicPath#/js/RXXML.js" type="text/javascript" charset="utf-8"></script>
	--->
	</cfoutput>
<script TYPE="text/javascript">

	var UUID = getURLParameter('UUID');	
	var BATCHID = <cfoutput>#BATCHID#</cfoutput>;
	var QIDs = new Array();
	var SurveyCookieName = "SurveyCookie";

	var isSendSurvey = false;
	
	// Get the index of survey step that user has completing
	var curStepIndex = parseInt(getCookie(SurveyCookieName));
	var isGetData = false;
	var	tolito;
	$(document).ready(function() {
		var currentStep=1;
		$("#currentStep").val(1);
		if (isGetData) {
			return;	
		}
		else {
			isGetData = true;	
		}
		LoadSurveyQuestions(BATCHID, QIDs);
		$('.mobileRadio').trigger('create');
		//$("input[type='radio']").attr("checked",true).checkboxradio("refresh"); 
		var indicateHtml = '<div style="display: block;">* indicates a required question.<div>';
		$("#TableSurveyItems").children(2).append(indicateHtml);
		
		if (groups == undefined || groups.length == 0) {
			return;
		}
		
		tolito= TolitoProgressBar('progressbar')
				.setOuterTheme('d')
				.setInnerTheme('e')
				.isMini(true)
				.setMax(100)
				.setStartFrom(0)
				.setInterval(10)
				.showCounter(true)
				//.logOptions()
				.build();
		//call SetPercenTage function with parameter is currentStep 
     	SetPercenTage(currentStep);
		var totalStep=0;
		$("#TableSurveyItems span[class='step']").each(function(){
			totalStep+=1;
		});
		$("#totalStep").val(totalStep);
		DisplayButton(currentStep,totalStep);
		
		$('.jw-counter-progressbar').removeClass('ui-widget-content');
		$('.ui-progressbar-value').addClass("clear uncompletion_survey")
		$('.jw-counter-text').hide();
	});
	
	
	function SetPercenTage(currentStep){
		var totalQuestion=0;
		var currentTotalQuestionByPage=0;
		//alert(currentStep);
		$("#TableSurveyItems span[class='step']").each(function(){
	  		if($(this).attr("id") <=currentStep){
				$(this).find("div").each(function(){
					if($(this).attr("id")!=undefined){
						if($(this).attr("id").indexOf("Q_")!=-1){
							totalQuestion+=1;
							currentTotalQuestionByPage+=1;
						}
					}
				})					
			}
			else{
				$(this).find("div").each(function(){
					if($(this).attr("id")!=undefined){
						if($(this).attr("id").indexOf("Q_")!=-1){
							totalQuestion+=1;
						}
					}
				})	
			}
			if($(this).attr("id") != currentStep){
				$(this).css("display", "none");
			}
			else{
				$(this).css("display", "block");
			}
		})
		//console.log(totalQuestion);
		//console.log(currentTotalQuestionByPage);
		//set percentage for progress bar
		tolito.setValue(Math.round((currentTotalQuestionByPage / totalQuestion) * 100));
	}
	
	function Previous(){
		var currentStep = parseInt($("#currentStep").val());
		var totalStep = parseInt($("#totalStep").val());
		if(currentStep > 1){
			SetPercenTage(currentStep-1);
			$("#currentStep").val(currentStep-1);
		}
		DisplayButton(currentStep-1, totalStep);
	}
	
	function Continuos(){
		var currentStep = parseInt($("#currentStep").val());
		var requireAnswer = isAnswerAllRequiredQuestionFrom(currentStep-1);
		if (requireAnswer) {
			var totalStep = parseInt($("#totalStep").val());
			SetPercenTage(currentStep + 1);
			$("#currentStep").val(currentStep + 1);
			DisplayButton(currentStep + 1, totalStep);
		}
	}
	
	function SendSurvey(){
		sendsurvey();
	}
	
	function DisplayButton(currentStep, totalStep){		
		//set for Previous button		
		if(currentStep == totalStep){
			if (currentStep == 1) {
				$("#SendSurvey").addClass("displayBlock");
				
				$("#Continuos").addClass("displayNone");
				$("#Previous").addClass("displayNone");
				
				$("#Continuos").removeClass("displayBlock");
				$("#Previous").removeClass("displayBlock");
			}
			else{
				$("#SendSurvey").addClass("displayBlock");
				$("#Previous").addClass("displayBlock");
				
				$("#Continuos").addClass("displayNone");
				$("#Continuos").removeClass("displayBlock");
			}
		}
		else if(currentStep < totalStep){
			if (currentStep == 1) {
				$("#Continuos").addClass("displayBlock");
				$("#Continuos").removeClass("displayNone");
				
				$("#Previous").addClass("displayNone");
				$("#SendSurvey").addClass("displayNone");
				$("#Previous").removeClass("displayBlock");
				$("#SendSurvey").removeClass("displayBlock");
			}
			else{
				$("#Continuos").addClass("displayBlock");
				$("#Previous").addClass("displayBlock");
				
				$("#Continuos").removeClass("displayNone");
				$("#Previous").removeClass("displayNone");
				
				$("#SendSurvey").removeClass("displayBlock");
				$("#SendSurvey").addClass("displayNone");
			}
		}
		
	}
	
	function ShowCongratulations(callBack) {
		var formSurveyCompletion = $('<div></div>').append('<div style="padding: 10px">' + 'You have completed this survey' + '</div>');
		formSurveyCompletion.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Congratulations',
			open: function() {
				
			},
			close: function() {  $(this).dialog('destroy'); $(this).remove() },
			width: 600,
			height: 150,
			position: 'top',
			buttons:
				[
					{
						text: "OK",
						click: function(event)
						{
							formSurveyCompletion.remove();
							if (typeof(callBack) == "function") {
								callBack();
							}
						}
					}
				]
			});		
	}
	
		
	// Get value of cookie by name
	function getCookie(c_name)
	{
		// Only name given, get cookie
        var surveyStepIdx = 0;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = $.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, c_name.length + 1) == (c_name + '=')) {
                	var arrCookieValue = decodeURIComponent(cookie.substring(c_name.length + 1)).split("|");
                	
                    if(arrCookieValue[1] == BATCHID && arrCookieValue[2] == UUID){
	                    surveyStepIdx = arrCookieValue[0];
	                    break;	
                    }
                }
            }
        }

        return surveyStepIdx;
	}
	
	// Set value, exprires time for cookie
	function setCookie(c_name,value)
	{	
		var exdays = 7;// One week: Expires day for survey cookie.
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + "|" + BATCHID + "|" + UUID + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	
	function sendsurvey() {
		var Channel = 'Email';
		var RT = 3;

		var XML ="<Response Channel='" + Channel + "'>";
		
		var questionsTmp;
		
    	questionsTmp = JSON.parse(AllQuestions);

		var isAnswerAllRequired = true;
		for (var i = 0; i < questionsTmp.length; ++i) {
			var question = questionsTmp[i];
			switch (question.TYPE) {
					case 'ONESELECTION':
					case 'ONETOTENANSWER':
					case 'STRONGAGREEORDISAGREE':
					case 'NETPROMTERSCORE':
					case 'MATRIXONE':
						var isChecked = false;
						$('input:radio[name=inpRadio_' + question.ID + ']:checked').each(function() {
							isChecked = true;
						    //if($(this).val() == "" && question.REQUIREDANS == 1){
						    	//$("#err_" + question.ID).show();
						    	//isAnswerAllRequired = false;
						    //}else{
						    	XML += generateXmlBaseQuestionType(question.ID, RT, question.TYPE, $(this).val());
						    	$("#err_" + question.ID).hide();
						    //}
						});
						if (!isChecked) {
							if (question.REQUIREDANS == 1) {							
						    	$("#err_" + question.ID).show();
						    	isAnswerAllRequired = false;
					    	}
					    	else {
					    		XML += generateXmlBaseQuestionType(question.ID, RT, question.TYPE, '-1');
					    	}
						 }
						break;
					case 'SHORTANSWER':
						var comment = $('#inpComment_' + question.ID).val();
						
						if(comment == "") {
							if (question.REQUIREDANS == 1) {
								$("#err_" + question.ID).show();
							    isAnswerAllRequired = false;
						    }
						    else {
						    	XML += generateXmlBaseQuestionType(question.ID, RT, question.TYPE, '-1');
						    }
					    }
						else {
							$("#err_" + question.ID).hide();
							XML += generateXmlBaseQuestionType(question.ID, RT, question.TYPE, comment);
						}
						
						break;
					default:
						break;
				}
		}
		
		<!--- for(var i=0;i<QuestionIDs.length;i++){
			if(QuestionTypes[i]!='COMMENT' && QuestionTypes[i]!='MATRIXONE')
			{
				$('#Q_'+QuestionIDs[i]+' input:checked').each(function() {
				    AnswerChoices.push($(this).val());		
				});
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerChoices);
				AnswerChoices.splice(0,AnswerChoices.length);
			}
			else if(QuestionTypes[i]=='COMMENT'){
				AnswerTex_COMMENT = $('#Q_'+QuestionIDs[i]+' #inpComment').val();
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerTex_COMMENT);
			}
			else if(QuestionTypes[i]=='MATRIXONE'){
				$('#Q_'+QuestionIDs[i]+' .matrixAltRow').each(function() {
					$(this).find('input:checked').each(function() {
					   AnswerTex_MATRIXONE2 = $(this).val();		
					    
					});
					AnswerTex_MATRIXONE1 += '('+ $(this).find('th:first-child').attr('value') +','+AnswerTex_MATRIXONE2+'),';
				});
				AnswerTex_MATRIXONE1 = AnswerTex_MATRIXONE1.substr(0,AnswerTex_MATRIXONE1.length-1);
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerTex_MATRIXONE1);
			}
		} --->
		XML += '</Response>';

		BATCHID = $('#TableSurveyItems').attr('batchid');
		if(isAnswerAllRequired){
			//jAlert('Submit to system');
			submitAnswer(BATCHID, UUID, XML);
		}else{
			//jAlert('Please answer all required questions.');
		}
		
		return isAnswerAllRequired;
		<!--- for(var i=0;i<QuestionIDs.length;i++){
			if(QuestionTypes[i]!='COMMENT' && QuestionTypes[i]!='MATRIXONE')
			{
				$('#Q_'+QuestionIDs[i]+' input:checked').each(function() {
				    AnswerChoices.push($(this).val());		
				});
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerChoices);
				AnswerChoices.splice(0,AnswerChoices.length);
			}
			else if(QuestionTypes[i]=='COMMENT'){
				AnswerTex_COMMENT = $('#Q_'+QuestionIDs[i]+' #inpComment').val();
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerTex_COMMENT);
			}
			else if(QuestionTypes[i]=='MATRIXONE'){
				$('#Q_'+QuestionIDs[i]+' .matrixAltRow').each(function() {
					$(this).find('input:checked').each(function() {
					   AnswerTex_MATRIXONE2 = $(this).val();		
					    
					});
					AnswerTex_MATRIXONE1 += '('+ $(this).find('th:first-child').attr('value') +','+AnswerTex_MATRIXONE2+'),';
				});
				AnswerTex_MATRIXONE1 = AnswerTex_MATRIXONE1.substr(0,AnswerTex_MATRIXONE1.length-1);
				XML+=generateXmlBaseQuestionType(QuestionIDs[i],RT,QuestionTypes[i],AnswerTex_MATRIXONE1);
			}
		} --->
	}
	function isAnswerAllRequiredQuestionFrom(currentIndex){
		
		var allQuestionTmp;
		
		if(currentIndex < 0){
			currentIndex = 0;
		}
		
    	allQuestionTmp = groups[currentIndex].questions;
		var isAnswerAllRequired = true;
		
		for (var i = 0; i < allQuestionTmp.length; ++i) {
			var question = allQuestionTmp[i];
			switch (question.TYPE) {
					case 'MATRIXONE':
					case 'ONETOTENANSWER':
					case 'STRONGAGREEORDISAGREE':
					case 'NETPROMTERSCORE':
					case 'ONESELECTION':
						var isChecked = false;
						$('input:radio[name=inpRadio_' + question.ID + ']:checked').each(function() {
							isChecked = true;
						    $("#err_" + question.ID).hide();
						});
						if(!isChecked && question.REQUIREDANS == 1){
					    	$("#err_" + question.ID).show();
					    	isAnswerAllRequired = false;
						 }
						break;
					case 'SHORTANSWER':
						var comment = $('#inpComment_' + question.ID).val();
						
						if(comment == "" && question.REQUIREDANS == 1){
							$("#err_" + question.ID).show();
						    isAnswerAllRequired = false;
						}else{
							$("#err_" + question.ID).hide();
						}
						
						break;
					default:
						break;
				}
		}

		return isAnswerAllRequired;
	}

</script>	
</cfsavecontent>


<cfsavecontent variable="surveyContent">
	
</cfsavecontent>

<cfsavecontent variable="surveyHeader">
	<cfoutput>
		#cssContent#
		<div id="survey_header">
			<cfif StructKeyExists(configPage, "LOGO") AND configPage.LOGO NEQ ''>
				<img src="#rootUrl#/#sessionPath#/marketing/survey/act_previewImage.cfm?serverfileName=#configPage.LOGO#&userId=#configPage.USERID#" height='65'/>
			<cfelse>
				<img src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm.png" height="54px">
			</cfif>
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				<span id='survey_title'>#configPage.SURVEYTITLE#</span>
			</cfif>
		</div>
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="surveyFooter">
	<div id="footer" style="width: 100%; margin: 0 auto;text-align: center;">
	<cfif StructKeyExists(configPage, "FOOTERTEXT") AND configPage.FOOTERTEXT NEQ ''>
		<cfoutput>#configPage.FOOTERTEXT#</cfoutput>
	</cfif>
	</div>
	<cfoutput>#jsContent#</cfoutput>
</cfsavecontent>
<cfset displaySurvey = 'block'>
<cfset borderSurvey = 'none'>
<style>
	#surveyContent{
		display:<cfoutput>#displaySurvey#</cfoutput>;
	}
</style>
<cfoutput>
	<div id="surveyContent">
		<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
		<table width="100%" style="margin:auto;">
				<tr>
					<td>#surveyHeader#</td>
				</tr>
				<tr>
					<td>
						<div id = "switcher"></div>
						<div id="BSContentWide123">
							<h5 id="status"></h5>
							<form id="SurveyForm" method="post" class="survey_form">
								<input type="hidden" id="currentStep" value="1"/>
								<input type="hidden" id="totalStep" value=""/>								
								
								<div id="progressbar"></div>
								<div id="TableSurveyItems"></div>
																
								<span id="Previous" style="width:45%; float:left;" class="displayNone">
									<!---<button type="button" onclick="Previous()">< Previous</button>--->
									<a href="javascript:Previous()" data-role="button">< Previous</a> 
								</span>
								<span id="Continuos" style="width:45%; float:right;" class="displayNone">
									<!---<button type="button" onclick="Continuos()">Continuos ></button>--->
									<a href="javascript:Continuos()" data-role="button">Continue ></a> 
								</span>
								<span id="SendSurvey" style="width:45%; float:right;" class="displayNone">
									<!---<button type="button" onclick="SendSurvey()">Send survey</button>--->
									<a href="javascript:SendSurvey()" data-role="button">Send survey</a>
								</span>
							</form>
							</div>
							<p id="data"></p>  
						</div>
					</td>
				</tr>
				<tr>
					<td>#surveyFooter#</td>
				</tr>
			</table>
	</div>
</cfoutput>