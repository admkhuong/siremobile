<script language="javascript" src="<cfoutput>#rootUrl#/#sessionPath#/marketing/survey/js/survey.js</cfoutput>"></script>


<script TYPE="text/javascript">
	var AllQuestions;
	<!--- Load all xml Questions of previous Survey --->
	function getURLParameter(name) {
	    return decodeURI(
	        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
	    );
	}
	
	var groups;
	var isDisableBackButton = false;
	function LoadSurveyQuestions(BATCHID,QIDs){
		$.ajax({
			type:"POST",
			async:false,
			url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/marketing/marketingSurveyTools.cfc?method=ReadXMLEMAIL&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			data:{
				<!--- UUID : UUID	//'039739d0-673f-11e1-b6f1-20cf30ae4933', --->
				BATCHID : BATCHID
			},
			dataType: "json", 
	       	success: function(d2, textStatus, xhr) {
	        	if (d2.DATA.RXRESULTCODE > 0) {
	       			var data = d2.DATA.ARRAYQUESTION[0];
	       			var BATCHID = d2.DATA.INPBATCHID[0];
	       			<!--- JSON.stringify not supported IE7,IE6
	       			http://msdn.microsoft.com/en-us/library/cc836459%28VS.85%29.aspx
	       			 --->
	       			AllQuestions = JSON.stringify(data);
					isDisableBackButton = d2.DATA.ISDISABLEBACKBUTTON == '0';
	       			groups = GroupByPage(data);
					var QNo = 1;
	           	 	for (var i = 0; i < groups.length; i++) { 
	           	 		var questions = groups[i].questions;
	           	 		questions.sort(function(a,b) { return a.index - b.index });
				        groups[i].questionList = AddNewQuestion(BATCHID, groups[i], QNo);
				        
				        QNo += groups[i].questionList.length
						<!--- else {
						    for(j=0 ; j< LISTCASE[1].LISTANSWER.length; j++){
							    AnswerChoices+=LISTCASE[1].LISTANSWER[j].TEXT+'\n';
							    AnswerID+=LISTCASE[1].LISTANSWER[j].ID+'\n';
								    
					        };	
					        if(typeof(LISTCASE) != 'undefined'){
						    	for(j=0 ; j< LISTCASE[0].LISTANSWER.length; j++){
						    		
						    		if (LISTCASE[0].LISTANSWER[j].TEXT) {
						    			ColumnChoices += LISTCASE[0].LISTANSWER[j].TEXT+'\n';
						    		}
								     
					            }
					    	}
					    	AddNewQuestion(BATCHID,QuestionDesc,QuestionType,AnswerChoices,AnswerID,ColumnChoices,QuestionID)
						} --->
					    <!--- AddNewQuestion(QuestionDesc,QuestionType,QuestionChoices,ColumnChoices,QuestionID) --->
					}
					
	          	} 
	          	else {
	           		$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", d2.DATA.MESSAGE[0]);
					$("#TableSurveyItems").append(d2.DATA.MESSAGE[0]);
	           		return false;
	          	}
	
			}
		});
		return false;
	}
		

	function GroupByPage(questions) {
		var groups = new Array();
		for (var i = 0; i < questions.length; ++i) {
			questions[i].index = i;
			var group = GetGroupById(groups, questions[i].GROUPID);
			
			if (group == null) {
				group = new Object();
				group.GROUPID = questions[i].GROUPID;
				group.questions = new Array();
				group.questions.push(questions[i]);

				groups.push(group);
			}
			else {
				group.questions.push(questions[i]);
			}
		}
		
		groups.sort(function(a,b) { return a.GROUPID - b.GROUPID });

		return groups;
	}
		
	function GetGroupById(groups, groupId) {
		for (var i = 0; i < groups.length; ++i) {
			if (groups[i].GROUPID == groupId) {
				return groups[i];
			}
		}
		return null;
	}
	
	function Question(id, text, requiredans, errmesgtxt) {
		var result = new Object();
		result.ID = id;
		result.TEXT = text;
		result.REQUIREDANS = requiredans;
		result.ERRMSGTXT = errmesgtxt;
		
		return result;
	}
	function GroupByRateType(questions) {
		var result = new Array();
		for (var i = 0; i < questions.length; ++i) {
			questions[i].questionList = new Array();
			questions[i].questionList.push(Question(questions[i].ID, questions[i].TEXT, questions[i].REQUIREDANS, questions[i].ERRMSGTXT));
			
			result.push(questions[i]);
		}
		
		return result;
	}
	
	function GetAnswerList(question) {
		var result = new Array();
		for (var i = 0; i < question.ANSWERS.length; ++i) {
			result.push(question.ANSWERS[i].TEXT);
		}
		return result;
	}
	function AddNewQuestion(BATCHID, Group, index){
		<!--- QuestionID undefined when create a new question.It will be assigned by max QuestionID--->
		var isNew = false;//check question is new or not.
		var inpHTML = '';
		var questionList = GroupByRateType(Group.questions);
		//generate HTML question and push it to form edit survey questions.
	    inpHTML += '<span id="' + Group.GROUPID + '" class="step" >';
	    var no = index;
	    var groupQuestionCount = 0;
	    for (var i = 0; i < questionList.length; ++i) {
	    	var question = questionList[i];
			var txthelp = '';
			if (question.TYPE == 'SHORTANSWER') {
				txthelp = 'Answer';
			}
			inpHTML += '<tr id="' + question.ID + '" class="trsort"><td>';
			inpHTML += '<div class="ed_QstHdr question_body"  id="Q_' + question.ID + '" type="' + question.TYPE + '">';
			
			var required = "";
			 		
			 if(question.REQUIREDANS == 1){
				required = "<font style=''> *</font>";
			 }
			
			if (question.TYPE != 'MATRIXONE') {
			 inpHTML += '<div class="question_header"><h3 id="qShortHeader" class="question_header_text question_header_text">' 
			 + no + '.' + required + ' ' + question.TEXT  + '</h3></div>';
			}
			else {
			 inpHTML += '<div class="question_header"><h3 id="qShortHeader" class="question_header_text"></h3></div>';
			}
			if (question.TYPE == 'SHORTANSWER') {
			 inpHTML += '';
			}
			inpHTML += '<div class="qBody question_header_text">';
			inpHTML += generateHtmlBaseQuestionType(question,  i + index + groupQuestionCount);
			 if (question.TYPE == 'MATRIXONE') {
				 groupQuestionCount += question.questionList.length - 1;
			 }
			inpHTML += '</div>';
			inpHTML += '<div class="clear"></div>';
			inpHTML += '</div>';
			inpHTML += '</td></tr>';	 
			
			inpHTML += '<div class="question_footer"/>';
	    	no += 1;
			if (question.questionList != null & question.questionList.length > 0) {
				no += question.questionList.length - 1;
			}
 		}
		
		inpHTML += '</span>';

		$("#TableSurveyItems").append(inpHTML);
		//$("#SurveyForm").formwizard("update_steps");
		$("#TableSurveyItems").attr("BATCHID", BATCHID);
		
		return Group.questions;
	}
	
	function isValidRadioInput(q_id, required){
		var isChecked = false;
		$('input:radio[name=inpRadio_' + q_id + ']:checked').each(function() {
			isChecked = true;
		    $("#err_" + q_id).hide();
		});
		if(!isChecked && required == 1){
		    	$("#err_" + q_id).show();
		    	isAnswerAllRequired = false;
		}
	}
	
	function isValidCommentAns(q_id){
		if($("#inpComment_" + q_id).val().trim() == ''){
			$("#err_" + q_id).show();
		}else{
			$("#err_" + q_id).hide();
		}
	}
	
	function generateHtmlBaseQuestionType(question, index) {
		var html = '';
		var QuestionType = question.TYPE;

		switch (QuestionType) {
			case 'ONESELECTION':
			case 'ONETOTENANSWER':
			case 'STRONGAGREEORDISAGREE':
			case 'NETPROMTERSCORE':
				var required = "";
		   		
		   		if(question.REQUIREDANS == 1){
		   			required = "required";
		   			html+= '<br /><label class="error" style="display: none" id="err_' + question.ID + '"><strong>' 
		   				+ question.ERRMSGTXT + '</strong></label>';
		   		}
		   		
		   		html += '<div data-role="page_' +  question.ID + '" class="mobileRadio">';
		   		html += '<div data-role="content">';
		   		html += '<div data-role="fieldcontain">';
		   		html += '<fieldset data-role="controlgroup">';
				for (var i = 0; i <question.ANSWERS.length; ++i) {
					answer = question.ANSWERS[i];
					if (answer != '') {
			        	html += '<div class="qOption">';
			           	if (i == 0) {
							html += '<input type="radio" name="inpRadio_' + question.ID + '" id="inpRadio_' + question.ID + '_' + i 
									+ '" class="rb MarRight5 required" value="' + answer.ID 
										+ '">';			           				           	
		           	    }
		           	    else {
			           		html += '<input type="radio"  name="inpRadio_' + question.ID + '" id="inpRadio_' + question.ID + '_' + i 
			           				+ '" class="rb MarRight5 required" value="' + answer.ID 
			           					+ '">';
		           	    }
		           	    html += '<label for="inpRadio_' + question.ID + '_' + i	+ '">' + answer.TEXT + '</label>'
						html += '</div>';
		           	   }
				   };
				html += '</fieldset>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
		      	break;
		   case 'SHORTANSWER':
		   		var required = "";
		   		
		   		if(question.REQUIREDANS == 1){
		   			required = "required";
		   			html+= '<label class="error" style="display: none" id="err_' + question.ID + '"><strong>' 
		   				+ question.ERRMSGTXT + '</strong></label>';
		   		}
	   			html+='<center><textarea id="inpComment_' + question.ID + '"   class="qOption required" style="width:99%; margin-right:2px;" rows="2"></textarea></center>';	
	   			
	   			break;
		    default :
		    	break;
		};
	    html += '<input id="questionType" type="hidden" name="questionType" value="' + QuestionType + '">';
		return html;	   
	}
	
	function generateXmlBaseQuestionType(QuestionID, RT, QuestionType, AnswerChoices) {
		var xmlQuestion = '';
		
		if (AnswerChoices.length < 1) {
			AnswerChoices = 0;
		}
		switch(QuestionType){
			case 'ONESELECTION':
			case 'ONETOTENANSWER':
			case 'STRONGAGREEORDISAGREE':
			case 'NETPROMTERSCORE':
			case 'MATRIXONE':
				xmlQuestion += "<Q ID='" + QuestionID + "' RT='" + RT + "' CP='1'>" + AnswerChoices + "</Q>";          			           
	    		break;	
			case 'SHORTANSWER':
				xmlQuestion += "<Q ID='" + QuestionID + "' RT='" + RT + "' CP='3'>" + AnswerChoices + "</Q>";          
		        break;
			default :
				break;
		};
 
		return xmlQuestion;
	}	
	
	
	function GetSurveyQuestionCount() {
		var result = 0;
		for (var i = 0; i < groups.length; ++i) {
			result += groups[i].questions.length;
		}
		return result;
	}
	
	function GetQuestionsByPageIndex(pageIndex) {
		var result = 0;
		for (var i = 0; i <= pageIndex; ++i) {
			result += groups[i].questions.length;
		}
		return result;
	}
	function submitAnswer(BATCHID,UUID,XML){
	$.ajax({
		type: "POST",
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/marketing/marketingSurveyTools.cfc?method=SaveAnswerSurvey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			BATCHID : BATCHID,
			UUID: UUID,
			inpXML: XML			
			},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								window.location = "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/surveys/mobile/finish/?BATCHID=<cfoutput>#BATCHID#</cfoutput>&UUID=<cfoutput>#UUID#</cfoutput>";
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Survey has NOT been save.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}			
			} 		
			
		});
	}
			
</script>
