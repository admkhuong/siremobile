<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<cfparam name="UUID" default="-1">
<cfparam name="BATCHID" default="-1">

<cfinclude template="../../paths.cfm" >
<cfinclude template="../mobileDetect.cfm" >
<cfinvoke 
     component="#Session.SessionCFCPath#.marketing.marketingSurveyTools"
     method="getConfigPage"
     returnvariable="configPage">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
</cfinvoke>

		<style>
			.survey_form {
				margin: 0 auto; 
				max-width: 960px; 
				min-width: 700px; 
				width: 100%;
			}
		</style>
<cfinclude template="../survey/jsSurvey.cfm" >

<!---
<cfinvoke 
     component="#Session.SessionCFCPath#.SurveyTools"
     method="getSurveyTemplate"
     returnvariable="SurveyTemplate">                         
        <cfinvokeargument name="INPBATCHID" value="#BATCHID#"/>
		<cfinvokeargument name="public" value="1"/>
</cfinvoke>

<cfset TemplateContent =""/>
--->

<cfsavecontent variable="cssContent">
	<cfoutput>
	<style type="text/css">
		<!--- @import url('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/base/jquery-ui.css'); --->
		@import url('#rootUrl#/#PublicPath#/css/jquery-ui.css');
		@import url('#rootUrl#/#PublicPath#/css/jWizard.base.css');
		<!--- @import url('#rootUrl#/#PublicPath#/css/rxform2.css'); --->
		@import url('#rootUrl#/#PublicPath#/css/MB.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');
		@import url('#rootUrl#/#PublicPath#/js/validate/not_custom/validationEngine.jquery.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
	</style>
	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="jsContent">
	<cfoutput>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
<!--- <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.4.2.min.js">	</script> --->
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
<!---  	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
	<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/validate/not_custom/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script> --->
	<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.jgrowl.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.validate.js"></script>
     <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/bbq.js"></script> --->
    <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.form.wizard.js"></script> --->	
	
	<!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/jWizard/js/jquery.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/jWizard/js/jquery-ui.min.js"></script> --->
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.jWizard.RXMod.js"></script>
	<script src="#rootUrl#/#PublicPath#/js/RXXML.js" type="text/javascript" charset="utf-8"></script>
	</cfoutput>
<script TYPE="text/javascript">
	
	var UUID = getURLParameter('UUID');	
	var BATCHID = <cfoutput>#BATCHID#</cfoutput>;
	var QIDs = new Array();
	var SurveyCookieName = "SurveyCookie";
	
	var isSendSurvey = false;

	// Get the index of survey step that user has completing
	var curStepIndex = parseInt(getCookie(SurveyCookieName));
	
	$(document).ready(function() {
		
		LoadSurveyQuestions(BATCHID, QIDs);
		
		var indicateHtml = '<div style="display: block;">* indicates a required question.<div>';
		$("#TableSurveyItems").children(2).append(indicateHtml);
		
		if (groups == undefined || groups.length == 0) {
			return;
		}
		var $w = $("#SurveyForm #TableSurveyItems");

		// Survey progress bar
		$w.jWizard({
			counter: {
				hideCancelButton: false,
				enable: true,
				countIndex: 20,
				questionCount: GetSurveyQuestionCount(),
				getQuestionsByPageIndex: GetQuestionsByPageIndex,
				<cfif StructKeyExists(configPage, "BUTTONBACK") AND configPage.BUTTONBACK NEQ 0>
				previous: true,
				<cfelse>
				previous: false,
				</cfif>
				type: "percentage",	 // Default: "count"
				progressbar: true,	 // Default: false
				location: "header",	 // Default: "footer"
				startCount: true,	 // Default: true
				startHide: true,	 // Default: false
				finishCount: true,	 // Default: true
				finishHide: false,	 // Default: false
				appendText: "",	 // Default: "Complete"
				orientText: "center" // Default: "left" ("center" is also valid)
			}
		}); 

		 // Customize event to log index of step
		$w.jWizard({
				buttons: {
					cancelType: "reset",
					finishType: "submit",
					nextText: 'Continuos >',
					hideCancelButton: false
				},
	
				cancel: function(event, ui) {
					$w.jWizard("firstStep");
					curStepIndex = 0;
					setCookie(SurveyCookieName,curStepIndex);
				},
				previous: function(event, ui) {
					curStepIndex--;
					setCookie(SurveyCookieName,curStepIndex);
				},
				next: function(event, ui) {
					if(!isAnswerAllRequiredQuestionFrom(curStepIndex)){
						return false;
					}
					curStepIndex++;
					setCookie(SurveyCookieName,curStepIndex);
				},
				finish: function(event, ui) {
					//$w.jWizard("changeStep", curStepIndex + 1); 

					/* if(isSendSurvey){
						jAlert('You have completed this survey.');
						return false;
					}	 */				
										
					if(!isAnswerAllRequiredQuestionFrom(curStepIndex)){
						return false;
					}
					// Send survey
					isSendSurvey = true;
					//var isThankPage = false;
					window.location = "<cfoutput>#rootUrl#/#publicPath#</cfoutput>/surveys/finish/?BATCHID=<cfoutput>#BATCHID#</cfoutput>&UUID=<cfoutput>#UUID#</cfoutput>";
				}
			});
		
		// Jump to the old step that user has completing
		$w.jWizard("changeStep",curStepIndex); 
		
		$('.jw-counter-progressbar').removeClass('ui-widget-content');
		$('.ui-progressbar-value').addClass("clear uncompletion_survey")
		$('.jw-counter-text').hide();
	});
	
	function ShowCongratulations(callBack) {
		var formSurveyCompletion = $('<div></div>').append('<div style="padding: 10px">' + 'You have completed this survey' + '</div>');
		formSurveyCompletion.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Congratulations',
			open: function() {
				
			},
			close: function() {  $(this).dialog('destroy'); $(this).remove() },
			width: 600,
			height: 150,
			position: 'top',
			buttons:
				[
					{
						text: "OK",
						click: function(event)
						{
							formSurveyCompletion.remove();
							if (typeof(callBack) == "function") {
								callBack();
							}
						}
					}
				]
			});		
	}
	
		
	// Get value of cookie by name
	function getCookie(c_name)
	{
		// Only name given, get cookie
        var surveyStepIdx = 0;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = $.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, c_name.length + 1) == (c_name + '=')) {
                	var arrCookieValue = decodeURIComponent(cookie.substring(c_name.length + 1)).split("|");
                	
                    if(arrCookieValue[1] == BATCHID && arrCookieValue[2] == UUID){
	                    surveyStepIdx = arrCookieValue[0];
	                    break;	
                    }
                }
            }
        }

        return surveyStepIdx;
	}
	
	// Set value, exprires time for cookie
	function setCookie(c_name,value)
	{	
		var exdays = 7;// One week: Expires day for survey cookie.
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + "|" + BATCHID + "|" + UUID + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}
	
	function sendsurvey() {
		var Channel = 'Email';
		var RT = 3;
		
		var allQuestionTmp;
		
    	allQuestionTmp = JSON.parse(AllQuestions);
    	
		var isAnswerAllRequired = true;
		for (var i = 0; i < allQuestionTmp.length; ++i) {
			var question = allQuestionTmp[i];
			switch (question.TYPE) {
					case 'ONESELECTION':
					case 'ONETOTENANSWER':
					case 'STRONGAGREEORDISAGREE':
					case 'MATRIXONE':
						var isChecked = false;
						$('input:radio[name=inpRadio_' + question.ID + ']:checked').each(function() {
							isChecked = true;
						    	$("#err_" + question.ID).hide();
						});
						if(!isChecked && question.REQUIREDANS == 1){
						    	$("#err_" + question.ID).show();
						    	isAnswerAllRequired = false;
						 }
						break;
					case 'SHORTANSWER':
						var comment = $('#inpComment_' + question.ID).val();
						
						if(comment == "" && question.REQUIREDANS == 1){
							$("#err_" + question.ID).show();
						    isAnswerAllRequired = false;
						}else{
							$("#err_" + question.ID).hide();
						}
						
						break;
					default:
						break;
				}
		}

		BATCHID = $('#TableSurveyItems').attr('batchid');
		if(isAnswerAllRequired){
			//jAlert('Submit to system');
		}else{
			//jAlert('Please answer all required questions.', "Warning");
		}
		return isAnswerAllRequired;
		
		//submitAnswer(BATCHID, UUID, XML);
	}

	function isAnswerAllRequiredQuestionFrom(currentIndex){
		
		var allQuestionTmp;
		
		if(currentIndex < 0){
			currentIndex = 0;
		}

    	allQuestionTmp = groups[currentIndex].questions;
    	
		var isAnswerAllRequired = true;
		
		for (var i = 0; i < allQuestionTmp.length; ++i) {
			var question = allQuestionTmp[i];

			switch (question.TYPE) {
					case 'ONESELECTION':
					case 'ONETOTENANSWER':
					case 'STRONGAGREEORDISAGREE':
					case 'MATRIXONE':
						var isChecked = false;
						$('input:radio[name=inpRadio_' + question.ID + ']:checked').each(function() {
							isChecked = true;
						    	$("#err_" + question.ID).hide();
						});
						if(!isChecked && question.REQUIREDANS == 1){
						    	$("#err_" + question.ID).show();
						    	isAnswerAllRequired = false;
						 }
						break;
					case 'SHORTANSWER':
						var comment = $('#inpComment_' + question.ID).val();
						
						if(comment == "" && question.REQUIREDANS == 1){
							$("#err_" + question.ID).show();
						    isAnswerAllRequired = false;
						}else{
							$("#err_" + question.ID).hide();
						}
						
						break;
					default:
						break;
				}
		}

		if(isAnswerAllRequired){
			//jAlert('Submit to system');
		}else{
			//jAlert('Please answer all required questions.', "Warning");
		}
		return isAnswerAllRequired;
	}

</script>	
</cfsavecontent>


<cfsavecontent variable="surveyContent">
	<div id = "switcher"></div>
	<div id="BSContentWide123">
		<h5 id="status"></h5>
		<form id="SurveyForm" method="post" class="survey_form">
			<div id="TableSurveyItems"></div>
		</form>
		</div>
		<p id="data"></p>
		 
	</div>
</cfsavecontent>

<cfsavecontent variable="surveyHeader">
	<cfoutput>
		<head>
		#cssContent#
		<div id="survey_header">
		<cfif StructKeyExists(configPage, "LOGO") AND configPage.LOGO NEQ ''>
			<img src="#rootUrl#/#sessionPath#/marketing/survey/act_previewImage.cfm?serverfileName=#configPage.LOGO#&userId=#configPage.USERID#" height='65'/>
		<cfelse>
			<img src="#rootUrl#/#publicPath#/images/ebm_i_main/logo-ebm.png" height="54px">
		</cfif>
		<cfif StructKeyExists(configPage, "SURVEYTITLE")>
			<span id='survey_title'>#configPage.SURVEYTITLE#</span>
		</cfif>
		</div>
	</cfoutput>
</cfsavecontent>
<cfsavecontent variable="surveyFooter">
	<div id="footer" style="width: 70%; margin: 0 auto;text-align: center;">
	<cfif StructKeyExists(configPage, "FOOTERTEXT") AND configPage.FOOTERTEXT NEQ ''>
		<cfoutput>#configPage.FOOTERTEXT#</cfoutput>
	</cfif>
	</div>
	<cfoutput>#jsContent#</cfoutput>
</cfsavecontent>

<cfoutput>
<html>
	<head>
		<!--- fixbug JSON.stringify in ie8 --->
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
			<cfif StructKeyExists(configPage, "SURVEYTITLE")>
				#configPage.SURVEYTITLE#
			</cfif>
		</title>
	</head>
	<body>
		<div id="surveyContent">
		<!---
		<cfif Session.TemplateContent NEQ ''>
			<cfset surveyFullContent = Replace(Session.TemplateContent, "{%surveyContent%}", "#surveyContent#") />
		<cfelse>
			<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
		</cfif>
		--->
			<cfset surveyFullContent = surveyHeader & surveyContent & surveyFooter />
			<table width="70%" style="margin:auto;">
					<tr>
						<td>#surveyHeader#</td>
					</tr>
					<tr>
						<td>#surveyContent#</td>
					</tr>
					<tr>
						<td>#surveyFooter#</td>
					</tr>
				</table>
		</div>
	</body>
</html>
</cfoutput>