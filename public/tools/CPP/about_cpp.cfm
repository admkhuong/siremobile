
<!--- set this so navigation works properly--->
<cfset LevelMap = "../../">


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><cfoutput>#BrandFull#</cfoutput></title>




<style>
	@import url('<cfoutput>#LevelMap#</cfoutput>css/ebm.css');
	@import url('<cfoutput>#LevelMap#</cfoutput>css/ebmmenu.css');
	@import url('<cfoutput>#LevelMap#</cfoutput>css/camera.css');


</style>

<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jqueryddmenu.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/camera.min.js"></script>


<script type="text/javascript">
	
	
	$(function() {	
	
			$("#nav-one").dropmenu({closeSpeed: 100});	
			
			$("#camera_wrap").camera({  
				width: '600px',
				height: '450px',		
				pagination: false,
				thumbnails: false,
				piePosition: 'leftBottom',
				portrait: true,
				marginLeft: '20px',
				alignment: 'centerLeft',
				fx: 'scrollLeft',
				barPosition: 'top',
				<!---onStartLoading: function() {$("#camera_wrap")},--->
				onLoaded: function() {<!--- console.log($("#camera_wrap"));---> }
				
			});
			
	});		
	
	

</script>



</head>

<!---
Todos

http://www.pixedelic.com/plugins/camera/



Take snap shots at 1024x768
skew 15%
scale image to 95%
add drop shadow - 12 thick @ 260 degrees
Shrink image to 650x450
Export as 8bit png with alpha transparency with transparent background

--->

<style>

</style>



<body>

    <div id="TopMenu">
    	<cfinclude template="#LevelMap#menu.cfm">
    </div>
    
    <div id="Spacer1">    
 
    </div>
    
    
    <div id="Section1">
    
    </div>
    
    <div id="LTContent">
    	<div style="float:left;">
    		<img src="<cfoutput>#LevelMap#</cfoutput>images/EBMTriSwoosh105x103_WithShadowWeb.png" />    
        </div>
        
        <div style="float:right; vertical-align:central;">
        	<h1 class="grey inline">Event Based</h1><h1 class="gold inline">Messaging</h1>
            <h3 class="white TextShadowWhite">Contact Preference Portal</h3>
        </div>    
        
		    
      	<div id="RTContentHigh">        
        	<h3 class="white TextShadowWhite">Contact Preference Portal Information:</h3>
            <ul>
                    <li><a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/about_cpp.cfm">About CPP</a></li>
                    <li><a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/mobile_cpp.cfm">Mobile</a></li>
                    <li><a href="#">Customization</a></li>
                    <li><a href="#">Data</a></li>
                    <li><a href="#">Reporting</a></li>
                    <li><a href="#">Social Tools</a></li>
                    <li><a href="#">API</a></li>
                    <li><a href="#">Try it now</a></li>
                    <li><a href="#">FAQ</a></li>
            </ul>
        </div>
        
    </div>	
    
    <div id="InteractiveGraphic">
    	<div id="camera_wrap" class="camera_ash_skin" style="width:600px; height:450px; min-height:450px;">
    	
            <div data-src="<cfoutput>#LevelMap#</cfoutput>images/cpp/SignIn.jpg" data-portrait="true" data-alignment="topLeft">
                              
             <!---   <div class="camera_caption fadeIn camera_effected">
                    <h2 class="black TextShadowGrey">Option 1: Standalone</h2>
                    <ul>
                        <li><h3 class="black TextShadowGrey">No IT resources required</h3></li>
                        <li><h3 class="black TextShadowGrey">generic Name</h3></li>
                        <li><h3 class="black TextShadowGrey">Secure SSL</h3></li>
                        <li><h3 class="black TextShadowGrey">Configurable CSS Options</h3></li>    
                        
                    </ul>      
                </div>        --->
            </div>
            
            <div data-src="<cfoutput>#LevelMap#</cfoutput>images/cpp/MBWithSignIn.jpg" data-portrait="true" data-alignment="topLeft" data-fx="simpleFade">
              <!---  <div class="RBContentSlideInfo fadeIn camera_effected">
                    
                    <h2 class="black TextShadowGrey">Option 2: iFrame Integration</h2></li>
                    <ul>
                        <li><h3 class="black TextShadowGrey">Less IT resources required</h3></li>
                        <li><h3 class="black TextShadowGrey">generic Name</h3></li>
                        <li><h3 class="black TextShadowGrey">Secure SSL</h3></li>
                        <li><h3 class="black TextShadowGrey">Configurable CSS Options</h3></li>        
                    </ul>    
                </div>     --->
            </div>
            <div data-src="<cfoutput>#LevelMap#</cfoutput>images/cpp/MBFullWithSignInSkewedWeb.png" data-portrait="true" data-alignment="topLeft">        
               <!--- <div class="RBContentSlideInfo fadeIn camera_effected">
                    <h2 class="black TextShadowGrey">Option 3: API Integration</h2>
                    <ul>
                        <li><h3 class="black TextShadowGrey">More control for those who need it</h3></li>
                        <li><h3 class="black TextShadowGrey">Allows access for running Campaigns</h3></li>
                        <li><h3 class="black TextShadowGrey">Secure SSL</h3></li>
                        <li><h3 class="black TextShadowGrey"></h3></li>         
                    </ul>       
                </div>    --->
            </div>

		    <!---<img src="<cfoutput>#LevelMap#</cfoutput>images/cpp/CPPSA2Web.png" width="600px" height="450px" />--->
    	</div>
       
      <!--- 	<a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/details_cpp.cfm" class="inline extrapadding10">More Details</a>
        <a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/details_cpp.cfm" class="inline extrapadding10">How To Use</a>
        <a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/details_cpp.cfm" class="inline extrapadding10">See it in action</a>--->
            
    </div>
    
    <div id="Section2">
    	<div id="Section2BottomRow">
        	<div align="center">
                <table border="0">
                    <tr valign="top">
                        <td width="265px;">
                            <!--- http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-chronometer-icon.html --->
                            <div>
                                <div style="float:left; padding-right:10px; padding-bottom:10px;"><img class="reflectBelow" src="../../images/marketing/Actions-view-pim-contacts-icon.png" /></div>	
                            
                                <div style="display:inline;"><h5 class="black">LET YOUR CUSTOMERS CHOOSE</h5></div>
                            </div>
                            <BR />
                            <div>
                                 <h4 class="black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Contact Preference Portal quickly enables the collection, management and distribution of consumer preferences across your enterprise.</h4>                        
                            </div>
                        </td>
                        <td width="50px">&nbsp;</td>
                        <td width="265px;">
                            <!--- http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-chronometer-icon.html --->
                            <div>
                                <div style="float:left; padding-right:10px; padding-bottom:10px;"><img class="reflectBelow" src="../../images/marketing/Apps-kdevelop-icon.png" /></div>	
                            
                                <div style="display:inline;"><h5 class="black">SEAMLESS INTEGRATION<BR />&nbsp;</h5></div>
                            </div>
                            <BR />
                            <div>
                            	<!--- Execute life cycle communications and marketing campaigns based on what consumers want. --->
                                <h4 class="black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Those preferences can now be leveraged by our <cfoutput>#BrandFull#</cfoutput> cloud tools or in conjuction with your legacy internal CRM or legacy Marketing Automation System.</h4>                        
                            </div>
                        </td>
                        <td width="50px">&nbsp;</td>
                        <td width="265px;">
                            <!--- http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-chronometer-icon.html --->
                            <div>
                                <div style="float:left; padding-right:10px; padding-bottom:10px;"><img class="reflectBelow" src="../../images/marketing/Categories-preferences-other-icon.png" /></div>	
                            
                                <div style="display:inline;"><h5 class="black">CUSTOMIZABLE TOOL SET<BR />&nbsp;</h5></div>
                            </div>
                            <BR />
                            <div>
                                 <h4 class="black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Capture designated channel preferences from multiple entry points. Create your own text and HTML or upload external content to personalize using your own style.</h4>                        
                            </div>
                        </td>              
                    </tr>    
                </table>    
            </div>	
        </div>    	
    
	</div>
    
    
    <div id="Footer">
	    <cfset ShowCounter = 0>
    	<cfinclude template="#LevelMap#footer.cfm">   	       
    </div>
    


</body>
</html>