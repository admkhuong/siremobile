
<!--- set this so navigation works properly--->
<cfset LevelMap = "../../">


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><cfoutput>#BrandFull#</cfoutput></title>




<style>
	@import url('<cfoutput>#LevelMap#</cfoutput>css/ebm.css');
	@import url('<cfoutput>#LevelMap#</cfoutput>css/ebmmenu.css');
	@import url('<cfoutput>#LevelMap#</cfoutput>css/camera.css');


</style>

<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jqueryddmenu.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="<cfoutput>#LevelMap#</cfoutput>../js/camera.min.js"></script>


<script type="text/javascript">
	
	
	$(function() {	
	
			$("#nav-one").dropmenu({closeSpeed: 100});	
									
	});		
	
	

</script>



</head>

<!---
Todos

http://www.pixedelic.com/plugins/camera/



Take snap shots at 1024x768
skew 15%
scale image to 95%
add drop shadow - 12 thick @ 260 degrees
Shrink image to 650x450
Export as 8bit png with alpha transparency with transparent background

--->


<body>

    <div id="TopMenu">
    	<cfinclude template="#LevelMap#menu.cfm">
    </div>
    
    <div id="Spacer1">    
 
    </div>
    
    
    <div id="Section1">
    
    </div>
    
    <div id="LTContent">
    	<div style="float:left;">
    		<img src="<cfoutput>#LevelMap#</cfoutput>images/EBMTriSwoosh105x103_WithShadowWeb.png" />    
        </div>
        
        <div style="float:right; vertical-align:central;">
        	<h1 class="grey inline">Event Based</h1><h1 class="gold inline">Messaging</h1>
            <h3 class="white TextShadowWhite">Contact Preference Portal</h3>
        </div>    
        
    </div>	
    
    <div id="InteractiveGraphic"></div>
    
    <div id="CPPDetails" class="TextContent">
    
    	<div class="ExtraPadding">
            <h2 class="black TextShadowGrey">The Contact Preference Portal</h2>
            <HR />
            
            <h3>Simple Implementation</h3>
            <p>The Contact Preference Portal quickly enables the collection, management and distribution of consumer preferences across your enterprise. Those preferences can now be leveraged by our <cfoutput>#BrandFull#</cfoutput> system or in conjuction with your legacy internal CRM or legacy Marketing Automation System to execute Life Cycle Communications and marketing campaigns based on what consumers want.</p>
       
       		<BR />
            
            <h3>Three options to implement</h3>
            <p>Choose what works best for you.</p>
            
            <BR />
            
            <h3>Seamless <cfoutput>#BrandShort#</cfoutput> Integration</h3>
            <p>Personalize Cross-Channel content straight from CPP data.</p>
            
            <BR />
            
            <h3>Get Social</h3>
            <p></p>
            
       		<BR />
            
            <h3>Get Social</h3>
            <p></p>
       </div>
       
       <a href="<cfoutput>#LevelMap#</cfoutput>tools/cpp/about_cpp.cfm" class="BottomLink">Back</a>
    </div>
            
    
    
    <div id="Section2">
    
    	
    
	</div>
    
    
    <div id="Footer">
	    <cfset ShowCounter = 0>
    	<cfinclude template="#LevelMap#footer.cfm">   	       
    </div>
    


</body>
</html>