<cfinclude template="paths.cfm" >
<cfoutput>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css" rel="stylesheet" />
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/rxdsmenu.css" rel="stylesheet" /> 
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/rxform2.css" />
    <link rel="stylesheet" TYPE="text/css" media="screen" href="#rootUrl#/#PublicPath#/css/MB.css" />
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.5.1.min.js"></script>
    <script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
    <link TYPE="text/css" href="#rootUrl#/#PublicPath#/css/jquery.alerts.css" rel="stylesheet" /> 
 	<script src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>
</cfoutput>

<style TYPE="text/css">
	div#hr{
		background-color:#CCC;
		height:2px;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		if(parseInt($("#result").html()) == 1)
			countDown();
		
	});
	
	function countDown()
		{
			var val = parseInt($("#countDown").html());
			val = val - 1;
			$("#countDown").html(val);	
			if(val > 1)
				setTimeout(countDown,1000);
			else
				window.location = '../public/home';
		}
	
</script>

<div id="BSHeader">
	<div id="BSHeadCenter">
    	<div id="BSHeadLogo">
    		
        </div>
    </div>
</div>

<div id="BSContent">
	    <div id="BSCLeft">
            <img src="images/mb/logo.jpg" />
            <img src="images/mb/banner-img3.jpg" />
        </div>
    	<div id="BSCRight700" align="center" style="width:685px"><!--- page content --->
      <cfif structkeyexists(url,"email") and structkeyexists(url,"code")>
        	
			<cfquery name="verifyEmail" datasource="#DBSourceEBM#">
            	Select 
                	userid_int
                FROM 
                	simpleobjects.useraccount
                WHERE
                	EmailAddress_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#url.email#">
                    	AND
                    EmailAddressVerified_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#url.code#">
            </cfquery>
            
            <cfif verifyEmail.recordcount eq 1>
            	<cfquery name="updateEmailVerification" datasource="#DBSourceEBM#">
                	UPDATE 
                    	simpleobjects.useraccount
                    SET 
                    	EmailAddressVerified_vch = '1'
                    WHERE
                    	userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#verifyEmail.userid_int#"> 
                </cfquery>
                <p style="display:none" id="result">1</p>
                <p style="font-size:18px; color:#333; background-color:#FAF2F6; padding:40px 10px 40px 10px; line-height:2">
                    Thank you for verification. You will be re-directed to login page in <span style="color:green" id="countDown">10</span> seconds.<br />
                    <span style="font-size:14px;color:#666">Click <A href="session/account/home">continue</A> to go to your account now.</span>
                </p>
            <cfelse>
            	<p style="display:none" id="result">0</p>
            	<p style="font-size:18px; color:#333; background-color:#FAF2F6; padding:40px 10px 40px 10px; line-height:2">
                    <span style="color:#970C0C">Sorry email address OR code doesn't match. Please try again!!</span>
                </p>
            </cfif>
        <cfelseif structkeyexists(url,"email")>
        	<p style="display:none" id="result">0</p>
            
            <cfif find('@',url.email) lte 0>
            	<cfquery name="getEmail" datasource="#DBSourceEBM#">
                	select 
                    	emailaddress_vch
                    from
                    	simpleobjects.useraccount
                    where
                    	username_vch like <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#url.email#">
                </cfquery>
                <cfset url.email = getEmail.emailaddress_vch >
            </cfif>
        	
			<cfoutput>
                <cfif structkeyexists(url,"resend")>
                    <cfquery name="getRamdomCode" datasource="#DBSourceEBM#">
                        select
                            EmailAddressVerified_vch
                        from
                            simpleobjects.useraccount
                        where
                            emailaddress_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#url.email#">
                    </cfquery>
                    <cfmail to="#url.email#" from="validation@MessageBroadcast.com" subject="Email verification for MessageBroadcast demo account" 
						server="smtp.gmail.com" 
					     username="messagebroadcastsystem@gmail.com" 
					     password="mbs123456" 
					     port="465" 
					     useSSL="true" 
					     type="html">
<!---                     <cfmail to="#url.email#" from="validation@MessageBroadcast.com" subject="Email verification for MessageBroadcast demo account" >					      --->
                        <table width="75%" border="1px solid ##875569" style="border-collapse:collapse; padding:5px" cellpadding="5px" cellspacing="5px">
                           <tr>
                                <td colspan="2" style="padding:20px; font-size:15px; font-family:Verdana, geneva, sans-serif; color:##333; line-height:1.5; border:none">
                                    <span style="font-size:26px; color:##999999; font-family:Script; font-weight:bold"> 
                                        Hi #left(url.email,find('@',url.email)-1)#,
                            </span>
                                    <br />
                                    <br />
                                    <br /> 
                                    Please confirm your email address for MessageBroadcast account by clicking this link:<br>
                                    <A href="#rootUrl#/#PublicPath#/verifyEmail.cfm?email=#URLEncodedFormat(url.email)#&amp;code=#getRamdomCode.EmailAddressVerified_vch#">
                                        #rootUrl#/#PublicPath#/verifyEmail.cfm?email=#URLEncodedFormat(url.email)#&amp;code=#getRamdomCode.EmailAddressVerified_vch#
                            </A>
                                    <br />
                                    <br />
                                    <br />
                                    <br /> 
                                    <span style="font-family:verdana; color:##999999; font-size:16px">The MessageBroadcast Team</span><BR /> 
                                    <BR /> 
                                    <BR /> 
                                    <BR /> 
<!---                                    <img src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/mb/logo.jpg" /><BR /> 
            						<img src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/mb/banner-img3.jpg" />--->
                                </td>
                            </tr>
                        </table> 
                    </cfmail>
                    <p style="background-color:##FAFDD0; font-size:20px">Email Sent!!</p>
                </cfif>
                
                <p style="font-size:18px; color:##333; background-color:##FAF2F6; padding:40px 10px 40px 10px; line-height:2">
                    Thank you for registration. Please verify your email by clicking the link sent to <Br />
                    <cfif structkeyexists(url,"email")><span style="color:green">#url.email#</span><cfelse> you email address</cfif>&nbsp;&nbsp;&nbsp;
                    <span style="font-size:14px">[<a href="#verifyEmailPath#?email=#URLEncodedFormat(url.email)#&amp;resend">resend link</a>]</span><br />
                    <span style="font-size:14px;color:##666">Click <A href="##">conitnue to my account</A> once you verify your email address.</span>
                </p>
            </cfoutput>
        </cfif>
        <!---END: page content --->
        
        </div>
    </div>
</div>


<div id="BSFooter">
    <div id="BSFooterCenter">
    	<div id="hr"></div>
        <div id="FooterMenu"> 
            <span id="FooterMenuItem"><a>About</a></span> | 
            <span id="FooterMenuItem"><a href="<cfoutput>#rootUrl#/#PublicPath#/dsp_FindUs.cfm</cfoutput>">Find Us</a></span> | 
            <span id="FooterMenuItem"><a>Privacy</a></span> | 
            <span id="FooterMenuItem"><a>Terms of Use</a></span> | 
        </div>
        
        <div id="FooterNotice">
        	MessageBroadcast &copy; 2011       
        </div>
        
	</div>
</div>
