
<cfinclude template="../paths.cfm" >

<!--- Validate the keymaster --->

<cfparam name="inpUserID" default="" />
<cfparam name="inpPassword" default="" />


<!--- Hard coded one only for now --->
<cfif CompareNoCase(TRIM(inpUserID), "km@ebmui.com") EQ 0 AND CompareNoCase(TRIM(inpPassword), "123456") EQ 0>
	
    <cfset KeyMaster = 1 />
    
    <!--- Create Cookie --->        
    
    <!--- Create new string for Cookie---> 
	<cfset strKeyMasterMe = (
            CreateUUID() & ":" &
            CreateUUID() & ":" &
            1 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
            CreateUUID()  
    ) />
        
        
    <!--- Encrypt the value. --->
    <cfset 	strKeyMasterMe = Encrypt(
            strKeyMasterMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
    ) />


    <!--- Store the cookie such that it will expire after 30 days. --->
    <cfcookie
        name="EBMKM"
        value="#strKeyMasterMe#"
        expires="1"
    />
                                
        
	<cflocation url="gen?1=1" />
    
<cfelse>
	
    
    <!--- Update Cookie --->  
    <!--- Create new string for Cookie---> 
	<cfset strKeyMasterMe = (
            CreateUUID() & ":" &
            CreateUUID() & ":" &
            0 & ":" &  <!--- 0 is not authorized --- 1 is authorized --->
            CreateUUID()  
    ) />
        
        
    <!--- Encrypt the value. --->
    <cfset 	strKeyMasterMe = Encrypt(
            strKeyMasterMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
    ) />


    <!--- Store the cookie such that it will expire after 30 days. --->
    <cfcookie
        name="EBMKM"
        value="#strKeyMasterMe#"
        expires="1"
    />
    
      
    <cfset KeyMaster = 0 />
    <cflocation url="gen?msg=#URLEncodedFormat('Invalid Key - Please Try Again (code=1)')#" />

</cfif>



