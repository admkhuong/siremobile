<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--- Don't index this page for search engines --->
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<title>Simple Key Manager</title>

<cfinclude template="../paths.cfm" >
<cfinclude template="../header.cfm">

<cfparam name="inpFirstName" default="" />
<cfparam name="inpLastName" default="" />
<cfparam name="inpCompanyName" default="" />



<cfparam name="KeyMaster" default="0" />

<cftry>
 <!--- Decrypt out remember me cookie. --->
    <cfset LOCALOUTPUT.EBMKM = Decrypt(
		COOKIE.EBMKM,
		APPLICATION.EncryptionKey_Cookies,
		"cfmx_compat",
		"hex"
		) />
    <!---
	For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
	of list. Extract it from the list.
	--->
    <cfset KeyMaster = ListGetAt(
		LOCALOUTPUT.EBMKM,
		3,
		":"
		) />
<cfcatch type="any">

	<cfset KeyMaster = 0 />

</cfcatch>
	
</cftry>



<!---

10 digit key where digit 4 is the mod value 10 of the sum of the other digits

use random generator to make keys

validate sum on entry

--->



<!--- Add Basic Account Security Here ... --->








<style>


</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>






<!---

	CREATE TABLE `simpleobjects`.`simplekeys` (
	  `PKId_int` INT NOT NULL AUTO_INCREMENT,
	  `SimpleKey_vch` VARCHAR(45) NULL,
	  `FirstName_vch` VARCHAR(1024) NULL,
	  `LastName_vch` VARCHAR(1024) NULL,
	  `SignedNDA_int` TINYINT NULL DEFAULT 0,
	  `CompanyName_vch` VARCHAR(1024) NULL,
	  PRIMARY KEY (`PKId_int`));

--->

<cfset SimpleKeyArr = ArrayNew( 1 ) />
<cfset SimpleKey = "Invalid Key" />


<cfoutput>

   <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
    
    <!--- EBM site footer included here --->
    <cfinclude template="../act_ebmsiteheader.cfm">
           
    <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Simple Key Manager</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
					       I am Vinz, Vinz Clortho, Keymaster of Gozer. Volguus Zildrohar, Lord of the Sebouillia. Are you the Gatekeeper?    
                           <p style="font-size:10px;"><i>"Please forgive the Ghostbusters reference - I couldn't resist."</i></p>     	
                </div>
                
                <img style="position:absolute; top:0px; right:50px;" src="#rootUrl#/#publicPath#/images/m1/zuul.png" width="459" height="309" />
              
            </div>
        </div>
        
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                         <h2>I am the Key Master</h2>
                         <p>Are you the Gatekeeper?</p>
                    </header>
                </div>    
                                          

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;"> 
                
                
                	<cfif KeyMaster EQ 0>
                                
                 		<div class="loginform" style="min-height:200px;">
                            
                                <form id="MainForm" action="act_validatekm" method="post">
                                    
                                    <ul>
                                        
                                        <li>
                                            <label for="inpUserID">Email</label>
                                            <input type="email" id="inpUserID" name="inpUserID" placeholder="yourname@email.com" required autocomplete="on">
                                        </li>
                                        
                                        <li>
                                            <label for="inpPassword">Password</label>
                                            <input type="password" placeholder="password" required id="inpPassword" name="inpPassword">
                                        </li>
                                    
                                    	<li>
                                   			 <input type="submit" value="Submit" class="loginbutton bluelogin small" />     
                                        </li>                                                                                    
                                    </ul>
                                                                        
                                </form>
                                
                                
                            </div>
                            
                    <cfelse>
                               
               
						<cfif LEN(TRIM(inpFirstName)) GT 0 AND LEN(TRIM(inpLastName)) GT 0>
                
                            <!--- Lookup if Name already has a key 	--->
                        
                           <cfquery name="GetMemberInformation" datasource="#Session.DBSourceEBM#">
                               SELECT 
                                    PKId_int,
                                    SimpleKey_vch,
                                    FirstName_vch,
                                    LastName_vch,
                                    SignedNDA_int,
                                    CompanyName_vch,
                                    Created_dt
                                FROM 
                                    simpleobjects.simplekeys
                                WHERE
                                    FirstName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpFirstName)#">
                                AND    
                                    LastName_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastName)#">
                                AND
                                    CompanyName_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCompanyName)#">   
                                ORDER BY 
                                    LastName_vch DESC           
                            </cfquery>    
                            
                            
                            <cfif GetMemberInformation.RecordCount GT 0>
                                <!--- Member already exists --->
                            
                                <div>	
                                                                   
                                    <p>A key already exists for this user, it is #GetMemberInformation.SimpleKey_vch#</p>
                                        
                                </div>     
                                
                            <cfelse>
                                <!--- Add a new member --->
                            
                            
                                <!--- Create a key --->	
                                
                                <!--- Set up available numbers. --->
                                <cfset strNumbers = "123456789" />
                            
                                <cfset SumValue = 0 />
                            
                                <cfloop
                                    index="intChar"
                                    from="1"
                                    to="9"
                                    step="1">
                                
                                    <!---
                                        Pick random value. For this character, we can choose
                                        from the entire set of valid characters.
                                    --->
                                    
                                    <cfset CharBuff = Mid( strNumbers, RandRange( 1, Len( strNumbers ) ), 1 ) />
                                    
                                    <cfset SimpleKeyArr[ intChar ] = CharBuff />
                                    
                                    <cfset SumValue = SumValue + CharBuff  />                                    
                                
                                </cfloop>
                        
                               <!--- 
                                    <cfdump var="#SimpleKeyArr#" />
                                    <cfdump var="#SumValue#" />
                                --->
                        
                                <!--- Insert SumValue Mod 10 into fourth char position for a new string of length 10 --->
                                <cfset ArrayInsertAt(SimpleKeyArr, 4, (SumValue MOD 10)) />
    
                                <!---<cfdump var="#SimpleKeyArr#" />--->
                                
                                <cfset SimpleKey = ArrayToList(SimpleKeyArr, "") />
                            
                                <cftry>
                                    <cfquery name="AddMemberInformation" datasource="#Session.DBSourceEBM#">
                                       INSERT INTO
                                        simpleobjects.simplekeys	 
                                        (
                                            PKId_int,
                                            SimpleKey_vch,
                                            FirstName_vch,
                                            LastName_vch,
                                            SignedNDA_int,
                                            CompanyName_vch,
                                            Created_dt
                                            )
                                        VALUES
                                        (
                                            NULL,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SimpleKey)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpFirstName)#">,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastName)#">,
                                            0,
                                            <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpCompanyName)#">,
                                            NOW()                                        
                                        )   
                                        
                                    </cfquery>    
                        
                                <cfcatch type="any">
                                
                                    <!--- Add collision detection?--->
                                    
                                </cfcatch>
                                
                                </cftry>
                                    
                                <div>	
                                                                                                    
                                    <p>The key you generated is #SimpleKey#</p>
                                        
                                </div>                            
                            
                            </cfif>
                            
                        <cfelse>
                        
                            <div class="loginform" style="min-height:250px;">
                            
                                <form id="MainForm" action="gen">
                                    
                                    <ul>
                                        <li>
                                            <label for="inpFirstName">First Name</label>
                                            <input type="text" id="inpFirstName" name="inpFirstName" placeholder="First Name" required autocomplete="on">
                                        </li>
                                        
                                         <li>
                                            <label for="inpLastName">Last Name</label>
                                            <input type="text" id="inpLastName" name="inpLastName" placeholder="Last Name" required autocomplete="on">
                                        </li>
                                      
                                      
                                        <li>
                                            <label for="inpCompanyName">Company Name</label>
                                            <input type="text" id="inpCompanyName" name="inpCompanyName" placeholder="Company Name" autocomplete="on">
                                        </li>
                                      
                                    	<li>
                                   			 <input type="submit" value="Submit" class="loginbutton bluelogin small" />     
                                        </li>  
                                                                                            
                                    </ul>
                                    
                                    
                                </form>
                                
                                
                            </div>
                        
                        </cfif>
                
                	</cfif> <!--- validate the key master --->

				</div>
        
        	</div>
        
        </div>
        
        
        
          
        
    
       
		<div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omnichannel engagement
                
            </div>
        </div>    
        
        
                        
      
      	<div class="transpacer"></div>
        



    <!--- EBM site penultimate footer included here --->
        <cfinclude template="../act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>











