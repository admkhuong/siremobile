<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EBM - For Sale, Lease, or Rent</title>

<cfinclude template="../paths.cfm" >
<cfinclude template="../header.cfm">

<cfinclude template="inc_validatesk.cfm" />

<cfoutput>
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
</cfoutput>
        

<style>
		
</style>

<!--- Comparison Chart CSS3 --->
<style>

.table {overflow-x: auto;}
table.pricing {width:auto;}
table.pricing th {padding: 10px 0; color: #999; font: 300 1.154em sans-serif; text-align: center; width:125px;}
table.pricing strong {color: #3f3f3f; font-size: 12px;}
table.pricing sup {position: relative; top: -0.5em; color: #3f3f3f; font-size: 1.2em;}
table.pricing td {color: #3f3f3f; text-align: center; line-height:25px;}
table.pricing td:first-child {color: #999; text-align: left;}
table.pricing td:nth-child(2n+2) {background: #f7f7f7;}
table.pricing tr {margin-bottom: 5px; }
table.pricing tr.action td {padding: 20px 10px; border-bottom-width: 2px;}
table.pricing tr.action td:first-child a {padding-left: 20px; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/more.png") 0 50% no-repeat; color: #3f3f3f;}
table.pricing tr.action td:first-child a:hover {color: #ff8400;}
table.pricing span.yes {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/msg-success.png") 50% 50% no-repeat; text-indent: -50em;}
table.pricing span.no {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/no.png") 50% 50% no-repeat; text-indent: -50em;}


.tooltiptext{
	display: none;
}
.showToolTip{
	cursor:pointer;
	
}

div.hide-info{
	float:left;
	margin: 0 5px 0 0;
	line-height:25px;
}

div.hide-info:hover{
	opacity:.8;
}

.info-block{
	background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
    left: 100px;
    padding: 6px;
    position: relative;
    top: -10px;
}


	.tileicon
	{
		
		background: rgb(35,138,203); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(35,138,203,1) 0%, rgba(30,87,153,1) 0%, rgba(32,124,202,1) 98%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(35,138,203,1)), color-stop(0%,rgba(30,87,153,1)), color-stop(98%,rgba(32,124,202,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(35,138,203,1) 0%,rgba(30,87,153,1) 0%,rgba(32,124,202,1) 98%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#238acb', endColorstr='#207cca',GradientType=0 ); /* IE6-9 */
		
	   	padding-left: 15px;
    	padding-right: 0;
    	width: 100px;
		border-radius: 5px;
		width: 100px;
		height:100px;
		box-shadow: 0 5px 2px #d1d1d1;
		cursor:pointer;
	}
	
	.tileicon:hover
	{
	    background-color: #f8853b !important;
	}
	
	.tileiconBox 
	{		
		border-radius: 5px;
		display: inline-block;
		padding: 10px;	
		outline:none;
		border:none;
	}
	
	.shine {
				display: block;
				position: relative;
				background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 100%);
				background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(255,255,255,0)), color-stop(50%,rgba(255,255,255,1)), color-stop(100%,rgba(255,255,255,0)));
				background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				background: -o-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				background: -ms-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#00ffffff',GradientType=1 );
				background: linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 50%,rgba(255,255,255,0) 100%);
				padding: 0px 12px;
				top: 0px;
				left: 0px;
				height: 6px;
				-webkit-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				 -khtml-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				   -moz-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
					 -o-box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
						box-shadow: rgba(255,255,255,0.2) 0px 1px 5px;
				-webkit-transition: all 0.3s ease-in-out;
				 -khtml-transition: all 0.3s ease-in-out;
				   -moz-transition: all 0.3s ease-in-out;
					 -o-transition: all 0.3s ease-in-out;
						transition: all 0.3s ease-in-out;
	}
	.tileicon:hover .shine {left: 24px;}
	.tileicon:active .shine {opacity: 0;}
	
	.HideOutline
	{
		outline:none;
		border:none;
	}
	
	grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid:after {
		clear: both;
	}
	.grid:before, .grid:after {
		content: " ";
		display: table;
	}
	.grid {
		width: 100%;
	}
	
	.grid .col {
		float: left;
		font-size: 14px;
		margin-bottom: 15px;
		table-layout: auto;
		width: 50%;
	}
	.col {
		display: table;
		table-layout: fixed;
		width: 100%;
	}
	
	.HomeTilesInfo h2
	{
		font-size: 18px;
		margin-bottom:8px;
	}

	.HomeTilesInfo p
	{
		font-size: 14px;	
	}
	
	.TileCellHome
	{ 
		display: table-cell;
		padding-left:12px;
		vertical-align: top;
		position:relative;
		height:114px;
		
	}
	
	.HomeTilesInfo a
	{
		color: #07b4ee;
	    text-decoration: underline;	
	}
	
	.HomeTilesInfo a:hover
	{
		color: #f8853b;	    
	}
	
	
		.clear
		{
			clear:both;	
		}
			

</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:620px; margin-top:0px;">Success For Sale!</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content" style="width:620px;">
                	You have been given exclusive access to the private area of the EBM site. Content in the private section is targeted more for potential buyers or investors in the EBM technology. If you bookmark this page now you can get back to the private area quickly. You can also jump over to the public sections (via the logo or menu above) to read more about EBM features, but you must come back to the private area through this authenticated page.
                    <BR />
                  <!---  Proven Technology - Fully vetted in real world conditions.
                    <BR />
                    Do more with less
                    <BR />
                    <BR />--->
                    <BR />
                    <p>Jeffery L. Peterson <b>|</b> EBM </p>
                    <p>Tel: <a style="color:##0085c8" href="tel:9494000553">949.400.0553</a> </p>
                    <p><a style="color:##0085c8; margin-right:5px;" href="mailto:jp@ebmui.com">jp@ebmui.com</a><b>|</b> <a style="color:##0085c8; margin-right:20px;" href="https://ebmui.com">ebmui.com</a></p>

                 </div>
                
                <img style="position:absolute; top:38px; right:50px; " src="#rootUrl#/#publicPath#/images/m1/confidential.png" width="250" height="223" />
              
            </div>
        </div>
        
       <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="content1">
                
                <div id="servicemain">
                                          
                                          
                        <div class="section-title-content clearfix">
                            <header class="section-title-inner">
                                <h2>SMARTER TECHNOLOGY</h2>
                                <p>Success for sale, lease or rent</p>
                            </header>
                        </div>
 
                       
                       <div class="HomeTilesInfo" style="float:left; width:435px; text-align:left;">
                       
                            
                            <div style="position:relative; float:left;">
                                <a href="#rootUrl#/#publicPath#/wai/why?sk=#sk#" class="tileicon t1" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="64px" height="100px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/draw.png" style="margin: 0px 26px 0px 10px">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>	
                                </a>
                            </div>
                           
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/wai/why?sk=#sk#" style="color: ##015172; text-decoration:none;">Why #BrandShort#?</a>
                                </h2>
                                <p>
                                    Designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context... 
                                    <br>
                                    <a class="more" href="#rootUrl#/#publicPath#/wai/why?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                </p>
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                            
                            <div style="position:relative; float:left;">
                                <a href="#rootUrl#/#publicPath#/wai/jlp?sk=#sk#" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="85px" height="68px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/architech.png" style="margin: 16px 7px 16px 6px;">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>
                                </a>
                            </div>
                           
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/wai/jlp?sk=#sk#" style="color: ##015172; text-decoration:none;">The Architect and CTO</a>
                                </h2>
                                <p>
                                    Extensive expertise in the development of data driven applications for business, retail, utilities, healthcare. Over the last 13 years...  
                                    <br>
                                    <a class="more" href="#rootUrl#/#publicPath#/wai/jlp?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                </p>
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                            
                            <div style="position:relative; float:left;">
                                <a href="#rootUrl#/#publicPath#/hiw_comparisonchart?sk=#sk#" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="55px" height="64px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/compareisonicon.png" style="margin: 18px 23px 18px 22px;">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>
                                </a>
                            </div>
                            
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/hiw_comparisonchart?sk=#sk#" style="color: ##015172; text-decoration:none;">Competitive Feature Comparison</a>
                                </h2>
                                <p>
                                    EBM outperforms other messaging products by using a mix of proprietary technologies, open source tools, and experience...                                       
                                </p>
                                <BR />
                                    <a class="more" href="#rootUrl#/#publicPath#/hiw_comparisonchart?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                       
                       
                       </div>
                       
                       
                        <div class="HomeTilesInfo" style="float:right; width:435px; text-align:left;">
                       
                            <div style="position:relative; float:left;">
                                <a href="#rootUrl#/#publicPath#/hiw_clients?sk=#sk#" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="64px" height="64px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/clientsicon.png" style="margin: 18px 18px 18px 18px;">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>
                                </a>
                            </div>    
                           
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/hiw_clients?sk=#sk#" style="color: ##015172; text-decoration:none;">Clients Using It Now</a>
                                </h2>
                                <p>
                                    EBM has the privilege of working with some of the most well known companies in the world and we don't take this lightly. Our clients engage us in ...
                                    <br>
                                    <a class="more" href="#rootUrl#/#publicPath#/hiw_clients?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                </p>
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                            
                            <div style="position:relative; float:left;">
                                <a href="#rootUrl#/#publicPath#/wai/industrytransactions?sk=#sk#" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="79px" height="90px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/factory1.png" style="margin: 5px 21px 5px 0px;">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>
                                </a>
                            </div>
                            
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/wai/industrytransactions?sk=#sk#" style="color: ##015172; text-decoration:none;">Recent Industry Transactions</a>
                                </h2>
                                <p>
                                    October 10, 2013 - ***** Communications (NASDAQ: ****) today announced that it has signed an agreement to acquire **** , the leading provider of cloud-based outbound customer engagement solutions.
                                    <br>
                                    <a class="more" href="#rootUrl#/#publicPath#/wai/industrytransactions?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                </p>
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                            
                            <div style="position:relative; float:left;">	
                                <a href="#rootUrl#/#publicPath#/wai/future?sk=#sk#" class="tileicon" style="display: table-cell; position:relative;"><div class="shine"></div>
                                    <span>
                                        <img class="tileiconBox" width="80px" height="80px" alt="icon" src="#rootUrl#/#PublicPath#/images/m1/comingsoonicon.png" style="margin: 10px 20px 10px 0px; ">
                                    </span>
                                    
                                    <span style="position:absolute; bottom:5px; right:5px;">
                                        <img class="HideOutline" width="21px" height="24px" alt="" src="#rootUrl#/#PublicPath#/images/m1/next3dtp3.png">
                                    </span>
                                </a>
                            </div>
                           
                            <div class="TileCellHome">
                                <h2>
                                    <a href="#rootUrl#/#publicPath#/wai/future?sk=#sk#" style="color: ##015172; text-decoration:none;">Coming Attractions</a>
                                </h2>
                                <p>AAU, More Agent Tools, More Advanced Self Service Tools, More Social Tools - over the top options...
                                    <br>
                                    <a class="more" href="#rootUrl#/#publicPath#/wai/future?sk=#sk#" style="position:absolute; bottom:0px; right:12px;">learn more...</a>
                                </p>
                            </div>
                            
                            <div style="clear:both; padding: 8px 0;"></div>
                            
                       
                       
                       </div>
                       
                       
                </div>
                
            </div>
        
        </div>
        
        <div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omnichannel engagement
                
            </div>
        </div>    
        
        
          <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                        <h2>BUSINESS MODELS</h2>
                        <p>Many ways to make money and build a business</p>
                    </header>
                </div>    
         
         		<div align="left">
         			<h2 style="color: ##0085c8;">Gateway Model (my personal favorite)</h2>
                    <BR/>
                	<ul style="margin: 8px 0 0 30px;">
                    	<li>Fee per message usage - minimum %50 markup with typical in the %200-300 markup range not uncommon.</li>
                        <li>It really is good to be the gatekeeper - ask the credit card companies.</li>
                    </ul>    
                </div>
                
                <BR />
                
                <div align="left">
         			<h2 style="color: ##0085c8;">Capital Cost Model (Retail)</h2>
                    <BR/>
                	
                    <ul style="margin: 8px 0 0 30px;">
                   		<li>Unlimited Usage Voice, eMail, SMS</li>
                    	<li>1 contact x 1 per day x 365 days. Worst case voice cost is 3.65 per year. SMS is half that at 1.83 per year. eMail is half again or even less. </li>
                        <li>Most people will not tolerate too many messages - I'd predict no more than 1 every three days max </li>
                        <li>Require opt in only through #BrandShort# CPP. Not everyone will opt in or stay opted in.</li>
                        <li>Sell it for 2.00 per contact string per year. So an unlimited messaging  plan for up to 100,000 people would sell for 200,000 per year with a predicted margin of 50% or higher depending on opt ins percentage.</li>
                        <li>Outbound voice only so far - inbound voice pricing would take some more thought but might be doable as a future add on charge.</li>
                                            
                    </ul>    
    
                        
                        <p>
                        <BR />                        
                        <h3>Unlimited Voice, email, SMS</h3>
                        <BR />
                        <h3>Small package</h3>
                        <BR />
                        100,000 max list size
                        <BR />
                        $200,000 per year capital cost
                        <BR />
                        <BR />
                        <h3>Med package</h3>
                        <BR /> 
                        1,000,000 max list size
                        <BR />
                        $2,000,000 per year capital cost
                        <BR />
                        <BR />
                        <h3>Large package</h3>
                        <BR />
                        2,000,000 max list size
                        <BR />
                        $4,000,000 per year capital cost
                        <BR />
                        </p>



                    
                    
                </div>
                
                <BR />
                
                <div align="left">
         			<h2 style="color: ##0085c8;">Flat Fee Usage Model</h2>
                    <BR/>
                    
                     <ul style="margin: 8px 0 0 30px;">
                            
                    	<li>Large corporation provides their own bandwidth, Voice and SMS access. The corporation then buys EBM system access to these services for a flat monthly fee based on bandwidth volume.</li>    
                        <li>Sample Usage - $995 per month per T1 line. 1 t1=24 ports of voice service. 1 Client currently has 150+ of these lines. One time hardware cost generates no cost 150K a month in revenue for lifetime of equipment. Typical lifespan of equipment is 3-5 years.</li>
                        <li>In an additional barter model the Corporation can also get additional bandwidth by providing free bandwidth for other EBM client needs. Corporations can barter Co-Lo space, power, bandwidth in exchange for EBM services.</li>
                                                                                         
                     </ul>
	          	
                </div>
                
                <BR />
                <BR />
                
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                        <h2>Additional Revenue Sources</h2>
                        <p>Support Fees Model</p>
                    </header>
               	 </div>  
                 
                <div align="left">
                	                    
                     <ul style="margin: 8px 0 0 30px;">
                            
                    	<li>Consulting Services</li>    
                        <li>Campaign Creative Fees - Graphic Design, Voice Talent, HTML, Data Capture, Forms, Scripting, and more</li>
                        <li>Labor Outsourcing - App Development</li>
                        <li>SFTP services</li>
                        <li>VPN services</li>
                        <li>Complaince Audits by QSAs</li>
                        <li>Application building</li>
                        <li>Data hosting and DBA services</li>
                        <li>EAI exposes many corporate opportunities for additional services.</li>
                                                                                           
                     </ul>
	          	
                </div>
                
        	</div>
        
	      <!---  <a href="#rootUrl#/#publicPath#/wai/why?sk=#sk#">Why #BrandShort#?</a>
            <BR />
        	<a href="#rootUrl#/#publicPath#/hiw_clients?sk=#sk#">Clients Using It Now</a>
            <BR />
            <a href="#rootUrl#/#publicPath#/wai/jlp?sk=#sk#">The Architech and CTO behind the Technology</a>
            <BR />
            <a href="#rootUrl#/#publicPath#/hiw_comparisonchart?sk=#sk#">Competitive Feature Comparison</a>
            <BR />
            <a href="#rootUrl#/#publicPath#/wai/industrytransactions?sk=#sk#">Recent Industry Transactions</a>
        	<BR />
            <a href="#rootUrl#/#publicPath#/wai/future?sk=#sk#">Coming Attractions</a>
            <BR />--->
        
        </div>
                        
      
      	<div class="transpacer"></div>
        <div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="../act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>