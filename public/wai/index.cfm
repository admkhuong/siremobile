<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Offer Letter</title>


<cfinclude template="../paths.cfm" >
<cfinclude template="../header.cfm">


 <!--- Make code work so multiple log ins can exist on same page - for samples and sign up sections--->
    <cfparam name="SIID" default="0"/>
    
   	<cfinclude template="../header.cfm" />
   	
			<script type="text/javascript" language="javascript">
				
				$(function() {		 
			 					   
				   	$("#LoginLink").click(function(){
  						  doLogin();
  					});
					
					var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
					$img.bind( 'load', function()
					{
						$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
					});
					
					
					if( $img[0].width ){ $img.trigger( 'load' ); }
								 
					<!--- Preload images --->
					$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
						
					$("#PreLoadIcon").fadeOut(); 
					$("#PreLoadMask").delay(350).fadeOut("slow");
					
					
					$('.showToolTip').each(function() {
						 $(this).qtip({
							 content: {
								 text: $(this).next('.tooltiptext')
							 },
							  style: {
								classes: 'qtip-bootstrap'
							}
						 });
					 });	 	 
										  							
				});									
			
			
				<!--- Image preloader --->
				$.preloadImages = function() 
				{
					for (var i = 0; i < arguments.length; i++) 
					{
						$("<img />").attr("src", arguments[i]);				
					}
				}
	
				function doLogin()
				{										
					window.location = "ilinks?sk=" + $("#sk").val() + '&lg=1';					
				}
							
				
				<!--- Auto click Login button when press Enter key --->
				function submitenter(myfield,e) {
					var keycode;
					if (window.event) keycode = window.event.keyCode;
					else if (e) keycode = e.which;
					else return true;
									
					<!--- If the parent container is visible react to enter key press as login request  && $('#EBMSignInSection').parent().css("display") != "none"--->
					if (keycode == 13)
					{												
						doLogin();
						return false;
					}
					else
						return true;
				}
								
			</script>

</head>

<body>
   
   
   <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
    
   
     
        <div style="width:100%; height:100%; text-align:center;" align="center">
               			
            <div id="EBMSignInSection<cfoutput>_#SIID#</cfoutput>" class="loginform cf" style="width:550px; height:200px; text-align:left; position:absolute; z-index:15; top:50%; left:50%; margin:-100px 0 0 -150px; padding:25px; border-radius:15px; border:#007ead solid 1px;" align="left"> 
                
                             
                <div class="LoginHeader"><span>Enter Invitation Code</span> <cfoutput>To Find Out More</cfoutput></div>
                 
                <form name="login" action="index_submit" autocomplete="on">
                    <input id="inpSigninMethod" type="hidden" name="method" value="Signin" />
                    
                    <div>
                
                        <div class="LoginContainerMask">
                            <ul>
                                <li>
                                    <label for="sk">Invitation Code</label>
                                    <input type="text" id="sk" name="sk" placeholder="Your invitation code" required onKeyPress="return submitenter(this,event)" autocomplete="on">
                                </li>
                                                                                
                            </ul>
                        </div>
                                                     
                    </div>
                    
                    <div class="LoginContainerMask">
                        <div class="RememberMe"><!---Remember Me<input type="checkbox" required id="inpRememberMe" name="inpRememberMe" style="background:transparent;border:0">---> 
                            
                                <a href="javascript:;" class="loginbutton bluelogin small" id="LoginLink">Login</a>
                                          
                        </div>  
                    
                    </div>
                                     
                </form>
                          
               </div>
          
         
		</div>


</body>
</html>