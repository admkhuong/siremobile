<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Industry Transactions</title>

<cfinclude template="../paths.cfm" >
<cfinclude template="../header.cfm">

<cfoutput>
	<!---Tool tip tool - http://qtip2.com/guides --->
    <link type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.css" />
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/qtip/jquery.qtip.js"></script>
</cfoutput>
        

<style>
		
</style>

<!--- Comparison Chart CSS3 --->
<style>

.table {overflow-x: auto;}
table.pricing {width:auto;}
table.pricing th {padding: 10px 0; color: #999; font: 300 1.154em sans-serif; text-align: center; width:125px;}
table.pricing strong {color: #3f3f3f; font-size: 12px;}
table.pricing sup {position: relative; top: -0.5em; color: #3f3f3f; font-size: 1.2em;}
table.pricing td {color: #3f3f3f; text-align: center; line-height:25px;}
table.pricing td:first-child {color: #999; text-align: left;}
table.pricing td:nth-child(2n+2) {background: #f7f7f7;}
table.pricing tr {margin-bottom: 5px; }
table.pricing tr.action td {padding: 20px 10px; border-bottom-width: 2px;}
table.pricing tr.action td:first-child a {padding-left: 20px; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/more.png") 0 50% no-repeat; color: #3f3f3f;}
table.pricing tr.action td:first-child a:hover {color: #ff8400;}
table.pricing span.yes {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/msg-success.png") 50% 50% no-repeat; text-indent: -50em;}
table.pricing span.no {display: block; overflow: hidden; width: 18px; height: 18px; margin: 0 auto; background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/no.png") 50% 50% no-repeat; text-indent: -50em;}


.tooltiptext{
	display: none;
}
.showToolTip{
	cursor:pointer;
	
}

div.hide-info{
	float:left;
	margin: 0 5px 0 0;
	line-height:25px;
}

div.hide-info:hover{
	opacity:.8;
}

.info-block{
	background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
    left: 100px;
    padding: 6px;
    position: relative;
    top: -10px;
}


</style>


<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/dreamstime_xl_24928711.jpg" + ')' );			
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
			
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 },
				  style: {
					classes: 'qtip-bootstrap'
				}
			 });
		 });	 
	 
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>

<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        	<div id="content" style="position:relative;">
            	
                                               
                <div class="inner-main-hd" style="width:400px; margin-top:30px;">Recent Industry Transactions</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content">
					                	
                </div>
                
                <img style="position:absolute; top:0px; right:50px;" src="#rootUrl#/#publicPath#/images/m1/mobilerevolution.png" width="366" height="291" />
              
            </div>
        </div>
        
        
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                         <h2>Soundbite</h2>
                         <p>Genesys Completes Tender Offer to Acquire SoundBite</p>
                    </header>
                </div>    
                                 

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;">                
               
					<p><strong>DALY CITY, California, </strong><strong>July </strong><strong>2</strong><strong>, 2013</strong> - Genesys Telecommunications Laboratories, Inc. today announced that  the tender offer by its direct wholly-owned subsidiary, Sonar Merger Sub  Inc., to acquire all of the outstanding shares of common stock of  SoundBite Communications, Inc. (NASDAQ: SDBT) expired at 12:00 midnight,  New York City time, on July 1, 2013. All shares that were validly  tendered into the offer and not properly withdrawn have been accepted  for payment and will be paid promptly in accordance with the terms of  the offer.</p>
                    <BR />
                    <p>As previously announced, pursuant to the merger agreement among Sonar  Merger Sub, SoundBite and Genesys, Sonar Merger Sub commenced a tender  offer on June 4, 2013 to acquire all of the outstanding shares of common  stock of SoundBite for $5.00 per share, net to the seller in cash  without interest and less taxes required to be withheld.</p>
                    <BR />
                    <p>The depositary for the tender offer has advised that, as of the  offer&rsquo;s expiration, 16,067,612 shares of common stock of SoundBite have  been validly tendered and not properly withdrawn pursuant to the tender  offer. Those shares represent approximately 97% of the outstanding  shares of SoundBite.</p>
                    <BR />
                    <p>Sonar Merger Sub and SoundBite will promptly complete a &ldquo;short-form&rdquo;  merger under Delaware law, and SoundBite will become a direct  wholly-owned subsidiary of Genesys. The merger is expected to be  completed on or about July 3, 2013. As a result of the merger, any  shares of SoundBite common stock not previously tendered will be  cancelled and shall cease to exist and (other than shares owned by  SoundBite (including treasury shares), Genesys, Sonar Merger Sub, or by  stockholders of SoundBite who have exercised their appraisal rights  pursuant to Section 262 of the Delaware General Corporation Law) will be  converted into the right to receive the same $5.00 per share in cash  paid in the tender offer. Following the merger, SoundBite&rsquo;s common stock  will cease to be traded on The NASDAQ Global Market.</p>
        			
                    <BR />
                    
                    <a href=" http://www.genesys.com/about-genesys/newsroom/news/genesys-completes-tender-offer-to-acquire-soundbite"> http://www.genesys.com/about-genesys/newsroom/news/genesys-completes-tender-offer-to-acquire-soundbite</a>
        		</div>
        
        	</div>
        
        </div>
        
        
         <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                         <h2>Varoli</h2>
                         <p>Nuance buys Varoli which used to be Par3</p>
                    </header>
                </div>    
                                 

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;">
			  	    <p><strong>Burlington, MASS. - October 10, 2013</strong> - Nuance Communications  (NASDAQ: NUAN) today announced that it has signed an agreement to  acquire Varolii, the leading provider of cloud-based outbound customer  engagement solutions.  The Varolii platform is used by many of the  world's largest companies to manage outbound customer service  communications via automated phone, email and text messages, reaching an  estimated one out of every five American adults each year. The  combination of Varolii and Nuance will create a comprehensive  cloud-based solution that brings together the best of inbound and  outbound customer service, resulting in a complete, scalable, secure and  cost-effective platform.  </p>
                    <BR />
                    <p>Nuance&rsquo;s customer service solutions support many of today&rsquo;s largest  brands, including AT&amp;T, Barclay&rsquo;s Wealth &amp; Investment  Management, FedEx, Telstra, T-Mobile, USAA and others. Nuance&rsquo;s  solutions are available across voice, mobile and Web channels, allowing  consumers to engage how, when and where they want, conveniently and  effectively. Today, Nuance&rsquo;s customer service solutions process more  than 12 billion customer interactions each year across more than 6,500  organizations including 75% of the Fortune 100. </p>
                    <BR />
                    <p>The combination of Nuance&rsquo;s world-class customer service solutions  and Varolii&rsquo;s Customer Engagement Cloud will accelerate Nuance&rsquo;s  strategy to transform the customer service experience through  conversations that are engaging, personalized and proactive.  Varolii  will bring a full outbound communication portfolio to the Nuance  OnDemand Customer Service Cloud, which provides inbound hosted IVR  (interactive voice response) and Nina virtual assistant solutions for  mobile and Web applications.  Varolii also will extend Nuance&rsquo;s market  leadership in cloud-based customer service through the addition of over  400 deployments with enterprise organizations, many of which have  implemented complementary solutions from Nuance, including Alaska  Airlines, Southern California Edison, SunTrust, and Time Warner Cable. </p>
                    <BR />
                    <p>This acquisition will strengthen and broaden Nuance&rsquo;s participation  in the growing customer service solutions market.  According to  Forrester Research, 57% of U.S. consumers had unsatisfactory service  interactions in the past 12 months and 80% of customer service  decision-makers say that improving their customer experience is one of  their top two customer service goals.  Forrester buyer surveys indicate  that 25% of enterprises are considering proactive outbound as a key  contact center investment in the next year and that proactive outbound  will enjoy excellent growth rates as more and more enterprises adopt it.  ()</p>
                    <BR />
                    <p>&ldquo;The greatest opportunity to deliver superior service is when you can  anticipate a need and proactively contact the customer in a  personalized and intelligent way,&rdquo; said Robert Weideman, Executive Vice  President and General Manager, Nuance Enterprise Division.  &ldquo;The  combination of Nuance and Varolii not only represents the best of  inbound customer service with the best in outbound customer  communications, but the opportunity to deliver automated yet natural  conversations with customers that are more compelling and effective.&rdquo;</p>
                    <BR />
                    <p>&ldquo;Varolii has been trusted as a leading provider of repeatable  customer engagement applications across six B2C-intensive markets,  enabling leading companies to reduce costs, increase customer reach and  improve customer outcomes,&rdquo; said David McCann, President and Chief  Executive Officer of Varolii.  &ldquo;We're excited to become a part of  Nuance's comprehensive multi-channel vision.&rdquo;</p>
                    <BR />
                    <p>The transaction has been approved by both companies&rsquo; Boards of Directors and is expected to close next week. </p>                
               
                    <p>&nbsp;</p>
                    <p><BR />
                        
                     <a href="http://www.nuance.com/company/news-room/press-releases/Varolii_PressReleaseFINAL_10_10_13.doc">http://www.nuance.com/company/news-room/press-releases/Varolii_PressReleaseFINAL_10_10_13.doc</a></p>
                </div>
        
        	</div>
        
        </div>
          
        <div id="inner-bg-m1" style="padding-top:50px; padding-bottom:50px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                         <h2>Mobile Auth Corp</h2>
                        <p>Additional Info</p>
                    </header>
                </div>    
                                 

			  	<div class="hiw_Info" style="width:auto; margin-top:0px;">
			  	    <p>Mobile Auth Corp - Just starting out in last year. One of my old partners landed here. They got 5 million in funding from First Data for a pure MFA application around ATM and POS transactions. My old partner called me to fulfill via EBM. Currently sending MFA through API calls through EBM through MB.</p>
              
                    <p>&nbsp;</p>
                    <p><BR /> 
                    <a href="http://www.mobileauthcorp.com/company/team.html">Company</a></p>
                </div>
        
        	</div>
        
        </div>
               
        <div style="clear:both;"></div>
        
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	More Tools and support for Omnichannel engagement
                
            </div>
        </div>    
        
        
                        
      
      	<div class="transpacer"></div>
      
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="../act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>