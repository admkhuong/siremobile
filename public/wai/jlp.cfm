<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jeffery Lee Peterson</title>

<cfinclude template="../paths.cfm" >
<cfinclude template="../header.cfm">


<style>
	
	h6
	{
		color: #122664;
		font-weight: bold;
		letter-spacing: 2px;
		position: relative;
		text-transform: uppercase;
		z-index: 1;			
	}
	
	
	<!---.jlpatwork
	{
		
		padding:0;
		border:none;
		height:400px;
		min-height:400px;
		width:100%;
		min-width:1200px;	
		background-size: 100% 100%;
		background-repeat:no-repeat;
		background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/jlpatwork.jpg");
	}--->
	
	
	#bannersection
	{
		
		padding:0;
		border:none;
		margin:0;
		height:400px;
		min-height:400px;
		width:100%;
		min-width:1200px;	
		<!---background-size: 1632px 1224px;--->
		background-size: 100% 1224px;
		background-color:#cc9933;
		background-repeat:no-repeat;
		background-image: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/atwork.jpg");
		background-position: center -350px;		
	}
	
	
	#slideimage 
	{
		background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
		border: 0 solid;
		height: 400px;
		padding: 0;
		text-align: center;
		width: 970px;
	}
	
</style>

<script type="text/javascript" language="javascript">


	$(document).ready(function() 
	{
		var $img = $( '<img src="' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/jlpatwork.jpg" + '">' );
		
		$img.bind( 'load', function()
		{
			$( '#background_wrap' ).css( 'background-image', 'url(' + "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/jlpatwork.jpg" + ')' );		
		});
		
		
		if( $img[0].width ){ $img.trigger( 'load' ); }
					 
		<!--- Preload images --->
		$.preloadImages("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/zenbgdark.png", "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/icons/voice_web2.png","<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/m1/next3dtp3.png");
		
		<!--- Presentation section - Power Point for the web but works --->
		$(".royalSlider").royalSlider({
			<!---// options go here
			// as an example, enable keyboard arrows nav--->
			keyboardNavEnabled: true,
			navigateByClick: false,
			usePreloader: true,
			controlNavigation: 'none',
			autoPlay:
			{
				<!---// autoplay options go gere--->
				enabled: true,
				pauseOnHover: true
			},
			deeplinking: 
			{
				<!---// deep linking options go here--->
				enabled: true,
				prefix: 'slide-'
			},
			loop: true
		});  
			
				
		$("#PreLoadIcon").fadeOut(); 
		$("#PreLoadMask").delay(350).fadeOut("slow");
		
				
  	});
	
	
	<!--- Image preloader --->
	$.preloadImages = function() 
	{
		for (var i = 0; i < arguments.length; i++) 
		{
			$("<img />").attr("src", arguments[i]);				
		}
	}

</script>

</head>
<cfoutput>
    <body>
    
    <div id="background_wrap"></div>
    
    <div id="PreLoadMask">
        <div id="PreLoadIcon">
            <p>LOADING ...</p>
        </div>
    </div>
        
    <div id="main" align="center"> 
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsiteheader.cfm">
        
        <div id="bannersection">
        
        
        <div id="slideimage" class="royalSlider rsDefault" style="">
                        
                        <!--- Stage --->
                        <div class="" data-rsDelay="8000">
                        
                        
                        	<div class="rsABlock" data-move-effect="top" data-delay="1200" style="position:absolute; top:65px; left:90px; height:145px; width:500px; padding:30px 20px 10px 20px; color:##FFF; background-color: ##000033; background: rgba(0, 0, 51, 0.5);  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=##333333,endColorstr=##333333); overflow: hidden; text-align:left;border-radius: 6px 6px 6px 6px; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                           		
                            </div>  
                            
                          <!--- <!--- Left List of Channels--->
                           	
                        
                        	<!--- Left List of Channels--->
                           	<div class="rsABlock" data-move-effect="left" data-delay="1600" style="position:absolute; top:10px; left:40px; height:350px; width:600px; padding:0px 20px 10px 20px; color:##FFF; background:none; overflow: hidden; text-align:left; <!---box-shadow: 10px 10px rgba(0, 0, 0, 0.35);--->" >
                                                        
                                <div class="inner-main-hd" style="width:400px; margin-top:30px;">The #BrandShort# Platform</div>
                
                                <div style="clear:both;"></div>
                                
                                <div class="header-content">
                                    <!---#BrandShort# systems are designed to help you to deliver messaging that is high quality, relevant, personalized, and delivered in context. --->
                                    <h4>SMS</h4>
                                    <h4>Voice / IVR</h4>
                                    <h4>eMail</h4>
                                    <h4>Preference Tools</h4>
                                    <h4>EAI Services</h4>
                                    <h4>Data Services</h4>
                                    <h4>Social Media</h4>
                                    <h4>Print</h4> 
                                    <h4>Radio</h4> 
                                    <h4>Online</h4> 
                                    <h4>Marketing Tools and more...</h4>                                    
                                </div>
                 
                            </div>      --->
                                                                                   
                            <!--- Target Engage Deliver --->                                                        
                            <div class="rsABlock" data-move-effect="right" data-delay="600" style="position:absolute; top:40px; left:100px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<div class="inner-main-hd" style="width:400px; margin-top:30px; color:##FFFFFF;">Jeffery Lee Peterson</div>                               
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1200" style="position:absolute; top:115px; left:100px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<div class="header-content" style="width:560px; color:##FFFFFF; font-size:20px;">CTO, Programmer, Technology Savant.</div>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1600" style="position:absolute; top:140px; left:100px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<div class="header-content" style="width:560px; color:##FFFFFF;">4685 MacArthur Ct STE 250 Newport Beach, CA 92660</div>                                
                            </div>
                            
                            <div class="rsABlock" data-move-effect="right" data-delay="1600" style="position:absolute; top:160px; left:100px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<div class="header-content" style="width:560px; color:##FFFFFF;"><a href="mailto:jp@ebmui.com" style="color:##CCC; margin-right:20px;">jp@ebmui.com</a><a href="tel:9494000553" style="color:##CCC">(949) 400-0553</a></div>                                
                            </div>
                            
                             <div class="rsABlock" data-move-effect="right" data-delay="1600" style="position:absolute; top:185px; left:100px; padding:20px; color:##FFF; overflow: hidden;" >
                           		<div class="header-content" style="width:560px; color:##FFFFFF;">                                
                               		<a href="http://www.linkedin.com/in/jefferyleepeterson" style="color:##CCC; margin-right:20px;">      
          								<img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_profile_greytxt_80x15.png" width="80" height="15" border="0" alt="View Jeff Peterson's profile on LinkedIn"><span style="margin-left:20px;">Connect with me!</span>
        							</a>                    
    							</div>
                           	</div>                           
                            
        				</div>
                                        	                       
                	</div>       
                    
                    
        	<!---<div id="content" style="position:relative;">            	
                                                    
                <div class="inner-main-hd" style="width:500px; margin-top:30px;">Jeffery Lee Peterson</div>
                
                <div style="clear:both;"></div>
                
                <div class="header-content" style="width:560px;">
 				CTO, Programmer, Technology Savant.     
                
                <BR />
                
                4685 MacArthur Ct STE 250 Newport Beach, CA 92660
                <a href="mailto:jp@ebmui.com">jp@ebmui.com</a>
                <a href="tel:9494000553">(949) 400-0553</a>
                </div>
           
        <!---    	<img style="position:absolute; top:0px; right:10px; " src="#rootUrl#/#publicPath#/images/m1/jlpprofile.png" width="287px" height="409px" />
         --->     
            </div>--->
        </div>

		<div id="inner-bg-m1" style="padding-bottom:0px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-full">
                         <h2>Technology Executive / CTO</h2>
                         <p>Extensive expertise in data driven applications.</p>
                    </header>
                </div>     
	
          </div>
        </div>   
                
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                
                <div style="float:left;">
                	<img style="" src="#rootUrl#/#publicPath#/images/m1/jlpatwork250.png" width="250px" height="188px" />
                </div>
                
                <div style="margin: 100px 0 0 25px; float:left; width:540px" align="left">
                
	                <h2 style="color: ##0085c8;">Jeffery Lee Peterson</h2>
                    <BR />
                	Technology Executive / CTO. Extensive expertise in the development of data driven applications for business, retail, utilities, healthcare.
                    <BR/>
                    
                    <BR />
                    As an Application architect, Jeff has faced the challenges of keeping up with social, mobile and big data applications as business demands increase. He has successfully met these demands by identifying, building and delivering long-lasting apps while reducing risk, complexity and cost.
                    
                    <BR />
                    <BR />
                    Over the last 13 years, Jeff has successfully executed messaging campaigns for 1000's of companies, including some of the biggest names in Banking, Telecomm, Healthcare, Energy, and more.
                    <BR/>
                    <BR />
                    While solving the needs of Fortune 500 companies, Jeff has been able to add innovative new tools and improve the Event Based Messaging (EBM) technology platform making it faster, better, cheaper, more secure, and more reliable.  
                </div>
                               
            </div>
        </div>
               
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix" style="margin-bottom:0 !important;">
                    <header class="section-title-inner-wide">
                         <h2>Education</h2>
                         <p>University of California Riverside</p>                         
                    </header>                   
                </div>    
				
                <p style="color: ##888888; letter-spacing: 2px; position: relative; text-transform: uppercase;">Combo Bachelors Degree in Physics and Computer Science<p>                    
                                             
            </div>
        </div>   
               
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                                
                <div class="section-title-content clearfix" style="margin-bottom:0 !important;">
                    <header class="section-title-inner-wide">
                         <h2>Your Ideal Technology Partner</h2>
                         <p>Senior Executive with Broad set of Skills</p>                         
                    </header>                   
                </div>    
                
                                  
                     
                <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                   
                    <ul>
                		<li><b>Enterprise Application Integration (EAI)</b> </li>
						<li><b>Compliance - GLBA, PCI, HIPPA, SAS 70->SSAE 16</b></li>
						<li><b>Technology Development</b> </li>
						<li><b>Emerging Technologies</b> </li>
						<li><b>Team Leadership</b> </li>
						<li><b>Project/Program Management</b> </li>
						<li><b>Development Methodologies - Spiral, Agile</b> </li>
						<li><b>Budget Management</b> </li>
						<li><b>Process Engineering</b> </li>
                        <li><b>Reverse Engineering</b> </li>
						<li><b>Scalability</b> </li>
                        <li><b>Firewall, VPN, IPS/IDS</b></li>
                        <li><b>DNS, SSL, Web Services</b></li>
                        <li><b>API, REST, XML, JSON, HTTP(s), Code Libraries, Anyone/Anywhere</b></li>
                        <li><b>Load Balancing</b></li>
                        <li><b>Troubleshooting</b></li>
                        <li><b>Public or Private Cloud</b> </li>
                        <li><b>Social Media</b> </li>
                        <li><b>NOC Monitoring</b> </li>
                        <li><b>QA</b> </li>
                        <li><b>Multilingual Programmer - C++, Java, CSS, HTML, Coldfusion, .NET, </b></li>
                        <li><b>DBA / Programmer / Developer mySQL, SQL Server, Oracle</b> </li> 
                        						
                    </ul>      
                                     
              	</div>
                             
            </div>
        </div>
    <!---              
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Professional Experience</h2>
                         <p>Acting CTO and Owner of my own platform.</p>
                    </header>
                </div>    
              	
                0-6,000,000 Started with an idea and took from incubation to full production. For the last 13 years I have been a consulting CTO and extending my messaging application tool set. 
                             
            </div>
        </div>
        
        
        <div id="inner-bg-m1" style="padding-bottom:25px;">
        
            <div id="contentm1">
                                        
                <div class="section-title-content clearfix">
                    <header class="section-title-inner-wide">
                         <h2>Know what makes the tech tick</h2>
                         <p>What lies beneath the surface</p>
                    </header>
                </div>    
              	  
                             
            </div>
        </div>
        
        
      --->
               
        <div class="transpacer">
        	<div class="inner-transpacer-hd">
            
            	Tools and support for Omni-Channel engagement
                
            </div>
        </div>         
        
        <div id="inner-bg-m1" style="padding-top:50px;">
        
            <div id="contentm1">
                                                   
                <div class="section-title-content clearfix">
                    <header class="section-title-inner">
                        <h2>My History</h2>
                        <p>Learn, Build, Explore, Grow</p>
                    </header>
                </div>                    
                                        
                <div id="inner-box-left" align="center" style="margin-left:60px;">          
                    
                    <div class="inner-txt" style="margin-top:25px !important; width:900px;">                    	
                    
                    	<h6>Humble Beginnings</h6>
                        <p>When I was twelve years old, my dad bought me my first computer - a TRS80 Color Computer and a book on programming in Basic. In those days video games were just hitting their stride. I programmed my first game (Tron Light Cycles) using Basic. I would go on to build all sorts of computers from the ground up just for fun projects. As a teen I supplemented my income building hot-rod computers for engineers in aerospace.</p>
                        
                        <BR />
                        <BR />
                        
                        <h6>College Years - University of California Riverside</h6>
                        <img style="float:right; " src="#rootUrl#/#publicPath#/images/m1/ucr_logo.png" width="340px" height="342px" />
                        <p>Going to college, I initially went in for a Physics degree. I wanted to be a real rocket scientist. I had a summer internship lined up with TRW in my junior year but it was cancelled due to the recent peace dividend of the times. "Uh-oh" I thought, time to formalize some of my programming skills. I ended up with a combo degree in Physics and Computer Science from the University of California Riverside in 1992.</p>
                        <p>Paying my own way through college I worked a lot in the automotive industry. I wrote various applications for sales lead tracking and financial calculators. When I got out of college I continued in the automotive industry. I worked my way up from lot boy as a teenager, to sales, to sales manager, to finance manager, and then into service as a tech writer up through to service manager.</p>
                        
                        <BR />
                        <BR />
                        
                        <h6>Secret to Success</h6>
                        
                        <p>A few of years after I had graduated, a programming opportunity at TRW Ballastic Missle Division came open.
                        <img style="float:left; margin-right:15px; " src="#rootUrl#/#publicPath#/images/m1/trw.png" width="132px" height="45px" />
                        At a third of my current income, it was a tough decision, but I wanted to follow what I loved to do. I had confidence that I could showcase my skills and work my way up. I wrote a source code analyzer for ADA and Fortran and programmed the GUI for a Radar simulator in C++ and GPL. I also worked on a data integration project to tie disparate healthcare information systems into a system to display a centralized view of the patient.
                        <img style="float:right; margin-left:15px; " src="#rootUrl#/#publicPath#/images/m1/logicon.png" width="164px" height="43px" />
                        I moved on to Logicon RDA a division of Northrop. There I worked as the ETL guru for a new project for Los Angeles County called Welfare Fraud Linkage Analysis Database System (WFLADS). In WFLADS I wrote a bunch of C++ code for data extraction, transformation, and loading tools to move data from DMV and other public and private data systems into a centralized data warehouse to look for discrepancies in the Department of Social Services payouts. I also worked in the secure vault on old HP1000 Missle launch computers. They would lock me in the vault with a launch computer and see if I could hack it with the console buttons. I gained full control of the system via machine language input registers and could have my way with the computer. I also had the pleasure of extracting old code from Paper Tape to floppy disks.</p>
                        
                        <BR />
                        <BR />
                        
                        <h6>The Dot Com Boom and Bust</h6>
                        <p>Seeing some of my friends move off into the dot com world with good success, I decided to follow. At Global Link Securities I picked up technology skills
                        <img style="float:left; margin-right:15px; " src="#rootUrl#/#publicPath#/images/m1/globallink.png" width="221px" height="63px" />
                        while working on web based tools for real time stock quotes and alerts. I also learned about dealing with people from over seas. 
                        <img style="float:right; margin-left:15px; " src="#rootUrl#/#publicPath#/images/m1/abt.png" width="155px" height="42px" />
                      	At Autobytel I continued my data driven web based application experience. I worked on many marketing projects including setting up an email delivery system that delivered more than 1,000,000 emails per half hour. This is where I gained exposure to Coldfusion. I also learned new DB skill while working on a data warehouse project.</p>
                        
                        <BR />
                        <BR />
                        
                        <h6>ReactionX - What I have been doing since the year 2001 to present</h6>
                        <p>While at Globalink Securities I met another businessperson who had just sold out of a bio-metrics company. He talked me into starting a new company for messaging. I thought I would try my hand at being an entrepreneur. We formed ReactionX, LLC in the year 2001 with me as the Chief Technology Officer. In 2002 I bought out my only partner so he could pursue other interests.</p>
                        <img style="float:left; margin-right:15px; " src="#rootUrl#/#publicPath#/images/m1/mblogo.png" width="258px" height="46px" />
                       
                        <p>Through ReactionX I have gained experience in all ends of the business world. Sales Engineer, CTO, Developer, Network Operations, Financials, Sales, Marketing, Security and Compliance. </p>
                        
                        <p>Over the last 13 years, I have successfully executed messaging and marketing campaigns for 1000's of companies, including some of the biggest names in Banking, Telecomm, Healthcare, Energy, and more. </p>
                        <div style="clear:both;"></div>
                        
                        <!--- babblesphereicon46x75web.png --->
                        <img style="float:right; margin-left:15px;" src="#rootUrl#/#publicPath#/images/m1/babblescologo.png" width="177px" height="58px" />
                        
                        <img style="float:right; margin-left:15px; " src="#rootUrl#/#publicPath#/images/m1/logo-ebm-trans.png" width="145px" height="50px" />
                       	While solving the needs of Fortune 500 companies, I have created innovative new tools and improved the Event Based Messaging (EBM) technology platform making it faster, better, cheaper, secure, and reliable.
                        
                        
                        <div style="clear:both;"></div>
                                                                       
                        <h6>Experience with employee teams as well as off shore and on shore consultants.</h6>
                        <p>USA, Vietnam and India</p>

                        <BR />
                        <BR />
                        
                        <h6>Work hard play hard</h6>    
                        <p>I have an 80 Acre spread in Lucerne Valley where I like to go to relax and off-road and play with toys. I have a house in Big Bear on the lake.</p>
                        <p>I also ride mountain bikes, play basketball, poker, and golf.</p>               
                        
                        <BR />
                        <BR />
                                    	  
                  	</div>                
                
                </div>                   
                             
            </div>
        </div>
        
        
        <!---	
				<p style="color: ##888888; letter-spacing: 2px; position: relative; text-transform: uppercase;">(But we have all worked with that person...)<p>                    
                
                 <div class="hiw_Info" style="width:960px;  margin-top:0px;">
                   
                    <ul>
                		<li><b>800+ Credit Score</b></li>
                        <li><b>No bad habits - Pee in a cup is always clean.</b></li>  
                        <li><b>No young children</b> </li>
						<li><b>Non-Smoker</b></li>
						<li><b>Excellent Health - Blood Pressure/Sugar Good</b></li>
                        <li><b>No disabilities</b></li>
                        <li><b>Gets along with everybody</b></li>   
                        <li><b>No Litigation History</b></li>
                        <li><b>No Criminal Record</b></li>                                                                    
                    </ul>      
                                     
              	</div>
				--->
                
              
                 
      	<div class="transpacer" style="height:500px;"></div>
        
        <!--- EBM site penultimate footer included here --->
        <cfinclude template="../act_ebmsitepenultimatefooter.cfm">
      
        
        <!--- EBM site footer included here --->
        <cfinclude template="../act_ebmsitefooter.cfm">
    </div>
    </div>
    </body>
</cfoutput>
</html>

