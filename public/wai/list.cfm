<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>List Users</title>
</head>

<cfinclude template="../paths.cfm" >

<!--- Add Basic Account Security Here ... --->

<cfparam name="KeyMaster" default="0" />

<cftry>
 <!--- Decrypt out remember me cookie. --->
    <cfset LOCALOUTPUT.EBMKM = Decrypt(
		COOKIE.EBMKM,
		APPLICATION.EncryptionKey_Cookies,
		"cfmx_compat",
		"hex"
		) />
    <!---
	For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
	of list. Extract it from the list.
	--->
    <cfset KeyMaster = ListGetAt(
		LOCALOUTPUT.EBMKM,
		3,
		":"
		) />
<cfcatch type="any">

	<cfset KeyMaster = 0 />

</cfcatch>
	
</cftry>


<cfif KeyMaster EQ 0 >
	<cflocation url="gen?msg=#URLEncodedFormat('Invalid Key - Please Try Again (code=1)')#" />
</cfif>

<style>



</style>

    <body>
    
		<cfoutput>
        
            <cfquery name="GetMemberInformation" datasource="#Session.DBSourceEBM#">
               SELECT 
                    PKId_int,
                    SimpleKey_vch,
                    FirstName_vch,
                    LastName_vch,
                    SignedNDA_int,
                    CompanyName_vch,
                    Created_dt,
                    LastLogIn_dt,
                    LogInCount_int
                FROM 
                    simpleobjects.simplekeys
                
                ORDER BY 
                    LastName_vch DESC           
            </cfquery>    
            
           
          <!--- <cfdump var="#GetMemberInformation#" /> --->
            
           <table border="1" cellpadding="3px" cellspacing="2px"> 
           
           <tr>
           		<th>Id</th>
                <th>Simple Key</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>NDA Signed</th>
                <th>Company Name</th>
                <th>Created</th>
                <th>Last Logged In</th>
                <th>Login Count</th>               
           </tr>
           
           <cfloop query="GetMemberInformation" >
            
            
          		
                	
               <tr>
                    <td style="width:50px; overflow:scroll;">#GetMemberInformation.PKId_int#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.SimpleKey_vch#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.FirstName_vch#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.LastName_vch#</td>
                    <td style="width:20px;  overflow:scroll;">#GetMemberInformation.SignedNDA_int#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.CompanyName_vch#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.Created_dt#</td>
                    <td style="width:150px; overflow:scroll;">#GetMemberInformation.LastLogIn_dt#</td>
                    <td style="width:20px; overflow:scroll;">#GetMemberInformation.LogInCount_int#</td>
               </tr>
            
            	                
            
           </cfloop>
           
           </table> 
                                     
        
        </cfoutput>


	</body>
</html>