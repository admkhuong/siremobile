
<cfoutput>
	<cfset thisdate = #url.d#>
	<cfset list = #url.l#>
	<div style="float:left; padding:10px" >
		<cfquery name="getCampaignStatsSummary" datasource="MBASPSQL2K">
			SELECT 
				'progress' as Status 
				,sum(inProgressCount_int) as Cnt
			FROM rxservers..rx_dialersummary  (nolock)
			WHERE CONVERT(VARCHAR(10), #thisdate#, 102) = CONVERT(VARCHAR(10), date_dt, 102) and Batchid_num IN (#list#)
		
			union 
			
			SELECT 
				'queue' as Status 
				, sum(scheduledCount_int) as Cnt
			FROM rxservers..rx_dialersummary  (nolock)
			WHERE CONVERT(VARCHAR(10), #thisdate#, 102) = CONVERT(VARCHAR(10), date_dt, 102) and Batchid_num IN (#list#)
			
			union 
				   
			SELECT 
				'called' as Status 
				,sum(CompleteCount_int) as Cnt
			FROM rxservers..rx_dialersummary  (nolock)
			WHERE CONVERT(VARCHAR(10), #thisdate#, 102) = CONVERT(VARCHAR(10), date_dt, 102) and Batchid_num IN (#list#)
		
		</cfquery>		
		
		<cfchart format="flash" 
			xaxistitle="TOTAL NUMBER OF DIALS TODAY" 
			yaxistitle="## OF DIALS" 
			show3d="yes" 
			fontBold="yes"
			chartWidth="450" 
			chartheight="250"
			fontSize = "12"> 
		
			<cfchartseries type="bar" 
				query="getCampaignStatsSummary" 
				itemcolumn="Status" 
				valuecolumn="Cnt" 
				paintStyle="shade">
				
				<cfif TIMEFORMAT(NOW(),"HH:mm") GT "08:00">
					<cfloop query="getCampaignStatsSummary">
						<cfchartdata item="#getCampaignStatsSummary.Status#" value="#getCampaignStatsSummary.Cnt#">
					</cfloop>
				<cfelse>
					<cfchartdata item="called" value="0">
					<cfchartdata item="queue" value="0">    
					<cfchartdata item="progress" value="0">    
				</cfif>    
				   
		
			</cfchartseries>
		</cfchart> 
	</div>
	
	<div style="float:right; padding:5px"></div>
		<cfquery name="last8DaysP" datasource="MBASPSQL2K">
			SELECT top 8
				sum(CompleteCount_int) as Cnt
				,CONVERT(VARCHAR(5), date_dt, 1) as dt
				,DATENAME(dw,date_dt) as dy
			FROM rxservers..rx_dialersummary  (nolock)
			WHERE  Batchid_num IN (#list#)
			group by CONVERT(VARCHAR(5), date_dt, 1),DATENAME(dw,date_dt)
			order by dt desc
		</cfquery> 
		<cfset totalCnt = 0>
		<cfloop query="last8DaysP">
			<cfset totalCnt = totalCnt+Cnt>
		</cfloop>
		<cfset avgCntDays = (totalCnt/8)>
		<cfquery name="last8Days" dbtype="query">
			select * from last8DaysP order by dt
		</cfquery>
		
		
		<cfchart format="flash" 
			xaxistitle="DAYS" 
			yaxistitle="## OF DIALS" 
			show3d="no" 
			fontBold="yes"
			chartWidth="600" 
			chartheight="250"
			fontSize = "12"> 
		
			<cfchartseries type="line" 
				paintStyle="shade">
				
				<cfif TIMEFORMAT(NOW(),"HH:mm") GT "08:00">
					<cfloop query="last8Days">
						<cfchartdata item="#last8Days.dt#[#left(last8Days.dy,2)#]" value="#last8Days.Cnt#">
					</cfloop>
				</cfif>    
			</cfchartseries>
			
			<cfchartseries markerstyle="circle" seriescolor="##EFEFEF" type="line" 
				paintStyle="light">
				<cfif TIMEFORMAT(NOW(),"HH:mm") GT "08:00">
					<cfloop query="last8Days">
						<cfchartdata item="#last8Days.dt#[#left(last8Days.dy,2)#]" value="#avgCntDays#">
					</cfloop>
				</cfif>    
			</cfchartseries>
		</cfchart> 
	</div>
	<div style="clear:both"></div>
	<div align="center">
		<cfquery name="getCampaignStatsDetail" datasource="MBASPSQL2K">
			SELECT TOP 15  sum(CompleteCount_int) as Cnt,
				BatchDesc_vch,
				userid_int
			FROM rxservers..rx_dialersummary (nolock)
			WHERE CONVERT(VARCHAR(10), #thisdate#, 102) = CONVERT(VARCHAR(10), date_dt, 102) and Batchid_num IN (#list#)
			GROUP BY BatchDesc_vch,userid_int
			ORDER BY Cnt desc, BatchDesc_vch
		</cfquery>
		
		 <cfchart format="flash" 
			xaxistitle="TOP 15 CAMPAIGNS TODAY" 
			yaxistitle="Dials" 
			show3d="yes" 
			fontBold="yes"
			chartWidth="1000" 
			chartheight="325"
			fontSize = "12"> 
		
			<cfchartseries type="bar" 
				itemcolumn="BatchDesc_vch" 
				valuecolumn="Cnt" 
				paintStyle="shade">
	
				<cfloop query="getCampaignStatsDetail">
					<cfchartdata item="#BatchDesc_vch#" value="#Cnt#">
				</cfloop>
			  
			</cfchartseries>
		</cfchart>  
	</div>
</cfoutput>