<cfset display = 1/>
<cfset internal = 0>
<cfset ccount = 0/>
<cfparam name="startDate" default="#dateformat(now(),'yyyy/mm/dd')# 00:00:00"/>
<cfparam name="endDate" default="#dateformat(now(),'yyyy/mm/dd')# 23:59:59"/>

<cfparam name="startDateTimeMain" default="#DateFormat(dateadd('m',-1,NOW()),'mm/dd/yyyy')#">
<cfparam name="startDateTimeMainT" default="00:00">

<cfparam name="endDateTimeMain" default="#DateFormat(NOW(),'mm/dd/yyyy')#">
<cfparam name="endDateTimeMainT" default="#TimeFormat(NOW(),'HH:mm')#">

<cfset thisDate = startDate>
<cfset thisDateStop = endDate>

<!--- to find if user preference is setup already for main reporting --->
<cfquery name="checkMainReporting" datasource="#session.DBSourceEBM#">
    SELECT 	batchid_bi
    FROM	simpleobjects.reportingmainpref
    WHERE	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
    ORDER BY order_int 
</cfquery>

<cfset BatchIdList = ValueList(checkMainReporting.batchid_bi,'_') />

<cfoutput><span style="display:none;" id="batchListSpanMain">#BatchIdList#</span></cfoutput>
<cfset batchdesc_vch = 'test campaign description'>
<link href="../../public/css/style.css" rel="stylesheet" type="text/css"/>


<cfset code = 1>

<style type="text/css">

	#SBContentsR {
		top:0px;
		left:0px;	
		width:1250px;
		height: 1380px;
		min-height: 1380px;
	}
	
	#SBMenuBar {
		position:absolute;
		top:0px;
		left:0px;	
		width:1250px;
		height: 80px;
		min-height: 80px;
		border:#000 2px solid;
	}
	
	
	.SBMenuItem{
		position:relative;
		display:inline;
	}
	
	.menuItem{
		border:solid 1px black;	
		padding: 2px 5px 2px 5px;
		cursor:move;
		background-color:#333;
		color:#fff;
		font-size:10px;
		font-weight:bold;
	}
	.droppableLeft{
		height:300px;
		width:49%;
		float:left;
		border:solid 1px #999999;	
	}
	.droppableRight{
		height:300px;
		width:49%;
		float:right;
		border:solid 1px #999999;	
	}
	#draggable{
		margin:5px 0px 0px 0px;
		text-align:center;
	}
	span {
		font-size: 12px;
	}
	
<!---	dt.ui_tpicker_time_label{
		display:inline !important;
	}
	dd.ui_tpicker_time{
		display:inline !important;
		color:#333;
		font-weight:bold;
		margin-left:5px;
	}
	.ui-datepicker { width: 12em; height:22em; padding: 0; display: none; border-color: #DDDDDD; }
	.ui-datepicker table {font-size: 0em;}
	.ui-datepicker td { padding: 0px; }
	--->
	<!---.ui-dialog {
		overflow: hidden;
	}
	.ui-dialog:hover {
		overflow-y: scroll;
	}--->
	
</style>

<!---<script type="text/javascript">
	$('.ui-dialog').bind('hover', function() {
		alert('alert');
		alert($('.ui-icon-closethick').css('margin-right'))
	  	$('.ui-icon-closethick').css({'margin-right':'10px'});
		alert($('.ui-icon-closethick').css('margin-right'))
	});
</script>--->





<!---- set user'd prefs if not there--->
<!---<cfloop list="#batchidlist#" index="bid">--->
	<cfset bid = -1>
    <cfquery name="getPref" datasource="#Session.DBSourceEBM#">
          SELECT UserReportingPref_id
              ,UserId_int
              ,BatchId_bi
              ,ReportId_int
              ,QuadarantId_int
              ,DateUpdated_dt
          FROM simpleobjects.userreportingpref
          WHERE userid_int = #session.userid# 
          		and batchid_bi = #bid#
    </cfquery>
    
    <cfif getPref.recordcount eq 0>
        <cfquery name="insertDefaultPref" datasource="#Session.DBSourceEBM#">
            INSERT INTO simpleobjects.userreportingpref(userid_int,batchid_bi, ReportId_int, QuadarantId_int,dateupdated_dt)
                SELECT #session.userid#,#bid#,1,1,now()
                	UNION
                SELECT #session.userid#,#bid#,2,2,now()
                	UNION
                SELECT #session.userid#,#bid#,3,3,now()
                	UNION
                SELECT #session.userid#,#bid#,4,4,now()
        </cfquery>
    </cfif>
<!---</cfloop>--->
<!---- END:set user's prefs if not there --->

<!---- get user's prefs --->
 <cfquery name="getPref" datasource="#Session.DBSourceEBM#">
      SELECT UserReportingPref_id
          ,UserId_int
          ,BatchId_bi
          ,ReportId_int
          ,QuadarantId_int
          ,DateUpdated_dt
      FROM simpleobjects.userreportingpref
      WHERE userid_int = #session.userid# 
            AND batchid_bi IN (#bid#)
</cfquery>
<!--- END:get user's prefs --->



<cfoutput>
	<cfset batchid = -1>
<script>
	<!---$(document).ready(function() {--->
		function makeQuad()
		{
			//alert('now');
	<!---<cfloop list="#BatchIdList#" index="batchid">--->
			for(var i = 1 ; i <= 8 ; i++)
			{
				$( "##menuItem_#batchid#_"+i ).draggable({ 
					revert: "invalid", 
					containment: "##SBContentsR", 
					opacity: 0.7, 
					zIndex: 2700,
					helper: "clone",
					cursor: "move",
					start: function(event, ui) { 
						$('.droppable').css({'border':'solid 2px red'}) 
						
					},
					stop: function(event, ui) { 
						$('.droppable').css({'border':'solid 1px ##999999'}) }
					});
			}
			
			$("div.droppable").droppable({
				over: function(event, ui) { 
					$(this).css({'border':'solid 2px green','box-shadow':'0 0px 10px ##777','-webkit-box-shadow':'0 0px 10px ##777','-moz-box-shadow':'0 0px 10px ##777'});
				},
				out: function(event, ui) { 
					$(this).css({'border':'solid 1px ##999999','box-shadow':'0 0px 0px ##fff','-webkit-box-shadow':'0 0px 0px ##fff','-moz-box-shadow':'0 0px 0px ##fff'});
				},
				drop: function(event, ui ) {
					var reportType = new Array();
					var quadarant = new Array();
					reportType = ui.draggable.attr('id').split("_");
					quadarant = $(this).attr('id').split("_");
					
					if(parseInt(reportType[1]) == parseInt(quadarant[1]))
					{
						<!---- change pref and the values in JS var on this page --->
						$.getJSON("../cfc/reporting.cfc?method=changePref&batch_id="+parseInt(reportType[1])+"&reportno="+reportType[2]+"&Quadno="+quadarant[2]+"&returnformat=json&queryformat=column", {}, function(res,code) {
							if(parseInt(res.DATA.ID) == 1)
							{
								eval('var_1_'+quadarant[2] + '='+reportType[2]);
								//or we can use,//window['var_'+reportType[1]+'_'+quadarant[2]] = reportType[2];
							}
							
						});
						
						
						//alert($('##report_'+parseInt(reportType[1])+'_'+reportType[2]).html());
						
						$(this).addClass( "ui-state-highlight" ).html($('##report_'+parseInt(reportType[1])+'_'+reportType[2]).html());
						$(this).css({'border':'solid 1px ##999999','box-shadow':'0 0px 0px ##fff','-webkit-box-shadow':'0 0px 0px ##fff','-moz-box-shadow':'0 0px 0px ##fff'});
					}
					else
					{
						alert('Wrong quadrant selected \nPlease drag to its specific quadrant');	
					}
				}
			});
	<!---</cfloop>--->
		}
<!---
});--->
</script>
</cfoutput>

<!--- have to create JS vairable delimited by _ by using coldfusion QOQ and then parse them outr to display correct quad for report when we get charts from ajax.cfc --->
<cfquery name="getbatchtofill" dbtype="query">
    select  BatchId_bi,ReportId_int,QuadarantId_int,DateUpdated_dt
    from	getPref
    where 	userid_int = #session.userid#
</cfquery>

<script type="text/javascript">
	<cfoutput query="getbatchtofill">
		<cfset thisString = ReportId_int />
		var #toScript(thisString, 'var_1_'&QuadarantId_int)#
	</cfoutput>
</script>
            
<script type="text/javascript">
	var l;
	var lo;
    var a;
	var ao;
	var dsc, ciq, cip, tc, mlr, imageType, tableDet,r1,r2,r3,r4,r5,r6,r7,r8;
	function callList()
	{
		l = document.getElementById('batchListSpanMain').innerHTML;
	    a = l.split(",");
	  	setTimeout("ajaxcall(0)",1000);
		makeQuad();
	}
	function callListOld()
	{
		lo = document.getElementById('blo').innerHTML;
		ao = lo.split(",");
		setTimeout("ajaxcallOld(0)",1000);
	}
	
	
	$(document).ready(function() {
	  <cfoutput>
	  		$("##startDateTimeMainT").timepicker();
			$("##endDateTimeMainT").timepicker();
				
			$('##startDateTimeMain').datepicker({
				numberOfMonths: 1,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy'
			});
			$('##endDateTimeMain').datepicker({
				numberOfMonths: 1,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy'
			});
		
			<!---$("##endDateTime").datetimepicker({
				ampm: true,
				timeFormat: 'h:mm TT'
			});--->
	  </cfoutput>
	  $('#totalProblemToday').html(parseInt($('#totalProblemToday').text()) + parseInt($('#probCounterToday').text()));
	  //$('#totalProblemY').html(parseInt($('#totalProblemY').text()) + parseInt($('#probCounterY').text()));
	  
	  if(document.getElementById('completedTodayCount'))
	  	$('#completePast').html(parseInt($('#completedTodayCount').text()));
	  
	  jQuery.fn.exists = function(){return this.length>0;}

	  callList();
	  
	});
	
	function ajaxcall(counter)
	{
		
		if(a.length > counter)
		{

			$('#mainrow'+a[counter]).fadeTo(400,0.4);
			$('#detailTableInnerImg'+a[counter]).fadeTo(400,0.1);
			$('#trinner'+a[counter]).css("background-image", "url(../../public/images/loading.gif)");  
			document.getElementById('img'+a[counter]).src = '../../public/images/loading-small.gif';
			
			$('#img'+a[counter]).fadeTo(100,1);
			
			$("#dsc"+a[counter]).html('');
			$("#ciq"+a[counter]).html('Refreshing');
			$("#cip"+a[counter]).html('Data');
			$("#tc"+a[counter]).html('');
			$("#mlr"+a[counter]).html('');
			
			<cfoutput>
				var start_Dt = $("##startDateTimeMain").val()+' '+$("##startDateTimeMainT").val();
				var end_Dt = $("##endDateTimeMain").val()+' '+$("##endDateTimeMainT").val();
			</cfoutput>
			
			$.getJSON("../cfc/reporting.cfc?method=getcategories&batch_id="+a[counter]+"&sdt="+start_Dt+"&edt="+end_Dt+"&returnformat=json&queryformat=column", {}, function(res,code) {
				var bid = parseInt(res.DATA.ID);
				var dsc = parseInt(res.DATA.TOTALCALLS);
				var ciq = parseInt(res.DATA.TOTALQUEUE);
				var cip = parseInt(res.DATA.TOTALINPROGRESS);
				var tc  = parseInt(res.DATA.COMPLETECALLS);
				var mlr = parseFloat(res.DATA.MLR);
				var tableDet = res.DATA.DETAILTABLE.toString();
				var r1 = res.DATA.REPORT1.toString();
				var r2 = res.DATA.REPORT2.toString();
				var r3 = res.DATA.REPORT3.toString();
				var r4 = res.DATA.REPORT4.toString();
				var r5 = res.DATA.REPORT5.toString();
				var r6 = res.DATA.REPORT6.toString();
				var r7 = res.DATA.REPORT7.toString();
				var r8 = res.DATA.REPORT8.toString();
				
				
				<!---var res = xmlhttp.responseText;
				res = res.replace(new RegExp("[<||\>]","gi"),"");
				document.getElementById("charts").innerHTML = res;--->

				//alert(r1);
				
				if(dsc==tc)
					document.getElementById('mainrow'+a[counter]).style.backgroundImage = 'url("../../public/images/grayG.png")';
				else
					document.getElementById('mainrow'+a[counter]).style.backgroundImage = 'url("../../public/images/greenG.png")';
				
				$("#dsc"+a[counter]).html(dsc);
				$("#ciq"+a[counter]).html(ciq);
				$("#cip"+a[counter]).html(cip);
				$("#tc"+a[counter]).html(tc);
				$("#mlr"+a[counter]).html(mlr+" <b>%</b>");
				var idi = 'detailTableInner'+a[counter];
				//document.getElementById(idi).innerHTML = tableDet;
				
				for(var d=1;d<=8;d++)
				{
					if ($('#report_'+a[counter]+'_'+d).exists()) 
					{
						$('#report_'+a[counter]+'_'+d).html(eval('r'+d))
						if(d<5)
							$('#droppable_'+a[counter]+'_'+d).html(eval('r'+eval('var_1_'+d)));
					}
					else
					{
						var divTag = document.createElement("div");
						divTag.id = 'report_'+a[counter]+'_'+d;
						//divTag.setAttribute("align","center");
						divTag.style.display = "none";
						//divTag.className ="dynamicDiv";
						divTag.innerHTML = eval('r'+d);
						//alert("r"+d);
						//alert(divTag.id);
						document.body.appendChild(divTag);	
						if(d<5)
							$('#droppable_'+a[counter]+'_'+d).html(eval('r'+eval('var_1_'+d)));
					}
				}
				
				
				$('#mainrow'+a[counter]).fadeTo(400,1);
				$('#detailTableInnerImg'+a[counter]).fadeTo(400,1);
				$('#trinner'+a[counter]).css("background-image", "none");  
				$('#img'+a[counter]).fadeTo(100,1);
				
				if(document.getElementById("detailrow"+a[counter]).style.display == '')
					document.getElementById('img'+a[counter]).src = '../../public/images/down.png';
				else
					document.getElementById('img'+a[counter]).src = '../../public/images/up.png';
				
				counter+=1;
				setTimeout("ajaxcall("+counter+")",100);
			});
		}
		else
		{
			
			//callCharts();
			//setTimeout("ajaxcall(0)",300000); //5 mins refresh
		}
	}
</script>


<script type="text/javascript">

	function showhide(bid,all_flag)
	{
		if(document.getElementById('img'+bid).src != '../../public/images/loading-small.gif')
		{
			if (all_flag == 'all')
			{
				if($('#ect').html() == 'OPEN ALL')
				{
					$('table#detailrow'+bid).animate({ opacity: "show" }, "2000");
					
				}
				else
				{
					$('table#detailrow'+bid).animate({ opacity: "hide" }, "2000");
					
				}

				$('#img'+bid).hide();
					
				if(document.getElementById('img'+bid).src.search('up') > 0)
					document.getElementById('img'+bid).src = '../../public/images/down.png';
				else
					document.getElementById('img'+bid).src = '../../public/images/up.png';
				
				$('#img'+bid).fadeIn('slow');	
			}
			else
			{
				$('table#detailrow'+bid).animate({ opacity: "toggle" }, "2000");
				$('#img'+bid).hide();
				
				if(document.getElementById('img'+bid).src.search('up') > 0)
					document.getElementById('img'+bid).src = '../../public/images/down.png';
				else
					document.getElementById('img'+bid).src = '../../public/images/up.png';
				
				$('#img'+bid).fadeIn('slow');
			}
		}
	}
	
	function showhideall(batchList)
	{
		if(batchList ==1)
		{
			batchList = document.getElementById('blo').innerHTML;
			//serverid = document.getElementById('slo').innerHTML;
		}
		var id = batchList.split(",");
		//var sid = serverid.split(",");
		for(var i = 0 ; i < id.length ; i++)
		{
			showhide(id[i],'all');
			<!--- pvn if(document.getElementById('img'+id[i]))
			{
				$('#img'+id[i]).hide();
				
				if($('table#detailrow'+id[i]).attr('class') == 'detailTable')
				{
					$('table#detailrow'+id[i]).animate({ opacity: "show" }, "2000");
					$('table#detailrow'+id[i]).addClass('detailTableShow').not(this).removeClass('detailTable');
					document.getElementById('img'+id[i]).src = '../../public/images/up.png';
				}
				else
				{
					$('table#detailrow'+id[i]).animate({ opacity: "hide" }, "2000");
					$('table#detailrow'+id[i]).addClass('detailTable').not(this).removeClass('detailTableShow');
					document.getElementById('img'+id[i]).src = '../../public/images/down.png';
				}
				
				$('#img'+id[i]).fadeIn('slow');
			}--->
		}
		
		if($('#ect').html() == 'OPEN ALL')
		{
			$('#ect').hide();
			$('#ectImg').hide();
			$('#ect').html('CLOSE ALL');
			document.getElementById('ectImg').src = '../../public/images/up.png';
			$('#ect').fadeIn("slow");
			$('#ectImg').fadeIn("slow");
		}
		else
		{
			$('#ect').hide();
			$('#ectImg').hide();
			$('#ect').html('OPEN ALL');
			document.getElementById('ectImg').src = '../../public/images/down.png';
			$('#ect').fadeIn("slow");
			$('#ectImg').fadeIn("slow");
		}
	}
	
	function showhideally(batchList,serverid)
	{
		var id = batchList.split(",");
		var sid = serverid.split(",");
		for(var i = 0 ; i < id.length ; i++)
		{
			$('table#detailrowY'+id[i]).animate({ opacity: "toggle" }, "2000");
			
		}
		if(document.getElementById('ecy').innerHTML == '+ expand all')
			document.getElementById('ecy').innerHTML = '- collapse all';
		else
			document.getElementById('ecy').innerHTML = '+ expand all';
	}
</script>




<cfoutput>

	<div align="center">
			<div id="tabvanilla" class="widget">
	
				<!---<ul class="tabnav">
					
					<li><span>Automated Message Monitor</span></li>
					<li><span style="float:right; margin-top:-10px"> <span style="float:left; font-size:11px; padding-left:0px">Powered By</span><br /><img src="../../public/images/MB_Message-hz.jpg" width="120px"/></span></li>
                    <li><span style="float:left; margin-top:-10px"> <span style="float:left; font-size:11px; padding-left:0px"><a href="LogoutProcess.cfm">Log out</a></span><br />
                    </span></li>
                    
			  </ul>--->
			<div align="center">
                <cfform name="catForm" id="catForm" action="" method="post">
                    <cfinput type="text" name="startDateTimeMain" id="startDateTimeMain" value="#startDateTimeMain#" style="width:75px">
                    <cfinput type="text" name="startDateTimeMainT" id="startDateTimeMainT" value="#startDateTimeMainT#" style="width:40px">
                    &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp; 
                    <cfinput type="text" name="endDateTimeMain" id="endDateTimeMain" value="#endDateTimeMain#" style="width:75px">
                    <cfinput type="text" name="endDateTimeMainT" id="endDateTimeMainT" value="#endDateTimeMainT#" style="width:40px">
                    
                    <cfinput type="button" name="date_filter" id="date_filter" value="Filter" style="letter-spacing:0.5px; color:##333333; margin-left:30px" onClick="callList();">
                    <div style="float:right">
                    	<a href="##" style="display:inline" onclick="ViewReportPrefToolDialogMCContent('reportPage'); return false;">View/Change batches</a> | 
                        <a href="##" style="display:inline" onclick="ViewReportToolDialogMCContent('#ValueList(checkMainReporting.batchid_bi)#');">View each batch report</a>
                    </div>
                </cfform>
                
            </div>	
            
			  <div id="today" class="tabdiv" align="left">
			    <!---<div id="left">
			      <fieldset class="Intoday">
			        <legend>Overall Campaign Summary</legend>
			        <div style="margin-top:10px">
			        	here
		            </div>
		          </fieldset>
		        </div>--->
                				
                                
                                
			    		<div style="float:right; margin-right:40px; cursor:pointer" onClick="showhideall('#batchidlist#')">
							<A href="javascript:void(0);" style="display:inline" >
								<span id="ect" style="font-size:13px;">OPEN ALL</span>
							</a>&nbsp;&nbsp;<img src="../../public/images/down.png" width="30px" id="ectImg" align="middle" style="margin-top:-8px" />
						</div>
						<div style="clear: both"></div>
                        
                        		
                        
						<!---<div style="margin-top:15px;">
							<fieldset class="IntodayLess">
								<legend>Select a major category</legend>
								<div style="margin-top:10px" align="center">
									<cfform name="catForm" id="catForm" action="" method="post">
										<!---<label style="font-weight:bold" for="catSel">Please select category to view its campaigns</label>
										<cfinput type="hidden" name="dateS" id="dateS2" value="">
										<cfselect name="catSel" id="catSel" style="letter-spacing:1.5px; color:##666666; margin-left:30px">
											<option value="0"> -- Select One --</option>
											<cfloop query="uniqueCat">
												<option value="#categoryId_int#" <cfif not isdefined('form.dateS')><cfif categoryId_int eq 1>selected</cfif><cfelse><cfif catSel eq categoryId_int>selected</cfif></cfif> >#categoryDesc_vch#</option>
											</cfloop>
										</cfselect>--->
                                        <cfinput type="text" name="startDateTimeMain" id="startDateTimeMain" value="#startDateTimeMain#">
                        				<cfinput type="text" name="endDateTimeMain" id="endDateTimeMain" value="#endDateTimeMain#">
										<cfinput type="submit" name="showCam" id="showCam" onClick="document.getElementById('dateS2').value=document.getElementById('dateS').value" value="Show Campaign" style="letter-spacing:0.5px; color:##333333; margin-left:30px">
									</cfform>
								</div>
							</fieldset>
						</div>--->
						
                       <div style="width:100%">
						<ul class="rows" style="margin-top:15px; border:1px; margin-left:0px">
							<fieldset style="padding-bottom:10px; width:100%"><legend style="color:##666666;padding-left: 6px;padding-right: 6px;">Campaign Details</legend>
								
								<cfif batchidlist eq ''>
									<p style="font-size:14px; color:##666666" align="center"> No Batches found for the selected date and category!!</p>
									<cfabort/>
								</cfif>
								
							<li style="color:##666666; background:##FCFEBC; font-weight:bold; cursor:auto; margin-left:0px">
								<span class="desc">CAMPAIGN DESCRIPTION</span>
								<span class="tc">DAILY SCHEDULED CALLS</span>
								<span class="qc">CALLS IN QUEUE</span>
								<span class="pc">CALLS IN PROGRESS</span>
								<span class="cc">TOTAL COMPLETED</span>
								<span class="mlr">ML-RATE</span>
								<span class="arrow"></span>
							</li>
							
                            <cfloop list="#batchidlist#" index="batchid_num">
                            	<!---<cfquery name="getbatchDetail" datasource="#session.DBSourceEBM#">
                                	select 	desc_vch , created_dt, lastupdated_dt
                                    from	simpleobjects.batch
                                    where	batchid_bi = #batchid_num#
                                </cfquery>--->
                                <cfset batchdesc_vch = 'Main Batch' >
								<li style="background-color: ##ECFFFF; margin-left:0px" id="mainrow#batchid_num#" onClick="showhide('#batchid_num#','')" >
									<span class="desc" title="Campaign - #batchdesc_vch#">
										<cfif len(batchdesc_vch) gt 45>
                                            #left(batchdesc_vch,45)#...
                                        <cfelse>
                                            #batchdesc_vch#
                                        </cfif>
                                    </span>
									<span class="tc" id="dsc#batchid_num#">--TC--</span>
									<span class="qc" id="ciq#batchid_num#">--SC--</span>
									<span class="pc" id="cip#batchid_num#">--IP--</span>
									<span class="cc" id="tc#batchid_num#">--CC--</span>
									<span class="mlr" id="mlr#batchid_num#">--</span>
									<img id="img#batchid_num#"  src="../../public/images/down.png" width="20px"  align="absmiddle" />
                                </li>
															
								<table class="detailTable" id="detailrow#batchid_num#" style="display:table">
									<cfif internal eq 1>
										<tr>
											<td width="150px" ><strong>Batch ID</strong></td>
											<td width="350px" >#batchid_num#</td>
											<td width="150px" ><strong>Username</strong></td>
											<td width="350px" >verizon</td>
										</tr>
										<tr>
											<td width="150px" ><strong>Batch Start Date</strong></td>
											<td width="350px" >sd<!---#dateformat(getStartStopDate.start_Dt,'mmm dd, yyyy')#---></td>
											<td width="150px" ><strong>Batch Stop Date</strong></td>
											<td width="350px" >sd<!---#dateformat(getStartStopDate.stop_Dt,'mmm dd, yyyy')#---></td>
										</tr>
									</cfif>
									<tr>
										<td width="150px" ><strong>Batch start</strong></td>									
										<td width="350px">
                                        	-<!---#dateformat(getbatchDetail.created_dt,'mmm dd, yyyy')# #timeformat(getbatchDetail.created_Dt,'hh:mm:ss tt')#--->
										</td>
										<td width="150px" ><strong>Batch last update</strong></td>
										<td width="350px" >
                                        	-<!---#dateformat(getbatchDetail.lastUpdated_dt,'mmm dd, yyyy')# #timeformat(getbatchDetail.lastUpdated_dt,'hh:mm:ss tt')#--->
										</td>
									</tr>
									<tr>
										<td><strong>Campaign Description</strong></td>
										<td>
                                        	<cfif display eq 0>
                                            	Campaign - --123--
											<cfelse>
												#batchdesc_vch#
											</cfif>
                                           </td>
										<td><strong>Status</strong></td>
										<cfif internal eq 1>
											<td>
												<cfif code eq 1 or code  eq 3>
													--status-- <!---<span class="vtip tooltip" title="--problemR--">[reason]</span>--->
                                                    <span class="tooltip" title="--problemR--">[reason]</span>
												<cfelse>
													--status--
												</cfif>
											</td>
										<cfelse>
											<td>
												<cfif code eq 1>
													Not Running
												<cfelseif code eq 2>
													Completed
												<cfelse>
													Running
												</cfif>
											</td>
	
										</cfif>
									</tr>
									
									<tr style="background-repeat:no-repeat;background-position:center" id="trinner#batchid_num#">
                                    	<td colspan="4" align="center">
											<fieldset style="width:98%; padding:10px; "><legend style="color:##666666;padding-left: 6px;padding-right: 6px;">Reports</legend>
											<table width="100%" class="detailTableInner" id="detailTableInnerImg#batchid_num#" >
                                            	<tbody>
												<tr>
													<td style="font-size:16px;color:##999999; padding:0px" id="detailTableInner#batchid_num#">
														<!---<br /><p>Waiting for details .... </p><br />--->
													    <div id="draggable">
                                                            <span class="menuItem" id="menuItem_#batchid_num#_1" title="Gives a table having call results for different states">
                                                                States table
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_2" title="Show call result chart of two top two called state">
                                                                Top two states results
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_3" title="Show pie chart of all call result">
                                                                Call results
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_4" title="Show chart of average call time per state">
                                                                Avg call time
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_5" title="Chart for number of live calls per hour">
                                                                Live calls/hr
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_6" title="Chart for number of machine calls per hour">
                                                                Machine calls/hr
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_7" title="Chart for stacked call results per hour">
                                                                Call results/hr
                                                            </span>&nbsp;
                                                            <span class="menuItem" id="menuItem_#batchid_num#_8" title="Pie chart for live message delivery length">
                                                                Call time distribution
                                                            </span>
                                                        </div>
                                                        <br>
                                                        <div style="clear:both"></div>
                                                        
                                                        <div class="droppable droppableLeft" id="droppable_#batchid_num#_1">
                                                            
                                                        </div>
                                                        
                                                        <div class="droppable droppableRight" id="droppable_#batchid_num#_2">
                                                            
                                                        </div>
                                                        
                                                        <div style="clear:both">
                                                        </div><br />
                                                        
                                                        <div class="droppable droppableLeft" id="droppable_#batchid_num#_3">
                                                            
                                                        </div>
                                                        
                                                        <div class="droppable droppableRight" id="droppable_#batchid_num#_4">
                                                            
                                                        </div>
                                                    </td>
												</tr>
                                                </tbody>
											</table>
											</fieldset>
										</td>
										
									</tr>
								</table>
							</cfloop>
							<span id="probCounterToday" style="display:none">--probCount--</span>
							</fieldset>
							
							
						</ul>
                        
                        </div>
                       
                    <div id="divTag">
        			</div>
        
				</div><!--/today-->
				
				
	
		</div>
		
        

	</cfoutput>
    
    