<script>
	var LastSelBatchesMCContent;
	$(function() {
		$( "#tabs" ).tabs();
	});
</script>

<script>
	$(function() {
	$("#BatchListMCContent1").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=GetBatchesMCContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
		datatype: "json",
		height: 385,
		colNames:['Select','ID','Description','Created Date', 'Last Updated Date'],
		colModel:[			
			{name:'Select',index:'Options', width:30, editable:false, sortable:false, resizable:false,align:'center' },
			{name:'BatchId_bi',index:'BatchId_bi', width:30, editable:false, resizable:false,align:'right'},
			{name:'Desc_vch',index:'Desc_vch', width:320, editable:true, resizable:false},
			{name:'Created_dt',index:'Created_dt', width:110, editable:false, resizable:false,align:'center'},
			{name:'LASTUPDATED_DT',index:'LASTUPDATED_DT', width:110, editable:false, resizable:false,align:'center'}			
		],
		rowNum:15,
	//   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Campaigns Found.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'Created_dt',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
		ondblClickRow: function(BatchId_bi){ $('#BatchListMCContent').jqGrid('editRow',BatchId_bi,true, '', '', '', '', rx_AfterEditMCContent,'', ''); },
		onSelectRow: function(BatchId_bi){
				if(BatchId_bi && BatchId_bi!==LastSelBatchesMCContent){
					$('#BatchListMCContent').jqGrid('restoreRow',LastSelBatchesMCContent);
					//$('#BatchListMCContent').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEditMCContent,'', '');
					LastSelBatchesMCContent=BatchId_bi;
				}
			},
		 loadComplete: function(data){ 		
		 		//alert('done') 
			}, 
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default second column as ID
			  }	
	});
	
	});
	
	function checkBatch(bid,desc){
		
		$.getJSON("../cfc/reporting.cfc?method=AddToMainBatch&batch_id="+bid+"&isChecked="+$("#SelectBatch"+bid).is(':checked')+"&returnformat=json&queryformat=column", {}, function(res,code) {
			if(parseInt(res.DATA.ID) == 1)
			{
				if($("#SelectBatch"+bid).is(':checked'))
				{
					var liTag = document.createElement("li");
					liTag.id = 'li_'+bid;
					liTag.className ="ui-state-default";
					liTag.innerHTML = bid + '&nbsp;&nbsp;&nbsp;' + $('#s_Desc_'+bid).html();
					document.getElementById('sortable').appendChild(liTag);
				}
				else
				{	
					var d = document.getElementById('sortable');
					var olddiv = document.getElementById('li_'+bid);
					d.removeChild(olddiv);
				}
			}
			else
			{
				alert('Some error occured!!');
				//revert the checkbox action
				if($("#SelectBatch"+bid).is(':checked'))
					$("#SelectBatch"+bid).attr('checked', false);
				else
					$("#SelectBatch"+bid).attr('checked', true);
				
			}
		});
		
		

	}

</script>

<style type="text/css">
	ul{
		list-style-type:none;
	}
	li{
		padding:2px 0px 2px 5px;
	}
</style>


<script type="text/javascript">
	$(function() {
		var $items = $('#vtab>ul>li.MultiChannelVTab');
 		 $items.addClass('selected');
 		 
		$( "#sortable" ).sortable({
			revert: true,
			zIndex: 99999,
			opacity: 0.6,
			stop: function(event, ui) {
					order = [];
					$('ul#sortable').children('li').each(function(idx, elm) {
					  order.push(elm.id)
					}); 
						
				$.getJSON("../cfc/reporting.cfc?method=changeOrder&order="+order+"&returnformat=json&queryformat=column", {}, function(res,code) 
					{
						if(parseInt(res.DATA.ID) != 1)
							alert('Some error occured!!');				
					});
				
		
					
				}
		});
		$("#viewreport").click(function() {
			var ParamStr = '';
			<cfquery name="checkMainReporting" datasource="#session.DBSourceEBM#">
			    SELECT 	
					batchid_bi
			    FROM	
					simpleobjects.reportingmainpref
			    WHERE	
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#session.userid#">
			    ORDER BY 
					order_int 
			</cfquery>

			<cfset BatchIdList = ValueList(checkMainReporting.batchid_bi,'_') />
			var INPBATCHID = '<cfoutput>#BatchIdList#</cfoutput>';
			if(typeof(INPBATCHID) != "undefined" && INPBATCHID != "")					
				ParamStr = '?inpbatchid=' + INPBATCHID;
			else
				INPBATCHID = 0;
			window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reports/reportBatch.cfm" + ParamStr;
			return false;
		});
		//alert($('#sortable').sortable('serialize'))
		<!---$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});--->
		<!---$( "ul, li" ).disableSelection();--->
	});
</script>


<div id="tabs">
	<ul style="height: 30px;">
		<li><a href="#tabs-1">Select Batches</a></li>
		<li><a href="#tabs-2">Arrange Batches</a></li>
	</ul>
	<div id="tabs-1">
		<div style="text-align:left;">
            <table id="BatchListMCContent1"></table>
        </div>
		<button id="viewreport" TYPE="button" class="ui-corner-all" >View report</button>

	</div>
    <cfquery name="findSelectedBatches" datasource="#Session.DBSourceEBM#">
    	SELECT 	
			b.batchid_bi, 
			Desc_vch         
        FROM	
			simpleobjects.reportingmainpref rmp join simpleobjects.batch b on rmp.batchid_bi = b.batchid_bi
        WHERE	
			rmp.userid_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#session.userid#">
        ORDER BY 
			order_int
    </cfquery>
	<div id="tabs-2">
		<ul id="sortable">
            <cfoutput query="findSelectedBatches">
            	<li id="li_#batchid_bi#" class="ui-state-default">#batchid_bi#&nbsp;&nbsp;&nbsp;#desc_vch#</li>
            </cfoutput>
        </ul>
	</div>
	
</div>
