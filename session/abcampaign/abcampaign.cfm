<cfparam name="inpABCampaignId" default="0">
<cfparam name="isd" default="0">
<!---<cfdump var="#FORM#">--->


<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#edit_Contact_Details_Title#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
        <cfinvokeargument name="operator" value="#EMS_Add_Title#">
</cfinvoke>

<cfif NOT campaignEditPermission.havePermission >
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/abcampaign/listabcampaigns">
</cfif>


<script language="javascript">
	$('#subTitleText').text('<cfoutput>#ABCampaign_Text# >> #Manage_ABCampaign_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#ABCampaign_Title#</cfoutput>');
</script>

<cfoutput>
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
    <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
</cfoutput>


   	<!--- Get the data for this AB Campaign--->
	<cfinvoke method="GetABCampaignData" component="#Session.SessionCFCPath#.abcampaign" returnvariable="GetABCampaignDataResult">
    	<cfinvokeargument name="INPABCampaignID" value="#inpABCampaignId#">							
    </cfinvoke>                
   	<!--- <cfdump var="#GetABCampaignDataResult#">    --->
   
   	<!--- Get how many contacts are in the AB Campaigns Group --->
   	<cfinvoke method="GetGroupCount" component="#Session.SessionCFCPath#.multilists2" returnvariable="GetGroupCountResult">
    	<cfinvokeargument name="INPGROUPID" value="#GetABCampaignDataResult.GROUPID#">							
   	</cfinvoke>   
   	<!--- <cfdump var="#GetGroupCountResult#">    --->
      
   	<!--- Get how many contacts are in the AB Campaigns Group and in the queue not complete yet --->
   	<cfinvoke method="GetGroupCountInQueueNotComplete" component="#Session.SessionCFCPath#.multilists2" returnvariable="GetGroupCountInQueueNotCompleteResultA">
    	<cfinvokeargument name="INPGROUPID" value="#GetABCampaignDataResult.GROUPID#">
        <cfinvokeargument name="INPBATCHID" value="#GetABCampaignDataResult.BATCHIDA#">							
   	</cfinvoke>   
  	<!--- <cfdump var="#GetGroupCountInQueueNotCompleteResultA#">    --->
   
   	<!--- Get how many contacts are in the AB Campaigns Group and in the queue not complete yet --->
  	<cfinvoke method="GetGroupCountInResults" component="#Session.SessionCFCPath#.multilists2" returnvariable="GetGroupCountInResultsResultA">
    	<cfinvokeargument name="INPGROUPID" value="#GetABCampaignDataResult.GROUPID#">
        <cfinvokeargument name="INPBATCHID" value="#GetABCampaignDataResult.BATCHIDA#">							
   	</cfinvoke>   
   	<!--- <cfdump var="#GetGroupCountInResultsResultA#">    --->
   
    <!--- Get how many contacts are in the AB Campaigns Group and in the queue not complete yet --->
   	<cfinvoke method="GetGroupCountInQueueNotComplete" component="#Session.SessionCFCPath#.multilists2" returnvariable="GetGroupCountInQueueNotCompleteResultB">
    	<cfinvokeargument name="INPGROUPID" value="#GetABCampaignDataResult.GROUPID#">
        <cfinvokeargument name="INPBATCHID" value="#GetABCampaignDataResult.BATCHIDB#">							
   	</cfinvoke>   
   	<!--- <cfdump var="#GetGroupCountInQueueNotCompleteResultB#">    --->
   
   	<!--- Get how many contacts are in the AB Campaigns Group and in the queue not complete yet --->
   	<cfinvoke method="GetGroupCountInResults" component="#Session.SessionCFCPath#.multilists2" returnvariable="GetGroupCountInResultsResultB">
    	<cfinvokeargument name="INPGROUPID" value="#GetABCampaignDataResult.GROUPID#">
        <cfinvokeargument name="INPBATCHID" value="#GetABCampaignDataResult.BATCHIDB#">							
   	</cfinvoke>   
   	<!--- <cfdump var="#GetGroupCountInResultsResultB#">    --->
      
   <!---
		   <cfdump var="#GetABCampaignDataResult#"> 
		   <cfdump var="#GetGroupCountResult#">
		   <cfdump var="#GetGroupCountInQueueNotCompleteResultA#">
		   <cfdump var="#GetGroupCountInResultsResultA#">
		   <cfdump var="#GetGroupCountInQueueNotCompleteResultB#">
		   <cfdump var="#GetGroupCountInResultsResultB#">
   --->
   
    <cfset TotalInGroup = GetGroupCountResult.GROUPCOUNT>
    <cfset TotalQueuedBatchA = GetGroupCountInQueueNotCompleteResultA.GROUPCOUNT>
    <cfset TotalQueuedBatchB = GetGroupCountInQueueNotCompleteResultB.GROUPCOUNT> 
    <cfset TotalCompleteBatchA = GetGroupCountInResultsResultA.GROUPCOUNT>
    <cfset TotalCompleteBatchB = GetGroupCountInResultsResultB.GROUPCOUNT> 
    <cfset GoalsA = 0>
    <cfset GoalsB = 0>
    
    <cfif isd GT 0>
		<cfset TotalInGroup = 6000>
        <cfset TotalQueuedBatchA = 150>
        <cfset TotalQueuedBatchB = 150>
        <cfset TotalCompleteBatchA = 300>
	    <cfset TotalCompleteBatchB = 300>
        
        <cfset GetABCampaignDataResult.ABCAMPAIGNDESC = "Sample AB Campaign">
        <cfset GetABCampaignDataResult.GROUPNAME = "A Sample Large List of Contacts">
        <cfset GetABCampaignDataResult.BATCHDESCA = "Control Batch">
        <cfset GetABCampaignDataResult.BATCHDESCB = "Variation Test Batch">
        
        <cfset GoalsA = 110>
    	<cfset GoalsB = 140>
    
    </cfif>
    

<style>
	
	
#ManageABCampaignForm .info-button1 {
    background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
    cursor: pointer;
    float: right;
    height: 19px;
    margin-left: 3px;
    margin-right: 10px;
    margin-top: 19px;
    width: 18px;
}


#ManageABCampaignForm .info-box1 {
    background: #9FDAEE; 
	border: 1px solid #2BB0D7;
	border-radius: 5px 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
	-webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
	-moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
	
    color: #FFFFFF;
    display: none;   
    height: auto;
    margin-top: 7px;
    padding-bottom: 20px;
    padding-right: 12px;
    padding-top: 5px;
    width: 800px;
	z-index:1000;
		
	position:absolute;
    top: -25px;
	right: 35px;
}

#ManageABCampaignForm #tool1 {
    background-image: none;
    display: none;
    float: right;
    margin-top: 16px;
	z-index:1000 !important;
		
	position:absolute;
    top: 3px;
	right: 25px;
}

#ManageABCampaignForm .infobox {
    float: left;
    height: auto;
    margin-left: 0px;
	margin-bottom:15px;
    text-align: left;
    width: 268px;
	color: #161616;   
    font-size: 12px;
}


#ManageABCampaignForm .infobox label {
    color: #161616;
    display: block;
    font-size: 14px;
    font-weight: normal;
    line-height: 20px;
    margin-bottom: 0.2em;
    width: 100%;
}

#ManageABCampaignForm .inner-txt {   
    margin-bottom:15px;
}



#ManageABCampaignForm #inner-txt-box1 {   
    width:900px;
}

#ManageABCampaignForm #form-content {

	width: 960px;

}

#ManageABCampaignForm #form-right-portal {
	z-index:1000;
	float:right;
	margin-right:70px;
}

#ManageABCampaignForm .winner {   
    background-color: #D4F9C9;
    border: 1px solid #A7DD97;
	padding:9px !important;
	border-radius: 5px 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
	-webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
	-moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
}

#ManageABCampaignForm .bluebutton{	
	width: 190px;
}

#ManageABCampaignForm #form-content .MarketingImage 
{
	z-index:1;
}

#ManageABCampaignForm #form-content {

}

#ManageABCampaignForm {
    padding-left: 0px !important;
}

#ManageABCampaignForm hr
{
	color: #161616;	
	border : 0;
       height : 1px;
       background-color: #161616;
       margin : 1em 0;
}
	
#ManageABCampaignForm .input-box {
    border: 1px solid #CCCCCC;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    float: left;
    height: 23px;
    margin-top: 0;
    width: 196px;
	border-radius: 5px
}

#ManageABCampaignForm  .results-txt-black {
    color: #000000;    
    font-family: "Verdana",geneva,sans-serif;
    font-size: 13px;
    padding-left: 2px;
    text-align: left;
}
	
	div.MeterContainer
	{
		border: 3px solid rgba(0, 0, 0, 0.3);
		border-radius: 5px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		padding: 5px 0px 5px 7px;	
		margin-bottom:5px;
		
		background: #ffffff;	
		
		width:200px;
		min-width:200px;
	}
	
	div.PercentageMeter
	{
		width:100%;
		min-width:100%;
		height:20px;
		min-height:20px;	
		border-radius: 2px 2px 2px 2px;
			
		background: rgb(204,204,204); /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2NjY2NjYyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iIzk5OTk5OSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
		background: -moz-linear-gradient(left,  rgba(204,204,204,1) 0%, rgba(153,153,153,1) 96%, rgba(255,255,255,1) 96%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(204,204,204,1)), color-stop(96%,rgba(153,153,153,1)), color-stop(96%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(left,  rgba(204,204,204,1) 0%,rgba(153,153,153,1) 96%,rgba(255,255,255,1) 96%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(left,  rgba(204,204,204,1) 0%,rgba(153,153,153,1) 96%,rgba(255,255,255,1) 96%); /* Opera 11.10+ */
		background: -ms-linear-gradient(left,  rgba(204,204,204,1) 0%,rgba(153,153,153,1) 96%,rgba(255,255,255,1) 96%); /* IE10+ */
		background: linear-gradient(to right,  rgba(204,204,204,1) 0%,rgba(153,153,153,1) 96%,rgba(255,255,255,1) 96%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cccccc', endColorstr='#ffffff',GradientType=1 ); /* IE6-8 */

	}
			
	div.PercentageMeter .ARate
	{
		background: rgb(204,0,0); /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2NjMDAwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2NjMDAwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk3JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZmZmZmYiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
		background: -moz-linear-gradient(left,  rgba(204,0,0,1) 0%, rgba(204,0,0,1) 96%, rgba(255,255,255,1) 97%, rgba(255,255,255,1) 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(204,0,0,1)), color-stop(96%,rgba(204,0,0,1)), color-stop(97%,rgba(255,255,255,1)), color-stop(100%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(left,  rgba(204,0,0,1) 0%,rgba(204,0,0,1) 96%,rgba(255,255,255,1) 97%,rgba(255,255,255,1) 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(left,  rgba(204,0,0,1) 0%,rgba(204,0,0,1) 96%,rgba(255,255,255,1) 97%,rgba(255,255,255,1) 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(left,  rgba(204,0,0,1) 0%,rgba(204,0,0,1) 96%,rgba(255,255,255,1) 97%,rgba(255,255,255,1) 100%); /* IE10+ */
		background: linear-gradient(to right,  rgba(204,0,0,1) 0%,rgba(204,0,0,1) 96%,rgba(255,255,255,1) 97%,rgba(255,255,255,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cc0000', endColorstr='#ffffff',GradientType=1 ); /* IE6-8 */

		height:100%;
		min-height:100%;
		border-radius: 2px 2px 2px 2px;
	}
	
	div.PercentageMeter .BRate
	{
		background: rgb(255,175,75); /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYWY0YiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmOTIwYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
		background: -moz-linear-gradient(left,  rgba(255,175,75,1) 0%, rgba(255,146,10,1) 96%, rgba(255,255,255,1) 96%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(255,175,75,1)), color-stop(96%,rgba(255,146,10,1)), color-stop(96%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(left,  rgba(255,175,75,1) 0%,rgba(255,146,10,1) 96%,rgba(255,255,255,1) 96%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(left,  rgba(255,175,75,1) 0%,rgba(255,146,10,1) 96%,rgba(255,255,255,1) 96%); /* Opera 11.10+ */
		background: -ms-linear-gradient(left,  rgba(255,175,75,1) 0%,rgba(255,146,10,1) 96%,rgba(255,255,255,1) 96%); /* IE10+ */
		background: linear-gradient(to right,  rgba(255,175,75,1) 0%,rgba(255,146,10,1) 96%,rgba(255,255,255,1) 96%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffaf4b', endColorstr='#ffffff',GradientType=1 ); /* IE6-8 */

	
		height:100%;
		min-height:100%;
		border-radius: 2px 2px 2px 2px;
	}
	
	div.PercentageMeter .GreenRate
	{
		background: #299a0b; /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzI5OWEwYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iIzI5OWEwYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
		background: -moz-linear-gradient(left,  #299a0b 0%, #299a0b 96%, #ffffff 96%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right top, color-stop(0%,#299a0b), color-stop(96%,#299a0b), color-stop(96%,#ffffff)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(left,  #299a0b 0%,#299a0b 96%,#ffffff 96%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(left,  #299a0b 0%,#299a0b 96%,#ffffff 96%); /* Opera 11.10+ */
		background: -ms-linear-gradient(left,  #299a0b 0%,#299a0b 96%,#ffffff 96%); /* IE10+ */
		background: linear-gradient(to right,  #299a0b 0%,#299a0b 96%,#ffffff 96%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#299a0b', endColorstr='#ffffff',GradientType=1 ); /* IE6-8 */
	
		height:100%;
		min-height:100%;
		bord
	}
	
	div.PercentageMeter .YellowRate
	{
		background: rgb(255,255,136); /* Old browsers */
		/* IE9 SVG, needs conditional override of 'filter' to 'none' */
		background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmY4OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmZmY4OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk2JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
		background: -moz-linear-gradient(left,  rgba(255,255,136,1) 0%, rgba(255,255,136,1) 96%, rgba(255,255,255,1) 96%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(255,255,136,1)), color-stop(96%,rgba(255,255,136,1)), color-stop(96%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(left,  rgba(255,255,136,1) 0%,rgba(255,255,136,1) 96%,rgba(255,255,255,1) 96%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(left,  rgba(255,255,136,1) 0%,rgba(255,255,136,1) 96%,rgba(255,255,255,1) 96%); /* Opera 11.10+ */
		background: -ms-linear-gradient(left,  rgba(255,255,136,1) 0%,rgba(255,255,136,1) 96%,rgba(255,255,255,1) 96%); /* IE10+ */
		background: linear-gradient(to right,  rgba(255,255,136,1) 0%,rgba(255,255,136,1) 96%,rgba(255,255,255,1) 96%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffff88', endColorstr='#ffffff',GradientType=1 ); /* IE6-8 */

		height:100%;
		min-height:100%;
		bord
	}
	
	
	<!---

.homeimage {
	
	
   border: 0 solid;
    box-shadow: 0 4px 4px rgba(0, 0, 0, 0.4);
    padding-bottom: 20px;
    width: 860px;
	
	/*background:#0085C6;*/
	background: rgb(51,182,234); /* Old browsers */
	/* IE9 SVG, needs conditional override of 'filter' to 'none' */
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiMzM2I2ZWEiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI2OSUiIHN0b3AtY29sb3I9IiMwMDg1YzYiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI2OSUiIHN0b3AtY29sb3I9IiMwMDg1YzYiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvcmFkaWFsR3JhZGllbnQ+CiAgPHJlY3QgeD0iLTUwIiB5PSItNTAiIHdpZHRoPSIxMDEiIGhlaWdodD0iMTAxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
	background: -moz-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%, rgba(0,133,198,1) 69%, rgba(0,133,198,1) 69%); /* FF3.6+ */
	background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(51,182,234,1)), color-stop(69%,rgba(0,133,198,1)), color-stop(69%,rgba(0,133,198,1))); /* Chrome,Safari4+ */
	background: -webkit-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* Chrome10+,Safari5.1+ */
	background: -o-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* Opera 12+ */
	background: -ms-radial-gradient(center, ellipse cover,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* IE10+ */
	background: radial-gradient(ellipse at center,  rgba(51,182,234,1) 0%,rgba(0,133,198,1) 69%,rgba(0,133,198,1) 69%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#33b6ea', endColorstr='#0085c6',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */
}


--->
</style>


<script TYPE="text/javascript">

	var $dialogLaunchCampaignNoChange = null;

	$(function() {					
						
		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$("#tool1").toggle();			
		});		  
		 		
		$("#Cancel").click( function() 
		{
			window.history.back();
		});
			
		$("#ManageABCampaignForm #Calculate").click( function() 
		{
			calculate();
		});
						
		<!--- Auto calculate if all the valuers are there --->
		<cfif isd GT 0 AND GoalsA GT 0 AND GoalsA GT 0 AND TotalInGroup GT 0 AND TotalCompleteBatchA GT 0 AND TotalCompleteBatchB GT 0>calculate();</cfif>
		
		
		
		<!--- Campaign Launcher Dialog--->
		$("#ManageABCampaignForm #PushCustomA").click( function() 
		{			
			<!--- Kill any campaign pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogLaunchCampaignNoChange != null)
			{							
				$dialogLaunchCampaignNoChange.remove();
				$dialogLaunchCampaignNoChange = null;
			}	
			
			var INPBATCHDESC = encodeURIComponent('<cfoutput>#GetABCampaignDataResult.BATCHDESCA#</cfoutput>');
			
			<cfoutput>
			if($dialogLaunchCampaignNoChange  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogLaunchCampaignNoChange = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 600,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Launch Campaign',
					draggable: true,
					resizable: true,
					position: 'top'
				}).load('../campaign/act_launchCampaignNC?inpABCampaignId=#inpABCampaignId#&inpGroupId=#GetABCampaignDataResult.GROUPID#&INPBATCHID=#GetABCampaignDataResult.BATCHIDA#&ABTestingBatches=#GetABCampaignDataResult.BATCHIDA#,#GetABCampaignDataResult.BATCHIDB#&INPBATCHDESC=' + INPBATCHDESC);
			}
			</cfoutput>
			$dialogLaunchCampaignNoChange.dialog('open');
		}); 	
		
		<!--- Campaign Launcher Dialog--->
		$("#ManageABCampaignForm #PushCustomB").click( function() 
		{			
			<!--- Kill any campaign pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogLaunchCampaignNoChange != null)
			{							
				$dialogLaunchCampaignNoChange.remove();
				$dialogLaunchCampaignNoChange = null;
			}	
			
			var INPBATCHDESC = encodeURIComponent('<cfoutput>#GetABCampaignDataResult.BATCHDESCB#</cfoutput>');
			
			<cfoutput>
			if($dialogLaunchCampaignNoChange  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogLaunchCampaignNoChange = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 600,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Launch Campaign',
					draggable: true,
					resizable: true,
					position: 'top'
				}).load('../campaign/act_launchCampaignNC?inpABCampaignId=#inpABCampaignId#&inpGroupId=#GetABCampaignDataResult.GROUPID#&INPBATCHID=#GetABCampaignDataResult.BATCHIDB#&ABTestingBatches=#GetABCampaignDataResult.BATCHIDA#,#GetABCampaignDataResult.BATCHIDB#&INPBATCHDESC=' + INPBATCHDESC);
			}
			</cfoutput>
			$dialogLaunchCampaignNoChange.dialog('open');
		}); 	
		
		
		$("#loadingDlgaddgroup").hide();	
		
		
		<!--- Auto refresh pge every 30 seconds if there is somthing still in queue--->
		<cfif TotalQueuedBatchA + TotalQueuedBatchB GT 0 AND isd EQ 0>
			setInterval(function() {    			
										<cfoutput>
											var params = {};
											params.inpABCampaignId =  '#inpABCampaignId#';
											post_to_url('#rootUrl#/#SessionPath#/abcampaign/abcampaign', params, 'POST');
										</cfoutput>			
			
									}, 30000);
		
		</cfif>
		
	});
			
	function NormalP(x)
	{
		var d1 = 0.0498673470,
			d2 = 0.0211410061,
			d3 = 0.0032776263,
			d4 = 0.0000380036,
			d5 = 0.0000488906,
			d6 = 0.0000053830;
		var a = Math.abs(x);
		var t = 1.0 + a * (d1 + a * (d2 + a * (d3 + a * (d4 + a * (d5 + a * d6)))));
		t *= t;
		t *= t;
		t *= t;
		t *= t;
		t = 1.0 / (t + t);
		if (x >= 0)
			t = 1 - t;
		return t;
	}

	function calculate() 
	{	
		$("#ManageABCampaignForm #ARateContainer .WinnerCheck").hide();	
		$("#ManageABCampaignForm #BRateContainer .WinnerCheck").hide();	
				
		$("#ManageABCampaignForm #ARateContainer").removeClass('winner');
		$("#ManageABCampaignForm #BRateContainer").removeClass('winner');
	
		<!--- Validate inputs --->
		if ( $("#ManageABCampaignForm .ARate").attr('rel1') == "" || isNaN(parseInt($("#ManageABCampaignForm .ARate").attr('rel1'))) ) {
				
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are valid numeric values in the A Messages sent field.\n", "Problem!", function(result) { } );							
					
			return;
		}		
		else if ( $("#ManageABCampaignForm .BRate").attr('rel1') == "" || isNaN(parseInt($("#ManageABCampaignForm .BRate").attr('rel1'))) ) {
				
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are valid numeric values in the B Messages sent field.\n", "Problem!", function(result) { } );							
					
			return;
		}
		else if ( $("#ManageABCampaignForm #INPGOALA").val() == "" || isNaN(parseInt($("#ManageABCampaignForm #INPGOALA").val())) ) {
				
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are valid numeric values in the A Goal field.\n", "Problem!", function(result) { } );							
					
			return;
		}
		else if ( $("#ManageABCampaignForm #INPGOALB").val() == "" || isNaN(parseInt($("#ManageABCampaignForm #INPGOALB").val())) ) {
				
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are valid numeric values in the B Goal field.\n", "Problem!", function(result) { } );							
					
			return;
		}
		else if ( parseInt($("#ManageABCampaignForm #INPGOALA").val() ) > parseInt($("#ManageABCampaignForm .ARate").attr('rel1')) ) {
											
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are less A Goals than A Messages sent.\n", "Problem!", function(result) { } );							
					
			return;
		}
		else if ( parseInt($("#ManageABCampaignForm #INPGOALB").val() ) > parseInt($("#ManageABCampaignForm .ARate").attr('rel1')) ) {
				
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Please be sure there are less B Goals than B Messages sent.\n", "Problem!", function(result) { } );							
					
			return;
		}
		
		var c_t = parseInt($("#ManageABCampaignForm .ARate").attr('rel1'));
		var v_t = parseInt($("#ManageABCampaignForm .BRate").attr('rel1'));
		var c_c = parseInt($("#ManageABCampaignForm #INPGOALA").val());
		var v_c = parseInt($("#ManageABCampaignForm #INPGOALB").val());
		if (c_t < 15) {
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("There must be at least 15 Batch A trials for this tool to produce any meaningful results.\n", "Problem!", function(result) { } );							
			return;
		}
		if (v_t < 15) {
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("There must be at least 15 Batch B trials for this tool to produce any meaningful results.\n", "Problem!", function(result) { } );							
			return;
		}
		var c_p = c_c / c_t;
		var v_p = v_c / v_t;
		
		var std_error = Math.sqrt((c_p * (1 - c_p) / c_t) + (v_p * (1 - v_p) / v_t));
		var zvalue = (v_p - c_p) / std_error;
		var pvalue = NormalP(zvalue);		
		var pValueRaw = pvalue;
		
		if (pvalue > 0.5)
			pvalue = 1 - pvalue;
		
		var pValueRaw = pvalue;	
			
		pvalue = Math.round(pvalue * 1000) / 1000;
		$("#ManageABCampaignForm #std_error").html('Std Error=(' + std_error.toFixed(4) + ')');
		<!---$("#ManageABCampaignForm #zvalue").html('ZValue=(' + zvalue + ')');--->
		
		<!---console.log(pvalue);
		console.log(pValueRaw);--->
		
		if(pvalue.toFixed(3) <  0.01)
			$("#ManageABCampaignForm #pvalue").html('Confidence=(' + (99.99).toFixed(2)  + '%)');
		else
			$("#ManageABCampaignForm #pvalue").html('Confidence=(' + ((1-pValueRaw) * 100).toFixed(2)  + '%)');
			
		if (pvalue <= 0.05) 
		{
			$("#ManageABCampaignForm #significant").html("Significant=(Yes!)");
			
			if(c_p > v_p)
			{
				$("#ManageABCampaignForm #ARateContainer").addClass('winner');
				$("#ManageABCampaignForm #ARateContainer .WinnerCheck").show();	
			}
			else
			{
				$("#ManageABCampaignForm #BRateContainer").addClass('winner');			
				$("#ManageABCampaignForm #BRateContainer .WinnerCheck").show();	
			}
		} 
		else
		{
			$("#ManageABCampaignForm #significant").html("Significant=(No)");
		}
		
		$("#ManageABCampaignForm #AConversionRate").html('Conversion Rate = (' + (c_p * 100).toFixed(2)  + '%)');
		$("#ManageABCampaignForm #BConversionRate").html('Conversion Rate = (' + (v_p * 100).toFixed(2)  + '%)');
		
		$("#ManageABCampaignForm #Calculate").html($("#ReCalculate").tmpl());
	}
		
</script>
          


<cfoutput>
	       
    <form id="ManageABCampaignForm" name="ManageABCampaignForm" method="POST">
                            
        <div id="main">                       
                                    
            <div id="form-content" align="center">
            
                <div id="header">
                    <div class="header-text">Manage AB Campaign</div> 
                
                    <div class="info-button1"></div>        
                    
                    <div style="position:relative;">
                        <div id="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right.gif"/></div>
                        <div class="info-box1">        
                                                  
						  <span class="info-txt-blue" style="margin-bottom:10px;">Information - Manage AB Campaign</span>                      
						  
                          <span class="info-txt-black" style="margin-bottom:10px;"><img style="float:right; margin:5px;" width="264px" height="200px" src="<cfoutput>#rootUrl#/#publicPath#/images/ire</cfoutput>/abtest_web.png"/>
                   		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;First off, you will need a decent size sample for tests to become statistically valid. The Significance level is determined by the researcher, and is customarily set at 0.05, or 5 percent. Essentially, this means that 5 percent of the time, the results in your study would be derived by complete chance. But 95 percent of the time, the variable in your study would be the cause of your results. To be sure, the target confidence level  is 95%. When this value is reached, you can generally assume that the test is now statistically valid. Next you need to determine  degrees of freedom. Degrees of freedom are basically the amount of variability involved in the research, which is limited by the number of categories you are examining. There is one degree of freedom in this example because for the chi-square test, the equation for degrees of freedom is: n-1, where &quot;n&quot; is the number of categories (i.e. 2 different camapiagns, A and B)</span>
                          
                          <span class="info-txt-black" style="margin-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The difference between the two results must be larger than the square root of the sum of the two results. In other words, if A got <strong>125</strong> conversions, and B got <strong>129</strong>, then the total is <strong>254</strong>.</span>
                         
                          <span class="info-txt-black" style="margin-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The difference between the two values is <strong>4</strong>. The square root of <strong>254</strong> is <strong>15.93737745</strong></span>
                          
                          <span class="info-txt-black" style="margin-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At a simple level if this value is higher than the difference (<strong>4</strong>), then this result is not statistically valid. </span>
                        
                          <span class="info-txt-black" style="margin-bottom:10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confidence (or Significance) level is an important component in determining whether the data obtained from scientific research is statistically significant. You can use a statistics table to find the Confidence after you calculate other statistical values.</span>
                         <div style="clear:both"></div>
                          <span class="info-txt-black" style="margin-bottom:10px;"><em>Just so you know...</em><br />
                            <strong>89%</strong> or less could just be a fluke<br />
                            <strong>90%</strong> is pretty good for a rough sense<br />
                            <strong>95%</strong> is good enough for academic publishing<br />
                            <strong>99%</strong> or higher is <em>awesome</em><br />
                          </span>
                          <div style="clear:both"></div>
                          <span class="info-txt-black" style="margin-bottom:10px;">...but this is all assuming you set up the experiment correctly.</span>
                          
                         
                                                    
                        </div>
                     </div>
                
                </div>
               
                <div id="inner-txt-box1" class="EBMForm" style="min-height:350px;">
                            
                    <div class="inner-txt-hd" style="padding-left:10px;">Manage AB Campaign for testing messages</div>
                    <div class="inner-txt" style="padding-left:10px;">Use the tools below to manage your AB Campaign.</div>
                	    
                    <div class="form-left-portal">
                        
                       <div class="infobox" style="padding-left:10px;">
                            <label for="INPDESC">AB Campaign Name</label>
        					<span>#GetABCampaignDataResult.ABCAMPAIGNDESC#</span>
                       </div>
                      
                       <div style="clear:both"></div>
                        
                       <div class="infobox" style="padding-left:10px;">
                            <label for="INPDESC">Contact List  Name</label>
        					<span>#GetABCampaignDataResult.GROUPNAME#</span>
                       </div>
                       
                       <div style="clear:both"></div>  
                        
                       <div class="infobox" style="padding:10px; width:250px;">        
							<!--- Details ... no divide by 0--->       
                            <cfif TotalInGroup GT 0>   	
                                <span>Total in queue (#TotalQueuedBatchA+TotalQueuedBatchB# of #TotalInGroup#) (#LSNUMBERFORMAT((TotalQueuedBatchA+TotalQueuedBatchB)/TotalInGroup * 100,0)#%)</span>
                                <div class="MeterContainer">
                                    <div class="PercentageMeter">                                 
                                        <div class="YellowRate" style="width:#LSNUMBERFORMAT((TotalQueuedBatchA+TotalQueuedBatchB)/TotalInGroup * 100,0)#%">&nbsp;</div>                            
                                    </div>
                                </div>    
                            <cfelse>                                
                                <span>Total in queue (#TotalQueuedBatchA+TotalQueuedBatchB#) (0%)</span>
                                <div class="MeterContainer">
                                    <div class="PercentageMeter"> 
                            
                                        <div class="YellowRate" style="width:0%">&nbsp;</div>                                                          
                                    </div>
                                </div>       
                            </cfif>                            
                          </div>
                          
                          <div class="infobox" style="padding:10px; margin-left: 15px;  width:250px;">
                            
                            <!--- Details ... no divide by 0--->       
                            <cfif TotalInGroup GT 0>   	
                                <span>Total complete (#TotalCompleteBatchA+TotalCompleteBatchB# of #TotalInGroup#) (#LSNUMBERFORMAT((TotalCompleteBatchA+TotalCompleteBatchB)/TotalInGroup * 100,0)#%)</span>
                                <div class="MeterContainer">
                                    <div class="PercentageMeter">                                 
                                        <div class="GreenRate" style="width:#LSNUMBERFORMAT((TotalCompleteBatchA+TotalCompleteBatchB)/TotalInGroup * 100,0)#%">&nbsp;</div>                            
                                    </div>
                                </div>    
                            <cfelse>                                
                                <span>Total complete (#TotalCompleteBatchA+TotalCompleteBatchB#) (0%)</span>
                                <div class="MeterContainer">
                                    <div class="PercentageMeter"> 
                            
                                        <div class="GreenRate" style="width:0%">&nbsp;</div>                                                          
                                    </div>
                                </div>       
                            </cfif>                            
                       </div>
                                                                     
                       <div style="clear:both"></div> <hr />
                       
                       <div>
                       
                           <div id="ARateContainer" class="infobox" style="padding:10px; width:250px; position:relative;">

	                            <div class="WinnerCheck" style="display:none; position:absolute; top:5px; left: 235px;"><img width="24px" height="24px" src="<cfoutput>#rootUrl#/#publicPath#/images/</cfoutput>/accepted_48.png"/></div>

                                <label for="INPDESC">Batch A Campaign Name</label>
                                <span>#GetABCampaignDataResult.BATCHDESCA#</span>
                                
                                <div style="clear:both"></div> 
                                <BR />
                                
                                <!--- Details ... no divide by 0--->       
                                <cfif TotalInGroup GT 0>   	
                                	<label>Total Complete Batch A (#TotalCompleteBatchA#) (#LSNUMBERFORMAT(TotalCompleteBatchA/TotalInGroup * 100,0)#%)</label>
                                    <div class="MeterContainer">
                                        <div class="PercentageMeter">            	
                                            <div class="ARate" style="width:#LSNUMBERFORMAT(TotalCompleteBatchA/TotalInGroup * 100,0)#%" rel1="#TotalCompleteBatchA#">&nbsp;</div>                            
                                        </div>
                                    </div>
                                <cfelse>
                                	<label>Total Complete Batch A (#TotalCompleteBatchA#) (0%)</label>
                                    <div class="MeterContainer">
                                        <div class="PercentageMeter">            	
                                            <div class="ARate" style="width:0%" rel1="#TotalCompleteBatchA#">&nbsp;</div>                            
                                        </div>
                                    </div>
                                </cfif>
                                
                                <label for="INPGOALA">Goals (<span style="font-size: 12px;">Conversions, Opt Out, ... etc</span>)</label>
                      		    
                                <cfif isd GT 0 AND GoalsA GT 0>
                      		    	<input type="text" class="input-box" name="INPGOALA" id="INPGOALA" placeholder="Enter total Goals here" value="#GoalsA#">
                                <cfelse>
	                                <input type="text" class="input-box" name="INPGOALA" id="INPGOALA" placeholder="Enter total Goals here">
                                </cfif>
                      
                     			<div style="clear:both"></div>                      
                      			
                                <div id="AConversionRate" class="results-txt-black">&nbsp;</div>
                      
                           </div>                                                       
                           
                           <div id="BRateContainer" class="infobox" style="padding:10px; margin-left: 15px; width:250px; position:relative;">
                           
                           		<div class="WinnerCheck" style="display:none; position:absolute; top:5px; left: 235px;"><img width="24px" height="24px" src="<cfoutput>#rootUrl#/#publicPath#/images/</cfoutput>/accepted_48.png"/></div>

                                <label for="INPDESC">Batch B Campaign Name</label>
                                <span>#GetABCampaignDataResult.BATCHDESCB#</span>
                                
                                <div style="clear:both"></div> 
                                <BR />
                                
                                <!--- Details ... no divide by 0--->       
                                <cfif TotalInGroup GT 0>   
                                    <label>Total Complete Batch B (#TotalCompleteBatchB#) (#LSNUMBERFORMAT(TotalCompleteBatchB/TotalInGroup * 100,0)#%)</label>
                                    <div class="MeterContainer">
                                        <div class="PercentageMeter">            	
                                            <div class="BRate" style="width:#LSNUMBERFORMAT(TotalCompleteBatchB/TotalInGroup * 100,0)#%" rel1="#TotalCompleteBatchB#">&nbsp;</div>                            
                                        </div>
                                    </div>    
                                <cfelse>
                                    <label>Total Complete Batch B (#TotalCompleteBatchB#) (0%)</label>
                                    <div class="MeterContainer">
                                        <div class="PercentageMeter">            	
                                            <div class="BRate" style="width:0%" rel1="#TotalCompleteBatchB#">&nbsp;</div>                            
                                        </div>
                                    </div>     
                                </cfif>     
                                
                                <label for="INPGOALB">Goals (<span style="font-size: 12px;">Conversions, Opt Out, ... etc</span>)</label>
                                
                                <cfif isd GT 0 AND GoalsB GT 0>
                      		    	<input type="text" class="input-box" name="INPGOALB" id="INPGOALB" placeholder="Enter total Goals here" value="#GoalsB#">
                                <cfelse>
	                                <input type="text" class="input-box" name="INPGOALB" id="INPGOALB" placeholder="Enter total Goals here">
                                </cfif>
                                
                                <div style="clear:both"></div>
                                
                                <div id="BConversionRate" class="results-txt-black">&nbsp;</div>
                                               
                           </div>
                           
                           <div style="clear:both"></div> 
                                                                                 
                       </div>
                                                                    
                    </div>
                    
                     
                    <div id="form-right-portal">    
                                  
                        <a href="##" class="bluebutton small tooltipTypeII" id="PushCustomA" >Push Test Batch A
                            <div>
                                <img height="20px" width="20px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png" >
                                <span class="customTypeII infoTypeII">
                                    <img height="48px" width="48px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png">
                                    <em>Information</em>
                                    Use this to queue up Batch A test messages for delivery. Will only push to the queue what is not already in batch A OR batch B
                                </span>
                            </div>
                        </a>
    					<BR />
                        <a href="##" class="bluebutton small tooltipTypeII" id="PushCustomB" >Push Test Batch B
                            <div>
                                <img height="20px" width="20px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png" >
                                <span class="customTypeII infoTypeII">
                                    <img height="48px" width="48px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png">
                                    <em>Information</em>
                                    Use this to queue up Batch B test messages for delivery. Will only push to the queue what is not already in batch A OR batch B
                                </span>
                            </div>
                        </a>
    					                                        		
                        <div style="clear:both; padding-top:150px;"></div>
                            
        				<div id="std_error" class="results-txt-black">&nbsp;</div>
				        <!---<div id="zvalue" class="results-txt-black" >&nbsp;</div>--->
                        <div id="pvalue" class="results-txt-black" >&nbsp;</div>
                        <div id="significant" class="results-txt-black">&nbsp;</div>
                        
                         <a href="##" class="bluebutton small tooltipTypeII" id="Calculate" >Calculate Results
                            <div>
                                <img height="20px" width="20px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png" >
                                <span class="customTypeII infoTypeII">
                                    <img height="48px" width="48px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png">
                                    <em>Information</em>
                                    Enter your Goals (Conversions, Opt Out, ... etc) you want to measure and then <i>calculate</i> your optimal message.  
                            	</span>
                            </div>
                        </a>
    					<BR />
                       
                           
                    </div>
              
              		 
                       
                </div>
                
                <div style="clear:both"></div>
                
            </div>
        
        </div>
    
    </form>
      
</cfoutput>




<script id="ReCalculate" type="text/x-jquery-tmpl">
	Re-Calculate Results
	<div>
		<img height="20px" width="20px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png" >
		<span class="customTypeII infoTypeII">
			<img height="48px" width="48px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/infoii.png">
			<em>Information</em>
			Enter your Goals (Conversions, Opt Out, ... etc) you want to measure and then <i>calculate</i> your optimal message.  
		</span>
	</div>
</script>

