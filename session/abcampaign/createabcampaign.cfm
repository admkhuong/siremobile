
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<!---check permission--->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>
<cfset contactListPermission = permissionObject.havePermission(Contact_List_Title)>
<cfset contactViewPermission = permissionObject.havePermission(View_Contact_Details_Title)>
<cfset contactEditPermission = permissionObject.havePermission(edit_Contact_Details_Title)>
<cfset contactDeletePermission = permissionObject.havePermission(delete_Contact_Title)>
<cfset contactaddgroupPermission = permissionObject.havePermission(Add_Contact_Group_Title)>
<cfset contactAddContactToGroupPermission = permissionObject.havePermission(Add_Contacts_To_Group_Title)>

<cfif NOT contactListPermission.havePermission>
	<cfset session.permissionError = contactListPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<script language="javascript">
	$('#subTitleText').text('<cfoutput>#ABCampaign_Text# >> #Create_ABCampaign_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#ABCampaign_Title#</cfoutput>');
</script>

<cfoutput>
	<link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/form1.css" />
    <link rel="stylesheet" type="text/css"  href="#rootUrl#/#PublicPath#/css/ire/style3.css" />
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
</cfoutput>


<!--- To work correctly, Tool tips need to be css adjusted for each page - not in master style sheet --->
<style>

table.dataTable thead tr {
    color: #FFFFFF;
    line-height: 23px;
}

table.dataTable thead tr {
    background-color: #0888D1;
    border-bottom: 2px solid #FA7D29;
    color: #FFFFFF;
    height: 23px;
    line-height: 23px;
}
table.dataTable tr.odd, table.dataTable tr.odd td.sorting_1 {
    background-color: #F3F9FD !important;
}

#SelectContactPopUp {
    margin: 15px;
}

#tblListGroupContact_info {
    padding-left: 2%;
    width: 98%;
}

.paginate_button {
    background-color: #46A6DD !important;
    border: 1px solid #0888D1 !important;
    box-shadow: 0 1px 0 #2AA3E9 inset;
    color: #FFFFFF !important;
    padding: 3px 8px !important;
}
a.paginate_active {
    background-color: #0888D1 !important;
    border: 1px solid #0888D1 !important;
    border-radius: 5px;
    box-shadow: 0 1px 0 #2AA3E9 inset;
    color: #FFFFFF !important;
    cursor: pointer;
    padding: 3px 8px !important;
}
#CreateABCampaignForm .info-button1 {
    background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.png") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
    cursor: pointer;
    float: right;
    height: 18px;
    margin-left: 3px;
    margin-right: 10px;
    margin-top: 18px;
    width: 18px;
}


#CreateABCampaignForm .info-box1 {
    background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
    border-radius: 5px 5px 5px 5px;
    color: #FFFFFF;
    display: none;   
    height: auto;
    margin-top: 7px;
    padding-bottom: 20px;
    padding-right: 12px;
    padding-top: 5px;
    width: 800px;
	z-index:1000;
	box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
	position:absolute;
    top: -25px;
	right: 37px;
}

#CreateABCampaignForm #tool1 {
    background-image: none;
    display: none;
    float: right;
    margin-top: 16px;
	z-index:1001 !important;
	position:absolute;
    top: 5px;
	right: 25px;
}

#CreateABCampaignForm .wrapper-picker-container:after {
    clear: both;
    content: "";
    display: table;
}

#CreateABCampaignForm .wrapper-picker-container {
    float: left;
    height: 25px;
    margin-left: 0px;   
    text-align: left;
    width: 200px;
}

#CreateABCampaignForm .wrapper-dropdown-picker:after {
    border-color: #2484C6 transparent;
    border-style: solid;
    border-width: 6px 6px 0;
    content: "";
    height: 0;
    margin-top: -3px;
    position: absolute;
    right: 15px;
    top: 50%;
    width: 0;
}

#CreateABCampaignForm .wrapper-dropdown-picker {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.15);
    box-shadow: 0 1px 1px rgba(50, 50, 50, 0.1);
    color: #444444;
    cursor: pointer;
    font-size: 12px;
    height: 19px;
    padding: 6px 10px;
    position: relative;
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
    margin-top: 0px;
    width: 222px;	
}

#CreateABCampaignForm .inputbox {
    float: left;
    height: auto;
    margin-left: 0px;
    text-align: left;
    width: 268px;
}

#CreateABCampaignForm .inner-txt {   
    margin-bottom:15px;
}

#CreateABCampaignForm #form-content {

}

#CreateABCampaignForm #form-right-portal {
	z-index:1000;
}

#CreateABCampaignForm #form-content .MarketingImage 
{
	z-index:1;
}

#CreateABCampaignForm #form-content {

}

#CreateABCampaignForm {
    padding-left: 0px !important;
}

#inpGroupPickerDesc,
#inpBatchDescA,
#inpBatchDescB{
    float: left;
    width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: inline-block;
}
table.dataTable thead th{
    padding: 3px 15px 3px 10px;
}
</style>



<script TYPE="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	<!--- Global var for the Campaign picker dialog--->
	var $dialogCampaignPicker = null;
	
	<!--- allow multiple batch pickers on one page--->
	var $TargetBatchId = null;
	var $TargetBatchDesc = null;

	$(function() {	
				

		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$("#tool1").toggle();			
		});		  
		 		
		$("#Cancel").click( function() 
		{
			if (document.referrer == "") {
			    window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/abcampaign/listabcampaigns";
			} else {
			    history.back();
			}
		});
							
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any campaign pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogCampaignPicker != null)
			{							
				$dialogCampaignPicker.remove();
				$dialogCampaignPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div  id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 800,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true
				}).load('../ems/dsp_select_contact');
			}
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		}); 	
		
		<!--- Open a Campaign picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Campaign picker if its still there --->
		$(".BatchPicker").click( function() 
		{				
			<!--- Kill any group pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker  != null)
			{				
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}
			
			if($dialogCampaignPicker == null)
			{		
				<!--- Load content into a picker dialog--->			
				$dialogCampaignPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 1200,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Campaign Picker',
					draggable: true,
					resizable: true
				}).load('../campaign/act_campaignpicker');
			}
					
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogCampaignPicker.dialog('option', 'position', [x,y]);			
	
			$TargetBatchDesc = $("#" + $(this).attr('rel1') ); // $("#inpBatchDescA");
			$TargetBatchId =  $("#" + $(this).attr('rel2') ); //  $("#inpBatchPickerIdA");;
		
			$dialogCampaignPicker.dialog('open');
		}); 		
		
		$('#inpSubmitAddNewABCampaign').removeAttr("disabled");
		
		
		<!--- AJAX add the AB Campaign--->
		$('#CreateABCampaignForm #inpSubmitAddNewABCampaign').click(function(){
			
			<!--- Validate data --->
			
			if($('#CreateABCampaignForm #INPDESC').val() == ''){
				jAlert("Unique AB Campaign Name is required!", "Add AB Campaign Name");
				return false;
			}
			
			if($('#CreateABCampaignForm #inpGroupPickerId').val() == ''){
				jAlert("A Contact List is required!", "Add Contact List");
				return false;
			}
			
			if($('#CreateABCampaignForm #inpBatchPickerIdA').val() == ''){
				jAlert("Campaign A is required!", "Add Campaign A");
				return false;
			}
			
			if($('#CreateABCampaignForm #inpBatchPickerIdB').val() == ''){
				jAlert("Campaign B is required!", "Add Campaign B");
				return false;
			}
									
			$("#loadingDlgAddABCampaign").show();					
	
			$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/abcampaign.cfc?method=AddABCampaign&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  {
						INPDESC : $("#CreateABCampaignForm #INPDESC").val(),
						INPGROUPID : $("#CreateABCampaignForm #inpGroupPickerId").val(),
						INPBATCHIDA : $("#CreateABCampaignForm #inpBatchPickerIdA").val(),
						INPBATCHIDB : $("#CreateABCampaignForm #inpBatchPickerIdB").val()			
					},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#CreateABCampaignForm #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
									
								<cfoutput>				
									var params = {'inpABCampaignId': d.DATA.ABCAMPAIGNID[0] };
									post_to_url('abcampaign', params, 'POST');			
								</cfoutput>
								
								return false;
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("AB Campaign has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
						$("#CreateABCampaignForm #loadingDlgAddABCampaign").hide();									
				} 		
				
			});
		
			return false;			
		});
		
		 $("#loadingDlgaddgroup").hide();	
	});
	
	function SelectGroup(groupId,groupName){
        $("#inpGroupPickerId").val(groupId);
		$("#inpGroupPickerDesc").text(groupName);
		$dialogGroupPicker.dialog('close');
		$dialogGroupPicker.html("");
		$dialogGroupPicker.remove();
		$dialogGroupPicker = null;
	}
	
</script>

<!--- http://www.wikihow.com/Assess-Statistical-Significance 
http://www.webgeekly.com/lessons/website-testing/ab-testing-statistical-significance/
http://www.google.com/imgres?imgurl=http://www.webgeekly.com/wp-content/uploads/2010/06/endo-AB-test.jpg&imgrefurl=http://www.webgeekly.com/lessons/website-testing/ab-testing-statistical-significance/&h=282&w=332&sz=52&tbnid=GNL1ftsT9iHBnM:&tbnh=90&tbnw=106&zoom=1&usg=__0yuNl2zYZK5sI9mDFccPnelRBkE=&docid=Ig6w5GnaPZjHqM&sa=X&ei=uuZpUc7xLcHJiwKzwoC4Bw&ved=0CFEQ9QEwAQ&dur=8409
--->

<cfoutput>
       
    <form id="CreateABCampaignForm" name="CreateABCampaignForm" method="POST">
                            
        <div id="main">                       
                                    
            <div id="form-content" align="center">
            
                <div id="header">
                    <div class="header-text">Add a new AB Campaign</div> 
                
                    <div class="info-button1"></div>        
                    
                    <div style="position:relative;">
                        <div id="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
						<span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new AB Campaign</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
				
						<span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>
                
                </div>
               
                <div id="inner-txt-box1" class="EBMForm" style="min-height:350px;">
                            
                    <div class="inner-txt-hd">Add a new AB Campaign for testing messages</div>
                    <div class="inner-txt">Please begin creating your AB Campaign by giving it a unique name.</div>
                	    
                    <div class="form-left-portal">
                        
                        <div class="inputbox">
                            <label for="INPDESC">AB Campaign Name</label>
                            <input type="text" class="input-box" name="INPDESC" id="INPDESC" placeholder="Give it a unique name here">
                        </div>
                       <!--- <div class="info-button1"></div>--->
                        <div style="clear:both"></div>
                        
                        
                        <cfif isDefined("form.submit")>
                            <cfif Trim(form.INPDESC) EQ "">
                                <label id="lblInvalidExpirationDateError" style="color: red;">
                                    AB Campain name is required</label>  
                            </cfif> 
                        </cfif>  
                        
                        <div class="inputbox">
	                        <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="">
                            <label for="inpGroupPickerDesc">Select a Contact List</label>
                             <div class="wrapper-picker-container GroupPicker">                                    
                                <div class="wrapper-dropdown-picker input-box" tabindex="1"> <span id="inpGroupPickerDesc">click here to select a Contact List</span> </div>                                            
                            </div>
                        </div>
                       <!--- <div class="info-button2"></div>--->
                        <div style="clear:both"></div>
                        
                        <BR />
                        
                        <div class="inputbox">
                            <input type="hidden" name="inpBatchPickerIdA" id="inpBatchPickerIdA" value="">
                            <label for="inpBatchDescA">Select an A Campaign to test with</label>
                            <div class="wrapper-picker-container BatchPicker" rel1="inpBatchDescA" rel2="inpBatchPickerIdA">                                
                                <div class="wrapper-dropdown-picker input-box inpBatchA" tabindex="1"> <span id="inpBatchDescA">click here to select a A Campaign</span> </div>                                            
                            </div>
                        </div>
                        <!---<div class="info-button3"></div>--->
                        <div style="clear:both"></div>
                        
                        <BR />
                                       
                        <div class="inputbox">
                            <input type="hidden" name="inpBatchPickerIdB" id="inpBatchPickerIdB" value="">
                            <label for="inpBatchDescB">Select an B Campaign to test with</label>
                            <div class="wrapper-picker-container BatchPicker" rel1="inpBatchDescB" rel2="inpBatchPickerIdB">                                    
                                <div class="wrapper-dropdown-picker input-box inpBatchB" tabindex="1"> <span id="inpBatchDescB">click here to select a B Campaign</span> </div>                                            
                            </div>
                        </div>
                        <!---<div class="info-button4"></div>--->
                        <div style="clear:both"></div>
                        
                    </div>
                    
                     
                    <div id="form-right-portal">
                                       
                		<div class="MarketingImage">
               		    	<img src="<cfoutput>#rootUrl#/#publicPath#/images/ire</cfoutput>/abtest_web.png"/>
                   		</div>
                     
                    </div>
              
                </div>
                
                <div style="clear:both"></div>
                
            </div>
        
        </div>
        
        
        <div style="clear:both"></div>
        <div class="submitCpp">
            <a href="##" class="button blue small" id="inpSubmitAddNewABCampaign" >Add</a>     
            <a href="##" class="button blue small" id="Cancel" >Cancel</a>
        </div>

    </form>
      
</cfoutput>
