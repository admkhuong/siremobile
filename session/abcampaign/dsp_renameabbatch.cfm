<!---<cfinclude template="../lib/xml/xmlUtil.cfc">--->
<div id="overlay" class="web_dialog_overlay"></div>
<div id="dialog_renameCampaign" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Rename AB Campaign Description</span></strong></label> <span id="closeDialog" onclick="CloseDialog('renameCampaign');">Close</span> </div>
	<div style="padding-bottom: 10px; padding-top: 10px;">
		<div class="answer_padding">
			<div class="clear">
			    <div>
			    	<label class="qe-label-a">Please update your AB campaign Description</label>
				</div>
				<div>
					<input TYPE="hidden" name="INPABCampaignID" id="INPABCampaignID" value="0" />
		  			<input type="text" name="inpBatchDesc" id="inpBatchDesc" onkeypress="return handleKeyPress(event);" value="" style="height: 20px; width: 435px"/> 
		  			<br>
                	<label id="lblCampaignNameRequired" style="color: red; display: none;">AB Campaign description is required</label>
	  			</div>
  			</div>
		</div>
	</div>
	<div id="loadingDlgRenameCampaign" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
    </div>
	<div class='button_area' style="padding-bottom: 10px;">
		<button 
			id="btnRenameCampaign" 
			type="button" 
			class="ui-corner-all survey_builder_button"
		>Save</button>
		<button 
			id="Cancel" 
			class="ui-corner-all survey_builder_button" 
			type="button"
		>Cancel</button>
	</div>
</div>

<script TYPE="text/javascript">
	function SaveBatch(INPABCampaignID) {
		var INPABCampaignID = $("#dialog_renameCampaign #INPABCampaignID").val();
		if ($.trim($("#dialog_renameCampaign #inpBatchDesc").val()) == "") {
			$('#dialog_renameCampaign #lblCampaignNameRequired').show();
			return;
		}					
		var $items = $('#vtab>ul>li.MultiChannelVTab');
		$items.addClass('selected');
		$("#loadingDlgRenameCampaign").show();		
				
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/abcampaign.cfc?method=UpdateABCampaignDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			id : INPABCampaignID,
			Desc_vch : $("#dialog_renameCampaign #inpBatchDesc").val()},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								jAlert("AB Campaign renamed successfully", "Success", function(result) { 
									//window.location.reload();
									InitAbCampaigns();
								});
								$("#loadingDlgRenameCampaign").hide();	
								CloseDialog('renameCampaign');
								return false;
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("AB Campaign has NOT been saved.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
					$("#dialog_renameCampaign #loadingDlgRenameCampaign").hide();
								
			} 		
			
		});
		return false;
	}
	
	$(function() {	
		$("#dialog_renameCampaign #btnRenameCampaign").click(function() { 
			SaveBatch(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#dialog_renameCampaign #Cancel").click(function() {
			CloseDialog('renameCampaign');
			return false;
	  	}); 	
		
		$("#loadingDlgRenameCampaign").hide();	
	});
		
	function handleKeyPress(e){
		var key=e.keyCode || e.which;
		if (key==13){
			SaveBatch();
	  		return false; 
		}
	}
	
	function showRenameABCampaignDialog(INPABCampaignID, inpDesc) {
		$('#dialog_renameCampaign #lblCampaignNameRequired').hide();
		ShowDialog('renameCampaign', 'Rename AB Campaign Description');
		$("#dialog_renameCampaign #INPABCampaignID").val(INPABCampaignID);
		$("#dialog_renameCampaign #inpBatchDesc").val(inpDesc);		
		return false;
	}
	function ShowDialog(dialog_id, title){
		$("#overlay").show();
		$("#dialog_" + dialog_id).show();
		$("#dialog_" + dialog_id + " #title_msg").html(title);
		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	
	// Function use to close the current dialog
	function CloseDialog(dialog_id){
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
</script>


<style>

#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}




</style> 

<cfoutput>
 
</cfoutput>
