<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
</cfoutput>

<style type="text/css">
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListAbCampaigns_info {
		width: 98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
		
		.paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}
		
		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}
		
		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}
		
		table.dataTable tr td{
			color:#5E6AAD;
		}
		
		table { border-spacing: 0; }
</style>

<!---this is custom filter for datatable--->
<div id="filter">
	<cfoutput>
	<!---set up column --->
	<cfset datatable_ColumnModel = arrayNew(1)>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'AB Campaign Description', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'b.Desc_vch'})>
	<cfif session.userrole EQ 'SuperUser'>
		<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Owner ID', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'u.UserId_int'})>
		<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Company ID', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'c.CompanyName_vch'})>
	</cfif>
	<!---we must define javascript function name to be called from filter later--->
	<cfset datatable_jsCallback = "InitAbCampaigns">
	<cfinclude template="/session/ems/datatable_filter.cfm" >
	</cfoutput>
</div>

<!---check permission--->
<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css');
	</style>
	
</cfoutput>
<cfinclude template="dsp_renameabbatch.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#ABCampaign_Title#">
</cfinvoke>
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
<cfset newCampaign="newCampaign">

<div class="border_top_none">
	<table id="tblListAbCampaigns" class="table" cellspacing=0>
	</table>
</div>
<script>
	var _tblListAbCampaigns;
	//init datatable for active agent
	function InitAbCampaigns(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListAbCampaigns = $('#tblListAbCampaigns').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'ABCampaignId_bi', "sTitle": 'AB ID', "sWidth": '10%',"bSortable": false},
				<cfif session.userrole EQ 'SuperUser'>
					{"sName": 'Owner_ID', "sTitle": 'Owner ID', "sWidth": '10%',"bSortable": true},
					{"sName": 'CompanyName_vch', "sTitle": 'Company ID', "sWidth": '10%',"bSortable": true},
				</cfif>
				{"sName": 'Desc_vch', "sTitle": 'AB Description', "sWidth": '50%',"bSortable": true},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetabCampaignsListDataTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListAbCampaigns').attr('style', '');
				$('#tblListAbCampaigns').css('min-width', '1053px');
			}
	    });
	}
	InitAbCampaigns();
	
	$('#subTitleText').hide();
	$('#mainTitleText').text('<cfoutput>#ABCampaign_Title#</cfoutput>');
	
	$(function() {
		$(".del_RowMCContent").click(function() {
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var currPage =  $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					delBatch(CurrREL,currPage);
				}
				return false;																	
			});
		});
		
		$('.Cancel_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
			
			jConfirm( "Are you sure you want to cancel this Campaign?", "Cancel Campaign", function(result) { 
				if(result)
				{	
					cancelBatch(batchId,currPage);
				}
				return false;																	
			});
		});
			
	});
	
	function delBatch(INPBATCHID)
	{
		jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					$.ajax({
						type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/abcampaign.cfc?method=UpdateABCampaignStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						data:  { INPABCampaignID : INPBATCHID},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
						success:		
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d) 
							{
								<!--- Alert if failure --->
																											
									<!--- Get row 1 of results if exisits--->
									if (d.ROWCOUNT > 0) 
									{						
										<!--- Check if variable is part of JSON result string --->								
										if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
										{							
											CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
											
											if(CurrRXResultCode > 0)
											{
												InitAbCampaigns();
												return false;
											}
											else
											{
												$.alerts.okButton = '&nbsp;OK&nbsp;';
												if (d.DATA.ERRMESSAGE[0] != '') {
													jAlert("Campaign has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );						
												}
												else {
													jAlert(d.DATA.MESSAGE[0], "Failure!", function(result) { } );
												}
											}
										}
									}
									else
									{<!--- No result returned --->		
										$.alerts.okButton = '&nbsp;OK&nbsp;';					
										jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
									}
							} 		
						});
				}
				return false;																	
			});
		return false;
	}
	
	function cancelBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=CancelBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { } );
									InitAbCampaigns();
									return false;
										
								}
								else
								{
									if (d.DATA.MESSAGE[0] != "") {
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Campaign has NOT been cancelled.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
									}
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	
	function EditCampaign(inpABCampaignId) {
		<cfoutput>
					
			var params = {'inpABCampaignId':inpABCampaignId};				
		
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/abcampaign/abcampaign', params, 'POST');				
	
		</cfoutput>	
	}
	
       
    function OpenDialog(dialog_id){
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	function CloseDialog(dialog_id){
		currentUserID = '';
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
</script> 