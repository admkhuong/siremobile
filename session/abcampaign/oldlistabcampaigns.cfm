<cfparam name="page" default="1">
<cfparam name="query" default="">
<!---check permission--->
<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/survey.css');
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		<!---@import url('#rootUrl#/#PublicPath#/css/utility.css');--->
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css');
	</style>
	
</cfoutput>
<cfinclude template="dsp_renameabbatch.cfm">
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
<cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
<cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
<cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
<cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
<cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
<cfset runCampaignWithoutRecipients = "You must add recipients before running a campaign">
<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign.">
<cfif NOT campaignPermission.havePermission>
	<cfset session.permissionError = campaignPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#ABCampaign_Title#">
</cfinvoke>
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
<cfset newCampaign="newCampaign">
<cfimport taglib="../lib/grid" prefix="mb" />
		
<cfoutput>
	<cfscript>
		htmlOption = "";
		
		if(campaignEditPermission.havePermission){
			htmlOption = htmlOption & '<a href="##" onclick="return EditCampaign({%ABCAMPAIGNID_BI%});"><img class="ListIconLinks img16_16 view_group_16_16" title="Edit AB Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';		
		}		
	
		htmlOption = htmlOption & '<a href="##" onclick="return showRenameABCampaignDialog({%ABCAMPAIGNID_BI%}, ''{%DESC_VCH%}'');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="#rootUrl#/#PublicPath#/images/dock/blank.gif" /></a>';		
		
		if(campaignDeletePermission.havePermission){
			htmlOption = htmlOption & "{%DeleteAB%}";
		}
						
	</cfscript>
	
	<!---- Create template with name is 'normal'----->
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
		}>
			
	<!--- Prepare column data---->
	<cfset arrNames = ['AB Id', 'AB Description']>	
	<cfset arrColModel = [			
			
			{name='ABCAMPAIGNID_BI', width="50px", isHtmlEncode=false},
			{name='DESC_VCH', width="320px"}			
		   ] >		
      	
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayinsertAt(arrNames, 2, "Owner ID")>
		<cfset ArrayinsertAt(arrColModel, 2, {name='UserId_int',index='UserId_int',width='10%', class="CampaignOwner"})>
		<cfset ArrayinsertAt(arrNames, 3, "Company ID")>
		<cfset ArrayinsertAt(arrColModel, 3, {name='CompanyName_vch',index='CompanyName_vch',width='10%', class="CampaignCompany"})>
	</cfif>
	
	<cfif htmlOption NEQ ''>
		<cfset arrayAppend(arrNames, 'Options')>
		<cfset arrayAppend(arrColModel, {name='Options', width="120px", format = [htmlFormat]})>
	</cfif>
    
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='AB Campaign Description', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}
		]
	>
	<cfset 
		filterFields = [
			'Desc_vch'
		]	
	>
	
	<cfif session.userrole EQ 'SuperUser'>
		<cfset ArrayAppend(filterFields, "UserId_int")>
		<cfset ArrayAppend(filterKeys, {VALUE='2', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"})>
		<cfset ArrayAppend(filterFields, "CompanyName_vch")>
		<cfset ArrayAppend(filterKeys, {VALUE='3', DISPLAY='Company ID', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"})>
	</cfif>
	
	<mb:table 
		name="campaigns" 
		component="#LocalSessionDotPath#.cfc.abcampaign" 
		method="getListABCampaign"
		colNames= #arrNames# 
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
		width = "100%"
		>
	</mb:table>		

</cfoutput>		
		
<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/iconslist.css');
	</style>
</cfoutput>


<script>
	
	$('#subTitleText').hide();
	$('#mainTitleText').text('<cfoutput>#ABCampaign_Title#</cfoutput>');
	
	$(function() {
		$(".del_RowMCContent").click(function() {
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var currPage =  $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					delBatch(CurrREL,currPage);
				}
				return false;																	
			});
		});
		
		$('.Cancel_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
			
			jConfirm( "Are you sure you want to cancel this Campaign?", "Cancel Campaign", function(result) { 
				if(result)
				{	
					cancelBatch(batchId,currPage);
				}
				return false;																	
			});
		});
			
	});
	
	
	
	
	function delBatch(INPBATCHID,currPage)
	{
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { INPBATCHID : INPBATCHID},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								reloadPage();
								return false;
									
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								if (d.DATA.ERRMESSAGE[0] != '') {
									jAlert("Campaign has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );						
								}
								else {
									jAlert(d.DATA.MESSAGE[0], "Failure!", function(result) { } );
								}
							}
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
		});
		return false;
	}
	
	function cancelBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=CancelBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { } );
									reloadPage();
									return false;
										
								}
								else
								{
									if (d.DATA.MESSAGE[0] != "") {
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Campaign has NOT been cancelled.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
									}
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	
	
	function reloadPage(){
		
		<cfif StructKeyExists(URL, 'page')>
			<cfset pageUrl = "?page=" & URL.page>
		<cfelse>
			<cfset pageUrl = "">
		</cfif>
		var submitLink = "<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns#pageUrl#</cfoutput>";
		reIndexFilterID();
		$("#filters_content").attr("action", submitLink);
		// Check if dont user filter
		if($("#isClickFilter").val() == 0){
			$("#totalFilter").val(0);
		}
		$("#filters_content").submit();
		
	}
	
	function EditCampaign(inpABCampaignId) {
		<cfoutput>
					
			var params = {'inpABCampaignId':inpABCampaignId};				
		
			post_to_url('#LocalProtocol#://#CGI.SERVER_NAME#/#SessionPath#/abcampaign/abcampaign', params, 'POST');				
	
		</cfoutput>	
	}
	
       
    function OpenDialog(dialog_id){
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	function CloseDialog(dialog_id){
		currentUserID = '';
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
    
</script>
