<cfparam name="ENA_Message" default="No MESSAGE Specified">
<cfparam name="TroubleShootingTips" default="No Troubleshooting Tips Specified">
<cfparam name="SubjectLine" default="High Priority - Error SimpleX">
<cfparam name="ErrorNumber" default="0">
<cfparam name="AlertType" default="1">

<cfparam name="cfcatch.TYPE" default="">
<cfparam name="cfcatch.MESSAGE" default="">
<cfparam name="cfcatch.detail" default="">
<cfparam name="CGI.HTTP_HOST" default="">
<cfparam name="CGI.HTTP_REFERER" default="">
<cfparam name="CGI.HTTP_USER_AGENT" default="">
<cfparam name="CGI.PATH_TRANSLATED" default="">
<cfparam name="CGI.QUERY_STRING" default="">

<cfif RIGHT(SubjectLine,8) NEQ "- DEL408">
	<cfset SubjectLine = SubjectLine & " � DEL408">
</cfif>

<!--- no longer - 9494000553@mobile.att.net --->
<cfoutput>

<!---<cfset CONTACTLIST = "jpeterson@messagebroadcast.com;9494000553@messaging.sprintpcs.com;jim@messagebroadcast.com;9499458302@txt.att.net;IT@messagebroadcast.com;9497480569@tmomail.net" >
--->

<cfset CONTACTLIST = "it@messagebroadcast.com;">

<cfset ErrorLogInsertedOK = 1>

<cftry>

	<cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
        INSERT INTO simplequeue.errorlogs
        (
        	ErrorNumber_int,
            Created_dt,
            Subject_vch,
            Message_vch,
            TroubleShootingTips_vch,
            CatchType_vch,
            CatchMessage_vch,
            CatchDetail_vch,
            Host_vch,
            Referer_vch,
            UserAgent_vch,
            Path_vch,
            QueryString_vch
        )
        VALUES
        (
        	#ErrorNumber#,
            NOW(),
            '#REPLACE(SubjectLine, "'", "''")#',   
            '#REPLACE(ENA_Message, "'", "''")#',   
            '#REPLACE(TroubleShootingTips, "'", "''")#',     
            '#REPLACE(cfcatch.TYPE, "'", "''")#', 
            '#REPLACE(cfcatch.MESSAGE, "'", "''")#',
            '#REPLACE(cfcatch.detail, "'", "''")#',
            '#LEFT(REPLACE(CGI.HTTP_HOST, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.HTTP_REFERER, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.HTTP_USER_AGENT, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.PATH_TRANSLATED, "'", "''"), 2048)#',
            '#LEFT(REPLACE(CGI.QUERY_STRING, "'", "''"), 2048)#'
        );
	</cfquery>

<cfcatch type="any"><cfset ErrorLogInsertedOK = 0></cfcatch>

</cftry>

<!---<cfmail to="#CONTACTLIST#" from="IT@messagebroadcast.com" subject="#SubjectLine#" username="rxdialer@messagebroadcast.com" password="rxdialer" type="html">	
--->

	<cfsavecontent variable="CGIDump">

	#ENA_Message#<BR />
	#TroubleShootingTips#<BR />
    Logged in DB under simplequeue.errorlogs = (#ErrorLogInsertedOK#) <BR />
    
    

	<cfif IsDefined("cfcatch.TYPE") AND Len(Trim(cfcatch.TYPE)) GT 0 >
	cfcatch.TYPE<BR />
	-------------------------<BR />
	#cfcatch.TYPE#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("cfcatch.MESSAGE") AND Len(Trim(cfcatch.MESSAGE)) GT 0 >
	cfcatch.MESSAGE<BR />
	-------------------------<BR />
	#cfcatch.MESSAGE#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >
	cfcatch.detail<BR />
	-------------------------<BR />
	#cfcatch.detail#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("CGI.HTTP_HOST") AND Len(Trim(CGI.HTTP_HOST)) GT 0 >
	CGI.HTTP_HOST<BR />
	-------------------------<BR />
	#CGI.HTTP_HOST#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("CGI.HTTP_REFERER") AND Len(Trim(CGI.HTTP_REFERER)) GT 0 >
	CGI.HTTP_REFERER<BR />
	-------------------------<BR />
	#CGI.HTTP_REFERER#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("CGI.HTTP_USER_AGENT") AND Len(Trim(CGI.HTTP_USER_AGENT)) GT 0 >
	CGI.HTTP_USER_AGENT<BR />
	-------------------------<BR />
	#CGI.HTTP_USER_AGENT#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("CGI.PATH_TRANSLATED") AND Len(Trim(CGI.PATH_TRANSLATED)) GT 0 >
	CGI.PATH_TRANSLATED<BR />
	-------------------------<BR />
	#CGI.PATH_TRANSLATED#<BR />
	-------------------------<BR />
	</cfif>

	<cfif IsDefined("CGI.QUERY_STRING") AND Len(Trim(CGI.QUERY_STRING)) GT 0 >
	CGI.QUERY_STRING<BR />
	-------------------------<BR />
	#CGI.QUERY_STRING#<BR />
	-------------------------<BR />
	</cfif>

	</cfsavecontent>


       <cftry>
            <cfquery datasource="#Session.generalAlertsDBSource#">
                INSERT INTO [Alerts].[dbo].[Daily_Alerts]
                   (ServerIP_vch
                   ,ENASubject_vch
                   ,ENAMessage_vch
                   ,CGIDump_txt
                   ,HighPriority_bit
                   ,AlertType_si
                   ,To_vch)
             VALUES
                   ('10.11.0.75'
                   ,'#SubjectLine#'
                   ,'#ENA_Message#'
                   ,'#CGIDump#'
                   ,1
                   ,#AlertType#
                   ,'#CONTACTLIST#')
            </cfquery>
        <cfcatch>
            <cfmail to="#CONTACTLIST#" from="IT@messagebroadcast.com" subject="#SubjectLine#">
                Failed to post alert to database:
                cfcatch.Message: <cfif IsDefined("cfcatch.message") AND Len(Trim(cfcatch.message)) GT 0>#cfcatch.Message#<cfelse>Unknown</cfif>
                cfcatch.Detail: <cfif IsDefined("cfcatch.detail") AND Len(Trim(cfcatch.detail)) GT 0 >#cfcatch.Detail#<cfelse>Unknown</cfif>
                ---
                #ENA_Message#
                #CGIDump#
            </cfmail>
        </cfcatch>
        </cftry>
    
    	
        
        
</cfoutput>


