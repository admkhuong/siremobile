<cfinclude template="../scripts/data_ScriptPaths.cfm">
<cfif isDefined("scrId")>
	<cfset ids = ListToArray(scrId, "_") />
	<cfset inpUserId = ids[1] /> 
	<cfset inpLibId = ids[2] /> 
	<cfset inpEleId = ids[3] /> 
	<cfset inpScriptId = ids[4] />
    
	<cfset MainLibFile = "#rxdsLocalWritePath#/U#inpUserId#/L#inpLibId#/E#inpEleId#\RXDS_#inpUserId#_#inpLibId#_#inpEleId#_#inpScriptId#.mp3" />
    
    <cfquery name="updateView" datasource="#Session.DBSourceEBM#">
        Update rxds.scriptData
        set viewcount_int = viewcount_int+1
        where userid_int = #inpUserId# and DATAID_INT=#inpScriptId#    
    </cfquery>

	<cftry>
		<cfheader name="Content-Disposition" value="attachment;filename=TestFile.mp3">
		<cfcontent TYPE="application/mp3" file="#MainLibFile#" />
		<cfcatch>
		</cfcatch>
	</cftry>
</cfif>

