<cfparam name="Session.USERID" default="0">
<cfquery name="getTaskList" datasource="#Session.DBSourceEBM#">
SELECT l.id,UserId_int,ItemId_int,required_int,Value_vch,CreatedDate,changeDate,i.description
FROM
	simpleobjects.tasklist l
JOIN
  simpleobjects.tasklistitems i
ON
  l.ItemId_int = i.id
WHERE
	UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
</cfquery>
<style>
	<cfif find('Firefox',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 .35em 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('MSIE 9.0',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 .5em 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('MSIE',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('Chrome',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	<cfelseif find('Safari',cgi.HTTP_USER_AGENT)>
		input[type="input"] { height:17px; border:1px solid #ccc; margin:0 0 4px 0; padding:0 0 0 0; vertical-align:middle; display:inline-block;}
	</cfif>
	
	input[type="checkbox"] { height:20px; border:1px solid #ccc; margin:0; padding:0; vertical-align:middle; margin-bottom:.25em;}
	.colhdr {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0; background:url(../Rules/ProjectManagement/images/hdr-blu-40.png); border: 1px solid #666; text-align: center;}
	.colhdr:hover {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0;  -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blu-over-40.png); border: 1px solid #666; text-align: center;}
	.curCol .colhdr {  border:1px solid #fff; font-size:18px; color:#e7fbff; text-shadow: 0px 2px 2px #000; -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blu-over-40.png); filter:progid:DXImageTransform.Microsoft.Shadow(color='black', Direction=180, Strength=5)}
	
	.chcolhdr {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0; background:url(../Rules/ProjectManagement/images/hdr-blk-40.png); border: 1px solid #666; text-align: center;}
	.chcolhdr:hover {font-weight:bold; font-size:12px; color:#fff; padding: 10px 0;  -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blk-over-40.png); border: 1px solid #666; text-align: center;}
	.curCol .chcolhdr {  border:1px solid #fff; font-size:18px; color:#e7fbff; text-shadow: 0px 2px 2px #000; -moz-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  -webkit-box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5);  box-shadow: 0px 0px 10px 5px rgba(0, 0, 0, .5); background:url(../Rules/ProjectManagement/images/hdr-blk-over-40.png); filter:progid:DXImageTransform.Microsoft.Shadow(color='black', Direction=180, Strength=5)}
	
	
	#hdr-wrap { width: 1160px; padding: 0 0 20px 15px; margin: 0 auto; overflow: hidden; }
	
	#page-wrap { width: 1160px; padding: 0 0 20px 15px; margin: 0 auto; overflow: hidden; }
	
	#page-wrap2 { width: 1160px; padding: 0 0 0 15px; margin: 0 auto; overflow: hidden;}
		
	.info-col { float: left; width: 160px; height: 100%; padding: 0 0 0 0; line-height:18px;}
	/*.info-col h2 { text-align: center; font-weight: normal; padding: 10px 0; background:#FFF; border: 1px solid #666; }
	.curCol h2 { text-align: center; font-weight: normal; padding: 10px 0; background:#FFF; border: 1px solid #666; border-top:none; border-left:none; border-right:none; }*/
	
	.image { height: 100px; text-indent: -9999px; display: block; border-right: 1px solid white; }
	
	/*.dth { padding: 5px; background: #900; color: white; border-bottom: 1px solid white; border-right: 1px solid white; font-size:14px; }
	.ddh { position: absolute; left: -9999px; top: -9999px; width: 449px; background: #900; padding: 10px; color: white; border-right: 1px solid white; font-size:11px; display:inline-block; }
	*/
	
	.dt { padding: 5px; color: #666; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:#ddd;}
	.dt:hover { padding: 5px; color: #333; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:#ddd;}
	
	.dtde { padding: 5px; color: #666; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png);}
	.dtde:hover { padding: 5px; color: #333; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; font-size:12px; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top-over.png);}
	
	.dtsel { padding: 5px; color: #000; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png); cursor:none; text-shadow: 0px 1px 0px white;}
	.dtsel:hover { padding: 5px; color: #000; font-weight:bold; border-bottom: 1px solid white; border-right: 1px solid #ccc; border-top:1px solid #cccccc; text-shadow: 0px 1px 1px #e5e5ee; background:url(../Rules/ProjectManagement/images/blu-grd-top.png); cursor:none; text-shadow: 0px 1px 0px white;}
	
	.dd { position: absolute; left: -9999px; top: -9999px; width: 449px; padding: 10px; color: #666; border-right: 1px solid #ccc;; font-size:12px; display:inline-block; background:url(../Rules/ProjectManagement/images/wht-grd.png); }
		
	
	.curCol { position: relative; border: 1px solid #333; }

	.FullWidth
	{
		width: 100%;
		min-width:: 100%;
		max-height:225px;
	}
	
/* the input field */
.date {
	border:1px solid #ccc;
	font-size:18px;
	padding:4px;
	text-align:center;
	width:194px;
	
	-moz-box-shadow:0 0 10px #eee inset;
	-webkit-box-shadow:0 0 10px #eee inset;
}

/* calendar root element */
#calroot {
	/* place on top of other elements. set a higher value if nessessary */
	z-index:10000;
	
	margin-top:-1px;
	width:198px;
	padding:2px;
	background-color:#fff;
	font-size:11px;
	border:1px solid #ccc;
	
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	
	-moz-box-shadow: 0 0 15px #666;
	-webkit-box-shadow: 0 0 15px #666;	
}

/* head. contains title, prev/next month controls and possible month/year selectors */
#calhead {	
	padding:2px 0;
	height:22px;
} 

.savebtn {height:36px; width:135px; background:url(../Rules/ProjectManagement/images/savebtn.png); border:none;}
.savebtn:hover {height:36px; width:135px; background:url(../Rules/ProjectManagement/images/savebtn-o.png); border:none;}

#caltitle {
	font-size:14px;
	color:#0150D1;	
	float:left;
	text-align:center;
	width:155px;
	line-height:20px;
	text-shadow:0 1px 0 #ddd;
}

#calnext, #calprev {
	display:block;
	width:20px;
	height:20px;
	background:transparent url(../Rules/ProjectManagement/prev.gif) no-repeat scroll center center;
	float:left;
	cursor:pointer;
}

#calnext {
	background-image:url(../Rules/ProjectManagement/next.gif);
	float:right;
}

#calprev.caldisabled, #calnext.caldisabled {
	visibility:hidden;	
}

/* year/month selector */
#caltitle select {
	font-size:10px;	
}

/* names of the days */
#caldays {
	height:14px;
	border-bottom:1px solid #ddd;
}

#caldays span {
	display:block;
	float:left;
	width:28px;
	text-align:center;
}

/* container for weeks */
#calweeks {
	background-color:#fff;
	margin-top:4px;
}

/* single week */
.calweek {
	clear:left;
	height:22px;
}

/* single day */
.calweek a {
	display:block;
	float:left;
	width:27px;
	height:20px;
	text-decoration:none;
	font-size:11px;
	margin-left:1px;
	text-align:center;
	line-height:20px;
	color:#666;
	-moz-border-radius:3px;
	-webkit-border-radius:3px; 		
} 

/* different states */
.calweek a:hover, .calfocus {
	background-color:#ddd;
}

/* sunday */
a.calsun {
	color:red;		
}

/* offmonth day */
a.caloff {
	color:#ccc;		
}

a.caloff:hover {
	background-color:rgb(245, 245, 250);		
}


/* unselecteble day */
a.caldisabled {
	background-color:#efefef !important;
	color:#ccc	!important;
	cursor:default;
}

/* current day */
#calcurrent {
	background-color:#498CE2;
	color:#fff;
}

/* today */
#caltoday {
	background-color:#333;
	color:#fff;
}
	
</style>

<script TYPE="text/javascript">
	$(function()
	{	
	

	} );

	
	function UpdateUserAccount()
	{
		
		if($("#inpPhone").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid phone number.\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							

			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=UpdateUserAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpUserId : $("#UpdateUserAccountDiv #inpUserId").val(),inpUserLevelId : $("#UpdateUserAccountDiv #inpUserLevelId").val(), inpPrimaryPhone : $("#UpdateUserAccountDiv #inpPrimaryPhone").val(), inpAddress1 : $("#UpdateUserAccountDiv #inpAddress1").val(),inpAddress2 : $("#UpdateUserAccountDiv #inpAddress2").val()}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User Account has been updated.", "Success!", function(result) 
																			{ 
																				$("#loadingDlgUpdateCompanyAccount").hide();
																				UpdateUserAccountDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("User has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;

	}
		function updateTask(inpTaskId){
			var valueId='itemValue'+inpTaskId;
			var inpTaskValue=document.getElementById(valueId).value;
		if($("#valueId").val() == "")
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("You must input a valid value\n", "Failure!", function(result) { $("#loadingDlgUpdateCompanyAccount").hide(); return false; } );							

			return false;	
		}

	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=UpdateTaskValue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { inpTaskId : inpTaskId,inpTaskValue : inpTaskValue}, 
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Task Value has been updated.", "Success!", function(result) 
																			{ 
																				
																				
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Task has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgAddNewCompanyAccount").hide();
			} );		
	
		return false;			
			
	}
</script>

<div style="margin-bottom:10px; position:relative; display:inline-block; width:100%; border-top:1px solid ##cfe7fe;">
		<div style="margin-bottom:5px; float:left; width:300px">Task Item<br />
			
		</div>
		
		<div style="margin:0 0 5px 10px; float:left;">Value<br />
			
		</div>
		
		<div style="margin:0 0 5px 10px; float:right;"><br />
			
		</div>
</div>
<cfoutput query="getTaskList">
<div style="margin-bottom:10px; position:relative; display:inline-block; width:100%; border-top:1px solid ##cfe7fe;">
		<cfif #required_int# eq 1>
			<cfset bgColor="Red">
			<cfelse>
			<cfset bgColor="Black">
		</cfif>
		<div style="margin-bottom:5px; float:left; width:300px"><br />
			<input type="text" name="element" value="#description#" style="width:300px;border:0;font-family:arial;color:#bgColor#;font-size:14px;" disabled="true"/>
		</div>
		
		<div style="margin:0 0 5px 10px; float:left;"><br />
			<textarea name="itemValue#id#" id="itemValue#id#" style="width:400px;">#Value_vch#</textarea>
		</div>
		
		<div style="margin:0 0 5px 10px; float:right;"><br />
			<input type="button" name="UpdateTaskButton" value="Update" onClick="updateTask(#id#);"/>
		</div>

</cfoutput>