<script TYPE="text/javascript">

	OldGroupName = "";

	function UpdatePAssword()
	{					
		$("#loadingDlgUpdatePassword").show();		
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/users.cfc?method=UpdatePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',    
		dataType: 'json',
		data:  { inpOldPassword: $("#UpdatePasswordDiv #inpOldPassword").val(), inpNewPasswordA : $("#UpdatePasswordDiv #inpNewPasswordA").val(), inpNewPasswordB : $("#UpdatePasswordDiv #inpNewPasswordB").val()},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) { jAlert("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{													
					
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Old password has been updated to new one.", "Success!", function(result) 
																			{ 
																				$("#loadingDlgUpdatePassword").hide();																			
																				ChangePasswordDialog.remove(); 
																			} );								
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error - Old password has NOT been updated.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					$("#loadingDlgUpdatePassword").hide();
			} 		
			
		});
				
	
		return false;

	}
	
	
	$(function()
	{				
			
		$("#UpdatePasswordDiv #UpdatePasswordButton").click( function() { UpdatePAssword(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#UpdatePasswordDiv #Cancel").click( function() 
			{
					$("#loadingDlgUpdatePassword").hide();					
					ChangePasswordDialog.remove(); 
					return false;
			}); 		
		
		$("#loadingDlgUpdatePassword").hide();	
		  
	} );
		
		
</script>


<style>

#UpdatePasswordDiv
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 375px;
	height: 375px;
	font-size:12px;
}


#UpdatePasswordDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#UpdatePasswordDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#UpdatePasswordDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

</style> 

<cfoutput>
        
<div id='UpdatePasswordDiv' class="RXForm">
        
    <div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Update Your Account Password</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img src="../../public/images/Lock-icon_Web.png" /></div>

		<div style="margin: 10px 5px 10px 20px; line-height: 20px;">
         	<ul>            	                              
                <li>Press "Update" button when ready.</li>
                <li>Press "Cancel" button to exit without changes.</li>
            </ul>
            
            <BR />
            <BR />
            <i>DEFINITION:</i> Your password is used to secure your account from unauthorized access. Use of strong passwords will help prevent unwanted usage.
            
        </div>
            
	</div>
    
    <div id="RightStage">
    
        <form id="UpdatePasswordForm" name="UpdatePasswordForm" action="" method="POST">
        
                <label>Enter your old password</label>
                <span class="small"></span>
                <input TYPE="password" name="inpOldPassword" id="inpOldPassword" size="255" /> 
                
                <br/>
                
                <label>Enter a new password</label>
                <span class="small"></span>
                <input TYPE="password" name="inpNewPasswordA" id="inpNewPasswordA" size="255" /> 
                
                <br/>
                
                   <label>Enter a new password</label>
                <span class="small"></span>
                <input type="password" name="inpNewPasswordB" id="inpNewPasswordB" size="255" /> 
                
                <BR>
                        
                <button id="UpdatePasswordButton" TYPE="button" class="ui-corner-all">Update</button>
                <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
                
                <div id="loadingDlgUpdatePassword" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
                        
        </form>
	
    </div>
</div>


</cfoutput>





















