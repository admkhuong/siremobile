


<cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#HOME_TITLE#">
</cfinvoke>

<cfset param = '?_search=#_search#&nd=#nd#&rows=#rows#&page=#page#&sidx=#sidx#&sord=#sord#'>

<!--- Check for Reporting Permission - if not send to home page --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfoutput>
		    
</cfoutput>




<style>
	
		
</style>



<style>


	
.nav-cell-outer
{
	margin-bottom:1em;	
}

.nav-cell:hover
{
	border-radius: 8px;
	border: 2px solid white;
    box-shadow: 0 0 6px 2px #4cb1ff;
	color:#222;
	margin: -2px;
   
}

.nav-cell a
{
	text-decoration:none;	
	color:#666;	
}

.nav-cell a:hover
{
	text-decoration:none;	
	color:#222;
}


.thumbnail-title {
    background-color: #f2f2f2;
    font-size: 13px;
    line-height: 30px;
    text-overflow: ellipsis;
    white-space: nowrap;
	width:100%;
	text-align:center;
	text-overflow: ellipsis;	
	border-radius: 0 0 8px 8px;
	overflow: hidden;
   	padding: 0 1em 0 1em;
}

.thumbnail
{
	  border-radius: 8px 8px 0 0;
	  transition: opacity 100ms ease-out 0s;
	
}

.thumbnail:hover
{
	
	
}

</style>



<script type="text/javascript">

	$(function(){
		<!---$('#subTitleText').hide();--->
		
		<!--- Don't overide titles with pickers --->
		$('#mainTitleText').html('Home');
		$('#subTitleText').html(' ');  <!--- Target - Engage - Deliver --->		
	
		<!---$("#testLoad").load("http://cppseta.com/home/3953024561");  ---> 
		
		
		$('#MenuTipSlide').click(function(){
			$('#MenuTipSlide').hide();
		});
		
		
		setTimeout(function() {
			$('#MenuTipSlide').fadeOut('fast');
		},5000);

	});

</script>

<div id="testLoad"></div>



<cfoutput>

<!---
#Session.CompanyUserId#
<BR />
#Session.UserID#
<BR />
--->
	
<!---	
<div id="MenuTipSlide"><img src="#rootUrl#/#publicPath#/images/m1/leftarrowicon.png" style="float:left; margin-left:8px;"  /><div style="padding-top:3px; margin-left:8px; float:left;">Tip: Click left bars for slide menu</div></div>
 --->   
    
    

<!---wrap the page content do not style this--->
<!--- /#page-content --->
<div id="page-content">
   
  <!--- /.container --->
  <div class="container" >
  
    <!---<h1 class="no-margin-top">SMS Content Management System</h1>--->
     
                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center; margin-bottom:2em;">
                	<h1>Quickstart something new.</h1>
                    <h3 style="text-transform: none;">When&nbsp;you&nbsp;use&nbsp;amazing&nbsp;tools, you&nbsp;can&nbsp;create&nbsp;amazing&nbsp;things.</h3>
                </div>
                               
                <div style="clear:both;">
                
                
                <div class="row">
                
	               <cfif permissionObject.havePermission(EMS_Add_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
                            <div class="nav-cell">
                                <a href='#rootUrl#/#SessionPath#/ems/createnewems' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Express Messaging System (EMS)</div>
                                </a> 
                            </div>        
                        </div>
                    </cfif>
                          
                    <cfif permissionObject.havePermission(Cpp_Title).havePermission>          
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/ire/cpp' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/cpp_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Customer Preference Portals</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(EMS_List_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/ems/campaigncontrolconsole' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/console_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Command &amp; Control Console</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(Reporting_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/ems/campaigncontrolconsole' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/reporting_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Reports, Exports, and Dashboards</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(Add_Contact_Group_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/contacts/grouplist' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/contact_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Contact Manager</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(SMS_Campaigns_Management_Title).havePermission>  
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/sms/list' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/smsic_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">SMS Campaign Management</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(EMS_Voice_Title).havePermission> 
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/campaign/batch/savenewbatch' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ivr_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Create Voice Content</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(EMS_Email_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/email/ebm_email_create' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/email_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Create eMail Content</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(SMS_Campaigns_Management_Title).havePermission> 
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/sms/smsqa' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/smsqa_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">SMS QA Tool</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif AgentToolsPermission.havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/agents/portallist' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/agentportal_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">Agent Portals</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <cfif permissionObject.havePermission(EMS_Add_Title).havePermission>
                        <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                            <div class="nav-cell">
                            	<a href='#rootUrl#/#SessionPath#/abcampaign/createabcampaign' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ab_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">A/B Campaign Tools</div>
                                </a> 
                            </div>
                        </div>
                    </cfif>
                    
                    <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">
                        <div class="nav-cell">
                        	<a href='#rootUrl#/#SessionPath#/administration/administration' class="">
                                    <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                        <img src="#rootUrl#/#PublicPath#/home7assets/images/home/settings_web.png" alt="">	              
                                    </div>
                                 
                                    <div class="thumbnail-title">My Account</div>
                                </a> 
                        </div>
                    </div>
                  
				</div>
               
                <div id="inner-bg-alt" style="min-height: 650px; margin-top:125px;">
                    <div id="content" style="border-bottom: 1px solid ##ebebeb; height:620px; text-align:center;">
                            
                        <h2 class="super">Cross-Channel - Coordinating Between Channels</h2>                        
                        <img style="margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                                     
                    </div>
                    
                </div>
    
     
    	<p class="lead"></p>
    	
        <row>
        	
            <div class="">
                        
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content ---> 
      
</cfoutput>
