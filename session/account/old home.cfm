


<cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#HOME_TITLE#">
</cfinvoke>

<cfset param = '?_search=#_search#&nd=#nd#&rows=#rows#&page=#page#&sidx=#sidx#&sord=#sord#'>

<!--- Check for Reporting Permission - if not send to home page --->
<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfoutput>
		    
</cfoutput>




<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }

	h3
	{
		margin-bottom:10px;	
		
	}
	
	#content li
	{
		margin:8px;	
	}
		
	.white
	{		
		color:#FFFFFF;
	}
		
</style>



<style>


	#innertube
	{
		width:100% !important;	
		
	}
	
	h2.super
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	h2, legend, caption 
	{
		font-size: 1.3em;
		font-weight: 500;
	}
	
	h2 
	{
		font-size: 24px;
		font-weight: 700;
	}
	
	h3 
	{
    	margin-bottom: 10px;
	}
	
	h3 
	{
		font-size: 18px;
		font-weight: 200;
	}

	#inner-bg-alt {
		background-color: #ffffff;
		float: left;
		margin-top: 6px;
		min-height: 577px;
		padding-bottom: 0px;
		width: 96%;
	}

	.img-overlay {
		height: 100%;
		left: 0;
		opacity: 0;
		position: absolute;
		top: 0;
		transition: opacity 0.3s ease 0s;
		width: 100%;
		background-color: #fa7d2b; <!--- fa7d2b f64634 --->
		border-radius: 5px;
		z-index:100;

	}
		
	#inner-bg-alt:hover .img-overlay
	{		
		opacity: 0.3;	
		cursor:pointer;	
	}

	.OverTitle
	{
		z-index:120;		
	}

	.ItemTitle
	{		
		padding:10px;
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		z-index:120;	
	}

	.ItemLink
	{		
		width:590px; 
		
		height:40px;
		vertical-align:central;
			
	}
	
	.ItemLink:hover
	{	
				
		background-color: rgba(250, 125, 43, .3);
		transition: opacity 0.3s ease 0s;
	}
	
	
	.LinkItemQAS ul.c 
	{
		text-align:center;
		display:table;
		list-style-type: none;
	}
	
	.LinkItemQAS ul li 
	{   
		float:left;    
	}
	
	.LinkItemQAS ul li a 
	{
		text-decoration:none;
		padding:2px 12px;
		width:580px;
		height:35px;
		display:table-cell;
		vertical-align:middle;
		text-align:left;
		color: #15436c;
    	font-size: 22px;
	    font-weight: normal;
		-moz-border-radius: 8px;
        border-radius: 8px;
	}
	
	.LinkItemQAS ul li a:hover 
	{
		background-color: rgba(250, 125, 43, .3);
		transition: opacity 0.3s ease 0s;
	}


	#MenuTipSlide
	{
		position:absolute;
		top:0px;
		left:0px;
		width: 300px;
		height: 25px;
		border:#C00 1px solid;
		background-color: rgba(250, 125, 43, .8);
		transition: opacity 0.3s ease 0s;
		-moz-border-radius: 8px;
        border-radius: 8px;
		vertical-align:central;
	}
	
	
	
	
	<!--- account home styles --->
	
	#innertube 
	{
    	margin: 0 23px 0 0;
    }

	.multilevelpushmenu_wrapper {
    	z-index: 3500;
	}

	.account-home
	{		
 	   	height: 100%;
    	width: 100%;	
		height:100px;
		position: relative;
	}
	
	
	.copy
	{
		width: 100%;
		position:relative;		
	}
	
	.main-content 
	{
		margin-left: auto;
   	 	margin-right: auto;
		position: absolute;
		text-align: center;
		vertical-align: middle;
		top: 50%;
		width: 100%;
		z-index: 6;
		
		height:300px;
		
	}

	
	.main-content:after
	{
		background: linear-gradient(to bottom, rgba(237, 237, 237, 0) 0%, rgba(237, 237, 237, 0.7) 25%, rgba(237, 237, 237, 0) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
		content: "";
		height: 100px;
		left: 0;
		margin-top: 208px;
		position: absolute;
		top: 50%;
		width: 100%;
	}
	
	.main-content h1
	{
		font-size:36px;
	}
	
	.promos 
	{
    	border-bottom: 4px solid #ebebeb;
		clear:both;
		margin: 10px 0px;
	}
	
	.promos ul 
	{
		padding-top:50px;
		margin: 0;
		position: relative;
		z-index: 100;
	}
	
	.promos ul:hover 
	{
		cursor:pointer;
	}

	<!---.promos ul:before, 
	.promos ul:after 
	{
		content: " ";
		display: table;
	}
	
	.promos ul:after 
	{
    	clear: both;
	}--->

	.promos li 
	{		
		float: left;
		list-style: outside none none;
		margin: 0;
		min-height: 200px;
		min-width:250px;
		padding: 0;
		position: relative;
		width: 25%;	
		z-index:101;
	}
	
	.promos li:hover 
	{	
		cursor:pointer;
		
	}

	.promo
	{	
		background-color: #ebebeb;
		background-position: center top;
		background-repeat: no-repeat;
		border-left: 2px solid #fff;
		border-right: 2px solid #fff;
		border-top: 1px solid #fff;
		border-bottom: 1px solid #fff;
		color: transparent;
		display: block;
		font: 0px/0 a;
		min-height: 200px;		
	}
	
	.promo:hover 
	{	
		cursor:pointer;
		background-color: #fff;
		border: #333 solid 1px; 
		border-left: 1px solid #333;
		border-right: 1px solid #333;
	}
	
	
	.EMSBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/emspromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.SMSBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/smspromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.CCBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/listcamppromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.CPPBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/cpppromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.CONTACTSSBG
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/contactspromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.SMSQABG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/smsqapromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
		
	.ABBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/abpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.DASHBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/dashpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.PORTALBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/portalpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.ADMINBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/adminpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.EMAILBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/emailpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.IVRBG 
	{
   		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/ivrpromoweb.png</cfoutput>");
   		background-repeat: no-repeat;
    	background-size: 640px 200px;
	}
	
	.cimages
	{
		width:100%;
		height:200px;
		margin-left:20PX;		
	}
	
	.cimages div
	{
		float: left;
		list-style: outside none none;
		margin: 0;
		min-height: 200px;
		min-width:250px;
		padding: 0;
		position: relative;
		width: 25%;	
		z-index:2500;	
	}
	
	.emsrabbit
	{
		background-size: 100% 100%;
		background-image: url("<cfoutput>#rootUrl#/#publicPath#/images/m1/emsrabbitweb.png</cfoutput>");
		background-size: contain;
		background-repeat:no-repeat;
		background-position: center center;
		
	}


</style>



<script type="text/javascript">

	$(function(){
		<!---$('#subTitleText').hide();--->
		
		<!--- Don't overide titles with pickers --->
		$('#mainTitleText').html('Home');
		$('#subTitleText').html(' ');  <!--- Target - Engage - Deliver --->		
	
		<!---$("#testLoad").load("http://cppseta.com/home/3953024561");  ---> 
		
		
		$('#MenuTipSlide').click(function(){
			$('#MenuTipSlide').hide();
		});
		
		
		setTimeout(function() {
			$('#MenuTipSlide').fadeOut('fast');
		},5000);

	});

</script>

<div id="testLoad"></div>



<cfoutput>

<!---
#Session.CompanyUserId#
<BR />
#Session.UserID#
<BR />
--->
	
	<div id="MenuTipSlide"><img src="#rootUrl#/#publicPath#/images/m1/leftarrowicon.png" style="float:left; margin-left:8px;"  /><div style="padding-top:3px; margin-left:8px; float:left;">Tip: Click left bars for slide menu</div></div>
    
    
    
    
    
    
    <div class="account-home">
    
    	
        <div class="main-content">
    
		    <div class="copy">
				<h1>Quickstart something new.</h1>
				<h3>When&nbsp;you&nbsp;use&nbsp;amazing&nbsp;tools, you&nbsp;can&nbsp;create&nbsp;amazing&nbsp;things.</h3>
				<!---<ul class="links">
					<li data-analytics-region="view the gallery"><span class="icon icon-more">View the gallery</span></li>
				</ul>--->
			</div>
        
        </div>
         
        
            
            
    
    
    </div>
    
    <div class="cimages">
    
    	<div class="emsrabbit"></div>
        <div class="emsrabbitb"></div>
        <div class="emsrabbitc"></div>
    
    </div>
    

    <div class="promos">
         	
        <ul>
        	<cfif permissionObject.havePermission(EMS_Add_Title).havePermission>
	            <li>
	                <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="promo EMSBG">Create New Campaign - e-Messaging System (EMS)</a>
	            </li>
            </cfif>
            
            <cfif permissionObject.havePermission(Cpp_Title).havePermission>
	            <li>                
	                <a href='#rootUrl#/#SessionPath#/ire/cpp' class="promo CPPBG">Customer Preference Portals</a>
	            </li>
            </cfif>
            
            <cfif permissionObject.havePermission(EMS_List_Title).havePermission>
	            <li>
	                <a href='#rootUrl#/#SessionPath#/ems/campaigncontrolconsole' class="promo CCBG">Campaigns and Content</a>
	            </li>
            </cfif>
            
            <cfif permissionObject.havePermission(Reporting_Title).havePermission>
	            <li>
	                <a href='#rootUrl#/#SessionPath#/ems/campaigncontrolconsole' class="promo DASHBG">Reports, Exports, and Dashboards</a>
	            </li>
            </cfif>
    
    		<cfif permissionObject.havePermission(Add_Contact_Group_Title).havePermission>
	            <li>
	                <a href='#rootUrl#/#SessionPath#/contacts/grouplist' class="promo CONTACTSSBG">Contact Manager</a>
	            </li>
            </cfif>
                 
            <cfif permissionObject.havePermission(SMS_Campaigns_Management_Title).havePermission>          
	            <li>
	                <!---<a href='#rootUrl#/#SessionPath#/ire/smscampaign/sms' class="promo SMSBG">SMS Campaign MAnagement</a>--->
                    <a href='#rootUrl#/#SessionPath#/sire/sms/list' class="promo SMSBG">SMS Campaign MAnagement</a>
	            </li>
            </cfif>
            
            <cfif permissionObject.havePermission(EMS_Voice_Title).havePermission>         
	            <li>
	                <a href='#rootUrl#/#SessionPath#/campaign/batch/savenewbatch' class="promo IVRBG">Create Voice Content</a>
	            </li>
            </cfif>
            
            <cfif permissionObject.havePermission(EMS_Email_Title).havePermission>
	            <li>
	                <a href='#rootUrl#/#SessionPath#/email/ebm_email_create' class="promo EMAILBG">Create eMail Content</a>
	            </li>
            </cfif>
            
            <li>
                <a href='#rootUrl#/#SessionPath#/sms/smsqa' class="promo SMSQABG">SMS QA Tool</a>
            </li>
                        
			<cfif AgentToolsPermission.havePermission>
                <li>
                    <a href='#rootUrl#/#SessionPath#/agents/portallist' class="promo PORTALBG">Agent Portals</a> 
                </li>
            </cfif>
            
            <li>
                <a href='#rootUrl#/#SessionPath#/abcampaign/createabcampaign' class="promo ABBG">A/B Campaign Tools</a>
            </li>
             
            <li>
                <a href='#rootUrl#/#SessionPath#/administration/administration' class="promo ADMINBG">My Account</a>
            </li>
            
            
        </ul>
     
     
     </div>   
     
   
   
    <div id="inner-bg-alt" style="min-height: 650px; margin-top:125px;">
        <div id="content" style="border-bottom: 1px solid ##ebebeb; height:620px; text-align:center;">
                
            <h2 class="super">Cross-Channel - Coordinating Between Channels</h2>                        
            <img style="margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                         
        </div>
        
    </div>
    
     
    <!---  <div class="promos">
         	
        <ul>
            <li>
                <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="promo SampleReportBG" onclick="#rootUrl#/#SessionPath#/ems/createnewemergencymessage">Create New Campaign - e-Messaging System (EMS)</a>
            </li>
            
            <li>
                <div class="promo">Agent Portal</div> <!--- SMS, Content, other ?--->
            </li>
            
            <li>
                <div class="promo">CONTACTS</div>
            </li>
            
            <li>
                <div class="promo">REPORTS</div>
            </li>
        </ul>
     
     
     </div>   --->
         
         
    
    
	
 	<!---<h2 class="super">Quick Links</h2>
 
 	<cfif contactPermission.havePermission>
        <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
                                        
                <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                    
                    <ul id='ul1' class='c'>
                        <cfif contactPermission.havePermission>
                            <li><a href='#rootUrl#/#SessionPath#/contacts/addgroup'>Create New Group of Contacts</a></li>
                            <li><a href='#rootUrl#/#SessionPath#/contacts/grouplist'>List Groups of Contacts</a></li>              
                        </cfif>
                    </ul>    
                                 
                </div>
                             
            </div>
        </div>
    </cfif>
    
    <cfif campaignCreatePermission.havePermission OR campaignPermission.havePermission >
        <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
                            
                <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                  
                     <ul id='ul1' class='c'>
                        <cfif campaignCreatePermission.havePermission>
                            <li><a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage'>Create New Campaign - e-Messaging System (EMS)</a></li>
                        </cfif>
                        
                        <cfif campaignCreatePermission.havePermission>                    
                            <li><a href='#rootUrl#/#SessionPath#/ems/listemergencymessages'>List Campaigns</a></li>              
                        </cfif>
                    </ul>    
                    
                </div>
                             
            </div>
        </div>
    </cfif>
    
    <cfif cppPermission.havePermission>
        <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
               
                <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                    
                    <ul id='ul1' class='c'>
                        <cfif cppPermission.havePermission>
                            <li><a href='#rootUrl#/#SessionPath#/ire/cpp/createCPP'>Create New Customer Preference Portal (CPP)</a></li>
                            <li><a href='#rootUrl#/#SessionPath#/ire/cpp/'>List Customer Preference Portal(s) (CPP)(s)</a></li>              
                        </cfif>
                    </ul>    
                                 
                </div>
                       
            </div>
        </div>
    </cfif>
    
    <cfif AgentToolsPermission.havePermission>
        <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
            <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
               
                <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                    
                    <ul id='ul1' class='c'>
                        
                            <li><a href='#rootUrl#/#SessionPath#/agents/portallist'>Agent Portal Tools</a></li>
                                          
                        
                    </ul>    
                                 
                </div>
                       
            </div>
        </div>    
    </cfif>
    
    
   
    
   
    <div id="inner-bg-alt" style="min-height: 650px; margin-top:25px;">
        <div id="content" style="border-bottom: 1px solid ##ebebeb; height:620px; text-align:center;">
                
            <h2 class="super">Cross-Channel - Coordinating Between Channels</h2>                        
            <img style="margin-top:50px;" src="#rootUrl#/#publicPath#/images/m1/crosschannel.png" width="480" height="400" />
                         
        </div>
        
    </div>--->
    
      
</cfoutput>

<!---
<cfoutput>
Session.DBSourceEBM=#Session.DBSourceEBM#
<BR />
ESIID = #ESIID#
<BR />
CGI.SERVER_NAME=#CGI.SERVER_NAME#
<BR />
SphinxConfigPath = #SphinxConfigPath#

</cfoutput>--->