


<cfoutput>

	<link href="#rootUrl#/#PublicPath#/css/newtab.css" rel="stylesheet">

</cfoutput>

<style>

.nav-cell-outer
{
	margin-bottom:1em;	
}

.nav-cell:hover
{
	border-radius: 8px;
	border: 2px solid white;
    box-shadow: 0 0 6px 2px #4cb1ff;
	color:#222;
	margin: -2px;
   
}

.nav-cell a
{
	text-decoration:none;	
	color:#666;	
}

.nav-cell a:hover
{
	text-decoration:none;	
	color:#222;
}


.thumbnail-title {
    background-color: #f2f2f2;
    font-size: 13px;
    line-height: 30px;
    text-overflow: ellipsis;
    white-space: nowrap;
	width:100%;
	text-align:center;
	text-overflow: ellipsis;	
	border-radius: 0 0 8px 8px;
	overflow: hidden;
   	padding: 0 1em 0 1em;
}

.thumbnail
{
	  border-radius: 8px 8px 0 0;
	  transition: opacity 100ms ease-out 0s;
	
}

.thumbnail:hover
{
	
	
}

</style>


<cfoutput>

<!---wrap the page content do not style this--->
<!--- /#page-content --->
<div id="page-content">
   
  <!--- /.container --->
  <div class="container" >
               
                
        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
            <h1>Quickstart something new.</h1>
            <h3 style="text-transform: none;">When&nbsp;you&nbsp;use&nbsp;amazing&nbsp;tools, you&nbsp;can&nbsp;create&nbsp;amazing&nbsp;things.</h3>
        </div>

      <!---  <div class="row">
        
           <!--- <div id="newtab-scrollbox">--->
            
                <!---<div id="newtab-vertical-margin">--->
            
                    <!---  <div id="newtab-margin-top"></div>--->
                      
                      <!---<div id="newtab-horizontal-margin">--->
                           <!--- <div class="newtab-side-margin"></div>--->
                    
                           <!--- <div id="newtab-grid" style="height: 802px; max-height: 272px; max-width: 1551px;">--->
                            
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <div class="newtab-cell">
                                    <div class="newtab-site" draggable="true" type="history">                                    
                                        <a class="newtab-link" title="#rootUrl#/#SessionPath#/ems/createnewemergencymessage" href="#rootUrl#/#SessionPath#/ems/createnewemergencymessage">  
                                        <span class="newtab-thumbnail" style="background-image: url('#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png');"></span>  
                                        <span class="newtab-thumbnail enhanced-content"></span>  
                                        <span class="newtab-title">Express Messaging System (EMS)</span></a>
                                       <!--- <input type="button" title="Pin this site at its current position" class="newtab-control newtab-control-pin"/>
                                        <input type="button" title="Remove this site" class="newtab-control newtab-control-block"/>--->
                                        <span class="newtab-suggested"></span>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <div class="newtab-cell">
                                    <div class="newtab-site" draggable="true" type="history">                                    
                                        <a class="newtab-link" title="#rootUrl#/#SessionPath#/ems/createnewemergencymessage" href="#rootUrl#/#SessionPath#/ems/createnewemergencymessage">  
                                        <span class="newtab-thumbnail" style="background-image: url('#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png');"></span>  
                                        <span class="newtab-thumbnail enhanced-content"></span>  
                                        <span class="newtab-title">Express Messaging System (EMS)</span></a>
                                       <!--- <input type="button" title="Pin this site at its current position" class="newtab-control newtab-control-pin"/>
                                        <input type="button" title="Remove this site" class="newtab-control newtab-control-block"/>--->
                                        <span class="newtab-suggested"></span>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <div class="newtab-cell">
                                    <div class="newtab-site" draggable="true" type="history">                                    
                                        <a class="newtab-link" title="#rootUrl#/#SessionPath#/ems/createnewemergencymessage" href="#rootUrl#/#SessionPath#/ems/createnewemergencymessage">  
                                        <span class="newtab-thumbnail" style="background-image: url('#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png');"></span>  
                                        <span class="newtab-thumbnail enhanced-content"></span>  
                                        <span class="newtab-title">Express Messaging System (EMS)</span></a>
                                       <!--- <input type="button" title="Pin this site at its current position" class="newtab-control newtab-control-pin"/>
                                        <input type="button" title="Remove this site" class="newtab-control newtab-control-block"/>--->
                                        <span class="newtab-suggested"></span>
                                        
                                    </div>
                                </div>
                            </div>
                        	
                            <!---</div>--->              	 
                  	
                   <!--- </div>--->
                    
                  <!---  <div class="newtab-side-margin"></div>--->
              	
               <!--- </div>--->
            
                <!---<div id="newtab-margin-bottom"></div>--->
            
        	<!---</div> --->  
          
        </div>--->
        
        
        <div class="row" style="margin-top:2em;">
        
			<cfif permissionObject.havePermission(EMS_Add_Title).havePermission>
                <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
                    <div class="nav-cell">
                        <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                            <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                                <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                            </div>
                         
                            <div class="thumbnail-title">Express Messaging System (EMS)</div>
                        </a> 
                    </div>        
                </div>
           	</cfif>
            
            
           <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
           		<div class="nav-cell">
                    <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                        <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                            <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                        </div>
                     
                        <div class="thumbnail-title">Express Messaging System (EMS)</div>
                    </a> 
                </div>        
           	</div>
            
            <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
           		<div class="nav-cell">
                    <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                        <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                            <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                        </div>
                     
                        <div class="thumbnail-title">Express Messaging System (EMS)</div>
                    </a> 
                </div>        
           	</div>
            
            <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
           		<div class="nav-cell">
                    <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                        <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                            <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                        </div>
                     
                        <div class="thumbnail-title">Express Messaging System (EMS)</div>
                    </a> 
                </div>        
           	</div>
            
            <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
           		<div class="nav-cell">
                    <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                        <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                            <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                        </div>
                     
                        <div class="thumbnail-title">Express Messaging System (EMS)</div>
                    </a> 
                </div>        
           	</div>
            
            <div class="col-xs-6 col-sm-6 col-md-3 nav-cell-outer">  
           		<div class="nav-cell">
                    <a href='#rootUrl#/#SessionPath#/ems/createnewemergencymessage' class="">
                        <div class="thumbnail" style="margin-bottom:0; border-bottom:none;">
                            <img src="#rootUrl#/#PublicPath#/home7assets/images/home/ems_web.png" alt="Express Messaging System (EMS)">	              
                        </div>
                     
                        <div class="thumbnail-title">Express Messaging System (EMS)</div>
                    </a> 
                </div>        
           	</div>
         
           
        </div>

                  
 		<p class="lead"></p>
    	
        <row>
        	
            <div class="">
                        
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content ---> 

</cfoutput>

  