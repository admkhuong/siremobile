<script type="text/javascript">
	$(function() 
	{
		var $items = $('#vtab>ul>li.EmailVTab');
		$items.addClass('selected');
	<!--- eMail Tabs--->
		$( "#htabs_Launch" ).tabs({
			cache: true,
			selected: 1,
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"<div id='loadingDlg' style='display:inline;'><img class='loadingDlgRegisterNewAccount' src='<cfoutput>#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#</cfoutput>/images/loading-small.gif' width='20' height='20'></div>" );
				}
			}
		});

		<!--- Doesnt work yet - explore later
			$("#htabs_Launch div li.ui-state-default").css('background', '-moz-linear-gradient(center top, rgb(185,224,245), rgb(146,189,214) );'); 
		--->
	
		<!--- Re initialize fish-eye menu items on tab show - bug fix IE--->
		$( "#htabs_Launch" ).bind( "tabsshow", function(event, ui) {		
			switch(parseInt(ui.index))
			{
				case 5:
				
					$('#dock_Scripts').Fisheye(
						{
							maxWidth: 30,
							items: 'a',
							itemsText: 'span',
							container: '.dock-container_Scripts',
							itemWidth: 50,
							proximity: 60,
							alignment : 'left',
							valign: 'bottom',
							halign : 'left'
						}
					);				
					break;
				
				case 2:				
					$('#tab-panel-BabbleBuddies #dockBabbleBuddies').Fisheye(
							{
								maxWidth: 30,
								items: 'a',
								itemsText: 'span',
								container: '.dock-container_BabbleBuddies',
								itemWidth: 50,
								proximity: 60,
								alignment : 'left',
								valign: 'bottom',
								halign : 'left'
							}
					);	
					break;
					
				case 1:
					$('#tab-panel-BabbleBlasts #dockBabbleBlasts').Fisheye(
						{
							maxWidth: 30,
							items: 'a',
							itemsText: 'span',
							container: '.dock-container_Blaster',
							itemWidth: 50,
							proximity: 60,
							alignment : 'left',
							valign: 'bottom',
							halign : 'left'
						}
					);				
					break;		
				
			}
			
		});
		
		$(".tab_link").unbind("click");
		$(".tab_link").click(function() {
			window.location = $(this).attr("showLink");
		}); 
	});
</script>
<cfoutput>
	<div id="htabs_Launch">
		<ul style="height:30px;"> 
			<li><a showLink="activity" class="tab_link" href="../batch/dsp_Batches">Activity</a></li>        
			<li><a showLink="postingFacebook" class="tab_link" href="../batch/dsp_ReportFacebookPosting">Report posting facebook</a></li>         
		</ul>
	</div>
</cfoutput>