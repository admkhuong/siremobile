<!---
If also don't want the user to be automatically logged
in again, so remove the client cookies.
--->

<!--- De-Authorize Local Device--->
<cfparam name="DALD" default="0">
<cfparam name="LODest" default="" />


<!---<cftry>--->

	<cfif DALD GT 0 >
   		 <cfcookie name="MFAEBM#Session.USERID#" value="" expires="now" />     
    </cfif>	
    
    <cfcookie name="RememberMe" value="" expires="now" />    
    <cfcookie name="CFID" value="" expires="NOW" />
	<cfcookie name="CFTOKEN" value="" expires="NOW" />

    <cfset OnSessionEnd(session) >
    <cfset StructClear(session)>   
    
<!---<cfcatch TYPE="any">

</cfcatch>

</cftry>--->

<cfset Session.USERID = "">

<cflogout />



<cfif BrandingLOLink NEQ "">
	<cflocation url="#BrandingLOLink#" addtoken="false">
<cfelse>    
	<cflocation url="#rootUrl#/#PublicPath#/home" addtoken="false">
</cfif>