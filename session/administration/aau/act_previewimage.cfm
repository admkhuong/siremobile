<cfparam name='userId' default="">
<cfif isDefined("userId")>
	<cfset AVATARFOLDER = "AAUAvatar">
	<cfset localImageFolder = "#rxdsWebProcessingPath#\#AVATARFOLDER#/U#userId#\">
        <cfif DirectoryExists("#localImageFolder#")>
			<cfdirectory
				action="list"
				directory="#localImageFolder#"
				recurse="true"
				listinfo="name"
				name="qFile"
			/>
			<cfset avatarFileName="">
			<!--- Loop through file query and get the image avatar which start with "avatar."--->
			<cfloop query="qFile">
				<cfif LCase(qFile.name).startsWith("avatar.")>
			    	<cfset avatarFileName = qFile.name>
					<cfbreak>
				</cfif>
			</cfloop>
			<cfif avatarFileName NEQ "">
				<cftry>
					<cfset MainLibFile = "#rxdsWebProcessingPath#\#AvatarFolder#/U#userId#\#avatarFileName#" />
					<cfheader name="cache-control" value="#Now()#"> <!---add header to prevent cache --->
					<cfheader name="Content-Disposition" value="attachment;filename=preview.png">
					<cfcontent TYPE="image/png" file="#MainLibFile#" />
					<cfcatch>
					</cfcatch>
				</cftry>
			</cfif>
		</cfif>
</cfif>
