﻿<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
</cfoutput>

<style>
	@import url('<cfoutput>#rootUrl#/#publicPath#/js/jquery/css/jquery.autocomplete.css</cfoutput>') all;
</style>
<h2>
	Agent Administration
</h2>
<div style="float:right;">
	<a href="" id="AddAgent" onclick="javascript:OpenPopupAddAgent();return false;" class="button filterButton small">Add</a>
</div>

<cfinclude template="dsp_selectmultiple.cfm" >
<cfinclude template="inc_client.cfm" >

<style type="text/css">
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListAgentList_info {
		width: 98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
		
		.paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}
		
		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}
		
		table.dataTable tr td{
			color:#5E6AAD;
		}
		
		table { border-spacing: 0; }
		
		.icon_img {
		    margin-left: 10px;
		}
        #tblListAgentList_wrapper tr td:nth-child(1){
            width: 10%;
        }
        #tblListAgentList_wrapper tr td:nth-child(2){
            width: 20%;
        }
        #tblListAgentList_wrapper tr td:nth-child(3){
            width: 5%;
        }
        #tblListAgentList_wrapper tr td:nth-child(4){
            width: ; 10%
        }
        #tblListAgentList_wrapper tr td:nth-child(5){
            width: 5%;
        }
        #tblListAgentList_wrapper tr td:nth-child(6){
            width: 5%;
        }
        #tblListAgentList_wrapper tr td:nth-child(7){
            width: 5%;
        }
        #tblListAgentList_wrapper tr td:nth-child(8){
            width: 30%;
        }
</style>

<div class="border_top_none">
	<table id="tblListAgentList" class="table" cellspacing=0>
	</table>
</div>
<script>
	var _tblListAgentList;
	//init datatable for active agent
	function InitAgentList(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListAgentList = $('#tblListAgentList').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				{"sName": 'Name', "sTitle": 'Name', "sWidth": '10%',"bSortable": false},
				{"sName": 'Email', "sTitle": 'Email', "sWidth": '20%',"bSortable": false},
				{"sName": 'UserID', "sTitle": 'UserID', "sWidth": '5%',"bSortable": false},
				{"sName": 'CampaignCode', "sTitle": 'CampaignCode', "sWidth": '10%',"bSortable": false},
				{"sName": 'SignUp', "sTitle": 'SignUp', "sWidth": '5%',"bSortable": false},
				{"sName": 'Status', "sTitle": 'Status', "sWidth": '5%',"bSortable": false},
				{"sName": 'Active', "sTitle": 'Active', "sWidth": '5%',"bSortable": false},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '30%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetAgentListDataTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true&isCreateEms=0',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListAgentList').attr('style', '');
				$('#tblListAgentList').css('min-width', '1053px');
			}
	    });
	}
	InitAgentList();
	
	$("#subTitleText").html("Agent administrator");
	$('.revoke_user').css('cursor', 'pointer');
	$(".revoke_user").click(function(){
		var CurrCampaignCode = $(this).attr('rel');
		var CurrEmail =  $(this).attr('emailUser');
		jConfirm( "Are you sure you want to revoke this user?", "Revoke user", function(result) { 
			if(result)
			{	
				revokeUser(CurrCampaignCode,CurrEmail);
			}
			return false;																	
		});
	});
	
	function EditCampaignOfAgent(obj)
	{
		var agentid = $(obj).attr("data-userid");
		var hauinvitationcode = $(obj).attr("data-hauinvitationcode");
		var email = $(obj).attr("data-email");
		//Open popup edit campaign of agent 
		var url = "dsp_editCampaignOfAgent?agentid="+agentid+"&email="+email+"&hauinvitationcode="+hauinvitationcode;
		var title = "Edit invite Campaign code of Agent";
		OpenDialog(url, title, 355, 400);
		return false;
	}
	
	function revokeUser(CurrCampaignCode,CurrEmail)
	{
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=RevokeUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			CAMPAIGNCODE : CurrCampaignCode,
			EMAIL : CurrEmail
			},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				
				if(d.RXRESULTCODE == 1){
					jAlert("Sorry, this user was signup.", "Failure", function(result){
						if(result){
							window.setTimeout('location.reload()', 50);
						}
					});
				}else if(d.RXRESULTCODE == 0){
					jAlert("Success, this user was be revoked.", "Success", function(result){
						if(result){
							window.setTimeout('location.reload()', 50);
						}
					});
				}else{
					jAlert("No Response from the remote server. Check your connection and try again.", "Error");
				}
			} 		
		});
		return false;
	}
	
	function OpenPopupAddAgent()
	{		
		//Open popup invite agent 
		var url = "dsp_inviteAgent";
		var title = "Invite new Agent";
		OpenDialog(url, title, 426, 550,"popupInvite",false);
		return false;
	}
	
	function validateEmail(email) 
	{ 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	} 
	
	function InviteAgent()
	{
		var txtEmailInvitation = $("#txtEmailInvitation").val().trim();
		if (txtEmailInvitation.length == 0) {
			jAlert("Please enter email invitation", "Failure");
			return false;
		}
		var emailArr = txtEmailInvitation.split(",");
		for(var i = 0 ; i<emailArr.length;i++){
			var isEmail = validateEmail(emailArr[i]);
			if (!isEmail) {
				jAlert("Email invitation is not email, please try another", "Failure");
				return false;
			}
		}
		var campainArr = new Array();
		$("#campainToList option").each(function(){
			campainArr.push($(this).val())
		});
		if(campainArr.length==0){
			jAlert("Please choose at least one campain", "Failure");
			return false;
		}
		var data = {
			emailArr: JSON.stringify(emailArr),
			campainArr: JSON.stringify(campainArr)
		};
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'InviteAgent', data, "Invite Agent fail!", function(d){
			jAlert("Agent invited", "Success!", function(result){
				location.reload();
			});
		});
	}
</script> 