﻿<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/datepicker/datepicker.js"></script>
	<style>
		@import url('#rootUrl#/#publicPath#/css/datepicker/base.css');
		@import url('#rootUrl#/#publicPath#/css/datepicker/clean.css');
		@import url('#rootUrl#/#publicPath#/css/style.css');
	</style>	
</cfoutput>
<style type="text/css">
	.border-div{
		border:solid 1px #939292;
		margin-top:10px;
		padding-left:4px;
	}
	.cf_table{
		margin-top:10px;
	}
	tr.agent td{
		background-color:#F2DEDE;
	}
	tr.user td{
		background-color:#DFF0D8;
	}
	.ui-spinner {position: relative}
	.ui-spinner-buttons {position: absolute}
	.ui-spinner-button {overflow: hidden}
	
	.ui-corner-tr {
	    border-top-right-radius: 0px;
	    border: 1px solid rgba(0, 0, 0, 0.3);
	}
	.ui-corner-br {
	    border-bottom-right-radius: 0px;
	    border: 1px solid rgba(0, 0, 0, 0.3);
	}
</style>
<div>
	<div>
		<input type="button" id="btnShowHide" value="Hide Filters" onclick="ShowHideFilter();" style="float:left;" class="button filterButton small"/>
		<input type="button" id="btnRefresh" value="Refresh" style="float:left;" class="button filterButton small">
		<input type="button" id="btnShowThreads" value="Show Threads" style="float:right;" class="button filterButton small" onclick="ShowHideThread();"/>
	</div>
	<div style="clear:both;">
	</div>
	<div>
		<div id="divFilter" style="float:left;width:20%;">
			<div id="divDate" class="border-div">
				<label>Date</label>
				<div>
					<select id ="ddlDate" style="width:95%;">
						<option value="allTime" selected="selected">
							All Time
						</option>
						<option value="customRange">
							Custome Range
						</option>
						<option value="today">
							Today
						</option>
						<option value="yesterday">
							Yesterday
						</option>
						<option value="week">
							This week
						</option>
						<option value="month">
							This month
						</option>
						<option value="year">
							This year
						</option>
					</select>
				</div>
				<div style="margin-top:10px;">
					<div> 
						Start
						<input type="text" id="startTime">
					</div>
					<div>
						End
						<input type="text" id="endTime" style="margin-left:4px;">
					</div>
				</div>
			</div>
			<div id="divKeyword" class="border-div">
				<div>
					<label>Keyword</label>
					<input type="text" id="keyWord" style="width:86%;">
				</div>
				<div>
					<input type="radio" name="keyword" value="all" checked="true">Match all keywords<br>
					<input type="radio" name="keyword" value="any">Match any the keyword
				</div>
			</div>
			<div id="divLimit" class="border-div">
				<div> 
					Limit
				</div>
				<div>
					<input type="radio" name="limit" value="all" id="limitAll" checked="true">All Sessions<br>
					<input type="radio" name="limit" value="early" id="limitEarly">Earliest<input type="text" id="earlyNum" style="margin-left:20px;"><br>
					<input type="radio" name="limit" value="late" id="limitLate">Latest<input type="text" id="lateNum" style="margin-left:30px;">
				</div>
			</div>
			<div style="margin-top:10px;">
				<input type="button" id="btnApply" value="Apply" class="button filterButton small" style="margin-left:20px;"></input>
				<input type="button" id="btnReset" value="Reset" class="button filterButton small" style="margin-left:30px;"></input>
			</div>
		</div>
		<div id="divData" style="float:left;width:80%;">
			<div>
				<table class="cf_table" style="width:100%;">
					<tr>
						<th>
							Date/Time
						</th>
						<th>
							From
						</th>
						<th>
							To
						</th>
						<th>
							Message
						</th>
					</tr>
					<tr class="user">
						<td class="user">
							1/1/2011 5:30PM
						</td>
						<td>
							555-555-1212
						</td>
						<td>
							Agent Name
						</td>
						<td>
							Hi, I need a tool
						</td>
					</tr>
					<tr class="agent">
						<td>
							1/1/2011 5:31PM
						</td>
						<td>
							Agent Name
						</td>
						<td>
							555-555-1212
						</td>
						<td>
							What kind of tool?
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var showFilterState = 1;
	
	$(function(){
		MakeSpinner("earlyNum");
		MakeSpinner("lateNum");
		
		$("#earlyNum").attr('disabled','disabled');
		$("#lateNum").attr('disabled','disabled');
		$("#startTime").attr('disabled','disabled');
		$("#endTime").attr('disabled','disabled');
		
		$("#startTime").datepicker({
			inline: true,
			calendars: 4,
			extraWidth: 100
		});
		
		$("#endTime").datepicker({
			inline: true,
			calendars: 4,
			extraWidth: 100
		});
		
		
		$("#ddlDate").change(function(){
			if($("#ddlDate option:selected").val()=="customRange"){
				$("#startTime").removeAttr('disabled');
				$("#endTime").removeAttr('disabled');
			}
			else{
				$("#startTime").attr('disabled','disabled');
				$("#endTime").attr('disabled','disabled');
			};
		});
		
		//make textbox disable or enabke when selection of radio buttons change 
		$("input[name=limit]:radio").change(function(){
			if($("#limitAll").attr("checked")){
				$("#earlyNum").attr('disabled','disabled');
				$("#lateNum").attr('disabled','disabled');
			}
			else if($("#limitEarly").attr("checked")){
				$("#earlyNum").removeAttr('disabled');
				$("#lateNum").attr('disabled','disabled');
			}else{
				$("#lateNum").removeAttr('disabled');
				$("#earlyNum").attr('disabled','disabled');
			}
		});
	});
	
	//make spinner for textbox
	function MakeSpinner(textBoxId){
		$('#'+textBoxId).spinner({min: 0});
		$('#'+textBoxId).spinner({max: 99});
		$('#'+textBoxId).css( "width", "30px" );
		$('#'+textBoxId).ForceNumericOnly();
	}
	
	//toggle filter div when click show and hide button
	function ShowHideFilter(){
		if(showFilterState==1){
			$("#btnShowHide").val("Show filters");
			showFilterState=0;
			$("#divFilter").toggle(1000);
			$('#divData').css('width', '100%');
		}
		else{
			showFilterState=1;
			$("#btnShowHide").val("Hide filters");
			$('#divData').css('width', '80%');
			$("#divFilter").toggle(1000);
			$("#divFilter").css('width', '20%');
		}
	}
	
	function ShowHideThread(){
		
	}
</script>