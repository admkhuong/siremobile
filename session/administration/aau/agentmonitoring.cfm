﻿<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/DataTables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/DataTables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/DataTables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/administrator/AAU/agentmonitoring.css">
	
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
</cfoutput>
<cfinclude template="inc_client.cfm" >
<div class="header-bar">
	Agent Monitoring
</div>
<div id="divAgent" class="border_top_none">
	<ul>
		<li><a href="#activeAgents">Active Agents</a></li>
		<li><a href="#activeSessions">Active Sessions</a></li>
	</ul>
	<div id="activeAgents">
		<table id="tblActiveAgent" class="table">
		</table>
	</div>
	<div id="activeSessions">
		<table id="ActiveSessionTbl" class="hide" rel="">
			<tr>
				<th>
					Number
				</th>
				<th>
					Messages
				</th>
				<th>
					Duration
				</th>
			</tr>
		</table>
	</div>
</div>

<div class="header-bar m_top_20">
	Performance Metrics
</div>
<div id="tabs" class="border_top_none">
	<ul>
		<li><a href="#tabToday" onclick="GetStatistic('today','Today')">Today</a></li>
		<li><a href="#tabYesterday" onclick="GetStatistic('yesterday','Yesterday')">Yesterday</a></li>
		<li><a href="#tab7day" onclick="GetStatistic('7days','7day')">7 days</a></li>
		<li><a href="#tab30day" onclick="GetStatistic('30days','30day')">30 days</a></li>
		<li><a href="#tabAllTime" onclick="GetStatistic('allTime','AllTime')">All Time</a></li>
	</ul>
	<div id="tabToday">
		<table class="statistic-bold">
			<tr class="none-border-bottom">
				<td >
					<label id="lblTotalSessionToday" ></label>
				</td>
				<td >
					<label id="lblSessionPerHourToday" ></label>
				</td>
				<td >
					<label id="lblMinutesPerSessionToday"></label>
				</td>
				<td >
					<label id="lblMessagesPerSessionToday"></label>
				</td>
			</tr>
			<tr class="footer-text none-border-top">
				<th>
					Total Sessions
				</th>
				<th>
					Sessions/Hour
				</th>
				<th>
					Minutes/Session
				</th>
				<th>
					Messages/Session
				</th>
			</tr>
		</table>
	</div>
	<div id="tabYesterday">
		<table class="statistic-bold">
			<tr class="none-border-bottom">
				<td >
					<label id="lblTotalSessionYesterday" ></label>
				</td>
				<td >
					<label id="lblSessionPerHourYesterday"></label>
				</td>
				<td >
					<label id="lblMinutesPerSessionYesterday"></label>
				</td>
				<td >
					<label id="lblMessagesPerSessionYesterday"></label>
				</td>
			</tr>
			<tr class="footer-text none-border-top">
				<th>
					Total Sessions
				</th>
				<th>
					Sessions/Hour
				</th>
				<th>
					Minutes/Session
				</th>
				<th>
					Messages/Session
				</th>
			</tr>
		</table>
	</div>
	<div id="tab7day">
		<table class="statistic-bold">
			<tr class="none-border-bottom">
				<td >
					<label id="lblTotalSession7day"></label>
				</td>
				<td >
					<label id="lblSessionPerHour7day"></label>
				</td>
				<td >
					<label id="lblMinutesPerSession7day"></label>
				</td>
				<td >
					<label id="lblMessagesPerSession7day"></label>
				</td>
			</tr>
			<tr class="footer-text none-border-top">
				<th>
					Total Sessions
				</th>
				<th>
					Sessions/Hour
				</th>
				<th>
					Minutes/Session
				</th>
				<th>
					Messages/Session
				</th>
			</tr>
		</table>
	</div>
	<div id="tab30day">
		<table class="statistic-bold">
			<tr class="none-border-bottom">
				<td >
					<label id="lblTotalSession30day" ></label>
				</td>
				<td >
					<label id="lblSessionPerHour30day" ></label>
				</td>
				<td >
					<label id="lblMinutesPerSession30day" ></label>
				</td>
				<td >
					<label id="lblMessagesPerSession30day" ></label>
				</td>
			</tr>
			<tr class="footer-text none-border-top">
				<th>
					Total Sessions
				</th>
				<th>
					Sessions/Hour
				</th>
				<th>
					Minutes/Session
				</th>
				<th>
					Messages/Session
				</th>
			</tr>
		</table>
	</div>
	<div id="tabAllTime">
		<table class="statistic-bold">
			<tr class="none-border-bottom">
				<td >
					<label id="lblTotalSessionAllTime" ></label>
				</td>
				<td >
					<label id="lblSessionPerHourAllTime" ></label>
				</td>
				<td >
					<label id="lblMinutesPerSessionAllTime" ></label>
				</td>
				<td >
					<label id="lblMessagesPerSessionAllTime" ></label>
				</td>
			</tr>
			<tr class="footer-text none-border-top">
				<th>
					Total Sessions
				</th>
				<th>
					Sessions/Hour
				</th>
				<th>
					Minutes/Session
				</th>
				<th>
					Messages/Session
				</th>
			</tr>
		</table>
	</div>
</div>
<!---chat container--->
<div class="chat-container">
	<div class="header-bar">
		<div class="session_id">Session ID: 4446-0462-4034-4860</div>
		<div class="mobile_number">Mobile Number: (949) 400-0553</div>
		<div class="carrier">Carrier: Sprint</div>
		<div class="keyword">Keyword: FavColor</div>
	</div>
	<div class="clear-fix"></div>
	<div class="subheader">
		<div class="session-live">Session live for <span class="time-server">00 : 38 : 42</span></div>
		<div class="end-btn"><a class="filterButton btn-agent-admin delete-btn btn-danger">END</a></div>
		<div class="transfer-btn"><a class="filterButton btn-agent-admin">Transfer</a></div>
		<div class="give-back-btn"><a class="filterButton btn-agent-admin">Give Back Control</a></div>
	</div>
	<div class="clear-fix"></div>
	<div class="body-chat">
		<div class="message-row">
			<div class="time-send">2013-12-04 12:01</div>
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row">
			<div class="time-send">2013-12-04 12:01</div>
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row agent-message">
			<div class="time-send">2013-12-04 12:01</div>
			<div class="agent-name">0947561362</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row">
			<div class="time-send">2013-12-04 12:01</div>
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
	</div>
	<div class="clear-fix"></div>
	<div class="footer-chat">
		<div class="send-message-txt"><input type="text" name="message-txt" class="message-txt" placeholder="160/160"></div>
		<div class="insert-btn"><a class="filterButton btn-agent-admin">Insert</a></div>
		<div class="send-btn"><a class="filterButton btn-agent-admin">Send</a></div>
	</div>
</div>
<!---end chat container--->

<!---view chat container--->
<div class="view-chat-container">
	<div class="header-bar">
		<div class="user_name">Suspend user</div>
	</div>
	<div class="clear-fix"></div>
	<div class="subheader">
		Are you sure you want to suspend Peter Davis? <br /> 
		He currently has <span class="number-active-session">5</span> active sessions.
	</div>
	<div class="clear-fix"></div>
	<div class="body-chat">
		<div class="message-row">
			<div class="session-id">Session ID: 4446-0462-4034-4860</div>
			<div class="suspend-session">Suspend Action</div>
			<div class="status-session"><select class="state-session"><option value="allTime" selected="selected">All Time</option></select></div>
			<div class="hide-session">&nbsp;&nbsp;</div>
			<div class="show-session">&nbsp;&nbsp;</div>
		</div>
		<div class="message-row">
			<div class="session-id">Session ID: 4446-0462-4034-4860</div>
			<div class="suspend-session">Suspend Action</div>
			<div class="status-session"><select class="state-session"><option value="allTime" selected="selected">All Time</option></select></div>
			<div class="hide-session">&nbsp;&nbsp;</div>
			<div class="show-session">&nbsp;&nbsp;</div>
		</div>
		<div class="message-row agent-message">
			<div class="session-id">Session ID: 4446-0462-4034-4860</div>
			<div class="suspend-session">Suspend Action</div>
			<div class="status-session"><select class="state-session"><option value="allTime" selected="selected">All Time</option></select></div>
			<div class="hide-session">&nbsp;&nbsp;</div>
			<div class="show-session">&nbsp;&nbsp;</div>
		</div>
		<div class="message-row">
			<div class="session-id">Session ID: 4446-0462-4034-4860</div>
			<div class="suspend-session">Suspend Action</div>
			<div class="status-session"><select class="state-session"><option value="allTime" selected="selected">All Time</option></select></div>
			<div class="hide-session">&nbsp;&nbsp;</div>
			<div class="show-session">&nbsp;&nbsp;</div>
		</div>
	</div>
	<div class="clear-fix"></div>
	<div class="footer-chat">		
		<div class="suspend-btn"><a class="filterButton btn-agent-admin">Suspend</a></div>
		<div class="cancel-btn"><a class="filterButton btn-agent-admin">Cancel</a></div>
	</div>
</div>
<!---end view chat container--->

<!---small chat container--->
<div class="small-chat-container">
	<div class="header-bar">
		<div class="user-lbl">Robert Nielson</div>
		<div class="close-btn">&nbsp;&nbsp;&nbsp;</div>
		<div class="large-btn">&nbsp;&nbsp;&nbsp;</div>
		<div class="small-btn">&nbsp;&nbsp;&nbsp;</div>
	</div>
	<div class="clear-fix"></div>
	<div class="body-chat">
		<div class="message-row">
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row">
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row agent-message">
			<div class="agent-name">0947561362</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
		<div class="message-row">
			<div class="agent-name">AGENT</div>
			<div class="message-content">Hello! My name is Peter H.</div>
		</div>
	</div>
	<div class="clear-fix"></div>
	<div class="footer-chat">
		<div class="send-message-txt"><input type="text" name="message-txt" class="message-txt" placeholder="Text field"></div>
		<div class="send-btn"><a class="filterButton btn-agent-admin">Send</a></div>
	</div>
</div>
<!---end small chat container--->

<script type="text/javascript">
	var tblActiveAgent;
	var tblActiveSession;
	$(document).ready(function(){
		$("#divAgent").tabs();
		$("#tabs").tabs();
		InitControl();
		GetStatistic("today","Today");
		
		//a custom format option callback
		var addressFormatting = function(text, opt){
			var newText = text;
			//array of find replaces
			var findreps = [
				{find:/^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>'},
				{find:/([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
				{find:/([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2'},
				{find:/([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>'},
				{find:/(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>'}
			];
	
			for(var i in findreps){
				newText = newText.replace(findreps[i].find, findreps[i].rep);
			}
			return newText;
		}
		
		$('select.state-session').selectmenu({
			style:'popup',
			width: 138,
			format: addressFormatting
		});
		
		$('select.state-session').selectmenu({
			style:'popup',
			width: 138,
			format: addressFormatting
		});
		
		function InitControl(){
			//init datatable for active agent
			tblActiveAgent = $('#tblActiveAgent').dataTable( {
			    "bProcessing": true,
	//		    "bStateSave": true,
				"bFilter": false,
				"bServerSide":true,
				"sPaginationType": "full_numbers",
				"sAjaxDataProp": "aaData",
			    "bLengthChange": false,
				"iDisplayLength": 10,
			    "aoColumns": [
					{"sName": 'image', "sTitle": '', "sWidth": '7%',"bSortable": false},
					{"sName": 'name', "sTitle": 'Agent Name', "sWidth": '25%',"bSortable": true},
					{"sName": 'time', "sTitle": 'Time Logged In', "sWidth": '25%',"bSortable": true},
					{"sName": 'userCount', "sTitle": 'Total Users Served', "sWidth": '25%',"bSortable": true},
					{"sName": 'userId', "bVisible":false},//hide this column from user view,
					{"sName": 'option', "sTitle": '', "sWidth": '18%',"bSortable": false}
				],
				"sAjaxDataProp": "aaData",
			    "sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=GetActiveAgentListForDataTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					$(nRow).children('td:first').addClass("avatar");
					var agentId = aData[4];//get agent id from hidden column
					//add hover style for corresponding row
					$(nRow).addClass("tr-Session");
					//create onClick event for row whose param is sesssionID
					$(nRow).click(function(){
						LoadSessionsByAgent(agentId);
					});
		            return nRow;
		       	},
				"fnInitComplete":function(oSettings, json){
					$("#tblActiveAgent thead tr").find(':last-child').css("border-right","0px");
					$(".next.paginate_button").html("&raquo;");
					$(".previous.paginate_button").html("&laquo;");
					$(".first.paginate_button").css("display","none");
					$(".last.paginate_button").css("display","none");
				}
		    });
		}
		
		//get data for Total Sessions, Sessions/Hour, Minutes/Session, Messages/Session 
		function GetStatistic(type,labelSuffix){
		 	BeginLoadingData(labelSuffix);
			$("#lblTotalSession"+labelSuffix).text("");
			$("#lblSessionPerHour"+labelSuffix).text("");
			$("#lblMinutesPerSession"+labelSuffix).text("");
			$("#lblMessagesPerSession"+labelSuffix).text("");
			var data = {
					queryType: type
			};
		    ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc',
				'GetSessionStatistic',
				data,
				"Get statistic fail!",
				function(d){
					FinishLoadingData(labelSuffix);
					var sttData= d.DATA;
					$("#lblTotalSession"+labelSuffix).text(sttData.TOTALSESSIONS);
					$("#lblSessionPerHour"+labelSuffix).text(sttData.SESSIONSPERHOUR);
					$("#lblMinutesPerSession"+labelSuffix).text(sttData.MINUTESPERSESSION);
					$("#lblMessagesPerSession"+labelSuffix).text(sttData.MESSAGESPERSESSION);
				});
		}
		
		//this function is used to display all session that controlled by an agent 
		function LoadSessionsByAgent(userId){
			if(!tblActiveSession){//if datatable has not been initiated
				tblActiveSession = $("#ActiveSessionTbl").dataTable({
					"bProcessing": true,
				    "bStateSave": true,
					"bFilter": false,
					"bServerSide":true,
					"sPaginationType": "full_numbers",
					"sAjaxDataProp": "aaData",
				    "bLengthChange": false,
					"iDisplayLength": 10,
				    "aoColumns": [
						{"sName": 'image', "sTitle": 'Number', "sWidth": '35%',"bSortable": false},
						{"sName": 'name', "sTitle": 'Messages', "sWidth": '30%',"bSortable": true},
						{"sName": 'time', "sTitle": 'Duration', "sWidth": '35%',"bSortable": true},
					],
					"sAjaxDataProp": "aaData",
				    "sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=GetActiveSessionListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					"fnServerData": function ( sSource, aoData, fnCallback ) {
				       aoData.push(
					   		//this command pushes the UserID param in to server
				            { "name": "UserID", "value": userId}
			            );
				        $.ajax({dataType: 'json',
				                 type: "POST",
				                 url: sSource,
					             data: aoData,
					             success: fnCallback
					 	});
			        },
					"fnInitComplete": function(oSetting,json){
						$("#ActiveSessionTbl").removeClass("hide");
						//message column's header shouldn't have border
						$("#ActiveSessionTbl thead tr").find(':last-child').css("border-right","0px");
						$(".next.paginate_button").html("&raquo;");
						$(".previous.paginate_button").html("&laquo;");
						$(".first.paginate_button").css("display","none");
						$(".last.paginate_button").css("display","none");
						$("#ui-id-2").click();
					}
				});
			}
			else{//if datatalbe has been initiated, redraw it
				var oSettings = tblActiveSession.fnSettings();
			    tblActiveSession._fnCalculateEnd( oSettings );
				tblActiveSession.fnDraw( oSettings );
				$("#ActiveSessionTbl").removeClass("hide");
				$("#ui-id-2").click();
			}
		}
		
		function BeginLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif')");
			$('#tab'+divSuffix).css("background-repeat","no-repeat");
			$('#tab'+divSuffix).css("background-position","center"); 
			$('#tab'+divSuffix +"> table").css("visibility","hidden");
		}
		
		function FinishLoadingData(divSuffix){
			$('#tab'+divSuffix).css("background-image", "none");
			$('#tab'+divSuffix +"> table").css("visibility","visible");
		}
		
		$(".message-row>.hide-session").click(function(){
			$(".message-row>.hide-session").css("display","none");
			$(".message-row>.show-session").css("display","block");
		});
		
		$(".message-row>.show-session").click(function(){
			$(".message-row>.show-session").css("display","none");
			$(".message-row>.hide-session").css("display","block");
		});
	});
</script>