﻿<cfparam name="ActiveAgents" default="">
<cfparam name="SessionId" default="">
<cfparam name="SessionCodeString" default="">
<cfparam name="ShortCodeNumber" default="">
<cfparam name="ContactNumber" default="">
<cfparam name="AgentId" default="">
<cfparam name="Keyword" default="">
<cfset fromAgent = AgentId >
<cfset fromAgentName = "A manager" >
<cfinclude template="../../../public/paths.cfm" >

<script src="<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>/socket.io/socket.io.js"></script>

<cfset ListActiveAgents = deserializeJSON(ActiveAgents)>
<cfoutput>
	
<div style="height:280px; overflow:auto; padding:20px" id="TransferBox_<cfoutput>#SessionId#</cfoutput>">
	<cfloop array="#ListActiveAgents#" index="agent">
		<cfif agent.UserId NEQ fromAgent && agent.UserId NEQ AgentId>		
			<input type="radio" name="ActiveAgent" value="#agent.UserId#" id="agent#agent.UserId#" data-val="#agent.AgentName#"/>
			<label for="agent#agent.UserId#">#agent.AgentName#</label><br/>
		</cfif>
	</cfloop>
</div>
<input type="button" id="TransferToOtherAgent_btn_#SessionId#" class="button filterButton small" value="Transfer" style="float:right;">
</cfoutput>

<script type="text/javascript">
	var socket = io.connect('<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>');
	
	var TransferToOtherAgent = '#TransferToOtherAgent_btn_'+<cfoutput>#SessionId#</cfoutput>;
	$(TransferToOtherAgent).click(function(){
		var toAgent = $('input[name=ActiveAgent]:checked', '#TransferBox_<cfoutput>#SessionId#</cfoutput>').val();
		var toAgentName = $('input[name=ActiveAgent]:checked', '#TransferBox_<cfoutput>#SessionId#</cfoutput>').attr("data-val");
		if(typeof toAgent == 'undefined')
		{
			jAlert("Please select an Agent.", "Fail");
		}
		else
		{
			socket.emit('tranfer_session_to_other_agent', 
			{
				FromAgent : "<cfoutput>#fromAgent#</cfoutput>",
				ToAgent: toAgent,
				FromAgentName: '<cfoutput>#fromAgentName#</cfoutput>',
				SessionId: "<cfoutput>#SessionId#</cfoutput>",
				SessionCodeString: "<cfoutput>#SessionCodeString#</cfoutput>",
				ShortCodeNumber: "<cfoutput>#ShortCodeNumber#</cfoutput>",
				ContactNumber: "<cfoutput>#ContactNumber#</cfoutput>",
				Keyword: "<cfoutput>#Keyword#</cfoutput>"
			});
			$(".ui-icon-closethick").click();
			
			//display transfering box
			var buttonTransferings = '<input type="button" id="CancelTransfer_<cfoutput>#SessionId#</cfoutput>" onclick="client.CancelTransfer(<cfoutput>#SessionId#</cfoutput>, <cfoutput>#fromAgent#</cfoutput>, '+toAgent+')" class="declineButton" value="Cancel">';
			buttonTransferings = buttonTransferings + '<a class="ui-notify-close ui-notify-cross" href="#" style="display:none;" id="CloseTransfering_<cfoutput>#SessionId#</cfoutput>">x</a>';
			createTransfering("transferingBox", 
				{
					title:'Transfering...', 
					text: 'Waiting '+ toAgentName + ' accept a session.',
					button: buttonTransferings
				},
				{
					expires:false
				}
			);
		}
	});
	
</script>