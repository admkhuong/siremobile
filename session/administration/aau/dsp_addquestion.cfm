﻿<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetCategoryListForAutoComplete" returnvariable="categoryList">
</cfinvoke>
<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div style="width: 100%; margin:20px;">
	<div class="sms_popup_row_first">
		<div class="sms_popup_row_label">
			Category
		</div>
		<div class="sms_popup_row_control_keyword">
			<input type="hidden" id="hdfCategoryId" />
			<input type="text" class="sms_text_width" id="txtCategory" maxlength='160'>
		</div>	
	</div>
	<div class="sms_popup_row">
		<div class="sms_popup_row_label">
			Question
		</div>
		<div class="sms_popup_row_control_keyword">
			<textarea type="text" class="sms_text_width" id="txtQuestion" row="5" maxlength='255'></textarea> <br>
		</div>	
	</div>
	<div class="sms_popup_row">
		<div class="sms_popup_row_label">
			Answer
		</div>
		<div class="sms_popup_row_control_keyword">
			<textarea type="text" class="sms_text_width" id="txtAnswer" row="5" maxlength='255'></textarea> <br>
		</div>	
	</div>
</div>

<div class="scrButtonBar">
	<a class="button filterButton small" onclick="CloseDialogWithTinyMCE(); return false;" href="javasrcipt:void(0)">Cancel</a>
	<a class="button filterButton small" onclick="AddQuestion(); return false;" href="javasrcipt:void(0)">Save</a>
</div>

<script type="text/javascript">
	$(function(){
		Init();
	});
	
	//send request to add new question
	function AddQuestion(){
		if(!ValidateCategory()){
			jAlert("Please input category again!","Invalid category");
			return false;
		}
		var questionContent = $("#txtQuestion").val();
		var answerContent = tinyMCE.activeEditor.getContent();
		var category = $("#hdfCategoryId").val();//get value from hidden field
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=AddQuestion&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				categoryId: category,
				question:questionContent,
				answer:answerContent
			},					  
			success:function(d){
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.RXRESULTCODE) != "undefined"){							
					if(d.RXRESULTCODE > 0){
						jAlert("Question Added","Success",function(result){
							//location.reload();
							InitQuestionAdministration();
							closeDialog();
						});
					}else{
						jAlert(d.MESSAGE, d.ERRMESSAGE);							
					}
				}
			}
		});
	}
	
	function Init(){
		//get category arr
		
		var categoryArr = <cfoutput>#SerializeJSON(categoryList.DATA)#</cfoutput>;
		//init autocomplete
	 	$( "#txtCategory" ).autocomplete({
			minLength: 0,
			source: categoryArr,
			focus: function( event, ui ) {
				$( "#txtCategory" ).val( ui.item.TITLE );
				return false;
			},
			select: function( event, ui ) {
				$( "#txtCategory" ).val( ui.item.TITLE );
				$( "#hdfCategoryId" ).val( ui.item.CATEGORYID );
				return false;
			},
			})
			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
				return $( "<li>" )
					.append( "<a>" + item.TITLE  + "</a>" )
					.appendTo( ul );
		};
		
		$('#txtCategory').click(function() {
			$( "#txtCategory" ).autocomplete( "search", "" ); //show drop down list when click into text box
		});
		
		tinyMCE.execCommand('mceAddControl', false, 'txtAnswer');
		//remove tinyMce from txtAnswer when close dialog to make it work next time dialog is loaded
		$("div#AddQuestionPopUp").parent().bind('dialogclose', function(event) {
			CloseDialogWithTinyMCE();
 		});
	}
	
	function ValidateCategory(){
		//check whether input category data in categoryList
		var categoryArr = <cfoutput>#SerializeJSON(categoryList.DATA)#</cfoutput>;
		var result = false;
		for(var i=0;i<categoryArr.length;i++){
			if(categoryArr[i].TITLE == $( "#txtCategory" ).val()) {
				result= true;
				return result;
			}
		}
		return result;
	}
	
	function CloseDialogWithTinyMCE(){
		if (tinyMCE.getInstanceById('txtAnswer'))
		{
		    tinyMCE.execCommand('mceFocus', false, 'txtAnswer');                    
		    tinyMCE.execCommand('mceRemoveControl', false, 'txtAnswer');
		}
		closeDialog();
	}
</script>
