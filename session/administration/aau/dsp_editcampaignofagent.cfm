﻿<cfparam name="agentid" default="">
<cfparam name="hauinvitationcode" default="">
<cfparam name="email" default="">

<!---get all keyword/campaign code by user--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="getKeywordAndShortCodeByUser" returnvariable="getKeywordAndShortCodeByUser">
	<cfinvokeargument name="email" value="#email#" >
	<cfinvokeargument name="hauinvitationcode" value="#hauinvitationcode#" >
</cfinvoke>
<cfset listKeywordAndShortCodeByUser = #getKeywordAndShortCodeByUser.ROWS#>

<!---get all batch code by hauinvitation--->

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="getBatchByHauinvitationCode" returnvariable="getBatchByHauinvitationCode">
	<cfinvokeargument name="HauinvitationCode" value="#HauinvitationCode#" >
</cfinvoke>
<cfset listBatchByHauinvitationCode = #getBatchByHauinvitationCode.ROWS#>

<cfoutput>
<h2 class="title">
Choose one or more Campaign Code for Agent
</h2>
<div style="padding: 0 20px 0 20px">
	<select name="dllBatchId" id="dllBatchId" class="inpMultiSelectbox" multiple="multiple" style="height:160px;">
	<cfif Arraylen(listBatchByHauinvitationCode) GT 0>
		<option value="-1">Choose data</option>
		<cfloop array="#listKeywordAndShortCodeByUser#" index="keyword">
			<cfset select="">
			<cfloop array="#listBatchByHauinvitationCode#" index="campaigncode">
				<!---check whether batch has been selected before --->
				<cfif campaigncode.BATCHID EQ keyword.BATCHID>
					<cfset select='selected="selected"'>
					<cfbreak>	
				</cfif>
			</cfloop>
			<!---if batch hasnt been selected, render it --->
			<cfif select EQ "">
				<option #select# value="#keyword.BATCHID#">#keyword.BATCHID & " (Short Code: " &keyword.ShortCode & " - Keyword: " & keyword.KEYWORD & ")" #</option>
			</cfif>
		</cfloop>
	<cfelse>
		<option selected="selected" value="-1">Choose data</option>
		<cfloop array="#listKeywordAndShortCodeByUser#" index="keyword">
			<option value="#keyword.BATCHID#">#keyword.KEYWORD & " (Campaign Code " & keyword.BATCHID & ")" #</option>
		</cfloop>
	</cfif>
	</select>
</div>
<div class="SCRow" style="text-align:center; margin-top:10px;">
	<a class="button filterButton small" onclick="closeDialog(); return false;" href="javasrcipt:void(0)">Cancel</a>
	<a class="button filterButton small" onclick="SaveEditCampaignCodeOfAgent(); return false;" href="javasrcipt:void(0)">Save</a>
</div>
</cfoutput>

<script>
	
	function SaveEditCampaignCodeOfAgent()
	{
		var dllBatchId = $("#dllBatchId").val();
		if (dllBatchId == -1) {
			jAlert("Campaign code could not be empty", "Failure!");
		}
		else {
			var data = {
				hauinvitationcode: '<cfoutput>#hauinvitationcode#</cfoutput>',
				campaigncodes: JSON.stringify(dllBatchId)
			};
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'EditCampaignCodeOfAgent', data, "Edit Campaign Code Of Agent fail!", function(d){
				jAlert("Edit campaign code of agent success", "Success!", function(result){
					location.reload();
				});
			});
		}
	}
	
</script>

<style>
	.inpMultiSelectbox{
		width:100%;
	}
	.inpMultiSelectbox option{
		padding:5px;
		border-bottom:1px solid #CCC; 
		height:20px;
	}
	.title{
		text-align:center;
		font-weight:bold;
		font-size:15px;
		padding:10px;
	}
</style>