﻿<!-- HANDLER PARAMS -->
<cfparam name="CannedId">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetCannedById" returnvariable="CannedById">
	<cfinvokeargument name="CannedId" value="#CannedId#" >
</cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="getKeywordAndShortCodeByUserForCanned" returnvariable="getKeywordAndShortCodeByUser">
</cfinvoke>
<cfset listKeywordAndShortCodeByUser = #getKeywordAndShortCodeByUser.ROWS#>

<cfoutput>

<!-- OUTPUT HTML-->
<div class="boxAddCanned">
	<div class="field">
		Keyword/Campaign Code
	</div>
	<div class="editor">
		<cfoutput>
			<cfif getKeywordAndShortCodeByUser.HASDATA EQ 1>
				<cfset selected="">
				<select name="dllBatchId" id="dllBatchId" class="inpSelectbox">
					<option value="-1">Chose data</option>
				<cfloop array="#listKeywordAndShortCodeByUser#" index="keyword">
					<cfif keyword.BATCHID EQ CannedById.DATA.BATCHID>
						<option selected="selected" value="#keyword.BATCHID#">#keyword.KEYWORD & " (Campaign Code " & keyword.BATCHID & ")" #</option>
					<cfelse>
						<option #selected# value="#keyword.BATCHID#">#keyword.KEYWORD & " (Campaign Code " & keyword.BATCHID & ")" #</option>
					</cfif>

				</cfloop>
				</select>
			<cfelse>
				<select name="dllKeyword" id="dllKeyword" class="inpSelectbox" disabled="true">
					<option selected="selected">No data</option>
				</select>
			</cfif>
		</cfoutput>
	</div>
	<div style="clear:both;"></div>
	<div class="field">
		Title
	</div>
	<div class="editor">
		<input class="inpText" type="text" id="txtTitle" name="txtTitle" value="#CannedById.DATA.TITLE#">
	</div>
	<div class="field">
		Description
	</div>
	<div class="editor">
		<textarea id ="txtResponse" name="txtResponse" rows="4" cols="40">#CannedById.DATA.RESPONSE#</textarea>
	</div>
</div>
<div  class="SCRow" style="text-align:center; margin-top:10px;">
		<a class="button filterButton small" onclick="closeDialog(); return false;" href="javasrcipt:void(0)">Cancel</a>
		<cfif getKeywordAndShortCodeByUser.HASDATA EQ 1>
			<a class="button filterButton small" onclick="EditCanned(); return false;" href="javasrcipt:void(0)">Save</a>
		</cfif>
</div>
</cfoutput>
<style>
	.boxAddCanned
	{
		padding:20px;
	}
	.field{
		float:left;
		width:35%;
	}
	.editor
	{
		float:left;
		width:60%;
	}
	.inpText
	{
		width:90%;
	}
	.inpSelectbox
	{
		width:95%;
		height:23px;
		margin-bottom:5px;
	}
	.inpSelectbox option
	{
		height:23px;
		padding:3px;
	}
</style>

<script type="text/javascript">
	function EditCanned(){
		var batchid = $("#dllBatchId").val();
		var title = $("#txtTitle").val();
		var response = $("#txtResponse").val();
		$.ajax({
			type: "POST",
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=EditCanned&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			dataType: 'json',
			data:  {
				batchid: batchid,
				title: title,
				response:response,
				CannedId:'<cfoutput>#CannedId#</cfoutput>'
			},
			success:function(d){
				<!--- Check if variable is part of JSON result string --->
				if(typeof(d.RXRESULTCODE) != "undefined"){
					if(d.RXRESULTCODE > 0){
						location.reload();
					}else{
						jAlert(d.MESSAGE, d.ERRMESSAGE);
					}
				}
			}
		});
	}
</script>