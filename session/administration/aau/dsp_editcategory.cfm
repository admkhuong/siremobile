﻿<cfparam name="CategoryId">
<div class="boxAddCategory">
	<div class="field">
		Title
	</div>
	<div class="editor">
		<input class="inpText" type="text" id="txtTitle" name="txtTitle">
	</div>	
	<div class="field">
		Description
	</div>
	<div class="editor">
		<textarea id ="txtDescription" name="txtDescription" rows="4" cols="38"></textarea>
	</div>
</div>
<div  class="SCRow" style="text-align:center; margin-top:10px;">
		<a class="button filterButton small" onclick="closeDialog(); return false;" href="javasrcipt:void(0)">Cancel</a>
		<a class="button filterButton small" onclick="EditCategory(); return false;" href="javasrcipt:void(0)">Save</a>
</div>
<style>
	.boxAddCategory
	{
		padding:20px;
	}
	.field{
		float:left;
		width:30%;
	}
	.editor
	{
		float:left;
		width:70%;
	}
	.inpText
	{
		width:90%;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		InitData();
	});
	
	function InitData(){
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=GetCategoryById&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				CategoryId: '<cfoutput>#CategoryId#</cfoutput>'
			},					  
			success:function(d){
				<!--- Check if variable is part of JSON result string --->		
				if(typeof(d.RXRESULTCODE) != "undefined"){							
					if(d.RXRESULTCODE > 0){
						$("#txtTitle").val(d.DATA.TITLE);
						$("#txtDescription").val(d.DATA.DESCRIPTION);
					}else{
						jAlert(d.MESSAGE, d.ERRMESSAGE);							
					}
				}
			}
		});
	}

	function EditCategory(){
		var title = $("#txtTitle").val();
		var description = $("#txtDescription").val();
		$.ajax({
			type: "POST", 
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=EditCategory&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				title: title,
				description:description,
				categoryId:'<cfoutput>#categoryId#</cfoutput>'
			},					  
			success:function(d){
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.RXRESULTCODE) != "undefined"){							
					if(d.RXRESULTCODE > 0){
						//location.reload();
						InitCategoryAdministration();
						closeDialog();
					}else{
						jAlert(d.MESSAGE, d.ERRMESSAGE);							
					}
				}
			}
		});
	}
</script>