﻿<script type="text/javascript">	
	var btnId = '#comment_btn_'+<cfoutput>#sessionId#</cfoutput>;
	$(btnId).click(function(){		
		AddComment_<cfoutput>#sessionId#</cfoutput>();
	});
	
	function AddComment_<cfoutput>#sessionId#</cfoutput>(){
		var _comment = $('#content_'+<cfoutput>#sessionId#</cfoutput>).val();
		if(_comment != ""){
			client.socket.emit('add_comment', 
				{
					member : client.data.username,
					shortcode: '<cfoutput>#shortCode#</cfoutput>',
					userid : client.data.userid,
					usertype : client.data.usertype,
					sessionid : '<cfoutput>#sessionId#</cfoutput>',
					sessionCode: '<cfoutput>#sessionCode#</cfoutput>',
					carrier: client.data.carrier,
					contactstring : '<cfoutput>#contactNumber#</cfoutput>',
					message : _comment
				});
		}
	}
	
	client.appendMessage_<cfoutput>#sessionId#</cfoutput> = function(result, type){
		var session_Id = '<cfoutput>#sessionId#</cfoutput>';
		var nowdate = new Date();
	    var curr_month = nowdate.getMonth() + 1; //Months are zero based
	    var date_time = nowdate.getFullYear() + "-" + ('0' + curr_month).slice(-2) + "-" + ('0' + nowdate.getDate()).slice(-2) + " " + ('0' + nowdate.getHours()).slice(-2) +":" + ('0' + nowdate.getMinutes()).slice(-2);
		
		// type (Agent=1,User=2)
		if(type ==1){
			var item = $("<li>").addClass('chatitem me');
			var date = $("<div>").addClass('date').html(date_time);
			var member = $("<div>").addClass('member').text(result.member);
			var message = $("<div>").addClass('comment').html(result.message);
			item.append(date).append(member).append(message);			
			var a = $("#chatPanel_"+<cfoutput>#sessionId#</cfoutput>+" > ul.list");
			//alert(a.Id);	
			$("#chatPanel_"+<cfoutput>#sessionId#</cfoutput>+" > ul.list").append(item);	
		} else{
			var item = $("<li>").addClass('chatitem you');
			var date = $("<div>").addClass('date').html(date_time);
			var member = $("<div>").addClass('member').text(result.member);
			var message = $("<div>").addClass('comment').html(result.message);
			item.append(date).append(member).append(message);				
			$("#chatPanel_"+<cfoutput>#sessionId#</cfoutput>+" > ul.list").append(item);	
		}
		AnimateChatPanel_<cfoutput>#sessionId#</cfoutput>();
	}	
	
	client.socket.on('addnewcomment', function(result) {
		if(result.shortcode == '<cfoutput>#shortCode#</cfoutput>'
			&& result.contactstring == '<cfoutput>#contactNumber#</cfoutput>'
//				&& result.userid == client.data.userid
//				&& result.sessionid == <cfoutput>#sessionId#</cfoutput>
			)
		{
			client.appendMessage_<cfoutput>#sessionId#</cfoutput>(result, 1);
			$("#content_"+<cfoutput>#sessionId#</cfoutput>).val("").keyup();
		}
	});
	
	client.socket.on('notification', function(result) {
		if(result.shortcode == '<cfoutput>#shortCode#</cfoutput>' 
			&& result.contactstring == '<cfoutput>#contactNumber#</cfoutput>')
		{
			client.appendMessage_<cfoutput>#sessionId#</cfoutput>(result, 2); // append new message from user mobile
			var myDiv = $("#chatPanel_<cfoutput>#sessionId#</cfoutput>");
			myDiv.animate({ scrollTop: myDiv.prop("scrollHeight")}, 1000);
		}
	});
	
	client.socket.on('removeActiveSession',function(result){
		if("<cfoutput>#sessionId#</cfoutput>" == result.SessionId){
			location.reload();
		}
	});		

	
	var transferButton = '#transfer_btn_'+<cfoutput>#sessionId#</cfoutput>;
	$(transferButton).click(function(){
		client.socket.emit('get_active_agents_from_adminitration',{
			shortcode: '<cfoutput>#shortCode#</cfoutput>',
			contactstring: '<cfoutput>#contactNumber#</cfoutput>',
			keyword: '<cfoutput>#keyword#</cfoutput>'
		});
	});

	client.socket.on('getActiveAgentsFromAdministration', function(result) {
		if(result.ShortCode == '<cfoutput>#shortCode#</cfoutput>' 
		&& result.ContactString == '<cfoutput>#contactNumber#</cfoutput>')
		{
			var ActiveAgents = JSON.stringify(result.ActiveAgents);
			var url = "dsp_ActiveAgents?ActiveAgents=" + ActiveAgents +"&SessionId=<cfoutput>#sessionId#</cfoutput>&SessionCodeString='<cfoutput>#sessionCode#</cfoutput>'&ShortCodeNumber='<cfoutput>#shortCode#</cfoutput>'&ContactNumber='<cfoutput>#ContactNumber#</cfoutput>'&AgentId=<cfoutput>#AgentId#</cfoutput>&keyword=<cfoutput>#keyword#</cfoutput>";
			var title = "List Active Agent";
				OpenDialog(url, 
				title, 
				350, 
				500
			);
			return false;
		}
	});
	
	client.socket.on('TranferSessionToOtherAgent', function(result) {	
		if("<cfoutput>#Session.UserId#</cfoutput>" == result.ToAgent)
		{
			$("#transferButton_<cfoutput>#Session.UserId#</cfoutput>").trigger('click', [result]);
		}
	});
	
	client.AcceptTransfer = function(sessionid, fromagent, toagent, sessioncodestring, shortcodenumber, contactnumber, keyword){			
		client.socket.emit('accept_session_transfer',{
			SessionId: sessionid,
			FromAgent: fromagent,
			ToAgent: toagent,
			SessionCodeString: sessioncodestring,
			ShortCodeNumber: shortcodenumber,
			ContactNumber: contactnumber,
			TimeLeft: 5400,
			Keyword: keyword
		});
	}
	
	client.DeclineTransfer = function(sessionid, fromagent, toagent, sessioncodestring, shortcodenumber, contactnumber){			
		client.socket.emit('decline_session_transfer',{
			SessionId: sessionid,
			FromAgent: fromagent,
			ToAgent: toagent,
			SessionCodeString: sessioncodestring,
			ShortCodeNumber: shortcodenumber,
			ContactNumber: contactnumber,
			TimeLeft: 5400
		});
	}
	
	client.CancelTransfer = function(sessionid, fromagent, toagent){
		client.socket.emit('cancel_session_transfer',{
			SessionId: sessionid,
			FromAgent: fromagent,
			ToAgent: toagent
		});
	}
	
	client.socket.on('AcceptSessionTransfer', function(result) {
		var data = {
				SessionId: result.SessionId,
				AgentId: result.ToAgent
			};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'UpdateOwnerSession', data, "Update owner session fail!", function(d){			 
			if("<cfoutput>#Session.UserId#</cfoutput>" == result.ToAgent)
			{
				var islocked = 0;//new sesion is unlocked
				AppendSession(result.SessionId, result.SessionCodeString, result.ShortCodeNumber, result.TimeLeft, result.ContactNumber, islocked, result.Keyword);
				//close transfer box
				$("#CloseTransfer_"+result.SessionId).click();
			}
			//remove session tab for fromagent
			if("<cfoutput>#Session.UserId#</cfoutput>" == result.FromAgent)
			{	
				$("#CloseTransfering_"+result.SessionId).click();
				$("li[aria-controls='tab"+result.SessionId+"']").remove();
			    $("#tab"+result.SessionId).remove();
			    $("#tabs").tabs("refresh");
				var data = {
								UserId : result.FromAgent,
								ContactString: result.ContactNumber
							};
				client.socket.emit('remove_active_session', data);
			}
		});		
		
		
	});
	
	client.socket.on('DeclineSessionTransfer', function(result) {		
		if("<cfoutput>#Session.UserId#</cfoutput>" == result.ToAgent)
		{
			$("#CloseTransfer_"+result.SessionId).click();
			client.socket.emit('declined_session_transfer',{
				SessionId: result.SessionId,
				FromAgent: result.FromAgent,
				ToAgent: result.ToAgent,
				SessionCodeString: result.SessionCodeString,
				ShortCodeNumber: result.ShortCodeNumber,
				ContactNumber: result.ContactNumber,
				TimeLeft: 5400
			});
		}
	});
	
	client.socket.on('DeclinedSessionTransfer', function(result) {		
		if("<cfoutput>#Session.UserId#</cfoutput>" == result.FromAgent)
		{
			$("#CloseTransfering_"+result.SessionId).click();
			$("#denclinedTransferButton_<cfoutput>#Session.UserId#</cfoutput>").trigger('click', [result]);
		}
	});
	
	client.socket.on('CancelSessionTransfer', function(result) {
		if("<cfoutput>#Session.UserId#</cfoutput>" == result.ToAgent)
		{
			$("#CloseTransfer_"+result.SessionId).click();
		}		
		if("<cfoutput>#Session.UserId#</cfoutput>" == result.FromAgent)
		{
			$("#CloseTransfering_"+result.SessionId).click();
		}
	});
</script>