﻿<style type="text/css">
	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
</style>
<div class="log-bar-popup">
	Invite Users
</div>
<div class="popup-intro">
	<p>Please separate each email address with a comma.</p>
	<p>Warning: Each user will be associated with the campains code below!</p>
</div>
<div class="box">
	<div class="left_box">
		Email Addresses
	</div>
	<div class=" right_box">
		<textarea class="input_box" id="txtEmailInvitation" name="txtEmailInvitation"></textarea>
	</div>	
	<!---<div class="field">
		Campaign Code
	</div>
	<div class="editor">
		<input class="inpText" type="text" id="txtCampaignCode" name="txtCampaignCode">
	</div>--->
</div>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetShortCodeAndKeywordForAddNewAgent" returnvariable="campainCodeArr">
</cfinvoke>
<div class="selectboxes">
	<select multiple id="campainFromList" class="select-box">
		<cfloop array="#campainCodeArr.ROWS#" index="campainItem">
			<cfoutput><option value="#campainItem.BATCHID#" title="#campainItem.CAMPAINCODE#">#campainItem.CAMPAINCODE#</option></cfoutput>
		</cfloop>
	</select>
	<select multiple id="campainToList" class="select-box">
	</select>
</div>
<div style="clear:both;">
</div>
<div class="filter-footer log-footer">
	<a class="SCRow blue button filterButton small" onclick="InviteAgent(); return false;" href="javasrcipt:void(0);">Send</a>
	<a class="SCRow blue button filterButton small" onclick="closeDialog(); return false;" href="javasrcipt:void(0);">Cancel</a>
</div>
<div style="clear:both;">
</div>
 <script>
	$(function(){
		var leftSideLalbel = 'Available';
		var rightSideLabel = 'Assigned';
		createMovableOptions("campainFromList","campainToList",529,200,leftSideLalbel,rightSideLabel);
		$("#divMid").css("padding-left","0px");
		return false;
		var availableTags = <cfoutput>#SerializeJSON(campainCodeArr.ROWS)#</cfoutput>;
		var AvailableCampaignCodes = [];
		for (i in availableTags){
		    AvailableCampaignCodes.push(""+availableTags[i].CAMPAINCODE +"");
		}
		
		$( "#txtCampaignCode" ).autocomplete({
			source: AvailableCampaignCodes
		});
	});
	$(document).ready(function() {
	    $("#txtCampaignCode").keydown(function(event) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	    });
	});
</script>
<style>
	.boxInviteAgent
	{
		padding:20px;
	}
	.field{
		float:left;
		width:30%;
	}
	.editor
	{
		float:left;
		width:70%;
	}
	.inpText
	{
		width:90%;
	}
</style>