﻿<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/cf_table.css') all;
	</style>
</cfoutput>
<cfparam name="keyword" default = ''>
<cfimport taglib="../../lib/grid" prefix="mb" />
<cfset paramObj = {}>
<cfset paramObj.name = "keyword">
<cfset paramObj.value = #keyword#>
<cfset params = [#paramObj#]>
<cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="OpenQuestionForm({%QuestionId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>';
		htmlOption = htmlOption & '<a href="##" onclick="DeleteQuestion({%QuestionId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>';		
	</cfscript>
	
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
	}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Question', 'Category','Options']>	

	<cfset arrColModel = [
			{name='Question', width='60%'},
			{name='Category', width='30%'},
			{name='FORMAT', width='10%', format = [htmlFormat]}
			] >
			<!---attributes.params--->
<cfif #keyword# NEQ ''>
 	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau"
		method="SearchQuestionByKeyword"
		width="100%"
		colNames= #arrNames#
		colModels= #arrColModel#
		params= #params#
	>
	</mb:table>
<cfelse>
	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau"
		method="GetQuestionList"
		width="100%"
		colNames= #arrNames#
		colModels= #arrColModel#
	>
	</mb:table>
</cfif>
</cfoutput>
<style>
	.icon_img{
		margin-left:10px;
	}
</style>