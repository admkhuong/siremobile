﻿<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>	
	<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
</cfoutput>
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetHAUSetting" returnvariable="retGetHAUSetting">	
</cfinvoke>

<!---<div style="width:50%;">
	<h2>AAU Administration Setting</h2>
	<input type="hidden" id="HauSettingId" value="<cfoutput>#retGetHAUSetting.HauSettingId#</cfoutput>">
	<div class="field">
		Auto router incoming request
	</div>
	<div class="editor">
		<div class="cb-bg" >
			<div id="AutoRouterIncomingSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AutoRouteIncomingRequest#</cfoutput>"></div>
		</div>	
	</div>
	<div class="clear"></div>
	<div class="field">
		Allow session transfer
	</div>
	<div class="editor">
		<div class="cb-bg" >
			<div id="AllowSessionSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowSesstionTransfer#</cfoutput>"></div>
		</div>	
	</div>
	<div class="clear"></div>
	<div class="field">
		Maximum sessions per agent
	</div>
	<div class="editor">
		<div class="cb-bg" style="min-width:80px; float:left;">
			<div id="MaximumSessionSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgent#</cfoutput>"></div>
		</div>
		<div id="MaximumSession" style="float:left; margin-left:5px; height: 26px;">
			<input type="text" id="inpEtaValue" style="width: 10px;" value="<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgentValue#</cfoutput>" >
		</div>
	</div>
	<div class="clear"></div>
	<div class="field">
		Allow canned responses
	</div>
	<div class="editor">
		<div class="cb-bg" >
			<div id="AllowCannedSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowCannedResponses#</cfoutput>"></div>
		</div>	 
	</div>
	<div class="clear"></div>
	<div class="field">
		Allow agent created canned responses
	</div>
	<div class="editor">
		<div class="cb-bg" >
			<div id="AllowAgentCreatedCannedSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowAgnetCreatedCannedResponses#</cfoutput>"></div>
		</div>	 
	</div>
	<div class="clear"></div>
	<div class="field">
		Auto router returning request
	</div>
	<div class="editor">
		<div class="cb-bg" >
			<div id="AutoRouterReturningSlider" class="cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AutoRouteReturningRequest#</cfoutput>"></div>
		</div>	
	</div>
	<div style="margin-top:10px;">
	<a href="##" onclick="UpdateSetting(); return false;" class="button filterButton small">Update</a>
	</div>
</div>--->

<div id="content-AAU-Admin">
	<div id="head-AAU-Admin">AAU Administration Setting</div>
	<input type="hidden" id="HauSettingId" value="<cfoutput>#retGetHAUSetting.HauSettingId#</cfoutput>">
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Auto router incoming request
		</div>
		<div class="editor">
			<div class="new-cb-bg" >
				<span class='on'>ON</span>
				<div id="AutoRouterIncomingSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AutoRouteIncomingRequest#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>	
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Allow session transfer
		</div>
		<div class="editor">
			<div class="new-cb-bg" >
				<span class='on'>ON</span>
				<div id="AllowSessionSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowSesstionTransfer#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>	
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Maximum sessions per agent
		</div>
		<div class="editor">
			<div id="MaximumSession" style="float:left; margin-left:5px; height: 26px;">
				<!---<input type="text" id="inpEtaValue" style="width: 10px;" value="<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgentValue#</cfoutput>" >--->
				<select name="inpEtaValue" id="inpEtaValue">
					<cfloop from="1" to="99" index="i">
					  <cfoutput>
					  	  <option value="#i#" <cfif retGetHAUSetting.MAXIMUMSESSIONSPERAGENTVALUE EQ i> selected </cfif>>#i#</option>
					  </cfoutput>
					</cfloop>
				</select>
			</div>
			<div class="new-cb-bg" >
				<span class='on'>ON</span>
				<div id="MaximumSessionSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgent#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Allow canned responses
		</div>
		<div class="editor">
			<div class="new-cb-bg">
				<span class='on'>ON</span>
				<div id="AllowCannedSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowCannedResponses#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>	 
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Allow agent created canned responses
		</div>
		<div class="editor">
			<div class="new-cb-bg">
				<span class='on'>ON</span>
				<div id="AllowAgentCreatedCannedSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AllowAgnetCreatedCannedResponses#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>	 
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div class="row">
		<div class="hide-description control-description">&nbsp;&nbsp;&nbsp;</div>
		<div class="field">
			Auto router returning request
		</div>
		<div class="editor">
			<div class="new-cb-bg" >
				<span class='on'>ON</span>
				<div id="AutoRouterReturningSlider" class="new-cb-slider" cb-status="<cfoutput>#retGetHAUSetting.AutoRouteReturningRequest#</cfoutput>"></div>
				<span class='off'>OFF</span>
			</div>	
		</div>
		<div class="description">description</div>
		<div class="clear"></div>
	</div>
	<div id="footer-AAU-Admin">
		<a href="##" onclick="ResetForm(); return false;" class="button filterButton small btn-auu-admin">Reset</a>
		<a href="##" onclick="UpdateSetting(); return false;" class="button filterButton small btn-auu-admin">Save</a>
	</div>
</div>

	<script type="text/javascript">

	<!--- On off slider for survey mode --->
	var slideSpeed = 150;
	var leftDist = 13;
	
	function ResetForm(){
		changeBitBox($("#AutoRouterIncomingSlider"), "", false, '<cfoutput>#retGetHAUSetting.AutoRouteIncomingRequest#</cfoutput>');
		changeBitBox($("#AllowSessionSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowSesstionTransfer#</cfoutput>');
		changeBitBox($("#MaximumSessionSlider"), "#MaximumSession", true, '<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgent#</cfoutput>');
		changeBitBox($("#AllowCannedSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowCannedResponses#</cfoutput>');
		changeBitBox($("#AllowAgentCreatedCannedSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowAgnetCreatedCannedResponses#</cfoutput>');
		changeBitBox($("#AutoRouterReturningSlider"), "", false, '<cfoutput>#retGetHAUSetting.AutoRouteReturningRequest#</cfoutput>');
	}
	
	function changeBitBox(inpObj, divTarget, isTarget, currentValue){
		//inpObj.attr("cb-status", "0");
		//inpObj.css("left", 41 + 'px');
		if (currentValue == '0') {
			inpObj.css("left", 13 + 'px');
			inpObj.attr("cb-status", "0");
		}else{
			inpObj.css("left", 0 + 'px');
			inpObj.attr("cb-status", "1");
		}
		
		inpObj.attr('is_active', false); //to track if the slider was dragged completely and released outside
		inpObj.draggable({
			containment: "parent",
			stop: function(event, ui){
				//trigger a mouse up only if is_active is left at true
				if ($(ui.helper).attr('is_active')) 
					$(ui.helper).parent().click();
			}
		});
	}
	
	$(function() {	
		
		$('.description').addClass('displayNone');
		$("#subTitleText").html("AAU Administration");
		initBitBox($("#AutoRouterIncomingSlider"), "", false, '<cfoutput>#retGetHAUSetting.AutoRouteIncomingRequest#</cfoutput>');
		initBitBox($("#AllowSessionSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowSesstionTransfer#</cfoutput>');
		initBitBox($("#MaximumSessionSlider"), "#MaximumSession", true, '<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgent#</cfoutput>');
		initBitBox($("#AllowCannedSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowCannedResponses#</cfoutput>');
		initBitBox($("#AllowAgentCreatedCannedSlider"), "", false, '<cfoutput>#retGetHAUSetting.AllowAgnetCreatedCannedResponses#</cfoutput>');
		initBitBox($("#AutoRouterReturningSlider"), "", false, '<cfoutput>#retGetHAUSetting.AutoRouteReturningRequest#</cfoutput>');
		
		//$('#inpEtaValue').spinner({min: 0});
		//$('#inpEtaValue').spinner({max: 99});
		
		$('#inpEtaValue').css( "width", "30px" );
		$('#inpEtaValue').ForceNumericOnly();
		var IsAllowMaximumSessionSlider= '<cfoutput>#retGetHAUSetting.MaximumSessionsPerAgent#</cfoutput>';
		$('select#inpEtaValue').selectmenu({
			style: 'popup',
			width: '60',
			menuWidth: '60'
		});
		if(IsAllowMaximumSessionSlider == '0')
		{
			$("#MaximumSession").css('visibility','hidden');
			var text1 = '3';
			$("select#inpEtaValue option").filter(function() {
			    //may want to use $.trim in here
			    return $(this).text() == text1; 
			}).prop('selected', true);
		}
		
		$('.control-description').click(function(){
			if ($(this).hasClass('hide-description')) {
				$(this).removeClass('hide-description');
				$(this).addClass('show-description');
				$(this).parent().find('.description').removeClass('displayNone');
				$(this).parent().find('.description').addClass('displayInline');
			}else{
				$(this).addClass('hide-description');
				$(this).removeClass('show-description');
				$(this).parent().find('.description').addClass('displayNone');
				$(this).parent().find('.description').removeClass('displayInline');
			}
		});
	});
	
	$(document).ready(function(){
	  $("p").click(function(){
	    $(this).hide();
	  });
	}); 

	function initBitBox(inpObj, divTarget, isTarget, currentValue)
	{
		//inpObj.attr("cb-status", "0");
		inpObj.css("left", 0 + 'px');
		if(currentValue == '0'){
			inpObj.css("left", 13 + 'px');	
		}
		
		inpObj.attr('is_active', false);   //to track if the slider was dragged completely and released outside
		inpObj.draggable({
			 containment: [inpObj.position().left,inpObj.position().top,inpObj.position().left + 13,inpObj.position().top],
			 stop: function(event, ui){
				 //trigger a mouse up only if is_active is left at true
				if($(ui.helper).attr('is_active')) $(ui.helper).parent().click();
			 }
		});
	
		inpObj.parent().click(function()
		{							
			//alert('event listenning!!');
			var status = inpObj.attr("cb-status");
		
			switch(status)
			{
				case "0":
				<!---/its off, slide it to on by leftDist;--->
				inpObj.animate({left: "0px"}, slideSpeed,  function() {
																			<!---// Animation complete.--->																			
																});
				if (isTarget == true) {
				 	$(divTarget).css('visibility','inherit');
				}						
				<!---//change status to 1--->
				inpObj.attr("cb-status", "1");
				
				break;
		
				case "1":
				<!---//its 'on', slide it to off 0px;--->
				
				inpObj.animate({left: leftDist + 'px'}, slideSpeed,  function() {
																		<!---// Animation complete.	--->																	
																	  });
				
				if (isTarget == true) {
				 	$(divTarget).css('visibility','hidden');
				}															  
				<!---//change status to 0--->
				inpObj.attr("cb-status", "0");
				
				break;
			}
			//inpObj.attr("is_active", false); 
			
	 	});
	}
	
	function UpdateSetting()
	{
		var hauSettingId = $("#HauSettingId").val();
		
		var AutoRouterIncomingSlider = $("#AutoRouterIncomingSlider").attr("cb-status");
		var AllowSessionSlider = $("#AllowSessionSlider").attr("cb-status");
		var MaximumSessionSlider = $("#MaximumSessionSlider").attr("cb-status");
		var MaximumValue = 0;
		if(MaximumSessionSlider == "1")
		{
			MaximumValue = $("#inpEtaValue").val();	
		}
		var AllowCannedSlider = $("#AllowCannedSlider").attr("cb-status");
		var AllowAgentCreatedCannedSlider = $("#AllowAgentCreatedCannedSlider").attr("cb-status");
		var AutoRouterReturningSlider = $("#AutoRouterReturningSlider").attr("cb-status");
		if (parseInt(hauSettingId) > 0) {
			var data = {
				HauSettingId: hauSettingId,
				AutoRouterIncomingSlider: AutoRouterIncomingSlider,
				AllowSessionSlider: AllowSessionSlider,
				MaximumSessionSlider: MaximumSessionSlider,
				MaximumValue: MaximumValue,
				AllowCannedSlider: AllowCannedSlider,
				AllowAgentCreatedCannedSlider: AllowAgentCreatedCannedSlider,
				AutoRouterReturningSlider: AutoRouterReturningSlider
			};
			
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'UpdateHAUSetting', data, "Update aau setting fail!", function(d){
				jAlert("Aau settings updated ", "Success!", function(result){
					location.reload();
				});
			});
		}
		else
		{
			var data = {
				AutoRouterIncomingSlider: AutoRouterIncomingSlider,
				AllowSessionSlider: AllowSessionSlider,
				MaximumSessionSlider: MaximumSessionSlider,
				MaximumValue: MaximumValue,
				AllowCannedSlider: AllowCannedSlider,
				AllowAgentCreatedCannedSlider: AllowAgentCreatedCannedSlider,
				AutoRouterReturningSlider: AutoRouterReturningSlider
			};
			
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'InsertHAUSetting', data, "Create aau setting fail!", function(d){
				jAlert("Aau settings created ", "Success!", function(result){
					location.reload();
				});
			});
		}
	}
</script>

<style>

	.field{
		float:left;
		width:60%;
	}
	.editor
	{
		float:left;
		width:40%;
	}
	.clear
	{
		margin-bottom:10px;
	}
</style>
