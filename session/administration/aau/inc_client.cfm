﻿<script src="<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>/socket.io/lib/socket.io.js"></script>
<script type="text/javascript">
/**
 * @author SETACINQ
 */
 var client = client || {};

!function(window, $, client) {
	// build client object
	client.data = {};
	client.socket = io.connect('<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>');
	
	client.on = function(event, element, callback) {
		/* Used jQuery 1.7 event handler */
		$(element).live(event, function(e) {
			e.preventDefault();
			callback.apply(this, arguments); // Used arguments to fixed error in IE
		});
	}

	client.off = function(event, element) {
		/* Used jQuery 1.7 event handler */
		$(element).die(event);
	}

	// DOM ready
	$(function() {		
		client.socket.on('connect', function () {
			client.socket.emit('connect');
		});		
		
		client.socket.on('notificationLogIn', function(result) {
			var userId = result.UserId;			
			var objectAgent = $("#OnlineStatus_" + userId);
			if(objectAgent != undefined)
			{
				objectAgent.html("Online");
				objectAgent.removeClass().addClass("sttOnline");
				$("#Active_"+userId).html(result.TotalActive);
			}			
			
			//update agent monitoring page
			var activeAgentList = $("#ActiveAgentList");//div ActiveAgentList in agentMonitoring page
			var tmplPreviewActiveAgents = $("#tmplPreviewActiveAgents");
			if(activeAgentList != undefined && tmplPreviewActiveAgents != undefined){
				if(typeof(ApplyTemplate) == "function")(ApplyTemplate("tmplPreviewActiveAgents",result,"ActiveAgentList"));
				if(typeof(UpdateActiveAgentNumber) == "function")(UpdateActiveAgentNumber(1));
				if($('#ActiveAgentDiv ul').children().length = 1){
					$('#ActiveAgentDiv ul').children().first().find('a').first().click();//fire click event of a tag to load data into session div
				}
			}
		});
		
		client.socket.on('notificationLogOff', function(result) {
			var userId = result.UserId;
			var objectAgent = $("#OnlineStatus_" + userId);
			if(objectAgent != undefined)
			{
				objectAgent.html("Offline");
				objectAgent.removeClass().addClass("sttOffline");
				$("#Active_"+userId).html(result.TotalActive);
			}
					
			//Update agent monitoring page
			//remove from active agent list
			var activeAgentList = $('#ActiveAgentDiv ul li#item'+userId);
			if (activeAgentList != undefined) {
				activeAgentList.remove();
				if(typeof(UpdateActiveAgentNumber) == "function")(UpdateActiveAgentNumber(-1));
			}
			//active first record in active agent list
			$('#ActiveAgentDiv ul').children().first().find('a').first().click();//fire click event of a tag to load data into session div
			//remove from Active Sessions list table if removed user is being selected
			var activeSessionTbl = $("#ActiveSessionTbl");
			if(activeSessionTbl != undefined){
				if(activeSessionTbl.attr("rel")== userId)
					activeSessionTbl.find("tr:gt(0)").remove();//remove session data from logged out agent
			}
		});
		
		
		client.socket.on('notificationChangeActive', function(result) {
			var userId = result.UserId;			
			$("#Active_"+userId).html(result.TotalActives);
			
			//update session list in agent monitoring page
			var activeSessionTbl = $("#ActiveSessionTbl");
			if(activeSessionTbl != undefined){
				if(activeSessionTbl.attr("rel")== userId){
					$("#lblActiveSessionNumber").text(result.TotalActives+ " active session(s)");
					$("#ActiveSessionTbl").find("tr:gt(0)").remove();//remove session data from previous agent
					LoadActiveSession(userId);
				}
			}
		});		
		
		client.socket.on('disconnect', function (msg) {
			// TODO: call when time out session
		});
		
		window.onbeforeunload = function() {
	    	client.socket.disconnect();
		};
	}); // End DOM ready
}(window, window.jQuery, window.client);
</script>