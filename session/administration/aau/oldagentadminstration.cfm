﻿<cfparam name="page" default="1">
<cfimport taglib="../../lib/grid" prefix="mb" />
<style>
	@import url('<cfoutput>#rootUrl#/#publicPath#/js/jquery/css/jquery.autocomplete.css</cfoutput>') all;
</style>
<h2>
	Agent Administration
</h2>
<div style="float:right;">
	<a href="" id="AddAgent" onclick="javascript:OpenPopupAddAgent();return false;" class="button filterButton small">Add</a>
</div>
<cfinclude template="dsp_selectmultiple.cfm" >
<cfinclude template="inc_client.cfm" >

<cfoutput>
	<!--- Prepare column data---->
	<cfset arrNames = ['Name', 'Email', 'AgentId', 'Campaign Code', 'SignUp', 'Status', 'Active', 'Options']>	

	<cfset arrColModel = [
			{name='Name', width='20%'},
			{name='Email', width='20%'},
			{name='UserID', width='5%'},
			{name='CampaignCode', width='20%', isHtmlEncode = false},
			{name='SignUp', width='5%'},
			{name='Status', width='5%', isHtmlEncode = false},
			{name='Active', width='5%', isHtmlEncode = false},
			{name='Options', width='20%', isHtmlEncode = false}
			] >
 	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau" 
		method="GetAgentList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
	   	FilterSetId="AgentFSList"
	>
	</mb:table>
</cfoutput>

<script>
	$("#subTitleText").html("Agent administrator");
	$('.revoke_user').css('cursor', 'pointer');
	$(".revoke_user").click(function(){
		var CurrCampaignCode = $(this).attr('rel');
		var CurrEmail =  $(this).attr('emailUser');
		jConfirm( "Are you sure you want to revoke this user?", "Revoke user", function(result) { 
			if(result)
			{	
				revokeUser(CurrCampaignCode,CurrEmail);
			}
			return false;																	
		});
	});
	
	function EditCampaignOfAgent(obj)
	{
		var agentid = $(obj).attr("data-userid");
		var hauinvitationcode = $(obj).attr("data-hauinvitationcode");
		var email = $(obj).attr("data-email");
		//Open popup edit campaign of agent 
		var url = "dsp_editCampaignOfAgent?agentid="+agentid+"&email="+email+"&hauinvitationcode="+hauinvitationcode;
		var title = "Edit invite Campaign code of Agent";
		OpenDialog(url, title, 355, 400);
		return false;
	}
	
	function revokeUser(CurrCampaignCode,CurrEmail)
	{
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc?method=RevokeUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			CAMPAIGNCODE : CurrCampaignCode,
			EMAIL : CurrEmail
			},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				
				if(d.RXRESULTCODE == 1){
					jAlert("Sorry, this user was signup.", "Failure", function(result){
						if(result){
							window.setTimeout('location.reload()', 50);
						}
					});
				}else if(d.RXRESULTCODE == 0){
					jAlert("Success, this user was be revoked.", "Success", function(result){
						if(result){
							window.setTimeout('location.reload()', 50);
						}
					});
				}else{
					jAlert("No Response from the remote server. Check your connection and try again.", "Error");
				}
			} 		
		});
		return false;
	}
	
	function OpenPopupAddAgent()
	{		
		//Open popup invite agent 
		var url = "dsp_inviteAgent";
		var title = "Invite new Agent";
		OpenDialog(url, title, 426, 550,"popupInvite",false);
		return false;
	}
	
	function validateEmail(email) 
	{ 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	} 
	
	function InviteAgent()
	{
		var txtEmailInvitation = $("#txtEmailInvitation").val().trim();
//		var txtCampaignCode = $("#txtCampaignCode").val();
//		var n = txtCampaignCode.indexOf("("); 
//		var resCampaignCode = txtCampaignCode.substring(0,n); 
		if (txtEmailInvitation.length == 0) {
			jAlert("Please enter email invitation", "Failure");
			return false;
		}
		var emailArr = txtEmailInvitation.split(",");
		for(var i = 0 ; i<emailArr.length;i++){
			var isEmail = validateEmail(emailArr[i]);
			if (!isEmail) {
				jAlert("Email invitation is not email, please try another", "Failure");
				return false;
			}
		}
		var campainArr = new Array();
		$("#campainToList option").each(function(){
			campainArr.push($(this).val())
		});
		if(campainArr.length==0){
			jAlert("Please choose at least one campain", "Failure");
			return false;
		}
		var data = {
			emailArr: JSON.stringify(emailArr),
			campainArr: JSON.stringify(campainArr)
		};
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'InviteAgent', data, "Invite Agent fail!", function(d){
			jAlert("Agent invited", "Success!", function(result){
				location.reload();
			});
		});
	}
</script>	
<style>
	.sttOffline
	{
		color:red;
	}
	.sttOnline
	{
		color:green;
	}
	.icon_img{
		margin-left:10px;
	}
	.editor {
	    height: 44px;
	}
</style>