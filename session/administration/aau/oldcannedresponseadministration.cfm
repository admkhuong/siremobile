﻿<cfparam name="page" default="1">
<cfimport taglib="../../lib/grid" prefix="mb" />
<cfinclude template="../../../public/paths.cfm" >
<h2>Canned Response Administration</h2>
<br>
<div style="float:right;">
	<a href="#" onclick="AddCannedResponse(); return false;" class="button filterButton small">Add</a>
</div>
<cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="OpenCannedForm({%CannedId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>';
		htmlOption = htmlOption & '<a href="##" onclick="DeleteCanned({%CannedId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>';
	</cfscript>

	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'
	}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Title', 'Response', 'Keyword/Campaign Code', 'Options']>

	<cfset arrColModel = [
			{name='Title', width='30%'},
			{name='Response', width='40%'},
			{name='KeywordVsBatchId', width='20%'},
			{name='FORMAT', width='10%', format = [htmlFormat]}
			] >
 	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau"
		method="GetCannedList"
		width="100%"
		colNames= #arrNames#
		colModels= #arrColModel#
	   	FilterSetId="GetCannedList"
	>
	</mb:table>
</cfoutput>
<style>
	.icon_img{
		margin-left:10px;
	}
</style>


<script>
	$("#subTitleText").html("Canned Response Administration");

	function AddCannedResponse()
	{
		//Open popup add canned
		var url = "dsp_AddCannedResponse";
		var title = "Add Canned Response";
		OpenDialog(url, title, 370, 600);
		return false;
	}

	function DeleteCanned(CannedId){
		var data = {
			CannedId: CannedId
	    };
		jConfirm( 'Do you want to delete this canned response?', 'Delete canned response', function(result) {
			if(result){
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'DeleteCanned', data, "Delete canned response fail!", function(d ) {
					jAlert("Canned response deleted", "Success!", function(result) {
						location.reload();
					});
				});
			}
		});
	}

	function OpenCannedForm(CannedId)
	{
		var url = "dsp_EditCannedResponse?CannedId="+CannedId;
		var title = "Edit Canned response";
		OpenDialog(url, title, 370, 600);
		return false;
	}
</script>