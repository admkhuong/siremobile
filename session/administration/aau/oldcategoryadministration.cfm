﻿<cfparam name="page" default="1">
<cfimport taglib="../../lib/grid" prefix="mb" />
<h2>Category Administration</h2>
<br>
<div style="float:right;">
	<a href="#" onclick="AddCategory(); return false;" class="button filterButton small">Add</a>
</div>
<cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="OpenCategoryForm({%CategoryID%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>';
		htmlOption = htmlOption & '<a href="##" onclick="DeleteCategory({%CategoryID%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>';		
	</cfscript>
	
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
	}>	
	<!--- Prepare column data---->
	<cfset arrNames = ['Title', 'Description', 'Questions', 'Order', 'Options']>	

	<cfset arrColModel = [
			{name='Title', width='30%'},
			{name='Description', width='40%'},
			{name='Questions', width='10%'},
			{name='Order', width='10%', isHtmlEncode = false},			
			{name='FORMAT', width='10%', format = [htmlFormat]}
			] >
 	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau" 
		method="GetCategoryList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
	   	FilterSetId="CategoryFSList"
	>
	</mb:table>
</cfoutput>
<style>
	.icon_img{
		margin-left:10px;
	}
</style>


<script>
	$("#subTitleText").html("Category administrator");
	
	function AddCategory()
	{
		//Open popup add category 
		var url = "dsp_AddCategory";
		var title = "Add Category";
		OpenDialog(url, title, 320, 500);
		return false;
	}
	
	function SaveCategory()
	{
		alert("TODO: coding here line 60 - on going");
	}
	function DeleteCategory(categoryid){
		var data = { 
			categoryid: categoryid
	    };
		jConfirm( 'Do you want to delete this category?', 'Delete Category', function(result) { 
			if(result){			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'DeleteCategory', data, "Delete category fail!", function(d ) {
					jAlert("Category deleted", "Success!", function(result) { 
						location.reload();
					});
				});	
			}
		});
	
	}
	
	function OpenCategoryForm(categoryid)
	{
		var url = "dsp_EditCategory?categoryId="+categoryid;
		var title = "Edit Category";
		OpenDialog(url, title, 320, 500);
		return false;
	}
	
	function MoveCategory(categoryId,moveType){
		var data ={
			categoryId: categoryId,
			moveType: moveType
		};
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'MoveCategory', data, "Move category fail!", function(d ) {
			jAlert("Category moved", "Success!", function(result) { 
				location.reload();
			});
		});	
	}
</script>