﻿<cfparam name="page" default="1">
<cfimport taglib="../../lib/grid" prefix="mb" />
<style>
	@import url('<cfoutput>#rootUrl#/#publicPath#/js/jquery/css/jquery.autocomplete.css</cfoutput>') all;
</style>
<cfoutput>
	<script src="#rootUrl#/#PublicPath#/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
</cfoutput>
<h2>Question Administration</h2>
<br>
<div style="float:right;">
	<a href="#" class="button filterButton small" onclick = "OpenAddQuestionForm();return false;">Add</a>
</div>
<div style="clear:both;"></div>
<cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="OpenQuestionForm({%QuestionId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/mb/rename.png"></a>';
		htmlOption = htmlOption & '<a href="##" onclick="DeleteQuestion({%QuestionId%})"><img class="icon_img" title="" src="#rootUrl#/#publicPath#/images/AAU/hau_del_16x16.png"></a>';		
	</cfscript>
	
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
	}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Question', 'Category','Options']>	

	<cfset arrColModel = [
			{name='Question', width='60%', sortObject= {isDefault='false', sortType="DESC", sortColName ='q.Question_vch'}},
			{name='Category', width='30%', sortObject= {isDefault='false', sortType="DESC", sortColName ='hc.Title_vch'}},
			{name='FORMAT', width='10%', format = [htmlFormat]}
			] >
			
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Category Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='2', DISPLAY='Question Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Answer', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}
		]
	>
	<cfset 
		filterFields = [
			'hc.Title_vch',
			'q.Question_vch',
			'q.Answer_vch'
		]	
		>
		
 	<mb:table
		component="#LocalSessionDotPath#.cfc.administrator.aau"
		method="GetQuestionList"
		width="100%"
		colNames= #arrNames#
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	   	FilterSetId="QuestionFSList"
	>
	</mb:table>
</cfoutput>
<style>
	.icon_img{
		margin-left:10px;
	}
</style>

<script>
	$("#subTitleText").html("Question administrator");
	
	$(function(){
		var a = '<cfoutput>#rootUrl#/#PublicPath#/images/AAU/hau_cancel_16x16.png</cfoutput>';

	    // Click to submit search form
	    $('#btnSearch').click(function(){
			var keyword = $("#txtSearch").val();
	        $.ajax({
			  	type: "POST",
				url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/aau/dsp_Question?_cf_nodebug=true&_cf_nocache=true&keyword='+keyword,
				data: {
				},
				async:false,
				error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
			  	success:
					function(d) {
						$("#QuestionFSList").html(d);
					}				
			});
	    });
		
		InitTinyMCE();
	});
	
	function DeleteQuestion(questionId){
		var data = { 
			questionId: questionId
	    };
		jConfirm( 'Do you want to delete this question?', 'Delete question', function(result) { 
			if(result){			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'DeleteQuestion', data, "Delete question fail!", function(d ) {
					jAlert("question deleted", "Success!", function(result) { 
						location.reload();
					});
				});	
			}
		});
	}
	
	function OpenQuestionForm(questionId)
	{
		OpenDialog(
			"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/aau/dsp_EditQuestion?questionId="+questionId,
			"Edit Question",
			480,
			640,
			"EditQuestionPopUp",//define popup id to make tinyMCE work
			false);
	}
	
	function OpenAddQuestionForm(){
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/aau/dsp_AddQuestion",
		"Add Question",
		480,
		640,
		"AddQuestionPopUp",//define popup id to make tinyMCE work
		false);
	}
	
	function Search(){
		
	}
	
	function InitTinyMCE(){
		tinyMCE.init({
			 mode : "none",
			 theme : "simple",
			 width: "280px",
			 height: "57px",
			 visual : false,
			 convert_urls : false,
			 mode: "none",
			 theme : "advanced",
			 theme_advanced_toolbar_location : "bottom",		 
			 setup : function(ed) { 
			   ed.onInit.add(function(ed) { 
			   		var dom = ed.dom;
		            var doc = ed.getDoc();
		        	
		        	doc.addEventListener('blur', function(e) {
						if(typeof(tinyMCE.activeEditor) !='undefined')
		                content = tinyMCE.activeEditor.getContent();
		            }, false);
		            
		            doc.addEventListener('click', function(e) {
		            	content = tinyMCE.activeEditor.getContent();
		            	if(content == "<p>Please enter the text to be display</p>"){
			                tinyMCE.activeEditor.setContent("");
		            	}
		            }, false);
			   }); 
			 }
	    });
		
		//tinyMCE.execCommand('mceAddControl', false, 'txtAnswer');
	}
</script>