﻿<cfparam name="sessionId" default="" >
<cfparam name="carrier" default="Sprint">
<cfparam name="listcanned" default="" >
<cfparam name="keyword" default="" >
<!---<cfparam name="timeleft" default="5400" >
<cfparam name="shortCode" default="1244">
<cfparam name="contactNumber" default="447900000000">
<cfparam name="sessionCode" default="123" >--->

<style>
	.chatBox{
		width:100%;
		height:538px;
		background-color: #DCDCE6;
	}
	textarea{
		padding-right: 0px;
		padding-left: 0px;
		margin-left:10px;
		margin-right:50px;
	}
	.filterButton {
	    background: -moz-linear-gradient(center top , #2484C6, #2484C6) repeat scroll 0 0 transparent;
	    border: 1px solid #0076A3;
	    border-radius: 5px 5px 5px 5px;
	    box-shadow: 1px 1px 1px 1px #9A9A9A;
	    color: #FFFFFF !important;
	    cursor: pointer;
	    display: inline-block;
	    font: 14px/100% "Verdana",geneva,sans-serif;
	    outline: medium none;
	    padding: 2px 5px;
		margin-left:5px;
	    text-align: center;
	    text-decoration: none !important;
	    text-transform: uppercase;
	    vertical-align: baseline;
	    width: auto;
	}
	.filterButton:hover {
	    background: -moz-linear-gradient(center top , #0095CC, #00678E) repeat scroll 0 0 transparent;
	}
	.endButton{
		float:left;
		background-color:#CF2A27;
	    border: 1px solid red;
	    border-radius: 5px 5px 5px 5px;
	    box-shadow: 1px 1px 1px 1px #9A9A9A;
	    color: #FFFFFF !important;
		margin-left:5px;
		margin-top:5px;
	    cursor: pointer;
	    display: inline-block;
	    font: 14px/100% "Verdana",geneva,sans-serif;
	    outline: medium none;
	    padding: 2px 5px;
	    text-align: center;
	    text-decoration: none !important;
	    text-transform: uppercase;
	    vertical-align: baseline;
	    width: auto;
	}
	.endButton:hover{
		background-color:#C10000;
	}
	.transferButton{
		float:left;
		background-color:#4285F4;
	    border: 1px solid #0076A3;
	    border-radius: 5px 5px 5px 5px;
	    box-shadow: 1px 1px 1px 1px #9A9A9A;
	    color: #FFFFFF !important;
		margin-left:5px;
		margin-top:5px;
	    cursor: pointer;
	    display: inline-block;
	    font: 14px/100% "Verdana",geneva,sans-serif;
	    outline: medium none;
	    padding: 2px 5px;
	    text-align: center;
	    text-decoration: none !important;
	    text-transform: uppercase;
	    vertical-align: baseline;
	    width: auto;
	}
	.transferButton:hover{
		background-color:#2B78E4;
	}
</style>

<cfinclude template="../../cfc/csc/constants.cfm" >
<link type="text/css" href='<cfoutput>#rootURL#/#publicPath#/css/ui.notify.css</cfoutput>' rel="stylesheet">
<script type="text/javascript" src="<cfoutput>#rootURL#/#publicPath#/js/jquery.notify.js</cfoutput>"></script>
<cfset jSonUtil = createObject("component", "#sessionPath#.lib.json.JSONUtil").init() />
<cftry>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetSessionById" returnvariable="sessionObj">
		<cfinvokeargument name="sessionId" value="#sessionId#" >
	</cfinvoke>
	<cfset shortCode= sessionObj.DATA.SHORTCODE>
	<cfset contactNumber = sessionObj.DATA.CONTACTNUMBER>
	<cfset sessionId = sessionObj.DATA.SESSIONID >
	<cfset sessionCode = sessionObj.DATA.SESSIONCODE >
	<cfset timeleft = sessionObj.DATA.TIMELEFT>
	<cfset isLocked = sessionObj.DATA.ISLOCKED>
	<cfset AgentId = sessionObj.DATA.AGENTID>
	
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetHauUserSetting" returnvariable="hauSettObj">
	</cfinvoke>
	<cfset HauUserSetting = {}>
	<cfset HauUserSetting.AutoRouteIncomingRequest = hauSettObj.AutoRouteIncomingRequest/>    
	<cfset HauUserSetting.AllowSesstionTransfer = hauSettObj.AllowSesstionTransfer />
	<cfset HauUserSetting.MaximumSessionsPerAgent = hauSettObj.MaximumSessionsPerAgent/>      
	<cfset HauUserSetting.MaximumSessionsPerAgentValue = hauSettObj.MaximumSessionsPerAgentValue/>
	<cfset HauUserSetting.AllowCannedResponses = hauSettObj.AllowCannedResponses/>
	<cfset HauUserSetting.AllowAgnetCreatedCannedResponses = hauSettObj.AllowAgnetCreatedCannedResponses/>
	<cfset HauUserSetting.AutoRouteReturningRequest = hauSettObj.AutoRouteReturningRequest/>
	
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.aau" method="GetCannedListForSessionDetail" returnvariable="ListCanned">
			<cfinvokeargument name="sessionId" value="#sessionObj.DATA.SESSIONID#" >
	</cfinvoke>
	<cfset listcanned = "#jSonUtil.serializeJSON(ListCanned.ROWS)#">  
<cfcatch type="Any" >
	<div>
		<H1 align="center" style="margin-top:50px">You are not allowed to view that session or session timed out</H1>
	</div>
	<cfreturn>
</cfcatch>
</cftry>

<cfif sessionCode EQ ''>
<div>
	<H1 align="center" style="margin-top:50px">You are not allowed to view that session or session timed out</H1>
</div>
<cfreturn>
</cfif>
<cfinclude template="inc_client.cfm" >
<!---<cfinclude template="dsp_HauScript.cfm" >--->
<div class="chatBox">
<div>
	Session ID: <cfoutput>#sessionCode#</cfoutput>
<div style="width:100%;min-width:80%px">
	<label id="MobileNumber_<cfoutput>#sessionId#</cfoutput>">
		Mobile Number: <cfoutput>#contactNumber#</cfoutput>
	</label>
	<label id="Carrier_<cfoutput>#sessionId#</cfoutput>" style="padding-left:50px">
		Carrier: <cfoutput >#carrier#</cfoutput>
	</label>
	<label id="CSC_<cfoutput>#sessionId#</cfoutput>" style="padding-left:50px">
		CSC: <cfoutput >#shortCode#</cfoutput>
	</label>
	<label id="Keyword_<cfoutput>#sessionId#</cfoutput>" style="padding-left:50px">
		Keyword: <cfoutput >#keyword#</cfoutput>
	</label>
	<label id="Time_<cfoutput>#sessionId#</cfoutput>" style="float:right; padding-left:20px;">
		<div id="countdown_time_<cfoutput>#sessionId#</cfoutput>"></div>
	</label>
</div>
<div>
	<hr/>
</div>
<div>
	Conversation log
</div>
<div id="chatPanel_<cfoutput>#sessionId#</cfoutput>" style="overflow-y:auto;height:300px;">
	<ul class = "list">
		<li class="item"></li>
	</ul>
</div>
<div>
<br/>

<cfif isLocked EQ SESSION_LOCKED>
<div>
	<label style="margin-left:2px;">
		Agent
	</label>
</div>
<textarea id="content_<cfoutput>#sessionId#</cfoutput>" style maxlength="160" rows="4" cols="115">
</textarea>
</cfif>
<div style="margin-bottom:20px;">
	<cfif isLocked EQ SESSION_LOCKED>
	<label id="remainChar_<cfoutput>#sessionId#</cfoutput>" style="float:right">
		160 of 160 characters available 
	</label>
	</cfif>
	</br>
	<div style="padding: 10px 0px;">
		<div>
			<cfif isLocked EQ SESSION_LOCKED>
				<input type="button" value="End" class="endButton" onclick="CloseSession_<cfoutput>#sessionId#</cfoutput>()"/>
				<cfif #HauUserSetting.AllowSesstionTransfer# EQ 1>
					<input type="button" value="Transfer" class="transferButton" id = "transfer_btn_<cfoutput>#sessionId#</cfoutput>"/>
				</cfif>
			</cfif>
		</div>
		<div style="float:right;">
		<cfif isLocked EQ SESSION_LOCKED>
			<cfif #HauUserSetting.AllowCannedResponses# EQ 1>
				<input type="button" value="Canned Response" class="button filterButton small CannedBtn" id="canned_btn_<cfoutput>#sessionId#</cfoutput>">
			    <div class="tooltip" id="cannedList<cfoutput>#sessionId#</cfoutput>">
			    	<div>
			    		<h1>Insert Canned Response
						<span onclick="CloseCannedList(<cfoutput>#sessionId#</cfoutput>)" id="CloseCannedList_<cfoutput>#sessionId#</cfoutput>" class="closeCanned" style="float:right" title="Close Canned"></span>
						</h1>
			    	</div>
					<input type="text" name="searchCanned" id="searchCanned_<cfoutput>#sessionId#</cfoutput>" onkeyup="SearchCanned_<cfoutput>#sessionId#</cfoutput>()" placeholder="Search canned">
					<p>Select a response to insert it into the reply field</p>
				  	<div id="listcanned_<cfoutput>#sessionId#</cfoutput>" class="listcanneds" style="height:170px; overflow:auto;">
			  		<cfoutput>
				  		<ul id="ul_listcanned_<cfoutput>#sessionId#</cfoutput>">
				  		<cfloop array="#jSonUtil.deserialzeJSON(listcanned)#" index="canneditem">
				  			<li onclick="SetReply('#sessionId#', '#canneditem.RESPONSEENCODE#')">
				  				<b>#canneditem.TITLE#</b><br>
				  				#canneditem.RESPONSE#
				  			</li>
				  		</cfloop>
				  		</ul>
					</cfoutput>
				  	</div>
				</div>
			</cfif>
			<input type="button" onclick="SendChat_<cfoutput>#sessionId#</cfoutput>()" value="Send" class="button filterButton small" id = "comment_btn_<cfoutput>#sessionId#</cfoutput>">
		</cfif>
			<cfif isLocked NEQ SESSION_LOCKED>
				<input type="button" onclick="LockSession(<cfoutput>#sessionId#</cfoutput>)" value="Lock Session" class="button filterButton small" id = "lockSession_btn">	
			<cfelse>
				<input type="button" onclick="UnLockSession(<cfoutput>#sessionId#</cfoutput>)" value="UnLock Session" class="button filterButton small" id = "unlockSession_btn">
			</cfif>
		</div>
	</div>
</div>
</div>
<div>
<input type="hidden" id="transferButton_<cfoutput>#Session.UserId#</cfoutput>" value="" />
<input type="hidden" id="transferingButton_<cfoutput>#Session.UserId#</cfoutput>" value="" />
<input type="hidden" id="denclinedTransferButton_<cfoutput>#Session.UserId#</cfoutput>" value="" />
<div id="container" style="display:none">
	<div id="transferBox">		
		<h1>#{title}</h1>
		<div class="title">#{text}</div>
		<div style="margin-top:10px;"><center>#{button}</center></div>
	</div>	
</div>

<div id="TransferingContainer" style="display:none">
	<div id="transferingBox">		
		<h1>#{title}</h1>
		<div class="title">#{text}</div>
		<div style="margin-top:10px;"><center>#{button}</center></div>
	</div>
</div>

<div id="DeclinedTransferingContainer" style="display:none">
	<div id="declinedTransferingBox">	
		<a class="ui-notify-close ui-notify-cross" href="#">X</a>	
		<h1>#{title}</h1>
		<div class="title">#{text}</div>
	</div>
</div>
<script type="text/javascript" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/js/jquery.tooltip.min.js"></script>
<script id="tmplPreviewListCanned<cfoutput>#sessionId#</cfoutput>" type="text/x-jquery-tmpl">
	<li onclick="SetReply('<cfoutput>#sessionId#</cfoutput>', '${RESPONSEENCODE}')">
				  				<b>${TITLE}</b><br>
				  				${RESPONSE}
	</li>
</script>
<script>
	
	function create( template, vars, opts ){
		return $container.notify("create", template, vars, opts);
	}
	
	function createTransfering( template, vars, opts ){
		return $transferingContainer.notify("create", template, vars, opts);
	}
	
	function createDeclinedTransfering( template, vars, opts ){
		return $declinedTransferingContainer.notify("create", template, vars, opts);
	}
	
	$(function(){
		$container = $("#container").notify();
		$transferingContainer = $("#TransferingContainer").notify();
		$("#transferButton_<cfoutput>#Session.UserId#</cfoutput>").on('click', function(event, params){
			var buttons = '<input type="button" id="AcceptTransfer_'+params.SessionId+'" onclick="client.AcceptTransfer('+params.SessionId+','+ params.FromAgent + ','+ params.ToAgent +','+ params.SessionCodeString +','+ params.ShortCodeNumber +','+ params.ContactNumber + ',<cfoutput>#keyword#</cfoutput>)" class="acceptButton" value="Accept">';
			buttons = buttons + '<input style="margin-left:20px;" type="button" id="DeclineTransfer_'+params.SessionId+'" onclick="client.DeclineTransfer('+params.SessionId+','+ params.FromAgent + ','+ params.ToAgent +','+ params.SessionCodeString +','+ params.ShortCodeNumber +','+ params.ContactNumber +')" class="declineButton" value="Decline">';
			buttons = buttons + '<a class="ui-notify-close ui-notify-cross" href="#" style="display:none;" id="CloseTransfer_'+params.SessionId+'">x</a>';
			create("transferBox", 
				{
					title:'Incoming Transfer', 
					text: params.FromAgentName + ' would like to transfer a session to you.',
					button: buttons
				},
				{
					expires:false
				}
			);
		});
		
		$declinedTransferingContainer = $("#DeclinedTransferingContainer").notify();
		$("#denclinedTransferButton_<cfoutput>#Session.UserId#</cfoutput>").on('click', function(event, params){
			createDeclinedTransfering("declinedTransferingBox", 
				{
					title:'Decline transfer', 
					text: 'Session transfer has declined'
				},
				{
					expires:false
				}
			);
		});
		
	});
	
	$("#canned_btn_<cfoutput>#sessionId#</cfoutput>").tooltip({ effect: 'slide'});
	
	function getObjects<cfoutput>#sessionId#</cfoutput>(obj, key, key1, val) {
		if (val != "") {
			var objects = [];
			for (var i in obj) {				
				if (!obj.hasOwnProperty(i)) 
					continue;
				if (typeof obj[i] == 'object') {
					objects = objects.concat(getObjects<cfoutput>#sessionId#</cfoutput>(obj[i], key, key1, val));
				}
				else
				{
					var valueKey = decodeURIComponent(obj[key]).toLowerCase();
					var valueKey1 = decodeURIComponent(obj[key1]).toLowerCase();
					
					if ((i == key && valueKey.indexOf(val) != -1) || (i == key1 && valueKey1.indexOf(val) != -1)) {
						var index = -1;
						if (objects.length > 0) {
							for (item in objects) {
								if (objects[item].CANNEDID == obj["CANNEDID"]) {
									index = item;
								}
							}
						}
						if (index == -1) {
							objects.push(obj);
						}
					}
				}
			}
			return objects;
		}
	}
	
	function SearchCanned_<cfoutput>#sessionId#</cfoutput>()
	{
		Canneds = {
			"Canneds": <cfoutput>#listcanned#</cfoutput>
		};
		var searchCannedText = $("#searchCanned_<cfoutput>#sessionId#</cfoutput>").val();
		var listSearchCanned = <cfoutput>#listcanned#</cfoutput>;
		if (searchCannedText != "") {
			var listSearchCanned = getObjects<cfoutput>#sessionId#</cfoutput>(Canneds, 'TITLE', 'RESPONSE', searchCannedText);
		}		
		
		$("#ul_listcanned_<cfoutput>#sessionId#</cfoutput>").html("");
		$("#tmplPreviewListCanned<cfoutput>#sessionId#</cfoutput>").tmpl(listSearchCanned).appendTo('#ul_listcanned_<cfoutput>#sessionId#</cfoutput>');
	}
	function SetReply(sessionid, response)
	{
		response = decodeURIComponent(response);
		$("#content_"+sessionid).val(response);
		eval("ViewRemainChars_"+sessionid+"();");
		$("#canned_btn_"+sessionid).mouseleave();
	}
	function CloseCannedList(sessionid)
	{
		$("#canned_btn_"+sessionid).mouseleave();
	}
	
	var SessionCountdown_<cfoutput>#sessionId#</cfoutput> = function () {
    var time_left = 5400; //number of seconds for countdown
    var output_element_id = 'countdown_time_<cfoutput>#sessionId#</cfoutput>';
    var keep_counting = 1;
    var no_time_left_message = 'Session is timeout!';
 
    function countdown() {
        if(time_left < 2) {
            keep_counting = 0;
        }
 
        time_left = time_left - 1;
		//disconnect when session timout
		if(time_left == 0){
			client.socket.disconnect();
		}
    }
	

 
    function add_leading_zero(n) {
        if(n.toString().length < 2) {
            return '0' + n;
        } else {
            return n;
        }
    }
 
    function format_output() {
        var minutes, seconds;
        seconds = time_left % 60;
        minutes = Math.floor(time_left / 60) % 60;        
  		hours = Math.floor(time_left / 3600);
        seconds = add_leading_zero(seconds);
        minutes = add_leading_zero(minutes);
        hours = add_leading_zero(hours);
 		
        return 'Session live for: ' + hours + ' hours ' + minutes + ' minutes ' + seconds + ' seconds';
    }
 
    function show_time_left() {
        $("#"+output_element_id).html(format_output());//time_left;
    }
 
    function no_time_left() {
        $("#"+output_element_id).html(no_time_left_message);
    }
 
    return {
        count: function () {
            countdown();
            show_time_left();
        },
        timer: function () {
            SessionCountdown_<cfoutput>#sessionId#</cfoutput>.count();
 
            if(keep_counting) {
                setTimeout("SessionCountdown_<cfoutput>#sessionId#</cfoutput>.timer();", 1000);
            } else {
                no_time_left();
            }
        },
        //Recalculation of time that is left
        setTimeLeft: function (t) {
            time_left = t;
            if(keep_counting == 0) {
                SessionCountdown_<cfoutput>#sessionId#</cfoutput>.timer();
            }
			
        },
        init: function (time, keep_counting, element_id) {
            time_left = time;
			keep_counting = keep_counting;
            output_element_id = element_id;
            SessionCountdown_<cfoutput>#sessionId#</cfoutput>.timer();
        }
    };
}();
	
</script>
<script type="text/javascript">
	SessionCountdown_<cfoutput>#sessionId#</cfoutput>.init(<cfoutput>#timeleft#</cfoutput>, 1, 'countdown_time_<cfoutput>#sessionId#</cfoutput>');
	!function(window, $, client) {
				client.data.username = "Agent";
				client.data.contactstring ='<cfoutput>#contactNumber#</cfoutput>';
				client.data.userid=10;
				client.data.usertype=1;
				client.data.shortcode = '<cfoutput>#shortCode#</cfoutput>';
				client.data.sessionid = '<cfoutput>#sessionId#</cfoutput>';
				client.data.carrier = '<cfoutput>#carrier#</cfoutput>';
			}
	(window, window.jQuery, window.client);
	
	
	$('#content_<cfoutput>#sessionId#</cfoutput>').keyup(function(){  
			SessionCountdown_<cfoutput>#sessionId#</cfoutput>.setTimeLeft(5400);
	        //get the limit from maxlength attribute  
	        var limit = parseInt($(this).attr('maxlength'));  
	        //get the current text inside the textarea  
	        var text = $(this).val();  
	        //count the number of characters in the text  
	        var chars = text.length;  
	  
	        //check if there are more characters then allowed  
	        if(chars > limit){  
	            //and if there are use substr to get the text before the limit  
	            var new_text = text.substr(0, limit);  
	  
	            //and change the current text with the new text  
	            $(this).val(new_text);  
	        }  
	    });  
	    $('#content_<cfoutput>#sessionId#</cfoutput>').bind('cut copy paste', function (e) {
			ViewRemainChars_<cfoutput>#sessionId#</cfoutput>();
		});
	    $('#content_<cfoutput>#sessionId#</cfoutput>').keyup(function(){  
	        ViewRemainChars_<cfoutput>#sessionId#</cfoutput>();
	    });
		InitData_<cfoutput>#sessionId#</cfoutput>();

		function ViewRemainChars_<cfoutput>#sessionId#</cfoutput>(){
			var limit = parseInt($("#content_<cfoutput>#sessionId#</cfoutput>").attr('maxlength'));
			var value = (limit - $('#content_<cfoutput>#sessionId#</cfoutput>').val().length) + ' of 160 characters available';
			$('#remainChar_<cfoutput>#sessionId#</cfoutput>').text(value);
		}
		
		function InitData_<cfoutput>#sessionId#</cfoutput>(){
			var data = {
				shortCode: '<cfoutput>#shortCode#</cfoutput>',
				contactNumber:'<cfoutput>#contactNumber#</cfoutput>',
				sessionid: '<cfoutput>#sessionId#</cfoutput>',
				keyword:'<cfoutput>#keyword#</cfoutput>'
				};
		    ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc','GetChatContent',data,"Get messages fail!",
			function(d){
				var chatData =d.DATA;
				$("#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list").append(chatData);
				AnimateChatPanel_<cfoutput>#sessionId#</cfoutput>();
			});
		}
		
		function SendChat_<cfoutput>#sessionId#</cfoutput>(){
			var chatMes = $("#content_<cfoutput>#sessionId#</cfoutput>").val();			
			if(chatMes == undefined || chatMes == "" ) return;
			var data = {
				shortCode: '<cfoutput>#shortCode#</cfoutput>',
				contactString:'<cfoutput>#contactNumber#</cfoutput>',
				chatData: chatMes
			};
		    ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc','SendChat',data,"Send messages fail!",
			function(d){
				var chatData =d.DATA;
				//$("#chatPanel_<cfoutput>#sessionId#</cfoutput>").append(chatData);
				AnimateChatPanel_<cfoutput>#sessionId#</cfoutput>();
			});
		}
		
		function CloseSession_<cfoutput>#sessionId#</cfoutput>(){
			jConfirm( "Would you  like to end session " + '<cfoutput>#sessionCode#</cfoutput>' + "?", "END SESSION", function(result){ 
				if (result) {
					var data = {
						sessionId: '<cfoutput>#sessionId#</cfoutput>',
					};
					ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'CloseSession', data, "Close Session fail!", function(d){
						var agentData = {
							UserId : '<cfoutput>#AgentId#</cfoutput>',
							ContactString: '<cfoutput>#contactNumber#</cfoutput>',
							SessionId:'<cfoutput>#sessionId#</cfoutput>'
						};
						client.socket.emit('remove_active_session', agentData);
						location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/aau/AgentMonitoring";
					});
				}
			});
		}
		
		function AnimateChatPanel_<cfoutput>#sessionId#</cfoutput>(){
			var myDiv = $("#chatPanel_<cfoutput>#sessionId#</cfoutput>");
			myDiv.animate({ scrollTop: myDiv.prop("scrollHeight")}, 500);
		}
		
		//lock session
	function LockSession(sesionid)
	{
		jConfirm( 'Do you want to lock this session?', 'Lock Sesion', function(result) { 
			if(result){			    		
				var data = {sesionid: sesionid, sessionlock: '<cfoutput>#SESSION_LOCKED#</cfoutput>'};
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'UpdateSesionLock', data, "Lock Session fail!", function(d ) {
					jAlert("Session locked", "Success!", function(result) {
						//emit to socket server, lock session screen of aau agent
						client.socket.emit('lock_session', {
							sesionId: sesionid
						});
						location.reload(); 
					});
				});
			}
		});
	}
	function UnLockSession(sesionid)
	{
		jConfirm( 'Do you want to unlock this session?', 'UnLock Sesion', function(result) { 
			if(result){			    		
				var data = {sesionid: sesionid, sessionlock: '<cfoutput>#SESSION_UNLOCKED#</cfoutput>'};
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/aau.cfc', 'UpdateSesionLock', data, "UnLock Session fail!", function(d ) {
					jAlert("Session unlocked", "Success!", function(result) {
						//emit to socket server, un lock session screen of hau agent
						client.socket.emit('unlock_session', {
							sesionId: sesionid
						});
						location.reload(); 
					});
				});
			}
		});
	}
		
</script>
<cfinclude template="dsp_HauScript.cfm" >
<style>
	#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list{
	margin: 0;
	padding: 0;
}
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.item{
	list-style: none outside none;
	margin-top:2px;
}
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.item .member{
	margin: 5px 10px 5px 0;
	float:left;
}
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.item .comment{
    font-size: 14px;
	width:100%;	
	max-width:700px;
	padding-left:20px;
    white-space: pre-wrap;      /* CSS3 */   
    white-space: -moz-pre-wrap; /* Firefox */    
    white-space: -pre-wrap;     /* Opera <7 */   
    white-space: -o-pre-wrap;   /* Opera 7 */    
    word-wrap: break-word;      /* IE */
}
	
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.item .date{
    margin: 5px 0px 5px 0px;
	float:right;
    font-size: 13px;
	width:20%;
}
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.User{
    background-color:#BBBBBB;
	border-radius: 5px;
	min-height:70px;
	height:auto;
	margin: 0px 10px 0px 10px;
}
#chatPanel_<cfoutput>#sessionId#</cfoutput> > ul.list > li.Agent{
    background-color:#28CBEF;
	border-radius: 5px;
	min-height:70px;
	margin: 2px 5px 0px 5px;
}


  /* tooltip styling */
  .tooltip {
	    display:none;
	    background:url(<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/black_arrow_big.png);
	    height:163px;
	    padding:25px 30px 10px 30px;
	    width:310px;
		height:280px;
	    font-size:12px;
  	}
	.tooltip h1
	{
		font-size:13px;
		font-weight:bold;
		margin-top:0px;	
		text-align:center;
	}
	.tooltip .closeCanned
	{
		background:url(<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/close.png);
		cursor:pointer;
		display: block;
	    font-size: 0;
	    height: 12px;
	    line-height: 12px;
	    width: 12px;
	}
	.tooltip .closeCanned:hover
	{
		cursor:pointer;
		background:url(<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/close_hover.png);
	}
	.tooltip p
	{
		font-size:11px;
		margin-bottom:3px;	
	}
  .tooltip a {
    color:#ad4;
    font-size:11px;
    font-weight:bold;
  }
  .tooltip input{
	    border: 1px solid black;
	    border-radius: 12px 12px 12px 12px;
	    font-size: 11px;
	    outline: medium none;
	    padding: 4px 10px;
	    position: relative;
	    top: 3px;
	    width: 280px;
	}
	.tooltip ul
	{
		padding:10px;
		margin:0px;
	}
	.tooltip ul li
	{
		list-style:none;
		padding-bottom:2px;
		padding-top:2px;
		font-size:13px;
		border-bottom:1px solid #CCC;
	}
	.tooltip ul li:hover
	{
		background-color:#CCC;
		cursor:pointer;
	}
	
	/*style for chat item*/
	.chatitem {
	    box-shadow: 0 1px #B2B2B2;
	    display: inline-block;
	    padding: 10px 18px;
	    position: relative;
	    vertical-align: top;
		width:95%;	
		white-space: pre-wrap;      /* CSS3 */   
	    white-space: -moz-pre-wrap; /* Firefox */    
	    white-space: -pre-wrap;     /* Opera <7 */   
	    white-space: -o-pre-wrap;   /* Opera 7 */    
	    word-wrap: break-word;      /* IE */
	}
	.me .member{
		float:left;
		margin-left: 5px;
		font-weight:bold;
		width:10%;
	}
	
	.you .member{
		float:left;
		margin-left: 5px;
		font-weight:bold;
		width:10%;
		color: #B00301;
	}	
	.chatitem .date{
		float:left;
	    font-size: 11px;
		width:10%;
	}
	.me .comment{
		float:left;
	    margin-left: 20px;
	    font-size: 13px;
		width:75%;
		white-space: pre-wrap;      /* CSS3 */   
	    white-space: -moz-pre-wrap; /* Firefox */    
	    white-space: -pre-wrap;     /* Opera <7 */   
	    white-space: -o-pre-wrap;   /* Opera 7 */    
	    word-wrap: break-word;      /* IE */
	}
	.you .comment{
		float:left;
	    margin-left: 20px;
	    font-size: 13px;
		width:75%;
		color: #B00301;
		white-space: pre-wrap;      /* CSS3 */   
	    white-space: -moz-pre-wrap; /* Firefox */    
	    white-space: -pre-wrap;     /* Opera <7 */   
	    white-space: -o-pre-wrap;   /* Opera 7 */    
	    word-wrap: break-word;      /* IE */
	}
  </style>