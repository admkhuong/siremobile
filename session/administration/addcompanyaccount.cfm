

<!--- Verify Super User --->


<cfoutput>
			<style type="text/css">
				<!---@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/rxform2.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/MB.css');--->
			<!---	@import url('#rootUrl#/#PublicPath#/css/jquery.alerts.css');--->
				@import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
			</style>
		   <!--- <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-1.6.4.min.js"></script>
		    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery-ui-1.8.9.custom.min.js"></script> 
			<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.alerts.js"></script>
		 	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/ValidationRegEx.js"></script>--->
		</cfoutput>
	    
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>


  <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					$('#account_type_personal').click();
					if($('#account_type_company').is(':checked')){
			  			$("#company_name").show();
			  		}
				});
				
				function isValidRequiredField(fieldId)
				{
					if(fieldId == 'company_name')
					{
						if(!$('#account_type_company').is(':checked'))
						{
							return true;
						}
					}
					if($("#" + fieldId).val() == '')
					{
						$("#err_" + fieldId).show();
						return false;
					}
					else
					{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				
				function isValidEmail()
				{
					var isValid = true;
					if($("#emailadd").val() == '')
					{
						isValid = false;
						$("#err_emailadd").show();
					}
					else
					{
						var filter = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if (!filter.test($("#emailadd").val())) 
						{
							isValid = false;
							$("#err_emailadd").show();
						}
						else
						{
							$("#err_emailadd").hide();
						}
					}
					return isValid;
				}
				
				<!--- Validay Name specified --->
				function isValidAccountType() 
				{ 					
					if(!isValidRequiredField('company_name'))
					{
						$("#err_company_name").show();
						return false;
					}
					else
					{
						$('#err_type').hide();
					}
											
					return true;
					
				}
				
				<!---// validate pass and confirm pass--->
				function isValidPassword(){
					var isvalid = true;
					if(!isValidRequiredField('inpPasswordSignup')){
		  				isvalid = false;
		  			}
		  			if(!isValidRequiredField('inpConfirmPassword')){
		  				isvalid = false;
		  			}
		  			if($("#inpPasswordSignup").val() != $("#inpConfirmPassword").val()){
		  				isvalid = false;
		  			}
					return isvalid;
				}
				<!---// validate form--->
				function isValidSignupForm(){
					var isValidForm = true;
					
					<!---// account type--->
					if(!isValidAccountType()){
						isValidForm = false;
					}
					
					<!---// name--->
					if(!isValidRequiredField('fname')){
		  				isValidForm = false;
		  			}
					if(!isValidRequiredField('lname')){
		  				isValidForm = false;
		  			}
		  			<!---// email--->
		  			if(!isValidEmail()){
		  				isValidForm = false;
		  			}
		  			<!---// password--->
		  			if(!isValidPassword()){
		  				isValidForm = false;
		  			}
					return isValidForm;
				}
				<!---// Signup--->
				function signUp(){
					if(!isValidSignupForm())
					{
						return false;
					}else
					{
						<!---// input--->
						var accountType = 'company';
						var companyName = '';
												
						accountType = 'company';
						companyName = $("#company_name").val();
						
						var fname = $("#fname").val();
						var lname = $("#lname").val();
						var userEmail = $("#emailadd").val();
						var pass = $("#inpPasswordSignup").val();
						var confirmPass = $("#inpConfirmPassword").val();
						
						try{
							$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							data:  {
								INPMAINEMAIL: userEmail,
								INPCOMPANYNAME: companyName,
								INPACCOUNTTYPE: accountType,
								INPNAPASSWORD: pass,
								INPFNAME: fname,
								INPLNAME: lname,
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
							success:		
								function(d) 
								{
									if(d.RESULT != "SUCCESS"){
										jAlert(d.MESSAGE, 'Error');
										return false;
									}else{
										<!---// sign up successfull - go to the administrator page--->
										
											
										jAlert('Signup Success!', 'OK!', function(result) { window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/permission/permission?UCID=" + d.USERID + "&roleType=2"; });
											
										
										<!---//$("#signup_box").html('');
										//var loginLink = '<a href="<cfoutput>#rootUrl#/#PublicPath#/home</cfoutput>">here</a>';
										//$("#signup_box").append('<h2 class="title message">Register successfully! Click ' + loginLink + ' to go login page.</h2>');--->
									}
								} 		
								
							});
						}catch(ex){
							jAlert('Signup Fail', 'Error');
							return false;
						}
					}
					return true;
				}
</script>				
				
	<cfoutput>
    
    
<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    	<h2 class="no-margin-top">				
              <label class='main_title title'><h2><span>Create <cfoutput>#BrandShort#</cfoutput> Company Admin Account</span></h2></label>            
        </h2>
  
        <div id="main" align="center"> 
    
            
            <div id="inner-bg-alt" style="min-height: 450px">
    
                <div class="content" id="signup_box">
                    <form action="" name="sign_up" method="post" id="sign_up" style="width:90%;">
                        
                                                                              
                        <div class='inp_box' id='company_box'>
                           
                            <div style="clear:both"></div>
                            
                            <label class='title'><span>Company Name</span></label><br />
                            <input name="company_name" id="company_name" type="text" class="textfield half"><br />
                            <label class='error' id='err_company_name'>This field is required.</label>
                        </div>
                    
                    
                    
                        <div class='inp_box'>
                                                        
                            <div style="clear:both"></div>
                            
                            <div class='inp_box'>
                                <div class='half'>
                                    <label class='title'><span>First Name</span></label>
                                    <input name="fname" id="fname" type="text" 
                                            class="textfield"><br />
                                    <label class='error' id='err_fname'>This field is required.</label>
                                </div>
                                <div class='half padd-left'>
                                    <label class='title'><span>Last Name</span></label>
                                    <input name="lname" id="lname" type="text" 
                                            class="textfield">
                                    <label class='error' id='err_lname'>This field is required.</label>
                                </div>
                            </div>
                            <div class='inp_box'>
                            <label class='title'><span>Email Address</span></label><br />
                            <input name="emailadd" id="emailadd" type="text" 
                                    class="textfield full"><br />
                            <label class='error' id='err_emailadd'>Invalid Email.</label>
                            </div>
                            
                            
                            
                             <div style="float:left; width:300px;">                               								
                                <div class='inp_box' style="margin-top:15px;">
                                    <label class="inTitle">Password</label><br />
                                    <input type="text" name="inpPasswordSignup" id="inpPasswordSignup" class="textfield half" value="#getSecurePassword#">
                                    <br />
                                    <label class="error" id="err_inpPassword">This field is required.</label>
                                </div>
                                
                                <div class='inp_box' style="margin-top:15px;" >
                                    <label class="inTitle">Confirm Password</label><br />
                                    <input type="text" name="inpConfirmPassword" id="inpConfirmPassword" class="textfield half" value="#getSecurePassword#">
                                    <br />
                                    <label class="error" id="err_inpConfirmPassword">Invalid confirm password.</label>
                                </div>
                                
                                <div class='inp_box' style="margin-top:15px;">
                                    <span id="SignUpButton"> <button type="button" class="ui-corner-all" onClick="signUp(); return false;">Signup</button></span>
                                    <div id="loadingSignUp" style="float:right; display:inline; visibility:hidden; margin-right:5px;">Adding New Account...<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                                </div>
                                                                 
                            </div>
                             
                            <div class='inp_box' style="float:left; width:300px; margin-left:30px;">
                                <h4>Secure Password Requirements</h4>
                                Because you are dealing with messaging to consumers, passwords must meet the following strict requirements.
                                    <ul style="margin-left:25px;">
                                        <li class='title'>must be at least 8 characters in length</li>
                                        <li class='title'>must have at least 1 number</li>
                                        <li class='title'>must have at least 1 uppercase letter</li>
                                        <li class='title'>must have at least 1 lower case letter</li>
                                        <li class='title'>must have at least 1 special character</li>
                                    </ul>
                                <BR />
                                <BR />
                                NEVER - send your username and password in the same email
                                <BR />
                                NEVER - sticky note your password. There are many tools available to help you from having to type your password over and over - including those built in to modern browsers, 
                                <BR />
                                <BR />
                               
                               
                            </div>
                            
                            
                        </div>
                    </form>
                </div>
                
            </div>
            
        </div>		

		<p class="lead"></p>
    	
        <row>
        	
            <div class="" style="margin-bottom:25px;">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->
        
    </cfoutput>





