
<cfinclude template="../../session/cfc/csc/constants.cfm">

<!--- Verify Super User --->
<!--- Verify Company Account Admin --->

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
    <cfinvokeargument name="userId" value="#session.userId#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>


<!---<cfdump var="#getCurrentUser#">--->

<!--- Stop the URL hackers!--->
<cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" AND getCurrentUser.USERROLE NEQ "SuperUser">
	<H1>You do not have access rights to this page!</H1>
    
    <!--- Log error log the user id who attempted this to track who might be attempting URL hacking--->
    <cftry>  
                	
		<cfset ENA_Message = "Attempt to add user page access without valid credentials">
        <cfset SubjectLine = "SimpleX Security Notification">
        <cfset TroubleShootingTips="See CatchMessage_vch in this log for details">
        <cfset ErrorNumber="#LOG_BADCREDENTIALS#">
        <cfset AlertType="1">
                       
        <cfquery name="InsertToErrorLog" datasource="#Session.DBSourceEBM#">
            INSERT INTO simplequeue.errorlogs
            (
                ErrorNumber_int,
                Created_dt,
                Subject_vch,
                Message_vch,
                TroubleShootingTips_vch,
                CatchType_vch,
                CatchMessage_vch,
                CatchDetail_vch,
                Host_vch,
                Referer_vch,
                UserAgent_vch,
                Path_vch,
                QueryString_vch
            )
            VALUES
            (
                #ErrorNumber#,
                NOW(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(SubjectLine)#">, 
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(ENA_Message)#">, 
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(TroubleShootingTips)#">,     
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM('User ID Attempting Access ' & session.userId)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#CGI.REMOTE_ADDR#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_HOST)#">, 
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_REFERER)#">, 
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.HTTP_USER_AGENT)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.PATH_TRANSLATED)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(CGI.QUERY_STRING)#">
            )
        </cfquery>
    
    <cfcatch type="any">
        
    </cfcatch>
        
    </cftry>    
              
    <cfabort />            
</cfif>




<cfoutput>

	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>

    <style type="text/css">
    
        @import url('#rootUrl#/#PublicPath#/css/administrator/login.css');
		
		span.selectmenucustom {
			border: 1px ##eee solid;
			border-radius: 0;
			height: 24px !important;
			outline:none;
		}	
		
		##inpCompanyId-button
		{
			background:none !important;
			border:none !important;	
			padding:none !important;
			outline:none;
		}
		
		.btn 
		{
			height: 32px;
			padding: 5px;
		}
				
    </style>
    
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	
</cfoutput>
	    


  <script type="text/javascript" language="javascript">
				
				$(document).ready(function(){
					
					<cfif getCurrentUser.USERROLE NEQ "CompanyAdmin">
						
					</cfif>
		
				});
				function isValidRequiredField(fieldId){
					if(fieldId == 'company_name'){
						if(!$('#account_type_company').is(':checked')){
							return true;
						}
					}
					if($("#" + fieldId).val() == ''){
						$("#err_" + fieldId).show();
						return false;
					}else{
						$("#err_" + fieldId).hide();
						return true;
					}
				}
				function isValidEmail(){
					var isValid = true;
					if($("#emailadd").val() == ''){
						isValid = false;
						$("#err_emailadd").show();
					}else{
						var filter = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
						if (!filter.test($("#emailadd").val())) {
							isValid = false;
							$("#err_emailadd").show();
						}else{
							$("#err_emailadd").hide();
						}
					}
					return isValid;
				}
													
				
				<!---// validate pass and confirm pass--->
				function isValidPassword(){
					var isvalid = true;
					if(!isValidRequiredField('inpPasswordSignup')){
		  				isvalid = false;
		  			}
		  			if(!isValidRequiredField('inpConfirmPassword')){
		  				isvalid = false;
		  			}
		  			if($("#inpPasswordSignup").val() != $("#inpConfirmPassword").val()){
		  				isvalid = false;
		  			}
					return isvalid;
				}
				<!---// validate form--->
				function isValidSignupForm(){
					var isValidForm = true;
					
					<!---// name--->
					if(!isValidRequiredField('fname')){
		  				isValidForm = false;
		  			}
					if(!isValidRequiredField('lname')){
		  				isValidForm = false;
		  			}
		  			<!---// email--->
		  			if(!isValidEmail()){
		  				isValidForm = false;
		  			}
		  			<!---// password--->
		  			if(!isValidPassword()){
		  				isValidForm = false;
		  			}
					return isValidForm;
				}
				
				function isValidPassword()
				{
					if ($("#inpPasswordSignup").val().length < 8)
					{
						
						bootbox.alert("Password Validation Failure - Password must be at least 8 characters in length");
						return false;
					 					  
					}
					  
					var hasUpperCase = /[A-Z]/.test($("#inpPasswordSignup").val());
					if (!hasUpperCase)
					{
						
						bootbox.alert("Password Validation Failure - Password must have at least 1 uppercase letter");
						return false;
					 					  
					}
										
					var hasLowerCase = /[a-z]/.test($("#inpPasswordSignup").val());
					if (!hasLowerCase)
					{
						
						bootbox.alert("Password Validation Failure - Password must have at least 1 lowercase letter");
						return false;
					 					  
					}
					
					var hasNumbers = /\d/.test($("#inpPasswordSignup").val());
					if (!hasNumbers)
					{
						
						bootbox.alert("Password Validation Failure - Password must have at least 1 number");
						return false;
					 					  
					}
										
					var hasNonalphas = /\W/.test($("#inpPasswordSignup").val());
					if (!hasNonalphas)
					{
						
						bootbox.alert("Password Validation Failure - Password must have at least 1 special character");
						return false;
					 					  
					}
					
					return true;
															
				}
				
				<!---// Signup--->
				function signUp(){
							
					$('#SignUpButton').hide();			
					$("#loadingSignUp").css('visibility', 'visible');
			
					
					if(!isValidPassword())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					
					if(!isValidSignupForm())
					{
						$('#SignUpButton').show();			
						$("#loadingSignUp").css('visibility', 'hidden');
						return false;
					}
					else
					{
						<!---// input--->
															
						
						var fname = $("#fname").val();
						var lname = $("#lname").val();
						var userEmail = $("#emailadd").val();
						var pass = $("#inpPasswordSignup").val();
						var confirmPass = $("#inpConfirmPassword").val();
						
						try
						{
							$.ajax({
							type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
							url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RegisterNewAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
							dataType: 'json',
							data:  {
								INPMAINEMAIL: userEmail,
								INPTIMEZONE: '31',
								INPACCOUNTTYPE: 'personal',
								INPNAPASSWORD: pass,
								INPFNAME: fname,
								INPLNAME: lname,
								INPCOMPANYID: $("#inpCompanyId").val()
							},					  
							error: function(XMLHttpRequest, textStatus, errorThrown) {$('#SignUpButton').show(); $("#loadingSignUp").css('visibility', 'hidden');},					  
							success:		
								function(d) 
								{
									if(d.RESULT != "SUCCESS")
									{
										bootbox.alert(d.MESSAGE);
										$('#SignUpButton').show();			
										$("#loadingSignUp").css('visibility', 'hidden');
										return false;
									}
									else
									{
										<!---// sign up successfull - go to the administrator page--->
																				
										<cfif getCurrentUser.USERROLE EQ "CompanyAdmin">
																				
											bootbox.alert('Signup Success!', function(result) { window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/permission/permission?UCID=" + d.USERID + "&roleType=2"; });
										
										<cfelseif getCurrentUser.USERROLE EQ "SuperUser">
											
											bootbox.alert('Signup Success!', function(result) { window.location.href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/administration/permission/permission?UCID=" + d.USERID + "&roleType=2"; });
											
										</cfif>
																				
										<!---//$("#signup_box").html('');
										//var loginLink = '<a href="<cfoutput>#rootUrl#/#PublicPath#/home</cfoutput>">here</a>';
										//$("#signup_box").append('<h2 class="title message">Register successfully! Click ' + loginLink + ' to go login page.</h2>');--->
									}
								} 		
								
							});
						}
						catch(ex)
						{
							bootbox.alert('Signup Fail');
							$('#SignUpButton').show();			
							$("#loadingSignUp").css('visibility', 'hidden');
							return false;
						}
					}
					return true;
				}
</script>				
				
		<cfoutput>
        
        

<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    	<h2 class="no-margin-top">
				<cfif getCurrentUser.USERROLE NEQ "CompanyAdmin" >
                    <label class='main_title title'><span>Create New <cfoutput>#BrandShort#</cfoutput> Account</span></label>
                <cfelse>
                    <label class='main_title title'><span>Create New Company Account</span></label>
                </cfif>
         </h2>
    
	  
    		<div id="main" align="center">         
				
        		<div id="inner-bg-alt" style="min-height: 450px">
        
					<div class="content" id="signup_box">
						<form action="" name="sign_up" method="post" id="sign_up" style="width:90%;">
							                            
                            
                            <cfif getCurrentUser.USERROLE eq "CompanyAdmin">
									<input type="hidden" name="inpCompanyId" id="inpCompanyId" value="#getCurrentUser.COMPANYACCOUNTID#"  />                            
                            <cfelse>
                                                        
                            	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="GetCompanyList" returnvariable="RetVarGetCompanyList"></cfinvoke>
            
            					<!--- <cfdump var="#RetVarGetCompanyList#">	--->
            
                            	<!--- List all Companies in Drop Down - add filter later ??? --->
                                
                                   <div class='' id='company_box'>
                                        <label class='title'><span>Choose Company - optional</span></label><br />
                                        
                                        <select id="inpCompanyId" name="inpCompanyId">
                                            <option value="0">Default - Not a Company User account</option>
                                            
                                            <cfif RetVarGetCompanyList.RXRESULTCODE EQ "1">
                                                <cfloop query="RetVarGetCompanyList.QUERYOUT">
                                                        <option value="#RetVarGetCompanyList.QUERYOUT.CompanyAccountId_int#">#RetVarGetCompanyList.QUERYOUT.CompanyName_vch#</option>	
                                                </cfloop>
                                            </cfif>
                                         </select>       
                                       
                                    </div>               
                            
							</cfif>
                        
	                        <div style="clear:both"></div>
                        
							<div class='inp_box'>
								                                
								<div class='inp_box'>
									<div class='half'>
										<label class='title'><span>First Name</span></label><br />
										<input name="fname" id="fname" type="text" 
												class="textfield"><br />
										<label class='error' id='err_fname'>This field is required.</label>
									</div>
                                </div>
                                 
                                <div class='inp_box'>
								    <div class='half'>
										<label class='title'><span>Last Name</span></label>
										<input name="lname" id="lname" type="text" 
												class="textfield">
										<label class='error' id='err_lname'>This field is required.</label>
									</div>
								</div>
								
                                <div style="clear:both"></div>
                                
                                <div class='inp_box'>
								<label class='title'><span>Email Address</span></label><br />
								<input name="emailadd" id="emailadd" type="text" 
										class="textfield full"><br />
								<label class='error' id='err_emailadd'>Invalid Email.</label>
								</div>
								
                                <div style="clear:both"></div>
                                                                                              
                                <div style="float:left; width:300px;">                               								
                                    <div class='inp_box' style="margin-top:15px;">
                                        <label class="inTitle">Password</label><br />
                                        <input type="text" name="inpPasswordSignup" id="inpPasswordSignup" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpPassword">This field is required.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;" >
                                        <label class="inTitle">Confirm Password</label><br />
                                        <input type="text" name="inpConfirmPassword" id="inpConfirmPassword" class="textfield half" value="#getSecurePassword#">
                                        <br />
                                        <label class="error" id="err_inpConfirmPassword">Invalid confirm password.</label>
                                    </div>
                                    
                                    <div class='inp_box' style="margin-top:15px;">
                                        <span id="SignUpButton"> <button type="button" class="ui-corner-all" onClick="signUp(); return false;">Signup</button></span>
                                        <div id="loadingSignUp" style="float:right; display:inline; visibility:hidden; margin-right:5px;">Adding New Account...<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                                    </div>
                                                                     
                               	</div>
                                 
                                <div class='inp_box' style="float:left; width:300px; margin-left:30px;">
                                    <h4>Secure Password Requirements</h4>
                                    Because you are dealing with messaging to consumers, passwords must meet the following strict requirements.
                                        <ul style="margin-left:25px;">
                                            <li class='title'>must be at least 8 characters in length</li>
                                            <li class='title'>must have at least 1 number</li>
                                            <li class='title'>must have at least 1 uppercase letter</li>
                                            <li class='title'>must have at least 1 lower case letter</li>
                                            <li class='title'>must have at least 1 special character</li>
                                        </ul>
                                    <BR />
                                    <BR />
                                    NEVER - send your username and password in the same email
                                    <BR />
                                    NEVER - sticky note your password. There are many tools available to help you from having to type your password over and over - including those built in to modern browsers, 
                                   	<BR />
                                    <BR />
                                   
                                   
                              	</div>
                                
							</div>
						</form>
			
            	</div>
			
            </div>
				
		</div>	
        
        <p class="lead"></p>

    	
        <row>
        	
            <div class="" style="margin-bottom:25px;">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->
	
</cfoutput>









