<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="getAccountInfo" returnvariable="getAccount" />

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Profile_Title#">
</cfinvoke>
<a href ="#" onclick="OpenDialog('changePassword'); return false;">Change password</a>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
	<cfinvokeargument name="operator" value="#Caller_ID_Title#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.passwordgen" method="GenSecurePassword" returnvariable="getSecurePassword"></cfinvoke>

<style>

form input {
    border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 3px 8px;
}

</style>

<cfoutput>	


<!---wrap the page content do not style this--->
<!--- /#page-content --->
<div id="page-content">
   
  <!--- /.container --->
  <div class="container" >
  
    <!---<h1 class="no-margin-top">SMS Content Management System</h1>--->
   		
                <form name="accounti" id="accounti" method="post">
                    
                    <cfif haveCidPermission.HAVEPERMISSION>
                        <div class="accountRow">
                            <label>Default Caller ID:</label>
                        </div>
                        <div class="accountRow">				
                            <input type="text" name="DefaultCID_vch" id="DefaultCID_vch" value="#getaccount.INFO.DefaultCID_vch#" mask="(999) 999-9999" />
                        </div>
                    </cfif>
            
                    <div class="accountRow">
                        <label>First Name:</label>
                    </div>
                    <div class="accountRow">
                        <input type="text" name="FirstName_vch" id="firstName" value="#getaccount.INFO.FirstName_vch#" />
                    </div>
                    
                    <div class="accountRow">
                        <label> Last Name: </label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="LastName_vch" id="lastName" value="#getaccount.INFO.LastName_vch#" />
                    </div>
                    
                    <div class="accountRow">
                        <label>Department:</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="Department_vch" id="department" value="#getaccount.INFO.Department_vch#" />
                    </div>
                       
                    <div class="accountRow">
                        <label> Position: </label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="Position_vch" id="position" value="#getaccount.INFO.Position_vch#"/>
                    </div>
                       
                    <div class="accountRow">
                        <label> Email:</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="EmailAddress_vch" id="emailAddress" value="#getaccount.INFO.EmailAddress_vch#" />
                    </div>
                    
                    <cfif getaccount.INFO.CompanyAccountId_int GT 0>
                        <div class="accountRow">
                            <label>Company Name: </label>
                            <i>#getaccount.INFO.CompanyName_vch#</i>
                        </div>
                    </cfif>
                    
                    <div class="accountRow">
                        <label> Work Phone Number: </label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="WorkPhoneStr_vch" id="WorkPhoneStr" value="#getaccount.INFO.WorkPhoneStr_vch#" mask="(999)999-9999" />
                    </div>
                       
                    <div class="accountRow">
                        <label> Extension: </label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="ExtensionWork_vch" id="extensionWork" value="#getaccount.INFO.ExtensionWork_vch#" mask="9999" />
                    </div>
                    
                    <div class="accountRow">
                        <label>Cell Phone:</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="HomePhoneStr_vch" id="homePhoneStr" value="#getaccount.INFO.HomePhoneStr_vch#"  mask="(999) 999-9999" />
                    </div>
                    
                    <div class="accountRow">
                        <label> Alternate number:</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="AlternatePhoneStr_vch" id="AlternatePhoneStr" value="#getaccount.INFO.AlternatePhoneStr_vch#" mask="(999) 999-9999" />
                    </div>
                    
                    <div class="accountRow">
                        <label> MFA Enabled:</label>
                    </div>
                    
                    <div class="accountRow">
                     	<select id="MFAEnabled_ti" name="MFAEnabled_ti">	
                            <option value="0">No</option>
                            <option value="1" <cfif getaccount.INFO.MFAEnabled_ti GT 0>selected="selected"</cfif>>Yes</option>                               
                        </select>
                    </div>
                           
                                                       
                    <div class="accountRow">
                        <label> MFA Phone Number:</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="MFAContactString_vch" id="MFAContactString_vch" value="#getaccount.INFO.MFAContactString_vch#" mask="(999) 999-9999" />
                    </div>
                    
                    <div class="accountRow">
                        <label> MFA Phone Extension: optional</label>
                    </div>
                    
                    <div class="accountRow">
                        <input type="text" name="MFAEXT_vch" id="MFAEXT_vch" value="#getaccount.INFO.MFAEXT_vch#" />
                    </div>
                    
                    <div class="clear row_padding_top" id="AnswerDisplayOption">
                        <div class="accountRow">
                            <label>MFA Type:</label>
                        </div>
                        <div class="left">
                            <select id="MFAContactType_ti" name="MFAContactType_ti">	
                                <option value="1" selected="selected">Phone Call</option>
                                <option value="3">SMS </option>                               
                            </select>
                        </div>
                    </div>
                    
                    <div style="clear:both;"></div>
                    <BR />
                    
                    <div class="accountRow">        
                        <input type="submit" value="Save" name="save" id="saveInfo">
                    </div>
                </form>
            </cfoutput>
            
            <div id="overlay" class="web_dialog_overlay"></div>
            <!--- Delete Company Popup --->
            <div id="dialog_changePassword" class="web_dialog" style="display: none;">
                <div class='dialog_title'><label><strong><span id='title_msg'>Change Password</span></strong></label>  <span id="closeDialog" onclick="CloseDialog('changePassword');">Close</span></div>
                <form action="" method="POST" id="dialog_changePassword">
                    <div class="dialog_content">
                        <table>
                            <tr>
                                <th align="right">
                                    Current password:
                                </th>
                                <td>
                                    <input type="password" name="currentPassword" id="currentPassword" autocomplete="off" />
                                </td>
                            </tr>
                            <tr>
                                <th align="right">
                                    New Password:
                                </th>
                                <td>
                                    <input type="text" name="inpPasswordSignup" id="inpPasswordSignup" autocomplete="off"  value="<cfoutput>#getSecurePassword#</cfoutput>" style="height:15px; width:149px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 3px 8px;"/>
                                </td>
                            </tr>
                            <tr>
                                <th align="right">
                                    Verify New Password:
                                </th>
                                <td>
                                    <input type="text" name="confirmPassword" id="confirmPassword" autocomplete="off"  value="<cfoutput>#getSecurePassword#</cfoutput>" style="height:15px; width:149px; border: 1px solid rgba(0, 0, 0, 0.3); border-radius: 3px; box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5); margin-bottom: 8px; padding: 3px 8px;" />
                                </td>
                            </tr>
                        </table>
                        <label class='error' id='company_fund_err'></label>
                    </div>
                    
                     <div class='inp_box' style="float:left; width:500px; margin-left:30px;">
                        <h2>Secure Password Requirements</h2>
                            <ul style="margin-left:25px;">
                                <li class='title'>must be at least 8 characters in length</li>
                                <li class='title'>must have at least 1 number</li>
                                <li class='title'>must have at least 1 uppercase letter</li>
                                <li class='title'>must have at least 1 lower case letter</li>
                                <li class='title'>must have at least 1 special character</li>
                            </ul>
                        <BR />
                        <h4>A secure sample of one has been randomly generated for you.
                            <BR />
                            Feel free to use or replace with your own.  
                        </h4>
                       
                    </div>
                                            
                    <div class="dialog_content">
                        <button 
                            id="close_dialog" 
                            type="button" 
                            class="somadesign1"
                            onClick="changePassword(); return false;"	
                        >Change</button>
                        <button 
                            id="close_dialog" 
                            type="button" 
                            class="somadesign1"
                            onClick="CloseDialog('changePassword');"	
                        >Close</button>
                    </div>
                </form>
            </div>

		<p class="lead"></p>
    	
        <row>
        	
            <div class="">
                        
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content ---> 

<script type="text/javascript">

	<!--- On off slider for survey mode --->
	var slideSpeed = 150;
	var leftDist = 41;
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Admin_Profile_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
		
									
					
		$("#MFAContactType_ti").val('<cfoutput>#getaccount.INFO.MFAContactType_ti#</cfoutput>');
					
		$('#saveInfo').click(function(){
								
			$.ajax({
				
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=updateAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					firstName: $('#firstName').val(),
					lastName: $('#lastName').val(),
					department: $('#department').val(),
					position: $('#position').val(),
					emailAddress: $('#emailAddress').val(),
					WorkPhoneStr: $('#WorkPhoneStr').val(),
					extensionWork: $('#extensionWork').val(),
					homePhoneStr: $('#homePhoneStr').val(),
					AlternatePhoneStr: $('#AlternatePhoneStr').val(),
					DefaultCIDVch: $('#DefaultCID_vch').val(),
					MFAContactString_vch: $('#MFAContactString_vch').val(),
					MFAContactType_ti: $('#MFAContactType_ti').val(),
					MFAEnabled_ti: $('#MFAEnabled_ti').val(),
					MFAEXT_vch: $('#MFAEXT_vch').val(),					 
				},					  
				success:function(data){
					if(data.ERROR){
						bootbox.alert(data.MESSAGE);
					}else{
						bootbox.alert(data.MESSAGE);
					}
				} 		
			});
			return false;
		});
	});
	
	function OpenDialog(dialog_id){
		<!---$('#confirmPassword').val('');--->
		$('#currentPassword').val('');
		<!---$('#inpPasswordSignup').val('');--->
		
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
      	return false;
	}
	
	function CloseDialog(dialog_id){
		currentUserID = '';
		$("#company_fund_err").hide();
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
	
	function changePassword(){
		var password = $('#inpPasswordSignup').val();
		var confirmPassword = $('#confirmPassword').val();
		var currentPassword = $('#currentPassword').val();
		
		if(!isValidPassword())
		{
			$('#SignUpButton').show();			
			$("#loadingSignUp").css('visibility', 'hidden');
			return false;
		}
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=changePassword&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				password : password,
				confirmPassword:confirmPassword,
				currentPassword:currentPassword
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(data){
				
				bootbox.alert(data.MESSAGE);
				if(data.SUCCESS == true){
					$('#currentPassword').val('');
					$('#inpPasswordSignup').val('');
					$('#confirmPassword').val('');
					CloseDialog('changePassword');
				}
			} 		
				
		});
	}
	
		
	
	function isValidPassword()
	{
		if ($("#inpPasswordSignup").val().length < 8)
		{			
			bootbox.alert("Password Validation Failure. Password must be at least 8 characters in length");
			return false;
							  
		}
		  
		var hasUpperCase = /[A-Z]/.test($("#inpPasswordSignup").val());
		if (!hasUpperCase)
		{			
			bootbox.alert("Password Validation Failure. Password must have at least 1 uppercase letter");
			return false;
							  
		}
							
		var hasLowerCase = /[a-z]/.test($("#inpPasswordSignup").val());
		if (!hasLowerCase)
		{
			
			bootbox.alert("Password Validation Failure. Password must have at least 1 lowercase letter");
			return false;
							  
		}
		
		var hasNumbers = /\d/.test($("#inpPasswordSignup").val());
		if (!hasNumbers)
		{
			
			bootbox.alert("Password Validation Failure. Password must have at least 1 number");
			return false;
							  
		}
							
		var hasNonalphas = /\W/.test($("#inpPasswordSignup").val());
		if (!hasNonalphas)
		{
			
			bootbox.alert("Password Validation Failure. Password must have at least 1 special character.");
			return false;
							  
		}
		
		return true;
												
	}
				
</script>