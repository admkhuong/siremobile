<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Buy_Credits_Title#">
</cfinvoke>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Admin_Buy_Credits_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
</script>

<cfoutput>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
</cfoutput>

<script type="text/javascript">
	
	$(document).ready(function()
	 { 	 
		 // MAKE SURE YOUR SELECTOR MATCHES SOMETHING IN YOUR HTML!!!
		 $('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });	 
		
		$('#PurchaseAmount').selectmenu({
			style:'popup',
			width: 387,
            appendTo: '#ADDContactStringToGroupContainer .right-input'
		});
	
		
	 }); 
	 
</script>


<style>

	.EBMDialog, .stand-alone-content 
	{
    	padding-bottom: 0;
	}

	.EBMDialog form input 
	{
	   height:22px;
	}

	.btn {
		border-radius: 0px 4px 4px 0px;
		border: none;
	}
	
	#PurchaseAmount, #PurchaseAmount-button
	{
		outline:none;		
		
	}
	
	
	.odd 
	{
		background-color: #F9F9F9;		
	}
	
	.BuyNow td
	{
		width:400px;
		text-align:right;
		padding:10px;
	}
	
	.BuyNow th
	{		
		text-align:right;
		padding:10px;
	}
	.servicemainline 
	{
    	border: 1px solid #0085C8;
    	float: left;
    	width: 100%;
		clear:both;
	}
	
</style>


<cfoutput>

	<div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:800px;">
        <div class="EBMDialog">
                                          
                <div class="header">
                    <div class="header-text">Buy Credits</div>
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
               
               
                                                       
                    <div style="">
                    
                    
                     	<form action="#rootUrl#/#SessionPath#/cfc/accountinfo.cfc?method=addPurchasePP&returnformat=json&queryformat=column" method="post" target="_top">
                     
                    
							<!--- List pending credits --->
                            
                            
                                  
                            <div class="messages-lbl">Number of Credits to Purchase</div>
                            <div class="message-block">
                                <div class="left-input">
                                    <span class="em-lbl">Credits</span>
                                    <div class="hide-info showToolTip">&nbsp;</div>
                                    <div class="tooltiptext">
                                        How many credits do you wish to purchase at this time? The more you buy, the more you save.
                                    </div>		
                                    <div class="info-block" style="display:none">content info block</div>
                                </div>
                                <div class="right-input">
                                  	<select id="PurchaseAmount" name="PurchaseAmount">
                                       <option value="20">$20 = 500 Credit Purchase</option>
                                       <option value="45">$45 = 1,250 Credit Purchase</option>
                                       <option value="75">$75 = 2,000 Credit Purchase</option>
                                       <option value="90">$90 = 4,600 Credit Purchase</option>
                                       <option value="100" selected="selected">$100 = 6,750 Credit Purchase</option>
                                       <option value="125">$125 = 13,000 Credit Purchase</option>
                                       <option value="175">$175 = 25,000 Credit Purchase</option>
                                       <option value="320">$320 = 50,000 Credit Purchase</option>
                                       <option value="600">$600 = 100,000 Credit Purchase</option>
                                       <option value="1375">$1375 = 250,000 Credit Purchase</option>
                                       <option value="2500">$2500 = 500,000 Credit Purchase</option>
                                       <option value="4400">$4500 = 1,000,000 Credit Purchase</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clear"></div> 
                            <br/>
                            
                            <div style="text-align:right; padding-right:20px;">
                               
                                    <input type="image" src="#rootUrl#/#publicPath#/images/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="width:171px; height:47px; border:none; box-shadow:none;">
                                    <!---<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">--->
                               
                            </div>

                                              
                       </form>                                                         
                    </div>
                                  
                
                
               <div class="servicemainline"></div>                
                
                <div style="text-align:center; padding:30px;" align="center">
                	
                    <div class="messages-lbl" style="margin-left:0; text-align:left;">The more credits you buy up front, the less you pay cost per credit!</div>
                    
                    <div align="center">
                        <table class="BuyNow" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <th class="odd">Credits</th>
                                <th class="odd">Price $USD</th>
                            </tr>
                            
                            <tr><td>500</td><td>$20</td></tr>
                            <tr><td class="odd">1,250</td><td class="odd">$45</td></tr>
                            <tr><td>2,000</td><td>$75</td></tr>
                            <tr><td class="odd">4,600</td><td class="odd">$90</td></tr>
                            <tr><td>6,750</td><td>$100</td></tr>
                            <tr><td class="odd">13,000</td><td class="odd">$125</td></tr>
                            <tr><td>25,000</td><td>$175</td></tr>
                            <tr><td class="odd">50,000</td><td class="odd">$320</td></tr>
                            <tr><td>100,000</td><td>$600</td></tr>
                            <tr><td class="odd">250,000</td><td class="odd">$1,375</td></tr>
                            <tr><td>500,000</td><td>$2,500</td></tr>
                            <tr><td class="odd">1,000,000</td><td class="odd">$4,500</td></tr>
                        </table>
                	</div>   
                </div>
               
                <div class="servicemainline"></div>
                
                <div align="center" style="background-color: ##F8F8F8; padding:30px;">
                
                	<div class="messages-lbl" style="margin-left:0; text-align:left;">How Credits are used</div>
                    
                		<div align="center">
                        
                        
                             <table class="BuyNow" cellpadding="0" cellspacing="0" border="0">
                                
                                
                                <tr><th>eMail</th><td nowrap="nowrap">1 Credit per email sent</td></tr>
                                <tr><th class="odd">SMS</th><td class="odd" nowrap="nowrap">2 Credits for up to 160 characters</td></tr>
                                <tr><th>Voice</th><td nowrap="nowrap">3 Credits for up to 1 minute messages</td></tr>
                               
                            </table>
                        
                        
                        </div>
                
                
                </div>
               
                                          
                
        </div>
		 
	</div>    
</cfoutput>
