<cfif isDefined("form.submit")>
		<CFINVOKE METHOD="ADDPURCHASE" COMPONENT="#SESSION.SESSIONCFCPATH#.ACCOUNTINFO" RETURNVARIABLE="result">
			<CFINVOKEARGUMENT NAME="BILLINGADDRESS" VALUE="#FORM.BILLINGADDRESS#">
			<CFINVOKEARGUMENT NAME="CCNUMBER" VALUE="#FORM.CCNUMBER#">
			<CFINVOKEARGUMENT NAME="CCVNUMBER" VALUE="#FORM.CCVNUMBER#">
			<CFINVOKEARGUMENT NAME="EXPMONTH" VALUE="#FORM.EXPMONTH#"/>
			<CFINVOKEARGUMENT NAME="EXPYEAR" VALUE="#FORM.EXPYEAR#"/>
			<CFINVOKEARGUMENT NAME="NAMEONCARD" VALUE="#FORM.NAMEONCARD#">
			<CFINVOKEARGUMENT NAME="ZIPPAYMENT" VALUE="#FORM.ZIPPAYMENT#">
			<CFINVOKEARGUMENT NAME="AMOUNT" VALUE="#FORM.AMOUNT#">																
		</CFINVOKE>
		<!--- <cfif result.ID EQ -1>
			<script>
				alert("Purchase saved error. Details:" + '<cfoutput>#result.MSG#</cfoutput>')
			</script>
		<cfelse>
			<script>
				alert("Purchase saved.")
			</script>
		</cfif> --->
	</cfif>
<cfinvoke component="#Session.SessionCFCPath#.accountinfo" method="getBilling" returnvariable="getbinfo" />

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Buy_Credits_Title#">
</cfinvoke>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Admin_Buy_Credits_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
</script>

<div>
    <b>*Current Available Credits For Sending: ( <cfoutput>#NumberFormat(getbinfo.Balance_int,"999,999,999")#</cfoutput> )</b>
</div>
<div>
	
    <div id="step3">
        <div>
        <cfform name="payment" id="payment" method="post">
        <h2>Add To Credit Balance:</h2>
        
        <label>
            Name as appears on Credit Card<br />
        <div>
            <cfinput type="text" name="nameOnCard" id="nameOnCard" required="yes" message="You must include the name exactly as it appears on your credit card" />
        </div>
        </label>
        
        <div>
        	<span id="nameoncardMsg"></span>
        </div>
        
        <div></div>
        
        <label>
            Billing Address<br />
        <div>
            <cfinput type="text" name="billingAddress" id="billingAddress" required="yes" message="You must include the billing address for your credit card" />
        </div>
        </label>
        
        <div>
        	<span id="billingAddressMsg"></span>
        </div>
        
        <div></div>
        
        <label>
            Zip<br />
        <div>
            <cfinput type="text" name="zipPayment" id="zipPayment" required="yes" message="You must include the Zipcode for your credit card" />
        </div>
        </label>
        
        <div>
        	<span id="zipPaymentMsg"></span>
        </div>
        
        <div></div>
        
        <div>
            <label>
                Credit Card Number<br />
            <div class="registrationValueCC">
                <cfinput type="text" name="ccNumber" id="ccNumber" mask="9999999999999999" required="yes" message="You must include the credit card number" />
            </div>
            </label>
        </div>
        
        <div style="float:left;">
            <label style="text-align:left; margin-left:15px;">
                CCV<br />
            <div class="registrationValueCCV">
                <cfinput type="text" name="ccvNumber" id="ccvNumber" mask="9999" required="yes" message="You must include the Card Verification Code on the back of your credit card" />
            </div>
            </label>
        </div>
        <br>
        <div>
        	<span id="ccNumberMsg"></span>
        </div>
        <div></div>
        
        
        <cfoutput>
        <div>
            <label>
                Expiration Month<br />
            <div>
                <select name="expMonth" id="expMonth">
                    <option id="0expMonth" value="0">Month</option>
                    <cfloop from="1" to="12" index="i">
                        <option id="#i#expMonth" value="#numberformat(i,'00')#">#numberformat(i,'00')#</option>
                    </cfloop>
                </select>
            </div>
            </label>
        </div>
        <div>
            <label>
            Expiration Year<br />
            <div>
                <select name="expYear" id="expYear">
                    <option id="0expYear" value="0">Year</option>
                    <cfloop from="#year(now())#" to="#year(now())+5#" index="i">
                        <option id="#i#expYear" value="#i#">#i#</option>
                    </cfloop>
                </select>
            </div>
            </label>
        </div>
        <div>
        	<span id="expYearMsg"></span>
        </div>
        </cfoutput>
        <div></div>
        
        <div>
            <div>PURCHASE AMOUNT</div>       	
        </div>
        
        <div>
            <div>
                <div>
                    * Credits
                </div>
            </div>
            <div>
                <div>
                    625
                </div>
            </div>
            <div>
                <div>
                    1,250
                </div>
            </div>
            <div>
                <div>
                    2,000
                </div>
            </div>
            <div>
                <div>
                    4,600
                </div>
            </div>
            <div>
                <div>
                    6,750
                </div>
            </div>
            <div>
                <div>
                    13,000
                </div>
            </div>
            <div>
                <div>
                    25,000
                </div>
            </div>
            <div>
                <div>
                    50,000
                </div>
            </div>
            <div>
                <div>
                    100,000
                </div>
            </div>
        </div>
        <div>
            <div>
                <div>
                    Price<!---<br />
                    <span style="font-size:.7em;">Per Month</span>--->
                </div>
            </div>
            <div>
                <div>
                    $25
                </div>
            </div>
            <div>
                <div>
                    $50
                </div>
            </div>
            <div>
                <div>
                    $80
                </div>
            </div>
            <div>
                <div>
                    $175
                </div>
            </div>
            <div>
                <div>
                    $250
                </div>
            </div>
            <div>
                <div>
                    $450
                </div>
            </div>
            <div>
                <div>
                    $750
                </div>
            </div>
            <div>
                <div>
                    $1,250
                </div>
            </div>
            <div>
                <div>
                    $1,900
                </div>
            </div>
        </div>
        <div>
            <div>
                <div>
                    Purchase
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="25" checked>
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="50">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="80">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="175">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="250">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="450">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="750">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="1250">
                </div>
            </div>
            <div>
                <div>
                    <input type="radio" name="amount" value="1900">
                </div>
            </div>
        </div>

            <cfinput type="submit" name="submit" id="submit" value="Place Order"/> <!--- onclick="return checkVar();" --->

        </cfform>
        </div>
        <div>
        	* Each message sent is charged at 1 credit per 30 seconds of sent script recording.
        </div>
    </div>
    
</div>
