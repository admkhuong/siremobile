<cfparam name="BatchId_int" default="0">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Reports_Views_Title#">
</cfinvoke>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Admin_Buy_Credits_Title# >> #Admin_Reports_Views_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
</script>

<h4>Call result by: <cfoutput>#BatchId_int#</cfoutput></h4>
<cfsilent>
	<cfimport taglib="../lib/grid" prefix="mb" />
</cfsilent>
<cfoutput>
	<!--- Prepare column data---->
	<cfset arrNames = ['Id','BatchId_bi','TotalCallTime','MessageDelivered', 'CallStartTime','CallEndTime']>	
	<cfset arrColModel = [			
			{name='MasterRXCallDetailId_int', width='15%'},
			{name='BatchId_bi', width='15%'},	
			{name='TotalCallTime_int', width='15%'},
			{name='MessageDelivered_si', width='15%'},
			{name='CallStartTime', width='20%'},
			{name='CallEndTime', width='20%'}] >		
	<mb:table 
		component="#SessionPath#.cfc.billing" 
		method="getCallResult" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		width = '100%'		
	>
	</mb:table>
</cfoutput>