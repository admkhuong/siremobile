<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
    
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/autonumeric-1.7.js"></script>
        
</cfoutput>

<cfif Session.USERROLE neq 'SuperUser'>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<style type="text/css">
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListCompany_info {
		width: 100%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		   
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 3px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		
		table.dataTable thead tr th{
			border-bottom: 2px solid #FA7D29;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
						
		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}		

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		table { border-spacing: 0; }
		
		.table 
		{
		    margin-bottom: 0;
    		max-width: 100%;
    		width: 100%;
		}
		
		.paging_full_numbers {
			float: right;
			height: 22px;
			line-height: 22px;
			margin-top: 10px;
			position: relative;
		}
		
</style>



<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    <h2 class="no-margin-top">Company Account Management</h2>


		<!---this is custom filter for datatable--->
        <div id="filter">
            <cfoutput>
                <!---set up column --->
                <cfset datatable_ColumnModel = arrayNew(1)>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Company ID', CF_SQL_TYPE = 'CF_SQL_BIGINT', TYPE='INTEGER', SQL_FIELD_NAME = 'c.CompanyAccountId_int'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Company Name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'c.CompanyName_vch'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total Standard Users', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'IF(ISNULL(TotalUser), 0, TotalUser)'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total Company Administrators', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'TotalUserAdmin'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total Balance', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'c.Balance_fl'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Total Campaigns', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'TotalCampaign'})>
                <!---we must define javascript function name to be called from filter later--->
                <cfset datatable_jsCallback = "InitListUser">
                <cfinclude template="/session/ems/datatable_filter.cfm" >
            </cfoutput>
        </div>
        <div class="border_top_none">
            <table id="tblListCompany" class="table" cellspacing=0>
            </table>
        </div>
        

		<p class="lead"></p>

    	
        <row>
        	
            <div class="" style="margin-bottom:25px;">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->


<div id="overlay" class="web_dialog_overlay"></div>

<!--- Change company rate Popup --->
<div id="dialog_rates" title="Set Cost per Transaction" style="display: none;">
	<cfform action="" method="POST" id="dialog_rates_form">
		<div class="dialog_content">
			Enter the rate <b><span id="companyName"></span></b> will be paying per transaction.
			<cfinput name="companyId" id="cid" type="hidden">
		</div>
		<div class="dialog_content">
			<label><strong>Rate type</strong></label>
			<select name="rateType" id="rateType">
				<option value="1">Per minute</option>
				<option value="2">Per message</option>
				<option value="4">Per transaction</option>
			</select>
		</div>
		<div class="dialog_content">
			<label><strong>Rate 1</strong></label>
			<cfinput name="rate1" id="rate1">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 2</strong></label>
			<cfinput name="rate2" id="rate2">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 3</strong></label>
			<cfinput name="rate3" id="rate3">
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateRates(); return false;"	
			>Update</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('rates');"	
			>Cancel</button>
		</div>
	</cfform>
</div>

<!--- Change User Balance Popup --->
<div id="dialog_addFunds" title="Update Company Funds" style="display: none;">
	<cfform action="" method="POST" id="dialog_addFunds">
		<div class="dialog_content">
			<label><strong>Total Amount</strong></label>
			<span class="dolaFund">$</span>
			<cfinput type="text" name="company_fund" validate="integer" id="company_fund"/> <br />
		</div>
		<div class="dialog_content">
			<cfinput type="checkbox" name="unlimited" id="unlimited">
			<label for="unlimited">Unlimited Amount</label>
			<cfinput type="hidden" name="company_id" validate="integer" id="company_id"/>
			<label class='error' id='company_fund_err'></label>
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="addFunds(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('addFunds');"	
			>Close</button>
		</div>
	</cfform>
</div>

<!--- Delete Company Popup --->
<div id="dialog_password" title="Input Password" style="display: none;">
	<div class="dialog_content">
		<p>Please enter your password to continue </p>
		<input type="password" name="user_password" id="user_password"/> <br />
		<input type="hidden" name="company_id" id="company_id"/>
		<label class='error' id='company_fund_err'></label>
	</div>
	<div class="dialog_content">
		<button 
			id="close_dialog" 
			type="button" 
			class="somadesign1"
			onClick="confirmPassword(); return false;"	
		>Delete</button>
		<button 
			id="close_dialog" 
			type="button" 
			class="somadesign1"
			onClick="CloseDialog('password');"	
		>Close</button>
	</div>
</div>

<!--- Change CID Popup --->
<div id="dialog_cid" style="display: none;" title="Edit Caller ID">
	<cfform action="" method="POST" id="dialog_cid">
		<cfinput type="hidden" id="CallerCompanyId" name="CallerCompanyId" >
		<div class="dialog_content">
			<label><strong>Caller ID</strong></label>
			<cfinput type="text" name="callerId" id="callerId" />
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateCid(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('cid');"	
			>Close</button>
		</div>
	</cfform>
</div>

<script>
	var _tblListCompany;
	//init datatable for active agent
	function InitListUser(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListCompany = $('#tblListCompany').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				{"sName": 'CompanyAccountId', "sTitle": 'Company ID', "sWidth": '15%',"bSortable": true},
				{"sName": 'CompanyName', "sTitle": 'Company Name', "sWidth": '15%',"bSortable": true},
				{"sName": 'TotalNormalUser', "sTitle": 'Total Standard Users', "sWidth": '13%',"bSortable": true},
				{"sName": 'TotalUserAdmin', "sTitle": 'Total Company Administrators', "sWidth": '10%',"bSortable": true},
				{"sName": 'Balance', "sTitle": 'Total Balance', "sWidth": '10%',"bSortable": true},
				{"sName": 'TotalCampaign', "sTitle": 'Total Campaigns', "sWidth": '10%',"bSortable": true},
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '25%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetCompanyListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){				
				
			}
	    });
	}
	InitListUser();
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Admin_Companies_Management_Title# </cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
		
		$('#unlimited').click(function(){
			if($('#unlimited').is(':checked')){
				$('#company_fund').attr('readonly',true);
				$('#company_fund').css('opacity','0.5');
			}else{
				$('#company_fund').removeAttr('readonly');
				$('#company_fund').css('opacity',1);
			}
		});
		
		$('#company_fund').autoNumeric();
	});
	
	function changeCID(userId, cid){
		$('#callerId').val(cid);
		$('#CallerCompanyId').val(userId);
		OpenDialog('cid');
	}
	
	function updateCid(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=updateCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				companyId : $('#CallerCompanyId').val(), 
				callerId : $('#callerId').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.DATA.RXRESULTCODE[0] > 0){
					bootbox.alert( "Set Caller Id successfully", function(result) { 
						$('#tblListCompany').DataTable().ajax.reload();
						return;						
					});
				}else{
					bootbox.alert(d.DATA.MESSAGE[0]);
				}
			} 		
		});
	}
	
	function changeRate(companyId,rateType,rate1,rate2,rate3,name){
		$('#cid').val(companyId);
		$('#rateType').val(rateType);
		$('#rate1').autoNumericSet(rate1);
		$('#rate2').autoNumericSet(rate2);
		$('#rate3').autoNumericSet(rate3);
		$('#companyName').text(name);
		OpenDialog('rates');
	}
	
	function updateRates(){
		$.ajax({
           type: "POST",
           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=updateRate&&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: $('#dialog_rates_form').serialize(),
           success: function(d){
           	if (d.ROWCOUNT > 0) 
			{													
			
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{           				
          				bootbox.alert(d.DATA.MESSAGE[0]);
           				$('#tblListCompany').DataTable().ajax.reload();
           				CloseDialog('rates');	
           				
           			}else{
						bootbox.alert(d.DATA.MESSAGE[0]);		
					}
           		}
           	}
           	$('#accepCondition').removeAttr('checked');
           }
  		});
	}
	
	function activeSuspend(companyId,status){
		
		bootbox.confirm( 'Do you want to '+ (status ==0 ? 'suspend':'activate ') + ' this company?', function(result) { 
			if(result)
			{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=activeSuspendCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						CompanyAccountId : companyId,
						activeSuspend: status
					},					  
					success:function(d){
						if(d.DATA.RXRESULTCODE[0] > 0 ){
							bootbox.alert(d.DATA.MESSAGE[0] + " " + d.DATA.ERRMESSAGE[0], function(result) 
							{ 
								$('#tblListCompany').DataTable().ajax.reload();
							});
						}
						else
						{
							bootbox.alert(d.DATA.MESSAGE[0] + " " + d.DATA.ERRMESSAGE[0]);
						}
					} 		
				});
			
				return;	
			}
			else
			{
				return;	
			}
		});
	}
	
	function changeAPIRequest(companyId)
	{
		var options = {
			show: true,
			"remote" : "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/administration/company/dsp_changeapirate?userID=" + companyId + "&inpType=1"
		}

		$('#UserAPIModal').modal(options);	
		
	}
	
	function OpenDialog(dialog_id){
      	$("#dialog_" + dialog_id).dialog({
			width: 380,
			open: function( event, ui ) {
				$("#overlay").show();
			},
			close: function(event, ui){
				currentUserID = '';
				$('#user_password').val('');
				$("#company_fund_err").hide();
				$("#overlay").hide();
			}
		});
	}
	
	function CloseDialog(dialog_id){
		currentUserID = '';
		$('#user_password').val('');
		$("#company_fund_err").hide();
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).dialog("close");
	}
	
	function openAddFundsDialog(companyId,balance,unlimitedBalance)
	{
		
		bootbox.alert('Warning! - Add Funds to a Company Admin in the user account management section');
		
		<!---OpenDialog('addFunds');
		if(balance.indexOf('$') != -1){
			balance = balance.substring(balance.indexOf('$') + 1, balance.length);
		}
		
		if(unlimitedBalance == 1){
			$('#unlimited').attr('checked',true);
			$('#company_fund').attr('readonly',true);
			$('#company_fund').css('opacity','0.5');
		}else{
			$('#unlimited').attr('checked',false);
			$('#company_fund').removeAttr('readonly');
			$('#company_fund').css('opacity',1);
		}
		$('#company_id').val(companyId);
		$('#company_fund').val(balance);--->
	}	
	
	function addFunds(){
		
		bootbox.alert('Warning! - Add Funds to a Company Admin in the user account management section');
	
	}
	
	function deleteCompany(companyId,obj){
		OpenDialog('password');
		$('#company_id').val(companyId);
		$('#company_name').html($($(obj).parent().parent().children()[1]).html());
	}
	
	function confirmPassword(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpUserID : '<cfoutput>#session.username#</cfoutput>',
				inpPassword : $('#user_password').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.RXRESULTCODE) != "undefined")
						{					
							CurrRXResultCode = d.RXRESULTCODE;	
							if(CurrRXResultCode > 0)
							{
								$.ajax({
									type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
									url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=deleteCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
									dataType: 'json',
									data:  { 
										companyId : $('#company_id').val()
									},
									success: function(data){
										if(data.DATA.RXRESULTCODE[0] > 0 ){
											CloseDialog('password');
											bootbox.alert(data.DATA.MESSAGE[0] + " - Delete success", function(result) { 
												location.href = window.location.pathname;
											});
										}else{
											bootbox.alert(data.DATA.MESSAGE[0] + " - Delete failed");
										}
									}
									
								});
								
							}
							
							else
							{
								<!--- Unsuccessful Login --->
								<!--- Check if variable is part of JSON result string   d.CCDXMLString  --->								
								if(typeof(d.REASONMSG) != "undefined")
								{	 
									bootbox.alert("Error while trying to login.\n" + d.REASONMSG, function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
								}		
								else
								{
									bootbox.alert("Error while trying to login.\n" + "Invalid response from server", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
								}
								
							}
						}
						else
						{<!--- Invalid structure returned --->	
							// Do something		
							
							bootbox.alert("Error while trying to login.\n" + "Invalid response from server" , function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
				
						}
					
					
					
				} 		
				
			});
	}
</script> 



<!-- Modal -->
<div class="modal fade" id="UserAPIModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Modal title</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

