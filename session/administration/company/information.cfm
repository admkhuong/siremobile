<cfif Session.USERROLE neq 'CompanyAdmin'>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Companies_Information_Title#">
</cfinvoke>

<cfif isdefined('session.messageUpdate') AND session.messageUpdate NEQ '' >
	<dif style="color:red"><cfoutput>#session.messageUpdate#</cfoutput></div>
	<cfset session.messageUpdate = ''>
</cfif>

<cfparam name="stateCode" default="">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="getCompanyById" returnvariable="getCompanyById">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="getAllStates" returnvariable="getAllStates">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="getStateCodeByStateName" returnvariable="getStateCodeByStateName">
	<cfinvokeargument name="StateName" value="#getCompanyById.State#">
</cfinvoke>

<cfif stateCode EQ "">
	<cfif getStateCodeByStateName EQ "">
		<cfset stateCode = 'AK'>
	<cfelse>
		<cfset stateCode = getStateCodeByStateName>
	</cfif>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="getCitesByStateCode" returnvariable="getCitesByStateCode">
	<cfinvokeargument name="stateCode" value="#stateCode#">
</cfinvoke>

<cfform method="post"  >
	
	<div class="accountRow">
		<label>Company Name</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="CompanyName" value="#getCompanyById.CompanyName#" required="true" message="Error in Company Name" validateat="onServer, OnSubmit"><br/>
	</div>
	
	<div class="accountRow">
		<label>Street 1</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="Address1" value="#getCompanyById.Address1#" >
	</div>
	
	<div class="accountRow">
		<label>Street 2</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="Address2" value="#getCompanyById.Address2#" >
	</div>
	
	<div class="accountRow">
		<label>State</label>
	</div>
	
	<div class="accountRow">
		<cfselect name="State" id="StateSelect" value="stateCode" display="state" selected="#stateCode#" query="getAllStates" ></cfselect><br/>
	</div>
	
	<div class="accountRow">
		<label>City</label>
	</div>
	
	<div class="accountRow">
		<cfselect name="City" id="citySelect" value="City" selected="#getCompanyById.City#" query="getCitesByStateCode" ></cfselect><br/>
	</div>
	
	<div class="accountRow">
		<label>Zip Code</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="PostalCode" value="#getCompanyById.PostalCode#"  validate="zipcode" validateat="onServer,OnSubmit"><br/>
	</div>
	
	<div class="accountRow">
		<label>Phone Number</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="PrimaryPhoneStr" value="#getCompanyById.PrimaryPhoneStr#" mask="(999)999-9999"><br/>
	</div>
	
	<div class="accountRow">
		<label>Fax Number</label>
	</div>
	
	<div class="accountRow">
		<cfinput id="FaxPhone" name="FaxPhone" value="#getCompanyById.FaxPhoneStr#"  mask="(999)999-9999">
	</div>
	
	<div class="accountRow">
		<label>Point of Contact Name</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="ContactNamePoint" value="#getCompanyById.ContactNamePoint#">
	</div>
	
	<div class="accountRow">
		<label>Point of Contact Number</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="ContactNumberPoint" value="#getCompanyById.ContactNumberPoint#">
	</div>
	
	<div class="accountRow">
		<label>Point of Contact Email Address</label>
	</div>
	
	<div class="accountRow">
		<cfinput name="ContactEmailPoint" value="#getCompanyById.ContactEmailPoint#">
	</div>
	
	<div class="accountRow">
		<cfinput type="submit" name="updateCompany" value="save">
	</div>
	
</cfform>

<script type="text/javascript">
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Admin_Companies_Management_Title# >> #Admin_Companies_Information_Text#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
	
		$('#StateSelect').change(function(){
			$.ajax({
				url:'<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/administrator/company.cfc?method=citiesDropDownDependStateCode&stateCode='+$('#StateSelect').val(),
				success: function(data){
					$('#citySelect').html(data);
				}
			});
		});
	});
	
</script>