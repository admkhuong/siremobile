<cfif Session.USERROLE neq 'SuperUser'>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Companies_Management_Title#">
</cfinvoke>

<cfparam name="page" default="1">
<cfimport taglib="../../lib/grid" prefix="mb" />

<cfscript>
	htmlOption = "<a href='#rootUrl#/#SessionPath#/Administration/company/showLogs?COMPANYID={%CompanyAccountId%}'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs'/></a>";
	htmlOption = htmlOption & "<a href='##'onClick='openAddFundsDialog({%CompanyAccountId%},""{%BalanceNumber%}"",{%UnlimitedBalance%}); return false;'><img class='ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Total Amount'/></a>";
	htmlOption = htmlOption & "<a href='##'onClick='changeRate({%CompanyAccountId%},""{%rateType%}"",""{%rate1%}"",""{%rate2%}"",""{%rate3%}"",""{%CompanyName%}""); return false;'><img class='ListIconLinks img16_16 changeRates_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Rates'/></a>";
	htmlOption = htmlOption & "<a href='##'onClick='changeAPIRequest({%CompanyAccountId%}); return false;'><img class='ListIconLinks img16_16 changeApiRequest_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change API Requests'/></a>";
	htmlOption = htmlOption & "<a href='##'onClick='activeSuspend({%CompanyAccountId%},0); return false;'><img class='ListIconLinks img16_16 active_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Suspend Company'/></a>";
	htmlOption = htmlOption & "<a href='#rootUrl#/#sessionPath#/Administration/permission/permission?UCID={%CompanyAccountId%}&roleType=1'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>";
	htmlOption = htmlOption & "<a href='##' onClick='changeCID({%CompanyAccountId%}, ""{%DefaultCID%}"" ); return false;'><img class='ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Change Caller ID'/></a>";
	htmlOption = htmlOption & "<a href='##'onClick='deleteCompany({%CompanyAccountId%},this); return false;'><img class='ListIconLinks img16_16 delete_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Delete Company'/></a>";
	
	htmlOption2 = "<a href='#rootUrl#/#SessionPath#/Administration/company/showLogs?COMPANYID={%CompanyAccountId%}'><img class='ListIconLinks img16_16 showlog_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif' title='Show Logs'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##'onClick='openAddFundsDialog({%CompanyAccountId%},""{%BalanceNumber%}"",{%UnlimitedBalance%}); return false;'><img class='ListIconLinks img16_16 changeBalance_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Total Amount'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##'onClick='changeRate({%CompanyAccountId%},""{%rateType%}"",""{%rate1%}"",""{%rate2%}"",""{%rate3%}"",""{%CompanyName%}""); return false;'><img class='ListIconLinks img16_16 changeRates_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Rates'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##'onClick='changeAPIRequest({%CompanyAccountId%}); return false;'><img class='ListIconLinks img16_16 changeApiRequest_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change API Requests'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##'onClick='activeSuspend({%CompanyAccountId%},1); return false;'><img class='ListIconLinks img16_16 block_user_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Active Company'/></a>";
	htmlOption2 = htmlOption2 & "<a href='#rootUrl#/#sessionPath#/Administration/permission/permission?UCID={%CompanyAccountId%}&roleType=1'><img class='ListIconLinks img16_16 permission_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Permission'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##' onClick='changeCID({%CompanyAccountId%}, ""{%DefaultCID%}"" ); return false;'><img class='ListIconLinks img16_16 changeRole_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Change Caller ID'/></a>";
	htmlOption2 = htmlOption2 & "<a href='##'onClick='deleteCompany({%CompanyAccountId%},this); return false;'><img class='ListIconLinks img16_16 delete_16_16' src='#rootUrl#/#PublicPath#/images/dock/blank.gif'title='Delete Company'/></a>";
</cfscript>
<cfset htmlFormat = {
		name ='suspend',
		html = '#htmlOption#'
	}>
<cfset htmlFormat2 = {
		name ='active',
		html = '#htmlOption2#'
	}>
<cfset colNames = ['Company ID', 'Company Name', 'Total Standard Users', 'Total Company Administrators', 'Total Balance', 'Total Campaigns', 'Options']>
<cfset colModels = [			
		{name='CompanyAccountId', width=40, sortObject= {isDefault='false', sortType="ASC", sortColName ='CompanyAccountId'}},
		{name='CompanyName', width=150, sortObject= {isDefault='false', sortType="ASC", sortColName ='CompanyName'}},	
		{name='TotalNormalUser',width=150, sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalUser'}},
		{name='TotalUserAdmin', width=150, sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalUserAdmin'}},
		{name='Balance', width=150, sortObject= {isDefault='false', sortType="ASC", sortColName ='UnlimitedBalance'}},	
		{name='TotalCampaign',width=100, sortObject= {isDefault='false', sortType="ASC", sortColName ='TotalCampaign'}},
		{name='Options', width=250, format=[htmlFormat, htmlFormat2]}
	   ]>
<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Company ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='Company Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Total Standard Users', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='4', DISPLAY='Total Company Administrators', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='5', DISPLAY='Total Balance', TYPE='CF_SQL_FLOAT', FILTERTYPE="INTEGER", EXCEPTIONCASE="Unlimited"},
			{VALUE='6', DISPLAY='Total Campaigns', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"}
		]
	>
<cfset 
	filterFields = [
		'c.CompanyAccountId_int',
		'c.CompanyName_vch',
		'TotalUser',
		'TotalUserAdmin',
		'c.Balance_fl',
		'TotalCampaign'
	]	
	>
<cfoutput>
	<mb:table 
		component="#LocalSessionDotPath#.cfc.administrator.company"
		method="getAllCompanies"
		colNames="#colNames#"
		colModels="#colModels#"
		page="#page#"
		width="100%"
		name="Companies"
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	  	FilterSetId="DefaulFSListCompany"
		>
	</mb:table>
</cfoutput>

<div id="overlay" class="web_dialog_overlay"></div>

<!--- Change company rate Popup --->
<div id="dialog_rates" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Set Cost per Transaction</span></strong></label><span id="closeDialog" onclick="CloseDialog('rates');">Close</span></div>
	<cfform action="" method="POST" id="dialog_rates_form">
		<div class="dialog_content">
			Enter the rate <b><span id="companyName"></span></b> will be paying per transaction.
			<cfinput name="companyId" id="cid" type="hidden">
		</div>
		<div class="dialog_content">
			<label><strong>Rate type</strong></label>
			<select name="rateType" id="rateType">
				<option value="1">Per minute</option>
				<option value="2">Per message</option>
				<option value="4">Per transaction</option>
			</select>
		</div>
		<div class="dialog_content">
			<label><strong>Rate 1</strong></label>
			<cfinput name="rate1" id="rate1">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 2</strong></label>
			<cfinput name="rate2" id="rate2">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 3</strong></label>
			<cfinput name="rate3" id="rate3">
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateRates(); return false;"	
			>Update</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('rates');"	
			>Cancel</button>
		</div>
	</cfform>
</div>

<!--- Change User Balance Popup --->
<div id="dialog_addFunds" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Update Company Funds</span></strong></label><span id="closeDialog" onclick="CloseDialog('addFunds');">Close</span></div>
	<cfform action="" method="POST" id="dialog_addFunds">
		<div class="dialog_content">
			<label><strong>Total Amount</strong></label>
			<span class="dolaFund">$</span>
			<cfinput type="text" name="company_fund" validate="integer" id="company_fund"/> <br />
		</div>
		<div class="dialog_content">
			<cfinput type="checkbox" name="unlimited" id="unlimited">
			<label for="unlimited">Unlimited Amount</label>
			<cfinput type="hidden" name="company_id" validate="integer" id="company_id"/>
			<label class='error' id='company_fund_err'></label>
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="addFunds(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('addFunds');"	
			>Close</button>
		</div>
	</cfform>
</div>

<!--- Delete Company Popup --->
<div id="dialog_password" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Input Password</span></strong></label><span id="closeDialog" onclick="CloseDialog('password');">Close</span></div>
	<div class="dialog_content">
		<p>Please enter your password to continue </p>
		<input type="password" name="user_password" id="user_password"/> <br />
		<input type="hidden" name="company_id" id="company_id"/>
		<label class='error' id='company_fund_err'></label>
	</div>
	<div class="dialog_content">
		<button 
			id="close_dialog" 
			type="button" 
			class="somadesign1"
			onClick="confirmPassword(); return false;"	
		>Delete</button>
		<button 
			id="close_dialog" 
			type="button" 
			class="somadesign1"
			onClick="CloseDialog('password');"	
		>Close</button>
	</div>
</div>

<!--- Change CID Popup --->
<div id="dialog_cid" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Edit Caller ID</span></strong></label><span id="closeDialog" onclick="CloseDialog('cid');">Close</span></div>
	<cfform action="" method="POST" id="dialog_cid">
		<cfinput type="hidden" id="CallerCompanyId" name="CallerCompanyId" >
		<div class="dialog_content">
			<label><strong>Caller ID</strong></label>
			<cfinput type="text" name="callerId" id="callerId" />
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateCid(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('cid');"	
			>Close</button>
		</div>
	</cfform>
</div>

<script type="text/javascript" language="javascript">
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Admin_Companies_Management_Title# </cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
		
		$('#unlimited').click(function(){
			if($('#unlimited').is(':checked')){
				$('#company_fund').attr('readonly',true);
				$('#company_fund').css('opacity','0.5');
			}else{
				$('#company_fund').removeAttr('readonly');
				$('#company_fund').css('opacity',1);
			}
		});
		
		$('#company_fund').autoNumeric();
	});
	
	function changeCID(userId, cid){
		$('#callerId').val(cid);
		$('#CallerCompanyId').val(userId);
		OpenDialog('cid');
	}
	
	function updateCid(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=updateCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				companyId : $('#CallerCompanyId').val(), 
				callerId : $('#callerId').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.DATA.RXRESULTCODE[0] > 0){
					jConfirm( "Set Caller Id successfully", " Success!", function(result) { 
						location.reload();						
					});
				}else{
					jAlert(d.DATA.MESSAGE[0], 'Warning');
				}
			} 		
		});
	}
	
	function changeRate(companyId,rateType,rate1,rate2,rate3,name){
		$('#cid').val(companyId);
		$('#rateType').val(rateType);
		$('#rate1').autoNumericSet(rate1);
		$('#rate2').autoNumericSet(rate2);
		$('#rate3').autoNumericSet(rate3);
		$('#companyName').text(name);
		OpenDialog('rates');
	}
	
	function updateRates(){
		$.ajax({
           type: "POST",
           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=updateRate&&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: $('#dialog_rates_form').serialize(),
           success: function(d){
           	if (d.ROWCOUNT > 0) 
			{													
			
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{
           				CloseDialog('rates');
          				jAlert(d.DATA.MESSAGE[0], "Change Company Rate",function(result){
           					if(result){
           						location.reload();
           					}
           				});
           			}else{
						jAlert(d.DATA.MESSAGE[0], "Change Company Rate");		
					}
           		}
           	}
           	$('#accepCondition').removeAttr('checked');
           }
  		});
	}
	
	function activeSuspend(companyId,status){
		
		jConfirm( 'Do you want to '+ (status ==0 ? 'suspend':'activate ') + ' this company?', (status ==0 ? 'Suspend':'Activate') + ' Company', function(result) { 
			if(result){
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=activeSuspendCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { 
						CompanyAccountId : companyId,
						activeSuspend: status
					},					  
					success:function(d){
						if(d.DATA.RXRESULTCODE[0] > 0 ){
							jAlertOK(d.DATA.MESSAGE[0] , d.DATA.ERRMESSAGE[0], function(result) { 
								location.reload();
							});
						}else{
							jAlertOK(d.DATA.MESSAGE[0] , d.DATA.ERRMESSAGE[0]);
						}
					} 		
				});
				
			}
		});
	}
	
	function changeAPIRequest(companyId){
		jPopup("<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/dsp_changeApiRequest?inpUserId=" + companyId + "&inpType=1", "API Access Throttling", function(result){
			if(result != false){
				
			}
		});
	}
	
	function OpenDialog(dialog_id){
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	
	function CloseDialog(dialog_id){
		currentUserID = '';
		$('#user_password').val('');
		$("#company_fund_err").hide();
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
	
	function openAddFundsDialog(companyId,balance,unlimitedBalance){
		OpenDialog('addFunds');
		if(balance.indexOf('$') != -1){
			balance = balance.substring(balance.indexOf('$') + 1, balance.length);
		}
		
		if(unlimitedBalance == 1){
			$('#unlimited').attr('checked',true);
			$('#company_fund').attr('readonly',true);
			$('#company_fund').css('opacity','0.5');
		}else{
			$('#unlimited').attr('checked',false);
			$('#company_fund').removeAttr('readonly');
			$('#company_fund').css('opacity',1);
		}
		$('#company_id').val(companyId);
		$('#company_fund').val(balance);
	}	
	
	function addFunds(){
		var UnlimitedBalance = $('#unlimited').is(':checked') ? 1 : 0;
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=addFundsToCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				companyId : $('#company_id').val(),
				funds : $('#company_fund').autoNumericGet(),
				UnlimitedBalance: UnlimitedBalance
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					try{
						if(d.ERRTYPE == 0){
							CloseDialog('addFunds');
							jAlertOK(d.MESSAGE,"Update Company Funds", function(result) { 
								location.reload();
							});
							return false;
						}else{
							jAlertOK(d.MESSAGE,"Update Company Funds");
						}
					}catch(exception){
						return false;
					}
				} 		
				
			});
	}
	
	function deleteCompany(companyId,obj){
		OpenDialog('password');
		$('#company_id').val(companyId);
		$('#company_name').html($($(obj).parent().parent().children()[1]).html());
	}
	
	function confirmPassword(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/authentication.cfc?method=validateUsers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpUserID : '<cfoutput>#session.username#</cfoutput>',
				inpPassword : $('#user_password').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{		
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{					
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							if(CurrRXResultCode > 0)
							{
								$.ajax({
									type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
									url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc?method=deleteCompany&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
									dataType: 'json',
									data:  { 
										companyId : $('#company_id').val()
									},
									success: function(data){
										if(data.DATA.RXRESULTCODE[0] > 0 ){
											CloseDialog('password');
											jAlertOK(data.DATA.MESSAGE[0] ,"Delete success", function(result) { 
												location.href = window.location.pathname;
											});
										}else{
											jAlertOK(data.DATA.MESSAGE[0] ,"Delete failed");
										}
									}
									
								});
								
							}
							
							else
							{
								<!--- Unsuccessful Login --->
								<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
								if(typeof(d.DATA.REASONMSG[0]) != "undefined")
								{	 
									jAlertOK("Error while trying to login.\n" + d.DATA.REASONMSG[0] , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
								}		
								else
								{
									jAlertOK("Error while trying to login.\n" + "Invalid response from server" , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
								}
								
							}
						}
						else
						{<!--- Invalid structure returned --->	
							// Do something		
							
							jAlertOK("Error while trying to login.\n" + "Invalid response from server" , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
				
						}
					}
					else
					{<!--- No result returned --->
						// Do something		
						
						jAlertOK("Error while trying to login.\n" + "No response form remote server" , "Failure!", function(result) { $("#LoginLink").show(); $("#loadingDlgLogin").hide(); return false; } );
					
					}
					
				} 		
				
			});
	}
	
</script>