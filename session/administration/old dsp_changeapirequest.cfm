<style>
	#btnUpdate, #btnCancel{
		background: -moz-linear-gradient(center top , #2484C6, #2484C6) repeat scroll 0 0 rgba(0, 0, 0, 0);
	    border: medium none !important;
	    color: #FFFFFF;
	    cursor: pointer;
	    font-size: 12px;
	    height: 25px;
	    margin-left: 10px;
	    padding: 0 5px;
	    text-transform: uppercase;
	    width: auto;
	}
</style>

<cfparam name="inpUserId" default="0">
<cfparam name="inpType" default="0">

<!--- get user API access information --->
<cfif inpType EQ 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="GetUserApiLimit" returnvariable="getUserAPILimit">
		<cfinvokeargument name="inpUserid" value="#inpUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="GetCompanyApiLimit" returnvariable="getUserAPILimit">
		<cfinvokeargument name="inpCompanyid" value="#inpUserId#">
	</cfinvoke>
</cfif>
<cfset disableButton = 'disabled="true"'>
<cfset checkButton = 'checked="checked"'>
<cfif getUserAPILimit.INPAPILIMIT NEQ "">
	<cfset disableButton = ''>
	<cfset checkButton = ''>
</cfif>

<cfoutput>
	<div style="width:250px; margin:20px">
		<div style="font-weight:bold">Please enter the number of API calls</div>
		<div style="margin:10px">
			<input type="text" name="inpNumberLimit" id="inpNumberLimit" style=" margin: 3px; padding: 3px;width:60px" value="#getUserAPILimit.INPAPILIMIT#" #disableButton# > 
		 per 
			<select name="inpTimeLimit" id="inpTimeLimit" style=" margin: 3px; padding: 3px;"  #disableButton# >
				<option value="0" #GetSelectedTime(getUserAPILimit.INPTIME, 0)# >Minute</option>
				<option value="1" #GetSelectedTime(getUserAPILimit.INPTIME, 1)# >Hour</option>
				<option value="2" #GetSelectedTime(getUserAPILimit.INPTIME, 2)# >Day</option>
				<option value="3" #GetSelectedTime(getUserAPILimit.INPTIME, 3)# >Week</option>
				<option value="4" #GetSelectedTime(getUserAPILimit.INPTIME, 4)# >Month</option>
			</select>
		
		</div>
		<div style="margin-left:20px">Eg., 100 api calls per day</div>
		<div style="margin:15px">
			<input type="checkbox" name="inpUnlimitedRequest" id="inpUnlimitedRequest" #checkButton#> Unlimited Requests
		</div>
		<div style="margin-left:45px">
			<input type="button" value="Update" id="btnUpdate">
			<input type="button" value="Cancel" id="btnCancel">
		</div>
	</div>
</cfoutput>

<script type="text/javascript">
	$(document).ready(function(){
		$('#inpUnlimitedRequest').change(function(){
			if($('#inpUnlimitedRequest').is(":checked")){
				$('#inpNumberLimit').prop('disabled',true);
				$('#inpTimeLimit').prop('disabled',true);
			}else{
				$('#inpNumberLimit').prop('disabled',false);
				$('#inpTimeLimit').prop('disabled',false);
			}
		});
		
		$('#btnUpdate').click(function(){
			<cfif inpType EQ 0>
				var data = {
					inpUserID : '<cfoutput>#inpUserId#</cfoutput>',
					inpApiLimit : $('#inpNumberLimit').val(),
					inpTime : $('#inpTimeLimit').val(),
					inpIsUnlimit : $('#inpUnlimitedRequest').is(":checked")
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc', 'updateUserApiLimit', data, "Update API access throttling.", function(d ) {
					$.alerts._hide();
					jAlert( 
						"Update API access throttling successfully", 
						'API access throttling'
					);
		    	});	 
		    <cfelse>
		    	var data = {
					inpCompanyid : '<cfoutput>#inpUserId#</cfoutput>',
					inpApiLimit : $('#inpNumberLimit').val(),
					inpTime : $('#inpTimeLimit').val(),
					inpIsUnlimit : $('#inpUnlimitedRequest').is(":checked")
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc', 'updateCompanyApiLimit', data, "Update API access throttling.", function(d ) {
					$.alerts._hide();
					jAlert( 
						"Update API access throttling successfully", 
						'API access throttling'
					);
		    	});
			</cfif>
			
		});
		
		$('#btnCancel').click(function(){
			$.alerts._hide();
		});
	});
</script>

<cffunction name="GetSelectedTime">
	<cfargument name="inpTime" required="true" default="0">
	<cfargument name="inpValue" required="true" default="0">
	<cfset selected = ''>
	
	<cfif inpTime EQ inpValue>
		<cfset selected = 'selected="true"'>
	</cfif>
	
	<cfreturn selected>
</cffunction>