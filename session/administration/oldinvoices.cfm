<cfsilent>
	<cfimport taglib="../lib/grid" prefix="mb" />
</cfsilent>

<div style="text-align:left;position:relative; overflow: hidden;">
<h4>Invoices</h4>
<cfparam name="inpUserId" default="#Session.USERID#">
<cfparam name="inpCompanyId" default="#Session.CompanyId#"/>

<cfinvoke 
    component="#Session.SessionCFCPath#.billing"
    method="invoices"
    returnvariable="getInvoices">                         
     <cfinvokeargument name="inpCompanyId" value="#inpCompanyId#"/>
     <cfinvokeargument name="inpUserId" value="#inpUserId#"/> 
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Transaction_Title#">
</cfinvoke>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Admin_Transaction_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
</script>

<ul>
	<li>Total balance: <cfoutput>#getInvoices[1]#</cfoutput></li>
	<li>Total Company Account usage: <cfoutput>#getInvoices[2]#</cfoutput></li>
</ul>

<cfoutput>
	<cfscript>
		htmlOption = "<a href='callResult?BatchId_int={%BATCHID_BI%}'><img class='view_ReportBilling ListIconLinks details_25_25' src='../../public/images/dock/blank.gif' title='Reports Viewer'></a>";
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>
	<!--- Prepare column data---->
	<cfset arrNames = ['ID', 'Description', 'Created', 'Edited', 'Actions']>	
	<cfset arrColModel = [			
			{name='BATCHID_BI', width='10%'},
			{name='DESC_VCH', width='40%'},	
			{name='Created_dt', width='20%'},
		{name='LASTUPDATED_DT', width='20%'},
		{name='FORMAT', width='10%', format = [htmlFormat]}] >		
<mb:table 
	component="#SessionPath#.cfc.billing" 
	method="GetBatchesBilling" 
	colNames= #arrNames# 
	colModels= #arrColModel#
	width = '100%'
	FilterSetId="DefaulFSListInvoices"
>
</mb:table>
</cfoutput>

