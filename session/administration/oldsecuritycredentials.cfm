<cfparam name="page" default="1">

<cfif Session.userId GT 0>

	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
		<cfinvokeargument name="pageTitle" value="#Admin_Security_Credentials_Title#">
	</cfinvoke>
	
	<cfimport taglib="../lib/grid" prefix="mb" />
	
	<cfset colNames = ['Created','Access Key ID','Secret Access Key', 'Status']>
	<cfset colModels = [			
			{name='Created', width=40},
			{name='AccessKey', width=90},	
			{name='SecretKey',width=90,isHtmlEncode=false},
			{name='Active', width=150, isHtmlEncode=false}
		   ]>
		   
	<cfoutput>
		<mb:table 
			component="#LocalSessionDotPath#.cfc.administrator.credential"
			method="getAllCredentials"
			colNames="#colNames#"
			colModels="#colModels#"
			page="#page#"
			width="100%"
			name="Credentials"
			FilterSetId="DefaulFSListSecurity"
			>
		</mb:table>
	</cfoutput>
	
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.credential" method="countCredentials" returnvariable="countCredentials">
	
	<cfif countCredentials.success EQ 1 AND countCredentials.count LT 2 >
		<a href="##" onclick="createAccessKey(); return false;">Create a new access key</a>
	</cfif>
	
	<script type="text/javascript">
		
		$('#subTitleText').text('<cfoutput>#Admin_Security_Credentials_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
		
		function createAccessKey(){
			
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';	
			
			jConfirm( 'Are you sure you would to create a new access key?', 'Create Access Key', function(result) { 
				if(result){
					$.ajax({
						url:'<cfoutput>#rootUrl#/#sessionPath#/cfc/administrator/credential.cfc?method=createAccesskey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true</cfoutput>',
						type:'post',
						dataType:'json',
						success:function(d){
							jAlert(d.MESSAGE, 'Create Access Key', function(result){
								if(d.SUCCESS == 1 &&  result ){
									location.reload();
								}
							});
							
						}
					});
				}
			});
			
		}
		
		function showSecretKey(secretKey){
			jAlert(secretKey, 'Secret Access Key');
		}
		
		function makeActiveInactiveDialog(accessKey,activeInactive){
			
			jConfirm( 
				'Are you sure you would like to make <b>'+accessKey+ '</b> ' + (activeInactive == 0 ? 'inactive?' : 'active?'), 
				'Access Key', 
				function(result) { 
					if(result){
						$.ajax({
							url:'<cfoutput>#rootUrl#/#sessionPath#/cfc/administrator/credential.cfc?method=activeInactiveAccessKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true</cfoutput>',
							type:'post',
							dataType:'json',
							data:{
								accessKey: accessKey,
								activeInactive: activeInactive
							},
							success:function(d){
								jAlert(d.MESSAGE, 'Access Key', function(result){
									if(d.SUCCESS == 1 &&  result ){
										location.reload();
									}
								});
							}
						});
					}
				});
			
		}
					
		function deleteAccessKey(accessKey){
			
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';	
			
			jConfirm( 'Are you sure delete Access key with ID '+accessKey +'? This action is permanent and cannot be undone.', 'Delete Security Credential', function(result) { 
				if(result){
					$.ajax({
						url:'<cfoutput>#rootUrl#/#sessionPath#/cfc/administrator/credential.cfc?method=deleteAccessKey&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true</cfoutput>',
						type:'post',
						dataType:'json',
						data:{
							accessKey:accessKey
						},
						success:function(data){
							if(data.SUCCESS ==1){
								location.reload();
							}else{
								jAlert(data.MESSAGE);
							}
						}
					});
				}
			});
			
		}
		
	</script>
	
<cfelse>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>