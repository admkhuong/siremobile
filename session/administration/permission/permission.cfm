<cfparam name="UCID" default="">
<cfparam name="RoleType" default="">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkPermissionPageAccess" returnvariable="checkPermissionPageAccess">
	<cfinvokeargument name="UCID" value="#UCID#">
	<cfinvokeargument name="RoleType" value="#RoleType#">
</cfinvoke>


<cfset listAllPermission = ''>
<cfset checkedAllPermistion = ''>

<cfif checkPermissionPageAccess.havePermission>
	<cfset listAllPermission = checkPermissionPageAccess.listAllPermission >
	<cfset checkedAllPermistion = checkPermissionPageAccess.checkedAllPermistion>
<cfelse>

<!---
<cfdump var="#checkPermissionPageAccess#">
<cfdump var="#UCID#">
<cfdump var="#RoleType#">
--->

	<h1>You do not have access permissions for this page.</h1>
	<cfset session.permissionError = checkPermissionPageAccess.message>
<!---	<cflocation url="#rootUrl#/#sessionPath#/account/home"><cfscript></cfscript>--->
	<cfabort/>
</cfif>

<cfif RoleType EQ 2>
	<cfset permissionSubtitle = Admin_Users_Management_Title>
<cfelse>
	<cfset permissionSubtitle = Admin_Companies_Management_Title>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_User_Permission_Title#">
</cfinvoke>


<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<!---<cfdump var="#checkedAllPermistion#">--->

<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    <h2 class="no-margin-top">Account Permission</h2>
    

<cfoutput>
	<cfform id="permissionForm">
		<ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Campaign_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Campaign_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<!--- <cfinput type="checkbox" name="#Campaign_Title#" id="Campaigns" onclick = "parentCheckboxCheck(this,'.CampaignChilds','')" value="#Campaign_Title#" checked="#Checked_checkbox#">  ---> 
                    <cfinput type="checkbox" name="#Campaign_Title#" id="Campaigns"  value="#Campaign_Title#" checked="#Checked_checkbox#" class="grouping" >  
					<label class="label_permission" for="Campaigns">#Campaign_Title#</label>
				</span>
			</cfif>
			
			<cfloop array="#Campaigns_List#" index="ListLoop" >
                				
				<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                    
                	<cfif listfindNoCase(ListLoop,edit_Campaign_Title) GT 0>
						<ul class="list_permission">
							<cfif listfindNoCase(listAllPermission,edit_Campaign_Title) GT 0>
								<span class="label_permission">
									<cfset Checked_checkbox = false>
									<cfif listfindNoCase(checkedAllPermistion,edit_Campaign_Title) GT 0>
										<cfset Checked_checkbox = true>
									</cfif>
									<!--- <cfinput type="checkbox" name="#ems_title#" id="ems" onclick = "parentCheckboxCheck(this,'.emsChilds','')" value="#Campaign_Title#" checked="#Checked_checkbox#"> --->  
				                    
				                    <cfinput type="checkbox" name="#edit_Campaign_Title#" id="ems"  value="#Campaign_Title#" checked="#Checked_checkbox#" class="grouping">
				                    
									<label class="label_permission" for="EMS">#edit_Campaign_Title#</label>
								</span>
							</cfif>
							
							<cfloop array="#MCID_List#" index="ListLoop" >
								<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
				                    <li>
				                        <cfset Checked_checkbox = false>
				                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
				                            <cfset Checked_checkbox = true>
				                        </cfif>
				                        <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="emsChilds" value="#ListLoop#" checked="#Checked_checkbox#"> ---> 
				                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="emsChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
				                        <label for="label_permission">#ListLoop#</label>
				                    </li>
				                </cfif>
				            </cfloop>
						</ul>
					<cfelse>
						<li>
						<cfset Checked_checkbox = false>
                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                            <cfset Checked_checkbox = true>
                        </cfif>
                        <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="CampaignChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="CampaignChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                        <label for="label_permission">#ListLoop#</label>
						</li>
					</cfif>
                </cfif>
            </cfloop>
			
		</ul>
		
		<ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Marketing_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Marketing_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>	
					<!--- <cfinput type="checkbox" name="#Marketing_Title#" id="Marketing" onclick = "parentCheckboxCheck(this,'.marketingChilds', '')" value="#Marketing_Title#" checked="#Checked_checkbox#">  --->
                    <cfinput type="checkbox" name="#Marketing_Title#" id="Marketing" value="#Marketing_Title#" checked="#Checked_checkbox#" class = "grouping"> 
					<label class="label_permission" for="Marketing">#Marketing_Title#</label>
				</span>
			</cfif>
			
			<ul class="list_permission">
				
				<cfif listfindNoCase(listAllPermission,Survey_Title) GT 0>
					<span class="label_permission">
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Survey_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>
						<!--- <cfinput type="checkbox" name="#Survey_Title#" id="Surveys" class="marketingChilds" onclick = "parentCheckboxCheck(this,'.surveyChilds','Marketing')" value="#Survey_Title#" checked="#Checked_checkbox#">  --->
                        <cfinput type="checkbox" name="#Survey_Title#" id="Surveys" <!--- class="marketingChilds" --->  value="#Survey_Title#" checked="#Checked_checkbox#" class = "grouping">
						<label class="label_permission" for="Surveys">#Survey_Title#</label> 
					</span>
				</cfif>
				
				<cfloop array="#Surveys_list#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="marketingChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="marketingChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="label_permission">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
				
			</ul>	
			
			<ul class="list_permission">
				
				<cfif listfindNoCase(listAllPermission,CPP_Title) GT 0>
					<span class="label_permission">
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,CPP_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>
						<!--- <cfinput type="checkbox" name="#CPP_Title#" id="CPP" class="marketingChilds" onclick = "parentCheckboxCheck(this,'.cppChilds','Marketing')" value="#CPP_Title#" checked="#Checked_checkbox#">  --->
                        
                        <cfinput type="checkbox" name="#CPP_Title#" id="CPP" <!--- class="marketingChilds" --->  value="#CPP_Title#" checked="#Checked_checkbox#" class="grouping"> 
						<label class="label_permission" for="CPP">#CPP_Title#</label> 
					</span>
				</cfif>
				
				<cfloop array="#CPP_list#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="marketingChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="marketingChilds"  --->value="#ListLoop#" checked="#Checked_checkbox#">
                            <label for="label_permission">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>    
				
			</ul>
			
			<ul class="list_permission">
				
				<cfif listfindNoCase(listAllPermission,SMS_Campaigns_Management_Title) GT 0>
					<span class="label_permission">
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,SMS_Campaigns_Management_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>
						<!--- <cfinput type="checkbox" name="#SMS_Campaigns_Management_Title#" id="sms" class="marketingChilds" onclick = "parentCheckboxCheck(this,'.smsChilds','Marketing')" value="#SMS_Campaigns_Management_Title#" checked="#Checked_checkbox#">  --->
                        
                        <cfinput type="checkbox" name="#SMS_Campaigns_Management_Title#" id="sms" <!--- class="marketingChilds" --->  value="#SMS_Campaigns_Management_Title#" checked="#Checked_checkbox#" class="grouping"> 
						<label class="label_permission" for="sms">#SMS_Campaigns_Management_Title#</label> 
					</span>
				</cfif>
				
				<cfloop array="#SMS_Campaigns_list#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="marketingChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="marketingChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
                            <label for="label_permission">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                    
				
			</ul>
		
		</ul>
		
		<ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Contact_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Contact_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>	
					<!--- <cfinput type="checkbox" name="#Contact_Title#" id="Contacts" onclick = "parentCheckboxCheck(this,'.contactChilds', '')" value="#Contact_Title#" checked="#Checked_checkbox#">  --->
                    
                    <cfinput type="checkbox" name="#Contact_Title#" id="Contacts"  value="#Contact_Title#" checked="#Checked_checkbox#" class="grouping"> 
					<label class="label_permission" for="Contacts">#Contact_Title#</label>
				</span>
			</cfif>
			
			<ul class="list_permission">
			
				<cfloop array="#Contacts_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="contactChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="contactChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="label_permission">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			
			</ul>
			
			<ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Contact_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Contact_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Contact_Group_Title#" id="ContactGroup" onclick = "parentCheckboxCheck(this,'.contactGroupChilds','Contacts')" class="contactChilds" value="#Contact_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        <cfinput type="checkbox" name="#Contact_Group_Title#" id="ContactGroup"   value="#Contact_Group_Title#" checked="#Checked_checkbox#" class="grouping"> 
						<label for="ContactGroup">#Contact_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Contact_Group_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop"  class="contactGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#"> --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="contactGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="ContactGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
			
		</ul>
		
		 <!--- <ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Reporting_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Reporting_Title#" id="Reporting" onclick = "parentCheckboxCheck(this,'.ReportingChilds','')" value="#Campaign_Title#" checked="#Checked_checkbox#">  
					<label class="label_permission" for="Reporting">#Reporting_Title#</label>
				</span>
			</cfif>
		
        <!---	
			<cfif listfindNoCase(listAllPermission,Reporting_Campaigns_Title) GT 0>
				<li>
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_Campaigns_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Reporting_Campaigns_Title#" id="ReportingCampaigns" class="ReportingChilds" value="#Reporting_Campaigns_Title#" checked="#Checked_checkbox#"> 
					<label for="ReportingCampaigns">Campaigns</label> 
				</li>
			</cfif>
			
			<cfif listfindNoCase(listAllPermission,Reporting_Surveys_Title) GT 0>
				<li>
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_Surveys_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Reporting_Surveys_Title#" id="ReportingSurveys" class="ReportingChilds" value="#Reporting_Surveys_Title#" checked="#Checked_checkbox#"> 
					<label for="ReportingSurveys">Surveys</label>
				</li>
			</cfif>
			
			<cfif listfindNoCase(listAllPermission,Reporting_SmsMessagings_Title) GT 0>
				<li>
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_SmsMessagings_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Reporting_SmsMessagings_Title#" id="ReportingSmsMessaging" class="ReportingChilds" value="#Reporting_SmsMessagings_Title#" checked="#Checked_checkbox#"> 
					<label for="ReportingSmsMessaging">#Reporting_SmsMessagings_Title#</label>
				</li>
			</cfif>
            
            <cfif listfindNoCase(listAllPermission,Reporting_Dial_Title) GT 0>
				<li>
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_Dial_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Reporting_Dial_Title#" id="ReportingDialTitle" class="ReportingChilds" value="#Reporting_Dial_Title#" checked="#Checked_checkbox#"> 
					<label for="ReportingDialTitle">#Reporting_Dial_Title#</label>
				</li>
			</cfif>--->
            
            
                        
            <cfloop array="#Reporting_List#" index="ListLoop" >
                				
				<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                    <li>
                        <cfset Checked_checkbox = false>
                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                            <cfset Checked_checkbox = true>
                        </cfif>
                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="ReportingChilds" value="#ListLoop#" checked="#Checked_checkbox#"> 
                        <label for="ReportingDialTitle">#ListLoop#</label>
                    </li>
                </cfif>
                
            </cfloop>
            
		</ul>  --->
        
        <!--- Start:Changes by Adarsh --->
         <ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Reporting_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Reporting_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>	
					 <!--- <cfinput type="checkbox" name="#Reporting_Title#" id="Reporting" onclick = "parentCheckboxCheck(this,'.ReportingChilds', '')" value="#Reporting_Title#" checked="#Checked_checkbox#">  --->
                     
                     <cfinput type="checkbox" name="#Reporting_Title#" id="Reporting"  value="#Reporting_Title#" checked="#Checked_checkbox#" class="grouping"> 
                   
					<label class="label_permission" for="Reporting">#Reporting_Title#</label>
				</span>
			</cfif>
			
			<ul class="list_permission">
			
				<cfloop array="#Reporting_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="ReportingChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="ReportingChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="label_permission">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			
			</ul>
			<!--- For Email Group --->
			<ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_Group_Title#" id="ReportGroup" onclick = "parentCheckboxCheck(this,'.reportGroupChilds','Reporting')" class="reportingChilds" value="#Reporting_Group_Title#" checked="#Checked_checkbox#"> ---> 
                        
                        <cfinput type="checkbox" name="#Reporting_Group_Title#" id="ReportGroup"  <!--- class="reportingChilds" ---> value="#Reporting_Group_Title#" checked="#Checked_checkbox#" class = "grouping"> 
                        
						<label for="ReportGroup">#Reporting_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_Group_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="reportGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="ReportGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
             <!--- For SMS Group --->
             <ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_SMS_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_SMS_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_SMS_Group_Title#" id="ReportSMSGroup" onclick = "parentCheckboxCheck(this,'.reportSMSGroupChilds','Reporting')" class="reportingSMSChilds" value="#Reporting_SMS_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        <cfinput type="checkbox" name="#Reporting_SMS_Group_Title#" id="ReportSMSGroup" onclick = "parentCheckboxCheck(this,'.reportSMSGroupChilds','Reporting')" <!--- class="reportingSMSChilds" ---> value="#Reporting_SMS_Group_Title#" checked="#Checked_checkbox#" class = "grouping"> 
                        
						<label for="ReportSMSGroup">#Reporting_SMS_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_SMSGroup_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportSMSGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#"> --->
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="reportSMSGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                            <label for="ReportSMSGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul> 
            <!--- For STATES GROUP --->
            <ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_States_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_States_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_States_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportSTATESGroupChilds','Reporting')" class="reportingSTATESChilds" value="#Reporting_States_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        <cfinput type="checkbox" name="#Reporting_States_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportSTATESGroupChilds','Reporting')" <!--- class="reportingSTATESChilds" ---> value="#Reporting_States_Group_Title#" checked="#Checked_checkbox#" class = "grouping">
                        
						<label for="ReportSTATESGroup">#Reporting_States_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_StatesGroup_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportSTATESGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#">
							 --->
                             <cfinput type="checkbox" name="#ListLoop#" id="ListLoop"<!---  class="reportSTATESGroupChilds"  --->value="#ListLoop#" checked="#Checked_checkbox#">  
                            <label for="ReportSTATESGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
            
            <!--- For CALL RESULTS Group --->
            
            <ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_CallResults_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_CallResults_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_CallResults_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" class="reportingCALLRESULTSChilds" value="#Reporting_CallResults_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        
                        <cfinput type="checkbox" name="#Reporting_CallResults_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" <!--- class="reportingCALLRESULTSChilds" ---> value="#Reporting_CallResults_Group_Title#" checked="#Checked_checkbox#" class = "grouping"> 
						<label for="ReportCALLRESULTSGroup">#Reporting_CallResults_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_CallResultsGroup_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportCALLRESULTSGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#"> ---> 
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="reportCALLRESULTSGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
                            <label for="ReportCALLRESULTSGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
            
             
        	<!--- End:Changes by Adarsh --->
        
        
            <!--- For System Reports  Group --->
            
            <ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_System_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_System_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_System_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" class="reportingCALLRESULTSChilds" value="#Reporting_System_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        
                        <cfinput type="checkbox" name="#Reporting_System_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" <!--- class="reportingCALLRESULTSChilds" ---> value="#Reporting_System_Group_Title#" checked="#Checked_checkbox#" class = "grouping"> 
						<label for="ReportCALLRESULTSGroup">#Reporting_System_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_System_Group_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportCALLRESULTSGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#"> ---> 
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="reportCALLRESULTSGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
                            <label for="ReportCALLRESULTSGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
            
            <!--- For System Reports  Group --->
            
            <ul class="list_permission">
			
				<cfif listfindNoCase(listAllPermission,Reporting_Dashboard_Template_Group_Title) GT 0>
					<span class="label_permission">	
						<cfset Checked_checkbox = false>
						<cfif listfindNoCase(checkedAllPermistion,Reporting_Dashboard_Template_Group_Title) GT 0>
							<cfset Checked_checkbox = true>
						</cfif>	
						<!--- <cfinput type="checkbox" name="#Reporting_Dashboard_Template_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" class="reportingCALLRESULTSChilds" value="#Reporting_Dashboard_Template_Group_Title#" checked="#Checked_checkbox#">  --->
                        
                        
                        <cfinput type="checkbox" name="#Reporting_Dashboard_Template_Group_Title#" id="ReportSTATESGroup" onclick = "parentCheckboxCheck(this,'.reportCALLRESULTSGroupChilds','Reporting')" <!--- class="reportingCALLRESULTSChilds" ---> value="#Reporting_Dashboard_Template_Group_Title#" checked="#Checked_checkbox#" class = "grouping"> 
						<label for="ReportCALLRESULTSGroup">#Reporting_Dashboard_Template_Group_Title#</label> 
					</span>
				</cfif>
			
				<cfloop array="#Reporting_Dashboard_Template_List#" index="ListLoop" >
                				
					<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                        <li>
                            <cfset Checked_checkbox = false>
                            <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                                <cfset Checked_checkbox = true>
                            </cfif>
                            <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="reportCALLRESULTSGroupChilds" value="#ListLoop#" checked="#Checked_checkbox#"> ---> 
                            <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="reportCALLRESULTSGroupChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
                            <label for="ReportCALLRESULTSGroup">#ListLoop#</label>
                        </li>
                    </cfif>
                    
                </cfloop>
                
			</ul>
			
</ul> 
       
        <ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Developers_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Developers_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<!--- <cfinput type="checkbox" name="#Developers_Title#" id="Developers" onclick = "parentCheckboxCheck(this,'.DevelopersChilds','')" value="#Campaign_Title#" checked="#Checked_checkbox#">  ---> 
                    
                    <cfinput type="checkbox" name="#Developers_Title#" id="Developers"  value="#Campaign_Title#" checked="#Checked_checkbox#" class="grouping">  
                    
					<label class="label_permission" for="Developers">#Developers_Title#</label>
				</span>
			</cfif>
			
            
           <cfloop array="#Developers_List#" index="ListLoop" >
                				
				<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                    <li>
                        <cfset Checked_checkbox = false>
                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                            <cfset Checked_checkbox = true>
                        </cfif>


                        <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="DeveloperChilds" value="#ListLoop#" checked="#Checked_checkbox#">  --->
                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="DeveloperChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                        <label for="DevelopersHelp">#ListLoop#</label>
                    </li>
                </cfif>
                
            </cfloop>
        						
		</ul>
		
		<ul class="list_permission">
			<cfif listfindNoCase(listAllPermission,EMS_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,EMS_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<!--- <cfinput type="checkbox" name="#ems_title#" id="ems" onclick = "parentCheckboxCheck(this,'.emsChilds','')" value="#Campaign_Title#" checked="#Checked_checkbox#"> --->

                    <cfinput type="checkbox" name="#ems_title#" id="ems"  value="#EMS_Title#" checked="#Checked_checkbox#" class="grouping">

					<label class="label_permission" for="EMS">#EMS_Title#</label>
				</span>
			</cfif>
			
			<cfloop array="#EMS_List#" index="ListLoop" >
				<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                    <li>
                        <cfset Checked_checkbox = false>
                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                            <cfset Checked_checkbox = true>
                        </cfif>
                        <!--- <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" class="emsChilds" value="#ListLoop#" checked="#Checked_checkbox#"> --->
                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="emsChilds" ---> value="#ListLoop#" checked="#Checked_checkbox#">
                        <label for="label_permission">#ListLoop#</label>
                    </li>
                </cfif>
            </cfloop>
		</ul>
		
		<ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,SFTPTools_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,SFTPTools_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<!--- <cfinput type="checkbox" name="#UnitTest_Title#" id="UnitTest" onclick = "parentCheckboxCheck(this,'.UnitTestChilds','')" value="#UnitTest_Title#" checked="#Checked_checkbox#">  --->
                    
                    <cfinput type="checkbox" name="#SFTPTools_Title#" id="SFTPTools_Title"  value="#SFTPTools_Title#" checked="#Checked_checkbox#" class = "grouping">   
					<label class="label_permission" for="UnitTest">#SFTPTools_Title#</label>
				</span>
			</cfif>
			
		</ul>
        
        <ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,UnitTest_Title) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,UnitTest_Title) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<!--- <cfinput type="checkbox" name="#UnitTest_Title#" id="UnitTest" onclick = "parentCheckboxCheck(this,'.UnitTestChilds','')" value="#UnitTest_Title#" checked="#Checked_checkbox#">  --->
                    
                    <cfinput type="checkbox" name="#UnitTest_Title#" id="UnitTest"  value="#UnitTest_Title#" checked="#Checked_checkbox#" class = "grouping">   
					<label class="label_permission" for="UnitTest">#UnitTest_Title#</label>
				</span>
			</cfif>
			
		</ul>
        
            <!--- Agents Section --->	
         <ul class="list_permission">
			
			<cfif listfindNoCase(listAllPermission,Agents) GT 0>
				<span class="label_permission">
					<cfset Checked_checkbox = false>
					<cfif listfindNoCase(checkedAllPermistion,Agents) GT 0>
						<cfset Checked_checkbox = true>
					</cfif>
					<cfinput type="checkbox" name="#Agents#" id="Agents"  value="#Agents#" checked="#Checked_checkbox#" class="grouping" >  
					<label class="label_permission" for="Agents">#Agents#</label>
				</span>
			</cfif>
			
			<cfloop array="#Agent_List#" index="ListLoop" >
                				
				<cfif listfindNoCase(listAllPermission,ListLoop) GT 0>
                    <li>
                        <cfset Checked_checkbox = false>
                        <cfif listfindNoCase(checkedAllPermistion,ListLoop) GT 0>
                            <cfset Checked_checkbox = true>
                        </cfif>
                        <cfinput type="checkbox" name="#ListLoop#" id="ListLoop" <!--- class="Agents Childs" ---> value="#ListLoop#" checked="#Checked_checkbox#"> 
                        <label for="label_permission">#ListLoop#</label>
                    </li>
                </cfif>
                
            </cfloop>
			
		</ul>
        
        
        <ul class="list_permission">
            <cfif listfindNoCase(listAllPermission,Agent_Tool_Title) GT 0>
                    <span class="label_permission">
                    <cfset Checked_checkbox = false>
                    <cfif listfindNoCase(checkedAllPermistion,Agent_Tool_Title) GT 0>
                        <cfset Checked_checkbox = true>
                    </cfif>
                    <cfinput type="checkbox" name="#Agent_Tool_Title#" id="Agent tool"  value="#Agent_Tool_Title#" checked="#Checked_checkbox#" class = "grouping">
                    <label class="label_permission" for="Agent tool">#Agent_Tool_Title#</label>
                </span>
            </cfif>
        </ul>
		
               
        <button type="button" class="btn btn-primary" onclick ="SavePermissions();return false">Save changes</button>
       
        
        
	</cfform>
</cfoutput>


 <p class="lead"></p>

    	
        <row>
        	
            <div class="">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->

<script type="text/javascript">
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#permissionSubtitle# >> #Admin_User_Permission_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
		
		
	<!---	/*
		if(!$('#Campaigns').is(':checked')){
			$('.CampaignChilds').attr('disabled',true);
		}else{
			$('.CampaignChilds').attr('disabled',false);
		}
		
		if(!$('#Marketing').is(':checked')){
			$('.marketingChilds').attr('disabled',true);
		}else{
			$('.marketingChilds').attr('disabled',false);
		}
		
		if(!$('#Surveys').is(':checked')){
			$('.surveyChilds').attr('disabled',true);
		}else{
			$('.surveyChilds').attr('disabled',false);
		}
		
		if(!$('#Contacts').is(':checked')){
			$('.contactChilds').attr('disabled',true);
		}else{
			$('.contactChilds').attr('disabled',false);
		}
		
		if(!$('#ContactList').is(':checked')){
			$('.contactListChilds').attr('disabled',true);
		}else{
			$('.contactListChilds').attr('disabled',false);
		}
		
		if(!$('#ContactGroup').is(':checked')){
			$('.contactGroupChilds').attr('disabled',true);
		}else{
			$('.contactGroupChilds').attr('disabled',false);
		}
		
		if(!$('#Reporting').is(':checked')){
			$('.ReportingChilds').attr('disabled',true);
		}else{
			$('.ReportingChilds').attr('disabled',false);
		}
		
		
		if(!$('#ReportGroup').is(':checked')){
			$('.reportGroupChilds').attr('disabled',true);
		}else{
			$('.reportGroupChilds').attr('disabled',false);
		}
		
		if(!$('#ReportSMSGroup').is(':checked')){
			$('.reportSMSGroupChilds').attr('disabled',true);
		}else{
			$('.reportSMSGroupChilds').attr('disabled',false);
		}
		
		if(!$('#ReportSTATESGroup').is(':checked')){
			$('.reportSTATESGroupChilds').attr('disabled',true);
		}else{
			$('.reportSTATESGroupChilds').attr('disabled',false);
		}
		
		if(!$('#ReportCALLRESULTSGroup').is(':checked')){
			$('.reportCALLRESULTSGroupChilds').attr('disabled',true);
		}else{
			$('.reportCALLRESULTSGroupChilds').attr('disabled',false);
		}
		
		*/--->
		
		<!---console.log('RoleType=' + '<cfoutput>#checkPermissionPageAccess.userRole#</cfoutput>');
		console.log('RoleType=' + '<cfoutput>#RoleType#</cfoutput>');
		console.log(<cfoutput>#RoleType#</cfoutput>==2 ? 'User':'Company');--->
			
		
		$('.grouping').on('click', function() {
        parentCheckboxCheck(this)
        });
		
			
	});
	
	
	function SavePermissions()
	{
		$.ajax({
				type: "POST",
				url: <cfoutput>'#rootUrl#/#sessionPath#/cfc/administrator/permission.cfc?method=editUCRole&UCID=#UCID#&RoleType=#RoleType#&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true'</cfoutput>,
				dataType: 'json',
				data: { dataContent : JSON.stringify($('#permissionForm').serializeArray())},
				success: function(data) {
					
					var url ='';

				  	if(data.SUCCESS == 1)
					{
				  		var UserRole = '<cfoutput>#checkPermissionPageAccess.userRole#</cfoutput>';
				  		var RoleType = '<cfoutput>#RoleType#</cfoutput>';

				  		if(UserRole == 'CompanyAdmin'){
				  			url = 'company/users';
				  		}else if(UserRole == 'SuperUser' && RoleType == 1){
				  			url = 'company/company';
				  		}else if(UserRole == 'SuperUser' && RoleType == 2){
				  			url = 'users/users';
				  		}

						bootbox.alert(data.MESSAGE + ' - Update ' + (RoleType==2 ? 'User':'Company') + ' Permissions', function(result)
						{
							if(data.SUCCESS == 1 && url != ''){
								window.location ='<cfoutput>#rootUrl#/#sessionPath#/Administration/</cfoutput>'+url;
							}
						});
				  	}
					else
					{

						bootbox.alert(data.MESSAGE + ' - Update ' + (RoleType==2 ? 'User':'Company') + ' Permissions', function(result)
						{

						});

					}
				}
			});
			return false;		
		
	}
	
	
	function parentCheckboxCheck(obj, childrenClass, parentId ){
    var el = $(obj);
    el.closest('.list_permission')
        .find(':checkbox')
        .not(this).each(function(){
            $(this).prop('checked', el.is(':checked'));
        });
	};
	
	/*
	function parentCheckboxCheck(obj, childrenClass, parentId ){
		if(parentId == '' ){
			if($(obj).is(':checked')){
				$(childrenClass).attr('disabled',false);
				$(childrenClass).attr('checked',true);
			}
			else {
				$(childrenClass).attr('checked',false);
				$(childrenClass).attr('disabled',true);
			}
				
		}else{
			if($(obj).is(':checked') && $('#'+parentId).is(':checked')){
				$(childrenClass).attr('disabled',false);
				$(childrenClass).attr('checked',true);
			}
			else {
				$(childrenClass).attr('checked',false);
				$(childrenClass).attr('disabled',true);
			}
		}
	}
	
	
*/
	
	
	
	
	
	
	

</script>

<cfoutput>
	<style type="text/css">
		@import url('#rootUrl#/#PublicPath#/css/administrator/administrator.css');
	</style>
</cfoutput>