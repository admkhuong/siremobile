<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfinclude template="../../../cfc/csc/constants.cfm">
<cfparam name="aggregatorId" default="">
<cfparam name="IsView" default="">
<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>

<cfif aggregatorId EQ "">
	<cfinvoke 
		component="#SessionPath#.cfc.csc.csc"
		method="GetMaxAggregatorId"
		returnvariable="dbResult">
	</cfinvoke>

	<cfset newAggregatorId = dbResult.MaxAggregatorId + 1>
</cfif>
<div class="sms_full_width">
	<br/>
	<div class="sms_popup_row_first">
		<div class="sms_popup_row_label">
			ID
		</div>
		<div class="sms_popup_row_control">
			<label id="txtAggregatorId">
			<cfif aggregatorId EQ "">
				<cfoutput>#newAggregatorId#</cfoutput>
			</cfif>
			</label>
		</div>
	</div>
	
	<div class="sms_popup_row">
		<div class="sms_popup_row_label">
			Name
		</div>
		<div class="sms_popup_row_control3">
			<cfif IsView EQ "">
				<input id="txtName" class="sms_text_width" type="text">
			<cfelse>
				<input id="txtName" class="sms_text_width" type="text" disabled>
			</cfif>
		</div>
	</div>
	<div class="sms_popup_action">
		<cfif IsView EQ "">
			<input type="button" value="Cancel" class="button filterButton small" onclick="closeDialog()">
			<input type="button" value="Save" class="button filterButton small" onclick="Save()">
		<cfelse>
			<input type="button" value="OK" class="button filterButton small" onclick="closeDialog()">
		</cfif>
	</div>	
</div>

<script type="text/javascript">
	$(function() {
		InitData();
	});
	
	function Save() {
		var ID = $('#txtAggregatorId').html();
		var Name = $.trim($('#txtName').val());
		if (!Validate(Name)) {
			return;	
		}
		if ('<cfoutput>#aggregatorId#</cfoutput>' == '') {
			var data = { 
		     	AggregatorId : ID,
		     	AggregatorName : Name			     	
		    };
		    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'AddAggregator', data, "Add Aggregator fail!", function(d ) {
				closeDialog();
				jAlert("Aggregator added", "Success!", function(result) { 
					//location.reload();
					InitAggregatorManager();
				});
			});
		}
		else {
			var data = { 
		     	AggregatorId : ID,
		     	AggregatorName : Name			     	
		    };
		    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'UpdateAggregator', data, "Update Aggregator fail!", function(d ) {
				closeDialog();
				jAlert("Aggregator updated", "Success!", function(result) { 
					InitAggregatorManager();
					//location.reload();
				});
			});						
		}	
	}
	
	function Validate(name) {
		if (name == '') {
			jAlert("You must enter the name of the aggregator before saving.", "Failure!", function(result) {});
			return false;	
		}
		return true;
	}
	function InitData() {
		<cfif aggregatorId NEQ "">
			var data = { 
			     	aggregatorId : '<cfoutput>#aggregatorId#</cfoutput>'
			    };
			    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetAggregator', data, "Get Aggregator fail!", function(d ) {
				$('#txtName').val(d.NAME);
		     	$('#txtAggregatorId').html('<cfoutput>#aggregatorId#</cfoutput>');
			});	
		</cfif>	
	}
</script>
