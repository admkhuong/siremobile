<cfparam name="AggregatorId" default="">

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>

<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="GetAggregator" returnvariable="retGetAggregator">
	<cfinvokeargument name="aggregatorId" value="#aggregatorId#">
</cfinvoke>

<div class="sms_full_width">
	<div class="scrText sms_align_center">
		<cfoutput>
			Would you like to delete <cfoutput>#retGetAggregator.Name#</cfoutput> from the aggregator list?
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<input type="button" class="button filterButton small" id="btnNo" value="Cancel" >
		<input type="button" class="button filterButton small" id="btnYes" value="Delete" >
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			newDialog.dialog('close');
		});
		
		$('#btnYes').click(function(){
			var data = { 
					     	AggregatorId : <cfoutput>#AggregatorId#</cfoutput>
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'DeleteAggregator', data, "Delete Aggregator fail!", function(d ) {
					jAlert("Aggregator deleted", "Success!", function(result) { 
						location.reload();
					});
				});	
		});
	});
</script>
