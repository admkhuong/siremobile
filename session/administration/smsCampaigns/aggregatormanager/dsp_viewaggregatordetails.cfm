<cfparam name="aggregatorId" default="">

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="GetAggregator" returnvariable="retGetAggregator">
	<cfinvokeargument name="aggregatorId" value="#aggregatorId#">
</cfinvoke>

<cfimport taglib="../../../lib/grid" prefix="mb" />
<div class="sms_grid_header">
	<div class="sms_grid_header_content">
		Carrier using <cfoutput>#retGetAggregator.Name#</cfoutput>
	</div>
</div>
 <cfoutput>
	<cfscript>
		htmlOption = '<a href="##">Edit</a>';
	</cfscript>
	
	<cfset htmlFormat = {
		name ='normal',
		html = '#htmlOption#'
	}>

	<cfset params = [{name="AggregatorId", value="#AggregatorId#"}]>
	<!--- Prepare column data---->
	<cfset arrNames = ['Name', 'Options']>	
	<cfset arrColModel = [			
			{name='Name', width='85%'},	
			{name='FORMAT', width='15%', format = [htmlFormat]}] >		
 	<mb:table 
		component="#SessionPath#.cfc.csc.csc" 
		method="GetCarrierListByAggregatorId" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		params = #params#
	>
	</mb:table>
</cfoutput>
