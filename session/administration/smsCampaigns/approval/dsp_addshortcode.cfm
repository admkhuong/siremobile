<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfimport taglib="../../../lib/grid" prefix="mb" />
<cfinclude template="../../../cfc/csc/constants.cfm">

<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/sms/ui.spinner.js"></script>
<link rel="stylesheet" type="text/css" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/sms/ui.spinner.css"/>
<div style="width: 100%">
	
		<div class="scrText">
			<div class="item">
				<label>
					Short code
				</label>
				<div class="control">
					<input type="text" style="width:160px;" id="txtShortCode" maxlength="7" > 
				</div>					
			</div>
			
			<div class="item">
				<label>
					Classification
				</label>
				<div class="control">
					<select id="selectClassification" style="width:180px" >
						<option value="<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>">Public (Shared)</option>
						<option value="<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>">Public (Not Shared)</option>
						<option value="<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>">Private</option>
					</select>
				</div>					
			</div>
				
				
				<cfscript>
					htmlApproval = "<input type='checkbox' name='{%CarrierId%}'>";
					htmlStatus =   "<select id='selectStatus' name='{%CarrierId%}' style='width:180px'>
										<option value='#UNSUBMITTED_VALUE#'>#UNSUBMITTED#</option>
										<option value='#SUBMITTED_VALUE#'>#SUBMITTED#</option>
										<option value='#DECLINED_VALUE#'>#DECLINED#</option>
										<option value='#DECLINED_RESUBMITTED_VALUE#'>#DECLINED_RESUBMITTED#</option>
										<option value='#HALTED_VALUE#'>#HALTED#</option>
										<option value='#HALTED_RESUBMITTED_VALUE#'>#HALTED_RESUBMITTED#</option>
										<option value='#APPROVED_VALUE#'>#APPROVED#</option>
									</select>";
				</cfscript>
				
				<cfset htmlFormatApproval = {
							name ='normal',
							html = '#htmlApproval#'
				}>
				
				<cfset htmlFormatStatus = {
							name ='normal',
							html = '#htmlStatus#'
				}>
				
				<!--- Prepare column data---->
				<cfset arrNames = ['Carrier', 'Status', 'Approval required for Release']>	
				<cfset arrColModel = [			
						{name='Carrier', width='30%'},	
						{name='FORMAT', width='40%',  format = [htmlFormatStatus]},
						{name='FORMAT', width='30%', format = [htmlFormatApproval]}]>
				<div class="item" style="height:200px;  overflow-x:auto; margin-top:40px;" >				
				 	<mb:table 
						component="#SessionPath#.cfc.smscampaigns.shortCodeApproval" 
						method="GetCarrierList" 
						width="100%" 
						colNames= #arrNames# 
						colModels= #arrColModel#
						IsPaging = false
					>
					</mb:table>
			 	</div> 
		</div>
		<div class="scrButtonBar">
			<input type="button" class="button filterButton small" value="Cancel" onclick="closeDialog()">
			<input type="button" class="button filterButton small" value="Save" onclick="saveSC(); return false;">
		</div>
	</div>

<style type="text/css">
	.item .cf_table{
		margin-top:0;
	}
</style>

<script type="text/javascript">
	function saveSC(){
		
		var carrierList = new Array();
		
		$("input[type='checkbox']").each(function( index ) {
			var carrierID= $("input[type='checkbox']")[index].name;
			var status = $("select[name = " + carrierID +"]").val();
			var approval = $(this).is(':checked') ? 1 : 0;
			carrierList.push({"carrierId":carrierID, "status": status,"approval": approval});
		});
		
		$.ajax({
			type: "POST", <!--- Update short code if exist else alert error --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeApproval.cfc?method=InsertShortCodeCarrier&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpSCText: $('#txtShortCode').val(),
				inpScType: $('#selectClassification').val(),
				carrierList: JSON.stringify(carrierList)
				
			},					  
			success:function(d){
				<!--- Get row 1 of results if exisits, Alert if failure--->
				if (d.ROWCOUNT > 0){						
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
						if(d.DATA.RXRESULTCODE[0] > 0){
							closeDialog();
							InitGetShortCodeApprovalList();
						}else{
							jAlert(d.DATA.MESSAGE[0], 'Failure');							
						}
					}
				}else{
					<!--- No result returned --->		
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
			} 			
		});
	}	
</script>
