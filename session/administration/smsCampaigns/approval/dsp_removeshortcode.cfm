﻿<cfparam name="ShortCode" default="">
<cfparam name="ShortCodeId" default="">

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div style="width: 100%">
	<div class="scrText" style="text-align: center;">
		<cfoutput>
			Do you no longer need to track the approval statuses of <cfoutput>#ShortCode#</cfoutput>?
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<input type="button" class="button filterButton small" id="btnNo" value="Cancel" >
		<input type="button" class="button filterButton small" id="btnYes" value="Delete" >
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			newDialog.dialog('close');
		});
		
		$('#btnYes').click(function(){
			var data = { 
					     	ShortCodeId : <cfoutput>#ShortCodeId#</cfoutput>
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeApproval.cfc', 'removeShortCode', data, "Remove Short Code approval fail!", function(d ) {
					jAlert("Short Code approval deleted", "Success!", function(result) { 
						location.reload();
					});
				});	
		});
	});
</script>
