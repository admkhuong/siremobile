﻿<cfparam name="shortCodeApprovalId" default="">
<cfimport taglib="../../../lib/grid" prefix="mb" />
<cfinclude template="../../../cfc/csc/constants.cfm">

 <cfoutput>
 	 
	<!--- Prepare column data---->
	<cfset arrNames = ['Previous Status', 'Current Status', 'Date', 'Notes']>	
	<cfset arrColModel = [			
			{name='PreviousStatus', width='25%'},
			{name='CurrentStatus', width='25%', align="center"},	
			{name='Date', width='25%', align="center"},
			{name='Notes', width='25%', align="center"}] >		


	<cfset arrParams = [{name='shortCodeApprovalId',value="#shortCodeApprovalId#"}] />
 	<mb:table 
		component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeApproval" 
		method="GetUpdateLogList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		params="#arrParams#"
		IsPaging = false 
	>
	</mb:table>
</cfoutput>

<script type="text/javascript">
	
</script>