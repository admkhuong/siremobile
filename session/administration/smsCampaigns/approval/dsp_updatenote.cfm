﻿<cfparam name="noteId" default="">
<cfparam name="note" default="">
 <cfoutput>
	<textarea id="inpNote" cols="40" rows="13" style="margin:20px 10px 10px 20px;" placeholder="Note contents go here" >#note#</textarea>
 </cfoutput>

<div class="scrButtonBar" style="text-align:right;margin-right:20px">
	<a href="#" class="button filterButton small" id="btnSaveUpdateStatus">Save</a>
</div>
<script type="text/javascript">
	$(function(){
		$('#btnSaveUpdateStatus').click(function(){
			var newNote = $('#inpNote').val();
			$('#<cfoutput>#noteId#</cfoutput>').val(newNote);
			$('#updateStatus').dialog('close');
			return false;
		});
	});
</script>