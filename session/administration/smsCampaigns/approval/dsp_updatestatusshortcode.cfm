﻿<cfparam name="shortCodeId" default="">
<cfimport taglib="../../../lib/grid" prefix="mb" />
<cfinclude template="../../../cfc/csc/constants.cfm">

<style>
	.slAllApproved {
	    text-decoration: underline !important;
	    color: #365581 !important;
		position:relative;
		top:30px;
		left:500px;
	}
	
	.slAllApproved:hover, .slAllApproved:active {
	    text-decoration: none !important;
	    color: #365581 !important;
	}
</style>
 <cfoutput>
	<cfscript>
		htmlStatus =            '<select class="slStatus" id="status{%RowId%}"> ';
		htmlStatus = htmlStatus &   '<option value="#UNSUBMITTED_VALUE#" {%UNSUBMITTED_SELECTED%} >#UNSUBMITTED#</option>';
		htmlStatus = htmlStatus &   '<option value="#SUBMITTED_VALUE#" {%SUBMITTED_SELECTED%}>#SUBMITTED#</option>';
		htmlStatus = htmlStatus &   '<option value="#DECLINED_VALUE#" {%DECLINED_SELECTED%}>#DECLINED#</option>';
		htmlStatus = htmlStatus &   '<option value="#DECLINED_RESUBMITTED_VALUE#" {%DECLINED_RESUBMITTED_SELECTED%}>#DECLINED_RESUBMITTED#</option>';
		htmlStatus = htmlStatus &   '<option value="#HALTED_VALUE#" {%HALTED_SELECTED%}>#HALTED#</option>';
		htmlStatus = htmlStatus &   '<option value="#HALTED_RESUBMITTED_VALUE#" {%HALTED_RESUBMITTED_SELECTED%}>#HALTED_RESUBMITTED#</option>';
		htmlStatus = htmlStatus &   '<option value="#APPROVED_VALUE#" {%APPROVED_SELECTED%}>#APPROVED#</option>';
		htmlStatus = htmlStatus & '</select>';
		htmlStatus = htmlStatus & '<img class="updateNote" src="#rootUrl#/#PublicPath#/images/notes.png" rel="{%RowId%}" width="14px" style="position:relative;top:2px;margin-left:5px;cursor:pointer;">';
		htmlStatus = htmlStatus & '<input id="hdNotes{%RowId%}" type="hidden" value="" />';
		htmlStatus = htmlStatus & '<input id="shortCodeApproval{%RowId%}" type="hidden" value="{%ShortCodeApprovalId%}" />';
		
		htmlDate = '<input type="text" class="datefield" id="updateDate{%RowId%}" value="{%UpdatedDate%}" style="width:80px" />';
		
		htmlApprovalRequired = '<input type="checkbox" id="approvalRequired{%RowId%}" {%ApprovalRequiredChecked%} />';
		
		htmlStatusLog = '<a href="##" onclick="showUpdateLog({%ShortCodeApprovalId%}); return false;">{%NumberUpdates%} updates</a>';
	</cfscript>
	
	<cfset htmlStatusFormat = {
				name ='normal',
				html = '#htmlStatus#'
			}>
	<cfset htmlDateFormat = {
				name ='normal',
				html = '#htmlDate#'
			}>	
			
	<cfset htmlApprovalRequiredFormat = {
				name ='normal',
				html = '#htmlApprovalRequired#'
			}>			
	<cfset htmlStatusLogFormat = {
				name ='normal',
				html = '#htmlStatusLog#'
	}>
	<!--- Prepare column data---->
	<cfset arrNames = ['Carrier', 'Status', 'Date', 'Approval Required for Release', 'Status Log']>	
	<cfset arrColModel = [			
			{name='Carrier', width='20%'},
			{name='StatusFormat', width='30%', format = [htmlStatusFormat], align="center"},	
			{name='DateFormat', width='20%', format = [htmlDateFormat], align="center"},
			{name='ApprovalRequiredFormat', width='20%', format=[htmlApprovalRequiredFormat], align="center"},
			{name='StatusLogFormat', width='20%',format=[htmlStatusLogFormat], align="center"}] >		


	<cfset arrParams = [{name='shortCodeId',value="#shortCodeId#"}] />
<div style="height:250px;overflow-x:auto; margin:20px 10px 10px 10px;">
	<a href="##" class='slAllApproved' onclick="setAllToApproved(); return false;" >Set all to Approved</a>
 	<mb:table 
		component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeApproval" 
		method="GetUpdateShortCodeList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		params="#arrParams#"
		rowClass="updateStatusRow"	 
		IsPaging = false  
	>
	</mb:table>
</div>	
</cfoutput>

<div class="scrButtonBar" style="text-align:center;">
	<a href="#" class="button filterButton small" onclick="closeThis(); return false;">Cancel</a>
	<a href="#" class="button filterButton small" onclick="saveUpdateStatus();">Save</a>
</div>
<script type="text/javascript">
	$(function(){
		
		$('.datefield').datepicker({
				numberOfMonths: 1,
				showButtonPanel: true,
				dateFormat: 'mm/dd/yy',
				showOn: 'button',
				buttonImage: "<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/date_picker.jpg",
		        buttonImageOnly: true,
		        constrainInput: false
		});
		
		$('.updateNote').click(function(){
			var id = "hdNotes" + $(this).attr("rel");
			var note = $(this).parent().find('#' + id).val();
			
			var $updateStatusDialog = $('<div id="updateStatus"></div>');
			
			$updateStatusDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/approval/dsp_updateNote?note=' + note + '&noteId=' + id)
				.dialog({
					modal : true,
					title: '',
					width: 400,
					minHeight: 350,
					position: 'top',
					resizable: false,
					close: function() { 
						$updateStatusDialog.remove(); 
					}
				});
		
			return false;
		});	
	});
	
	function showUpdateLog(shortCodeApprovalId){
		var $showUpdateLog = $('<div id="updateLog"></div>');
			
		$showUpdateLog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/approval/dsp_showUpdateStatusLog?shortCodeApprovalId=' + shortCodeApprovalId)
			.dialog({
				modal : true,
				title: 'Update Status of Short Code',
				width: 700,
				minHeight: 350,
				position: 'top',
				resizable: false,
				close: function() { 
					$showUpdateLog.remove(); 
			    }
		  });
	}
	
	function closeThis(){
		$('#updateShortCodeStatus').dialog('close');
	}
	
	function saveUpdateStatus(){
		var updateStatusArr = new Array();
		$.each($('.updateStatusRow'), function(index, value) {
			var rowIndex = index +1;
			var shortCodeApprovalId = $(this).find('#shortCodeApproval' + rowIndex).val();
			var statusId = $(this).find('#status' + rowIndex).val();
			var note = $(this).find('#hdNotes' + rowIndex).val();
			var date = $(this).find('#updateDate' + rowIndex).val();
			var approvalRequired = $(this).find('#approvalRequired' + rowIndex).is(':checked');
			
			var updateStatusItem  = {};
			updateStatusItem.ShortCodeApprovalId = shortCodeApprovalId;
			updateStatusItem.StatusId = statusId;
			updateStatusItem.Note = note;
			updateStatusItem.Date = date;
			updateStatusItem.ApprovalRequired = approvalRequired;
			updateStatusArr.push(updateStatusItem);
		});
		
		var data = {
			updateStatusData: JSON.stringify(updateStatusArr)
		};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeApproval.cfc', 
				'UpdateStatus', 
				 data, 
				 "Update status fail.", 
				 function(d ) {
					closeDialog();
					location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/shortCodeApproval'; 
				 });	
	}
	function setAllToApproved(){
		$('.slStatus').val('<cfoutput>#APPROVED_VALUE#</cfoutput>');
		$('.datefield').datepicker('setDate', new Date());
		return false;
	}
</script>