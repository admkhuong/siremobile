<cfparam name="page" default="1">

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<cfinclude template="../../../cfc/csc/constants.cfm">

<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<script language="javascript" src="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/js/sms/ui.spinner.js"></script>
<link rel="stylesheet" type="text/css" href="<cfoutput>#rootUrl#/#publicPath#</cfoutput>/css/sms/ui.spinner.css"/>
<div style="width: 100%">
	
		<div class="scrText">
			<div class="item">
				<label>
					Type
				</label>
				<div class="control">
					<select id="selectClassification" style="width:180px" onchange="ChangeClassification()">
						<option value="<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>">Public (Shared)</option>
						<option value="<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>">Public (Not Shared)</option>
						<option value="<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>">Private</option>
					</select>
				</div>					
			</div>
			
			<div class="item">
				<label>
					Short code
				</label>
				<div class="control">
					<input type="text" style="width:160px; display:none;" id="txtShortCode" maxlength="7" > 
					<select id="sltShortCode" style="width:180px;"></select>
				</div>					
			</div>
			
			<div class="item">
				<label>
					Account Type
				</label>
				<div class="control">
					<select id="selectAccountType" style="width:180px">
						<option value="<cfoutput>#OWNER_TYPE_COMPANY#</cfoutput>">Company</option>
						<option value="<cfoutput>#OWNER_TYPE_USER#</cfoutput>">User</option>
					</select>
				</div>					
			</div>
			
			<div class="item">
				<label>
					Account ID
				</label>
				<div class="control">
					<input type="text" style="width: 160px;" id="txtOwnerId">
				</div>					
			</div>
			<div class="item" id="keywords">
				<label>
					Keywords
				</label>
				<div class="control">
					<input type="text" style="width: 20px;" id="inpKeywords">
				</div>					
			</div>
		</div>
		<div class="scrButtonBar">
			<input type="button" class="button filterButton small" value="Cancel" onclick="closeDialog()">
			<input type="button" class="button filterButton small" value="Save" onclick="Save()">
		</div>
	</div>

<script type="text/javascript">
	
	$(function(){
		initValue();
	});

	function initValue(){
		getAvaiablePublicShortCode('<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>');
			
		$('#keywords').show();
		$('#sltShortCode').show();
		$('#txtShortCode').hide();
		$('#inpKeywords').spinner({min: 0});
		$('#inpKeywords').spinner( "value", 3 );
		$('#inpKeywords').css( "width", "30px" );
		$('#inpKeywords').ForceNumericOnly();
	}	
	
	function Save() {
		var classificationId = $.trim($('#selectClassification').val());
		var ownerType = $.trim($('#selectAccountType').val());
		var ownerId = $.trim($('#txtOwnerId').val());
		var numberKeywords = 0;
		if($.trim($('#inpKeywords').val()) != ''){
			numberKeywords = $.trim($('#inpKeywords').val());
		}
		
		var shortCodeId = 0;
		if (classificationId != '<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>') {
			shortCodeId = $.trim($('#sltShortCode option:selected').val());
		}
		
		var shortCode = $.trim($('#txtShortCode').val()).replace(new RegExp(" ", "g"), '');
		if (classificationId != '<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>') {
			shortCode = $.trim($('#sltShortCode option:selected').html());
		}
		var isValid = Validate(shortCode, classificationId, ownerId);
		
		if (isValid) {
			
			var validationData = { 
			     	ShortCode : shortCode
			    };		
								
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetShortCode', validationData, "Get Short Code fail!", function(d ) {
				if(classificationId == '<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>'){
						if (d.ISEXISTS != 1) {
							var data = { 
									ClassificationId: classificationId,
							     	OwnerId: ownerId,
							     	OwnerType: ownerType
							    };
							    		
							ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'CheckAccount', data, "Get owner name fail!", function(d ) {
								if (d.ISEXISTED == false) {
									jAlert("Account ID could not be found", "Failure!", function(result) { });
								}
								else {
									SaveData(shortCode,shortCodeId, classificationId, ownerId, ownerType, numberKeywords);
								}
							});	
						}
						else {
							jAlert("This shortcode is existing.", "Failure!", function(result) { });
						}
				} else{
					if (d.ISEXISTS == 1) {
						var data = { 
								ClassificationId: classificationId,
						     	OwnerId: ownerId,
						     	OwnerType: ownerType
						    };
						    		
						ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'CheckAccount', data, "Get owner name fail!", function(d ) {
							if (d.ISEXISTED == false) {
								jAlert("Account ID could not be found", "Failure!", function(result) { });
							}
							else {
								SaveData(shortCode, shortCodeId, classificationId, ownerId, ownerType, numberKeywords);
							}
						});	
					}
					else {
						jAlert("This public shortcode does not exist.", "Failure!", function(result) { });
					}
					
				}
			});	
		}
		
		function SaveData(shortCode, shortCodeId, classificationId, ownerId, ownerType, numberKeywords) {
			var data = { 
			     	ShortCode : shortCode,
					shortCodeId : shortCodeId,
			     	Classification : $('#selectClassification').val(),
			     	OwnerId: ownerId,
			     	OwnerType: $('#selectAccountType').val(),
					NumberKeywords: numberKeywords
			    };
			    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'AssignShortCode', data, "Assign Short Code fail!", function(d ) {
					<cfoutput>
						var params = {
							'page':'#page#'
						};
						post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeAssignment', params, 'POST');
					</cfoutput>
				});
			
		}
		
	}
		
	function Validate(shortCode, classificationId, ownerId) {
		if (!IsNumber(shortCode)) {
			jAlert("Short Code must be a number", "Failure!", function(result) { } );	
			return false;
		}
		else {
			var maxValue = parseInt('<cfoutput>#MAXINTEGERVALUE#</cfoutput>');
			if (shortCode < 0 || shortCode > maxValue) {
				jAlert("Short Code must be in range 0-<cfoutput>#MAXINTEGERVALUE#</cfoutput>", "Failure!", function(result) { } );					
				return false;
			}
		}
		if (classificationId == '<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>') {
			if (!IsNumber(ownerId)) {
				jAlert("Owner Id must be a number", "Failure!", function(result) { } );	
				return false;
			}
			else {
				var ownerId = parseInt(ownerId);
				var maxValue = parseInt('<cfoutput>#MAXINTEGERVALUE#</cfoutput>');
				if (ownerId < 0 || ownerId > maxValue) {
					jAlert("Owner Id must be in range 0-<cfoutput>#MAXINTEGERVALUE#</cfoutput>", "Failure!", function(result) { } );					
					return false;
				}
			}
		}
		return true;
	}
	
	function ChangeClassification() {
		var value = $('#selectClassification').val();
		if (value == '<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>') {
			
			getAvaiablePublicShortCode(value);
			
			$('#keywords').show();
			$('#sltShortCode').show();
			$('#txtShortCode').hide();
			$('#inpKeywords').spinner({min: 0});
			$('#inpKeywords').spinner( "value", 3 );
			$('#inpKeywords').css( "width", "30px" );
			$('#inpKeywords').ForceNumericOnly();
		}
		else if(value == '<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>') {
			
			getAvaiablePublicShortCode(value);
			
			$('#keywords').hide();	
			$('#sltShortCode').show();
			$('#txtShortCode').hide();	
		}else{
			$('#keywords').hide();	
			$('#sltShortCode').hide();
			$('#txtShortCode').show();	
		}
	}
	
	/**
	 * Filter data to short code selection
	 */
	function getAvaiablePublicShortCode(publicType){
		<cfoutput>
			var data = { 
			     	'inpPublicType':publicType
			    };
			    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'getAvailableShortCode', data, "Get Short Code fail!", function(d ) {
				var shortCodeArr = d.SHORTCODEARR;
				$('##sltShortCode').html('');
				
				var htmlOption = ''; 
				for(i=0; i< shortCodeArr.length; i++){
					var shortCodeObj = shortCodeArr[i];
					htmlOption = htmlOption + '<option value="'+ shortCodeObj.SHORTCODE_VCH +'">' + shortCodeObj.SHORTCODE_VCH + '</option>';
				}
				$('##sltShortCode').append(htmlOption);
			});
		</cfoutput>
	}
</script>
