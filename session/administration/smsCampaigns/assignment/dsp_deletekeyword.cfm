<cfparam name="keywordId" default="-1">
<cfparam name="page" default="1">

<cfinclude template="../../../cfc/csc/constants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeAssignment" method="GetKeywordInformation" returnvariable="retKeywordInfo">
	<cfinvokeargument name="keywordId" value="#keywordId#">
</cfinvoke>

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div style="width: 100%">
	<div class="scrText">
		<cfoutput>
			You are about to delete #retKeywordInfo.AccountName#'s keyword #retKeywordInfo.Keyword# of short code #retKeywordInfo.ShortCode#. Would you like to continue?
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<input type="button" class="button filterButton small" id="btnNo" value="No" >
		<input type="button" class="button filterButton small" id="btnYes" value="Yes" >
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			$('#removeKeyword').dialog('close');
		});
		
		$('#btnYes').click(function(){
			<cfoutput>
			var data = { 
			     	keywordId : '#keywordId#'
			    };
			</cfoutput>
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'DeleteKeyword', data, "Delete keyword fail!", function(d ) {
				<cfoutput>
					var params = {
						'page':'#page#',
						'shortCodeRequestId' : '#retKeywordInfo.ShortCodeRequestId#'
					};
					post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeAssignmentDetail', params, 'POST');
				</cfoutput>
			}); 
		});
	});
</script>
