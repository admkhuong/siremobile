<cfparam name="shortCodeRequestId" default="-1">
<cfparam name="page" default="1">

<cfinclude template="../../../cfc/csc/constants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeAssignment" method="GetShortCodeInformation" returnvariable="retShortCodeInfo">
	<cfinvokeargument name="shortCodeRequestId" value="#shortCodeRequestId#">
</cfinvoke>

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div style="width: 100%">
	<cfoutput>
	<div class="scrText">
		You are about to terminate #retShortCodeInfo.AccountName#'s access to short code #retShortCodeInfo.ShortCode#. Would you like to continue?
	</div>
	</cfoutput>
	<div class="scrButtonBar">
		<input type="button" class="button filterButton small" id="btnNo" value="No" >
		<input type="button" class="button filterButton small" id="btnYes" value="Yes" >
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			$('#terminateShortCode').dialog('close');
		});
		
		$('#btnYes').click(function(){
			<cfoutput>
				var data = { 
					 	shortCodeRequestId : '#shortCodeRequestId#'
					};
			</cfoutput>
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeAssignment.cfc', 'TerminateShortCode', data, "terminate fail!", function(d ) {
				<cfoutput>
					var params = {
						'page':'#page#'
					};
					post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeAssignment', params, 'POST');
				</cfoutput>
			}); 
		});
	});
</script>
