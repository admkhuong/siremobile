﻿<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfparam name="carrierId" default="">

<cfimport taglib="../../../lib/grid" prefix="mb" />
<div style="width: 100%;text-align:center; padding-top: 10px;">
	Short Codes and their respective status
</div>
 <cfoutput>
	<cfset params = [{name="CarrierId", value="#carrierId#"}]>
	<!--- Prepare column data---->
	<cfset arrNames = ['Short Code', 'Carrier']>	
	<cfset arrColModel = [			
			{name='ShortCode', width='70%'},	
			{name='Carrier', width='30%'}] >
			
	<div style="text-align:center;margin-left:80px;">				
	 	<mb:table 
			component="#SessionPath#.cfc.smscampaigns.carrierManagement" 
			method="GetCarrierDetailsByCarrierId" 
			width="80%" 
			colNames= #arrNames# 
			colModels= #arrColModel#
			params = #params#
		>
		</mb:table>
 	</div> 
</cfoutput>
