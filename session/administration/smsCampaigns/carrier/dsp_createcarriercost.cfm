﻿<cfparam name="page" default="1">

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<cfinclude template="../../../cfc/csc/constants.cfm">

<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.formatCurrency-1.4.0.js" type="text/javascript" ></script>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>

<div style="width: 100%">
		<div class="scrText">
			<div class="item">
				<label>
					Title*
				</label>
				<div class="control">
					<input type="text" style="width:160px" id="inpTitle"  >
				</div>					
			</div>
			
			<div class="item">
				<label>
					Amount*
				</label>
				<div class="control">
					<input type="text" id="inpAmount" style="width:70px;" > format 0.00
				</div>					
			</div>
			
			<div class="item">
				<label>
					Type*
				</label>
				<div class="control">
					<cfoutput>
						<select id="inpCarrierCost" style="width:180px">
							<option value="#CARRIER_COST_VALUE#">#CARRIER_COST_DISPLAY#</option>
							<option value="#CARRIER_COST_SURCHARGE_VALUE#">#CARRIER_COST_SURCHARGE_DISPLAY#</option>
							<option value="#CARRIER_COST_CUSTOMER_VALUE#">#CARRIER_COST_CUSTOMER_DISPLAY#</option>
							<option value="#CARRIER_COST_CUSTOMER_SURCHARGE_VALUE#">#CARRIER_COST_CUSTOMER_SURCHARGE_DISPLAY#</option>
						</select>
					</cfoutput>
				</div>					
			</div>
		</div>
		<div class="scrButtonBar">
			<input type="button" class="button filterButton small" value="Cancel" onclick="closeDialog()">
			<input type="button" class="button filterButton small" value="Save" onclick="addNewCarrierCost()">
		</div>
	</div>

<script type="text/javascript">
	
	$(function(){
		initValue();
	});

	function initValue(){
		 $('#inpAmount').blur(function(){
		 	$('#inpAmount').formatCurrency();
		 });
	}	
	
	function addNewCarrierCost(){
		var title = $('#inpTitle').val();
		var amount = $('#inpAmount').val();
		var type = $('#inpCarrierCost option:selected').html();
		
		// validate carrier cost
		var isValid = validateCarrierCost(title, amount, type);
		
		if(isValid == true){
			var nextCarrierCostId = "carrierCost" + $('#carrierCostData table tr').length;
			
			var rowCarrierCost = '';
			rowCarrierCost = rowCarrierCost + '<tr class="carrierCost" id="' + nextCarrierCostId + '">';
			rowCarrierCost = rowCarrierCost + '<td>' + title + '</td>';
			rowCarrierCost = rowCarrierCost + '<td>' + amount + '</td>';
			rowCarrierCost = rowCarrierCost + '<td>' + type + '</td>';
			rowCarrierCost = rowCarrierCost + '<td>';
			amount = amount.replace(',', '');
			amount = amount.replace('$', '');
			rowCarrierCost = rowCarrierCost + '<img src="../../../../public/images/list_remove.png" onclick="DeleteRow(this); return false;">';
			rowCarrierCost = rowCarrierCost + '<input type="hidden" id="title" value="' + title + '" />';
			rowCarrierCost = rowCarrierCost + '<input type="hidden" id="amount" value="' + amount + '" />';
			rowCarrierCost = rowCarrierCost + '<input type="hidden" id="type" value="' + $("#inpCarrierCost option:selected").val() + '" />';
			rowCarrierCost = rowCarrierCost + '</td>';
			rowCarrierCost = rowCarrierCost + '</tr>';
			closeDialog();
			
			$('#carrierCostData table').append(rowCarrierCost);
			$('#addCostInformation').html('Add Additional Cost Information');
			$('#carrierCostData').show();
		}
	}
	
	function validateCarrierCost(title, amount, type){
		var isValid = true;
		if(title == ""){
			isValid = false; 
			jAlert("Title Carrier Cost is required.", "Failure!", function(result) {
			});
		}
		if(amount == ""){
			isValid = false;
			jAlert("Amount Carrier Cost is required.", "Failure!", function(result) { 
			});
		} 
		
		amount = amount.replace(',', '');
		amount = amount.replace('$', '');
		if(isNaN(amount)){
			isValid = false;
			jAlert("Amount Carrier Cost is not a number. Please input another one.", "Failure!", function(result) { 
			});
		} 
		return isValid;
	}
</script>
