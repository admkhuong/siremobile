﻿<cfparam name="CarrierName" default="">
<cfparam name="CarrierId" default="">

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div style="width: 100%">
	<div class="scrText" style="text-align: center;">
		<cfoutput>
			Would you like to delete <cfoutput>#CarrierName#</cfoutput> from the carrier list?
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<input type="button" class="button filterButton small" id="btnNo" value="Cancel" >
		<input type="button" class="button filterButton small" id="btnYes" value="Delete" >
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			newDialog.dialog('close');
		});
		
		$('#btnYes').click(function(){
			var data = { 
					     	CarrierId : <cfoutput>#CarrierId#</cfoutput>
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/carrierManagement.cfc', 'DeleteCarrier', data, "Delete Carrier fail!", function(d ) {
					jAlert("Carrier deleted", "Success!", function(result) { 
						//location.reload();
						newDialog.dialog('close');
						InitCarrierManager();
					});
				});	
		});
	});
</script>
