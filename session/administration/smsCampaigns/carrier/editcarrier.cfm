﻿<cfparam name="carrierId" default="">

<cfimport taglib="../../../lib/grid" prefix="mb" />
<cfinclude template="../../../cfc/csc/constants.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.carrierManagement" method="GetCarrierInformation" returnvariable="retCarrierInformation">
	<cfinvokeargument name="CarrierId" value="#CarrierId#">
</cfinvoke>
<div style="width: 100%">
	<div style="width: 40%; float: left; margin-left: 10px; font-size: 25px;">
		Edit Carrier
	</div>
</div>
<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/carrierManager.css" type="text/css" rel="stylesheet">
	<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
</cfoutput>
<div style="width: 100%">
	<cfoutput>
		<div class="carrierText">
			<div class="item">
				<label>
					Carrier Name*
				</label>
				<div class="control">
					<input type="text" id="inpCarrierName"  maxlength="255" value="#retCarrierInformation.CARRIERNAME#">
				</div>					
			</div>
			
			<div class="item">
				<label>
					Reseller
				</label>
				<div class="control">
					<input type="text" id="inpReseller" maxlength="255" value="#retCarrierInformation.RESELLER#"> 
				</div>					
			</div>
			
			<div class="item">
				<label>
					Aggregator*
				</label>
				<div class="control">
					<select id="inpAggregator" style="width:180px">
					</select>
				</div>					
			</div>
			
			<div class="item">
				<label>
					Est No. Subscribers
				</label>
				<div class="control">
					<input type="text" style="width: 160px;" id="inpEstSubscrebers" maxlength="255" value="#retCarrierInformation.ESTNOSUBCRIBERS#">
				</div>					
			</div>
			<div class="item">
				<label>
					Base Technology
				</label>
				<div class="control">
					<input type="text" id="inpBaseTechnology" maxlength="255" value="#retCarrierInformation.BASETECHNOLOGY#">
				</div>					
			</div>
			<div class="item">
				<label>
					MMS Support*
				</label>
				<div class="control">
					<input type="radio" name="inpMmsSupport" value="0" <cfif retCarrierInformation.MmsSupport EQ 0>checked="checked"</cfif>> Off
					<input type="radio" name="inpMmsSupport" value="1" <cfif retCarrierInformation.MmsSupport EQ 1>checked="checked"</cfif>> On
				</div>					
			</div>
			<div class="item">
				<label>
					Contact Notes
				</label>
				<div class="control">
					<textarea id="inpContactNotes" maxlength="1000" >#retCarrierInformation.CONTACTNOTES#</textarea>
				</div>					
			</div>
			<div class="item">
				<label>
					generic Notes
				</label>
				<div class="control">
					<textarea id="inpgenericNotes" maxlength="1000" >#retCarrierInformation.GENERICNOTES#</textarea>
				</div>					
			</div>
		
	</cfoutput>
		
		<div class="item">	
			<label>
				Approval ETA*
			</label>
			<div class="control">
				<input type="text" id="inpEtaValue" style="width: 20px;" value="#retCarrierInformation.APPROVALETA#" >
					<select id="inpEtaUnit" name="inpEtaUnit">
						<cfoutput>
							<option value="#APPROVE_ETA_UNIT_HOURS_VALUE#" <cfif retCarrierInformation.APPROVALETATYPE EQ APPROVE_ETA_UNIT_HOURS_VALUE>selected="true"</cfif> > #APPROVE_ETA_UNIT_HOURS_DISPLAY#</option>
							<option value="#APPROVE_ETA_UNIT_DAYS_VALUE#" <cfif retCarrierInformation.APPROVALETATYPE EQ APPROVE_ETA_UNIT_DAYS_VALUE> selected="true"</cfif> > #APPROVE_ETA_UNIT_DAYS_DISPLAY#</option>
							<option value="#APPROVE_ETA_UNIT_WEEKS_VALUE#" <cfif retCarrierInformation.APPROVALETATYPE EQ APPROVE_ETA_UNIT_WEEKS_VALUE> selected="true"</cfif> > #APPROVE_ETA_UNIT_WEEKS_DISPLAY#</option>
							<option value="#APPROVE_ETA_UNIT_MONTHS_VALUE#" <cfif retCarrierInformation.APPROVALETATYPE EQ APPROVE_ETA_UNIT_MONTHS_VALUE> selected="true"</cfif>> #APPROVE_ETA_UNIT_MONTHS_DISPLAY#</option>
							<option value="#APPROVE_ETA_UNIT_YEARS_VALUE#" <cfif retCarrierInformation.APPROVALETATYPE EQ APPROVE_ETA_UNIT_YEARS_VALUE> selected="true"</cfif>> #APPROVE_ETA_UNIT_YEARS_DISPLAY#</option>
						</cfoutput>
					</select>
			</div>					
		</div>
		
		<div class="item">	
			<label>
			</label>
			<div class="control">
				<div id="carrierCostData" style="display:none;">
					<table class="cf_table" width="600px" cellspacing="0" cellpadding="0">
						<tr>
							<th width="30%">Title</th>
							<th width="30%">Cost</th>
							<th width="30%">Type</th>
							<th width="10%"></th>
						</tr>
					</table>
				</div>
				<div style="width:450px">
					<a href="#" id="addCostInformation">Add Cost Information</a>
				</div>
			</div>					
		</div>
		
		<div class="item">	
			<label>
			</label>
			<div class="control">
				<span style="font-weight:bold">* Indecates a required field</span>
			</div>					
		</div>
	</div>
	<div class="carrierButtonBar">
		<input type="button" class="button filterButton small" value="Cancel" onclick="CancelEdit()">
		<input type="button" class="button filterButton small" value="Save" onclick="SubmitData()">
	</div>
</div>


<script type="text/javascript">
	$('#subTitleText').text('Edit Carrier');
	$('#mainTitleText').text('Edit Carrier');
	
	$(function(){
		initValue();
	});
	function initValue(){
		$('#inpEtaValue').spinner({min: 0});
		$('#inpEtaValue').spinner({max: 99});
		$('#inpEtaValue').spinner( "value", '<cfoutput>#retCarrierInformation.APPROVALETA#</cfoutput>' );
		$('#inpEtaValue').css( "width", "30px" );
		$('#inpEtaValue').ForceNumericOnly();
		
		var data = {
			carrierId:'<cfoutput>#carrierId#</cfoutput>'			
		};
		// get available aggregator
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/carrierManagement.cfc', 'GetAvailableAggregator', data, "Get Available Aggregator fail!", function(d ) {
			var agregatorArr = d.AGGREGATORARR;
			var aggregatorHtml = '';
			for(var i=0; i< agregatorArr.length; i++){
				var aggregator = agregatorArr[i];
				if('<cfoutput>#retCarrierInformation.AGGREGATORID#</cfoutput>' == aggregator.AGGREGATORID){
					aggregatorHtml = aggregatorHtml + '<option selected="true" value="' + aggregator.AGGREGATORID + '">' + aggregator.AGGREGATORNAME + '</option>';					
				}else{
					aggregatorHtml = aggregatorHtml + '<option value="' + aggregator.AGGREGATORID + '">' + aggregator.AGGREGATORNAME + '</option>';				
				}
			}
			
			$('#inpAggregator').append(aggregatorHtml);
		});
		
		$('#addCostInformation').click(function(){
			OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/carrier/dsp_createCarrierCost",
						 "Add Carrier Cost", 
						 330, 
						 500,
						 "",
						 false
						 );
			return false;
		});
		
		var carrierCostArr = <cfoutput>#retCarrierInformation.CarrierCostArr#</cfoutput>;
		if(carrierCostArr.length >0){
			$('#addCostInformation').html('Add Additional Cost Information');
			$('#carrierCostData').show();
			for(i=0; i<carrierCostArr.length; i++){
				var carrierCostItem = carrierCostArr[i];
				addCarrierCost(carrierCostItem.TITLE,carrierCostItem.AMOUNT,carrierCostItem.TYPE, carrierCostItem.TYPENAME);
			}
		}
	}
	
	function addCarrierCost(title, amount, type, typeName){
		var nextCarrierCostId = "carrierCost" + $('#carrierCostData table tr').length;
		
		var rowCarrierCost = '';
		rowCarrierCost = rowCarrierCost + '<tr class="carrierCost" id="' + nextCarrierCostId + '">';
		rowCarrierCost = rowCarrierCost + '<td>' + title + '</td>';
		rowCarrierCost = rowCarrierCost + '<td>$' + amount + '</td>';
		rowCarrierCost = rowCarrierCost + '<td>' + typeName + '</td>';
		rowCarrierCost = rowCarrierCost + '<td>';
		
		rowCarrierCost = rowCarrierCost + '<img src="../../../../public/images/list_remove.png" onclick="DeleteRow(this); return false;">';
		rowCarrierCost = rowCarrierCost + '<input type="hidden" id="title" value="' + title + '" />';
		rowCarrierCost = rowCarrierCost + '<input type="hidden" id="amount" value="' + amount + '" />';
		rowCarrierCost = rowCarrierCost + '<input type="hidden" id="type" value="' + type + '" />';
		rowCarrierCost = rowCarrierCost + '</td>';
		rowCarrierCost = rowCarrierCost + '</tr>';
		
		$('#carrierCostData table').append(rowCarrierCost);
	}
	
	function SubmitData(){
		var carrierName = $('#inpCarrierName').val();
		var reseller = $('#inpReseller').val();
		var aggregatorId = $('#inpAggregator').val();
		var estNoSubscriber = $('#inpEstSubscrebers').val();
		var baseTechnology =  $('#inpBaseTechnology').val();
		var mmsSupport = $('input[name=inpMmsSupport]:checked').val();
		var contactNotes = $('#inpContactNotes').val();
		var generictNotes = $('#inpgenericNotes').val();
		
		var approveEta = {};
		approveEta.Value = $('#inpEtaValue').val();
		approveEta.Unit = $('#inpEtaUnit').val();
		
		var costInfoArr = new Array();
		 $('.carrierCost').each(function( index ) {
		 	costInfoItem = {};
			costInfoItem.Title = $(this).find('#title').val();
			costInfoItem.Amount = $(this).find('#amount').val();
			costInfoItem.Type = $(this).find('#type').val();
			costInfoArr.push(costInfoItem);
		 });
		 
		 var data = {
		 	carrierId: '<cfoutput>#carrierId#</cfoutput>',
		 	carrierName: carrierName,
			reseller : reseller,
			aggregatorId:aggregatorId,
			estNoSubscriber:estNoSubscriber,
			baseTechnology:baseTechnology,
			mmsSupport:mmsSupport,
			contactNotes:contactNotes,
			generictNotes:generictNotes,
			approveEta:JSON.stringify(approveEta),
			costInformationArray : JSON.stringify(costInfoArr)
		 };

		// Submit data
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/carrierManagement.cfc', 'EditCarrier', data, "", function(d ) {
			location.href = '<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/carrier/CarrierManager'; 
		});
	}
	
	function DeleteRow(obj){
		$(obj).parent().parent().remove();
		var numberCostData = $('#carrierCostData table tr').length - 1;
		if(numberCostData <=0){
			$('#addCostInformation').html('Add Cost Information');
			$('#carrierCostData').hide();
		}
	}
	
	function CancelEdit(){
		location.href = "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/smscampaigns/carrier/CarrierManager";
	}
</script>

