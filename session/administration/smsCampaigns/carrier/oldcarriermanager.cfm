﻿<cfimport taglib="../../../lib/grid" prefix="mb" />
<div style="width: 100%">
	<div style="width: 40%; float: left; margin-left: 10px; font-size: 25px;">
		Carrier Manager
	</div>
	<div style="width: 20%; float: right;">
		<a class="button filterButton small"  style="float: right;" href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/carrier/CreateCarrier" >Add</a>
	</div>
</div>
 <cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="DetailsCarrier({%CarrierId%},''{%CarrierName%}''); return false;"> details </a>';
		htmlOption = htmlOption & '<a href="##" onclick="ModifyCarrier({%CarrierId%}); return false;"> modify </a>';
		htmlOption = htmlOption & '<a href="##" onclick="DeleteCarrier({%CarrierId%},''{%CarrierName%}''); return false;"> delete </a>';
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>
	<!--- Prepare column data---->
	<cfset arrNames = ['ID', 'Name', 'Aggregator', 'Options']>	
	<cfset arrColModel = [			
			{name='CarrierId', width='25%'},
			{name='CarrierName', width='25%'},	
			{name='AggregatorName', width='25%'},
			{name='FORMAT', width='25%', format = [htmlFormat]}] >		
	
 	<mb:table 
		component="#LocalSessionDotPath#.cfc.smscampaigns.carrierManagement" 
		method="GetCarrierList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		FilterSetId="DefaulFSListCarrier"
	>
	</mb:table>
</cfoutput>

<script type="text/javascript">
	$('#subTitleText').text('Carrier Manager');
	$('#mainTitleText').text('Carrier Manager');
	
	function DetailsCarrier(carrierId,carrierName) {
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/carrier/dsp_CarrierDetails?carrierId=" + carrierId, 
				carrierName + " Carrier Details", 
				400, 
				400,
				"",
				false);
		return false;
	}
	
	function ModifyCarrier(carrierId){
		location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/carrier/EditCarrier?CarrierId=' + carrierId;
		return false;
	}
	
	function DeleteCarrier(carrierId, carrierName){
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/carrier/dsp_deleteCarrier?carrierId=" + carrierId + "&carrierName=" + encodeURIComponent(carrierName), 
						"Delete Carrier", 
						250, 
						500,
						"",
						false
						);
		return false;
	}
</script>