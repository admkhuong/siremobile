﻿<script type="text/javascript">
	$("#mainTitleText").html("Administration");
	$("#subTitleText").html("Keyword management");
	function OpenUpdateKeyword(keywordid, statusid, Keyword, ShortCode){
		var title = Keyword + " @ " + ShortCode;
		OpenDialog("dsp_updateStatusKeyword?keywordid=" + keywordid + "&statusid=" + statusid, 
			title, 
			400, 
			400
		);
		return false;
	}
	
	
	<!---
	var CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;
	 function OpenUpdateKeyword(keywordid, statusid, OwnerName, Keyword){                                                          
        var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
                                                                                       
        var ParamStr = '';

        ParamStr = "?keywordid=" + keywordid + "&statusid=" + statusid + "&OwnerName=" + OwnerName+ "&Keyword=" + Keyword;
       
        <!--- Erase any existing dialog data --->
        if(CreateXMLContorlStringMasterTemplateDialogVoiceTools != 0)
        {
                        <!--- Cleanup and remove old stuff or tree won t work on second attempt to open same dialog --->
                       CreateXMLContorlStringMasterTemplateDialogVoiceTools.remove();
                       CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;
                     
       }
                                                                     
       CreateXMLContorlStringMasterTemplateDialogVoiceTools = $('<div></div>').append($loading.clone());
     
       CreateXMLContorlStringMasterTemplateDialogVoiceTools
                       .load('dsp_updateStatusKeyword' + ParamStr)
                       .dialog({
                                       modal : true,
                                       close: function() { CreateXMLContorlStringMasterTemplateDialogVoiceTools.remove(); CreateXMLContorlStringMasterTemplateDialogVoiceTools = 0;},
                                       title: 'Master Template Add - XML Control String',
                                       width: 600,
                           resizable: false,
                                       height: 'auto',
                                       position: 'top',
                                       draggable: false,
                                       dialogClass: 'EBMDialog',
                                       beforeClose: function(event, ui) {            }
                   }).parent().draggable();
   return false;                      
           }
	--->
	
	function UpdateKeywordStatus(keywordid){
		var keywordStatus = $("input[type='radio'][name='KeywordStatus']:checked").val();
		var data = {
			    	keywordId : keywordid,
					statusId : keywordStatus
				    };	
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/keywordManagement.cfc', 'UpdateKeywordStatus', data, "Update keyword status fail!", function(d ) {
			jAlert("Updated keyword status", "Success!", function(result) { 
				// location.reload();
				InitKeyword();
				closeDialog(); return false;
			});
		});	
	}
</script>