﻿<cfimport taglib="../../../lib/grid" prefix="mb" />
<cfinclude template="KeywordManagerJS.cfm">
<cfparam name="page" default="1">
<cfparam name="query" default="">
<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.keywordManagement" method="GetStatusList" returnvariable="GetStatusList">
<cfset StatusArray = ArrayNew(1)>
<cfoutput>
	<cfloop array="#GetStatusList.ROWS#" index="StatusIndex">
			<cfset ArrayAppend(StatusArray, {value = StatusIndex.Active_int, display = StatusIndex.Desc_vch})>
	</cfloop>
</cfoutput>

<cfoutput>
	<style TYPE="text/css">
		@import url('#rootUrl#/#PublicPath#/css/jquery.jgrowl.css');
		@import url('#rootUrl#/#PublicPath#/css/utility.css');
		
		
	</style>
</cfoutput>

<style>
	label {
	    display: inline!important;
	}
		
	form .ui-corner-all, #SurveyContent .ui-corner-all {
	    border: medium none;
	    border-radius: 6px;
	    box-shadow: none;
	    color: #000000;
	    font-weight: normal;
	    height: 28px;
	    line-height: 20px;
	    margin-right: 0;
	    padding: 0;
	}
	
	div.ui-dialog div.ui-dialog-content{
		height: 250px!important;
	}
</style>

 <cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="OpenUpdateKeyword({%KeywordId%}, {%Active%}, ''{%Keyword%}'', ''{%ShortCode%}''); return false;"> update </a>';
	</cfscript>
	
	<cfset htmlFormat = {
			name ='normal',
			html = '#htmlOption#'	
	}>	
	<!--- Prepare column data---->
	<cfset arrNames = ['Keyword', 'Short Code', 'OwnerID', 'Owner Name', 'Status', 'Created', 'Options']>	

	<cfset arrColModel = [			
			{name='Keyword', width='24%', sortObject= {isDefault='false', sortType="ASC", sortColName ='k.Keyword_vch'}},
			{name='ShortCode', width='14%', sortObject= {isDefault='false', sortType="DESC", sortColName ='sc.ShortCode_vch'}},
			{name='RequesterId', width='10%', sortObject= {isDefault='false', sortType="DESC", sortColName ='scr.RequesterId_int'}},			
			{name='OwnerName', width='14%', sortObject= {isDefault='false', sortType="DESC", sortColName ='ua.UserName_vch'}},
			{name='Status', width='14%', sortObject= {isDefault='false', sortType="DESC", sortColName ='k.Active_int'}},
			{name='Created', width='14%', sortObject= {isDefault='true', sortType="DESC", sortColName ='k.Created_dt'}},
			{name='Format', width="8%", format = [htmlFormat]}
			] >
	<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='Keyword', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='2', DISPLAY='Short Code', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Owner ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='4', DISPLAY='Owner Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='5', DISPLAY='Status', TYPE='CF_SQL_INTEGER', FILTERTYPE="LIST", LISTOFVALUES = StatusArray},
			{VALUE='6', DISPLAY='Created', TYPE='CF_SQL_DATE', FILTERTYPE="DATE", EXCEPTIONCASE="Never"}
		]
	>
	<cfset 
		filterFields = [
			'k.Keyword_vch',
			'sc.ShortCode_vch',
			'scr.RequesterId_int',
			'ua.UserName_vch',
			'k.Active_int',
			"EXTRACTVALUE(XMLControlString_vch, '//EXP //@DATE')"
		]	
	>
 	<mb:table 
		component="#LocalSessionDotPath#.cfc.smscampaigns.keywordManagement" 
		method="GetKeywordList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
	   	FilterSetId="DefaulFSListKeyword"
	>
	</mb:table>
</cfoutput>