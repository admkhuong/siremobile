<cfparam name="shortCodeRequestId" default="">
<cfparam name="page" default="1">

<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeRequest" method="GetShortCodeRequestDetails" returnvariable="retShortCodeRequestDetails">
	<cfinvokeargument name="shortCodeRequestId" value="#shortCodeRequestId#">
</cfinvoke>


<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>

<div style="width: 100%">

	<div class="scrText">
		<cfoutput>
			Would you like to revoke #retShortCodeRequestDetails.REQUESTERNAME#'s request to add #retShortCodeRequestDetails.ShortCode# to their account?
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<a href="#" class="button filterButton small" id="btnCancel">Cancel</a>
		<a href="#" class="button filterButton small" id="btnRevoke">Revoke</a>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnCancel').click(function(){
			$('#revokeShortCode').dialog('close');
			return false;
		});
		
		$('#btnRevoke').click(function(){
			<cfoutput>
			var data = { 
			     	shortCodeRequestId : '#shortCodeRequestId#',
			     	processType : 2 <!--- for revoke processType=1 --->
			    };
			</cfoutput>
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeRequest.cfc', 'ProcessShortCodeRequest', data, "Revoke fail!", function(d ) {
				<cfoutput>
					var params = {
						'page':'#page#'
					};
					post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeRequest', params, 'POST');
				</cfoutput>
			});
			return false;
		});
	});
</script>
