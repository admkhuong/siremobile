<cfparam name="shortCodeRequestId" default="">
<cfparam name="page" default="1">

<cfinclude template="../../../cfc/csc/constants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeRequest" method="GetShortCodeRequestDetails" returnvariable="retShortCodeRequestDetails">
	<cfinvokeargument name="shortCodeRequestId" value="#shortCodeRequestId#">
</cfinvoke>

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<!--- Get short code request information --->
<cfoutput>
	<div style="width: 100%">
	
		<div class="scrText">
			<div>
				Short Code: #retShortCodeRequestDetails.ShortCode#
			</div>
			<div>
				Classification: #retShortCodeRequestDetails.CLASSIFICATION#
			</div>
			<div>
				Requester's Name: #retShortCodeRequestDetails.REQUESTERNAME#
			</div>
			<div>
				Requester's ID: #retShortCodeRequestDetails.REQUESTERID#
			</div>
			<div>
				Request Date: #retShortCodeRequestDetails.REQUESTDATE#
			</div>
			<cfif retShortCodeRequestDetails.KeywordQuantity NEQ -1>
				<div>
					Total Keywords Requested: #retShortCodeRequestDetails.KeywordQuantity#
				</div>
				<div>
					Total Volume Expected: #retShortCodeRequestDetails.VolumeExpected#
				</div>
			</cfif>
		</div>
		
		<div class="scrButtonBar">
			<a href="##" class="button filterButton small" id="btnGrant">Grant</a>
			<a href="##" class="button filterButton small" id="btnRevoke">Revoke</a>
		</div>
	</div>
</cfoutput>

<script type="text/javascript">
	$(document).ready(function(){
		
		$('#btnGrant').click(function(){
			<cfoutput>
			var data = { 
			     	shortCodeRequestId : '#shortCodeRequestId#',
			     	processType : 1 <!--- for grant processType=1 --->
			    };
			</cfoutput>
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeRequest.cfc', 'ProcessShortCodeRequest', data, "Grant fail!", function(d ) {
				<cfoutput>
					var params = {
						'page':'#page#'
					};
					post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeRequest', params, 'POST');
				</cfoutput>
			});
			return false;
		});
		
		$('#btnRevoke').click(function(){
			<cfoutput>
			var data = { 
			     	shortCodeRequestId : '#shortCodeRequestId#',
			     	processType : 2 <!--- for revoke processType=1 --->
			    };
			</cfoutput>
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/smscampaigns/shortCodeRequest.cfc', 'ProcessShortCodeRequest', data, "Revoke fail!", function(d ) {
				<cfoutput>
					var params = {
						'page':'#page#'
					};
					post_to_url('#rootUrl#/#sessionPath#/Administration/smscampaigns/shortCodeRequest', params, 'POST');
				</cfoutput>
			});
			return false;
		});
	});
</script>
