<cfparam name="shortCodeRequestId" default="1">
<cfparam name="page" default="1">

<cfinclude template="../../cfc/csc/constants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeAssignment" method="GetShortCodeInformation" returnvariable="retShortCodeInfo">
	<cfinvokeargument name="shortCodeRequestId" value="#shortCodeRequestId#">
</cfinvoke>
<cfimport taglib="../../lib/grid" prefix="mb" />

<cfoutput>
	<div style="width: 100%">
		<div style="width: 40%; margin-left: 10px; font-size: 25px;">
			Short Code Keywords for #retShortCodeInfo.ShortCode#
		</div>
		<div style="width: 40%; margin-left: 10px; font-size: 18px;">
			Account Name: #retShortCodeInfo.AccountName#
		</div>
	</div>
</cfoutput>
 <cfoutput>
	<cfscript>
		htmlOption = '<a href="##" onclick="RemoveKeywordFromShortCode({%KeywordId%}); return false;"> remove </a>';
	</cfscript>
	
	<cfset htmlFormat = {
				name ='normal',
				html = '#htmlOption#'
			}>

	<!--- Prepare column data---->
	<cfset arrNames = ['Keyword', 'Response', 'Total Received', 'Options']>	
	<cfset arrColModel = [			
			{name='Keyword', width='25%'},
			{name='Response', width='25%'},	
			{name='TotalReceived', width='25%'},
			{name='FORMAT', width='25%', format = [htmlFormat]}] >		

	<cfset arrParams = [{name='shortCodeRequestId',value="#shortCodeRequestId#"}] />
	
 	<mb:table 
		component="#LocalSessionDotPath#.cfc.smscampaigns.shortCodeAssignment" 
		method="GetKeywordFromShortCodeList" 
		width="100%" 
		colNames= #arrNames# 
		colModels= #arrColModel#
		params="#arrParams#"
	>
	</mb:table>
</cfoutput>

<script type="text/javascript">
	$('#subTitleText').text('Short Code Request Manager');
	$('#mainTitleText').text('Short Code Request Manager');
	
	function RemoveKeywordFromShortCode(keywordId) {
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/assignment/dsp_deleteKeyword?keywordId=" + keywordId, 
						"Delete Keyword", 
						280, 
						500,
						"removeKeyword",
						false
						);
		return false;
	}
	
</script>