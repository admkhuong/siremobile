<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<style>

.EBMDialog .inputbox-container 
{
	display:block !important;	
	width: 368px !important;
	
}

</style>

<cfinclude template="../../../cfc/csc/constants.cfm">
<cfparam name="shortCodeId" default="">
<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div class="sms_full_width">
	
	<br/>
	<div class="sms_popup_header">
		<cfif shortCodeId EQ "">
			Add a shortcode to be used by users
		<cfelse>
			Click save to save your changes.
		</cfif>
	</div>
	<div class="sms_popup_row_first">
		<div class="sms_popup_row_label">
			Short Code
		</div>
		<div class="sms_popup_row_control">
			<input type="text" class="sms_text_width" id="txtShortCode" maxlength="50">
		</div>
		<div class="sms_popup_row_control1">
			<select id="selectClassification" onchange="ChangeClassification()">
	            <option value="<cfoutput>#CLASSIFICATION_SIRE_SHARED_VALUE#</cfoutput>"><cfoutput>#CLASSIFICATION_SIRE_SHARED_DISPLAY#</cfoutput></option>
				<option value="<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>"><cfoutput>#CLASSIFICATION_PUBLIC_SHARED_DISPLAY#</cfoutput></option>
				<option value="<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>"><cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_DISPLAY#</cfoutput></option>
				<option value="<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>"><cfoutput>#CLASSIFICATION_PRIVATE_DISPLAY#</cfoutput></option>				
			</select>
		</div>		
	</div>
	
	<div class="sms_popup_row" id="divAccountInformation">
		<div class="sms_popup_row_label">
			Account Type
		</div>
		<div class="sms_popup_row_control1">
			<select id="selectAccountType">
				<option value="<cfoutput>#OWNER_TYPE_COMPANY#</cfoutput>">Company</option>
				<option value="<cfoutput>#OWNER_TYPE_USER#</cfoutput>">User</option>
			</select>
		</div>
		<div class="sms_popup_row_label1">
			Owner ID
		</div>
		<div class="sms_popup_row_control2">
			<input type="text" class="sms_text_width" id="txtOwnerId">
		</div>
        
        <BR />
        
       
		
	</div>
    
    <div style="margin-left:25px; margin-top:35px;" class="EBMDialog">
    
	    <div class="inputbox-container" style="display:block;">
            <label for="selectAggregator">Preferred Aggregator</label> 
                       
            <cfinvoke component="#LocalSessionDotPath#.cfc.csc.csc" method="GetAggregatorList" returnvariable="RetVarGetAggregatorList">
                       			
            <select id="selectAggregator" style="width:273px; display:block;">
            	
                <cfloop from="1" to="#arrayLen(RetVarGetAggregatorList.ROWS)#" index="AggIndex">
                
                	<cfoutput><option value="#RetVarGetAggregatorList.ROWS[AggIndex].AGGREGATORID#" selected="selected">#HTMLCodeFormat(RetVarGetAggregatorList.ROWS[AggIndex].NAME)#</option></cfoutput>
                
                </cfloop>
             
            	<!--- DB driven from cfc lookup query --->
              	<!---  <option value="1" selected="selected">MBlox</option>
                <option value="2">AT&amp;T</option>
                <option value="3">ALLTEL</option>
				--->
            </select>
        </div>
                    
        <div class="inputbox-container" style="display:block;">
            <label for="txtServiceIdOne">Custom Service Id 1 <span class="small">Optional</span></label>            
            <input id="txtServiceIdOne" name="txtServiceIdOne" placeholder="Enter Custome Service Id Here" size="40"  style="display:block;" title="Service ID for MBLOX is 1 for all providers if we DO NOT know the carrier up front"/>
        </div>
                
        <div class="inputbox-container"  style="display:block;">
            <label for="txtServiceIdTwo">Custom Service Id 2 <span class="small">Optional</span></label>            
            <input id="txtServiceIdTwo" name="txtServiceIdTwo" placeholder="Enter Custome Service Id Here" size="40"  style="display:block;" title="MBLOX TMobile if we know Carrier"/>
        </div>
                
        <div class="inputbox-container"  style="display:block;">
            <label for="txtServiceIdThree">Custom Service Id 3 <span class="small">Optional</span></label>            
            <input id="txtServiceIdThree" name="txtServiceIdThree" placeholder="Enter Custome Service Id Here" size="40"  style="display:block;" title="MBLOX Verizon if we know Carrier"/>
        </div>
                        
        <div class="inputbox-container"  style="display:block;">
            <label for="txtServiceIdFour">Custom Service Id 4 <span class="small">Optional</span></label>            
            <input id="txtServiceIdFour" name="txtServiceIdFour" placeholder="Enter Custome Service Id Here" size="40"  style="display:block;"/>
        </div>
                 
        <div class="inputbox-container"  style="display:block;">
            <label for="txtServiceIdFive">Custom Service Id 5 <span class="small">Optional</span></label>            
            <input id="txtServiceIdFive" name="txtServiceIdFive" placeholder="Enter Custome Service Id Here" size="40"  style="display:block;"/>
        </div>
    
    </div>                        
                       
	<div class="sms_popup_action">
		<a href="#" class="button filterButton small" onclick="closeDialog(); return false;" id="btnCancel">Cancel</a>
		<a href="#" class="button filterButton small" onclick="Save()" id="btnSave">Save</a>
	</div>	
</div>

<script type="text/javascript">
	$(function() {
		InitData();
		ChangeClassification()
	});
	function Save() {
		var shortCode = $.trim($('#txtShortCode').val()).replace(new RegExp(" ", "g"), '');
		var classificationId = $.trim($('#selectClassification').val());
		var ownerType = $.trim($('#selectAccountType').val());
		var ownerId = $.trim($('#txtOwnerId').val());

		var isValid = Validate(shortCode, classificationId, ownerId);
		
		if (isValid) {
			if (ownerId == '') {
				ownerId = '<cfoutput>#DEFAULT_ACCOUNT_ID#</cfoutput>';
				
				if (classificationId != '<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>' && classificationId != '<cfoutput>#CLASSIFICATION_SIRE_SHARED_VALUE#</cfoutput>') {
					jQuery.alerts.okButton = 'Yes';
					jQuery.alerts.cancelButton = 'No'; 
					
					jConfirm("You have not assigned this Public (not share) Short Code to any company/user. Please confirm to save?", "Confirm", function(result) { 
						if (result)
						{	
							SaveSCData(shortCode, classificationId, ownerId, ownerType);
						}});
				}
				else {
					SaveSCData(shortCode, classificationId, ownerId, ownerType);
				}
			}
			
			else {
				SaveSCData(shortCode, classificationId, ownerId, ownerType);	
			}	
			
			
		}
	}
	
	function SaveSCData(shortCode, classificationId, ownerId, ownerType) {
		if ('<cfoutput>#shortCodeId#</cfoutput>' == '') {
				var validationData = { 
				     	ShortCode : shortCode
				    };
			}
			else {
				var validationData = { 
				     	ShortCode : shortCode,
				     	shortCodeId: '<cfoutput>#shortCodeId#</cfoutput>'
				    };						
			}
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetShortCode', validationData, "Get Short Code fail!", function(d ) {
				if (d.ISEXISTS != 1) {
					var data = { 
							ClassificationId: classificationId,
					     	OwnerId: ownerId,
					     	OwnerType: ownerType							
					    };
					    		
					ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetOwnerName', data, "Get owner name fail!", function(d ) {
						if (d.OWNERNAME == '' && '<cfoutput>#shortCodeId#</cfoutput>' != '') {
							jAlert("Account ID could not be found", "Failure!", function(result) { });
						}
						else {
							SaveData(shortCode, classificationId, ownerId, ownerType);
						}
					});	
				}
				else {
					jAlert("This shortcode is existing! cannot add", "Failure!", function(result) { });
				}
			});
	}
	
	function SaveData(shortCode, classificationId, ownerId, ownerType) {
		var tmpOwnerId = 1;
		if (classificationId != '<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>' && classificationId != '<cfoutput>#CLASSIFICATION_SIRE_SHARED_VALUE#</cfoutput>') {
			var tmpOwnerId = ownerId;
		}

		if ('<cfoutput>#shortCodeId#</cfoutput>' == '') {
			var data = { 
		     	ShortCode : shortCode,
		     	Classification : classificationId,
		     	OwnerId: tmpOwnerId,
		     	OwnerType: ownerType,
				selectAggregator: $('#selectAggregator').val(),
				CustomServiceId1: $.trim($('#txtServiceIdOne').val()),
				CustomServiceId2: $.trim($('#txtServiceIdTwo').val()),
				CustomServiceId3: $.trim($('#txtServiceIdThree').val()),
				CustomServiceId4: $.trim($('#txtServiceIdFour').val()),
				CustomServiceId5: $.trim($('#txtServiceIdFive').val())			     	
		    };
		    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'AddShortCode', data, "Add Short Code fail!", function(d ) {
				jAlert("Short Code added", "Success!", function(result) { 
					location.reload();
				});
			});
		}
		else {
			var data = { 
		     	ShortCodeId : '<cfoutput>#shortCodeId#</cfoutput>',				
		     	ShortCode : shortCode,
		     	Classification : classificationId,
		     	OwnerId: tmpOwnerId,
		     	OwnerType: ownerType,
				selectAggregator: $('#selectAggregator').val(),
				CustomServiceId1: $.trim($('#txtServiceIdOne').val()),
				CustomServiceId2: $.trim($('#txtServiceIdTwo').val()),
				CustomServiceId3: $.trim($('#txtServiceIdThree').val()),
				CustomServiceId4: $.trim($('#txtServiceIdFour').val()),
				CustomServiceId5: $.trim($('#txtServiceIdFive').val())
		    };
		    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'UpdateShortCode', data, "Update Short Code fail!", function(d ) {
				jAlert("Short Code updated", "Success!", function(result) { 
					location.reload();
				});
			});						
		}	
	}
	function Validate(shortCode, classificationId, ownerId) {
		<!---if (!IsNumber(shortCode)) {
			jAlert("Short Code must be a number", "Failure!", function(result) { } );	
			return false;
		}
		else {
			var shortCode = parseInt($('#txtShortCode').val());
			var maxValue = parseInt('<cfoutput>#MAXINTEGERVALUE#</cfoutput>');
			if (shortCode < 0 || shortCode > maxValue) {
				jAlert("Short Code must be in range 0-<cfoutput>#MAXINTEGERVALUE#</cfoutput>", "Failure!", function(result) { } );					
				return false;
			}
		}--->
		
		if (classificationId == '<cfoutput>#CLASSIFICATION_PRIVATE_VALUE#</cfoutput>') {
			if (!IsNumber(ownerId)) {
				jAlert("Owner Id must be a number", "Failure!", function(result) { } );	
				return false;
			}
			else {
				var ownerId = parseInt($('#txtOwnerId').val());
				var maxValue = parseInt('<cfoutput>#MAXINTEGERVALUE#</cfoutput>');
				if (ownerId < 0 || ownerId > maxValue) {
					jAlert("Owner Id must be in range 0-<cfoutput>#MAXINTEGERVALUE#</cfoutput>", "Failure!", function(result) { } );					
					return false;
				}
			}
		}
		else if (classificationId == '<cfoutput>#CLASSIFICATION_PUBLIC_NOTSHARED_VALUE#</cfoutput>') {
			if (ownerId != '') {
				if (!IsNumber(ownerId)) {
					jAlert("Owner Id must be a number", "Failure!", function(result) { } );	
					return false;
				}
				else {
					var ownerId = parseInt($('#txtOwnerId').val());
					var maxValue = parseInt('<cfoutput>#MAXINTEGERVALUE#</cfoutput>');
					if (ownerId < 0 || ownerId > maxValue) {
						jAlert("Owner Id must be in range 0-<cfoutput>#MAXINTEGERVALUE#</cfoutput>", "Failure!", function(result) { } );					
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	function InitData() {
		<cfif shortCodeId NEQ "">
			var data = { 
			     	ShortCodeId : '<cfoutput>#shortCodeId#</cfoutput>'
			    };
			    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetShortCodeById', data, "Get Short Code fail!", function(d ) {
				$('#txtShortCode').val(d.SHORTCODE);
		     	$('#selectClassification').val(d.CLASSIFICATION);
		     	if (d.OWNERID != '<cfoutput>#DEFAULT_ACCOUNT_ID#</cfoutput>') {
			     	$('#txtOwnerId').val(d.OWNERID);
		     	}
		     	$('#selectAccountType').val(d.OWNERTYPE)
				$('#txtServiceIdOne').val(d.CUSTOMSERVICEID1)
				$('#txtServiceIdTwo').val(d.CUSTOMSERVICEID2)
				$('#txtServiceIdThree').val(d.CUSTOMSERVICEID3)
				$('#txtServiceIdFour').val(d.CUSTOMSERVICEID4)
				$('#txtServiceIdFive').val(d.CUSTOMSERVICEID5)				
				$('#selectAggregator').val(d.PREFERREDAGGREGATOR)
								
			});	
			
			 $('#txtShortCode').attr("disabled", "disabled");	
			 $('#selectClassification').attr("disabled", "disabled");					 
				 
		</cfif>	
	}
	
	function ChangeClassification() {
		var value = $('#selectClassification').val();
		if (value != '<cfoutput>#CLASSIFICATION_PUBLIC_SHARED_VALUE#</cfoutput>' && value != '<cfoutput>#CLASSIFICATION_SIRE_SHARED_VALUE#</cfoutput>') {
			$('#divAccountInformation').show();
		}
		else {
			$('#divAccountInformation').hide();	
		}
	}
</script>
