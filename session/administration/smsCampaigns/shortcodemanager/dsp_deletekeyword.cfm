﻿<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfparam name="KeywordId" default="">
<cfparam name="Keyword" default="">
<cfparam name="ShortCodeId" default="">
<!---<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>--->
<div class="sms_full_width">
	<div class="scrText" style="text-align: center;">
		<cfoutput>
			Click yes to confirm </br></br> Delete keyword<cfoutput> #Keyword#</cfoutput>? These changes cannot be undone!
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<a href="#" class="button filterButton small" id="btnNoDeleteForm" onclick="closeDialog()" >No</a>
		<a href="#" class="button filterButton small" id="btnYesDeleteForm" onclick="DeleteKeywordScript()">Yes</a>
	</div>
</div>

<script type="text/javascript">
//	$(document).ready(function(){
//		$('#btnNoDeleteForm').click(function(){
//			newDialog.dialog('close');
//		});
//		
//		$('#btnYesDeleteForm').click(function(){
//				
//		});
//	});
	
	function DeleteKeywordScript(){
		var data = { 
				     	KeywordId : <cfoutput>#KeywordId#</cfoutput>
				    };
					    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'DeleteKeyword', data, "Delete keyword fail!", function(d ) {
				jAlert("Keyword has been deleted", "Success!", function(result) { 
					closeDialog();
					$.ajax({
						url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/dsp_publicResponses?ShortCodeId=" + "<cfoutput>#ShortCodeId#</cfoutput>",
						cache: false
					}).done(function( html ) {
						$("#dvPublicResponse").html(html);
					});
				});
			});
	}
</script>