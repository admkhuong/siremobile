<cfparam name="ShortCodeId" default="">
<cfparam name="shortCode" default="">

<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" type="text/css" rel="stylesheet">
</cfoutput>
<div class="sms_full_width">
	<div class="scrText" style="text-align: center;">
		<cfoutput>
			Click yes to confirm </br></br> Delete short code <cfoutput>#shortCode#</cfoutput>? These changes cannot be undone!
		</cfoutput>
	</div>
	<div class="scrButtonBar">
		<a href="#" class="button filterButton small" id="btnNo">No</a>
		<a href="#" class="button filterButton small" id="btnYes">Yes</a>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btnNo').click(function(){
			newDialog.dialog('close');
		});
		
		$('#btnYes').click(function(){
			var data = { 
					     	ShortCodeId : <cfoutput>#ShortCodeId#</cfoutput>
					    };
					    		
				ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'DeleteShortCode', data, "Delete Short Code fail!", function(d ) {
					jAlert("Short Code deleted", "Success!", function(result) { 
						location.reload();
					});
				});	
		});
	});
</script>
