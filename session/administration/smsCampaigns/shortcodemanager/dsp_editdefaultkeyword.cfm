﻿<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfinclude template="../../../cfc/csc/constants.cfm">
<cfparam name="KeywordId" default="">
<cfparam name="Keyword" default="">
<cfparam name="Response" default="">
<cfparam name="ToolTip" default="">
<cfparam name="ShortCodeId" default="">
<div style="width: 100%; margin:20px;">
	<div style="width: 100%; text-align: center;">
		Please enter the following information for keyword <cfoutput>#Keyword#</cfoutput>.
	</div>
	<div class="sms_popup_row_first">
		<div class="sms_popup_row_label">
			Keyword
		</div>
		<div class="sms_popup_row_control_keyword">
			<input type="text" class="sms_text_width" id="txtKeyword1" maxlength='160'>
		</div>	
	</div>
	<div class="sms_popup_row">
		<div class="sms_popup_row_label">
			Response
		</div>
		<div class="sms_popup_row_control_keyword">
			<textarea type="text" class="sms_text_width" id="txtResponse1" row="5" maxlength='480'></textarea> <br>
			<label class="sms_remaining_chars_keyword" id="lblRemainChars1" >160/160 characters</label>
		</div>	
	</div>
	<div class="sms_popup_row">
		<div class="sms_popup_row_label">
			ToolTip
		</div>
		<div class="sms_popup_row_control_keyword">
			<textarea type="text" class="sms_text_width" id="txtToolTip1" row="5" ></textarea> <br>
		</div>	
	</div>
	<div class="sms_popup_action">

		<a class="button filterButton small" onclick ="closeEditDefaultKeywordForm()" href ="##" >Cancel</a>
		<a class="button filterButton small" onclick ="SaveKeyword()" href ="##" >Save</a>
	</div>	
</div>

<script type="text/javascript">
	InitDataEditDefaultKeywordForm();
	
	$('textarea[maxlength]').keyup(function(){  
        //get the limit from maxlength attribute  
        var limit = parseInt($(this).attr('maxlength'));  
        //get the current text inside the textarea  
        var text = $(this).val();  
        //count the number of characters in the text  
        var chars = text.length;  
  
        //check if there are more characters then allowed  
        if(chars > limit){  
            //and if there are use substr to get the text before the limit  
            var new_text = text.substr(0, limit);  
  
            //and change the current text with the new text  
            $(this).val(new_text);  
        }  
    });  
    $('#txtResponse1').bind('cut copy paste', function (e) {
		ViewRemainChars1();
	});
    $('#txtResponse1').keyup(function(){  
        ViewRemainChars1();
    });

	function ViewRemainChars1() {
		var value = $('#txtResponse1').val().length + '/160 characters';
		$('#lblRemainChars1').text(value);	
	}	
	
	function SaveKeyword(){
		if(validateDefaultKeyword1()==false)return;
		var data = { 
					keywordId: '<cfoutput>#keywordId#</cfoutput>',
			     	Keyword: $('#txtKeyword1').val(),
			     	Response: $('#txtResponse1').val(),
					ToolTip: $('#txtToolTip1').val(),
					ShortCodeId: '<cfoutput>#ShortCodeId#</cfoutput>'
			    };
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'UpdateDefaultKeyword', data, "Update Keyword fail!", function(d ) {
			jAlert("Keyword updated", "Success!", function(result) { 
				$.ajax({
					url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/dsp_publicResponses?ShortCodeId=" + '<cfoutput>#ShortCodeId#</cfoutput>',
					cache: false
				}).done(function( html ) {
					$("#dvPublicResponse").html(html);
				});
				closeEditDefaultKeywordForm();
			});
		});		
	}
	function InitDataEditDefaultKeywordForm() {
		<cfif KeywordId NEQ "">
			var data = { 
			     	KeywordId : '<cfoutput>#KeywordId#</cfoutput>'
			    };
			    		
			ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'GetKeyword', data, "Get keyword fail!", function(d ) {
				$('#txtKeyword1').val(d.KEYWORD);
		     	$('#txtResponse1').val(d.RESPONSE);
				$('#txtToolTip1').val(d.TOOLTIP);
		     	ViewRemainChars1();
			});		
		</cfif>	
	}
	
	//named validateDefaultKeyword1 to distinct from validateDefaultKeyword in parent dialog
	function validateDefaultKeyword1(){
		if ($.trim($('#txtKeyword1').val()) == '') {
			jAlert("Keyword cannot be empty", "Failure!", function(result){
			});
			return false;
		}
		return true;
	}
	
	function closeEditDefaultKeywordForm(){
		var editDefaultKeywordDialog = $('#editDefaultKeyword');
		editDefaultKeywordDialog.dialog('close');
	}
	
</script>
