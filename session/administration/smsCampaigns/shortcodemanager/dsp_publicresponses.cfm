﻿<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>
<cfinclude template="../../../cfc/csc/constants.cfm">
<cfparam name="shortCodeId" default="">
<cfoutput>
	<link href="#rootUrl#/#PublicPath#/css/administrator/smscampaigns/shortcoderequest.css" 
	      type="text/css" rel="stylesheet">
</cfoutput>
<cfimport taglib="../../../lib/grid" prefix="mb"/>

<div class="sms_full_width" id="dvPublicResponse">

<br/>
<div class="sms_popup_header">
	<cfif shortCodeId EQ "">
		Add a shortcode to be used by users
	<cfelse>
		Please enter public short code keywords that will be added to EBM users.
	</cfif>
</div>
<div>
	<cfoutput>
	
		<cfscript>
			htmlOption = '<a href="##" onclick="EditKeyword({%KeywordId%},{%shortCodeId%})">Edit</a>';
			htmlOption = htmlOption & '<a class="sms_padding_left5" href="##" onclick="DeleteKeyword({%KeywordId%},{%KeywordWithoutImage%},{%ShortCodeId%})">Delete</a>';
		</cfscript>
		
		<cfset htmlFormat = {name='normal', html='#htmlOption#'}>
		<cfset arrNames = ['Keyword', 'Response', 'Options']>
		<cfset arrColModel = [	{name='Keyword', width='30%', isHtmlEncode=false}, 
								{name='Response', width='30%'},
								{name ='Format',width='40%', format = [htmlFormat] }]>
		<div class="item" style="height:200px;  overflow-x:auto; margin-top:0px;" >
			<mb:table 
				component="#SessionPath#.cfc.csc.csc" 
				method="GetKeywordListByShortCode" 
				width="100%" 
			  	colnames=#arrNames# 
			  	colmodels=#arrColModel# 
			  	IsPaging = false>
			</mb:table>
		</div>
	</cfoutput>
	
</div>
<table cellspacing="8" width = "100%" style="outline:gray solid thin">
	<tr>
		<td >
			Keyword
		</td>
		<td >
			<input type="text" id="txtKeyword">
		</td>
		<td align="right">
			Response
		</td>
		<td	align="right">
			<textarea id="txtResponse" maxlength="160" rows="2" />
		</td>
	</tr>
	<tr >
		<td colspan="4" align="right">
			<label id="lblRemainChars" style="text-align:right">160/160 characters</label>
		</td>		
	</tr>
	<tr>
		<td colspan="3" align="right">
			Tool Tip
		</td>
		<td align="right">
			<textarea id="txtToolTip"rows="2"/>
		</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td align="right" colspan ="2">
			<cfoutput>
				<input type="button" class="button filterButton small" value="Add Response" onclick= "addResponse()">
			</cfoutput>
		</td>
	</tr>
	<tr>
		<td align="right">
			<input type="button"  class="button filterButton small" value="Cancel" onclick="closePublicResponseForm()">
		</td>
		<td>
			<input type="button"  class="button filterButton small" value="Save" onclick="closePublicResponseForm()">
		</td>
	</tr>
</table>
<script type="text/javascript">
	$('textarea[maxlength]').keyup(function(){  
        //get the limit from maxlength attribute  
        var limit = parseInt($(this).attr('maxlength'));  
        //get the current text inside the textarea  
        var text = $(this).val();  
        //count the number of characters in the text  
        var chars = text.length;  
  
        //check if there are more characters then allowed  
        if(chars > limit){  
            //and if there are use substr to get the text before the limit  
            var new_text = text.substr(0, limit);  
  
            //and change the current text with the new text  
            $(this).val(new_text);  
        }  
    });  
    $('#txtResponse').bind('cut copy paste', function (e) {
		ViewRemainChars();
	});
    $('#txtResponse').keyup(function(){  
        ViewRemainChars();
    });
	
	function EditKeyword(KeywordId,ShortCodeId){
		var editDefaultKeywordDialog = $('<div id="editDefaultKeyword"></div>');
   
	  	editDefaultKeywordDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/dsp_editDefaultKeyword?KeywordId=' + KeywordId+"&ShortCodeId="+ShortCodeId)
	   .dialog({
		    modal : true,
		    title: 'Edit keyword',
		    width: 400,
		    minHeight: 350,
		    position: 'top',
		    resizable: false,
		    close: function() { 
	     		editDefaultKeywordDialog.remove(); 
	    	}
	   });				
	}
	
	function DeleteKeyword(KeywordId,Keyword,ShortCodeId){
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/dsp_deleteKeyword?KeywordId=" + KeywordId + "&Keyword=" + Keyword+"&ShortCodeId="+ShortCodeId, 
						"Delete keyword", 
						"auto", 
						500,
						"deleteKeywordForm",
						false,
						true);
	}	
	
	function addResponse(){
		if(validateDefaultKeyword()==false)return;
		//add data
		var data ={
			ShortCodeId : '<cfoutput>#shortCodeId#</cfoutput>',
			Keyword : $('#txtKeyword').val(),
			Response : $('#txtResponse').val(),
			ToolTip : $('#txtToolTip').val(),
			IsDefault : '1'
		};
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/csc/csc.cfc', 'AddDefaultKeyword', data, "Add keyword fail!", function(d ) {
				jAlert("Keyword added", "Success!", function(result) { 
					$.ajax({
						url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Administration/smscampaigns/ShortCodeManager/dsp_publicResponses?ShortCodeId=" + "<cfoutput>#ShortCodeId#</cfoutput>",
						cache: false
					}).done(function( html ) {
						$("#dvPublicResponse").html(html);
					});
				});
		});
	}
	
	function validateDefaultKeyword(){
		if ($.trim($('#txtKeyword').val()) == '') {
			jAlert("Keyword cannot be empty", "Failure!", function(result){
			});
			return false;
		}
		return true;
	}
	
	function closePublicResponseForm(){
		location.reload();
	}
	
	function ViewRemainChars() {
		var value = $('#txtResponse').val().length + '/160 characters';
		$('#lblRemainChars').text(value);	
	}
</script>
