<cfparam name="token" default="0">
<cfparam name="PayerId" default="0">


<cfset AllGood = 0/>


<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>Transaction Complete</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
</script>

<cfoutput>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
</cfoutput>


<!--- Paypal approved transaction --->
<cfset CompletePPReturnVar = {} />
<cfset CompletePPReturnVar.ID = -1 />
<cfset CompletePPReturnVar.MSG = "We're Sorry! Unable to complete your transaction at this time." />


<!--- Verify against EBM tranasaction number --->
<cfif token NEQ "0">


	<!--- Insert into EBM DB payment system  --->

	<!--- Log event in user log --->
    <cfinvoke method="CompletePurchasePP" component="#Session.SessionCFCPath#.accountinfo" returnvariable="CompletePPReturnVar">
        <cfinvokeargument name="token" value="#token#">
        <cfinvokeargument name="PayerId" value="#PayerId#">
    </cfinvoke>  
                            
	<!---<cfdump var="#CompletePPReturnVar#" />--->
    
    <cfif CompletePPReturnVar.ID GT 0>
    	<cfset AllGood = 1 />
    </cfif>
    
  
    <cfif CompletePPReturnVar.ID EQ 1>
		<!--- Update balance at top of the page - using javascript/jquery--->
        
        
		    	
    </cfif>
    
  
    
	
</cfif>

















<style>

	.EBMDialog form input 
	{
	   height:22px;
	}

	.btn {
		border-radius: 0px 4px 4px 0px;
		border: none;
	}
	
	#PurchaseAmount, #PurchaseAmount-button
	{
		outline:none;		
		
	}
</style>


<cfoutput>

	<div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:1000px;">
        <div class="EBMDialog">
                                          
                <div class="header">
                
                	<cfif AllGood GT 0>
                    	<div class="header-text">Buy Credits - Transaction Completed</div>
                    <cfelse>
                    	<div class="header-text">Buy Credits - Transaction Failed!</div>
                    </cfif>
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
               
                <div class="inner-txt-box">
                             
                <div style="clear:both"></div>
                <div style="clear:both"></div>
                
                
                <div style="text-align:left; padding:30px; width:600px;">
                	
                    
					<cfif AllGood GT 0>
                    
                        <div class="messages-lbl" style="">#CompletePPReturnVar.MSG#</div>
                    
                    <cfelse>
                    
                        <div class="messages-lbl" style="">#CompletePPReturnVar.MSG#</div>
                    
                    </cfif>               	
                   
                                   
                </div>
               
          
               
                                          
                
        </div>
		 
	</div>    
</cfoutput>

