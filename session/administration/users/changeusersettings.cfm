

<script type="text/javascript" language="javascript">
	
	$(function(){
		
				
		<!---$('#unlimitedFunds').click(function(){
			if($('#unlimitedFunds').is(':checked')){
				$('#user_fund_limit').attr('readonly',true);
				$('#user_fund_limit').css('opacity','0.5');
			}else{
				$('#user_fund_limit').removeAttr('readonly');
				$('#user_fund_limit').css('opacity',1);
			}
		});
		
		$('#user_fund').autoNumeric({aPad: false});
		$('#user_fund_limit').autoNumeric({aPad: false});--->
	});
		
	
	//Revoke Invitation
	function revokeInvitation(userID){
		bootbox.confirm( "Are you sure you want to revoke this invitation? - Revoke Invitation", function(result) { 
			if(result)
			{	
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=RevokeInvitation&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  { USERID : userID},					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
					success:		
						function(d) 
						{
							if(d.RESULT == 1){
								$('#tblListUser').DataTable().ajax.reload();
							}else{
								bootbox.alert(d.MESSAGE);
							}
						} 		
						
					});
					
					return;
			}else
			{
				return;
			}
		});
	}
	
		
	<!---
		**************************************** 
		*	User Limits Section
		****************************************
	--->
		
	function changeUserInfo(userID){
		currentUserID = userID;
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=getAccountInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { USERID : userID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.SUCCESS == 0)
					{
						bootbox.alert(d.MESSAGE);
						return false;
					}
					else
					{						
						//$("#dialog_role #user_role").val(d.USERROLE);
						
							$('#dialog_usersettings #inpUserId').val(userID);
							$('#dialog_usersettings #inpFirstName').val(d.INFO.DATA.FIRSTNAME_VCH);
							$('#dialog_usersettings #inpLastName').val(d.INFO.DATA.LASTNAME_VCH);
							$('#dialog_usersettings #inpPassword').val('**********');
							
						ShowDialog('usersettings');
					}
				} 		
				
			});
	}
	
	<!--- Update basic account information --->
	function updateUserInfo()
	{
						
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=updateBasicAccount&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { USERID:   $('#dialog_usersettings #inpUserId').val(), 
					FirstName: $('#dialog_usersettings #inpFirstName').val(),
					LastName:  $('#dialog_usersettings #inpLastName').val(),
					Password:  $('#dialog_usersettings #inpPassword').val()
			
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.SUCCESS == 0)
					{						
						CloseDialog('usersettings');
						bootbox.alert(d.MESSAGE);
					}
					else
					{						
						CloseDialog('usersettings');
						$('#tblListUser').DataTable().ajax.reload();
						return false;
					}
				} 		
				
			});
	}
	
	
</script>


<!--- Invite User Popup --->
<div id="dialog_invite" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Invite User</span></strong></label><span id="closeDialog" onclick="CloseDialog('invite');">Close</span></div>
	<cfform action="" method="POST">
		
		<div class="dialog_content">
			<label><strong>First Name:</strong></label>
			<cfinput type="text" name="invite_fname" id="invite_fname"/>
			<div class='error' id='invite_fname_err'>Invalid First Name.</div>
		</div>
		<div class="dialog_content">
			<label><strong>Last Name:</strong></label>
			<cfinput type="text" name="invite_lname" id="invite_lname"/>
			<div class='error' id='invite_lname_err'>Invalid Last Name.</div>
		</div>
		<div class="dialog_content">
			<label><strong>Email:</strong></label>
			<cfinput type="text" name="invite_email" id="invite_email" validate="email"/>
			<div class='error' id='invite_email_err'>Invalid Email.</div>
		</div>
		<div class="dialog_content">
			<button  
				type="button" 
				class="somadesign1"
				onClick="sendInviteEmail(); return false;"	
			>Send</button>
			<button 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('invite');"	
			>Close</button>
		</div>
	</cfform>
</div>



<!--- Change email subaccount user information --->
<div id="dialog_usersettings" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><b><span id='title_msg'>User Information</span></b></label><span id="closeDialog" onclick="CloseDialog('usersettings');">Close</span></div>
	<form action="" method="POST" id="form_usersettings">
	
    	<input type="hidden" id="inpUserId" name="inpUserId" >
		<div class="dialog_content">
			<label><strong>First Name<strong></label>
			<input type="text" name="inpFirstName" id="inpFirstName" />
		</div>
        
        <div class="dialog_content">
			<label><strong>Last Name<strong></label>
			<input type="text" name="inpLastName" id="inpLastName" />
		</div>
        
        <div class="dialog_content">
			<label><strong>Account Password</strong></label>
			<input type="text" name="inpPassword" id="inpPassword" />
		</div>
                
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateUserInfo(); return false;"	
			>Save</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('usersettings');"	
			>Close</button>
		</div>
	</form>
</div>



