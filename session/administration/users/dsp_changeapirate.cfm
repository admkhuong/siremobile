<cfparam name="userID" default="">

<style>
	#btnUpdate, #btnCancel{
		background: -moz-linear-gradient(center top , #2484C6, #2484C6) repeat scroll 0 0 rgba(0, 0, 0, 0);
	    border: medium none !important;
	    color: #FFFFFF;
	    cursor: pointer;
	    font-size: 12px;
	    height: 25px;
	    margin-left: 10px;
	    padding: 0 5px;
	    text-transform: uppercase;
	    width: auto;
	}
</style>

<cfparam name="inpType" default="0">

<!--- get user API access information --->
<cfif inpType EQ 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.usersTool" method="GetUserApiLimit" returnvariable="getUserAPILimit">
		<cfinvokeargument name="inpUserid" value="#userID#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.company" method="GetCompanyApiLimit" returnvariable="getUserAPILimit">
		<cfinvokeargument name="inpCompanyid" value="#userID#">
	</cfinvoke>
</cfif>
<cfset disableButton = 'disabled="true"'>
<cfset checkButton = 'checked="checked"'>
<cfif getUserAPILimit.INPAPILIMIT NEQ "">
	<cfset disableButton = ''>
	<cfset checkButton = ''>
</cfif>

<cfoutput>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User API Limits</h4>        
</div>

<div class="modal-body">
	
		<div style="font-weight:bold">Please enter the number of API calls</div>
		<div style="margin:10px">
			<input type="text" name="inpNumberLimit" id="inpNumberLimit" style=" margin: 3px; padding: 3px;width:60px" value="#getUserAPILimit.INPAPILIMIT#" #disableButton# > 
		 per 
			<select name="inpTimeLimit" id="inpTimeLimit" style=" margin: 3px; padding: 3px;"  #disableButton# >
				<option value="0" #GetSelectedTime(getUserAPILimit.INPTIME, 0)# >Minute</option>
				<option value="1" #GetSelectedTime(getUserAPILimit.INPTIME, 1)# >Hour</option>
				<option value="2" #GetSelectedTime(getUserAPILimit.INPTIME, 2)# >Day</option>
				<option value="3" #GetSelectedTime(getUserAPILimit.INPTIME, 3)# >Week</option>
				<option value="4" #GetSelectedTime(getUserAPILimit.INPTIME, 4)# >Month</option>
			</select>
		
		</div>
		<div style="margin-left:20px">Eg., 100 api calls per day</div>
		<div style="margin:15px">
			<input type="checkbox" name="inpUnlimitedRequest" id="inpUnlimitedRequest" #checkButton#> Unlimited Requests
		</div>
		
	</div>
    
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false">Save changes</button>

</div>	

</cfoutput>

<script type="text/javascript">
	$(document).ready(function(){
		
		
		$('#inpUnlimitedRequest').change(function(){
			if($('#inpUnlimitedRequest').is(":checked")){
				$('#inpNumberLimit').prop('disabled',true);
				$('#inpTimeLimit').prop('disabled',true);
			}else{
				$('#inpNumberLimit').prop('disabled',false);
				$('#inpTimeLimit').prop('disabled',false);
			}
		});
				
	});
	
	
	function Save() {
		
		<cfif inpType EQ 0>
				var data = {
					inpUserID : '<cfoutput>#userID#</cfoutput>',
					inpApiLimit : $('#inpNumberLimit').val(),
					inpTime : $('#inpTimeLimit').val(),
					inpIsUnlimit : $('#inpUnlimitedRequest').is(":checked")
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc', 'updateUserApiLimit', data, "Update API access throttling.", function(d ) {
					bootbox.alert( "Update API access throttling successfully");
					$('#UserAPIModal').modal('hide');		
		    	});	 
		    <cfelse>
		    	var data = {
					inpCompanyid : '<cfoutput>#userID#</cfoutput>',
					inpApiLimit : $('#inpNumberLimit').val(),
					inpTime : $('#inpTimeLimit').val(),
					inpIsUnlimit : $('#inpUnlimitedRequest').is(":checked")
				};
				ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/company.cfc', 'updateCompanyApiLimit', data, "Update API access throttling.", function(d ) {
					bootbox.alert( "Update API access throttling successfully");
					$('#UserAPIModal').modal('hide');		
		    	});
			</cfif>
			
	}
	
	
</script>

<cffunction name="GetSelectedTime">
	<cfargument name="inpTime" required="true" default="0">
	<cfargument name="inpValue" required="true" default="0">
	<cfset selected = ''>
	
	<cfif inpTime EQ inpValue>
		<cfset selected = 'selected="true"'>
	</cfif>
	
	<cfreturn selected>
</cffunction>