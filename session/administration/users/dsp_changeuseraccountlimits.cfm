<cfparam name="userID" default="">


<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<style>

	#form_userlimits label
	{
		display:block;
		margin-bottom: 0;
		
	}

</style>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User Account Limits</h4>        
</div>

<div class="modal-body"> 

	<form action="" method="POST" id="form_userlimits">
	
    	<input type="hidden" id="UserID" name="UserID" value="<cfoutput>#userID#</cfoutput>" >
		<div class="">
			<label><strong>Max amount allowed to send per EMS</strong></label>
			<input type="text" name="inpMaxEMSPerSend" id="inpMaxEMSPerSend" />
		</div>
        <div class="">
			<label><strong>Max Length of audio recording for EMS</strong></label>
			<input type="text" name="inpMaxEMSAudioFileLength" id="inpMaxEMSAudioFileLength" />
		</div>        
		
	</form>
 
</div>        
    
<div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false" data-dismiss="modal">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
		<cfif userID NEQ "">
		
		<!--- Read data in from current user --->
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=GetCurrentUserLimits&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { inpUserId : $('#form_userlimits #UserID').val()},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					try
					{
						if(d.RXRESULTCODE != "1")
						{
							bootbox.alert('Cannot get user information.');
							return false;
						}
						else
						{	
							$('#form_userlimits #inpMaxEMSPerSend').val(d.EMSPERSEND);
							$('#form_userlimits #inpMaxEMSAudioFileLength').val(d.EMSAUDIOLENGTH);												
						}
					}
					catch(exception)
					{
						return false;
					}
				} 		
				
			});
						
		</cfif>	
		
	});
	
	function Save() {
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=UpdateUserLimits&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpUserId : $('#form_userlimits #UserID').val(), 
				inpMaxEMSPerSend : $('#form_userlimits #inpMaxEMSPerSend').val(),
				inpMaxEMSAudioFileLength : $('#form_userlimits #inpMaxEMSAudioFileLength').val()				
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.RXRESULTCODE > 0){
					bootbox.alert( "Set User limits successfully - Success!", function(result) { 
						$('#ChangeUserAccountLimitsModal').modal('hide');				
					});
				}
				else
				{
					bootbox.alert(d.MESSAGE);
					$('#ChangeUserAccountLimitsModal').modal('hide');	
				}
			} 		
		});
		
	}
	
	
		
</script>
