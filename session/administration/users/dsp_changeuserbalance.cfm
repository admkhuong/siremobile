<cfparam name="userID" default="">


<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User Balance</h4>        
</div>

<div class="modal-body">        
  		 <div>
			<label><strong>User's Funds</strong></label>
			<span class="dolaFund">$</span> 
			<input type="text" name="user_fund" validate="integer" id="user_fund" />			
		</div>
		<div>
			<input type="checkbox" name="unlimited" id="unlimited">
			<label for="unlimited">Unlimited Fund</label>
		</div>
</div>        
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
		$('#unlimited').click(function(){
			if($('#unlimited').is(':checked')){
				$('#user_fund').attr('readonly',true);
				$('#user_fund').css('opacity','0.5');
			}else{
				$('#user_fund').removeAttr('readonly');
				$('#user_fund').css('opacity',1);
			}
		});
		
		$('#user_fund').autoNumeric({aPad: false});
		$('#user_fund_limit').autoNumeric({aPad: false});
		
		<cfif userID NEQ "">
			
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=GetCurrentUserFund&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { USERID : '<cfoutput>#userID#</cfoutput>'},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					try{
						if(d.RESULT != "SUCCESS"){
							bootbox.alert('Cannot get user information.');
							return false;
						}else{
							//return parseInt(d.USERFUND);
							if(d.USERFUND.indexOf('$') != -1){
								d.USERFUND = d.USERFUND.substring(d.USERFUND.indexOf('$') + 1, d.USERFUND.length);
							}
							$("#UserBalanceModal #user_fund").val(d.USERFUND);
							if(d.UNLIMITEDBALANCE == 1){
								$('#UserBalanceModal #unlimited').prop('checked', true);
								$('#user_fund').attr('readonly',true);
								$('#user_fund').css('opacity','0.5');
							}else{
								$('#UserBalanceModal #unlimited').prop('checked', false);
								$('#user_fund').removeAttr('readonly');
								$('#user_fund').css('opacity',1);
							}
							
						}
					}catch(exception){
						return false;
					}
				} 		
				
			});
			
		</cfif>	
		
	});
	
	function Save() {
		
		var new_fund = $("#UserBalanceModal #user_fund").val();
		if(new_fund == ''){
			$("#UserBalanceModal #user_fund_err").show();
			return false;
		}else{
			$("#UserBalanceModal #user_fund_err").hide();
		}
		if('<cfoutput>#userID#</cfoutput>' == '')
		{
			$('#UserBalanceModal').modal('hide');
			return false;
		}
		
		var UnlimitedBalance = $('#UserBalanceModal #unlimited').is(':checked') ? 1 : 0;
		
		// Update funds
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=UpdateUserFund&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				USERID : '<cfoutput>#userID#</cfoutput>', 
				NEWFUND : new_fund,
				UnlimitedBalance: UnlimitedBalance
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				function(d) 
				{
					if(d.RESULT == 'SUCCESS'){
						$('#tblListUser').DataTable().ajax.reload();
						$('#UserBalanceModal').modal('hide');
					}else{
						bootbox.alert(d.MESSAGE);
						$('#UserBalanceModal').modal('hide');
					}
				} 		
				
			});
			
	}
	
	
		
</script>
