<cfparam name="userID" default="">
<cfparam name="cid" default="">
 
        

<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<style>

	#cid_form label
	{
		display:block;
		margin-bottom: 0;
		
	}

</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User Default CID</h4>        
</div>

<cfoutput>
<div class="modal-body">        
  	<form action="" method="POST" id="cid_form">
		<input name="userID" id="userID" type="hidden" value="#userID#">
		
		<div class="">
			<label><strong>Caller ID</strong></label>
			<input name="callerId" id="cid" value="#cid#">
		</div>
		
	</form>
</div>        
</cfoutput>
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
	
		
	});
	
	function Save() {
		
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=updateCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				userId : $('#cid_form #userID').val(), 
				callerId : $('#cid_form #cid').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d)
			{
				if(d.DATA.RXRESULTCODE[0] > 0)
				{
					bootbox.alert( "Set Caller Id successfully - Success!");
					$('#tblListUser').DataTable().ajax.reload();	
					$('#ChangeUserCIDModal').modal('hide');					
					
				}
				else
				{
					bootbox.alert(d.DATA.MESSAGE[0]);
					$('#ChangeUserCIDModal').modal('hide');
				}
			} 		
		});
	}
		
</script>
