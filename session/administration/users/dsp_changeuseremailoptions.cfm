<cfparam name="userID" default="">


<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<style>

	#form_emailsu label
	{
		display:block;
		margin-bottom: 0;
		
	}

</style>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User eMail Options</h4>        
</div>

<div class="modal-body">        
   <form id="form_emailsu">
	
    	<input type="hidden" id="inpUserId" name="inpUserId" value="<cfoutput>#userID#</cfoutput>" >
		<div class="">
			<label><strong>Sub User Account User Name ID</strong></label>
			<input type="text" name="inpUserName" id="inpUserName" />
		</div>
        <div class="">
			<label><strong>Sub User Account Password</strong></label>
			<input type="text" name="inpPassword" id="inpPassword" />
		</div>
        <div class="">
			<label><strong>Sub User Account eMail Remote Address</strong></label>
			<input type="text" name="inpRemoteAddress" id="inpRemoteAddress" />
		</div>
        <div class="">
			<label><strong>Sub User Account eMail Remote Port</strong></label>
			<input type="text" name="inpRemotePort" id="inpRemotePort" />
		</div>
        <div class="">
			<label><strong>Sub User Account eMail TLS Flag</strong></label>
			<input type="text" name="inpTLSFlag" id="inpTLSFlag" />
		</div>
        <div class="">
			<label><strong>Sub User Account eMail SSL Flag</strong></label>
			<input type="text" name="inpSSLFlag" id="inpSSLFlag" />
		</div>
       
	</form>
</div>        
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false" data-dismiss="modal">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
		<cfif userID NEQ "">
			
			<!--- Read data in from current user --->
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=GetCurrentUserEmailSubaccountInfo&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { inpUserId : $('#form_emailsu #inpUserId').val()},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					function(d) 
					{
						try
						{
							if(d.RXRESULTCODE != "1")
							{
								bootbox.alert('Cannot get user information.');
								return false;
							}
							else
							{														
								$('#form_emailsu #inpUserId').val(userId);
								$('#form_emailsu #inpUserName').val(d.USERNAME);
								$('#form_emailsu #inpPassword').val(d.PASSWORD);
								$('#form_emailsu #inpRemoteAddress').val(d.REMOTEADDRESS);
								$('#form_emailsu #inpRemotePort').val(d.REMOTEPORT);
								$('#form_emailsu #inpTLSFlag').val(d.TLSFLAG);
								$('#form_emailsu #inpSSLFlag').val(d.SSLFLAG);															
							}
						}
						catch(exception)
						{
							return false;
						}
					} 		
					
				});
		</cfif>	
		
	});
	
	function Save() {
		
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=UpdateUsereMailTarget&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				inpUserId : $('#form_emailsu #inpUserId').val(), 
				inpUserName : $('#form_emailsu #inpUserName').val(),
				inpPassword : $('#form_emailsu #inpPassword').val(), 
				inpRemoteAddress : $('#form_emailsu #inpRemoteAddress').val(), 
				inpRemotePort : $('#form_emailsu #inpRemotePort').val(), 
				inpTLSFlag : $('#form_emailsu #inpTLSFlag').val(), 
				inpSSLFlag : $('#form_emailsu #inpSSLFlag').val() 
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.RXRESULTCODE > 0){
					bootbox.alert( "Set User eMail target information successfully - Success!", function(result) { 
						$('#ChangeUserEMailOptionsModal').modal('hide');				
					});
				}else{
					bootbox.alert(d.MESSAGE);
					$('#ChangeUserEMailOptionsModal').modal('hide');
				}
			} 		
		});
		
				
		
	}
	
	
		
</script>
