<cfparam name="userID" default="">
<cfparam name="rate1" default="">
<cfparam name="rate2" default="">
<cfparam name="rate3" default="">
<cfparam name="rateType" default="">
<cfparam name="name" default="">
   
        

<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User Balance</h4>        
</div>

<cfoutput>
<div class="modal-body">        
  		<form action="" method="POST" id="dialog_rates_form">
		<div class="dialog_content">
			<label>Enter the rate <b><span id='userNameText'>#name#</span></b> will be paying per transaction.</label><br/><br/>
			<input name="userId" id="uid" type="hidden" value="#userID#">
		</div>
		<div class="dialog_content">
			<label><strong>Rate type</strong></label>
			<select name="rateType" id="rateType">
				<option value="1" <cfif rateType EQ 1>selected </cfif>>Per minute</option>
				<option value="2" <cfif rateType EQ 2>selected </cfif>>Per message</option>
				<option value="4" <cfif rateType EQ 4>selected </cfif>>Per transaction</option>
			</select>
		</div>
		<div class="dialog_content">
			<label><strong>Rate 1</strong></label>
			<input name="rate1" id="rate1" value="#rate1#">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 2</strong></label>
			<input name="rate2" id="rate2" value="#rate2#">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 3</strong></label>
			<input name="rate3" id="rate3" value="#rate3#">
		</div>
	
	</form>
</div>        
</cfoutput>
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
	
		
	});
	
	function Save() {
		
		$.ajax({
           type: "POST",
           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=updateRate&&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: $('#dialog_rates_form').serialize(),
           success: function(d)
		   {
           		if (d.ROWCOUNT > 0) 
				{													
				
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{           				
							bootbox.alert(d.DATA.MESSAGE[0]);						
							$('#tblListUser').DataTable().ajax.reload();
							$('#UserRatePlanModal').modal('hide');
													
						}           			
						else
						{
							bootbox.alert(d.DATA.MESSAGE[0] );
							$('#UserRatePlanModal').modal('hide');		
						}
					}
				}           	
		   }
		   
  		});
			
	}
	
	
		
</script>
