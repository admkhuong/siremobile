<cfparam name="userID" default="">


<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif session.userrole NEQ 'SuperUser'>
	<h4 class="err">
		You don't have permission to access this page!
	</h4>
	<cfexit>
</cfif>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Change User Role</h4>        
</div>

<div class="modal-body">        
    <label><strong>User Role</strong></label>
    <select name="user_role" id="user_role" required="yes">
    </select>
</div>        
    
 <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick ="Save();return false" data-dismiss="modal">Save changes</button>

</div>	

<script type="text/javascript">

		
	$(function() {
	
		<cfif userID NEQ "">
			
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=GetCurrentUserRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { USERID : '<cfoutput>#userID#</cfoutput>'},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					function(d) 
					{
						if(d.SUCCESS == 0)
						{
							bootbox.alert(d.MESSAGE);
							return false;
						}
						else
						{
							<!---// generate user role--->
							var roleArray = d.ROLEARRAY;
							var roleOptionHtml = '';
							<!---//for(var option in roleArray){
								//roleOptionHtml += '<option value=" + option.ID + ">' + option.DisplayName + '</option>';
							//}--->
							for(var i = 0; i < roleArray.length; i ++)
							{
								if(d.USERROLE != roleArray[i].ID)
								{
									roleOptionHtml += '<option value="' + roleArray[i].ID + '">' + roleArray[i].DISPLAYNAME + '</option>';
								}
								else
								{
									roleOptionHtml += '<option value="' + roleArray[i].ID + '" selected="selected">' + roleArray[i].DISPLAYNAME + '</option>';
								}
							}
													
							$("#user_role").html('');
							$("#user_role").append(roleOptionHtml);
							
						}
					} 		
					
				});
		</cfif>	
		
	});
	
	function Save() {
		
			var new_role = $("#UserRoleModal #user_role").val();
			if('<cfoutput>#userID#</cfoutput>' == '' <!---|| currentRoleID == new_role--->){
				$('#UserRoleModal').modal('hide');
				//bootbox.alert('Update Failed!');
				return false;
			}
			// Update user role
			
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=UpdateUserRole&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { USERID : '<cfoutput>#userID#</cfoutput>', NEWROLEID : new_role},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:		
					function(d) 
					{
						if(d == 'Success'){
							$('#tblListUser').DataTable().ajax.reload();
							$('#UserRoleModal').modal('hide');
							return false;
						}else{
							bootbox.alert(d.MESSAGE);
							$('#UserRoleModal').modal('hide');
						}
					} 		
					
				});
			
		
	}
	
	
		
</script>
