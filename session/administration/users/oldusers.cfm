<!--- Check permission against acutal logged in user not "Shared" user--->
<cfif Session.CompanyUserId GT 0>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#Session.CompanyUserId#">
	</cfinvoke>
<cfelse>
	<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getUserByUserId" returnvariable="getCurrentUser">
		<cfinvokeargument name="userId" value="#session.userId#">
	</cfinvoke>
</cfif>

<cfif getCurrentUser.USERROLE neq 'SuperUser' >
	<cfset session.permissionError = "You don't have permission to access this page!">
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_Users_Management_Title#">
</cfinvoke>

<cfsetting showdebugoutput="true">

<cfimport taglib="../../lib/grid" prefix="mb" />
<cfparam name="_search" default="">
<cfparam name="nd" default="">
<cfparam name="rows" default="">
<cfparam name="page" default="">
<cfparam name="sidx" default="">
<cfparam name="sord" default="">


<cfset colNames = ['EBM ID','First Name','Last Name', 'Email Address', 'Last Logged In', 'Role', 'Balance', 'Company ID', 'Company Name', 'Options']>
<cfset colModels = [			
		{name='UserID', width='80px', sortObject= {isDefault='false', sortType="DESC", sortColName ='users.userId_int'}},
		{name='FirstName', width='120px', sortObject= {isDefault='false', sortType="DESC", sortColName ='users.FirstName_vch'}},	
		{name='LastName',width='120px', sortObject= {isDefault='false', sortType="DESC", sortColName ='users.LastName_vch'}},
		{name='Email', width='250px', sortObject= {isDefault='false', sortType="DESC", sortColName ='users.EmailAddress_vch'}},
		{name='LastLoggedIn', width='250px', sortObject= {isDefault='false', sortType="DESC", sortColName ='users.LastLogin_dt'}},	
		{name='Role',width='90px', sortObject= {isDefault='false', sortType="DESC", sortColName ='Rol.rolename_vch'}},
		{name='Balance', width='70px', sortObject= {isDefault='false', sortType="DESC", sortColName ='bill.Balance_int'}},
		{name='CompanyID', width='120px', isHtmlEncode=false, sortObject= {isDefault='false', sortType="DESC", sortColName ='com.CompanyAccountId_int'}},	
		{name='CompanyName',width='150px', sortObject= {isDefault='false', sortType="DESC", sortColName ='com.CompanyName_vch'}},
		{name='Options', width='350px', isHtmlEncode=false}
	   ]>
<!-- generate list -->
<cfset
		filterKeys = [
			{VALUE='1', DISPLAY='EBM ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='2', DISPLAY='First Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='3', DISPLAY='Last Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='4', DISPLAY='Email Address', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"},
			{VALUE='5', DISPLAY='Last Logged In', TYPE='CF_SQL_DATE', FILTERTYPE="DATE"},
			{VALUE='6', DISPLAY='Role', TYPE='CF_SQL_VARCHAR', FILTERTYPE="LIST", LISTOFVALUES = 
																				[{VALUE = 'Normal User', DISPLAY="Normal User"},
																				{VALUE = 'Company Administrator', DISPLAY="Company Administrator"}, 
																				{VALUE = 'Super Administrator', DISPLAY="Super Administrator"},
																				{VALUE = 'Company User', DISPLAY="Company User"}]},
			{VALUE='7', DISPLAY='Balance', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER", EXCEPTIONCASE="Unlimited"},
			{VALUE='8', DISPLAY='Company ID', TYPE='CF_SQL_INTEGER', FILTERTYPE="INTEGER"},
			{VALUE='9', DISPLAY='Company Name', TYPE='CF_SQL_VARCHAR', FILTERTYPE="TEXT"}
		]
	>
<cfset 
	filterFields = [
		'users.userId_int',
		'users.FirstName_vch',
		'users.LastName_vch',
		'users.EmailAddress_vch',
		'users.LastLogin_dt',
		'Rol.rolename_vch',
		'bill.Balance_int',
		'com.CompanyAccountId_int',
		'com.CompanyName_vch'
	]	
	>

<cfset FilterSetId = "UserMain">
<cfoutput>
	<mb:table 
		component="#LocalSessionDotPath#.cfc.administrator.usersTool"
		method="getUserList"
		colNames="#colNames#"
		colModels="#colModels#"
		page="#page#"
		width="100%"
		name="users"
		filterkeys = "#filterKeys#"
		filterFields="#filterFields#"
        FilterSetId="#FilterSetId#"
		>
	</mb:table>
</cfoutput>

<!--- Change company rate Popup --->
<div id="dialog_rates" class="web_dialog" style="display: none;">
	<div class='dialog_title'><label><strong><span id='title_msg'>Set Cost per Transaction</span></strong></label><span id="closeDialog" onclick="CloseDialog('rates');">Close</span></div>
	<cfform action="" method="POST" id="dialog_rates_form">
		<div class="dialog_content">
			<label>Enter the rate <b><span id='userNameText'></span></b> will be paying per transaction.</label><br/><br/>
			<cfinput name="userId" id="uid" type="hidden">
		</div>
		<div class="dialog_content">
			<label><strong>Rate type</strong></label>
			<select name="rateType" id="rateType">
				<option value="1">Per minute</option>
				<option value="2">Per message</option>
				<option value="4">Per transaction</option>
			</select>
		</div>
		<div class="dialog_content">
			<label><strong>Rate 1</strong></label>
			<cfinput name="rate1" id="rate1">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 2</strong></label>
			<cfinput name="rate2" id="rate2">
		</div>
		<div class="dialog_content">
			<label><strong>Rate 3</strong></label>
			<cfinput name="rate3" id="rate3">
		</div>
		<div class="dialog_content">
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="updateRates(); return false;"	
			>Update</button>
			<button 
				id="close_dialog" 
				type="button" 
				class="somadesign1"
				onClick="CloseDialog('rates');"	
			>Cancel</button>
		</div>
	</cfform>
</div>

<cfinclude template="changeusersettings.cfm">

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>#Admin_Users_Management_Title#</cfoutput>');
	$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
	
	function changeRate(userId,rateType,rate1,rate2,rate3,name){
		$('#uid').val(userId);
		$('#rate1').autoNumericSet(rate1);
		$('#rate2').autoNumericSet(rate2);
		$('#rate3').autoNumericSet(rate3);
		$('#rateType').val(rateType);
		
		$('#userNameText').text(name);
		ShowDialog('rates');
	}
	
	function changeAPIRequest(userid){
		jPopup("<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/Administration/dsp_changeApiRequest?inpUserId=" + userid, "API Access Throttling", function(result){
		});
	}
	
	function updateRates(){
		$.ajax({
           type: "POST",
           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/administrator/usersTool.cfc?method=updateRate&&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
           dataType: "json", 
           data: $('#dialog_rates_form').serialize(),
           success: function(d){
           	if (d.ROWCOUNT > 0) 
			{													
			
				<!--- Check if variable is part of JSON result string --->								
				if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
				{							
					CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
					
					if(CurrRXResultCode > 0)
					{
           				CloseDialog('rates');
          				jAlert(d.DATA.MESSAGE[0], "Change Company Rate",function(result){
           					if(result){
           						location.reload();
           					}
           				});
           			}else{
						jAlert(d.DATA.MESSAGE[0], "Change Company Rate");		
					}
           		}
           	}
           	$('#accepCondition').removeAttr('checked');
           }
  		});
	}
	
</script>

<style type="text/css" media="screen">
	#innertube {
	    margin: 0 23px 0 20px;
	    padding: 19px 0;
	    width: 1700px;
	}
	.filter_box{
		width:80%;
	}
</style>	
