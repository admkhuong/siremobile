<cfparam name="page" default="1">
<cfparam name="userId" default="0">
<cfparam name="filterTime" default="0">

<cfinvoke 
      component="#LocalSessionDotPath#.cfc.administrator.permission"
      method="showlogUserPermission"
      returnvariable="showlogUserPermission">     
     <cfinvokeargument name="userId" value="#userId#"/>  
</cfinvoke> 
<cfif NOT showlogUserPermission.permission>
	<cfoutput>#showlogUserPermission.message#</cfoutput>
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Admin_User_Logs_Title#">
</cfinvoke>

<cfimport taglib="../../lib/grid" prefix="mb" />

<script type="text/javascript">

		<!--- OLD crap - remove!--->
		function post_to_url(path, params, method)
		{

			<!---//fix mod-rewrite removing data for POST method--->
			if(typeof(path) == "undefined")
				path = "";

			path = path.replace(".cfm","");
		<!---
			method = method || "post"; // Set method to post by default, if not specified.
		--->

			var newform = $( document.createElement('form') );
			newform.attr("method","post")
			newform.attr("action",path)

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = $( document.createElement('input') ); // document.createElement("input");
					hiddenField.attr("type", "hidden");
					hiddenField.attr("name", key);
					hiddenField.attr("value", params[key]);

					newform.append(hiddenField);
				 }
			}
			$( document.body).append(newform);
			newform.submit();
		}
		
	$(function(){
		$('#filterTimeButton').click(function(){
			<cfoutput>
				var params = {
					'USERID':'#userId#',
					filterTime:$('##filterTime option:selected').val()
				};
				<!---post_to_url('#rootUrl#/#sessionPath#/Administration/users/showLogs', params, 'POST');--->
			</cfoutput>
			
			<cfoutput>	
				window.location = generateUrl("#rootUrl#/#sessionPath#/Administration/users/showLogs/contacts/GroupDetails", params );  
			</cfoutput>	
		
		});
		
		$('#subTitleText').text('<cfoutput>#Admin_Users_Management_Title# >> #Admin_User_Logs_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Administrator_Title#</cfoutput>');
	});
</script>



<!---wrap the page content do not style this--->
<div id="page-content">
   
  <div class="container" >
    <h2 class="no-margin-top">User Account Logs</h2>
    

		<cfoutput>
            <select name="filterTime" id='filterTime'>
                <option value="0" > #All_Time# </option>
                <option value="1" <cfif filterTime EQ 1>selected="selected"</cfif> > #Today_Time# </option>
                <option value="2" <cfif filterTime EQ 2>selected="selected"</cfif> > #Last_7_Days# </option>
                <option value="3" <cfif filterTime EQ 3>selected="selected"</cfif> > #Last_30_Days# </option>
            </select>
            <input type="button" value="Filter" id="filterTimeButton">
        </cfoutput>
        
        <cfset colNames = ['EBM ID','First Name','Last Name', 'Module Name', 'Operation', 'Timestamp of operation']>
        <cfset colModels = [			
                {name='UserID', width=40},
                {name='FirstName', width=90},	
                {name='LastName',width=90},
                {name='ModuleName', width=150},
                {name='Operator', width=150},	
                {name='Timestamp',width=100}
               ]>
        <!-- generate list -->
        <cfset params = [
                {
                    name = "userId",
                    value = "#UserId#"
                },
                {
                    name = "filterTime",
                    value = "#filterTime#"
                }
            ]>
        
        <cfoutput>
            <mb:table 
                component="#LocalSessionDotPath#.cfc.administrator.usersLogs"
                method="getUserLogs"
                colNames="#colNames#"
                colModels="#colModels#"
                params = #params#
                page="#page#"
                width="100%"
                name="User Logs"
                >
            </mb:table>
            <cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                <cfinvokeargument name="userId" value="#session.userid#">
                <cfinvokeargument name="moduleName" value="Administration">
                <cfinvokeargument name="operator" value="View log user #UserId#">
            </cfinvoke>
        </cfoutput>
        



 		<p class="lead"></p>

    	
        <row>
        	
            <div class="" style="margin-bottom:25px;">
            
            
            </div>
        
        </row>
    
   </div>
  <!--- /.container --->
  
</div>
<!--- /#page-content --->
