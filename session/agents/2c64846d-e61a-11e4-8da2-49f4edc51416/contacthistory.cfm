<cfparam name="inpContactTypeId" default="3">
<cfparam name="inpContactString" default="">
<cfparam name="inpMSG" default="">
<cfparam name="inpBatchIdList" default="">
<cfparam name="inpStart" default="#dateformat(DateAdd('d', -30, NOW()),'yyyy-mm-dd')#">
<cfparam name="inpEnd" default="#dateformat(NOW(),'yyyy-mm-dd')#">

<!--- Not AJAX in case session expires - page load will recover cookie where CFC will not in EBM--->

<!--- Validate Security--->
<cfinvoke component="#LocalSessionDotPath#.cfc.agentportals" method="AuthenticateUser" returnvariable="getAuth">
    <cfinvokeargument name="INPPORTALID" value="2004">
</cfinvoke>

<!---<cfdump var="#getAuth#" />--->

<cfif getAuth.RXRESULTCODE NEQ 1 >

	<h2>You do not have permissions to view this page!</h2>
    <cfabort />

</cfif>

<cfif NOT AgentToolsPermission.havePermission>

	<h2>You do not have permissions to view this page!<BR />Contact your company administrator if you need privileges turned on.</h2>
    <cfabort />

</cfif>

<cfoutput>

	<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    
    <script type='text/javascript' src='#rootUrl#/#publicPath#/js/jquery.mousewheel.min.js'></script>   
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    <script type="text/javascript" src="#rootUrl#/#publicPath#/js/datepicker/datepicker.js"></script>  
    
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/jquery.colorbox-min.js"></script>
    
    <style>
		@import url('#rootUrl#/#publicPath#/css/datepicker/base.css');
		@import url('#rootUrl#/#publicPath#/css/datepicker/clean.css');
		@import url('#rootUrl#/#publicPath#/css/reporting/campaign.css');
		@import url('#rootUrl#/#publicPath#/css/style.css');
	</style>

</cfoutput>

<!--- Lock down page to authenticated users either company admin or linked access user id --->



<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Agent tools <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Custom Agent Interfaces</cfoutput>');
	$('#subTitleText').html('Contact History');
</script>

</script>

<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	#AgentInterfaceContainer .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}


	#AgentInterfaceContainer .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);
		position:absolute;
		top: -25px;
		right: 37px;
	}

	#AgentInterfaceContainer .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#AgentInterfaceContainer .form-right-portal {
    z-index: 1000;
}


	.sbHolder
	{
		width:266px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px 3px 3px 3px;
	}

	<!--- 30 less than sbHolder --->
	.sbSelector
	{
		width:246px;
	}

	.sbOptions
	{
		max-height:250px !important;
	}

	.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}

		.datatable
		{
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}

		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		.wrapper-picker-container{
			 margin-left: 20px;
		}


		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
			padding-bottom:20px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 230px;
		}
		
		.MIDHighlight
		{
			color:#F90;
			font-size:18px;			
		}
		
		.MSGAlert
		{
			color:#F00;
			font-size:18px;	
			width:100%;
			text-align:center;	
			margin-top:8px;	
		}

		<!--- Tighten up form for smaller screens --->
		.EBMDialog .inputbox-container 
		{
			margin-bottom: 6px;
		}
		
		.EBMDialog form input 
		{ 
			margin-bottom: 2px;
			padding: 2px 0;
			width: 100%;
		}
	
		.EBMDialog label 
		{
			margin-bottom: 2px;
		}
		
		
		.EBMDialog .header-text {
			line-height:28px;
		}

		
						
</style>


<!--- REPORT FORM STYLES --->
<style>

	#datepicker-calendar {
		
		left: 0px;
		z-index: 100;
	}


	.ResetChartOption
	{
		position:relative;
		top:-20px;
		right:10px;
		z-index:10000;	
		cursor:pointer;	
		text-decoration:none;
		font-size: 12px;
	    text-align: right;
	}

	.ResetChartOption:hover
	{
		text-decoration:underline;
	}
	
	.messages-lbl{
		color: #0888D1 !important;
		font-family: "Verdana" ;
		font-weight: bold ;
		line-height: 14px ;
		margin-left: 20px ;
		padding-bottom: 20px ;
		padding-top: 30px ;
		font-size: 16px;
	}
	
	.caller_id{
		padding-top:0;
	}
	
	#content-AAU-Admin{
		background-color:#fff;
		border:1px solid #ccc;
		border-radius:4px;
	}
	
	#divAgent{
		margin-bottom:90px;
	}
	
	#content-AAU-Admin img{
		width:16px; 
		float:left;
		cursor:pointer;
	}
	
	#tblActiveAgent tbody{
		border-left:1px solid #ccc;
		border-right:1px solid #ccc;
	}
	
	#tblActiveAgent_paginate {
		border-left: 1px solid #CCCCCC;
		margin-top: -21px;
	}
	
	#em-name-block{
		width:70%;
		margin-left:21px;
		font-family:"Verdana" !important;
		font-size:12px !important;
	}
	
	.left-input{
		width:95px;
		float:left;
		border-left:1px solid #ccc;
		border-top:1px solid #ccc;
		border-bottom:1px solid #ccc;
		border-right:none;
		height:28px;
		border-radius: 4px 0 0 4px;
		color: #666666;
		line-height: 25px;
		padding-left: 15px;
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#f4f4f4));
		background-image: -webkit-linear-gradient(top, #fff, #f4f4f4);
		background-image: -moz-linear-gradient(to bottom, #fff, #f4f4f4);
		background-image: -o-linear-gradient(top, #fff, #f4f4f4);
		background-image: linear-gradient(top, #fff, #f4f4f4);
	}
	
	.left-input>span.em-lbl{
		width: 72px;
		float:left;
	}
	
	.left-input>div.hide-info{
		width:18px;
		float:left;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
	}
	
	.left-input>div.hide-info:hover{
		width:18px;
		float:left;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
	}
	
	.message-block .right-input{
		width:387px;
		float: left;
		border:1px solid #ccc;
		height:28px;
		border-radius: 0 4px 4px 0;
		color: #666666;
	}
	
			
	.message-block .right-input{
		width:381px;
		float: left;
		border:1px solid #ccc;
		height:28px;
		border-radius: 0 4px 4px 0;
		color: #666666;
	}
	
	.message-block .right-input>span.selectmenucustom{
		height:28px!important;
		border: medium none;
		border-radius: 0;
		overflow:hidden;
	}
	
	.message-block .right-input .ui-selectmenu-status {
		line-height: 1.4em;
	}
	
	.message-block .right-input .ui-selectmenu-icon{
		margin-top: -13px;
	} 
	
	.right-input>input{
		border:none !important;
		height: 21px;
		line-height: 30px;
		width: 321px;
		background-color:#fbfbfb;
		font-family:"Verdana" !important;
		font-size:12px !important;
		color:#000 !important;
		outline: 0 none !important;
	}
	
		
	.message-block{
		margin-left:21px;
		font-family:"Verdana";
		font-size:12px;
	}
	
	.left-input{
		width:130px;
		float:left;
		border-left:1px solid #ccc;
		border-top:1px solid #ccc;
		border-bottom:1px solid #ccc;
		border-right:none;
		height:28px;
		border-radius: 4px 0 0 4px;
		color: #666666;
		line-height: 25px;
		padding-left: 10px;
	}
	
	.left-input>span.em-lbl{
		width: 106px;
		float:left;
	}
	
	.left-input>div.hide-info{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
	}
	
	.left-input>div.hide-info:hover{
		width:18px;
		float:left;
		background: url("<cfoutput>#rootUrl#/#publicPath#/css</cfoutput>/ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
	}
	
	.info-block{
		background-color: #FFFFFF;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		left: 100px;
		padding: 6px;
		position: relative;
		top: -10px;
	}
	
	.EM-btn{
		height:30px;
		border:1px solid #ccc;
		width:128px;
		float:left;
		line-height: 28px;
		margin-bottom: 10px;
		margin-left: 21px;
		text-align: center;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1) inset, 0 1px 5px rgba(0, 0, 0, 0.25);
		border-radius:4px;
		font-family:"Verdana";
		font-size:14px;
		color:#fff;
	} 
	

	.top-input{
		border: 1px solid #CCCCCC;
		border-radius: 4px 4px 0 0;
		color: #666666;
		margin-top: 10px;
		min-height: 40px;
		padding: 10px;
		width: 505px;
	}
	
	.bottom-input{
		-moz-border-bottom-colors: #CCCCCC;
		-moz-border-left-colors: #CCCCCC;
		-moz-border-right-colors: #CCCCCC;
		-moz-border-top-colors: none;
		border-image: none;
		border-radius: 0 0 4px 4px;
		border-style: none solid solid;
		border-width: medium 1px 1px;
		color: #666666;
		width: 505px;
		float:left;
		height: 14px;
		padding: 8px 10px;
	}
	
	.bottom-input input{
		float:left;
	}
	
	.bottom-input label{
		float:left;
		margin-left:10px;
	}
	
	.bottom-input .hide-info{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:0.4;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	
	.bottom-input .hide-info:hover{
		width: 50px;
		float:right;
		background: url("../ire/images/info.png") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		opacity:1;
		display:inline-block;
		height: 25px;
		margin-right: -30px;
		margin-top: -6px;
	}
	

	
	.method-btn {
		background-color: #F5F5F5;
		background-image: linear-gradient(to bottom, #FFFFFF, #E6E6E6);
		background-repeat: repeat-x;
		border: 1px solid #CCCCCC;
		border-radius: 4px;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
		cursor: pointer;
		display: inline-block;
		font-size: 14px;
		line-height: 20px;
		margin-bottom: 0;
		padding: 4px 12px;
		text-align: center;
		text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
		vertical-align: middle;
	}
	
	div.warning-block{
		border: 1px solid #FFB400;
		border-radius: 4px;
		margin-left: 20px;
		width: 93%;
		padding:16px;
		font-family:"Verdana";
		font-size:13px;
		font-style:normal;
		color:#666666;
		background-color:#fff7e5;
	}
	
	span.warning-text{
		font-weight:bolder;
	}
	
	div.warning-checkbox{
		color: #737373;
		margin-top:25px;
	}
	
	div#warning-content{
		margin-bottom:25px;
	}
	
	div#head-AAU-Admin{
		font-weight:bolder;
	}
	
	div#sent-messages-lbl{
		padding:30px 20px 24px;
		font-family:"Verdana";
		font-size:14px;
		font-weight:bolder;
		color:#0888d1;
		float:left;
	}
	
	div#btnCreateNew{
		float:right;
		margin:20px 25px 0 20px;
	}
	
	#buttonBar{
		width:100%;
	}
	
	table#tblListEMS{
		width:96%!important;
		border: 1px solid #CCCCCC;
	}	
	
		
	.tool-tip1{
		background: url("../images/info.png") no-repeat scroll 0px 0px rgba(0, 0, 0, 0);
		display: block;
		opacity: 0.4;
		width: 20px;
		height: 20px;
	}
	.tool-tip1:hover {
		opacity: 1;
	}
	
	#inpAvailableShortCode-menu{
		margin-left:-6px;
		width:381px!important;
	}
	
	.ui-selectmenu-menu ul, ui-selectmenu-item-selected ul li, .ui-state-hover ul li {
    border: 1px solid #AAAAAA !important;

	}
	
	.btn
	{   
    	padding: 0px 5px !important;
	}

	#inpAvailableShortCode-button
	{		
		outline:none;		
	}



</style>




<style type="text/css">
	.reportSummaryTitle {
	    font-weight: bold;
	    align: left
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
			
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;		
	}
			
	#ReportMenu .ui-state-active, #ReportMenu .ui-widget-content #ReportMenu .ui-state-active, #ReportMenu .ui-widget-header #ReportMenu .ui-state-active 
	{
  		background-color: #118ACF; 
		border: none;
		color: #FFFFFF !important;
		outline: medium none;	
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;	
	}

.ui-widget-content {
    background: none repeat scroll 0 0 #FFFFFF;
    border: none;
		
}

..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
  
    border: none;
    color: #313131;
    font-weight: normal;			
}

	.SideSummary
	{ 
		width:100%; 
		height:250px; 
		margin: 0 0 0 0; 
		float:left; 
		overflow-y:auto;">
	}
			
	.reportingContent {
		float: none;
		width: 2000px;
		min-height: 1200px;
		background:none;
		border-radius: 8px 8px 8px 8px;	
		position:static;	
	}
	
	#tblListBatch_1_info
	{
	   width: 300px !important;
	   padding-left: 10px;
	   margin-left: 13px;
	   display: none;
	}


	#chartContent
	{
		background: #fff;
		float:left;
		width: 1200px;		
	}
		
</style>


<style>

	hr.style-five 
	{
		border: 0;
		height: 0; 
		box-shadow: 0 0 3px 1px #CCCCCC;
	}
	
	hr.style-five:after 
	{  
		content: "\00a0";  
	}

	#reportSummaryOverlay {
		background-color: #FFFFFF;
		display: none;
		left: 0;
		opacity: 0.8;
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 90;		
		background: "url(../../public/images/loading.gif) center no-repeat");		
	}
	
	.summaryAnalytics 
	{
		display: inline-block;
		margin: 15px 10px 5px 20px;
		vertical-align: top;
		width: 100%;
	}

	#date-range {
		float: right;
		position: absolute;
		margin-right:15px; 
	}
	
	
	.reportSummaryTitle {
	    font-weight: bold;
	    align: left
	}
	.tableBorderRight {
	    border-right: 2px solid black;
	    text-align: left;
	}
	.contentPaddingLeft {
	    padding-left: 10px;
	}
	.contentPaddingRight {
	    padding-right: 10px;
	}
	.reportList ul li span{		
		cursor:pointer;
	} 
	
	.reportList ul li,  .reportList ul 
	{
		overflow:hidden !important;				
	} 
					
	#ReportMenu .ui-widget-content
	{
		background: none !important;
		
	}
			
	.ui-accordion .ui-accordion-header a {
		display: block;
		font-size: 12px;
		font-weight: bold;
		padding: 0.5em 0.5em 0.5em 30px;
	}
	
		
	.ui-widget-content {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
			
	}
	
	..ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
	  
		border: none;
		color: #313131;
		font-weight: normal;			
	}

	
		
	

		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 16px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
			font-size:12px;
		}
		
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 2px 4px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 2px 4px !important;
		}
		
		.paging_full_numbers {
    		line-height: 16px;
		}

		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}
		
		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}
		
			
		.EBMDataTableWrapper
		{
			height:85%;
			min-height:260px;
			margin-top: 15px;
		}

		.dataTables_wrapper {
			position: relative;
			clear: both;
			zoom: 1; /* Feeling sorry for IE */	
			overflow:auto !important;
			max-height:100%;						
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		
		
		.dashboard2x 
		{
			background-position: center center;
			background-repeat: no-repeat;
			border: nonr;
			<!---float: right;--->
			<!---height: 500px;
			min-height: 415px;
			margin: 5px;
		   <!--- min-width: 560px;--->
			position: static;
			width: 1120px;--->
			
				
			margin: 5px;
		   <!--- min-width: 560px;--->
			position: static;
			
		<!---	
			height: 400px;
			min-height: 400px;
			width: 750px;--->
			
			height: 500px;
			min-height: 500px;
			width: 1120px;
			
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 22px;
			height: 22px;  
			width:625px;
			max-width:625px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 22px; 
			height: 22px; 
		}
		
		.dataTable tbody > tr > td
		{ 
			white-space: nowrap; 
			overflow:hidden !important;
		}

		#tblListQueue 
		{
			<!--- width: 625px; --->			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
		#tblListResults 
		{
			<!--- width: 625px;	 --->		
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}
		
	
	
		.tblListBatch
		{
			width: 300px !Important;			
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
			border-bottom: 1px solid #CCCCCC;
		}
		
		#tblListBatch_1_info
		{
		   width: 300px !important;
		   padding-left: 10px;
		   margin-left: 13px;
		   display: none;
		}
	
	
		.dataTables_filter 
		{
			float: right;
			margin-right: 15px;
			text-align: right;
		}

		.datatable td 
		{
		  overflow: hidden; 
		  text-overflow: ellipsis; 
		  white-space: nowrap;
		}
		
		.rsheader
		{	
			width:1345px; 
			float:left;
		}
		
		.SingleWide
		{
		 	width:650px; 
		 	height:365px;
		}
		
		.DoubleWide
		{
		 	width:1320px; 
		}
		
		.dataTable tbody > tr 
		{
		    min-height: 18px;
			height: 18px;  
			width:417px;
			max-width:417px;
		}
					
		.dataTable tbody > tr > td
		{
		    min-height: 18px; 
			height: 18px; 
		}
		
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:10px;
			color:#666;
		}
				
		.dataTables_length 
		{
		    margin-left: 10px;
		}
		
		
		
table 
{
    table-layout:fixed;
	border-collapse:collapse;
}

td
{
    overflow:hidden;
    text-overflow: elipsis;
	padding: 0 8px 0 8px;
}

th
{
	text-align:left;
	padding: 0 8px 0 8px;	
}

thead tr
{
	background-color: #E9E9CE;	
	
}

.alignRight { text-align: right; }
.alignLeft { text-align: left; }
.alignCenter { text-align: center; }


.DownloadLinkText {
	color: #0063DC !important;
	text-decoration: none !important;
}

.DownloadLink {
	color: #0063DC !important;
	text-decoration: none !important;
}

.excelIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.excelIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_excel.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.wordIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.wordIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_word.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.pdfIcon
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf_over.png) no-repeat;
	height:16px;
	width:16px;	
	text-decoration: none !important;
	float:left;
	margin: 5px 0 0 8px;
}

.pdfIcon:hover
{
	background: url(<cfoutput>#rootUrl#/#publicPath#/</cfoutput>images/icons/grid_pdf.png) no-repeat;
	text-decoration: underline !important;
	cursor:pointer;
	text-decoration: none !important;
}


.dataTables_info {
    background-color: #EEEEEE;
    border-top: 2px solid #46A6DD;
    box-shadow: 3px 3px 5px #888888;
    line-height: 39px;
    margin-left: 0;
    /*padding-left: 10px;*/
    padding-left:0;
    position: relative;
    top: 0 !important;
    width: 100%;
	font-size:12px;
	text-align:left;
	padding-bottom:20px !important;
}

			

</style>

<!--- Dashboard object styles --->
<style>

	.DashboardLeftMenu 
	{
		float: left;
		min-height: 870px;
		width: 350px;
		background-color: #FFFFFF;
    	border: 1px solid #CCCCCC;
	    border-radius: 4px;
	}

	.droppableContentDashboard
	{
		box-shadow: 3px 3px 5px #888888 !important;
		margin: 0 15px 15px 15px;
		padding: 10px 10px 300px 10px;		
		width: 1210px;
		max-width: 1210px;
		min-height:800px;
		border:none !important;
		float:left;
	}
	
	.DashObjHeader 
	{
		background-color: #118ACF;
		border: 1px solid #0C6599;
		border-radius: 4px 4px 0 0;
		color: #FFFFFF;
		font-family: "Verdana";
		font-size: 14px;
		height: 28px;
		min-height: 28px;
		line-height: 28px;
		padding-left: 10px;
		position:relative;	
		cursor: move; 		
	}
	
	.DashObjHeader:hover
	{
		<!---color:#15436C;	--->	
	}
	
<!---	.DashObjHeader:after 
	{  
		content: "\f0c9";  
		display: inline-block;  
		font-family: "FontAwesome";  
		position: absolute;  
		right: 18px;  
		top: 3px;  
		text-align: center;  
		line-height: 28px;  
		color: rgba(255,255,255,.2);  
		text-shadow: 0px 0px 0px rgba(0,0,0,0);  
		cursor: move;  
	}  
	--->

	.DashBoardHeaderMenu
	{
		float: right;
		margin-right:10px;			
	}
		
	.DashBoardHeaderMenu a
	{		
		margin-left:3px;
		color:#118ACF !important; 	
	}
	
	.DashObjHeader:hover a
	{				
		color:#15436C !important; 	
	}
				
	<!--- Class used to distiguish between drop and sort objects --->	
	.DashObj
	{
		
	}
	
	.DashObjPlaceHolder
	{
		width: 575px;
		min-width: 575px;
		height:300px;
		min-height:300px;		
	}
	
	.DashBorder
	{		
    	box-shadow: 3px 3px 5px #888888;
		margin:15px;
	}
	
	.DashObjChart
	{
		width:100%;
		height:100%;	
		position:relative;	
	}
	
	.DashLeft
	{
		float:left;
	}
	
	.DashRight
	{
		float:right;
	}
	
	.DashSize_Width_Small
	{		
		width: 272px;
		min-width: 272px;		
	}
	
	.DashSize_Width_Med
	{		
		width: 575px;
		min-width: 575px;		
	}
	
	.DashSize_Width_Large
	{
		width: 1180px;
		min-width: 1180px;
	}
	
	.DashSize_Height_Tiny
	{
		height:135px;
		min-height:135px;
	}
	
	.DashSize_Height_Small
	{
		height:300px;
		min-height:300px;
	}
	
	.DashSize_Height_Med
	{
		height:575px;
		min-height:575px;
	}
	
	.DashSize_Height_Large
	{
		height:1180px;
		min-height:1180px;
	}	
	
	
	.Trans90
	{
		display:block;	
		transform: rotate(90deg);	 
		-o-transform: rotate(90deg);
		-khtml-transform: rotate(90deg);
		-webkit-transform: rotate(90deg); 
		-moz-transform: rotate(90deg);				
	}
	
	.Trans180
	{
		display:block;
		transform: rotate(180deg);	 
		-o-transform: rotate(180deg);
		-khtml-transform: rotate(180deg);
		-webkit-transform: rotate(180deg); 
		-moz-transform: rotate(180deg);		
	}
	
	.Trans270
	{
		display:block;	
		transform: rotate(270deg);	 
		-o-transform: rotate(270deg);
		-khtml-transform: rotate(270deg);
		-webkit-transform: rotate(270deg); 
		-moz-transform: rotate(270deg);		
	}

	<!--- text based table for sizer --->
	.DashObjSizer
	{	
	 	border:none;	
		table-layout:fixed;
		border-collapse:collapse;	
		font-size:12px;
	}
	
	.DashObjSizer td
	{	
	 	padding: 0 3px 0 3px;		
		margin: 0;
		line-height:9px;
		text-align:center;
	}
	
	.DashObjSizer td:hover
	{	
	 	background:#666666;	
	}
	
	.DashObjSizer tr
	{		 
		line-height:9px;
	}
	
	.DashObjHeaderText
	{
		width:70%; 
		overflow:hidden; 
		display:inline-block;	
		text-align:left;	
	}
	
	.EBMDashCounterWrapper
	{
		
		background-color: #8099b1; <!---#118ACF; rgba(0,51,102,0.5);--->
		text-align:center;		
	}

	.Absolute-Center {
	  width: 90%;
	  height: 60%;
	  overflow: auto;
	  margin: auto;
	  position: absolute;
	  top: 10; left: 0; bottom: 0; right: 0;
	}

	.EBMDashCounterWrapper h1
	{
		color:#FFF;
		font-size:36px;		
	}
	
	.RSHighAlert
	{
		background-color: rgba(255,0,0,0.7); 
		font-size:36px;		
	}
	
	.RSMedAlert
	{
		background-color: rgba(255,255,0,0.7); 
		font-size:36px;		
	}
	
	.EBMDashCounterWrapper h2
	{
		color:#CCC;
		font-size:24px;	
	}

</style>



<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;
	
	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var PriorStartDate = null;
	var PriorEndDate = null;
	
	var DataTable1;
	var DataTable2;
	
	$(function()
	{			
						
		<!--- var oStringMask = new Mask("(###) ###-####");
		 if(document.getElementById("inpContactString")){
			 oStringMask.attach(document.getElementById("inpContactString"));
		 }--->
		 
				
		<!--- Trigger blur to force mask update of DB data --->
		<!---$('#inpContactString').trigger('blur');--->
		<!---// $('#inpDOB').trigger('blur');--->
		
		$("#inpLastSentAsOfDate" ).datepicker({ dateFormat: "mm-dd-yy" });
		 
		$("#inpContactTypeId").selectbox();

		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();
		});
 				
				
		<!--- Update Member Info --->		
		$('#AgentInterfaceContainer #inpUpdateStatus').click(function(){
			
			<!--- Set block SMS to off --->
			$("#inpBlockSend").val(1);
						
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpSubmit').hide();
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingUpdatingStatus").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #UpdateStatus').html('<span style="float: right;">Updating Data...</span>');			
			$('#AgentInterfaceContainer form#ContactHistoryForm').attr("action", "contacthistory_proc");
			$('#AgentInterfaceContainer form#ContactHistoryForm').submit();
		});
		<!---
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmit').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingSendSMS").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMS').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#ContactHistoryForm').attr("action", "contacthistory_proc");
			$('#AgentInterfaceContainer form#ContactHistoryForm').submit();
		});
		
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmitNoConfirm').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			$("#inpSendSMSNoConfirm").val(1);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmit').hide();			
			$("#loadingSendSMSNoConfirm").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMSNoConfirm').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#ContactHistoryForm').attr("action", "contacthistory_proc");
			$('#AgentInterfaceContainer form#ContactHistoryForm').submit();
		});
		--->
		
		
		$('#AgentInterfaceContainer #inpLookup').click(function(){
			window.location = "contacthistory?inpContactString=" + $("#inpContactString").val();
		});
		
		$('#AgentInterfaceContainer #inpReset').click(function(){
			<!--- Form Validation --->
			window.location = "contacthistory";
		});
		
		
		initComponents();
		
		<cfif TRIM(inpContactString) NEQ "">
		
			<!---console.log('inpContactString triggered');--->
			initDataTable($('#droppable_BatchList_'+ "1" + ' #ReportDataTable1'), "ContactStringCampaignHistory", "<cfoutput>#inpContactString#</cfoutput>", "", "", "", "", "");
						
			initDataTable($('#droppable_BatchList_'+ "2" + ' #ReportDataTable2'), "ContactStringGroupHistory", "<cfoutput>#inpContactString#</cfoutput>", "", "", "", "", "");
									
		</cfif>
						
				
	});


	function initDataTable(inpObj, inpReportName, inpcustomdata1, inpcustomdata2, inpcustomdata3, inpcustomdata4, inpcustomdata5, inpCustomConfigDataTablesSort)
	{				
		<!---	"bAutoWidth": false,
			"aoColumns" : [
            { sWidth: '25%' },
            { sWidth: '25%' },
            { sWidth: '25%' },
            { sWidth: '25%' } ],
			
			 "sScrollX": "100%",
		    "sScrollY": "100%",	
			
			
			--->
		
		
		<!--- Set default sorting if not defined in display method --->	
		if(typeof(inpCustomConfigDataTablesSort) == "undefined" || inpCustomConfigDataTablesSort == "")
			inpCustomConfigDataTablesSort = '[[ 0, "desc" ]]';	
					
		inpObj.dataTable( {		
		   
			"bProcessing": true,
			"iDisplayLength": 15,
			"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
			"bLengthChange": true,
			"aaSorting": eval(inpCustomConfigDataTablesSort),
			"bServerSide": true,
			"sPaginationType": "full_numbers",
			"sAjaxSource": "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/reporting.cfc?method=" + inpReportName + "&returnformat=plain&queryformat=column",
			"fnServerData": function ( sSource, aoData, fnCallback ) {
								
					var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
		
					<!--- These value pairs need to be on a seperate line each --->
					aoData.push({"name": "inpBatchIdList", "value": encodeURI("<cfoutput>#inpBatchIdList#</cfoutput>")});
					aoData.push({"name": "inpStart", "value": startDate});
					aoData.push({"name": "inpEnd", "value": endDate});
					
					if(String(inpcustomdata1).length > 0)
						aoData.push({"name": "inpcustomdata1", "value": encodeURI(inpcustomdata1)});
					
					if(String(inpcustomdata2).length > 0)
						aoData.push({"name": "inpcustomdata2", "value": encodeURI(inpcustomdata2)});
					
					if(String(inpcustomdata3).length > 0)
						aoData.push({"name": "inpcustomdata3", "value": encodeURI(inpcustomdata3)});
					
					if(String(inpcustomdata4).length > 0)
						aoData.push({"name": "inpcustomdata4", "value": encodeURI(inpcustomdata4)});
						
					if(String(inpcustomdata5).length > 0)
						aoData.push({"name": "inpcustomdata5", "value": encodeURI(inpcustomdata5)});		
						
   								
					$.getJSON( sSource, aoData, function (json) { 
						<!---console.log(json);--->
                        fnCallback(json);
                    });			
              },
			  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                    $('td', nRow).attr('nowrap','nowrap');
                    
					<!---$('td', nRow).css('text-decoration','underline');--->
					
					<!---if(iDisplayIndex == 2)
						$('td', nRow).css('text-decoration','underline');--->
		
		
					return nRow;
					
					
					
               },
			   "fnInitComplete":function(oSettings, json){
										
					inpObj.find('thead tr th').each(function(index){
															
						$this = $(this);
						if($this.hasClass("nosort"))
						{				
							<!--- disable sorting on column "1" --->
							oSettings.aoColumns[$this.index()].bSortable = false;
							<!--- hide arrows --->
							$this.css("background","none");
						}
						
					  });
				  
												
					
				},
			   "fnDrawCallback": function( oSettings ) {
					
					<!--- For tables that are hidden overflow - add full text to title --->								
					inpObj.find('tbody tr td').each(function(index){
																
						$this = $(this);
						var titleVal = $this.text();
						if (titleVal != '') {
						  $this.attr('title', titleVal);
						}
					  });
					  			  
					$(".CPPLink").unbind();
					$(".CPPLink").click(function() {
					
						$('#CPPDialog').append($("<h4>CPP ID: " + $(this).attr('rel4')  + "</h4><h4>Account Id: " + $(this).attr('rel2') + "</h4><h4>Contact String: " + $(this).attr('rel3') + "</h4><iframe style='margin:25px;' />").attr(
								{
									src: $(this).attr('rel1') ,
									scrolling: "auto",
									align:"left",
									height:"825px",
									width:"800px",
									frameborder:"0" 
									// id="CPPReviewFrame"									 									 
									
								})).dialog({ 
							autoOpen: true,
							modal: true,
							height: "auto",
							width: 900,
							position: { my: "left top", at: "left bottom", of: $(this), collision: 'none' },
							title: 'CPP Agent Impersonate',
							close: function(event, ui) {
								  $('#CPPDialog').empty();								  
								  $('#droppable_BatchList_'+ "2" + ' #ReportDataTable2').dataTable()._fnAjaxUpdate();								  
                    			}
			  
							<!---// buttons: { "Close": function () { $(this).dialog('destroy'); } },	--->		
							//  
						});
					
					});
			
					<!---$(".CPPLink").click(function() {
						
						$("#dialog").append($("<iframe />").attr("src", $(this).attr('rel1') )).dialog({dialogoptions});
						
						window.location = $(this).attr('rel1');
					});--->
					
								  
					<!--- Init Download Link(s) They are contained in the parent two levels up - .parents().eq(1) is the same as .parent().parent()--->
					inpObj.parents().eq(2).find($(".DownloadLink")).unbind();
					inpObj.parents().eq(2).find($(".DownloadLink")).click(function() {
		  
			  	    var dateRange = $('#datepicker-calendar').DatePickerGetDate();
					var startDate = convertDateToTimestamp(dateRange[0][0]);
					var endDate = convertDateToTimestamp(dateRange[0][1]);
					
					var CustomParams = "";
					
					if(typeof($(this).attr("inpcustomdata1")) != "undefined")
						CustomParams += "&inpcustomdata1=" + encodeURIComponent($(this).attr("inpcustomdata1")) ;
						
					if(typeof($(this).attr("inpcustomdata2")) != "undefined")
						CustomParams += "&inpcustomdata2=" + encodeURIComponent($(this).attr("inpcustomdata2")) ;
						
					if(typeof($(this).attr("inpcustomdata3")) != "undefined")
						CustomParams += "&inpcustomdata3=" + encodeURIComponent($(this).attr("inpcustomdata3")) ;		
					
					if(typeof($(this).attr("inpcustomdata4")) != "undefined")
						CustomParams += "&inpcustomdata4=" + encodeURIComponent($(this).attr("inpcustomdata4")) ;	
						
					if(typeof($(this).attr("inpcustomdata5")) != "undefined")
						CustomParams += "&inpcustomdata5=" + encodeURIComponent($(this).attr("inpcustomdata5")) ;	
							
					      <!---console.log("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>"))--->
						window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + "&inprel2=" + encodeURIComponent($(this).attr("rel2")) + "&inpBatchIdList=<cfoutput>#inpBatchIdList#</cfoutput>&inpStart=" + encodeURIComponent(startDate) + "&inpEnd=" + encodeURIComponent(endDate) + CustomParams + "&UK=" + encodeURIComponent("<cfoutput>#NOW()#</cfoutput>");
										  
				<!---	  	OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/reporting/dsp_exportcsv?inprel1=" + encodeURIComponent($(this).attr("rel1")) + '',
						"Export Table Data", 
						280, 
						500,
						"ExportTableDataForm",
						false);			--->  
					  
				  });
									
				
				  inpObj.find($("td")).mousewheel(function(event, delta) {
				
					<!---console.log('MouseWheel=' + delta);--->
					
					  this.scrollLeft -= (delta * 30);
					
					  event.preventDefault();
				
				   });
		
				}
	
			
    	} );
						
	}
	
	
	function initComponents(){
		
		<!---/* Special date widget */--->
		var to = new Date();
		var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
		if($("#MaxBoundaryDate").html() != "")
		{
			from = new Date($("#MinBoundaryDate").html());
			to = new Date($("#MaxBoundaryDate").html());
		}
		
   	    <!--- This widget was manually customized for EBM with preset date range options in the raw #rootUrl#/#publicPath#/js/datepicker/datepicker.js --->
       $('#datepicker-calendar').DatePicker({
			inline: true,
			date: [from, to],
			calendars: 3,
			mode: 'range',
			extraWidth: 100,
			current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
			onChange: function(dates, el){					
				
				// update the range display
				$('#date-range-field span').text(dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ', ' + dates[0].getFullYear() + ' - ' +
				dates[1].getDate() +
				' ' +
				dates[1].getMonthName(true) +
				', ' +
				dates[1].getFullYear());
									
				$("#SelectedMinBoundaryDate").html(days[dates[1].getDay()] + ', ' + dates[1].getMonthName(true) + ' ' + dates[1].getDate() + ', ' + dates[1].getFullYear());
				$("#SelectedMinBoundaryDate").attr("rel1", dates[1].getFullYear() + '-' + dates[1].getMonth() + '-' + dates[1].getDate());
				$("#SelectedMaxBoundaryDate").html(days[dates[0].getDay()] + ', ' + dates[0].getMonthName(true) + ' ' + dates[0].getDate() + ', ' + dates[0].getFullYear());
				$("#SelectedMaxBoundaryDate").attr("rel1", dates[0].getFullYear() + '-' + dates[0].getMonth() + '-' + dates[0].getDate());
			}
		});
		
		<!---// initialize the special date dropdown field--->
		$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
		to.getDate() +
		' ' +
		to.getMonthName(true) +
		', ' +
		to.getFullYear());
		
		<!---// bind a click handler to the date display field, which when clicked
		// toggles the date picker calendar, flips the up/down indicator arrow,
		// and keeps the borders looking pretty--->
		$('#date-range-field').bind('click', function()
		{
			var dateRange = $('#datepicker-calendar').DatePickerGetDate();
								
			<!--- Check if opening or closing range picker --->				
			if($('#datepicker-calendar').is(":visible"))
			{
				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
			}
			else
			{	
							
				PriorStartDate = days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear();
				PriorEndDate = days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear();										

				$('#datepicker-calendar').toggle();
				$('#reportSummaryOverlay').toggle();
				
			}
				
			if ($('#date-range-field a').text().charCodeAt(0) == 9650) 
			{
				// switch to up-arrow
				$('#date-range-field a').html('&#9660;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 0,
					borderBottomRightRadius: 0
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 0
				});
			}
			else 
			{
				// switch to down-arrow
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			}
			return false;
		});
		
		
		<!--- Special easy to select preset values --->
		<!---$('#EBMPBFull').bind('click', function()
		{							
			var from = new Date($("#MinBoundaryDate").html());
			var to = new Date($("#MaxBoundaryDate").html());
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());

		});--->
					
		$('#EBMPBLastSeven').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 7); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
		
		});
		
		$('#EBMPBLastThirty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastSixty').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 60); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
		});
		
		$('#EBMPBLastNinety').bind('click', function()
		{							
			var to = new Date();
			var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 90); 
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);		
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
								
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
				
		});
		
		<!--- Allow user to cancel and revert changes in date picker --->
		$('#EBMPresetsCancel').bind('click', function()
		{		
			var to = new Date(PriorEndDate);
			var from = new Date(PriorStartDate);
			
			$('#datepicker-calendar').DatePickerSetDate([from, to], true);
			
			// update the range display
			$('#date-range-field span').text(from.getDate() + ' ' + from.getMonthName(true) + ', ' + from.getFullYear() + ' - ' +
			to.getDate() +
			' ' +
			to.getMonthName(true) +
			', ' +
			to.getFullYear());
			
			$("#SelectedMinBoundaryDate").html(days[from.getDay()] + ', ' + from.getMonthName(true) + ' ' + from.getDate() + ', ' + from.getFullYear());
			$("#SelectedMinBoundaryDate").attr("rel1", from.getFullYear() + '-' + from.getMonth() + '-' + from.getDate());
			$("#SelectedMaxBoundaryDate").html(days[to.getDay()] + ', ' + to.getMonthName(true) + ' ' + to.getDate() + ', ' + to.getFullYear());
			$("#SelectedMaxBoundaryDate").attr("rel1", to.getFullYear() + '-' + to.getMonth() + '-' + to.getDate());
	
			$('#datepicker-calendar').hide();
			$('#reportSummaryOverlay').hide();
		
		});
		
					 
	   <!--- // global click handler to hide the widget calendar when it's open, and
		// some other part of the document is clicked.  Note that this works best
		// defined out here rather than built in to the datepicker core because this
		// particular example is actually an 'inline' datepicker which is displayed
		// by an external event, unlike a non-inline datepicker which is automatically
		// displayed/hidden by clicks within/without the datepicker element and datepicker respectively--->
		$('html').click(function(){
							
			if ($('#datepicker-calendar').is(":visible")) 
			{
				
				$('#datepicker-calendar').hide();
				$('#reportSummaryOverlay').hide();
				$('#date-range-field a').html('&#9650;');
				$('#date-range-field').css({
					borderBottomLeftRadius: 5,
					borderBottomRightRadius: 5
				});
				$('#date-range-field a').css({
					borderBottomRightRadius: 5
				});
			
				var dateRange = $('#datepicker-calendar').DatePickerGetDate();
			
				<!--- Redraw the charts if the date-picker has changed as is closing --->	
				if(
					PriorStartDate != days[dateRange[0][0].getDay()] + ', ' + dateRange[0][0].getMonthName(true) + ' ' + dateRange[0][0].getDate() + ', ' + dateRange[0][0].getFullYear() ||
					PriorEndDate !=  days[dateRange[0][1].getDay()] + ', ' + dateRange[0][1].getMonthName(true) + ' ' + dateRange[0][1].getDate() + ', ' + dateRange[0][1].getFullYear()   
				  )	
					drawChart();
										
			}
		});
		
	   <!--- // stop the click propagation when clicking on the calendar element
		// so that we don't close it--->
		$('#datepicker-calendar').click(function(event)
		{
			event.stopPropagation();
		});	
		<!--- /* End special page widget */--->
					
		
	}
	
	
	function convertDateToTimestamp(date){
		return date.getFullYear() + '-' + (date.getMonth() +1) + '-' +date.getDate();
	}
	
	function drawChart()
	{
		<cfif TRIM(inpContactString) NEQ "">
			
			<!---console.log('inpContactString triggered');--->
			$('#droppable_BatchList_'+ "1" + ' #ReportDataTable1').dataTable()._fnAjaxUpdate();
			<!---$('#droppable_BatchList_'+ "2" + ' #ReportDataTable2').dataTable()._fnAjaxUpdate();--->
				
		</cfif>
		
	}
	
	function Left(str, n){
		if (n <= 0)
		    return "";
		else if (n > String(str).length)
		    return str;
		else
		    return String(str).substring(0,n);
	}
	
	function Right(str, n){
	    if (n <= 0)
	       return "";
	    else if (n > String(str).length)
	       return str;
	    else {
	       var iLen = String(str).length;
	       return String(str).substring(iLen, iLen - n);
	    }
	}
	
	
	function DeleteContactsFromGroup(INPCONTACTSTRING, inpContactType, INPGROUPID, INPCONTACTID) {
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to remove this contact?\n(" + displayList + ") ";
		msg2 = "Remove contacts from this list.";		
				
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();	
					
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromGroup&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING), INPCONTACTTYPE: inpContactType, INPGROUPID: INPGROUPID, INPCONTACTID: INPCONTACTID},
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){
								
								<!--- Refresh data --->
								$('#droppable_BatchList_'+ "2" + ' #ReportDataTable2').dataTable()._fnAjaxUpdate();
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	
	function DeleteContactsFromAllGroup(INPCONTACTSTRING) {
		<!--- Dont go overboard on displaying too much info --->
		var displayList =  Left(INPCONTACTSTRING, 50);
		if (String(INPCONTACTSTRING).length > 50) {
			displayList = displayList + " (...)";
		}
		
		var msg1 = '';
		var msg2 = '';
			
		msg1 = "Are you sure you want to remove this contact from all Groups?\n(" + displayList + ") ";
		msg2 = "Remove contacts from all Groups.";		
				
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';		
		$.alerts.confirm( msg1, msg2, function(result) { 
			if (!result) {
				$("#loadingPhoneList").hide();   
				return;
			}
			else {			
				$("#loadingPhoneList").show();	
					
					
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->				  
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/MultiLists2.cfc?method=RemoveContactFromAllGroups&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPCONTACTSTRING: String(INPCONTACTSTRING) },
					success:
					function(d2){
						if(d2.ROWCOUNT > 0){							
							if(parseInt(d2.DATA.RXRESULTCODE) == 1){								
								<!--- Refresh data --->
								$('#droppable_BatchList_'+ "2" + ' #ReportDataTable2').dataTable()._fnAjaxUpdate();
							}						
						}  
					}
				});
						
		 	}  
		});
		return false;
	}
	
	
</script>

<cfoutput>


	
        

	<div id="AgentInterfaceContainer" class="stand-alone-content" style="width:850px;">
        <div class="EBMDialog">

            <form method="POST" id="ContactHistoryForm">
            
            	<input type="hidden" id="inpBlockSend" name="inpBlockSend" value="1" />
                <input type="hidden" id="inpSendSMSNoConfirm" name="inpSendSMSNoConfirm" value="0" />


				<cfif TRIM(inpContactString) EQ "">
                    <div class="header">
                        <div class="header-text" style="padding-top:1px !important;">Lookup Contact History</div>                         
                    </div>
                    
                <cfelse>
                	<input type="hidden" id="inpContactString" name="inpContactString" value="#inpContactString#" />
                
                 	<div class="header">
                        <div class="header-text" style="padding-top:1px !important;">Current Contact Information - <span class="MIDHighlight">#inpContactString#</span> <span style="padding-bottom:5px; margin-right:10px; float:right;"><a href="##" class="button filterButton small" id="inpReset" >Start Over</a></span></div>                         
                    </div>
                	                            
				</cfif>
                
                
                <div class="inner-txt-box">

					 
                     
					<!---<cfif TRIM(inpContactString) EQ "">

                        <div class="inner-txt-hd">Lookup Member Information</div>
                        <div class="inner-txt"></div>
                        
					<cfelse>
                    
                    	<div class="inner-txt-hd">Current Member Information</div>
                        <div class="inner-txt"></div>
                        
					</cfif>--->
                    
                    
                    <div class="" style="float:left; height:auto; width:650px; margin-top:8px;">
                       
                       
                       <cfif TRIM(inpContactString) EQ "">
                           
                           	<div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpContactString">Enter Contact String<span class="small"><!---Required---></span></label>
                                <input id="inpContactString" name="inpContactString" placeholder="Enter Contact String Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#inpContactString#" />
                            </div>
                            
    					    <div style="clear:both"></div>
                            
                            <div class="inputbox-container" style="margin-top:1px;">
                                <BR />
                                <a href="##" class="button filterButton small" id="inpLookup" >Lookup Contact History</a>
                            </div>
                      
                      <cfelse>
                      
                     		<!---<div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpContactString">Current Member ID</label>
                                <input type="hidden" id="inpContactString" name="inpContactString" value="#inpContactString#" />
                           		<span class="MIDHighlight">#inpContactString#</span>
                            </div>
							
							<div style="margin-top:3px; width:300px; float:right;">
                                <a href="##" class="button filterButton small" id="inpReset" >Start Over - Re-Enter Member Id</a>
                            </div>
							
							--->                            
                      
                      </cfif>		                               
                                             
                       <cfif TRIM(inpContactString) NEQ "">
                       
                       		<cfinvoke method="createUserLog" component="#Session.SessionCFCPath#.administrator.usersLogs">
                                <cfinvokeargument name="userId" value="#session.userid#">
                                <cfinvokeargument name="moduleName" value="Contact History Lookup">
                                <cfinvokeargument name="operator" value="#TRIM(inpContactString)#">
                            </cfinvoke>
                    
                       
                        	<div class="content-EMS SideSummary no-print">
                               
                                <div class="head-EMS">Search Range</div>
                                
                                <div class="summaryAnalytics"> 
                                    <div id="date-range">
                                        <div id="date-range-field">
                                            <span>
                                            </span>
                                            <a href="javascript:void(0)">
                                                &##9660;
                                            </a>
                                        </div>
                                        <!--- This widget was manually customized for EBM with preset date range options in the raw #rootUrl#/#publicPath#/js/datepicker/datepicker.js --->
                                        <div id="datepicker-calendar">
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="summaryAnalytics" style="margin-top:40px;"> 
                                    <span class="reportSummaryTitle">
                                        Currently Selected Range
                                    </span>
                                    
                                    <BR />
                                     
                                    <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
                                    <span id="SelectedMinBoundaryDate" rel1="">#inpStart#</span>   
                                   
                                    <BR />
                                    
                                    <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
                                    <span id="SelectedMaxBoundaryDate" rel1="">#inpEnd#</span>
                                                     
                                </div>
                                
                                
                                <div class="summaryAnalytics" style="visibility:hidden;"> 
                                    <span class="reportSummaryTitle">
                                        Overall Campaign Range
                                    </span>
                                    
                                    <BR />
                                    
                                    <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">From: </span>
                                    <span id="MinBoundaryDate" rel1="">#inpStart#</span>   
                                    
                                    <BR />
                                    
                                    <span style="float:left; text-align:right; min-width:100px; margin-right:10px;">To: </span>
                                    <span id="MaxBoundaryDate" rel1="">#inpEnd#</span>
                                         
                                </div>
                                   
                            </div>
                                     
                            <div style="clear:both"></div>
                                                       
                            <cfset inpBatchIdList = "" />
                            <cfset inpChartPostion = "1" />
                            <cfset inpcustomdata1 = "#inpContactString#" />
                            
                            <!--- Build javascript objects here - include any custom data parameters at the end --->
							<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_ContactStringCampaignHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
                            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_ContactStringCampaignHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
                            
                            <div id='droppable_BatchList_#inpChartPostion#' style="margin-top:20px;">
                                       
								<!--- This outer wrapper is two parents up from ReportDataTable1 - this is important for daownload links to be initialized--->
                                <div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                                    
                                    <div class="head-EMS"><div class="DashObjHeaderText">Contact History #inpcustomdata1#</div></div>
                                    
                                    <!--- Pass in extra data to set ddefault sorting you would like --->
                                    <input type="hidden" name="ExtraDTInitSort_#inpChartPostion#" id="ExtraDTInitSort_#inpChartPostion#" value='[[ 3, "desc" ]]' >
                                                    
                                    <!--- Keep table auto-scroll in managed area--->
                                    <div class="EBMDataTableWrapper">
                                    
                                        <!--- ID must be ReportDataTable1 for all reports - this is used by CSS and jQuery to locate stuu--->     
                                        <table id="ReportDataTable1" style="clear:both; width:100%;" border="0">
                                            <thead>
                                                <tr>
                                                    <th width="20%" style="width:20%;">Contact String</th>
                                                    <th width="20%" style="width:20%;">Last Contact</th>
                                                    <th width="20%" style="width:20%;">Campaign</th>                            
                                                    <th width="20%" style="width:20%;">Date</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <tr>
                                                    <td colspan="5" class="datatables_empty">Loading data from server</td> 
                                                </tr>        
                                            </tbody>            
                                        </table>                
                                    
                                    </div>
                                                    
                                    <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                                        <!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                                        <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                                       <div class="DownloadLink excelIcon" rel1="ContactStringCampaignHistory" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#"></div>
                                       <div class="DownloadLink wordIcon" rel1="ContactStringCampaignHistory" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#"></div>
                                       <div class="DownloadLink pdfIcon" rel1="ContactStringCampaignHistory" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#"></div>                                   
                                    </div>
                                </div>
                       
	                       	</div>
                           
                           	<div style="clear:both"></div>                                                      
                            
                         <!---   <!--- Build javascript objects here - include any custom data parameters at the end --->
							<cfset inpDataURP = "{inpBatchIdList: '#inpBatchIdList#', inpReportName: 'Form_ContactStringGroupHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
                            <cfset inpDataGC = "{inpBatchIdList: '#inpBatchIdList#', inpStart: '#inpStart#', inpEnd: '#inpEnd#', inpChartName: 'Form_ContactStringGroupHistory', inpReportType: 'FORM', inpChartPostion: #inpChartPostion#, inpcustomdata1: '#inpcustomdata1#'}" />
                        --->    
                            <div id='droppable_BatchList_2' style="margin-top:20px;">
                                       
								<!--- This outer wrapper is two parents up from ReportDataTable2 - this is important for daownload links to be initialized--->
                                <div style="width:100%; height:100%; position:relative;" class="DataTableContainerX content-EMS">
                                    
                                    <div class="head-EMS"><div class="DashObjHeaderText">Current Groups for #inpcustomdata1#</div></div>
                                    
                                    <!--- Pass in extra data to set ddefault sorting you would like --->
                                    <input type="hidden" name="ExtraDTInitSort_2" id="ExtraDTInitSort_2" value='[[ 3, "desc" ]]' >
                                                    
                                    <!--- Keep table auto-scroll in managed area--->
                                    <div class="EBMDataTableWrapper">
                                    
                                        <!--- ID must be ReportDataTable2 for all reports - this is used by CSS and jQuery to locate stuu--->     
                                        <table id="ReportDataTable2" style="clear:both; width:100%;" border="0">
                                            <thead>
                                                <tr>
                                                    
                                                    <th width="20%" style="width:20%;">CPP AID</th>
                                                    <th width="20%" style="width:20%;">CPP</th>                            
                                                    <th width="25%" style="width:25%;">Contact String</th>
                                                    <th width="30%" style="width:30%;">Group</th>
                                                    <th width="5%" style="width:5%;" class="nosort">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <tr>
                                                    <td colspan="5" class="datatables_empty">Loading data from server</td> 
                                                </tr>        
                                            </tbody>            
                                        </table>                
                                    
                                    </div>
                                                    
                                    <div style="font-size:12px; text-align:left; clear:both; position:absolute; bottom:5px; width:100%; ">
                                        <!--- You must specify the name of the method that contains the query to output - dialog uses special param OutQueryResults=1 to force query output instead of table output --->
                                        <!--- src="#LocalProtocol#://#CGI.SERVER_NAME#/#PublicPath#/images/icons/grid_excel_over.png" --->
                                       <div class="DownloadLink excelIcon" rel1="ContactStringGroupHistory" rel2="CSV" title="Download Tabular Data as CSV" inpcustomdata1="#inpcustomdata1#"></div>
                                       <div class="DownloadLink wordIcon" rel1="ContactStringGroupHistory" rel2="WORD" title="Download Tabular Data as Word Document" inpcustomdata1="#inpcustomdata1#"></div>
                                       <div class="DownloadLink pdfIcon" rel1="ContactStringGroupHistory" rel2="PDF" title="Download Tabular Data as PDF" inpcustomdata1="#inpcustomdata1#"></div>                                   
                                    </div>
                                </div>
                       
	                       	</div>
                            
                            <div style="clear:both"></div>
               
                            <div id="buttonBar" style="margin-bottom:25px;">
                                <div id="btnCreateNew" class="button filterButton small" onclick="DeleteContactsFromAllGroup('#inpContactString#');">Nuclear - Remove contact string from ALL groups</div>
                            </div>
                            
                            <div style="clear:both"></div>
                             
                            <div class="MSGAlert">#inpMSG#</div>
                                       
                		</cfif>
                                       
                    </div>
                </div>

                <div style="clear:both"></div>
                
               
               
            </form>

        </div>
	</div>
    
    
    <div id="CPPDialog" style="display:none; margin:25px;"></div>
</cfoutput>


