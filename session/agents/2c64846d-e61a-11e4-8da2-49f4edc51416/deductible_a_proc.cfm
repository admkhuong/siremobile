<!--- Page is re-entrant - Lookup, Add, Update --->
<!--- Not AJAX in case session expires - page load will recover cookie where CFC will not in EBM--->
<cfparam name="VerboseDebug" default="0">
<cfparam name="inpBlockSend" default="1">
<cfparam name="inpSendSMSNoConfirm" default="0">
<cfparam name="inpMID" default="">
<cfparam name="inpDOB" default="">
<cfparam name="inpPKId" default="">
<cfparam name="inpStatus" default="1">
<cfparam name="inpFirstName" default="">
<cfparam name="inpLastName" default="">
<cfparam name="inpContactString" default="">
<cfparam name="inpContactTypeId" default="3">
<cfparam name="inpLastSentDeductableIn" default="">
<cfparam name="inpLastSentDeductableInMet" default="">
<cfparam name="inpLastSentDeductableOut" default="">
<cfparam name="inpLastSentDeductableOutMet" default="">
<cfparam name="inpLastSentAsOfDate" default="#NOW()#">
<cfparam name="inpProgramNotes" default="">


<cfset inpMSG = "" />

<!--- Validate Security--->
<cfinvoke component="#LocalSessionDotPath#.cfc.agentportals" method="AuthenticateUser" returnvariable="getAuth">
    <cfinvokeargument name="INPPORTALID" value="2005">
</cfinvoke>

<!---<cfdump var="#getAuth#" />--->

<cfif getAuth.RXRESULTCODE NEQ 1 >

	<cfset inpMSG = "You do not have permissions to post to this page!" />
   
   	<cfif VerboseDebug EQ "0" > 
                               
        <cfif TRIM(inpMSG) NEQ "">
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#&inpMSG=#inpMSG#" />
        <cfelse>
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#" />
        </cfif>
    </cfif>

</cfif>

<cfif NOT AgentToolsPermission.havePermission>

	<cfset inpMSG = "You do not have permissions to post to this page!<BR />Contact your company administrator if you need privileges turned on." />
	 
     <cfif VerboseDebug EQ "0" > 
                               
        <cfif TRIM(inpMSG) NEQ "">
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#&inpMSG=#inpMSG#" />
        <cfelse>
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#" />
        </cfif>
        
    </cfif>

</cfif>





<cfset inpMSG = "" />

<cfset INPCONTACTSTRINGCLEAN = inpContactString />

<!--- Phone number validation--->                 
<cfif INPCONTACTTYPEID EQ 1>  
	<!---Find and replace all non numerics except P X * #--->
	<cfset INPCONTACTSTRINGCLEAN = REReplaceNoCase(INPCONTACTSTRING, "[^\d^\*^P^X^##]", "", "ALL")>
</cfif>

<!--- email validation --->
<cfif INPCONTACTTYPEID EQ 2>  
	<!--- Add email validation --->    
</cfif>

<!--- SMS validation--->
<cfif INPCONTACTTYPEID EQ 3>  
	<!---Find and replace all non numerics except P X * #--->
	<cfset INPCONTACTSTRINGCLEAN = REReplaceNoCase(INPCONTACTSTRING, "[^\d]", "", "ALL")>
</cfif>
                    

<cfset inpLastSentAsOfDate = "#dateformat(inpLastSentAsOfDate,'yyyy-mm-dd')#" & " " & "00:00:00">


<!---<cfdump var="#inpDOB#" />
<cfdump var="#IsValid("USdate", inpDOB)#" />--->

<cfif TRIM(inpDOB) NEQ "">
	
    <cfif IsValid("USdate", inpDOB)>
    	<cfset inpDOB = "#dateformat(inpDOB,'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfelse>
	    <cfset inpDOB = "">
    </cfif>
	
</cfif>

<!---<cfdump var="#inpDOB#" />

<cfabort />--->




<!--- Debugging delay o verify buttons disappear --->
<!---<cfscript>
    thread = CreateObject("java", "java.lang.Thread");
    thread.sleep(4000);
</cfscript>--->

<!---
<!--- Does member ID exist --->
<cfquery name="GetMemberInformation" datasource="#Session.DBSourceEBM#">
   SELECT 
        COUNT(*) AS TOTALCOUNT
    FROM 
        simplexprogramdata.portal_link_c8_p1
    WHERE
        MemberId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpMID)#">
</cfquery>    --->
       
    <!--- If sending message than update appropriately--->
    <cfif inpStatus EQ 1>
    
    	<cfif inpBlockSend EQ 0>
        
        	<cfif inpSendSMSNoConfirm EQ 1>
    			<cfset inpStatus = 3 />
            <cfelse>
            	<cfset inpStatus = 2 />
    		</cfif>
        
        </cfif>
        
    </cfif>   
       
	<!--- Don't Update existing record always insert new to be able to track changes --->
	
    <!--- Insert New record - track agent?--->
    <cfquery name="addProgramInfo" datasource="#Session.DBSourceEBM#" result="newCom">
        INSERT INTO
            simplexprogramdata.portal_link_c8_p1
            (
                <!---PKId_int,--->
                MemberId_vch,
                DateOfBirth_dt,
                Status_int,
                FirstName_vch,
                LastName_vch,
                LastSent_dt,
                ContactString_vch,
                ContactTypeId_int,
                LastSentDeductableIn_vch,
                LastSentDeductableInMet_vch,
                LastSentDeductableOut_vch,
                LastSentDeductableOutMet_vch,
                LastSentAsOfDate_dt,
                ProgramNotes_vch,
                LastAgentUserId_int
            )
        VALUES
            (
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LEFT(inpMID, 255))#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpDOB#" null="#TRIM(inpDOB) EQ ''#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpStatus#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LEFT(inpFirstName, 1024))#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(LEFT(inpLastName, 1024))#">,
                Now(),
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(INPCONTACTSTRINGCLEAN)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#inpContactTypeId#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastSentDeductableIn)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastSentDeductableInMet)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastSentDeductableOut)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpLastSentDeductableOutMet)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#inpLastSentAsOfDate#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpProgramNotes)#">,
                <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.UserId#">
            )
    </cfquery>
                  
    <cfif inpBlockSend EQ 0>
    
    	<cfif inpSendSMSNoConfirm EQ 1>
                  
			<!--- Batch Id 1255--->                
            <cfset inpBatchId = "1555" />
            <cfset inpMSG = "Monthly Update Sent" />
           
    	<cfelse>
        
        	<!--- Batch Id 1254--->  
            <cfset inpSkipLocalUserDNCCheck = 1 />              
            <cfset inpBatchId = "1554" />
            <cfset inpMSG = "Program Start Sent" />
            
                   
    	</cfif>
                
		<!--- Do this once - separated for quicker looping where needed --->
        <cfinclude template="authsetup.cfm" />
        
        <!--- Post to API --->
        <cfinclude template="sendsinglesms.cfm" />
        
    <cfelse>
    
    	<cfset inpMSG = "Program Member Information Updated" /> 
            
    </cfif>            

    
    <cfif VerboseDebug EQ "0" > 
                               
        <cfif TRIM(inpDOB) NEQ "">               
        	<cfset inpDOB = "#dateformat(inpDOB,'mm-dd-yyyy')#">
        </cfif>
                               
        <cfif TRIM(inpMSG) NEQ "">
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#&inpMSG=#inpMSG#" />
        <cfelse>
            <cflocation addtoken="no" url="deductible_a?inpMID=#inpMID#&inpDOB=#inpDOB#" />
        </cfif>
        
        
    </cfif>

