<cfparam name="inpContactTypeId" default="3">
<cfparam name="inpMID" default="">
<cfparam name="inpDOB" default="">
<cfparam name="inpMSG" default="">

<!--- Not AJAX in case session expires - page load will recover cookie where CFC will not in EBM--->

<!--- Validate Security--->
<cfinvoke component="#LocalSessionDotPath#.cfc.agentportals" method="AuthenticateUser" returnvariable="getAuth">
    <cfinvokeargument name="INPPORTALID" value="2006">
</cfinvoke>

<!---<cfdump var="#getAuth#" />--->

<cfif getAuth.RXRESULTCODE NEQ 1 >

	<h2>You do not have permissions to view this page!</h2>
    <cfabort />

</cfif>

<cfif NOT AgentToolsPermission.havePermission>

	<h2>You do not have permissions to view this page!<BR />Contact your company administrator if you need privileges turned on.</h2>
    <cfabort />

</cfif>

<!---
UnitedHealthcare Monthly Msg:
As of {%inpAsOfDate%} deductible met:
In Network $ {%inpDeductableInMet%} of $ {%inpDeductableIn%} 
Out of Network $ {%inpDeductableOutMet%} of $ {%inpDeductableOut%}. 
Text STOP to stop.

--->

<cfoutput>

	<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>

</cfoutput>

<!--- Lock down page to authenticated users either company admin or linked access user id --->



<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Agent tools <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Custom Agent Interfaces</cfoutput>');
	$('#subTitleText').html('Send Prescription Refill Notice');
</script>


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	#AgentInterfaceContainer .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}


	#AgentInterfaceContainer .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);
		position:absolute;
		top: -25px;
		right: 37px;
	}

	#AgentInterfaceContainer .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#AgentInterfaceContainer .form-right-portal {
    z-index: 1000;
}


	.sbHolder
	{
		width:266px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px 3px 3px 3px;
	}

	<!--- 30 less than sbHolder --->
	.sbSelector
	{
		width:246px;
	}

	.sbOptions
	{
		max-height:250px !important;
	}

	.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}

		.datatable
		{
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}

		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		.wrapper-picker-container{
			 margin-left: 20px;
		}


		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 230px;
		}
		
		.MIDHighlight
		{
			color:#F90;
			font-size:18px;			
		}
		
		.MSGAlert
		{
			color:#F00;
			font-size:18px;	
			width:100%;
			text-align:center;	
			margin-top:8px;	
		}

		<!--- Tighten up form for smaller screens --->
		.EBMDialog .inputbox-container 
		{
			margin-bottom: 6px;
		}
		
		.EBMDialog form input 
		{ 
			margin-bottom: 2px;
			padding: 2px 0;
			width: 100%;
		}
	
		.EBMDialog label 
		{
			margin-bottom: 2px;
		}
		
		
		.EBMDialog .header-text {
			line-height:28px;
		}

		.EBMDialog .submit a {
			float: left;
		}
						
</style>


<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;

	$(function()
	{				
		 var oStringMask = new Mask("(###) ###-####");
		 if(document.getElementById("inpContactString")){
			 oStringMask.attach(document.getElementById("inpContactString"));
		 }
		 
		 var oStringMaskDOB = new Mask("##-##-####");
		 if(document.getElementById("inpDOB")){
			 oStringMaskDOB.attach(document.getElementById("inpDOB"));
		 }
		
		<!--- Trigger blur to force mask update of DB data --->
		$('#inpContactString').trigger('blur');
		// $('#inpDOB').trigger('blur');
		
		$("#inpLastSentAsOfDate" ).datepicker({ dateFormat: "mm-dd-yy" });
		 
		$("#inpContactTypeId").selectbox();

		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();
		});
 				
				
		<!--- Update Member Info --->		
		$('#AgentInterfaceContainer #inpUpdateStatus').click(function(){
			
			<!--- Set block SMS to off --->
			$("#inpBlockSend").val(1);
						
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpSubmit').hide();
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingUpdatingStatus").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #UpdateStatus').html('<span style="float: right;">Updating Data...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmit').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingSendSMS").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMS').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmitNoConfirm').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			$("#inpSendSMSNoConfirm").val(1);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmit').hide();			
			$("#loadingSendSMSNoConfirm").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMSNoConfirm').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		
		
		$('#AgentInterfaceContainer #inpLookup').click(function(){
			window.location = "deductible_a?inpMID=" + $("#inpMID").val() + "&inpDOB=" + encodeURI($("#inpDOB").val());
		});
		
		$('#AgentInterfaceContainer #inpReset').click(function(){
			<!--- Form Validation --->
			window.location = "deductible_a";
		});
				
	});

	
</script>

<cfoutput>

	<div id="AgentInterfaceContainer" class="stand-alone-content" style="width:850px;">
        <div class="EBMDialog">

            <form method="POST" id="SMSDeductibleForm">
            
            	<input type="hidden" id="inpBlockSend" name="inpBlockSend" value="1" />
                <input type="hidden" id="inpSendSMSNoConfirm" name="inpSendSMSNoConfirm" value="0" />
                
                <div class="header">
                    <div class="header-text" style="padding-top:1px !important;">Send Refill Notice - Via SMS</div>                         
                </div>
                
                <div class="inner-txt-box">
                    
                    
                    <div class="" style="float:left; height:auto; width:650px; margin-top:8px;">
                       
       						<!--- Get opt in and opt out info - update status appropriately --->
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpContactString">Mobile Phone Number<span class="small"><!---Required---></span></label>
                                <input id="inpContactString" name="inpContactString" placeholder="Enter Mobile Phone Number Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="" />
                            </div>
                            
                            <div style="clear:both"></div>
                            
                            <div class="inputbox-container">
                                <label for="inpDOB">Date of Birth MM-DD-YYYY<span class="small"><!---Required---></span></label>
                                <input id="inpDOB" name="inpDOB" placeholder="Enter DOB Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="" />
                              
                            </div>
        
                            <div style="clear:both"></div>
    
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpFirstName">Member First Name<span class="small"><!---Required---></span></label>
                                <input id="inpFirstName" name="inpFirstName" placeholder="Enter First Name Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="" />
                            </div>
                            
                            <div style="clear:both"></div>
                                                                            
                            <div class="inputbox-container">
                                <label for="inpLastName">Member Last Name<span class="small"><!---Required---></span></label>
                                <input id="inpLastName" name="inpLastName" placeholder="Enter Last Name Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="" />
                            </div>
    
                            <div style="clear:both"></div>
    
                            <!--- In Network --->                          
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpMedicationName">Medication Name <span class="small"><!---Required---></span></label>
                                <input id="inpMedicationName" name="inpMedicationName" placeholder="Enter Medication Name Here" size="20" style="padding-left:5px;" value=""/>
                            </div>
                                                            
                            <div style="clear:both"></div>
                            
                            <div class="inputbox-container">                           
                                <label for="inpProgramNotes" style="margin-top:6px;">API Sample<span class="small"></span></label>
                                <textarea id="inpProgramNotes" name="inpProgramNotes" placeholder="Enter API Sample Here" style="padding-left:5px; width:261px; min-height:50px;"></textarea>
                            </div>
       						                                
                            <div style="clear:both"></div>
                                                   
                            <div class="submit">
                                <span id="SendSMS"><a id="inpSubmit" class="button filterButton small" href="##">Send SMS Confirmation</a></span>   
                                <div id="loadingSendSMS" style="float:left; display:inline; visibility:hidden; margin-left:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                            </div>
                                                       
                            <div style="clear:both"></div>
                            
                            <div class="MSGAlert">#inpMSG#</div>
                
                		
			                <div style="clear:both"></div>
                            
                            <div class="submit">
                                <p>This page will submit a request for notification of refills. It will validate contact is currently opted in to receive these types of messages, and will then send.</p>    
                            </div>
                            
                       
                    </div>
                </div>

                <div style="clear:both"></div>
               

            </form>



        </div>
	</div>
</cfoutput>


