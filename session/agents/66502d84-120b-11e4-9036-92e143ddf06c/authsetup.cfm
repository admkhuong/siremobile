

<cfparam name="VerboseDebug" default="0" />
<cfparam name="AuthSetupRan" default="0" />


<cfif AuthSetupRan EQ 0>

	<cfset AuthSetupRan = 1>
	
    <!--- Methods must be declared only once --->
	<cfinclude template="authsetupfunctions.cfm" />

</cfif>
    
	<cfset variables.accessKey = 'E05BE8685A8F38033D38' />
    <cfset variables.secretKey = 'C60DDdc4C8453bD0e33315D0aE3+c8E4e/4e5a+0' />
   
 	<cfset variables.sign = generateSignature("POST") /> <!---Verb must match that of request type--->
	<cfset datetime = variables.sign.DATE>
    <cfset signature = variables.sign.SIGNATURE>
    
    <cfset authorization = variables.accessKey & ":" & signature>
    
    
    <cfif VerboseDebug GT 0>
    
    	<style>
				.wordwrap { 
			   white-space: pre-wrap;      /* CSS3 */   
			   white-space: -moz-pre-wrap; /* Firefox */    
			   white-space: -pre-wrap;     /* Opera <7 */   
			   white-space: -o-pre-wrap;   /* Opera 7 */    
			   word-wrap: break-word;      /* IE */
			}
        
        </style>

		<div style="max-width:800px; margin:20px;" class="wordwrap">    
    
            <cfdump var="#datetime#"> 
            <BR />
            <cfdump var="#signature#"> 
            <BR />
            <cfdump var="#authorization#"> 
            <BR />
            <cfoutput>
                variables.accessKey = #variables.accessKey#
                <BR />
                variables.secretKey = #variables.secretKey#
                <BR />
            </cfoutput>
   		
        </div>
        
    </cfif>
     
