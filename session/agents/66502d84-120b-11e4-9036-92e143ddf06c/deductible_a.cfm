<cfparam name="inpContactTypeId" default="3">
<cfparam name="inpMID" default="">
<cfparam name="inpDOB" default="">
<cfparam name="inpMSG" default="">

<!--- Not AJAX in case session expires - page load will recover cookie where CFC will not in EBM--->

<!--- Validate Security--->
<cfinvoke component="#LocalSessionDotPath#.cfc.agentportals" method="AuthenticateUser" returnvariable="getAuth">
    <cfinvokeargument name="INPPORTALID" value="1">
</cfinvoke>

<!---<cfdump var="#getAuth#" />--->

<cfif getAuth.RXRESULTCODE NEQ 1 >

	<h2>You do not have permissions to view this page!</h2>
    <cfabort />

</cfif>

<cfif NOT AgentToolsPermission.havePermission>

	<h2>You do not have permissions to view this page!<BR />Contact your company administrator if you need privileges turned on.</h2>
    <cfabort />

</cfif>

<!---
UnitedHealthcare Monthly Msg:
As of {%inpAsOfDate%} deductible met:
In Network $ {%inpDeductableInMet%} of $ {%inpDeductableIn%} 
Out of Network $ {%inpDeductableOutMet%} of $ {%inpDeductableOut%}. 
Text STOP to stop.

--->

<cfoutput>

	<script language="javascript" src="#rootUrl#/#PublicPath#/js/mask-phone-date/input-mask.js"></script>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>

</cfoutput>

<!--- Lock down page to authenticated users either company admin or linked access user id --->



<script language="javascript">
	$('#mainTitleText').html('<cfoutput>Agent tools <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> Custom Agent Interfaces</cfoutput>');
	$('#subTitleText').html('UHC Deductible Tools');
</script>


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	#AgentInterfaceContainer .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}


	#AgentInterfaceContainer .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 800px;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);
		position:absolute;
		top: -25px;
		right: 37px;
	}

	#AgentInterfaceContainer .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#AgentInterfaceContainer .form-right-portal {
    z-index: 1000;
}


	.sbHolder
	{
		width:266px;
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		border: 1px solid rgba(0, 0, 0, 0.3);
	    border-radius: 3px 3px 3px 3px;
	}

	<!--- 30 less than sbHolder --->
	.sbSelector
	{
		width:246px;
	}

	.sbOptions
	{
		max-height:250px !important;
	}

	.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}

		.datatable
		{
			table-layout:fixed;
			border-collapse:collapse;
			border-left: 1px solid #CCCCCC;
		}

		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		.wrapper-picker-container{
			 margin-left: 20px;
		}


		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		
		
		#inpGroupPickerDesc{
			display: inline-block;
		    float: left;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    width: 230px;
		}
		
		.MIDHighlight
		{
			color:#F90;
			font-size:18px;			
		}
		
		.MSGAlert
		{
			color:#F00;
			font-size:18px;	
			width:100%;
			text-align:center;	
			margin-top:8px;	
		}

		<!--- Tighten up form for smaller screens --->
		.EBMDialog .inputbox-container 
		{
			margin-bottom: 6px;
		}
		
		.EBMDialog form input 
		{ 
			margin-bottom: 2px;
			padding: 2px 0;
			width: 100%;
		}
	
		.EBMDialog label 
		{
			margin-bottom: 2px;
		}
		
		
		.EBMDialog .header-text {
			line-height:28px;
		}

		
						
</style>


<script type="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;

	$(function()
	{				
		 var oStringMask = new Mask("(###) ###-####");
		 if(document.getElementById("inpContactString")){
			 oStringMask.attach(document.getElementById("inpContactString"));
		 }
		 
		 var oStringMaskDOB = new Mask("##-##-####");
		 if(document.getElementById("inpDOB")){
			 oStringMaskDOB.attach(document.getElementById("inpDOB"));
		 }
		
		<!--- Trigger blur to force mask update of DB data --->
		$('#inpContactString').trigger('blur');
		// $('#inpDOB').trigger('blur');
		
		$("#inpLastSentAsOfDate" ).datepicker({ dateFormat: "mm-dd-yy" });
		 
		$("#inpContactTypeId").selectbox();

		$(".info-button1").hover(function(){
			$(".info-box1").toggle();
			$(".tool1").toggle();
		});
 				
				
		<!--- Update Member Info --->		
		$('#AgentInterfaceContainer #inpUpdateStatus').click(function(){
			
			<!--- Set block SMS to off --->
			$("#inpBlockSend").val(1);
						
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpSubmit').hide();
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingUpdatingStatus").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #UpdateStatus').html('<span style="float: right;">Updating Data...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmit').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmitNoConfirm').hide();			
			$("#loadingSendSMS").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMS').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		<!--- Send SMS information --->
		$('#AgentInterfaceContainer #inpSubmitNoConfirm').click(function(){
			
			<!--- Set block SMS to on --->
			$("#inpBlockSend").val(0);
			$("#inpSendSMSNoConfirm").val(1);
			
			$('#AgentInterfaceContainer .MSGAlert').hide();
			$('#AgentInterfaceContainer #inpUpdateStatus').hide();	
			$('#AgentInterfaceContainer #inpSubmit').hide();			
			$("#loadingSendSMSNoConfirm").css('visibility', 'visible');
				
			$('#AgentInterfaceContainer #SendSMSNoConfirm').html('<span style="float: right;">Sending SMS...</span>');			
			$('#AgentInterfaceContainer form#SMSDeductibleForm').attr("action", "deductible_a_proc");
			$('#AgentInterfaceContainer form#SMSDeductibleForm').submit();
		});
		
		
		
		$('#AgentInterfaceContainer #inpLookup').click(function(){
			window.location = "deductible_a?inpMID=" + $("#inpMID").val() + "&inpDOB=" + encodeURI($("#inpDOB").val());
		});
		
		$('#AgentInterfaceContainer #inpReset').click(function(){
			<!--- Form Validation --->
			window.location = "deductible_a";
		});
				
	});

	
</script>

<cfoutput>

	<div id="AgentInterfaceContainer" class="stand-alone-content" style="width:850px;">
        <div class="EBMDialog">

            <form method="POST" id="SMSDeductibleForm">
            
            	<input type="hidden" id="inpBlockSend" name="inpBlockSend" value="1" />
                <input type="hidden" id="inpSendSMSNoConfirm" name="inpSendSMSNoConfirm" value="0" />


				<cfif TRIM(inpMID) EQ "">
                    <div class="header">
                        <div class="header-text">Lookup Member Information</div>                         
                    </div>
                    
                <cfelse>
                	<input type="hidden" id="inpMID" name="inpMID" value="#inpMID#" />
                
                 	<div class="header">
                        <div class="header-text" style="padding-top:1px !important;">Current Member Information - ID - <span class="MIDHighlight">#inpMID#</span> <span style="padding-bottom:5px; margin-right:10px; float:right;"><a href="##" class="button filterButton small" id="inpReset" >Start Over</a></span></div>                         
                    </div>
                	                            
				</cfif>
                
                
                <div class="inner-txt-box">

					 
                     
					<!---<cfif TRIM(inpMID) EQ "">

                        <div class="inner-txt-hd">Lookup Member Information</div>
                        <div class="inner-txt"></div>
                        
					<cfelse>
                    
                    	<div class="inner-txt-hd">Current Member Information</div>
                        <div class="inner-txt"></div>
                        
					</cfif>--->
                    
                    
                    <div class="" style="float:left; height:auto; width:650px; margin-top:8px;">
                       
                       
                       <cfif TRIM(inpMID) EQ "">
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpMID">Member ID<span class="small"><!---Required---></span></label>
                                <input id="inpMID" name="inpMID" placeholder="Enter Member ID Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#inpMID#" />
                            </div>
    
    						<div style="clear:both"></div>
    
    						<div class="inputbox-container">
                                <label for="inpDOB">Date of Birth MM-DD-YYYY<span class="small"><!---Required---></span></label>
                                <input id="inpDOB" name="inpDOB" placeholder="Enter DOB Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#dateformat(inpDOB,'yyyy-mm-dd')#" />
                            </div>
                            
                            <div style="clear:both"></div>
                            
                            <div class="inputbox-container" style="margin-top:1px;">
                                <BR />
                                <a href="##" class="button filterButton small" id="inpLookup" >Lookup Program Status</a>
                            </div>
                      
                      <cfelse>
                      
                     		<!---<div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpMID">Current Member ID</label>
                                <input type="hidden" id="inpMID" name="inpMID" value="#inpMID#" />
                           		<span class="MIDHighlight">#inpMID#</span>
                            </div>
							
							<div style="margin-top:3px; width:300px; float:right;">
                                <a href="##" class="button filterButton small" id="inpReset" >Start Over - Re-Enter Member Id</a>
                            </div>
							
							--->                            
                      
                      </cfif>		                               
                                             
                       <cfif TRIM(inpMID) NEQ "">
                       
                       	   <!--- Get last entry for status --->	
                           <cfquery name="GetMemberInformation" datasource="#Session.DBSourceEBM#">
                               SELECT 
                                    PKId_int,
                                    MemberId_vch,
                                    DateOfBirth_dt,
                                    Status_int,
                                    FirstName_vch,
                                    LastName_vch,
                                    LastSent_dt,
                                    ContactString_vch,
                                    ContactTypeId_int,
                                    LastSentDeductableIn_vch,
                                    LastSentDeductableInMet_vch,
                                    LastSentDeductableOut_vch,
                                    LastSentDeductableOutMet_vch,
                                    LastSentAsOfDate_dt,
                                    ProgramNotes_vch,
                                    LastAgentUserId_int
                                FROM 
                                    simplexprogramdata.portal_link_c8_p1
                                WHERE
                                    MemberId_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(inpMID)#">
                                AND
                                	<cfif TRIM(inpDOB) NEQ "">
                                    	DateOfBirth_dt = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#dateformat(inpDOB,'yyyy-mm-dd 00:00:00')#"  />
                                    <cfelse>
	                                    DateOfBirth_dt IS NULL
                                    </cfif>
                                ORDER BY 
                                	PKId_int DESC
                                LIMIT 1
                           	</cfquery>    
                                   
       						<!---<cfdump var="#GetMemberInformation#">--->
                            
       						<cfif GetMemberInformation.RecordCount EQ 0>
                            
                            	<cfif inpMSG EQ "">
                                	<cfset inpMSG = "No Record Found for this Member Id - #TRIM(inpMID)#" />
                                </cfif>
                                	
                            	<cfset GetMemberInformation = StructNew() />
                                <cfset GetMemberInformation.PKId_int = 0 />
                                <cfset GetMemberInformation.MemberId_vch = "" />
                                <cfset GetMemberInformation.DateOfBirth_dt = "" />
                                <cfset GetMemberInformation.Status_int = 1 />
                                <cfset GetMemberInformation.FirstName_vch = "" />
                                <cfset GetMemberInformation.LastName_vch = "" />
                                <cfset GetMemberInformation.ContactString_vch = "" />
                                <cfset GetMemberInformation.ContactTypeId_int = 3 />
                                <cfset GetMemberInformation.LastSentDeductableIn_vch = "" />
                                <cfset GetMemberInformation.LastSentDeductableInMet_vch = "" />
                                <cfset GetMemberInformation.LastSentDeductableOut_vch = "" />
                                <cfset GetMemberInformation.LastSentDeductableOutMet_vch = "" />
                                <cfset GetMemberInformation.LastSentAsOfDate_dt = NOW() />
                                <cfset GetMemberInformation.ProgramNotes_vch = "" />
                                <cfset GetMemberInformation.LastAgentUserId_int = 0 />
                                                            
                            </cfif>
       
       
       
       						<cfif GetMemberInformation.Status_int EQ 2 OR GetMemberInformation.Status_int EQ 5>
                            	<!--- Look for opt ins in DNC Table --->
                            
								<!--- Look for opt outs--->
                                <cfquery name="GetUserOptInRequest" datasource="#Session.DBSourceEBM#" > 
                                    SELECT 
                                        COUNT(oi.ContactString_vch) AS TotalCount
                                    FROM 
                                        simplelists.optinout AS oi 
                                    WHERE 
                                        OptIn_dt IS NOT NULL 
                                    AND 
                                        OptOut_dt IS NULL 
                                    AND 
                                        ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="244687">  
                                    AND 
                                        oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(GetMemberInformation.ContactString_vch)#">                                                                                                                                                           
                                </cfquery>    
                                
                                <cfif GetUserOptInRequest.TotalCount GT 0>
	                                <cfset GetMemberInformation.Status_int = 3 />
                                </cfif>
                            
                            </cfif>
                            
                            
                            <!--- Look for opt outs--->
                            <cfquery name="GetUserDNCFromServiceRequest" datasource="#Session.DBSourceEBM#" > 
                                SELECT 
                                    COUNT(oi.ContactString_vch) AS TotalCount
                                FROM 
                                    simplelists.optinout AS oi 
                                WHERE 
                                    OptOut_dt IS NOT NULL 
                                AND 
                                    OptIn_dt IS NULL 
                                AND 
                                    ShortCode_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="244687">  
                                AND 
                                    oi.ContactString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(GetMemberInformation.ContactString_vch)#">       
                                                                                                                     	            	    	
                            </cfquery>    
                            
                            <cfif GetUserDNCFromServiceRequest.TotalCount GT 0>                            	
                            	<cfset GetMemberInformation.Status_int = 5 />
                            </cfif>
                            
                            
       
       						<!--- Get opt in and opt out info - update status appropriately --->
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpContactString">Mobile Phone Number<span class="small"><!---Required---></span></label>
                                <input id="inpContactString" name="inpContactString" placeholder="Enter Mobile Phone Number Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#GetMemberInformation.ContactString_vch#" />
                            </div>
                            
                            <div class="inputbox-container">
                                <label for="inpDOB">Date of Birth MM-DD-YYYY<span class="small"><!---Required---></span></label>
                                <cfif GetMemberInformation.DateOfBirth_dt NEQ "">
                                	<input id="inpDOB" name="inpDOB" placeholder="Enter DOB Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#dateformat(GetMemberInformation.DateOfBirth_dt,'mm-dd-yyyy')#" />
                                <cfelse>
                                	<input id="inpDOB" name="inpDOB" placeholder="Enter DOB Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#dateformat(inpDOB,'mm-dd-yyyy')#" />
                                </cfif>    
                            </div>
        
                            <div style="clear:both"></div>
    
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpFirstName">Member First Name<span class="small"><!---Required---></span></label>
                                <input id="inpFirstName" name="inpFirstName" placeholder="Enter First Name Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#GetMemberInformation.FirstName_vch #" />
                            </div>
                                                                            
                            <div class="inputbox-container">
                                <label for="inpLastName">Member Last Name<span class="small"><!---Required---></span></label>
                                <input id="inpLastName" name="inpLastName" placeholder="Enter Last Name Here" size="20" autofocus="autofocus" style="padding-left:5px;" value="#GetMemberInformation.LastName_vch#" />
                            </div>
    
                            <div style="clear:both"></div>
    
                            <!--- In Network --->                          
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpLastSentDeductableInMet">Deductible Met (In Network) <span class="small"><!---Required---></span></label>
                                <input id="inpLastSentDeductableInMet" name="inpLastSentDeductableInMet" placeholder="" size="20" style="padding-left:5px;" value="#GetMemberInformation.LastSentDeductableInMet_vch#"/>
                            </div>
                            
                            <div class="inputbox-container"> 
                                <label for="inpLastSentDeductableIn">Plan Deductible (In Network) <span class="small"><!---Required---></span></label>
                                <input id="inpLastSentDeductableIn" name="inpLastSentDeductableIn" placeholder="" size="20" style="padding-left:5px;" value="#GetMemberInformation.LastSentDeductableIn_vch#"/>
                            </div>
                                
                            <div style="clear:both"></div>
                                                    
                            <!--- In Network --->
                            <div class="inputbox-container" style="margin-right:25px;">
                                <label for="inpLastSentDeductableOutMet">Deductible Met (Out of Network)<span class="small"><!---Required---></span></label>
                                <input id="inpLastSentDeductableOutMet" name="inpLastSentDeductableOutMet" placeholder="" size="20" style="padding-left:5px;" value="#GetMemberInformation.LastSentDeductableOutMet_vch#"/>
                            </div>
                            
                            <div class="inputbox-container">
                                <label for="inpLastSentDeductableOut">Plan Deductible (Out of Network)<span class="small"><!---Required---></span></label>
                                <input id="inpLastSentDeductableOut" name="inpLastSentDeductableOut" placeholder="" size="20" style="padding-left:5px;" value="#GetMemberInformation.LastSentDeductableOut_vch#"/>
                            </div>
                                                           
                            <div style="clear:both"></div>
                         
                            <div class="inputbox-container">                           
                                <label for="inpLastSentAsOfDate">Deductible As of this date <span class="small"><!---Required---></span></label>
                                <input type="text" id="inpLastSentAsOfDate" name="inpLastSentAsOfDate" style="padding-left:5px;" value="#dateformat(GetMemberInformation.LastSentAsOfDate_dt,'yyyy-mm-dd')#">
                                <BR />
                                <label for="inpProgramNotes" style="margin-top:6px;">Program Notes <span class="small">Optional</span></label>
                                <textarea id="inpProgramNotes" name="inpProgramNotes" placeholder="Enter Notes Here" style="padding-left:5px; width:261px; min-height:50px;">#GetMemberInformation.ProgramNotes_vch#</textarea>
                            </div>
    
   							<div style="width:300px; float:left; margin-left:25px; margin-top:6px;">
                                    <label class="left bold_label">Opt In Status</label>
                                    <input type="radio" name="inpStatus" value="2" id="inpStatus" class="EBMRadio" <cfif GetMemberInformation.Status_int EQ 2>checked</cfif> />
                                    <h3 class="apply_yesno_label">Offer Sent Via SMS - Awaiting Acceptance</h3> <!---<a id="inpSubmit" class="button filterButton small" href="##">Resend SMS</a>--->
                                    <BR />
                                     <input type="radio" name="inpStatus" value="3" id="inpStatus" class="EBMRadio" <cfif GetMemberInformation.Status_int EQ 3>checked</cfif> />
                                    <h3 class="apply_yesno_label">Opted In</h3>
                                    <BR />
                                    <input type="radio" name="inpStatus" id="inpStatus" value="4" class="EBMRadio" <cfif GetMemberInformation.Status_int EQ 4>checked</cfif>/>
                                    <h3 class="apply_yesno_label">Declined (Agent)</h3>
                                    <BR />
                                    <input type="radio" name="inpStatus" id="inpStatus" value="5" class="EBMRadio" <cfif GetMemberInformation.Status_int EQ 5>checked</cfif>/>
                                    <h3 class="apply_yesno_label">Opted Out (SMS)</h3>
                                    <BR />
                                    <input type="radio" name="inpStatus" id="inpStatus" value="1" class="EBMRadio" <cfif GetMemberInformation.Status_int EQ 1>checked</cfif>/>
                                    <h3 class="apply_yesno_label">Not Offered Yet</h3>
                                </div>
                                
                            <div style="clear:both"></div>
                                                   
                            <div class="submit">
                                <span id="SendSMS"><a id="inpSubmit" class="button filterButton small" href="##">Send SMS Confirmation</a></span>   
                                <div id="loadingSendSMS" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                            </div>
                            
                            <div class="submit">
                                <span id="SendSMSNoConfirm"><a id="inpSubmitNoConfirm" class="button filterButton small" href="##">SMS Monthly Update</a></span>   
                                <div id="loadingSendSMSNoConfirm" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                            </div>
                            
                            <div class="submit">
                                <span id="UpdateStatus"><a id="inpUpdateStatus" class="button filterButton small" href="##">Update Status</a></span>   
                                <div id="loadingUpdatingStatus" style="float:right; display:inline; visibility:hidden; margin-right:5px;"><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20"></div>
                            </div>
                            
                            <div style="clear:both"></div>
                            
                            <div class="MSGAlert">#inpMSG#</div>
                
                		</cfif>
                
                       
                    </div>
                </div>

                <div style="clear:both"></div>
               

            </form>



        </div>
	</div>
</cfoutput>


