<!---
  Created by neovinhtru on 11/08/2014.
--->
<cfset campainId= 0>
<cfif structKeyExists(url, "code")>
    <cfset campainId= url.code>
</cfif>
<cfset AgentToolTitlePermission = permissionObject.havePermission(Agent_Tool_Title)>
<cfif AgentToolTitlePermission.havePermission>
<cfif isValid("integer", campainId)>
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.agenttools" method="GetListCampaignCodeForAgent" returnvariable="campaignList"></cfinvoke>
<cfinvoke component="#LocalSessionDotPath#.cfc.agenttools" method="GetListUserOfCompanyByCompanyId" returnvariable="userList">
    <cfinvokeargument name="inpCompanyId" value="#session.CompanyId#">
</cfinvoke>
<cfset arrayCampaign = arrayNew(1)>
<cfloop query="campaignList.LISTCAMPAIN">
    <cfset arrayAppend(arrayCampaign,campaignList.LISTCAMPAIN.BatchId_bi)>
</cfloop>
<cfset listUser = {}>
 <cfset i=0>
<cfinvoke component="#LocalSessionDotPath#.cfc.agenttools" method="GetCannedResponse" returnvariable="cannedList">
    <cfinvokeargument name="inpCampaignId" value="#campainId#">
</cfinvoke>
<!--- List all Users access Agent tool --->
<cfif userList.NUMBERUSER GT 0 AND userList.NUMBERUSER NEQ "">
    <cfloop query="userList.LISTUSER">
        <cfset user = {}>
        <cfset user.userId = userList.LISTUSER.UserId_int>
        <cfset user.UsesFullName = userList.LISTUSER.FirstName_vch & ' ' & userList.LISTUSER.LastName_vch>
        <cfset listUser.row[i] = user>
        <cfset i++>
    </cfloop>
</cfif>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<script src="<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>/socket.io/socket.io.js"></script>
<script type="text/javascript">
    (function($, document){
        var loadFirst = true;

        //List User On Company
        var userList = <cfoutput>#serializeJson(listUser)#</cfoutput>;

        // Current user
        var userid = "<cfoutput>#session.USERID#</cfoutput>";

        // Current campain ID
        var campainId = "<cfoutput>#campainId#</cfoutput>";

        // Get Config time end client
        var timeEndClient = "<cfoutput>#clientTimeEndSession#</cfoutput>";

        // Common functions
        Agent = {
            init: function() {
                this.userid = userid;
                this.campaignid = campainId;
                this.companyid = <cfoutput>#session.CompanyId#</cfoutput>;
                this.socket = io.connect("<cfoutput>#serverSocket#:#serverSocketPort#</cfoutput>");
                var _self = this;
                this.phonenumberInQueue = [];
                this.rooms = [];
                var campaignList = <cfoutput>#serializeJson(arrayCampaign)#</cfoutput>;

                // Agent send information to Nodejs server
                this.socket.on('client.get.agentid', function() {
                    _self.socket.emit('client.join', {
                        userid:_self.userid,
                        campaignid: _self.campaignid,
                        companyid: _self.companyid
                    });
                });

                // Action Session End Time form Nodejs server
                this.socket.on('room.session.end', function(data){
                    Agent.timeEndSession(data);
                });

                // Agent connect to Session success
                this.socket.on('client.join.success', function(data) {
                    // Load session tranfer and receive user
                    Agent.loadPageTranfer(data);
                    loadFirst = false;
                    for(var i in data.queue) {
                        _self.phonenumberInQueue.push(data.queue[i]);
                    }
                    for(var i in data.room) {
                        _self.rooms.push(data.room[i]);
                    }
                    _self.socket.emit('agent.register.campaign', {
                        campaign: campaignList
                    });
                    var nuQueue = data.queue.length;
                    if(nuQueue >0) {
                        $('#innertube .span-peopleinqueue .get-queue').html(nuQueue);
                    }
                    for(var i in data.campaign){
                        $('#campaign_' + data.campaign[i].id +' .numContact').html(data.campaign[i].activeRoom);
                    }

                    // Enable block Canned response
                    Agent.cannedResponseStatus();

                });

                // Session push in Queue
                this.socket.on('agent.sms.in.queue', function(data) {
                    _self.phonenumberInQueue.push(data);
                    Agent.updateQueue();
                });

                // Agent request get Session in Queue
                this.socket.on('agent.sms.out.queue', function(data) {
                    for(var i in _self.phonenumberInQueue) {
                        if(_self.phonenumberInQueue[i].phonenumber == data.phonenumber) {
                            _self.phonenumberInQueue.splice(i, 1);
                            Agent.updateQueue();
                            return;
                        }
                    }
                });

                // Agent get in all information of Session
                this.socket.on('client.join.to.room', function(data) {
                    if(loadFirst == false){
                        _self.rooms.push(data.room);
                        Agent.appendJoinRomActive(data,false);
                    }else {
                        _self.rooms.push(data.room);
                        Agent.appendJoinRomNone(data);
                    }

                    // Disible block canned response
                    Agent.cannedResponseStatus();

                    // Update total number contact on Campaign
                    var nuContact = parseInt($("#campaign_" + data.campaign.id + ' .numContact').html());

                    $('#campaign_' + data.campaign.id +' .numContact').html(nuContact + 1);
                });

                // Agent Get content message form Nodejs server
                this.socket.on('broatcast.message', function(data) {
                    Agent.appendNotify(data);
                });

                // Agent connect Session and redirect location when Agent request session in queue success and current campaign ID different campaign ID in queue
                this.socket.on('client.join.user.to.room',function(data){

                    // Update total number contact on Campaign
                    var nuContact = parseInt($("#campaign_" + data.campaign.id + ' .numContact').html());
                    $('#campaign_' + data.campaign.id +' .numContact').html(nuContact + 1);

                    window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/agents/campaign_code_manager?code="+data.campaign.id + '&active=' + data.room.id;
                });

                // Agent request transfer to Agent different
                this.socket.on('send.request.transfer.to.agent',function(data){
                    var template = $("#get-transfer-template").html();
                    var userName = 'none'
                    for(var i in userList.ROW) {
                        if(userList.ROW[i].USERID == data.from.id){
                            var userName = userList.ROW[i].USESFULLNAME;
                        }
                    }
                    var content = _.template(template, { user_id: data.from.id, user_name: userName, room_id: data.room.id});
                    $('#wrap-incoming').append(content);
                    $('#wrap-incoming').scrollTop($('#wrap-incoming .panel-' + data.from.id + '-' + data.room.id).offset().top);
                });

                // Agent transfer session success.
                this.socket.on('agent.transfer.success',function(data){
                    $("#wrap-incoming .panel-" + data.agent.id + "-" + data.room.id).remove();
                    $("#room-"+ data.room.id + " span."+ data.room.id).countdown('destroy');
                    var userName = 'none';
                    if(data.agent.id != userid){
                        for(var x in userList.ROW) {
                            if(userList.ROW[x].USERID == data.agent.id){
                                userName = userList.ROW[x].USESFULLNAME;
                            }
                        }
                        $.jGrowl("Your Session has been transfer success to " + userName, { life: 2000, position: "center", header: '' });
                        var numTabContent = $("#tabs_room .content-tab").length;
                        if(numTabContent > 0) {
                            $("#cancel_response").trigger('click');
                        }else {
                            Agent.cannedResponseStatus();
                        }
                    }
                });

                // Agent reject transfer accept.
                this.socket.on('agent.accept.transfer.success',function(data){
                    if( campainId == data.campaign.id) {
                        $("#wrap-incoming .panel-" + data.agent.id + "-" + data.room.id).remove();
                    }else{
                        window.location.href = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/agents/campaign_code_manager?code="+data.campaign.id + '&active=' + data.room.id;
                    }
                });

                // Agent reject transfer session.
                this.socket.on('agent.reject.transfer.success', function(data){
                    $("#wrap-incoming .panel-" + data.rejectTo.id + "-" + data.room.id).remove();
                });

                // Notify message to Agent when Agent decline transfer session
                this.socket.on('agent.transfer.error', function(data){
                    $("#wrap-incoming .panel-" + data.rejectFrom.id + "-" + data.room.id).remove();
                    $("#room-" + data.room.id +" .block-waiting-transfer").remove();
                    $('.transfer-' + data.rejectFrom.id+'-' + data.room.id).css('display','block');
                    var userName = 'none';
                    if(data.rejectFrom.id != userid){
                        for(var x in userList.ROW) {
                            if(userList.ROW[x].USERID == data.rejectFrom.id){
                                userName = userList.ROW[x].USESFULLNAME;
                            }
                        }
                        $.jGrowl("Your Session has been reject transfer by " + userName, { life: 2000, position: "center", header: '' });
                    }
                });

                // Agent request cancal transfer session
                this.socket.on('send.cancel.request.transfer', function(data){
                    $("#wrap-incoming .panel-" + data.from.id + "-" + data.room.id).remove();
                    var userName = 'none';
                    if(data.from.id != userid){
                        for(var x in userList.ROW) {
                            if(userList.ROW[x].USERID == data.from.id){
                                userName = userList.ROW[x].USESFULLNAME;
                            }
                        }
                        $.jGrowl(userName + " Cancelled transfer session for you", { life: 2000, position: "center", header: '' });
                    }
                });

                // Session time end show message for Agent receive transfer
                this.socket.on('agent.transfer.room.timeout', function(data){
                    Agent.timeEndWaitReceiveSession(data);
                });

                // Agent change status online - offline
                this.socket.on('change.online.status', function(data){
                    if(data.online == false) {
                        $(".transfer .user-" + data.agent.id).removeClass('online');
                        $(".transfer .user-" + data.agent.id).addClass('offline');
                    }else{
                        $(".transfer .user-" + data.agent.id).removeClass('offline');
                        $(".transfer .user-" + data.agent.id).addClass('online');
                    }
                    if($(".transfer ul li.online").length > 0){
                        $(".over_transfer div.transfer").css('display','block');
                    }else{
                        $(".over_transfer div.transfer").css('display','none');
                    }
                });

            },

            // Request SMS in queue
            requestphonenumberInQueue: function() {
                this.socket.emit('agent.request.queue');
            },

            // Agent request out Session
            requestOutRoom: function(roomId) {
                this.socket.emit('agent.request.out.room', roomId);
            },

            // Send message to SMS
            sendMessage: function(roomId, connentMessage) {

                if(this.rooms.length == 0) {
                    return;
                }
                if(connentMessage == "") {
                    connentMessage = $('#room-' + roomId + ' .type_message_area').val();
                }
                if(typeof(connentMessage) !="undefined" && connentMessage.length != 0) {
                    $.ajax({
                        dataType: 'json',
                        async: false,
                        type: "GET",
                        data: {
                            inpPhoneNumber: $('#room-'+roomId +" .contact-number").attr('rel'),
                            inpSessionId: $('#room-'+roomId +" .box_chat_area").attr('rel'),
                            inpCampaignId: campainId,
                            inpShortCode: $('#room-'+roomId +"").attr('data-short-code'),
                            inpMessages: connentMessage,
                            inpTypeDevice: 1 // 1: SMS QA tool, 2: Physical device
                        },
                        url: "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/agenttools.cfc?method=sendMTMessage&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                        success: function(data) {
                            if(data.RXRESULTCODE > 0) {
                                if(data.SMSTYPE == "QATOOL") {
                                    Agent.sendSMSToQATool(roomId, connentMessage, data.MESSAGEID);
                                }
                                connentMessage = connentMessage.replace(/</g,'&lt;');
                                connentMessage = connentMessage.replace(/>/g,'&gt;');
                                var d = new Date();
                                var content = "<tr><td>" + d.toLocaleString() + "</td><td>AGENT</td><td><p>" + connentMessage + "</p></td></tr>";
                                $('#room-' + roomId + ' .head_chat_body table.table tbody').append(content);
                                $('#room-' + roomId + ' .type_message_area').val('');
                                $('#room-' + roomId + ' .head_chat_body').animate({ scrollTop: $('#room-' + roomId + ' .head_chat_body').height() }, 1000);
                                $('#room-' + roomId + ' .type_message_area').focus();
                                // Reset time countdown
                                $("#room-" + roomId + " span." + roomId).countdown('destroy');
                                // Config end time a SMS 5400 second
                                $("#room-" + roomId + " span." + roomId).countdown({until: +timeEndClient, onExpiry: Agent.endRoomCountDown});
                            }else{
                                jAlert(data.MESSAGE+"\n", "Failure!", function(result) { } );
                            }
                        }
                    });
                }else{
                    jAlert("Message has not been entered.\n", "Failure!", function(result) { } );
                    return false;
                }

            },

            // Update total number queue in client.
            updateQueue: function() {
                $('#innertube .span-peopleinqueue .get-queue').html(this.phonenumberInQueue.length);
            },

            // Append message chat in a session.
            appendNotify: function(data){
                var contact =  $('#room-'+data.roomId).attr('rel');
                var msg = data.msg;
                msg = msg.replace(/</g,'&lt;');
                msg = msg.replace(/>/g,'&gt;');
                var d = new Date();
                var content = '';
                if(data.role =="sms"){
                    var status =  $('#room-'+data.roomId).is(':visible');
                    if(status == false && $('#etabs li.'+data.room + ' .icon_new_mess').length < 1) {
                        $('#etabs li.'+data.roomId).append('<i class="icon_new_mess"></i>');
                    }
                    $('#tabs-wait-chat li.'+data.roomId).addClass('newmgs');
                    content = "<tr class='success'><td>" + d.toLocaleString() + "</td><td class='text-orange'>" + contact + "</td><td><p>" + msg + "</p></td></tr>";
                    $('#room-'+data.roomId+ ' .head_chat_body table.table tbody').append(content);
                    $('#room-'+data.roomId+ ' .head_chat_body').animate({ scrollTop: $('#room-'+data.roomId+ ' .head_chat_body').height() }, 1000);
                }else{
                    content = "<tr><td>"+d.toLocaleString()+"</td><td>AGENT</td><td><p>"+msg+"</p></td></tr>";
                    $('#room-'+data.roomId+ ' .head_chat_body table.table tbody').append(content);
                    $('#room-'+data.roomId+ ' .head_chat_body').animate({ scrollTop: $('#room-'+data.roomId+ ' .head_chat_body').height() }, 1000);
                }
            },

            // Get session to campaign
            appendJoinRomNone: function(data){

                // Check time load Room
                var timeZoneServer = data.timezone;
                var nowDate = new Date();
                var nowTimeServer = data.servertime;
                var timeZoneServer = data.timezone;
                var end = data.room.end - (timeZoneServer*60*60*1000);

                if(end > nowTimeServer) {
                    var activeRoomId = Agent.getParams('active');
                    var code = Agent.getParams('code');
                    if(activeRoomId != null){
                        $('#maincontent .left_col_menu').scrollTop($('#maincontent #campaign_'+code).offset().top);
                        Agent.appendJoinRomActive(data);
                    }else {
                        var link = '';
                        var moreTabs = 0;

                        // check max is 4 tab active
                        if ($('#etabs li').length < 4) {
                            link = '<li class="tab ' + data.room.id + '"><a rel="#room-' + data.room.id + '">' + data.room.phonenumber + '</a></li>';
                            $('#etabs').append(link);
                        } else {
                            link = '<li class="' + data.room.id + '"><a rel="#room-' + data.room.id + '">' + data.room.phonenumber + '</a></li>';
                            $('#tabs-wait-chat').append(link);
                            moreTabs = $('#tabs-wait-chat li').length;
                            $('#tab-container .number').html(moreTabs);
                        }

                        if (moreTabs > 0) {
                            $('#tab-container .wrap_icon').css('display', 'block');
                        } else {
                            $('#tab-container .wrap_icon').css('display', 'none');
                        }
                        var template = $("#room-template").html();
                        var nowDate = new Date();
                        var time_end = Math.round(( end - nowTimeServer) / 1000);
                        var msg_created = Agent.loadSmsCreated(data.msg, data.room);
                        var keyword = $("#campaign_" + data.campaign.id).attr('rel');
                        var code = $("#campaign_"+data.campaign.id).attr('data-short-code');
                        var session_sms = data.room.session;
                        var content = _.template(template, { room_id: data.room.id, carrier: data.room.carrier, keyword: keyword, code:code, contact: data.room.phonenumber, msg_created: msg_created, session_sms: session_sms});
                        $('#tabs_room').append(content);

                        // Show list user online of company
                        $.ajax({
                            dataType: 'json',
                            async: false,
                            type: "GET",
                            url: "<cfoutput>#serverSocket#:#serverSocketPort#/online/</cfoutput>" + userid,
                            success: function(data) {
                                for (var i  in data.data.online) {
                                    $(".transfer .user-" + data.data.online[i]).removeClass('offline');
                                    $(".transfer .user-" + data.data.online[i]).addClass('online');
                                }
                            }
                        });

                        if($("#room-" + data.room.id + " .over_transfer .transfer ul li.online").length > 0){
                            $("#room-" + data.room.id + " .over_transfer div.transfer").css('display','block');
                        }else{
                            $("#room-" + data.room.id + " .over_transfer div.transfer").css('display','none');
                        }

                        // Set time count down session
                        $("#room-"+ data.room.id + " ."+data.room.id).countdown({until: +time_end, onExpiry: Agent.endRoomCountDown});

                        $('#tab-container .etabs li:nth-child(1) a').trigger('click');
                    }
                }else{
                    Agent.clientEndRoom(data.room.id);
                }
            },

            // Get session to campaign and active session this.
            appendJoinRomActive: function(data){
                var link = '';
                var moreTabs = 0;
                // check max is 4 tab active
                if ($('#etabs li').length == 4) {

                    // move main to to more tab
                    var clone_in_tab = $('#tab-container .etabs li:nth-child(4)').clone();
                    clone_in_tab = clone_in_tab.removeClass('tab');
                    clone_in_tab = clone_in_tab.removeClass('active');
                    if (clone_in_tab.find('.icon_new_mess').length > 0) {
                        clone_in_tab = clone_in_tab.addClass('newmgs');
                    }
                    $('#tab-container .etabs li:nth-child(4)').remove();
                    $('#tabs-wait-chat').append(clone_in_tab);

                    // count link more
                    moreTabs = $('#tabs-wait-chat li').length;

                    $('#tab-container .number').html(moreTabs);
                }

                // Create new link active
                $('#etabs .tab').removeClass('active');
                $('#etabs .tab a').removeClass('t_active');
                link = '<li class="tab active ' + data.room.id + '"><a class="t_active" rel="#room-' + data.room.id + '">' + data.room.phonenumber + '</a></li>';
                $('#etabs').append(link);
                if (moreTabs > 0) {
                    $('#tab-container .wrap_icon').css('display', 'block');
                } else {
                    $('#tab-container .wrap_icon').css('display', 'none');
                }
                var template = $("#room-template").html();
                var timeZoneServer = data.timezone;
                var nowTimeServer = data.servertime;
                var end = data.room.end - (timeZoneServer*60*60*1000);
                // total time end session
                var time_end = Math.round(( end - nowTimeServer) / 1000);
                var msg_created = Agent.loadSmsCreated(data.msg,data.room);
                var keyword = $("#campaign_"+data.campaign.id).attr('rel');
                var code = $("#campaign_"+data.campaign.id).attr('data-short-code');
                var session_sms = data.room.session;
                var content = _.template(template, { room_id: data.room.id, carrier:data.room.carrier, keyword:keyword, code:code, contact: data.room.phonenumber, msg_created:msg_created, session_sms: session_sms});
                $('#tabs_room').append(content);

                // Show list user online of company
                $.ajax({
                    dataType: 'json',
                    async: false,
                    type: "GET",
                    url: "<cfoutput>#serverSocket#:#serverSocketPort#/online/</cfoutput>" + userid,
                    success: function(data) {
                        for (var i  in data.data.online) {
                            $(".transfer .user-" + data.data.online[i]).removeClass('offline');
                            $(".transfer .user-" + data.data.online[i]).addClass('online');
                        }
                    }
                });

                if($("#room-" + data.room.id + " .over_transfer .transfer ul li.online").length > 0){
                    $("#room-" + data.room.id + " .over_transfer div.transfer").css('display','block');
                }else{
                    $("#room-" + data.room.id + " .over_transfer div.transfer").css('display','none');
                }
                $("#tabs_room .content-tab").css('display', 'none');
                $("#room-"+ data.room.id).css('display', 'block');
                $('#campaign_'+data.campaign.id + ' .numContact').html(data.room.length);

                // Set time count down session
                $("#room-"+ data.room.id + " ."+data.room.id).countdown({until: +time_end, onExpiry: Agent.endRoomCountDown});

            },

            // Append message form Agent.
            loadSmsCreated: function(sms,room){
                var string = '';
                sms = sms.reverse();
                $.each(sms, function(index,value) {
                    var connentMessage = value.msg;
                    connentMessage = connentMessage.replace(/</g,'&lt;');
                    connentMessage = connentMessage.replace(/>/g,'&gt;');
                    var time = new Date(value.timestamp);
                    var hours = time.getHours();
                    var minutes = "0" + time.getMinutes();
                    var seconds = "0" + time.getSeconds();
                    var year = time.getFullYear();
                    var month = time.getMonth() + 1;
                    month = month<10?'0'+month:month;
                    var date = time.getDate();
                    var formattedTime = hours + ':' + minutes.substr(minutes.length-2) + ':' + seconds.substr(seconds.length-2) + ' ' + month + '/' + date + '/' + year;
                    if(value.role == 'sms'){
                        string += '<tr class="success"><td>'+ formattedTime +'</td><td class="text-orange">' + room.phonenumber + '</td><td><p>' + connentMessage + '</p></td></tr>';
                    }else{
                        string += '<tr><td>' + formattedTime + '</td><td>AGENT</td><td><p>' + connentMessage + '</p></td></tr>';
                    }
                });
                return string;
            },

            // Request end a session.
            clientEndRoom: function(roomId){
                this.socket.emit('agent.request.end.room', roomId);
            },

            // Click end a session
            endRoom: function(roomId){
                jConfirm( "This will end the session and clear data. Are you sure you want to end it?", "End connect SMS", function(result) {
                    if(result)
                    {
                        Agent.clientEndRoom(roomId);
                    }
                    return false;
                });
            },

            // Session end time
            endRoomCountDown: function(){
                var roomId = $(this).attr('rel');
                Agent.clientEndRoom(roomId);
            },

            // Function get params on URL
            getParams: function(name)
            {
                name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                var regexS = "[\\?&]"+name+"=([^&#]*)";
                var regex = new RegExp( regexS );
                var results = regex.exec( window.location.href );
                if( results == null )
                    return null;
                else
                    return results[1];
            },

            // Agent quest transfer session to Agent different
            transferUser: function(userId, userName, roomId){

                var template = $("#transfer-template").html();
                var content = _.template(template, { user_id: userId, user_name: userName, room_id: roomId});
                var offline = $('.transfer-'+userId+'-'+roomId).find('.u_off').length;
                if(offline > 0)
                    return false;
                $('#wrap-incoming').append(content);
                $('#wrap-incoming').scrollTop($('#wrap-incoming .panel-' + userId + '-' + roomId).offset().top);
                $('.transfer-' + userId + '-' + roomId).css('display', 'none');
                this.socket.emit('agent.request.transfer', roomId, userId);

                // hide session has been request transfer
                Agent.hideSessionRequestTranfer(roomId);

            },

            // Agent quest cancel Transfer session
            cancelTransfer: function(userId,roomId){
                $('.transfer-'+userId+'-'+roomId).css('display','block');
                $('#wrap-incoming .panel-'+userId+'-'+roomId).remove();
                this.socket.emit('agent.cancel.request.transfer',roomId,userId);
                $("#room-" + roomId +" .block-waiting-transfer").remove();
            },

            // Agent accept transfer session to Agent different
            acceptTransfer: function(userId,roomId){
                this.socket.emit('agent.accept.transfer',roomId);
                $("#wrap-incoming .panel-" + userId + "-" + roomId).remove();
            },

            // Agent decline transfer session to Agent different
            declineTransfer: function(userId,roomId){
                this.socket.emit('agent.reject.transfer',roomId);
                $("#wrap-incoming .panel-" + userId + "-" + roomId).remove();
            },

            // Hide session after Agent request transfer
            hideSessionRequestTranfer: function(roomId){
                var waittingTranfer = "<div class='block-waiting-transfer'>Waiting transfer this session </div>";
                $("#room-"+roomId).append(waittingTranfer);
                $("#room-"+roomId).css('position','relative');
                $("#tabs_room .block-waiting-transfer").css('width', $("#room-"+roomId +" .box_chat_area").width());
                $("#tabs_room .block-waiting-transfer").css('height', $("#room-"+roomId +" .box_chat_area").height());
                $("#tabs_room .block-waiting-transfer").css('line-height', $("#room-"+roomId +" .box_chat_area").height()+'px');
                $("#room-" + roomId + "type_message_area f_left").attr('readonly',true);
            },

            // Load state transfer session or transfer receive from Agent different when page reload
            loadPageTranfer: function(data) {
                var userName = 'none';
                for (var i in data.campaign) {

                    //load user request transfer session
                    if (data.campaign[i].transferSend.length > 0) {
                        for ( var y in data.campaign[i].transferSend ) {
                            for(var x in userList.ROW) {
                                if(userList.ROW[x].USERID == data.campaign[i].transferSend[y].to.id){
                                    userName = userList.ROW[x].USESFULLNAME;
                                }
                            }
                            var template = $("#transfer-template").html();
                            var content = _.template(template, { user_id: data.campaign[i].transferSend[y].to.id, user_name: userName, room_id: data.campaign[i].transferSend[y].room.id});
                            $('#wrap-incoming').append(content);
                            $('#wrap-incoming').scrollTop($('#wrap-incoming .panel-' + data.campaign[i].transferSend[y].to.id + '-' + data.campaign[i].transferSend[y].room.id).offset().top);
                            $('.transfer-' + data.campaign[i].transferSend[y].to.id + '-' + data.campaign[i].transferSend[y].room.id).css('display', 'none');
                            Agent.hideSessionRequestTranfer(data.campaign[i].transferSend[y].room.id);
                        }
                    }

                    // Load user transfer receive session
                    if (data.campaign[i].transferReceive.length > 0) {
                        for ( var y in data.campaign[i].transferReceive ) {
                            for(var x in userList.ROW) {
                                if(userList.ROW[x].USERID == data.campaign[i].transferReceive[y].from.id){
                                    userName = userList.ROW[x].USESFULLNAME;
                                }
                            }
                            var template = $("#get-transfer-template").html();
                            var content = _.template(template, { user_id: data.campaign[i].transferReceive[y].from.id, user_name: userName, room_id: data.campaign[i].transferReceive[y].room.id});
                            $('#wrap-incoming').append(content);
                            $('#wrap-incoming').scrollTop($('#wrap-incoming .panel-' + data.campaign[i].transferReceive[y].from.id + '-' + data.campaign[i].transferReceive[y].room.id).offset().top);
                            $('.transfer-' + data.campaign[i].transferReceive[y].from.id + '-' + data.campaign[i].transferReceive[y].room.id).css('display', 'none');
                            Agent.hideSessionRequestTranfer(data.campaign[i].transferReceive[y].room.id);
                        }
                    }

                }
            },

            // Notify time end Session of Agent
            timeEndSession: function(data){
                var roomId = data.room.id;

                // Remove request and Receive Session
                $('#panel-'+roomId).remove();

                $("#room-"+ roomId + " span."+roomId).countdown('destroy');
                var link = "<a href='<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/agents/campaign_code_manager?code="+data.campaign.id+"'>"+data.campaign.id+"</a>";
                $("#room-"+roomId).remove();
                var thisCampaignId = data.campaign.id;
                var currentCampaignId = $("#main_area_chat").attr('rel');
                if( thisCampaignId == currentCampaignId){
                    if($("#etabs li").length > 3){
                        if($("#etabs li."+roomId).length > 0){
                            $("#etabs li." + roomId).remove();
                            if($("#tabs-wait-chat li").length > 0) {
                                var waitFirst = $("#tabs-wait-chat li:nth-child(1)");
                                waitFirst.removeClass('newmgs');
                                waitFirst.find('a').removeClass('t_active');
                                var clone_more = waitFirst.clone();
                                clone_more = clone_more.addClass('tab');
                                $('#tab-container .etabs').append(clone_more);
                                waitFirst.remove();
                                $('#tab-container .etabs li:nth-child(4) a').trigger('click');
                                var moreTabs = $('#tabs-wait-chat li').length;
                                $('#tab-container .number').html(moreTabs);
                                if(moreTabs == 0){
                                    $("#tab-container .wrap_icon").css('display','none');
                                }
                            }
                        }else if($("#tabs-wait-chat li."+roomId).length >0){
                            $("#tabs-wait-chat li."+roomId).remove();
                            var moreTabs = $('#tabs-wait-chat li').length;
                            $('#tab-container .number').html(moreTabs);
                            if(moreTabs == 0){
                                $("#tab-container .wrap_icon").css('display','none');
                            }
                        }
                    }else{
                        $("#etabs li."+roomId).remove();
                    }
                }

                // Update total number contact on Campaign
                var nuContact = parseInt($("#campaign_" + data.campaign.id + ' .numContact').html());

                if(nuContact > 0) {
                    $('#campaign_' + data.campaign.id + ' .numContact').html(nuContact - 1);
                }else {
                    $('#campaign_' + data.campaign.id + ' .numContact').html(0);
                }
                if(data.type =="END_BY_TIMEOUT"){
                    $.jGrowl("Your Session has timed out for campaign " + link + "  SMS number " + data.room.phonenumber + "", { life: 2000, position: "center", header: '' });
                }
                if(data.type =="END_BY_USER_MOs"){
                    $.jGrowl("Your Session has end for campaign " + link + " by SMS number " + data.room.phonenumber + "", { life: 2000, position: "center", header: '' });
                }
                $('#tab-container .etabs li:nth-child(1) a').trigger('click');
                var numTabContent = $("#tabs_room .content-tab").length;
                if(numTabContent > 0) {
                    $("#cancel_response").trigger('click');
                }else {
                    Agent.cannedResponseStatus();
                }
            },

            // Notify time end Session of a Session to Agent receive Session
            timeEndWaitReceiveSession: function(data){
                // Remove request and Receive Session
                var roomId = data.room.id;
                var userName = 'none';
                $('#panel-'+roomId).remove();
                for(var x in userList.ROW) {
                    if(userList.ROW[x].USERID == data.from.id){
                        userName = userList.ROW[x].USESFULLNAME;
                    }
                }
                if(typeof (data.type !='undefined') && data.type == 'END_BY_USER_MOs') {
                    $.jGrowl("Session transfer form " + userName + " to you is end because SMS client request Stop", { life: 2000, position: "center", header: '' });
                }else{
                    $.jGrowl("Session transfer form " + userName + " to you is time end", { life: 2000, position: "center", header: '' });
                }
            },

            // Send SMS from Agent to SMS QA Tool
            sendSMSToQATool: function(roomId,msg, messageId){
                this.socket.emit('agent.send.message', roomId, msg, messageId);
            },

            // Create new a Canned response
            newResponse: function(){
                OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/agents/dsp_new_canned_response.cfm",
                    "New Canned Response",
                    380,
                    600,
                    "newCannedResponseForm",
                    false);
                return false;
            },

            // Send message by canned reponse
            cannedResponse: function(roomId){
                $("#wrap-response").animate({bottom:0},300,'swing');
                $(".overlay_canned").css("opacity",".4");
                $("#wrap-response").css("display",'block');
                $("#send-canned-response-message").attr('rel',roomId);
            },

            // Check visible create Canned response
            cannedResponseStatus:function(){
                var numTabContent = $("#tabs_room .content-tab").length;
                if(numTabContent == 0 && campainId > 0){
                    $("#wrap-response").css('display','block');
                    $("#cancel_response").css('display','none');
                    $("#send-canned-response-message").css('display','none');
                    $("#wrap-response").css('top','0px');
                    $("#wrap-response").css('bottom','auto');
                }else{
                    $("#wrap-response").css('display','none');
                    $("#cancel_response").css('display','block');
                    $("#send-canned-response-message").css('display','block');
                    $("#wrap-response").css('top','auto');
                    $("#wrap-response").css('bottom','-400px');
                }
            }

        };

    })(jQuery, window.document);

    $(function() {
        Agent.init();
        $("#cancel_response").click(function(){

            $("#search-canned-response-item").val('');
            $("#wrap-response").animate({bottom:-400},300,'swing');
            $(".overlay_canned").css("opacity","1");
            setTimeout(
                function()
                {
                    $("#cancel_response").parent().parent().css("display",'none');
                }, 400);
            $("#search-canned-response-item").trigger('keyup');
        });
        $("ul.response_tem li").die();
        $("ul.response_tem li").live('click', function(){
            $("ul.response_tem").find("li").removeClass("active_response");
            $(this).addClass("active_response");
        });

        // Edit a Canned Response
        $("a.edit-canned-response").die();
        $("a.edit-canned-response").live('click', function(){
            var cannedResponseId = $(this).attr('rel');
            OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/agents/dsp_new_canned_response.cfm?cannedId="+cannedResponseId,
                "Edit a Canned Response",
                380,
                600,
                "newCannedResponseForm",
                false);
            return false;
        });

        // Delete a Canned Reponse
        $("a.delete-canned-response").die();
        $("a.delete-canned-response").live('click', function(){
            var cannedResponseId = $(this).attr('rel');
            var parent = $(this).parent().parent().parent().parent();
            jConfirm( "Are you sure you want to delete it?", "Delete Canned Response", function(result) {
                if(result){
                    $.ajax({
                        dataType: 'json',
                        async: false,
                        type: "POST",
                        data: {
                            inpCannedResponseId: cannedResponseId,
                            inpCampaignId: "<cfoutput>#campainId#</cfoutput>"
                        },
                        url: "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/agenttools.cfc?method=DeleteCannedResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                        success: function (data) {
                            if(data.RXRESULTCODE <0){
                                jAlert(data.MESSAGE+"\n", "Failure!", function(result) { } );
                            }else{
                                parent.remove();
                            }
                        }
                    });
                }
                return false;
            });
        });

        // Search Canned response Item
        $("#search-canned-response-item").keyup(function(e){
            var text = $(this).val();
            if(text.trim().length >0) {
                $('#wrap-response .response_tem li.cannedLoad').css('display', 'none');
                var result = $('#wrap-response .response_tem li.clearfix .response_item:contains("' + text + '")').parent().css('display', 'block');
                if(result.length == 0){
                    $('div#find-not-found').css('display', 'block');
                }else{
                    $('div#find-not-found').css('display', 'none');
                }
            }else{
                $('#wrap-response .response_tem li.clearfix').css('display', 'block');
                $('div#find-not-found').css('display', 'none');
                $('#wrap-response .response_tem li.clearfix').removeClass('active_response');
            }
        });

        // Send canned response to Client
        $("#send-canned-response-message").click(function(){
            var check = $("#wrap-response .response_tem li.active_response").length;
            var roomId = $(this).attr('rel');
            if(check == 0){
                jAlert("Please choose a Canned response.\n", "Failure!", function(result) { } );
                return false;
            }
            if(check > 1){
                jAlert("Please choose only one Canned response.\n", "Failure!", function(result) { } );
                return false;
            }
            var msg = $("#wrap-response .response_tem li.active_response").find(".response_item").attr('rel');
            Agent.sendMessage(roomId,msg);
            $('#wrap-response .response_tem li.clearfix').removeClass('active_response');
            $("#cancel_response").trigger('click');
        });
        var ajaxRunning = false;
        // loading paging scroll canned response
        $("#wrap-response .body_response").scroll(function(){
            if(!ajaxRunning && $("#wrap-response .body_response").scrollTop() + $("#wrap-response .body_response").height() > $("#wrap-response .body_response ul").height()){
                var current_page = $(this).find('#current-page').val();
                ajaxRunning = true;
                $("#wrap-response .body_response .loading").css('display','block');
                setTimeout(function() {
                    $.ajax({
                        dataType: 'json',
                        async: false,
                        type: "POST",
                        data: {
                            iDisplayStart: parseInt(current_page),
                            inpCampaignId: "<cfoutput>#campainId#</cfoutput>"
                            },
                        url: "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/agenttools.cfc?method=GetCannedResponsePaging&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                        success: function (data) {
                            if (data.RXRESULTCODE > 0) {
                                if(data.NUMBERCANNEDRESPONSE > 0) {
                                    $("#wrap-response .body_response #current-page").val(parseInt(current_page) + data.NUMBERCANNEDRESPONSE);
                                    for (var i in data.aaData) {
                                        var lCannedResponse = data.aaData[i];
                                        $("#wrap-response .body_response .response_tem").append(lCannedResponse);
                                    }
                                }
                            }
                            else if (data.RXRESULTCODE < 0) {
                                jAlert(data.MESSAGE + "\n", "Failure!", function (result) {
                                });
                            }
                        },
                        complete: function() {
                            ajaxRunning = false;
                        }
                    });
                    $("#wrap-response .body_response .loading").css('display','none');
                }, 1500);
            }
        });
    });

</script>

<!---    Template html a Session   --->
<script type="text/html" id="room-template">
    <div id="room-<%= room_id %>" rel="<%= contact %>" class="content-tab" style="display: none" data-short-code="<%= code %>">
        <div class="overlay_canned">
            <div class="box_chat_area" rel="<%= session_sms %>">
                <div class="head_chat_infomation">
                    <ul class="clearfix">
                        <li class="contact-number" rel="<%= contact %>">Mobile Number: <%= contact %></li>
                        <li class="phone-carrier" rel="<%= carrier %>">Carrier: <%= carrier %></li>
                        <li>Keyword: <%= keyword %></li>
                    </ul>
                </div>
                <div class="head_chat_title clearfix">
                    <ul class="clearfix">
                        <cfif userList.NUMBERUSER GT 0 AND userList.NUMBERUSER NEQ "">
                            <li class="over_transfer">
                                <a id="transfer" class="btn btn-primary f_right">Transfer</a>
                                <div class="transfer">
                                    <ul>
                                        <cfloop query="userList.LISTUSER">
                                            <li class="transfer-<cfoutput>#userList.LISTUSER.UserId_int#</cfoutput>-<%= room_id %> user-<cfoutput>#userList.LISTUSER.UserId_int#</cfoutput> offline">
                                                <a onclick="Agent.transferUser('<cfoutput>#userList.LISTUSER.UserId_int#</cfoutput>','<cfoutput>#userList.LISTUSER.FirstName_vch# #userList.LISTUSER.LastName_vch#</cfoutput>','<%= room_id %>')" title="<cfoutput>#userList.LISTUSER.FirstName_vch# #userList.LISTUSER.LastName_vch#</cfoutput>">
                                                    <i class="u_onl"></i>
                                                    <span><cfoutput>#userList.LISTUSER.FirstName_vch# #userList.LISTUSER.LastName_vch#</cfoutput></span>
                                                </a>
                                            </li>
                                        </cfloop>
                                    </ul>
                                </div>
                            </li>
                        </cfif>
                        <li><a class="btn btn-danger f_right" onclick="Agent.endRoom('<%= room_id %>')">END</a></li>

                        </ul>
                        <p class="f_left">Session live for <span><span rel="<%= room_id %>" class="<%= room_id %>"></span></span></p>
                    </div>
                    <div class="head_chat_body">
                        <table class="table">
                            <tbody><%= msg_created %></tbody>
                        </table>
                    </div>
                    <div class="head_chat_foot clearfix">
                        <div class="for_type_message f_left clearfix">
                            <input type="text" class="type_message_area f_left" rel="<%= room_id %>" name="area_type" id="area_type" onkeydown="if (event.keyCode == 13) document.getElementById('sendsms_<%= room_id %>').click()"/>
                            <span id="count_char"></span>
                        </div>
                        <button id="sendsms_<%= room_id %>" class="btn btn-primary btn-send-area f_right" rel="<%= room_id %>" onclick="Agent.sendMessage('<%= room_id %>','')" type="submit" name="Send">Send</button>
                        <button onclick="Agent.cannedResponse('<%= room_id %>')" class="btn btn-primary btn-send-area f_right" type="button" name="Canned">Canned</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<!---    Template html transfer a Session --->
<script type="text/html" id="transfer-template">
    <div class="panel panel-default panel-<%= user_id %>-<%= room_id %>" id="panel-<%= room_id %>">
        <div class="panel-heading">
            <h3 class="panel-title inc-title">Request transfer</h3>
        </div>
        <div class="panel-body inc-body">
            <p><span>You</span> transfer a session to <%= user_name %>.</p>
        </div>
        <div class="panel-footer clearfix">
            <a class="btn btn-danger f_right action-transfer" onclick="Agent.cancelTransfer('<%= user_id %>','<%= room_id%>')">CANCEL</a>
        </div>
    </div>
</script>

<!---    Template html receive a Session --->
<script type="text/html" id="get-transfer-template">
    <div class="panel panel-default panel-<%= user_id %>-<%= room_id %>" id="panel-<%= room_id %>">
        <div class="panel-heading">
            <h3 class="panel-title inc-title">incoming transfer</h3>
        </div>
        <div class="panel-body inc-body">
            <p><span><%= user_name %></span> would like to transfer a session to you.</p>
        </div>
        <div class="panel-footer clearfix">
            <a class="btn btn-primary f_right action-transfer" onclick="Agent.acceptTransfer('<%= user_id %>','<%= room_id%>')">ACCEPT</a>
            <a class="btn btn-danger f_right action-transfer" onclick="Agent.declineTransfer('<%= user_id %>','<%= room_id%>')">DECLINE</a>
        </div>
    </div>
</script>

    <cfhtmlhead text='<link rel="stylesheet" href="#rootUrl#/#PublicPath#/css/agent.css">'>
    <cfhtmlhead text='<link rel="stylesheet" href="#rootUrl#/#PublicPath#/css/tooltipster.css">'>
    <cfhtmlhead text='<script src="#rootUrl#/#PublicPath#/js/jquery.tooltipster.min.js" type="text/javascript"></script>'>
    <cfhtmlhead text='<script src="#rootUrl#/#PublicPath#/js/agent/jquery.plugin.js" type="text/javascript"></script>'>
    <cfhtmlhead text='<script src="#rootUrl#/#PublicPath#/js/agent/jquery.countdown.js" type="text/javascript"></script>'>
    <cfhtmlhead text='<script src="#rootUrl#/#PublicPath#/js/agent/agent_chat.js" type="text/javascript"></script>'>
    <cfhtmlhead text='<script src="#rootUrl#/session/campaign/mcid/js/jquery.jgrowl.js"></script>'>

    <!--Incoming transfer-->
    <div id="wrap-incoming"></div>

    <!--End Incoming transfer-->
    <cfif campaignList.RXRESULTCODE EQ 1>
        <!--Start window chat-->
        <div class="window_wrap clearfix">
            <div class="col_group f_left">
                <div class="col_title">
                    <h4>Group</h4>
                </div>
                <div class="col_body">
                    <div class="sidebar left_col_menu">
                        <ul class="sidebar-menu">
                            <cfloop query="campaignList.LISTCAMPAIN">
                                <cfset arrayMatchKeyWord = REMatch('\Keyword {(.*)}', campaignList.LISTCAMPAIN.DESC_VCH) >
                                <cfset arrayMatchShortCode = REMatch('\Short Code {(.*)} ', campaignList.LISTCAMPAIN.DESC_VCH) >
                                <cfif arraylen(arrayMatchShortCode) GT 0 >
                                    <cfset key = replace(arrayMatchKeyWord[1],'Keyword {','') />
                                    <cfset key = replace(key,'}','') />
                                    <cfset code = replace(arrayMatchShortCode[1],'Short Code {','') />
                                    <cfset code = replace(code,'} ','') />
                                    <cfif structKeyExists(url, "code") AND url.code EQ  campaignList.LISTCAMPAIN.BatchId_bi>
                                        <li id="campaign_<cfoutput>#campaignList.LISTCAMPAIN.BatchId_bi#</cfoutput>" class="active" rel="<cfoutput>#key#</cfoutput>" data-short-code="<cfoutput>#code#</cfoutput>" >
                                    <cfelse>
                                        <li id="campaign_<cfoutput>#campaignList.LISTCAMPAIN.BatchId_bi#</cfoutput>" rel="<cfoutput>#key#</cfoutput>" data-short-code="<cfoutput>#code#</cfoutput>" >
                                    </cfif>
                                        <a href="<cfoutput>#rootUrl#/#SessionPath#/agents/campaign_code_manager?code=#campaignList.LISTCAMPAIN.BatchId_bi#</cfoutput>">
                                            <span>campaign <cfoutput>#campaignList.LISTCAMPAIN.BatchId_bi#</cfoutput></span>
                                            <small class="badge f_right bg-yellow">0</small>
                                            <small class="badge f_right bg-red numContact">0</small>
                                            <small class="badge f_right bg-blue tooltip numUnReadMessage" title="<cfoutput>#campaignList.LISTCAMPAIN.DESC_VCH#</cfoutput>">?</small>
                                        </a>
                                        </li>
                                </cfif>
                            </cfloop>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="chat_area f_right" rel="<cfoutput>#campainId#</cfoutput>" id="main_area_chat">
                <div class="head_notification">
                    <p class="text-blue" onclick="Agent.requestphonenumberInQueue()">There are currently <span class="span-peopleinqueue text-red"><a class="get-queue"> 0 </a> people</span> in queue</p>
                </div>
                <div class="chat_area_body">
                    <div id="tab-container" class="tab-container">
                        <div class="wrap_icon" style="display: none">
                            <i class="icon_shoow_more"></i>
                            <span class="number"></span>
                        </div>
                        <div class="show_more_number">
                            <ul id="tabs-wait-chat"></ul>
                        </div>
                        <ul id="etabs" class='etabs'></ul>
                        <div id="tabs_room" class="tabs_cont"></div>
                    </div>
                    <!--New Response-->
                    <div id="wrap-response">
                        <div id="response-manager" class="head_chat_foot top-response clearfix">
                            <div class="for_type_response f_left clearfix">
                                <input type="text" id="search-canned-response-item" class="type_message_area search_response-area f_left" name="search_response" placeholder="Search canned responses">
                            </div>
                            <button class="btn btn-primary btn-send-area f_right" onclick="Agent.newResponse()" type="button" name="">New response</button>
                            <button id="send-canned-response-message" class="btn btn-primary btn-send-area f_right" type="button" name="">Send</button>
                            <button id="cancel_response" class="btn btn-primary btn-send-area f_right" type="button" name="">Cancel</button>
                        </div>
                        <div class="body_response">
                            <input type="hidden" id="current-page" value="<cfoutput>#cannedList.NUMBERCANNEDRESPONSE#</cfoutput>">
                            <ul class="response_tem">
                                <cfloop query="cannedList.CANNEDRESPONSE">
                                    <li class="clearfix cannedLoad">
                                        <cfif (cannedList.CANNEDRESPONSE.OwnerId_int EQ session.USERID
                                            AND (
                                                ( cannedList.CANNEDRESPONSE.Public_bt EQ 1 AND cannedList.CANNEDRESPONSE.Approved_bt EQ 0 )
                                                OR ( cannedList.CANNEDRESPONSE.Public_bt EQ 0 AND cannedList.CANNEDRESPONSE.Approved_bt EQ 0)
                                            )
                                            OR ( session.userrole EQ 'CompanyAdmin' AND cannedList.CANNEDRESPONSE.company_int EQ Session.CompanyId)
                                            )>
                                            <div class="action-res f_left">
                                                <ul>
                                                    <li><a class="btn btn-primary ac_response edit-canned-response" rel="<cfoutput>#cannedList.CANNEDRESPONSE.CannedResponseId_int#</cfoutput>">Edit</a></li>
                                                    <li><a class="btn btn-danger ac_response delete-canned-response" rel="<cfoutput>#cannedList.CANNEDRESPONSE.CannedResponseId_int#</cfoutput>">Delete</a></li>
                                                </ul>
                                            </div>
                                        </cfif>
                                        <div class="response_item f_right" rel="<cfoutput>#htmlEditFormat(cannedList.CANNEDRESPONSE.Response_vch)#</cfoutput>">
                                            <cfoutput><p>#htmlEditFormat(cannedList.CANNEDRESPONSE.Response_vch)#</p></cfoutput>
                                        </div>
                                    </li>
                                </cfloop>
                                <div id="find-not-found" style="display: none">No Results</div>
                            </ul>
                            <div class="loading" style="display: none;">loading ...</div>
                        </div>
                    </div>
                    <!--End new response-->
                </div>
            </div>
        </div>
        <!--End window chat-->
    </cfif>
<cfelse>
    <cfoutput>No campaign data found.</cfoutput>
</cfif>
<cfelse>
    <cfoutput>#HAVE_NOT_PERMISSION#</cfoutput>
</cfif>
