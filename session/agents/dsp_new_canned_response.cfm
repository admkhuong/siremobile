<!---
  Created by neovinhtru on 03/11/2014.
--->
<cfparam name="cannedId" default="0">
<cfif cannedId GT 0>
    <cfinvoke component="#LocalSessionDotPath#.cfc.agenttools" method="GetCannedResponseById" returnvariable="CannedDetail">
        <cfinvokeargument name="inpCannedResponseId" value="#cannedId#">
    </cfinvoke>
</cfif>
<script TYPE="text/javascript">
    $(document).ready(function() {
        $("#content-canned").focus();
        $("#save_canned_response").click(function(){
            if($("#content-canned").val().length > 160){
                jAlert("Please enter content canned response < or = 160 characters.\n", "Failure!", function(result) { } );
                return false;
            }
            if($("#content-canned").val().trim().length == 0){
                jAlert("Please enter content of canned response.\n", "Failure!", function(result) { } );
                return false;
            }
            var inpPublic = 0;
            if($("#canned-modal input#make-public").length > 0){
                inpPublic = $("input#make-public").is(':checked') ? 1: 0;
            }
            var campaignId = 0;
            campaignId = Agent.getParams('code');
            <cfif cannedId GT 0>
                $.ajax({
                    dataType: 'json',
                    async: false,
                    type: "POST",
                    data: {
                        inppublic: inpPublic,
                        inpContentCannedResponse: $("#content-canned").val(),
                        inpCannedResponseId: "<cfoutput>#cannedId#</cfoutput>"
                    },
                    url: "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/agenttools.cfc?method=UpdateCannedResponse&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                    success: function (data) {
                        if(data.RXRESULTCODE <0){
                            jAlert(data.MESSAGE+"\n", "Failure!", function(result) { } );
                        }else{
                            $.jGrowl("You has created canned response success", { life: 2000, position: "center", header: '' });
                            location.reload();
                            return false;
                        }
                    }
                });
            <cfelse>
                $.ajax({
                    dataType: 'json',
                    async: false,
                    type: "POST",
                    data: {
                        inppublic: inpPublic,
                        inpContentCannedResponse: $("#content-canned").val(),
                        inpCampaignId: campaignId
                    },
                    url: "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/cfc/agenttools.cfc?method=CreateCannedResponseForUser&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
                    success: function (data) {
                        if(data.RXRESULTCODE <0){
                            jAlert(data.MESSAGE+"\n", "Failure!", function(result) { } );
                        }else{
                            $.jGrowl("You has created canned response success", { life: 2000, position: "center", header: '' });
                            location.reload();
                            return false;
                        }
                    }
                });
            </cfif>
        });

        <!--- Kill the new dialog --->
        $("#Cancel").click(function() {
            closeDialog();
            return false;
        });
    });
</script>
<div id="canned-modal">
    <cfif session.CompanyId GT 0>
        <div class="public-canned clearfix">
            <label for="make-public">
                <cfif cannedId GT 0>
                    <cfif CannedDetail.PUBLIC_BT EQ 1>
                        <input id="make-public" type="checkbox" name="check" checked="true"/>
                    <cfelse>
                        <input id="make-public" type="checkbox" name="check"/>
                    </cfif>
                <cfelse>
                    <input id="make-public" type="checkbox" name="check" />
                </cfif>
            </label>
            <div class="make-public">
                <h3>Make Public</h3>
                <p>Making a canned response public requires approval form an administrator. You will not be able to edit or delete the response after being reviewed.</p>
            </div>
        </div>
    </cfif>
    <div class="make-canned">
        <div class="content-canned-response">
            <cfif cannedId GT 0>
                <textarea id="content-canned" name="abc" rows="6"><cfoutput>#CannedDetail.RESPONSE_VCH#</cfoutput></textarea>
            <cfelse>
                <textarea id="content-canned" name="abc" rows="6"></textarea>
            </cfif>
        </div>
    </div>
    <div class="canned-foot clearfix">
        <button id="save_canned_response" type="submit" class="btn btn-primary f_right">Save</button>
        <button id="Cancel" type="button" class="btn btn-primary f_right">Cancel</button>
    </div>
</div>