

<!--- Someone globally replaced AgentToolsPermission with AgentToolTitlePermission  WHY?!?!? --->
<cfif NOT AgentToolsPermission.havePermission>

	<h2>You do not have permissions to view this page!<BR />Contact your company administrator if you need privileges turned on.</h2>
    <cfabort />

</cfif>

<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
</cfoutput>

<!---
<style>
	
	#bannerimage {
		background: url("<cfoutput>#rootUrl#/#publicPath#</cfoutput>/images/m1/congruent_pentagon.png") repeat scroll 0 0 / auto 408px #33B6EA;
		border: 0 solid;
		margin-top: 5px;
		padding: 0;
		height:300px;
	}
	
	.header-content {
		color: #858585;
		font-size: 18px;
		padding-top: 10px;
		width: 480px;
		float:left;
		text-align:left;
	}

	.hiw_Info
	{
		color: #444;
	    font-size: 18px;
		text-align:left;	
	}
	
	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.round
    {
        -moz-border-radius: 15px;
        border-radius: 15px;
        padding: 5px;
        border: 2px solid #0085c8;
    }

	h3
	{
		margin-bottom:10px;	
		
	}
	
	#content li
	{
		margin:8px;	
	}
		
	.white
	{		
		color:#FFFFFF;
	}
		
</style>



<style>


	#innertube
	{
		width:100% !important;	
		
	}
	
	h2.super
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	h2, legend, caption 
	{
		font-size: 1.3em;
		font-weight: 500;
	}
	
	h2 
	{
		font-size: 24px;
		font-weight: 700;
	}
	
	h3 
	{
    	margin-bottom: 10px;
	}
	
	h3 
	{
		font-size: 18px;
		font-weight: 200;
	}

	#inner-bg-alt {
		background-color: #ffffff;
		float: left;
		margin-top: 6px;
		min-height: 577px;
		padding-bottom: 0px;
		width: 96%;
	}

	.img-overlay {
		height: 100%;
		left: 0;
		opacity: 0;
		position: absolute;
		top: 0;
		transition: opacity 0.3s ease 0s;
		width: 100%;
		background-color: #fa7d2b; <!--- fa7d2b f64634 --->
		border-radius: 5px;
		z-index:100;

	}
		
	#inner-bg-alt:hover .img-overlay
	{		
		opacity: 0.3;	
		cursor:pointer;	
	}

	.OverTitle
	{
		z-index:120;		
	}

	.ItemTitle
	{		
		padding:10px;
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		z-index:120;	
	}

	.ItemLink
	{		
		width:590px; 
		
		height:40px;
		vertical-align:central;
			
	}
	
	.ItemLink:hover
	{	
				
		background-color: rgba(250, 125, 43, .3);
		transition: opacity 0.3s ease 0s;
	}
	
	
	.LinkItemQAS ul.c 
	{
		text-align:center;
		display:table;
		list-style-type: none;
	}
	
	.LinkItemQAS ul li 
	{   
		float:left;    
	}
	
	.LinkItemQAS ul li a 
	{
		text-decoration:none;
		padding:2px 12px;
		width:580px;
		height:35px;
		display:table-cell;
		vertical-align:middle;
		text-align:left;
		color: #15436c;
    	font-size: 22px;
	    font-weight: normal;
		-moz-border-radius: 8px;
        border-radius: 8px;
	}
	
	.LinkItemQAS ul li a:hover 
	{
		background-color: rgba(250, 125, 43, .3);
		transition: opacity 0.3s ease 0s;
	}


	#MenuTipSlide
	{
		position:absolute;
		top:0px;
		left:0px;
		width: 268px;
		height: 25px;
		border:#C00 1px solid;
		background-color: rgba(250, 125, 43, .8);
		transition: opacity 0.3s ease 0s;
		-moz-border-radius: 8px;
        border-radius: 8px;
		vertical-align:central;
	}

</style>--->

<style>

	h2.super 
	{
		color: #0085c8;
		font-size: 22px;
		font-weight: normal;
		padding-bottom: 8px;
	}
	
	.agentportallink:hover
	{
		text-decoration:underline !important;		
	}
	

	#SelectContactPopUp {
		margin: 15px;
	}
	#InitListPortals_info {
		width: 98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
		
		.paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}
		
		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}
		
		

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		.filter_box{
			width:1294px;
		}
		#InitListPortals{
			width:1299px;
		}
		
		
		#InitListPortals_info {
			width: 1277px;
		}
		
		#InitListPortals_paginate{
			 right: -210px;
		}
		
		table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}
		
		table.dataTable tr td{
			color:#5E6AAD;
		}
		
		table { border-spacing: 0; }
		
	
		.OptionsIcon {
			margin-left: 3px;
			margin-right: 3px;
			cursor: pointer;
			float: left;
			width: 16px;
		}
		
	<!---	#filter_rows .row .filter_key, #filter_rows .row .filter_operator, #filter_rows .row .filter_val 
		{
			float:none !important;	
			
		}--->


</style>


<cfoutput>

<!---
#Session.CompanyUserId#
<BR />
#Session.UserID#
<BR />
--->

  <!---
    <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
        <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
                                    
            <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                
                <ul id='ul1' class='c'>
                    <li><a href='#rootUrl#/#SessionPath#/agents/66502d84-120b-11e4-9036-92e143ddf06c/deductable_a'>UHC Deductable</a></li>
                </ul>    
                             
            </div>
                         
        </div>
    </div>
    
    <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
        <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
                        
            <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
              
                 <ul id='ul1' class='c'>
                 </ul>    
                
            </div>
                         
        </div>
    </div>
    
    <div id="inner-bg-alt" style="min-height: 50px; min-width:1200px;">
        <div id="content" style="border-bottom: 1px solid ##ebebeb; position:relative; padding:0 8px; min-height:25px;">
           
            <div class="hiw_Info LinkItemQAS" style="width:100%; margin-top:10px;">
                
                <ul id='ul1' class='c'>
                </ul>    
                             
            </div>
                   
        </div>
    </div>
    --->
    
</cfoutput>


<div style="clear:both;"></div>
<BR />

<!---this is custom filter for datatable--->
<div id="filter">
	<cfoutput>
	<!---set up column --->
	<cfset datatable_ColumnModel = arrayNew(1)>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Portal Id', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'pkid_int'})>
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Link Name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'LinkTitle_vch'})>
	
	<!---we must define javascript function name to be called from filter later--->
	<cfset datatable_jsCallback = "InitListPortals">
	<cfinclude template="/session/ems/datatable_filter.cfm" >
	</cfoutput>
</div>

<div class="border_top_none">
	<table id="InitListPortals" class="table" cellspacing=0>
	</table>
</div>
       

<script>

	$('#mainTitleText').html('Agent Tools');
	$('#subTitleText').html('Agent Portal Links');		
		
	var _tblListCompanyAgentPortals;
		
	$(function(){
		
		InitListPortals();
	
	});
		
		
	//init datatable for active agent
	function InitListPortals(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListCompanyAgentPortals = $('#InitListPortals').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 20,
		    "aoColumns": [
				
				{"sName": 'LinkTitle_vch', "sTitle": 'Portal Link', "sWidth": '40%',"bSortable": true},
				{"sName": 'Desc_vch', "sTitle": 'Portal Description', "sWidth": '60%',"bSortable": false}
				
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/agentportals.cfc?method=GetListUsersCompanyAgentPortalsDataTable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       
				aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
				
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#InitListPortals').attr('style', '');
				$('.filter_box').css('min-width', '1294px');
				$('#InitListPortals').css('min-width', '1299px');
				//InitEvents();
			}
	    });
	}
	
	
	
	
	
	function dosomething(INPPORTALID){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			jConfirm( "Are you sure you want to do something to this Agent Portal?", "Agent Portal", function(result) { 
				if(result)
				{	
					$.ajax({
						type: "POST", // Posts data as form data rather than on query string and allows larger data transfers than URL GET does
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/agentportals.cfc?method=dosomething&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
						dataType: 'json',
						data:  { INPPORTALID : INPPORTALID},					  
						error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
						success:		
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d) 
							{
								if(d.RXRESULTCODE > 0)
								{
									// reload data
									var oTable = $('#InitListPortals').dataTable();
								  	oTable.fnDraw();							
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("This Agent Portal has NOT been updated.\n"  + d.MESSAGE[0] + "\n" + d.ERRMESSAGE[0], "Failure!", function(result) { } );						
								}
							} 		
						});
				}
				return false;																	
			});
		}
		
		
	
</script> 




<!---
<cfoutput>
Session.DBSourceEBM=#Session.DBSourceEBM#
<BR />
ESIID = #ESIID#
<BR />
CGI.SERVER_NAME=#CGI.SERVER_NAME#
<BR />
SphinxConfigPath = #SphinxConfigPath#

</cfoutput>--->