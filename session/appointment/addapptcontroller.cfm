<cfparam name="inpDesc" default="">


<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>Add Calendar</cfoutput>');
	$('#mainTitleText').text('<cfoutput>Appointment Remiders</cfoutput>');
</script>

<cfoutput>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>

	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables_themeroller.css">

	

</cfoutput>

<style>

	.small {
		font-size: 12px;
		padding: 8px;
	}

	.EBMDialog .inputbox-container 
	{
		width: 350px;
	}
	
	.EBMDialog .inner-txt-box
	{
		height: 400px;
	}
	
	.EBMDialog, .stand-alone-content 
	{
    	padding-bottom: 0;
	}

	.EBMDialog form input 
	{
	   height:22px;
	}

	.btn {
		border-radius: 0px 4px 4px 0px;
		border: none;
		
		
	}
	
	a
	{		
		outline:none !important;		
	}
	
	.odd 
	{
		background-color: #F9F9F9;		
	}
	
	
	.servicemainline 
	{
    	border: 1px solid #0085C8;
    	float: left;
    	width: 100%;
		clear:both;
	}
			
	
	#footer-AAU-Admin 
	{
		border-color: #46A6DD #CCCCCC #CCCCCC;
		border-style: solid;
		border-width: 3px 0 0 0;				
		height: 45px;
		padding: 10px;
		text-align: right;
		width: 650px;
	}
	
	div.hide-info 
	{
		background: url("<cfoutput>#rootUrl#/#PublicPath#/css/ire/images/info.png</cfoutput>") no-repeat scroll 0 6px rgba(0, 0, 0, 0);
		float: left;
		opacity: 0.4;
		width: 18px;
	}
	
	.wrapper-picker-container365:after {
		clear: both;
		content: "";
		display: table;
	}
	
	.wrapper-picker-container365 {
		float: left;
		height: 25px;
		margin-left: 0px;   
		text-align: left;
		width: 200px;
	}
	
	.wrapper-dropdown-picker365:after {
		border-color: #2484C6 transparent;
		border-style: solid;
		border-width: 6px 6px 0;
		content: "";
		height: 0;
		margin-top: -3px;
		position: absolute;
		right: 15px;
		top: 50%;
		width: 0;
	}
	
	.wrapper-dropdown-picker365 {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
		box-shadow:none;
		color: #444444;
		cursor: pointer;
		font-size: 12px;
		height: 16px;
		padding: 5px 10px;
		position: relative;
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		margin-top: 0px;
		width: 365px;	
	}
    #inpGroupPickerDesc{
        float: left;
        overflow: hidden;
        width: 350px;
        text-overflow: ellipsis;
        white-space: nowrap;
        display: inline-block;
    }
</style> 

<cfoutput>


    <div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:800px;">
    
        <div class="EBMDialog">
        
        
          		<div class="header" style="position:relative;">
                    <div class="header-text" >Add a New Appointment Calendar</div>
                    
                <!---  	 <div style="position:absolute; right: 90px; top: 10px; width:50px; height:50px;">
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Your must choose which time zone your appointments are set for.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>	
                    --->
                   
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
                
                
                <div class="servicemainline"></div>                
                
              	<div class="inner-txt-box">
            
                    <div style="padding-bottom: 10px; padding-top: 10px;">
                        
                       <!--- <div class="inputbox-container" style="width:650px;">
                            <label for="inpDesc">Your Appointment Calendar is where you add campaigns to schedule reminders at set intervals before and after the appointment.</label>  
                        </div>--->
                        
                        <div style="clear:both"></div>
                         
                        <div class="messages-lbl">Appointment Calendar Name</div>
                        <div class="message-block">
                            <div class="left-input">
                                <span class="em-lbl">Name</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Give your Appointment Calendar a name so you may more easily identify it later.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                            <div class="right-input"><input type="text" value="Appointment Calendar - #LSDateFormat(now(), 'yyyy-mm-dd')# #LSTimeFormat(now(), 'HH:mm:ss')#" id="inpDesc" name="inpDesc"></div>
                        </div>
    
                        <div style="clear:both"></div>
                        
                        <div class="messages-lbl">Contact List</div>
                        <div class="message-block">                        
                         	<div class="left-input">
                                <span class="em-lbl">Contact List</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Select a Contact List to store Customer Data in.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                            <div class="right-input">
                                <input type="hidden" name="inpGroupPickerId" id="inpGroupPickerId" value="0">
                                 <div class="wrapper-picker-container365">        	    
                                    <div class="wrapper-dropdown-picker365 GroupPicker" tabindex="1"> 
                                        <span id="inpGroupPickerDesc">Click here to select a contact list</span>                                            
                                    </div>                                            
                                </div>
	                        </div>
                        </div>
    
    
                        <div style="clear:both"></div>
                                                              
                                               
                        <div class="messages-lbl">Appointment Calendar Time Zone</div>
                        <div class="message-block">
				        	<div class="left-input">
                                <span class="em-lbl">Time Zone</span>
                                <div class="hide-info showToolTip">&nbsp;</div>
                                <div class="tooltiptext">
                                    Your must choose which time zone your appointments are set for.
                                </div>		
                                <div class="info-block" style="display:none">content info block</div>
                            </div>
                        
                       		<div class="right-input">
                      
                               <select id="inpTimeZone" name="inpTimeZone">
                                   <option value="37">GUAM</option>
                                   <option value="34">SAMOA</option>
                                   <option value="33">HAWAII</option>
                                   <option value="32">ALASKA</option>
                                   <option value="31" selected="selected">PACIFIC</option>
                                   <option value="30">MOUNTAIN</option>
                                   <option value="29">CENTRAL</option>
                                   <option value="28">EASTERN</option>
                                  
                                </select>
                       
                       		</div>
                       
                       </div>
                       
                       
                                
                    </div>
                    
                    <div style="clear:both"></div>
                    <br />
                    <br/>
                 
                    <div id="footer-AAU-Admin">
                        <a id="btnAddAppt" href="javascript:void(0);" class="button filterButton small btn-auu-admin">Create</a> 
                    </div>
    
                    <div id="loadingDlgRenameAppt" style="display:inline;">
                        <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                    </div>
                    
                    
                 <!---   <div class="save-process">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>--->
                    
                                   
                </div>

			</div>
                   
        </div>

	 
	</div>   
    
</cfoutput>


<script TYPE="text/javascript">

	<!--- Global var for the group picker dialog--->
	var $dialogGroupPicker = null;


	function AddApptControllerButton(INPBATCHID) {
		
		$("#loadingDlgRenameAppt").show();		
					
		if($("#inpContactString").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Appointment Calendar has not been created.\n"  + "Desc can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameAppt").hide();	
			return;	
		}
				
		if( parseInt($("#inpGroupPickerId").val()) < 1  )
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Appointment Calendar has not been created.\n"  + "Contact List must Be Selected." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameAppt").hide();	
			return;	
		}
	
		var data = 
		{ 
			inpDesc : $("#inpDesc").val(),
			inpStart : $("#inpTimeZone").val(),
			inpGroupId : $("#inpGroupPickerId").val()	 		
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/appointment/appointment.cfc', 'AddAppointmentController', data, "Error - Appointment Calendar has not been added!", function(d ) {
				
				<!--- Move on to edit --->	
				window.location = 'apptcontroller?inpBatchId=' + d.NEXTBATCHID;
				return false;
			
		});		
	
	}
	
	
	$(function() {	
	
		$('#inpTimeZone').selectmenu({
						style:'popup',
						width: 387 
					});
					
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });	 
		 
		 
		$("#btnAddAppt").click(function() { 
			AddApptControllerButton(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#Cancel").click(function() {
			<!---closeDialog(); --->
			return false;
	  	}); 	
							
		<!--- Open a Group picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Group picker if its still there --->
		$(".GroupPicker").click( function() 
		{			
			<!--- Kill any pickers that may also be opend on this page - table/filters dont play nice with multiple on page--->
			if($dialogGroupPicker != null)
			{							
				$dialogGroupPicker.remove();
				$dialogGroupPicker = null;
			}	
			
			if($dialogGroupPicker  == null)
			{						
				<!--- Load content into a picker dialog--->			
				$dialogGroupPicker = $('<div id="SelectContactPopUp"><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 850,
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Contact List Picker',
					draggable: true,
					resizable: true,
					close: function( event, ui ) {
				      	<!---//now before close dialog, we should remove all styles which have been loaded into this page from external css file --->
				      	<!---removejscssfile("customize.css","css");--->
						$("#SelectContactPopUp").html('');
						if($dialogGroupPicker != null)
						{							
							$dialogGroupPicker.remove();
							$dialogGroupPicker = null;
						}
				     }
				}).load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/dsp_select_contact');
			}
			
			<!---console.log(this);
			console.log($(this).position().left);
			console.log($(this).position().top);
			console.log($(this).position().top + 30);
			console.log($(this).position().top + 30 - $(document).scrollTop());			
			console.log($('#AddToAnotherGroupDialog').dialog("widget").position().left);			
			console.log($('#AddToAnotherGroupDialog').position().top);
			--->
				
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogGroupPicker.dialog('option', 'position', [x,y]);			
	
			$dialogGroupPicker.dialog('open');
		});
		
		$("#loadingDlgRenameAppt").hide();	
	});
		
	function SelectGroup(groupId,groupName){
		$("#inpGroupPickerId").val(groupId);
		$("#inpGroupPickerDesc").text(groupName);
		$dialogGroupPicker.dialog('close');		
	}
		
	
</script>



