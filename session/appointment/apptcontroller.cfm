<cfparam name="inpBatchId" default="0">
<cfparam name="inpGroupId" default="0">


<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="apptcontroller">
</cfinvoke>

<script type="text/javascript">
	$('#subTitleText').text('<cfoutput>Appointment Reminder Controller</cfoutput>');
	$('#mainTitleText').text('<cfoutput>Appointments</cfoutput>');
</script>

<cfoutput>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
    
    <script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>
    
    <script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>	
    
</cfoutput>

<script type="text/javascript">

	<!--- Global var for the Campaign picker dialog--->
	var $dialogCampaignPicker = null;
		
	<!--- allow multiple batch pickers on one page--->
	var $TargetBatchId = null;
	var $TargetBatchDesc = null;
	
	
	$(document).ready(function()
	 { 	 
		 
		 $('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });	 
		
		
		$("#btnAddREM").click(function() { 
			AddREM(); 
			return false; 
		}); 	
	
		
	 }); 
	
	<!--- Dialog to allow editing of Reminder values ---> 
	function EditREM(inpREMId) 
	{
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/appointment/dsp_editrem?inpBatchId=<cfoutput>#inpBatchId#</cfoutput>&inpREMId=" + encodeURIComponent(inpREMId), 
				"Edit Reminder", 
				'auto',
				700,
				"",
				false);
	}
	
	<!--- Delete specified Reminder from list - reload page to refresh --->
	function DeleteREM(inpREMId) 
	{		
		var data = 
		{ 
			inpID :  $("#ID_" + inpREMId).val(),
			inpBatchId : "<cfoutput>#inpBatchId#</cfoutput>" ,
		};
		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc//appointment/appointment.cfc', 'DeleteReminder', data, "Error - Reminder has not been removed!", function(d ) {
			jAlert("Reminder has been removed OK", "Success!", function(result) { 
				
				window.location.reload();
				return false;
			});
		});		
	}
	
	<!--- Dialog to allow editing of Reminder values ---> 
	function AddREM(inpREMId) 
	{
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/appointment/dsp_addrem?inpBatchId=<cfoutput>#inpBatchId#</cfoutput>", 
				"Add Reminder", 
				'auto',
				700,
				"",
				false);
	}
	 
	 
	 <!---//open preview box--->
	function OpenPreviewPopup(batchid, methodtype, elem)
	{
		<!---//methodtype is not voice, show preview popup --->
		if(methodtype !=1 )
		{
			$openDialogInline = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
				width: 600,
				height: 500,
				modal: true,
				dialogClass:'GreyBG',
				autoOpen: false,
				title: 'Preview Message',
				draggable: true,
				resizable: true
			}).load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ems/dsp_preview_ems?batchid='+batchid+'&methodType='+methodtype);
			
			
			var x = elem.offset().left - 300; 
			var y = elem.offset().top + 16 - $(document).scrollTop();
			
			$openDialogInline.dialog('option', 'position', [x,y]);
			$openDialogInline.dialog('open');
		}
	}
	
	function InitializeBabbles(inpNewObj)
	{
		<!--- initialize jplayers based on relX values --->
		$(inpNewObj + " .cp-jplayer").each(function(index, input)
		{
			if(typeof($(this).attr("rel1")) == "undefined")
			{					
				return;
			}	
			if(typeof($(this).attr("rel2")) == "undefined")
			{					
				return;
			}
			
			var CurrIndex = parseInt($(this).attr("rel1"));
			
			$("#jquery_jplayer_bs_" + CurrIndex).jPlayer({
				ready: function () {
					$(this).jPlayer("setmedia", {
						mp3:$(this).attr("rel2")
					});
				},
				cssSelectorAncestor: '#jquery_jplayer_bs_' + CurrIndex,				 	
				swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
				supplied: "mp3",
				wmode: "window"
			});
			
			
			$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { <!---// Using ".jp-repeat" namespace so we can easily remove this event--->
				$("#cp-play-"+index).show();
				$("#cp-pause-"+index).hide();
			});
		});
	}
		
	function Play(selector, index, curObj)
	{
		//check rel2 is mp3 path file
		var mp3file = $("#jquery_jplayer_bs_" + index).attr("rel2");
		//if does not exist, call ajax to load file
		if(mp3file == "")
		{
			//load file to play
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/emergencymessages.cfc?method=LoadMp3File&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				async:false,
				data:  { 
					idFile : $(curObj).data("id")
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {					
				},					  
				success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{								
						CurrRXResultCode = d.RXRESULTCODE;	
						if(CurrRXResultCode > 0)
						{
							$("#jquery_jplayer_bs_" + index).attr("rel2", d.PLAYMYMP3);							
							$("#jquery_jplayer_bs_" + index).jPlayer({
								ready: function () {
									$(this).jPlayer("setmedia", {
										mp3: d.PLAYMYMP3
									}).jPlayer("play");
								},
								cssSelectorAncestor: '#jquery_jplayer_bs_' + index,				 	
								swfPath: "<cfoutput>#rootUrl#/#PublicPath#/swf</cfoutput>",
								supplied: "mp3",
								wmode: "window"
							});							
							
							$("#jquery_jplayer_bs_" + index).bind($.jPlayer.event.ended, function(event) { // Using ".jp-repeat" namespace so we can easily remove this event
								$("#cp-play-"+index).show();
								$("#cp-pause-"+index).hide();
							});
						}
					} 		
			});
		}
		else{
			$(selector).jPlayer("play");	
		}
		$("#cp-play-"+index).hide();
		$("#cp-pause-"+index).show();
	}
	
	
	function Pause(selector, index)
	{
		$(selector).jPlayer("pause");
		$("#cp-play-"+index).show();
		$("#cp-pause-"+index).hide();
	}
		
		
</script>


<style>

	a
	{		
		outline:none !important;		
	}
	

	.EBMDialog, .stand-alone-content 
	{
    	padding-bottom: 0;
	}

	.EBMDialog form input 
	{
	   height:22px;
	}

	.btn {
		border-radius: 0px 4px 4px 0px;
		border: none;
		
		
	}
	
	.odd 
	{
		background-color: #F9F9F9;		
	}
	
	
	.REMDetail
	{
		<!---border: 1px solid #CCCCCC;--->
		border:  none;
		border-radius: 4px;
		display: block;
		margin: 16px 20px 0;	
		padding:8px;
		width:300px;		
	}
	
	.servicemainline 
	{
    	border: 1px solid #0085C8;
    	float: left;
    	width: 100%;
		clear:both;
	}
	
	#footer-AAU-Admin 
	{
		<!---border-color: #46A6DD #CCCCCC #CCCCCC;
		border-style: solid;
		border-width: 3px 0 0 0;			--->	
		height: 45px;
		padding: 10px;
		text-align: right;
		width: 720px;
	}
	
	.REMMenu a
	{
			
		
	}
	
	.REMMenu a:hover
	{
		text-decoration:underline !important;		
	}
	
		
	.wrapper-picker-container365:after {
		clear: both;
		content: "";
		display: table;
	}
	
	.wrapper-picker-container365 {
		float: left;
		height: 25px;
		margin-left: 0px;   
		text-align: left;
		width: 200px;
	}
	
	.wrapper-dropdown-picker365:after {
		border-color: #2484C6 transparent;
		border-style: solid;
		border-width: 6px 6px 0;
		content: "";
		height: 0;
		margin-top: -3px;
		position: absolute;
		right: 15px;
		top: 50%;
		width: 0;
	}
	
	.wrapper-dropdown-picker365 {
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
		box-shadow:none;
		color: #444444;
		cursor: pointer;
		font-size: 12px;
		height: 16px;
		padding: 5px 10px;
		position: relative;
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		margin-top: 0px;
		width: 365px;	
	}
	
</style>


   	<cfinvoke component="#LocalSessionDotPath#.cfc.appointment.appointment" method="ReadAppointmentController" returnvariable="RetVarReadAppointmentController">
        <cfinvokeargument name="inpBatchId" value="#inpBatchId#">
    </cfinvoke>

	<!---<cfdump var="#RetVarReadAppointmentController#" />--->
	
<cfoutput>

	<div id="ADDContactStringToGroupContainer" class="stand-alone-content" style="width:800px;">
        <div class="EBMDialog">
                                          
                <div class="header">
                    <div class="header-text">Appointment Controller - Reminder List</div>
                    <!--- <div class="info-button1"></div>        
                
                    <div style="position:relative;">
                        <div class="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip-right-shadow.gif"/></div>
                        <div class="info-box1">        
                        
                        <span class="info-txt-blue" style="margin-bottom:10px;">Information - Add a new Contact</span>                      
                       
                        <span class="info-txt-black" style="margin-bottom:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In event based messageing, as well as in more traditional forms of advertising, A/B testing (or split testing), is an experimental approach to content design (especially user experience design), which aims to identify changes to messaging content that increase or maximize an outcome of interest (e.g., opt-in rate for a customer loyalty program). As the name implies, two versions (A and B) are compared, which are identical except for one variation that might impact a user's behavior. Version A might be the currently used version, while Version B is modified in some respect. For instance, on a HELP keyword SMS response, the opt out rate is typically a good candidate for A/B testing, as even marginal improvements in drop-off rates can represent a significant gain in sales.</span>
                
                        <span class="info-txt-black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant improvements can be seen through testing elements like copy text, layouts, images and colors. Multivariate testing or bucket testing is similar to A/B testing, but tests more than two different versions at the same time. While the approach is identical to a between-subjects design, which is commonly used in a variety of research traditions, A/B testing is seen as a significant change in philosophy and business strategy. A/B testing as a philosophy of content development brings the field into line with a broader movement toward evidence-based practice.</span>
           
                         
                        </div>
                     </div>--->
                     
                     <span id="closeDialog">Close</span>
                </div>
               
                            
                
                <div class="servicemainline"></div>                
                
                <div style="text-align:left; padding:30px;" align="center">
                
                <cfset REMIndex = 0/>
                	
                <!--- Read in current list  - no paging - should not be more than two or three rmeinders any way --->
                <cfloop index="index" array="#RetVarReadAppointmentController.APPTOBJ.REMS#" >
                
                	<cfset REMIndex = REMIndex + 1/>
                
                	<!--- REM Object List--->
                	<div style="width:100%">
                    
                    	<!--- NAme - Batch Desc Accross the top --->
                    	<div style="width:100%">
                        
                        	<span style="width: 600px; float:left;">
                            	<h2>#index.ID#</h2>
                            </span>
                            
                            <!--- Cleanup for javascript--->
                            <cfset BuffText = REPLACE(index.BDESC, '"', '&quot;') />
                            
                            <form id="REMIndex_#REMIndex#">
                            	<input type="hidden"id="ID_#REMIndex#" value="#index.ID#" />
                                <input type="hidden"id="BATCHID_#REMIndex#" value="#index.BATCHID#" />
                                <input type="hidden"id="ITYPE_#REMIndex#" value="#index.ITYPE#" />
                                <input type="hidden"id="IVAL_#REMIndex#" value="#index.IVAL#" />
                                <input type="hidden"id="BDESC_#REMIndex#" value="#BuffText#" />
                                                                                            
									<!--- Menu options --->
                                    <span class="REMMenu" style="width: 100px; float:right; text-align:right;">
                                        <a onclick='EditREM("#REMIndex#");' title='Edit Reminder Data'>edit</a> | <a onclick='DeleteREM("#REMIndex#")' title='Delete this reminder'>delete</a>
                                    </span>
                            </form>
                                                   	
                        </div>
                        
                        <div style="clear:both"></div>
                    
                        <div style="width:100%">
	                        #index.BDESC#
                        </div>
                    
                        <div>
                        
                        	<!--- Message Preview --->
                        	<div class="REMDetail" style="float:left; ">
                            	<h3>Preview</h3>
                                <BR>                            
                            	<!---#index.BATCHID# --->                                
                                #index.PREVIEWHTML#
                            
                            </div>
                            
                            <!--- Interval --->
                            <div class="REMDetail" style="float:right;">    	                        
                                <h3>Interval</h3>
	                            <br/>
                            	#index.IVAL# #index.ITYPE# Before Appointment
                            </div>
                        
                        </div>
                    
                    	<div style="clear:both"></div>
                    	<br />
                    
                    </div>
                                    
                    <div style="clear:both"></div>
                    <div class="servicemainline"></div>
                    
                    
                    <BR />
                
                </cfloop>
                   
                   
                  	<div id="footer-AAU-Admin">
                       
                        <a href="#rootUrl#/#SessionPath#/contacts/uploadtogroup?inpGroupId=#inpGroupId#&AR=1&ACBID=#inpBatchId#" class="button filterButton small btn-auu-admin" style="">
	                        <img class="EMSIcon" width="16px" height="16px" style="float:left;" src="#rootUrl#/#PublicPath#/css/images/icons16x16/add_contact.png" title="Load Appointment Reminders from CSV"><span style="line-height:16px; margin-left:5px;">Load Data</span>
                        </a>
                       
                        <a href="#rootUrl#/#SessionPath#/appointment/review?inpbatchid=#inpBatchId#&inpGroupId=#inpGroupId#" class="button filterButton small btn-auu-admin" style="">
	                        <img class="EMSIcon" width="16px" height="16px" style="float:left;" src="#rootUrl#/#PublicPath#/css/images/date_picker.jpg" title="Review Appointment Calendar"><span style="line-height:16px; margin-left:5px;">Calender</span>
                        </a>
                       
                        <a id="btnAddREM" href="javascript:void(0);" class="button filterButton small btn-auu-admin">
                        	<img class="EMSIcon" width="16px" height="16px" style="float:left;" src="#rootUrl#/#PublicPath#/images/aau/edit.png" title="Add Appointment Reminder"><span style="line-height:16px; margin-left:5px;">Add Reminder</span>
                        </a> 
                                                
                    </div>    
                        
                        
                 
                   
                
                <!--- Add new scheculed Reminder --->  
                
                <!--- Remove scheculed Reminder --->   
                   
                   
                   
                </div>
               
                              
               
              <!--- <cfdump var="#RetVarReadAppointmentController#" />--->
                                          
                
        </div>
		 
	</div>    
</cfoutput>
