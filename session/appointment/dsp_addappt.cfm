<cfparam name="inpDesc" default="Appointment">
<cfparam name="inpBatchId" default="0">
<cfparam name="inpStart" default="">

<cfset inpEnd = "" />
<cfset inpAllDayFlag = "0" />
<cfset inpContactString = "" />
<cfset inpContactTypeId = "1" />
<cfset inpDuration = "60">
<cfset inpConfirmAdd = "0">
    
    
<cfoutput>
	     

</cfoutput>

<style>

	/* css for timepicker */
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
	.ui-timepicker-div dl { text-align: left; }
	.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
	.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
	.ui-timepicker-div td { font-size: 90%; }
	.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
	
	.ui-timepicker-rtl{ direction: rtl; }
	.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
	.ui-timepicker-rtl dl dt{ float: right; clear: right; }
	.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>

<cfoutput>

    <div class="EBMDialog">
        
        <div class="inner-txt-box">
        
			<div id="AddApptForm">        
        	
                <div style="padding-bottom: 10px; padding-top: 10px;">
                    
                    <div class="messages-lbl">Note</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Note</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Give your Appointment a note so you may more easily identify it later.
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input"><input type="text" value="#inpDesc#" id="inpDesc" name="inpDesc"></div>
                    </div>
        
                    <div style="clear:both"></div>
        
                    <div class="messages-lbl">Date and Time</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Appointment</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Choose Your Appointment Start Time.
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input"><input type="text" value="#inpStart#" id="inpStart" name="inpStart"></div>
                    </div>
        
                    <div style="clear:both"></div>
                    
                    <div class="messages-lbl">Duration</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Duration</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Enter duration of the appointment here
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input">
                        
                            <select id="inpDuration" name="inpDuration">
                               <option value="15" <cfif inpDuration EQ 15>selected</cfif>>15 Minutes</option>
                               <option value="30" <cfif inpDuration EQ 30>selected</cfif>>30 Minutes</option>
                               <option value="45" <cfif inpDuration EQ 45>selected</cfif>>45 Minutes</option>
                               <option value="60" <cfif inpDuration EQ 60 or inpDuration EQ 0>selected</cfif>>1 Hour</option>
                               <option value="120" <cfif inpDuration EQ 120>selected</cfif>>2 Hours</option>
                               <option value="180" <cfif inpDuration EQ 180>selected</cfif>>3 Hours</option>
                               <option value="240" <cfif inpDuration EQ 240>selected</cfif>>4 Hours</option>
                               <option value="580" <cfif inpDuration EQ 580>selected</cfif>>All Day</option>
                            </select>  
                        
                        </div>
                    </div>
                    
                    <div style="clear:both"></div>
                    
                    <input type="hidden" name="inpEnd" id="inpEnd" value="" />
                    <!---                    
                    <div class="inputbox-container">
                        <label for="inpEnd">Choose Your Appointment End Time <span class="small">Required</span></label>            
                        <input id="inpEnd" name="inpEnd" placeholder="Click Here to Choose End Time" size="40" value="#inpEnd#"/>
                    </div>
                    --->
                    
                    <div class="messages-lbl">Contact Type</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Contact Type</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Enter Contact Type Here
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input">
                        
                            <select id="inpContactTypeId" name="inpContactTypeId">
                               <option value="1" <cfif inpContactTypeId EQ 1>selected</cfif>>Voice</option>
                               <option value="2" <cfif inpContactTypeId EQ 2>selected</cfif>>e-mail</option>
                               <option value="3" <cfif inpContactTypeId EQ 3>selected</cfif>>SMS</option>
                            </select>  
                        
                        </div>
                    </div>
                    
                    <div style="clear:both"></div>
                                                  
                    <div class="messages-lbl">Contact String</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Contact String</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Enter Contact String Here. Phone, SMS, or eMail
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input"><input type="text" value="#inpContactString#" id="inpContactString" name="inpContactString"></div>
                    </div>
        
                    <div style="clear:both"></div>
    
                    <div class="messages-lbl">Appointment Confirmed</div>
                    <div class="message-block">
                        <div class="left-input">
                            <span class="em-lbl">Status</span>
                            <div class="hide-info showToolTip">&nbsp;</div>
                            <div class="tooltiptext">
                                Select Confirmation Status Here
                            </div>		
                            <div class="info-block" style="display:none">content info block</div>
                        </div>
                        <div class="right-input">
                        
                            <select id="inpConfirmAdd" name="inpConfirmAdd">
                               <option value="0" <cfif inpConfirmAdd EQ 0>selected</cfif>>Not Confirmed Yet</option>
                               <option value="1" <cfif inpConfirmAdd EQ 1>selected</cfif>>Confirmed Yes</option>
                               <option value="2" <cfif inpConfirmAdd EQ 2>selected</cfif>>Maybe</option>
                               <option value="3" <cfif inpConfirmAdd EQ 3>selected</cfif>>Needs Rescheduled</option>
                               <option value="4" <cfif inpConfirmAdd EQ 4>selected</cfif>>Running Late</option>
                               <option value="5" <cfif inpConfirmAdd EQ 5>selected</cfif>>Canceled</option>
                            </select>  
                      
                        </div>
                    </div>
                    
                    <div style="clear:both"></div>
                    
                    <input type="hidden" name="inpAllDayFlag" id="inpAllDayFlag" value="0" />
                
                    <!---
                
                    <div class="inputbox-container">
                        <label class="left bold_label">All Day Appointment?</label>
                        <input type="radio" name="inpAllDayFlag" value="1" id="inpAllDayFlag" class="apply_yesno"  <cfif inpAllDayFlag EQ 1>selected</cfif>/>                               
                        <h3 class="apply_yesno_label">Yes</h3>
                        <input type="radio" name="inpAllDayFlag" id="inpAllDayFlag" value="0" class="apply_yesno" checked/>                                
                        <h3 class="apply_yesno_label">No</h3>
                    </div>--->
                            
                </div>
                <div id="loadingDlgRenameAppt" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
                <div class='button_area' style="padding-bottom: 10px;">
                    <button 
                        id="btnRenameAppt" 
                        type="button" 
                        class="ui-corner-all survey_builder_button"
                    >Save</button>
                    <button 
                        id="Cancel" 
                        class="ui-corner-all survey_builder_button" 
                        type="button"
                    >Cancel</button>
                </div>
			
            </div>            
        	
        </div>
    
    </div>



</cfoutput>


<!---

t name="<cfargument name="inpStart" required="yes" type="string">
		<cfargument name="inpEnd" required="yes" type="string">   
        <cfargument name="INPBATCHID" TYPE="string" required="yes"/>
        <cfargument name="inpContactString" required="yes" hint="Contact string">
        <cfargument name="inpContactType" required="yes" hint="Contact Type">
        <cfargument name="inpContactId" required="no" hint="Contact Id" default="0">
        <cfargument name="inpDuration" required="no" type="string" default="60" hint="Duration in Minutes. If inpEnd is blank than this will be added to inpStart">   
        <cfargument name="inpDesc" required="no" type="string" default="Appointment" hint="Appointment description - defaults to 'Appointment'">  
        <cfargument name="inpAllDayFlag" re


--->

<script TYPE="text/javascript">
	function AddApptButton(INPBATCHID) {
		
		$("#AddApptForm #loadingDlgRenameAppt").show();		
					
		if($("#AddApptForm #inpContactString").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Appointmnethas not been created.\n"  + "Contact String can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#AddApptForm #loadingDlgRenameAppt").hide();	
			return;	
		}
	
		var data = 
		{ 
			inpDesc : $("#AddApptForm #inpDesc").val(),
			inpStart : $("#AddApptForm #inpStart").val(),
			inpEnd : $("#AddApptForm #inpEnd").val(),
			inpBatchId : "<cfoutput>#inpBatchId#</cfoutput>" ,
			inpContactString : $("#AddApptForm #inpContactString").val(),
			inpContactTypeId : $("#AddApptForm #inpContactTypeId").val(),
			inpContactId : $("#AddApptForm #inpContactId").val(),
			inpDuration : $("#AddApptForm #inpDuration").val(),
			inpAllDayFlag : $("#AddApptForm #inpAllDayFlag").val(),
			inpConfirm : $("#AddApptForm #inpConfirmAdd").val()
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc//appointment/appointment.cfc', 'AddAppointment', data, "Error - Appointment has not been added!", function(d ) {
				
				<!--- Add to Calendar --->
				var StatusColorAdd = '#3a87ad'
				
				switch(parseInt($("#AddApptForm #inpConfirmAdd").val()))
				{
					case(0):	StatusColorAdd = '#3a87ad'; break;	
					case(1):	StatusColorAdd = '#3aad60'; break;	
					case(2):	StatusColorAdd = '#ad433a'; break;	
					case(3):	StatusColorAdd = '#4d3aad'; break;	
					case(4):	StatusColorAdd = '#ad7d3a'; break;	
					case(5):	StatusColorAdd = '#ad9a3a'; break;	
				}
				
				newEvent = 	{								
								title: $("#AddApptForm #inpDesc").val(),
								start: $("#AddApptForm #inpStart").val() + ":00",
								end: null,
								id: parseInt(d.NEWAPPTID),
								allDay: parseInt($("#AddApptForm #inpAllDayFlag").val()),													
								CONTACTID_BI:  parseInt($("#AddApptForm #inpContactId").val()),
								CONTACTTYPEID_INT: parseInt($("#AddApptForm #inpContactTypeId").val()),
								CONTACTSTRING_VCH: $("#AddApptForm #inpContactString").val(),
								DURATION_INT: parseInt($("#AddApptForm #inpDuration").val()),
								color:StatusColorAdd							
								
							};
				
				$('#calendar').fullCalendar( 'renderEvent', newEvent, false  );
				
				closeDialog(); 
				return false;
			
		});		
	
	}
	
	$(function() {	
	
		$('#AddApptForm #inpConfirmAdd').selectmenu({
			style:'popup',
			width: 387 
		});
		
		$('#AddApptForm #inpDuration').selectmenu({
			style:'popup',
			width: 387 
		});
		
		$('#AddApptForm #inpContactTypeId').selectmenu({
			style:'popup',
			width: 387 
		});
		
		$("#AddApptForm #btnRenameAppt").click(function() { 
			AddApptButton(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#AddApptForm #Cancel").click(function() {
			closeDialog(); 
			return false;
	  	}); 	
		
		$('#AddApptForm #inpStart').datetimepicker({
			dateFormat:'yy-mm-dd',
			timeFormat: "HH:mm"
		});
		
		$('#AddApptForm #inpEnd').datetimepicker({
			dateFormat:'yy-mm-dd',
			timeFormat: "HH:mm"
		});

		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });
		 
		$("#AddApptForm #loadingDlgRenameAppt").hide();	
	});
		
		
	
</script>


<style>

#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

.EBMDialog .inputbox-container {
    width: 350px;
}


</style> 

