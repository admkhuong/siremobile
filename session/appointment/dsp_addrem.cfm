<cfparam name="inpBatchId" default="0">


<cfoutput>
	
   

</cfoutput>


<style>

	/* css for timepicker */
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
	.ui-timepicker-div dl { text-align: left; }
	.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
	.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
	.ui-timepicker-div td { font-size: 90%; }
	.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
	
	.ui-timepicker-rtl{ direction: rtl; }
	.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
	.ui-timepicker-rtl dl dt{ float: right; clear: right; }
	.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

</style>

<cfoutput>

    <div class="EBMDialog">
        
        <div class="inner-txt-box">
        
            <div style="padding-bottom: 10px; padding-top: 10px;">
            
                <div class="messages-lbl">Reminder Name</div>
                <div class="message-block">
                    <div class="left-input">
                        <span class="em-lbl">Name</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Give your Reminder a name so you may more easily identify it later.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                    <div class="right-input"><input type="text" value="Appointment Reminder - #LSDateFormat(now(), 'yyyy-mm-dd')# #LSTimeFormat(now(), 'HH:mm:ss')#" id="inpDesc" name="inpDesc"></div>
                </div>
    
                
               <div style="clear:both"></div>
                
               <div class="messages-lbl">Choose Campaign</div> 
               <div class="message-block">
               
                	<div class="left-input">
                        <span class="em-lbl">Campaign</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                           Choose a Campaign from which to deliver messages for this Appointment Reminder
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
               
                   <div class="right-input">
                        <input type="hidden" name="inpBatchPickerId" id="inpBatchPickerId" value="0">
                         <div class="wrapper-picker-container365 BatchPicker" rel1="inpBatchDesc" rel2="inpBatchPickerId">        	    
                            <div class="wrapper-dropdown-picker365" tabindex="1"> 
                                <span id="inpBatchDesc">Click here to select a campaign</span> 
                               <!--- <a id="sbToggle_62009651" class="sbToggle" href="javascript:void(0);"></a>   --->
                            </div>                                            
                        </div>
                    </div>
                </div>
                
                <div style="clear:both"></div>
                
                
                <div class="messages-lbl">Interval Type</div>
                <div class="message-block">
                    <div class="left-input">
                        <span class="em-lbl">Interval Type</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Your must choose which interval type this appointment reminder is set for.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                
                    <div class="right-input">
              
                       <select id="inpITYPE">	                                    
                            <option value="MINUTES">Minute(s)</option>
                            <option value="HOURS">Hour(s)</option>
                            <option value="DAYS" selected="selected">Day(s)</option>
                            <option value="WEEKS">Week(s)</option>
                            <option value="MONTHS">Month(s)</option>   
                            <option value="SUN">Sunday</option> 
                            <option value="MON">Monday</option> 
                            <option value="TUE">Tuesday</option> 
                            <option value="WED">Wednesday</option> 
                            <option value="THU">Thursday</option> 
                            <option value="FRI">Friday</option> 
                            <option value="SAT">Saturday</option>                                                                                                            
                        </select>
               
                    </div>
               
               </div>
               
               
                <div style="clear:both"></div>
                
                
                <div class="messages-lbl">Interval Amount</div>
                <div class="message-block">
                    <div class="left-input">
                        <span class="em-lbl">Interval Value</span>
                        <div class="hide-info showToolTip">&nbsp;</div>
                        <div class="tooltiptext">
                            Your must choose which interval amount this appointment reminder is set for.
                        </div>		
                        <div class="info-block" style="display:none">content info block</div>
                    </div>
                
                    <div class="right-input">
              
                       <select id="inpIVAL">	                                    
                            <option value="0">0</option>
                            <option value="1" selected="selected">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                            <option value="41">41</option>
                            <option value="42">42</option>
                            <option value="43">43</option>
                            <option value="44">44</option>
                            <option value="45">45</option>
                            <option value="46">46</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                            <option value="60">60</option>                                                                                                       
                        </select>
               
                    </div>
               
               </div>
                             
            
            <div id="loadingDlgRenameAppt" style="display:inline;">
                <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
            <div class='button_area' style="padding-bottom: 10px;">
                <button 
                    id="btnRenameAppt" 
                    type="button" 
                    class="ui-corner-all survey_builder_button"
                >Save</button>
                <button 
                    id="Cancel" 
                    class="ui-corner-all survey_builder_button" 
                    type="button"
                >Cancel</button>
            </div>
        
        </div>
    
    </div>

</cfoutput>

<script TYPE="text/javascript">
	function AddREMButton(INPBATCHID) {
		
		$("#loadingDlgRenameAppt").show();		
							
		if($("#inpDesc").val() == '')
		{
			$.alerts.okButton = '&nbsp;OK&nbsp;';
			jAlert("Remider has not been updated.\n"  + "Name can not be blank." + "\n", "Failure!", function(result) { } );										
			$("#loadingDlgRenameAppt").hide();	
			return;	
		}
		
		var data = 
		{ 			
			inpID : $("#inpDesc").val(),
			inpREMBatchId : $("#inpBatchPickerId").val(),
			inpBatchId : "<cfoutput>#inpBatchId#</cfoutput>",
			inpITYPE : $("#inpITYPE").val(),
			inpIVAL : $("#inpIVAL").val()
		};
				
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc//appointment/appointment.cfc', 'AddReminder', data, "Error - Reminder has not been added!", function(d ) {
				
				window.location = 'apptcontroller?inpBatchId=<cfoutput>#inpBatchId#</cfoutput>';
				return false;			
		});		
	
	}
	
		
	$(function() {	
		
		$('#inpITYPE').selectmenu({
			style:'popup',
			width: 387 
		});
		
		$('#inpIVAL').selectmenu({
			style:'popup',
			width: 387 
		});
		
	
		$("#btnRenameAppt").click(function() { 
			AddREMButton(); 
			return false; 
		}); 	
		
		<!--- Kill the new dialog --->
		$("#Cancel").click(function() {
			closeDialog(); 
			return false;
	  	}); 	
		
		
		$('.showToolTip').each(function() {
			 $(this).qtip({
				 content: {
					 text: $(this).next('.tooltiptext')
				 }
			 });
		 });	
		 
			
			
		<!--- Open a Campaign picker dialog - kill any other picker dialogs that may be open first--->
		<!--- Reuse current Campaign picker if its still there --->
		$(".BatchPicker").click( function() 
		{			
						
			if($dialogCampaignPicker == null)
			{		
				<!--- Load content into a picker dialog--->			
				$dialogCampaignPicker = $('<div><img height="24px" width="24px" alt="Information" src="<cfoutput>#rootUrl#/#publicPath#/images</cfoutput>/ajax-loader.gif"></div>').dialog({
					width: 'auto',
					height: 'auto',
					modal: true,
					dialogClass:'GreyBG',
					autoOpen: false,
					title: 'Campaign Picker',
					draggable: true,
					resizable: true
				}).load('../campaign/act_campaignpicker');
			}
					
			<!--- Tie picker dialog to the biottom of the object that opend it.--->
			var x = $(this).position().left; 
    		var y = $(this).position().top + 30 - $(document).scrollTop();
    		$dialogCampaignPicker.dialog('option', 'position', [x,y]);			
	
			$TargetBatchDesc = $("#" + $(this).attr('rel1') ); // $("#inpBatchDescA");
			$TargetBatchId =  $("#" + $(this).attr('rel2') ); //  $("#inpBatchPickerIdA");;
		
			$dialogCampaignPicker.dialog('open');
		}); 		
			
			
			
			
			
		$("#loadingDlgRenameAppt").hide();	
		
		
	});
		
		
	
</script>


<style>

#dialog_renameCampaign
{
	margin:0 0;
	padding:0px;
	font-size:14px;
	left: 30%;
}


#dialog_renameCampaign #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#dialog_renameCampaign #RightStage
{

	padding:15px;
	margin:0px;	
	border: 0;
}


#dialog_renameCampaign h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

.EBMDialog .inputbox-container {
    width: 350px;
}


#inpITYPE
{	
	width:387px !important;	
}

</style> 

