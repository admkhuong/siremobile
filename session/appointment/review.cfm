<cfparam name="inpBatchId" default="1000">


<!--- Load Calendar Resources --->
<cfoutput>
	
    <link rel='stylesheet' href='#rootUrl#/#publicPath#/js/fullcalendar/fullcalendar/fullcalendar.css' />
	
	<script src='#rootUrl#/#publicPath#/js/fullcalendar/lib/moment.min.js'></script>
	<script src='#rootUrl#/#publicPath#/js/fullcalendar/fullcalendar/fullcalendar.js'></script>
    <script src='#rootUrl#/#publicPath#/js/datepicker/timepicker.js'></script>
    
     <link href="#rootUrl#/#publicPath#/js/datepicker/jquery-ui-timepicker-addon.css" type="text/css" media="all" rel="stylesheet">
    
	<script src='#rootUrl#/#publicPath#/js/datepicker/jquery-ui-timepicker-addon.js'></script>
    <script src='#rootUrl#/#publicPath#/js/datepicker/jquery-ui-sliderAccess.js'></script>
    
    <script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>
    <link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
   

</cfoutput>


 	<cfset inpStart = "#dateformat( dateformat(DATEADD("d", -30, now()),'yyyy-mm-dd'),'yyyy-mm-dd')#" & " " & "00:00:00">
    <cfset inpEnd = "#dateformat( dateformat(DATEADD("d", 31, now()),'yyyy-mm-dd'),'yyyy-mm-dd')#" & " " & "23:59:59">
    
    <cfset RangeInDays = DateDiff("d", inpStart, inpEnd) />
    
    <!--- Limit to 90 day range to keep load off DB --->
    <cfif RangeInDays GT 3>
    	<cfset inpEnd = dateformat(DATEADD("d", 3, inpStart),'yyyy-mm-dd') />
    </cfif>

    <cfinvoke component="#LocalSessionDotPath#.cfc.appointment.appointment" method="GetAppointments" returnvariable="RetVarGetAppointments">
        <cfinvokeargument name="inpBatchIdList" value="#inpBatchId#">
        <cfinvokeargument name="inpStart" value="#inpStart#">
        <cfinvokeargument name="inpEnd" value="#inpEnd#">
    </cfinvoke>

    <!--- Dont access directly through DB - this method will help validate user and prevent url hacking of Batch Id--->        				                    
    <cfinvoke method="GetXMLControlString" component="#Session.SessionCFCPath#.batch" returnvariable="RetVarXML">
        <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">  
        <cfinvokeargument name="REQSESSION" value="1">               
    </cfinvoke> 
   
   <script type="text/javascript">
		$('#subTitleText').text('<cfoutput>#RetVarXML.DESC#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>Appointments</cfoutput>');
	</script>
         

<style>

	a
	{		
		outline:none !important;		
	}
	
	
	
	.btn 
	{
		border-radius: 0px 4px 4px 0px;
		border: none;
	}
	.wrapper-picker-container365:after 
	{
		clear: both;
		content: "";
		display: table;
	}
	
	.wrapper-picker-container365 
	{
		float: left;
		height: 25px;
		margin-left: 0px;   
		text-align: left;
		width: 200px;
	}
	
	.wrapper-dropdown-picker365:after 
	{
		border-color: #2484C6 transparent;
		border-style: solid;
		border-width: 6px 6px 0;
		content: "";
		height: 0;
		margin-top: -3px;
		position: absolute;
		right: 15px;
		top: 50%;
		width: 0;
	}
	
	.wrapper-dropdown-picker365 
	{
		background: none repeat scroll 0 0 #FFFFFF;
		border: none;
		box-shadow:none;
		color: #444444;
		cursor: pointer;
		font-size: 12px;
		height: 16px;
		padding: 5px 10px;
		position: relative;
		background: none repeat scroll 0 0 #FFFFFF;
		border: 1px solid #CCCCCC;
		margin-top: 0px;
		width: 365px;	
	}
	

</style>

<script type="text/javascript">
	
	$(document).ready(function() {
	
		<!---// page is now ready, initialize the calendar...--->
	
		$('#calendar').fullCalendar({
			<!---// put your options and callbacks here--->
			
			
		
				events: function(start, end, callback) 
				{
					<!---console.log(start);
					console.log(end);--->
					
					ReadAppointments(start, end, callback);
					
					
				},
				color: 'yellow',   // an option!
				textColor: 'black', // an option!
			

			  eventClick: function(calEvent, jsEvent, view) 
			  {
					
					
					var LastClickEvent = $(this);				
					<!---	
					console.log('Event: ' + calEvent.title);
					console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
					console.log('View: ' + view.name);
					--->
			
					<!---// change the border color just for fun--->
					$(this).css('border-color', 'red');
								
				<!---	OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/appointment/dsp_editappt?inpPKID=" + calEvent.id, 
					"Edit an Appointment", 
					'auto',
					500,
					"",
					false);--->
									
					
					if (newDialog != 0) 
					{
						newDialog.remove();
						newDialog = 0;
					}
										
					var dialogId = "";
										
					newDialog = $('<div id="'+ dialogId +'"></div>');
				
					newDialog
						.load("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/appointment/dsp_editappt?inpPKID=" + calEvent.id)
						.dialog({
							modal : true,
							title: "Edit an Appointment",
							width: 700,
							height: 'auto',
							resizable: true,
							position: 'top',
							beforeClose: function(event, ui) {
								LastClickEvent.css('border-color', '#3A87AD');
							},
							close: function(event, ui)
							{
								$(this).dialog('destroy').remove();
							}
										
						});
					
					
					newDialog.parent().css({top: "80px"});  // position:"fixed" - no - this wont allow small browsers to scroll large dialogs 
							
					newDialog.dialog('open');	
			
				},
				
				dayClick: function(date, jsEvent, view) {

					<!---console.log('Clicked on: ' + date.format());			
					console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);			
					console.log('Current view: ' + view.name);--->
								
					<!---// change the day's background color just for fun--->
					<!---$(this).css('background-color', 'red');--->
					
					
					AddApptNow(date.format());
					
			
				}
	
	
				
		})
		
		<!---newEvent = {
					id: 3,
					title: 'This is a test of the Peterson Appointment - Long form.',
					start: '2014-05-17 16:00:00',
					end: '2014-05-17 17:00:00',
					allDay: false
				};
		
		$('#calendar').fullCalendar( 'renderEvent', newEvent, true  )
		
		 newEvent = {
			 
	  			 	id: 4,
					title: 'This is a test of the Peterson Appointment - Long form.',
					start: '2014-05-17 18:00:00',
					end: '2014-05-17 18:30:00',
					allDay: false
				};
		
		$('#calendar').fullCalendar( 'renderEvent', newEvent, true  )
		--->
		
		
		$('#AddAppt').click(function(){
			AddApptNow();
		});		
		
	
	});
	
	
	
	function ReadAppointments(start, end, callback)
	{
		
		$.ajax({
					type: "POST", // Posts data as form data rather than on query string and allows larger data transfers than URL GET does
					url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/appointment/appointment.cfc?method=GetAppointments&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
					dataType: 'json',
					data:  
					{ 
						inpBatchIdList : "<cfoutput>#inpBatchId#</cfoutput>" ,
						inpStart : "<cfoutput>#inpStart#</cfoutput>" ,
						inpEnd : "<cfoutput>#inpEnd#</cfoutput>" 
					 },					  
					error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
					success:		
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d) 
						{
							if(d.RXRESULTCODE > 0)
							{			
							
								
								var StatusColor = '#3a87ad'
										
								<!--- For each appointment--->		
								for (var i = 0; i < d.APPTS.ROWCOUNT; i++) 
								{	
								
									switch(d.APPTS.DATA.CONFIRMATION_INT[i])
									{
										case(0):	StatusColor = '#3a87ad'; break;	
										case(1):	StatusColor = '#3aad60'; break;	
										case(2):	StatusColor = '#ad433a'; break;	
										case(3):	StatusColor = '#4d3aad'; break;	
										case(4):	StatusColor = '#ad7d3a'; break;	
										case(5):	StatusColor = '#ad9a3a'; break;	
									}
								
									<!--- Create a new event object --->																			
									newEvent = 	{
													title: d.APPTS.DATA.DESC_VCH[i],
													start: d.APPTS.DATA.START_DT[i],
													end: d.APPTS.DATA.END_DT[i],
													allDay: d.APPTS.DATA.ALLDAYFLAG_INT[i],														
													id: d.APPTS.DATA.PKID_INT[i],
													CONTACTID_BI: d.APPTS.DATA.CONTACTID_BI[i],
													CONTACTTYPEID_INT: d.APPTS.DATA.CONTACTTYPEID_INT[i],
													CONTACTSTRING_VCH: d.APPTS.DATA.CONTACTSTRING_VCH[i],
													DURATION_INT: d.APPTS.DATA.DURATION_INT[i],
													color:StatusColor												
												};
									
									
									<!---console.log(newEvent);--->
									
									<!--- Add the event to the calendar --->
									$('#calendar').fullCalendar( 'renderEvent', newEvent, false  )
										
								}
															
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								if (d.DATA.ERRMESSAGE[0] != '') {
									jAlert("This Appointments could NOT be loaded.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );						
								}
							}
						} 		
					});
						
		
		
	}
	
	
	function AddApptNow(inpStart) 
	{
		OpenDialog("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/appointment/dsp_addappt?inpBatchId=<cfoutput>#inpBatchId#</cfoutput>&inpStart=" + encodeURIComponent(inpStart), 
				"Add a new Appointment", 
				'auto',
				700,
				"",
				false);
	}

</script>



<!---<cfdump var="#RetVarGetAppointments#"/>--->

<!---<input id="AddAppt" name="AddAppt" type="button" value="Add" />

<BR/>--->


<!--- <cfdump var="#RetVarXML#"> --->

<cfif RetVarXML.RXResultCode GT 0>
	<div id='calendar'></div>
    <div>
    
   	 	<cfoutput>
            <a href="#rootUrl#/#SessionPath#/contacts/uploadtogroup?inpGroupId=#inpGroupId#&AR=1&ACBID=#inpBatchId#" class="button filterButton small btn-auu-admin" style="">
                <img class="EMSIcon" width="16px" height="16px" style="float:left;" src="#rootUrl#/#PublicPath#/css/images/icons16x16/add_contact.png" title="Load Appointment Reminders from CSV"><span style="line-height:16px; margin-left:5px;">Load Data</span>
            </a>
           
            <a href="#rootUrl#/#SessionPath#/appointment/apptcontroller?inpBatchId=#inpBatchId#&inpGroupId=#inpGroupId#" class="button filterButton small btn-auu-admin" style="">
                <img class="EMSIcon" width="16px" height="16px" style="float:left;" src="#rootUrl#/#PublicPath#/images/aau/edit.png" title="Edit Appointment Reminders"><span style="line-height:16px; margin-left:5px;">Edit Reminders</span>
            </a>
          
   		</cfoutput>                    
    </div>
    
<cfelse>
	You do not have permission to access this Appointment Reminder Calendar. <BR />
    
     <a href="<cfoutput>#rootUrl#/#SessionPath#/ems/campaigncontrolconsole</cfoutput>" class="button filterButton small btn-auu-admin" style="">Choose from List</a>
	                   
    
</cfif>






























