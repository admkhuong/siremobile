<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>

<script type="text/javascript"> 

	$(function()
	{		
		$("#CampaignAdvancedOptionsMain #loadingDlgAdvancedBatchOptions").hide();	
		
		ReadAdvancedBatchOptions();	
		
		$("#CampaignAdvancedOptionsMain #UpdateAdvancedBatchOptionsButton").click( function() 
			{
					UpdateAdvancedBatchOptions();
					WriteCCDXMLString();
					return false;
			}); 
				
			
			
		ReadCCDXMLString();
		
		$("#MC_Stage_CCD #SaveCCD").click(function() { WriteCCDXMLString(); });
		$("#MC_Stage_CCD #CancelCCDFormButton").click(function() { ReadCCDXMLString(); });
		
		
		  
	} );
	
		
	function ReadAdvancedBatchOptions()	
	{	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CampaignAdvancedOptionsMain #loadingDlgAdvancedBatchOptions").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/batch.cfc?method=GetAdvancedBatchData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> }, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $.alerts.okButton = '&nbsp;OK&nbsp;'; jAlert("Service Password for This Campaign Retrieved Error.\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { } );	 <!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLSTRING[0]  --->								
										if(typeof(d.DATA.SERVICEPASSWORD[0]) != "undefined")
										{												
											$("#CampaignAdvancedOptionsMain #INPSERVICEPASSWORD").val(d.DATA.SERVICEPASSWORD[0]);	
										}
																			
										if(typeof(d.DATA.ALLOWDUPLICATES[0]) != "undefined")
											if(parseInt(d.DATA.ALLOWDUPLICATES[0]))								
												$("#CampaignAdvancedOptionsMain #INPALLOWDUPLICATES").attr('checked', true);  
											else
												$("#CampaignAdvancedOptionsMain #INPALLOWDUPLICATES").attr('checked', false); 
										
										
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Service Password for This Campaign Retrieved Error.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );	
									}
								}
								else
								{<!--- Invalid structure returned --->										
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("Service Password for This Campaign Retrieved Error.", "Invalid structure.");										
								}
							}
							else
							{<!--- No result returned --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';					
								jAlert("Service Password for This Campaign Retrieved Error.", "No Response from the remote server. Check your connection and try again.");								
							}
					} 								
		});
	
	}
	
	
	function UpdateAdvancedBatchOptions()	
	{	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CampaignAdvancedOptionsMain #loadingDlgAdvancedBatchOptions").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/batch.cfc?method=UpdateAdvancedBatchOptions&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, INPSERVICEPASSWORD : $("#CampaignAdvancedOptionsMain #INPSERVICEPASSWORD").val(), INPALLOWDUPLICATES : $("#CampaignAdvancedOptionsMain #INPALLOWDUPLICATES").val() }, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $.alerts.okButton = '&nbsp;OK&nbsp;'; jAlert("Service Password for This Campaign Update Error.\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { } );	 <!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
																						
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Service Password for This Campaign Update Error.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );	
									}
								}
								else
								{<!--- Invalid structure returned --->										
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("Service Password for This Campaign Update Error.", "Invalid structure.");										
								}
							}
							else
							{<!--- No result returned --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';					
								jAlert("Service Password for This Campaign Update Error.", "No Response from the remote server. Check your connection and try again.");								
							}
					} 								
		});
	
	}


	var LastReadCCDString = "";

<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools fiel for updating preview menu item--->
		<!--- Output an XML CCD String based on form values --->
	function ReadCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#MC_Stage_CCD #CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		         $.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		    		data:{
		    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
						},
		            dataType: "json", 
		            success: function(d2, textStatus, xhr) {
		            	var d = eval('(' + xhr.responseText + ')');
		          		<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLSTRING[0]  --->								
										if(typeof(d.DATA.FORMATTEDCID[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpCID").val(d.DATA.FORMATTEDCID[0]);												
										
											<!--- Update preview panel--->
											$("#CIDNumber").html(d.DATA.FORMATTEDCID[0]);											
										}
																			
										if(typeof(d.DATA.FILESEQ[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpFileSeq").val(d.DATA.FILESEQ[0]);												
										}
										
										if(typeof(d.DATA.USERSPECDATA[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpUserSpecData").val(d.DATA.USERSPECDATA[0]);												
										}
										
										if(typeof(d.DATA.DRD[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpDRD").val(d.DATA.DRD[0]);												
										}									 
									
										$("#MC_Stage_CCD #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);	
																				
										$("#MC_Stage_CCD #inpCCDSpecial").val(d.DATA.CCDXMLSTRINGRAW[0]);	
										
										LastReadCCDString = d.DATA.CCDXMLSTRINGRAW[0];
																   
									}
									else
									{
										$("#MC_Stage_CCD #CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
		          }
    		 });			
	}
	
	<!--- Output an XML CCD String based on form values --->
	function WriteCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		var inpCCDSpecial = "";
	<!---	 
		console.log(LastReadCCDString);
		console.log($("#MC_Stage_CCD #inpCCDSpecial").val());
		console.log(LastReadCCDString);--->
		
		 
		if($("#MC_Stage_CCD #inpCCDSpecial").val() != LastReadCCDString)
			inpCCDSpecial = $("#MC_Stage_CCD #inpCCDSpecial").val();
		
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpCID : $("#MC_Stage_CCD #inpCID").val(), 
    			 inpFileSeq : $("#MC_Stage_CCD #inpFileSeq").val(), 
    			 inpUserSpecData : $("#MC_Stage_CCD #inpUserSpecData").val(), 
    			 inpDRD : $("#MC_Stage_CCD #inpDRD").val(),
				 inpXML : inpCCDSpecial
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadCCDXMLString();
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.CCDXMLSTRING[0]) != "undefined")
										{									
											$("#MC_Stage_CCD #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);																					
										}									
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
          }
     });
}
	
	
	

</script> 




<!--- Read in each optional value and give an input field for it--->


<cfoutput>


<div id="CampaignAdvancedOptionsMain" class="rxform2">


	<div style="width:600px; min-width:600px;" class="RXFormContainerX" >
        
        <form>
            
            <!--- Default from current User--->
            <!--- Validate e-Mail Address --->
            <label>Service Password
            <span class="small">This is where you specify a custom password for individual service requests to this Campaign</span>
            </label>
            <input type="text" name="INPSERVICEPASSWORD" id="INPSERVICEPASSWORD" class="ui-corner-all" value="Still loading..." > 
        	        
            <br/>
             
            <input TYPE="checkbox" name="INPALLOWDUPLICATES" id="INPALLOWDUPLICATES" class="checkboxInline" value="1" style="display:inline;" title="Allow duplicate calls to the same contact string and type to be made - Advanced users only!"/> 
            <label title="Allow duplicate calls to the same contact string and type to be made - Advanced users only!" style="display:inline;">Allow Duplicates</label>
   			                         
                       
            <br/>
            
            <button id="UpdateAdvancedBatchOptionsButton" TYPE="button">Update Now</button>
            
            <BR />
            
           	<span class="small" title="Use this ID to identify which account this campaign is associated with." style="display:block; width:580px; text-align:right;">The Current User Id for Services is #Session.USERID#</span>
   	
        </form>


	  <div id="loadingDlgAdvancedBatchOptions" style="display:inline;">
             <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
      </div>
                
                
	</div>
	
</div>



<div id="MC_Stage_CCD" class="RXFormContainerX rxform2">


       <div id='RXCCDEditSubMenu'> 
        	<div id='pLinkII'>
            <p></p>
            	<a id='SaveCCD' style='display:inline;' title='Save Changes and Show Call Control Data Summary'>Save</a> |  <a id='CancelCCDFormButton' style='display:inline;' title='Cancel Changes to Call Control Data'>Cancel</a>   
            </div>
       	</div> 
        
        <p class='pActive'></p>	
     
        <form id="CCDForm" name="CCDForm" method="post" action="index.html">
        
        <h1>Call Control Data</h1>
        
        <p>Setup optionl capaign control items here. Redials, Caller Ids, etc...</p>
        
        <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#">
         
        <!--- Validate all numerics 0-9 --->
        <!--- Validate less than 15 digits ---> 
        <label>Caller ID Number
        <span class="small">Add your CID</span>
        </label>
        <input type="text" name="inpCID" id="inpCID" class="ui-corner-all" /> 
       
        <!--- Validate max 255 characters? --->
        <label>File Sequence Number
        <span class="small">Add valid data</span>
        </label>
        <input type="text" name="inpFileSeq" id="inpFileSeq" class="ui-corner-all" >
            
        
        <!--- Validate max 255 characters? --->
        <label>User Specified Data
        <span class="small">Max 255 Characters</span>
        </label>
        <input type="text" name="inpUserSpecData" id="inpUserSpecData" class="ui-corner-all" > 
        
        
        <!--- Validate max 30 days --->    
        <label>Minutes Between Redials
        <span class="small">Default 1440 - One Day</span>
        </label>
        <input type="text" name="inpDRD" id="inpDRD" class="ui-corner-all" > 
        
                    
        <!--- Validate max 255 characters? --->
        <label>Super Duper Advanced
        <span class="small">Add your own customized CCD string options here</span>
        </label>
        <textarea name="inpCCDSpecial" id="inpCCDSpecial" class="ui-corner-all" style="width:450px; min-height:150px; height:150px; min-width:450px; display:inline;"></textarea>
        
             
        
        
        <div class="spacer"></div>
               
        <div id="CurrentCCDXMLString" style="font-size:10px;">
            NA
        </div>
        
        </form>
        
        <p class='pActive'></p>	
        
</div>     

</cfoutput>