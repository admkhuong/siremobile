
<cfparam name="inpGroupId" default="0">
<cfparam name="inpShowSocialmediaOptions" default="0">

<cfif inpShowSocialmediaOptions GT 0>
	<cfset BatchTitle = "My Babble Blasts">
    <cfset BatchPopupTitleText = "Babble Blast">
<cfelse>
	<cfset BatchTitle = "Message Queue">
    <cfset BatchPopupTitleText = "Campaign">
</cfif>



<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
font:Verdana, geneva, sans-serif
	}
	
<!---	.dock-container_Blaster { position: relative; top: -8px; height: 50px; padding-left: 20px; z-index:1; }--->

#tt {
 position:absolute;
 display:block;
 
 }
 #tttop {
 display:block;
 height:5px;
 margin-left:5px;
 
 overflow:hidden;
 }
 #ttcont {
 display:block;
 padding:2px 12px 3px 7px;
 margin-left:5px;
 background:#666;
 color:#fff;
 }
#ttbot {
display:block;
height:5px;
margin-left:5px;

overflow:hidden;
}		




</style>
<cfoutput>
	
<script src="#rootUrl#/#PublicPath#/js/help/jquery.tipsy.js" type="text/javascript" charset="utf-8"></script>
</cfoutput>
<script>
var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
var ShowRequest = 0;

$(function() {
	$("#BatchList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
		datatype: "json",
		height: "auto",
		colNames:['Status','ID', 'Desc', 'Date Time (Pacific)','Total', 'Queued', 'In Progress', 'Complete', 'Options'],
		colModel:[			
			{name:'Status',index:'Status', width:100, editable:false, sortable:false, resizable:false},
			{name:'BatchId_bi',index:'BatchId_bi', width:75, editable:false, resizable:false},
			{name:'DESC_VCH',index:'DESC_VCH', width:100, editable:false, resizable:false},
			{name:'Created_dt',index:'Created_dt', width:150, editable:false, resizable:false},
			{name:'CountTotal',index:'CountTotal', width:60, editable:false, sortable:false, resizable:false},
			{name:'CountQueued',index:'CountQueued', width:60, editable:false, sortable:false, resizable:false},
			{name:'CountQueuedOnRXDialer',index:'CountQueuedOnRXDialer', width:60, editable:false, sortable:false, resizable:false},
			{name:'CountComplete',index:'CountComplete', width:60, editable:false, sortable:false, resizable:false},
			{name:'Options',index:'Options', width:125, editable:false, align:'left', sortable:false, resizable:false}
		],
		rowNum:20,
	   	//rowList:[10,20,30,40],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Blasts Found.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'Created_dt',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(Created_dt){ if(!ShowRequest) $('#BatchList').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(Created_dt){
				if(Created_dt && Created_dt!==lastsel){
					$('#BatchList').jqGrid('restoreRow',lastsel);
					//$('#BatchList').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=Created_dt;
				}
			},
		 loadComplete: function(data){ 		 
				$(".del_RowBatches").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_RowBatches").click( function() { DeleteQueuedPhoneNumbers($(this).attr('rel')); } );
		 		
		 		$(".publish_FacebookBatches").hover(function(){
		 			$(this).addClass("ui-state-hover");
		 		}, function(){
		 			$(this).removeClass("ui-state-hover");
		 		});
		 		
		 		
		 		$(".publish_ShareBatches").click( function() { 
		 			var batchId = $(this).attr('rel');
		 			var $shareSocialDialog = $('<div id="shareSocialDialog" style="padding-left:10px"></div>');
					$shareSocialDialog
						.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/social/dsp_shareMenu',{INPBATCHID:batchId})
						.dialog({
							modal : true,
							show: {effect: "fade", duration: 1000},
							hide: {effect: "fold", duration: 500},
							title: 'Social media share',
							width: 250,
							height: 80,
							position:'center',
							close: function() { 
								$shareSocialDialog.remove(); 
							}
						});
				
					$shareSocialDialog.dialog('open');
		 		});
				$(".view_RowBatches").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_RowBatches").click( function() { ViewBatchDetailsDialog($(this).attr('rel')); } );
		 		$('.tooltip_er').tipsy({gravity: 'w',html: true});
   			},	
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdatePhoneData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "1" //will default second column as ID
			  }	
	});
	
	$('#pagerDiv').hide();

	$("#BatchList_Created_dt").css('text-align', 'left');
	$("#BatchList_BatchId_bi").css('text-align', 'left');
	$("#BatchList_Status").css('text-align', 'left');
	$("#BatchList_CountTotal").css('text-align', 'left');
	$("#BatchList_CountQueued").css('text-align', 'left');
	$("#BatchList_CountQueuedOnRXDialer").css('text-align', 'left');
	$("#BatchList_CountComplete").css('text-align', 'left');
	$("#BatchList_Options").css('text-align', 'right');

	$("#inpGroupId").change(function() 
    { 
		LastGroupId = $("#inpGroupId").val();
		gridReloadBatches();

	});
	

	$("#tester").click(function() { gridReloadBatches();  });	
	$("#UploadTesting").click(function() { UploadPhoneToListDialog(); return false; });
	
	
	//$("#refresh_List_Batch").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#BatchList_toppager_left").html("<b><cfoutput>#BatchTitle#</cfoutput></b><img id='refresh_Activity_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
	//$("#refresh_List_Batch").click(function() { gridReloadBatches(); return false; });
		$("#refresh_Activity_List").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#refresh_Activity_List").click(function() { gridReloadBatches(); return false; });
	
	$("#OustandingRequests").click(function() { 
	
		if(ShowRequest)
		{
			ShowRequest = 0;
			gridReloadBatches();	
		}
		else
		{			
			ShowRequest = 1;
			gridReloadBatches();			
		}
			
		return false; 
	
	});
		
	$("#tab-panel-BabbleBlasts").toggleClass("ui-tabs-hide");
		
<!---		
		
	$('#tab-panel-BabbleBlasts #dockBabbleBlasts').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_Blaster',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
--->		
				
});



<!---
This function is called after an add/edit happens. We just take the MSG from the response and display it in the toolbar.
Note currently, since we reload the grid after the add/edit, the msg will only be visible for a short second or so
--->
	function commonSubmit(data,params)
	{
		return true;
	}	


function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}



	function refreshGrid()
	{
	   var grid = $("#BatchList");
	   grid.trigger("reloadGrid");
	}

function doSearch(ev){
	
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReloadBatches,500)
}

function gridReloadBatches(){
		var ParamStr = '';
		var CurrSORD =  $("#BatchList").jqGrid('getGridParam','sord');
		var CurrSIDX =  $("#BatchList").jqGrid('getGridParam','sidx');
		var CurrPage = $('#BatchList').getGridParam('page'); 
		
		if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
			
		if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
		
		if(ShowRequest)  ParamStr += '&inpSocialmediaRequests=1';

	$("#BatchList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&inpSocialmediaFlag=1&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:CurrPage}).trigger("reloadGrid");

}

function enableAutosubmit(state){
	flAuto = state;
	gridReloadBatches();
	$("#submitButton").attr("disabled",state);
}



var addgroupToCallQueueDialog = 0;

function addgroupToCallQueueDialogCall(inpSocialmediaFlag)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	var ParamStr = '';
						
//	if(typeof(inpSocialmediaFlag) != "undefined" && inpSocialmediaFlag != "")
//	{					
		ParamStr = '<cfoutput>#rootUrl#/#SessionPath#/distribution/dsp_addgroupToQueue?inpShowSocialmediaOptions=#inpShowSocialmediaOptions#</cfoutput>';
//	}
//	else
//	{
//		ParamStr = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/distribution/dsp_addgroupToQueue'; 
//	}

	<!--- Erase any existing dialog data --->
	if(addgroupToCallQueueDialog != 0)
	{
		addgroupToCallQueueDialog.remove();
		addgroupToCallQueueDialog = 0;
	}
					
	addgroupToCallQueueDialog = $('<div></div>').append($loading.clone());
	
	
	addgroupToCallQueueDialog
		.load(ParamStr)
		.dialog({
			modal : true,
			title: 'Publish To Call Queue',
			width: 700,
			position:'top',
			height: 'auto'
		});

	addgroupToCallQueueDialog.dialog('open');

	return false;		
}	

function DeleteQueuedPhoneNumbers(INPBATCHID)
{	
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to remove\n(" + INPBATCHID + ") \n\nAre you absolutely sure?", "About to remove remaining queued number(s) from your calling gqueue.", function(result) { if(!result){$("#loadingPhoneList").hide();  $("#BatchList").setSelection(lastsel);  return;}else{	
		
		  $("#loadingPhoneList").show();		
				
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=DeleteQueuedPhoneNumbers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { INPBATCHID: String(INPBATCHID), inpSocialmediaFlag : 1},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
							}
																										
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridReloadBatches();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;
}



<!--- Global so popup can refernece it to close it--->
var CreateViewBatchDetailsDialog = 0;

function ViewBatchDetailsDialog(INPBATCHID)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(INPBATCHID) != "undefined" && INPBATCHID != "")					
		ParamStr = '?inpbatchid=' + encodeURIComponent(INPBATCHID);
						
	<!--- Erase any existing dialog data --->
	if(CreateViewBatchDetailsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateViewBatchDetailsDialog.remove();
		CreateViewBatchDetailsDialog.dialog('destroy');
		CreateViewBatchDetailsDialog.remove();
		CreateViewBatchDetailsDialog = 0;
		
	}
					
	CreateViewBatchDetailsDialog = $('<div></div>').append($loading.clone());
	
	CreateViewBatchDetailsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_ViewBatchDetails' + ParamStr)
		.dialog({
			modal : true,
			title: 'Message Queue Details',
			width: 650,
			height: 'auto',
			position: 'top', 
			dialogClass: 'noTitleStuff'
		});

	CreateViewBatchDetailsDialog.dialog('open');

	return false;		
}

var tooltip=function(){
 var id = 'tt';
 var top = 3;
 var left = 3;
 var maxw = 300;
 var speed = 10;
 var timer = 20;
 var endalpha = 95;
 var alpha = 0;
 var tt,t,c,b,h;
 var ie = document.all ? true : false;
 return{
  show:function(v,w){
  	v='error message';
   if(tt == null){
    tt = document.createElement('div');
    tt.setAttribute('id',id);
    t = document.createElement('div');
    t.setAttribute('id',id + 'top');
    c = document.createElement('div');
    c.setAttribute('id',id + 'cont');
    b = document.createElement('div');
    b.setAttribute('id',id + 'bot');
    tt.appendChild(t);
    tt.appendChild(c);
    tt.appendChild(b);
    document.body.appendChild(tt);
    tt.style.opacity = 0;
    tt.style.filter = 'alpha(opacity=0)';
    document.onmousemove = this.pos;
   }
   tt.style.display = 'block';
   c.innerHTML = v;
   tt.style.width = w ? w + 'px' : 'auto';
   if(!w && ie){
    t.style.display = 'none';
    b.style.display = 'none';
    tt.style.width = tt.offsetWidth;
    t.style.display = 'block';
    b.style.display = 'block';
   }
  if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
  h = parseInt(tt.offsetHeight) + top;
  clearInterval(tt.timer);
  tt.timer = setInterval(function(){tooltip.fade(1)},timer);
  },
  pos:function(e){
   var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
   var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
   tt.style.top = (u - h) + 'px';
   tt.style.left = (l + left) + 'px';
  },
  fade:function(d){
   var a = alpha;
   if((a != endalpha && d == 1) || (a != 0 && d == -1)){
    var i = speed;
   if(endalpha - a < speed && d == 1){
    i = endalpha - a;
   }else if(alpha < speed && d == -1){
     i = a;
   }
   alpha = a + (i * d);
   tt.style.opacity = alpha * .01;
   tt.style.filter = 'alpha(opacity=' + alpha + ')';
  }else{
    clearInterval(tt.timer);
     if(d == -1){tt.style.display = 'none'}
  }
 },
 hide:function(){
  clearInterval(tt.timer);
   tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
  }
 };
}();
</script>
     
	    
<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->  
	
    <div id="tab-panel-BabbleBlasts" class="ui-tabs-hide" style="position:relative; overflow: hidden;">   
      
        <div style="table-layout:fixed; min-height:500px; height:500px;">
                
            <div id="pagerDiv"></div>
                           
            <div style="text-align:left;">
            <table id="BatchList"></table>
            </div>
        
        </div>
        
     <!---   <br />
        <br />
          
   	   <div id="dockBabbleBlasts" style="text-align:left;">
            <div class="dock-container_Blaster">
                <a class="dock-item" href="##" onclick="addgroupToCallQueueDialogCall(1); return false;"><span>Start a New #BatchPopupTitleText#</span><img src="../../public/images/dock/Add_Web_64x64II.png" alt="Send a New #BatchPopupTitleText#" /></a> 
                <!--- Add a blank here to force menu to expand right - single item seems to expand left if not a second menu item--->
                <a class="dock-item BBMainMenu" href="##" onclick="return false;"><span></span><img src="../../public/images/dock/Spacer_Web_64x64.png" alt="" /></a>
             </div>
        </div>
	--->
        
	</div>        
	
</cfoutput>    
     
