<cfparam name="inpShowSocialmediaOptions" default="0">

<cfif inpShowSocialmediaOptions GT 0>
	<cfset BatchTitle = "My Babble Blasts">
    <cfset BatchPopupTitleText = "Babble Blast">
<cfelse>
	<cfset BatchTitle = "Calling Queue">
    <cfset BatchPopupTitleText = "Calling Campaign">
</cfif>

<style>

<style>

.printable {
border: 1px dotted #CCCCCC ;
padding: 10px 10px 10px 10px ;
font:Verdana, geneva, sans-serif
	}
	
<!---	.dock-container_Blaster { position: relative; top: -8px; height: 50px; padding-left: 20px; z-index:1; }--->

		
</style>



</style>


<script>

var timeoutHnd;
var flAuto = true;
var lastsel;
var FirstClick = 0;
var isInEdit = false;
var LastGroupId = 0;
var ShowRequest = 0;
	
$(function() {

	$("#BatchesListMultiChannel").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
		datatype: "json",
		height: 350,
		colNames:['Status','Date Time (Pacific)','Total', 'Queued', 'In Progress', 'Complete', 'Options'],
		colModel:[			
			{name:'Status',index:'Status', width:150, editable:false, sortable:false, resizable:false},
		//	{name:'BatchId_bi',index:'BatchId_bi', width:100, editable:false, resizable:false},
			{name:'Created_dt',index:'Created_dt', width:200, editable:false, resizable:false},
			{name:'CountTotal',index:'CountTotal', width:100, editable:false, sortable:false, resizable:false},
			{name:'CountQueued',index:'CountQueued', width:100, editable:false, sortable:false, resizable:false},
			{name:'CountQueuedOnRXDialer',index:'CountQueuedOnRXDialer', width:100, editable:false, sortable:false, resizable:false},
			{name:'CountComplete',index:'CountComplete', width:100, editable:false, sortable:false, resizable:false},
			{name:'Options',index:'Options', width:75, editable:false, align:'right', sortable:false, resizable:false}
		],
		rowNum:15,
	//   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Blasts Found.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'Created_dt',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
	//	ondblClickRow: function(Created_dt){ if(!ShowRequest) $('#BatchesListMultiChannel').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEdit,'', ''); },
		onSelectRow: function(Created_dt){
				if(Created_dt && Created_dt!==lastsel){
					$('#BatchesListMultiChannel').jqGrid('restoreRow',lastsel);
					//$('#BatchesListMultiChannel').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEdit,'', '');
					lastsel=Created_dt;
				}
			},
		 loadComplete: function(data){ 		 
				$(".del_RowBatches").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_RowBatches").click( function() { DeleteQueuedPhoneNumbers($(this).attr('rel')); } );
				$(".view_RowBatches").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_RowBatches").click( function() { ViewBatchDetailsDialog($(this).attr('rel')); } );
   			},	
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdatePhoneData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "1" //will default second column as ID
			  }	
	});
	
	$('#pagerDiv').hide();

	$("#BatchesListMultiChannel_Created_dt").css('text-align', 'left');
	$("#BatchesListMultiChannel_Status").css('text-align', 'left');
	$("#BatchesListMultiChannel_CountTotal").css('text-align', 'left');
	$("#BatchesListMultiChannel_CountQueued").css('text-align', 'left');
	$("#BatchesListMultiChannel_CountQueuedOnRXDialer").css('text-align', 'left');
	$("#BatchesListMultiChannel_CountComplete").css('text-align', 'left');
	$("#BatchesListMultiChannel_Options").css('text-align', 'right');


	

	$("#tester").click(function() { gridReloadBatches();  });	
	$("#UploadTesting").click(function() { UploadPhoneToListDialog(); return false; });
	
	
	$("#refresh_List_Batch").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#BatchesListMultiChannel_toppager_left").html("<b><cfoutput>#BatchTitle#</cfoutput></b><img id='refresh_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
	$("#refresh_List_Batch").click(function() { gridReloadBatches(); return false; });
	
	$("#OustandingRequests").click(function() { 
	
		if(ShowRequest)
		{
			ShowRequest = 0;
			gridReloadBatches();	
		}
		else
		{			
			ShowRequest = 1;
			gridReloadBatches();			
		}
			
		return false; 
	
	});
		
	$("#tab-panel-Batches_MultiChannel").toggleClass("ui-tabs-hide");	
		
	$('#dockBatches_MultiChannel').Fisheye(
		{
			maxWidth: 30,
			items: 'a',
			itemsText: 'span',
			container: '.dock-container_Blaster',
			itemWidth: 50,
			proximity: 60,
			alignment : 'left',
			valign: 'bottom',
			halign : 'left'
		}
	);
		
});



<!---
This function is called after an add/edit happens. We just take the MSG from the response and display it in the toolbar.
Note currently, since we reload the grid after the add/edit, the msg will only be visible for a short second or so
--->
	function commonSubmit(data,params)
	{
		return true;
	}	


function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}





function doSearch(ev){
	
	if(!flAuto)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHnd)
		clearTimeout(timeoutHnd)
	timeoutHnd = setTimeout(gridReloadBatches,500)
}

function gridReloadBatches(){
	
		var ParamStr = '';
		var CurrSORD =  $("#BatchesListMultiChannel").jqGrid('getGridParam','sord');
		var CurrSIDX =  $("#BatchesListMultiChannel").jqGrid('getGridParam','sidx');
		var CurrPage = $('#BatchesListMultiChannel').getGridParam('page'); 
		
		if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
			
		if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
		
		if(ShowRequest)  ParamStr += '&inpSocialmediaRequests=1';

	$("#BatchesListMultiChannel").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&inpSocialmediaFlag=1&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:CurrPage}).trigger("reloadGrid");

}

function enableAutosubmit(state){
	flAuto = state;
	gridReloadBatches();
	$("#submitButton").attr("disabled",state);
}



var addgroupToCallQueueDialog = 0;

function addgroupToCallQueueDialogCall(inpSocialmediaFlag)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
	
	var ParamStr = '';
						
//	if(typeof(inpSocialmediaFlag) != "undefined" && inpSocialmediaFlag != "")
//	{					
		ParamStr = '<cfoutput>#rootUrl#/#SessionPath#/distribution/dsp_addgroupToQueue?inpShowSocialmediaOptions=#inpShowSocialmediaOptions#</cfoutput>';
//	}
//	else
//	{
//		ParamStr = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/distribution/dsp_addgroupToQueue'; 
//	}

	<!--- Erase any existing dialog data --->
	if(addgroupToCallQueueDialog != 0)
	{
		addgroupToCallQueueDialog.remove();
		addgroupToCallQueueDialog = 0;
	}
					
	addgroupToCallQueueDialog = $('<div></div>').append($loading.clone());
	
	
	addgroupToCallQueueDialog
		.load(ParamStr)
		.dialog({
			modal : true,
			title: 'Publish To Call Queue',
			width: 700,
			position:'top',
			height: 'auto'
		});

	addgroupToCallQueueDialog.dialog('open');

	return false;		
}	

	
	

function DeleteQueuedPhoneNumbers(INPBATCHID)
{	
	$.alerts.okButton = '&nbsp;Yes&nbsp;';
	$.alerts.cancelButton = '&nbsp;No&nbsp;';		
	$.alerts.confirm( "About to remove\n(" + INPBATCHID + ") \n\nAre you absolutely sure?", "About to remove remaining queued number(s) from your calling gqueue.", function(result) { if(!result){$("#loadingPhoneList").hide();  $("#BatchesListMultiChannel").setSelection(lastsel);  return;}else{	
		
		  $("#loadingPhoneList").show();		
				
		  $.ajax({
		  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=DeleteQueuedPhoneNumbers&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true', 
		  dataType: 'json',
		  data:  { INPBATCHID: String(INPBATCHID), inpSocialmediaFlag : 1},					  
		  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},					  
		  success:
			  
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{								
							
							}
																										
							$("#loadingPhoneList").hide();
					
						}
						else
						{<!--- Invalid structure returned --->	
							$("#loadingPhoneList").hide();
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
					
					gridReloadBatches();
			} 		
					
		});

	 }  } );<!--- Close alert here --->


	return false;
}



<!--- Global so popup can refernece it to close it--->
var CreateViewBatchDetailsDialog = 0;

function ViewBatchDetailsDialog(INPBATCHID)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(INPBATCHID) != "undefined" && INPBATCHID != "")					
		ParamStr = '?inpbatchid=' + encodeURIComponent(INPBATCHID);
						
	<!--- Erase any existing dialog data --->
	if(CreateViewBatchDetailsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateViewBatchDetailsDialog.remove();
		CreateViewBatchDetailsDialog = 0;
		
	}
					
	CreateViewBatchDetailsDialog = $('<div></div>').append($loading.clone());
	
	CreateViewBatchDetailsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_ViewBatchDetails' + ParamStr)
		.dialog({
			modal : true,
			title: 'Calling Queue Details',
			width: 660,
			height: 'auto'
		});

	CreateViewBatchDetailsDialog.dialog('open');

	return false;		
}

</script>
     
	    
<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-Batches_MultiChannel" class="ui-tabs-hide" style="position:relative;">   
      
        <div style="table-layout:fixed; min-height:475px; height:475px;">
                
            <div id="pagerDiv"></div>
                           
            <div style="text-align:left;">
            <table id="BatchesListMultiChannel"></table>
            </div>
        
        </div>
        
	</div>        
    
    <div style="position:relative; bottom:-80px;">                           

    <!---http://www.iconfinder.com/icondetails/13509/24/delete_group_users_icon--->
        <div id="dockBatches_MultiChannel" style="text-align:left;">
            <div class="dock-container_Blaster">
                <a class="dock-item" href="##" onclick="addgroupToCallQueueDialogCall(1); return false;"><span>Start a New #BatchPopupTitleText#</span><img src="../../public/images/dock/Add_Web64x64.png" alt="Send a New #BatchPopupTitleText#" /></a> 
                <!--- Add a blank here to force menu to expand right - single item seems to expand left if not a second menu item--->
                <a class="dock-item BBMainMenu" href="##" onclick="return false;"><span></span><img src="../../public/images/dock/Spacer_Web_64x64.png" alt="" /></a>
             </div>
        </div>
        
	</div>
	
</cfoutput>    
     
