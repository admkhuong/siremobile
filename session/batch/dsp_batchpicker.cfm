<cfparam name="inpCallBackUpdateBatchId" default="NAF">

<!--- NAF is Not a Function --->
<cfif inpCallBackUpdateBatchId EQ ""><cfset inpCallBackUpdateBatchId = "NAF"></cfif>

<style>



	
</style>


<script>

var timeoutHndBatchPickerContent;
var fAutoBatchPickerContent = true;
var LastSelBatchPickerContent = 0;
var LastGroupIdBatchPickerContent = 0;
var lastObj_MCContent;
var sipPos_MCContentFilters = 0;
	
$(function() {

	$("#BatchPickerList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchesMCContent&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&inpSocialmediaFlag=1',
		datatype: "json",
		height: 385,
		colNames:['ID','Description','Created Date', 'Last Updated Date' ],
		colModel:[			
			{name:'BatchId_bi',index:'BatchId_bi', width:100, editable:false, resizable:false},
			{name:'Desc_vch',index:'Desc_vch', width:320, editable:true, resizable:false},
			{name:'Created_dt',index:'Created_dt', width:125, editable:false, resizable:false},
			{name:'LASTUPDATED_DT',index:'LASTUPDATED_DT', width:125, editable:false, resizable:false},			
			<!---{name:'Options',index:'Options', width:105, editable:false, align:'right', sortable:false, resizable:false}--->
		],
		rowNum:15,
	//   	rowList:[20,4,8,10,20,30],
		mtype: "POST",
		pager: $('#pagerDiv'),
	//	pagerpos: "right",
	//	cellEdit: false,
		toppager: true,
    	emptyrecords: "No Current Campaigns Found.",
    	pgbuttons: true,
		altRows: true,
		altclass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'Created_dt',
		toolbar:[false,"top"],
		viewrecords: true,
		sortorder: "DESC",
		caption: "",	
		multiselect: false,
		ondblClickRow: function(BatchId_bi){ 
												if(typeof(<cfoutput>"#inpCallBackUpdateBatchId#"</cfoutput>) != 'undefined')
												{
			 										<cfoutput>#inpCallBackUpdateBatchId#</cfoutput>(BatchId_bi, $("#BatchPickerList").jqGrid("getCell",BatchId_bi, "Desc_vch") );  
													CreateBatchPickerDialog.remove();
												}
			 
		 },
		onSelectRow: function(BatchId_bi){				
				if(BatchId_bi && BatchId_bi!==LastSelBatchPickerContent){
					$('#BatchPickerList').jqGrid('restoreRow',LastSelBatchPickerContent);
					//$('#BatchPickerList').jqGrid('editRow',Created_dt,true, '', '', '', '', rx_AfterEditMCContent,'', '');
					LastSelBatchPickerContent=BatchId_bi;
				}
				else
				{
					LastSelBatchPickerContent = 0;					
				}
			},
		 loadComplete: function(data){ 		
		 		 
		 <!---
				$(".view_ReportTool").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_ReportTool").click( function() { ViewReportToolDialogMCContent($(this).attr('rel')); } );			
					$(".view_RowBatchPickerContent3").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".view_RowBatchPickerContent3").click( function() { ViewBatchDetailsDialogMCContent3($(this).attr('rel'), $(this).attr('rel2') ); } );
							
				$(".del_RowMCContent").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".del_RowMCContent").click( function() { 
				
												$.alerts.okButton = '&nbsp;Confirm Delete!&nbsp;';
												$.alerts.cancelButton = '&nbsp;No&nbsp;';		
												
												var CurrREL = $(this).attr('rel');
												
												jConfirm( "Are you sure you want to delete this Campaign?", "About to delete Campaign.", function(result) { 
													if(result)
													{	
														DelBatch(CurrREL);
													}
													return false;																	
												});
																 
											} );--->
			
				<!--- Add a filter row--->
				
				var BatchPickerListSearchFilterRow = ''+
				'<tr class="ui-widget-content jqgrow ui-row-ltr" role="row" id="BatchPickerListSearchFilterRow" aria-selected="true">'+
				'	<td aria-describedby="" style="" role="gridcell"></td>'+
				'	<td aria-describedby="BatchPickerList_Desc_vch" title=""  align="left" style="text-align:left;" role="gridcell">'+
				'Descritpion Filter<BR><input type="text" class="ui-corner-all" style="width:240px; display:inline; margin-bottom:3px;" value="' + data.NOTES_MASK + '" id="search_notes">'+
				'	</td>'+
				'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="" title="" style="" role="gridcell">&nbsp;</td>'+
				'	<td aria-describedby="" title="" style="display:none;" role="gridcell">&nbsp;</td>'+
				'</tr>'


				<!--- Append to end of table body - nice and tight to the form - be sure structure is correct--->
				$("#BatchPickerList tbody").append(BatchPickerListSearchFilterRow);
				
				<!--- Bind all search functionality to new row --->
				$("#BatchPickerList #search_notes").unbind();
				$("#BatchPickerList #search_notes").keydown(function(event) { doSearchMCContent(arguments[0]||event,'search_notes') }); 
				
				<!--- Reset the focus to an inline filter box--->
				if(typeof(lastObj_MCContent) != "undefined" && lastObj_MCContent != "")
				{					
					<!--- Stupid IE not rendering yet so call again in 10 ms--->
					setTimeout(function() 
					{ 
					
						 var LocalFocus = $("#BatchPickerList #" + lastObj_MCContent);
						 if (LocalFocus.setSelectionRange) 
						 {         
							 <!--- ... then use it        
							   (Doesn't work in IE)
							   Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh. 
							  ---> 
							   var len = $(LocalFocus).val().length * 2;         
							   LocalFocus.setSelectionRange(len, len);         
						 }        
						 else
  			          	 {
						     <!---... otherwise replace the contents with itself         
							   (Doesn't work in Google Chrome)    --->     
							  $(LocalFocus).focus(); 
							  var str = $(LocalFocus).val();
							  $(LocalFocus).val("");    
							  $(LocalFocus).val(str);        
					     }         
						 
						 <!--- Scroll to the bottom, in case we're in a tall textarea
						  (Necessary for Firefox and Google Chrome)--->
						 LocalFocus.scrollTop = 999999; 
						 
						
						lastObj_MCContent = undefined;
						$("#BatchPickerList #" + lastObj_MCContent).focus(); 
					}, 10);
				}
			
				
   			},	
		editurl: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
	
		<!--- //The JSON reader. This defines what the JSON data returned from the CFC should look like--->

		  jsonReader: {
			  root: "ROWS", //our data
			  page: "PAGE", //current page
			  total: "TOTAL", //total pages
			  records:"RECORDS", //total records
			  cell: "", //not used
			  id: "0" //will default second column as ID
			  }	
	});
	
	$('#pagerDiv').hide();

	$("#BatchPickerList_BatchId_bi").css('text-align', 'left');
	$("#BatchPickerList_Desc_vch").css('text-align', 'left');
	$("#BatchPickerList_Created_dt").css('text-align', 'left');
	$("#BatchPickerList_LASTUPDATED_DT").css('text-align', 'left');
	$("#BatchPickerList_Options").css('text-align', 'right');

	$("#refresh_List_BatchMCContent").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
	$("#BatchPickerList_toppager_left").html("<b>Campaigns</b><img id='refresh_List' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
	$("#refresh_List_BatchMCContent").click(function() { gridReloadBatchPickerContent(); return false; });
		
	$("#tab-panel-BatchPickerContent").toggleClass("ui-tabs-hide");	
				
			
	$('#MCCFilter_STOP_DT').datepicker({
		numberOfMonths: 2,
		showButtonPanel: true,
		dateFormat: 'yy-mm-dd'
	});
	
	$('#MCCFilter_START_DT').datepicker({
		numberOfMonths: 2,
		showButtonPanel: true,
		dateFormat: 'yy-mm-dd'
	});
		

	<!--- Kill the new dialog --->
	$("#tab-panel-BatchPickerContent #Cancel").click( function() 
	{						
		CreateBatchPickerDialog.remove(); 
		return false;
	}); 	
		

	$("#tab-panel-BatchPickerContent #Select").click( function() 
	{			
	
		LastSelBatchPickerContent = $("#BatchPickerList").jqGrid('getGridParam','selrow');
		
		if(LastSelBatchPickerContent == null)
		{
			jAlert("No Campaign has been selected yet. Click cancel if you wish to exit without selecting a new Campaign.\n" , "Notice!", function(result) { } );					
			return false;		
		}
	
		<!---console.log(LastSelBatchPickerContent);
		
		if(LastSelBatchPickerContent > 0 && LastSelBatchPickerContent != null)
			console.log(LastSelBatchPickerContent + "  second level" );
		
		return;--->
			
		if(LastSelBatchPickerContent > 0 && LastSelBatchPickerContent != null)	
			if(typeof(<cfoutput>"#inpCallBackUpdateBatchId#"</cfoutput>) != 'undefined') <cfoutput>#inpCallBackUpdateBatchId#</cfoutput>(LastSelBatchPickerContent, $("#BatchPickerList").jqGrid("getCell",LastSelBatchPickerContent, "Desc_vch"));  

		CreateBatchPickerDialog.remove(); 
		return false;
	}); 	
		
		
		
			
});


<!--- Updates data to scrubbed values on the server side - removes formatting--->
function rx_AfterEditMCContent(rowid, result) 
{
	<!--- inefficient and annoying to reload entire grid for just one edit--->
	<!---$("#BatchPickerList").trigger("reloadGrid");  --->
	
	<!--- jqGrid returns an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
	var d = eval('(' + result.responseText + ')');
	
	<!--- Get row 1 of results if exisits--->
	if (d.ROWCOUNT > 0) 
	{							
																			
		<!--- Check if variable is part of JSON result string --->								
		if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
		{							
			CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
			
			if(CurrRXResultCode > 0)
			{								
				<!--- Chance to apply custom formatiing rules here--->
				if(typeof(d.DATA.INPDESC[0]) != "undefined")								
				{					 	
						$("#BatchPickerList").jqGrid('setRowData',rowid,{Desc_vch:d.DATA.INPDESC[0]});
				}
				
			}
				
		}
		else
		{<!--- Invalid structure returned --->	
			
		}
	}
	else
	{<!--- No result returned --->
		
	}  
};




function doSearchMCContent(ev, sourceObj){
	
	if(!fAutoBatchPickerContent)
		return;
//	var elem = ev.target||ev.srcElement;
	if(timeoutHndBatchPickerContent)
		clearTimeout(timeoutHndBatchPickerContent)
	timeoutHndBatchPickerContent = setTimeout(gridReloadBatchPickerContent,500)
	
	lastObj_MCContent = sourceObj;
}

function gridReloadBatchPickerContent(){
			
		var ParamStr = '';
		var CurrSORD =  $("#BatchPickerList").jqGrid('getGridParam','sord');
		var CurrSIDX =  $("#BatchPickerList").jqGrid('getGridParam','sidx');
		var CurrPage = $('#BatchPickerList').getGridParam('page'); 
		var notes_mask = $("#BatchPickerList #search_notes").val();
		
		if(notes_mask) ParamStr += '&notes_mask=' + notes_mask;
				
		if(CurrSORD != '' && CurrSORD != null) ParamStr += '&sord=' + CurrSORD;
			
		if(CurrSIDX != '' && CurrSIDX != null) ParamStr += '&sidx=' + CurrSIDX;	
		
	$("#BatchPickerList").jqGrid('setGridParam',{url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchesMCContent&returnformat=json&queryformat=column&inpSocialmediaFlag=0&_cf_nodebug=true&_cf_nocache=true" + ParamStr,page:CurrPage}).trigger("reloadGrid");

}

function enableAutosubmitMCContent(state){
	fAutoBatchPickerContent = state;
	gridReloadBatchPickerContent();
	$("#submitButton").attr("disabled",state);
}





</script>
     
	    
<cfoutput>
         
    <!--- Outer div to prevent FOUC ups - (Flash of Unstyled Content)--->     
    <div id="tab-panel-BatchPickerContent" class="ui-tabs-hide" style="position:relative; overflow: hidden;">   
      
        <div style="table-layout:fixed; min-height:475px; height:475px;">
                
            <div id="pagerDiv"></div>
                           
            <div style="text-align:left;">
            <table id="BatchPickerList"></table>
            </div>
        
        </div>    
   
   	<BR>
    <BR>
    
    <div class="RXForm">
    
	    <button id="Cancel" TYPE="button" class="ui-corner-all">Cancel</button>
        <button id="Select" TYPE="button" class="ui-corner-all">Select</button>
    </div>
        
	</div>    
    
	
</cfoutput>    
     
