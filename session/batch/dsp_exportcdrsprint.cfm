<cfparam name="sidx" default="ContactString_vch">
<cfparam name="sord" default="ASC">
<cfparam name="INPBATCHID" default="0">

<!--- Validate Session User---> 
<cfif Session.USERID GT 0>
     
    <!--- Cleanup SQL injection --->
    
    <!--- Verify all numbers are actual numbers --->                    
    <cfset sidx = REPLACE(sidx, "'", "", "ALL") />
    <cfset sord = REPLACE(sord, "'", "", "ALL") />
                        
    <!--- Get data --->
    <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
    	SELECT         
          rxcd1.ContactString_vch AS MainContactString_vch,
          CASE rxcd1.CallResult_int
            WHEN 3 THEN 'LIVE'
            WHEN 5 THEN 'LIVE'
            WHEN 4 THEN 'MACHINE'
            WHEN 7 THEN 'BUSY'
            WHEN 10 THEN 'NO ANSWER'
            WHEN 48 THEN 'NO ANSWER'
            ELSE 'OTHER'		
           END AS CallResult,
          rxcd1.RXCDLStartTime_dt AS MainRXCDLStartTime_dt
         FROM
           simplexresults.contactresults AS rxcd1 
         WHERE
           rxcd1.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
           AND rxcd1.RXCallDetailId_int = 
           (
            SELECT 
              MAX(rxcd2.RXCallDetailId_int) 
            FROM 
              simplexresults.contactresults AS rxcd2 
            WHERE 
              rxcd2.ContactString_vch = rxcd1.ContactString_vch
              AND rxcd2.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
            
           )        
    </cfquery>
        
    <body>    
        

     <div id="ExportPreviewContainer" style="width:550px; max-height:600px; overflow-y: scroll; overflow-x: hidden;">
        
            <table border="0" width="550px">
                <tr>
                   <th width="250px" align="left">Contact String</th>
                   <th width="75px" align="left">Result</th>
                   <th width="225px" align="left">Date (Pacific Time zone)</th> 
                </tr>
                
                <cfset CurrRowNumber = 0>
                <cfoutput query="GetNumbers">
                
                <cfset CurrRowNumber = CurrRowNumber + 1>
                
                <cfset DisplayOutPutNumberString = #GetNumbers.MainContactString_vch#>
                
                  <!--- Leave e-mails alone--->
                            
                <!--- North american number formating--->
                <cfif ISNUMERIC(DisplayOutPutNumberString) AND LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                    <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                </cfif>
                            
                <cfif (CurrRowNumber MOD 2) GT 0>
                    <tr bgcolor="##00CC99">                     
                <cfelse>
                     <tr>                
                </cfif>         
               
                <td style="padding: 1px 1px 1px 5px;">#DisplayOutPutNumberString#</td>
                <td style="padding: 1px 1px 1px 5px;">#GetNumbers.CallResult#</td> 
                <td style="padding: 1px 1px 1px 5px;">#GetNumbers.MainRXCDLStartTime_dt#</td> 
                </tr>
                
                </cfoutput>
            
            </table>

			
    
        </div>
        
	</body>
    
    

</cfif>


<script>
CreateBatchDetailsExportCDRsPrintDialog.print();
</script>