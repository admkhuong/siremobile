<cfparam name="sidx" default="ContactString_vch">
<cfparam name="sord" default="ASC">
<cfparam name="INPBATCHID" default="0">

<!--- Validate Session User---> 
<cfif Session.USERID GT 0>
     
    <!--- Cleanup SQL injection --->
    
    <!--- Verify all numbers are actual numbers --->                    
    <cfset sidx = REPLACE(sidx, "'", "", "ALL") />
    <cfset sord = REPLACE(sord, "'", "", "ALL") />
                        
    <!--- Get data --->
    <cfquery name="GetNumbers" datasource="#Session.DBSourceEBM#">
    	SELECT         
          rxcd1.ContactString_vch AS MainContactString_vch,
          CASE rxcd1.CallResult_int
            WHEN 3 THEN 'LIVE'
            WHEN 5 THEN 'LIVE'
            WHEN 4 THEN 'MACHINE'
            WHEN 7 THEN 'BUSY'
            WHEN 10 THEN 'NO ANSWER'
            WHEN 48 THEN 'NO ANSWER'
            ELSE 'OTHER'		
           END AS CallResult,
          rxcd1.RXCDLStartTime_dt AS MainRXCDLStartTime_dt
         FROM
           simplexresults.contactresults AS rxcd1 
         WHERE
           rxcd1.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
           AND rxcd1.RXCallDetailId_int = 
           (
            SELECT 
              MAX(rxcd2.RXCallDetailId_int) 
            FROM 
              simplexresults.contactresults AS rxcd2 
            WHERE 
              rxcd2.ContactString_vch = rxcd1.ContactString_vch
              AND rxcd2.BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
            
           )        
    </cfquery>
        
    <body>    
        <cfheader name="Content-Disposition" 
        value="attachment;filename=CDRsExport_#INPBATCHID#.doc">
        <cfcontent TYPE="application/msword">       
        
        	<table border="1">
            	<tr>
                	<td>Phone Number</td>
                    <td>Call Result</td>
                    <td>Date (Pacific Time zone)</td> 
                </tr>
				<cfoutput query="GetNumbers">
                
                <cfset DisplayOutPutNumberString = #GetNumbers.MainContactString_vch#>
                
                <!--- North american number formating--->
                <cfif LEN(DisplayOutPutNumberString) GT 9 AND LEFT(DisplayOutPutNumberString,1) NEQ "0" AND LEFT(DisplayOutPutNumberString,1) NEQ "1">            
                    <cfset DisplayOutPutNumberString = "(" & LEFT(DisplayOutPutNumberString,3) & ") " & MID(DisplayOutPutNumberString,4,3) & "-" & RIGHT(DisplayOutPutNumberString, LEN(DisplayOutPutNumberString)-6)>            
                </cfif>
                <tr>
                	<td>#DisplayOutPutNumberString#</td>
                    <td>#GetNumbers.CallResult#</td>
                    <td>#GetNumbers.MainRXCDLStartTime_dt#</td>
                </tr>
                </cfoutput>
            </table>
    
        </cfcontent>
	</body>
    
    

</cfif>

<!---
<script>
CreatePrintListDialog.print();
</script>
--->