<cfparam name="data" default="" />
<cfparam name="fanpageId" default="" />
<cfset facebook = deserializeJSON(data)>
<cfset type = facebook.TYPE>
<cfsaveContent variable="htmlContent">
	
		<div id="privewFacebook">
			<div id="preview">
				<div id="previewHeader">
					<div id="previewImage"></div>
					<div id="previewFanpageInfo">
						<div id="name"></div>
					</div>
			</div>
			<div id="previewContent">
			</div>
		<cfif type NEQ 'message'>
			<div id="previewLink">
				<div id="linkImage"></div>
				<div id="linkContent">
					<div id="linkName"></div>
					<div id="linkCaption">
					</div>
					<div id="linkDescription"></div>
				</div>	
			</div>
		</cfif>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
		var message = data.MESSAGE;
		var name = data.NAME;
		var url = data.URL;
		var picture = data.PICTURE;
		var caption = data.CAPTION;
			
   		// bind data
   		$('#privewFacebook #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#privewFacebook #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#privewFacebook #previewContent').html(message);
   		$('#privewFacebook #linkImage').append('<img src="'+ picture +'" width="80" />');
   		$('#privewFacebook #linkCaption').html(getDomainFromUrl(url));
   		$('#privewFacebook #linkName').html('<a href="'+ url +'" >' + name +'</a>');
   		$('#privewFacebook #linkDescription').html(caption);
   		
		function getDomainFromUrl(url){
			var urlParts = url.replace('http://','').replace('https://','').split(/[/?#]/);
			var domain = urlParts[0];
			return domain;
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">

</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>