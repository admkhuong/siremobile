<cfsaveContent variable="htmlContent">
     <table id="PostingFacebookList"></table>
     <div id="postedFacebookPagerDiv"></div>
</cfsaveContent>

<cfsaveContent variable="cssContent">
</cfsaveContent>

<cfsaveContent variable="jsContent">
	
	<script type="text/javascript">
		$(function() {
			$("#PostingFacebookList").jqGrid({        
				url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetFacebookPostingData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				datatype: "json",
				height: "auto",
				colNames:['Batch','Fan page', 'Type', 'Date time','Status', 'Error Message', 'Options'],
				colModel:[			
					{
						name:'BatchId_bi',
						index:'BatchId_bi', 
						width:70, 
						editable:false, 
						resizable:false, 
						align:'left'
					},
					{
						name:'FanPageId',
						index:'FanPageId', 
						width:150, 
						editable:false, 
						resizable:false,
						search: false,
						sortable:false
					},
					{
						name:'TypePublish_vch',
						index:'TypePublish_vch', 
						width:100, 
						editable:false, 
						resizable:false,
						stype: "select",
						searchoptions: {
							value: ":All;message:message only;messageLink:message with link;link:link only"
						}
					},
					{
						name:'Posted_dt',
						index:'Posted_dt', 
						width:150, 
						editable:false, 
						resizable:false,
						searchoptions:{
							<!--- sopt: [
								"eq", "ne", "gt", "lt"
							], --->
							dataInit:function(el){
	                        	$(el).datepicker({dateFormat:'yy-mm-dd',onSelect: function(dateText, inst){ 
	                        		if (this.id.substr(0, 3) === "gs_") {
				                        setTimeout(function () {
				                            $("#PostingFacebookList")[0].triggerToolbar();
				                        }, 100);
	                    			} 
	                        	}
	                        })
	                        }
						}
					},
					{
						name:'Status_publish',
						index:'Status_publish', 
						width:40, 
						editable:false, 
						sortable:false, 
						resizable:false,
						search: false
					},
					{
						name:'Error_msg',
						index:'Error_msg', 
						width:200, 
						editable:false, 
						sortable:false, 
						resizable:false,
						search: false
					},
					{
						name:'Options',
						index:'Options', 
						width:40, 
						editable:false, 
						sortable:false, 
						resizable:false,
						search: false
					}
				],
				rowNum:20,
			   	rowList:[10,20,30,40],
				mtype: "POST",
				pager: $('#postedFacebookPagerDiv'),
				toppager: true,
		    	emptyrecords: "No Current post Found.",
		    	pgbuttons: true,
				altRows: true,
				altclass: "ui-priority-altRow",
				pgtext: false,
				pginput:true,
				sortname: 'Posted_dt',
				viewrecords: true,
				sortorder: "DESC",
				caption: "",
				multiselect: false,
			 	loadComplete: function(data){ 		 
			 		$("#PostingFacebookList").jqGrid ('setLabel', 'BatchId_bi', '', {'text-align':'left'});
			 		$("#PostingFacebookList").jqGrid ('setLabel', 'TypePublish_vch', '', {'text-align':'left'});
	   			},	
			    jsonReader: {
				    root: "ROWS", //our data
				    page: "PAGE", //current page
				    total: "TOTAL", //total pages
				    records:"RECORDS", //total records
				    cell: "", //not used
				    id: "1" //will default second column as ID
			   }	
			});
			
			$("#PostingFacebookList_toppager_left").html("<b>Facebook</b><img id='refresh_postringFacebook' class='ListIconLinks Refresh_16x16' src='../../public/images/dock/blank.gif' width='16' height='16' style='display:inline; margin:0 5px 0 5px;'>");
			$("#PostingFacebookList").jqGrid('filterToolbar', {
				searchOnEnter: false
			});
			
			$('#refresh_postringFacebook').click(function(){
				$("#PostingFacebookList").trigger("reloadGrid"); 
			});
		});
		function previewFacebookPost(obj){
			var type = $(obj).attr('inpType');
			var message = $(obj).attr('inpMessage');
			var caption = $(obj).attr('inpCaption');
			var url = $(obj).attr('inpUrl');
			var picture = $(obj).attr('inpPicture');
			var name = $(obj).attr('inpName');
			
			var inpFanpageId = $(obj).attr('inpFanpageId');
			var inpFacebook = {
				TYPE    : type,  
				MESSAGE : message,
				URL     : url,
				PICTURE : picture,
				NAME    : name,
				CAPTION : caption
			};
			
			var $previewFacebookDialog = $('<div id="PreviewFacebookDialog"></div>');
			$previewFacebookDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_PreviewFacebook', 
				{
					data: JSON.stringify(inpFacebook),
	   			 	fanpageId : inpFanpageId
				})
				.dialog({
					modal : true,
					title: 'Preview past post to fan page',
					width: 550,
					minHeight: 200,
					height: 'auto',
					position: 'top',
					close:function(){
						$(this).remove(); 
					},
					dialogClass: 'noTitleStuff',  
					buttons:[
						{
							text: 'Exit',
							click: function(){
								$(this).remove();
							}
						}
					]
				});	
			
		}
	</script>
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>