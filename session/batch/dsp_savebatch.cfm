<cfparam name="INPBATCHID" default="0">
<script TYPE="text/javascript">
	function SaveBatch(CallBackFunc)
	{					
		$("#loadingDlgAddBatchMCContent").show();		
				
	
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchDesc&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { 
			id : <cfoutput>#INPBATCHID#</cfoutput>,
			Desc_vch : $("#AddNewBatchMCContent #inpBatchDesc").val()},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
									if(typeof(CallBackFunc) != "undefined")	
										CallBackFunc();
							
									<!--- Load editor now --->
									CreateSaveBatchDetailsDialogMCContent.remove(); 
									return false;
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Campaign has NOT been saved.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
							}
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->						
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
										
					$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
								
			} 		
			
		});
	
		return false;

	}
	
	$(function()
	{	
	
		$("#AddNewBatchMCContent #AddNewBatchButton").click( function() { SaveBatch(gridReloadBatchesMCContent); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#AddNewBatchMCContent #Cancel").click( function() 
			{
					$("#loadingDlgAddBatchMCContent").hide();	
					CreateSaveBatchDetailsDialogMCContent.remove(); 
					return false;
		  }); 	
		$("#AddNewBatchMCContent #Remove").click( function() 
			{
					$("#loadingDlgAddBatchMCContent").hide();	
					removeBatchDetails(<cfoutput>#INPBATCHID#</cfoutput>);	
					return false;
		  }); 
		  
		  $("#loadingDlgAddBatchMCContent").hide();	
	} );
		
	function handleKeyPress(e){
		var key=e.keyCode || e.which;
		if (key==13){
			SaveBatch(gridReloadBatchesMCContent);
	  		return false; 
		}
	}
</script>


<style>

#AddNewBatchMCContent
{
	margin:0 0;
	width:450px;
	padding:0px;
	border: none;
	min-height: 330px;
	height: 330px;
	font-size:12px;
}


#AddNewBatchMCContent #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#AddNewBatchMCContent #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#AddNewBatchMCContent h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}




</style> 

<cfoutput>
        
<div id='AddNewBatchMCContent' class="RXForm">

	<div id="LeftMenu">

		<div width="100%" align="center" style="margin-bottom:5px;"><h1>Add a New Campaign</h1></div>

		<div width="100%" align="center" style="margin-bottom:20px;"><img class = "batch-file-icon_web" src="../../public/images/dock/blank.gif" /></div>

		<div style="margin: 10px 5px 10px 20px;">
            <ul>
         		<li>Give your <i>Campaign</i> a title you can use to uniquely distiguish it.</li>
                <li>Press "Add" button when finished.</li>
                <li>Press "Cancel" button to exit without creating a new one.</li>               
            </ul>   
            
            <BR />
            <BR />
            <i>DEFINITION:</i> A Campaign is a logical content container for managing single or multi-channel communications.
                   
        </div>
        
	</div>
    
    <div id="RightStage">
                       
        <form id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">
        
         <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
        
                <label>Campaign Title
                <span class="small">Used as a helpful reminder of what this campaign is all about.</span>
                </label>
                <input type="text" name="inpBatchDesc" id="inpBatchDesc" size="265" class="ui-corner-all" onkeypress="return handleKeyPress(event);" value="Campaign <<cfoutput>#INPBATCHID#</cfoutput>>"/> 
                <BR>
                <button id="AddNewBatchButton" type="button" class="ui-corner-all">Save</button>
				<button id="Remove" type="button" class="ui-corner-all">Don't Save</button>
                <button id="Cancel" type="button" class="ui-corner-all">Cancel</button>
                        
                <div id="loadingDlgAddBatchMCContent" style="display:inline;">
                    <img class="loadingDlgDeleteGroupImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
        </form>
    
    </div>

</div>

</cfoutput>
