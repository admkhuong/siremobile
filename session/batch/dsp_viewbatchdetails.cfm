<cfparam name="INPBATCHID" default="0">
<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->  


<cfquery name="GetScheduleOptions" datasource="#Session.DBSourceEBM#">
    SELECT
        ENABLED_TI,
        STARTHOUR_TI,
        ENDHOUR_TI, 
        SUNDAY_TI,
        MONDAY_TI,
        TUESDAY_TI,
        WEDNESDAY_TI,
        THURSDAY_TI,
        FRIDAY_TI,
        SATURDAY_TI,
        LOOPLIMIT_INT,
        STOP_DT,
        START_DT,
        STARTMINUTE_TI,
        ENDMINUTE_TI,
        BLACKOUTSTARTHOUR_TI,
        BLACKOUTENDHOUR_TI,
        LASTUPDATED_DT           
    FROM 
    	simpleobjects.scheduleoptions 
    WHERE 
        BatchId_bi = #INPBATCHID#   
</cfquery>


<cfset SUNDAY_TI = "#GetScheduleOptions.SUNDAY_TI#">
<cfset MONDAY_TI="#GetScheduleOptions.MONDAY_TI#">
<cfset TUESDAY_TI="#GetScheduleOptions.TUESDAY_TI#">
<cfset WEDNESDAY_TI="#GetScheduleOptions.WEDNESDAY_TI#">
<cfset THURSDAY_TI="#GetScheduleOptions.THURSDAY_TI#">
<cfset FRIDAY_TI="#GetScheduleOptions.FRIDAY_TI#">
<cfset SATURDAY_TI="#GetScheduleOptions.SATURDAY_TI#">
<cfset LOOPLIMIT_INT="#GetScheduleOptions.LOOPLIMIT_INT#">
<cfset START_DT="#GetScheduleOptions.START_DT#">
<cfset STOP_DT="#GetScheduleOptions.STOP_DT#">
<cfset STARTHOUR_TI="#GetScheduleOptions.STARTHOUR_TI#">
<cfset ENDHOUR_TI="#GetScheduleOptions.ENDHOUR_TI#">
<cfset STARTMINUTE_TI="#GetScheduleOptions.STARTMINUTE_TI#">
<cfset ENDMINUTE_TI="#GetScheduleOptions.ENDMINUTE_TI#">
<cfset BLACKOUTSTARTHOUR_TI="#GetScheduleOptions.BLACKOUTSTARTHOUR_TI#">
<cfset BLACKOUTENDHOUR_TI="#GetScheduleOptions.BLACKOUTENDHOUR_TI#">
<cfset ENABLED_TI="#GetScheduleOptions.ENABLED_TI#"> 

<cfquery name="GetBatchDetails" datasource="#Session.DBSourceEBM#">
    SELECT
        BatchId_bi,
        DESC_VCH,
        Created_dt,
        rxdsLibrary_int,
        rxdsElement_int,
        rxdsScript_int        
    FROM
        simpleobjects.batch
    WHERE        
        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#Session.USERID#">
    AND
        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#">      
</cfquery>     

     			            	
<cfset DisplayOutputStatus = "">
<cfset DisplayOptions = ""> 

<cfset CountTotal = 0>
<cfset CountQueued = 0>
<cfset CountComplete = 0>

<!--- Get Total Count --->
<cfquery name="GetTotal" datasource="#Session.DBSourceEBM#">
	SELECT
		COUNT(*) AS TOTALCOUNT
	FROM
		simplequeue.contactqueue
	WHERE        
		BatchId_bi = #GetBatchDetails.BatchId_bi#
</cfquery>

<cfset CountTotal = GetTotal.TOTALCOUNT>

<!--- Get Total Queued Count --->
<cfquery name="GetTotalQueued" datasource="#Session.DBSourceEBM#">
	SELECT
		COUNT(*) AS TotalQueuedCount
	FROM
		simplequeue.contactqueue
	WHERE        
		BatchId_bi = #GetBatchDetails.BatchId_bi#
	AND 
		DTSStatusType_ti = 1                	    
</cfquery>

<cfset CountQueued = GetTotalQueued.TotalQueuedCount>


<!--- Get Total Queued Count Sent --->
<cfquery name="GetTotalQueuedSent" datasource="#Session.DBSourceEBM#">
	SELECT
		COUNT(*) AS TotalQueuedCountSent
	FROM
		simplequeue.contactqueue
	WHERE        
		BatchId_bi = #GetBatchDetails.BatchId_bi#
	AND 
		DTSStatusType_ti = 2                	    
</cfquery>

<cfset CountQueuedSent = GetTotalQueuedSent.TotalQueuedCountSent>


<!--- Get Total Count --->
<cfquery name="GetTotalComplete" datasource="#Session.DBSourceEBM#">
	SELECT
		COUNT(*) AS TotalCompleteCount
	FROM
		simplexresults.contactresults
	WHERE        
		BatchId_bi = #GetBatchDetails.BatchId_bi#
</cfquery>

<cfset CountComplete = GetTotalComplete.TotalCompleteCount>


	   
<cfif CountComplete GTE CountTotal AND CountQueued EQ 0>

	<cfif CountComplete EQ 0 AND CountTotal EQ 0 AND CountQueued EQ 0>
	<!--- Canceled--->
		 <cfset DisplayOutputStatus = DisplayOutputStatus & "<span id='s_Canceled'>Cancelled</font>">
	<cfelse>
	<!--- Complete--->    
		<cfset DisplayOutputStatus = DisplayOutputStatus & "<span id='s_Finished'>Finished</font>">
	</cfif>

<cfelseif CountTotal GT CountComplete AND ( CountQueued GT 0 OR CountQueuedSent GT 0)>
<!--- Running --->
	<cfset DisplayOutputStatus = DisplayOutputStatus & "<span id='s_Running'>Running</font>">	
<cfelse>
<!--- Clean up errors --->
	<cfset DisplayOutputStatus = DisplayOutputStatus & "<span id='s_Error'>Error</font>"> 
</cfif>
                

<script TYPE="text/javascript">
	
	$(function()
	{		
		$('#STOP_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$('#START_DT').datepicker({
			numberOfMonths: 2,
			showButtonPanel: true,
			dateFormat: 'yy-mm-dd'
		});
		
		$("#BatchDetailsDiv #Cancel").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
					
		$("#AdvancedOptionsToggleBD").click( function() 
	
			{
				if($("#AdvancedOptionsToggleBD").data("CurrState") )
				{			
					$("#AdvancedOptionsBD").hide();
					$("#AdvancedOptionsToggleBD").html("Show Advanced Schedule Options");
					$("#AdvancedOptionsToggleBD").data("CurrState", false);
				}
				else
				{
					$("#AdvancedOptionsBD").show();	
					$("#AdvancedOptionsToggleBD").html("Hide Advanced Schedule Options");
					$("#AdvancedOptionsToggleBD").data("CurrState", true);
				}			
			}
		); 
		
		<cfif Session.AdvancedScheduleOptions EQ 0>
			$("#AdvancedOptionsBD").hide();
			$("#AdvancedOptionsToggleBD").html("Show Advanced Schedule Options");
			$("#AdvancedOptionsToggleBD").data("CurrState", false);	
		<cfelse>
			$("#AdvancedOptionsBD").show();	
			$("#AdvancedOptionsToggleBD").html("Hide Advanced Schedule Options");
			$("#AdvancedOptionsToggleBD").data("CurrState", true);
		</cfif>



		$("#ScheduleOptionsToggleBD").click( function() 
		
			{
				if($("#ScheduleOptionsToggleBD").data("CurrState") )
				{			
					$("#SetScheduleBD").hide();
					$("#UpdateScheduleButtonBD").hide();					
					$("#ScheduleOptionsToggleBD").html("Change Schedule");
					$("#ScheduleOptionsToggleBD").data("CurrState", false);
				}
				else
				{
					$("#SetScheduleBD").show();	
					$("#UpdateScheduleButtonBD").show();	
					$("#ScheduleOptionsToggleBD").html("Hide Schedule");
					$("#ScheduleOptionsToggleBD").data("CurrState", true);
				}			
			}
		
		); 
		
		$("#SetScheduleBD").hide();
		$("#UpdateScheduleButtonBD").hide();
		$("#ScheduleOptionsToggleBD").html("Change Schedule");
		$("#ScheduleOptionsToggleBD").data("CurrState", false);
		
		
		
		
			<!--- Kill the new dialog --->
			$("#BatchDetailsDiv #Cancel").click( function() 
				{
						$("#loadingDlgBatchDetails").hide();					
						CreateViewBatchDetailsDialog.remove(); 
						return false;
				}); 		
			  
		  
		$("#UpdateScheduleButtonBD").click( function() { UpdateScheduleBD(); return false;  }); 
		
		$("#CallDetailReportBD").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#CallDetailReportBD").click( function() { BatchDetailsExportCDRsDialog(<cfoutput>#GetBatchDetails.BatchId_bi#</cfoutput>); return false;  }); 
				
		$("#exp_excelBD").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#exp_excelBD").click(function(e) { 
			<!--- stop the browser from following  --->
			e.preventDefault(); 
			
			<cfoutput>
				var params = {
					'INPBATCHID':encodeURIComponent('#GetBatchDetails.BatchId_bi#')
				};
				post_to_url('#rootUrl#/#SessionPath#/batch/dsp_ExportCDRsExcel', params, 'POST');
			</cfoutput>
		}); 
		
		$("#exp_wordBD").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#exp_wordBD").click(function(e) { 
			<!--- stop the browser from following  --->
			e.preventDefault(); 
			
			<cfoutput>
				var params = {
					'INPBATCHID':encodeURIComponent('#GetBatchDetails.BatchId_bi#')
				};
				post_to_url('#rootUrl#/#SessionPath#/batch/dsp_ExportCDRsWord', params, 'POST');
			</cfoutput>	
		}); 
		
		
		$("#exp_pdfBD").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#exp_pdfBD").click(function(e) { 
			<!--- stop the browser from following  --->
			e.preventDefault(); 
			
			<cfoutput>
				var params = {
					'INPBATCHID':encodeURIComponent('#GetBatchDetails.BatchId_bi#')
				};
				post_to_url('#rootUrl#/#SessionPath#/batch/dsp_ExportCDRsPDF', params, 'POST');
			</cfoutput>	
				
		}); 
		
		$("#exp_printBD").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		$("#exp_printBD").click(function(e) { BatchDetailsExportCDRsPrintDialog(); return false;  }); 
		]/l
		
		<!--- Kill the new dialog --->
		$("#BatchDetailsDiv #Cancel").click( function() 
			{
					$("#loadingDlgBatchDetails").hide();					
					CreateViewBatchDetailsDialog.remove(); 
					return false;
			}); 	
				
		
		
		$("#loadingDlgBatchDetails").hide();	
		  
	} );
		
		
	function UpdateScheduleBD()
	{	
		
		$("#loadingDlgBatchDetails").show();		
	
		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=UpdateSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#GetBatchDetails.BatchId_bi#</cfoutput>, SUNDAY_TI : $("#BatchDetailsDiv #SUNDAY_TI").val(), MONDAY_TI : $("#BatchDetailsDiv #MONDAY_TI").val(), TUESDAY_TI : $("#BatchDetailsDiv #TUESDAY_TI").val(), WEDNESDAY_TI : $("#BatchDetailsDiv #WEDNESDAY_TI").val(), THURSDAY_TI : $("#BatchDetailsDiv #THURSDAY_TI").val(), FRIDAY_TI : $("#BatchDetailsDiv #FRIDAY_TI").val(), SATURDAY_TI : $("#BatchDetailsDiv #SATURDAY_TI").val(), LOOPLIMIT_INT : $("#BatchDetailsDiv #LOOPLIMIT_INT").val(), START_DT : $("#BatchDetailsDiv #START_DT").val(), STOP_DT : $("#BatchDetailsDiv #STOP_DT").val(), STARTHOUR_TI : $("#BatchDetailsDiv #STARTHOUR_TI").val(), ENDHOUR_TI : $("#BatchDetailsDiv #ENDHOUR_TI").val(), STARTMINUTE_TI : $("#BatchDetailsDiv #STARTMINUTE_TI").val(), ENDMINUTE_TI : $("#BatchDetailsDiv #ENDMINUTE_TI").val(), BLACKOUTSTARTHOUR_TI : $("#BatchDetailsDiv #BLACKOUTSTARTHOUR_TI").val(), BLACKOUTENDHOUR_TI : $("#BatchDetailsDiv #BLACKOUTENDHOUR_TI").val(), ENABLED_TI : $("#BatchDetailsDiv #ENABLED_TI").val()}, 
		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																						
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								<!--- Optionally close edit window when complete --->
								<!--- alert('NewQID = ' + d.DATA.NextQID[0]); --->	
							
								if(typeof(d.DATA.MESSAGE[0]) != "undefined")
									$("#BatchDetailsDiv #AJAXInfoOut").html(d.DATA.MESSAGE[0]);
							}
							
							$("#loadingDlgBatchDetails").hide();
							
						}
						else
						{<!--- Invalid structure returned --->	
							
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "Invalid Response from the remote server.");
					}
					
					$("#loadingDlgBatchDetails").hide();
			} );		
	
		return false;
	}
	
	
	

<!--- Global so popup can refernece it to close it--->
var CreateBatchDetailsExportCDRsDialog = 0;

function BatchDetailsExportCDRsDialog(INPBATCHID)
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
						
	var ParamStr = '';
	
	if(typeof(INPBATCHID) != "undefined" && INPBATCHID != "")					
		ParamStr = '?inpbatchid=' + encodeURIComponent(INPBATCHID);
						
	<!--- Erase any existing dialog data --->
	if(CreateBatchDetailsExportCDRsDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateBatchDetailsExportCDRsDialog.remove();
		CreateBatchDetailsExportCDRsDialog = 0;
		
	}
					
	CreateBatchDetailsExportCDRsDialog = $('<div></div>').append($loading.clone());
	
	CreateBatchDetailsExportCDRsDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_ExportCDRs' + ParamStr)
		.dialog({
			modal : true,
			title: 'Campaign Result Details - Message Detail Records',
			width: 'auto',
			height: 'auto',
			position: 'top'
		});

	CreateBatchDetailsExportCDRsDialog.dialog('open');

	return false;		
}




<!--- Global so popup can refernece it to close it--->
var CreateBatchDetailsExportCDRsPrintDialog = 0;

function BatchDetailsExportCDRsPrintDialog()
{				
	var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');

	var ParamStr = '';
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#GetBatchDetails.BatchId_bi#</cfoutput>);
						
	<!--- Erase any existing dialog data --->
	if(CreateBatchDetailsExportCDRsPrintDialog != 0)
	{
		<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
		<!--- console.log($SelectScriptDialog); --->
		CreateBatchDetailsExportCDRsPrintDialog.remove();
		CreateBatchDetailsExportCDRsPrintDialog = 0;
		
	}
					
	CreateBatchDetailsExportCDRsPrintDialog = $('<div></div>').append($loading.clone());
	
	CreateBatchDetailsExportCDRsPrintDialog
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_ExportCDRsPrint' + ParamStr)
		.dialog({
			modal : true,
			title: 'Campaign Result Details - Message Detail Records',
			width: 'auto',
			height: 'auto',
			position: 'top'
		});

	CreateBatchDetailsExportCDRsPrintDialog.dialog('open');

	return false;		
}


</script>


<style>

#BatchDetailsDiv
{
	margin:0 0;
	width: 650px;
	padding:0px;
	border: none;
	min-height: 530px;
	height: 530px;
	font-size:12px;
}


#BatchDetailsDiv #LeftMenu
{
	width:270px;
	min-width:270px;
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#BatchDetailsDiv #RightStage
{
	position:absolute;
	top:0;
	left:287px;
	padding:15px;
	margin:0px;	
	border: 0;
}


#BatchDetailsDiv h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}


</style> 

<cfoutput>
        
<div id='BatchDetailsDiv' class="RXForm">

<form id="BatchDetailsForm" name="BatchDetailsForm" action="" method="POST">

        <div id="LeftMenu">     
                       
            <div width="100%" align="center" style="margin-bottom:5px;"><h1>Campaign Activity</h1></div>

            <div width="100%" align="center" style="margin-bottom:20px;"><img src="../../public/images/Activity-Monitor-icon_web.png" /></div>
    
            <div style="margin: 10px 5px 10px 20px; line-height: 20px;">
                <ul>
                    <li>View what is running.</li>
                    <li>View what is running.</li>
                    <li>Press "exit" button when finished.</li>
                </ul>
                
                <BR />
                <BR />
                <i>DEFINITION:</i> Campaign Activity gives you a quick overview of what campaigns are running.
                <BR />
                <BR />
	            <i>DEFINITION:</i> A Campaign is a logical content container for managing single or multi-channel communications.
                
            </div>
                                            
        </div>
        
		
		<div id="RightStage">
               
            <div style="width 310px; min-width: 310px; margin:0px 0 0px 0;">
            	
                 <table class="smallReg" border="0"  style="table-layout:fixed; width:280px;">
                    <tr>
                        <th align="left" width="100px">Campaign Id:</td>
                        <td align="right" width="180px">#GetBatchDetails.BatchId_bi#</td>
                    </tr>
                    
                    <tr>
                        <th align="left" width="100px">Load Date (PTZ):</td>
                        <td align="right" width="180px">#GetBatchDetails.Created_dt#</td>
                    </tr>       
                
                    <tr>
                        <th align="left" width="100px">Total:</td>
                        <td align="right" width="180px">#CountTotal#</td>
                    </tr>   
                    
                    <tr>
                        <th align="left" width="100px">Queued:</td>
                        <td align="right" width="180px">#CountQueued#</td>
                    </tr>   
                    
                    <tr>
                        <th align="left" width="100px">In Progress:</td>
                        <td align="right" width="180px">#CountQueuedSent#</td>
                    </tr>  
                    
                    <tr>
                        <th align="left" width="100px">Complete:</td>
                        <td align="right" width="180px">#CountComplete#</td>
                    </tr>   
                    
                    <tr>
                        <th align="left" width="100px">Status</td>
                        <td align="right" width="180px">#DisplayOutputStatus#</td>
                    </tr>   
                                        
                </table>
                                
                <div>
                	<table cellspacing="0" cellpadding="0" border="0" class="" style="">
                        <tr>
                        	<td><span class="small300" style="padding-right:5px;">Call Detail Records</span></td>
                            <td style="margin: 4px;" title="View Call Detail Records" id="CallDetailReportBD" nowrap>
                                <div class="ui-output-detail"></div>
                            </td>     
                            <td style="width: 4px;" class="ui-pg-button ui-state-disabled"><span class="ui-separator"></span></td>
                            <td class="ui-pg-button ui-corner-all" title="Export Call Detial Records to Excel" id="exp_excelBD" nowrap>
                                <div class="ui-output-excel"></div>
                            </td>
                            <td style="width: 4px;" class="ui-pg-button ui-state-disabled"><span class="ui-separator"></span></td>
                            <td class="ui-pg-button ui-corner-all" title="Export Call Detial Records to Word" id="exp_wordBD" nowrap>
                                <div class="ui-output-word"></div>
                            </td>
                            <td style="width: 4px;" class="ui-pg-button ui-state-disabled"><span class="ui-separator"></span></td>
                            <td class="ui-pg-button ui-corner-all" title="Export Call Detial Records to PDF" id="exp_pdfBD" nowrap>
                                <div class="ui-output-pdf"></div>
                            </td>
                            <td style="width: 4px;" class="ui-pg-button ui-state-disabled"><span class="ui-separator"></span></td>
                            <td style="margin: 1px 0 5px 0;" title="Print Call Detail Records" id="exp_printBD" nowrap>
                                <div class="ui-output-print"></div>
                            </td>                            
                       </tr>
                       
                       <tr>
                       		<td colspan="10">
	                            <a id="ScheduleOptionsToggleBD">Change Schedule</a>   
                            </td>
                       </tr>
                    </table>
                </div>
                     	
            </div>
           
            <div id="SetScheduleBD">
                           
                <cfinclude template="..\schedule\dsp_basic.cfm">    
            
                <div style="width 200px; margin:0px 0 5px 0;"><a id="AdvancedOptionsToggleBD">Show Advanced Schedule Options</a></div>
            
                <cfset inpShowPauseOption = 0>    
                <div id="AdvancedOptionsBD"> 
                <cfinclude template="..\schedule\dsp_advanced.cfm">
                </div>
        
            </div>	    
            
            <button id="UpdateScheduleButtonBD" TYPE="button">Update Schedule</button>
            <div id="loadingDlgBatchDetails" style="display:inline;">
	            <img class="loadingDlgBatchDetailsImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
            </div>
            <div id="AJAXInfoOut" class="smallReg" style="text-align:left;"></div>
            
            
            <div style="width 310px; min-width: 310px; margin:0px 0 0px 0; text-align:right;">
	            <button id="Cancel" TYPE="button" class="ui-corner-all">Exit</button>
            </div>
		</div>          
</form>

</div>


</cfoutput>

