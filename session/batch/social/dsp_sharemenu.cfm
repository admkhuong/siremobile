<cfparam name="INPBATCHID" default="0">
<cfhttp  url="#rootUrl#/#SessionPath#/cfc/socialmedia.cfc?method=CheckPostedBatch&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true" method="POST" result="resultFanpageInfo">
	<cfhttpparam type="formfield" name="INPBATCHID" value="#INPBATCHID#">
</cfhttp>

<cfset trackResult = deserializeJSON(resultFanpageInfo.fileContent)>
<cfset trackFacebookCount = trackResult.DATA.FACEBOOK_RESULT_COUNT[1]>

<cfsaveContent variable="htmlContent">
	<cfoutput>
		<cfif trackFacebookCount LT 1>
			<img class='publish_FacebookBatches ListIconLinks' id="postFacebook" rel='#INPBATCHID#' src='../../public/images/Facebook_publish_160X160.png' width='40' height='40'>
		</cfif>
		<img class='publish_TwitterBatches ListIconLinks' rel='#INPBATCHID#' src='../../public/images/Twitter_publish_256X256.png' width='40' height='40'>
		<div id="fb-root"></div>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="jsContent">
	
<cfif trackFacebookCount LT 1>
	<script type="text/javascript">
		
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		$('#postFacebook').click(function(){
			$.ajax({
	           type: "POST",
	           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/socialmedia.cfc?method=PublishFacebookObject&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
			   data:{
	   			 INPBATCHID : batchId
				},
	           dataType: "json", 
	           success: function(d) {
		           	if(d.ROWCOUNT >0){
		           		if(d.DATA.RXRESULTCODE >0){
		           			$('#shareSocialDialog').remove();
	           				$.jGrowl("Post to facebook fan page sucessfully", { life:2000, position:"center", header:' Message' });
		           		}
		           	}
		           	else
					{<!--- No result returned --->
						<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("No Response from the remote server. Check your connection and try again.", "Error.");
					}
		     	}
			});
		});
	</script>
	</cfif>
</cfsaveContent>

<cfoutput>
	#jsContent#
	#htmlContent#
</cfoutput>