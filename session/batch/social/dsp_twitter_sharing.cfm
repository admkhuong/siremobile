<cfparam name="INPBATCHID" default="0">

<cfparam name="INPBATCHID" default="0">
<cfsaveContent variable="htmlContent">
	<cfoutput>
		<div id="htabs_TwitterSharing">
			<ul style="height:30px;">          
				<li><a href="#rootUrl#/#SessionPath#/batch/social/dsp_twitterWall?inpbatchid=#INPBATCHID#">Tweet to home</a></li>
				<li><a href="#rootUrl#/#SessionPath#/batch/social/dsp_twitterFriends?inpbatchid=#INPBATCHID#">Tweet to friends</a></li>
				<li><a href="#rootUrl#/#SessionPath#/batch/social/dsp_twitterContacts?inpbatchid=#INPBATCHID#">Tweet to Contacts</a></li>
			</ul>
		</div>		
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="jsContent">
	<cfoutput>
		<script>
			var rootUrl = '#rootUrl#';
			var SessionPath = '#SessionPath#';
			var batchId = '#INPBATCHID#';
		</script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/twitter_sharing.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>