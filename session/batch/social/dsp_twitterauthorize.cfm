<cfsaveContent variable="serverCode">
	<cftry>
		<cfscript>
			returnData	= application.objMonkehTweet.getAccessToken(  
											requestToken	= 	session.oAuthToken,
											requestSecret	= 	session.oAuthTokenSecret,
											verifier		=	url.oauth_verifier
										);
						
		if (returnData.success) {
		
			//Save these off to your database against your User so you can access their account in the future
			session['twitterAccessToken']	= returnData.token;
			session['twitterAccessSecret']	= returnData.token_secret;
			session['twitterUserId']		= returnData.user_id;
			session['twitterUserName']		= returnData.screen_name;
		}
		
		</cfscript>
		<script type="text/javascript">
			window.opener.bindData();
			window.close();
		</script>
	<cfcatch>
		<script type="text/javascript">
			window.close();
			window.opener.$('#twitterDialog').remove();
		</script>
	</cfcatch>
	</cftry>
</cfsaveContent>

<cfoutput>
	#serverCode#
</cfoutput>

