<cfparam name="INPBATCHID" default="0">
<cfsaveContent variable="htmlContent">
	<cfoutput>
		<table id="listTwitterContact" align="center"></table>
		<div id="pagerTwitterContactList"></div>
		<img class='publishTwitterContactWall ListIconLinks' src='../../public/images/twitter_publish_60x30.png' title='Publish data'/>
	</cfoutput>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfoutput>
		<script type="text/javascript">
			var rootUrl = '#rootUrl#';
			var sessionPath = '#SessionPath#';
			var publicPath = '#PublicPath#';
			var batchId = '#INPBATCHID#';
			var babbleSphereDomain = '#BabbleSphereDomain#';
			var userId = '#Session.USERID#';
		</script>
		<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.base64.min.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/ebm_twitter.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/twitterContacts.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>