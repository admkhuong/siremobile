<cfparam name="INPBATCHID" default="0">
<cfsaveContent variable="htmlContent">
	<table id="listTwitterFriend" align="center"></table>
	<div id="pagerTwitterFriendList"></div>
	<img class='publishTwitterFriendWall ListIconLinks' src='../../public/images/twitter_publish_60x30.png' title='Publish data'/>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfoutput>
		<script>
			var rootUrl = '#rootUrl#';
			var sessionPath = '#SessionPath#';
			var publicPath = '#PublicPath#';
			var batchId = '#INPBATCHID#';
			var babbleSphereDomain = '#BabbleSphereDomain#';
			var userId = '#Session.USERID#';
		</script>
		<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.base64.min.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/ebm_twitter.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/twitterFriends.js"></script>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>