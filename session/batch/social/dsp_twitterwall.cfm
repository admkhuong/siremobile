<cfparam name="INPBATCHID" default="0">
<cfsaveContent variable="htmlContent">
	<table id="twitterWall" align="center"></table>
	<div id="pagerTwitterWall"></div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfoutput>
	<cfscript>
	    application.objMonkehTweet = createObject('component',  
	        '#cfcmonkehTweet#')  
	        .init(  
	            consumerKey         =   '#APPLICATION.Twitter_Consumerkey#',  
	            consumerSecret      =   '#APPLICATION.Twitter_Consumersecret#',  
	             
	            oauthToken          =   '#APPLICATION.Twitter_AccessToken#',  
	            oauthTokenSecret    =   '#APPLICATION.Twitter_Consumersecret#',  
	            /* 
	            userAccountName     =   '< enter your twitter account name >',  
	            */  
	            parseResults        =   true  
	        ); 
	    
	        authStruct = application.objMonkehTweet;
			authStruct = application.objMonkehTweet.getAuthentication(callbackURL='#rootUrl#/#SessionPath#/batch/social/dsp_twitterAuthorize');
		
		//if (authStruct.success){
			//	Here, the returned information is being set into the session scope.
			//	You could also store these into a DB (if running an application for multiple users)
			session.oAuthToken		= authStruct.token;
			session.oAuthTokenSecret	= authStruct.token_secret;

		//}
		
	</cfscript>
		<script type="text/javascript">
			var rootUrl = '#rootUrl#';
			var sessionPath = '#SessionPath#';
			var publicPath = '#PublicPath#';
			var batchId = '#INPBATCHID#';
			var babbleSphereDomain = '#BabbleSphereDomain#';
			var userId = '#Session.USERID#';
		</script>
		<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery.base64.min.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/ebm_twitter.js"></script>
		<script TYPE="text/javascript" src="#rootUrl#/#SessionPath#/js/social/twitterWall.js"></script>
		<cfif StructKeyExists(session, 'twitterUserId')>
		<script TYPE="text/javascript">
			bindData();
		</script>
		<cfelse>
		<script type="text/javascript">
			var w = window.open("#authStruct.authURL#", 'authenWindow', 'width=500,height=500');
		 	w.moveTo(300, 100);
		</script>
		</cfif>
	</cfoutput>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>