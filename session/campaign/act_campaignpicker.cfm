<!--- This page hides all of the regular site stuff so this looks good in a dialog --->

<cfinclude template="../../public/paths.cfm" >

<!---<cfinclude template="../#SessionDisplayPath#/dsp_header.cfm" />   --->         

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfparam name="isDialog" default="1">

<div id="coverfilterload" style="padding: 10px 20px 60px 20px; display:none;">
	<cfinclude template="listcampaigns.cfm">
</div>

<cfset FilterSetId = "BatchPickerList">
<cfparam name="FormParams" default="">

<!--- Override filter methods to call $Dialog.Load method instead .....--->
<script>
	$(document).ready(function(){
		
		<!--- Remove default click event--->
		$("#<cfoutput>#FilterSetId#</cfoutput> .page_button").unbind('click');
							
		$("#<cfoutput>#FilterSetId#</cfoutput> .page_button").click(function(){
					
			var submitLink = '../campaign/act_campaignpicker';
	<!---		
			var filterData = decodeURI($(this).attr('filterData')); 
			var sortData = decodeURI($(this).attr('sortData')); 
			var page= $(this).attr('page');
		
			var data = {
				filterData : JSON.stringify(filterData),
				sortData : JSON.stringify(sortData),
				page : page
			};
			
			console.log(filterData);		
			console.log(JSON.stringify(filterData));		
						
			<cfoutput>
				<!---// $("##filters_content").submit();--->
				$dialogCampaignPicker.load(submitLink, data, function () { });
			</cfoutput>
	--->
		
			
			<!---var filterForm = document.getElementById("filters_content");--->
				var filterForm = $("#<cfoutput>#FilterSetId#</cfoutput> #filters_content");
				if(filterForm == null)
				{
					<!---window.location = submitLink;--->
					<cfoutput>				
						$dialogCampaignPicker.load(submitLink, {"requestOveride": 'PageButtonClick', #FormParams#}, function () { });
					</cfoutput>
				}
				else
				{					
					<cfoutput>#FilterSetId#</cfoutput>reIndexFilterID();					
					
					$("#<cfoutput>#FilterSetId#</cfoutput> #filters_content").attr("action", submitLink);
														
					// Check if dont user filter
					if($("#<cfoutput>#FilterSetId#</cfoutput> #isClickFilter").val() == 0)
					{
						$("#<cfoutput>#FilterSetId#</cfoutput> #totalFilter").val(0);
					}
					
					var $inputs = $('#<cfoutput>#FilterSetId#</cfoutput> #filters_content :input');
					
					var values = {"page": $(this).attr('page'), "requestOveride": "FilterSubmit"};
											
					$inputs.each(function() {
						values[this.name] = $(this).val();
					})
								
					<cfoutput>
						<!---// $("##filters_content").submit();--->
						$dialogCampaignPicker.load(submitLink, values, function () { });
					</cfoutput>
				}
				
			
			return false;
		});
		
		<!--- Override form default action--->
		$("#<cfoutput>#FilterSetId#</cfoutput> #filters_content").submit( function(e){ 
						 
  			<!--- Prevent default events like submit from bubbleing up--->
			e.preventDefault();
				
			var $inputs = $('#<cfoutput>#FilterSetId#</cfoutput> #filters_content :input');
			
			var values = {"requestOveride": "FilterSubmit"};
									
    		$inputs.each(function() {
				values[this.name] = $(this).val();
			})
			
			<!---// $("##filters_content").submit();--->
			$dialogCampaignPicker.load('../campaign/act_campaignpicker', values, function () { });
		});
		
	});	
		
	<!--- Override clear filters default action--->
	function <cfoutput>#FilterSetId#</cfoutput>ClearFilter() {
			
			$dialogCampaignPicker.load('../campaign/act_campaignpicker', {"requestOveride": 'ClearFilter'}, function () { });
			
		}

</script>

        