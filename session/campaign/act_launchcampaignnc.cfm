<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/campaign/mycampaign/recipients.css');
	</style>
</cfoutput>
<cfparam name="INPGROUPID" default="-1">
<cfparam name="INPBATCHID" default="-1">
<cfparam name="INPBATCHDESC" default="-1">
<cfparam name="NOTE" default="">
<cfparam name="CONTACTTYPES" default="1,2,3">
<cfparam name="APPLYRULE" default="1">
<cfparam name="percentageSend" default="0">
<cfparam name="inpLimitDistribution" default="0">
<cfparam name="ABTestingBatches" default="">
<cfparam name="inpABCampaignId" default="0">

<cfset VOICECount = 0>
<cfset EMAILCount = 0>
<cfset SMSCount = 0>
<cfif CONTACTTYPES EQ "">
	<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetBatchRecipients"
		 returnvariable="GetBatchRecipientData">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                
	</cfinvoke>

	<cfif GetBatchRecipientData.RXRESULTCODE EQ 1>
		<cfset CONTACTTYPES = GetBatchRecipientData.DATA[1].ContactTypes_vch>
		<cfset NOTE = GetBatchRecipientData.DATA[1].ContactNote_vch>
		<cfset APPLYRULE = GetBatchRecipientData.DATA[1].ContactIsApplyFilter>
	</cfif> 
</cfif>

<!--- Added to allow support for no data selected/defined yet--->
<cfset recipientData = ArrayNew(1)>

<cfif CONTACTTYPES NEQ "">
	<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetRecipientList"
		 returnvariable="GetRecipientList">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
		<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#"/>
		<cfinvokeargument name="APPLYRULE" value="#APPLYRULE#"/>
		<cfinvokeargument name="NOTE" value="#NOTE#"/> 
		<cfinvokeargument name="INPGROUPID" value="#INPGROUPID#"/>
        <cfinvokeargument name="BlockGroupMembersAlreadyInQueue" value="1"/>
        <cfinvokeargument name="ABTestingBatches" value="#ABTestingBatches#"/>   
	</cfinvoke>
    
    <!---<cfdump var="#GetRecipientList#">--->
    
	<cfif GetRecipientList.RXRESULTCODE LT 1>
	    <cfthrow MESSAGE="Filter Recipient Data Error" TYPE="Any" detail="#GetRecipientList.MESSAGE# - #GetRecipientList.ERRMESSAGE#" errorcode="-5">                        
	</cfif> 
	
	<cfset RecipientData = GetRecipientList.DATA>

	<cfloop Array="#RecipientData#" index="recipientItem">
		<cfif recipientItem.ContactTypeId_int EQ 1>
			<cfset VOICECount = VOICECount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 2>
			<cfset EMAILCount = EMAILCount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 3>
			<cfset SMSCount = SMSCount + 1>
		</cfif>
	</cfloop>
</cfif>

<!--- get group contact --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.multilists2" method="GetSingleGroupData" returnvariable="GetGroupInfoReturnValue">	  
     <cfinvokeargument name="INPGROUPID" value="#INPGROUPID#"/> 
</cfinvoke>

<!---<cfdump var="#GetGroupInfoReturnValue#">--->

<cfset GetGroupInfoReturnValue.GROUPNAME_VCH = "No Data Found!">
 
<!---<cfif GetGroupInfoReturnValue.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Get group contact" TYPE="Any" detail="#GetGroupInfoReturnValue.MESSAGE# - #GetGroupInfoReturnValue.ERRMESSAGE#" errorcode="-5">                        
   
</cfif> --->

 
<style>

#CampaignRecipients .table_body {
    border: 1px solid #636363;
    border-radius: 10px 10px 10px 10px;
    margin-left: 10%;
    margin-top: 50px;
	margin-bottom: 50px;
    width: 80%;
}

</style> 
 
<!--- JS-------->
<script type="text/javascript">
	
	$(function(){
						
		$('#btnRefreshFilter').click(function(){
			var params = GetParams();
				
			<cfoutput>				
					$dialogLaunchCampaignNoChange.load('#rootUrl#/#SessionPath#/campaign/act_LaunchCampaignNC', params, function () { });
			</cfoutput>
		});
					
		$('#btnCancel').click(function(){
			$dialogLaunchCampaignNoChange.dialog('close');
		});		
		
		$(".nav_recipient_detail").click(function() {
			var contactType = '';
			var selectedItem = $(this).attr("value");
			if ($('#chkVoice_ContactType').is(':checked') && selectedItem == "1") {
				contactType += '1,';
			}
			
			if ($('#chkEmail_ContactType').is(':checked') && selectedItem == "2") {
				contactType += '2,';
			}
			
			if ($('#chkSMS_ContactType').is(':checked') && selectedItem == "3") {
				contactType += '3,';
			}
			<cfoutput>
				var params = GetParams(contactType);
				params.page =  'launchCampaign';
				post_to_url('#rootUrl#/#SessionPath#/campaign/mycampaign/RecipientListDetails', params, 'POST');
			</cfoutput>
		});
		
		$('#btnLaunchCampaign').click(function(){
			addgroupToQueueConsoleII();
		});		
				
	});
				
	function GetParams(inpContactType)
	{
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		var INPBATCHDESC = '<cfoutput>#INPBATCHDESC#</cfoutput>';

		var contactType = '';
		
		<!---console.log(batchId);--->
		
	
		if ($('#CampaignRecipients #chkVoice_ContactType').is(':checked')) {
			contactType += '1,';
		}
		
		if ($('#CampaignRecipients #chkEmail_ContactType').is(':checked')) {
			contactType += '2,';
		}
		
		if ($('#CampaignRecipients #chkSMS_ContactType').is(':checked')) {
			contactType += '3,';
		}
		
		var params = {};
		params.INPBATCHDESC = INPBATCHDESC;
		params.INPBATCHID = batchId;
		params.CONTACTTYPES = contactType;
		params.INPGROUPID = $('#CampaignRecipients #INPGROUPID').val();
		params.ABTestingBatches = $('#CampaignRecipients #ABTestingBatches').val();
		params.inpLimitDistribution = $('#CampaignRecipients #inpLimitDistribution').val();
		params.inpABCampaignId = '<cfoutput>#inpABCampaignId#</cfoutput>';
				
		if (inpContactType != undefined)
		{
			params.SELECTEDCONTACTTYPE = inpContactType;
		}
	
			
		return params;
	}
	
	function GetRecipientsData(){
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		var INPBATCHDESC = '<cfoutput>#INPBATCHDESC#</cfoutput>';
		var groupId = $('#INPGROUPID').val();		
		var inpLimitDistribution = $('#inpLimitDistribution').val();
		
		if(parseInt(inpLimitDistribution) >= 0)
			inpLimitDistribution = parseInt(inpLimitDistribution);
		else
			inpLimitDistribution = 0;
		
		var TotalCountElligable = 0;
		var contactType = '';
		if ($('#chkVoice_ContactType').is(':checked')) {
			contactType += '1,';
			TotalCountElligable = TotalCountElligable + parseInt('#VOICECount#');
		}
		
		if ($('#chkEmail_ContactType').is(':checked')) {
			contactType += '2,';
			TotalCountElligable = TotalCountElligable + parseInt('#EMAILCount#');
		}
		
		if ($('#chkSMS_ContactType').is(':checked')) {
			contactType += '3,';
			TotalCountElligable = TotalCountElligable + parseInt('#SMSCount#');
		}
		
		
		var data = {};
		data.INPBATCHDESC = encodeURIComponent(INPBATCHDESC);
		data.INPBATCHID = batchId;
		data.INPGROUPID = groupId;
		data.ContactTypes = contactType;				
		data.inpLimitDistribution = inpLimitDistribution;
		data.TotalCountElligable = TotalCountElligable;
		data.ABTestingBatches = $('#CampaignRecipients #ABTestingBatches').val();
				
		return data;
	}
	
	function ShowRecipientList() {
		var formRecipientList = $("#tmplPreviewRecipients").tmpl();
		formRecipientList.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Communication',
			open: function() {
				$("#QuestionContainer").validate();
			},
			close: function() {  $(this).dialog('destroy'); $(this).remove() },
			width: 600,
			height: 150,
			position: 'top',
			buttons:
				[
					{
						text: "OK",
						click: function(event)
						{
							formRecipientList.remove();	
							return false;
						}
					}
				]
			});	
	}
			
	function addgroupToQueueConsoleII() {	
		if (parseInt('<cfoutput>#ArrayLen(RecipientData)#</cfoutput>') < 1) {			
			jAlertOK("No elligible contacts to dial as part of currently selected filters.", "Warning");
			return;	
		}
		
		var data = GetRecipientsData();
				
		if (parseInt(data.inpLimitDistribution) < 1) {			
			jAlertOK("You have not specified how many contacts to send as part of this batch.", "Warning");
			return;	
		}
		
		
		$.ajax({
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddFiltersToQueue&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data: data,					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {<!---$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide();---> <!---console.log(textStatus, errorThrown);--->},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{						
					<!--- Check if variable is part of JSON result string --->								
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{							
						CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
						if(CurrRXResultCode > 0)
						{
							
							var TotalCountQueued = 0;
							
							if(parseInt(d.DATA.COUNTQUEUEDUPVOICE[0]) > 0)
								TotalCountQueued = TotalCountQueued + parseInt(d.DATA.COUNTQUEUEDUPVOICE[0]);
								
							if(parseInt(d.DATA.COUNTQUEUEDUPEMAIL[0]) > 0)
								TotalCountQueued = TotalCountQueued + parseInt(d.DATA.COUNTQUEUEDUPEMAIL[0]);
							
							if(parseInt(d.DATA.COUNTQUEUEDUPSMS[0]) > 0)
								TotalCountQueued = TotalCountQueued + parseInt(d.DATA.COUNTQUEUEDUPSMS[0]);		
							
							if(TotalCountQueued > 0)
							{	
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Campaign queued up successfully. Total added to queue (" + TotalCountQueued.toFixed(0) + ")", "Success", function(result) { 
									<!---$dialogLaunchCampaignNoChange.dialog('close');--->
									
									<cfoutput>
										var params = {};
										params.inpABCampaignId =  '#inpABCampaignId#';
										post_to_url('#rootUrl#/#SessionPath#/abcampaign/abcampaign', params, 'POST');
									</cfoutput>
			
								});
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("No data was elligible to queued up with current filters in place. Please adjust and try again.", "Warning", function(result) { 
									<!---$dialogLaunchCampaignNoChange.dialog('close');		--->							
								});								
							}
							
							
							$("#loadingDlgRenameCampaign").hide();	
							return false;
								
						}
						else
						{
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Campaign has NOT been queued.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
						}
					}
					else
					{<!--- Invalid structure returned --->	
						
					}
				}
				else
				{<!--- No result returned --->						
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
									
				<!---$("#dialog_renameCampaign #loadingDlgRenameCampaign").hide();--->								
			} 		
			
		});
		return false;		
	}
				
</script>


<cfoutput>

<!----Display information------->	   
<form id="CampaignRecipients" name="CampaignRecipients" action="" method="POST">

	<input type="hidden" id="ABTestingBatches" value="#ABTestingBatches#">
   
	
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label">Launch Campaign</label>
            <BR />
            <label class="table_header_label">(#INPBATCHID#) #INPBATCHDESC#</label>
		</div>
		<div class="content_padding">
			
           <!--- <div class="data_row">
				(#ABTestingBatches#)
			</div>--->
           
			<div class="padding_top">
	            <label class="bold_label left">Elligible counts remaining in Contact List </label>
                <BR />
				<div class="contact_type_body">
					<div class="contact_type_label">VOICE</div>
					<div class="contact_type_number_label"><a href="##" id="navVoiceRecipient" value="1" class="nav_recipient_detail"><cfoutput>#VOICECount#</cfoutput></a></div>
				</div>
				<div class="contact_type_body contact_type_body_margin">
					<div class="contact_type_label">EMAIL</div>
					<div class="contact_type_number_label"><a href="##" id="navEmailRecipient" value="2" class="nav_recipient_detail"><cfoutput>#EMAILCount#</cfoutput></a></div>
				</div>
				<div class="contact_type_body contact_type_body_margin">
					<div class="contact_type_label">SMS</div>
					<div class="contact_type_number_label"><a href="##" id="navSMSRecipient" value="3" class="nav_recipient_detail"><cfoutput>#SMSCount#</cfoutput></a></div>
				</div>
			</div>
			<div class="data_row">
				<div class="left"><label class="bold_label left">Contact Group</label></div>
                <div style="clear:both"></div>  
				<div class="left">
                    <input type="hidden" id="INPGROUPID" value="#INPGROUPID#">
                    <label>#HtmleditFormat(GetGroupInfoReturnValue.GROUPNAME)#</label>
				</div>
			</div>
			<div class="large_data_row">
				<div class="left"><label class="bold_label">Contact Type</label></div>
				<div class="contact_type_checkbox">			
					<input type="checkbox" id="chkVoice_ContactType"
						<cfif ListContains(CONTACTTYPES, 1)>
							checked
						</cfif>>
					<label for="chkVoice_ContactType">Voice</label>
				</div>
				<div class="contact_type_checkbox">
					<input type="checkbox" id="chkEmail_ContactType"
						<cfif ListContains(CONTACTTYPES, 2)>
							checked
						</cfif>>
					<label for="chkEmail_ContactType">Email</label>
				</div>
				<div class="contact_type_checkbox">
					<input type="checkbox" id="chkSMS_ContactType"
						<cfif ListContains(CONTACTTYPES, 3)>
							checked
						</cfif>>
					<label for="chkSMS_ContactType">SMS</label>
				</div>
			</div>
			
			<div id="divAdvancedOptions">
				
                <div class="large_data_row">
					<label class="bold_label">Limit Amount Distributed.</label>
                    <BR />
                  <!---  <label>Warning this will only work if duplicates are not allowed for the batch in advanced batch options.</label>
					<BR />--->
                    
                     <!--- Check for wether duplicates allowed or other BR's here--->                           
                    <cfquery name="getDistinctBatchIdsDupeProcessingFlag" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            AllowDuplicates_ti
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>  
                    
                    <cfif getDistinctBatchIdsDupeProcessingFlag.AllowDuplicates_ti GT 0>
                        <input type="text" id="inpLimitDistribution" value="0" disabled="disabled">
                    <cfelse>
                    	<input type="text" id="inpLimitDistribution" value="#inpLimitDistribution#">
                    </cfif>   
                    
				</div>
				
			</div>
            
            <div class="data_row">
				<label class="bold_label left">Total cost to run campaign:</label><span style="padding-left: 5px;"><cfoutput>#lscurrencyFormat(Arraylen(RecipientData)/10)#</cfoutput></span>
			</div>
            
		</div>
		<div class="large_data_row" style="height: 35px;">
			<button class="right" type="button" id="btnCancel">Cancel</button>			
            <button class="right" type="button" id="btnLaunchCampaign">Launch Campaign</button>
            <button class="right" type="button" id="btnRefreshFilter">Update</button>			
		</div>
	</div>
</form>

</cfoutput>


<script id="tmplPreviewRecipients" type="text/x-jquery-tmpl">
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label">Campaign Recipients</label>
		</div>
		<div class="right">
			3 Email Recipients
		</div>
		<div>
			<div style="width: 50%; float: left">Email Address</div>
			<div style="width: 50%; float: right">Note</div>
		</div>
	</div>
</script>