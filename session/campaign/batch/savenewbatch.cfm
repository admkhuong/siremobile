<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Create_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>
	<cfexit>
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="getCurentUser" returnvariable="getCurentUserVar">

<script TYPE="text/javascript">
	function addBatch() {
		if ($.trim($("#AddNewBatchMCContent #inpBatchDesc").val()) == "") {
			$('#lblCampaignNameRequired').show();
			return;
		}
		$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddNewBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				INPBATCHDESC : $("#AddNewBatchMCContent #inpBatchDesc").val(),
				DefaultCIDVch: $('#DefaultCID_vch').val() 
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) {
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) {						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0){							
										if (typeof(d.DATA.NEXTBATCHID[0]) != "undefined") {		
											var batchId = d.DATA.NEXTBATCHID[0];		
											window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/stage/src/?inpbatchid=</cfoutput>' + batchId;
										}
										return false;
								}
								else {
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
				} 		
			});
	}
	
	
	$(function()
	{	
		$('#subTitleText').text('Campaign >> Create Campaign');
		
		$("#AddNewBatchMCContent #AddNewBatchButton").click( function() { addBatch(); return false;  }); 	
		
		<!--- Kill the new dialog --->
		$("#AddNewBatchMCContent #Cancel").click(function() {
			window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
		}); 	
	});	
	function handleKeyPress(e){
		var key=e.keyCode || e.which;
		if (key==13){
			addBatch();
	  		return false; 
		}
	}
</script>


<style>

.label-campaign-container{
	width:185px;
}

#AddNewBatchMCContent #LeftMenu
{
	width:270px;
	min-width:270px;		
	background: #B6C29A;
	background: -webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(1, rgb(237,237,237)),
    color-stop(0, rgb(200,216,143))
	);
	background: -moz-linear-gradient(
		center top,
		rgb(237,237,237),
		rgb(200,216,143)
	);
	
	position:absolute;
	top:-8px;
	left:-17px;
	padding:15px;
	margin:0px;	
	border: 0;
	border-right: 1px solid #CCC;
	box-shadow: 5px 5px 5px -5px rgba(88, 88, 88, 0.5);
	min-height: 100%;
	height: 100%;
	z-index:2300;
}


#AddNewBatchMCContent #RightStage
{

	padding:0;
	margin:0px;	
	border: 0;
}


#AddNewBatchMCContent h1
{
	font-size:12px;
	font-weight:bold;	
	display:inline;
	padding-right:10px;	
	min-width: 100px;
	width: 100px;	
}

#info-box1{
	width:215px;
	left: 460px;
    position: absolute;
    top: 238px;
	background:#eee;
	padding:15px;
}

#tool1{
	left: 447px;
    position: absolute;
    top: 238px;
}

#info-box2{
	width:215px;
	left: 460px;
    position: absolute;
    top: 290px;
	background:#eee;
	padding:15px;
}

#tool2{
	left: 447px;
    position: absolute;
    top: 290px;
}


</style> 

<cfoutput>
       
<div id='AddNewBatchMCContent' class="RXForm">
    
    <div id="RightStage">
                       
        <cfform id="AddDialStringForm" name="AddDialStringForm" action="" method="POST">
        
        <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
		
		<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="haveCidPermission">
			<cfinvokeargument name="operator" value="#Caller_ID_Title#">
		</cfinvoke>
        <div class="header-campaign-container">Create a Campaign</div>
		<div class="content-campaign-container">
	        <div class="subheader-campaign-container">Setup Campaign</div>
	        
			<div style="padding-top: 10px; padding-bottom: 10px">
               	<div class="label-campaign-container">New Campaign Name <div id="CampaignNameInfo" class="info_box"></div></div>
				<div id="tool1"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip.gif"/></div>
				<div id="info-box1"></div>
				<input type="text" name="inpBatchDesc" id="inpBatchDesc" onkeypress="return handleKeyPress(event);"/> 
	            <label id="lblCampaignNameRequired" style="color: red; display: none;">
					Campaign name is required
				</label>
			</div>
			<cfif haveCidPermission.HAVEPERMISSION>
				<div>
				    <div class="label-campaign-container">Call ID <div id="CallIdInfo" class="info_box"></div></div>
					<div id="tool2"><img src="<cfoutput>#rootUrl#/#publicPath#/css/ire/</cfoutput>images/toll-tip.gif"/></div>
					<div id="info-box2"></div>
					<cfinput type="text" name="DefaultCID_vch" id="DefaultCID_vch" value="#getCurentUserVar.DefaultCID#" />
				</div>
			</cfif>
		</div>
		<div class="footer-campaign-container">
			<div style="padding-top: 10px; padding-bottom: 10px">
				<button id="AddNewBatchButton" type="button" class="ui-corner-all">&nbsp;&nbsp;Next&nbsp;&nbsp;</button>
	            <button id="Cancel" type="button" class="ui-corner-all">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
			</div>
		</div>
        </cfform>
    
    </div>

</div>

</cfoutput>

<script>
	$(document).ready(function(){
		$('#info-box1').hide();
		$('#tool1').hide();
		$('#info-box2').hide();
		$('#tool2').hide();
		
		$('#CampaignNameInfo').hover(
			function() {
				$('#info-box1').show();
				$('#tool1').show();
				$('#CampaignNameInfo.info_box').css('opacity',1);
			}, function() {
				$('#info-box1').hide();
				$('#tool1').hide();
				$('#CampaignNameInfo.info_box').css('opacity',.4);
			}
		);
		
		$('#CallIdInfo').hover(
			function() {
				$('#info-box2').show();
				$('#tool2').show();
				$('#CallIdInfo.info_box').css('opacity',1);
			}, function() {
				$('#info-box2').hide();
				$('#tool2').hide();
				$('#CallIdInfo.info_box').css('opacity',.4);
			}
		);
	});
</script>

