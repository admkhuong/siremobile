<cfparam name="isDialog" default="0">

<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#EMS_List_Title#">
</cfinvoke>


<cfif NOT Session.USERID GT 0>
	<cfexit><!---exit if session expired --->
</cfif>



<cfoutput>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
<!---	<script language="javascript" src="#rootUrl#/#publicPath#/js/sms/ui.spinner.js"></script>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/sms/ui.spinner.css"/>
	<script language="javascript" src="#rootUrl#/#PublicPath#/js/selectmenu/jquery.ui.selectmenu.js"></script>--->

	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.jplayer.inspector.js" type="text/javascript"></script>

	<script src="#rootUrl#/#PublicPath#/js/jquery.transform.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/jquery.grab.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/mod.csstransforms.min.js" type="text/javascript"></script>
	<script src="#rootUrl#/#PublicPath#/js/circle.player.js" type="text/javascript"></script>
	<link  type="text/css" rel="stylesheet" href="#rootUrl#/#PublicPath#/css/selectmenu/jquery.ui.selectmenu.css" />
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/ems/emergencymessages.css"/>
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/css/circle.skin/circle.player4.css"/>
</cfoutput>


<!--- Style load --->
<cfoutput>
    
    <script src="#rootUrl#/#PublicPath#/js/validationregex.js"></script>
	<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="#rootUrl#/#PublicPath#/js/tinymce_4.0.8/tinymce.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="#rootUrl#/#publicPath#/js/jquery.ddslick.min.js"></script>
        
	<style>		
		@import url('#rootUrl#/#PublicPath#/css/survey.css') all;
		@import url('#rootUrl#/#PublicPath#/css/utility.css') all;
		@import url('#rootUrl#/#PublicPath#/css/survey/surveyquestion.css') all;		
	</style>
      
</cfoutput>


<style type="text/css">
		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}

		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			<!---width: 700px;--->
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			min-height:300px;
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			font-family:Verdana;
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}

		table.dataTable th{
			border-right: 1px solid #CCC;
		}

		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.statistic-bold{
			color:#0888d1;
			font-family:Verdana;
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			font-family:Verdana;
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.datatables_info {
		    color: #666666;
		    font-size: 14px;
		    left: 12px;
		    position: relative;
		    top: 32px;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}

		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}

		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}

		table.dataTable tr.not_run,
		table.dataTable tr.not_run td.sorting_1{
		    background-color: #FEF1B5 !important;
		}

		table.dataTable tr.paused,
		table.dataTable tr.paused td.sorting_1{
		    background-color: #EEB4B4 !important;
		}
		.preview-ems{
			padding:10px;
		}

		.EMSIcon
		{
			margin-left:3px;
			margin-right:3px;
		}

		.BatchDescLink:hover
		{
			text-decoration:underline !important;
		}

	</style>


<!---wrap the page content do not style this--->
<div id="page-content">

  <div class="container" >
    	<h2 class="no-margin-top">Campaign Template</h2>

        <div id="filter">
            <cfoutput>
            <!---set up column --->
            <cfset datatable_ColumnModel = arrayNew(1)>
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Template name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 't.Name_vch'})>
            
            <!---we must define javascript function name to be called from filter later--->
            <cfset datatable_jsCallback = "InitControl">
            <cfset datatable_showFilterDashboard = "false">
            <cfinclude template="/session/ems/datatable_filter.cfm" >
            </cfoutput>
        </div>
        <div id="content-AAU-Admin">
            <div id="buttonBar">
                                
            </div>
            <div class="clear"></div>

			<cfoutput>
				<script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/js/jquery.datatables.min.js"></script>
                <script type="text/javascript" src="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/js/dataTables.responsive.min.js"></script>
                <link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/home7assets/js/datatables1.10.7/extensions/Responsive/css/dataTables.responsive.css">
  			</cfoutput>

            <div id="divAgent" class="border_top_none">
                <table id="tblListEMS" class="table" width="100%">
                </table>
            </div>
            <div class="clear-fix"></div>
        </div>

 		<p class="lead"></p>


        <row>

            <div class="">


            </div>

        </row>

   </div>
  <!--- /.container --->

</div>
<!--- /#page-content --->





<!-- Modal -->
<div class="modal fade" id="ScheduleOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:auto; max-width:900px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Save Template</h4>

            </div>
            <div class="modal-body">

                <!---<div id="AdvanceScheduleContent">
                    <cfinclude template="../../ems/dsp_advance_schedule.cfm">
                </div>--->

			</div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->




<script type="text/javascript">


<!--- Don't overide titles with pickers --->
	<cfif isDialog EQ 0>
		$('#mainTitleText').html('<cfoutput>Campaigns <img height="9px" width="17px" class="levelsep" src="#rootUrl#/#publicPath#/images/ebmlogosmall_web.png"> EMS </cfoutput>');
		$('#subTitleText').html('List Campaigns');
	</cfif>

		var _tblListEMS;
		$(document).ready(function(){
			InitControl();

		});


		function InitControl(customFilterObj){//customFilterObj will be initiated and passed from datatable_filter
			var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
			//init datatable for active agent
			_tblListEMS = $('#tblListEMS').dataTable( {
			    "bProcessing": true,
				"bFilter": false,
				"bServerSide":true,
				"bDestroy":true,<!---this attribute must be explicit, otherwise custom filter will not work properly  --->
				"sPaginationType": "full_numbers",
			    "bLengthChange": false,
				"iDisplayLength": 10,
			    "aoColumns": [
					{"sName": 'image', "sTitle": 'ID', "sWidth": '8%',"bSortable": false},
					{"sName": 'name', "sTitle": 'Name', "sWidth": '28%',"bSortable": false},
					{"sName": 'Category', "sTitle": 'Category', "sWidth": '12%',"bSortable": false},
					{"sName": 'Order', "sTitle": 'Order', "sWidth": '12%',"bSortable": false},
					{"sName": 'Active', "sTitle": 'Active', "sWidth": '8%',"bSortable": false},
					
					{"sName": 'link', "sTitle": '', "sWidth": '15%',"bSortable": false}
					
				],
				"sAjaxDataProp": "ListEMSData",
				"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetCampaignTemplate&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
					$(nRow).children('td:first').addClass("avatar");

					if (aData && aData[8] == 'Not running') {
						$(nRow).removeClass('odd').removeClass('even').addClass('not_run');
					}
					else if (aData && ((aData[8] == 'Paused') || (aData[8] == 'Stopped'))) {
						$(nRow).removeClass('odd').removeClass('even').addClass('paused');
					}

		            return nRow;
		       	},
			   "fnDrawCallback": function( oSettings ) {
			      <!---$(".jquery_jplayer").each(function(){
						InitializeBabbles(".jquery_jplayer");
					});--->
			    },
				"fnServerData": function ( sSource, aoData, fnCallback ) {//this fn is used for filtering data
			       aoData.push(
			            { "name": "customFilter", "value": customFilterData}
		            );
			        $.ajax({dataType: 'json',
			                 type: "POST",
			                 url: sSource,
				             data: aoData,
				             success: fnCallback
				 	});
		        },
				"fnInitComplete":function(oSettings, json){
					$("#tblListEMS thead tr").find(':last-child').css("border-right","0px");
					<!---$(".next.paginate_button").html("&raquo;");
					$(".previous.paginate_button").html("&laquo;");
					$(".first.paginate_button").css("display","none");
					$(".last.paginate_button").css("display","none");--->

					<!---$(".jquery_jplayer").each(function(){
						InitializeBabbles(".jquery_jplayer");
					});--->
				}
		    });

			// event for create new EMS
			$('#btnCreateNew').click(function(){
				location.href = "<cfoutput>#rootUrl#/#sessionPath#</cfoutput>/ems/createnewems";
			});

		}

		// delete EMS
		function deleteEMS(TID){



			bootbox.confirm( "Are you sure you want to delete this template?", function(result) {

				if(result)
				{
					$.ajax({
						type: "POST", // Posts data as form data rather than on query string and allows larger data transfers than URL GET does
						url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=DeleteCampaignTemplate&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
						dataType: 'json',
						data:  { TID : TID},
						error: function(XMLHttpRequest, textStatus, errorThrown) {},
						success:
							<!--- Default return function for Do CFTE Demo - Async call back --->
							function(d)
							{
								if(d.RXRESULTCODE > 0)
								{
									// reload data
									var oTable = $('#tblListEMS').dataTable();
								  	oTable.fnDraw();
									return;
								}
								else
								{

									if (d.DATA.ERRMESSAGE[0] != '') {
										 bootbox.alert("This Template has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], function() { return; } );
									}
								}
							}
						});

						<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
						return;
				}
				else
				{	<!--- bootbox.confirm result funtion must return to make popup go away - even on cancel --->
					return;
				}

				return false;
			});
		}

		


	</script>
