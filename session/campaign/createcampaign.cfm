<cfparam name="INPBATCHID" default="">

<!---check permission--->
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#Create_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Create_Campaign_Title#">
</cfinvoke>

<cfoutput>
	<div id="MC_Voice_Container" style="position:relative;">
		<button id="SaveCampaign" type="button" class="ui-corner-all">Save Campaign</button>
	       
	    <div id="MC_Stage_MCIDgen" class="MC_Content_Stage_Voice  ui-corner-all">
	
	    </div>
	</div>
	
	<div id="overlay" style="text-align: center">
		<img src="#rootUrl#/#PublicPath#/images/loading.gif" width="100" height="100" style="padding-top: 200px;">
	    
	</div>
</cfoutput>

<script>
	
	$(function() {
		
		$('#subTitleText').text('<cfoutput>#Create_Campaign_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
		
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		if(batchId == ''){
			 addBatch();
		}else{
			 importCampaign(batchId)
		}
	});
	
	function addBatch() {
		$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=AddNewBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHDESC : 'New Batch'},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) {
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) {						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined"){							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								if(CurrRXResultCode > 0){							
										if(typeof(d.DATA.NEXTBATCHID[0]) != "undefined"){		
											var batchId = d.DATA.NEXTBATCHID[0];												
											importCampaign(batchId)
										}
										return false;
								}
								else {
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been added.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
						$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
				} 		
			});
	}
	
	function importCampaign(batchId){
		
		$("#MC_Stage_MCIDgen").load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_scriptbuilder?inpbatchid=' + batchId, function() { 
			return false; 
		});
		
		bindEventChangeUrl(batchId);
		
		$("#SaveCampaign").click(function() {
			$(window).unbind('unload');
			showSaveCampaignDialog(batchId);
			return false;
		});
	}
	
	function bindEventChangeUrl(batchId){
		$(window).unload(function(){
			if (confirm("Are you sure you want to quit this page without saving the campaign?")) {
			    removeBatchDetails(batchId);
		  	} else {
		  		window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/createCampaign?inpbatchid=' + batchId;
		  	}
		}); 
	}
	
	function showSaveCampaignDialog(batchId){
		var ParamStr = '';
			
		if(typeof(batchId) != "undefined" && batchId != "")					
			ParamStr = '?inpbatchid=' + encodeURIComponent(batchId);
		else
			batchId = 0;
		
		var $saveCampaignDialog = $("<div id='saveCampaignDialog'></div>");
		$saveCampaignDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/batch/dsp_saveBatch' + ParamStr)
			.dialog({
				modal : true,
				title: 'Campaign Name',
				width: 500,
				height: 160,
				position: 'top',
				close: function() { 
					bindEventChangeUrl(batchId);
					$saveCampaignDialog.remove(); 
				}
		});
	}
	
	function removeBatchDetails(INPBATCHID) {
		$("#loadingDlgAddBatchMCContent").show();		
		$.ajax({
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			async:false,
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {$("#AddAppendCommentsDiv #loadingDlgAppendComments").hide(); <!---console.log(textStatus, errorThrown);--->},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{								
										window.location = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';	
										return false;
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been saved.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );							
								}
							}
						}
						else
						{<!--- No result returned --->						
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
						$("#AddNewBatchMCContent #loadingDlgAddBatchMCContent").hide();
				} 		
		});
		return false;
	}
</script>

<style>
	<cfoutput>
		@import url('#rootUrl#/#PublicPath#/css/mb/jquery-ui-1.8.7.custom.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/poolballs/ivr.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/rxt/edit_rxt.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/scriptbuilder.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/ruler.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/campaignicons.css');
		@import url('#rootUrl#/#PublicPath#/css/rxdsmenu.css');
		@import url('#rootUrl#/#PublicPath#/css/socialmedia.css');
		@import url('#rootUrl#/#PublicPath#/js/help/tipsy.css');
		@import url('#rootUrl#/#PublicPath#/css/mcid/override.css');
	</cfoutput>
</style>