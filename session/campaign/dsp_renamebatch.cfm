<cfparam name="INPBATCHID" default="">
<cfparam name="inpOldDesc" default="">
<cfparam name="FormParams" default="">

<cfinvoke method="GetBatchDesc" component="#Session.SessionCFCPath#.batch" 
          returnvariable="RetVarGetBatchDesc">
	<cfinvokeargument name="INPBATCHID" value="#inpBatchId#">
</cfinvoke>

<cfset inpOldDesc = RetVarGetBatchDesc.DESC/>

<cfif RetVarGetBatchDesc.RXRESULTCODE GT 0>
	<cfset inpBatchDesc = "#Replace(HTMLEditFormat(RetVarGetBatchDesc.DESC), '''', '`', 'ALL')#"/>
</cfif>

<cfoutput>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Rename Campaign</h4>        
	
</div>

<div style="margin:20px;" id="KeyWordDetailsPopup">
		
    <div style="padding-bottom: 10px; padding-top: 10px;">
    
        <div class="inputbox-container">
            <label for="inpBatchDesc">
                Change Your Campaign Name 
                <span class="small">
                    (Required)
                </span>
            </label>
            <input id="inpBatchDesc" name="inpBatchDesc" placeholder="Enter Your Campaign Name Here" 
                   size="40" value="#inpBatchDesc#"/>
        </div>
        
        <div style="clear:both">
        </div>
        
        <div class="inputbox-container">
        
            <label id="lblCampaignNameRequired" style="color: red; display: none;">
                Campaign name is required
            </label>
        </div>
    </div>
    <div id="loadingDlgRenameCampaign" style="display:inline;">
        <img class="loadingDlgDeleteGroupImg" 
             src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" 
             height="20">
    </div>
    <div class='button_area' style="padding-bottom: 10px;">
        <button id="btnRenameCampaign" type="button" class="ui-corner-all survey_builder_button">
            Save
        </button>
        <button id="Cancel" class="ui-corner-all survey_builder_button" type="button">
            Cancel
        </button>
    </div>
			
	
    
       
</div>		

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        <button type="button" class="btn btn-primary" onclick="SaveBatch();">Save</button>
    
    </div>	
</div>   

</cfoutput>

<script type="text/javascript">
	function SaveBatch(INPBATCHID) {
				
		$("#loadingDlgRenameCampaign").show();		
		
		if('<cfoutput>#TRIM(inpOldDesc)#</cfoutput>' == $("#inpBatchDesc").val())
		{
			$('#RenameBatchtModal').modal('hide');
			return false;
		}
	
		if($("#inpBatchDesc").val() == '')
		{			
			bootbox.alert("Campaign has not been renamed.\n"  + "New name can not be blank." + "\n", function(result) { } );										
			$("#loadingDlgRenameCampaign").hide();	
			return;	
		}
	
		var data = 
		{ 
			INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>',
			Desc_vch : $("#inpBatchDesc").val()
		};
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc', 'UpdateBatchDesc', data, "Error - Batch has not been renamed!",
			function(d) {
				<!--- Just reload the current datatable with the current filters and paginging information --->
				$('#tblListEMS').DataTable().ajax.reload( function ( json ) {								
					<!--- Dont kill the modal until the table update is complete --->
					$('#RenameBatchtModal').modal('hide');
				}, false );					
			}
		);
	
	}
	
	$(function() {	
						
		$("#loadingDlgRenameCampaign").hide();	
	});
		
</script>
