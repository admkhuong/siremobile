
<cfinclude template="../sire/configs/paths.cfm">

<cfparam name="INPBATCHID" default="0">
<cfparam name="getMaxOrder" default="">
<cfparam name="RXOrderNumber" default="1">
<cfparam name="orderNumber" default="1">

<!--- Presumes dialog is opened assigned to a javascript var of CreateXMLContorlStringMasterTemplateDialogVoiceTools--->


<!--- Tool tips should not be centralized as they subject to many different positions and layouts --->
<style>

	
	#MC_Stage_XMLControlString_Master_Template .info-button1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/msg.gif") no-repeat scroll 0 0 transparent;
		color: #FFFFFF;
		cursor: pointer;
		float: right;
		height: 19px;
		margin-left: 3px;
		margin-right: 10px;
		margin-top: 19px;
		width: 18px;
	}
	
	
	#MC_Stage_XMLControlString_Master_Template .info-box1 {
		background: url("<cfoutput>#rootUrl#/#publicPath#/css/ire</cfoutput>/images/tool-bg.gif") repeat scroll 0 0 transparent;
		border-radius: 5px 5px 5px 5px;
		color: #FFFFFF;
		display: none;   
		height: auto;
		margin-top: 7px;
		padding-bottom: 20px;
		padding-right: 12px;
		padding-top: 5px;
		width: 100%;
		z-index:1000;
		box-shadow: 0 1px 1px rgba(50, 50, 50, 0.9);	
		position:absolute;
		top: -25px;
		right: 37px;
	}
	
	#MC_Stage_XMLControlString_Master_Template .tool1 {
		background-image: none;
		display: none;
		float: right;
		margin-top: 16px;
		z-index:1001 !important;
		position:absolute;
		top: 5px;
		right: 25px;
}

	#MC_Stage_XMLControlString_Master_Template .form-right-portal {
    z-index: 1000;
}

.dd-option-text,.dd-selected-text{
	line-height: 30px !important;
}
.dd-option-image, .dd-selected-image{
	max-height: 50px;
}

</style>
<script type="text/javascript">

	
	
	$(function() 
	{		
		
		$("#MC_Stage_XMLControlString_Master_Template #SaveCCD").click(function() { WriteCCDXMLString(); });	
		
		$('#AddNewTemplateModal').on('hide.bs.modal', function () {
		   $('#AddNewTemplateModal')
		   	.removeData();
		   $('.modal-body').empty();
		})					

		$("#inpSubmit").click(function() { 

			var INPNAME = $('#INPNAME').val();
			var INPDESC = tinymce.get("INPDESC").getContent();


			if(INPNAME === ""){
				bootbox.alert('Category Name can not blank');
				return false;
			}


            tinymce.get("INPDESC").save();

			try{
				$.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/templates.cfc?method=CreateCategoryTemplate&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:$('#form-create-template').serialize(),
		            dataType: "json", 
		            success: function(d) {
		            	
         				if(d.RXRESULTCODE == 1){
							
							bootbox.alert('Save Category Success', function() { 
								$('#AddNewTemplateModal').modal('hide');
								window.location.href = '/session/campaign/template_category'
							});
						}
						else
						{
							<!--- No result returned --->							
							bootbox.alert(d.MESSAGE);
						}
		         	}
		   		});
			}catch(ex){
				bootbox.alert('Create Fail', function() {});
			}
		});

        tinymce.init({
            selector: "textarea.normal",
//            theme: "modern",
            plugins: [
                 "advlist autolink link image lists charmap preview hr anchor pagebreak",
                 "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
                 "table contextmenu directionality emoticons paste textcolor code"
            ],
            relative_urls: true,
            browser_spellcheck: true,
            codemirror: {
                indentOnInit: true, // Whether or not to indent code on init. 
                path: 'CodeMirror'
            },
            convert_urls: false,
            menubar: false,
            image_advtab: true,
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | print preview code ",
            plugin_preview_width : "208",
        	plugin_preview_height : "180",
        	/*
            external_plugins: {
                "filemanager": "/public/js/tinymce_4.0.8/plugins/filemanager/plugin.js"
            }*/
         });

         $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

         $('#template_image').ddslick({
         	height: '200px',
         	width: '400px',
		    selectText: "Select Template Images",
		    onSelected: function (data) {
		        //console.log(data.selectedData.value);
		        $("#INPIMAGE").val(data.selectedData.value)
		    }
		});

	});




	

	
	
	

	

</script>	

<cfoutput>



<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Save Category</h4>  
          
</div>


<cfquery name="getCategory" datasource="#Session.DBSourceEBM#">
					SELECT * FROM simpleobjects.template_category WHERE CID_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#INPBATCHID#"> 
</cfquery>

<div class="modal-body">


    <div id="MC_Stage_XMLControlString_Master_Template" class="EBMDialog">
                          
        <form method="POST" id="form-create-template">
                                      
            <div class="inner-txt-box">
   
                <div class="inner-txt">Please give your category a unique name.</div>
            
                <div class="form-left-portal">
            
                    <input TYPE="hidden" name="INPCID" id="INPCID" value="<cfoutput>#INPBATCHID#</cfoutput>" />
                                                            
    	
                   
                    <label for="INPDESC">Category Name</label>
                    <input type="text" class="input-box" name="INPNAME" id="INPNAME" placeholder=""  style="width:100%;" value="<cfoutput>#getCategory.CategoryName_vch#</cfoutput>">
                    	
                    <label for="INPDESC">Category Display</label>
                    <input type="text" class="input-box" name="INPDISPLAY" id="INPDISPLAY" placeholder=""  style="width:100%;" value="<cfoutput>#getCategory.DisplayName_vch#</cfoutput>">
                    	
                    <div style="clear:both"></div>

                    <label for="INPDESC">Category Description</label>
                    <textarea id="INPDESC" name="INPDESC" placeholder="Give it a good description here" class="normal"><cfoutput>#getCategory.Description_txt#</cfoutput></textarea>
                                                
                    <div style="clear:both"></div>

                    
					<label for="INPORDER">Category Order</label>
                	<select id="template_order" name="INPORDER">
                		<cfloop from ="1" to="20" step="1" index="index">
                			<option <cfif getCategory.Order_int EQ index>selected</cfif> value="#index#">#index#</option>
                		</cfloop>
				    </select>

				                                        
                </div>                            
                                       
          
          
            </div>
                       
        </form>
           
    </div>     
    
</div> <!--- modal-body --->
    
<div class="modal-footer">

	<button type="button" class="btn btn-default" data-dismiss="modal" id="cancel">Cancel</button>
     
	<button type="button" class="btn btn-primary" id="inpSubmit">Save</button>
   
</div>	

    
</cfoutput>



    