<cfoutput>
	<style>
		@import url('#rootUrl#/#PublicPath#/css/campaign/mycampaign/recipients.css');
	</style>
</cfoutput>
<cfparam name="selectedGroup" default="-1">
<cfparam name="INPBATCHID" default="-1">
<cfparam name="NOTE" default="">
<cfparam name="CONTACTTYPES" default="">
<cfparam name="APPLYRULE" default="1">
<cfparam name="mode" default="edit">
<cfset VOICECount = 0>
<cfset EMAILCount = 0>
<cfset SMSCount = 0>
<cfif CONTACTTYPES EQ "">
	<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetBatchRecipients"
		 returnvariable="GetBatchRecipientData">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>                
	</cfinvoke>

	<cfif GetBatchRecipientData.RXRESULTCODE EQ 1>
		<cfif selectedGroup EQ "-1">
			<cfset selectedGroup = GetBatchRecipientData.DATA[1].CONTACTGROUPID_INT>
		</cfif>
		<cfset CONTACTTYPES = GetBatchRecipientData.DATA[1].ContactTypes_vch>
		<cfset NOTE = GetBatchRecipientData.DATA[1].ContactNote_vch>
		<cfset APPLYRULE = GetBatchRecipientData.DATA[1].ContactIsApplyFilter>
	</cfif> 
</cfif>

<!--- Added to allow support for no data selected/defined yet--->
<cfset recipientData = ArrayNew(1)>

<cfif CONTACTTYPES NEQ "">
	<cfinvoke 
		 component="#LocalSessionDotPath#.cfc.distribution"
		 method="GetRecipientList"
		 returnvariable="GetRecipientList">
		<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
		<cfinvokeargument name="CONTACTTYPES" value="#CONTACTTYPES#"/>
		<cfinvokeargument name="APPLYRULE" value="#APPLYRULE#"/>
		<cfinvokeargument name="NOTE" value="#NOTE#"/> 
		<cfinvokeargument name="INPGROUPID" value="#selectedGroup#"/>                        
	</cfinvoke>
	<cfif GetRecipientList.RXRESULTCODE LT 1>
	    <cfthrow MESSAGE="Filter Recipient Data Error" TYPE="Any" detail="#GetRecipientList.MESSAGE# - #GetRecipientList.ERRMESSAGE#" errorcode="-5">                        
	</cfif> 
	
	<cfset RecipientData = GetRecipientList.DATA>

	<cfloop Array="#RecipientData#" index="recipientItem">
		<cfif recipientItem.ContactTypeId_int EQ 1>
			<cfset VOICECount = VOICECount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 2>
			<cfset EMAILCount = EMAILCount + 1>
		<cfelseif recipientItem.ContactTypeId_int EQ 3>
			<cfset SMSCount = SMSCount + 1>
		</cfif>
	</cfloop>
</cfif>

<!---check permission--->
<!--- 
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#Run_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#urlBatchID#">
</cfinvoke>

<cfif NOT checkBatchPermissionByBatchId.havePermission >
	<cfset session.permissionError = permissionStr.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.history" method="addHistory" returnvariable="addHistory">
	<cfinvokeargument name="pageTitle" value="#Run_Campaign_Title# #urlBatchID#">
</cfinvoke>

<!----- Get information from database-------->
<cfset contactTypePhone = 1>
<cfset contactTypeEmail = 2>
<cfset contactTypeSMS = 3>

<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.distribution"
	 method="GetBatchDescription"
	 returnvariable="RetValCampaignDesc">                
	<cfinvokeargument name="INPBATCHID" value="#urlBatchID#"/>  
</cfinvoke>
                                        
<cfif RetValCampaignDesc.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="No description" TYPE="Any" detail="#RetValCampaignDesc.MESSAGE# - #RetValCampaignDesc.ERRMESSAGE#" errorcode="-5">                        
</cfif>  

<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.billing"
	 method="GetBalance"
	 returnvariable="RetValBillingData">                     
</cfinvoke>
                                        
<cfif RetValBillingData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Billing Error" TYPE="Any" detail="#RetValBillingData.MESSAGE# - #RetValBillingData.ERRMESSAGE#" errorcode="-5">                        
</cfif>  
<!--- Get filter phone --->	   
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.distribution"
	 method="GetListElligableGroupCount_ByType"
	 returnvariable="RetFilterPhone">
	<cfinvokeargument name="INPBATCHID" value="#urlBatchID#"/>  
	<cfif urlContactType EQ 0>
		<cfinvokeargument name="INPCONTACTTYPE" value="#contactTypePhone#"/>
	<cfelse>
		<cfinvokeargument name="INPCONTACTTYPE" value="#urlContactType#"/>
	</cfif>
	<cfinvokeargument name="inpDoRules" value="#urlDoRules#"/>    
	<cfinvokeargument name="inpSourceMask" value="#urlSourceMask#"/> 
	<cfinvokeargument name="type_mask" value="#urlTypeMask#"/> 
	<cfinvokeargument name="MCCONTACT_MASK" value="#urlContactMask#"/> 
	<cfinvokeargument name="notes_mask" value="#urlNoteMask#"/> 
	<cfinvokeargument name="INPGROUPID" value="#urlGroupId#"/> 
</cfinvoke>

<cfif RetFilterPhone.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Filter phone Error" TYPE="Any" detail="#RetFilterPhone.MESSAGE# - #RetFilterPhone.ERRMESSAGE#" errorcode="-5">                        
</cfif> 

<!-----Get filter email------->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.distribution"
	 method="GetListElligableGroupCount_ByType"
	 returnvariable="RetFilterEmail">
	<cfinvokeargument name="INPBATCHID" value="#urlBatchID#"/>  
	<cfif urlContactType EQ 0>
		<cfinvokeargument name="INPCONTACTTYPE" value="#contactTypeEmail#"/>
	<cfelse>
		<cfinvokeargument name="INPCONTACTTYPE" value="#urlContactType#"/>
	</cfif>
	<cfinvokeargument name="inpDoRules" value="#urlDoRules#"/>    	
	<cfinvokeargument name="inpSourceMask" value="#urlSourceMask#"/> 
	<cfinvokeargument name="type_mask" value="#urlTypeMask#"/> 
	<cfinvokeargument name="MCCONTACT_MASK" value="#urlContactMask#"/> 
	<cfinvokeargument name="notes_mask" value="#urlNoteMask#"/> 
	<cfinvokeargument name="INPGROUPID" value="#urlGroupId#"/>                      
</cfinvoke>
                                        
<cfif RetFilterEmail.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Filter email Error" TYPE="Any" detail="#RetFilterEmail.MESSAGE# - #RetFilterEmail.ERRMESSAGE#" errorcode="-5">                        
</cfif> 

<!--- get filter SMS --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.distribution"
	 method="GetListElligableGroupCount_ByType"
	 returnvariable="RetFilterSMS">
	<cfinvokeargument name="INPBATCHID" value="#urlBatchID#"/>  
	<cfif urlContactType EQ 0>
		<cfinvokeargument name="INPCONTACTTYPE" value="#contactTypeSMS#"/>
	<cfelse>
		<cfinvokeargument name="INPCONTACTTYPE" value="#urlContactType#"/>
	</cfif>
	<cfinvokeargument name="inpDoRules" value="#urlDoRules#"/>    	
	<cfinvokeargument name="inpSourceMask" value="#urlSourceMask#"/> 
	<cfinvokeargument name="type_mask" value="#urlTypeMask#"/> 
	<cfinvokeargument name="MCCONTACT_MASK" value="#urlContactMask#"/> 
	<cfinvokeargument name="notes_mask" value="#urlNoteMask#"/> 
	<cfinvokeargument name="INPGROUPID" value="#urlGroupId#"/>                      
</cfinvoke>
<cfif RetFilterSMS.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Filter SMS Error" TYPE="Any" detail="#RetFilterSMS.MESSAGE# - #RetFilterSMS.ERRMESSAGE#" errorcode="-5">                        
</cfif> 

<!--- get group contact --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.simplelists"
	 method="GetGroupData"
	 returnvariable="RetGroupContact">
	<cfinvokeargument name="inpShowSystemGroups" value="1"/>  
</cfinvoke>
<cfif RetGroupContact.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Get group contact" TYPE="Any" detail="#RetGroupContact.MESSAGE# - #RetGroupContact.ERRMESSAGE#" errorcode="-5">                        
</cfif> 

<!--- Get source data --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.MultiLists"
	 method="GetSourceData"
	 returnvariable="RetSourceData">
	<cfinvokeargument name="inpShowSystemGroups" value="1"/>  
</cfinvoke>
<cfif RetSourceData.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Get source data" TYPE="Any" detail="#RetSourceData.MESSAGE# - #RetSourceData.ERRMESSAGE#" errorcode="-5">                        
</cfif> 



<cfinvoke 
      component="#LocalSessionDotPath#.cfc.distribution"
      method="GetRunningCampaignStatus"
      returnvariable="runningStatusResult">     
     <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
 </cfinvoke> 
<cfset launchAbble = true>
<cfif runningStatusResult.ISRUNNING>
   <cfset launchAbble = false>
   <div style ="color:red">You must stop the campaign in order to make a change!</div>
<cfelse>

</cfIf>
 --->

<!--- get group contact --->
<cfinvoke 
	 component="#LocalSessionDotPath#.cfc.multilists2"
	 method="GetGroupData"
	 returnvariable="RetGroupContact">	  
</cfinvoke>
<cfif RetGroupContact.RXRESULTCODE LT 1>
    <cfthrow MESSAGE="Get group contact" TYPE="Any" detail="#RetGroupContact.MESSAGE# - #RetGroupContact.ERRMESSAGE#" errorcode="-5">                        
</cfif> 

<cfinvoke 
      component="#LocalSessionDotPath#.cfc.distribution"
      method="GetRunningCampaignStatus"
      returnvariable="runningStatusResult">     
     <cfinvokeargument name="INPBATCHID" value="#INPBATCHID#"/>  
 </cfinvoke> 


<!----Display information------->	   
<cfform id="CampaignRecipients" name="CampaignRecipients" action="" method="POST">
	<cfif runningStatusResult.ISRUNNING>
		<div class="campaign_running_error">You must stop the campaign in order to make a change!</div>
    </cfIf>
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label">Launch Campaign</label>
		</div>
		<div class="content_padding">
			<div class="data_row">
				<label class="bold_label left">Total cost to run campaign:</label><span style="padding-left: 5px;"><cfoutput>#lscurrencyFormat(Arraylen(RecipientData)/10)#</cfoutput></span>
			</div>
			<div class="padding_top">
				<div class="contact_type_body">
					<div class="contact_type_label">VOICE</div>
					<div class="contact_type_number_label"><a href="##" id="navVoiceRecipient" value="1" class="nav_recipient_detail"><cfoutput>#VOICECount#</cfoutput></a></div>
				</div>
				<div class="contact_type_body contact_type_body_margin">
					<div class="contact_type_label">EMAIL</div>
					<div class="contact_type_number_label"><a href="##" id="navEmailRecipient" value="2" class="nav_recipient_detail"><cfoutput>#EMAILCount#</cfoutput></a></div>
				</div>
				<div class="contact_type_body contact_type_body_margin">
					<div class="contact_type_label">SMS</div>
					<div class="contact_type_number_label"><a href="##" id="navSMSRecipient" value="3" class="nav_recipient_detail"><cfoutput>#SMSCount#</cfoutput></a></div>
				</div>
			</div>
			<div class="data_row">Select recipients from one of the following:</div>
			<div class="data_row">
				<div class="left"><label class="bold_label left">Contact Group</label></div>
				<div class="left">
					<select id="cbogrouplist" class="left field_margin">
						<cfoutput>
							<cfloop query="RetGroupContact">
								<option value="#RetGroupContact.GROUPID#" <cfif RetGroupContact.GROUPID EQ selectedGroup>selected</cfif>>#HtmleditFormat(RetGroupContact.GROUPNAME)#</option>
							</cfloop>	
						</cfoutput>	
					</select>
					<div class="create_new_group">
						<u><a href="#" onclick="return CreateNewGroup();">Create new group</a></u>
					</div>
				</div>
			</div>
			<div class="large_data_row">
				<div class="left"><label class="bold_label">Contact Type</label></div>
				<div class="contact_type_checkbox">			
					<input type="checkbox" id="chkVoice_ContactType"
						<cfif ListContains(CONTACTTYPES, 1)>
							checked
						</cfif>>
					<label for="chkVoice_ContactType">Voice</label>
				</div>
				<div class="contact_type_checkbox">
					<input type="checkbox" id="chkEmail_ContactType"
						<cfif ListContains(CONTACTTYPES, 2)>
							checked
						</cfif>>
					<label for="chkEmail_ContactType">Email</label>
				</div>
				<div class="contact_type_checkbox">
					<input type="checkbox" id="chkSMS_ContactType"
						<cfif ListContains(CONTACTTYPES, 3)>
							checked
						</cfif>>
					<label for="chkSMS_ContactType">SMS</label>
				</div>
			</div>
			<div class="large_data_row">
				<u><a href="#" onclick="VisibleAdvancedOptions(this)">Show Advanced Options</a></u>
			</div>
			<div id="divAdvancedOptions" style="display: none;">
				<div class="large_data_row">
					<label class="bold_label">Search Contacts Notes</label>
					<input type="text" id="txtSearchNote" value="<cfoutput>#NOTE#</cfoutput>">
				</div>
                <div class="large_data_row">
					<label class="bold_label">Limit Amount Distributed. Warning this will only work if duplicates are not allowed for the batch in advanced batch options.</label>
					
                     <!--- Check for wether duplicates allowed or other BR's here--->                           
                    <cfquery name="getDistinctBatchIdsDupeProcessingFlag" datasource="#Session.DBSourceEBM#">
                        SELECT 
                            AllowDuplicates_ti
                        FROM
                            simpleobjects.batch
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>  
                    
                    <cfif getDistinctBatchIdsDupeProcessingFlag.AllowDuplicates_ti GT 0>
                        <input type="text" id="inpLimitDistribution" value="0" disabled="disabled">
                    <cfelse>
                    	<input type="text" id="inpLimitDistribution" value="0">
                    </cfif>   
                    
				</div>
				<div class="large_data_row">
					<label class="left bold_label">Apply</label>
					<input type="radio" name="IsApply" value = "1" id="rdoApplyYes" class="apply_yesno"
						<cfif APPLYRULE NEQ 0>
							checked
						</cfif>>
					<label for="rdoApplyYes" class="apply_yesno_label">Yes</label>
					<input type="radio" name="IsApply" id="rdoApplyNo" value="0" class="apply_yesno"
						<cfif APPLYRULE EQ 0>
							checked
						</cfif>
					<label for="rdoApplyNo" class="apply_yesno_label">No</label>
				</div>
			</div>
		</div>
		<div class="large_data_row" style="height: 35px;">
			<button class="right" type="button" id="btnCancel">Cancel</button>
			<cfif runningStatusResult.ISRUNNING EQ false AND mode EQ "edit">
				<button class="right" type="button" id="btnLaunchCampaign">Launch Campaign</button>
				<button class="right" type="button" id="btnRefreshFilter">Update</button>
			</cfif>
		</div>
	</div>
</cfform>
<!--- JS-------->
<script type="text/javascript">
	
	$(function(){
		
		$('#subTitleText').text('<cfoutput>#Run_Campaign_Title#</cfoutput>');
		$('#mainTitleText').text('<cfoutput>#Campaign_Title#</cfoutput>');
		
		$('#btnRefreshFilter').click(function(){
			<cfoutput>
				var params = GetParams();
				post_to_url('#rootUrl#/#SessionPath#/campaign/LaunchCampaign', params, 'POST');
			</cfoutput>
		});
		
		$('#btnSave').click(function(){
			UpdateCampaignRecipients();
		});
		
		$('#btnCancel').click(function(){
			document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
		});
		
		if ('<cfoutput>#mode#</cfoutput>' == 'view') {
			$(':input[type != button]').attr('disabled', true);
		}
		
		$(".nav_recipient_detail").click(function() {
			var contactType = '';
			var selectedItem = $(this).attr("value");
			if ($('#chkVoice_ContactType').is(':checked') && selectedItem == "1") {
				contactType += '1,';
			}
			
			if ($('#chkEmail_ContactType').is(':checked') && selectedItem == "2") {
				contactType += '2,';
			}
			
			if ($('#chkSMS_ContactType').is(':checked') && selectedItem == "3") {
				contactType += '3,';
			}
			<cfoutput>
				var params = GetParams(contactType);
				params.page =  'launchCampaign';
				post_to_url('#rootUrl#/#SessionPath#/campaign/mycampaign/RecipientListDetails', params, 'POST');
			</cfoutput>
		});
		
		$('#btnLaunchCampaign').click(function(){
			addgroupToQueueConsoleII();
		});
	});
	
	function VisibleAdvancedOptions(obj) {
		var display = $('#divAdvancedOptions').css('display');
		if (display == 'none') {
			$('#divAdvancedOptions').show();
			$(obj).html("Hide Advanced Options")
		}
		else {
			$('#divAdvancedOptions').hide();
			$(obj).html("Show Advanced Options")
		}
	}
		
	function GetParams(inpContactType){
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		var inpDoRules = $('input[name=IsApply]:checked').val();
		var contactType = '';
		if ($('#chkVoice_ContactType').is(':checked')) {
			contactType += '1,';
		}
		
		if ($('#chkEmail_ContactType').is(':checked')) {
			contactType += '2,';
		}
		
		if ($('#chkSMS_ContactType').is(':checked')) {
			contactType += '3,';
		}
		
		var groupId = $('#cbogrouplist').val();
		var note = $('#txtSearchNote').val();
		
		<!--- change param to post 
		var params = "?inpbatchid=" + batchId + "&APPLYRULE=" + inpDoRules;
		params += "&CONTACTTYPES=" + contactType;
		params += "&SELECTEDGROUP=" + groupId;
		if (inpContactType != undefined) {
			params += "&SELECTEDCONTACTTYPE=" + inpContactType;
		}
		if($.trim(note) != ""){
			params += "&NOTE=" + note;
		} --->
		var params = {};
		params.INPBATCHID = batchId;
		params.APPLYRULE = inpDoRules;
		params.CONTACTTYPES = contactType;
		params.SELECTEDGROUP = groupId;
		
		if (inpContactType != undefined) {
			params.SELECTEDCONTACTTYPE = inpContactType;
		}
		if($.trim(note) != ""){
			params.NOTE = note;
		}
		
		return params;
	}
	
	function GetRecipientsData(){
		var batchId = '<cfoutput>#INPBATCHID#</cfoutput>';
		var groupId = $('#cbogrouplist').val();
		var note = $('#txtSearchNote').val();
		var inpDoRules = $('input[name=IsApply]:checked').val();
		var contactType = '';
		var inpLimitDistribution = $('#inpLimitDistribution').val();
		
		if(parseInt(inpLimitDistribution) >= 0)
			inpLimitDistribution = parseInt(inpLimitDistribution);
		else
			inpLimitDistribution = 0;
		
		if ($('#chkVoice_ContactType').is(':checked')) {
			contactType += '1,';
		}
		
		if ($('#chkEmail_ContactType').is(':checked')) {
			contactType += '2,';
		}
		
		if ($('#chkSMS_ContactType').is(':checked')) {
			contactType += '3,';
		}
		
		
		var data = {};
		data.INPBATCHID = batchId;
		data.InpGroupId = groupId;
		data.ContactTypes = contactType;
		data.Note = note;
		data.IsApplyFilter = inpDoRules;
		data.inpLimitDistribution = inpLimitDistribution;
				
		return data;
	}
	
	function ShowRecipientList() {
		var formRecipientList = $("#tmplPreviewRecipients").tmpl();
		formRecipientList.dialog({
			show: {effect: "fade", duration: 500},
			hide: {effect: "fold", duration: 500},
			modal : true,
			title: 'Communication',
			open: function() {
				$("#QuestionContainer").validate();
			},
			close: function() {  $(this).dialog('destroy'); $(this).remove() },
			width: 600,
			height: 150,
			position: 'top',
			buttons:
				[
					{
						text: "OK",
						click: function(event)
						{
							formRecipientList.remove();	
							return false;
						}
					}
				]
			});	
	}
	function CreateNewGroup() {
		<cfoutput>
			var params = {};
			params.INPBATCHID = '#INPBATCHID#';
			params.selectedGroup = $('##cbogrouplist').val();
			params.page = 'launchCampaign';
			post_to_url('#rootUrl#/#SessionPath#/contacts/addgroup', params, 'POST');
		</cfoutput>
		return false;
	}
		
	function addgroupToQueueConsoleII() {	
		if (parseInt('<cfoutput>#ArrayLen(RecipientData)#</cfoutput>') < 1) {			
			jAlertOK("No elligible contacts to dial as part of currently selected filters.", "Warning");
			return;	
		}
		var data = GetRecipientsData();
				
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc', 'UpdateBatchRecipients', data, "Update Batch Recipients error!", function(d) {
			ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc', 'AddFiltersToQueue', data, "Group has NOT been added to call queue.\n", function(d) {
				AlertScheduleDetails();
			});
		});
		return false;		
	}
		
	function AlertScheduleDetails() {
			$.ajax({
				url : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
				type: "POST",
				dataType: "json",
				data : { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>', 
				}
				
			}).done(function(d){
				var data = d.DATA;
				if (data.RXRESULTCODE[0] == 1) {
					var dataRows = data.ROWS[0];
					if (dataRows.length > 0) {
						var isMultipleAlert = false;
						
						for (var i = dataRows.length - 1; i >= 0; --i) {
							var dataRow = dataRows[i];
							var scheduleType = dataRow.SCHEDULETYPE_INT;
							if (scheduleType == 0) {
								jAlertOK("Filtered list has been added to call queue.\nBased on current system load your messages should go out in approximately two (2) minutes.", "Success!", function(result) {						
									document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
									return;
								});		
							}
							else if (scheduleType == 1) {
								jAlertOK("Your campaign will run on " + dataRow.START_DT_DISPLAY + " at " + GetFullDisplayTime(dataRow.STARTHOUR_TI, dataRow.STARTMINUTE_TI), "Success!", function(result) {						
									document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
									return;
								});
							}
							else if (scheduleType == 2) {
								isMultipleAlert = true;
							}
	
						}
						
						if (isMultipleAlert) {
							AlertLaunchSchedule(dataRows, -1);
						}
					}
				}
				else {
					jAlertOK("Filtered list has been added to call queue.\nBased on current system load your messages should go out in approximately two (2) minutes.", "Success!", function(result) {						
						SaveDefaultSchedule();
						return;
					});		
				}
		});
	}
	
	function AlertLaunchSchedule(dataRows, index) {
		index++;
		if (index == dataRows.length) {
			document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
			return;
		}
		var dataRow = dataRows[index];
		var dayName = GetDayNameBySchedule(dataRow);
		
		var blackoutMessage = " with blackout times of " + GetFullDisplayTime(dataRow.BLACKOUTSTARTHOUR_TI, 0) + " - " + GetFullDisplayTime(dataRow.BLACKOUTENDHOUR_TI, 0)
		if (dataRows.length == 1 & GetDayNameBySchedule(dataRow) == "All") {
			var message = "Your campaign will run between " + dataRow.START_DT_DISPLAY + " - " + dataRow.STOP_DT_DISPLAY + 
						" from  " + GetFullDisplayTime(dataRow.STARTHOUR_TI, dataRow.STARTMINUTE_TI) + " - " + GetFullDisplayTime(dataRow.ENDHOUR_TI, dataRow.ENDMINUTE_TI);
			if (dataRow.ENABLEDBLACKOUT_BI) {
				message += blackoutMessage;
			}
			jAlertOK(message, "Success!", function(result) {						
				document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
				return;
			});
		}
		else {
			var message = "Your campaign will run between " + dataRow.START_DT_DISPLAY + " - " + dataRow.STOP_DT_DISPLAY + ". On " + dayName + 
						" it will run between " + GetFullDisplayTime(dataRow.STARTHOUR_TI, dataRow.STARTMINUTE_TI) + " - " + GetFullDisplayTime(dataRow.ENDHOUR_TI, dataRow.ENDMINUTE_TI);
			if (dataRow.ENABLEDBLACKOUT_BI) {
				message += blackoutMessage;
			}			
			jAlertOK(message, "Success!", function(result) {
				AlertLaunchSchedule(dataRows, index);
			});	
		}
	}
	
	function SaveDefaultSchedule() {
		var data = new Array();
		var obj = new Object();
		obj.scheduleType = 0;
		obj.startDate = null;
		obj.stopDate = null;
		obj.startHour = 0;
		obj.startMinute = 0;
		obj.endHour = 0;
		obj.endMinute = 0;	
		obj.enabledBlackout = true;
		obj.blackoutStartHour = 0;				
		obj.blackoutEndHour = 0;
		obj.dayId = 0;
		data.push(obj);
		data = { 
					INPBATCHID : '<cfoutput>#INPBATCHID#</cfoutput>', 
					LOOPLIMIT_INT : 100, 
					SCHEDULETIME: JSON.stringify(data)
				};
		ServerProxy.PostToServer('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc', 'UpdateSchedule', data, "Default schedule has NOT saved", function(d) {
			document.location.href = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/listcampaigns';
		});
	}
	function GetNoon(hour) {
		return Math.floor(parseInt(hour) / 12) == 0 ? "AM" : "PM";
	}
	
	function GetDisplayTime(value) {
		value = parseInt(value);
		return value < 10 ? "0" + value : value;
	}	
	
	function GetFullDisplayTime(hour, minute) {
		var hourValue = parseInt(hour) % 12;
		return GetDisplayTime(hourValue) + ":" + GetDisplayTime(minute) + " " + GetNoon(hour);
	}
	function GetDayNameBySchedule(schedule) {
		var result = -1;
		var count = 0;
		if (schedule.SUNDAY_TI == 1) {
			count++;
			result = "Sunday";
		}
		if (schedule.MONDAY_TI == 1) {
			count++;
			result = "Monday";
		}
		if (schedule.TUESDAY_TI == 1) {
			count++;
			result = "Tuesday";
		}
		if (schedule.WEDNESDAY_TI == 1) {
			count++;
			result ="Wednesday";
		}
		if (schedule.THURSDAY_TI == 1) {
			count++;
			result = "Thursday";
		}
		if (schedule.FRIDAY_TI == 1) {
			count++;
			result = "Friday";
		}
		if (schedule.SATURDAY_TI == 1) {
			count++;
			result = "Saturday";
		}
		if (count == 7) {
			return "All"
		}
		
		return result;
	}
</script>

<script id="tmplPreviewRecipients" type="text/x-jquery-tmpl">
	<div class="table_body">
		<div class="table_header">
			<label class="table_header_label">Campaign Recipients</label>
		</div>
		<div class="right">
			3 Email Recipients
		</div>
		<div>
			<div style="width: 50%; float: left">Email Address</div>
			<div style="width: 50%; float: right">Note</div>
		</div>
	</div>
</script>