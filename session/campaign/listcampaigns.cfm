<cfparam name="isDialog" default="0">
<cfparam name="query" default="">
<cfparam name="inpSkipDT" default="0">

<cfparam name="inpContainerType" default="0"> <!--- pass on to list look up - hint="if 1 filter on only batches that contain <RXSS> IVR content" --->


<cfset isReportPicker="1">

<cfset permissionObject = CreateObject("component", "#LocalSessionDotPath#.cfc.administrator.permission")>

<cfset campaignPermission = permissionObject.havePermission(Campaign_Title)>
<cfset campaignEditPermission = permissionObject.havePermission(edit_Campaign_Title)>
<cfset campaignSchedulePermission = permissionObject.havePermission(Campaign_Schedule_Title)>
<cfset campaignRunPermission = permissionObject.havePermission(Run_Campaign_Title)>
<cfset campaignStopPermission = permissionObject.havePermission(Stop_Campaign_Title)>
<cfset campaignCancelPermission = permissionObject.havePermission(Cancel_Campaign_Title)>
<cfset campaignDeletePermission = permissionObject.havePermission(delete_Campaign_Title)>
<cfset campaignCIDPermission = permissionObject.havePermission(Caller_ID_Title)>
<cfset ReportingPermission = permissionObject.havePermission(Reporting_Campaigns_Title)>
<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
<cfset runCampaignWithoutRecipients = "You must add recipients before running a campaign">
<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign.">
<cfif NOT campaignPermission.havePermission>
	<cfset session.permissionError = campaignPermission.message>
	<cflocation url="#rootUrl#/#sessionPath#/account/home">
</cfif>

<style type="text/css">
	#ListCampaignContainer #SelectContactPopUp {
		margin: 15px;
	}
	#ListCampaignContainer #tblListBatchSelect_info {
		width:98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
		
	}
	#ListCampaignContainer .ui-dialog{
		padding-bottom:10px;
	}
	#ListCampaignContainer .loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		#ListCampaignContainer .save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		#ListCampaignContainer .ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		#ListCampaignContainer .ui-tabs-active a{
			color:#0888d1 !important;
		}
		#ListCampaignContainer .ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		#ListCampaignContainer .ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		#ListCampaignContainer .ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#ListCampaignContainer #emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		#ListCampaignContainer .ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		#ListCampaignContainer .ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		#ListCampaignContainer .ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		#ListCampaignContainer .ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			/*font-family:Verdana;*/
		}
		#ListCampaignContainer .ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		#ListCampaignContainer .ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#ListCampaignContainer #tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#ListCampaignContainer #tabs .ui-tabs-nav li{
			width: 90px;
		}
		#ListCampaignContainer #tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		#ListCampaignContainer .header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    font-family: Verdana;
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 100%;
			text-indent:10px;
		}
		#ListCampaignContainer .datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		#ListCampaignContainer table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		#ListCampaignContainer table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		#ListCampaignContainer .border_top_none{
			border-radius: 0 0;
		}
		#ListCampaignContainer .m_top_20{
			margin-top:20px;
		}
		#ListCampaignContainer .m_top_10{
			margin-top:10px;
		}
		#ListCampaignContainer .m_left_20{
			margin-left:20px;
		}
		#ListCampaignContainer .m_left_0{
			margin-left:0px;
		}
		#ListCampaignContainer .statistic-bold{
			color:#0888d1;
			/*font-family:Verdana;*/
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:100%;
			text-align:center;
			border-spacing:10px 0px;
		}
		#ListCampaignContainer .footer-text{
			color:#666666;
			/*font-family:Verdana;*/
			font-size:14px;
		}
		#ListCampaignContainer .none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		#ListCampaignContainer .none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		#ListCampaignContainer .spacing_10{
			border-spacing:10px 0px;
		}
		#ListCampaignContainer #ActiveSessionTbl{
			width:100%;
			margin-top:0px;
		}
		#ListCampaignContainer tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		#ListCampaignContainer table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		#ListCampaignContainer .datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		#ListCampaignContainer .dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: none !important; /*3px 3px 5px #888888;  0px 3px 5px #888*/
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
		
		#ListCampaignContainer .paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}
		
		#ListCampaignContainer .dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		#ListCampaignContainer .paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		#ListCampaignContainer a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		#ListCampaignContainer .hide{
			display:none;
		}
		#ListCampaignContainer .avatar{
			padding: 0px 8px 0px 10px !important;
		}
		#ListCampaignContainer .clear-fix{
			clear:both;
		}
		
		#ListCampaignContainer table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		#ListCampaignContainer table.dataTable tr.odd,
		#ListCampaignContainer table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		#ListCampaignContainer .wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			font-family:Verdana Verdana, Geneva, sans-serif;

		}
		#ListCampaignContainer .wrapper-picker-container{
			 margin-left: 20px;
		} 
		#ListCampaignContainer .tooltiptext{
		    display: none;
		}
		#ListCampaignContainer .showToolTip{
			cursor:pointer;
		}
		#ListCampaignContainer .deliverymethods{
			margin-bottom:50px;
		}
		
		#ListCampaignContainer #ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ListCampaignContainer #ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ListCampaignContainer #ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ListCampaignContainer #ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ListCampaignContainer .TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#ListCampaignContainer #jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		#ListCampaignContainer a.select_script{
			text-decoration: none !important;
		}
		
		#ListCampaignContainer a.select_script:HOVER{
			text-decoration: underline !important;
		}

		#ListCampaignContainer #inpAvailableShortCode-button
		{		
			outline:none;		
		}
		
		#ListCampaignContainer table.dataTable thead tr th{
			border-bottom: 3px solid #FA7D29;
		}
		
		#ListCampaignContainer table.dataTable tr td{
			color:#5E6AAD;
		}
		
		#ListCampaignContainer table { border-spacing: 0; }
				
</style>


<style type="text/css">
	#ListCampaignContainer .filter-btn{
		border: 1px solid #0888D1;
	    /*font-family: verdana;*/
		height:22px;
	    color: #FFFFFF !important;
		margin-left:10px;
	 	background-color: #006DCC;
	    background-image: linear-gradient(to bottom, #4AA5D9, #4AA5D9);
	    background-repeat: repeat-x;
	    color: #FFFFFF;
	    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
		border-radius: 4px;
	    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 0;
	    padding: 4px 12px;
	    text-align: center;
	    vertical-align: middle;
	}
	#ListCampaignContainer .filter-btn:hover{   
		background: -moz-linear-gradient(center top , #0095CC, #00678E) repeat scroll 0 0 rgba(0, 0, 0, 0);
	}
	#ListCampaignContainer .btn{
		padding:0px;
	}
	#ListCampaignContainer .datatable_filter_row{
		margin-top:8px;
	}
	#ListCampaignContainer .btn-primary{
		height: 22px;
	}
	#ListCampaignContainer input.filter_val {
	    border: 1px solid #CCCCCC;
	    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
	    transition: border 0.2s linear 0s, box-shadow 0.2s linear 0s;
		background-color: #F5F5F5;
	    border-radius: 4px;
	    color: #555555;
	    display: inline-block;
	    font-size: 14px;
	    line-height: 20px;
	    margin-bottom: 10px;
	    padding: 4px 6px;
	    vertical-align: middle;
	}
	#ListCampaignContainer input.filter_val:hover {
		border-color:rgba(82, 168, 263, 0.8);
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(82, 168, 236, 0.6);
	    outline: 0 none;
	}
	
	
	#ListCampaignContainer .ui-datepicker .ui-widget-content{
		width:200px;
	}
		
	#ListCampaignContainer .ui-custom-css{
		margin-left:-2px!important;
	}
</style>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Select the campaign you wish to use content from</h4>     
</div>

<div class="modal-body">
    
    <!--- Surround in own styled div so styles dont apply to parent objects when this is used as a picker --->
    <div id="ListCampaignContainer">
        
        <!---this is custom filter for datatable--->
        <div id="filter">
            <cfoutput>
            <!---set up column --->
            <cfset datatable_ColumnModel = arrayNew(1)>
            <!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Status', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ''})>--->
            <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Campaign name', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'b.DESC_VCH'})>
            <cfif session.userrole EQ 'SuperUser'>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Owner ID', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'b.UserId_int'})>
                <cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Company ID', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'c.CompanyName_vch'})>
            </cfif>
            <!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Options', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ''})>--->
            <!---we must define javascript function name to be called from filter later--->
            <cfset datatable_jsCallback = "InitBatchSelect">
            <cfset datatable_showFilterDashboard = "true">
            <cfinclude template="/session/ems/datatable_filter.cfm" >
            </cfoutput>
        </div>
        <div class="border_top_none">
            <table id="tblListBatchSelect" class="table" cellspacing=0 width="100%">
            </table>
        </div>
    
    </div>
    
</div>

<div class="modal-footer">   
    <div class="sms_popup_action">
    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    
    </div>	
</div>   
    
    
<script>
	var _tblListBatchSelect;
	//init datatable for active agent
	function InitBatchSelect(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListBatchSelect = $('#tblListBatchSelect').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				<!---{"sName": 'List Id', "sTitle": 'List Id', "sWidth": '8%',"bSortable": false},--->
				<!---{"sName": 'Status', "sTitle": 'Status', "sWidth": '20%',"bSortable": false,"bVisible": false},--->
				{"sName": 'Campaign', "sTitle": 'Campaign', "sWidth": '36%',"bSortable": true},
				<cfoutput >
					<cfif session.userrole EQ 'SuperUser'>
						{"sName": 'Owner ID', "sTitle": 'Owner ID', "sWidth": '12%',"bSortable": true},
						{"sName": 'Company ID', "sTitle": 'Company ID', "sWidth": '12%',"bSortable": true},
					</cfif>
				</cfoutput>
				
				<!---{"sName": 'Email', "sTitle": 'Email', "sWidth": '12%',"bSortable": true}--->
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetCampaignsListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData},
		            { "name": "isDialog", "value": <cfoutput>#isDialog#</cfoutput>}	,
					{ "name": "inpContainerType", "value": '<cfoutput>#inpContainerType#</cfoutput>'}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListBatchSelect').attr('style', '');
				<cfif isDialog neq 0>
					$('#coverfilterload').show();
					$('#coverfilterload').css("padding","0px")
				</cfif>				
			}
	    });
	}
	
	<cfif isDialog EQ 1>
		function SelectCampaign(INPBATCHID, INPDESC)
		{
						
			<!--- Set hidden variables and then Close dialog --->
			$TargetBatchId.val(INPBATCHID);
			$TargetBatchDesc.html(INPDESC);
			<!---$dialogCampaignPicker.dialog('close');--->
					
			return false;
		}
	</cfif>
</script>

<script>
	$(function() {
	
		InitBatchSelect();
		
		
	});

		

	
	
	
		
 
</script>
