<cfoutput>
	<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/datatables-1.9.4/media/js/jquery.datatables.min.js"></script>	
	<link rel="stylesheet" type="text/css" href="#rootUrl#/#publicPath#/js/datatables-1.9.4/media/css/jquery.datatables.css">
</cfoutput>

<!--- Calculate for form refresh - if not already defined in Filters --->
<cfparam name="FormParams" default="">


<cfif FormParams EQ "">
    
    <cfset StartCommaFlag = 0>
    
    <CFLOOP collection="#FORM#" item="whichField">
    
        <cfif StartCommaFlag GT 0>
        
        	<cfif COMPARE(LEFT(TRIM(FORM[whichField]),2), '"[') EQ 0 >
            	<cfset FormParams = FormParams & ",#whichField#:#serializeJSON(FORM[whichField])#">
            <cfelse>
            	<cfset FormParams = FormParams & ",#whichField#:'#FORM[whichField]#'">
            </cfif>
             
        <cfelse>
	        <cfif COMPARE(LEFT(TRIM(FORM[whichField]),2), '"[') EQ 0 >
    	        <cfset FormParams = "#whichField#:#serializeJSON(FORM[whichField])#">
            <cfelse>
            	<cfset FormParams = "#whichField#:'#FORM[whichField]#'">
            </cfif>
                
            <cfset StartCommaFlag = 1>
        </cfif>        
    </CFLOOP>
  
  	<!---<cfset FormParams = URLDECODE(FormParams) />--->
	<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
	<cfset runCampaignWithoutRecipients = "You must add recipients before running a campaign">
	<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
	<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign.">
</cfif>

<style type="text/css">
	#SelectContactPopUp {
		margin: 15px;
	}
	#tblListGroupContact_info {
		width: 98%;
		padding-left: 2%;
		margin-left:0px;
		height:auto;
	}
	.ui-dialog{
		padding-bottom:10px;
	}
	.loading-data{
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}
		
		.save-process{
			display: none;
		    height: 300px;
		    left: 40%;
		    position: fixed;
		    top: 30%;
		    width: 300px;
			background: url('<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading.gif') ;
			background-repeat: no-repeat;
			background-position: center;
		}

		.ui-tabs-active{
			background-color:#FFFFFF !important;
			color:#fff !important;
		}
		
		.ui-tabs-active a{
			color:#0888d1 !important;
		}
		.ui-widget-content{
			border: 1px solid #AAAAAA;
			/*width: 700px;*/
		}
		.ui-tabs .ui-tabs-nav{
			background: #DDDDDD;	
		}
		.ui-tabs .ui-tabs-panel{
			border:0px;
			/*min-height:300px;*/
			height:auto;
			padding-top:40px;
			padding-left:10px;
		}
		
		#emsRecorder{
			border: 1px solid #CCCCCC;
		    border-radius: 4px;
		    display: block;
		    margin: 16px 20px 0;
		}
		.ui-tabs-nav{
			height: 38px;
			border-left-width: 0;
		    border-right-width: 0;
		}
		.ui-tabs .ui-tabs-nav li{
		 	bottom: 0;
		    height: 30px;
		    margin-bottom: 0;
		    margin-left: 5px;
		    margin-top: 9px;
		    top: 0;
		    width: 170px;	
			background: #f0f0f0;
		}
		.ui-tabs .ui-tabs-nav li.ui-state-default{
			border-bottom:0px;
		}
		.ui-tabs .ui-tabs-nav li a{
			padding: 0;
			line-height:30px;
		    width: 168px;
			text-align:center;
			font-size:14px;
			font-weight:bold;
			color:#666666;
			/*font-family:Verdana;*/
		}
		.ui-tabs-nav a,ui-tabs-nav a:link
		{
			height: 28px;
			color:#666666;	
			border-bottom:1px solid #AAA;
			border-left:1px solid #AAA;
			border-top:1px solid #AAA;
			border-right:1px solid #AAA;
			border-radius:5px 5px 0 0;	
		}
		.ui-tabs-nav .ui-tabs-active a{
			border-bottom:0px !important;
		}
		#tabs .ui-tabs-nav li a{
			width: 88px;
		}
		#tabs .ui-tabs-nav li{
			width: 90px;
		}
		#tabs .ui-tabs .ui-tabs-panel{
			padding-left:0px;
		}
		.header-bar{
			background-color: #0888D1;
		    border: 1px solid #0E88CD;
		    border-radius: 5px 5px 0 0;
		    color: #FFFFFF;
		    /*font-family: Verdana;*/
		    font-size: 14px;
		    font-weight: bold;
		    height: 30px;
		    line-height: 30px;
		    width: 700px;
			text-indent:10px;
		}
		.datatables_wrapper{
			width:96%;
			margin:0 auto;
		}
		table.dataTable td{
			border-right: 1px solid #CCC;
			font-size:13px;
			color:#666;
		}
		
		table.dataTable th{
			border-right: 1px solid #CCC;
		}
		
		.border_top_none{
			border-radius: 0 0;
		}
		.m_top_20{
			margin-top:20px;
		}
		.m_top_10{
			margin-top:10px;
		}
		.m_left_20{
			margin-left:20px;
		}
		.m_left_0{
			margin-left:0px;
		}
		.statistic-bold{
			color:#0888d1;
			/*font-family:Verdana;*/
			font-size:48px;
			word-wrap:break-word;
			table-layout:fixed;
			width:680px;
			text-align:center;
			border-spacing:10px 0px;
		}
		.footer-text{
			color:#666666;
			/*font-family:Verdana;*/
			font-size:14px;
		}
		.none-border-bottom td{
			border-top:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-bottom:0px none;
			background-color:#f0f0f0;
		}
		.none-border-top th{
			border-bottom:1px solid #cccccc;
			border-right:1px solid #cccccc;
			border-left:1px solid #cccccc;
			border-top:0px none;
			background-color:#f0f0f0;
			height:75px;
		}
		.spacing_10{
			border-spacing:10px 0px;
		}
		#ActiveSessionTbl{
			width:680px;
			margin-top:0px;
		}
		tr.tr-Session:hover td{
			cursor:pointer;
			background: yellow;
		}
		table.dataTable thead tr {
		    background-color: #0888D1;
		    border-bottom: 2px solid #FA7D29;
		    color: #FFFFFF;
		    height: 23px;
		    line-height: 23px;
		}
		.datatables_paginate {
		    background-color: #EEEEEE;
		    border-radius: 0 0 3px 3px;
		    border-top: 2px solid #0888D1;
		    box-shadow: 3px 4px 3px -3px rgba(0, 0, 0, 0.5);
		    float: right;
		    height: 33px !important;
		    padding-top: 10px;
		    text-align: right;
		    width: 100%;
		}
		.dataTables_info {
			background-color: #EEEEEE;
		    border-top: 2px solid #46A6DD;
		    box-shadow: 3px 3px 5px #888888;
		    line-height: 39px;
			position: relative;
		    top: 0 !important;
			clear: both;
		    float: left;
		    padding-top: 3px;
		}
		
		.paging_full_numbers {
		    height: 22px;
		    line-height: 22px;
		    margin-top: 10px;
		    position: absolute;
		    right: 30px;
		}
		
		.dataTables_paginate {
		    float: right;
		    text-align: right;
		}
		.paginate_button {
		    background-color: #46A6DD !important;
		    border: 1px solid #0888D1 !important;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    padding: 3px 8px !important;
		}
		a.paginate_active {
		    background-color: #0888D1 !important;
		    border: 1px solid #0888D1 !important;
		    border-radius: 5px;
		    box-shadow: 0 1px 0 #2AA3E9 inset;
		    color: #FFFFFF !important;
		    cursor: pointer;
		    padding: 3px 8px !important;
		}
		.hide{
			display:none;
		}
		.avatar{
			padding: 0px 8px 0px 10px !important;
		}
		.clear-fix{
			clear:both;
		}
		
		table.dataTable tr.even td.sorting_1 {
		    background-color: #FFF!important;
		}
		
		table.dataTable tr.odd,
		table.dataTable tr.odd td.sorting_1{
		    background-color: #f3f9fd!important;
		}
		.wrapper-dropdown-picker {
		    background: none repeat scroll 0 0 #FFFFFF;
		    border: 1px solid rgba(0, 0, 0, 0.3);
		    border-radius: 3px;
		    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1) inset, 0 1px 0 0 rgba(250, 250, 250, 0.5);
		    color: #666666;
		    cursor: pointer;
		    height: 21px;
		    margin-top: 0;
		    position: relative;
		    width: 300px;
			padding:5px 10px 0 9px;
			font-size: 12px;
			/*font-family:Verdana Verdana, Geneva, sans-serif;*/
				
		}
		.wrapper-picker-container{
			 margin-left: 20px;
		} 
		.tooltiptext{
		    display: none;
		}
		.showToolTip{
			cursor:pointer;
		}
		.deliverymethods{
			margin-bottom:50px;
		}
		
		#ScheduleBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScheduleBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/date_picker.jpg) 0 0; width: 20px; height: 20px; cursor:pointer;
		}
		
		#ScriptPickerBtn.active
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		#ScriptPickerBtn.inactive
		{
			/* background: url(images/redo20n_inactive.png); */
			background: url(<cfoutput>#rootUrl#/#publicPath#/css/</cfoutput>images/music-icon_24x24.png) 0 0; width: 24px; height: 24px; cursor:pointer;
		}
		
		.TextLinkHoverStyle:hover
		{
			text-decoration:underline;
			cursor:pointer;
		}
		
		#jstree_demo_div .jstree_no_icon{
			display: none;
		}
		
		a.select_script{
			text-decoration: none !important;
		}
		
		a.select_script:HOVER{
			text-decoration: underline !important;
		}
		
		

		#inpAvailableShortCode-button
		{		
			outline:none;		
		}
</style>
<!---this is custom filter for datatable--->
<div id="filter">
	<cfscript>
		strTotalFilter = "( SELECT COUNT(*) FROM simplelists.rxmultilist r WHERE r.grouplist_vch LIKE CONCAT('%,'" ;
		strTotalFilter =  strTotalFilter & " , g.GROUPID_BI, ',%')"; 
		if(session.userRole NEQ 'SuperUser'){
			strTotalFilter = strTotalFilter & " AND r.UserId_int = #Session.USERID#";
		}
		strTotalFilterPhone = strTotalFilter & " AND r.ContactTypeId_int=1 )";
		strTotalFilterEmail = strTotalFilter & " AND r.ContactTypeId_int=2 )";				
		strTotalFilterSMS = strTotalFilter & " AND r.ContactTypeId_int=3 )";
	</cfscript>
	<cfoutput>
	<!---set up column --->
	<cfset datatable_ColumnModel = arrayNew(1)>
	<!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Status', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ''})>--->
	<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Campaign', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = 'b.DESC_VCH'})>
	<cfif session.userrole EQ 'SuperUser'>
		<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Owner ID', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'b.UserId_int'})>
		<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Company ID', CF_SQL_TYPE = 'CF_SQL_INTEGER', TYPE='INTEGER', SQL_FIELD_NAME = 'c.CompanyName_vch'})>
	</cfif>
	<!---<cfset arrayappend(datatable_ColumnModel,{DISPLAY_NAME = 'Options', CF_SQL_TYPE = 'CF_SQL_VARCHAR', TYPE='TEXT', SQL_FIELD_NAME = ''})>--->
	<!---we must define javascript function name to be called from filter later--->
	<cfset datatable_jsCallback = "InitGroupContact">
	<cfinclude template="datatable_filter.cfm" >
	</cfoutput>
</div>
<div class="border_top_none">
	<table id="tblListGroupContact" class="table">
	</table>
</div>
<script>
	var _tblListGroupContact;
	//init datatable for active agent
	function InitGroupContact(customFilterObj){
		var customFilterData =  typeof(customFilterObj)!='undefined'?JSON.stringify(customFilterObj):"";
		_tblListGroupContact = $('#tblListGroupContact').dataTable( {
		    "bProcessing": true,
			"bFilter": false,
			"bServerSide":true,
			"bDestroy":true,<!---this attribute must be explicit, or custom filter will not work properly  --->
			"sPaginationType": "full_numbers",
		    "bLengthChange": false,
			"iDisplayLength": 10,
		    "aoColumns": [
				<!---{"sName": 'List Id', "sTitle": 'List Id', "sWidth": '8%',"bSortable": false},--->
				{"sName": 'Status', "sTitle": 'Status', "sWidth": '20%',"bSortable": false},
				{"sName": 'Campaign', "sTitle": 'Campaign', "sWidth": '36%',"bSortable": true},
				<cfoutput >
					<cfif session.userrole EQ 'SuperUser'>
						{"sName": 'Owner ID', "sTitle": 'Owner ID', "sWidth": '12%',"bSortable": true},
						{"sName": 'Company ID', "sTitle": 'Company ID', "sWidth": '12%',"bSortable": true},
					</cfif>
				</cfoutput>
				
				<!---{"sName": 'Email', "sTitle": 'Email', "sWidth": '12%',"bSortable": true}--->
				{"sName": 'Options', "sTitle": 'Options', "sWidth": '20%',"bSortable": false}
			],
			"sAjaxSource": '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/multilists2.cfc?method=GetCampaignsListForDatatable&returnformat=plain&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
			"fnServerData": function ( sSource, aoData, fnCallback ) {
		       aoData.push(
		            { "name": "customFilter", "value": customFilterData}
	            );
		        $.ajax({dataType: 'json',
		                 type: "POST",
		                 url: sSource,
			             data: aoData,
			             success: fnCallback
			 	});
	        },
			"fnInitComplete":function(oSettings, json){
				//in order to prevent this talbe from collapsing, we must fix its min width 
				$('#tblListGroupContact').attr('style', '');
				$('#tblListGroupContact').css('min-width', '820px');
			}
	    });
	}
	InitGroupContact();
	
	$(function() {
		$(".del_RowMCContent").click(function() {
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;Cancel&nbsp;';		
			
			var CurrREL = $(this).attr('rel');
			var currPage =  $(this).attr('pageRedirect');
			jConfirm( "Are you sure you want to delete this Campaign?", "Delete Campaign", function(result) { 
				if(result)
				{	
					delBatch(CurrREL,currPage);
				}
				return false;																	
			});
		});
		
		$('.Cancel_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
			
			jConfirm( "Are you sure you want to cancel this Campaign?", "Cancel Campaign", function(result) { 
				if(result)
				{	
					cancelBatch(batchId,currPage);
				}
				return false;																	
			});
		});
		
		$('.Stop_RowBatch').click(function(){
			$.alerts.okButton = '&nbsp;Yes&nbsp;';
			$.alerts.cancelButton = '&nbsp;No&nbsp;';		
			var batchId = $(this).attr('rel');
			var currPage = $(this).attr('pageRedirect');
				
			jConfirm( "Are you sure you want to stop this Campaign?", "Stop Campaign", function(result) { 
				if(result)
				{	
					stopBatch(batchId,currPage);
				}
				return false;																	
			});
		});
		
		$('.changeCID').click(function(){
			var inpBatchID = parseInt($(this).attr('rel'));
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=getXMLCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					INPBATCHID : inpBatchID, 
				},					  
				error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
				success:function(d){
					if(d.DATA.RXRESULTCODE[0] > 0){
						$('#callerId').val(d.DATA.CID[0]);
						$('#callerCampaignId').val(d.DATA.INPBATCHID[0]);
						OpenDialog('cid');
					}else{
						jAlert(d.DATA.MESSAGE[0], 'Warning');
					}
				} 		
			});
			
		})
	});
	
	function updateCid(){
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateXMLCallerId&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { 
				INPBATCHID : $('#callerCampaignId').val(), 
				callerId : $('#callerId').val()
			},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:function(d){
				if(d.DATA.RXRESULTCODE[0] > 0){
					jConfirm( "Set Caller Id successfully", " Success!", function(result) { 
						location.reload();						
					});
				}else{
					jAlert(d.DATA.MESSAGE[0], 'Warning');
				}
			} 		
		});
	}
	
	function ViewProgress(INPBATCHID, linkObj){
		var data = { 
	     	INPBATCHID : INPBATCHID
	    };
	    		
		ServerProxy.PostToServerStruct('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc', 'GetStatusCampaign', data, "View campaign fail.", function(d ) {
			var total = d.TOTAL_COUNT;

			var totalPending = d.TOTAL_PENDING;
			var totalInProgress = d.TOTAL_IN_PROGRESS;
			var totalComplete = d.TOTAL_COMPLETE;
			
			var percentPending = d.PERCENT_PENDING;
			var percentInProgress = d.PERCENT_IN_PROGRESS;
			var percentComplete = d.PERCENT_COMPLETE;
			
			var strContent = percentComplete + '% of your campaign has been delivered. ('+ totalComplete +' of '+ total +') <br />';
			strContent = strContent + percentInProgress + '% of your campaign is in progress for delivery. ('+ totalInProgress  +' of '+ total+') <br />';
			strContent = strContent + percentPending + '% of your campaign is pending for delivery. ('+ totalPending +' of '+ total+') ';
			var offset = $(linkObj).offset();	
			var top = offset.top + 15;
			var left = offset.left;	
			jAlert( 
				strContent, 
				'Campaign Progress', 
				function(result) { },
				top,
				left
			);
    	});	
		return false;
	}
	
	function RunCampaign(INPBATCHID) {
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetBatchRecipients&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { INPBATCHID : INPBATCHID},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			function(d) {
				if (d.DATA.RXRESULTCODE[0] == 1) {
					var selectedGroup = d.DATA.DATA[0].CONTACTGROUPID_INT;
					var CONTACTTYPES = d.DATA.DATA[0].CONTACTTYPES_VCH;
					var	NOTE = d.DATA.DATA[0].CONTACTNOTE_VCH
					var APPLYRULE = d.DATA.DATA[0].CONTACTISAPPLYFILTER;
					$.ajax({
				  	type: "POST",
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRecipientList&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
					data: {
							INPBATCHID: INPBATCHID,
							CONTACTTYPES: CONTACTTYPES,
							NOTE: NOTE,
							APPLYRULE: APPLYRULE,
							INPGROUPID: selectedGroup
						},
					error: function(XMLHttpRequest, textStatus, errorThrown) {jAlertOK("Error.", "Bad Response from the remote server. Check your connection and try again."); <!---console.log(textStatus, errorThrown);--->},					  
				  	success:
					function(d2, textStatus, xhr ) {
							var recipientData = eval('(' + xhr.responseText + ')');
							if (recipientData.DATA.RXRESULTCODE[0] > 0 && recipientData.DATA.DATA[0].length > 0) {
								$.ajax({
									url : '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/schedule.cfc?method=GetSchedule&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',
									type: "POST",
									dataType: "json",
									data : { 
										INPBATCHID : INPBATCHID
									}
									
								}).done(function(d){
									var isConfirmRunCampaign = false;
									var data = d.DATA;
									if (data.RXRESULTCODE[0] == 1) {
										var dataRows = data.ROWS[0];
										if (dataRows.length > 0) {
											if (parseFloat('<cfoutput>#RetValBillingData.Balance#</cfoutput>') > parseFloat(recipientData.DATA.DATA[0].length / 10)) {
												window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/launchcampaign?inpbatchid=" + INPBATCHID;
											}
											else {
												jAlert('<cfoutput>#runCampaignNotEnoughFund#</cfoutput>', "", function(result) { } );
											}
											
										}
										else {
											isConfirmRunCampaign = true;
										}
									}
									else {
										isConfirmRunCampaign = true;
									}
									
									if (isConfirmRunCampaign) {
										$.alerts.okButton = 'yes, run campaign';
										$.alerts.cancelButton = 'cancel';

										jConfirm('<cfoutput>#runCampaignWithoutSchedule#</cfoutput>', "", function(result) { 
											if (result) {
												if (parseFloat('<cfoutput>#RetValBillingData.Balance#</cfoutput>') > parseFloat(recipientData.DATA.DATA[0].length / 10)) {
													window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/launchcampaign?inpbatchid=" + INPBATCHID;
												}
												else {
													jAlert('<cfoutput>#runCampaignNotEnoughFund#</cfoutput>', "", function(result) { } );
												}
											}
										}, $.alerts);

									}
									
								});
							}
							else {
								$.alerts.okButton = 'Add';
								$.alerts.cancelButton = 'Cancel';
								jConfirm('<cfoutput>#runCampaignWithoutRecipients#</cfoutput>', "Error", function(result) {
									if (result) {
										window.location = "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid=" + INPBATCHID + "&mode=edit";
									}
								});
							}
						}				
					});		
				}
				<!--- 
				<cfset setScheduleMessage = "You must stop the campaign before making any changes.">
<cfset runCampaignWithoutRecipients = "You have not selected any recipients for this campaign.">
<cfset runCampaignWithoutSchedule = "You have not set the schedule for this campaign, would you like to run now?">
<cfset runCampaignNotEnoughFund = "You do not have enough funds to run this campaign."> --->
			}
		});
	}
	function delBatch(INPBATCHID,currPage)
	{
		$.ajax({
		type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
		url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=UpdateBatchStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
		dataType: 'json',
		data:  { INPBATCHID : INPBATCHID},					  
		error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
		success:		
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{						
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
							
							if(CurrRXResultCode > 0)
							{
								reloadPage();
								return false;
									
									
							}
							else
							{
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								if (d.DATA.ERRMESSAGE[0] != '') {
									jAlert("Campaign has NOT been deleted.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );						
								}
								else {
									jAlert(d.DATA.MESSAGE[0], "Failure!", function(result) { } );
								}
							}
						}
					}
					else
					{<!--- No result returned --->		
						$.alerts.okButton = '&nbsp;OK&nbsp;';					
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			} 		
		});
		return false;
	}
	
	function cancelBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=CancelBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { } );
									reloadPage();
									return false;
										
								}
								else
								{
									if (d.DATA.MESSAGE[0] != "") {
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("Campaign has NOT been cancelled.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
									}
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	
	function stopBatch(INPBATCHID,currPage)
	{
			$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=StopBatch&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					<!--- Alert if failure --->
																								
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{						
							<!--- Check if variable is part of JSON result string --->								
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{							
								CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
								
								if(CurrRXResultCode > 0)
								{
									jAlert(d.DATA.MESSAGE[0], "Success!", function(result) { 
										location.reload(); 
									});
									
									return false;
										
								}
								else
								{
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Campaign has NOT been stoped.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );
								}
							}
						}
						else
						{<!--- No result returned --->		
							$.alerts.okButton = '&nbsp;OK&nbsp;';					
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
											
				} 		
				
			});
			return false;
	}
	function reloadPage(){
		
		<cfif StructKeyExists(URL, 'page')>
			<cfset pageUrl = "?page=" & URL.page>
		<cfelse>
			<cfset pageUrl = "">
		</cfif>
		var submitLink = "<cfoutput>#rootUrl#/#SessionPath#/campaign/listcampaigns#pageUrl#</cfoutput>";
		reIndexFilterID();
		$("#filters_content").attr("action", submitLink);
		// Check if dont user filter
		if($("#isClickFilter").val() == 0){
			$("#totalFilter").val(0);
		}
		$("#filters_content").submit();
		
	}
	
	function EditCampaign(INPBATCHID, INPCONTACTTYPES) {
		
		
		<!--- Do not block editing of running campaign...
		
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetRunningCampaignStatus&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : INPBATCHID},					  
			error: function(XMLHttpRequest, textStatus, errorThrown) {},					  
			success:		
				<!--- Default return function for Do CFTE Demo - Async call back --->
				function(d) 
				{
					 
						window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/viewcampaign?inpbatchid=</cfoutput>' + INPBATCHID;
					
					
				<!---	 if (!d.DATA.ISRUNNING[0]) {
						window.location = '<cfoutput>#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=</cfoutput>' + INPBATCHID;
					} --->
					
				<!---	
					else {
						jAlert("The campaign is currently running. To make changes, stop the campaign first.", 'Error', function(result) {});	
					}--->
				}
			});	--->
			
			<!--- if(typeof(INPCONTACTTYPES) != "undefined") --->
			
			<!--- Changes by Adarsh --->
			
			if(typeof INPCONTACTTYPES  != "string")
			
			<!--- Changes by Adarsh Ends Here --->
							
				INPCONTACTTYPES = "1";
			
			if (INPCONTACTTYPES.match("2")) 
			{
				window.location = '<cfoutput>#rootUrl#/#SessionPath#/email/ebm_email_create?inpbatchid=</cfoutput>' + INPBATCHID;
			}
			else
			{			
				window.location = '<cfoutput>#rootUrl#/#SessionPath#/campaign/viewcampaign?inpbatchid=</cfoutput>' + INPBATCHID;					
			}
			
	}
	
	<cfif isDialog EQ 1>
		function SelectCampaign(INPBATCHID, INPDESC)
		{
			<!--- Set hidden variables and then Close dialog --->
			
			$TargetBatchId.val(INPBATCHID);
			$TargetBatchDesc.html(INPDESC);
			$dialogCampaignPicker.dialog('close');
					
			return false;
		}
	</cfif>
	
	<!--- Limit to a smaller version without all the extra data for a picker--->
	<cfif isDialog EQ 0>
			
		setInterval(function() {
			$.ajax({
				type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/distribution.cfc?method=GetSimpleDistributionData&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
				dataType: 'json',
				data:  { 
					page : '<cfoutput>#page#</cfoutput>',
					query: '<cfoutput>#query#</cfoutput>'
				},
				success: function(d) 
				{
					// update all status of camapain
					$('.View_status').each(function(index){
						$(this).removeClass();
						$(this).addClass('View_status img16_16 ' + d.ROWS[index].IMAGESTATUS);
						$(this).attr('title', d.ROWS[index].STATUSNAME);
						
						// update view progress icon
						var viewProgressObj = $('.ViewProgress').get(index);
						$(viewProgressObj).empty();
						
						if(d.ROWS[index].STATUSVIEWFORMAT == 'viewProgress'){
							$(viewProgressObj).append('<a href="##" onclick="ViewProgress('+ d.ROWS[index].BATCHID_BI +', this); return false;" ><img class="Run_RowBatch ListIconLinks img16_16 campaign_view_progress_16_16" title="View Campaign Progress" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						}
						
						// update campaign description
						var campaignDescObj = $('.CampaignDescription').get(index);
						$(campaignDescObj).html(d.ROWS[index].DESC_VCH);
						
						// update recipients
						var campaignRecipient = $('.CampaignRecipients').get(index);
						$(campaignRecipient).empty();
						if(d.ROWS[index].RECIPIENTVIEWFORMAT == 'none' || d.ROWS[index].RECIPIENTCOUNT == -1){
							$(campaignRecipient).append('None ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">Add</a> )');
						}else{
							$(campaignRecipient).append(d.ROWS[index].RECIPIENTCOUNT + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/viewrecipientlistdetails?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&APPLYRULE=' + d.ROWS[index].CONTACTISAPPLYFILTER + '&CONTACTTYPES='+ d.ROWS[index].CONTACTTYPES_VCH + '&SELECTEDGROUP='+ d.ROWS[index].CONTACTGROUPID_INT + '&NOTE='+ d.ROWS[index].CONTACTNOTE_VCH +'">view</a> | <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/campaignrecipients?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">edit</a> )');
						}
						
						var campaignSchedule = $('.CampaignSchedule').get(index);
						$(campaignSchedule).empty();			
						<cfif campaignSchedulePermission.havePermission>
							if(d.ROWS[index].SCHEDULEFORMAT == 'NoSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">set</a> )');
							}else if(d.ROWS[index].SCHEDULEFORMAT == 'YesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> | <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=edit">edit</a> )');
							} else if(d.ROWS[index].SCHEDULEFORMAT == 'DisabledNoSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="javascript:alert(\'<cfoutput>#setScheduleMessage#</cfoutput>\')">set</a> )');
							} else if(d.ROWS[index].SCHEDULEFORMAT == 'DisabledYesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="#rootUrl#/#SessionPath#/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> | <a href="javascript:alert(\'<cfoutput>#setScheduleMessage#</cfoutput>\')">edit</a> )');
							}
						<cfelse>
							if(d.ROWS[index].SCHEDULEFORMAT == 'NoSchedule' || d.ROWS[index].SCHEDULEFORMAT == 'DisabledNoSchedule'){
								$(campaignSchedule).append(d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No');
							}else if(d.ROWS[index].SCHEDULEFORMAT == 'YesSchedule' || d.ROWS[index].SCHEDULEFORMAT == 'DisabledYesSchedule'){
								$(campaignSchedule).append((d.ROWS[index].HAVESCHEDULE==true? 'Yes': 'No') + ' ( <a href="<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mycampaign/schedulecampaign?inpbatchid='+ d.ROWS[index].BATCHID_BI +'&mode=view">view</a> )');
							}
						</cfif>
						
						<!--- Show campaign user id and company column only for super user --->
						<cfif session.userrole EQ 'SuperUser'>
						var campaignOwner = $('.CampaignOwner').get(index);
						$(campaignOwner).empty();
						$(campaignOwner).html(d.ROWS[index].USERID_INT);
						
						var campaignCompany = $('.CampaignCompany').get(index);
						$(campaignCompany).empty();
						$(campaignCompany).html(d.ROWS[index].COMPANYNAME_VCH);
						</cfif>
						
						<!--- for campaign controls column --->
						var campaignControls = $('.CampaignControls').get(index);
						$(campaignControls).empty();
						<cfif campaignEditPermission.havePermission>
							$(campaignControls).append('<a href="##" onclick="RunCampaign('+ d.ROWS[index].BATCHID_BI +')" ><img class="Run_RowBatch ListIconLinks img16_16 run_16_16" title="Run Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						<cfif campaignStopPermission.havePermission>
							$(campaignControls).append('<a href="##"  ><img class="Stop_RowBatch ListIconLinks img16_16 stop_16_16" rel="'+ d.ROWS[index].PAGEREDIRECT +'" title="Stop Campaign" pageRedirect="'+ d.ROWS[index].BATCHID_BI +'" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						<cfif campaignCancelPermission.havePermission>
							$(campaignControls).append('<a href="##"  ><img class="Cancel_RowBatch ListIconLinks img16_16 cancel_16_16" rel="'+ d.ROWS[index].PAGEREDIRECT +'" pageRedirect="'+ d.ROWS[index].BATCHID_BI +'" title="Cancel Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
						
						<!--- for campaign options column --->
						var campaignOptions = $('.CampaignOptions').get(index);
						$(campaignOptions).empty();
						<cfif campaignEditPermission.havePermission>
							$(campaignOptions).append('<a href="##" onclick="EditCampaign('+ d.ROWS[index].BATCHID_BI +')"><img class="ListIconLinks img16_16 view_16_16" title="View/Edit Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
						</cfif>
						$(campaignOptions).append('<a href="##" onclick="return showRenameCampaignDialog('+ d.ROWS[index].BATCHID_BI +');"><img class="ListIconLinks img16_16 rename_16_16" title="Rename Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" /></a>');
						<cfif campaignDeletePermission.havePermission>
							$(campaignOptions).append('<a href="##" ><img class="del_RowMCContent ListIconLinks img16_16 delete_16_16" rel="'+ d.ROWS[index].BATCHID_BI +'" pageRedirect="'+ d.ROWS[index].PAGEREDIRECT +'" title="Delete Campaign" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif"/></a>');
						</cfif>
					});
					
				}
			});	
		}, 60000);
    
	</cfif>
	
    function OpenDialog(dialog_id){
		$("#overlay").show();		
      	$("#dialog_" + dialog_id).fadeIn(300);
	}
	function CloseDialog(dialog_id){
		currentUserID = '';
		$("#overlay").hide();
      	$("#dialog_" + dialog_id).fadeOut(300);
	}
</script>