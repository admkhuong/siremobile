

<!--- No display --->
<cfcomponent output="true">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off DEBUGging --->
    <cfsetting showDEBUGoutput="no" />
	<cfinclude template="../../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
    	
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="10"/> 
    
       
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse CCD Values --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="BuildCCDXML" access="remote" output="false" hint="Build the CCD XML from input values.">
        <cfargument name="inpCID" TYPE="string"/>
        <cfargument name="inpFILESEQ" TYPE="string"/>
        <cfargument name="inpUSERSPECDATA" TYPE="string"/>
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, CCDXMLSTRING")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                
       
            <cftry>
            
         	 <!--- 
			   	<cfset thread = CreateObject("java", "java.lang.Thread")>
				<cfset thread.sleep(3000)> 
				 --->
			
                           
             <!---     <cfscript> sleep(3000); </cfscript>  --->
            
            	<!--- <cfscript> sleep(3000); </cfscript>   --->
                 
                <!--- Start CCD XML --->
                <cfset CCDXMLSTRINGBuff = "<CCD">
                
                <!--- Validate Caller ID --->
                <cfif inpCID NEQ "">
                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID='#inpCID#'">
                
                </cfif>
             
                <!--- Validate User Specified Data --->
                <cfif inpUSERSPECDATA NEQ "">
                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " USERSPECDATA='#inpUSERSPECDATA#'">
                
                </cfif>
                   
                
                <!--- Validate FilSeqNum --->
                <cfif inpFILESEQ NEQ "">
                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " FILESEQ='#inpFILESEQ#'">
                
                </cfif>
                
                
                
                
                <!--- Close CCD XML --->
                <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & ">0</CCD>">
                    
				<cfset dataout =  QueryNew("RXRESULTCODE, CCDXMLSTRING")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(CCDXMLSTRINGBuff)#") />
                 
            <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, CCDXMLSTRING, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the CCD Values --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="ReadCCDXML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING ")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CID", "") />
            <cfset QuerySetCell(dataout, "USERSPECDATA", "") />
            <cfset QuerySetCell(dataout, "FILESEQ", "") />
            <cfset QuerySetCell(dataout, "DRD", "") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "empty") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset USERSPECDATA = "" />
            <cfset FILESEQ = "" />
                                                 
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                             
                 
				
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                <cfset CCDXMLSTRING = "">
                
                <!--- Get last of each element/attribute if there is more than one by using ArrayLen(selectedElements) --->
                
                <cfscript>             
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@CID");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						CID = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);		
					}
					else
					{										
						CID = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@USERSPECDATA");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						USERSPECDATA = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						USERSPECDATA = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@FILESEQ");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						FILESEQ = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						FILESEQ = "";	
					}
					
					selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD/@DRD");
					
					if(ArrayLen(selectedElements) GT 0)
					{						
						DRD = trim(selectedElements[ArrayLen(selectedElements)].XmlValue);							
					}
					else
					{										
						DRD = "";	
					}
												
				</cfscript>         
                
               <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                                                          
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                
                </cfloop>
                
                
                
                
            
            <!--- 	<cfset CCDXMLSTRING = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>
              --->   
                
            <!--- 		CCDXMLSTRING = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD");
			 --->	
                     
             	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING ")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CID", "#CID#") />
                <cfset QuerySetCell(dataout, "USERSPECDATA", "#USERSPECDATA#") />
                <cfset QuerySetCell(dataout, "FILESEQ", "#FILESEQ#") />
                <cfset QuerySetCell(dataout, "DRD", "#DRD#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(OutToDBXMLBuff)#") />
                 
            <cfcatch TYPE="any">
                
				 <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE,TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, CID, USERSPECDATA, FILESEQ, DRD, CCDXMLSTRING")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				<cfset QuerySetCell(dataout, "CID", "") />
                <cfset QuerySetCell(dataout, "USERSPECDATA", "") />
                <cfset QuerySetCell(dataout, "FILESEQ", "") />
                <cfset QuerySetCell(dataout, "DRD", "") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "Error") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="WriteCCDXML" access="remote" output="false" hint="Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpCID" TYPE="string"/>
        <cfargument name="inpFILESEQ" TYPE="string"/>
        <cfargument name="inpUSERSPECDATA" TYPE="string"/>
        <cfargument name="inpDRD" TYPE="string"/>
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset USERSPECDATA = "" />
            <cfset FILESEQ = "" />
                                                 
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
				
             
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                <!--- Delete existing CCD from XML  --->
                <cfset StructDelete(myxmldocResultDoc.XMLRoot, "CCD")>

              
                    
                <!--- Start CCD XML --->
                <cfset CCDXMLSTRINGBuff = "<CCD">
                
                <!--- Validate Caller ID --->
                <cfif inpCID NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID=""#inpCID#""">                
                </cfif>
             
                <!--- Validate User Specified Data --->
                <cfif inpUSERSPECDATA NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " USERSPECDATA=""#inpUSERSPECDATA#""">                
                </cfif>                   
                
                <!--- Validate FilSeqNum --->
                <cfif inpFILESEQ NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " FILESEQ=""#inpFILESEQ#""">                
                </cfif>
                
                <!--- Validate DRD --->
                <cfif inpDRD NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " DRD=""#inpDRD#""">                
                </cfif>
                                                             
                <!--- Close CCD XML --->
                <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & ">0</CCD>">
                
                <!--- Clean up for DB storage --->
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "")>
                
                <cfset TotalControlStringBuff = OutToDBXMLBuff & CCDXMLSTRINGBuff>
           
           		<!--- CF is adding "" somewhere alond the line for ' - replace all here --->
           		<cfset TotalControlStringBuff = Replace(TotalControlStringBuff, '"', "'", "ALL")> 
                
                <!--- DB errors on string with '' already in it - only one getting written to DB --->
           		<!---<cfset TotalControlStringBuff = trim(Replace(TotalControlStringBuff, "''", "''''", "ALL"))>  --->              
           
              	<!--- Write to DB Batch Options --->
                <cfquery name="UpdateBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                    	simpleobjects.batch
                    SET 
                    	XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TotalControlStringBuff#">
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
		 		<cfquery name="InsertHistory" datasource="#Session.DBSourceEBM#">
		            INSERT INTO
		              simpleobjects.history (BatchId,UserId,XMLControl,`time`,`event`)
		            VALUES
		            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
						#SESSION.UserID#,
						'#GetBatchOptions.XMLControlString_vch#',
						'#DateFormat(Now(),"yyyy-mm-dd")#',
						'hello'
						)  
		       </cfquery>
			                      
              	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(CCDXMLSTRINGBuff)#") />

                 
            <cfcatch TYPE="any">
				 <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, CCDXMLSTRING")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
            </cfcatch>
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
     
    <!--- ******************************************************************************************************************** --->
    <!--- Read an XML string from DB and parse out the Voicemail DM Values --->
    <!--- ******************************************************************************************************************** --->
        
	<cffunction name="ReadvmdmXML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="INPQID" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRCK1, CURRCK2, CURRCK3, CURRCK4, CURRCK5, CURRCK6, CURRCK7, CURRCK8, CURRCK9, vmdmXMLSTRING ")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
                <cfset QuerySetCell(dataout, "CURRCK1", "") />
                <cfset QuerySetCell(dataout, "CURRCK2", "") />
                <cfset QuerySetCell(dataout, "CURRCK3", "") />
                <cfset QuerySetCell(dataout, "CURRCK4", "") />
                <cfset QuerySetCell(dataout, "CURRCK5", "") />
                <cfset QuerySetCell(dataout, "CURRCK6", "") />
                <cfset QuerySetCell(dataout, "CURRCK7", "") />
                <cfset QuerySetCell(dataout, "CURRCK8", "") />
                <cfset QuerySetCell(dataout, "CURRCK9", "") />
                <cfset QuerySetCell(dataout, "vmdmXMLSTRING", "") />
                
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
				

                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                    
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	               
                    <cfset CURRMT = "-1">
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/DM/@MT")>
                    
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
					   <cfif CURRMT EQ 2>
                                                        
                            <!--- Read DM data --->         
                            <cfinclude template="RXT\read_vmdm.cfm">
                    
                    
							<!--- Clean up for raw XML --->
                            <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        
                            <cfset QuerySetCell(dataout, "vmdmXMLSTRING", "#HTMLCodeFormat(OutToDBXMLBuff)#") />
                            
                    
                            <!--- Exit Loop --->
                            <cfbreak>
                            
                        <cfelse>
                            <cfset CURRQID = "-1">                        
                        </cfif>
					</cfif>                    
                                              
            
                </cfloop>
              
				                             
            <cfcatch TYPE="any">
                
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, vmdmXMLSTRING")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
              	<cfset QuerySetCell(dataout, "vmdmXMLSTRING", "") />
              
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse out the CCD Values - Replace with new values - Write back to DB --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="WritevmdmXML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpCID" TYPE="string"/>
        <cfargument name="inpFILESEQ" TYPE="string"/>
        <cfargument name="inpUSERSPECDATA" TYPE="string"/>
        <cfargument name="inpDRD" TYPE="string"/>
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset USERSPECDATA = "" />
            <cfset FILESEQ = "" />
                                                 
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                 			
				
            
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                
                <!--- Delete existing CCD from XML  --->
                <cfset StructDelete(myxmldocResultDoc.XMLRoot, "CCD")>

              
                    
                <!--- Start CCD XML --->
                <cfset CCDXMLSTRINGBuff = "<CCD">
                
                <!--- Validate Caller ID --->
                <cfif inpCID NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " CID=""#inpCID#""">                
                </cfif>
             
                <!--- Validate User Specified Data --->
                <cfif inpUSERSPECDATA NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " USERSPECDATA=""#inpUSERSPECDATA#""">                
                </cfif>                   
                
                <!--- Validate FilSeqNum --->
                <cfif inpFILESEQ NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " FILESEQ=""#inpFILESEQ#""">                
                </cfif>
                
                <!--- Validate DRD --->
                <cfif inpDRD NEQ "">                
               		<cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & " DRD=""#inpDRD#""">                
                </cfif>
                                                             
                <!--- Close CCD XML --->
                <cfset CCDXMLSTRINGBuff = CCDXMLSTRINGBuff & ">0</CCD>">
                
                <!--- Clean up for DB storage --->
                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "")>
                
                <cfset TotalControlStringBuff = OutToDBXMLBuff & CCDXMLSTRINGBuff>
           
	            <!--- CF is adding "" somewhere alond the line for ' - replace all here --->
           		<cfset TotalControlStringBuff = Replace(TotalControlStringBuff, '"', "'", "ALL")>
                
              	<!--- Write to DB Batch Options --->
                <cfquery name="UpdateBatchOptions" datasource="#Session.DBSourceEBM#">
                    UPDATE 
                    	simpleobjects.batch
                    SET 
                    	XMLControlString_vch =  '#TotalControlStringBuff#' 
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                                     
              	<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, CCDXMLSTRING")>    
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "#HTMLCodeFormat(CCDXMLSTRINGBuff)#") />

                 
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,INPBATCHID, CCDXMLSTRING")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "CCDXMLSTRING", "") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cfinclude template="RXT/genvmdm.cfm">
    <cfinclude template="RXT/genRXT1.cfm">
    <cfinclude template="RXT/genRXT2.cfm">
    <cfinclude template="RXT/genRXT3.cfm">
    <cfinclude template="RXT/genRXT4.cfm">
    <cfinclude template="RXT/genRXT5.cfm">
    <cfinclude template="RXT/genRXT6.cfm">
    <cfinclude template="RXT/genRXT7.cfm">
    <cfinclude template="RXT/genRXT8.cfm">
    <cfinclude template="RXT/genRXT9.cfm">
    <cfinclude template="RXT/genRXT10.cfm">
    <cfinclude template="RXT/genRXT11.cfm">
    <cfinclude template="RXT/genRXT12.cfm">
    <cfinclude template="RXT/genRXT13.cfm">
    <cfinclude template="RXT/genRXT14.cfm">
    <cfinclude template="RXT/genRXT15.cfm">
    <cfinclude template="RXT/genRXT16.cfm">
    <cfinclude template="RXT/genRXT17.cfm">
    <cfinclude template="RXT/genRXT18.cfm">
    <cfinclude template="RXT/genRXT19.cfm">
    <cfinclude template="RXT/genRXT20.cfm">
    <cfinclude template="RXT/genRXT21.cfm">
    <cfinclude template="RXT/genRXT22.cfm">
	<cfinclude template="RXT/genRXT24.cfm">
 
	<cffunction name="ReadRXSSXMLELE" access="remote" output="false" hind="Read all RXSS MCIDs from XML string from the DB"> 
		<cfargument name="selectedElements" TYPE="xml"/>


		<cfoutput>
	        <cfset CURRLINK1 = "">
			<cfset CURRLINK2 = "">
            <cfset CURRLINK3 = "">
            <cfset CURRLINK4 = "">
            <cfset CURRLINK5 = "">
            <cfset CURRLINK6 = "">
            <cfset CURRLINK7 = "">
            <cfset CURRLINK8 = "">
            <cfset CURRLINK9 = "">
            <cfset CURRLINK0 = "">
            <cfset CURRLINKDEF = "">
            <cfset CURRLINKErr = "">
            <cfset CURRLINKNR = "">
            <cfset CURRLINKPound = "">
            <cfset CURRLINKStar = "">
					
			
                
		</cfoutput>
		<cfreturn dataout>
	</cffunction> 

 	<!--- ************************************************************************************************************************* --->
    <!--- input Batch ID and QUID then output the RXT Values and TYPE --->
    <!--- ************************************************************************************************************************* --->
     
    
    
  <cffunction name="ReadRXTXML" access="remote" output="false" hint="Read all RXSS MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
       
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRCK1, CURRCK2, CURRCK3, CURRCK4, CURRCK5, CURRCK6, CURRCK7, CURRCK8, CURRCK9, CURRCK15 ")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
                <cfset QuerySetCell(dataout, "CURRCK1", "") />
                <cfset QuerySetCell(dataout, "CURRCK2", "") />
                <cfset QuerySetCell(dataout, "CURRCK3", "") />
                <cfset QuerySetCell(dataout, "CURRCK4", "") />
                <cfset QuerySetCell(dataout, "CURRCK5", "") />
                <cfset QuerySetCell(dataout, "CURRCK6", "") />
                <cfset QuerySetCell(dataout, "CURRCK7", "") />
                <cfset QuerySetCell(dataout, "CURRCK8", "") />
                <cfset QuerySetCell(dataout, "CURRCK9", "") />
				<cfset QuerySetCell(dataout, "CURRCK15", "") />

                
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                <!---check permission--->
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
				<cfelse>
				
	                <!--- <cfscript> sleep(3000); </cfscript>  --->
	                     
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	                 
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	                           
	             
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//ELE")>
	                <cfset dataout = QueryNew("RXRESULTCODE, RXQID, RXT, RXTDESC, DESC, RXSSMCIDXMLSTRING")>  
	                
	                <cfloop array="#selectedElements#" index="CURRMCIDXML">
	                
		                <cfset CURRQID = "-1">
	                    <cfset CURRRXT = "-1">
	                    <cfset CURRRXTDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
	                    
	                    
	                    	
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry> 
	                
		                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
	                    		
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        
	                        <!--- Is this the one we want? --->
	                        <cfif CURRQID EQ INPQID>
	                        
								<!--- Read all the values and break --->                        
	                            <cfset selectedElementsRXT = XmlSearch(XMLELEDoc, "/ELE/@RXT")>
	                                        
	                                <cfif ArrayLen(selectedElementsRXT) GT 0>
	                                    <cfset CURRRXT = selectedElementsRXT[ArrayLen(selectedElementsRXT)].XmlValue>
	                                <cfelse>
	                                    <cfset CURRRXT = "-1">                        
	                                </cfif>
								
	                                <!--- Do specialized processing in include templates --->
	                                <cfswitch expression="#CURRRXT#">                                            
	                                    <cfcase value="1"><cfset CURRRXTDESC = "Statement"> <cfinclude template="RXT\read_RXT1.cfm"> </cfcase>
	                                    <cfcase value="2"><cfset CURRRXTDESC = "IVR - Single Digit Response"> <cfinclude template="RXT\read_RXT2.cfm"> </cfcase>
	                                    <cfcase value="3"><cfset CURRRXTDESC = "Record a response - with erase and re-record options"> <cfinclude template="RXT\read_RXT3.cfm"> </cfcase>
	                                    <cfcase value="4"><cfinclude template="RXT\read_RXT4.cfm"><cfset CURRRXTDESC = "Live Agent Transfer (LAT)"></cfcase>
	                                    <cfcase value="5"><cfinclude template="RXT\read_RXT5.cfm"><cfset CURRRXTDESC = "Opt Out"></cfcase>
	                                    <cfcase value="6"><cfinclude template="RXT\read_RXT6.cfm"><cfset CURRRXTDESC = "IVR multi string - press pound or five second pause to complete entry"></cfcase>
	                                    <cfcase value="7"><cfset CURRRXTDESC = "Opt In - 10 Digit String"><cfinclude template="RXT\read_RXT7.cfm"> </cfcase>
	                                    <cfcase value="8"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT8.cfm"> </cfcase>
	                                    <cfcase value="9"><cfset CURRRXTDESC = "Read out previous/specified digit strings 1-10 - no IVR options"><cfinclude template="RXT\read_RXT9.cfm"> </cfcase>
	                                    <cfcase value="10"><cfset CURRRXTDESC = "SQL Query String - INSERT, UPDATE or SELECT (1,0)"><cfinclude template="RXT\read_RXT10.cfm"> </cfcase>
	                                    <cfcase value="11"><cfset CURRRXTDESC = "Play DTMFs - no IVR options"><cfinclude template="RXT\read_RXT11.cfm"> </cfcase>
	                                    <cfcase value="12"><cfset CURRRXTDESC = "Strategic Pause - line still listens for hangups but will pause for specified number of milli-seconds"><cfinclude template="RXT\read_RXT12.cfm"> </cfcase>
	                                    <cfcase value="13"><cfset CURRRXTDESC = "eMail"><cfinclude template="RXT\read_RXT13.cfm"> </cfcase>
	                                    <cfcase value="14"><cfset CURRRXTDESC = "Store IGData for inbound calls"><cfinclude template="RXT\read_RXT14.cfm"> </cfcase>
	                                    <cfcase value="15"><cfset CURRRXTDESC = "Play from absolute path"><cfinclude template="RXT\read_RXT15.cfm"> </cfcase>
	                                    <cfcase value="16"><cfset CURRRXTDESC = "Switch based on previous responses to a previous MCID"><cfinclude template="RXT\read_RXT16.cfm"> </cfcase>
	                                    <cfcase value="17"><cfset CURRRXTDESC = "Switchout MCIDs based on SQL query - Ignores CCD changes - Justs switches out MCID - only generates one path - no live/machine options"><cfinclude template="RXT\read_RXT17.cfm"> </cfcase>
	                                    <cfcase value="18"><cfset CURRRXTDESC = "Automatic Speech Recognition (ASR)"><cfinclude template="RXT\read_RXT18.cfm"> </cfcase>
	                                    <cfcase value="19"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT19.cfm"> </cfcase>
	                                    <cfcase value="20"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT20.cfm"> </cfcase>
	                                    <cfcase value="21"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT21.cfm"> </cfcase>
										<cfcase value="22"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT22.cfm"> </cfcase>                                          
										<cfcase value="24"><cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT24.cfm"> </cfcase>                                          
	                                </cfswitch>        
	                        
	                        		<!--- Exit Loop --->
	                        		<cfbreak>
	                            </cfif>
	                        
	                        
	                    <cfelse>
	                        <cfset CURRQID = "-1">                        
	                    </cfif>
	            
	                </cfloop>
	              
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//SWITCH")>
					<cfloop array="#selectedElements#" index="CURRMCIDXML">
		                <cfset CURRQID = "-1">
	                    <cfset CURRRXT = "21">
	                    <cfset CURRRXTDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry> 
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/SWITCH/@QID")>
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        <!--- Is this the one we want? --->
	                        <cfif CURRQID EQ INPQID>
								<cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT21.cfm">
							</cfif>
						</cfif>
					</cfloop>
					
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//CONV")>
					<cfloop array="#selectedElements#" index="CURRMCIDXML">
	
		                <cfset CURRQID = "-1">
	                    <cfset CURRRXT = "22">
	                    <cfset CURRRXTDESC = "NA">
	                    <cfset CURRDESC = "Description Not Specified">
						<!--- Parse for ELE data --->                             
	                    <cftry>
	                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
	                           
	                          <cfcatch TYPE="any">
	                            <!--- Squash bad data  --->    
	                            <cfset XMLELEDoc = XmlParse("BadData")>                       
	                         </cfcatch>              
	                    </cftry>
						
						<cfset selectedElementsII = XmlSearch(XMLELEDoc, "/CONV/@QID")>
						<cfif ArrayLen(selectedElementsII) GT 0>
	                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
	                        <!--- Is this the one we want? --->
	                        <cfif CURRQID EQ INPQID>
								<cfset CURRRXTDESC = "NA"><cfinclude template="RXT\read_RXT22.cfm">
							</cfif>
						</cfif>
					</cfloop>
				
				</cfif>
       
	            <cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3>
					</cfif>    
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID")> 
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
	                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
	                <cfset QuerySetCell(dataout, "CURRDESC", "") />
	                <cfset QuerySetCell(dataout, "CURRBS", "") />
	                <cfset QuerySetCell(dataout, "CURRUID", "") />
	                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
	                <cfset QuerySetCell(dataout, "CURRELEID", "") />
	                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
	            
	            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
  
    <!--- ************************************************************************************************************************* --->
    <!--- input BatchID, StartQID, and direction then reorders MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
    
   
  	<cffunction name="ReorderQID" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpStartQID" TYPE="string"/>	 <!--- QID to start with --->
        <cfargument name="inpDirection" TYPE="string"/>  <!--- 0 is down and 1 is up --->
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
			
				

                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURRMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                   
                   	
                    
                    <cfif inpDirection GT 0>
                    <!--- Move up --->
                    
    	               	<cfif CURRQID GTE inpStartQID> 
                             <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "QID='#CURRQID#'", "QID='#CURRQID + 1#")>	
					 	     
							 <!--- Modify values based on TYPE here ? --->
        
                        </cfif>
                    
                    
                    <cfelse>
                    <!--- Move down --->
                    
                    	<!--- Must be greater than 1 to move down --->
                    	<cfif CURRQID LTE inpStartQID AND CURRQID > 1> 
                             <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "QID='#CURRQID#'", "QID='#CURRQID - 1#")>	
					 	     
							 <!--- Modify values based on TYPE here ? --->
        
                        </cfif>
                    
                    
                    </cfif>
                    
                   
                   	<!--- Insert CURRent XML --->
                   	<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                <!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'DM'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                 
				
		
                <cfloop query="GetDMs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_VCH#"> 
                </cfloop>
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                 
				

                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">  
                
                <!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'CCD'  
                </cfquery>
                 
				

                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_VCH#"> 
                                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
                
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch = '#OutToDBXMLBuff#'
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                
                 
		 		                             
            <cfcatch TYPE="any">
                
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,INPBATCHID, INPQID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

	<!--- Delete only connection --->
	<cffunction name="DeleteConnect" access="remote" output="false" hint="Delete connect width two divice">
		<cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="LINKObj" TYPE="string"/>	 <!--- number connect --->
        <cfargument name="INPDELETESTART" TYPE="string"/>	 <!--- QID to start with --->
        <cfargument name="INPDELETEEND" TYPE="string"/>	 <!--- QID to end with --->
		
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' /> 
		
		<cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>
		
         <cfset myxmldoc = "" />
         <cfset selectedElements = "" />
         <cfset OutTodisplayxmlBuff = ""/>

        <!--- Read from DB Batch Options --->
        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
            SELECT                
              XMLControlString_vch
            FROM
              simpleobjects.batch
            WHERE
              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
        </cfquery>  
		
        <!--- Parse for data --->                             
        <cftry>
              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
               
              <cfcatch TYPE="any">
                <!--- Squash bad data  --->    
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
             </cfcatch>              
        </cftry>
		
		<cfreturn myxmldocResultDoc />
	</cffunction>
                
    <!--- ************************************************************************************************************************* --->
    <!--- input Batch ID Then Output a List of Script Libray Ids --->
    <!--- ************************************************************************************************************************* --->
       
  <cffunction name="GetSCRIPTLIBs" access="remote" output="false" hint="Replace Existing DM, RXSS ELE, OR CCD in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="inpStartLibId" TYPE="string" default="0"/>
                             
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = No Libraries Found
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
            
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataoutBuff = QueryNew("RXRESULTCODE, INPBATCHID, SCRIPTLIB, DEBUG")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            
            <cftry>                	
      
      			<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           
                           
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">

                
	                <cfset CURRLib = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@LIB")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURRLib = "-1">                        
                    </cfif>
                    
                              
          			<cfset QueryAddRow(dataoutBuff) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "SCRIPTLIB", "#CURRLib#") /> 
               	
               		<cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />     
              
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRLib = "-1">
                                       	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@DS") />
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRLib = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                                             
                        
                    <cfelse>
                        <cfset CURRLib = "-1" />                        
                    </cfif>
          
          			<cfset QueryAddRow(dataoutBuff) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataoutBuff, "RXRESULTCODE", "1") /> 
          
           			<cfset QuerySetCell(dataoutBuff, "INPBATCHID", "#INPBATCHID#") />
           
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataoutBuff, "SCRIPTLIB", "#CURRLib#") /> 
                                                
                </cfloop>
                                          
            
            	<cfset dataout = QueryNew("RXRESULTCODE, SCRIPTLIB, INPBATCHID, MESSAGE")>   
            
            	<!--- Write out local Query ---> 
                
                <!--- DMs First --->
                <cfquery dbTYPE="query" name="GetLibs">
                    SELECT 
                    	DISTINCT SCRIPTLIB    
                    FROM 
                        dataoutBuff
                    WHERE 
                        SCRIPTLIB > 0
                    
                    <cfif inpStartLibId GT 0>
                       	AND SCRIPTLIB > #inpStartLibId#
                    </cfif>
                    ORDER BY
                    	SCRIPTLIB
                </cfquery>
                 
                 
                <cfif GetLibs.RecordCount GT 0>
               
                    <cfloop query="GetLibs">
                        <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "SCRIPTLIB", "#GetLibs.SCRIPTLIB#") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "Library Found Lib ID=(#GetLibs.SCRIPTLIB#)") /> 
                        
                    </cfloop>
                 
                <cfelse>
                 
                <cfset QueryAddRow(dataout) />
                        
                        <!--- All is well --->
                        <cfset QuerySetCell(dataout, "RXRESULTCODE", "3") /> 
                        
                        <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
              
                        <!--- Either DM1, DM2, RXSSELE, CCD --->
                        <cfset QuerySetCell(dataout, "SCRIPTLIB", "0") />
                        
                        <cfset QuerySetCell(dataout, "MESSAGE", "No More Libraries Found") /> 
               
                </cfif>
                 
                                                            
		  		                             
            <cfcatch TYPE="any">
                
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE,INPBATCHID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
            </cfcatch>
            
            </cftry>      
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
  

  	<cffunction name="AddNewQIDRXSS" access="remote" output="false" hint="Add a new MCID to the existing RXSS Control String">
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="inpMCIDTYPE" TYPE="string"/>	 <!--- MCID TYPE to add --->
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
        <cfset DEBUGVAR1 = "0">               
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
			
			<cfset DEBUGVAR1 = inpRXSS> 
			
			<!--- Strip out HTML display markup --->
			<!--- Change case sensitivity --->
			<cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
			<!--- <cfset inpRXSSLocalBuff = ReplaceNoCase(inpRXSSLocalBuff, "<BR>", "", "all")>
			<cfset inpRXSSLocalBuff = ReplaceNoCase(inpRXSSLocalBuff, "rxss>", "RXSS>", "all")>
			<cfset inpRXSSLocalBuff = ReplaceNoCase(inpRXSSLocalBuff, "ele>", "ELE>", "all")>
			<cfset inpRXSSLocalBuff = ReplaceNoCase(inpRXSSLocalBuff, "<ele", "<ELE", "all")>
			<cfset inpRXSSLocalBuff = ReplaceNoCase(inpRXSSLocalBuff, "qid=", "QID=", "all")>
			--->
			
			<cfset DEBUGVAR1 = inpRXSSLocalBuff> 
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                	
                             
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           
                           
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                								 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, """", "'", "ALL")>
                    
                   	<!--- Insert CURRent XML --->
                   	<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
               
            	<cfset OutToDBXMLBuff = ""> 
            
            
            	<!--- Get Next available QID --->
             	<cfquery dbTYPE="query" name="GetNEXTQID">
                    SELECT 
                    	MAX(ELEMENTID_INT) + 1  AS NEXTQID 
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                </cfquery>

				<cfset NEXTQID = GetNEXTQID.NEXTQID>

				<cfif NEXTQID EQ "">
                	<cfset NEXTQID = 1>
                </cfif>
                			                

            
            	<!--- Add a record for the new XML - use the RXT/genRXT(x).cfm to build defaults --->                
                <cfswitch expression="#inpMCIDTYPE#">
 					<cfcase value="1"><cfset RetVal = genRXT1XML(NEXTQID)></cfcase>
                    <cfcase value="2"><cfset RetVal = genRXT2XML(NEXTQID)></cfcase>
                    <cfcase value="3"><cfset RetVal = genRXT3XML(NEXTQID)></cfcase>
                    <cfcase value="4"><cfset RetVal = genRXT4XML(NEXTQID)></cfcase>
                    <cfcase value="5"><cfset RetVal = genRXT5XML(NEXTQID)></cfcase>
                    <cfcase value="6"><cfset RetVal = genRXT6XML(NEXTQID)></cfcase>
                    <cfcase value="7"><cfset RetVal = genRXT7XML(NEXTQID)></cfcase>
                    <cfcase value="8"><cfset RetVal = genRXT8XML(NEXTQID)></cfcase>
                    <cfcase value="9"><cfset RetVal = genRXT9XML(NEXTQID)></cfcase>
                    <cfcase value="10"><cfset RetVal = genRXT10XML(NEXTQID)></cfcase>
                    <cfcase value="11"><cfset RetVal = genRXT11XML(NEXTQID)></cfcase>
                    <cfcase value="12"><cfset RetVal = genRXT12XML(NEXTQID)></cfcase>
                    <cfcase value="13"><cfset RetVal = genRXT13XML(NEXTQID)></cfcase>
                    <cfcase value="14"><cfset RetVal = genRXT14XML(NEXTQID)></cfcase>
                    <cfcase value="15"><cfset RetVal = genRXT15XML(NEXTQID)></cfcase>
                    <cfcase value="16"><cfset RetVal = genRXT16XML(NEXTQID)></cfcase>
                    <cfcase value="17"><cfset RetVal = genRXT17XML(NEXTQID)></cfcase>
                    <cfcase value="18"><cfset RetVal = genRXT18XML(NEXTQID)></cfcase>
                    <cfcase value="19"><cfset RetVal = genRXT19XML(NEXTQID)></cfcase>
                    <cfcase value="20"><cfset RetVal = genRXT20XML(NEXTQID)></cfcase>                           
                </cfswitch>
                                
                                                                
                <cfif LEN(RetVal.RAWXML) GT 0>                
            		
					<cfset QueryAddRow(dataout) />
                        
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
                    <!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", NEXTQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(RetVal.RAWXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert CURRent XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 
                        
                
                    <!--- Write out local Query ---> 					
					<cfset OutToDBXMLBuff = ""> 
					<cfset OutTodisplayxmlBuff = ""> 
					
                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
					<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                    
                    <!--- RXSS Next  --->
                    <cfquery dbTYPE="query" name="GetELEs">
                        SELECT 
                            XML_VCH    
                        FROM 
                            dataout
                        WHERE 
                            ELETYPE_VCH = 'RXSS'
                        ORDER BY
                            ELEMENTID_INT ASC       
                    </cfquery>
                     
                    <cfloop query="GetELEs">
                        <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
						<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#"> 
                    </cfloop>
                                   
                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
					<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('<RXSS>')#</UL>">  
                    
					<!--- <cfset DEBUGVAR1 = OutToDBXMLBuff>  --->
					
					<cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, NEXTQID, DEBUGVAR1")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
					<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                    <cfset QuerySetCell(dataout, "NEXTQID", "#NEXTQID#") />
					<cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
					
					
					
                     
				<cfelse>
                	<!--- Error generating new MCID --->
                	<cfthrow MESSAGE=" Error generating new MCID" TYPE="Any" extendedinfo="" errorcode="-4">
                </cfif>                 
		 		                             
            <cfcatch TYPE="any">
            
            	<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                
                <!---    
				<cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            --->
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>     

    <!--- ************************************************************************************************************************* --->
    <!--- input RXSS XML and QUID and new ELE XML String then replace existing RXSS ELE in main control string --->
    <!--- ************************************************************************************************************************* --->
    
    <!--- DELETE is just a special instance of this but just passing in a blank string --->     
    
  <cffunction name="SaveMCIDObjRXSS" access="remote" output="false" hint="Replace Existing DM, RXSS ELE, OR CCD in existing Control String">
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string" default="0"/>
        <cfargument name="inpTYPE" TYPE="string" default="None"/> <!--- DM, RXSS, CCD --->
        <cfargument name="inpXML" TYPE="string" default="NADA"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
        <cfset DEBUGVAR1 = "0">  
        <cfset OutTodisplayxmlBuff = "">                 
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH, DEBUG")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            
            <cftry>                	
      
      			<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                
                
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID") />
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                                             
                        
                    <cfelse>
                        <cfset CURRQID = "-1" />                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRQID) /> 
                    
                    <!--- Here is the beef --->
                    <cfif inpTYPE EQ "RXSS" AND INPQID EQ CURRQID>
                    	<!--- Insert New XML --->
                    	<cfset OutToDBXMLBuff = inpXML> 
            		<cfelse>
                    <!--- Clean up for raw XML --->
						<cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                        <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    </cfif>
                    
                     <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 
                    
                    
                </cfloop>
                                  
            
                <!--- Write out local Query ---> 					
                <cfset OutToDBXMLBuff = ""> 
                <cfset OutTodisplayxmlBuff = ""> 
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                        XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                 
                <cfloop query="GetELEs">
                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#"> 
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">  
                
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
               
               
                <!--- <cfset DEBUGVAR1 = OutToDBXMLBuff>  --->
                
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
                <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
                
		  		                             
            <cfcatch TYPE="any">
            	<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, XML_VCH, DISPLAYXML_VCH")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
            
            </cfcatch>
            
            </cftry>      
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
                
  
    
  <cffunction name="ReadRXTXMLRXSS" access="remote" output="true" hint="Read all RXSS MCIDs from XML string from the DB">
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
         
         <cfset INPBATCHID = "0">
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, CURRCK1, CURRCK2, CURRCK3, CURRCK4, CURRCK5, CURRCK6, CURRCK7, CURRCK8, CURRCK9 ")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
                <cfset QuerySetCell(dataout, "CURRCK1", "") />
                <cfset QuerySetCell(dataout, "CURRCK2", "") />
                <cfset QuerySetCell(dataout, "CURRCK3", "") />
                <cfset QuerySetCell(dataout, "CURRCK4", "") />
                <cfset QuerySetCell(dataout, "CURRCK5", "") />
                <cfset QuerySetCell(dataout, "CURRCK6", "") />
                <cfset QuerySetCell(dataout, "CURRCK7", "") />
                <cfset QuerySetCell(dataout, "CURRCK8", "") />
                <cfset QuerySetCell(dataout, "CURRCK9", "") />
                
                            
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           
              
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>
                
                <cfset dataout = QueryNew("RXRESULTCODE, RXQID, RXT, RXTDESC, DESC, RXSSMCIDXMLSTRING")>  
                
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                    
                    
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                        
                        <!--- Is this the one we want? --->
                        <cfif CURRQID EQ INPQID>
                        
							<!--- Read all the values and break --->                        
                            <cfset selectedElementsRXT = XmlSearch(XMLELEDoc, "/ELE/@RXT")>
                                        
                                <cfif ArrayLen(selectedElementsRXT) GT 0>
                                    <cfset CURRRXT = selectedElementsRXT[ArrayLen(selectedElementsRXT)].XmlValue>
                                <cfelse>
                                    <cfset CURRRXT = "-1">                        
                                </cfif>
                                
                                <!--- Do specialized processing in include templates --->
                                <cfswitch expression="#CURRRXT#">                                            
                                    <cfcase value="1"><cfset CURRRXTDESC = "Statement"> <cfinclude template="RXT\read_RXT1.cfm"> </cfcase>
                                    <cfcase value="2"><cfset CURRRXTDESC = "IVR - Single Digit Response"> <cfinclude template="RXT\read_RXT2.cfm"> </cfcase>
                                    <cfcase value="3"><cfset CURRRXTDESC = "Record a response - with erase and re-record options"> <cfinclude template="RXT\read_RXT3.cfm"> </cfcase>
                                    <cfcase value="4"><cfinclude template="RXT\read_RXT4.cfm"><cfset CURRRXTDESC = "Live Agent Transfer (LAT)"></cfcase>
                                    <cfcase value="5"><cfinclude template="RXT\read_RXT5.cfm"><cfset CURRRXTDESC = "Opt Out"></cfcase>
                                    <cfcase value="6"><cfinclude template="RXT\read_RXT6.cfm"><cfset CURRRXTDESC = "IVR multi string - press pound or five second pause to complete entry"></cfcase>
                                    <cfcase value="7"><cfset CURRRXTDESC = "Opt In - 10 Digit String"></cfcase>
                                    <cfcase value="8"><cfset CURRRXTDESC = "NA"></cfcase>
                                    <cfcase value="9"><cfset CURRRXTDESC = "Read out previous/specified digit strings 1-10 - no IVR options"></cfcase>
                                    <cfcase value="10"><cfset CURRRXTDESC = "SQL Query String - INSERT, UPDATE or SELECT (1,0)"></cfcase>
                                    <cfcase value="11"><cfset CURRRXTDESC = "Play DTMFs - no IVR options"></cfcase>
                                    <cfcase value="12"><cfset CURRRXTDESC = "Strategic Pause - line still listens for hangups but will pause for specified number of milli-seconds"></cfcase>
                                    <cfcase value="13"><cfset CURRRXTDESC = "eMail"></cfcase>
                                    <cfcase value="14"><cfset CURRRXTDESC = "Store IGData for inbound calls"></cfcase>
                                    <cfcase value="15"><cfset CURRRXTDESC = "Play from absolute path"></cfcase>
                                    <cfcase value="16"><cfset CURRRXTDESC = "Switch based on previous responses to a previous MCID"></cfcase>
                                    <cfcase value="17"><cfset CURRRXTDESC = "Switchout MCIDs based on SQL query - Ignores CCD changes - Justs switches out MCID - only generates one path - no live/machine options"></cfcase>
                                    <cfcase value="18"><cfset CURRRXTDESC = "Automatic Speech Recognition (ASR)"></cfcase>
                                    <cfcase value="19"><cfset CURRRXTDESC = "NA"></cfcase>
                                    <cfcase value="20"><cfset CURRRXTDESC = "NA"></cfcase>
                                    <cfcase value="21"><cfset CURRRXTDESC = "NA"></cfcase>                                            
                                </cfswitch>        
                        
                        		<!--- Exit Loop --->
                        		<cfbreak>
                            </cfif>
                        
                        
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
                    
                                              
            
                </cfloop>
              
				                             
            <cfcatch TYPE="any">
                
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, CURRRXT, CURRDESC, CURRBS, CURRUID, CURRLIBID, CURRELEID, CURRDATAID, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -3) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "CURRRXT", "-1") />
                <cfset QuerySetCell(dataout, "CURRDESC", "") />
                <cfset QuerySetCell(dataout, "CURRBS", "") />
                <cfset QuerySetCell(dataout, "CURRUID", "") />
                <cfset QuerySetCell(dataout, "CURRLIBID", "") />
                <cfset QuerySetCell(dataout, "CURRELEID", "") />
                <cfset QuerySetCell(dataout, "CURRDATAID", "") />
	            <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
    
   
  	<cffunction name="UpdateAnswerMapValue" access="remote" output="false" hint="Replace LINK in answer map in MCIDs in existing Control String">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>	 
        <cfargument name="inpAnswer" TYPE="string"/>	 
        <cfargument name="inpNewValue" TYPE="string"/>  
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>
                
                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLControlString_vch
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>

				<cfelse>
                
	                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                
                </cfif>               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRMT = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@MT")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRMT = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURRMT = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
          
			        <!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "DM") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRMT) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>

              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
          
          			<cfset QueryAddRow(dataout) />
                    
                    <!--- All is well --->
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
          
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRQID) /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                                      
                   	<!--- Insert CURRent XML --->
                   	<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 
                   
                    
                    
                </cfloop>
                <!---  save switch --->
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/SWITCH")>
				
				<cfloop array="#selectedElements#" index="CURRMCIDXML">
					
					<!--- Parse for ELE data --->                         
	                <cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset XMLELEDoc = ToString(XmlParse(#CURRMCIDXML#))>
						<cfcatch TYPE="any">
						<!--- Squash bad data  --->    
							<cfset XMLELEDoc = XmlParse("BadData")>
						</cfcatch>              
	                </cftry> 
	                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/SWITCH/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
					<!--- if update switch --->

						<!--- else add switch to xml --->
	          			<cfset QueryAddRow(dataout) />
	                    
	                    <!--- All is well --->
	                    <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
	          
						<!--- Either DM1, DM2, RXSSELE, CCD --->
	                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RXSS") /> 
	                    
	                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
	                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRQID) /> 
	                    <!--- Clean up for raw XML --->
	                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
	                                      
	                   	<!--- Insert CURRent XML --->
	                   	<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") /> 

				</cfloop>
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                
               <cfif INPBATCHID NEQ ""  AND INPBATCHID GT 0>
            		<!--- DMS First --->
                    <cfquery dbTYPE="query" name="GetDMs">
                        SELECT 
                            XML_VCH    
                        FROM 
                            dataout
                        WHERE 
                            ELETYPE_VCH = 'DM'
                        ORDER BY
                            ELEMENTID_INT ASC       
                    </cfquery>
                     
                    
                    <cfloop query="GetDMs">
                		<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_VCH#">
                    	<cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_VCH)#</UL>"> 
                	</cfloop>
                
			    </cfif>
                
                                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> 
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH,
                        ELEMENTID_INT    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
				<!--- if add code check Coldfusion hates single pound signs in any string when redrag(*,'#') answer map append data instead of  update data(#,*) --->
				<!--- must add code check for inpAnswer - bug fix --->
				<cfset inpAnswer = REPLACE(inpAnswer, "#CHR(35)#", "-13", "ALL" ) >  
                <cfset inpAnswer = REPLACE(inpAnswer, "#CHR(42)#", "-6", "ALL" ) > 
                <cfloop query="GetELEs">
                
                	<!--- Default value --->
                	<cfset GetXMLBuff = GetELEs.XML_VCH>
                	
                    <!--- Coldfusion hates single pound signs in any string - bug fix --->
                    <cfset GetXMLBuff = REPLACE(GetXMLBuff, "#CHR(35)#", "-13", "ALL" ) >  
                    <cfset GetXMLBuff = REPLACE(GetXMLBuff, "#CHR(42)#", "-6", "ALL" ) >  
   				
					<!--- Replace value --->
					<cfif GetELEs.ELEMENTID_INT EQ INPQID>
                            
							<cfset CURRAnswerMap = "">    
                            <cfset ValidKeyBuff = "#inpAnswer#">
                            <cfset AnswerDataOut = QueryNew("INPQID, INPUTKEY, LINKTOQID")>
                             
                             <!--- Parse for data --->                             
                            <cftry>
                                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                                 <cfset myxmldocResultDocAM = XmlParse("<XMLControlStringDoc>" & #GetXMLBuff# & "</XMLControlStringDoc>")>
                                   
                                  <cfcatch TYPE="any">
                                    <!--- Squash bad data  --->    
                                    <cfset myxmldocResultDocAM = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                                 </cfcatch>              
                            </cftry> 
                                
							<cfset selectedElementsAMII = XmlSearch(GetXMLBuff, "/ELE/@CK4")>
	                       
							<cfif ArrayLen(selectedElementsAMII) GT 0>
                                <cfset CURRAnswerMap = selectedElementsAMII[ArrayLen(selectedElementsAMII)].XmlValue>                                 
                            <cfelse>
                                <cfset CURRAnswerMap = "">                        
                            </cfif>
                
                            <cfset AnswerMapBuff  = CURRAnswerMap>
                            <cfloop condition="Len(AnswerMapBuff) GT 0">
                            
                            <!--- Get first close ) --->  
                            <cfif FIND(")", AnswerMapBuff) GT 0 >
                            
                                <!--- Get first pair --->
                                <cfset AnswerPairBuff = LEFT(AnswerMapBuff, FIND(")", AnswerMapBuff) ) >
                                    
                                 <!--- Get rest of pairs --->
                                <cfif LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) GT 0>
                                    <cfset AnswerMapBuff = RIGHT(AnswerMapBuff, LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) ) >
                                <cfelse>
                                    <!--- Exit loop if no more --->
                                    <cfset AnswerMapBuff = "">
                                </cfif>             
                                
                                <!--- Remove parenthesis --->
                                <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ",(", "") >
                                <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, "(", "") >
                                <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ")", "") >
                                                        
                                <cfif FIND(",", AnswerPairBuff) GT 0 >
                                    
                                    <!--- Dont add comma first time --->
                                    <cfif LEN(ValidKeyBuff) GT 0>                             
                                        <cfset ValidKeyBuff = ValidKeyBuff & "," & ListGetAt(AnswerPairBuff, 1, ",")>                            
                                    <cfelse>
                                        <cfset ValidKeyBuff = ListGetAt(AnswerPairBuff, 1, ",")>
                                    </cfif>
                                    
                                    <cfset RespondToBuff = ListGetAt(AnswerPairBuff, 2, ",")>
                            
                                    <!--- Still can't delete from QoQ so only add ones that are going to be replaced --->
                                    <cfif ListGetAt(AnswerPairBuff, 1, ",") NEQ inpAnswer>						 	
                                        <cfset QueryAddRow(AnswerDataOut) />          				
                                        <cfset QuerySetCell(AnswerDataOut, "INPQID", "#INPQID#") />							
                                        <cfset QuerySetCell(AnswerDataOut, "INPUTKEY", "#ListGetAt(AnswerPairBuff, 1, ",")#") />                
                                        <cfset QuerySetCell(AnswerDataOut, "LINKTOQID", "#ListGetAt(AnswerPairBuff, 2, ",")#") />
                                     </cfif>
                                
                                </cfif>
                            
                            <cfelse>
                                <!--- Exit loop if no more --->
                                <cfset AnswerMapBuff = "">
                            </cfif>
                                        
                        </cfloop>
                           
                        <cfif inpNewValue NEQ "">   
                            <cfset QueryAddRow(AnswerDataOut) />          				
                            <cfset QuerySetCell(AnswerDataOut, "INPQID", "#INPQID#") />              
                            <cfset QuerySetCell(AnswerDataOut, "INPUTKEY", "#inpAnswer#") />                
                            <cfset QuerySetCell(AnswerDataOut, "LINKTOQID", "#inpNewValue#") />   
                        </cfif>                                    
            
                        <cfset NewAnswerMap = "">
                        
                        <!--- Write out new answer map--->
                        <cfquery dbTYPE="query" name="GetMap">
                            SELECT  
                                INPUTKEY,
                                LINKTOQID
                            FROM 
                                AnswerDataOut
                            ORDER BY
                                INPUTKEY
                        </cfquery>
            			
						
                        <cfloop query="GetMap">
                            <cfif LEN(NewAnswerMap) GT 0>                             
                                <cfset NewAnswerMap = NewAnswerMap & ",(#GetMap.INPUTKEY#,#GetMap.LINKTOQID#)">>                            
                            <cfelse>
                                <cfset NewAnswerMap = "(#GetMap.INPUTKEY#,#GetMap.LINKTOQID#)">
                            </cfif>
                        </cfloop>
                   
		
            			<!--- Update answer map --->
 	                    <cfset GetXMLBuff = Replace(Replace(GetXMLBuff, '"', "'", "ALL"), "CK4='#CURRAnswerMap#'", "CK4='#NewAnswerMap#'", "ALL")>
                   
                        <!--- Now update allowed keys --->    
                        <cfset GetXMLBuff = REReplaceNoCase(GetXMLBuff, "CK2='[-,a-zA-Z0-9\(\)]*'", "CK2='#ValidKeyBuff#'", "ALL")>     
                                           
                    </cfif>            
                    <!--- Coldfusion hates single pound signs in any string - bug fix --->
                	<cfset GetXMLBuff = REPLACE(GetXMLBuff, "-13", "#CHR(35)#", "ALL" ) >
                    <cfset GetXMLBuff = REPLACE(GetXMLBuff, "-6", "#CHR(42)#", "ALL" ) >
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetXMLBuff#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetXMLBuff)#">  
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   
                
                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
					<!--- CCD  Last  --->
                    <cfquery dbTYPE="query" name="GetCCD">
                        SELECT 
                            XML_VCH    
                        FROM 
                            dataout
                        WHERE 
                            ELETYPE_VCH = 'CCD'  
                    </cfquery>
                   
                    <!--- no loop - only supposed to be one --->                
                    <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_VCH)#</UL>">
                </cfif>                   
                                   
                                    
               <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
               <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                
               <!--- Write output --->
               <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
                    


					<!--- Save Local Query to DB --->
                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                        UPDATE
                            simpleobjects.batch
                        SET
                            XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>  
					<!--- Save History tranglt 
			 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
			            INSERT INTO
			              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
			            VALUES
			            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
							#SESSION.UserID#,
							'#OutToDBXMLBuff#',
							'#DateFormat(Now(),"yyyy-mm-dd")# #TimeFormat(Now(),"hh:mm:ss")#',
							'UpdateAnswerMapValue'
							)  
			       </cfquery>
                	--->
					<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'UpdateAnswerMapValue') />
               </cfif> 
               
               
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <!---<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />--->
                <cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
                
                  
		 		                             
            <cfcatch TYPE="any">
				
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, INPQID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID, then removes MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
   
  	<cffunction name="ClearXML" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
        <cfargument name="inpClearRules" TYPE="string" default="1"/>
        <cfargument name="inpClearEmail" TYPE="string" default="1"/>
        <cfargument name="inpClearSMS" TYPE="string" default="1"/>
                      
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        

        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
           
            <cftry>                	
                        	            

                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURQID = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "DM/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURQID = "-1">                        
                    </cfif>
          			<cfif CURQID EQ "-1">
	          			<cfset QueryAddRow(dataout) />
				        <!--- All is well --->
	          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
						<!--- Either DM1, DM2, RXSSELE, CCD --->
	                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "DM") /> 
	                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
	                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURQID) /> 
	                    
	                    <!--- Clean up for raw XML --->
	                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
	                    <!--- Insert XML --->
	                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
					</cfif>
            
                </cfloop>

				
                <cfif inpClearRules GT 0>
					<!--- Get the Rules --->              	
                    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES/ELE")>                
                     
                    <cfloop array="#selectedElements#" index="CURRMCIDXML">
                    
                        <cfset CURRULEID = "-1">
                            
                        <!--- Parse for DM data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLBuffDoc = XmlParse("BadData")>                       
                             </cfcatch>              
                        </cftry> 
                    
                        <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/RULES/@RULEID")>
                                
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                            
                        <cfelse>
                            <cfset CURRULEID = "-1">                        
                        </cfif>
                        <cfif CURRULEID EQ "-1">
                            <cfset QueryAddRow(dataout) />
                            <!--- All is well --->
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                            <!--- Either DM1, DM2, RXSSELE, CCD --->
                            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RULES") /> 
                            <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                            <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRULEID) /> 
                            
                            <!--- Clean up for raw XML --->
                            <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                            <!--- Insert XML --->
                            <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
                        </cfif>
                
                    </cfloop>
                </cfif>    
                
                              
              	<!--- DON'T Get the RXSS ELE's --->
                
                
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
				
                <!--- Get the STAGE --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/STAGE")>
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "STAGE") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            	<cfset OutToDBXMLBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                <!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'DM'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetDMs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_VCH#">
                    <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_VCH)#</UL>"> ---> 
                </cfloop>
                
          <!---      <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>"> --->
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#"> --->
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">  
                <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">   --->
             --->
             
             <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS></RXSS>">
			
				<cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RULES>">
				
             	<cfif inpClearRules EQ 0 OR inpClearRules EQ "">
                
                   <!---  <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RULES>')#<UL>"> --->
                    
                    <!--- RULES Next  --->
                    <cfquery dbTYPE="query" name="GetELEs">
                        SELECT 
                            XML_VCH    
                        FROM 
                            dataout
                        WHERE 
                            ELETYPE_VCH = 'RULES'
                        ORDER BY
                            ELEMENTID_INT ASC       
                    </cfquery>
                    
                    <!--- <cfloop query="GetELEs">
                        <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                        <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#">
                    </cfloop> --->
                                   
  
                    <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RULES>')#</UL>"> ---> 
				</cfif>
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RULES>">             
                   
                <!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'CCD'  
                </cfquery>
                
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_VCH#"> 
                <!--- <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_VCH)#</UL>"> --->
				
                <cfquery dbTYPE="query" name="GetSTAGE">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'STAGE'  
                </cfquery>
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetSTAGE.XML_VCH#">
				
				
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                 
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, DISPLAYXML_VCH")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                
                        
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction> 
   
  	<cffunction name="ClearRXSS" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
        <cfargument name="inpClearRules" TYPE="string" default="0"/>
                      
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
           
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<!--- Get the DMs --->
              	
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURQID = "-1">
                    	
					<!--- Parse for DM data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLBuffDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/DM/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                        
                    <cfelse>
                        <cfset CURQID = "-1">                        
                    </cfif>
          			<cfif CURQID EQ "-1">
	          			<cfset QueryAddRow(dataout) />
				        <!--- All is well --->
	          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
						<!--- Either DM1, DM2, RXSSELE, CCD --->
	                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "DM") /> 
	                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
	                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURQID) /> 
	                    
	                    <!--- Clean up for raw XML --->
	                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
	                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
	                    <!--- Insert XML --->
	                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
					</cfif>
            
                </cfloop>

				
                <cfif inpClearRules EQ 0 OR inpClearRules EQ "">
					<!--- Get the Rules --->              	
                    <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES/ELE")>                
                     
                    <cfloop array="#selectedElements#" index="CURRMCIDXML">
                    
                        <cfset CURRULEID = "-1">
                            
                        <!--- Parse for DM data --->                             
                        <cftry>
                              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                             <cfset XMLBuffDoc = XmlParse(#CURRMCIDXML#)>
                               
                              <cfcatch TYPE="any">
                                <!--- Squash bad data  --->    
                                <cfset XMLBuffDoc = XmlParse("BadData")>                       
                             </cfcatch>              
                        </cftry> 
                    
                        <cfset selectedElementsII = XmlSearch(XMLBuffDoc, "/RULES/@RULEID")>
                                
                        <cfif ArrayLen(selectedElementsII) GT 0>
                            <cfset CURRULEID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                    
                            
                        <cfelse>
                            <cfset CURRULEID = "-1">                        
                        </cfif>
                        <cfif CURRULEID EQ "-1">
                            <cfset QueryAddRow(dataout) />
                            <!--- All is well --->
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") /> 
                            <!--- Either DM1, DM2, RXSSELE, CCD --->
                            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "RULES") /> 
                            <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                            <cfset QuerySetCell(dataout, "ELEMENTID_INT", CURRULEID) /> 
                            
                            <!--- Clean up for raw XML --->
                            <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                            <!--- Insert XML --->
                            <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
                        </cfif>
                
                    </cfloop>
                </cfif>    
                
                              
              	<!--- DON'T Get the RXSS ELE's --->
                
                
              
                <!--- Get the CCD --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/CCD")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                	                                  	
					<!--- No need to parse - only one --->                             
                    
                    <cfset QueryAddRow(dataout) />  
                    
					<!--- All is well --->
          			<cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
                    
					<!--- Either DM1, DM2, RXSSELE, CCD --->
                    <cfset QuerySetCell(dataout, "ELETYPE_VCH", "CCD") /> 
                    
                    <!--- 0 for DM1, DM2, CCD - QID for RXSSELE --->
                    <cfset QuerySetCell(dataout, "ELEMENTID_INT", "0") /> 
                    
                    <!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(CURRMCIDXML)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
                    
                    <!--- Insert XML --->
                    <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />                       
            
                </cfloop>
            
            
            	<cfset OutToDBXMLBuff = ""> 
            
            	<!--- Write out local Query ---> 
                
                <!--- DMS First --->
                <cfquery dbTYPE="query" name="GetDMs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'DM'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetDMs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetDMs.XML_VCH#">
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetDMs.XML_VCH)#</UL>"> 
                </cfloop>
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RXSS>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RXSS>')#<UL>">
                
                <!--- RXSS Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RXSS'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#">
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RXSS>">  
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RXSS>')#</UL>">  
             
             
                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "<RULES>"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat('<RULES>')#<UL>">
                
                <!--- RULES Next  --->
                <cfquery dbTYPE="query" name="GetELEs">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'RULES'
                    ORDER BY
                        ELEMENTID_INT ASC       
                </cfquery>
                
                <cfloop query="GetELEs">
                	<cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetELEs.XML_VCH#"> 
                    <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<LI>#HTMLCodeFormat(GetELEs.XML_VCH)#">
                </cfloop>
                               
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "</RULES>">  
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "</UL><LI>#HTMLCodeFormat('</RULES>')#</UL>"> 
                
                   
                <!--- CCD  Last  --->
                <cfquery dbTYPE="query" name="GetCCD">
                    SELECT 
                    	XML_VCH    
                    FROM 
                        dataout
                    WHERE 
                        ELETYPE_VCH = 'CCD'  
                </cfquery>
                
                <!--- no loop - only supposed to be one --->                
                <cfset OutToDBXMLBuff = OutToDBXMLBuff & "#GetCCD.XML_VCH#"> 
                <cfset OutTodisplayxmlBuff = OutTodisplayxmlBuff & "<UL><LI>#HTMLCodeFormat(GetCCD.XML_VCH)#</UL>">
                
                <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
                <cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '&quot;', "'", "ALL")> 
                 
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, DISPLAYXML_VCH")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                
                        
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
   	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
     
 	<cffunction name="UpdateQIDDESC" access="remote" output="false" hint="Change DESCription" returnTYPE="string" returnformat="plain">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="inpNewDESC" TYPE="string" default=""/>
        <cfargument name="INPQID" TYPE="string"/>	 
        <cfargument name="inpOldDESC" TYPE="string" default=""/>
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
        
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Invalid Lib
    
	     --->
          
                           
       	<cfoutput>
                       
        	<!--- Set default to original in case later processing goes bad --->
			<cfset dataout =  '#inpOldDESC#'>  
                               
       
        	<cftry> 
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                                   
                    <!--- All good --->
	                <cfset dataout =  '#inpNewDESC#'>   
                   
                    <!--- Cleanup SQL injection --->
                    
                    <!--- Replace ' with '' --->
                    <cfset inpNewDESC = Replace(inpNewDESC, "'", "''", "ALL")>
                    
             		<cfif LEN(inpNewDESC) GT 60>
	                    <cfset dataout =  '#LEFT(inpNewDESC, 60)# (...)'>
                    <cfelse>
	  	             	<cfset dataout =  '#inpNewDESC#'>
                    </cfif>    
                        
	                <cfset RetVal = UpdateMCIDDESC('#INPBATCHID#', '#INPQID#', '#inpNewDESC#' )>
                        
                  <cfelse>
                    <!--- No user id --->
                    <cfset dataout =  '#inpOldDESC#'>                    
                  </cfif>          
                           
            <cfcatch TYPE="any">
	             <!--- Severe Error - Revert --->
				 <cfset dataout =  '#inpOldDESC#'>                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>

 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
    
   
  	<cffunction name="UpdateMCIDDESC" access="remote" output="false" hint="Replace DESC in MCID in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>	 
        <cfargument name="inpNewValue" TYPE="string"/>  
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>
                     
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           
              	<!--- Get the DMs --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc//RXSS//*[@QID=#INPQID#]")>  
                <cfloop array="#selectedElements#" index="currElement">
                	<cfset currElement.XmlAttributes.DESC = inpNewValue>
                </cfloop>
                                 
                                    
               <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>  
                
               <!--- Write output --->
                
				<!--- Save Local Query to DB --->
                <!--- <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                        simpleobjects.batch
                    SET
                        XMLControlString_vch = '#OutToDBXMLBuff#'
                    WHERE
                        BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>      --->
               
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
				<cfif inpPassBackdisplayxml GT 0>
	            	<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                </cfif>
                <cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
		 		                             
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, INPQID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
        
	<cffunction name="UpdateXMLMCID" access="remote" output="true" hint="Replate XMLControlString">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLSTRING" TYPE="string" default=""/>
		<cfset dataout = ''>
		<cfset OutToDBXMLBuff = ''/>
		<!--- <cfdump var="#XMLSTRING#"> --->
                
		<cfoutput>
			<cftry>
				<cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #XMLSTRING# & "</XMLControlStringDoc>")>
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>          
                     </cfcatch>              
                </cftry>

					<cfset DSUID = Session.USERID>
					<cfset OutToDBXMLBuff = REReplace(trim(XMLSTRING), '"',"'",'ALL') />
					<cfset OutToDBXMLBuff = REReplaceNoCase(OutToDBXMLBuff, "DSUID='\d+'","DSUID='#DSUID#'", "ALL") />
					
                    <!--- Save Local Query to DB --->
                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                        UPDATE
                            simpleobjects.batch
                        SET
                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                        WHERE
                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>  
					<!--- Save History tranglt --->
					<cfset a = History("#INPBATCHID#", "#XMLSTRING#", 'UpdateXMLMCID') />
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, XMLSTRING")> 
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  1) />  
					<cfset QuerySetCell(dataout, "TYPE", 1) />
					<cfset QuerySetCell(dataout, "MESSAGE", "Success!") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "XMLSTRING", "#OutToDBXMLBuff#") />
	            <cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3>
					</cfif>    
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                
	            </cfcatch>
            </cftry> 
		</cfoutput>
		<cfreturn dataout />
	</cffunction>
    <!--- ************************************************************************************************************************* --->
    <!--- input BatchID, QID, and read back DESC value for MCID --->
    <!--- ************************************************************************************************************************* --->
 
    
  	<cffunction name="ReadMCIDDESC" access="remote" output="false" hint="Replace DESC in MCID in existing Control String" returnTYPE="string">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>	 
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
            <cfset CURRDESC = "Description Not Specified">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                                   
				<!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry>                            
              
              	<!--- Get the RXSS ELE's --->
              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS/ELE")>                
                 
                <cfloop array="#selectedElements#" index="CURRMCIDXML">
                
	                <cfset CURRQID = "-1">
                    <cfset CURRRXT = "-1">
                    <cfset CURRRXTDESC = "NA">
                    <cfset CURRDESC = "Description Not Specified">
                                        
                    	
					<!--- Parse for ELE data --->                             
                    <cftry>
                          <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                         <cfset XMLELEDoc = XmlParse(#CURRMCIDXML#)>
                           
                          <cfcatch TYPE="any">
                            <!--- Squash bad data  --->    
                            <cfset XMLELEDoc = XmlParse("BadData")>                       
                         </cfcatch>              
                    </cftry> 
                
	                <cfset selectedElementsII = XmlSearch(XMLELEDoc, "/ELE/@QID")>
                    		
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
                    <cfelse>
                        <cfset CURRQID = "-1">                        
                    </cfif>
          
          			<cfif CURRQID EQ INPQID>
                    
					 	<cfset selectedElementsIII = XmlSearch(XMLELEDoc, "/ELE/@DESC")>
                                
                        <cfif ArrayLen(selectedElementsIII) GT 0>                        
                            <cfset CURRDESC = selectedElementsIII[ArrayLen(selectedElementsII)].XmlValue>                                 
                        <cfelse>
                            <cfset CURRDESC = "Description Not Specified">                        
                        </cfif>
                    
                    </cfif>
                   
                    
                </cfloop>
                          
                
		 		                             
            <cfcatch TYPE="any">
                
				<cfset CURRDESC = "READ ERROR">         
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn CURRDESC />
    </cffunction>
        
    
    

	<!--- assign log  --->
	<cffunction name="History" access="remote" output="true" hint="History">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>
		<cftry>
			<!--- Remove history when add event --->
			<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
				SELECT 
					id 
				FROM 
					simpleobjects.history 
				WHERE 
					flag = 1 
					AND 
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				ORDER BY 
					id ASC 
				LIMIT 0,1
	      	 </cfquery>
	
			<cfif GetHistory.recordCount GT 0>
				<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
					DELETE FROM
						simpleobjects.history
					WHERE
						flag = 1
						AND
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
						id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
				</cfquery>
				<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.history
					SET
						flag = 0
					WHERE
						id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
				</cfquery>
			</cfif>
			<!--- Update history --->
	 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
	            INSERT INTO
	              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
	            VALUES
	            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">,
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
					NOW(),
					<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
					)  
	       </cfquery>	
			<cfreturn 0>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout>
		</cfcatch>
		</cftry>
		
	</cffunction>
    
	<cffunction name="ClearHistory" access="remote" output="false" hint="clear history">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cftry>
			<cfquery name="DeleteHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
			</cfquery>
			<cfreturn 1>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout>
		</cfcatch>
		</cftry>
	</cffunction>
	
	
	<cffunction name="UndoMCID" access="remote" output="true" hint="Undo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cfset STEP = 5>
		
		<cftry>
			
			<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
				<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
				<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
			</cfinvoke>
			
           	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				<!--- have not permission --->
                   
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "") />
				<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
			<cfelse>
			
				<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
					SELECT
						id, XMLControlString_vch, event
					FROM
						simpleobjects.history
					WHERE
						BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						AND
						UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
						AND
							flag = 0
					ORDER BY
						`id` DESC
					LIMIT 0,2
				</cfquery>
		
				<cfif GetHistory.recordCount neq 0>
					
			        <!--- Read from DB Batch Options --->
			        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
			            SELECT                
			              XMLControlString_vch
			            FROM
			              simpleobjects.batch
			            WHERE
			              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			        </cfquery>
		
			        <cfif trim(GetBatchOptions.XMLControlString_vch) eq trim(GetHistory.XMLControlString_vch)>
				        <cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>
						<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 1
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_INTEGER" VALUE="#GetHistory.id[2]#">
						</cfquery>
		
			        <cfelse>
			        	<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
			        </cfif>
		
					<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
						UPDATE
							simpleobjects.history
						SET
							flag = 1
						WHERE
							id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
					</cfquery>
		
					<cfset OutToDBXMLBuff = REReplace(OutToDBXMLBuff,"'",'"',"all")>
			        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
			             UPDATE
			                 simpleobjects.batch
			             SET
			                 XMLControlString_vch = '#OutToDBXMLBuff#'
			             WHERE
			                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			         </cfquery>
			         <cfif GetHistory.recordCount eq 1>
				         <cfset dataout = 1 />
				     </cfif>
				<cfelse>
					<cfset dataout = 0 />
				</cfif>
				<cfset dataout = 2 />
				
			</cfif>
			
			<cfcatch>
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
				
			</cfcatch>
		</cftry>
		<cfreturn dataout />
		
	</cffunction>
	
	<cffunction name="RedoMCID" access="remote" output="false" hint="Redo event on MCID tools">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cftry>
			<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch, event, id
				FROM
					simpleobjects.history
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#SESSION.UserID#">
					AND
					flag = 1
				ORDER BY
					`id` ASC
				LIMIT 0,2
			</cfquery>
			
			<cfif GetHistory.recordcount neq 0>
		        <!--- Read from DB Batch Options --->
		        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
		            SELECT                
		              XMLControlString_vch
		            FROM
		              simpleobjects.batch
		            WHERE
		              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		        </cfquery>
		        
				
				<cfset ID = GetHistory.id>
				<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch>
				<cfif GetBatchOptions.XMLControlString_vch eq GetHistory.XMLControlString_vch>
					<cfset ID2 = GetHistory.id[2]>
					<cfset OutToDBXMLBuff = GetHistory.XMLControlString_vch[2]>
					<cfif ID2 neq 0>
						<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
							UPDATE
								simpleobjects.history
							SET
								flag = 0
							WHERE
								id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID2#">
						</cfquery>
					</cfif>
				</cfif>
		        <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		             UPDATE
		                 simpleobjects.batch
		             SET
		                 XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
		             WHERE
		                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		         </cfquery>
	
				<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
					UPDATE
						simpleobjects.history
					SET
						flag = 0
					WHERE
						id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_DECIMAL" VALUE="#ID#">
				</cfquery>
		         <cfif GetHistory.recordCount eq 1>
			         <cfreturn 1 />
			     </cfif>
			<cfelse>
				<cfreturn 0>
			</cfif>
			<cfreturn GetHistory.recordcount>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
		
	</cffunction>
    
	<cffunction name="changeStageSize" access="remote" output="true" hint="Zoom in for Stage">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cfargument name="stageWidth" Type="string" default="0">
		<cfargument name="stageHeight" Type="string" default="0">

		<cfset DEBUGVAR1 = "0">
		
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			
            <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
             
	            <!--- Parse for data --->                             
	            <cftry>
	                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	                   
	                  <cfcatch TYPE="any">
	                    <!--- Squash bad data  --->    
	                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                 </cfcatch>              
	            </cftry> 
	           	<!--- Get the STAGE 's 
	           	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/STAGE")>--->
	
				<!--- get width stage --->
	            <cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//STAGE")>     		
		
				
				<!---Find and replace key--->
				<cfif ArrayLen(selectedElementsII) GT 0>
					<cfset selectedElementsII[1].XmlAttributes["w"] = stageWidth />
				    <cfset selectedElementsII[1].XmlAttributes["h"] = stageHeight />
				    <cfset GetXMLBuff = ToString(selectedElementsII[1]) />
				</cfif>





	            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>
	            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "","ALL")>   
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "","ALL")>              
	            
	            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>

		         <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		             UPDATE
		                 simpleobjects.batch
		             SET
		                 XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
		             WHERE
		                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		         </cfquery>
	         
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, DEBUGVAR1")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "DEBUGVAR1", "#DEBUGVAR1#") />
				
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            </cfcatch>
            
        </cftry>
		<cfreturn dataout>
	</cffunction>
	
	<cffunction name="changeStageZoom" access="remote" output="true" hint="Zoom in for Stage">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cfargument name="stageZoomScale" Type="string" default="0">

		<cfset DEBUGVAR1 = "0">
		<cfset Width = "0">
		<cfset Height = "0">
		<cftry>
			<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
				SELECT
					XMLControlString_vch
				FROM
					simpleobjects.batch
				WHERE
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			</cfquery>
			
            <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
             
	            <!--- Parse for data --->                             
	            <cftry>
	                  <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                 <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	                   
	                  <cfcatch TYPE="any">
	                    <!--- Squash bad data  --->    
	                    <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                 </cfcatch>              
	            </cftry> 
	           	<!--- Get the STAGE 's 
	           	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/STAGE")>--->
	
				<!--- get width stage --->
	            <cfset selectedElementsII = XmlSearch(myxmldocResultDoc, "//STAGE")>     		
		
				
				<!---Find and replace key--->
				<cfif ArrayLen(selectedElementsII) GT 0>
					<cfset selectedElementsII[1].XmlAttributes["s"] = stageZoomScale />
					<cfset Width = selectedElementsII[1].XmlAttributes["w"]>
					<cfset Height = selectedElementsII[1].XmlAttributes["h"]>
				    <cfset GetXMLBuff = ToString(selectedElementsII[1]) />
				</cfif>

	            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>
	            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "","ALL")>   
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "","ALL")>              
	            
	            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
				<cfset OutToDBXMLBuff = trim(OutToDBXMLBuff)>
		         <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		             UPDATE
		                 simpleobjects.batch
		             SET
		                 XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
		             WHERE
		                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		         </cfquery>
	         
                <cfset dataout = QueryNew("RXRESULTCODE, XML_VCH, DISPLAYXML_VCH, WIDTH, HEIGHT, SCALE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				<cfset QuerySetCell(dataout, "XML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutToDBXMLBuff#") />
				<cfset QuerySetCell(dataout, "WIDTH", "#Width#") />
				<cfset QuerySetCell(dataout, "HEIGHT", "#Height#") />
				<cfset QuerySetCell(dataout, "SCALE", "#stageZoomScale#") />
				
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
            </cfcatch>
            
        </cftry>
		<cfreturn dataout>
	</cffunction>
  	
	<cffunction name="getValueSwitch" access="remote" output="true" hint="get value option in switch of type 21">
		<cfargument name="INPBATCHID" Type="string" default="0">
		<cfargument name="INPQID" Type="string" default="0">

		<cfset selectedElements = "">
		<cfset CK2LOCATION = "LocationKey7_vch">
		<cftry>
			<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
                  
			<!--- Read from DB Batch Options --->
		    	<cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                 SELECT                
	                   XMLControlString_vch
	                 FROM
	                   simpleobjects.batch
	                 WHERE
	                   BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	             </cfquery>     
	                 
				<cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>             
	        </cfif>  
	
	        <!--- Parse for data --->                             
	        <cftry>
	              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	               
	              <cfcatch TYPE="any">
	                <!--- Squash bad data  --->    
	                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	             </cfcatch>              
	        </cftry> 
	      	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//SWITCH")>
	      	<cfloop array="#selectedElements#" index="CURRMCIDXML">
			
		        <cfset selectedElementsII = XmlSearch(CURRMCIDXML, "@QID")>
				<cfif ArrayLen(selectedElementsII) GT 0>
	                <cfset CURRQID = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                 
	            <cfelse>
	                <cfset CURRQID = "-1">                        
	            </cfif>
				<cfif INPQID EQ CURRQID>
			        <cfset selectedElementsII = XmlSearch(CURRMCIDXML, "@CK2")>
					<cfif ArrayLen(selectedElementsII) GT 0>
		                <cfset CK2LOCATION = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>                                                   
		            </cfif>
				</cfif>
			</cfloop>
			
			<cfquery name="GetLocaltionKey" datasource="#Session.DBSourceEBM#">
				SELECT
					distinct #CK2LOCATION# as LocaltionKey
				FROM
					simplelists.rxmultilist
			</cfquery>
			<cfset dataout =  QueryNew("LOCALTIONKEY")> 
	        
			<cfloop query="GetLocaltionKey">
				<cfif GetLocaltionKey.LocaltionKey NEQ 0>
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "LOCALTIONKEY", #GetLocaltionKey.LocaltionKey#) />
				</cfif>
				
			</cfloop>
			<cfreturn dataout>
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
        
	</cffunction>
	<!--- Retrieve custom field data  --->
	<cffunction name="RetrieveCustomFieldData" access="remote" output="false" hint="Retrieve custom field data based on location key">
		<cfargument name="locationKey" TYPE="string" default="CustomField5_int"/>
		<cfset var customFieldData = ArrayNew(1)>
		<cftry>
			<cfquery name="getCustomfieldData" datasource="#Session.DBSourceEBM#">
				SELECT  
					DISTINCT #locationKey# AS LocationKey
				FROM
					simplelists.rxmultilist
			</cfquery>	
			
			<cfloop query="getCustomfieldData">
				<cfset ArrayAppend(customFieldData, #getCustomfieldData.LocationKey#)>
			</cfloop>
			<cfset dataout =  QueryNew("CUSTOMFIELD")> 
	        <cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "CUSTOMFIELD", #customFieldData#) />
			<cfreturn dataout />
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
		
	</cffunction>
	
	<cffunction name="RetrieveCustomField1Data" access="remote" output="false" hint="Retrieve data customfield 1">
		<cfargument name="arrChoseSwitch" Type="string" default="-100">
		<cfset var customField1Data = ArrayNew(1)>
		<cftry>
			<cfquery name="getCustomfieldData" datasource="#Session.DBSourceEBM#">
				SELECT  
					CustomField1_int
				FROM
					simplelists.rxmultilist
				WHERE 
				CustomField1_int NOT IN (#arrChoseSwitch#)
			</cfquery>	
			<cfloop query="getCustomfieldData">
				<cfset ArrayAppend(customField1Data, #getCustomfieldData.CustomField1_int#)>
			</cfloop>
			<cfset dataout =  QueryNew("CUSTOMFIELD1")> 
	        <cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "CUSTOMFIELD1", #customField1Data#) />
			<cfreturn dataout />
		<cfcatch>
			<cfif cfcatch.errorcode EQ "">
				<cfset cfcatch.errorcode = -3>
			</cfif>    
			<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE ")> 
			<cfset QueryAddRow(dataout) />
			<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
			<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
			<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
			<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
			<cfreturn dataout />
		</cfcatch>
		</cftry>
		
	</cffunction>
	
	<cffunction name="changeAnswerMap" access="remote" output="false" hint="">
		
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="inpdest0" TYPE="string" default="0"/>
		<cfargument name="inpdest1" TYPE="string" default="0"/>
		<cfargument name="inpdest2" TYPE="string" default="0"/>
		<cfargument name="inpdest3" TYPE="string" default="0"/>
		<cfargument name="inpdest4" TYPE="string" default="0"/>
		<cfargument name="inpdest5" TYPE="string" default="0"/>
		<cfargument name="inpdest6" TYPE="string" default="0"/>
		<cfargument name="inpdest7" TYPE="string" default="0"/>
		<cfargument name="inpdest8" TYPE="string" default="0"/>
		<cfargument name="inpdest9" TYPE="string" default="0"/>
		<cfargument name="inpdest10" TYPE="string" default="0"/>
		<cfargument name="inpdest11" TYPE="string" default="0"/>
		<cfargument name="inpdest12" TYPE="string" default="0"/>
		
		<cfset dataout = ''>

		<cfif inpdest0 GT 0>
			<cfset dataout = dataout & '(0,'& #inpdest0# &'),'>
		</cfif>
		<cfif inpdest1 GT 0>
			<cfset dataout = dataout & '(1,'& #inpdest1# &'),'>
		</cfif>
		<cfif inpdest2 GT 0>
			<cfset dataout = dataout & '(2,'& #inpdest2# &'),'>
		</cfif>
		<cfif inpdest3 GT 0>
			<cfset dataout = dataout & '(3,'& #inpdest3# &'),'>
		</cfif>
		<cfif inpdest4 GT 0>
			<cfset dataout = dataout & '(4,'& #inpdest4# &'),'>
		</cfif>
		<cfif inpdest5 GT 0>
			<cfset dataout = dataout & '(5,'& #inpdest5# &'),'>
		</cfif>
		<cfif inpdest6 GT 0>
			<cfset dataout = dataout & '(6,'& #inpdest6# &'),'>
		</cfif>
		<cfif inpdest7 GT 0>
			<cfset dataout = dataout & '(7,'& #inpdest7# &'),'>
		</cfif>
		<cfif inpdest8 GT 0>
			<cfset dataout = dataout & '(8,'& #inpdest8# &'),'>
		</cfif>
		<cfif inpdest9 GT 0>
			<cfset dataout = dataout & '(9,'& #inpdest9# &'),'>
		</cfif>
		<cfif inpdest10 GT 0>
			<cfset dataout = dataout & '(-13,'& #inpdest10# &'),'>
		</cfif>
		<cfif inpdest11 GT 0>
			<cfset dataout = dataout & '(-6,'& #inpdest11# &'),'>
		</cfif>
		<cfif inpdest12 GT 0>
			<cfset dataout = dataout & '(-1,'& #inpdest12# &'),'>
		</cfif>
		<cfif right(dataout,1) EQ ",">
			<cfset dataout = left(dataout,len(dataout)-1) />
		</cfif>
		<cfreturn dataout>
	</cffunction>
</cfcomponent>
