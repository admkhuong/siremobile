

<!--- No display --->
<cfcomponent output="false">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off DEBUGging --->
    <cfsetting showDEBUGoutput="no" />
	<cfinclude template="../../../../public/paths.cfm" >
	<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
	<cfinclude template="ScriptsExtend.cfm">

    
    	
    <!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="#DBSourceEBM#"/> 
    <cfparam name="Session.UserID" default="10"/> 
  
    <cffunction name="AddNewQID" access="remote" output="true" hint="Add a new MCID to the existing Control String">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpMCIDTYPE" TYPE="string"/>	 <!--- MCID TYPE to add --->
        <cfargument name="inpXPOS" TYPE="string" default="0"/>
        <cfargument name="inpYPOS" TYPE="string" default="0"/>
        <cfargument name="maxQID" TYPE="string" default="0"/>
        
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
         <cfset NEXTCURRQID = '-1' />  
		<cfset CURRQID = '-1' />            
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
            <cftry>                	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                   
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<!--- have not permission --->
                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
				  
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	
					<cfset NEXTQID = maxQID+1>
	            	<!--- Add a record for the new XML - use the rxt/genrxt(x).cfm to build defaults --->   
	            
	                <cfswitch expression="#inpMCIDTYPE#">
	 					<cfcase value="1"><cfset RetVal = genrxt1XML(NEXTQID)></cfcase>
	                    <cfcase value="2"><cfset RetVal = genrxt2XML(NEXTQID)></cfcase>
	                    <cfcase value="3"><cfset RetVal = genrxt3XML(NEXTQID)></cfcase>
	                    <cfcase value="4"><cfset RetVal = genrxt4XML(NEXTQID)></cfcase>
	                    <cfcase value="5"><cfset RetVal = genrxt5XML(NEXTQID)></cfcase>
	                    <cfcase value="6"><cfset RetVal = genrxt6XML(NEXTQID)></cfcase>
	                    <cfcase value="7"><cfset RetVal = genrxt7XML(NEXTQID)></cfcase>
	                    <cfcase value="8"><cfset RetVal = genrxt8XML(NEXTQID)></cfcase>
	                    <cfcase value="9"><cfset RetVal = genrxt9XML(NEXTQID)></cfcase>
	                    <cfcase value="10"><cfset RetVal = genrxt10XML(NEXTQID)></cfcase>
	                    <cfcase value="11"><cfset RetVal = genrxt11XML(NEXTQID)></cfcase>
	                    <cfcase value="12"><cfset RetVal = genrxt12XML(NEXTQID)></cfcase>
	                    <cfcase value="13"><cfset RetVal = genrxt13XML(NEXTQID)></cfcase>
	                    <cfcase value="14"><cfset RetVal = genrxt14XML(NEXTQID)></cfcase>
	                    <cfcase value="15"><cfset RetVal = genrxt15XML(NEXTQID)></cfcase>
	                    <cfcase value="16"><cfset RetVal = genrxt16XML(NEXTQID)></cfcase>
	                    <cfcase value="17"><cfset RetVal = genrxt17XML(NEXTQID)></cfcase>
	                    <cfcase value="18"><cfset RetVal = genrxt18XML(NEXTQID)></cfcase>
	                    <cfcase value="19"><cfset RetVal = genrxt19XML(NEXTQID)></cfcase>
	                    <cfcase value="20"><cfset RetVal = genrxt20XML(NEXTQID)></cfcase>
						<cfcase value="21"><cfset RetVal = genrxt21XML(NEXTQID)></cfcase>
						<cfcase value="22"><cfset RetVal = genrxt22XML(NEXTQID)></cfcase>
						<cfcase value="24"><cfset RetVal = genrxt24XML(NEXTQID)></cfcase>
	                </cfswitch>
	
		                <cfif LEN(RetVal.RAWXML) GT 0>                
		            		<cfset GetXMLBuff = XmlParse(RetVal.RAWXML) >
		            		<cfset genEleArr = XmlSearch(GetXMLBuff, "/*") >
		            		<cfset generatedElement = genEleArr[1] >
		            		<cfset generatedElement.XmlAttributes.X = "#inpXPOS#">
							<cfset generatedElement.XmlAttributes.Y = "#ceiling(inpYPOS)#">
	
		            		<cfset rxssDoc = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>
		            		<cfset XmlAppend(rxssDoc[1],generatedElement) />
	
							<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
							<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
							<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
							<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>
							
		                    <!--- Save Local Query to DB --->
		                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
		                        UPDATE
		                            simpleobjects.batch
		                        SET
		                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
		                        WHERE
		                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
		                    </cfquery>     
		                
							<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'AddNewQID') />
							
							<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, NEXTQID")>   
		                    <cfset QueryAddRow(dataout) />
		                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
		                    <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
		                    <cfset QuerySetCell(dataout, "NEXTQID", "#NEXTQID#") />
		                  
		                     
						<cfelse>
		                	<!--- Error generating new MCID --->
		                	<cfthrow MESSAGE=" Error generating new MCID" TYPE="Any" extendedinfo="" errorcode="-4">
		                </cfif>   
		         
		        </cfif>              
	
	            <cfcatch TYPE="any">
	                <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>                    
	                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#Session.DBSourceEBM# #cfcatch.detail#") />            
	            
	            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    	
 	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, upade position values  --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="UpdateObjXYPOS" access="remote" output="false" hint="Replace LINK in answer map in MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>	 
        <cfargument name="inpXPOS" TYPE="string"/>	 
        <cfargument name="inpYPOS" TYPE="string"/>

         <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
				
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, INPDESC, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
					
				<cfelse>  
				   
	                <!--- <cfscript> sleep(3000); </cfscript>  --->
	                              
	                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                     
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT                
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                    
	                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
						
					<cfelse>
	                
		                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
	                
	                </cfif>  
	
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	
	               	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//*[@QID=#INPQID#]")>
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.X = "#inpXPOS#">
					<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes.Y = "#ceiling(inpYPOS)#">
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>
	               <!--- Write output --->
	               <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
	                 
						<!--- Save Local Query to DB --->
	                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>
						<cfset a = History(INPBATCHID,OutToDBXMLBuff,'UpdateObjXYPOS')>
	               </cfif> 
	               
	
	                <cfset dataout = QueryNew("RXRESULTCODE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
           		
				</cfif>                   
                  
	            <cfcatch TYPE="any">
					<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	                </cfif>    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
	            </cfcatch>
            
            </cftry>     
     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<cffunction name="GetStageDemension" access="remote" output="true" hint="Replace LINK in answer map in MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
       

         <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, WIDTH, HEIGHT")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>                	
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLControlString_vch
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
					
				<cfelse>
                
	                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                
                </cfif>  
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 

               	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//STAGE")>
				<cfif ArrayLen(selectedElements) EQ 0>
					<cfset widthStage = 1200>
					<cfset heightStage = 900>
					<cfset scaleStage = 1>
						<cfset stage = "<STAGE w='"&widthStage&"' h='"&heightStage&"' s='"&scaleStage&"'>0</STAGE>">

						<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
						
						<cfset OutToDBXMLBuff = OutToDBXMLBuff & stage>
						<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
						
	                    <!--- Save Local Query to DB --->
	                     <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>
						
				<cfelse>
					<cfset widthStage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.w>
					<cfset heightStage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.h>
					<!--- <cfset selectedElementsII = XmlSearch(selectedElements, "@s")> --->

					<cfif #StructKeyExists(selectedElements[ArrayLen(selectedElements)].XmlAttributes,'s')#>
						<cfset scaleStage = selectedElements[ArrayLen(selectedElements)].XmlAttributes.s>
					<cfelse>
						<cfset scaleStage = 1>
						<cfset selectedElements[ArrayLen(selectedElements)].XmlAttributes["s"] = scaleStage />		
									
			            <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc)>
			            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<XMLControlStringDoc>", "","ALL")>   
						<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "</XMLControlStringDoc>", "","ALL")>              
			            
			            <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "\r\n", "")>
						<cfset OutToDBXMLBuff = trim(OutToDBXMLBuff)>
				         <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
				             UPDATE
				                 simpleobjects.batch
				             SET
				                 XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
				             WHERE
				                 BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
				         </cfquery>
					</cfif>
				</cfif>
				
                <cfset dataout = QueryNew("RXRESULTCODE, WIDTH, HEIGHT, SCALE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "WIDTH", "#widthStage#") />
				<cfset QuerySetCell(dataout, "HEIGHT", "#heightStage#") />                         
                <cfset QuerySetCell(dataout, "SCALE", "#scaleStage#") />   
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            
            </cftry>     
     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!--- ************************************************************************************************************************* --->
	<!--- Delete a MCID, update other MCID's CK --->
    <!--- input BatchID, DeleteQID, then rmoves and then reorders MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="DeleteQID" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="INPDELETEQID" TYPE="string"/>	 <!--- QID to start with --->
                      
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
            <cftry>                	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>        	            
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<cfset xmlRxssElement =  XmlSearch(myxmldocResultDoc,"//RXSS") >
				<cfset xmlRxss =  xmlRxssElement[1] >
				
				<!---Search element which is deleted--->
				<cfset elementDeleted = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #INPDELETEQID# ]") />
				<cfif ArrayLen(elementDeleted) GT 0 >
					<cfset parentNode = elementDeleted[1].XmlParent />
					<cfset deletedTagName = elementDeleted[1].XmlName >
					<cfif deletedTagName EQ 'ELE' OR deletedTagName EQ 'CONV'>
						<!--- check position in stage (Has switch or not)--->
						<!--- get parentName --->
						<cfset parentTagName = parentNode.XmlName >
						<cfif parentTagName EQ "RXSS">
							<!--- Check switch case in it --->
							<cfset findSwitchChild = XmlSearch(elementDeleted[1], "SWITCH") >
							<!--- Has switch in it (Copy switch to RXSS then Delete delection element)---->
							<cfif ArrayLen(findSwitchChild) GT 0>
								<cfloop array="#findSwitchChild#" index="currSwitch">
									<cfset ArrayAppend(xmlRxss[1].XmlChildren, currSwitch) >  
								</cfloop>
							</cfif>
							<cfset XmlDeleteNodes(myxmldocResultDoc,elementDeleted[1]) />
						</cfif>
						<cfif parentTagName EQ "CASE" OR parentTagName EQ "DEFAULT">
							<cfset switchNode = parentNode.XmlParent>
							<!--- check if ELE(inside SWITCH) has SWITCH inside it(<CASE><ELE><SWITCH></SWITCH></ELE></CASE>) then move SWITCH outside --->
						    <cfset findSwitchChildInELE = XmlSearch(elementDeleted[1], "SWITCH") >
						    <cfif ArrayLen(findSwitchChildInELE) GT 0>
								<cfloop array="#findSwitchChildInELE#" index="currSwitch">
									<cfset ArrayAppend(xmlRxss[1].XmlChildren, currSwitch) >  
								</cfloop>
							</cfif>
							<cfset XmlDeleteNodes(myxmldocResultDoc,parentNode) />
							<cfif ArrayLen(switchNode.XmlChildren) EQ 0>
								<cfset switchNode.XmlText = '0'>
							</cfif>
						</cfif>
					<cfelseif deletedTagName EQ 'SWITCH'>
					    <!--- get all element inside swich before delete switch --->
			            <cfset childOfSwitch = XmlSearch(elementDeleted[1] ,"*/*") />
						<cfset XmlDeleteNodes(myxmldocResultDoc, elementDeleted) />
						<!--- copy all element inside SWITCH delteted to RXSS --->
						<cfloop array="#childOfSwitch#" index="currELE">
							<cfset ArrayAppend(xmlRxss.XmlChildren, currELE) >  
						</cfloop>
					</cfif>
					<cfif parentNode.XmlName NEQ 'RXSS'>
						<cfif ArrayLen(parentNode.XmlChildren) EQ 0>
							<cfset parentNode.XmlText = '0'>
						</cfif>
					</cfif>
					<!--- update next QID of ELE has next QID point to ELE deleted --->
					<cfset eleHasQid = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID ]") />	
				   	<cfloop index="node" array="#eleHasQid#">
					   	<cfif node.XmlName NEQ 'CONV'>
						   	<cfif node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] EQ INPDELETEQID>		   	
								<cfset node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] = -1> 	 	
						   	</cfif>					
					   	</cfif>
		     	   </cfloop>
				</cfif>
				<!--- if rxt 2 has yellow line(CK6) or red line(CK7) or Answer Map(CK4)point to MCID obj deleted need to reset them--->			
     	  		   <cfset updateYellowRedLine = XmlSearch(myxmldocResultDoc, "//*[ @rxt ='2']") />					    
				   <cfloop index="node" array="#updateYellowRedLine#"> 
	     	          	<cfif node.XmlAttributes["CK6"] EQ INPDELETEQID>  
	     	          		<cfset node.XmlAttributes["CK6"]=-1> 	 	
					    </cfif>	
	     	            <cfif node.XmlAttributes["CK7"] EQ INPDELETEQID>  
     					   	<cfset node.XmlAttributes["CK7"]=-1>				
					    </cfif>	
					    <!---  update Anwser Map for rxt 2  --->
					    <cfset ans=node.XmlAttributes["CK4"]>
					    	<cfset RegExpA = "\(\d\," & INPDELETEQID & "\)\,?">
							<cfif Find("," & "#INPDELETEQID#" & ")", ans)>
										<cfset ans=REReplace(ans,RegExpA,"", "ALL" )>
										<cfset node.XmlAttributes["CK4"]=ans>	
							</cfif> 	
							<!--- Set CK4="0" if connection number = 0 --->
							<cfif ans  EQ ''>
								<cfset ans = '0'>	
							</cfif>	
							<cfset node.XmlAttributes["CK4"]=ans>		
		     	   </cfloop>	
		     	   <!--- if Switch on Call State object has CK1(Live) or CK2(Machine) point to MCID obj deleted need to reset them--->			
     	  		   <cfset updateLiveMachineState = XmlSearch(myxmldocResultDoc, "//*[ @rxt ='24']") />					    
				   <cfloop index="node" array="#updateLiveMachineState#"> 
	     	          	<cfif node.XmlAttributes["CK1"] EQ INPDELETEQID>  
	     	          		<cfset node.XmlAttributes["CK1"]=-1> 	 	
					    </cfif>	
	     	            <cfif node.XmlAttributes["CK2"] EQ INPDELETEQID>  
     					   	<cfset node.XmlAttributes["CK2"]=-1>				
					    </cfif>		
		     	   </cfloop>		     	
				<!--- update QID of MCID object --->
				   <cfset eleUpdateQID = XmlSearch(myxmldocResultDoc, "//*[ @QID > '#INPDELETEQID#' ]") />
				   <cfloop index="node" array="#eleUpdateQID#">
				        <cfset  currQID = node.XmlAttributes.QID>
						<cfset  node.XmlAttributes.QID = node.XmlAttributes.QID -1 >
						<!--- update CK6 for rxt 2	  --->			
						<cfset  eleUpdateCK6 = XmlSearch(myxmldocResultDoc, "//*[ @RXT='2' and @CK6 = '#currQID#']") />
				    	<cfloop array="#eleUpdateCK6#" index="elecurr">
					   		 <cfset elecurr.XmlAttributes["CK6"] = node.XmlAttributes.QID>      
						</cfloop>	 
						<!--- update CK7 for rxt 2	  --->
						<cfset  eleUpdateCK7 = XmlSearch(myxmldocResultDoc, "//*[ @RXT='2' and @CK7 = '#currQID#' ]") />
					    <cfloop array="#eleUpdateCK7#" index="elecurr">
						    <cfset elecurr.XmlAttributes["CK7"] = node.XmlAttributes.QID>      
						</cfloop>
						<!--- update CK1(Live State) for rxt 24	  --->			
						<cfset  eleUpdateCK1 = XmlSearch(myxmldocResultDoc, "//*[ @RXT='24' and @CK1 = '#currQID#']") />
				    	<cfloop array="#eleUpdateCK1#" index="elecurr">
					   		 <cfset elecurr.XmlAttributes["CK1"] = node.XmlAttributes.QID>      
						</cfloop>
						<!--- update CK2(Machine State) for rxt 24	  --->			
						<cfset  eleUpdateCK2 = XmlSearch(myxmldocResultDoc, "//*[ @RXT='24' and @CK2 = '#currQID#']") />
				    	<cfloop array="#eleUpdateCK2#" index="elecurr">
					   		 <cfset elecurr.XmlAttributes["CK2"] = node.XmlAttributes.QID>      
						</cfloop>		 
						<!--- 	update Answer Map 	  --->
						<cfset  eleUpdateAns = XmlSearch(myxmldocResultDoc, "//*[ @RXT='2']") />
					    <cfloop array="#eleUpdateAns#" index="elecurr">
				 	        <cfset ans=elecurr.XmlAttributes["CK4"]>
							<cfif Find("," & "#currQID#" & ")", ans)>
									<cfset ans=Replace(ans,"," & "#currQID#" & ")","," & "#node.XmlAttributes.QID#" & ")", "ALL" )>
									<cfset elecurr.XmlAttributes["CK4"]=ans>	
							</cfif> 	 
						</cfloop>
		     	   </cfloop>
				<!--- Update Next QID --->
				<cfset eleUpdateNextQID = XmlSearch(myxmldocResultDoc, "//RXSS//ELE[ @QID ]") >
				<cfloop array="#eleUpdateNextQID#" index="element">
					<cfset  rxt = element.XmlAttributes.rxt>			
					<cfif element.XmlAttributes["#NextQID(rxt)#"] GT INPDELETEQID >
					    <cfset element.XmlAttributes["#NextQID(rxt)#"] = element.XmlAttributes["#NextQID(rxt)#"] - 1>  	    
					</cfif>
				</cfloop>
				
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfset OutToDBXMLBuff = UpdateVoiceObjectPosition(OutToDBXMLBuff)>
				
				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
				
				<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'DeleteQID') />
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETEQID")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPDELETEQID", "#INPDELETEQID#") />
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPDELETEQID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPDELETEQID", "#INPDELETEQID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            </cftry>     
        <cfreturn dataout />
    </cffunction>
	
	<!--- ************************************************************************************************************************* --->
    <!--- input Batch ID and QUID and XML String then replace existing RXSS ELE in main control string --->
    <!--- ************************************************************************************************************************* --->
    
	 <cffunction name="SaveMCIDObj" access="remote" output="false" hint="Replace Existing DM, RXSS ELE, OR CCD in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string" default="0"/>
        <cfargument name="INPQID" TYPE="string" default="0"/>
        <cfargument name="inpTYPE" TYPE="string" default="None"/> <!--- DM, RXSS, CCD --->
        <cfargument name="inpXML" TYPE="string" default="NADA"/>
                       
        <cfset var dataout = '0' />    
                 
       	<cfoutput>
        
            <cftry>                	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
			
				<cfset editElementArr = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID='#INPQID#']") >
				
				<cfset editElement = editElementArr[1] >
				
				<cfset xPos = editElement.XmlAttributes.X >
				<cfset yPos = editElement.XmlAttributes.Y >
				
				<!--- Clear old attribute---->
				<cfset StructClear(editElement.XmlAttributes)>
				<!--- Clear all old child except for switch element---->
				<cfset switchChildArr = ArrayNew(1)>
					
               	<cfloop array="#editElement.XmlChildren#" index="child">
					<cfif child.XmlName EQ "SWITCH" OR child.XmlName EQ "CASE" OR child.XmlName EQ "DEFAULT">
						<cfset ArrayAppend(switchChildArr, child)>
					</cfif>
				</cfloop>
				
				<cfset ArrayClear(editElement.XmlChildren) >
				<!--- Copy switch child --->
				<cfif ArrayLen(switchChildArr) GT 0>
					<cfloop array="#switchChildArr#" index="switchChild">
						<cfset ArrayAppend(editElement.XmlChildren, switchChild) >
					</cfloop>
				</cfif>
				
				 <cftry>
                      <cfset GetXMLBuff = XmlParse("<Xmlgen>" & inpXML & "</Xmlgen>") >	
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset GetXMLBuff = XmlParse("<Xmlgen>BadData</Xmlgen>")>                       
                     </cfcatch>              
                </cftry> 
				
           		<cfset genEleArr = XmlSearch(GetXMLBuff, "/Xmlgen/*") >
           		<cfset generatedElement = genEleArr[1] > 
			 	<!--- Save attribute --->
			 	<cfset editElement.XmlAttributes = generatedElement.XmlAttributes>
			 	<cfset editElement.XmlAttributes.X = xPos>
			 	<cfset editElement.XmlAttributes.Y = yPos>
			 	<!----Save All child ---->
			 	<cfloop array="#generatedElement.XmlChildren#" index="genChild">
					<cfset XmlAppend(editElement, genChild)>
				</cfloop>
				
				<!--- Add 0 to end tag if it has no children --->
				<cfif ArrayLen(editElement.XmlChildren) EQ 0>
					<cfset editElement.XmlText = generatedElement.XmlText >
				<cfelse>	
					<cfset editElement.XmlText = "">
				</cfif>
				
              	<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#TRIM(OutToDBXMLBuff)#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#"> 
                </cfquery>     
		  		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            </cfcatch>
            </cftry>      
			</cfoutput>
	        <cfreturn dataout />
	  </cffunction>
	  	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString , QID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
	<cffunction name="UpdateKeyValue" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpRXSS" TYPE="string"/>
	    <cfargument name="INPQID" TYPE="string"/>
		<cfargument name="inpKey" TYPE="string"/><!--- inpKey CK6,CK7(Update for rxt 2)inpKey CK1,CK2(Update Live/Machine State)--->
        <cfargument name="inpNewValue" TYPE="string"/>  
		<cfargument name="caseValue" TYPE="string" default="default"/>
                      
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
            <cftry>  
			
				<!---check permission--->
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<cfset dataout =  QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>   
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
					<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />  
                    <cfset QuerySetCell(dataout, "TYPE", "-2") />
					<cfset QuerySetCell(dataout, "MESSAGE", checkBatchPermissionByBatchId.message) />                
                    <cfset QuerySetCell(dataout, "ERRMESSAGE", "") /> 
				<cfelse>
					    
			     	<cfif inpNewValue EQ "" OR inpNewValue LT 1>
	                	<cfset inpNewValue = "-1">                
	                </cfif>        
					<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT                
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>     
	                    
	                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
					<cfelse>
		                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
	                </cfif>               
					
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
					
					<cfset xmlRxssElement =  XmlSearch(myxmldocResultDoc,"//RXSS") >
					<cfset xmlRxss =  xmlRxssElement[1] >
					<!---Search element which need to update data--->
					<cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #INPQID# ]") />	
					<cfset MCIDEnd = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID = #inpNewValue# ]") />		  	  	
					<cfif ArrayLen( MCIDStart) GT 0 AND  ArrayLen( MCIDEnd) GT 0 >
						<cfset MCIDStartTagName=MCIDStart[1].XmlName>
						<cfset MCIDEndTagName=MCIDEnd[1].XmlName>
						
						<!--- ELE/CONV to ELE  --->
						<cfif MCIDStartTagName EQ 'ELE' OR MCIDStartTagName EQ 'CONV'>
						    <!--- update CK6,CK7 of rxt 2 point to target ELE --->
						    <cfif inpKey  EQ 'CK6' OR inpKey EQ 'CK7' OR inpKey EQ 'CK1' OR  inpKey EQ 'CK2'>
						        <cfset MCIDStart[1].XmlAttributes["#inpKey#"] = inpNewValue>
						    <cfelse>
						        <!--- update next QID of Start ELE point to target ELE --->
						    	<cfset MCIDStart[1].XmlAttributes["#NextQID(MCIDStart[1].XmlAttributes.rxt)#"] = inpNewValue> 
							</cfif>
	   					    
				 	     
							<!--- check if exists SWITCH inside ELE move it outside --->
							<cfset switchInEle = XmlSearch(MCIDStart[1],"*[ @QID = #inpNewValue# ]")>
							<cfif ArrayLen(switchInEle) GT 0>
								<cfset ArrayAppend(xmlRxss[1].XmlChildren, switchInEle[1]) >  
								<cfset XmlDeleteNodes(myxmldocResultDoc,switchInEle[1]) />
							</cfif>		
							<!--- if no ELE inside ELE start after set text =0 --->
						    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
								<cfset MCIDStart[1].XmlText = '0'>
							</cfif>
							
	
	                        <!---Start is ELE to end is SWITCH ,copy SWITCH inside ELE and delete it outside ELE--->
							<cfif MCIDEndTagName EQ 'SWITCH'>				
								<!---  must be check SWITCH has ELE parents or not --->
							    <cfset EleParentOfSwitch=MCIDEnd[1].XmlParent>	
							    <cfif EleParentOfSwitch.XmlName EQ 'ELE'>
								     <cfset EleParentOfSwitch.XmlAttributes["#NextQID(EleParentOfSwitch.XmlAttributes.rxt)#"] = -1>	
							    </cfif>	
							    <!--- remove text of ELE  before add SWITCH inside it --->
								<cfset MCIDStart[1].XmlText = ''>    
								<!---  first append to ELE start then delete SWITCH outside --->
								<cfset XmlAppend(MCIDStart[1], MCIDEnd[1]) />	
							    <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) />
							        <!--- if no ELE inside ELE start then set text =0 --->
							    <cfif ArrayLen(EleParentOfSwitch.XmlChildren) EQ 0>
									<cfset EleParentOfSwitch.XmlText = '0'>
								</cfif>	
						   </cfif>
	
					   <cfelseif MCIDStartTagName EQ 'SWITCH'>
							<!--- create new CASE/DEFAULT tag --->
							<cfset EleString= Replace(MCIDEnd[1], "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
										
							<cfif caseValue EQ ''>							 
									<cfset EleString = "<DEFAULT>"&EleString&"</DEFAULT>">                  
							<cfelse>
								 	<cfset EleString = "<CASE VAL='"&caseValue&"'>"&EleString&"</CASE>">
							</cfif>	
							
							<cfset EleString=XmlParse(EleString)>
							<cfset genEleArr = XmlSearch(EleString, "/*") >
						    <!--- remove text of SWITCH  before add ELE inside it --->
						    <cfset MCIDStart[1].XmlText = ''>    
							<!--- check ELE/CONV exists in SWITCH or not --->
							<cfset eleInSwitch = XmlSearch(MCIDStart[1], "*/*[ @QID = #inpNewValue# ]") >
						
							<cfif Arraylen(eleInSwitch) GT 0>
	 							<!--- get parent of ELE (CASE/DEFAULT) --->
								<cfset caseDefault=eleInSwitch[1].XmlParent>
								<cfset caseEle = XmlSearch(MCIDStart[1],"*/")>	
								<!--- find postion of ELE in SWITCH --->					
								<!--- asssing position here --->
								<cfset pos=1>						
								  <cfloop array="#MCIDStart[1].XmlChildren#" index="child">
								 	  <cfif Find('QID="'&inpNewValue&'"',ToString(child.XmlChildren[1])) EQ 0>
									 	<cfset pos=pos+1>							
									  <cfelse>
									    <cfbreak>
									  </cfif>
								  </cfloop>				  
								 <!--- insert new ELE after ELE alredy in SWITCH,them delete old ELE  --->
								 <cfset XmlInsertAfter(MCIDStart[1], pos ,genEleArr[1])>  
								 <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1].XmlParent) />
						    <cfelse>							    			
						    	 <!--- need check ELE inside other SWITCH or not --->
							     <cfset XmlAppend(MCIDStart[1], genEleArr[1]) /> 
							     <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) /> 
						    </cfif>
							
						</cfif>
					</cfif>
	
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					
					<!--- Save Local Query to DB --->
	                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                    UPDATE
	                      	simpleobjects.batch
	                    SET
	                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                    WHERE
	                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
						<!--- Save History --->
						<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'UpdateKeyValue') />
	                </cfquery>
					
					<cfset dataout = QueryNew("RXRESULTCODE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
         		
				</cfif>
				
           	 	<cfcatch TYPE="any">
				  	<cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	              	</cfif>    
				  	<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
               		<cfset QueryAddRow(dataout) />
                	<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                	<cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                	<cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                	<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                	<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />          
            	</cfcatch>
           
            </cftry>     

        <cfreturn dataout />
    </cffunction>
	
	
	<cffunction name="EditSwitchKeyValue" access="remote" output="false" hint="Update key value for switch case">
        <cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpRXSS" TYPE="string"/>
	    <cfargument name="INPQID" TYPE="string"/>
		<cfargument name="inpKey" TYPE="string"/><!--- inpKey CK6,CK7(Update for rxt 2)inpKey CK1,CK2(Update Live/Machine State)--->
        <cfargument name="inpNewValue" TYPE="string"/>  
		<cfargument name="caseValue" TYPE="string" default="default"/>
                      
        <cfset var dataout = '0' />    
		
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
            <cftry>      
			     <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>        
				<cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLControlString_vch
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
				<cfelse>
	                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                </cfif>               
				
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>

                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
				
				<cfset xmlRxssElement =  XmlSearch(myxmldocResultDoc,"//RXSS") >
				<cfset xmlRxss =  xmlRxssElement[1] >
				<!---Search element which need to update data--->
				<cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #INPQID# ]") />	
				<cfset MCIDEnd = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID = #inpNewValue# ]") />		  	  	
				<cfif ArrayLen( MCIDStart) GT 0 AND  ArrayLen( MCIDEnd) GT 0 >
					<cfset MCIDStartTagName=MCIDStart[1].XmlName>
					<cfset MCIDEndTagName=MCIDEnd[1].XmlName>
					
					<!--- ELE/CONV to ELE  --->
					<cfif MCIDStartTagName EQ 'ELE' OR MCIDStartTagName EQ 'CONV'>
					    <!--- update CK6,CK7 of rxt 2 point to target ELE --->
					    <cfif inpKey  EQ 'CK6' OR inpKey EQ 'CK7' OR inpKey EQ 'CK1' OR  inpKey EQ 'CK2'>
					        <cfset MCIDStart[1].XmlAttributes["#inpKey#"] = inpNewValue>
					    <cfelse>
					        <!--- update next QID of Start ELE point to target ELE --->
					    	<cfset MCIDStart[1].XmlAttributes["#NextQID(MCIDStart[1].XmlAttributes.rxt)#"] = inpNewValue> 
						</cfif>
   					    
			 	     
						<!--- check if exists SWITCH inside ELE move it outside --->
						<cfset switchInEle = XmlSearch(MCIDStart[1],"*[ @QID = #inpNewValue# ]")>
						<cfif ArrayLen(switchInEle) GT 0>
							<cfset ArrayAppend(xmlRxss[1].XmlChildren, switchInEle[1]) >  
							<cfset XmlDeleteNodes(myxmldocResultDoc,switchInEle[1]) />
						</cfif>		
						<!--- if no ELE inside ELE start after set text =0 --->
					    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
							<cfset MCIDStart[1].XmlText = '0'>
						</cfif>
						

                        <!---Start is ELE to end is SWITCH ,copy SWITCH inside ELE and delete it outside ELE--->
						<cfif MCIDEndTagName EQ 'SWITCH'>				
							<!---  must be check SWITCH has ELE parents or not --->
						    <cfset EleParentOfSwitch=MCIDEnd[1].XmlParent>	
						    <cfif EleParentOfSwitch.XmlName EQ 'ELE'>
							     <cfset EleParentOfSwitch.XmlAttributes["#NextQID(EleParentOfSwitch.XmlAttributes.rxt)#"] = -1>	
						    </cfif>	
						    <!--- remove text of ELE  before add SWITCH inside it --->
							<cfset MCIDStart[1].XmlText = ''>    
							<!---  first append to ELE start then delete SWITCH outside --->
							<cfset XmlAppend(MCIDStart[1], MCIDEnd[1]) />	
						    <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) />
						        <!--- if no ELE inside ELE start then set text =0 --->
						    <cfif ArrayLen(EleParentOfSwitch.XmlChildren) EQ 0>
								<cfset EleParentOfSwitch.XmlText = '0'>
							</cfif>	
					   </cfif>

				   <cfelseif MCIDStartTagName EQ 'SWITCH'>
						<!--- create new CASE/DEFAULT tag --->
						<cfset EleString= Replace(MCIDEnd[1], "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
									
						<cfif caseValue EQ ''>							 
								<cfset EleString = "<DEFAULT>"&EleString&"</DEFAULT>">                  
						<cfelse>
							 	<cfset EleString = "<CASE VAL='"&caseValue&"'>"&EleString&"</CASE>">
						</cfif>	
						
						<cfset EleString=XmlParse(EleString)>
						<cfset genEleArr = XmlSearch(EleString, "/*") >
					    <!--- remove text of SWITCH  before add ELE inside it --->
					    <cfset MCIDStart[1].XmlText = ''>    
						<!--- check ELE/CONV exists in SWITCH or not --->
						<cfset eleInSwitch = XmlSearch(MCIDStart[1], "*/*[ @QID = #inpNewValue# ]") >
					
						<cfif Arraylen(eleInSwitch) GT 0>
 							<!--- get parent of ELE (CASE/DEFAULT) --->
							<cfset caseDefault=eleInSwitch[1].XmlParent>
							<cfset caseEle = XmlSearch(MCIDStart[1],"*/")>	
							<!--- find postion of ELE in SWITCH --->					
							<!--- asssing position here --->
							<cfset pos=1>						
							  <cfloop array="#MCIDStart[1].XmlChildren#" index="child">
							 	  <cfif Find('QID="'&inpNewValue&'"',ToString(child.XmlChildren[1])) EQ 0>
								 	<cfset pos=pos+1>							
								  <cfelse>
								    <cfbreak>
								  </cfif>
							  </cfloop>				  
							 <!--- insert new ELE after ELE alredy in SWITCH,them delete old ELE  --->
							 <cfset XmlInsertAfter(MCIDStart[1], pos ,genEleArr[1])>  
							 <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1].XmlParent) />
					    <cfelse>							    			
					    	 <!--- need check ELE inside other SWITCH or not --->
						     <cfset XmlAppend(MCIDStart[1], genEleArr[1]) /> 
						     <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) /> 
					    </cfif>
						
					</cfif>
				</cfif>

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				
				<!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					<!--- Save History --->
					<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'UpdateKeyValue') />
                </cfquery>
				
				<cfset dataout = QueryNew("RXRESULTCODE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
           	 <cfcatch TYPE="any">
				  <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
	              </cfif>    
				  <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />          
            
            </cfcatch>
           
            </cftry>     

        <cfreturn caseValue />
    </cffunction>
	
	<!--- ++++++++++++++++++++++++++++++++ --->
	<!--- ************************************************************************************************************************* --->
	<!--- Delete connection between 2 MCIDs --->
    <!--- input BatchID OR XMLString , QID, NEw default exit key value and --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="DeleteKeyValue" access="remote" output="false" hint="Delete key in MCIDs in existing Control String">
		
		<cfargument name="INPBATCHID" TYPE="string"/>
		<cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>
		<cfargument name="INPQIDFinish" TYPE="string"/>
        <cfargument name="inpKey" TYPE="string"/>
		<cfargument name="inpNumber" TYPE="string"/>

        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />  
		<cfset var selectedSwitch = ''>  
		<cfset var CURRSWITCHXML = ''>
 
		  	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   		
			<cfoutput>
            <cftry>
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     			
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                      <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>

                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
						
				<cfset xmlRxssElement =  XmlSearch(myxmldocResultDoc,"//RXSS") >
				<cfset xmlRxss =  xmlRxssElement[1] >
				<!---Search element which need to update data--->
			    <cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #INPQID# ]") />	
				<cfset MCIDEnd = XmlSearch(myxmldocResultDoc,"//RXSS//*[ @QID = #INPQIDFinish# ]") />		  	  				
				<cfif ArrayLen( MCIDStart) GT 0 AND  ArrayLen( MCIDEnd) GT 0 >
					<cfset MCIDStartTagName=MCIDStart[1].XmlName>
					<cfset MCIDEndTagName=MCIDEnd[1].XmlName>
						<!--- ELE/CONV to ELE  --->
					<cfif MCIDStartTagName EQ 'ELE' OR MCIDStartTagName EQ 'CONV'>
					    <!--- reset CK6,CK7 of rxt 2 or CK1(Live) CK2(Machine) of rxt 24(Switch Call on State)--->
					    <cfif inpKey  EQ 'CK6' OR inpKey EQ 'CK7' OR inpKey  EQ 'CK1' OR inpKey EQ 'CK2'>
					        <cfset MCIDStart[1].XmlAttributes["#inpKey#"] = -1>
					    <cfelse>
					        <!--- reset next QID of Start ELE  --->
					    	<cfset MCIDStart[1].XmlAttributes["#NextQID(MCIDStart[1].XmlAttributes.rxt)#"] = -1> 
						</cfif>
						<!--- update Answer Map --->					
							<cfif find("CK4", inpKey) AND find("RXT=""2""", MCIDStart[1])>
								<!--- Delete answer map CK4 --->
								<cfset inpNumberStart = "undefined">
								<cfif find("Obj0Color", inpNumber)>
									<cfset inpNumberStart = "0">
								<cfelseif find("Obj1Color", inpNumber)>
									<cfset inpNumberStart = "1">
								<cfelseif find("Obj2Color", inpNumber)>
									<cfset inpNumberStart = "2">
								<cfelseif find("Obj3Color", inpNumber)>
									<cfset inpNumberStart = "3">
								<cfelseif find("Obj4Color", inpNumber)>
									<cfset inpNumberStart = "4">
								<cfelseif find("Obj5Color", inpNumber)>
									<cfset inpNumberStart = "5">
								<cfelseif find("Obj6Color", inpNumber)>
									<cfset inpNumberStart = "6">
								<cfelseif find("Obj7Color", inpNumber)>
									<cfset inpNumberStart = "7">
								<cfelseif find("Obj8Color", inpNumber)>
									<cfset inpNumberStart = "8">
								<cfelseif find("Obj9Color", inpNumber)>
									<cfset inpNumberStart = "9">
								<cfelseif find("ObjPoundColor", inpNumber)>
									<cfset inpNumberStart = "#CHR(35)#">
								<cfelseif find("ObjStarColor", inpNumber)>
									<cfset inpNumberStart = "#CHR(42)#">
								</cfif>
								
								<cfset answerMap = MCIDStart[1].XmlAttributes["CK4"] >
								<cfset newAnswerMap = ''>
								<cfset ck2 = ''>
								<cfset answerMap = MID(answerMap,2,len(answerMap) -2) >
								
								<cfset answerMap = answerMap.split("\),\(")>
								<cfset ck2 = '('>
								<cfloop array="#answerMap#" index="answer">
									<cfset answer = answer.split(',')>
									<cfset str = answer[1]>
									<cfset value = answer[2]>
									<cfif str NEQ inpNumberStart>
										<cfset ck2 = ck2 & '#str#,'>
										<cfset newAnswerMap = newAnswerMap & '(#str#,#value#),'>
									</cfif>
								</cfloop>
								<cfif ck2 EQ "(">
									<cfset ck2 = ''>
									<cfset newAnswerMap = ''>
								<cfelse>
									<cfset ck2 = MID(ck2, 1, len(ck2)-1) & ")" >
									<cfset newAnswerMap = MID(newAnswerMap,1, len(newAnswerMap) -1 )  >
								</cfif>
				               <cfset MCIDStart[1].XmlAttributes["CK4"] = newAnswerMap>  
				               <cfset MCIDStart[1].XmlAttributes["CK2"] = ck2>  
															
							</cfif>
				    		<!--- ELE to SWITCH just move SWITCH outside  --->
				    		<cfif MCIDEndTagName EQ 'SWITCH'>  
								<cfset ArrayAppend(xmlRxss[1].XmlChildren, MCIDEnd[1]) >  
							    <cfset XmlDeleteNodes(myxmldocResultDoc, MCIDEnd[1]) />
							    <!--- if no ELE inside ELE start then set text =0 --->
							    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
									<cfset MCIDStart[1].XmlText = '0'>
								</cfif>
							</cfif>
					<!--- SWITCH to ELE,move ELE outside SWITCH --->
					<cfelseif MCIDStartTagName EQ 'SWITCH'>
							<cfset parents=MCIDEnd[1].XmlParent />
							<cfset ArrayAppend(xmlRxss[1].XmlChildren, MCIDEnd[1]) >  
							<cfset XmlDeleteNodes(myxmldocResultDoc, parents) />
							<!--- if no ELE inside ELE start then set text =0 --->
						    <cfif ArrayLen(MCIDStart[1].XmlChildren) EQ 0>
								<cfset MCIDStart[1].XmlText = '0'>
							</cfif>							
					</cfif>
				</cfif>
                              

				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>  
				

				<cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'DeleteKeyValue') />
				
                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                        
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>  
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                 <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	

<!--- ++++++++++++++ --->
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID, StartQID, inpEndQID in the main control string --->
    <!--- ************************************************************************************************************************* --->
   	<!--- Swap QID between MCID Obj (ELE,CONV,SWITCH)  --->
  	<cffunction name="SwapQID" access="remote" output="false" hint="Swap 2 QIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpStartQID" TYPE="string"/>
		<cfargument name="inpEndQID" TYPE="string"/>

        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           
            <cftry>                	

                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
				
				
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>

                     <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 

				 <cfset xmlRxssElement =  XmlSearch(myxmldocResultDoc,"//RXSS") >
				 <cfset xmlRxss =  xmlRxssElement[1] >
				 <!---Search element which is deleted--->
				 <cfset MCIDStart = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #inpStartQID# ]") />		
				 <cfset MCIDEnd   = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID = #inpEndQID#   ]") />	
				 <cfif ArrayLen(MCIDStart) GT 0 and ArrayLen(MCIDEnd) GT 0>
				 <!---  begin swap QIDs     --->  
					<cfset MCIDStart[1].XmlAttributes["QID"] = inpEndQID>	
					<cfset MCIDEnd[1].XmlAttributes["QID"] = inpStartQID>

				    <!--- update next QID of 2 MCID objs point to 2 ELE changed QID --->
				    <cfset eleHasQid = XmlSearch(myxmldocResultDoc, "//RXSS//*[ @QID ]") />	
				   	<cfloop index="node" array="#eleHasQid#">
					   	<cfif node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] EQ inpStartQID>		   	
							<cfset node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] = inpEndQID> 	 	
					   	<cfelseif node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] EQ inpEndQID>		   	
							<cfset node.XmlAttributes["#NextQID(node.XmlAttributes.rxt)#"] = inpStartQID> 	 	
					   	</cfif>							
		     	    </cfloop> 
		     	    
	     	   		<!--- update CK6,CK7 for rxt 2	  --->			
					<cfset  eleUpdateCK6 = XmlSearch(myxmldocResultDoc, "//*[ @RXT='2' and @CK6 = '#inpStartQID#' or @CK7 = '#inpStartQID#' or @CK6 = '#inpEndQID#' or @CK7 = '#inpEndQID#']") />
			    	<cfloop array="#eleUpdateCK6#" index="elecurr">
				    	 <cfif elecurr.XmlAttributes["CK6"] EQ inpStartQID>
				    	      <cfset elecurr.XmlAttributes["CK6"] = inpEndQID>     
						 <cfelseif elecurr.XmlAttributes["CK6"] EQ inpEndQID> 
						 	      <cfset elecurr.XmlAttributes["CK6"] = inpStartQID>     
						 </cfif> 
						 <cfif elecurr.XmlAttributes["CK7"] EQ inpStartQID>
				    	      <cfset elecurr.XmlAttributes["CK7"] = inpEndQID>     
						 <cfelseif elecurr.XmlAttributes["CK7"] EQ inpEndQID>
				    	      <cfset elecurr.XmlAttributes["CK7"] = inpStartQID>     
						 </cfif>
					</cfloop>
					<!--- update CK1(Live State),CK2(Machine State) for Switch on Call State(rxt 24)	  --->			
					<cfset  updateLiveMachineState = XmlSearch(myxmldocResultDoc, "//*[ @RXT='24' and @CK1 = '#inpStartQID#' or @CK2 = '#inpStartQID#' or @CK1 = '#inpEndQID#' or @CK2 = '#inpEndQID#']") />
			    	<cfloop array="#updateLiveMachineState#" index="elecurr">
				    	 <cfif elecurr.XmlAttributes["CK1"] EQ inpStartQID>
				    	      <cfset elecurr.XmlAttributes["CK1"] = inpEndQID>     
						 <cfelseif elecurr.XmlAttributes["CK1"] EQ inpEndQID> 
						 	      <cfset elecurr.XmlAttributes["CK1"] = inpStartQID>     
						 </cfif> 
						 <cfif elecurr.XmlAttributes["CK2"] EQ inpStartQID>
				    	      <cfset elecurr.XmlAttributes["CK2"] = inpEndQID>     
						 <cfelseif elecurr.XmlAttributes["CK2"] EQ inpEndQID>
				    	      <cfset elecurr.XmlAttributes["CK2"] = inpStartQID>     
						 </cfif>
					</cfloop>		 				   
					<!--- 	update Answer Map for rxt 2	  --->
					<cfset  eleUpdateAns = XmlSearch(myxmldocResultDoc, "//*[ @RXT='2']") />
				    <cfloop array="#eleUpdateAns#" index="elecurr">
					 	        <cfset ans=elecurr.XmlAttributes["CK4"]>
								<cfif Find("," & "#inpStartQID#" & ")", ans)>
										<cfset ans=Replace(ans,"," & "#inpStartQID#" & ")","," & "#inpEndQID#" & ")", "ALL" )>
										<cfset elecurr.XmlAttributes["CK4"]=ans>	
								<cfelseif Find("," & "#inpEndQID#" & ")", ans)>
										<cfset ans=Replace(ans,"," & "#inpEndQID#" & ")","," & "#inpStartQID#" & ")", "ALL" )>
										<cfset elecurr.XmlAttributes["CK4"]=ans>	
								</cfif> 	 	 
					</cfloop>
	     
				</cfif>
				 
			
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfset QueryAddRow(dataout) />

                <!--- All is well --->
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />         
                <!--- Save Local Query to DB --->
                <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
		 		                             
            <cfcatch TYPE="any">
                <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, INPQID, TYPE, MESSAGE, ERRMESSAGE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <!--- <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") /> --->
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />            
            
            </cfcatch>
            
            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>

  	<!--- ************************************************************************************************************************* --->
    <!--- Read XML control string --->
    <!--- ************************************************************************************************************************* --->
	 <cffunction name="Readdisplayxml" access="remote" output="false" hint="Read all RXSS MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
                       
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = 
    
	     --->
         
       	<cfoutput>
                                    
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />         
           
            <cftry>                  	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
 
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                     
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
               

                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry>     
                <!--- Set default to error in case later processing goes bad --->		           
                        
                <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
               	<cfset OutTodisplayxmlBuff = ToString(inpRXSSLocalBuff)> 
		   		<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
              	<cfset OutTodisplayxmlBuff = Replace(OutTodisplayxmlBuff, '"', "'", "ALL")> 
			
              <!--- <cfset OutTodisplayxmlBuff = REReplace(OutTodisplayxmlBuff, "'\s*'", "''", "ALL")>   --->
	
     		

               <cfset dataout = QueryNew("RXRESULTCODE, DISPLAYXML_VCH")>   
               <cfset QueryAddRow(dataout) />
               <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
               <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "#OutTodisplayxmlBuff#") />
                  
		    <cfcatch TYPE="any">
			    <cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, DISPLAYXML_VCH, TYPE, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
                <cfset QuerySetCell(dataout, "DISPLAYXML_VCH", "") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />  
            </cfcatch>
            </cftry>     
		</cfoutput>

        <cfreturn dataout />
    </cffunction>    

	<cffunction name="ReadRXSSXML" access="remote" output="false" hint="Read all RXSS MCIDs from XML string from the DB">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfset var dataout = '0' />    
		<!---  LSTCURRLINKDEF array store list of target QID from switch --->
	    <cfset var LSTCURRLINKDEF = ArrayNew(1)>
	    <!---  LSTELEINSWITCH_QID,LSTCASE_VALUE array store list of target QID  and text ---> 
	    <cfset var LSTELEINSWITCH_QID = ArrayNew(1)>
	    <cfset var LSTCASE_VALUE = ArrayNew(1)>
	    <cfset var LSTARRAYOBJECT = ArrayNew(1) >
       	<cfoutput>
			
			<cfset CURRLINK1 = "">
			<cfset CURRLINK2 = "">
            <cfset CURRLINK3 = "">
            <cfset CURRLINK4 = "">
            <cfset CURRLINK5 = "">
            <cfset CURRLINK6 = "">
            <cfset CURRLINK7 = "">
            <cfset CURRLINK8 = "">
            <cfset CURRLINK9 = "">
            <cfset CURRLINK0 = "">
            <cfset CURRLINKDEF = "">
            <cfset CURRLINKErr = "">
            <cfset CURRLINKNR = "">
            <cfset CURRLINKPound = "">
            <cfset CURRLINKStar = "">
            <cfset CURRLINKLIVE = "">
            <cfset CURRLINKMACHINE = "">
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
			<cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", "-1") />
            <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
			
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
           	<cfset selectedSwitch = "" />
           
            <cftry>                  	
                <!--- Read from DB Batch Options --->
                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                    SELECT                
                      XMLControlString_vch
                    FROM
                      simpleobjects.batch
                    WHERE
                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>     
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
					
                </cftry>
				
				<cfset CURRLINK1 = "">
                <cfset CURRLINK2 = "">
                <cfset CURRLINK3 = "">
                <cfset CURRLINK4 = "">
                <cfset CURRLINK5 = "">
                <cfset CURRLINK6 = "">
                <cfset CURRLINK7 = "">
                <cfset CURRLINK8 = "">
                <cfset CURRLINK9 = "">
                <cfset CURRLINK0 = "">
                <cfset CURRLINKDEF = "">
                <cfset CURRLINKErr = "">
                <cfset CURRLINKNR = "">
                <cfset CURRLINKPound = "">
                <cfset CURRLINKStar = "">
				<cfset CURRDynamicFlag = "0">
				<cfset CURRLINKLIVE = "">
            	<cfset CURRLINKMACHINE = "">
				<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")>  
				
				<cfset xmlElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID]")>
				<cfloop array="#xmlElements#" index="element">
				
					<cfset MCIDOBJECT = StructNew() >
					<cfset MCIDOBJECT.RXQID = element.XmlAttributes.QID >
					<cfset MCIDOBJECT.rxt = element.XmlAttributes.rxt >
					<cfset MCIDOBJECT.rxtDESC = "">
					<cfset MCIDOBJECT.DESC = element.XmlAttributes.DESC > 
					<cfset MCIDOBJECT.XPOS = element.XmlAttributes.X >
					<cfset MCIDOBJECT.YPOS = element.XmlAttributes.Y >
					<cfset MCIDOBJECT.DYNAMICFLAG = "">
					
					<cfset CURRDESC = "#element.XmlAttributes.DESC#">
					<cfset CURRDynamicFlag = "0">
					<!--- Check Dynamic script --->
					<cfset eleChild = XmlSearch(element, "ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<!--- ELE child is not TTS--->
						<cfif eleChild[1].XmlAttributes.ID EQ "TTS">
							<cfset CURRDynamicFlag = "2">
						<cfelse>
							<cfset eleChildDesc = XmlSearch(eleChild[1], "@DESC")>
							<cfif ArrayLen(eleChildDesc) GT 0 >
								<cfset CURRDynamicFlag = '1' >
							</cfif>
						</cfif>
					<cfelse>
						<cfif element.XmlAttributes.BS EQ '1'>
							<cfset CURRDynamicFlag = "1">
						</cfif>	
					</cfif>
					<cfset MCIDOBJECT.DYNAMICFLAG =  CURRDynamicFlag >
					
					<!--- need to be draw line between rxt 21 to other MCID and put text at the end of the lines--->
			        <cfif element.XmlAttributes.rxt EQ 21>
			        <!--- fetch all QID of ELE/CONV inside SWITCH to array --->
	                      <cfset CURrxt21QID = element.XmlAttributes.QID>	                      
	                      <cfset eleChildSwitch = XmlSearch(element, "*/*")>
	                      <cfloop array="#eleChildSwitch#" index="eleIn">   
							  <cfset ArrayAppend(LSTCURRLINKDEF,eleIn.XmlAttributes.QID) > 				  		 
						      <cfif eleIn.XmlParent.XmlName EQ 'CASE'>    
							     <cfset  ArrayAppend(LSTELEINSWITCH_QID,eleIn.XmlAttributes.QID)> 	   
							 	 <cfset  ArrayAppend(LSTCASE_VALUE,eleIn.XmlParent.XmlAttributes["VAL"])> 
							  </cfif>   
						  </cfloop>
					</cfif>
				    
					<cfset eleChild = XmlSearch(element, "SCRIPT/ELE")>
					<cfif ArrayLen(eleChild) GT 0>
						<cfset CURRDynamicScript = '1' >
					</cfif>
                    
				    <cfset selectedElementsII = XmlSearch(element, "@CK1")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK1 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK1 = "-1">                        
                    </cfif>
					
					<cfset selectedElementsII = XmlSearch(element, "@CK2")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK2 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK2 = "-1">                        
                    </cfif>
					
                    <cfset selectedElementsII = XmlSearch(element, "@CK4")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK4 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK4 = "-1">                        
                    </cfif>
					
                    <cfset selectedElementsII = XmlSearch(element, "@CK5")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK5 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK5 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK6")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK6 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK6 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK7")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK7 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK7 = "-1">                        
                    </cfif>
                    
                    <cfset selectedElementsII = XmlSearch(element, "@CK8")>
                    <cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK8 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK8 = "-1">                        
                    </cfif>
                    
                     <cfset selectedElementsII = XmlSearch(element, "@CK15")>
					<cfif ArrayLen(selectedElementsII) GT 0>
                        <cfset CURRCK15 = selectedElementsII[ArrayLen(selectedElementsII)].XmlValue>
                    <cfelse>
                        <cfset CURRCK15 = "-1">                        
                    </cfif>        
					                            
                    <cfswitch expression="#element.XmlAttributes.rxt#">                                            
                        <cfcase value="1">
							<cfset currrxtDESC = "Statement">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="2">
							<cfset currrxtDESC = "IVR - Single Digit Response">                            
                            <cfset CURRLINKDEF = CURRCK5>
                            <cfset CURRLINKErr = CURRCK7>
		                    <cfset CURRLINKNR = CURRCK6>
                            <cfset CURRAnswerMap = Replace(CURRCK4, '"', "'", "ALL") >                                                    
                            <cfset AnswerMapBuff  = CURRAnswerMap>
                            <cfloop condition="Len(AnswerMapBuff) GT 0">
                                <!--- Get first close ) --->  
                                <cfif FIND(")", AnswerMapBuff) GT 0 >
                                    <!--- Get first pair --->
                                    <cfset AnswerPairBuff = LEFT(AnswerMapBuff, FIND(")", AnswerMapBuff) ) >
                                     <!--- Get rest of pairs --->
                                    <cfif LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) GT 0>
                                        <cfset AnswerMapBuff = RIGHT(AnswerMapBuff, LEN(AnswerMapBuff) - FIND(")", AnswerMapBuff) ) >
                                    <cfelse>
                                        <!--- Exit loop if no more --->
                                        <cfset AnswerMapBuff = "">
                                    </cfif>             
                                    <!--- Remove parenthesis --->
                                    <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ",(", "") >
                                    <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, "(", "") >
                                    <cfset AnswerPairBuff = REPLACE(AnswerPairBuff, ")", "") >
                                    <cfif FIND(",", AnswerPairBuff) GT 0 >
                                        <cfset ValidKeyBuff = ListGetAt(AnswerPairBuff, 1, ",")>
                                        <cfset RespondToBuff = ListGetAt(AnswerPairBuff, 2, ",")>
                                        <cfswitch expression="#ValidKeyBuff#">
                                        	<cfcase value="1"><cfset CURRLINK1 = RespondToBuff></cfcase>                                            
                                            <cfcase value="2"><cfset CURRLINK2 = RespondToBuff></cfcase>                                            
                                            <cfcase value="3"><cfset CURRLINK3 = RespondToBuff></cfcase>                                            
                                            <cfcase value="4"><cfset CURRLINK4 = RespondToBuff></cfcase>                                            
                                            <cfcase value="5"><cfset CURRLINK5 = RespondToBuff></cfcase>
                                            <cfcase value="6"><cfset CURRLINK6 = RespondToBuff></cfcase>
                                            <cfcase value="7"><cfset CURRLINK7 = RespondToBuff></cfcase>
                                            <cfcase value="8"><cfset CURRLINK8 = RespondToBuff></cfcase>
                                            <cfcase value="9"><cfset CURRLINK9 = RespondToBuff></cfcase>
                                            <cfcase value="0"><cfset CURRLINK0 = RespondToBuff></cfcase>
                                            <cfcase value="##"><cfset CURRLINKPound = RespondToBuff></cfcase>
                                            <cfcase value="*"><cfset CURRLINKStar = RespondToBuff></cfcase>
                                            <cfcase value="-13"><cfset CURRLINKPound = RespondToBuff></cfcase>
                                            <cfcase value="-6"><cfset CURRLINKStar = RespondToBuff></cfcase>
											
                                        </cfswitch>
                                    </cfif>
                                <cfelse>
                                    <!--- Exit loop if no more --->
                                    <cfset AnswerMapBuff = "">
                                </cfif>
                            </cfloop>
							<cfset MCIDOBJECT.LINK1 = CURRLINK1 >
							<cfset MCIDOBJECT.LINK2 = CURRLINK2 >
							<cfset MCIDOBJECT.LINK3 = CURRLINK3 >
							<cfset MCIDOBJECT.LINK4 = CURRLINK4 > 
							<cfset MCIDOBJECT.LINK5 = CURRLINK5 >
							<cfset MCIDOBJECT.LINK6 = CURRLINK6 >
							<cfset MCIDOBJECT.LINK7 = CURRLINK7 >
							<cfset MCIDOBJECT.LINK8 = CURRLINK8 >
							<cfset MCIDOBJECT.LINK9 = CURRLINK9 >
							<cfset MCIDOBJECT.LINK0 = CURRLINK0 >
							<cfset MCIDOBJECT.LINKERR = CURRLINKErr >
							<cfset MCIDOBJECT.LINKNR = CURRLINKNR >
							<cfset MCIDOBJECT.LINKPOUND = CURRLINKPound >
							<cfset MCIDOBJECT.LINKSTAR = CURRLINKStar >
							<!--- reset all data temp --->
							<cfset CURRLINK1 = "">
			                <cfset CURRLINK2 = "">
			                <cfset CURRLINK3 = "">
			                <cfset CURRLINK4 = "">
			                <cfset CURRLINK5 = "">
			                <cfset CURRLINK6 = "">
			                <cfset CURRLINK7 = "">
			                <cfset CURRLINK8 = "">
			                <cfset CURRLINK9 = "">
			                <cfset CURRLINK0 = "">
			                <cfset CURRLINKErr = "">
			                <cfset CURRLINKNR = "">
			                <cfset CURRLINKPound = "">
			                <cfset CURRLINKStar = "">
							<cfset CURRDynamicFlag = "0">
                        </cfcase>
                        <cfcase value="3">
							<cfset currrxtDESC = "Record a response - with erase and re-record options">
                            <cfset CURRLINKDEF = CURRCK8>
                        </cfcase>
                        <cfcase value="4">
							<cfset currrxtDESC = "Live Agent Transfer (LAT)">
                            <!--- <cfset CURRLINKDEF = CURRCK9> --->
							<cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="5">
							<cfset currrxtDESC = "Opt Out">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="6">
							<cfset currrxtDESC = "IVR multi string - press pound or five second pause to complete entry">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="7">
							<cfset currrxtDESC = "Opt In - 10 Digit String">
                            <cfset CURRLINKDEF = CURRCK8>
                        </cfcase>
                        <cfcase value="8">
							<cfset currrxtDESC = "NA">
                            <cfset CURRLINKDEF = CURRCK8>
                        </cfcase>
                        <cfcase value="9">
							<cfset currrxtDESC = "Read out previous/specified digit strings 1-10 - no IVR options">
                            <cfset CURRLINKDEF = CURRCK5> 
                        </cfcase>
                        <!--- hailp change code rxt 10-18 CURRLINKDEF point to right CK base on req for draw line and display data on form edit--->
                        <cfcase value="10">
							<cfset currrxtDESC = "SQL Query String - INSERT, UPDATE or SELECT (1,0)">
                            <cfset CURRLINKDEF = CURRCK5>
						<!--- 	<cfset CURRLINKDEF = CURRCK8> CK8 N/A--->
                        </cfcase>
                        <cfcase value="11">
							<cfset currrxtDESC = "Play DTMFs - no IVR options">
                            <!--- <cfset CURRLINKDEF = CURRCK8> CK8 N/A--->
							<cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="12">
							<cfset currrxtDESC = "Strategic Pause - line still listens for hangups but will puase for specified number of milli-seconds">
                            <cfset CURRLINKDEF = CURRCK5>
							<!--- <cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="13">
							<cfset currrxtDESC = "eMail">
                            <!--- <cfset CURRLINKDEF = CURRCK5> --->
							<cfset CURRLINKDEF = CURRCK15>
                        </cfcase>
                        <cfcase value="14">
							<cfset currrxtDESC = "Store IGData for inbound calls">
                            <cfset CURRLINKDEF = CURRCK5>
							<!--- <cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="15">
							<cfset currrxtDESC = "Play from absolute path">
                            <cfset CURRLINKDEF = CURRCK5> 
						<!--- 	<cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="16">
							<cfset currrxtDESC = "Switch based on previous responses to a previous MCID">
                            <cfset CURRLINKDEF = CURRCK5> 
						<!--- 	<cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="17">
							<cfset currrxtDESC = "Switchout MCIDs based on SQL query - Ignores CCD changes - Justs switches out MCID - only generates one path - no live/machine options">
                            <cfset CURRLINKDEF = CURRCK5> 
							<!--- <cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="18">
							<cfset currrxtDESC = "Automatic Speech Recognition (ASR)">
                            <cfset CURRLINKDEF = CURRCK5>
							<!--- <cfset CURRLINKDEF = CURRCK8> --->
                        </cfcase>
                        <cfcase value="19">
							<cfset currrxtDESC = "Web Service">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="20">
							<cfset currrxtDESC = "ASR">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>
                        <cfcase value="21">
							<cfset currrxtDESC = "Switching">
                            <cfset CURRLINKDEF = CURRCK5>
                        </cfcase>  
                        <cfcase value="22">
							<cfset currrxtDESC = "Conversion">
                            <cfset CURRLINKDEF = CURRCK15>
                        </cfcase>
						<cfcase value="24">
							<cfset currrxtDESC = "Switch on Call State">
                            <cfset CURRLINKDEF = CURRCK5>
							<cfset CURRLINKLIVE = CURRCK1>
		                    <cfset CURRLINKMACHINE = CURRCK2>
		                    <cfset MCIDOBJECT.LINKLIVE = CURRLINKLIVE>
							<cfset MCIDOBJECT.LINKMACHINE = CURRLINKMACHINE>
							<!--- reset current live/machine link --->
							<cfset CURRLINKLIVE = "">
		                    <cfset CURRLINKMACHINE = ""> 
                        </cfcase>                                            
                    </cfswitch>  
					
					<cfset MCIDOBJECT.rxtDESC = currrxtDESC>
					<cfset MCIDOBJECT.LINKDEF = CURRLINKDEF>
					<!--- Clean up for raw XML --->
                    <cfset OutToDBXMLBuff = ToString(element)>                
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
                    <cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					
					<cfif element.XmlAttributes.rxt EQ 21>
						<cfset MCIDOBJECT.rxt21QID = CURrxt21QID >
						<cfset MCIDOBJECT.LSTCURRLINKDEF = LSTCURRLINKDEF >
						<cfset MCIDOBJECT.LSTELEINSWITCH_QID = LSTELEINSWITCH_QID >
						<cfset MCIDOBJECT.LSTCASE_VALUE = LSTCASE_VALUE >
						<!-- delete all element existed for each switch-->
						<cfset LSTCURRLINKDEF = ArrayNew(1)>
	     				<cfset LSTELEINSWITCH_QID = ArrayNew(1)>
	     				<cfset LSTCASE_VALUE = ArrayNew(1)>
					</cfif>		
					<cfset ArrayAppend(LSTARRAYOBJECT, MCIDOBJECT) >
				</cfloop>
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "1") />
				<cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
				
	            <cfcatch TYPE="any">
		            <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -1>
                    </cfif>
					<cfset dataout = QueryNew("RXRESULTCODE, LSTARRAYOBJECT")> 
					<cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
	                <cfset QuerySetCell(dataout, "LSTARRAYOBJECT", #LSTARRAYOBJECT#) />
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
</cffunction>
	
	<!--- AUTO SORT --->
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="AutoSort" access="remote" output="true" hint="Auto sort MCID">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRXSS" TYPE="string"/>

        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
		<cfset var arrElement = ArrayNew(1)>
		<cfset var arrElementOut = ArrayNew(1) />
        <cfset var objElement = {qid=-1,RXT=-1,nextId=-1,side=-1,curX=0,curY=0} />
        <cfset var side1X=100>
		<cfset var side1Y=200>
		<cfset var side2X=300>
		<cfset var side2Y=200>
		<cfset var side3X=500>
		<cfset var side3Y=200>
		<cfset var side4X=100>
		<cfset var side4Y=20>
        <cfset var count=1>

       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
			
            
            <cftry>
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<!--- have not permission --->
                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
					
	                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
						<!--- Read from DB Batch Options --->
	                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                        SELECT
	                          XMLControlString_vch
	                        FROM
	                          simpleobjects.batch
	                        WHERE
	                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>
						
	                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
					<cfelse>
						 
		                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
	                </cfif>
	
	                <!--- Parse for data --->
	                <cftry>
						<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
	
						<cfcatch TYPE="any">
							<!--- Squash bad data --->
							<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>
	                     </cfcatch>
	                </cftry>
					
					<!--- Sort Comms object --->
					<cfset commsX = 20>
					<cfset commsY = 0>
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/DM[@X]")>
					<cfloop array="#selectedElements#" index="CURRDMXML">
						<cfset commsY = 20>
						<cfset CURRDMXML.XmlAttributes.X = '#commsX#'>
						<cfset CURRDMXML.XmlAttributes.Y = '#commsY#'>
						<cfset commsX = commsX + 140>
					</cfloop>
					
					<cfset rulesX = 20>
					<cfset rulesY = commsY + 100>
					
					<!--- Sort for RULES Object --->
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RULES/*[@RULEID]")>
					<cfloop array="#selectedElements#" index="CURRDMXML">
						<cfset CURRDMXML.XmlAttributes.X = '#rulesX#'>
						<cfset CURRDMXML.XmlAttributes.Y = '#rulesY#'>
						<cfset rulesX = rulesX + 140>
					</cfloop>
					<cfif rulesX EQ 20>
						<cfset rulesY = commsY>
					</cfif>
	              	<!--- Get the RXSS ELE's --->
	              	<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//*[@QID]")>
	                <cfloop array="#selectedElements#" index="CURRMCIDXML">
						<!--- check output of mcid --->
						<cfset objElement = {qid = -1, rxt = -1, nextId = ArrayNew(1), side = -1, curX = 0, curY = 0} />
						<cfset objElement.qid = #CURRMCIDXML.XmlAttributes.QID#>
						<cfset objElement.rxt = #CURRMCIDXML.XmlAttributes.rxt#>
						
						<cfswitch expression="#CURRMCIDXML.XmlName#">
							<cfcase value='SWITCH'>
								<cfset switchChild = XmlSearch(CURRMCIDXML,"*/*")>
								<cfif ArrayLen(switchChild) GT 0>
									<cfloop array="#switchChild#" index="switchEleChild">
										<cfset ArrayAppend(arrElementOut, switchEleChild.XmlAttributes.QID)>
										<cfset ArrayAppend(objElement.nextId, switchEleChild.XmlAttributes.QID)>
									</cfloop>
									<cfset objElement.side = 1>		
								<cfelse>
									<cfset objElement.side = 4>		
								</cfif>
							</cfcase>
							<cfdefaultcase>
								<cfset value = CURRMCIDXML.XmlAttributes[NextQID(objElement.rxt)] >
								<cfif (value EQ -1) OR (value EQ 0) OR (value EQ "")>
									<cfset objElement.side = 4>
								<cfelse>
									<cfset ArrayAppend(arrElementOut, value)>
									<cfset objElement.side = 1>
									<cfset ArrayAppend(objElement.nextId, value)>
								</cfif>
							</cfdefaultcase>
						</cfswitch>
						<cfset ArrayAppend(arrElement, objElement)>
	                </cfloop>
					
					<cfloop array="#arrElement#" index="element">
						<cfloop array="#arrElementOut#" index="elementOut">
							<cfif (#elementOut# EQ #element.qid#)>
								<cfif ArrayLen(element.nextId) GT 0>
									<cfset element.side = 2>
								<cfelse>
									<cfset element.side = 3>
								</cfif>	
							</cfif>
						</cfloop>
					</cfloop>
					<!--- Varriable calculate side position --->
					
					<cfset side4X = 20>
					<cfset side4Y = rulesY + 130>
					<!--- for side 4 --->
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS")>
					<cfloop array="#selectedElements#" index="CURRDMXML">
						<cfset CURRDMXML.XmlAttributes.X = '#side4X#'>
						<cfset CURRDMXML.XmlAttributes.Y = '#side4Y#'>
						<cfset side4X = side4X + 150>
					</cfloop>
					<cfloop array="#arrElement#" index="element">
						<cfif #side4X# GT 920>
							<cfset side4X = 20>
							<cfset side4Y = side4Y + 150 >
						</cfif>
						<cfif #element.side# EQ 4>
							<cfset element.curX = #side4X# >		
							<cfset element.curY = #side4Y# >
							<cfset side4X = side4X + 150>
						</cfif>
					</cfloop>
					<cfif side4X EQ 20>
						<cfset side4Y = rulesY >
					</cfif>
					<cfset side1X = 20>
					<cfset side1Y = side4Y + 180>
					
					<cfset side2X = 200>
					<cfset side2Y = side4Y + 180>
					
					<cfset side3X = 380>
					<cfset side3Y = side4Y + 180>
					
					<cfloop array="#arrElement#" index="element">
						<cfif #side1Y# GT 800>
							<cfset side1X = 560>
							<cfset side1Y = side4Y + 180 >
						</cfif>
						<cfif #side2Y# GT 800>
							<cfset side2X = 740>
							<cfset side2Y = side4Y + 180 >
						</cfif>
						<cfif #side3Y# GT 800>
							<cfset side3X = 920>
							<cfset side3Y = side4Y + 180 >
						</cfif>
						<cfif #element.side# EQ 1>
							<cfset element.curX = #side1X#>		
							<cfset element.curY =#side1Y#>
							<cfset side1Y = side1Y + 200>
						</cfif>
						<cfif #element.side# EQ 2>
							<cfset element.curX = #side2X#>		
							<cfset element.curY = #side2Y#>
							<cfset side2Y= side2Y + 200>
						</cfif>
						<cfif #element.side# EQ 3>
							<cfset element.curX = #side3X#>		
							<cfset element.curY = #side3Y#>
							<cfset side3Y = side3Y + 200>
						</cfif>
					</cfloop>
					
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//*[@QID]")>
					<cfset count=1>
	                <cfloop array = "#selectedElements#" index="ele">
						<cfset ele.XmlAttributes.X = #arrElement[count].curX#>
						<cfset ele.XmlAttributes.Y = #arrElement[count].curY#>
						<cfset count= count+1>
	                </cfloop>
	
					<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
	
	               <!--- Write output --->
	               <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
						<!--- Save Local Query to DB --->
	                    <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#" result="abc">
	                        UPDATE
	                            simpleobjects.batch
	                        SET
	                            XMLControlString_vch = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                        WHERE
	                            BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                    </cfquery>

						<cfset a = History(INPBATCHID,OutToDBXMLBuff,'AutoSort')>
	               </cfif>
	
	                <cfset dataout = QueryNew("RXRESULTCODE")>
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
				</cfif>
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
            </cfcatch>

            </cftry>     
			
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	
	<!--- END AUTO SORT --->
	
	<!--- Update voice object position --->
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID OR XMLString --->
    <!--- ************************************************************************************************************************* --->
  	<cffunction name="UpdateVoiceObjectPosition" access="remote" output="false" hint="Update voice object position when the stage has at least one MCID object">
        <cfargument name="inpXML" TYPE="string"/>
        <cfset var dataout = '0' />    

       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE")>   
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutToDBXMLBuff = ""> 
            
            <cftry>
                <!--- Parse for data --->
                <cftry>
					<!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
					<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpXML# & "</XMLControlStringDoc>")>

					<cfcatch TYPE="any">
						<!--- Squash bad data --->
						<cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>
                     </cfcatch>
                </cftry>
				
				<!--- Get the first MCID object----->
				<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID=1]")>
				<cfif ArrayLen(selectedElements) EQ 0>
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//DM[@QID=1]")>
				</cfif>
				<cfif ArrayLen(selectedElements) GT 0>
					<!--- Get position of the first MCID --->
					<cfset mcidXPos = selectedElements[1].XmlAttributes.X >
					<cfset mcidYPos = selectedElements[1].XmlAttributes.Y >
					<!--- Get position of voice object --->
					<cfset selectedElements = XmlSearch(myxmldocResultDoc, "//RXSS")>
					<cfif mcidXPos GT 140 OR  mcidXPos EQ 140>
						<cfset selectedElements[1].XmlAttributes.X = mcidXPos - 140>
						<cfset selectedElements[1].XmlAttributes.Y = mcidYPos>
					<cfelse>
						<cfif mcidYPos GT 120 OR  mcidYPos EQ 120>
							<cfset selectedElements[1].XmlAttributes.X = mcidXPos>
							<cfset selectedElements[1].XmlAttributes.Y = mcidYPos - 120>
						<cfelse>
							<cfset selectedElements[1].XmlAttributes.X = mcidXPos + 150>
							<cfset selectedElements[1].XmlAttributes.Y = mcidYPos>
						</cfif>
					</cfif>
				</cfif>
				<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
				<cfreturn OutToDBXMLBuff />
            <cfcatch TYPE="any">
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -1>
                </cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
		        <cfreturn dataout />
            </cfcatch>

            </cftry>     
			
		</cfoutput>

    </cffunction>
	
	<cffunction name="UpdateAnswerMapValue" access="remote" output="false" hint="Replace LINK in answer map in MCIDs in existing Control String">
	
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpRXSS" TYPE="string"/>
        <cfargument name="INPQID" TYPE="string"/>	 
        <cfargument name="inpAnswer" TYPE="string"/>	 
        <cfargument name="inpNewValue" TYPE="string"/>  
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
                       
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
       <!---  
	   		<!--- Build an internal query to sort and modify the MCID XML Control String --->		           
            <cfset dataout = QueryNew("ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			<cfset QueryAddRow(dataout) />
            
            <!--- Either DM1, DM2, RXSS, CCD --->
            <cfset QuerySetCell(dataout, "ELETYPE_VCH", "") /> 
            
            <!--- 0 for DM1, DM2, CCD - QID for RXSS --->
            <cfset QuerySetCell(dataout, "ELEMENTID_INT", 0) /> 
            
            <!--- Actual Elements --->
            <cfset QuerySetCell(dataout, "XML_VCH", "") /> 
             --->
        
                
        
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset inpRXSSLocalBuff = "" />
            <cfset DEBUGVAR1 = "0">  
	        <cfset OutTodisplayxmlBuff = "">
            <cfset OutToDBXMLBuff = ""> 
            <cfset DEBUGVAR1 = "0">
            
            <cftry>                	
                        	            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                
                
                <cfif inpNewValue EQ "" OR inpNewValue LT 1>
                	<cfset inpNewValue = "-1">                
                </cfif>
                
                <cfif INPBATCHID NEQ "" AND INPBATCHID GT 0>
                     
					<!--- Read from DB Batch Options --->
                    <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
                        SELECT                
                          XMLControlString_vch
                        FROM
                          simpleobjects.batch
                        WHERE
                          BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                    </cfquery>     
                    
                    <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>

				<cfelse>
                
	                 <cfset inpRXSSLocalBuff = TRIM(inpRXSS)>
                
                </cfif>               
                 
                <!--- Parse for data --->                             
                <cftry>
                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
                       
                      <cfcatch TYPE="any">
                        <!--- Squash bad data  --->    
                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
                     </cfcatch>              
                </cftry> 
                           

              	<cfset selectElement = XmlSearch(myxmldocResultDoc, "//RXSS//*[@QID=#INPQID#]")>
				
				<!--- if add code check Coldfusion hates single pound signs in any string when redrag(*,'#') answer map append data instead of  update data(#,*) --->
				<!--- must add code check for inpAnswer - bug fix --->
				
				
				<cfset answerMap = selectElement[1].XmlAttributes.CK4 >
				<cfset newAnswerMap = ''>
				<cfset ck2 = ''>
				
				<cfset isOldValue = false>
				
				<cfif answerMap NEQ ''>
					<cfset answerMap = MID(answerMap,2,len(answerMap) -2) >
					<cfset answerMap = answerMap.split("\),\(")>
					<cfset ck2 = '('>
					<cfloop array="#answerMap#" index="answer">
						<cfset answer = answer.split(',')>
						<cfset str = answer[1]>
						<cfset value = answer[2]>
						<cfset ck2 = ck2 & '#str#,'>
						<cfif str EQ inpAnswer>
							<cfset isOldValue = true>
							<cfset newAnswerMap = newAnswerMap & '(#str#,#inpNewValue#),'>
						<cfelse>
							<cfset newAnswerMap = newAnswerMap & '(#str#,#value#),'>
						</cfif>
					</cfloop>
					
					<cfif NOT isOldValue>
						<cfset newAnswerMap = newAnswerMap & '(#inpAnswer#,#inpNewValue#),'>
						<cfset ck2 = ck2 & '#inpAnswer#,'>
					</cfif>
					<cfset ck2 = MID(ck2, 1, len(ck2)-1) & ")" >
					<cfset newAnswerMap = MID(newAnswerMap,1, len(newAnswerMap) -1 )  >
				<cfelse>
					<cfset newAnswerMap = '(#inpAnswer#,#inpNewValue#)'>
					<cfset ck2 = '(#inpAnswer#)'>	
				</cfif>
				
               <cfset selectElement[1].XmlAttributes.CK4 = newAnswerMap>  
               <cfset selectElement[1].XmlAttributes.CK2 = ck2>  
			                
               
                                    
	  			<cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
				<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<RXSS/>', "<RXSS></RXSS>", "ALL")>
				<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
                
                <!--- Save Local Query to DB --->
                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
                    UPDATE
                      	simpleobjects.batch
                    SET
                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
                    WHERE
                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
                </cfquery>
				
                <cfset a = History("#INPBATCHID#", "#OutToDBXMLBuff#", 'UpdateAnswerMapValue') />
				
                <cfset dataout = QueryNew("RXRESULTCODE")>   
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                  
		 		                             
            <cfcatch TYPE="any">
				
				<cfif cfcatch.errorcode EQ "">
					<cfset cfcatch.errorcode = -3>
				</cfif>    
				<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID, INPQID")> 
				<cfset QueryAddRow(dataout) />
				<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
				<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
				<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
				<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
                <cfset QuerySetCell(dataout, "INPQID", "#INPQID#") />
                
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
	<!--- END Update voice position --->
	<!--- ************************************************************************************************************************* --->
    <!--- input BatchID, then removes MCIDs in the main control string --->
    <!--- ************************************************************************************************************************* --->
   
  	<cffunction name="ClearXML" access="remote" output="false" hint="Re-order MCIDs in existing Control String">
        <cfargument name="INPBATCHID" TYPE="string"/>
        <cfargument name="inpPassBackdisplayxml" TYPE="string" default="0"/>
        <cfargument name="inpClearRules" TYPE="string" default="1"/>
        <cfargument name="inpClearEmail" TYPE="string" default="1"/>
        <cfargument name="inpClearSMS" TYPE="string" default="1"/>
                      
        <cfset var dataout = '0' />    
        <cfset var StageQuery = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = 
        -3 = Unhandled Exception
    
	     --->
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->		           
            <cfset dataout = QueryNew("RXRESULTCODE, ELETYPE_VCH, ELEMENTID_INT, XML_VCH")>   
			                
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
            <cfset OutTodisplayxmlBuff = ""/>
           
            <cftry>                	
                        	            
				<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="checkBatchPermissionByBatchId" returnvariable="checkBatchPermissionByBatchId">
					<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
					<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
				</cfinvoke>
				
            	<cfif NOT checkBatchPermissionByBatchId.havePermission>
					<!--- have not permission --->
                    
					<cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID, TYPE, MESSAGE, ERRMESSAGE")>
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE",  "-2") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	                <cfset QuerySetCell(dataout, "TYPE", "") />
					<cfset QuerySetCell(dataout, "MESSAGE",  checkBatchPermissionByBatchId.message) />
	                <cfset QuerySetCell(dataout, "ERRMESSAGE", "") />
				<cfelse>
                     
	                <!--- Read from DB Batch Options --->
	                <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
	                    SELECT                
	                      XMLControlString_vch
	                    FROM
	                      simpleobjects.batch
	                    WHERE
	                      BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	               
	
	                 
	                <!--- Parse for data --->                             
	                <cftry>
	                      <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
	                     <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #GetBatchOptions.XMLControlString_vch# & "</XMLControlStringDoc>")>
	                       
	                      <cfcatch TYPE="any">
	                        <!--- Squash bad data  --->    
	                        <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
	                     </cfcatch>              
	                </cftry> 
	                           
					<!--- Delete DM object (Search All DM has QID and remove)--->
					<!--- Search all DM object has QID --->
	              	<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//DM[@QID]")>
					<!--- Remove DM object matched --->
					<cfset XmlDeleteNodes(myxmldocResultDoc, selectedElement) >  
					
					<!--- Search all rules object has RulesID and remove --->
					<cfif inpClearRules GT 0>
						<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//RULES/*[@RULEID]")>
						<cfset XmlDeleteNodes(myxmldocResultDoc, selectedElement) >  
					</cfif>
					
					<!--- Remove all Child of RXSS --->
					<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//RXSS/*[@QID]")>
					<cfset XmlDeleteNodes(myxmldocResultDoc, selectedElement) >
					
					<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//RXSS")>
					<cfif ArrayLen(selectedElement) GT 0>
						<cfset selectedElement[1].XmlText = " ">			
					</cfif>
					
					<cfset selectedElement = XmlSearch(myxmldocResultDoc, "//RULES")>
					<cfif ArrayLen(selectedElement) GT 0>
						<cfset selectedElement[1].XmlText = " ">			
					</cfif>
					
	                <cfset OutToDBXMLBuff = ToString(myxmldocResultDoc) > 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, "<?xml version=""1.0"" encoding=""UTF-8""?>", "")>
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '<XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '</XMLControlStringDoc>', "", "ALL")> 
					<cfset OutToDBXMLBuff = Replace(OutToDBXMLBuff, '"', "'", "ALL")>
					<cfset OutToDBXMLBuff = TRIM(OutToDBXMLBuff)>
					
	                <!--- Save Local Query to DB --->
	                 <cfquery name="WriteBatchOptions" datasource="#Session.DBSourceEBM#">                    
	                    UPDATE
	                      	simpleobjects.batch
	                    SET
	                   		XMLControlString_vch =  <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#OutToDBXMLBuff#">
	                    WHERE
	                      	BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
	                </cfquery>     
	                
	                <cfset dataout = QueryNew("RXRESULTCODE, INPBATCHID")>   
	                <cfset QueryAddRow(dataout) />
	                <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
				
                </cfif>
				    
	            <cfcatch TYPE="any">
	                <cfif cfcatch.errorcode EQ "">
						<cfset cfcatch.errorcode = -3>
					</cfif>    
					<cfset dataout = QueryNew("RXRESULTCODE, TYPE, MESSAGE, ERRMESSAGE, INPBATCHID")> 
					<cfset QueryAddRow(dataout) />
					<cfset QuerySetCell(dataout, "RXRESULTCODE",  "#cfcatch.errorcode#") />  
					<cfset QuerySetCell(dataout, "TYPE", "#cfcatch.TYPE#") />
					<cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />                
					<cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />
	                <cfset QuerySetCell(dataout, "INPBATCHID", "#INPBATCHID#") />
	            </cfcatch>
            </cftry>     
		</cfoutput>
        <cfreturn dataout />
    </cffunction>
	
	<!--- Store to database --->
	<cffunction name="History" access="remote" output="false" hint="History">
		<cfargument name="INPBATCHID" TYPE="string" default="0"/>
		<cfargument name="XMLControlString_vch" TYPE="string" default="0"/>
		<cfargument name="EVENT" TYPE="string" default="0"/>
		<!--- Remove history when add event --->
		<cfquery name="GetHistory" datasource="#Session.DBSourceEBM#">
			SELECT id FROM simpleobjects.history 
			WHERE 
				flag = 1 AND BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
			ORDER BY id ASC LIMIT 0,1
       </cfquery>

		<cfif GetHistory.recordCount GT 0>
			<cfquery name="RemoveHistory" datasource="#Session.DBSourceEBM#">
				DELETE FROM
					simpleobjects.history
				WHERE
					flag = 1
					AND
					BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
					AND
					UserId_int = #SESSION.UserID#
					AND
					id > <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
			<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
				UPDATE
					simpleobjects.history
				SET
					flag = 0
				WHERE
					id = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#GetHistory.id#">
			</cfquery>
		</cfif>

		<!--- Update history --->
 		<cfquery name="UpdateHistory" datasource="#Session.DBSourceEBM#">
            INSERT INTO
              simpleobjects.history (BatchId_bi,UserId_int,XMLControlString_vch,`time`,`event`)
            VALUES
            	(<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">,
				#SESSION.UserID#,
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#XMLControlString_vch#">,
				NOW(),
				<CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#EVENT#">
				)  
       </cfquery>	
	<cfreturn 0>
	</cffunction>
	
	<cffunction name="NextQID" 	access="public"	returntype="String"	output="false"hint="Get next QID control key based on rxt type">
	   	<cfargument name="rxtType" TYPE="string"/>	
	   	<cfset CKNext="">
		<cfswitch expression="#rxtType#">
			<cfcase value='3'>
				<cfset CKNext="CK8">
			</cfcase>
			<cfcase value='7'>
				<cfset CKNext="CK8">
			</cfcase>
			<cfcase value='13'>
				<cfset CKNext="CK15">
			</cfcase>
			<cfcase value='22'>
				<cfset CKNext="CK15">
			</cfcase>
			<cfdefaultcase>
				<cfset CKNext="CK5">
			</cfdefaultcase>
		</cfswitch>
		<cfreturn CKNext/>
	</cffunction>
	
	<!--- Include template files---->
	<cfinclude template="rxt/genvmdm.cfm">
    <cfinclude template="rxt/genrxt1.cfm">
    <cfinclude template="rxt/genrxt2.cfm">
    <cfinclude template="rxt/genrxt3.cfm">
    <cfinclude template="rxt/genrxt4.cfm">
    <cfinclude template="rxt/genrxt5.cfm">
    <cfinclude template="rxt/genrxt6.cfm">
    <cfinclude template="rxt/genrxt7.cfm">
    <cfinclude template="rxt/genrxt8.cfm">
    <cfinclude template="rxt/genrxt9.cfm">
    <cfinclude template="rxt/genrxt10.cfm">
    <cfinclude template="rxt/genrxt11.cfm">
    <cfinclude template="rxt/genrxt12.cfm">
    <cfinclude template="rxt/genrxt13.cfm">
    <cfinclude template="rxt/genrxt14.cfm">
    <cfinclude template="rxt/genrxt15.cfm">
    <cfinclude template="rxt/genrxt16.cfm">
    <cfinclude template="rxt/genrxt17.cfm">
    <cfinclude template="rxt/genrxt18.cfm">
    <cfinclude template="rxt/genrxt19.cfm">
    <cfinclude template="rxt/genrxt20.cfm">
    <cfinclude template="rxt/genrxt21.cfm">
    <cfinclude template="rxt/genrxt22.cfm">
	<cfinclude template="rxt/genrxt24.cfm">
</cfcomponent>
