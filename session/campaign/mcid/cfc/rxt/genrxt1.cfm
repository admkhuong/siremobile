
    <!--- ************************************************************************************************************************* --->
    <!--- input rxt values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genrxt1XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPQID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpLibID" type="string" default="0"/>
        <cfargument name="inpEleID" type="string" default="0"/>
        <cfargument name="inpDataID" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default="0"/>
        <cfargument name="inpCK5" type="string" default="-1"/>
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>      
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>    
		
		<cfset var multiScriptData = "0">
        <cfset var dataout = '0' />    
		<cfset var multiScriptCount = '0' />    
		<cfset var multiScriptEle = '' />    
		<cfset var multiSwitchScriptEle = ' ' />    
		<cfset var inpMultiSwitchScript = 0 />  
		<cfset var inpParentDataKey = -1 /> 
		<cfset var rxvid = 0 /> 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
		<cfset requestBody = toString( getHttpRequestData().content ) />
		<!--- Get JSON data Double-check to make sure it's a JSON value. --->
		<cfif isJSON( requestBody )>
		 	<cfset structData = deserializeJSON( requestBody ) >
			<cfset INPQID = structData.INPQID />
	        <cfset inpBS = structData.inpBS />
			
	        <cfset inpLibID = structData.inpLibID />
	        <cfset inpEleID = structData.inpEleID />
	        <cfset inpDataID = structData.inpDataID />
	        <cfset inpCK1 = structData.inpCK1 />
	        <cfset inpCK5 = structData.inpCK5 />
	        <cfset INPDESC = structData.INPDESC />
			
	        <cfset inpX = structData.inpX />
	        <cfset inpY = structData.inpY />
	        
         	<cfset inpRQ = structData.inpRQ />
	        <cfset inpCP = structData.inpCP />
	        
	        <cfset inpLINKTo = structData.inpLINKTo />   
	        <cfset inpParentDataKey = structData.inpParentDataKey />
	        <cfset rxvid = structData.rxvid />
		 	<cfset multiScriptData = structData.multiScriptData />    
		</cfif>
		
		
       	<cfoutput>
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />

            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
            <cfset MAXQID = 999>                                     

            <cftry>      
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                <!--- <cfscript> sleep(3000); </cfscript>  --->

                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPQID LT 1 OR  INPQID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
				
                <cfset inpCK1 = TRIM(inpCK1) />
				<cfset inpCK5 = TRIM(inpCK5) />
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 
	            <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
                 
				<!--- Start rxt XML --->				
                <cfset RXTXMLSTRINGBuff = "<ELE QID='#INPQID#'">                

				<!--- ++++++++++++++++++++++++++++++ 1 ++++++++++++++++++++++++++++++ --->
                <!--- Set the type --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RXT='1'">     
			    <cfset TTSXMLStringBuff = "">      
	            <cfif inpBS EQ 2>
		            <cfset inpBS = 1 >          	
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "<ELE ID='TTS'">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & " RXVID ='#rxvid#'">
					<cfif inpParentDataKey NEQ -1>
						<cfset TTSXMLStringBuff = TTSXMLStringBuff & " DK='#inpParentDataKey#' ">
					</cfif>
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & " RXBR='16'>">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "#XMLFormat(inpDataID)#">
					<cfset TTSXMLStringBuff = TTSXMLStringBuff & "</ELE>">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DS='0'">
					<!--- Set the type Dyanamic Script Elements Data ID --->
		            <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DI='0'">
				<cfelse>
               		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DS='#inpLibID#'">
					<!--- Set the type Dyanamic Script Elements Data ID --->
		            <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DI='#inpDataId#'">
	            </cfif>
	            
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " BS='#inpBS#'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSUID='#Session.UserID#'">  


                <!--- Set the Dyanamic Script Element ID --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSE='#inpEleID#'">
                
				
				<!--- ++++++++++++++++++++++++++++++ 3 (CK) ++++++++++++++++++++++++++++++ --->
                <!--- Set the CK1 - Retries--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Set the CK5 - Next QID - no match --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                <!--- Set the CKX - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='#inpLINKTo#'">
                
				
				
				
				<!--- ++++++++++++++++++++++++++++++ 4 ++++++++++++++++++++++++++++++ --->
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>    
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  

                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
			
				<cfinclude template="genMultiScript_common.cfm">
				
				<cfif TTSXMLStringBuff NEQ "">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">"& TTSXMLStringBuff &"</ELE>">	
				<cfelse>
					<cfif multiScriptEle NEQ 0 AND inpBS NEQ 0>
						<cfif multiScriptEle EQ "">
							<cfset multiScriptEle = 0>
						</cfif>
						<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">" & multiScriptEle>
						<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & "</ELE>">	
					<cfelse>
						<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">0</ELE>">	
					</cfif>
				</cfif>
					
           	   <!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
				<cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "#HTMLCodeFormat(RXTXMLSTRINGBuff)#") />
				<cfset QuerySetCell(dataout, "RAWXML", "#RXTXMLSTRINGBuff#") />

            <cfcatch type="any">

            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	

                <cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />       

            </cfcatch>

            </cftry>     

		</cfoutput>

        <cfreturn dataout />
    </cffunction>