
    <!--- ************************************************************************************************************************* --->
    <!--- input rxt values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genrxt20XML" access="remote" output="true" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPQID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default=""/>
        <cfargument name="inpCK2" type="string" default=""/>

        <cfargument name="inpCK4" type="string" default=""/>
        <cfargument name="inpCK5" type="string" default=""/>
        <cfargument name="inpCK6" type="string" default="" required="no"/>
        <cfargument name="inpCK7" type="string" default="" required="no"/>
        <cfargument name="inpCK8" type="string" default="" required="no"/>     
        <cfargument name="inpCK9" type="string" default="" required="no"/>     
        <cfargument name="inpCK10" type="string" default="" required="no"/>     
        <cfargument name="inpCK11" type="string" default="" required="no"/>     
        <cfargument name="inpCK12" type="string" default="" required="no"/>     
        <cfargument name="inpCK13" type="string" default="" required="no"/>   
        <cfargument name="inpCK14" type="string" default="" required="no"/>         
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>   
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>   
              
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPQID LT 1 OR  INPQID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
                                
              <cfset inpCK1 = TRIM(inpCK1) />
			 <cfset inpCK2 = TRIM(inpCK2) />		
			 <cfset inpCK4 = TRIM(inpCK4) />
			 <cfset inpCK5 = TRIM(inpCK5) />
			 <cfset inpCK6 = TRIM(inpCK6) />
			 <cfset inpCK7 = TRIM(inpCK7) />
			 <cfset inpCK8 = TRIM(inpCK8) />
			 <cfset inpCK9 = TRIM(inpCK9) />
			 <cfset inpCK10 = TRIM(inpCK10) />
			 <cfset inpCK11 = TRIM(inpCK11) />
			 <cfset inpCK12 = TRIM(inpCK12) />
			 <cfset inpCK13 = TRIM(inpCK13) />
		      <cfset inpRQ = TRIM(inpRQ) />
			 <cfset inpCP = TRIM(inpCP) />
			 
			 <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
			 <cfif inpCK2 EQ ""><cfset inpCK2 = "0"></cfif> 
			 <cfif inpCK4 EQ ""><cfset inpCK4 = "0"></cfif> 
			 <cfif inpCK5 EQ ""><cfset inpCK5 = "0"></cfif> 
			 <cfif inpCK6 EQ ""><cfset inpCK6 = "0"></cfif> 
			 <cfif inpCK7 EQ ""><cfset inpCK7 = "0"></cfif> 
			 <cfif inpCK8 EQ ""><cfset inpCK8 = "0"></cfif> 
			 <cfif inpCK9 EQ ""><cfset inpCK9 = "0"></cfif> 
			 <cfif inpCK10 EQ ""><cfset inpCK10 = "0"></cfif>
			 <cfif inpCK11 EQ ""><cfset inpCK11 = "0"></cfif>
			 <cfif inpCK12 EQ ""><cfset inpCK12 = "0"></cfif>
			 <cfif inpCK13 EQ ""><cfset inpCK13 = "0"></cfif>
			 <cfif inpCK14 EQ ""><cfset inpCK14 = "0"></cfif>
			 <cfif inpRQ EQ ""><cfset inpRQ = "0"></cfif>
			 <cfif inpCP EQ ""><cfset inpCP = "0"></cfif>


     

			
			           
				<!--- Start rxt XML --->				

                <cfset RXTXMLSTRINGBuff = "<ELE QID='#INPQID#'">
               
                <!--- XML doesn't like '' data values ---> 
				<cfif INPDESC EQ "">
                    <cfset INPDESC = "Description Not Specified">
                <cfelse>
               		<cfset HTMLBuff = Replace(INPDESC, #chr(10)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                    
               		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DESC='#XmlFormat(HTMLBuff)#'">
                </cfif> 
                 
                <!--- Set the type --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RXT='20'">                
	             
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " BS='#inpBS#'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                
               

               		<!--- Set the inpCK1  --->
                    <!---
                    <cfset HTMLBuff = Replace(inpCK1, #chr(10)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                    
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#XmlFormat(HTMLBuff)#'">--->
               		<cfset HTMLBuff = Replace(inpCK1, #chr(10)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#XmlFormat(HTMLBuff)#'">
               		<!--- Set the inpCK2 --->
                     <!---
                    <cfset HTMLBuff = Replace(inpCK2, #chr(10)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                    
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK2='#XmlFormat(HTMLBuff)#'">--->
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK2='#inpCK2#'">
  

               		<!--- Set the inpCK4  --->
                    
                    <!---<cfset HTMLBuff = Replace(inpCK4, #chr(10)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, #chr(13)#, "", "all")>
                    <cfset HTMLBuff = Replace(HTMLBuff, "'", "&apos;", "all")>
                    
             		 <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK4='#XmlFormat(HTMLBuff)#'">--->
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK4='#inpCK4#'">

               		<!--- Set the inpCK5  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK5='#inpCK5#'">

               		<!--- Set the inpCK6  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK6='#inpCK6#'">

               		<!--- Set the inpCK7  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK7='#inpCK7#'">

               		<!--- Set the inpCK8  --->                   
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK8='#inpCK8#'">
		
					<!--- Set the inpCK9  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK9='#inpCK9#'">

               		<!--- Set the inpCK10  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK10='#inpCK10#'">

               		<!--- Set the inpCK11  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK11='#inpCK11#'">

               		<!--- Set the inpCK12  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK12='#inpCK12#'">

               		<!--- Set the inpCK13  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK13='#inpCK13#'">

               		<!--- Set the inpCK14  --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK14='#inpCK14#'">
                       
                <!--- Set the CKX - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " X='#inpX#'">
                
                <!--- Set the CKY - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='#inpLINKTo#'">

            	<cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>       
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                             
                                 
                <!--- Set the MCID Decription --->
                                                                            
                <!--- Close ELE XML --->
                <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">0</ELE>">
           
              	<!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
               
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "#HTMLCodeFormat(RXTXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RXTXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
                <cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>