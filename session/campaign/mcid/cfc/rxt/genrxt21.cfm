
    <!--- ************************************************************************************************************************* --->
    <!--- input rxt values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genrxt21XML" access="remote" output="true" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPQID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default="{%LocationKey7_vch%}"/>
        <cfargument name="inpCK2" type="string" default="3"/>
		<cfargument name="inpCK3" type="string" default="{%LocationKey7_vch%}"/>
        <cfargument name="inpCK5" type="string" default="-1"/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
		<cfargument name="inpRQ" type="string" default=""/>
        <cfargument name="inpCP" type="string" default=""/>
		<!---
        <cfargument name="inpLINKTo" type="string" default="0"/>
	
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>    
       --->
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>

                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPQID LT 1 OR  INPQID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
                
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 

                <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
             
                                                                                                
                
				<!--- Start rxt XML --->				
                <cfset RXTXMLSTRINGBuff = "<SWITCH QID='#INPQID#'">
                
                               
                <!--- Set the type --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RXT='21'">                
	             
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " BS='#inpBS#'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                <cfif inpCK2 EQ 2>
					<cfset inpCK1 = inpCK3>
				<cfelseif inpCK2 EQ 3>
					<cfset ck3 = Mid(inpCK3, 3, len(inpCK3)-4)>
	            	<!--- Get data CK1 from DB --->
					<cftry>
						<cfquery name="GetCK1" datasource="#Session.DBSourceEBM#">
		                    SELECT 
		                    	#ck3# AS LocationKey
		                    FROM 
		                        simplelists.rxmultilist
		                    WHERE 
		                        UserId_int = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#SESSION.UserID#">
								AND
								ContactTypeId_int = 1
								AND
								ContactString_vch = '1234343447a'
		                </cfquery>
						
						<cfcatch TYPE="any">
							<cfset GetCK1.LocationKey = "COL">
						</cfcatch>
					</cftry>
					<cfset inpCK1 = #GetCK1.LocationKey#>
					<cfif inpCK1 EQ ''>
						<cfset inpCK1 = "0">
					</cfif>
				<cfelse>
					<cfset inpCK1 = "Default">
				</cfif>       



				<!---<cfloop array="#selectedElements#" index="CURRMCIDXML">
				
				</cfloop> --->
                <!--- Set the CK1 - Retries--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Set the CK2 - map switch--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK2='#inpCK2#'">
			
                <!--- Set the CK2 - map switch--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK3='#inpCK3#'">
			

				
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                 <!--- Set the CKX - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info 
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='#inpLINKTo#'">--->
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif> 
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
                                                             
                <!--- Close CCD XML --->
                <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">0</SWITCH>">
           		<!---<cfset RXTXMLSTRINGBuff =  "<SWITCH CK1='0' CK2='2' DESC='Greeting'>1235</SWITCH>"> --->
              	<!--- Write to DB Batch Options --->
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "#HTMLCodeFormat(RXTXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RXTXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
                <cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>