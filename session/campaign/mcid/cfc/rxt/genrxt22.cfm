
    <!--- ************************************************************************************************************************* --->
    <!--- input rxt values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genrxt22XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPQID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpEleId" type="string" default="0"/>
        <cfargument name="inpDataId" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default="1"/>
        <cfargument name="inpCK2" type="string" default="0"/>
        <cfargument name="inpCK3" type="string" default="0"/>
		<cfargument name="inpCK4" type="string" default="0"/>
		<cfargument name="inpCK5" type="string" default="0"/>
		<cfargument name="inpCK6" type="string" default="0"/>
		<cfargument name="inpCK7" type="string" default="0"/>
		<cfargument name="inpCK8" type="string" default="0"/>
		<cfargument name="inpCK9" type="string" default="0"/>
		<cfargument name="inpCK15" type="string" default="-1"/>
		<cfargument name="inpDataKey" type="string" default="0"/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>   
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>    
       
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>

        <cfset var dataout = '0' />    
       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPQID LT 1 OR  INPQID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
        
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 

            <cfif inpCK1 EQ ""><cfset inpCK1 = "0"></cfif> 
            <cfif inpCK2 EQ ""><cfset inpCK2 = "0"></cfif> 
			<cfif inpCK3 EQ ""><cfset inpCK3 = "0"></cfif> 
			<cfif inpCK4 EQ ""><cfset inpCK4 = "0"></cfif> 
			<cfif inpCK5 EQ ""><cfset inpCK5 = "0"></cfif> 
			<cfif inpCK6 EQ ""><cfset inpCK6 = "0"></cfif>
			<cfif inpCK7 EQ ""><cfset inpCK7 = "0"></cfif>
			<cfif inpCK8 EQ ""><cfset inpCK8 = "0"></cfif>
            <cfif inpCK9 EQ ""><cfset inpCK9 = "0"></cfif>   
			<cfif inpCK15 EQ ""><cfset inpCK15 = "-1"></cfif>                                       

				<!--- Start rxt XML --->				
                <cfset RXTXMLSTRINGBuff = "<CONV QID='#INPQID#'">
                
                               
                <!--- Set the type --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RXT='22'">
			
           		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DS='#inpLibID#'">
			  	<!--- Set the type Dyanamic Script Elements Data ID --->
           		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DI='#inpDataId#'">
	             
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " BS='0'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                              
             	<!--- Set the Dyanamic Script Element ID --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSE='#inpEleId#'">
                
				<!---<cfloop array="#selectedElements#" index="CURRMCIDXML">
				</cfloop> --->
                <!--- Set the CK1 - Retries--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Set the CK2 - map switch--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK2='#inpCK2#'">
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK3='#inpCK3#'">
                
				<cfif inpCK1 NEQ 1>
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK4='#inpCK4#'">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK5='#inpCK5#'">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK6='#inpCK6#'">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK7='#inpCK7#'">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK8='#inpCK8#'">
					<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK9='#inpCK9#'">
				</cfif>
				<!--- Set the CKX - next qid --->
				<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK15='#inpCK15#'">
                 <!--- Set the CKX - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
				<!---<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='#inpLINKTo#'"> --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='0'">
               
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>       
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                            
                                 
                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
				<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">#inpDataKey#</CONV>">	
           		<!---<cfset RXTXMLSTRINGBuff =  "<SWITCH CK1='0' CK2='2' DESC='Greeting'>1235</SWITCH>"> --->
              	<!--- Write to DB Batch Options --->
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "#HTMLCodeFormat(RXTXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RXTXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
                <cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>