
    <!--- ************************************************************************************************************************* --->
    <!--- input rxt values and output a MCID string --->
    <!--- ************************************************************************************************************************* --->
        
	<cffunction name="genrxt3XML" access="remote" output="false" hint="Read an XML string from the DB and parse out the CCD values">
        <cfargument name="INPQID" type="string" default="0"/>
        <cfargument name="inpBS" type="string" default="0"/>
        <cfargument name="inpCK1" type="string" default=""/>
        <cfargument name="inpCK2" type="string" default="60"/>
        <cfargument name="inpCK3" type="string" default="0"/>
        <cfargument name="inpCK4" type="string" default="0"/>
        <cfargument name="inpCK5" type="string" default="0"/>
        <cfargument name="inpCK6" type="string" default=""/>
        <cfargument name="inpCK7" type="string" default="0"/>
        <cfargument name="inpCK8" type="string" default="-1"/>
        <cfargument name="inpCK9" type="string" default=""/>
        <cfargument name="inpCK10" type="string" default=""/>
        <cfargument name="inpCK11" type="string" default=""/>
        <cfargument name="inpCK12" type="string" default=""/>
        <cfargument name="inpCK13" type="string" default=""/>
        <cfargument name="inpCK14" type="string" default=""/>
        <cfargument name="inpCK15" type="string" default=""/>
        <cfargument name="inpCK16" type="string" default=""/>
        <cfargument name="inpCK17" type="string" default=""/>
        <cfargument name="inpCK18" type="string" default=""/>
        <cfargument name="inpCK19" type="string" default=""/>
        <cfargument name="inpCK20" type="string" default=""/>
        <cfargument name="inpCK21" type="string" default=""/>
        <cfargument name="inpCK22" type="string" default=""/>
        <cfargument name="inpCK23" type="string" default=""/>
        <cfargument name="inpCK24" type="string" default=""/>
        <cfargument name="inpCK25" type="string" default=""/>
        <cfargument name="inpX" type="string" default="0"/>
        <cfargument name="inpY" type="string" default="0"/>
        <cfargument name="inpLINKTo" type="string" default="0"/>
        <cfargument name="inpRQ" type="string" default=""/>   
        <cfargument name="inpCP" type="string" default=""/>       
       
        <cfargument name="INPDESC" type="string" default="Description Not Specified"/>
              
                
        <cfset var dataout = '0' />    
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = general Exception Failure
        -3 = 
		-4 = Invalid QID
		-5 = Invalid UserId
		-6 =
		-7 = 
		-8 = 
		-9 =
		
    
	     --->
          
       
                       
       	<cfoutput>
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")> 
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
            <cfset QuerySetCell(dataout, "RAWXML", "") />
                        
            <cfset myxmldoc = "" />
            <cfset selectedElements = "" />
                        
            <cfset CID = "" />
            <cfset UserSpecData = "" />
            <cfset FileSeq = "" />
                        
            <cfset MAXQID = 999>                                     
       
            <cftry>      
            	
            
				<!--- 
                    <cfset thread = CreateObject("java", "java.lang.Thread")>
                    <cfset thread.sleep(3000)> 
                 --->			
                    
                <!--- <cfscript> sleep(3000); </cfscript>  --->
                              
                              
                <!--- Validations --->              
                <!--- Valid QID's only --->            
                <cfif INPQID LT 1 OR  INPQID GT MAXQID>
                	<cfthrow errorcode="-4">
                </cfif>
               
               <!--- Validate User Id --->
                <cfif #SESSION.UserID# LT 1>
                	<cfthrow errorcode="-5">
                </cfif>
                
                <cfif inpCK3 EQ "*"> 
                	<cfset inpCK3 = "-6">
                </cfif> 
                
                <cfif inpCK3 EQ "##"> 
                	<cfset inpCK3 = "-13">
                </cfif> 
                <cfset inpCK1 = TRIM(inpCK1) />
				<cfset inpCK2 = TRIM(inpCK2) />
			    <cfset inpCK3 = TRIM(inpCK3) />
			    <cfset inpCK4 = TRIM(inpCK4) />
			    <cfset inpCK5 = TRIM(inpCK5) />
			    <cfset inpCK6 = TRIM(inpCK6) />
			    <cfset inpCK7 = TRIM(inpCK7) />
			    <cfset inpCK8 = TRIM(inpCK8) />
			    <cfset inpCK9 = TRIM(inpCK9) />
			    <cfset inpCK10 = TRIM(inpCK10) />
			    <cfset inpCK11 = TRIM(inpCK11) />
			    <cfset inpCK12 = TRIM(inpCK12) />
			    <cfset inpCK13 = TRIM(inpCK13) />
                <cfset inpCK14 = TRIM(inpCK14) />
                
                <cfif inpCK14 EQ "*"> 
                	<cfset inpCK14 = "-6">
                </cfif> 
                
                <cfif inpCK14 EQ "##"> 
                	<cfset inpCK14 = "-13">
                </cfif> 
                
                
                <cfset inpCK15 = TRIM(inpCK15) />
                <cfset inpCK16 = TRIM(inpCK16) />
                <!--- When inserting into DB accidental \'' will crash statement - fix is toy require paths with forward slashes instead--->
                <cfset inpCK16 = REPLACE(inpCK16, "\", "\\", "ALL")>
                <cfset inpCK17 = TRIM(inpCK17) />
                <cfset inpCK18 = TRIM(rxdsLocalWritePath) />
                <cfset inpCK18 = REPLACE(inpCK18, "\", "\\", "ALL")>
                 
                <cfset inpCK19= TRIM(inpCK19) />
                <cfset inpCK20 = TRIM(inpCK20) />
                
                <!--- XML doesn't like '' data values ---> 
                <cfif INPDESC EQ ""><cfset INPDESC = "Description Not Specified"></cfif> 
                <cfif inpCK3 EQ "" ><cfset inpCK3 = "0"></cfif> 
                <cfif inpCK5 EQ "" ><cfset inpCK5 = "0"></cfif> 
                <cfif inpCK7 EQ ""><cfset inpCK7 = "0"></cfif> 
                <cfif inpCK1 EQ "" OR inpCK1 EQ "0"><cfset inpCK1 = "1"></cfif> 
                <cfif inpCK4 EQ ""><cfset inpCK4 = "0"></cfif>
                              
                                                                                                
                 
				<!--- Start rxt XML --->				
                <cfset RXTXMLSTRINGBuff = "<ELE QID='#INPQID#'">
                
                               
                <!--- Set the type --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RXT='3'">                
	             
    	        <!--- Set the build script type - 0 is static for now --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " BS='#inpBS#'">          
                                                
                <!--- Set the User to current session - --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DSUID='#Session.UserID#'">  
                              
                <!--- Set the CK1 - Retries--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK1='#inpCK1#'">
                
                <!--- Set the CK2 - Valid Keys --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK2='#inpCK2#'">
                
                <!--- Set the CK3 - Repeat Key --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK3='#inpCK3#'">
                
                <!--- Set the CK4 - Answer Map--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK4='#inpCK4#'">
                
                <!--- Set the CK5 - Next QID - no match --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK5='#inpCK5#'">
                
                <!--- Set the CK6 - no match MESSAGE if retries--->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK6='#inpCK6#'">
                
                <!--- Set the CK7 - No entry MESSAGE --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK7='#inpCK7#'">
                
                <!--- Set the CK8 - Next QID - no match --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK8='#inpCK8#'">
                
                <!--- Set the CK9 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK9='#inpCK9#'">
                                
                <!--- Set the CK10 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK10='#inpCK10#'">
                                
                <!--- Set the CK11 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK11='#inpCK11#'">
                                
                <!--- Set the CK12 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK12='#inpCK12#'">
                                
                <!--- Set the CK13 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK13='#inpCK13#'">
                                
                <!--- Set the CK14 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK14='#inpCK14#'">
                
                <!--- Set the CK15 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK15='#inpCK15#'">
                
                <!--- Set the CK16 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK16='#inpCK16#'">
                
                <!--- Set the CK17 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK17='#inpCK17#'">
                
                <!--- Set the CK18 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK18='#inpCK18#'">
                
                <!--- Set the CK19 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK19='#inpCK19#'">
                
                <!--- Set the CK20 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK20='#inpCK20#'">
                
                <!--- Set the CK21 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK21='#inpCK21#'">
                
                <!--- Set the CK22 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK22='#inpCK22#'">
                
                <!--- Set the CK23 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK23='#inpCK23#'">
                
                <!--- Set the CK24 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK24='#inpCK24#'">
                
                <!--- Set the CK25 -   --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CK25='#inpCK25#'">
                                                                
                <!--- Set the CKX - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " X='#inpX#'">
                
                 <!--- Set the CKY - position info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " Y='#inpY#'">
                
                 <!--- Set the LINK - LINK to obj info --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " LINK='#inpLINKTo#'">
                
                <cfif inpRQ NEQ ""> 
               		<!--- Set the RQ --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " RQ='#inpRQ#'">
                </cfif>    
                
                <cfif inpCP NEQ ""> 
               		<!--- Set the CP --->
             		<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " CP='#inpCP#'">
                </cfif>    
                
                       
                
                <!--- 
					DS='0' - 0 is for scripts that use the generic boxes
					DSUID='562' - the user ID of the account you are distributing from
					*** CAMPID='946'- The script box ID - this changes for each campaign
					GSID='1'- the wave file generic script box type ID 1 through 10				
				 --->  
                             
                                 
                <!--- Set the MCID Decription --->
                <!--- Clean up XML? --->
             	<cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & " DESC='#XMLFormat(INPDESC)#'">
                                                             
                <!--- Close CCD XML --->
                <cfset RXTXMLSTRINGBuff = RXTXMLSTRINGBuff & ">0</ELE>">
           
              	<!--- Write to DB Batch Options --->
                                                                         
               <!---  <cfset UserSpecData = "#HTMLCodeFormat(myxmldocResultDoc)#" /> --->
               <!---  <cfset UserSpecData = "  SELECT ScriptLibrary_int, LASTUPDATED_DT, XMLControlString_vch FROM CallControl..BatchOptions (NOLOCK) WHERE BatchId_bi = #INPBATCHID#"/> --->
               
                    
				<cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML")>   
                <cfset QueryAddRow(dataout) />
			    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "#HTMLCodeFormat(RXTXMLSTRINGBuff)#") />
                <cfset QuerySetCell(dataout, "RAWXML", "#RXTXMLSTRINGBuff#") />

                 
            <cfcatch type="any">
            
            	<cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = "-2">                
                </cfif>	
                                                                 
                <cfset dataout = QueryNew("RXRESULTCODE, RXTXMLSTRING, RAWXML, MESSAGE, ERRMESSAGE ")> 
				<cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", "#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "RXTXMLSTRING", "") />
                <cfset QuerySetCell(dataout, "RAWXML", "") />
                <cfset QuerySetCell(dataout, "MESSAGE", "#cfcatch.MESSAGE#") />
                <cfset QuerySetCell(dataout, "ERRMESSAGE", "#cfcatch.detail#") />   
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>