<cfset MULTIARRAY = ArrayNew(1)>
<cfset Rxvid = "-1">
<cfset ISTTS = "no">
<cfset DataKey = "-1">

<cfset selectedElementsII = XmlSearch(XMLELEDoc,"/ELE/*")>
<cfloop array="#selectedElementsII#" index="CURR">
	<cfif CURR.XmlName EQ "ELE">
		<!--- Script --->
		<cfset multiElement = {eleId=0,desc=0,scriptID = 0,rxvid =0, dk=-1,type='script'} />
		<cfset multiElement.scriptID = CURR[ArrayLen(CURR)].XmlText>
		<cfset selectedElementId = XmlSearch(CURR, "@ID")>
		<cfset multiElement.eleId = selectedElementId[ArrayLen(selectedElementId)].XmlValue>
		
		<cfset selectedElementsRxvid = XmlSearch(CURR, "@RXVID")>
		<cfif ArrayLen(selectedElementsRxvid) GT 0>
			<cfset multiElement.rxvid = selectedElementsRxvid[ArrayLen(selectedElementsRxvid)].XmlValue >
		</cfif>
		<!--- Read data key--->
		<cfset selectedDataKey = XmlSearch(CURR, "@DK")>
		<cfif ArrayLen(selectedDataKey) GT 0>
			<cfset multiElement.dk = selectedDataKey[ArrayLen(selectedDataKey)].XmlValue >
		</cfif>
		<!--- Read Description --->	
		<cfset selectedElementsDes = XmlSearch(CURR, "@DESC")>
		<cfif ArrayLen(selectedElementsDes) GT 0>
			<cfset multiElement.desc = selectedElementsDes[ArrayLen(selectedElementsDes)].XmlValue>	
			<cfset ArrayAppend(MULTIARRAY, multiElement)>
		<cfelse>
			<!---Read data TTS toplevel---->
			<cfset ISTTS = "yes">
			<cfif ArrayLen(selectedElementsRxvid) GT 0>
				<cfset Rxvid = selectedElementsRxvid[ArrayLen(selectedElementsRxvid)].XmlValue >
			</cfif>
			<cfset CURRDATAID = CURR[ArrayLen(CURR)].XmlText>
			
			<cfif ArrayLen(selectedDataKey) GT 0>
				<cfset DataKey = selectedDataKey[ArrayLen(selectedDataKey)].XmlValue >
			</cfif>
		</cfif>
	</cfif>	
	<cfif CURR.XmlName EQ "SCRIPT">
		<!--- Switch ---> 
		<cfset multiElement = {switchValue=0, switchLocationKey=0, arrCase = ArrayNew(1),type='switch'} />
		<cfset selectedLc = XmlSearch(CURR, "@CASE")>
		<cfset multiElement.switchLocationKey = selectedLc[ArrayLen(selectedLc)].XmlValue>
		
		<cfset selectedCase = XmlSearch(CURR, "./CASE")>
		<cfloop array="#selectedCase#" index="caseIndex">
			<cfset caseObj={caseValue=0,eleId=0,desc=0,scriptID = 0}>
			<cfset selectedCaseValue = XmlSearch(caseIndex, "@VAL")>
			<cfset caseObj.switchValue = selectedCaseValue[ArrayLen(selectedCaseValue)].XmlValue>
			<cfset selectedEleCase = XmlSearch(caseIndex, "./ELE")>
			<cfloop array="#selectedEleCase#" index="eleCaseIndex">
				<cfset eleCaseIndexId = XmlSearch(eleCaseIndex, "@ID")>
				<cfset caseObj.eleId = eleCaseIndexId[ArrayLen(eleCaseIndexId)].XmlValue>
				<!--- get element child description --->
				<cfset eleChildDesc = XmlSearch(eleCaseIndex, "@DESC")>
				<cfset caseObj.desc = eleChildDesc[ArrayLen(eleChildDesc)].XmlValue>
				<!--- get case text --->
				<cfset caseObj.scriptID = eleCaseIndex[ArrayLen(eleCaseIndex)].XmlText>
			</cfloop>
			<cfset ArrayAppend(multiElement.arrCase, caseObj)>
		</cfloop>
		<cfset ArrayAppend(MULTIARRAY, multiElement)>
	</cfif>
	<cfif CURR.XmlName EQ "CONV">
		<!--- Script --->
		<cfset multiElement = {ck1=0,ck2=0,ck3=0,ck4=0, ck5=0, ck6=0, ck7=0, ck8=0, ck9=0, cp=0, rq=0,desc='',dataKey='',type='conv'} />
		<cfset multiElement.dataKey = CURR[ArrayLen(CURR)].XmlText>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK1")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck1 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK2")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck2 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK3")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck3 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK4")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck4 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK5")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck5 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK6")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck6 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK7")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck7 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK8")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck8 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCK = XmlSearch(CURR, "@CK9")>
		<cfif ArrayLen(selectedElementsCK) GT 0>
			<cfset multiElement.ck9 = selectedElementsCK[ArrayLen(selectedElementsCK)].XmlValue >
		</cfif>
		
		<cfset selectedElementsCP = XmlSearch(CURR, "@CP")>
		<cfif ArrayLen(selectedElementsCP) GT 0>
			<cfset multiElement.cp = selectedElementsCP[ArrayLen(selectedElementsCP)].XmlValue >
		</cfif>
		
		<cfset selectedElementsRQ = XmlSearch(CURR, "@RQ")>
		<cfif ArrayLen(selectedElementsRQ) GT 0>
			<cfset multiElement.rq = selectedElementsRQ[ArrayLen(selectedElementsRQ)].XmlValue >
		</cfif>
		
		<!--- Read Description --->	
		<cfset selectedElementsDes = XmlSearch(CURR, "@DESC")>
		<cfif ArrayLen(selectedElementsDes) GT 0>
			<cfset multiElement.desc = selectedElementsDes[ArrayLen(selectedElementsDes)].XmlValue>	
			<cfset ArrayAppend(MULTIARRAY, multiElement)>
		</cfif>
	</cfif>
</cfloop>