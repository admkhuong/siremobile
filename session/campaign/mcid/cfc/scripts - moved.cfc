<!--- No display --->
<cfcomponent output="false">

	<!--- Hoses the output...JSON/AJAX/ETC  so turn off debugging --->
    <cfsetting showdebugoutput="no" />
    
    <cfinclude template="../../../../public/paths.cfm" >

	<!--- Used IF locking down by session - User has to be logged in --->
	<cfparam name="Session.DBSourceEBM" default="Bishop"/> 
    <cfparam name="Session.UserID" default="0"/> 
    
	<!---<!---<cfinclude template="../../scripts/data_ScriptPaths.cfm">--->--->  
       
    <!--- ************************************************************************************************************************* --->
    <!--- Read an XML string from DB and parse CCD Values --->
    <!--- ************************************************************************************************************************* --->       
 
    <cffunction name="AddLibrary" access="remote" output="false" hint="Add a new library to the current user">
        <cfargument name="inpDesc" type="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
        
        	<!--- move to just Lib number
			<cfif inpDesc EQ "">
            	<cfset inpDesc = "LIB - #DateFormat(Now(),'yyyy-mm-dd') & " " & TimeFormat(Now(),'HH:mm:ss')#">
            </cfif>
			 --->
        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "NextLibId", 0) />     
            <cfset QuerySetCell(dataout, "message", "") />   
                        
       
            <cftry>
            
            <!--- Cleanup SQL injection --->
                    <!--- Replace ' with '' --->
                    <!--- Verify all numbers are actual numbers --->                    
                                        
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                    
					<cfset CreationCount = 0> 
					<cfset ExistsCount = 0> 
                    
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
            
					<!--- Get next Lib ID for current user --->               
                    <cfquery name="GetNextLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DSID_int) IS NULL THEN 1
                            ELSE
                                MAX(DSID_int) + 1 
                            END AS NextLibId  
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #Session.UserID#
                    </cfquery>  
                    
                    <cfif inpDesc EQ "">
						<cfset inpDesc = "Lib #GetNextLibId.NextLibId#">
		            </cfif>
                    
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="GetLibraries" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.DynamicScript
                        	(DSID_int, UserID_int, Active_int, Desc_vch )
                        VALUES
                      	  	(#GetNextLibId.NextLibId#, #Session.UserID#, 1, '#inpDesc#')                                        
                    </cfquery>       
                 
                 
	                <!--- Create Library Path --->
                   	<!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#">
                    					
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow message="Unable to create remote user directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "#rxdsLocalWritePath#/U#Session.UserID#/L#GetNextLibId.NextLibId#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                                    
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "NextLibId", "#GetNextLibId.NextLibId#") />
                    <cfset QuerySetCell(dataout, "message", "New Library created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "NextLibId", 0) />     
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch type="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, NextLibId, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "NextLibId", 0) />     
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    <cffunction name="AddElement" access="remote" output="false" hint="Add a new library to the current user">
        <cfargument name="inpLibId" type="string" default=""/>
        <cfargument name="inpDesc" type="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, NextEleId, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
            <cfset QuerySetCell(dataout, "NextEleId", 0) />     
            <cfset QuerySetCell(dataout, "message", "") />   
                        
       
            <cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId)>
                    	<cfthrow message="Invalid Numbers Specified" type="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                    
            
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow message="Invalid Library Specified" type="Any" extendedinfo="" errorcode="-2">
                    </cfif>
            
					<!--- Get next Ele ID for current user --->               
                    <cfquery name="GetNextEleId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DSEId_int) IS NULL THEN 1
                            ELSE
                                MAX(DSEId_int) + 1 
                            END AS NextEleId  
                        FROM
                            rxds.dselement
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                    </cfquery>  
        
					<cfif inpDesc EQ "">
                        <cfset inpDesc = "Ele #GetNextEleId.NextEleId#">
                    </cfif>
                        
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="AddElement" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.dselement
                        	(DSEId_int, DSID_int, UserID_int, Active_int, Desc_vch )
                        VALUES
                      	  	(#GetNextEleId.NextEleId#, #inpLibId#, #Session.UserID#, 1, '#inpDesc#')                                        
                    </cfquery>       
                 
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, NextEleId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                    <cfset QuerySetCell(dataout, "NextEleId", "#GetNextEleId.NextEleId#") />
                    <cfset QuerySetCell(dataout, "message", "New Element created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, NextEleId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                    <cfset QuerySetCell(dataout, "NextEleId", 0) />     
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch type="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, NextEleId, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                <cfset QuerySetCell(dataout, "NextEleId", 0) />     
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <cffunction name="AddScript" access="remote" output="false" hint="Add a new script to the current User - Lib - Ele">
        <cfargument name="inpLibId" type="string" default=""/>
        <cfargument name="inpEleId" type="string" default=""/>
        <cfargument name="inpDesc" type="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = 
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, NextScriptId, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
            <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />  
            <cfset QuerySetCell(dataout, "NextScriptId", 0) />     
            <cfset QuerySetCell(dataout, "message", "") />   
                        
       
            <cftry>
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId) OR !isnumeric(inpEleId) >
                    	<cfthrow message="Invalid Numbers Specified" type="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    <!--- Replace ' with '' --->
                    <cfset inpDesc = Replace(inpDesc, "'", "''", "ALL")>
                                
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow message="Invalid Library Specified" type="Any" extendedinfo="" errorcode="-2">
                    </cfif>
                    
                    
                    <!--- Validate valid library Id --->
                    <cfquery name="ValidateEleId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSEId_int) AS TotalCount
                        FROM
                            rxds.dselement
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND DSEId_int = #inpEleId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateEleId.TotalCount EQ 0>
                    	<cfthrow message="Invalid Ele Specified" type="Any" extendedinfo="" errorcode="-3">
                    </cfif>
                    
            
					<!--- Get next Script ID for current user --->               
                    <cfquery name="GetNextScriptId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            CASE 
                                WHEN MAX(DataId_int) IS NULL THEN 1
                            ELSE
                                MAX(DataId_int) + 1 
                            END AS NextScriptId  
                        FROM
                            rxds.scriptdata
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND DSEId_int = #inpEleId#
                    </cfquery>  
        
					<cfif inpDesc EQ "">
                        <cfset inpDesc = "Script #GetNextScriptId.NextScriptId#">
                    </cfif>
                        
                    <!--- slight issues if multiple users logged in at same time trying to add at exact same time  --->
                    <!--- Add record --->
                    <cfquery name="AddElement" datasource="#Session.DBSourceEBM#">
                        INSERT INTO rxds.scriptdata
                        	(DataId_int, DSEId_int, DSID_int, UserID_int, Active_int, Desc_vch, Length_int, Format_int, AltId_vch, StatusId_int )
                        VALUES
                      	  	(#GetNextScriptId.NextScriptId#, #inpEleId#, #inpLibId#, #Session.UserID#, 1, '#inpDesc#', 0, 0, '', 0)                                        
                    </cfquery>       
                 
                
                    <!--- All good --->
	                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, NextScriptId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                    <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />  
                    <cfset QuerySetCell(dataout, "NextScriptId", "#GetNextScriptId.NextScriptId#") />
                    <cfset QuerySetCell(dataout, "message", "New Element created OK") />    
        
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, NextScriptId, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                    <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") />  
                    <cfset QuerySetCell(dataout, "NextScriptId", 0) />     
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch type="any">
                
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpEleId, NextScriptId, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                <cfset QuerySetCell(dataout, "inpEleId", "#inpEleId#") /> 
                <cfset QuerySetCell(dataout, "NextScriptId", 0) />     
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                            
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>


 <cffunction name="RenameObject" access="remote" output="false" hint="Rename Object" returntype="string" returnformat="plain">
		<cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpEleId" type="string" default="0"/>
        <cfargument name="inpDataId" type="string" default="0"/>
        <cfargument name="inpNewDesc" type="string" default=""/>
        <cfargument name="inpOldDesc" type="string" default=""/>
        <cfset var dataout = '0' />    
                                         
                 
         <!--- 
        Positive is success
        1 = OK
		2 = 
		3 = 
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Invalid Lib
    
	     --->
          
                           
       	<cfoutput>
                       
        	<!--- Set default to original in case later processing goes bad --->
			<cfset dataout =  '#inpOldDesc#'>  
                               
       
        	<cftry> 
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
            
					
                                   
                    <!--- All good --->
	                <cfset dataout =  '#inpNewDesc#'>   
                   
                    <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId) OR !isnumeric(inpEleId)  OR !isnumeric(inpDataId)>
                    	<cfthrow message="Invalid Numbers Specified" type="Any" extendedinfo="" errorcode="-2">
                    </cfif> 
                    
                    <!--- Replace ' with '' --->
                    <cfset inpNewDesc = Replace(inpNewDesc, "'", "''", "ALL")>
                    
                    <cfif inpLibId GT 0>
                    
                    	<cfif inpEleId GT 0>
                        
                        	<cfif inpDataId GT 0>
                            
                            	<cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.scriptData
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = #Session.UserID#
                                        AND DSID_int = #inpLibId#
                                        AND DSEId_int = #inpEleId#
                                        AND DataId_int = #inpDataId#
                                </cfquery>                              
                            
                            <cfelse>
	                            <!--- No Script Id - Update Element Desc --->                                
                                <cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.dselement
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = #Session.UserID#
                                        AND DSID_int = #inpLibId#
                                        AND DSEId_int = #inpEleId#
                                </cfquery>  
                            
                            </cfif>
                            
                        <cfelse>
    	                    <!--- No Element Id - Update Library Desc --->
	                            <cfquery name="UpdateDesc" datasource="#Session.DBSourceEBM#">
                                    UPDATE
                                        rxds.DynamicScript
                                    SET 
                                    	Desc_vch = '#inpNewDesc#'
                                    WHERE
                                        UserId_int = #Session.UserID#
                                        AND DSID_int = #inpLibId#
                                </cfquery>  
		                    
                        </cfif>
                    
                    <cfelse>
                    	<!--- No Library Id --->
	                    <cfset dataout =  '#inpOldDesc#'> 
                    </cfif>
                        
                  <cfelse>
                    <!--- No user id --->
                    <cfset dataout =  '#inpOldDesc#'>                    
                  </cfif>          
                           
            <cfcatch type="any">
	             <!--- Severe Error - Revert --->
				 <cfset dataout =  '#inpOldDesc#'>                            
            </cfcatch>
            
            </cftry>     
        
		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemotePaths" access="remote" output="false" hint="Verify all Script Library paths are valid in remote location User - Lib - Ele">
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpDialerIPAddr" type="string" default="0.0.0.0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - All Paths already exist
		2 = OK - Partial Paths Existed - New Paths Added
		3 = OK - OK - No Paths were there but now all paths have been created
		4 = 
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = No paths to create
    
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
            <cfset QuerySetCell(dataout, "message", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry>
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId)>
                    	<cfthrow message="Invalid Library Id type Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            COUNT(DSID_int) AS TotalCount
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.TotalCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                    
                   
                   	<!--- Does User Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#/U#Session.UserID#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Lib Directory Exist --->
                    <cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#inpLibId#">
                    
                   	<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                   		
                        <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                        <cfdirectory action="create" directory="#CurrRemoteUserPath#">

						<cfset CreationCount = CreationCount + 1> 
                        
                        <!--- Still doesn't exist - check your access settings --->
                        <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                        	<cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                        </cfif>
                        
					<cfelse>
                    
                    	<cfset ExistsCount = ExistsCount + 1> 
					
                    </cfif>
                    
                    
                    <!--- Does Ele Directory(s) Exist --->
                    <!--- Get list of all elements --->
                    <cfquery name="GetElements" datasource="#Session.DBSourceEBM#">
                        SELECT                            
                            DSEId_int                                                
                        FROM
                            rxds.dselement
                        WHERE
                            Active_int = 1
                            AND DSID_int = #inpLibId#
                            AND UserId_int = #Session.UserID#                                   
                    </cfquery>       
                    
                    <cfloop query="GetElements">
                    
                     
                    	<cfset CurrRemoteUserPath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#inpLibId#/E#GetElements.DSEId_int#">
                    
						<cfif !DirectoryExists("#CurrRemoteUserPath#")>
                            
                            <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                            <cfdirectory action="create" directory="#CurrRemoteUserPath#">
    
                            <cfset CreationCount = CreationCount + 1> 
                            
                            <!--- Still doesn't exist - check your access settings --->
                            <cfif !DirectoryExists("#CurrRemoteUserPath#")>
                                <cfthrow message="Unable to create remote user script library directory " type="Any" detail="Failed to create #CurrRemoteUserPath# - Check your permissions settings." errorcode="-2">
                            </cfif>
                            
                        <cfelse>
                        
                            <cfset ExistsCount = ExistsCount + 1> 
                        
                        </cfif>
                    
                    
                    </cfloop>
                    
                                 
                    
                    <!--- Four possibilities --->
                   	<cfif CreationCount EQ 0 AND ExistsCount GT 0>
                   
					  	<!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, message")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 1) />
                       	<cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                       	<cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                   		<cfset QuerySetCell(dataout, "message", "OK - All Paths already exist") />    
                    
                   	<cfelseif CreationCount GT 0 AND ExistsCount GT 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, message")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                       	<cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                       	<cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                   		<cfset QuerySetCell(dataout, "message", "OK - Partial Paths Existed - New Paths Added") />   
                    
                    <cfelseif CreationCount GT 0 AND ExistsCount EQ 0>
                    
	                    <!--- All good --->
                       	<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, message")>  
                       	<cfset QueryAddRow(dataout) />
                       	<cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                       	<cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                       	<cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                   		<cfset QuerySetCell(dataout, "message", "OK - No Paths were there but now all paths have been created") />                                    
                   	<cfelse>
						<cfthrow message="Unable to create any user script library directory paths." type="Any" detail="Failed to create any paths - Systme Error" errorcode="-4"> 
                   	</cfif>
                 
                        
                  <cfelse>
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif>          
                           
            <cfcatch type="any">
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpDialerIPAddr, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") /> 
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                                                        
            </cfcatch>
            
            </cftry>     
        

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptData" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpNextEleId" type="string" default="-1"/>
        <cfargument name="inpNextDataId" type="string" default="-1"/>
        <cfargument name="inpDialerIPAddr" type="string" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" type="string" default="0"/>
        <cfset var dataout = '0' />    
                                      
                 
         <!--- 
        Positive is success
        1 = OK - Complete
		2 = OK - File Written
		3 = WARNING - Main File Does not Exists
		4 = OK - File Up To Date
		5 = WARNING - No elements in Library
		6 = WARNING - No scripts in element - no more elements
		7 = WARNING - No scripts in element
		
	
        Negative is failure
        -1 = general failure
        -2 = Session Expired
        -3 = Bad data specified
		-4 = Error invalid data specified
    
	if inpNextEleId GT 0 - keep going
	
	inpNextDataId can be 0 - use -1 to start at beginning of Library 
	
	     --->
          
                           
       	<cfoutput>
                	        
        	<!--- Set default to error in case later processing goes bad --->
			<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
            <cfset QueryAddRow(dataout) />
            <cfset QuerySetCell(dataout, "RXRESULTCODE", -1) />
            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
            <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 
            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") /> 
            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
            <cfset QuerySetCell(dataout, "message", "general failure") />   
                     
            <cfset CreationCount = 0>  
            <cfset ExistsCount = 0>            
       
            <cftry> 
            
	            <!--- Set here only so if changes can be quickly updated --->
				<!---<cfinclude template="../scripts/data_ScriptPaths.cfm">--->
            
            	<!--- Validate session still in play - handle gracefully if not --->
            	<cfif Session.UserID GT 0>
                
	                <!--- Cleanup SQL injection --->
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpLibId)>
                    	<cfthrow message="Invalid Library Id Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpNextEleId)>
                    	<cfthrow message="Invalid Element Id Specified" type="Any" detail="(#inpNextEleId#) is invalid." errorcode="-3">
                    </cfif>
                    
                    <!--- Verify all numbers are actual numbers --->                    
                    <cfif !isnumeric(inpNextDataId)>
                    	<cfthrow message="Invalid Script Id Specified" type="Any" detail="(#inpNextDataId#) is invalid." errorcode="-3">
                    </cfif>
                                                    
            		<!--- Validate valid library Id --->
                    <cfquery name="ValidateLibId" datasource="#Session.DBSourceEBM#">
                        SELECT
                            Desc_vch
                        FROM
                            rxds.DynamicScript
                        WHERE
                            UserId_int = #Session.UserID#
                            AND DSID_int = #inpLibId#
                            AND Active_int = 1
                    </cfquery>  
            
            		<cfif ValidateLibId.RecordCount EQ 0>
                    	<cfthrow message="Invalid Library Id Specified" type="Any" detail="(#inpLibId#) is invalid." errorcode="-3">
                    </cfif>
                    
                                        
                    <cfif inpNextEleId EQ 0><!--- What data to look for--->
                    	<!--- Get first element --->
                        <cfquery name="GetFirstElement" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DSEId_int,
                                Desc_vch                    
                            FROM
                                rxds.dselement
                            WHERE
                                Active_int = 1
                                AND DSID_int = #inpLibId#                                
                                AND UserId_int = #Session.UserID#
                            ORDER BY
                                DSEId_int ASC
                            LIMIT 1            
                        </cfquery>
                        
                        <cfif GetFirstElement.RecordCount EQ 0><!--- Check Element Records --->
							
                            <!--- All still good? Warning Only? TTS Only? --->
							<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 5) />
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                            <cfset QuerySetCell(dataout, "inpNextEleId", "-1") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "-1") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                            <cfset QuerySetCell(dataout, "message", "WARNING - No Elements Found in Library ID (#inpLibId#) Library Name(#ValidateLibId.Desc_vch#") />
                            
                            
                        <cfelse><!--- Check Element Records --->
                        	
							<!--- Get first script --->
                            <cfquery name="GetFirstScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int,
                                    DSEId_int,
                                    DSId_int,
                                    UserId_int,
                                    StatusId_int,
                                    AltId_vch,
                                    Length_int,
                                    Format_int,
                                    Desc_vch
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#
                                    AND DSEId_int = #GetFirstElement.DSEId_int#
                                    AND UserId_int = #Session.UserID#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
							<cfif GetFirstScript.RecordCount EQ 0><!--- Check Script Records --->
                                                            
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#                                
                                        AND UserId_int = #Session.UserID#
                                        AND DSEId_int > #GetFirstElement.DSEId_int#
                                    ORDER BY
                                        DSEId_int ASC
                                    LIMIT 1              
                                </cfquery>
                                                                
                                <cfif GetNextElement.RecordCount LT 1>
                                	<cfset inpNextEleId = -1>
                                    <cfset inpNextDataId = -1>
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                    <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                    <cfset QuerySetCell(dataout, "message", "WARNING - Finished - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#) - No more Elements found.")  />
                                
                                <cfelse>                                
	                                
									<!--- <cfthrow message="Made it here 1" type="Any" detail="(#inpLibId#) is invalid." errorcode="-15"> --->
									<cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                    
                                    <!--- Start at first script ID --->
                                    <cfset inpNextDataId = -1>
                                    
                                     <!--- All still good? Warning Only? TTS Only? --->
									<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                                    <cfset QueryAddRow(dataout) />
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 7) />
                                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                    <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                    <cfset QuerySetCell(dataout, "message", "WARNING - Moving on - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#GetFirstElement.DSEId_int#) Element Name(#GetFirstElement.Desc_vch#")  />
                                
                                </cfif>
                                
                            <cfelse><!--- Check Script Records --->
                            
                   
                           
                            
                            	<!--- _PhoneRes = Phone Resolustion --->	
            					<cfset MainConversionfile = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#GetFirstElement.DSEId_int#\PhoneRes\RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">     
                                
                                <cfset MainConversionfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#GetFirstElement.DSEId_int#\PhoneRes">           
                            
                            	<!--- Get current side last modified date --->                                
                                <cfset MainfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#GetFirstElement.DSEId_int#\RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
							<!--- 	<cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                                <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
 --->
  
								<!--- Verify Directory Exists --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                            
                                    <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                    <cfdirectory action="create" directory="#MainConversionfilePath#">
            
                                    <cfset CreationCount = CreationCount + 1> 
                                    
                                    <!--- Still doesn't exist - check your access settings --->
                                    <cfif !DirectoryExists("#MainConversionfilePath#")>
                                        <cfthrow message="Unable to create remote phone resolution script library directory " type="Any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                    </cfif>
                                    
                                <cfelse>
                                
                                    <cfset ExistsCount = ExistsCount + 1> 
                                
                                </cfif>
                            
								<cfset MainPathFileExists = FileExists(MainfilePath)>
                                <cfif MainPathFileExists> 
                                    <cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                                <cfelse>
                                     <cfset MainfileDate = '1901-01-01 00:00:00'>
                                </cfif>
                            
 
                                <!--- Get remote last modified date --->
                              	<cfset RemotefilePath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#inpLibId#/E#GetFirstElement.DSEId_int#\RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav">
								<!--- <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                                <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())> --->
								<cfset RemotePathFileExists = FileExists(RemotefilePath)>
                                <cfif RemotePathFileExists> 
									<cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                                <cfelse>
                                     <cfset RemotefileDate = '1900-01-01 00:00:00'>
                                </cfif>
                                
                                <!--- Start Result set Here --->
                                <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>        
                                <cfset QueryAddRow(dataout) />                     	
                                                        
                                <!--- if same dont update unless forced too --->
								<cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                               
                                    <cfif MainPathFileExists>
                                        
										<cfif FileExists(MainConversionfile)> 
                                            <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                        <cfelse>
                                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                        </cfif>
                                
                                         <!--- Compare phoneres with main before generating --->
                                        <!--- Only reconvert if file is out of date --->
                                        <cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                        
                                            <!--- Convert File --->
                                            <!--- /fmt WAV = .wav NOTE: This IS case sensitive--->
                                            <!--- /wavm 0 = uncompressed --->
                                            <!--- /wavf 7= 11025 Hz --->
                                            <!--- /wavc 1 = mono --->
                                            <!--- wavbd 0 = 16 Bit --->
                                            <!--- /owe = overwirte existing files --->
                                            <cftry>
                                                <cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                                </cfexecute>
                                            
                                            <cfcatch type="any">
                                            
                                            
                                            </cfcatch>
                                            
                                            </cftry>
                                            
                                        </cfif>
                                        
                                        <!--- Validate new audio created OK--->
                                        <cfif FileExists(MainConversionfile)> 
                                        
                                            <!--- Copy file --->
                                            <cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                        
                                        <cfelse>
                                        
                                            <cfthrow message="Unable to create local phone resolution script library file" type="Any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                        
                                        </cfif>
                                                                                
                                        
                                        <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                        
                                        <cfif RemotePathFileExists>                                        
                                        	<cfset QuerySetCell(dataout, "message", "OK - File Copied - RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />                             
                             			<cfelse>                                         
										 	<cfset QuerySetCell(dataout, "message", "OK - File Copied - RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                             			
                                        </cfif>
                             
                             
                                       
                                     <cfelse>
                                        
                                          <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                          <cfset QuerySetCell(dataout, "message", "WARNING - File Does not Exists - RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch#")  />
                                        
                                     </cfif>
                                     
                                     
                                <cfelse><!--- Date Compare --->
                                
                                    <!--- Skip file ---> 
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                    
                                    <cfif RemotePathFileExists> 
	                                    <cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                    <cfelse>                                    
                                     	<cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - RXDS_#Session.UserID#_#inpLibId#_#GetFirstElement.DSEId_int#_#GetFirstScript.DataId_int#.wav #GetFirstScript.Desc_vch# (#MainfileDate#) (N/A)")  />                                     
                                    </cfif>
                                 
                                </cfif><!--- Date Compare --->
                            
                                
                                
                                <!--- Check for next script --->
                                <!--- Get Next script --->
                                <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DataId_int                                        
                                    FROM
                                        rxds.scriptdata
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#
                                        AND DSEId_int = #GetFirstElement.DSEId_int#
                                        AND UserId_int = #Session.UserID#
                                        AND DataId_int > #GetFirstScript.DataId_int#
                                    ORDER BY
                                        DataId_int ASC
                                    LIMIT 1              
                                </cfquery>                               		
                                                            
                                <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records II--->
                                	                                
                                    <!--- Move on - Get next element --->                              
                                    <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                        SELECT 
                                            DSEId_int,
                                            Desc_vch                    
                                        FROM
                                            rxds.dselement
                                        WHERE
                                            Active_int = 1
                                            AND DSID_int = #inpLibId#                                
                                            AND UserId_int = #Session.UserID#
                                            AND DSEId_int > #GetFirstElement.DSEId_int#
                                        ORDER BY
                                            DSEId_int ASC            
                                        LIMIT 1  
                                    </cfquery>
                                    
                                    <cfif GetNextElement.RecordCount LT 1>
                                        <cfset inpNextEleId = -1>
                                        <cfset inpNextDataId = -1>
                                    <cfelse>                                
                                        
										<!--- <cfthrow message="Made it here 2" type="Any" detail="(#inpLibId#) is invalid." errorcode="-15"> --->                                        
										<cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                        
                                        <!--- Start at first script ID --->
                                        <cfset inpNextDataId = -1>
                                    </cfif>
                                                                    
                                <cfelse><!--- Check Script Records II --->
                                
	                                
    	                            <!--- <cfset inpNextEleId = GetNextElement.DSEId_int> --->
                                    <cfset inpNextEleId = GetFirstElement.DSEId_int>
                                    <!--- <cfthrow message="Made it here 3" type="Any" detail="(#inpLibId#) is invalid." errorcode="-15"> --->
                                    
	   								<cfset inpNextDataId = GetNextScript.DataId_int>
                                </cfif><!--- Check Script Records II --->       
                                
                                <!--- All still good --->
                           		<cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />       
                                <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                                <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") />  
                                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                                                        
							</cfif><!--- Check Script Records --->

                        </cfif><!--- Check Element Records --->
                        
                    
                    <cfelseif inpNextEleId GT 0><!--- What data to look for--->
                    	<!--- Get as specified --->
                        <!--- Get script ---> <!--- Always start where you specify and work your way up --->
                        <cfquery name="GetScript" datasource="#Session.DBSourceEBM#">
                            SELECT 
                                DataId_int,
                                DSEId_int,
                                DSId_int,
                                UserId_int,
                                StatusId_int,
                                AltId_vch,
                                Length_int,
                                Format_int,
                                Desc_vch
                            FROM
                                rxds.scriptdata
                            WHERE
                                Active_int = 1
                                AND DSID_int = #inpLibId#
                                AND DSEId_int = #inpNextEleId#
                                AND UserId_int = #Session.UserID#
                                AND DataId_int > #inpNextDataId# - 1
                            ORDER BY
                                DataId_int ASC
                            LIMIT 1              
                        </cfquery>      
                        
                       <cfif GetScript.RecordCount EQ 0><!--- Check Script Records III --->
                       
                       		<!--- Move on - Get next element --->                              
                            <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DSEId_int,
                                    Desc_vch                    
                                FROM
                                    rxds.dselement
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#                                
                                    AND UserId_int = #Session.UserID#
                                    AND DSEId_int > #inpNextEleId#
                                ORDER BY
                                    DSEId_int ASC            
                                LIMIT 1  
                            </cfquery>
                            
                            <cfset LastinpNextEleId = inpNextEleId>
                            
                            <cfif GetNextElement.RecordCount LT 1>
                                <cfset inpNextEleId = -1>
                                <cfset inpNextDataId = -1>
                            <cfelse>                                
                                
								<!--- <cfthrow message="Made it here 4" type="Any" detail="(#inpLibId#) is invalid." errorcode="-15"> --->
								<cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                                                
                                
                                <!--- Start at first script ID --->
                                <cfset inpNextDataId = -1>
                            </cfif>
                                                  
                            
                            <!--- All still good? Warning Only? TTS Only? --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                            <cfset QueryAddRow(dataout) />
                            <cfset QuerySetCell(dataout, "RXRESULTCODE", 6) />
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                            <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "0") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                            <cfset QuerySetCell(dataout, "message", "WARNING - Moving on - No Script Data Found in Library Id(#inpLibId#) Library Name(#ValidateLibId.Desc_vch#) Element Id(#LastinpNextEleId#)")  />
                                
                       
                       <cfelse><!--- Check Script Records III --->
                       <!--- Get current side last modified date --->                                
		
        					<!--- _PhoneRes = Phone Resolustion --->	
            				<cfset MainConversionfile = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpNextEleId#\PhoneRes\RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav">       
                            <cfset MainConversionfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpNextEleId#\PhoneRes">            
            
            				<cfset MainfilePath = "#rxdsLocalWritePath#/U#Session.UserID#/L#inpLibId#/E#inpNextEleId#\RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav">
                           <!---  <cfset MainfileObj = createObject("java","java.io.File").init(expandPath(MainfilePath))>
                            <cfset MainfileDate = createObject("java","java.util.Date").init(MainfileObj.lastModified())>
                            --->     
                            
                            <!--- Verify Directory Exists --->
							<cfif !DirectoryExists("#MainConversionfilePath#")>
                        
                                <!--- Need to have WebAdmin as service login and WebAdmin as a local user account on the RXDialer --->
                                <cfdirectory action="create" directory="#MainConversionfilePath#">
        
                                <cfset CreationCount = CreationCount + 1> 
                                
                                <!--- Still doesn't exist - check your access settings --->
                                <cfif !DirectoryExists("#MainConversionfilePath#")>
                                    <cfthrow message="Unable to create remote phone resolution script library directory " type="Any" detail="Failed to create #MainConversionfilePath# - Check your permissions settings." errorcode="-2">
                                </cfif>
                                
                            <cfelse>
                            
                                <cfset ExistsCount = ExistsCount + 1> 
                            
                            </cfif>
                                
                                
                            <cfset MainPathFileExists = FileExists(MainfilePath)>
                            <cfif MainPathFileExists> 
                            	<cfset MainfileDate = GetFileInfo(MainfilePath).lastmodified>   
                            <cfelse>
	                             <cfset MainfileDate = '1901-01-01 00:00:00'>
                            </cfif>
                                                        
                            <!--- Get remote last modified date --->
                            <cfset RemotefilePath = "\\#inpDialerIPAddr#\#rxdsRemoteRXDialerPath#/U#Session.UserID#/L#inpLibId#/E#inpNextEleId#\RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav">
                          <!---   <cfset RemotefileObj = createObject("java","java.io.File").init(expandPath(RemotefilePath))>
                            <cfset RemotefileDate = createObject("java","java.util.Date").init(RemotefileObj.lastModified())>
                             --->
                                                        
                            <cfif FileExists(MainConversionfile)> 
                            	<cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                            <cfelse>
	                             <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                            </cfif>
                            
                            <cfset RemotePathFileExists = FileExists(RemotefilePath)>
                            <cfif RemotePathFileExists> 
                                <cfset RemotefileDate = GetFileInfo(RemotefilePath).lastmodified>   
                            <cfelse>
                                 <cfset RemotefileDate = '1900-01-01 00:00:00'>
                            </cfif>
                             
                            <!--- Start Result set Here --->
                            <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>                          
                            <cfset QueryAddRow(dataout) />   	
                            
                            <!--- if same dont update unless forced too --->
                            <cfif DateCompare(MainfileDate,RemotefileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->
                           
                           		<cfif MainPathFileExists>
                                   
                                 	<cfif FileExists(MainConversionfile)> 
                                        <cfset MainConversionfileDate = GetFileInfo(MainConversionfile).lastmodified>   
                                    <cfelse>
                                         <cfset MainConversionfileDate = '1900-01-01 00:00:00'>
                                    </cfif>
                            
		                             <!--- Compare phoneres with main before generating --->
                            		<!--- Only reconvert if file is out of date --->
									<cfif DateCompare(MainfileDate,MainConversionfileDate) EQ 1 OR inpForceUpdate GT 0 ><!--- Date Compare --->                             
                                    
										<!--- Convert File --->
                                        <!--- /fmt WAV = .wav NOTE: This IS case sensitive --->
                                        <!--- /wavm 0 = uncompressed --->
                                        <!--- /wavf 7= 11025 Hz --->
                                        <!--- /wavc 1 = mono --->
                                        <!--- wavbd 0 = 16 Bit --->
                                        <!--- /owe = overwirte existing files --->
                                        <cftry>
                                            <cfexecute name="#myAudiopath#" arguments=' /f #MainfilePath# /op #MainConversionfilePath# /fmt WAV /wavm 0 /wavf 7 /wavc 1 /wavbd 0 /owe'  timeout="3000"  >
                                            </cfexecute>
                                        
                                        <cfcatch type="any">
                                        
                                        
                                        </cfcatch>
                                        
                                        </cftry>
                                        
                                    </cfif>
                                    
                                    <!--- Validate new audio created OK--->
                                    <cfif FileExists(MainConversionfile)> 
                                    
                                    	<!--- Copy file --->
                                    	<cffile action="copy" source="#MainConversionfile#" destination="#RemotefilePath#" nameconflict="overwrite" >
                                    
                                    <cfelse>
                                    
                                    	<cfthrow message="Unable to create local phone resolution script library file" type="Any" detail="Failed to create #MainConversionfile# - Check your permissions settings." errorcode="-2"> 
                                    
                                    </cfif>
                                    
                                    
                                    <cfset QuerySetCell(dataout, "RXRESULTCODE", 2) />
                                    
                                    <cfif RemotePathFileExists> 
                                    	<cfset QuerySetCell(dataout, "message", "OK - File Copied - RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />			<cfelse>
                                       	<cfset QuerySetCell(dataout, "message", "OK - File Copied - RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />		                                     </cfif>
                                    
                                 <cfelse>
                                    
                                      <cfset QuerySetCell(dataout, "RXRESULTCODE", 3) />
                                      <cfset QuerySetCell(dataout, "message", "WARNING - Main File Does not Exists - RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch#")  />
                                    
                                 </cfif>
                                 
                                 
                            <cfelse><!--- Date Compare --->
                            
                                <!--- Skip file ---> 
                                <cfset QuerySetCell(dataout, "RXRESULTCODE", 4) />
                                
								<cfif RemotePathFileExists>
	                                <cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (#RemotefileDate#)")  />
                                <cfelse>
									<cfset QuerySetCell(dataout, "message", "OK - File Already Up To Date - RXDS_#Session.UserID#_#inpLibId#_#inpNextEleId#_#GetScript.DataId_int#.wav #GetScript.Desc_vch# (#MainfileDate#) (N/A)")  />
                                </cfif>
                             
                            </cfif><!--- Date Compare --->
                            
                            
                            <!--- Check for next script --->
                            <!--- Get Next script --->
                            <cfquery name="GetNextScript" datasource="#Session.DBSourceEBM#">
                                SELECT 
                                    DataId_int                                        
                                FROM
                                    rxds.scriptdata
                                WHERE
                                    Active_int = 1
                                    AND DSID_int = #inpLibId#
                                    AND DSEId_int = #inpNextEleId#
                                    AND UserId_int = #Session.UserID#
                                    AND DataId_int > #GetScript.DataId_int#
                                ORDER BY
                                    DataId_int ASC
                                LIMIT 1              
                            </cfquery>                               		
                                                        
                            <cfif GetNextScript.RecordCount EQ 0><!--- Check Script Records IV--->
                                                                
                                <!--- Move on - Get next element --->                              
                                <cfquery name="GetNextElement" datasource="#Session.DBSourceEBM#">
                                    SELECT 
                                        DSEId_int,
                                        Desc_vch                    
                                    FROM
                                        rxds.dselement
                                    WHERE
                                        Active_int = 1
                                        AND DSID_int = #inpLibId#                                
                                        AND UserId_int = #Session.UserID#
                                        AND DSEId_int > #inpNextEleId#
                                    ORDER BY
                                        DSEId_int ASC            
                                    LIMIT 1  
                                </cfquery>
                                
                                <cfif GetNextElement.RecordCount LT 1>
	                                <!--- If no more data and no more elements then just exit --->
                                    <cfset inpNextEleId = -1>
                                    <cfset inpNextDataId = 0>
                                <cfelse>                                
                                
                                	<!--- <cfthrow message="Made it here 5" type="Any" detail="(#inpLibId#) is invalid." errorcode="-15"> --->
                                
                                    <cfset inpNextEleId = #GetNextElement.DSEId_int#>
                                    <!--- Start at first script ID --->
                                    <cfset inpNextDataId = 0>
                                </cfif>
                                                                
                            <cfelse><!--- Check Script Records IV --->
                                <cfset inpNextEleId = inpNextEleId>
                                <cfset inpNextDataId = GetNextScript.DataId_int>
                            </cfif><!--- Check Script Records IV --->       
                            
                            <!--- All still good --->
                            <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />       
                            <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 	
                            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") />  
                            <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                       
                       </cfif><!--- Check Script Records III --->
                    
                    <cfelse><!--- What data to look for--->
                    	<!--- No valid data found --->
                         	<cfthrow message="Invalid Data Specified" type="Any" detail="Data specified in the request is invalid." errorcode="-4">  
                    
                    </cfif><!--- What data to look for--->
                        
                        
                  <cfelse><!--- Validate session still in play - handle gracefully if not --->
                        
                    <cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, message")>  
                    <cfset QueryAddRow(dataout) />
                    <cfset QuerySetCell(dataout, "RXRESULTCODE", -2) />
                    <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") /> 
                    <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 
		            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") /> 
                    <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") />  
                    <cfset QuerySetCell(dataout, "message", "User Session is expired") />     
                  
                  </cfif><!--- Validate session still in play - handle gracefully if not --->        
                           
             <cfcatch type="any"> 
                 
                <cfif cfcatch.errorcode EQ "">
                	<cfset cfcatch.errorcode = -1>
                </cfif>
                 
				<cfset dataout =  QueryNew("RXRESULTCODE, inpLibId, inpNextEleId, inpNextDataId, inpDialerIPAddr, type, message, ErrMessage")>  
                <cfset QueryAddRow(dataout) />
                <cfset QuerySetCell(dataout, "RXRESULTCODE","#cfcatch.errorcode#") />
                <cfset QuerySetCell(dataout, "inpLibId", "#inpLibId#") />  
                <cfset QuerySetCell(dataout, "inpNextEleId", "#inpNextEleId#") /> 
	            <cfset QuerySetCell(dataout, "inpNextDataId", "#inpNextDataId#") /> 
                <cfset QuerySetCell(dataout, "inpDialerIPAddr", "#inpDialerIPAddr#") /> 
                <cfset QuerySetCell(dataout, "type", "#cfcatch.type#") />
				<cfset QuerySetCell(dataout, "message", "#cfcatch.message#") />                
                <cfset QuerySetCell(dataout, "ErrMessage", "#cfcatch.detail#") />  
                                                         
            </cfcatch>
            
            </cftry>   

		</cfoutput>

        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemotePathsLegacy" access="remote" output="false" hint="Verify all Script Library paths are valid in remote location User - Lib - Ele">
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpDialerIPAddr" type="string" default="0.0.0.0"/>
        <cfset var dataout = '0' />    
        
		<!---  --->
        
        <cfreturn dataout />
    </cffunction>
    
    
    
    <!--- Validate paths is on remote dialer --->
    <cffunction name="ValidateRemoteScriptDataLegacy" access="remote" output="false" hint="Verify all Script Library data files are valid in remote location User - Lib - Ele">
        <cfargument name="inpLibId" type="string" default="0"/>
        <cfargument name="inpNextEleId" type="string" default="-1"/>
        <cfargument name="inpNextDataId" type="string" default="-1"/>
        <cfargument name="inpDialerIPAddr" type="string" default="0.0.0.0"/>
        <cfargument name="inpForceUpdate" type="string" default="0"/>
        <cfset var dataout = '0' />    
                                      
       <!---  --->

        <cfreturn dataout />
    </cffunction>
    
    
    
    
    
</cfcomponent>