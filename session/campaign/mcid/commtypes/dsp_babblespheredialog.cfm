<cfparam name="inpGroupId" default="0">
<script>
$(function() {
	
	var lastsel=0;
	$("#ScriptList").jqGrid({        
		url:'<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=GetBabbleRecording&flash='+plugInCheck[0]+'&socialNetwork='+socialNetwork+'&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true&sidx=created_dt&sord=DESC',
		datatype: "json",
		height: "auto",
		//colNames:['Option', 'SNo','Script Notes','Play Length','Options'],
		colModel:[		
			{name:'DESC_VCH',index:'DESC_VCH', width:340, editable:false},
			{name:'decimalformat(GetRecording.Length_int/1024)',index:'Length_int', width:40, editable:false,align:'center'},
			{name:'Babbles',index:'', width:210, editable:false,align:'center'}
		],
		rowNum:10,
	   	rowList:[5,10,15,20,25],
		mtype: "POST",
		pager: jQuery('#pagerb'),
		toppager: true,
		emptyrecords: "Nothing to display.",
		pgbuttons: true,
		altRows: true,
		altClass: "ui-priority-altRow",
		pgtext: false,
		pginput:true,
		sortname: 'DATAID_INT',
		viewrecords: true,
		sortorder: "asc",
		caption: "",	
		multiselect: false,
		loadComplete: function(data){ 
				$("#ScriptList").css('margin','10px');		 
				$(".edit_Row").hover(function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 		$(".edit_Row").click( function() { EditScripts($(this).attr('rel')); } );
   			},	
	  	jsonReader: {
		  root: "ROWS", //our data
		  page: "PAGE", //current page
		  total: "TOTAL", //total pages
		  records:"RECORDS", //total records
		  cell: "CELL",
		  id: "ID" //will default first column as ID
	  	}	
	});
	
});

</script>
        	<div id="pagerb"></div>
            <div style="text-align:left;">
                <table id="ScriptList"></table>
            </div>
