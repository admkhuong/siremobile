<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfsavecontent variable="htmlContent">
	<cfoutput>
		<form name="form" action="" method="POST" enctype="multipart/form-data" id="frmTwitterObject">
	    <input TYPE="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#">
		<div class="CommSection" >
			<div class="item" >
			    <label>Publish type:</label>
			    <div class="content">
		    		&nbsp; Image <input type="radio" name="inpPublishType" value="image" checked="checked" />
		    		&nbsp; Text <input type="radio" name="inpPublishType" value="text" />
		    		&nbsp; Babbles <input type="radio" name="inpPublishType" value="babble" />
		    		&nbsp; Audio <input type="radio" name="inpPublishType" value="audio"/>
		     	</div>
			</div>
			<div id="imageArea" style="" class="CommFacebookTypeArea">
				<div class="item">
				    <label>Browse Image:</label>
				    <div class="content">
			    		<input TYPE="file" name="inpImgFile" id="inpImgFile" accept="image/*" size="30" onchange="uploadTwittermedia('image')"/>
			    		<input TYPE="hidden" name="inpImgServerFile" id="inpImgServerFile" />
			    		<div id="imgPreview"></div>
			     	</div>
				</div>
				<div class="item">
				    <label>Status:</label>
				    <div class="content">
					    <div style="padding-left:20px;font-size:10px">Max 100 characters</div>
			    		<textarea TYPE="text" name="inpImgStatus" id="inpImgStatus" maxlength="100" ></textarea>
			     	</div>
				</div>
		    	<div class="spacer clear"></div>
			</div>
			
			<div id="textArea" style="display:none;" class="CommTwitterTypeArea">
				<div class="item">
				    <label>Status:</label>
				    <div class="content">
					    <div style="padding-left:20px;font-size:10px">Max 120 characters</div>
			    		<textArea name="inpTextStatus" id="inpTextStatus" maxlength="120" ></textarea>
			     	</div>
				</div>
		    	<div class="spacer clear"></div>
			</div>
			
			<div id="babbleArea" style="display:none;" class="CommTwitterTypeArea">
				<div class="item">
					<label>Load babbles: </label>
					<div class="content">
			    		<button class="inpLoadBabble" title="Load babbles" id="inpLoadBabble" >Load</button>
			    		<input TYPE="hidden" name="inpBabbleScript" id="inpBabbleScript" >
			    		<span id="babblePreview"></span>
			     	</div>
		     	</div>
		     	<div class="item">
				    <label>Status:</label>
				    <div class="content">
					    <div style="padding-left:20px;font-size:10px">Max 100 characters</div>
			    		<textarea name="inpBabbleStatus" id="inpBabbleStatus" maxlength="100"></textarea>
			     	</div>
				</div>
		     	<div class="spacer clear"></div>
			</div>
			
			<div id="audioArea" style="display:none;" class="CommTwitterTypeArea" >
				<div class="item">
					<label>Browse Audio: </label>
					<div class="content">
			    		<input TYPE="file" name="inpAudioFile" id="inpAudioFile" accept="audio/*" size="30" onchange="uploadTwittermedia('audio')"/>
			    		<input TYPE="hidden" name="inpAudioScript" id="inpAudioScript" >
			    		<div id="audioPreview"></div>
			     	</div>
		     	</div>
		     	<div class="item">
				    <label>Status:</label>
				    <div class="content">
					    <div style="padding-left:20px;font-size:10px">Max 100 characters</div>
			    		<textarea name="inpAudioStatus" id="inpAudioStatus" maxlength="100"></textarea>
			     	</div>
				</div>
		     	<div class="spacer clear"></div>
			</div>
   		</div>        
	</form>
	</cfoutput>
</cfsavecontent>

<cfsavecontent variable="editCommJs">
	<cfoutput>
		<script type="text/javascript">
			/* Config */
			var rootUrl = "#rootUrl#";
			var publicPath = "#PublicPath#";
			var sessionPath = "#SessionPath#";
			var INPBATCHID = #INPBATCHID#;
			var twitterObject = #data#;
		</script>
		<script TYPE="text/javascript" src="#rootUrl#/#PublicPath#/js/ajaxfileupload.js"></script>
		<script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/commTypes/js/editTwitterCommType.js"></script>
	</cfoutput> 
</cfsavecontent>

<cfsavecontent variable="editCommCSS">
	<cfoutput>
		<style>
			@import url('#rootUrl#/#PublicPath#/css/mcid/Comm/editComm.css');
		</style>
	</cfoutput> 
</cfsavecontent>

<cfoutput>
	#editCommJs#
	#htmlContent#
	#editCommCSS#
</cfoutput>