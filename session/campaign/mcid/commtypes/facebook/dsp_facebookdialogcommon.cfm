<script type="text/javascript">
	/***for message object only **/
	// for create message object dialog
	function openMessageObjectDialog(batchId, data, access_token, fanpageId, fromPage){
		var $editFacebookMessageDialog = $('<div id="StageMessageFacebookDialog"></div>');
		if(typeof(fromPage) == 'undefined'){
			fromPage = 'firstPage';
		}
		$editFacebookMessageDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectMessage', 
			{
				INPBATCHID   : batchId, 
				access_token : access_token, 
				data         : JSON.stringify(data),
   			 	fanpageId    : fanpageId,
   			 	fromPage       : fromPage
			})
			.dialog({
				modal : true,
				title: 'Customize Facebook Fan Page Post',
				width: 550,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	// for message object preview
	function openMessagePreivewDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessagePreviewDialog = $('<div id="StageMessageFacebookPreviewDialog"></div>');
		$editFacebookMessagePreviewDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectMessagePreview', 
		{
			INPBATCHID:batchId,
			data: JSON.stringify(data), 
			access_token:access_token,
			fanpageId : fanpageId
		})
		.dialog({
			modal : true,
			title: 'Preview Facebook Fan Page Post',
			width: 550,
			minHeight: 300,
			height: 'auto',
			position: 'top',
			close:function(){
				socialDialogIsOpen = false;
				$(this).remove(); 
			}
		});	
	}
	// for message object final step
	function openMessageFinalDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessagePreviewDialog = $('<div id="MessageFacebookFinalDialog"></div>');
		$editFacebookMessagePreviewDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectMessageFinal', 
			{
				INPBATCHID:batchId,
				data: JSON.stringify(data), 
				access_token:access_token,
   			 	fanpageId : fanpageId
			})
			.dialog({
				modal : true,
				title: 'Facebook Campaign Module',
				width: 550,
				minHeight: 300,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	
	
	/***for message with link **/
	// for create message with link dialog
	function openMessageWithLinkDialog(batchId, data, access_token, fanpageId, fromPage){
		var $editFacebookMessageWebsiteLinkDialog = $('<div id="StageMessageWebsiteLinkeFacebookDialog"></div>');
		if(typeof(fromPage) == 'undefined'){
			fromPage = 'firstPage';
		}
		$editFacebookMessageWebsiteLinkDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookWebsiteLink', 
		{
			INPBATCHID:batchId, 
			access_token:access_token, 
			data: JSON.stringify(data),
 			fanpageId : fanpageId,
 			fromPage: fromPage
		})
			.dialog({
				modal : true,
				title: 'Customize Facebook Fan Page Post',
				width: 600,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});
	}
	// for message object preview
	function openMessageWithLinkPreivewDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessagePreviewDialog = $('<div id="facebookWebsiteLinkPreviewDialog"></div>');
		$editFacebookMessagePreviewDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookWebsiteLinkPreview', 
			{
				INPBATCHID:batchId,
				data: JSON.stringify(data), 
				access_token:access_token,
   			 	fanpageId : fanpageId
			})
			.dialog({
				modal : true,
				title: 'Preview Facebook Fan Page Post',
				width: 550,
				minHeight: 300,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	// for message object final step
	function openMessageWithLinkFinalDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessageFinalDialog = $('<div id="facebookWebsiteLinkFinalDialog"></div>');
		$editFacebookMessageFinalDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookWebsiteLinkFinal', 
			{
				INPBATCHID:batchId,
				data: JSON.stringify(data),
				access_token:access_token,
   			 	fanpageId : fanpageId
			})
			.dialog({
				modal : true,
				title: 'Facebook Campaign Module',
				width: 550,
				minHeight: 300,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	
	
	/***for link only **/
	// for create link only dialog
	function openLinkDialog(batchId, data, access_token, fanpageId, fromPage){
		var $editFacebookMessageLinkDialog = $('<div id="StageWebsiteLinkFacebookDialog"></div>');
		
		if(typeof(fromPage) == 'undefined'){
			fromPage = 'firstPage';
		}
		
		$editFacebookMessageLinkDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectLink', 
		{
			INPBATCHID:batchId, 
			access_token:access_token,
			data: JSON.stringify(data),
			fanpageId : fanpageId,
			fromPage : fromPage
		})
			.dialog({
				modal : true,
				title: 'Customize Facebook Fan Page Post',
				width: 600,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	// for message object preview
	function openLinkPreivewDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessageFinalDialog = $('<div id="facebookLinkPreviewDialog"></div>');
		$editFacebookMessageFinalDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectLinkPreview', 
			{
				INPBATCHID:batchId,
				data: JSON.stringify(data), 
				access_token:access_token,
   			 	fanpageId : fanpageId
			})
			.dialog({
				modal : true,
				title: 'Preview Facebook Fan Page Post',
				width: 550,
				minHeight: 300,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});
	}
	// for message object final step
	function openLinkFinalDialog(batchId, data, access_token, fanpageId){
		var $editFacebookMessageFinalDialog = $('<div id="facebookLinkFinalDialog"></div>');
		$editFacebookMessageFinalDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectLinkFinal', 
			{
				INPBATCHID:batchId,
				data: JSON.stringify(data), 
				access_token:access_token,
   			 	fanpageId : fanpageId
			})
			.dialog({
				modal : true,
				title: 'Facebook Campaign Module',
				width: 550,
				minHeight: 300,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});	
	}
	
	
	/*******For first screen********/
	function openFirstSocialDialog(batchId, data, access_token){
		var $editFacebookObjectDialog = $('<div id="StageCommFacebookEditDiv"></div>');
		data = {
			CAPTION:"",
			FBID: data.FBID,
			MESSAGE:"",
			NAME:"",
			PICTURE:"",
			TYPE:"message",
			URL:""
		};
		$editFacebookObjectDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/commTypes/facebook/dsp_facebookObjectMenu', 
			{
				INPBATCHID   : batchId,
				data         : JSON.stringify( data ), 
				access_token : access_token
			}
		)
			.dialog({
				modal : true,
				title: 'Facebook Campaign Module',
				width: 550,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close:function(){
					socialDialogIsOpen = false;
					$(this).remove(); 
				}
			});
	
	}
	
	var saveFacebookDialogIsOpen = false;
	/*******For Saving facebook object********/
	function saveFacebookObject(batchId, data, access_token, fanpageId, callback){
		saveFacebookDialogIsOpen = true;
		$.ajax({
           type: "POST",
           url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=EditFacebookObject&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		   data:{
   			 INPBATCHID : batchId,  
   			 INPFACEBOOK : JSON.stringify( data ),
   			 ACCESS_TOKEN : access_token,
   			 fanpageId : fanpageId
			},
           dataType: "json", 
           success: function(d) {
	           	if(d.ROWCOUNT >0){
	           		if(d.DATA.RXRESULTCODE >0){
	           			callback();	
	           		}
	           	}
	           	else
				{<!--- No result returned --->
					<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("No Response from the remote server. Check your connection and try again.", "Error.");
				}
	     	}
		});
	}
	
	function getDomainFromUrl(url){
		var urlParts = url.replace('http://','').replace('https://','').split(/[/?#]/);
		var domain = urlParts[0];
		return domain;
	}
</script>