<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />
<cfparam name="fromPage" default="firstPage" />

<cfsaveContent variable="htmlContent">
	<div id="finalFacebookLink">
		<div class="caption">Your Facebook fan page post will show up as:</div>
		<div id="preview">
			<div id="previewHeader">
				<div id="previewImage"></div>
				<div id="previewFanpageInfo">
					<div id="name"></div>
				</div>
			</div>
			<div id="previewLink">
				<div id="linkImage"></div>
				<div id="linkContent">
					<div id="linkName"></div>
					<div id="linkCaption">
					</div>
					<div id="linkDescription"></div>
				</div>	
			</div>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="  Edit  " id="btnEdit" />
			<input type="button" class="ui-corner-all" value="  Start Over  " id="btnStartOver" />
			<input type="button" class="ui-corner-all" value="  Exit  " id="btnExit" />
		</div>
	</div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
		var name = data.NAME;
		var url = data.URL;
		if(url.indexOf('http://') == -1){
			url = 'http://' + url;
		}
		var picture = data.PICTURE;
		var caption = data.CAPTION;
		// bind data
   		$('#finalFacebookLink #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#finalFacebookLink #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#finalFacebookLink #linkImage').append('<img src="'+ picture +'" width="80" />');
   		$('#finalFacebookLink #linkCaption').html(getDomainFromUrl(url));
   		$('#finalFacebookLink #linkName').html('<a href="'+ url +'" >' + name +'</a>');
   		$('#finalFacebookLink #linkDescription').html(caption);
		
		/**** Button event****/
		// edit button
		$('#finalFacebookLink #btnEdit').click(function(){
			var fromPage = 'final';
			openLinkDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', fromPage);
			closeCurrentDialog();
		});
		// start over button
		$('#finalFacebookLink #btnStartOver').click(function(){
			openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');
			closeCurrentDialog();
		});
		// cancel button
		$('#finalFacebookLink #btnExit').click(function(){
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#facebookLinkFinalDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>