<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />

<cfsaveContent variable="htmlContent">
	<div id="privewFacebookLink">
		<div class="caption">Your Facebook fan page post will show up as:</div>
		<div id="preview">
			<div id="previewHeader">
				<div id="previewImage"></div>
				<div id="previewFanpageInfo">
					<div id="name"></div>
				</div>
			</div>
			<div id="previewLink">
				<div id="linkImage"></div>
				<div id="linkContent">
					<div id="linkName"></div>
					<div id="linkCaption">
					</div>
					<div id="linkDescription"></div>
				</div>	
			</div>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="  Accept  " id="btnAccept" />
			<input type="button" class="ui-corner-all" value="  Edit  " id="btnEdit" />
			<input type="button" class="ui-corner-all" value="  Cancel  " id="btnCancel" />
		</div>
	</div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
		var name = data.NAME;
		var url = data.URL;
		if(url.indexOf('http://') == -1){
			url = 'http://' + url;
		}
		var picture = data.PICTURE;
		var caption = data.CAPTION;
		// bind data
   		$('#privewFacebookLink #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#privewFacebookLink #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#privewFacebookLink #linkImage').append('<img src="'+ picture +'" width="80"  />');
   		$('#privewFacebookLink #linkCaption').html(getDomainFromUrl(url));
   		$('#privewFacebookLink #linkName').html('<a href="'+ url +'" >' + name +'</a>');
   		$('#privewFacebookLink #linkDescription').html(caption);
		
		/****** button event*****/
		// accept button
		$('#btnAccept').click(function(){
			var callback = function(){
				openLinkFinalDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');
				closeCurrentDialog();
				saveFacebookDialogIsOpen = false;
			};
			if(!saveFacebookDialogIsOpen){
				saveFacebookObject('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', callback);
			}
			return false;
		});
		$('#btnEdit').click(function(){
			var data = <cfoutput>#data#</cfoutput>;
			var fromPage = 'preview';
			openLinkDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', fromPage);
			closeCurrentDialog();
		});
		// edit button
		// cancel button
		$('#privewFacebookLink #btnCancel').click(function(){
			openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#facebookLinkPreviewDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">

</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>