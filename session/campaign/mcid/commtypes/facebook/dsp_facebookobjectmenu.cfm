<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />

<cfsaveContent variable="htmlContent">
	<div class="messageTitle">Which of the following would you like to add?</div>
	<div class="messageLink"><a href='#'id="messageOnlyDialog">Message Only</a></div>
	<div class="messageLink"><a href='#' id="messageWebsiteLinkDialog">Message with Website Link</a></div>
	<div class="messageLink"><a href='#' id="messageWebsiteLinkOnlyDialog">Website Link Only</a></div>
	<div class="buttonBar">
		<input type="button" value="   Exit   " id="btnExit" class="ui-corner-all">
	</div> 
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfinclude template="dsp_FacebookDialogCommon.cfm" >
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
		$('#messageOnlyDialog').click(function(){
			openMessageObjectDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '');
			closeCurrentDialog();
			return false;
		});
		
		$('#messageWebsiteLinkDialog').click(function(){
			openMessageWithLinkDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '');
			closeCurrentDialog();
			return false;
		});
		
		$('#messageWebsiteLinkOnlyDialog').click(function(){
			openLinkDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '');
			closeCurrentDialog();
			return false;
		});
		
		$('#btnExit').click(function(){
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#StageCommFacebookEditDiv').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<style type="text/css">
		.messageTitle{
			margin:50px 0 20px 60px;
			font-weight:bold;
		}
		.messageLink{
			margin:0 0 10px 60px;
		}
		.messageLink a{
			text-decoration:underline;
		}
		
		.messageLink a:hover{
			text-decoration:none;
		}
		.buttonBar{
			padding-left:60px;
			margin: 30px 0 10px 0;
		}
	</style>
</cfsaveContent>

<cfoutput>
	#jsContent#
	#cssContent#
	#htmlContent#
</cfoutput>