<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />
<cfparam name="fromPage" default="firstPage" />

<cfinclude template="dsp_getListFanPage.cfm" >
<cfsaveContent variable="htmlContent" >
	<form id="frmFacebookMessage" action="">
		<cfif ArrayLen(fanpageArr) GT 1>
			<div class="caption">Please choose the fan page:</div>
			<div class="control">
				<select id="inpPagePage">
				<cfloop array="#fanpageArr#" index="fanpageInfo">
					<cfoutput>
						<cfif fanpageId EQ fanpageInfo.ID>
							<option value="#fanpageInfo.ID#" selected="true">#fanpageInfo.NAME#</option>
						<cfelse>
							<option value="#fanpageInfo.ID#">#fanpageInfo.NAME#</option>	
						</cfif>
					</cfoutput>
				</cfloop>
				</select>
			</div>
		</cfif>
		<div class="caption">Please enter the text for your post:</div>
		<div class="control">
			<span>
			<textarea cols="60" id="txtMessage" maxlength="420" tabindex="1" class="validate[required]"></textarea>
			</span>
		</div>
		<div class="status"><span id='charRemain'>420</span> characters remaining</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="Save" id="btnSave" />
			<input type="button" class="ui-corner-all" value="Cancel" id="btnCancel" />
		</div>
	</form>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfinclude template="dsp_FacebookDialogCommon.cfm" >
	<script type="text/javascript">
		// validate
		$('#frmFacebookMessage').validationEngine({
			promptPosition : "topLeft",
			scroll: false,
			validationEventTrigger:'',
			onValidationComplete : function(form, r) { 
				if (r) {
					<cfoutput>
						<cfif ArrayLen(fanpageArr) EQ 1>
							<cfset fanpageSelect = fanpageArr[1]>
							var fanpageId = '#fanpageSelect.ID#';
						<cfelse>
							var fanpageId = $('##inpPagePage').val();							
						</cfif>
					</cfoutput>
					var data = <cfoutput>#data#</cfoutput>;
					var submitData = {
						TYPE:'message', 
						MESSAGE:$('#txtMessage').val(),
						FBID: data.FBID
					};
					openMessagePreivewDialog('<cfoutput>#INPBATCHID#</cfoutput>', submitData , '<cfoutput>#access_token#</cfoutput>', fanpageId);
					closeCurrentDialog();
					return false;
				}
			}
		});
		
		$('#txtMessage').focus();
		// for edit task
		<cfif data NEQ "">
			var data = <cfoutput>#data#</cfoutput>;
			$('#txtMessage').val($.trim(data.MESSAGE));
			var characterRemaining = 420 - parseInt($('#txtMessage').val().length);
			$('#charRemain').html(characterRemaining);
		</cfif>		
		
		
		$('#txtMessage').keyup(function(){
			var characterRemaining = 420 - parseInt($('#txtMessage').val().length);
			$('#charRemain').html(characterRemaining);
		});
		
		/******Button Event********/
		$('#btnSave').click(function(){
			$('#frmFacebookMessage').submit();
			return false;		
		});
		
		$('#btnCancel').click(function(){
			var data = <cfoutput>#data#</cfoutput>;
			<cfif fromPage EQ 'final'>
				openMessageFinalDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');
			<cfelseif fromPage EQ 'preview'>
				openMessagePreivewDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');	
			<cfelse>	
				openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			</cfif>
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#StageMessageFacebookDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<style type="text/css">
		.buttonBar{
			padding-left:60px;
			margin: 30px 0 10px 0;
		}
		
		.control{
			padding-left:60px;
			margin:5px;
		}
		.control textarea{
			height:70px;
		}
		
		.caption{
			padding-left:60px;
			margin:25px 0 10px 5px;
		}
		.status{
			margin-right: 70px;
			float:right;
			font-size:12px;
			color:#c0c0c0;
		}
	</style>
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>