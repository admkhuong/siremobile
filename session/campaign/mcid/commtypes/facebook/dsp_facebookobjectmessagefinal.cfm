<cfparam name="INPBATCHID" default="" />
<cfparam name="data" type="any" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />

<cfsaveContent variable="htmlContent">
	<div id="finalFacebookMessage">
		<div class="caption">Your Facebook fan page post will show up as:</div>
		<div id="preview">
			<div id="previewHeader">
				<div id="previewImage"></div>
				<div id="previewFanpageInfo">
					<div id="name"></div>
				</div>
			</div>
			<div id="previewContent">
				
			</div>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="  Edit  " id="btnEdit" />
			<input type="button" class="ui-corner-all" value="  Start Over  " id="btnStartOver" />
			<input type="button" class="ui-corner-all" value="  Exit  " id="btnExit" />
		</div>
	</div>
</cfsaveContent>

<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
   		$('#finalFacebookMessage #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#finalFacebookMessage #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#finalFacebookMessage #previewContent').html(data.MESSAGE);
		/****Button event***/
		$('#finalFacebookMessage #btnEdit').click(function(){
			var fromPage = 'final';
			openMessageObjectDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>',fromPage);
			closeCurrentDialog();
		});
		
		$('#finalFacebookMessage #btnStartOver').click(function(){
			openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			closeCurrentDialog();
		});
		
		$('#finalFacebookMessage #btnExit').click(function(){
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#MessageFacebookFinalDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">

</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>