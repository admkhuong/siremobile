<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />

<cfsaveContent variable="htmlContent">
	<div id="privewFacebookMessage">
		<div class="caption">Your Facebook fan page post will show up as:</div>
		<div id="preview">
			<div id="previewHeader">
				<div id="previewImage"></div>
				<div id="previewFanpageInfo">
					<div id="name"></div>
				</div>
			</div>
			<div id="previewContent">
				
			</div>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="  Accept  " id="btnAccept" />
			<input type="button" class="ui-corner-all" value="  Edit  " id="btnEdit" />
			<input type="button" class="ui-corner-all" value="  Cancel  " id="btnCancel" />
		</div>
	</div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
   		$('#privewFacebookMessage #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#privewFacebookMessage #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#privewFacebookMessage #previewContent').html(data.MESSAGE);
		/****Button event******/
		// accept button
		$('#btnAccept').click(function(){
			var callback = function(){
				openMessageFinalDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');
				closeCurrentDialog();
				saveFacebookDialogIsOpen = false;
			};
			if(!saveFacebookDialogIsOpen){
				saveFacebookObject('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', callback);
			}
			return false;		
		});
		// edit button
		$('#btnEdit').click(function(){
			var fromPage = 'preview';
			openMessageObjectDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', fromPage);
			closeCurrentDialog();
			return false;		
		});
		// cancel button
		$('#privewFacebookMessage #btnCancel').click(function(){
			openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#StageMessageFacebookPreviewDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">

</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>