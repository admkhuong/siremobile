<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />
<cfparam name="fromPage"  default="firstPage" />

<cfinclude template="dsp_getListFanPage.cfm" >
<cfsaveContent variable="htmlContent">
	<form id="frmFacebookWebsiteLink" action="">
	<div class="caption">Please enter the message and link information for your post:</div>
	<cfif ArrayLen(fanpageArr) GT 1>
		<div class="itemControl">
			<span class='label'>Fan page</span>
			<span class="control">
				<select id="inpPagePage">
				<cfloop array="#fanpageArr#" index="fanpageInfo">
					<cfoutput>
						<cfif fanpageId EQ fanpageInfo.ID>
							<option value="#fanpageInfo.ID#" selected="true">#fanpageInfo.NAME#</option>
						<cfelse>
							<option value="#fanpageInfo.ID#">#fanpageInfo.NAME#</option>	
						</cfif>
					</cfoutput>
				</cfloop>
				</select>
			</span>
		</div>
	</cfif>
	<div class="itemControl">
		<span class='label required'>Message*</span>
		<span class="control">
			<input type="text" id="inpMessage" tabindex="1" class="validate[required]" />
		</span>
	</div>
	<div class="clear"></div>
	<div class="itemControl">
		<span class='label required'>URL*</span>
		<span class="control">
			<input type="text" id="inpUrl" tabindex="2" class="validate[required,funcCall[checkUrl]]" />
		</span>
	</div>
	<div class="clear"></div>
	<div class="itemControl">
		<span class='label'>Picture</span>
		<span class="control">
			<input type="text" id="inpPicture" tabindex="3"/>
			<a class='toolTip' style="display:inline;" href='javascript:void(0);' title="Thumbnail picture. Recommended size 120x120px. JPG, GIF">
				<img src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/help.png'>
			</a>
		</span>
	</div>
	<div class="itemControl">
		<span class='label'>Name</span>
		<span class="control">
			<input type="text" id="inpName" tabindex="4"/>
			<a class='toolTip' style="display:inline;" href='javascript:void(0);' title="Title of your link.">
				<img style='border:none' src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/help.png'>
			</a>
		</span>
	</div>
	<div class="itemControl">
		<span class='label'>Caption</span>
		<span class="control">
			<textarea id="inpCaption" maxlength="200" tabindex="5"/>
			<a class='toolTip' style="display:inline;" href='javascript:void(0);' title="A short description of your link. Maximum length 200 characters.">
				<img src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/help.png'>
			</a>
		</span>
	</div>
	<div class="requireLabel">*Required Field</div>
	<div class="noteLabel"><span style="text-decoration: underline;">NOTE</span>: Picture, Name, Caption will be generated by facebook automatically if not inputted</div>
	<div class="buttonBar">
		<input type="button" class="ui-corner-all" value="Save" id="btnSave" />
		<input type="button" class="ui-corner-all" value="Cancel" id="btnCancel" />
	</div>
	</form>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfinclude template="dsp_FacebookDialogCommon.cfm" >
	<script type="text/javascript">
		$('.toolTip').tipsy({gravity: 'e',html: true});
		// validate
		$('#frmFacebookWebsiteLink').validationEngine({
			promptPosition : "topLeft",
			promptLine: false,
			scroll: false,
			validationEventTrigger:'',
			onValidationComplete : function(form, r) { 
				if (r) {
					<cfoutput>
						<cfif ArrayLen(fanpageArr) EQ 1>
							<cfset fanpageSelect = fanpageArr[1]>
							var fanpageId = '#fanpageSelect.ID#';
						<cfelse>
							var fanpageId = $('##inpPagePage').val();							
						</cfif>
					</cfoutput>
					var data = <cfoutput>#data#</cfoutput>;
					var submitData = {
						TYPE      :'messageLink', 
						MESSAGE   :$('#inpMessage').val(),
						URL       :$('#inpUrl').val(), 
						PICTURE   :$('#inpPicture').val(),
						NAME      :$('#inpName').val(),
						CAPTION   :$('#inpCaption').val(),
						FBID      : data.FBID
				    };
					openMessageWithLinkPreivewDialog('<cfoutput>#INPBATCHID#</cfoutput>', submitData, '<cfoutput>#access_token#</cfoutput>', fanpageId);
					// close current dialog
					closeCurrentDialog();
					return false;
				}
			}
		});
		$('#inpMessage').focus();
			
		// for edit task
		<cfif data NEQ "">
			var data = <cfoutput>#data#</cfoutput>;
			$('#inpMessage').val(data.MESSAGE);
			$('#inpUrl').val(data.URL); 
			$('#inpPicture').val(data.PICTURE);
			$('#inpName').val(data.NAME);
			$('#inpCaption').val(data.CAPTION);
		</cfif>
		/******Button event**********/
		// save button
		$('#btnSave').click(function(){
			$('#frmFacebookWebsiteLink').submit();
			return false;		
		});
		// cancel button
		$('#btnCancel').click(function(){
			var data = <cfoutput>#data#</cfoutput>;
			<cfif fromPage EQ 'final'>
				openMessageWithLinkFinalDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>','<cfoutput>#fanpageId#</cfoutput>');
			<cfelseif fromPage EQ 'preview'>
				openMessageWithLinkPreivewDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>')
			<cfelse>
				openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			</cfif>
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#StageMessageWebsiteLinkeFacebookDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>