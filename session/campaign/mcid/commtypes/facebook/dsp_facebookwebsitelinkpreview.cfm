<cfparam name="INPBATCHID" default="" />
<cfparam name="data" default="" />
<cfparam name="access_token" default="" />
<cfparam name="fanpageId" default="" />

<cfsaveContent variable="htmlContent">
	<div id="privewFacebookWebLink">
		<div class="caption">Your Facebook fan page post will show up as:</div>
		<div id="preview">
			<div id="previewHeader">
				<div id="previewImage"></div>
				<div id="previewFanpageInfo">
					<div id="name"></div>
				</div>
			</div>
			<div id="previewContent">
			</div>
			<div id="previewLink">
				<div id="linkImage"></div>
				<div id="linkContent">
					<div id="linkName"></div>
					<div id="linkCaption">
					</div>
					<div id="linkDescription"></div>
				</div>	
			</div>
			<div id="previewFooter">
				<div id="previewFooterBar">
					Like - Comment - Share
				</div>
				<div id="previewFooterControl">
					<div id="previewTextArea">Write a comment...</div>
				</div>
			</div>
		</div>
		<div class="buttonBar">
			<input type="button" class="ui-corner-all" value="  Accept  " id="btnAccept" />
			<input type="button" class="ui-corner-all" value="  Edit  " id="btnEdit" />
			<input type="button" class="ui-corner-all" value="  Cancel  " id="btnCancel" />
		</div>
	</div>
</cfsaveContent>


<cfsaveContent variable="jsContent">
	<cfhttp url="https://graph.facebook.com/#fanpageId#" method="get" result="resultFanpageInfo">
	<cfset fanpageInfo = deserializeJSON(resultFanpageInfo.fileContent)>
	<script type="text/javascript">
		var data = <cfoutput>#data#</cfoutput>;
		var message = data.MESSAGE;
		var name = data.NAME;
		var url = data.URL;
		if(url.indexOf('http://') == -1){
			url = 'http://' + url;
		}
		var picture = data.PICTURE;
		var caption = data.CAPTION;
			
   		// bind data
   		$('#privewFacebookWebLink #previewImage').append('<img src="<cfoutput>#fanpageInfo.picture#</cfoutput>" height="50" />');
   		$('#privewFacebookWebLink #name').html('<cfoutput>#fanpageInfo.name#</cfoutput>');
   		$('#privewFacebookWebLink #previewContent').html(message);
   		$('#privewFacebookWebLink #linkImage').append('<img src="'+ picture +'" width="80" />');
   		$('#privewFacebookWebLink #linkCaption').html(getDomainFromUrl(url));
   		$('#privewFacebookWebLink #linkName').html('<a href="'+ url +'" >' + name +'</a>');
   		$('#privewFacebookWebLink #linkDescription').html(caption);
   		
		/*****Button event*******/
		// button accept
		$('#privewFacebookWebLink #btnAccept').click(function(){
			var callback = function(){
				openMessageWithLinkFinalDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>');
				closeCurrentDialog();
				saveFacebookDialogIsOpen = false;
			};
			if(!saveFacebookDialogIsOpen){
				saveFacebookObject('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', callback);
			}
			return false;	
		});
		// button edit
		$('#privewFacebookWebLink #btnEdit').click(function(){
			var data = <cfoutput>#data#</cfoutput>;
			var fromPage = 'preview';
			openMessageWithLinkDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>', '<cfoutput>#fanpageId#</cfoutput>', fromPage);
			closeCurrentDialog();
		});
		// button cancel
		$('#privewFacebookWebLink #btnCancel').click(function(){
			var data = <cfoutput>#data#</cfoutput>;
			openFirstSocialDialog('<cfoutput>#INPBATCHID#</cfoutput>', data, '<cfoutput>#access_token#</cfoutput>');
			closeCurrentDialog();
		});
		
		function closeCurrentDialog(){
			$('#facebookWebsiteLinkPreviewDialog').remove();
		}
	</script>
</cfsaveContent>

<cfsaveContent variable="cssContent">
	<style type="text/css">
	</style>
</cfsaveContent>

<cfoutput>
	#cssContent#
	#jsContent#
	#htmlContent#
</cfoutput>