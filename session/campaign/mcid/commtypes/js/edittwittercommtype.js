	$(document).ready(function(){
		
		var type = twitterObject.TYPE;
		// display publish area
		displayPublishArea(type);
		switch(type){
		case 'image':
			$("input[name=inpPublishType]").filter("[value=image]").attr('checked', true);
			var status = twitterObject.IMGSTATUS;
			var imgSrc = rootUrl + '/' + sessionPath +'/account/act_GetMySocialmediaImage?serverfileName='+ twitterObject.IMGSRC;
			
			$('#inpImgStatus').val(status);
			$('#inpImgServerFile').val(twitterObject.IMGSRC);
			$('#imgPreview').empty();
			$('#imgPreview').append('<img src="' +imgSrc + '" width="60"/>');
			
			break;
		case 'text':
			$("input[name=inpPublishType]").filter("[value=text]").attr('checked', true);
			var status = twitterObject.TXTSTATUS;
			$('#inpTextStatus').val(status);
			break;
		case 'babble':
			$("input[name=inpPublishType]").filter("[value=babble]").attr('checked', true);
			var status = twitterObject.BBSTATUS;
			var babbleSrc = rootUrl + '/' + sessionPath +'/account/act_GetMyBabbleMP3?scrId='+ twitterObject.BBSCRIPT;
			
			$('#inpBabbleStatus').val(status);
			$('#inpBabbleScript').val(twitterObject.BBSCRIPT);
			// preview babble
			// embed object
			var embedPreviewBabble = '<object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file='+ babbleSrc +'" /> </object>';
			$('#babblePreview').append(embedPreviewBabble);
			
			break;
		case 'audio':
			$("input[name=inpPublishType]").filter("[value=audio]").attr('checked', true);
			var status = twitterObject.AUSTATUS;
			var audioSrc = rootUrl + '/' + sessionPath +'/account/act_GetMySocialmediaAudio?serverfileName='+ twitterObject.AUSCRIPT;
			
		    $('#inpAudioStatus').val(status);
			$('#inpAudioScript').val(twitterObject.AUSCRIPT);
			// preview audio
			// embed object
			var embedPreviewAudio = '<object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file=' +audioSrc +'" /> </object>';
			$('#audioPreview').append(embedPreviewAudio);
			break;
		}
		
	});
	
	$('#frmTwitterObject').validationEngine({
		promptPosition : "topLeft",
		scroll: false,
		onValidationComplete : function(form, r) { 
			if (r) {
				var publishType = $('input[name=inpPublishType]:checked').val();
				
				var inpImgStatus = $('#inpImgStatus').val();
				var inpImgServerFile = $('#inpImgServerFile').val();
				
				var inpTextStatus = $('#inpTextStatus').val();
				
				var inpBabbleStatus = $('#inpBabbleStatus').val();
				var inpBabbleScript = $('#inpBabbleScript').val();
				
				var inpAudioStatus = $('#inpAudioStatus').val();
				var inpAudioScript = $('#inpAudioScript').val();
				
				// validate facebook object
				
				$.ajax({
		            type: "POST",
		            url: rootUrl + "/"+ sessionPath +"/cfc/mcidtoolsii.cfc?method=SaveTwitterObject&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:{
		    			 INPBATCHID : INPBATCHID,     			
		    			 inpImgStatus : inpImgStatus,
		    			 inpImgServerFile:inpImgServerFile,
		    			 inpTextStatus : inpTextStatus,
		    			 inpBabbleStatus:inpBabbleStatus,
		    			 inpBabbleScript:inpBabbleScript,
		    			 inpAudioStatus:inpAudioStatus,
		    			 inpAudioScript:inpAudioScript,
		    			 inpPublishTyle:publishType
						},
		            dataType: "json", 
		            success: function(d2, textStatus, xhr) {
		            	var d = eval('(' + xhr.responseText + ')');
			     	}
		   		});
				
				$('#StageCommTwitterEditDiv').remove();
				return false;
			}
		}
	});
	
	/*Save content of facebook object*/
	$('#btnTwitterSave').click(function(){
		// Remove all class validation
		$('#frmTwitterObject textarea').removeClass();
		// Get publish type
		var publishType = $('input[name=inpPublishType]:checked').val();
		// Add validation belong with publish type
		if(publishType == 'image'){
			if($('#inpImgServerFile').val() == ''){
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Please choose an image file to publish.", "Failure.");
				return false;
			}
			$('#imageArea #inpImgStatus').addClass('validate[funcCall[required]]');
		}else if(publishType == 'text'){
			$('#textArea #inpTextStatus').addClass('validate[funcCall[required]]');
		}else if(publishType == 'babble'){
			if($('#inpBabbleScript').val() == ''){
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Please choose a babble to publish.", "Failure.");
				return false;
			}
			$('#babbleArea #inpBabbleStatus').addClass('validate[funcCall[required]]');
		}else if(publishType == 'audio'){
			if($('#inpAudioScript').val() == ''){
				$.alerts.okButton = '&nbsp;OK&nbsp;';
				jAlert("Please choose an audio file to publish.", "Failure.");
				return false;
			}
			$('#audioArea #inpAudioStatus').addClass('validate[funcCall[required]]');
		}
		
		$('#frmTwitterObject').submit();
		return false;
	});
	
	/*Change publish type*/
	$('input[name=inpPublishType]').change(function(){
		var publishTyle = $(this).val();
		displayPublishArea(publishTyle);
	});
	
	/**
	 * Display correct publish area
	 * 
	 * @param inpPublishType
	 */
	function displayPublishArea(inpPublishType){
		if(inpPublishType == 'image'){
			$('#imageArea').css('display','');
			$('#textArea').css('display','none');
			$('#babbleArea').css('display','none');
			$('#audioArea').css('display','none');
		} else if(inpPublishType == 'text'){
			$('#imageArea').css('display','none');
			$('#textArea').css('display','');
			$('#babbleArea').css('display','none');
			$('#audioArea').css('display','none');
		} else if(inpPublishType == 'babble'){
			$('#imageArea').css('display','none');
			$('#textArea').css('display','none');
			$('#babbleArea').css('display','');
			$('#audioArea').css('display','none');
		} else {
			$('#imageArea').css('display','none');
			$('#textArea').css('display','none');
			$('#babbleArea').css('display','none');
			$('#audioArea').css('display','');
		}
	}
	
    function uploadTwittermedia(type){
    	var inFileId = 'inpImgFile';
    	if(type=='image'){
    		inFileId = 'inpImgFile';
    	}else{
    		inFileId = 'inpAudioFile';
    	}
		$.ajaxFileUpload
		(
			{
				url: rootUrl + '/'+ sessionPath +'/campaign/mcid/commTypes/act_AsyncWriteSocialmediaFile?inpFileId='+inFileId ,
				secureuri:false,
				fileElementId:inFileId,
				dataType: 'json',				
				complete:function()
				{					
					
				},				
				success: function (data, status)
				{	
					if(type == 'image'){
						$('#inpImgServerFile').val(data.UniqueFileName);
						$('#imgPreview').empty();
						$('#imgPreview').append('<img src="' +rootUrl + '/' + sessionPath +'/account/act_GetMySocialmediaImage?serverfileName='+ data.UniqueFileName +'" width="60"/>');
					}else{
						$('#inpAudioScript').val(data.UniqueFileName);
						$('#audioPreview').empty();
						$('#audioPreview').append('<object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file=' +rootUrl + '/' + sessionPath +'/account/act_GetMySocialmediaAudio?serverfileName='+ data.UniqueFileName +'" /> </object>');
					}
				},
				error: function (data, status, e)
				{						
					alert('general Error ' + e);	
				}
			}
		);
	}
	
	$('#inpLoadBabble').click(function(){
		
		var CreatePopupDialog = 0;
		if(CreatePopupDialog != 0)
		{
			CreatePopupDialog.remove();
			CreatePopupDialog = 0;
			
		}
						
		var ParamStr = '?flash=17&socialNetwork=0';
		socialNetwork = 0;
		CreatePopupDialog = $('<div></div>');
		CreatePopupDialog
			.load(rootUrl + '/' + sessionPath +'/campaign/mcid/commTypes/dsp_babblesphereDialog' + ParamStr)
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
			    title: 'select babblesphere',
				close: function() {   $(this).dialog('destroy'); $(this).remove(); },
				width: 650,
				height: 'auto',
				position: 'top',
			    closeOnEscape: false, 
			    buttons:
				[
					{
						text: "Select",
						click: function(event)
						{

							var bsValue = $('#ScriptList').jqGrid('getGridParam','selrow');;
							if(bsValue == null){
								alert('Please select a babble to publish to your facebook');
							}else{
								// get babble script
								$('#inpBabbleScript').val($('#selectedScript' + bsValue).val());
								var scriptId = $('#selectedScript' + bsValue).val();
								// preview babble
								// embed object
								$('#babblePreview').empty();
								var embedPreviewBabble = '<object TYPE="application/x-shockwave-flash" data="singleplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="singleplayer.swf" /> <param name="flashvars" value="xmlConfig=config.xml&file='+ rootUrl + '/' + sessionPath +'/account/act_GetMyBabbleMP3?scrId='+ scriptId +'" /> </object>';
								$('#babblePreview').append(embedPreviewBabble);
								CreatePopupDialog.remove();	
							}
							return false; 
						}
					},
					{
						text: "Close",
						click: function(event)
						{
							 CreatePopupDialog.remove();	
							 return false; 
						}
					}
				]

			});
		
		CreatePopupDialog.dialog('open');	
		return false;
	});
