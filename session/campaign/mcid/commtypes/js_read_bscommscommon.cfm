var isTTS = false;
var rxvid = -1;
var dataKey = -1;

if(typeof(d.DATA.DATAKEY[0]) != "undefined"){
	if(d.DATA.DATAKEY[0] != -1){
	 	dataKey = d.DATA.DATAKEY[0];
	}
}

if(typeof(d.DATA.ISTTS[0]) != "undefined"){
	if(d.DATA.ISTTS[0] == true){
	 	isTTS = true;
	}
}
if(typeof(d.DATA.RXVID[0]) != "undefined"){
	rxvid = d.DATA.RXVID[0];
}
<!---Multi script region     --->								
if(typeof(d.DATA.CURRBS[0]) != "undefined")
{									
	if(d.DATA.CURRBS[0] == 1)
	{
		$("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]").filter("[value=1]").attr('checked', true);
	}else{
		$("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]").filter("[value=0]").attr('checked', true);
	}
	if(isTTS){
		$("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]").filter("[value=2]").attr('checked', true);
		var scriptDesc = $("#EditCOMMIDForm_" + INPCOMMID + " #INPDESC").val();
		var scriptId = $("#EditCOMMIDForm_" + INPCOMMID + " #inpDataId").val();
		if(scriptId == 0){
			scriptId = "&nbsp;";					
		}
		addParentTtsRow(INPCOMMID, scriptDesc, scriptId, rxvid, dataKey);
	}
}
else
{
 $("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]:checked").val('0')
}
if($("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]:checked").val() == 1){
	$("#EditCOMMIDForm_" + INPCOMMID + " #MultiScriptSelectArea").css("display","block");
	hideTopScriptLevel(INPCOMMID);
	
}else if($("#EditCOMMIDForm_" + INPCOMMID + " input[name=inpBS]:checked").val() == 0){
	$("#EditCOMMIDForm_" + INPCOMMID + " #MultiScriptSelectArea").css("display","none");
	showTopScriptLevel(INPCOMMID);
} else {
	showCurrentActiveArea(INPCOMMID,"tts");			
}

if(typeof(d.DATA.MULTISCRIPT[0]) != "undefined")
{	
	//for template
	for(index in d.DATA.MULTISCRIPT[0])
	{
		var currData = d.DATA.MULTISCRIPT[0][index];
		if( typeof(currData.DESC) != 'undefined'){
			var eleId =currData.ELEID;
			var desc = currData.DESC;
			var scriptId = currData.SCRIPTID;
			var rxvid = currData.RXVID;		
			var dk = currData.DK;		
			if(rxvid == 0){
				addScriptRow(INPCOMMID, eleId, desc, scriptId, <cfoutput>#SESSION.UserID#</cfoutput>, 'DESC');
			} else {
				addScriptTtsRow(INPCOMMID, desc, scriptId, rxvid, dk, 'DESC');			
			}
		} else {
			var locationKey = currData.SWITCHLOCATIONKEY;
			var caseContainer = addBlankSwitchScript(null,INPCOMMID,locationKey, 'DESC');
			for(caseIndex in currData.ARRCASE){
				var eleId = currData.ARRCASE[caseIndex].ELEID;
				var scriptId = currData.ARRCASE[caseIndex].SCRIPTID;
				var desc = currData.ARRCASE[caseIndex].DESC;
				var caseValue = currData.ARRCASE[caseIndex].SWITCHVALUE;
				addSwitchScriptRow(INPCOMMID, eleId, scriptId, desc,caseValue,caseContainer, 'DESC');
			}
		}
	}
	showHideScriptListTable(INPCOMMID);
	recalculatePaging(INPCOMMID);
}