<!--- used to update interface values based on JSON results --->
<!--- Assumes form fields --->

<!--- Update Summary info too? --->
			<!--- Check if variable is part of JSON result string     --->		
			if(typeof(d.DATA.INPCOMMID[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #INPCOMMID").val(d.DATA.INPCOMMID[0]);																							
			}
            else
            {
             	$("#COMMTYPEEditForm_" + INPCOMMID + " #INPCOMMID").val('0');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRRXT[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpCOMMTYPE").val(d.DATA.CURRRXT[0]);																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #inpCOMMTYPE").val('1');	
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRDESC[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #INPDESC").val(RXdecodeXML(d.DATA.CURRDESC[0]));																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #INPDESC").val('empty');		
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRUID[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpUID").val(d.DATA.CURRUID[0]);																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #inpUID").val('0');
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRLIBID[0]) != "undefined")
			{			
				var inpLibId = d.DATA.CURRLIBID[0]; 						
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpLibId").val(inpLibId);	
				$("#COMMTYPEEditForm_" + INPCOMMID + " #currLib").html(inpLibId);
				if(inpLibId != 0){
					$("#COMMTYPEEditForm_" + INPCOMMID + " .noneScriptLib").css("display","none");
					$("#COMMTYPEEditForm_" + INPCOMMID + " .fieldSetBox").css("display","block");
				} else {
					$("#COMMTYPEEditForm_" + INPCOMMID + " .noneScriptLib").css("display","block");
					$("#COMMTYPEEditForm_" + INPCOMMID + " .fieldSetBox").css("display","none");
				}
			}
            else
            {
             	$("#COMMTYPEEditForm_" + INPCOMMID + " #inpLibId").val('0');
				$("#COMMTYPEEditForm_" + INPCOMMID + " #currLib").html('0');
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRELEID[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpEleId").val(d.DATA.CURRELEID[0]);																							
			}
            else
            {
             	$("#COMMTYPEEditForm_" + INPCOMMID + " #inpEleId").val('0');
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRDATAID[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpDataId").val(d.DATA.CURRDATAID[0]);
				if (d.DATA.CURRLIBID[0] > 0) {
					var inpPlayId = <cfoutput>#SESSION.UserID#</cfoutput>+'_'+d.DATA.CURRLIBID[0]+'_'+d.DATA.CURRELEID[0]+'_'+d.DATA.CURRDATAID[0];
					$("#COMMTYPEEditForm_" + INPCOMMID + " #inpPlayId").val(inpPlayId);
					playerflash(inpPlayId);
				}
			}
            else
            {
             	$("#COMMTYPEEditForm_" + INPCOMMID + " #inpDataId").val('0');	
            }
	                                   
            <!--- Check if variable is part of JSON result string   CURRNumDTMFsToIgnore inpNumDTMFsToIgnore --->								
			if(typeof(d.DATA.CURRCK1[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpCK1").val(d.DATA.CURRCK1[0]);																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #inpCK1").val('0');		
            }
	                                   
	        <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRCK5[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpCK5").val(d.DATA.CURRCK5[0]);																							
			}
            else
            {
             	$("#COMMTYPEEditForm_" + INPCOMMID + " #inpCK5").val('-1');	
            }
	                       
	                          <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRX[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpX").val(d.DATA.CURRX[0]);																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #inpX").val('0');	
            }
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRY[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpY").val(d.DATA.CURRY[0]);																							
			}
           	else
           	{
           	 	$("#COMMTYPEEditForm_" + INPCOMMID + " #inpY").val('0');	
           	}
	                                   
	                                   <!--- Check if variable is part of JSON result string     --->								
			if(typeof(d.DATA.CURRLINK[0]) != "undefined")
			{									
				$("#COMMTYPEEditForm_" + INPCOMMID + " #inpLINK").val(d.DATA.CURRLINK[0]);																							
			}
            else
            {
             $("#COMMTYPEEditForm_" + INPCOMMID + " #inpLINK").val('-1');	
            }
			<cfinclude template="js_read_bsCOMMsCommon.cfm">