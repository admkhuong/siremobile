<style>
/**
 * CSS for drawing a tree for MCID data services
 */
.cx_mcid_tree {
	/*background: #f1f2f3;*/
}
.cx_mcid_tree .cx_title {
	font-size: 18px;
	font-weight: bold;
	text-align: center;
	text-transform: uppercase;
	width: 750px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10px;
	padding: 5px;
	background: #0a0a0a;
	border-bottom: 3px solid #cccec1;
	border-right: 2px solid #cccec1;
	color: #fff;
}
.cx_mcid_tree .cx_chart {
	float: left;
	margin: 5px;
	padding: 5px;
}
.cx_mcid_tree .cx_chartTitle {
	font-size: 15px;
	font-weight: bold;
	text-align: center;
	padding: 5px;
	background: #e0e0e0;
	border-top: 2px solid #999c8a;
	border-left: 2px solid #999c8a;
	border-bottom: 2px solid #999c8a;
	border-right: 2px solid #999c8a;
}
.cx_mcid_tree .cx_chartList {
	list-style-type: none;
	padding: 0;
	margin: 0;
}
.cx_mcid_tree .cx_chartList LI {
	background: #336699;
	float: left;
	text-align: center;
	background: url("<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/mcid_arrow.png") top no-repeat;
	margin-top: 0 !important;
	padding-top: 25px !important;
}
.cx_mcid_tree .cx_chartList A, .cx_mcid_tree .cx_chartList A:visited {
	display: block;
	height: 40px;
	padding: 3px;
	font-size: 11px;
	font-weight: bold;
	text-decoration: none;
	border: 2px solid #9ca5a0;
	background: #a5d2ff;
	color: #3a4a59;
}
.cx_mcid_tree .cx_chartList LI A:hover {
	text-decoration: none;
	border: 2px solid #dcdcdc;
	background: #336699;
	color: #fff;
}
.cx_mcid_tree .cx_chartListClear {
	/*clear: both;*/
}
.cx_mcid_tree .cx_chartClear {
	clear: both;
}
</style>
<script type="text/javascript">
/**
 * Draw a tree for MCID data services
 */
(function($){
$.cx_mcid_tree = function(option){
	var opt = $.extend({
		data:undefined,	/* JSON data of services */
		clazz:'',	/* CSS class for this tree container */
		horz:true,	/* To draw tree horizontally (TRUE) of vertically (FALSE) */
		itemWidth:0,	/* Width of LI item */
		itemHeight:0,	/* Height of LI item */
		itemPadding:2,	/* Padding of LI item */
		itemMargin:3,	/* Margin of LI item */
		itemLength:0,	/* Max number of characters for each item link */
		onClick:function(item){},	/* Handler to catch 'click' event on each item link */
		onDraw:function(div){}	/* Callback to draw output */
	},option);
	
	/* Container */
	var div = undefined;
	
	/* Load JSON data */
	function load(){
		div = $("<DIV>");
		div.attr('class', opt.clazz);
		var tmp = $("<DIV>");
		var hTitle = $.trim(opt.data.title);
		if (hTitle != '') {
			tmp.attr('class', 'cx_title');
			tmp.html(hTitle);
			tmp.appendTo(div);
		}
		var di = opt.data.items;
		var cx_len = di.length, cx;
		for (var i=0; i<cx_len; i++){
			cx = di[i];
			ui_item(i, cx.title, cx.items);
		}
		tmp = $("<DIV>");
		tmp.attr('class', 'cx_chartClear');
		tmp.appendTo(div);
	}
	
	/* Create items */
	function ui_item(i, title, items){
		/* Number of items */
		var len = items.length;
		
		/* Title for a chart */
		var chart = $("<DIV>");
		chart.attr('class', 'cx_chart');
		if (opt.horz && len>0){
			/* Calculate chart width */
			var cw = len*(opt.itemWidth+2*opt.itemPadding+2*opt.itemMargin);
			chart.width(cw);
		}
		chart.appendTo(div);
		var tmp = $("<DIV>");
		tmp.attr('class', 'cx_chartTitle');
		tmp.width('auto');
		tmp.html(title).appendTo(chart);
		
		/* Draw items */
		if (len>0){
			var li, a, title;
			var ul = $("<UL>");
			ul.attr('class', 'cx_chartList');
			ul.appendTo(chart);
			for (var j=0; j<len; j++){
				li = $("<LI>");
				li.attr('style', 'width:'+opt.itemWidth+'px;height:'+opt.itemHeight+'px;padding:'+opt.itemPadding+'px;margin:'+opt.itemMargin+'px');
				li.appendTo(ul);
				a = $("<A>");
				a.attr('href', "#"+i+","+j);
				title = items[j].title;
				if (opt.itemLength>0 && title.length>opt.itemLength) {
					a.html(title.substring(0, opt.itemLength)+"...");
					a.attr('title', title);
				} else {
					a.html(title);
				}
				a.click(function(e){
					exec($(this).attr('href'));
				});
				a.appendTo(li);
			}
		}
		tmp = $("<DIV>");
		tmp.attr('class', 'cx_chartListClear');
		tmp.appendTo(div);
	}
	
	/* Handle onClick event for each item */
	function exec(s){
		var p = s.substring(1).split(',');
		opt.onClick(opt.data.items[p[0]].items[p[1]]);
	}
	
	/* Process data & draw */
	load();
	opt.onDraw(div);
}
})(jQuery);
</script>