

<cfparam name="INPBATCHID" default="0">



<script type="text/javascript">


	$(function() 
	{
		ReadCCDXMLString();
		
		$("#MC_Stage_CCD #SaveCCD").click(function() { WriteCCDXMLString(); });
		$("#MC_Stage_CCD #CancelCCDFormButton").click(function() { ReadCCDXMLString(); });
				
	});



<!--- Leave read/write CCD methods inthe top level dsp_VoiceTools fiel for updating preview menu item--->
		<!--- Output an XML CCD String based on form values --->
	function ReadCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		         $.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=ReadCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		    		data:{
		    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
						},
		            dataType: "json", 
		            success: function(d2, textStatus, xhr) {
		            	var d = eval('(' + xhr.responseText + ')');
		          		<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLSTRING[0]  --->								
										if(typeof(d.DATA.FORMATTEDCID[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpCID").val(d.DATA.FORMATTEDCID[0]);												
										
											<!--- Update preview panel--->
											$("#CIDNumber").html(d.DATA.FORMATTEDCID[0]);											
										}
																			
										if(typeof(d.DATA.FILESEQ[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpFileSeq").val(d.DATA.FILESEQ[0]);												
										}
										
										if(typeof(d.DATA.USERSPECDATA[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpUserSpecData").val(d.DATA.USERSPECDATA[0]);												
										}
										
										if(typeof(d.DATA.DRD[0]) != "undefined")
										{												
											$("#MC_Stage_CCD #inpDRD").val(d.DATA.DRD[0]);												
										}									 
									
										$("#MC_Stage_CCD #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);	
																   
									}
									else
									{
										$("#MC_Stage_CCD #CurrentCCDXMLString").html("Returned" + d.DATA.RXRESULTCODE[0]);											
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
		          }
    		 });			
	}
	
	<!--- Output an XML CCD String based on form values --->
	function WriteCCDXMLString()
	{				
		<!--- AJAX/JSON Do CFTE Demo --->	
		
		<!--- Let the user know somthing is processing --->
		$("#CurrentCCDXMLString").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
		 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=WriteCCDXML&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpCID : $("#MC_Stage_CCD #inpCID").val(), 
    			 inpFileSeq : $("#MC_Stage_CCD #inpFileSeq").val(), 
    			 inpUserSpecData : $("#MC_Stage_CCD #inpUserSpecData").val(), 
    			 inpDRD : $("#MC_Stage_CCD #inpDRD").val()
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CurrRXResultCode > 0)
									{
										ReadCCDXMLString();
										
										<!--- Check if variable is part of JSON result string   d.DATA.CCDXMLString[0]  --->								
										if(typeof(d.DATA.CCDXMLSTRING[0]) != "undefined")
										{									
											$("#MC_Stage_CCD #CurrentCCDXMLString").html(d.DATA.CCDXMLSTRING[0]);																					
										}									
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - Invalid structure");										
								}
							}
							else
							{<!--- No result returned --->
								$("#MC_Stage_CCD #CurrentCCDXMLString").html("Write Error - No result returned");									
							}
          }
     });
}
	
	

</script>	

<div id="MC_Stage_CCD" class="RXFormContainerX rxform2">


       <div id='RXCCDEditSubMenu'> 
        	<div id='pLinkII'>
            <p></p>
            	<a id='SaveCCD' style='display:inline;' title='Save Changes and Show Call Control Data Summary'>Save</a> |  <a id='CancelCCDFormButton' style='display:inline;' title='Cancel Changes to Call Control Data'>Cancel</a>   
            </div>
       	</div> 
        
        <p class='pActive'></p>	
     
        <form id="CCDForm" name="CCDForm" method="post" action="index.html">
        
        <h1>Call Control Data</h1>
        
        <p>Setup optionl capaign control items here. Redials, Caller Ids, etc...</p>
        
        <input type="hidden" name="INPBATCHID" id="INPBATCHID" value="#INPBATCHID#">
         
        <!--- Validate all numerics 0-9 --->
        <!--- Validate less than 15 digits ---> 
        <label>Caller ID Number
        <span class="small">Add your CID</span>
        </label>
        <input type="text" name="inpCID" id="inpCID" class="ui-corner-all" /> 
       
        <!--- Validate max 255 characters? --->
        <label>File Sequence Number
        <span class="small">Add valid data</span>
        </label>
        <input type="text" name="inpFileSeq" id="inpFileSeq" class="ui-corner-all" >
            
        
        <!--- Validate max 255 characters? --->
        <label>User Specified Data
        <span class="small">Max 255 Characters</span>
        </label>
        <input type="text" name="inpUserSpecData" id="inpUserSpecData" class="ui-corner-all" > 
        
        
        <!--- Validate max 30 days --->    
        <label>Minutes Between Redials
        <span class="small">Default 1440 - One Day</span>
        </label>
        <input type="text" name="inpDRD" id="inpDRD" class="ui-corner-all" > 
        
        <div class="spacer"></div>
               
        <div id="CurrentCCDXMLString" style="font-size:10px;">
            NA
        </div>
        
        </form>
        
        <p class='pActive'></p>	
        
</div>     