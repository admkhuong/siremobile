<cfparam name="inpDescId" default="0">
<cfparam name="INPBATCHID" default="">
<script type="text/javascript">
	<!--- alert();	 --->
	var inpDescId = "<cfoutput>#inpDescId#</cfoutput>";
	// Only allow input numeric value for font size
	$("#inpFontSize").ForceNumericOnly();
	// Get current font weight
	var currFontWeight = $("#description"+inpDescId).css("font-weight");
	if(currFontWeight >= 700){
		$("input[name=inpFontWeight]").filter("[value=bold]").attr('checked', true);
	}
	
	// Get current font size
	var currFontSize = $("#description"+inpDescId).css("font-size");
	if(typeof(currFontSize) != 'undefined'){
		currFontSize = currFontSize.substring(0,currFontSize.length-2);
		$("#inpFontSize").val(currFontSize);
	}
	
	// Get current color
	var currColor = $("#description"+inpDescId).css("color");
	$('#editColorSample').css("background-color", currColor);
	$('#editColor').ColorPicker({
		color: currColor,
		onShow: function (colpkr) {
			$(colpkr).fadeIn(1000);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(1000);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#editColorSample').css("background-color", "#" + hex);
		}
	});
	
	
	function saveEditDescription(){
		
		var color = $('#editColorSample').css("background-color");
		// change color
		if(color != 'transparent'){
			$("#description"+inpDescId).css("color",color);
		}
		// change font weight
		$("#description"+inpDescId).css("font-weight",$("input[name=inpFontWeight]:checked").val());
		if($("#inpFontSize").val() != ''){
			// change font size
			$("#description"+inpDescId).css("font-size", $("#inpFontSize").val() + "px");
		}
		// save style to database
		$("#description" +inpDescId).css("border","");
		$("#description"+inpDescId).css("background-color","");
		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/mcidtoolsii.cfc?method=UpdateStyleStageDescription&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,     			
    			 newStyle : $("#description"+inpDescId).attr("style"),
    			 descId:inpDescId
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
   				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditCOMMIDForm_" + INPCOMMID + " #CURRentCOMMTYPEXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("No Response from the remote server. Check your connection and try again.", "Error.");
				}
	     	}
   		});
	}
</script>

<div class="editDescriptionFrm">
	<div class="frm_item">
		<div class="frm_label">Font Weight:</div>
		<div class="frm_control">
			<input type="radio" value="normal" name="inpFontWeight" checked="checked">
			Normal 
			<input type="radio" value="bold"  name="inpFontWeight">
			Bold
		</div>
	</div>
	<div class="frm_item">
		<div class="frm_label">Font size:</div>
		<div class="frm_control"><input id="inpFontSize" type="text"></div>
	</div>
	<div class="frm_item">
		<div class="frm_label">Color:</div>
		<div class="frm_control">
			<div id="editColor">
				<img src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/select.png' style="cursor:pointer" width='20px'>
				<div id="editColorSample" style="width:40px;height:20px;border-radius:4px;display:inline-block;">
				</div>
			</div>
		</div>
	</div>
	
</div>