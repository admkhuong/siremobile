<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>

<script type="text/javascript">

<!--- Main function called after page is loaded --->
	$(function()
	{		
		$("#ListLibs #ListLibsLog").append('<BR><font style="font-weight: bold; color: #F00">Starting</font>' + '<br>');
		
		$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #000">Started Publish/Validate Remote Script Libraries</font>' + '<br>');
		GetAllSCRIPTLIBs(0);
		
	});
	
	
	
	function GetAllSCRIPTLIBs(inpLastProcessedLibId)
	{			
	
		if(typeof(inpLastProcessedLibId) == 'undefined')
			inpLastProcessedLibId= 0;
			
			
		$("#ListLibs #loading").show();		
			 $.ajax({
		            type: "POST",
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=GetSCRIPTLIBs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true",
		    		data:{
		    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
		    			inpStartLibId : inpLastProcessedLibId 
						},
		            dataType: "json", 
		            success: function(d2, textStatus, xhr) {
		            	var d = eval('(' + xhr.responseText + ')');
		            				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								<!--- All good --->
								<!--- if 3 then distribute all and over write --->
								<!--- Add option to force over write --->
								$("#ListLibs #ListLibsLog").append(d.DATA.MESSAGE[0] + '<br>');
														
								<!---
								<!--- Kick off next AJAX call --->
								if(d.DATA.SCRIPTLIB[0] > 0)
								{
									<!--- Publish the library --->
									ValidateRemoteScriptPaths(d.DATA.SCRIPTLIB[0]);
								}
								else
								{
									if(<cfoutput>#inpDoSingleShot#</cfoutput> > 0)
									{
										$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #000">Starting Remote Schedule Updates</font>' + '<br>');
										UpdateRemoteSchedule();
										
									}
									else
									{									
										<!--- All done --->
										$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
										$("#ListLibs #loading").hide();	
									}
								}
								--->
								
								<!--- Kick off next AJAX call --->
								if(d.DATA.SCRIPTLIB[0] > 0 && inpLastProcessedLibId != d.DATA.SCRIPTLIB[0])
								{
									<!--- Get the next library --->
									GetAllSCRIPTLIBs(d.DATA.SCRIPTLIB[0]);
								}
								else
								{
									<!--- All done --->
									$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
									$("#ListLibs #loading").hide();
								}
								
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#ListLibs #loading").hide();} );
									$("#ListLibs #ListLibsLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server. Unable to get list of script librarys", function(result) {$("#ListLibs #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to get list of script librarys",function(result) {$("#ListLibs #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.",function(result) {$("#ListLibs #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#ListLibs #loading").hide(); --->
			
		          }
     });	
	<!--- 		$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=GetSCRIPTLIBs&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpStartLibId : inpLastProcessedLibId }, 
	
			<!--- Default return function for Do CFTE Demo - Async call back --->
			function(d) 
			{
				<!--- Alert if failure --->
																							
					<!--- Get row 1 of results if exisits--->
					if (d.ROWCOUNT > 0) 
					{																									
						<!--- Check if variable is part of JSON result string --->								
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{							
							CurrRXResultCode = d.DATA.RXRESULTCODE[0];	
						
							if(CurrRXResultCode > 0)
							{	
								<!--- All good --->
								<!--- if 3 then distribute all and over write --->
								<!--- Add option to force over write --->
								$("#ListLibs #ListLibsLog").append(d.DATA.MESSAGE[0] + '<br>');
														
								<!---
								<!--- Kick off next AJAX call --->
								if(d.DATA.SCRIPTLIB[0] > 0)
								{
									<!--- Publish the library --->
									ValidateRemoteScriptPaths(d.DATA.SCRIPTLIB[0]);
								}
								else
								{
									if(<cfoutput>#inpDoSingleShot#</cfoutput> > 0)
									{
										$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #000">Starting Remote Schedule Updates</font>' + '<br>');
										UpdateRemoteSchedule();
										
									}
									else
									{									
										<!--- All done --->
										$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
										$("#ListLibs #loading").hide();	
									}
								}
								--->
								
								<!--- Kick off next AJAX call --->
								if(d.DATA.SCRIPTLIB[0] > 0 && inpLastProcessedLibId != d.DATA.SCRIPTLIB[0])
								{
									<!--- Get the next library --->
									GetAllSCRIPTLIBs(d.DATA.SCRIPTLIB[0]);
								}
								else
								{
									<!--- All done --->
									$("#ListLibs #ListLibsLog").append('<font style="font-weight: bold; color: #F00">Finished!</font>' + '<br>');
									$("#ListLibs #loading").hide();
								}
								
							}
							else
							{
								if( typeof(d.DATA.MESSAGE[0]) != "undefined")
								{									
									jAlert(d.DATA.ERRMESSAGE[0], d.DATA.MESSAGE[0], function(result) {$("#ListLibs #loading").hide();} );
									$("#ListLibs #ListLibsLog").append(d.DATA.ERRMESSAGE[0] + ' - ' + d.DATA.MESSAGE[0] + '<br>');
								}
								else
								{
									jAlert("Error.", "Invalid Response from the remote server. Unable to get list of script librarys", function(result) {$("#ListLibs #loading").hide();});									
								}
							}
							
						}
						else
						{<!--- Invalid structure returned --->	
							jAlert("Error.", "Invalid Response from the remote server.  Unable to get list of script librarys",function(result) {$("#ListLibs #loading").hide();});
						}
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + inpQID + " #CurrentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.",function(result) {$("#ListLibs #loading").hide();});
					}
						
				<!--- All is well - we will still be processing --->		
				<!--- $("#ListLibs #loading").hide(); --->
			
			} ); --->	
		
			
		
		return false;
	}
    
    
</script>



<div id="ListLibs">
    
    <div id="loading">
    <img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">Processing...
    </div>
    
    <div id="ListLibsLog" style=" border:solid; border-bottom-width:2px; height:500px; width:750px; overflow:auto; white-space:nowrap;">
    
    </div>


</div>


    