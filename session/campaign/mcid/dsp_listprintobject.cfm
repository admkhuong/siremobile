<cfoutput>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/grid.locale-en.js"></script>
		<script type="text/javascript" src="#rootUrl#/#PublicPath#/js/jquery/js/jquery.jqgrid.min.js"></script>
    	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.print-preview.js" type="text/javascript" charset="utf-8"></script>  
</cfoutput>
<script type="text/javascript">
	var arrDataMCID = new Array();
	$("#SBStage > div[id]").each(function(){
		var mcidObject = {};
		mcidObject.id = $(this).attr("id");
		mcidObject.description = $(this).find("#BGImage").attr("desc");
		mcidObject.type = $(this).find("#BGImage").attr("objType");
		if(mcidObject.type == 'DESCRIPTION'){
			mcidObject.id = $(this).find("div:first").attr("id");
		}
		arrDataMCID.push(mcidObject);
	});
	
		
	$("#printObjectTable").jqGrid({ 
		data: arrDataMCID, 
		datatype: "local", 
		height: 'auto', 
		rowNum: 30, 
		rowList: [10,20,30,50,100], 
		colNames:['MCID ID', 'Description','Type'], 
		colModel:
			[ 
			{name:'id',index:'id', width:60, sorttype:"int"}, 
			{name:'description',index:'description', width:400 }, 
			{name:'type',index:'type', width:90}
			], 
		multiselect: true,
		pager: "#printObjectPaper", 
		grouping:true,
		groupingView : { groupField : ['type'] },
		viewrecords: true,
		sortname: 'name', 
		caption: "" 
	});
	
	
	function printSelectedObject(){
        var s = jQuery("#printObjectTable").jqGrid('getGridParam','selarrrow');
        // Find unselected id
        var unSelectedObj = new Array();
        for(index in arrDataMCID){
        	if($.inArray(arrDataMCID[index].id,s) == -1){
        		unSelectedObj.push(arrDataMCID[index].id);
        	}
        }
        $.printPreview({
        	printDiv : 'SBStage',
	      	arrExcludeObj:unSelectedObj
        });
	}
</script> 
<div>
	<table id="printObjectTable"></table>
	<div id="printObjectPaper"></div>
</div>
