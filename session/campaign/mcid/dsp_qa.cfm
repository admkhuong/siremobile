<cfparam name="INPBATCHID" default="0">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="permissionStr">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
</cfinvoke>
<cfif NOT permissionStr.havePermission>
	<cfoutput>#permissionStr.message#</cfoutput>
	<cfexit>
</cfif>


<script type="text/javascript"> 

	$(function()
	{		
		$("#VoiceQAMain #sendQAVoiceButton").click( function() 
			{
					DoQAVoice();
					return false;
			}); 
			
		$("#VoiceQAMain #QAListLibsButton").click( function() 
			{
					ListLibsDialogVoiceToolsQA();
					return false;
			}); 
			
		$("#VoiceQAMain #QAPublishLibsButton").click( function() 
			{
					PublishLibsDialogVoiceToolsQA();
					return false;
			}); 	
				
			
		$("#VoiceQAMain #loadingDlgsendQAVoice").hide();		
		
		$("#VoiceQAMain #LifeCycleEventMap").click(function() { EditLCEMDialog(); return false; });
		
		  
	} );
	
		
		
	<!--- Global so popup can refernece it to close it--->
	var CreateEditLCEMDialog = 0;
	
	function EditLCEMDialog()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
													
		<!--- Erase any existing dialog data --->
		if(CreateEditLCEMDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			CreateEditLCEMDialog.remove();
			CreateEditLCEMDialog = 0;
			
		}
						
		CreateEditLCEMDialog = $('<div style="overflow: auto;"></div>').append($loading.clone());
		
		CreateEditLCEMDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rules/dsp_LifeCycleEventMap?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>')
			.dialog({
				modal : true,
				title: 'Life Cycle Event Map',
				close: function() { CreateEditLCEMDialog.dialog('destroy'); CreateEditLCEMDialog.remove(); CreateEditLCEMDialog = 0;},
				width: 1100,
				height: 'auto'
			});
	
		CreateEditLCEMDialog.dialog('open');
	
		return false;		
	}

	function DoQAVoice()	
	{	
		<!--- AJAX/JSON  --->	
		
		<!--- Let the user know somthing is processing --->
		$("#VoiceQAMain #loadingDlgsendQAVoice").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	
	
		$.ajax({
			type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
			url: '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/cfc/Distribution.cfc?method=QASendVoice&returnformat=json&queryformat=column&_cf_nodebug=true&_cf_nocache=true',   
			dataType: 'json',
			data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, inpQAVoiceAddr : $("#VoiceQAMain #inpQAVoiceAddr").val(), inpSendHTML : 1 }, 					  
			error: function(XMLHttpRequest, textStatus, errorThrown) { $.alerts.okButton = '&nbsp;OK&nbsp;'; jAlert("QA e-Mail Error.\n"  + textStatus + "\n" + errorThrown, "Failure!", function(result) { } );	 <!---console.log(textStatus, errorThrown);--->},					  
			success:		
					<!--- Default return function for Do CFTE Demo - Async call back --->
					function(d) 
					{
						<!--- Alert if failure --->
																									
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{																									
								<!--- Check if variable is part of JSON result string --->								
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{							
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
									
									if(CURRRXResultCode > 0)
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';					
										jAlert("QA e-Mail Sent OK.", "Check your inbox.", function(result) {CreateScheduleQAShotDialogVoiceTools.remove(); } );	
									}
									else
									{
										$.alerts.okButton = '&nbsp;OK&nbsp;';
										jAlert("QA e-Mail Error.\n"  + d.DATA.MESSAGE[0] + "\n" + d.DATA.ERRMESSAGE[0], "Failure!", function(result) { } );	
									}
								}
								else
								{<!--- Invalid structure returned --->										
									$.alerts.okButton = '&nbsp;OK&nbsp;';					
									jAlert("QA e-Mail Error.", "Invalid structure.");										
								}
							}
							else
							{<!--- No result returned --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';					
								jAlert("QA e-Mail Error.", "No Response from the remote server. Check your connection and try again.");								
							}
					} 								
		});
	
	}

	
	
	<!--- Global so popup can refernece it to close it--->
	var CreateListLibsDialogVoiceToolsQA = 0;
	
	function ListLibsDialogVoiceToolsQA()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateListLibsDialogVoiceToolsQA != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateListLibsDialogVoiceToolsQA.remove();
			CreateListLibsDialogVoiceToolsQA = 0;
			
		}
						
		CreateListLibsDialogVoiceToolsQA = $('<div></div>').append($loading.clone());
		
		CreateListLibsDialogVoiceToolsQA
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_ListLibs' + ParamStr)
			.dialog({
				modal : true,
				title: 'List Voice Libraries Tool',
				width: 790,
				minHeight: 200,
				height: 'auto',
				position: 'top'
			});
	
		CreateListLibsDialogVoiceToolsQA.dialog('open');
	
		return false;		
	}
	
	
	<!--- Global so popup can refernece it to close it--->
	var CreatePublishLibsDialogVoiceToolsQA = 0;
	
	function PublishLibsDialogVoiceToolsQA()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreatePublishLibsDialogVoiceToolsQA != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreatePublishLibsDialogVoiceToolsQA.remove();
			CreatePublishLibsDialogVoiceToolsQA = 0;
			
		}
						
		CreatePublishLibsDialogVoiceToolsQA = $('<div></div>').append($loading.clone());
		
		CreatePublishLibsDialogVoiceToolsQA
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/scripts/dsp_PublishScripts' + ParamStr)
			.dialog({
				modal : true,
				title: 'Publish Voice Libraries Tool',
				width: 790,
				minHeight: 200,
				height: 'auto',
				position: 'top'
			});
	
		CreatePublishLibsDialogVoiceToolsQA.dialog('open');
	
		return false;		
	}



</script> 




<!--- Read in each optional value and give an input field for it--->


<cfoutput>


<div id="VoiceQAMain" class="rxform2">

<!---
	<div style="float:left; width:600px; min-width:600px;" class="RXFormContainerX" >
    
    
        <form>
            
            <!--- Default from current User--->
            <!--- Validate e-Mail Address --->
            <label>Test Voice Phone Number
            <span class="small">This is where your test Voice will be sent</span>
            </label>
            <input type="text" name="inpQAVoiceAddr" id="inpQAVoiceAddr" class="ui-corner-all" value="#Session.PrimaryPhone#" > 
        
            <br/>
             
            <div style="clear:both; width:300px;">
                        
            <button id="sendQAVoiceButton" TYPE="button">Send Now</button>
             <!---   <button id="Cancel" TYPE="button">Cancel</button>--->
             
                <div id="loadingDlgsendQAVoice" style="display:inline;">
                    <img class="loadingImg" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="20" height="20">
                </div>
            </div>       
            
            <br/>
            
        </form>

	</div>--->

	<div style="width:600px; min-width:600px;"  class="RXFormContainerX">
    	
        <label>Tools</label>
    	<BR />
        <button id="QAListLibsButton" TYPE="button">List Libraries</button>
    	<!---<br />
        <button id="QAPublishLibsButton" TYPE="button">Publish Libraries to QA</button>--->
        <br />                   
    	<button id="LifeCycleEventMap" TYPE="button">Life Cycle Event Map</button>
		<br />
            
    
    </div>

</div>


</cfoutput>