<cfsilent>
<cfparam name="INPBATCHID" type="string" default="0"/>
<cfparam name="INPQID" type="string" default="-1"/>	


<cfset CURRDESC = "Description Not Specified">
    
    <cftry>                	
                                              
        <!--- Read from DB Batch Options --->
        <cfquery name="GetBatchOptions" datasource="#Session.DBSourceEBM#">
            SELECT                
              XMLControlString_vch
            FROM
              simpleobjects.batch
            WHERE
              BatchId_bi = <CFQUERYPARAM CFSQLTYPE="CF_SQL_VARCHAR" VALUE="#INPBATCHID#">
        </cfquery>     
        
        <cfset inpRXSSLocalBuff = GetBatchOptions.XMLControlString_vch>
         
        <!--- Parse for data --->                             
        <cftry>
              <!--- Adding <XMLControlStringDoc> tag here ensures valid XML in case of blank data --->
             <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>" & #inpRXSSLocalBuff# & "</XMLControlStringDoc>")>
               
              <cfcatch type="any">
                <!--- Squash bad data  --->    
                <cfset myxmldocResultDoc = XmlParse("<XMLControlStringDoc>BadData</XMLControlStringDoc>")>                       
             </cfcatch>              
        </cftry>                            
      
        <!--- Get the RXSS ELE's --->
        <cfset selectedElements = XmlSearch(myxmldocResultDoc, "/XMLControlStringDoc/RXSS//*[@QID=#INPQID#]")>                
         
        <cfloop array="#selectedElements#" index="CURRMCIDXML">
			<cfset CURRDESC = CURRMCIDXML.XmlAttributes.DESC>  
        </cfloop>
            
                                     
    <cfcatch type="any">
        
        <cfset CURRDESC = "READ ERROR">         
        
    </cfcatch>
    
    </cftry>     
</cfsilent>
<cfoutput>#CURRDESC#</cfoutput>