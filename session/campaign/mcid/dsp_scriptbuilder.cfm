<cfparam name="INPBATCHID" default="0">

<cfoutput>
	<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine-en.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/validate/jquery.validationengine.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/validate/validationcustom.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/swfobject.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/help/language.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#PublicPath#/js/help/jquery.tipsy.js" type="text/javascript" charset="utf-8"></script>
    <script src="#rootUrl#/#PublicPath#/js/jquery.jeditable.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="#rootUrl#/#SessionPath#/campaign/mcid/js/raphael.js"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/zoom.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/mcidpathdrawer.js" type="text/javascript" charset="utf-8"></script>
    <script src="#rootUrl#/#SessionPath#/campaign/mcid/ruletypes/ruleidpathdrawer.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/editscriptform.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.pagination.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/displayxml.js" type="text/javascript" charset="utf-8"></script>
	<script src="#rootUrl#/#SessionPath#/campaign/mcid/js/jquery.numbericbox.js" type="text/javascript" ></script>
	<script src="#rootUrl#/#SessionPath#/js/social/facebook_all.js" type="text/javascript" ></script>
</cfoutput>

<cfinclude template="inc_RulesBuilder.cfm">
<cfinclude template="inc_CommsBuilder.cfm">

<cfset CurrSitePath = "#rootUrl#/#SessionPath#/MCID">
<cfset CurrSitePathIntegrated = "#rootUrl#/#SessionPath#">
<cfset StageWidth = 1250>
<cfset Session.UserLevel = 10>

<cfparam name="INPBATCHID" default="0">

<cfscript>
	 files = '\#SessionPath#\permissions.properties';
	 properties = createObject( 'component', '\#SessionPath#\cfc\propertiesutil' ).init( files );
	 
	//writeOutput( 'key1 is ' & properties.getProperty( 'RXT1' ) );
</cfscript>
<cfset RXT1="#properties.getProperty('RXT1')#">
<cfset RXT2="#properties.getProperty('RXT2')#">
<cfset RXT3="#properties.getProperty('RXT3')#">
<cfset RXT4="#properties.getProperty('RXT4')#">
<cfset RXT5="#properties.getProperty('RXT5')#">
<cfset RXT6="#properties.getProperty('RXT6')#">
<cfset RXT7="#properties.getProperty('RXT7')#">
<cfset RXT8="#properties.getProperty('RXT8')#">
<cfset RXT9="#properties.getProperty('RXT9')#">
<cfset RXT10="#properties.getProperty('RXT10')#">
<cfset RXT11="#properties.getProperty('RXT11')#">
<cfset RXT12="#properties.getProperty('RXT12')#">
<cfset RXT13="#properties.getProperty('RXT13')#">
<cfset RXT14="#properties.getProperty('RXT14')#">
<cfset RXT15="#properties.getProperty('RXT15')#">
<cfset RXT16="#properties.getProperty('RXT16')#">
<cfset RXT17="#properties.getProperty('RXT17')#">
<cfset RXT18="#properties.getProperty('RXT18')#">
<cfset RXT19="#properties.getProperty('RXT19')#">
<cfset RXT20="#properties.getProperty('RXT20')#">
<cfset RXT21="#properties.getProperty('RXT21')#">
<cfset RXT22="#properties.getProperty('RXT22')#">
<cfset RXT23="#properties.getProperty('RXT23')#">
<cfset RXT24="#properties.getProperty('RXT24')#">

<!---check permission--->
<cfinclude template="/#sessionPath#/Administration/constants/userConstants.cfm">
<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkBatchPermissionByBatchId">
	<cfinvokeargument name="operator" value="#edit_Campaign_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT1Permission">
	<cfinvokeargument name="operator" value="#RXT1_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT2Permission">
	<cfinvokeargument name="operator" value="#RXT2_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT3Permission">
	<cfinvokeargument name="operator" value="#RXT3_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT4Permission">
	<cfinvokeargument name="operator" value="#RXT4_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT5Permission">
	<cfinvokeargument name="operator" value="#RXT5_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT6Permission">
	<cfinvokeargument name="operator" value="#RXT6_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT7Permission">
	<cfinvokeargument name="operator" value="#RXT7_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT9Permission">
	<cfinvokeargument name="operator" value="#RXT9_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT10Permission">
	<cfinvokeargument name="operator" value="#RXT10_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT11Permission">
	<cfinvokeargument name="operator" value="#RXT11_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT12Permission">
	<cfinvokeargument name="operator" value="#RXT12_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT13Permission">
	<cfinvokeargument name="operator" value="#RXT13_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT14Permission">
	<cfinvokeargument name="operator" value="#RXT14_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT15Permission">
	<cfinvokeargument name="operator" value="#RXT15_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT16Permission">
	<cfinvokeargument name="operator" value="#RXT16_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT17Permission">
	<cfinvokeargument name="operator" value="#RXT17_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT18Permission">
	<cfinvokeargument name="operator" value="#RXT18_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT19Permission">
	<cfinvokeargument name="operator" value="#RXT19_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT20Permission">
	<cfinvokeargument name="operator" value="#RXT20_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT21Permission">
	<cfinvokeargument name="operator" value="#RXT21_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT22Permission">
	<cfinvokeargument name="operator" value="#RXT22_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<cfinvoke component="#LocalSessionDotPath#.cfc.administrator.permission" method="havePermission" returnvariable="checkRXT24Permission">
	<cfinvokeargument name="operator" value="#RXT24_Title#">
	<cfinvokeargument name="INPBATCHID" value="#INPBATCHID#">
</cfinvoke>

<script type="text/javascript">
	
	var editCampaignPermission = <cfoutput>#checkBatchPermissionByBatchId.havePermission#</cfoutput>;
	var permissionMessage = '<cfoutput>#checkBatchPermissionByBatchId.message#</cfoutput>';
	
	var QID_ARRAY = [];

	var CurrSitePath = '<cfoutput>#CurrSitePath#</cfoutput>';
	var CurrSitePathIntegrated = '<cfoutput>#CurrSitePathIntegrated#</cfoutput>';

	<!--- Drawing area of stage for LINKing IVR objects --->
	var StageCanvas;
	var Obj0Color = '#335C9F';
	var Obj1Color = '#D4DF23';
	var Obj2Color = '#335C9E';
	var Obj3Color = '#E82E15';
	var Obj4Color = '#8E1883';
	var Obj5Color = '#E3891E';
	var Obj6Color = '#098B47';
	var Obj7Color = '#58290A';
	var Obj8Color = '#000000';
	var Obj9Color = '#D4DF24';
	var ObjPoundColor = '#E3891D'; <!--- # --->
	var ObjErrColor = '#E82E16'; <!--- Red --->
	var ObjDefColor = '#00AA00'; <!--- Blue --->
	var ObjNRColor = '#D4DF25'; <!--- Yellow --->
	var ObjStarColor = '#7E1883'; <!--- * --->
	var ObjLiveColor = '#D4DF22'; <!--- Yellow --->
	var ObjMachineColor = '#E82E12'; <!--- Red --->
	<!--- Fix bug adhesion devices after drop --->
	var ObjIndex = 1;

	<!--- Max, Min (bottom) QID --->
	var maxQID = 0;

	<!--- Save selected QID to change --->
	var curSelectQID = 0;

	<!--- Connection highlight color --->
	var objHighlightBlueColor = '#00FF00';
	var mobileWidth = 80;
	var deviceWidth = 0;
	var deviceHeight = 0;

	var sbStageDefaultWidth;
	var sbStageDefaultHeight;
	var SBContentsDefaultWidth = 1265;
	var SBContentsDefaultHeight = 1362; 
	
	<!--- Global so popup can refernece it to close it--->
	var $CreateXMLContorlStringReviewDialogVoiceTools = null;
		
	<!--- Set MCID tool function --->
	$(document).ready(function(){
		<!--- Get Width and Height Stage --->
		var scale = 1;
			$.ajax({
				type:"POST",
				url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=GetStageDemension&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				data:{
					INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				},
	            dataType: "json", 
	            async: false,
	            success: function(d2, textStatus, xhr) {
	            		sbStageDefaultWidth=d2.DATA.WIDTH;
	            		sbStageDefaultHeight=d2.DATA.HEIGHT;
	            		sbStageDefaultScale = d2.DATA.SCALE;
	            		if (sbStageDefaultScale) {
	            			scale = sbStageDefaultScale;
	            		} else {
	            			scale = 1;
	            		}
	            		
	            		$('#txtWidth').val(sbStageDefaultWidth);
	            		$('#txtHeight').val(sbStageDefaultHeight);
	            		
	            		//if(scale > 0.90){
	            			$('#mainPage').css('height', '1000px');
	            		//}else{
	            			//$('#mainPage').css('height', sbStageDefaultHeight + 100);
	            		//}
						$('#SBStage').css('width',parseInt(sbStageDefaultWidth)).css('height',parseInt(sbStageDefaultHeight));
						
				        $('#stageContainer').css('width',parseInt(sbStageDefaultWidth)+22).css('height',parseInt(sbStageDefaultHeight) + 20);	            		
	            		$('#stageContainer').css('-moz-transform','scale('+parseFloat(scale)+')');
	            		
	            		
	            }
			});
		<!--- Clear all MCID system function --->		
		
		<!--- Zoom in, zoom out --->
		$('#zoomInBtn').click(function() {
			if ($('#zoomInBtn').attr('class') == 'inactive') {
				return;
			}
			$("#zoomOutBtn").removeClass().addClass('active');
			var $body = $(document.body);
			var divWidth = sbStageDefaultWidth;
			var divHeight = sbStageDefaultHeight;
			if (scale == 'NaN') {
				scale = 1;
			}
			if (scale > 1.5) {
				jAlert('No can zoom in.','Message');
				scale = 1.5;
				return false;
			}
			scale = parseFloat(scale)+0.1;
			var class1 = $('body').attr('class');
			<!--- Save zoom in database --->
			$.ajax({
	            type: "POST",
	            async: false,
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=changeStageZoom&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    		data:{
		    		 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
		    		 inpRXSS : "", 
		    		 stageZoomScale : scale
				},
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	var d = eval('(' + xhr.responseText + ')');
       				if (d.ROWCOUNT > 0) 
					{
						<!--- Refresh view --->
						
						$("#loading").hide();
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
         		 }
	   		  	});
	   		  	
				divWidth = Math.floor(divWidth * scale);
				divHeight = Math.floor(divHeight * scale);
			       				
				<!--- Refresh view --->
				$("#loading").hide();
				if (scale <=0.9) {
					$("#SBContents").css({
			           	'width': SBContentsDefaultWidth + 'px',
			           	'height': SBContentsDefaultHeight + 'px'
		           	});
		           	
				} else {
					$("#SBContents").css({
			           	'width': SBContentsDefaultWidth*scale + 'px',
			           	'height': SBContentsDefaultHeight*scale + 'px'
		           	});
		           	//$('#mainPage').css('height', scale * sbStageDefaultHeight + 80);
				}
				
				$("#stageContainer").css({
		           	<!---'height': divHeight + 'px',
		           	'width': divWidth + 'px',--->
		           	'-o-transform': 'scale(' + scale + ')',
		           	'-moz-transform': 'scale(' + scale + ')',
		           	'zoom': scale*100 +'%'
	           	});
		
			
	   		 
	   		  	
		});

		$('#zoomOutBtn').click(function() {
			if ($('#zoomOutBtn').attr('class') == 'inactive') {
				return;
			}
			<!--- var $body = $(document.body); --->
			var divWidth = sbStageDefaultWidth;
			var divHeight = sbStageDefaultHeight;
			if (scale == 'NaN') {
				scale = 1;
			}
			if (scale < 0.6) {
				jAlert('No can zoom out.','Message');
				scale = 0.5;
				return false;
			}
			scale = parseFloat(scale)-0.1;
			
			var class1 = $('body').attr('class');
			$("#zoomInBtn").removeClass().addClass('active');
			
			<!--- Save zoom in database --->
			$.ajax({
	            type: "POST",
	            async: false,
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=changeStageZoom&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    		data:{
		    		 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
		    		 inpRXSS : "", 
		    		 stageZoomScale : scale
				},
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	var d = eval('(' + xhr.responseText + ')');
       				if (d.ROWCOUNT > 0) 
					{
						<!--- Refresh view --->
						
						$("#loading").hide();
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
         		 }
	   		  	});
			divWidth *= scale;
			divHeight *= scale;

			
			<!--- Refresh view --->
			$("#loading").hide();
			if (scale <=0.9) {
				$("#SBContents").css({
		           	'width': SBContentsDefaultWidth + 'px',
		           	'height': SBContentsDefaultHeight + 'px'
	           	});
			} else {
				$("#SBContents").css({
		           	'width': SBContentsDefaultWidth*scale + 'px',
		           	'height': SBContentsDefaultHeight*scale + 'px'
	           	});
	           	//$('#mainPage').css('height', scale * sbStageDefaultHeight + 80);
			}
			
			$(".SBStageContainer").css({
	           	'-o-transform': 'scale(' + scale + ')',
	           	'-moz-transform': 'scale(' + scale + ')',
		        'zoom': scale*100 +'%'
           	});
					
			
		});

			
			
		<!--- Campaign/Batch Schedule --->
		$("#scheduleBtn").click(function() { DoCreateScheduleSBDialog(); 	});
		$("#CollaborationBtn").click(function() { DoCreateCollaborationSBDialog(); 	});
		$("#templateBtn").click(function() { 
			TemplatePickerDialogVoiceTools();
		});
		$("#qaBtn").click(function() { 
			ScheduleQAShotDialogVoiceTools();
		});
		$("#btnSelect").click(function(){
			showDialogScreen();
		});
		$("#advancedBtn").click(function() {
			AdvancedBatchOptionsDialogVoiceTools();
		});	
		<!--- Auto sort MCIDs --->
		$("#sortBtn").click(function() {
			if ($('#sortBtn').is('.inactive') == false){
			var maxDeviceWidth = 132;
			var maxDeviceHeight = 125;
			var stageWidth = parseInt($("#SBStage").css("width")) - maxDeviceWidth;
			var stageHeight = parseInt($("#SBStage").css("height")) - maxDeviceHeight;
			
			$.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=AutoSort&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    		data:{
		    		 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
		    		 inpRXSS : ""
				},
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	var d = eval('(' + xhr.responseText + ')');
       				if (d.ROWCOUNT > 0) 
					{
						if(d.DATA.RXRESULTCODE[0] < 0){
							alert(d.DATA.MESSAGE);
							return false;
						}
						<!--- Refresh view --->
						$("#loading").hide();
						LoadStage();
						$('#undoBtn').removeClass().addClass("active");
						$('#redoBtn').removeClass().addClass("active");
					}
					else
					{<!--- No result returned --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
         		 }
	   		  	});
				$("#loading").hide();
			}

			return false;
		});
		//show XML 
				
		<!--- show current XML on dialog --->
		$("#xmlBtn").click(function() {
			
			<!---XMLContorlStringReviewDialogVoiceTools();--->
			
			<!--- Erase any existing dialog data --->
			if($CreateXMLContorlStringReviewDialogVoiceTools != null)
			{
				<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
				$CreateXMLContorlStringReviewDialogVoiceTools.remove();
				$CreateXMLContorlStringReviewDialogVoiceTools = null;
			}			
					
			var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
			
			var ParamStr = '';
		
			ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>) 
	
			$CreateXMLContorlStringReviewDialogVoiceTools = $('<div></div>').append($loading.clone());
				
			$CreateXMLContorlStringReviewDialogVoiceTools
				.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/ire/Survey/EditQuestions/dsp_XMLControlString' + ParamStr)
				.dialog({
				modal : true,
				close: function() { $CreateXMLContorlStringReviewDialogVoiceTools.remove(); $CreateXMLContorlStringReviewDialogVoiceTools = null;}, 
				title: 'XML Control String',
				width: 1000,
				resizable: false,
				height: 'auto',
				position: 'top',
				draggable: true,
				autoOpen: true,				
				beforeClose: function(event, ui) { 	}
			})
			
		});
		
		$("#GoBigBtn").click(function() {
			
			if ($('#GoBigBtn').is('.inactive') == false)
			{
				console.log('was active');
				
				
				$('#GoBigBtn').removeClass().addClass("inactive");
				$('#main_stage').removeClass("main_stage_limiter");				
												
			}
			else
			{
				
				console.log('was inactive');
				
				$('#main_stage').removeClass().addClass("main_stage_limiter");
				$('#GoBigBtn').removeClass().addClass('active');
				
			}
			
		});
		
        $('#printBtn').click(function(){
        	var $printObjectSelectionDialog = $("<div></div>");
			$printObjectSelectionDialog.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_listPrintObject')
			.dialog({
				modal : true,
				title: 'MCID object selection',
				width: 650,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				close: function() { 
					$printObjectSelectionDialog.remove(); 
				},
				buttons:[{
						text:'Print',
						click:function(){
							printSelectedObject();
							$printObjectSelectionDialog.remove(); 					
						}
					},
					{
						text:'Cancel',
						click:function(){
							$printObjectSelectionDialog.remove(); 
						}
					}
					]
			});
	
        });
		
		$("#clearBtn").click(function() { 
			<!--- Erase any RXSS in CURRent batch ClearRXSS --->
			if ($('#clearBtn').is('.inactive') == false){
				ClearRXSS();
				QID_ARRAY = [];
			}
		});
		
		
		<!--- Undo function --->
		$("#undoBtn").click(function()
		{
			if ($('#undoBtn').is('.inactive') == false)
			{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=UndoMCID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					dataType: 'json',
					data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> },
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
					success: function(d2)
					{
						if(typeof(d2.DATA) != "undefined"){
							if(d2.DATA.RXRESULTCODE[0] < 0){
								alert(d2.DATA.MESSAGE[0]);
								return false;
							}
						}else{
						
							if (d2 == 0)
							{
								$('#undoBtn').removeClass().addClass('inactive');
							}
							else if (d2 == 1)
							{
								LoadStage();
								$('#undoBtn').removeClass().addClass('inactive')
								$('#redoBtn').removeClass().addClass('active');
							}
							else
							{
								LoadStage();
								$('#undoBtn').removeClass().addClass('active');
								$('#redoBtn').removeClass().addClass('active');
							}
						
						}
						
					}
				});
			}
		});
	
		<!--- Redo function --->
		$("#redoBtn").click(function()
		{
			if ($('#redoBtn').is('.active'))
			{
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=RedoMCID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					datatype: 'json',
					data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> },
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
					success: function(d2, textStatus, xhr)
					{
						if (d2 == 0)
						{
							$('#redoBtn').removeClass().addClass('inactive');
						}
						else if (d2 == 1)
						{
							LoadStage();
							////$('#redoBtn').removeClass();
							$('#undoBtn').removeClass().addClass('active');
							$('#redoBtn').removeClass().addClass('active');
						}
						else
						{
							LoadStage();
							$('#undoBtn').removeClass().addClass('active');
							$('#redoBtn').removeClass().addClass('active');
						}
					}
				});
			}
		});
		
		<!--- Remove a MCID --->
		$(".imgObjDelete").mouseover( function(){
			$(this).removeAttr('src');
			$(this).attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_hover.png');
		});
		
	});

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Call this when documenbt finishes loading is ready for action --->
	$(function() {
		<!--- Set draggable for menu item to create new mcid --->
	    <!--- if user click zoom in zoom out multi times then MCID object draggable hide after canvas SBStage so fix zIndex here --->
		$( ".SBMenuItem" ).draggable({ revert: "invalid", containment: "#stageContainer", opacity: 0.8, helper: "clone",zIndex:99999});

		<!--- Define what to do if correct object is dropped onto stage --->  
		$( "#SBStage" ).droppable({
			
			<!--- Custom fuction to accept both menu items and stage objects --->
			accept:  function(d) { 	if( d.hasClass("SBStageItem") || d.hasClass("SBMenuItem") || d.hasClass("ObjHDef33")) return true; },
			greedy: false,
			drop: function( event, ui ) {
				<!--- Ignore movement of objects within the drop zone --->
				if( ui.draggable.hasClass('SBStageItem') )
				{
					<!--- Just update position data --->
					return; 
				}
				if( ui.draggable.hasClass('SBMenuItemRules') )
				{
					//alert(1);
					if(!editCampaignPermission){
						alert(permissionMessage);
						return false;
					} 
					var containerScrollTop = document.getElementById("main_stage").scrollTop;
					var containerScrollLeft = document.getElementById("main_stage").scrollLeft;
					var dropXpos = ui.position.left - 320 + containerScrollLeft;
					var dropYpos = ui.position.top + 175 + containerScrollTop;
					
					<!--- Create new rule on stage --->
					LoadStageLifeCycleObject(dropXpos, dropYpos , $(ui.draggable).attr('rel'), 0, 'Description Not Specified');					 
				}
				else if( ui.draggable.hasClass('SBMenuItemComms') )
				{
					if(!editCampaignPermission){
						alert(permissionMessage);
						return false;
					} 
					var containerScrollTop = document.getElementById("main_stage").scrollTop;
					var containerScrollLeft = document.getElementById("main_stage").scrollLeft;
					var dropXpos = ui.position.left - 320 + containerScrollLeft;
					var dropYpos = ui.position.top - 40 + containerScrollTop;
					
					var curRXSSIDObject = {};
					curRXSSIDObject.XPOS = dropXpos;
					curRXSSIDObject.YPOS = dropYpos;
					curRXSSIDObject.COMMSTYPE = $(ui.draggable).attr('rel');
					curRXSSIDObject.COMMSID = 0;
					curRXSSIDObject.DESC = 'No Specific';
					curRXSSIDObject.DYNAMICFLAG = '';
					curRXSSIDObject.STYLE = '';
					if(curRXSSIDObject.COMMSTYPE == 9){
						curRXSSIDObject.ID = $("div.stageDescContainer").length + 1; 
					}
					<!--- Create new comm obj on stage --->
					LoadStageCommsObject(curRXSSIDObject);					 
				}
				else
				{
					if(!editCampaignPermission){
						alert(permissionMessage);
						return false;
					} 
					var containerScrollTop = document.getElementById("main_stage").scrollTop;
					var containerScrollLeft = document.getElementById("main_stage").scrollLeft;
					var dropXpos = ui.position.left - 300 + containerScrollLeft;
					var dropYpos = ui.position.top + 400 + containerScrollTop;
					<!--- Create new mcid on stage --->
					LoadStageObject(dropXpos, dropYpos, $(ui.draggable).attr('rel'), 0, 'Description Not Specified');
				}
				<!--- Active undo--->
				$('#undo').removeClass().addClass('active');
				
				$('#undoBtn').removeClass().addClass('active');
			}
		});

		<!--- Hide advanced uer items if access is too low.--->
		<cfif Session.UserLevel LT 7>$("#CURRRXSSXML").hide();</cfif>

		<!--- http://net.tutsplus.com/tutorials/javascript-ajax/an-introduction-to-the-raphael-js-library/ --->
		<!--- ??? why jquery selector does not work right? http://stackoverflow.com/questions/1310159/how-to-do-this-using-jquery-document-getelementbyidselectlist-value --->
		<!--- Create cavas to draw all connection path --->
		StageCanvas = new Raphael(document.getElementById('SBStage'));  
		<!--- Load previous MCID system to stage--->
		LoadStage();
		$("#loading").hide();
	});

	<!--- Global so popup can refernece it to close it--->
	var CreateTemplatePickerDialogVoiceTools = 0;
	
	function TemplatePickerDialogVoiceTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateTemplatePickerDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateTemplatePickerDialogVoiceTools.remove();
			CreateTemplatePickerDialogVoiceTools = 0;
		}
						
		CreateTemplatePickerDialogVoiceTools = $('<div></div>').append($loading.clone());
		var leftVoiceTools = $("#MC_Voice_Container").position().left;
		var topVoiceTools = $("#MC_Voice_Container").position().top;
		CreateTemplatePickerDialogVoiceTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_TemplatePicker' + ParamStr)
			.dialog({
				modal : true,
				title: 'Voice Template Tool',
				width: 1250,
				minHeight: 200,
				height: 'auto',
				//position: 'top' 
				position: ['center',topVoiceTools],
				beforeClose: function(event, ui) {
							$("#MC_PreviewPanel_Voice_Templates").attr("class", "MC_PreviewPanel_Voice");
							$("#mbtemplates").attr("class", "MC_Voice_Templates");
							}
			});
	
		return false;		
	}
	
	<!--- Global so popup can refernece it to close it--->
	var CreateScreenDialogVoiceTools = 0;
	function showDialogScreen(){
	//var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateScreenDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateScreenDialogVoiceTools.remove();
			CreateScreenDialogVoiceTools = 0;
			
		}
		var template = 
		"<div id='list_screen'><ul>"
		+"<li><lable>Width:</lable> <input type='text' id='txtWidthScreen' value=''/></li>"				
		+"<li><lable>Height:</lable> <input type='text' id='txtHeightScreen' value=''/></li>"
		+"</ul>"
		+"</div>";	
		var scale = 1;		
		var val_radio;
		var divWidth;	
		var divHeight;				
		CreateScreenDialogVoiceTools = $('<div>'+template+'</div>');
		
		CreateScreenDialogVoiceTools
			.dialog({
				modal : false,
				title: 'Screen',
				width: 250,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				buttons:
				[
					{
						text: "Save",
						click: function(event)
						{
							divWidth = $('#txtWidthScreen').val();
							divHeight = $('#txtHeightScreen').val(); 
							if(divWidth!=''&&divHeight!=''){
								if((divWidth<600||divHeight<600)||(divWidth>5000||divHeight>5000)){
									alert('Please input width and height in range (600-5000)!');
									return false;
								}
								else{
									$.ajax({
							            type: "POST",
							            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=changeStageSize&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
							    		data:{
								    		 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
								    		 stageWidth : divWidth, 
								    		 stageHeight : divHeight, 
										},
							            dataType: "json", 
							            success: function(d2, textStatus, xhr) {
											sbStageDefaultWidth = divWidth;
				            				sbStageDefaultHeight = divHeight;
				            				$('#txtWidth').val(sbStageDefaultWidth);
				            				$('#txtHeight').val(sbStageDefaultHeight);
				            				$('#SBStage').css('width',parseInt(sbStageDefaultWidth)).css('height',parseInt(sbStageDefaultHeight));
				            				$('#stageContainer').css('width',parseInt(sbStageDefaultWidth)+22).css('height',parseInt(sbStageDefaultHeight) + 20);
											//$('#mainPage').css('min-height', scale * sbStageDefaultHeight + 80);
						         		 }
						   		  	});
					   		  	}
								$('#txtWidth').val(divWidth);
			            		$('#txtHeight').val(divHeight);
			            		//$('#SBStage').css('width',divWidth).css('height',divHeight);
			            		//$('#stageContainer').css('width',parseInt(divWidth) + 20).css('height',parseInt(divHeight) + 20);
								$(this).dialog('destroy'); 
								$(this).remove();
							}	
							else{
								alert('Please input width and height!');
							}
						}
					},
					{
						text: "Cancel",
						click: function(event)
						{
							$(this).dialog('destroy'); 
							$(this).remove();
						}
					}
				],
				close: function() { CreateScreenDialogVoiceTools.remove(); CreateScreenDialogVoiceTools = 0;},
				open: function() {
					$('#txtWidthScreen').val($('#txtWidth').val());
					$('#txtHeightScreen').val($('#txtHeight').val());
					$('#txtWidthScreen').ForceNumericOnly();
					$('#txtHeightScreen').ForceNumericOnly();
					
				}
			});
	
		return false;
	}
	
	<!--- Global so popup can refernece it to close it--->
	var CreateScheduleQAShotDialogVoiceTools = 0;
	
	function ScheduleQAShotDialogVoiceTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateScheduleQAShotDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateScheduleQAShotDialogVoiceTools.remove();
			CreateScheduleQAShotDialogVoiceTools = 0;
			
		}
						
		CreateScheduleQAShotDialogVoiceTools = $('<div></div>').append($loading.clone());
		
		CreateScheduleQAShotDialogVoiceTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_QA' + ParamStr)
			.dialog({
				modal : true,
				title: 'QA Voice Tool',
				width: 640,
				minHeight: 200,
				height: 'auto',
				position: 'top',
				beforeClose: function(event, ui) {
							$("#MC_PreviewPanel_Voice_QA").attr("class", "MC_PreviewPanel_Voice");
							$("#msqa").attr("class", "MC_Voice_QA");
							}
			});
	
		return false;		
	}
	
	<!--- Global so popup can refernece it to close it--->
	var CreateAdvancedBatchOptionsDialogVoiceTools = 0;
	
	function AdvancedBatchOptionsDialogVoiceTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateAdvancedBatchOptionsDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateAdvancedBatchOptionsDialogVoiceTools.remove();
			CreateAdvancedBatchOptionsDialogVoiceTools = 0;
			
		}
						
		CreateAdvancedBatchOptionsDialogVoiceTools = $('<div></div>').append($loading.clone());
		
		CreateAdvancedBatchOptionsDialogVoiceTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/batch/dsp_Advanced' + ParamStr)
			.dialog({
				modal : true,
				close: function() { CreateAdvancedBatchOptionsDialogVoiceTools.remove(); CreateAdvancedBatchOptionsDialogVoiceTools = 0;}, 
				title: 'Advanced Campaign Options',
				width: 640,
				height: 'auto',
				position: 'top',
				beforeClose: function(event, ui) {
							$("#MC_PreviewPanel_Voice_Advanced").attr("class", "MC_PreviewPanel_Voice");
							$("#msadvanced").attr("class", "MC_Voice_Advanced");
							}
			});
		return false;		
	}
	
			
<!---	function XMLContorlStringReviewDialogVoiceTools()
	{				
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		var ParamStr = '';
	
		ParamStr = '?inpbatchid=' + encodeURIComponent(<cfoutput>#INPBATCHID#</cfoutput>);
		
		<!--- Erase any existing dialog data --->
		if(CreateXMLContorlStringReviewDialogVoiceTools != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			CreateXMLContorlStringReviewDialogVoiceTools.remove();
			CreateXMLContorlStringReviewDialogVoiceTools = 0;
			
		}
						
		CreateXMLContorlStringReviewDialogVoiceTools = $('<div></div>').append($loading.clone());
		
		CreateXMLContorlStringReviewDialogVoiceTools
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_XMLControlString' + ParamStr)
			.dialog({
				modal : true,
				close: function() { CreateXMLContorlStringReviewDialogVoiceTools.remove(); CreateXMLContorlStringReviewDialogVoiceTools = 0;}, 
				title: 'XML Contorl String',
				width: 1000,
				height: 'auto',
				position: 'top',
				beforeClose: function(event, ui) { 
								cx_delete_clipboard();
								$("#MC_PreviewPanel_Voice_Advanced").attr("class", "MC_PreviewPanel_Voice");
								$("#msadvanced").attr("class", "MC_Voice_Advanced");
							}
			});
		return false;		
	}--->
	
	
	<!--- Global so popup can refernece it to close it--->
	var CreateScheduleSBDialog = 0;
	
	<!--- Campaign/Batch Schedule --->
	function DoCreateScheduleSBDialog() {
					
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(CreateScheduleSBDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			CreateScheduleSBDialog.remove();
			CreateScheduleSBDialog.dialog('destroy');
			CreateScheduleSBDialog.remove();
			CreateScheduleSBDialog = 0;
			
		}
						
		CreateScheduleSBDialog = $('<div></div>').append($loading.clone());
		
		var ParamStr = '?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>';	
		
		CreateScheduleSBDialog
			<!---.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/schedule/dsp_Schedule4' + ParamStr)--->
			.load('<cfoutput>#rootUrl#/#SessionPath#/campaign/mycampaign/dsp_schedule?inpbatchid=#INPBATCHID#&mode=edit</cfoutput>' + ParamStr)
			.dialog({
				close: function() { $("#htabs_DynamicSchedules").tabs( "destroy" ); CreateScheduleSBDialog.remove(); CreateScheduleSBDialog = 0;}, 
				<!--- show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500}, --->
				modal : true,
				title: 'Schedule Options',
				width: 1000,
				height: 650,
				position: 'top'<!--- ,
				dialogClass: 'noTitleStuff'   --->
			});
	
		return false;		
			
	}
	
		
		
		
	<!--- Global so popup can refernece it to close it--->
	var CreateCollaborationSBDialog = 0;
	
	<!--- Campaign/Batch Collaboration --->
	function DoCreateCollaborationSBDialog() {
					
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
							
		<!--- Erase any existing dialog data --->
		if(CreateCollaborationSBDialog != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log($SelectScriptDialog); --->
			CreateCollaborationSBDialog.remove();
			CreateCollaborationSBDialog = 0;
			
		}
						
		CreateCollaborationSBDialog = $('<div></div>').append($loading.clone());
		
		var ParamStr = '?inpbatchid=<cfoutput>#INPBATCHID#</cfoutput>';	
		
		CreateCollaborationSBDialog
			.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/Rules/dsp_MultiChannelRulesUI3' + ParamStr)
			.dialog({
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
				title: 'Collaboration',
				width: 1500,
				height: 1140,
				position: 'top',
				dialogClass: 'noTitleStuff'  
			});
	
		return false;		
			
	}
	
	function CloseEditScheduleDialog() {
		CreateScheduleSBDialog.remove();
		CreateScheduleSBDialog = 0;
	}

		
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Function to read in exisitng data and layout --->
	<!--- Default layout is CURRent plus 1 --->

	<!--- Local xml only at first --->
	function CreateNewMCID(inpRXT, inpObj, inpXPOS, inpYPOS) {
		$("#loading").show();
		<!--- get max QID --->
            <!----<cfset LOCALOUTPUT.RememberMe = Decrypt(
            COOKIE.RememberMe,
            APPLICATION.EncryptionKey_Cookies,
            "cfmx_compat",
            "hex"
            ) />
            
            <!---
            For security purposes, we tried to obfuscate the way the ID was stored. We wrapped it in the middle
            of list. Extract it from the list.
            --->
            <cfset CurrRememberMe = ListGetAt(
            LOCALOUTPUT.RememberMe,
            3,
            ":"
            ) />
			<cfif IsNumeric( CurrRememberMe )>
				<cfset Session.USERID = CurrRememberMe /> 
			</cfif>--->
			var i,x,y,ARRcookies=document.cookie.split(";");

		var maxNewQID = FindNewQID(QID_ARRAY);
		<!--- Read CURRent RXSS XML and pass in  --->
	 		$.ajax({
 				  type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
				  url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=AddNewQID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
				  datatype: 'json',
				  data:  { 
				  	INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				  	inpMCIDTYPE : inpRXT, 
				  	inpXPOS: inpXPOS, 
				  	inpYPOS : inpYPOS, 
				  	maxQID : maxNewQID 
				  },
				  error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
				  success:
						<!--- Default return function for Do CFTE Demo - Async call back --->
						function(d2, textStatus, xhr ) 
						{
							<!--- .ajax is using POST instead of GET returning an XMLHttpRequest as the result - get the json string and convert to object for parsing (need to make your own 'd' object out of JSON string) --->
							var d = eval('(' + xhr.responseText + ')');
							var INPQID = 0;
							
							<!--- Alert if failure --->
								<!--- Get row 1 of results if exisits--->
								if (d.ROWCOUNT > 0) 
								{	
									// alert(d.DATA.DEBUGVAR1[0])
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											if(typeof(d.DATA.NEXTQID[0]) != "undefined")
											{
												INPQID = d.DATA.NEXTQID[0];
												maxQID = INPQID;
											}

											ModifyStageObjectAfterQID(inpObj, inpRXT, INPQID, 'Description Not Specified');	
											QID_ARRAY.push(INPQID);
											$('#clearBtn').removeClass().addClass('active');
											$('#sortBtn').removeClass().addClass('active');
										}

										$("#loading").hide();
									}
									else
									{<!--- Invalid structure returned --->
										$("#loading").hide();
									}
									LoadStage();
								}
								else
								{<!--- No result returned --->
									<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->
									$.alerts.okButton = '&nbsp;OK&nbsp;';
									jAlert("Error.", "Invalid Response from the remote server.");
								}

								<!--- show local menu options - re-show regardless of success or failure --->
								<!--- $("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).show(); --->

								<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
								<!--- $("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html(''); --->
						}
			} );

		return false;
	}


	function FindNewQID(QID_ARRAY) {
		var tmp = 0;
		if (is_array(QID_ARRAY)) {
			for(var i=0; i<QID_ARRAY.length; i++) {
				if (tmp < QID_ARRAY[i]) { tmp = QID_ARRAY[i]}
			}
		}
		return tmp;
	}
	function is_array(input){
	  return typeof(input)=='object'&&(input instanceof Array);
	}
	function delete_array(input,value) {
		var output = new Array();
		if (is_array(input)) {
			for(var i=0; i<input.length; i++) {
				if (value != input[i]) { output.push(input[i])}
			}
		}
		return output;
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Set obj color event to draw connection --->
	function BindToBallObject(INPQID, LINKObj, XPOS, YPOS, SidePos, inpObjColor)
	{
		<!--- Add draggable to default handle--->
		$('#QID_' + INPQID + ' .' + LINKObj).draggable(
		{
			containment: "#SBStage",
			revert: true,
			revertDuration: 100 
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		$('#QID_' + INPQID + ' .' + LINKObj).bind( "drag", function(event, ui) 
		{
			LINKIVRObjects(LINKObj, INPQID, -1, ui.position.left, ui.position.top, 1, XPOS, YPOS, SidePos, inpObjColor);
		});

		<!--- Erase last line after stop --->
		$('#QID_' + INPQID + ' .' + LINKObj).bind( "dragstop", function(event, ui) 
		{
			$('#QID_' + INPQID).draggable( "option", "disabled", false );
			<!--- DynamicLINKs is 0 for erase TempObj Line only--->
			LINKIVRObjects(LINKObj, INPQID, -1, -1, -1, 0, XPOS, YPOS, SidePos);
		});

		<!--- Erase last line after stop --->
		$('#QID_' + INPQID + ' .' + LINKObj).bind( "dragstart", function(event, ui) 
		{
			$('#QID_' + INPQID).draggable( "option", "disabled", true );
		});

		<!--- jquery bug fix / kludge - nest draggables grab all in IE--->
		$('#QID_' + INPQID + ' .' + LINKObj ).mousedown(function(e)
		{
			if($.browser.msie) {
				 e.stopPropagation();
			}
		});
	}

	function savexml(xmlstring) {
		var output = '';
		$.ajax({
				type:"POST",
				url:"<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=UpdateXMLMCID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				data:{
					INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
					XMLSTRING : xmlstring
				},
	            dataType: "json", 
	            async: false,
	            success: function(d2, textStatus, xhr) {
	            	var d = eval('(' + xhr.responseText + ')');
	            	//$FrmPaste.dialog('destroy');
					//$FrmPaste.remove();
					output = d.DATA.XMLSTRING[0];
					
	            }
			});
		if (output) {
			return output;
		}	
		return false;
	}
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Open Control form when click on QID number --->
	var $CreateEditMCIDPopup = 0;
	function editMCID(inpObj)
	{
		<!--- Get the MCID type --->
		var inpRXT = 0;

		<!--- Get the QID --->
		var INPQID = 0;
		inpRXT = inpObj.parent().data('RXT')
		INPQID = inpObj.parent().data('QID')
		<!--- Save selected QID to change --->
		curSelectQID = INPQID;

		var CURRForm = "<form id='RXTEditForm_" + INPQID + "' method='post' action=''>"

		var CURRTemplate = "";
		<!--- Switch based on RX type ID --->
		switch(parseInt(inpRXT))
		{
			case 1:
				CURRTemplate = $("#RXT1Template").html();
				break;

			case 2:
				CURRTemplate = $("#RXT2Template").html();
				break;
				
			case 3:
				CURRTemplate = $("#RXT3Template").html();
				break;

			case 4:
				CURRTemplate = $("#RXT4Template").html();
				break;

			case 5:
				CURRTemplate = $("#RXT5Template").html();
				break;

			case 6:
				CURRTemplate = $("#RXT6Template").html();
				break;

			case 7:
				CURRTemplate = $("#RXT7Template").html();
				break;
				
			<!---	Deprecated
			case 8:
				CURRTemplate = $("#RXT8Template").html();
				break;
			--->
			
			case 9:
				CURRTemplate = $("#RXT9Template").html();
				break;

			case 10:
				CURRTemplate = $("#RXT10Template").html();
				break;

			case 11:
				CURRTemplate = $("#RXT11Template").html();
				break;

			case 12:
				CURRTemplate = $("#RXT12Template").html();
				break;

			case 13:
				CURRTemplate = $("#RXT13Template").html();
				break;

			case 14:
				CURRTemplate = $("#RXT14Template").html();
				break;

			case 15:
				CURRTemplate = $("#RXT15Template").html();
				break;

			case 16:
				CURRTemplate = $("#RXT16Template").html();
				break;

			case 17:
				CURRTemplate = $("#RXT17Template").html();
				break;

			case 18:
				CURRTemplate = $("#RXT18Template").html();
				break;

			case 19:
				CURRTemplate = $("#RXT19Template").html();
				break;

			case 20:
				CURRTemplate = $("#RXT20Template").html();
				break;

			case 21:
				CURRTemplate = $("#RXT21Template").html();
				break;
				
			case 22:
				CURRTemplate = $("#RXT22Template").html();
				break;
			case 23:
				CURRTemplate = $("#RXT23Template").html();
				break;
			case 24:
				CURRTemplate = $("#RXT24Template").html();
				break;
			default:
				CURRTemplate = $("#RXT1Template").html();
				break;
		}

		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		$CreateEditMCIDPopup = $('<div></div>').append($loading.clone());
		$CreateEditMCIDPopup = $('<div></div>').append("<div id='EditMCIDForm_" + INPQID + "' style='min-width:600px' class='StageObjectEditDiv StageObjectEditDiv_type_"+inpRXT+"'>" + CURRForm + CURRTemplate + "</form></div>");

		return $CreateEditMCIDPopup;
	}
	
	function closePopupWindow(inpObj, INPQID){
	    $('#popup_editMCID_x_' +INPQID).remove(); 
		$(".popupMenu").html('');
		hideObjectGlow(inpObj);
	}
	<!--- right flash player ---->
	
	function playerflash(playId, container) {
		var width = 200;
		if(typeof(container) == 'undefined'){
			container = "flashplayer";
			width = 400;
		}
		
		var flashvars = {
			file : "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/rxds/get-script.cfm?id="+playId			
		};
		var params = {
		 	allowfullscreen:      'true', 
		 	allowscriptaccess:    'always',
		 	wmode:                'transparent'
		};
		var attributes = {
			id: container, 
			name: container
		};
  		swfobject.embedSWF('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/account/singleplayer_edit.swf', container, width, '27', '9.0.124', false, flashvars, params, attributes);

	}

	<!--- Swap 2 Qids --->
	function SwapQID(startQid, stopQid)
	{
		$("#loading").show();
		if (startQid != stopQid)
		{
			<!--- Check selection to change QID --->
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=SwapQID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    		 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    		 inpStartQID : startQid,
    		 inpEndQID : stopQid
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
         				if (d.ROWCOUNT > 0)
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								CURRRXResultCode = d.DATA.RXRESULTCODE[0];	
								$("#loading").hide();
								LoadStage();
							}
							else
							{
								<!--- Invalid structure returned --->
								$("#loading").hide();
							}
						}
						else
						{
							<!--- No result returned --->
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}
         		 }
   		  });
		
		}
		$("#loading").hide();
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	var SelectScriptDialogVar = 0;
	function SelectScriptDialog(INPQID,multi,inpLibId, inpEleId, inpDataId, inpPlayId)
	{
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		
		if(INPQID == 0)
		{
			<!--- Either use input values from context menu or try to get from local form --->
			if(typeof(inpLibId) == 'undefined')
				if(typeof($("#RXTvmdmEditForm #inpLibId").val()) == 'undefined')
					inpLibId = 0
				else
					inpLibId= + $("#RXTvmdmEditForm #inpLibId").val() 

			if(typeof(inpEleId) == 'undefined')
				if(typeof($("#RXTvmdmEditForm #inpEleId").val()) == 'undefined')
					inpEleId = 0
				else
					inpEleId= + $("#RXTvmdmEditForm #inpEleId").val() 

			if(typeof(inpDataId) == 'undefined')
				if(typeof($("#RXTvmdmEditForm #inpDataId").val()) == 'undefined')
					inpDataId = 0
				else
					inpDataId= + $("#RXTvmdmEditForm #inpDataId").val()
					
			if(typeof(inpPlayId) == 'undefined')
				if(typeof($("#RXTvmdmEditForm #inpPlayId").val()) == 'undefined')
					inpPlayId = 0
				else
					inpPlayId= + $("#RXTvmdmEditForm #inpPlayId").val() 
		}
		else
		{
			if(typeof(multi) !== 'undefined') {
				<!--- Either use input values from context menu or try to get from local form --->
				if(typeof(inpLibId) == 'undefined')
					if(typeof($("#RXTEditForm_" + INPQID + " #inpLibId").val()) == 'undefined')
						inpLibId = 0
					else
						inpLibId= + $("#RXTEditForm_" + INPQID + " #inpLibId").val() 
	
				if(typeof(inpEleId) == 'undefined')
					if(typeof($("#RXTEditForm_" + INPQID + " #inpEleId").val()) == 'undefined')
						inpEleId = 0
					else
						inpEleId= + $("#RXTEditForm_" + INPQID + " #inpEleId").val() 
	
				if(typeof(inpDataId) == 'undefined')
					if(typeof($("#RXTEditForm_" + INPQID + " #inpDataId").val()) == 'undefined')
						inpDataId = 0
					else
						inpDataId= + $("#RXTEditForm_" + INPQID + " #inpDataId").val() 
						
				if(typeof(inpPlayId) == 'undefined')
					if(typeof($("#RXTEditForm_" + INPQID + " #inpPlayId").val()) == 'undefined')
						inpPlayId = 0
					else
						inpPlayId = $("#RXTEditForm_" + INPQID + " #inpPlayId").val() 
			} else {
				inpLibId = 0;
				inpEleId = 0;
				inpDataId = 0;
				inpPlayId = 0;
			}
		}
		
		<!--- Change script library then stop play movie ---->
		if (inpPlayId != 0) {
			//getFlashMovieObject('flashplayer').stopSound();
		}
		
		<!--- 
		<!--- Validate file is CURRently specified/selected --->
		if(inpLibId == 0 || inpEleId == 0 || inpDataId == 0)
		{
			jAlert("Please select a Script first! No Script has been CURRently defined.", "Problem Playing Script");
			return false;	
		}			
		 --->
		 
		if(SelectScriptDialogVar != 0)
		{
			<!--- Cleanup and remove old stuff or tree won't work on second attempt to open same dialog --->
			<!--- console.log(SelectScriptDialogVar); --->
			SelectScriptDialogVar.remove();
		}

		SelectScriptDialogVar = $('<div></div>').append($loading.clone());
		SelectScriptDialogVar
		<!--- disable by tranglt --->
		<!---.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_SelectScript?inpSelectScript=1&inpLibId=' + inpLibId + '&inpEleId=' + inpEleId + '&inpDataId=' + inpDataId + '&INPQID=' + INPQID+ '&inpPlayId='+inpPlayId, function() { ScriptSelect_DelayedLoad();} )--->
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_SelectScript?inpSelectScript=1&inpLibId=' + inpLibId + '&inpEleId=' + inpEleId + '&inpDataId=' + inpDataId + '&INPQID=' + INPQID+'&IsMulti='+ multi+ '&inpPlayId='+inpPlayId, function() {} )
			.dialog({
				modal : true,
				title: 'Select Script',
				
				width: 750,
				height: 560,
				close: function() {
					$(this).dialog('destroy'); 
					$(this).remove(); 
					SelectScriptDialogVar = 0;
				} 
			});
		return false;
	}
	//Script picker tool (location key is used in case switch form)
	function SelectScriptOneLibDialog(INPQID,multi,locationKey) {
		var $loading = $('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Preparing');
		var inpLibId = 0;
		var	inpEleId = 0;
		var	inpDataId = 0;
		var	inpPlayId = 0;
		
		if(INPQID == 0)
		{
			<!--- Either use input values from context menu or try to get from local form --->
			if(typeof($("#RXTvmdmEditForm #inpLibId").val()) == 'undefined')
				inpLibId = 0;
			else
				inpLibId= + $("#RXTvmdmEditForm #inpLibId").val(); 

			if(typeof($("#RXTvmdmEditForm #inpEleId").val()) == 'undefined')
				inpEleId = 0;
			else
				inpEleId= + $("#RXTvmdmEditForm #inpEleId").val(); 

			if(typeof($("#RXTvmdmEditForm #inpDataId").val()) == 'undefined')
				inpDataId = 0;
			else
				inpDataId= + $("#RXTvmdmEditForm #inpDataId").val();
				
			if(typeof($("#RXTvmdmEditForm #inpPlayId").val()) == 'undefined')
				inpPlayId = 0;
			else
				inpPlayId= + $("#RXTvmdmEditForm #inpPlayId").val(); 
		}
		else
		{
			if(multi != 0 && multi != 3) {
				<!--- Either use input values from context menu or try to get from local form --->
				if(typeof($("#RXTEditForm_" + INPQID + " #inpLibId").val()) == 'undefined')
					inpLibId = 0;
				else
					inpLibId= + $("#RXTEditForm_" + INPQID + " #inpLibId").val(); 
				if(typeof($("#RXTEditForm_" + INPQID + " #inpEleId").val()) == 'undefined')
					inpEleId = 0;
				else
					inpEleId= + $("#RXTEditForm_" + INPQID + " #inpEleId").val();
	
				if(typeof($("#RXTEditForm_" + INPQID + " #inpDataId").val()) == 'undefined')
					inpDataId = 0;
				else
					inpDataId= + $("#RXTEditForm_" + INPQID + " #inpDataId").val(); 
					
				if(typeof($("#RXTEditForm_" + INPQID + " #inpPlayId").val()) == 'undefined')
					inpPlayId = 0;
				else
					inpPlayId = $("#RXTEditForm_" + INPQID + " #inpPlayId").val(); 
			} else {
				inpLibId = 0;
				inpEleId = 0;
				inpDataId = 0;
				inpPlayId = 0;
			}
		}
		
		if(SelectScriptDialogVar != 0)
		{
			SelectScriptDialogVar.remove();
		}

		SelectScriptDialogVar = $('<div id="selectScriptLibDialog"></div>').append($loading.clone());
		SelectScriptDialogVar
		.load('<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/dsp_selectscriptonelib?inpSelectScript=1&INPQID=' + INPQID+'&IsMulti='+ multi+ '&inpPlayId='+inpPlayId + '&locationKey=' +locationKey, function() {} )
			.dialog({
				modal : true,
				title: 'Select Script',				
				width: 530,
				height: 850,
				close: function() {
					$(this).remove(); 
					SelectScriptDialogVar = 0;
				} 
			});
		return false;
	}
	<!--- Get flash movie object  --->
	function getFlashMovieObject(movieName)
	   {
	     if (window.document[movieName]) 
	     {
	      return window.document[movieName];
	     }
	     if (navigator.appName.indexOf("Microsoft Internet")==-1)
	     {
	    if (document.embeds && document.embeds[movieName])
	      return document.embeds[movieName]; 
	     }
	     else // if (navigator.appName.indexOf("Microsoft Internet")!=-1)
	     {
	    return document.getElementById(movieName);
	     }  
	   }
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>	
	<!--- Output an XML RXSS String based on form values --->
	function WriteRXSSXMLString(INPQID, inpRXT)
	{			
		<!--- AJAX/JSON Do CFTE Demo --->
		<!--- Let the user know somthing is processing --->
		//$("#CURRentRXTXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	

		var LINKObj2 = -1;
		//change get postion x,y by get top,left of image MCID obj(if use data in form sometime get incorrect position )
		var XPos=$('#SBStage #QID_'+INPQID).position().left;
		var YPos=$('#SBStage #QID_'+ INPQID).position().top;
        $("#RXTEditForm_" + INPQID + " #inpX").val(XPos);
		$("#RXTEditForm_" + INPQID + " #inpY").val(parseInt(YPos));
		switch(parseInt(inpRXT))
		{
			case 1:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT1.cfm">
				break;
			case 2:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT2.cfm">
				break;
			case 3:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK8").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT3.cfm">
				break;
			case 4:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT4.cfm">
				break;
			case 5:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT5.cfm">
				break;
			case 6:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT6.cfm">
				break;
			case 7:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT7.cfm">
				break;
			<!--- Deprecated
			case 8:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT8.cfm">
				break;	
			--->
			case 9:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT9.cfm">
				break;
			case 10:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT10.cfm">
				break;
			case 11:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT11.cfm">
				break;
			case 12:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT12.cfm">
				break;
			case 13:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK15").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT13.cfm">
				break;
			case 14:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT14.cfm">
				break;
			
			case 15:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT15.cfm">
				break;
			case 16:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT16.cfm">
				break;	
			case 17:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT17.cfm">
				break;
			
			case 18:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT18.cfm">
				break;
			
			case 19:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT19.cfm">
				break;
			
			case 20:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT20.cfm">
				break;
			
			case 21:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK1").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT21.cfm">
				break;
			
			case 22:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT22.cfm">
				break;
			case 23:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_DM.cfm">
				break;
		    case 24:
				<!--- Update link info before saving--->
				LINKObj2 = $("#RXTEditForm_" + INPQID + " #inpCK5").val();
				$("#RXTEditForm_" + INPQID + " #inpLINKTo").val(LINKObj2);
				<cfinclude template="js/RXT/js_write_RXT24.cfm">
				break;
			default:
				break;
		}
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Save RXT change information --->
	function SaveChangesMCID(INPQID, inpTYPE, inpFormObjName, CloseEdit)
	{
		<!--- Hide local menu options --->
		$(inpFormObjName + " #RXEditSubMenu_" + INPQID).hide();
		<!--- Let user know something is being attempted --->
		$(inpFormObjName + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Saving');
		$('#QID_' + INPQID).data("CK2_LOCATION","");
		 $.ajax({
	            type: "POST",
	            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=SaveMCIDObj&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
	    		data:{
			    		INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
						INPQID : INPQID, 
						inpTYPE : inpTYPE, 
						inpXML : $(inpFormObjName + " #inpXML").val(),
			    },
	            dataType: "json", 
	            success: function(d2, textStatus, xhr) {
	            	<!--- Optionally close edit window when complete --->
					if(CloseEdit > 0)
						<!--- Kill the MCID edit dialog --->
						$CreateEditMCIDPopup.remove();	
					var LINKObj2 = -1;
					<!--- Switch based on RX type ID --->
					switch(parseInt($(inpFormObjName + " #inpRXT").val()))
					{
						case 3:
							LINKObj2 = $(inpFormObjName + " #inpCK8").val();
							break;
						case 13:
							LINKObj2 = $(inpFormObjName + " #inpCK15").val();
							break;
						case 1:
						case 2:
						case 4:
						case 5:
						case 6:
						case 7:
						case 9:
						case 10:
						case 11:
						case 12:
						case 14:
						case 15:
						case 16:
						case 17:
						case 18:
						<!--- tranglt add two form --->
						case 19:
						case 20:
						case 22:
						case 24:
							LINKObj2 = $(inpFormObjName + " #inpCK5").val();
							break;
						case 21:
						default:
							LINKObj2 = -1;
							break;
					}
	
					<!--- Update link line(s) --->
					<!---LINKIVRObjects('ObjDefBall', INPQID, LINKObj2, -1, -1, -1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor);--->
					closePopupWindow($('#QID_' + INPQID), INPQID);
					<!--- Refresh view --->
					LoadStage();
					$("#loading").hide();
	          	}
    	 });
	}
	<!--- draw  UI build script represent graphically --->
   function displayImgBS(INPQID){
   	 $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ReadRXTXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    		    INPQID : INPQID
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
							<!--- Get row 1 of results if exisits--->
							if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{
											<!--- Check if variable is part of JSON result string --->
											if(typeof(d.DATA.CURRRXT[0]) != "undefined")
											{
												
												var libId;
												<!--- Switch based on RX type ID --->
												switch(d.DATA.CURRRXT[0])
												{
													case 1:
													case 2:									
													case 4:	
													case 6:
													case 18:
													case 23:
											        var i=0;
												    var divBS=$("<DIV>");
												    divBS.attr("id",'BSS_' + INPQID);
												    divBS.attr("style",'display:none');
												    var ulBS=$("<UL>");
												   <!---  list unorder all elememnt in MCID : swtich(child element) and element other --->
												    ulBS.attr('style','list-style-type: none');
												    <!---  draw one switch build script with muiltiple child element  --->
												    <!---  use for set padding top all element outside switch --->
												    var padTop=10;
													<!--- draw for all ELE outside switch --->
										          	if(typeof(d.DATA.MULTISCRIPT[0]) != "undefined"){	
												      libId=d.DATA.CURRLIBID[0];											      
												      //calcute margin-top if muiltiple switch
												        var marginTop;
												       	for(index in d.DATA.MULTISCRIPT[0])
												        {   
												        	var currData = d.DATA.MULTISCRIPT[0][index];
												        	//single element inside ELE 
												        	var indexEleInSwitch=0;
												        	if( typeof(currData.DESC) != 'undefined'){	    
													        	eleId = d.DATA.MULTISCRIPT[0][index].ELEID;
															    eleDesc = d.DATA.MULTISCRIPT[0][index].DESC;
															    eleScriptId = d.DATA.MULTISCRIPT[0][index].SCRIPTID;
															    //draw single element and append it to list build script
															    var ele=$("<LI>");
	                                                            ele.attr('style','float:left;padding-left:10px;padding-top:'+padTop+'px;position:relative;right:15px;');
											    	      	    drawSingleElement(libId,eleId,eleDesc,eleScriptId).appendTo(ele);     
											    	      	    ele.appendTo(ulBS);
												        	}
												        	//SWITCH SCRIPT inside ELE
												        	else{
												        	switchEle=currData.ARRCASE.length;
												            var bottomSwitch=2+12*(switchEle-2);											        	
                                                            var eleS=$("<LI>");
											    	      	eleS.attr('style','float:left'); 
											    	      	var bsswitch=$("<DIV>");
											    	        bsswitch.attr('style','position:relative;right: 20px;');
											    	        var ul=$("<UL>");
											    	      	ul.attr('style','list-style-type: none;display:inline-block');
											    	      	if(typeof(currData.ARRCASE[0]) != 'undefined'){
											    	      	     var eleId = currData.ARRCASE[0].ELEID;
																 var scriptId = currData.ARRCASE[0].SCRIPTID;
																 var desc = currData.ARRCASE[0].DESC;
															}
											    	      	if(switchEle == 1){
											    	      	     var li=$('<LI>');
											    	      	     padTop=12;
											    	      	   	 li.attr('style','background:url(../campaign/mcid/images/one.png)  no-repeat scroll center top transparent;padding-left:110px;padding-top:'+padTop+'px;margin-top:'+marginTop+'px;');
											    	      	     drawSingleElement(libId,eleId,desc,scriptId).appendTo(li);
											    	      	     li.appendTo(ul); 
											    	      	} else if(switchEle > 1){		    	      
											    	      	   	<!---  set padding top for all element not in switch --->
											    	      	   	 padTop=12*(switchEle -1)-2;
											    	      	     var img=$("<IMG>");
											    	      	     img.attr('src','../campaign/mcid/images/switch_icon.png');
											    	      	     var divimgs=$("<DIV>");
											    	      	     divimgs.attr('style','display:inline-block;position:relative;left:25px;bottom:'+bottomSwitch+'px');
											    	      	     img.appendTo(divimgs);
											    	      	     divimgs.appendTo(bsswitch);
											    	      	   	 var li=$('<LI>');
											    	      	   	 li.attr('style','background:url(../campaign/mcid/images/arrow1.png)  no-repeat scroll center top transparent;padding-left:50px;');
											    	      	     drawSingleElement(libId,eleId,desc,scriptId).appendTo(li);
											    	      	     li.appendTo(ul); 											    	      	     
											    	      	     for(e=2;e < switchEle;e++ ){
											    	      	    	 indexEleInSwitch++;
														     	     eleId =currData.ARRCASE[indexEleInSwitch].ELEID;
														    		 desc = currData.ARRCASE[indexEleInSwitch].DESC;
														     		 scriptId = currData.ARRCASE[indexEleInSwitch].SCRIPTID;
											    	      	         li=$("<LI>");
											    	      	         li.attr('style','background:url(../campaign/mcid/images/arrow2.png)  no-repeat scroll center center transparent;padding-left:50px;');
											    	      	   
											    	      	         drawSingleElement(libId,eleId,desc,scriptId).appendTo(li);
											    	      	         li.appendTo(ul); 
											    	      	    };
											    	      	   <!---     get data of last element in switch --->
											    	      	    indexEleInSwitch++;
																eleId = currData.ARRCASE[indexEleInSwitch].ELEID;
																desc =  currData.ARRCASE[indexEleInSwitch].DESC;
																scriptId =  currData.ARRCASE[indexEleInSwitch].SCRIPTID;
											    	      	    li=$("<LI>");
											    	      	    li.attr('style','background:url(../campaign/mcid/images/arrow3.png)  no-repeat scroll bottom center transparent;padding-left:50px');
											    	      	    drawSingleElement(libId,eleId,desc,scriptId).appendTo(li);
											    	      	    li.appendTo(ul); 
											    	      	   }
									                           ul.appendTo(bsswitch);										    	      	   
											    	      	   bsswitch.appendTo(eleS)
											    	      	   eleS.appendTo(ulBS);    	  
												        	}	
												        	indexEleInSwitch=0;									
											        	}
											     	}	     	
											     	ulBS.appendTo(divBS);
											        $("#SBStage #QID_"+INPQID).append(divBS);
													default:
														break;
												}
												if($("#BSS_" + INPQID).css('display') == 'none'){
												   $("#BSS_" + INPQID).css('display',''); 				   		
											    }else{
							     	         	   $("#BSS_" + INPQID).css('display','none');
											    }
											}
									}
								}
							}
          }
     });
  }
       //draw sigle image ELE
    function drawSingleElement(libId,eleId,eleDesc,eleScriptId){
        var fullScript =  <cfoutput>#SESSION.UserID#</cfoutput> + '_'+ libId + '_' + eleId + '_' + eleScriptId;
		var imgEle=$("<IMG title=\""+eleDesc+"\">");
		if(eleScriptId == 0){
	  	   	  imgEle.attr('src','../campaign/mcid/images/menuitem_noplay.png');
	  	}else if(eleId == 'TTS'){
	  	   	  imgEle.attr('src','../campaign/mcid/images/menuitem_asr.png');
	    }else{
	    	if(typeof(eleId) == 'undefined'){
				imgEle.attr('src','../campaign/mcid/images/menuitem_switching.png');
	    	} else {
	    		imgEle.attr('src','../campaign/mcid/images/menuitem_play.png');
	  	   	  	imgEle.attr('onclick',"showPopup('"+fullScript+"','"+eleScriptId+"');");
	    	}
	    }
	  	imgEle.attr('style','height:20px;cursor:pointer');
	    return imgEle;								    	      	
    }
    //add image BS to MCID obj when MCID has script in it
    //flagDynamic :0(Static Script) :1((Dynamic Script) :2(TTS))
    function showHideImgBS(INPRXT,INPQID,flagDynamic){
    	if(flagDynamic == 1){
    		  //add image BS to MCID target object
              switch(INPRXT){
              	case 1:
		         	$("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="showBS_' + INPQID + '" title="Dynamic Script" align="center" style="top: 60px; left: 5px;">' + 'DS' + '</div>');
		        	break;
		        case 2:
		         	$("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="showBS_' + INPQID + '" title="Dynamic Script" align="center" style="top:127px; left: 2px;">' + 'DS' + '</div>');
		        	break;
		        case 4:
		        case 6:
		        case 18:
		        case 23:
		            $("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="showBS_' + INPQID + '" title="Dynamic Script" align="center" style="top:117px; left: 2px;">' + 'DS' + '</div>');
		      		break;
		      	default:
		      	    break;
		      }
		      $("#showBS_" + INPQID).click(function(){
				  displayImgBS(INPQID);
			  });
    	}else{
    		//need indicator static script or TTS
    		  var titleDS='S';
    		  var toolTip = "Static Script";
    		  if(flagDynamic == 2){
    		   	  titleDS ='TTS';
    		   	  toolTip = "TTS Script";
    		  }
    	  	  switch(INPRXT){
              	case 1:
		            $("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="staticBS_' + INPQID + '" title="'+ toolTip +'" align="center" style="top: 60px; left: 5px;cursor:default">' + titleDS + '</div>');
		        	break;
		        case  2:
		         $("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="staticBS_' + INPQID + '" title="'+ toolTip +'" align="center" style="top:127px; left: 2px;cursor:default">' + titleDS + '</div>');
		        	break;
		        case 4:
		        case 6:
		        case 18:
		        case 23:
		         	$("#SBStage #QID_"+INPQID).append('<div class="MCIDToken" id="staticBS_' + INPQID + '" title="'+ toolTip +'" align="center" style="top:117px; left: 2px;cursor:default">' + titleDS + '</div>');
		      	 	break;
		      	default:
		      	    break;
		      }  	
    	}
    }
    <!--- Show popup play build script  --->
    function showPopup(fullScript, scriptId){
    	var scriptFlashPopup = '<div id="scriptFlashPopup" style="padding:10px">Blank Script</div>';
    	scriptFlashPopup = $('<div></div>').append(scriptFlashPopup);
	 	scriptFlashPopup
			.dialog({
				modal : true,
				title: 'Play Script',
				zIndex : 99999,
				width : 200,
				height : 70,
				close: function() {
					$(this).dialog('destroy'); 
					$(this).remove(); 
					scriptFlashPopup = 0;
					} 
			});
			if(scriptId != 0){
				playerflash(fullScript, 'scriptFlashPopup');
			}
			
    }
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Read data into display based on given INPQID --->
	function ReadRXTData(INPQID, CloseEdit, OpenEditDialog)
	{	
		
		<!--- Let the user know somthing is processing --->

		<!--- Hide local menu options --->
		<!---$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).hide();--->

		<!--- Let user know something is being attempted --->
		if(CloseEdit > 0)
			$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Canceling');
		else
			$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="16" height="16">...Loading');
        
	   $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ReadRXTXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			INPQID : INPQID
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		<!--- Get row 1 of results if exisits--->
            			if (d.ROWCOUNT > 0) 
							 {
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{											
									
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{						
											<!--- Check if variable is part of JSON result string --->
											if(typeof(d.DATA.CURRRXT[0]) != "undefined")
											{
												<!--- Switch based on RX type ID --->
												switch(parseInt(d.DATA.CURRRXT[0]))
												{
													case 1:
														<cfinclude template="js/RXT/js_read_RXT1.cfm">
														break;
													case 2:
														<cfinclude template="js/RXT/js_read_RXT2.cfm">
														break;
													case 3:
														<cfinclude template="js/RXT/js_read_RXT3.cfm">
														break;
													case 4:
														<cfinclude template="js/RXT/js_read_RXT4.cfm">
														break;
													case 5:
														<cfinclude template="js/RXT/js_read_RXT5.cfm">
														break;
													case 6:
														<cfinclude template="js/RXT/js_read_RXT6.cfm">
														break;
													case 7:
														<cfinclude template="js/RXT/js_read_RXT7.cfm">
														break;
													<!---	
													case 8:
														<cfinclude template="js/RXT/js_read_RXT8.cfm">
														break;
													--->
													
													case 9:
														<cfinclude template="js/RXT/js_read_RXT9.cfm">
														break;
													case 10:
														<cfinclude template="js/RXT/js_read_RXT10.cfm">
														break;
													case 11:
														<cfinclude template="js/RXT/js_read_RXT11.cfm">
														break;
													case 12:
														<cfinclude template="js/RXT/js_read_RXT12.cfm">
														break;
													case 13:
														<cfinclude template="js/RXT/js_read_RXT13.cfm">
														break;
													case 14:
														<cfinclude template="js/RXT/js_read_RXT14.cfm">
														break;
													case 15:
														<cfinclude template="js/RXT/js_read_RXT15.cfm">
														break;
													case 16:
														<cfinclude template="js/RXT/js_read_RXT16.cfm">
														break;
													case 17:
														<cfinclude template="js/RXT/js_read_RXT17.cfm">
														break;
													case 18:
														<cfinclude template="js/RXT/js_read_RXT18.cfm">
														break;
													case 19:
														<cfinclude template="js/RXT/js_read_RXT19.cfm">
														break;
													case 20:
														<cfinclude template="js/RXT/js_read_RXT20.cfm">
														break;
													case 21:
														<cfinclude template="js/RXT/js_read_RXT21.cfm">
														break;
													case 22:
														<cfinclude template="js/RXT/js_read_RXT22.cfm">
														break;
													case 23:
														<cfinclude template="js/RXT/js_read_DM.cfm">
													    break;
													case 24:
														<cfinclude template="js/RXT/js_read_RXT24.cfm">
														break;
													default:
														break;
												}
												var RXT = d.DATA.CURRRXT[0];
												if(RXT == 1 || RXT == 2|| RXT == 4|| RXT == 6 ||  RXT == 22 || RXT == 23){
													var inpLibCheck = $("#RXTEditForm_" + INPQID + " #inpLibId").val();
													if(inpLibCheck != 0){
														$("#RXTEditForm_" + INPQID + " #changeScript").css("display","");
													}
												}

													<!--- Update CURRent position info --->
													$("#RXTEditForm_" + INPQID + " #inpX").val($('#QID_' + INPQID).data('XPOS'));
													$("#RXTEditForm_" + INPQID + " #inpY").val($('#QID_' + INPQID).data('YPOS'));
													$("#RXTEditForm_" + INPQID + " #inpLINKTo").val($('#QID_' + INPQID).data('LINKTo'));

											}
									}
								}
								else
								{<!--- Invalid structure returned --->	
									$("#RXTEditForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - Invalid structure");
								}
							}
							else
							{<!--- No result returned --->
								$("#RXTEditForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");
							}

							<!--- show local menu options - re-show regardless of success or failure --->
							$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).show();
							
							<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
							$("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html('');
          }
       });
	}


	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>

	function DeleteMCIDRXSS(INPQID, inpObj)
	{
		
		//check edit campaign permission
		if(!editCampaignPermission){
			alert(permissionMessage);
			return false;
		}
		
		$("#loading").show();		
		//QID_ARRAY.splice(INPQID,1);
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		

		$("#QID_"+INPQID+" .imgObjDelete").removeAttr('src');
		$("#QID_"+INPQID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_hover.png');
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(" Are you sure you want to delete QID (" + INPQID + ")? This change can NOT be undone!", "Last chance to cancel this delete!",function(result) { 
			<!--- Only finish delete if  --->
			if(result)
			{	       			
       			
				closePopupWindow($('#QID_' + INPQID), INPQID);
				//$('.ui-widget-overlay').hide();
				$('#QID_' + INPQID).draggable( "option", "disabled", true );

				<!--- Clear old LINKs first? Tracking existing LINKs as part of obj --->
				if(typeof($('#QID_' + INPQID).find('.ObjDefBall').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.ObjDefBall').data('LineObj').remove();

				<!--------------------- Deleta all IVR ball connections --------------------->
				if(typeof($('#QID_' + INPQID).find('.Obj0Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj0Ball').data('LineObj').remove();
					
				if(typeof($('#QID_' + INPQID).find('.Obj1Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj1Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj2Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj2Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj3Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj3Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj4Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj4Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj5Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj5Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj6Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj6Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj7Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj7Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj8Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj8Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.Obj9Ball').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.Obj9Ball').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.ObjNRBall').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.ObjNRBall').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.ObjPoundBall').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.ObjPoundBall').data('LineObj').remove();

				if(typeof($('#QID_' + INPQID).find('.ObjErrBall').data('LineObj')) != "undefined")
					$('#QID_' + INPQID).find('.ObjErrBall').data('LineObj').remove();

				<!--- Remove all object's connections --->
				removeAllObjectConnections(INPQID);

				<!--------------------- Deleta all IVR ball connections --------------------->
				<!--- Clear old link from --->
				if(typeof($('#QID_' + INPQID).find('.ObjDefBall').data('ObjDefBall')) != "undefined")
					$('#QID_' + INPQID).find('.ObjDefBall').data('ObjDefBall', null);

				<!--- Loop over all objects that can link to this object --->
				$('.SBStageItem').each(function(index)
				 {
					if(typeof($('#QID_' + INPQID).find('.ObjDefBall').data('LINKTOQID')) != "undefined")
					{
						if($('#QID_' + INPQID).find('.ObjDefBall').data('LINKTOQID') == INPQID)
						{
							$('#QID_' + INPQID).find('.ObjDefBall').data('LINKTOQID', "");

							if(typeof($('#QID_' + INPQID).find('.ObjDefBall').data('LineObj')) != "undefined")
								$('#QID_' + INPQID).find('.ObjDefBall').data('LineObj').remove();
						}
					}
					
					<!--- Add other objects here --->
					if($('#QID_' + INPQID).data('RXT') == 2)
					{
						if(typeof($('#QID_' + INPQID).find('.Obj1Ball').data('LINKTOQID')) != "undefined")
						{	
							if($('#QID_' + INPQID).find('.Obj1Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj1Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj1Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj1Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj2Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj2Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj2Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj2Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj2Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj3Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj3Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj3Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj3Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj3Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj4Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj4Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj4Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj4Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj4Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj5Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj5Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj5Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj5Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj5Ball').data('LineObj').remove();	
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj6Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj6Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj6Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj6Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj6Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj7Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj7Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj7Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj7Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj7Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj8Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj8Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj8Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj8Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj8Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj9Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj9Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj9Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj9Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj9Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.Obj0Ball').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.Obj0Ball').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.Obj0Ball').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.Obj0Ball').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.Obj0Ball').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.ObjStarBall').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.ObjStarBall').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.ObjStarBall').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.ObjStarBall').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.ObjStarBall').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.ObjPoundBall').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.ObjPoundBall').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.ObjPoundBall').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.ObjPoundBall').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.ObjPoundBall').data('LineObj').remove();
							}
						}

						if(typeof($('#QID_' + INPQID).find('.ObjErrBall').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.ObjErrBall').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.ObjErrBall').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.ObjErrBall').data('LineObj')) != "undefined")
								{
									$('#QID_' + INPQID).find('.ObjErrBall').data('LineObj').remove();
								}
							}
						}

						if(typeof($('#QID_' + INPQID).find('.ObjNRBall').data('LINKTOQID')) != "undefined")
						{
							if($('#QID_' + INPQID).find('.ObjNRBall').data('LINKTOQID') == INPQID)
							{
								$('#QID_' + INPQID).find('.ObjNRBall').data('LINKTOQID', "");
								if(typeof($('#QID_' + INPQID).find('.ObjNRBall').data('LineObj')) != "undefined")
									$('#QID_' + INPQID).find('.ObjNRBall').data('LineObj').remove();
							}
						}
					}
				 });
	            
				 $.ajax({
				            type: "POST",
				            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=DeleteQID&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
				    		data:{
				    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
				    			INPDELETEQID : INPQID,
				    			MAXQID : maxQID 
								},
				            dataType: "json", 
				            success: function(d2, textStatus, xhr) {
				            	var d = eval('(' + xhr.responseText + ')');
				            	if (d.ROWCOUNT > 0) 
								{
									<!--- Check if variable is part of JSON result string --->
									if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
									{
										CURRRXResultCode = d.DATA.RXRESULTCODE[0];
										if(CURRRXResultCode > 0)
										{
											<!--- Delete this drop object delete parent? --->
											inpObj.parent().remove();
										}

									$("#loading").hide();
									<!--- auto re draw disable by Trang.Lee--->
									LoadStage();
									//if data in database update success then update data in javascript arrays
									DeleteDataDrawLine(INPQID);
									if(typeof(d.DATA.INPDELETEQID[0]) != "undefined")
									{
										INPQID = d.DATA.INPDELETEQID[0];
										QID_ARRAY = delete_array(QID_ARRAY,INPQID);
									}
								}
								else
								{
									<!--- Invalid structure returned --->
									$("#loading").hide();
								}
							}
							else
							{
								<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
						}
				    });
			} else {
				//if cancel delete object
				$("#QID_"+INPQID+" .imgObjDelete").removeAttr('src');
				$("#QID_"+INPQID+" .imgObjDelete").attr('src','<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png');
			}

			$("#loading").hide();
		});	

		<!--- actice undo --->
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	//delete data from arrarys conatain data for draw line when delete MCID obj
	function DeleteDataDrawLine(INPQID){				
				var arr = new Array();
				var indexArr;
				for(index in listRXT21){
       				arr = $('#QID_' + listRXT21[index]).data('RXTLINKARR');
       				if(is_array(arr)){
	       				for(i in arr){
		       				if(INPQID == arr[i]){
		       					arr.splice(i,1);
		       					indexArr = index;
		       				}
		       			}
       				}
       			}
       			
       			$('#QID_' + listRXT21[indexArr]).data('RXTLINKARR',arr);
       			
       			if($('#QID_' + INPQID).data('RXT') == 21){
       				for(index in listRXT21){
       					if(INPQID == listRXT21[index]){
	       					listRXT21.splice(index,1);
	       				}
       				}
       			}
	}
	<!--- Remove all MCIDs --->
	function ClearRXSS()
	{
		$("#loading").show();
		$.alerts.okButton = '&nbsp;Yes&nbsp;';
		$.alerts.cancelButton = '&nbsp;No&nbsp;';
		<!--- Are you sure - this change can not be undone. --->
		$.alerts.confirm(" Are you sure you want to delete all MCIDs?  This change can NOT be undone!", "Last chance to cancel this delete!",function(result) { 
			<!--- Only finish delete if  --->
			if(result)
			{		
		    	$.ajax({
		            type: "POST",
		            <!--- url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ClearRXSS&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true", --->
		            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=ClearXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
		    		data:{
		    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
		    		    inpPassBackdisplayxml : "1" 
						},
		            dataType: "json", 
		            success: function(d2, textStatus, xhr) {
		            	var d = eval('(' + xhr.responseText + ')');
		            						<!--- Alert if failure --->
						<!--- Get row 1 of results if exisits--->
						if (d.ROWCOUNT > 0) 
						{
							<!--- Check if variable is part of JSON result string --->
							if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
							{
								if(d.DATA.RXRESULTCODE[0] < 0){
									alert(d.DATA.MESSAGE[0]);
									return false;
								}
								CURRRXResultCode = d.DATA.RXRESULTCODE[0];
								if(CURRRXResultCode > 0)
								{
									LoadStage();
									<!--- Most of this is handled in loadstage anyway
									
									// Refresh connections
									connectData = new Array();
							        connectLineF21 = new Array();
							        lstQIDLine21 = new Array();
							        listRXT21 = new Array();
									<!--- Clear all stage objects --->
									$('#SBStage').empty();

									<!--- Rebuild the drawing canvas --->
									StageCanvas = new Raphael(document.getElementById('SBStage'));--->
								}
								$("#loading").hide();
							}
							else
							{<!--- Invalid structure returned --->
								$("#loading").hide();
							}
						}
						else
						{<!--- No result returned --->
							<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned"); --->
							$.alerts.okButton = '&nbsp;OK&nbsp;';
							jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
						}

						<!--- show local menu options - re-show regardless of success or failure --->
						<!--- 	$("#EditMCIDForm_" + INPQID + " #RXEditSubMenu_" + INPQID).show(); --->
						
						<!--- Turn off AJAX notice - re-show regardless of success or failure  --->
						<!--- $("#EditMCIDForm_" + INPQID + " #RXEditSubMenuWait").html(''); --->
					     }
		      	});
				<!--- clear history --->
				$.ajax({
					type: "POST", <!--- Posts data as form data rather than on query string and allows larger data transfers than URL GET does --->
					url:  '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=ClearHistory&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true', 
					datatype: 'json',
					data:  { INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput> },
					error: function(XMLHttpRequest, textStatus, errorThrown) {<!---console.log(textStatus, errorThrown);--->},
					success: function(d2, textStatus, xhr)
					{
						$('#redoBtn').removeClass().addClass('inactive');
						$('#undoBtn').removeClass().addClass('inactive');
					}
				});
				$('#clearBtn').removeClass().addClass('inactive');
				$('#sortBtn').removeClass().addClass('inactive');
			}
			$("#loading").hide();
		});

		return false;
	}
	

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- line connect --->
	var connectData = new Array();
	<!--- store Connect Line object from RXT 21--->
	var connectLineF21 = new Array();
	<!--- store QID Start,Finish for line draw from RXT 21--->
	var lstQIDLine21 = new Array();
	var listRXT21 = new Array();
	
	function Connector(QIDStart, QIDEnd)
	{
		this.QIDStart = QIDStart;
		this.QIDEnd = QIDEnd;
	}

	<!--- Count connection number to an object --->
	function countConnectFromObject(INPQIDStart)
	{
		var count = 0;
		for (var i=0; i<connectData.length; i++)
		{
			var tempObject = connectData[i];
			if (tempObject.QIDStart == INPQIDStart)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Count connection number to an object --->
	function countConnectToObject(INPQIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectData.length; i++)
		{
			var tempObject = connectData[i];
			if (tempObject.QIDEnd == INPQIDFinish)
			{
				count++;
			}
			<!--- alert("Connection:" + tempObject.QIDStart + ", " + tempObject.QIDEnd + ""); --->
		}
		return count;
	}

	<!--- Count to draw connections for MCID --->
	function getConnectionPosition(INPQIDStart, INPQIDFinish)
	{
		var count = 0;
		for (var i=0; i<connectData.length; i++)
		{
			var tempObject = connectData[i];
			if (tempObject.QIDStart == INPQIDStart && tempObject.QIDEnd == INPQIDFinish)
			{
				count++;
				break;
			}
			else if (tempObject.QIDEnd == INPQIDFinish)
			{
				count++;
			}
		}
		return count;
	}

	<!--- Remove all object's connections --->
	function removeAllObjectConnections(QID)
	{
		for (var i=(connectData.length-1); i>=0; i--)
		{
			var tempObject = connectData[i];
			if (tempObject.QIDStart == QID || tempObject.QIDEnd == QID)
			{
				<!--- Remove connection --->
				connectData.splice(i, 1);
			}
		}
	}

	<!--- Remove an connection --->
	function removeAnObjectConnection(INPQIDStart, INPQIDFinish)
	{
		for (var i=0; i<connectData.length; i++)
		{
			var tempObject = connectData[i];
			if (tempObject.QIDStart == INPQIDStart && tempObject.QIDEnd == INPQIDFinish)
			{
				<!--- Remove connection --->
				connectData.splice(i, 1);
			}
		}
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	function UpdateKeyValue(INPQID, inpKey, inpNewValue, caseValue)
	{
		
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		if (inpKey == 'CK1') {
			if (caseValue == '') {
				//caseValue = 'default';
			}
		} else {
			caseValue = '';
		}
			
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=UpdateKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,  
				inpRXSS : "", INPQID : INPQID, 
				inpKey : inpKey, 
				inpNewValue : inpNewValue, 
				caseValue : caseValue, 
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
           		<!--- Alert if failure --->
				<!--- Get row 1 of results if exisits--->
				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{
					<!--- No result returned --->
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
		     }
      		});
		$("#loading").hide();
		$('#undo').removeClass().addClass('active');
		$('#undoBtn').removeClass().addClass('active');
		return false;
	}
	
	<!--- Delete connection between 2 MCIDs ---> 
	function DeleteKeyValue(INPQID, INPQIDFinish, inpKey, inpNumber)
	{	
		<!--- Let user know something is being attempted --->
		$("#loading").show();
		<!--- Call Ajax to delete connection --->
    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=DeleteKeyValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    			inpRXSS : "",
    		    INPQID : INPQID,
    		    INPQIDFinish : INPQIDFinish,
    		    inpKey : inpKey, 
    		    inpNumber: inpNumber,
    		    inpPassBackdisplayxml : "1"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            	if (d.ROWCOUNT > 0) 
					{
						<!--- Check if variable is part of JSON result string --->
						if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
						{
							$("#loading").hide();
						}
						else
						{<!--- Invalid structure returned --->
							$("#loading").hide();
						}
						//update data in arrays  contain line data from switch and text at the end of line
						UpdateDataDrawLineAndText(INPQID,INPQIDFinish);
					}
					else
					{<!--- No result returned --->
						<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned"); --->
						$.alerts.okButton = '&nbsp;OK&nbsp;';
						jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
					}
			     }
      		});
		$("#loading").hide();

		return false;
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	function UpdateAnswerMapValue(INPQID, inpAnswer, inpNewValue)
	{
		<!--- Let user know something is being attempted --->
		$("#loading").show();
			
 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=UpdateAnswerMapValue&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
    			inpRXSS : "", INPQID : INPQID,
    		    inpAnswer : inpAnswer, 
    		    inpNewValue : inpNewValue, 
    		    inpPassBackdisplayxml : "1"
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
            		<!--- Get row 1 of results if exisits--->
            		if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									
								$("#loading").hide();
								}
								else
								{<!--- Invalid structure returned --->
									$("#loading").hide();
								}
							}
							else
							{<!--- No result returned --->
								<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
								$.alerts.okButton = '&nbsp;OK&nbsp;';
								jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
							}
			     }
      		});
		$("#loading").hide();
		return false;
	}

	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Update object position --->
	function UpdateXYPOS(INPQID, inpXPOS, inpYPOS)
	{
		if(parseInt(inpXPOS) < 0 || parseInt(inpYPOS) < 0 || parseInt(INPQID) < 0)
			return;

		<!--- Let user know something is being attempted --->
		$("#loading").show();
		
	    $.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=UpdateObjXYPOS&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			 INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>, 
    			 inpRXSS : "", 
    			 INPQID : INPQID,
    			 inpXPOS : inpXPOS, 
    			 inpYPOS : inpYPOS, 
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	var d = eval('(' + xhr.responseText + ')');
   				if (d.ROWCOUNT > 0) 
				{
					<!--- Check if variable is part of JSON result string --->
					if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
					{
						$("#loading").hide();
					}
					else
					{<!--- Invalid structure returned --->
						$("#loading").hide();
					}
				}
				else
				{<!--- No result returned --->
					<!--- $("#EditMCIDForm_" + INPQID + " #CURRentRXTXMLSTRING").html("Write Error - No result returned");	 --->	
					$.alerts.okButton = '&nbsp;OK&nbsp;';
					jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
				}
				LoadStage();
	     	}
   		});
			
		$('#undoBtn').removeClass().addClass('active');
		$('#redoBtn').removeClass().addClass('active');
		$("#loading").hide();
		return false;
	}
	
	
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Load previous MCID system --->
	function LoadStage()
	{
		<!--- Draw all top level objects first --->
		<!--- Then draw all LINKs for each object--->
		<!--- AJAX/JSON Do CFTE Demo --->
		QID_ARRAY = [];
		<!--- Let the user know somthing is processing --->
		$("#CURRentCCDXMLSTRING").html('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">');	

 		$.ajax({
            type: "POST",
            url: "<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools_improve.cfc?method=ReadRXSSXML&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true",
    		data:{
    			INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>
				},
            dataType: "json", 
            success: function(d2, textStatus, xhr) {
            	<!--- Clear all stage child objects --->
				$('#SBStage').empty();
				<!--- Reinit canvas--->
				StageCanvas = new Raphael(document.getElementById('SBStage'));
            	var d = eval('(' + xhr.responseText + ')');
          			if (d.ROWCOUNT > 0) 
							{
								<!--- Check if variable is part of JSON result string --->
								if(typeof(d.DATA.RXRESULTCODE[0]) != "undefined")
								{
									<!--- Add MCIDs to stage --->
									CURRRXResultCode = d.DATA.RXRESULTCODE[0];
									if(CURRRXResultCode > 0)
									{
										for(i=0;i<d.DATA.LSTARRAYOBJECT[0].length;i++){
											var curMcidObject = d.DATA.LSTARRAYOBJECT[0][i];
											<!--- load stage object --->
											LoadStageObject(curMcidObject.XPOS, curMcidObject.YPOS, curMcidObject.RXT, curMcidObject.RXQID, curMcidObject.DESC, curMcidObject.DYNAMICFLAG);
										}
										for(i=0;i<d.DATA.LSTARRAYOBJECT[0].length;i++){
											var curMcidObject = d.DATA.LSTARRAYOBJECT[0][i];
											<!--- update link object --->
											<!--- alert(d.DATA.RXSSMCIDXMLSTRING[x]); --->
											CURRRXT = curMcidObject.RXT;

											<!--- Better way to do these easy to maintain/less loops?--->
											<cfinclude template="js/RXT/js_DragObjPositions.cfm">

											<!--- Load LINKs based on type --->

											switch(parseInt(CURRRXT))
										  	{
												<cfinclude template="js/RXT/js_UpdateLINKs.cfm">
										   	}
										}	
									}
									
									LoadStageRules();
	
									// LoadStageLifeCycleObjects(150, 150, 1, 0, 'Testing', 1);
									
									// LoadStageLifeCycleObjects(150, 150, 1, 1, 'Testing', 1);
									
//									CreateNewRULES(1, 1, 150, 150);
								}
								else
								{<!--- Invalid structure returned --->
									$("#CURRentCCDXMLSTRING").html("Write Error - Invalid structure");
								}
								$("#overlay").hide();
							}
							else
							{<!--- No result returned --->
								$("#CURRentCCDXMLSTRING").html("Write Error - No result returned");
								$("#overlay").hide();
							}
			     }
      		});
	}
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	<!--- Load a MCID to stage --->
	function LoadStageObject(inpXPOS, inpYPOS, inpRXT, INPQID, INPDESC,flagDynamic)
	{
		var LastAutoX = 0;
		var LastAutoY = 0;

		<!--- Pre process position to snap to grid --->
		var intXPOS = parseInt(inpXPOS / 10);
		inpXPOS = intXPOS * 10;

		var intYPOS = parseInt(inpYPOS / 10);
		inpYPOS = intYPOS * 10;		

		if(inpXPOS < 0)
		{
			inpXPOS = LastAutoX;
			if(LastAutoX < 600)
				LastAutoX = LastAutoX + 100;
			else
			{
				LastAutoX = 0;
				LastAutoY = LastAutoY + 100;
			}
		}

		if(inpYPOS < 0)
		{
			inpYPOS = LastAutoY;
			if(LastAutoY > 500)
				LastAutoX = 0;
		}
		
		//check error X position if user drag MCID obj outside campaign
		maxXPos = $( "#SBStage" ).width();
		
		if(inpXPOS > maxXPos){
			inpXPOS=maxXPos;
		}
		//check error Y position if user drar MCID obj below canvas
		maxYPos = $( "#SBStage" ).height();
		
		if(inpYPOS > maxYPos){
			inpYPOS=maxYPos;
		}
		<!--- Fix bug adhesion devices after drop --->
		$( "#SBStage" ).append('<div id="NewObj' + ObjIndex + '" class="SBStageItem" rel="2" style="position:absolute; top:' + parseInt(inpYPOS) + 'px; left:' + parseInt(inpXPOS) + 'px;"></div>');

		<!--- Init MCID object --->		
		var NewObject = $("#SBStage #NewObj" + ObjIndex);

		<!--- Allow object to be dragged on the SBStage --->
		<!--- Prevent dragging outside of stage --->
		<!--- var $stage = $("#SBStage");
		var left = parseInt($stage.css("left"));
		var top = parseInt($stage.css("top"));
		var width = parseInt($stage.css("width"));
		var height = parseInt($stage.css("height"));
		var deviceWidth = 0;
		var deviceHeight = 0;

		if (parseInt(inpRXT) == 1)
		{
			deviceWidth = 132;
			deviceHeight = 72;
		}
		else
		{
			deviceWidth = 80;
			deviceHeight = 125;
		} --->

		NewObject.draggable({
			<!--- containment: [left, top, width + left - deviceWidth, height + top - deviceHeight], --->
			containment: "parent",
			cancel: "img.imgObjDelete",
			grid: [ 10, 10 ]
		});

		<!--- link to last MCID by default? MenuItemSX
		$(this).append(NewObject);--->
        
		<!--- Check MCID type to draw --->
		switch(parseInt(inpRXT))
		{
			case 1:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemSX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="114" width="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemSX3_glow"/></div>');
				NewObject.append('<div class="ObjDefBall" style="top: 40px; left: 115px;" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 2:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="Obj1Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey1"/></div>');
				NewObject.append('<div class="Obj2Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey2"/></div>');
				NewObject.append('<div class="Obj3Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey3"/></div>');
				NewObject.append('<div class="Obj4Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey4"/></div>');
				NewObject.append('<div class="Obj5Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey5"/></div>');
				NewObject.append('<div class="Obj6Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey6"/></div>');
				NewObject.append('<div class="Obj7Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey7"/></div>');
				NewObject.append('<div class="Obj8Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey8"/></div>');
				NewObject.append('<div class="Obj9Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey9"/></div>');
				NewObject.append('<div class="Obj0Ball" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NKey0"/></div>');
				NewObject.append('<div class="ObjStarBall" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NStarKey"/></div>');
				NewObject.append('<div class="ObjPoundBall" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NPoundKey"/></div>');
				NewObject.append('<div class="ObjErrBall" title="After error execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinRedSX"/></div>');
				NewObject.append('<div class="ObjNRBall" title="If no response execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinYellowSX"/></div>');
				NewObject.append('<div class="ObjDefBall" title="This is where I go when all else fails. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 3:	
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_rec"/></div><div class="ObjTextDecription"><span style="color:red;">REC</span></div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 4:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_lat"/></div><div class="ObjTextDecription">LAT</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 5:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_optout"/></div><div class="ObjTextDecription">Opt Out</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 6:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_ivrms"/></div><div class="ObjTextDecription">IVR MS</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 7:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_optin"/></div><div class="ObjTextDecription">Opt In</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 9:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_readds"/></div><div class="ObjTextDecription">Read DS</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 10:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_sql"/></div><div class="ObjTextDecription">SQL</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 11:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_dtmf"/></div><div class="ObjTextDecription">DTMF</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 12:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_pause"/></div><div class="ObjTextDecription">Pause</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 13:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_email"/></div><div class="ObjTextDecription">Email</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 14:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_store"/></div><div class="ObjTextDecription">Store</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 15:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_path"/></div><div class="ObjTextDecription">Play path</div>');	
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;
				
			case 16:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_switch"/></div><div class="ObjTextDecription">Switch</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				<!---NewObject.append('<div class="ObjDropZone" title="Drop connections here!"><img src="../campaign/mcid/images/KeyPad/DropZoneX.png"/></div>');--->
				break;

			case 17:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_switchoutMCIDs"/></div><div class="ObjTextDecription">Switchout</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 18:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_asr"/></div><div class="ObjTextDecription">ASR</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;
			case 19:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_ws"/></div><div class="ObjTextDecription">Web Service</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;
			case 20:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_asr"/></div><div class="ObjTextDecription">ASR2</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;

			case 21:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="switch imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow" style="margin-top: 12px; margin-left:12px"><img width="70" height="40" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="switch2" /></div>');
				NewObject.append('<div class="ObjDefBall" style="top: 10px; left: 50px;" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;
			case 22:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_asr"/></div><div class="ObjTextDecription">CONV</div>');
				NewObject.append('<div class="ObjDefBall" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/KeyPad/PinGreenSX.png"/></div>');
				break;
			case 23:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img width="114" height="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3_glow"/>');
				NewObject.append('<div class="ObjIcon"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_asr"/></div><div class="ObjTextDecription">DM</div>');
				break;
			case 24:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemSX3 imgObjBackground"/>');
				NewObject.append('<div class="imgObjBackgroundGlow"><img height="114" width="166" id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemSX3_glow"/></div>');
				NewObject.append('<div class="ObjIcon24"><img id="BGImage" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="menuitem_switchingSwitch"/></div><div class="ObjTextDecription24">Switch on Call State</div>');
				NewObject.append('<div class="ObjLiveBall" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NLive"/></div>');
				NewObject.append('<div class="ObjMachineBall" title="Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="NMachine"/></div>');
				NewObject.append('<div class="ObjDefBall" style="top: 40px; left: 115px;" title="Default execution path. Drag me to an input connection hole."><img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="PinGreenSX"/></div>');
				break;				
			default:
				NewObject.append('<img id="BGImage" objType="MCID" desc="'+ INPDESC + '" src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/dock/blank.gif" class="MenuItemBGX3 imgObjBackground"/>');
				break;	
		}

		NewObject.data('RXT',inpRXT);
		NewObject.data('XPOS', inpXPOS);
		NewObject.data('YPOS', inpYPOS);
	
		if(parseInt(INPQID) > 0)
		{
			<!--- Push new QID to draw connection --->
			QID_ARRAY.push(INPQID);
			<!--- Redraw MCID's conncetions --->
			ModifyStageObjectAfterQID(NewObject, inpRXT, INPQID, INPDESC);
		 	showHideImgBS(inpRXT,INPQID,flagDynamic);
			<!--- Set maxQID --->
			if(parseInt(INPQID) > maxQID)
			{
				maxQID = parseInt(INPQID);
			}
		}
		else
		{
			CreateNewMCID(inpRXT, NewObject, inpXPOS, inpYPOS );
		}
	}

	<!--- Show, hide ruler line when a mcid moving --->
	<!--- function showMCIDRulerLine()
	{
		$('#ver_top').show();
		$('#ver_bottom').show();
		$('#hor_top').show();
		$('#hor_bottom').show();
	}
	function hideMCIDRulerLine()
	{
		$('#ver_top').hide();
		$('#ver_bottom').hide();
		$('#hor_top').hide();
		$('#hor_bottom').hide();
	} --->
	
	function showObjectGlow(inpObj)
	{
		inpObj.find(".imgObjBackgroundGlow").css('display','block');
	}
	
	function hideObjectGlow(inpObj)
	{
		inpObj.find(".imgObjBackgroundGlow").css('display','none');
	}
	
	function removeAllLineRXT21(inpObj){
		if(lstQIDLine21.indexOf(inpObj.data('QID')) >= 0 ){	
		 	for (var i in connectLineF21)
	      	{
				<!--- Remove connection from RXT 21--->
		     	connectLineF21[i].remove();
		    }  
		}
	}
  //draw line for all RXT 21 when move RXT 21 and other object it linked to
  function drawLineFromRXT21()
	{
	   //only draw line from type 21  to other type so assign ObjDefX,ObjDefY here if campain has line from RXT 1 to  type 21 
		ObjDefX = 50;
        ObjDefY = 96;
       		for(index in listRXT21){
       			var arr = $('#QID_' + listRXT21[index]).data('RXTLINKARR');
       			if (is_array(arr)) {
	       			for(i in arr){
		       			LINKIVRObjects('ObjDefBall', arr[0], arr[i], -1, -1, -1, ObjDefX+80, ObjDefY-50, 2, ObjDefColor);
		       		}
       			}
       		}	
	}

	/**
	OPTION:1 first load campaign
	OPTION:2 when update text at the end of line between SWITCH and ELE/CONV alredy exists inside SWITCH
	OPTION:3 when move MCID object
	*/
	function putTextLoadFirst(RXT21_QID,ELE_QID,TEXT_CASE,OPTION){
					var RXT21width=$( "#SBStage" ).find("#QID_"+RXT21_QID+" #BGImage").width();
					var ELEwidth=$( "#SBStage" ).find("#QID_"+ELE_QID+" #BGImage").width();
					//just remove text at end of arrow when every move MCID object has QID in list array of QID
					$( "#SBStage" ).find("#QID_"+ELE_QID).find("#textLineQID_"+ELE_QID).remove();
					//safe check
					if($('#SBStage #QID_'+RXT21_QID) == null || $('#SBStage #QID_'+ ELE_QID) == null){
						return;
					}
				    var leftPosCurrentRXT21=$('#SBStage #QID_'+RXT21_QID).position().left;
					var leftPosTargetRXT=$('#SBStage #QID_'+ ELE_QID).position().left;
					//append div contain text at the end of div of MCID object
					var div=$("<DIV>");
					div.text(TEXT_CASE);
					div.attr("id","textLineQID_"+ELE_QID);
					var  top;
					var  left;
		            //canculate base on x position of RXT 21 and other RXT 
		            // RXT21 position right on RXT other left
		            if(leftPosCurrentRXT21+RXT21width > leftPosTargetRXT+ELEwidth){
		              if($('#QID_' + ELE_QID).data('RXT') == 1 || $('#QID_' + ELE_QID).data('RXT') == 24){
		              top=-60;
		              left=139;//width of RXT 1
		             }else{
		              top=-115;
		              left=82;//width of other RXT
		             }
		             // RXT21 position left on RXT other right
		            }else if(leftPosCurrentRXT21+RXT21width <=leftPosTargetRXT){
		              if($('#QID_' + ELE_QID).data('RXT') == 1 || $('#QID_' + ELE_QID).data('RXT') == 24){
		              top=-70;
		             }else{
		              top=-125;
		             }
		              left=-30;
		            }else{
		             //project down the x of switch inside ELE 
		             var ELEheight=$( "#SBStage" ).find("#QID_"+ELE_QID+" #BGImage").height();
		             var topPosCurrentRXT21=$('#SBStage #QID_'+RXT21_QID).position().top;
					 var topPosTargetRXT=$('#SBStage #QID_'+ ELE_QID).position().top;	
					 //set position absoulute so need margin-left compared with target MCID
		             left=leftPosCurrentRXT21+RXT21width-leftPosTargetRXT;
		             //RXT21 higher than target MCID
		              if(topPosCurrentRXT21<topPosTargetRXT){
		                 top=-ELEheight-12;
		              }
		            }
		            
		          div.attr("style","position:absolute;margin-top:"+top+"px;z-index:99999;margin-left:"+left+"px;width:500px;");
		          
		          $( "#SBStage" ).find("#QID_"+ELE_QID).append(div); 
		          $('#textLineQID_' + ELE_QID).editable(CurrSitePath + "/cfc/mcidtools_improve.cfc?method=EditSwitchKeyValue&returnformat=plain&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true&INPBATCHID=" + <cfoutput>#INPBATCHID#</cfoutput> + "&INPQID=" + RXT21_QID + "&inpKey=ck1&inpNewValue=" + ELE_QID,{ 					
						  indicator : "<img src='../../public/images/loading-small.gif'>",
						  tooltip   : "Click to edit description...",
						  width : 78,
						  height : 15,
						  event     : "dblclick",
						  type      : 'text',							
						  submit    : 'OK',
						  cancel    : 'Cancel',
						  name : "caseValue",
						  onblur : 'ignore',
						  callback : function(value, settings)
									 {		
										  var arr = $('#QID_' + RXT21_QID).data('ELE_TEXT');
										  if(typeof(arr) == 'undefined'){
										   arr=new Array();
										  }
						                  if(arr.indexOf(ELE_QID) == -1){
									       arr.push(ELE_QID);
						                   arr.push(value); 
										  }
										  //update case value  when drag line same RXT21 and target obj again
								 		  else{
						       					var arr = $('#QID_' + RXT21_QID).data('ELE_TEXT');
						       				 	if(is_array(arr)){
						       						for(i=0;i<arr.length;i++){
								       		   	     if(arr[i] === ELE_QID && i%2 == 0){
								       		   	     	arr[i+1]=value;
								       		   	     	break;
								       		   	     }
						       					}
							       		    } 
									     }
									 }
					  });
					 
		   		  //store data(QID of RXT21 and all element data text) for process later 
		   		  //only 	OPTION==1 or ==2 update text data in array
		   		  if(OPTION != 3){
			   		  var arr = $('#QID_' + RXT21_QID).data('ELE_TEXT');
					  if(typeof(arr) == 'undefined'){
					   arr=new Array();
					  }
	                  if(arr.indexOf(ELE_QID) == -1){
				       arr.push(ELE_QID);
	                   arr.push(TEXT_CASE); 
					  }
					  //update case value  when drag line same RXT21 and target obj again
			 		  else{
	       					var arr = $('#QID_' + RXT21_QID).data('ELE_TEXT');
	       				 	if(is_array(arr)){
	       						for(i=0;i<arr.length;i++){
			       		   	     if(arr[i] === ELE_QID && i%2 == 0){
			       		   	     	arr[i+1]=TEXT_CASE;
			       		   	     	break;
			       		   	     }
	       					}
		       		    } 
				     }
		   		  }            		
			       
				  $('#QID_' + RXT21_QID).data('ELE_TEXT',arr);
	}
	//put text at the end of line form RXT 21 to other MCID obj
	function putTextToEndLine(){
		    for(index in listRXT21){
       			var arr = $('#QID_' + listRXT21[index]).data('ELE_TEXT');
       			if(is_array(arr)){
		       		for(i=0;i<arr.length;i++){
		       		   	if(i%2 == 0){
		       			 	putTextLoadFirst(listRXT21[index],arr[i],arr[i+1],3);
		       			}
		       		}
       			}
       		}
	}
	
	function loadDataRepeatKey(INPQID,type){
	 template="";
	 if(type == 0){
	 	     	template += '<label>LAT Number</label>';
	 	     	template +=	'   <div class="content">';
				template +=	'		  <input TYPE="text" name="inpCK1" id="inpCK1" class="validate[[funcCall[checkPhone]] ui-corner-all" >';
				template +=	'	</div>';
				$("#EditMCIDForm_" + INPQID + " #inpCK1LAT").empty().append(template);	
				var data=$("#EditMCIDForm_" + INPQID + " #inpCK1LAT").data("datainput");	
				if(typeof(data)!='undefined'){
					     $("#RXTEditForm_" + INPQID + " #inpCK1").val(data); 		
				}
	 	    }else{
	 	    	var curr=$("#RXTEditForm_" + INPQID + " #inpCK1").val(); 
	 	    	$("#EditMCIDForm_" + INPQID + " #inpCK1LAT").data("datainput", curr);			
	 	    	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
				var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';
		        template += '<label>LAT Number</label>';
		     	template +=	'   <div class="content">';
				template += '<select  name="inpCK1"id="inpCK1"  class="ui-corner-all" style="width:115px">';
				for(i=1; i<=10 ; i++){
					template += '<option value="LocationKey'+ i +'_vch">LocationKey'+ i +'</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="CustomField'+ i +'_int">CustomField'+ i +'_int</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="CustomField'+ i +'_vch">CustomField'+ i +'_vch</option>';
				};
				template += '</select>';
				template +=	'	</div>';
				template += '<label>Data Field</label>';
				template +=	'<div class="content">';
				template +=	' 	<input id="inpCK1DataField" type="text" />';
				template +=	'</div>';
				$("#EditMCIDForm_" + INPQID + " #inpCK1LAT").empty().append(template);
		    }
		}
function loadDataLATID(INPQID,type){
	 template="";
	 if(type == 0){
	 	     	template += '<label>LAT caller ID number</label>';
	 	     	template +=	'   <div class="content">';
				template +=	'		  <input TYPE="text" name="inpCK4" id="inpCK4" class="validate[[funcCall[checkPhone]] ui-corner-all" >';
				template +=	'	</div>';
				$("#EditMCIDForm_" + INPQID + " #inpCK4LAT").empty().append(template);	
				var data=$("#EditMCIDForm_" + INPQID + " #inpCK4LAT").data("datainput");	
				if(typeof(data)!='undefined'){
					     $("#RXTEditForm_" + INPQID + " #inpCK4").val(data); 		
				}
	 	    }else{
	 	    	var curr=$("#RXTEditForm_" + INPQID + " #inpCK4").val(); 
	 	    	$("#EditMCIDForm_" + INPQID + " #inpCK4LAT").data("datainput", curr);			
	 	    	var serverPath = '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>';
				var userId = '<cfoutput>#SESSION.UserID#</cfoutput>';
		        template += '<label>LAT caller ID number</label>';
		     	template +=	'   <div class="content">';
				template += '<select  name="inpCK4" id="inpCK4"  class="ui-corner-all" style="width:115px">';
				for(i=1; i<=10 ; i++){
					template += '<option value="LocationKey'+ i +'_vch">LocationKey'+ i +'</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="CustomField'+ i +'_int">CustomField'+ i +'_int</option>';
				};
				for(i=1; i<=10 ; i++){
					template += '<option value="CustomField'+ i +'_vch">CustomField'+ i +'_vch</option>';
				};
				template += '</select>';
				template +=	'	</div>';
				template += '<label>Data Field</label>';
				template +=	'<div class="content">';
				template +=	'	<input id="inpCK4DataField" type="text"/>';
				template +=	'</div>';
				$("#EditMCIDForm_" + INPQID + " #inpCK4LAT").empty().append(template);
		    }
		}
	<!------------------------------------------------------------------------------------------------------------------------------------------------------------>
	function ModifyStageObjectAfterQID(inpObj, inpRXT, INPQID, INPDESC)
	{
		var containerPopupMenu = $(".popupMenu");
		<!--- Set QID for new mcid --->
		inpObj.attr('id', 'QID_' + INPQID);

		<!--- Add a edit, delete button once a QID is assigned --->
		switch(parseInt(inpRXT))
		{
			case 1:
				inpObj.append('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png" style="top: 7px; left: 118px;" class="imgObjDelete" onclick="DeleteMCIDRXSS(' + INPQID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editMCID_x_' + INPQID + '" title="MCID ' + INPQID + '" align="center" style="top: 6px; left: 6px;">' + INPQID + '</div>');
				inpObj.append('<div class="EditObjDESC" title="'+ INPDESC +'" id="editDESC_' + INPQID + '">' +  calculateDescShort(INPDESC,60) + '</div>');
				<!--- $('#editDESC_' + INPQID).removeClass('ui-widget'); --->
				$('#editDESC_' + INPQID).editable("<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=UpdateQIDDESC&_cf_noDEBUG=true&_cf_nocache=true&INPBATCHID=" + <cfoutput>#INPBATCHID#</cfoutput> + "&INPQID=" + INPQID + "&inpOldDESC=" + $(this).html(), { 
									  indicator : "<img src='" + CurrSitePath + "/images/loading-small.gif'>",
									  method : 'POST',
									  tooltip   : "Doubleclick to edit...",
									  event     : "dblclick",
								      type      : 'textarea',
									  submit    : 'OK',
									  //  style     : 'inherit',
									  cancel    : 'Cancel',
									  name      : "inpNewDESC",
									  onblur    : 'ignore'
							  		}).bind("ajaxStop", function(){
										$(this).parent().find("#BGImage").attr('desc',$.trim($(this).html()))
       							});
				break;
			case 21:
				inpObj.append('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png" class="imgObjDelete" style="margin: -6px 0 0 -40px;" onclick="DeleteMCIDRXSS(' + INPQID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" style="left:1px;margin-top:4px;" id="editMCID_x_' + INPQID + '" title="MCID ' + INPQID + '" align="center">' + INPQID + '</div>');
				break;	
			case 24:
				inpObj.append('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png" style="top: 7px; left: 118px;" class="imgObjDelete" onclick="DeleteMCIDRXSS(' + INPQID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editMCID_x_' + INPQID + '" title="MCID ' + INPQID + '" align="center" style="top: 6px; left: 6px;">' + INPQID + '</div>');
				break;	
			default:
				inpObj.append('<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/delete_11x9.png" class="imgObjDelete" onclick="DeleteMCIDRXSS(' + INPQID + ', $(this));" />');
				inpObj.append('<div class="MCIDToken" id="editMCID_x_' + INPQID + '" title="MCID ' + INPQID + '" align="center">' + INPQID + '</div>');
				break;	
		}
		
		$("#editMCID_x_" + INPQID).unbind('click');
		$("#editMCID_x_" + INPQID).click(function(){
			
			containerPopupMenu.html('');
			containerPopupMenu.append("<div style='display:none;' id='popup_editMCID_x_" + INPQID + "'>" + editMCID($("#editMCID_x_" + INPQID)).html() + "</div>");
			showObjectGlow(inpObj);
			var form = $("#EditMCIDForm_"+INPQID);
			/* Find current RTX Help messages */
			var RXTId = "RXT"+parseInt(inpRXT);
			
			var currentRXT = MCID_HELP[RXTId];
			if (currentRXT) {
				/* Find all INPUT fields in current form */
				var inputs = $("INPUT", form);
				/* Check INPUT fields to add Help tooltip */
				inputs.each(function(i,item){
					var id = item.id;
					// This INPUT field is a CK input
					if ((id && id.indexOf("inpCK")==0) || (id && id.indexOf("inpRQ")==0) || (id && id.indexOf("inpCP")==0)) {
						var inp = $(item);
						if ($("A.tipsy1",inp.parent()).length==0) {
							var msg = currentRXT[id];
							if (msg) {
								$(item).before("<a class='tipsy1' style='float: right;' href='javascript:void(0);' title=\""+msg+"\"><img src='<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/css/mcid/images/help.png'></a>");
							}
						}
					} 
				});
			}
			/* Call tipsy to handle Help tooltips */
			$("#EditMCIDForm_" + INPQID + " #multiScript").css("display","block");
			$('.tipsy1').tipsy({gravity: 'w',html: true});
			<!--- Make dynamic changes after dialog creation --->
			var ScriptButtons =	"<div id='rxdsmenu' class='rxdsmenu'>" +
									"<div id='player'><div id='flashplayer'></div></div>" +
									"<div id='change' class='change' ><a class='rename1'>Change</a> </div>   " +     
								"</div>"; 
				
			//"#AudioPlayer_" + inpQID + " #DSData #inpLibId""
			$("#EditMCIDForm_" + INPQID + " .leftmenu").append("<div><a id='DeleteMCID_" + INPQID + "' class='QIDRXT DeleteMCID' title='Cancel Changes to MESSAGE Component'>Delete</a></div>");			
			$("#EditMCIDForm_" + INPQID + " #ScritpButtons").html(ScriptButtons); 	
			$("#EditMCIDForm_" + INPQID + " #ScritpButtons #play").click(function() { PlayScriptDialog(INPQID); });
			$("#EditMCIDForm_" + INPQID + " #ScritpButtons #change").click(function() { SelectScriptOneLibDialog(INPQID,0); });
			$("#EditMCIDForm_" + INPQID + " #ScritpButtons #play").hover( function(){ $(this).addClass("ui-state-hover"); }, function(){ $(this).removeClass("ui-state-hover"); } );
		 	
		 	$("#EditMCIDForm_" + INPQID + " input[name=inpBS]:radio").change(function(){
		 		var bsRadioValue = $("#EditMCIDForm_" + INPQID + " input[name=inpBS]:checked").val();	
				if(bsRadioValue == 1){
					if(isNotEmptyAllScript(INPQID)){
						jConfirm(
								"Are you sure want to change script to dynamic?  Your previous script data will be removed!!!",
								"Change build script",
								function(result){
									if(result){
										changeNewMode(INPQID, 1);
										$("#EditMCIDForm_" + INPQID + " #currLib").html(0);
										showCurrentActiveArea(INPQID, "dymamic");
										$("#RXTEditForm_" + INPQID + " #changeScript").css("display","none");
				 						
									} else{
										resetRadioOption(INPQID);
									}
									return false;
							});	
					} else {
						showCurrentActiveArea(INPQID, "dymamic");
						var inpLibIdValue = $("#EditMCIDForm_" + INPQID + " #inpLibId").val();
						showHideScriptListTable(INPQID);
						$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea").css("display","block");
						$("#EditMCIDForm_" + INPQID + " #currLib").html('0');
 						if(inpLibIdValue == 0){
							$("#EditMCIDForm_" + INPQID + " #multiScript").css("display","block");
							$("#RXTEditForm_" + INPQID + " #changeScript").css("display","none");
 						}else{
 							$("#RXTEditForm_" + INPQID + " #changeScript").css("display","");
 						}
					}
				} else if(bsRadioValue == 0){
					if(isNotEmptyAllScript(INPQID)){
						jConfirm(
							"Are you sure want to change script to static?  Your previous script data will be removed!!!",
							"Change build script",
							function(result){
								if(result){
									changeNewMode(INPQID, 0);
									showCurrentActiveArea(INPQID, "static");
								} else{
									resetRadioOption(INPQID);
								}
								return false;
						});	 
					} else {
						showCurrentActiveArea(INPQID, "static");
					}
				} else {
					if(isNotEmptyAllScript(INPQID)){
						jConfirm(
								"Are you sure want to change script to TTS?   Your previous script data will be removed!!!",
								"Change build script",
								function(result){
									if(result){
										changeNewMode(INPQID, 2);
										showCurrentActiveArea(INPQID,"tts");
									} else{
										resetRadioOption(INPQID);
									}
									return false;
							});	 
						// TTS option
						
					}else{
						showCurrentActiveArea(INPQID,"tts");
					}
				}
			});
			
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addMultiScript").click(function(){
				<!--- SelectScriptDialog(INPQID,1); --->
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					SelectScriptOneLibDialog(INPQID,1);
				}else{
					SelectScriptOneLibDialog(INPQID,3);
				}
			});

			
			$("#EditMCIDForm_" + INPQID + " #ttsArea #addTTSScript").click(function(){
				addTTSBuildScriptForm(INPQID, "parent");
			});
			
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addCustomFieldScript").click(function(){
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					addBuildScriptForm(INPQID);
				}else{
					alert("Please add at least 1 script first!");
				}				
			});
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addTTSCustomFieldScript").click(function(){
				addTTSBuildScriptForm(INPQID);       
			});
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addBlankScript").click(function(){
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					addBlankScriptForm(INPQID,'blank');
				}else{
					alert("Please add at least 1 script first!");
				}					
				
			});
			
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addSwitchScript").click(function(){
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					openSwitchColumnDialog(INPQID);
				}else{
					alert("Please add at least 1 script first!");
				}					
				
			});
			
			
			$("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #addConversionFieldScript").click(function(){
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					openConversionColumnDialog(INPQID);
				}else{
					alert("Please add at least 1 script first!");
				}					
				
			});
				
			$("#EditMCIDForm_" + INPQID + " #MultiScriptSelectArea #changeScript").click(function(){
				SelectScriptOneLibDialog(INPQID,3);
			});				
			
		 	$("#EditMCIDForm_" + INPQID + " input[name=scriptCheckBoxAll]:checkbox").change(function(){
			 	var totalEleSingle=$("#EditMCIDForm_" + INPQID + " #ScriptItems").data("scriptQuantity");
			 	if($("#EditMCIDForm_" + INPQID + " input[name=scriptCheckBoxAll]:checkbox").is(":checked")){
			 	   	for(i=1;i<=totalEleSingle;i++){
				   		 $("#EditMCIDForm_" + INPQID + " #ScriptItems ").find("#ck_"+i).attr('checked', true);
				    }
			 	}else{
			 	   for(i=1;i<=totalEleSingle;i++){
				    	 $("#EditMCIDForm_" + INPQID + " #ScriptItems ").find("#ck_"+i).attr('checked', false);
				   }
			 	}
			 });
			 			
		    $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #deleteAllScript").click(function(){
		         if($("#EditMCIDForm_" + INPQID + " input[name=scriptCheckBox]:checked").is(":checked")){
		          var check = confirm("Do you want to delete all build script seleted!");
			    	if(check){
			    	  deleteAllScriptChecked(INPQID);
		              $("#EditMCIDForm_" + INPQID + " input[name=scriptCheckBoxAll]:checkbox").attr("checked",false);
			    	}
		         }   	
			});
			
		   $("#EditMCIDForm_" + INPQID + " #MultiScritpButtons #viewAllScript").click(function(){
				if($("#EditMCIDForm_" + INPQID + " #inpLibId").val() != 0){
					viewAllScript(INPQID);
				}else{
					alert("Please add at least 1 script first!");
				}				
			});
			<!---
			$('#RXTEditForm_'+INPQID).validate();
			--->
			jQuery('#RXTEditForm_'+INPQID).validationEngine({
				promptPosition : "topLeft",
				scroll: false,
				onValidationComplete : function(form, r) { 
					if (r) {
						WriteRXSSXMLString(INPQID, inpRXT); 
						return false;
					}
				}
			});
	
			<!--- Delete divice --->
			$('.DeleteMCID').click(function() {
				DeleteMCIDRXSS(INPQID, inpObj.find(".imgObjDelete"));
			});
	
			<!--- Move MCID QID --->
			$('.MoveMCID').click(function() {
			});

			$('.cancelrxt').click(function() {
				closePopupWindow($('#QID_' + INPQID), INPQID);
			});
			<!--- Change QID --->
			$('#changeQID').click(function() {
				$("#popupchangeQID").dialog({
					dialogClass: 'popupchange',
					
					title: "Change QID",
					modal : true,
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove(); 
				        	}
					    }
					]
				});
			});
			<!--- Move current QID to Top --->
			$('#MoveQidToTop').unbind('click');
			$('#changeQIDToTop, #MoveQidToTop').click(function() {
				//$('.formedit').hide();
				//$('.ui-widget-overlay').hide();
				$("#popupchangeQID").remove();
				$('#popuptop').dialog({ 
					show: 'slide',
					modal : true,
					title: 'Message',
					
					buttons: [
					    {
					        text: "Ok", 
					        click: function() { 
					        	$(this).remove(); 
					        }
					    }
					]
				});
				SwapQID(curSelectQID, 1);
				closePopupWindow($('#QID_' + INPQID), INPQID);
			});
	
			<!--- Move current QID to Bottom --->
			$('#MoveQidToBottom').unbind('click');
			$('#changeQIDToBottom, #MoveQidToBottom').click(function() {
				$("#popupchangeQID").remove();
				closePopupWindow($('#QID_' + INPQID), INPQID);
				$('#popupbottom').dialog({ 
					show: 'slide',
					
					modal : true,
					title: 'Message',
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove(); 
				        	}
					    }
					]
				});
				SwapQID(curSelectQID, maxQID);
			});
	
			<!--- Increase current QID --->
			$('#addQIDMore1, #IncreaseQid').click(function() {
				maxQID = (curSelectQID + 1 > maxQID) ? (curSelectQID + 1) : maxQID
				closePopupWindow($('#QID_' + INPQID), INPQID);
				$("#popupchangeQID").remove();
				$('#popupIncrease').dialog({ 
					show: 'slide',
					modal : true,
					zIndex: 99999,
					title: 'Message',
					buttons: [
					    {
					        text: "Ok",
					        click: function() { 
					        	$(this).remove(); 
				        	}
					    }
					] 
				});
				SwapQID(curSelectQID, curSelectQID + 1);
			});
	
			<!--- Decrease current QID --->
			$('#minusQIDTo1, #DecreaseQid').click(function() {
				if (curSelectQID - 1 < 1)
				{	
					closePopupWindow($('#QID_' + INPQID), INPQID);
					$("#popupchangeQID").remove();		
					$('#popupnotmove').dialog({ 
						show: 'slide',
						modal : true,
						
						title: 'Message',
						buttons: [
						    {
						        text: "Ok",
						        click: function() { 
						        	$(this).remove();
					        	}
						    }
						] 
					});
				}
				else
				{
					closePopupWindow($('#QID_' + INPQID), INPQID);
					$("#popupchangeQID").remove();
					$('#popupDecrease').dialog({ 
						show: 'slide',
						modal : true,
						
						title: 'Message',
						buttons: [
						    {
						        text: "Ok",
						        click: function() { 
						        	$(this).remove();
					        	}
						    }
						] 
					});
					SwapQID(curSelectQID, curSelectQID - 1);
				}
			});
		
			<!--- Close Form Edit --->
			$('.cancelrxt').click(function () {
				closePopupWindow($('#QID_' + INPQID), INPQID);
			});
			<!---$("#SaveChangesRXSSXML_" + INPQID).click(function() { WriteRXSSXMLString(INPQID, inpRXT); return false; });  --->
			
			if(!editCampaignPermission){
				alert(permissionMessage);
				return false;
			}
			<!--- Read existing data --->
			ReadRXTData(INPQID, 0, 1);
			
			$('#popup_editMCID_x_' +INPQID).dialog({ 
				show: {effect: "fade", duration: 500},
				hide: {effect: "fold", duration: 500},
				modal : true,
				title: 'Message',
				position: 'top',
				width:'auto',
				height:'auto',
				dialogClass: 'noTitleStuff',
				close: function() {
					 $(this).remove(); 
				}
			});
		});
		
		
		
		var CURRRXT = inpRXT;
		<cfinclude template="js/RXT/js_DragObjPositions.cfm">

		<!--- Store the QID as part of the obj data --->
		inpObj.data('QID', INPQID);
		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "drag", function(event, ui) 
		{
			
			var myself = $(this);

			if( $(this).draggable( "option", "disabled" ) == true)
				return;

			var stageWidth = parseInt($("#SBStage").css("width"));
			var stageHeight = parseInt($("#SBStage").css("height"));
			var deviceTop = parseInt(ui.position.top);
			var deviceLeft = parseInt(ui.position.left);

			<!--- Draw Mcid ruler line when moving --->
			if (parseInt(inpRXT) == 1)
			{
				deviceWidth = 130;
				deviceHeight = 72;
				//new device
				deviceWidth = 139;
				deviceHeight = 84;
			} else if (parseInt(inpRXT) == 21) {
				deviceWidth = 65;
				deviceHeight = 32;
			}
			else
			{
				deviceWidth = 80;
				deviceHeight = 125;
				//new device
				deviceWidth = 82;
				deviceHeight = 138;
			}

			<!--- Keep Mcid in bound --->
			var checkLeft = (deviceLeft + deviceWidth >= stageWidth) ? true : false;
			var checkTop = (deviceTop + deviceHeight >= stageHeight) ? true : false;


			var SBMenuBar = $('#SBMenuBar');
			var MCIDToolbar = $('#MCIDToolbar');

			<!---
			var menuHeight = parseInt(SBMenuBar.css('height')) + 2 * parseInt(SBMenuBar.css('border-top-width'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			--->
			var menuHeight = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'));
			var menuWidth = parseInt(SBMenuBar.css('width')) +  2 * parseInt(SBMenuBar.css('border-top-width'))+parseInt(SBMenuBar.css('margin-right'))+parseInt(SBMenuBar.css('padding-left'));
			var toolbarHeight = parseInt(MCIDToolbar.css('height')) + 2 * parseInt(MCIDToolbar.css('padding-top'));
			var rulerWidth = parseInt($('.hor_ruler').css('height'));

			var horRulerTop = toolbarHeight + 1;
			var verRulerTop = toolbarHeight + rulerWidth;
	
			<!--- $('#hor_top').css("top", horRulerTop);
			$('#hor_bottom').css("top", horRulerTop);
			$('#ver_top').css("left", menuWidth+1);
			$('#ver_bottom').css("left", menuWidth+1);
			
			if (deviceTop <= stageHeight)
			{
				$('#ver_top').css("top", deviceTop + verRulerTop);

				if (!checkTop)
				{
					$('#ver_bottom').css("top", deviceTop + deviceHeight + verRulerTop);
				}
				else
				{
					$('#ver_bottom').css("top", stageHeight + verRulerTop);
				}
			}
			else
			{
				$('#ver_top').css("top", verRulerTop);
				$('#ver_bottom').css("top", verRulerTop);
			}

			if (deviceLeft <= stageWidth)
			{
				$('#hor_top').css("left", deviceLeft + rulerWidth+ menuWidth);

				if (!checkLeft)
				{
					$('#hor_bottom').css("left", deviceLeft + deviceWidth + rulerWidth+ menuWidth);
				}
				else
				{
					$('#hor_bottom').css("left", stageWidth + rulerWidth+ menuWidth);
				}
			}
			else
			{
				$('#hor_top').css("left", rulerWidth);
				$('#hor_bottom').css("left", rulerWidth);
			} --->
			<!--- Draw Def connection Start connection --->
			if(typeof(inpObj.find('.ObjDefBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
			{
				LINKIVRObjects('ObjDefBall', inpObj.data('QID'), inpObj.find('.ObjDefBall').data('LINKTOQID'), -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
			}

			//hailp6028 add code delete all line from type 21 when move MCID related RXT 21 
			 removeAllLineRXT21(inpObj);
			 if(lstQIDLine21.indexOf(INPQID) >= 0){
			  	drawLineFromRXT21();
			    //draw text again base on postion of source MCID type 21 and target MCID
			    putTextToEndLine();   
			} 
         		                       	
			//test 
			<!--- Draw connections from IVR ball when drag --->
			if(inpObj.data('RXT') == 2)
			{
				if(typeof(inpObj.find('.Obj1Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					<!--- LINKIVRObjects('Obj1Ball', inpObj.data('QID'), inpObj.find('.Obj1Ball').data('LINKTOQID'), -1, -1, -1, 0, 24, 4, Obj1Color); --->
					LINKIVRObjects('Obj1Ball', inpObj.data('QID'), inpObj.find('.Obj1Ball').data('LINKTOQID'), -1, -1, -1, 0, 20, 4, Obj1Color);
				}

				if(typeof(inpObj.find('.Obj2Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					<!--- LINKIVRObjects('Obj2Ball', inpObj.data('QID'), inpObj.find('.Obj2Ball').data('LINKTOQID'), -1, -1, -1, 71, 33, 2, Obj2Color); --->
					LINKIVRObjects('Obj2Ball', inpObj.data('QID'), inpObj.find('.Obj2Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 30, 2, Obj2Color);
				}

				if(typeof(inpObj.find('.Obj3Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					<!--- LINKIVRObjects('Obj3Ball', inpObj.data('QID'), inpObj.find('.Obj3Ball').data('LINKTOQID'), -1, -1, -1, 72, 24, 2, Obj3Color); --->
					LINKIVRObjects('Obj3Ball', inpObj.data('QID'), inpObj.find('.Obj3Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 20, 2, Obj3Color);
				}

				if(typeof(inpObj.find('.Obj4Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{		
					<!--- LINKIVRObjects('Obj4Ball', inpObj.data('QID'), inpObj.find('.Obj4Ball').data('LINKTOQID'), -1, -1, -1, 0, 42, 4, Obj4Color); --->
					LINKIVRObjects('Obj4Ball', inpObj.data('QID'), inpObj.find('.Obj4Ball').data('LINKTOQID'), -1, -1, -1, 0, 40, 4, Obj4Color);
				}

				if(typeof(inpObj.find('.Obj5Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{		
					LINKIVRObjects('Obj5Ball', inpObj.data('QID'), inpObj.find('.Obj5Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 50, 2, Obj5Color);
				}

				if(typeof(inpObj.find('.Obj6Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{		
					<!--- LINKIVRObjects('Obj6Ball', inpObj.data('QID'), inpObj.find('.Obj6Ball').data('LINKTOQID'), -1, -1, -1, 71, 42, 2, Obj6Color); --->
					LINKIVRObjects('Obj6Ball', inpObj.data('QID'), inpObj.find('.Obj6Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 40, 2, Obj6Color);
				}

				if(typeof(inpObj.find('.Obj7Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					<!--- LINKIVRObjects('Obj7Ball', inpObj.data('QID'), inpObj.find('.Obj7Ball').data('LINKTOQID'), -1, -1, -1, 0, 60, 4, Obj7Color); --->
					LINKIVRObjects('Obj7Ball', inpObj.data('QID'), inpObj.find('.Obj7Ball').data('LINKTOQID'), -1, -1, -1, 0, 60, 4, Obj7Color);
				}

				if(typeof(inpObj.find('.Obj8Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('Obj8Ball', inpObj.data('QID'), inpObj.find('.Obj8Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 70, 2, Obj8Color);
				}

				if(typeof(inpObj.find('.Obj9Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('Obj9Ball', inpObj.data('QID'), inpObj.find('.Obj9Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 60, 2, Obj9Color);
				}

				if(typeof(inpObj.find('.Obj0Ball').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('Obj0Ball', inpObj.data('QID'), inpObj.find('.Obj0Ball').data('LINKTOQID'), -1, -1, -1, mobileWidth, 90, 2, Obj0Color);
				}

				if(typeof(inpObj.find('.ObjStarBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjStarBall', inpObj.data('QID'), inpObj.find('.ObjStarBall').data('LINKTOQID'), -1, -1, -1, 4, 78, 4, ObjStarColor);
				}

				if(typeof(inpObj.find('.ObjPoundBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjPoundBall', inpObj.data('QID'), inpObj.find('.ObjPoundBall').data('LINKTOQID'), -1, -1, -1, mobileWidth, 80, 2, ObjPoundColor);
				}

				if(typeof(inpObj.find('.ObjErrBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjErrBall', inpObj.data('QID'), inpObj.find('.ObjErrBall').data('LINKTOQID'), -1, -1, -1, 0, 110, 4, ObjErrColor);
				}

				if(typeof(inpObj.find('.ObjNRBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjNRBall', inpObj.data('QID'), inpObj.find('.ObjNRBall').data('LINKTOQID'), -1, -1, -1, 40, 124, 3, ObjNRColor);
				}
			}
			if(inpObj.data('RXT') == 24)
			{
				if(typeof(inpObj.find('.ObjLiveBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjLiveBall', inpObj.data('QID'), inpObj.find('.ObjLiveBall').data('LINKTOQID'), -1, -1, -1, 0, 40, 2, ObjLiveColor);
				}
				if(typeof(inpObj.find('.ObjMachineBall').data('LINKTOQID')) != "undefined" && typeof(inpObj.data('QID')) != "undefined")
				{
					LINKIVRObjects('ObjMachineBall', inpObj.data('QID'), inpObj.find('.ObjMachineBall').data('LINKTOQID'), -1, -1, -1, 0, 80, 2, ObjMachineColor);
				}
			}

			<!--- Add other objects here --->

			<!--- Loop over all objects that can link to this object --->
			$('.SBStageItem').each(function(index)
			 {
				if(typeof($(this).find('.ObjDefBall').data('LINKTOQID')) != "undefined")
				{	
					if($(this).find('.ObjDefBall').data('LINKTOQID') == INPQID)
					{
						var CURRRXT = $(this).data("RXT");
						<cfinclude template="js/RXT/js_DragObjPositions.cfm">
						if (CURRRXT == 21) {
							LINKINVRObjectSwitch('ObjDefBall', $(this).data('QID'), INPQID, -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
						} else
						<!--- LINKIVRObjects('ObjDefBall', $(this).data('QID'), INPQID, -1, -1, -1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor); --->
						LINKIVRObjects('ObjDefBall', $(this).data('QID'), INPQID, -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
					}
				}

				<!--- Add other objects here --->
				if($(this).data('RXT') == 2)
				{
					if(typeof($(this).find('.Obj1Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj1Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj1Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 24, 4, Obj1Color); --->
							LINKIVRObjects('Obj1Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 20, 4, Obj1Color);
						}
					}

					if(typeof($(this).find('.Obj2Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj2Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj2Ball', $(this).data('QID'), INPQID, -1, -1, -1, 71, 33, 2, Obj2Color); --->
							LINKIVRObjects('Obj2Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 30, 2, Obj2Color);
						}
					}

					if(typeof($(this).find('.Obj3Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj3Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj3Ball', $(this).data('QID'), INPQID, -1, -1, -1, 72, 24, 2, Obj3Color); --->
							LINKIVRObjects('Obj3Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 20, 2, Obj3Color);
						}
					}

					if(typeof($(this).find('.Obj4Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj4Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj4Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 42, 4, Obj4Color); --->
							LINKIVRObjects('Obj4Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 40, 4, Obj4Color);
						}
					}

					if(typeof($(this).find('.Obj5Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj5Ball').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('Obj5Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 50, 2, Obj5Color);	
						}
					}

					if(typeof($(this).find('.Obj6Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj6Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj6Ball', $(this).data('QID'), INPQID, -1, -1, -1, 71, 42, 2, Obj6Color); --->
							LINKIVRObjects('Obj6Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 40, 2, Obj6Color);
						}
					}

					if(typeof($(this).find('.Obj7Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj7Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj7Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 60, 4, Obj7Color); --->
							LINKIVRObjects('Obj7Ball', $(this).data('QID'), INPQID, -1, -1, -1, 0, 60, 4, Obj7Color);
						}
					}

					if(typeof($(this).find('.Obj8Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj8Ball').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('Obj8Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 70, 2, Obj8Color);
						}
					}

					if(typeof($(this).find('.Obj9Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj9Ball').data('LINKTOQID') == INPQID)
						{
							<!--- LINKIVRObjects('Obj9Ball', $(this).data('QID'), INPQID, -1, -1, -1, 72, 60, 2, Obj9Color); --->
							LINKIVRObjects('Obj9Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 60, 2, Obj9Color);
						}
						<!--- alert("92"); --->
					}

					if(typeof($(this).find('.Obj0Ball').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.Obj0Ball').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('Obj0Ball', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 90, 2, Obj0Color);
						}
					}

					if(typeof($(this).find('.ObjStarBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjStarBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjStarBall', $(this).data('QID'), INPQID, -1, -1, -1, 4, 78, 4, ObjStarColor);
						}
					}

					if(typeof($(this).find('.ObjPoundBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjPoundBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjPoundBall', $(this).data('QID'), INPQID, -1, -1, -1, mobileWidth, 80, 2, ObjPoundColor);
						}
					}

					if(typeof($(this).find('.ObjErrBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjErrBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjErrBall', $(this).data('QID'), INPQID, -1, -1, -1, 0, 110, 4, ObjErrColor);
						}
					}

					if(typeof($(this).find('.ObjNRBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjNRBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjNRBall', $(this).data('QID'), INPQID, -1, -1, -1, 40, 124, 3, ObjNRColor);
						}
					}
				}
				if($(this).data('RXT') == 24)
				{
					if(typeof($(this).find('.ObjLiveBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjLiveBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjLiveBall', $(this).data('QID'), INPQID, -1, -1, -1, 0, 40, 2, ObjLiveColor);
						}
					}
					if(typeof($(this).find('.ObjMachineBall').data('LINKTOQID')) != "undefined")
					{
						if($(this).find('.ObjMachineBall').data('LINKTOQID') == INPQID)
						{
							LINKIVRObjects('ObjMachineBall', $(this).data('QID'), INPQID, -1, -1, -1, 0, 80, 2, ObjMachineColor);
						}
					}
				}
			 });
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstop", function(event, ui) 
		{
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top);

			<!--- Update object on finish drag - limit to script builder stage items--->
			if(ui.helper.hasClass('SBStageItem'))
			{
				UpdateXYPOS(INPQID, ui.position.left, ui.position.top);
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		inpObj.bind( "dragstart", function(event, ui) 
		{
			if(!editCampaignPermission){
				alert(permissionMessage);
				return false;
			}
			inpObj.data('XPOS', ui.position.left);
			inpObj.data('YPOS', ui.position.top - 75);
		});

		<!--- Add draggable to default handle--->
		$('#QID_' + INPQID + ' .ObjDefBall' ).draggable(
		{
			containment: "#SBStage",
			revert: true,
			revertDuration: 100 
			 // revert:  "invalid"
		});

		<!--- jquery bug fix / kludge - nest draggables grab all in IE--->
		$('#QID_' + INPQID + ' .ObjDefBall' ).mousedown(function(e)
		{
			if($.browser.msie) {
				 e.stopPropagation();
			}
		});

		<!--- Redraw line while dragging  - events are start, drag, stop --->
		$('#QID_' + INPQID + ' .ObjDefBall' ).bind( "drag", function(event, ui) 
		{
			var CURRRXT = $('#QID_' + INPQID).data("RXT");
			<cfinclude template="js/RXT/js_DragObjPositions.cfm">

			<!--- LINKIVRObjects('ObjDefBall', INPQID, -1, ui.position.left, ui.position.top, 1, ObjDefX+22, ObjDefY+14, 2, ObjDefColor); --->
			LINKIVRObjects('ObjDefBall', INPQID, -1, ui.position.left, ui.position.top, 1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
		});

		<!--- Erase last line after stop --->
		$('#QID_' + INPQID + ' .ObjDefBall' ).bind( "dragstart", function(event, ui) 
		{
			$('#QID_' + INPQID).draggable( "option", "disabled", true );
		});

		<!--- Erase last line after stop --->
		$('#QID_' + INPQID + ' .ObjDefBall' ).bind( "dragstop", function(event, ui) 
		{
			$('#QID_' + INPQID).draggable( "option", "disabled", false );

			<!--- DynamicLINKs is 0 for erase TempObj Line only--->
			LINKIVRObjects('ObjDefBall', INPQID, -1, -1, -1, 0, 999, 150, 2, ObjDefColor);
		});

		<!--- Define what to do if default  input object is dropped onto stage --->
//		$('#QID_' + INPQID + ' .ObjDropZone' ).droppable(
		$('#QID_' + INPQID + ' .imgObjBackground' ).droppable(
		{
			<!--- Custom fuction to accept both menu items and stage objects --->
		//	accept:  function(d) { 	if( d.hasClass("ObjDefBall") || d.hasClass("ObjDefBall")) return true; },
			greedy: true,
			accept: '.ObjDefBall, .Obj1Ball, .Obj2Ball, .Obj3Ball, .Obj4Ball, .Obj5Ball, .Obj6Ball, .Obj7Ball, .Obj8Ball, .Obj9Ball, .Obj0Ball, .ObjStarBall, .ObjPoundBall, .ObjErrBall, .ObjNRBall , .ObjLiveBall, .ObjMachineBall', 
			over:
				 function(event, ui)
				 {
				 	$('#QID_' + INPQID + ' .imgObjBackground' ).css("opacity","0.5");
				 },
			out:
				function(event, ui)
				{
					$('#QID_' + INPQID + ' .imgObjBackground' ).css("opacity","1");
				},
			drop: 
				function( event, ui ) 
				{
					if(!editCampaignPermission){
						alert(permissionMessage);
						return false;
					}
					$('#QID_' + INPQID + ' .imgObjBackground' ).css("opacity","1");
					<!--- Default Output is dropped--->
					if( ui.draggable.hasClass('ObjDefBall') )
					{
						var inputType = null;
						var mcidType = ui.draggable.parent().data('RXT');
						switch(parseInt(mcidType))
						{
							<!--- change type next QID CK by tranglt --->
							case 3:
							case 7:
								inputType = "CK8";
								break;
						    case 13:
						    case 22:
								inputType = "CK15";
								break;
							case 21:
								inputType = "CK1";
								break;
							default:
								inputType = "CK5";
								break;	
						}
						<!------>
						var CURRRXT = ui.draggable.parent().data("RXT");


						//alert if no exists line between two RXT 21 or CONV to RXT 21 
						if($(this).parent().data('RXT') == 21){
							  if(CURRRXT == 21 || CURRRXT == 22){
							    jAlert("Can't existed line connect between two SWITCH or CONV to SWITCH.", "Warning");	
							    return;		
							  }
						}
						//No Connection between DM  and other MCID obj
					    if($(this).parent().data('RXT') == 23){
								return;
						}
						if (CURRRXT == 21) {
							var inpNewValue = $(this).parent().data('QID');
							var INPQID1 = ui.draggable.parent().data('QID');

							var caseValue = '';

							var MenuEditStr = "<div class='button'><button type='button' id='" + INPQID + "' class='CancelSelectValue' title='Cancel Changes to Select value'>Cancel</button>" +
								"<button type='submit' id='SaveSelectValue_" + INPQID + "' class='SaveSelectValue' title='Save Changes to Select value' >Save</button></div>";
							

								var $html = $('<div></div>').append('<div></div>');
								var option = '<label>Case Value: </label>';
								option += '<input type="text" name="txtcasevalue" id="txtcasevalue" maxlength="1024">';
								$html = $('<div></div>').append('<div class="casevalue">' + option + '</div>');
								//fix bug enter key close top dialog
								$html.keyup(function(e) {
								    if (e.keyCode == 13) {
								        $('.ui-dialog-buttonpane button:first').click();
								    }
								});
						        $html.dialog({
										dialogClass: 'formedit noTitleStuff',
										
										title: "Change QID",
										modal : true,
										buttons: [
										    {
										        text: "OK",
										        click: function() {
										        	if ($(this).find('#txtcasevalue').val()) {
										        		caseValue = $(this).find('#txtcasevalue').val().toUpperCase();
										        	} else {
										        		 caseValue = '';
										        	}
										        	UpdateKeyValue(INPQID1, inputType, inpNewValue, caseValue);
										  
										        	$(this).remove();
										        	if($('#QID_' + ui.draggable.parent().data('QID')).data('RXT')== 21){
										        		  if(caseValue.toLowerCase() == 'Default'.toLowerCase()){
										        		    caseValue='';
										        		  }
                       							          putTextLoadFirst(INPQID1,inpNewValue,caseValue,2);
			                                        }
										        }
										    }
										]
									});	
								<!---
								$.getJSON( '<cfoutput>#rootUrl#/#SessionPath#</cfoutput>/campaign/mcid/cfc/mcidtools.cfc?method=getValueSwitch&returnformat=json&queryformat=column&_cf_noDEBUG=true&_cf_nocache=true',  
								{  
									INPBATCHID : <cfoutput>#INPBATCHID#</cfoutput>,
									INPQID : INPQID1
								}, 
								<!--- Default return function for Do CFTE Demo - Async call back --->
								function(d) 
								{
									<!--- Alert if failure --->
										<!--- Get row 1 of results if exisits--->
										if (d.ROWCOUNT > 0)
										{
											<!--- Check if variable is part of JSON result string --->
											var option = '<label>Case Value: </label>';
											option += '<select id="selection" name="selection" title="">';
											var locationkey = d.DATA.LOCALTIONKEY;
											for (value in locationkey) {
												option += '<option>'+ locationkey[value] + '</option>';
											}
											option += '<option value="default">Default</option>';
											option += '</select>';
											option += '<br /><label>or</label> <input type="text" name="txtcasevalue" id="txtcasevalue" maxlength="3">';
											selection = option;
											$html = $('<div></div>').append('<div class="casevalue"><form >' + option + '</form></div>');
											$html.dialog({
												dialogClass: 'formedit noTitleStuff',
												
												title: "Change QID",
												modal : true,
												buttons: [
												    <!---{
												        text: "Cancel",
												        click: function() { $(this).remove(); }
												    },--->
												    {
												        text: "OK",
												        click: function() {
												        	if ($(this).find('#txtcasevalue').val()) {
												        		caseValue = $(this).find('#txtcasevalue').val().toUpperCase();
												        	} else {
												        		caseValue = $(this).find('#selection').val();
												        	}
												        	UpdateKeyValue(INPQID1, inputType, inpNewValue, caseValue);
												        	$(this).remove(); 
												        	if($('#QID_' + ui.draggable.parent().data('QID')).data('RXT')== 21){
                         							           putTextLoadFirst(INPQID1,inpNewValue,caseValue);
					                                        }
					                                        $(this).dialog('destroy'); 
															$(this).remove(); 
												        }
												    }
												]
											});	
										}
										else
										{
											<!--- No result returned --->
											$.alerts.okButton = '&nbsp;OK&nbsp;';
											jAlert("Error.", "No Response from the remote server. Check your connection and try again.");
										}
								});	
								--->						
						} else { 
							//not allow line MCID Start=MCID End
							if(ui.draggable.parent().data('QID') != $(this).parent().data('QID')){
								for(index in listRXT21){
				       			var arr = $('#QID_' + listRXT21[index]).data('RXTLINKARR');
					       			if (is_array(arr)) {
						       			for(i in arr){    				
						       				//not allow ELE inside SWITCH line to parent of it(SWITCH)
						       				if( arr[0] ==  $(this).parent().data('QID') && ui.draggable.parent().data('QID') == arr[i]){
						       				    return;
						       				}
							       		}
					       			}
       						    } 
							    UpdateKeyValue(ui.draggable.parent().data('QID'), inputType, $(this).parent().data('QID'),'');	
							}		
						}
						
						<cfinclude template="js/RXT/js_DragObjPositions.cfm">


						<!--- check RXT 21 switch 
						if (CURRRXT == 21) {
							LINKINVRObjectSwitch('ObjDefBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, 1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
						}
		--->
						LINKIVRObjects('ObjDefBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, ObjDefX+30, ObjDefY+14, 2, ObjDefColor);
					}
					//not allow draw from number 0-9 to switch (RXT 21) or to DM (RXT 23)
					//check not allow the numeric key draw up and out back onto itself
                    if($(this).parent().data('RXT') != 21 && $(this).parent().data('RXT') != 23 && ui.draggable.parent().data('QID') != $(this).parent().data('QID')){
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj1Ball'))
						{
							LINKIVRObjects('Obj1Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 0, 20, 4, Obj1Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "1", $(this).parent().data('QID'));
						}
						
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj2Ball'))
						{
							LINKIVRObjects('Obj2Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 30, 2, Obj2Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "2", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj3Ball'))
						{
							LINKIVRObjects('Obj3Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 20, 2, Obj3Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "3", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj4Ball'))
						{
							LINKIVRObjects('Obj4Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 0, 40, 4, Obj4Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "4", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj5Ball'))
						{
							LINKIVRObjects('Obj5Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 50, 2, Obj5Color);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "5", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj6Ball'))
						{
							LINKIVRObjects('Obj6Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 40, 2, Obj6Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "6", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj7Ball'))
						{
							LINKIVRObjects('Obj7Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 0, 60, 4, Obj7Color);
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "7", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj8Ball'))
						{
							LINKIVRObjects('Obj8Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 70, 2, Obj8Color);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "8", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj9Ball'))
						{
							LINKIVRObjects('Obj9Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 60, 2, Obj9Color);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "9", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('Obj0Ball'))
						{
							LINKIVRObjects('Obj0Ball', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 90, 2, Obj0Color);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "0", $(this).parent().data('QID'));
						}
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjStarBall'))
						{
							LINKIVRObjects('ObjStarBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 4, 78, 4, ObjStarColor);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "*", $(this).parent().data('QID'));
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjPoundBall'))
						{
							LINKIVRObjects('ObjPoundBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 80, 2, ObjPoundColor);	
							UpdateAnswerMapValue(ui.draggable.parent().data('QID'), "#", $(this).parent().data('QID'));
						}
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjLiveBall'))
						{
							LINKIVRObjects('ObjLiveBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 40, 2, ObjLiveColor);	
							UpdateKeyValue(ui.draggable.parent().data('QID'), "CK1", $(this).parent().data('QID'),'');
						}
						<!--- Default Output is dropped--->
					    if( ui.draggable.hasClass('ObjMachineBall'))
						{
							LINKIVRObjects('ObjMachineBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, mobileWidth, 80, 2, ObjMachineColor);	
							UpdateKeyValue(ui.draggable.parent().data('QID'), "CK2", $(this).parent().data('QID'),'');
						}
					}
					//not allow red line,yellow line to DM obj
					if($(this).parent().data('RXT') != 23 && ui.draggable.parent().data('QID') != $(this).parent().data('QID')){
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjErrBall'))
						{
							LINKIVRObjects('ObjErrBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 0, 110, 4, ObjErrColor);	
							UpdateKeyValue(ui.draggable.parent().data('QID'), "CK7", $(this).parent().data('QID'),'');
						}
	
						<!--- Default Output is dropped--->
						if( ui.draggable.hasClass('ObjNRBall'))
						{
							LINKIVRObjects('ObjNRBall', ui.draggable.parent().data('QID'),$(this).parent().data('QID'), -1, -1, -1, 40, 124, 3, ObjNRColor);	
							UpdateKeyValue(ui.draggable.parent().data('QID'), "CK6", $(this).parent().data('QID'),'');
						}
					}
				}
		});

		<!--- Bind draggables --->
		switch(parseInt(inpRXT))
		{
			case 1:
				break;
			case 2:
				BindToBallObject(INPQID, 'Obj1Ball', 0, 20, 4, Obj1Color);
				BindToBallObject(INPQID, 'Obj4Ball', 0, 40, 4, Obj4Color);
				BindToBallObject(INPQID, 'Obj7Ball', 0, 60, 4, Obj7Color);
				BindToBallObject(INPQID, 'Obj2Ball', mobileWidth, 30, 2, Obj2Color);
				BindToBallObject(INPQID, 'Obj5Ball', mobileWidth, 50, 2, Obj5Color);
				BindToBallObject(INPQID, 'Obj8Ball', mobileWidth, 70, 2, Obj8Color);
				BindToBallObject(INPQID, 'Obj0Ball', mobileWidth, 90, 2, Obj0Color);
				BindToBallObject(INPQID, 'Obj3Ball', mobileWidth, 20, 2, Obj3Color);
				BindToBallObject(INPQID, 'Obj6Ball', mobileWidth, 40, 2, Obj6Color);
				BindToBallObject(INPQID, 'Obj9Ball', mobileWidth, 60, 2, Obj9Color);
				BindToBallObject(INPQID, 'ObjStarBall', 0, 78, 4, ObjStarColor);
				BindToBallObject(INPQID, 'ObjPoundBall', mobileWidth, 80, 2, ObjPoundColor);
				BindToBallObject(INPQID, 'ObjErrBall', 0, 107, 4, ObjErrColor);
				BindToBallObject(INPQID, 'ObjNRBall', 40, 124, 3, ObjNRColor);	
				break;
		   case 24:
		   		BindToBallObject(INPQID, 'ObjLiveBall', 0, 40, 2, ObjLiveColor);
				BindToBallObject(INPQID, 'ObjMachineBall', 0, 80, 2, ObjMachineColor);
			default:
				break;
		}
	}

</script>

<cfoutput>




<!--- <div id="viewport"> --->
	<div id="SBContents">
		<cfinclude template="dsp_scriptbuilderMenuBar.cfm">
	</div>

<!--- </div> --->


<div id="CURRRXSSXML" style="display:none;">
	<!--- Remove BR's on final storage --->
	#HTMLCodeFormat("<RXSS>")#
	<BR>
	#HTMLCodeFormat("</RXSS>")#	
</div>





<div>

</div> 



<input type="hidden" name="CURRRawRXSSXML" id="CURRRawRXSSXML" VALUE="<RXSS></RXSS>" />

<div id="loading">
	<img src="<cfoutput>#rootUrl#/#PublicPath#</cfoutput>/images/loading-small.gif" width="32" height="32">
</div>

</cfoutput>

<cfinclude template="js/RXT/js_addBuildScriptForm.cfm">
<cfinclude template="js/RXT/js_addSwitchScriptForm.cfm">
<cfinclude template="js/RXT/js_addConversionScriptForm.cfm">

<div id="RXT1Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT1.cfm">
</div>

<div id="RXT2Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT2.cfm">
</div>

<div id="RXT3Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT3.cfm">
</div>

<div id="RXT4Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT4.cfm">
</div>

<div id="RXT5Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT5.cfm">
</div>

<div id="RXT6Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT6.cfm">
</div>

<div id="RXT7Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT7.cfm">
</div>

<!--- Deprecated
<div id="RXT8Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT8.cfm">
</div>--->

<div id="RXT9Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT9.cfm">
</div>

<div id="RXT10Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT10.cfm">
</div>

<div id="RXT11Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT11.cfm">
</div>

<div id="RXT12Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT12.cfm">
</div>

<div id="RXT13Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT13.cfm">
</div>

<div id="RXT14Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT14.cfm">
</div>

<div id="RXT15Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT15.cfm">
</div>

<div id="RXT16Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT16.cfm">
</div>

<div id="RXT17Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT17.cfm">
</div>

<div id="RXT18Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT18.cfm">
</div>

<div id="RXT19Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT19.cfm">
</div>

<div id="RXT20Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT20.cfm">
</div>

<div id="RXT21Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT21.cfm">
</div>
<div id="RXT22Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT22.cfm">
</div>
<div id="RXT24Template" style=" visibility:hidden">
	<cfinclude template="RXT/dsp_edit_RXT24.cfm">
</div>
<div id="ChangeQIDPopup" style="">
	<div id="popuptop" class="popup">
		You change this QID into 1.
	</div>
	<div id="popupbottom" class="popup">
		You change this QID into QID end.
	</div>
	<div id="popupIncrease" class="popup">
		You change this QID increased 1.
	</div>
	<div id="popupDecrease" class="popup">
		You change this QID decreased 1.
	</div>
	<div id="popupnotmove" class="popup">
		Can't move Qid lower 1.
	</div>
	<div id="popupchangeQID" class="popup">
		<!------><a id='MoveQidToTop' class='QIDRXT' title='Move this QID to Top'>Top</a>
		<a id='MoveQidToBottom' class='QIDRXT' title='Move this QID to Bottom'>Bottom</a>
		<a id='IncreaseQid' class='QIDRXT' title='Add 1 to QID'>+1</a>
		<a id='DecreaseQid' class='QIDRXT' title='Minus 1 to QID'>-1</a>
	</div>
	<div id="switchValue" class="popup">


	</div>
</div>



<div id="RULETYPE1Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE1.cfm">
</div>

<div id="RULETYPE2Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE2.cfm">
</div>

<div id="RULETYPE3Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE3.cfm">
</div>

<div id="RULETYPE4Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE4.cfm">
</div>

<div id="RULETYPE5Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE5.cfm">
</div>

<div id="RULETYPE6Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE6.cfm">
</div>

<!--- <div id="RULETYPE7Template" style=" visibility:hidden">
	<cfinclude template="ruletypes/dsp_edit_RULETYPE7.cfm">
</div> --->

<script type="text/javascript">
	<!--- Hide Templates - no need for animation--->
	$("#RXT1Template").hide();
	$("#RXT2Template").hide();
	$("#RXT3Template").hide();
	$("#RXT4Template").hide();
	$("#RXT5Template").hide();
	$("#RXT6Template").hide();
	$("#RXT7Template").hide();
<!---	deprectated $("#RXT8Template").hide();     --->
	$("#RXT9Template").hide();
	$("#RXT10Template").hide();
	$("#RXT11Template").hide();
	$("#RXT12Template").hide();
	$("#RXT13Template").hide();
	$("#RXT14Template").hide();
	$("#RXT15Template").hide();
	$("#RXT16Template").hide();
	$("#RXT17Template").hide();
	$("#RXT18Template").hide();
	<!---form add--->
	$("#RXT19Template").hide();
	$("#RXT20Template").hide();
	$("#RXT21Template").hide();
	$("#RXT22Template").hide();   
	$("#RXT24Template").hide();     
	$("#ChangeQIDPopup").hide();

	<!--- Load page data --->
//	 GetMCIDs();
//	 ReadvmdmData(1);
//	 ReadCCDXMLSTRING();
</script>


<!--- 
<script type="text/javascript">
	var speed = 250 // greater is slower;
	var menuBar = document.getElementById('SBMenuBar');
	var container=document.getElementById('MC_Stage_MCIDgen');
	function stayHome(){
		//var nV = 0;
		//nV = container.scrollTop;

		//		menuBar.style.top = nV+"px";
		//nV = parseInt($(container).css("top").substring(0,$(container).css("top").indexOf("px")))
		var offset = $(container).scrollTop()+"px"; 
		if($(container).scrollTop()>=600){
			offset=600+"px";
		}

		$(menuBar).animate({top:offset},{duration:100,queue:false});  
		setTimeout("stayHome()",100);
	}
	function confirmChange(a) {
		jAlert("Changing Location Key means removing all ELE of this Switching out of Campaign!","Warning.");
	}
	$(document).ready(function() {
		stayHome();
	});	
	
</script>
 --->
